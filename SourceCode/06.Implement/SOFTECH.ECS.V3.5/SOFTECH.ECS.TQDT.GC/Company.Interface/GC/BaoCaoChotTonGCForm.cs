﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.VNACCS.Vouchers;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Company.Interface.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.Interface.GC
{
    public partial class BaoCaoChotTonGCForm : BaseFormHaveGuidPanel
    {
        public string LoaiHangHoa = "1";
#if SXXK_V4
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#elif GC_V4
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#endif
        public Company.KDT.SHARE.VNACCS.KDT_VNACC_ToKhaiMauDich TKMD;
        public KDT_VNACCS_TotalInventoryReport totalInventoryReport = new KDT_VNACCS_TotalInventoryReport();
        public KDT_VNACCS_TotalInventoryReport_ContractReference contractReference = new KDT_VNACCS_TotalInventoryReport_ContractReference();
        public KDT_VNACCS_TotalInventoryReport_Detail totalInventoryReport_Detail = new KDT_VNACCS_TotalInventoryReport_Detail();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public bool isAddContract = true;
        public bool isAdd = true;
        public BaoCaoChotTonGCForm()
        {
            InitializeComponent();
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void SetCommandStatus()
        {
            if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTiepNhan.Text = totalInventoryReport.SoTiepNhan.ToString();
                clcNgayTN.Value = totalInventoryReport.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                clcNgayTN.Value = DateTime.Now;
                lblTrangThai.Text = totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTiepNhan.Text = totalInventoryReport.SoTiepNhan.ToString();
                clcNgayTN.Value = totalInventoryReport.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã khai báo Sửa / Hủy", "Wait for approval");
                this.OpenType = OpenFormType.View;
            }
            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;


                txtSoTiepNhan.Text = totalInventoryReport.SoTiepNhan.ToString();
                clcNgayTN.Value = totalInventoryReport.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA || totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = totalInventoryReport.SoTiepNhan.ToString();
                clcNgayTN.Value = totalInventoryReport.NgayTiepNhan;
                lblTrangThai.Text = totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Not declared") : setText("Chờ duyệt Sửa / Hủy", "Wait for approval");
                this.OpenType = OpenFormType.View;
            }


        }
        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdSend":
                    this.SendV5(false);
                    break;
                case "cmdCancel":
                    if (ShowMessage("Doanh nghiệp có chắc chắn muốn Khai báo Hủy báo cáo chốt tồn này đến HQ không ? ", true) == "Yes")
                    {
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                        totalInventoryReport.Update();
                        SetCommandStatus();
                        this.SendV5(true);
                    }
                    break;
                case "cmdEdit":
                    totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    this.SendV5(false);
                    break;
                case "cmdFeedback":
                    this.FeedBackV5();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                case "cmdAdd":
                    this.Add();
                    break;
                case "cmdChuyenTT":
                    ChuyenTT();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;
            }
        }
        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = totalInventoryReport.ID;
                f.loaiKhaiBao = LoaiKhaiBao.TotalInventoryReport;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", totalInventoryReport.ID, LoaiKhaiBao.TotalInventoryReport), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageBCCT(totalInventoryReport);
                    totalInventoryReport.InsertUpdate();
                    ShowMessage("Cập nhật thông tin thành công .", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ChuyenTT()
        {
            try
            {
                if (ShowMessage("Doanh nghiệp có chắc chắn muốn Chuyển báo cáo chốt tồn này sang Khai báo sửa không ? ", true) == "Yes")
                {
                    totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    totalInventoryReport.Update();
                    SetCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Add()
        {
            BaoCaoChotTonHangHoaForm f = new BaoCaoChotTonHangHoaForm();
            f.OpenType = this.OpenType;
            f.totalInventoryReport = totalInventoryReport;
            f.LoaiHangHoa = totalInventoryReport.LoaiBaoCao == 1 ? "NPL" : "SP";
            f.ShowDialog(this);
            BindDataContract();
        }
        private void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_TotalInventoryReport", "", Convert.ToInt32(totalInventoryReport.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = this.totalInventoryReport.ID;
                form.DeclarationIssuer = DeclarationIssuer.TotalInventoryReport_GC;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = totalInventoryReport.GuidString;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.TotalInventoryReport_GC,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.TotalInventoryReport_GC,
                };
                subjectBase.Type = DeclarationIssuer.TotalInventoryReport_GC;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = totalInventoryReport.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(totalInventoryReport.MaHaiQuan.Trim())),
                                                  Identity = totalInventoryReport.MaHaiQuan
                                              }, subjectBase, null);
                if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(totalInventoryReport.MaHaiQuan));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                    {
                        totalInventoryReport.Update();
                        isFeedBack = true;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        totalInventoryReport.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        totalInventoryReport.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }
        }

        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.TotalInventoryReportNewSendHandler(totalInventoryReport, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void SendV5(bool IsCancel)
        {
            if (ShowMessage("Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (totalInventoryReport.ID == 0)
                {
                    ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    totalInventoryReport.ContractCollection = KDT_VNACCS_TotalInventoryReport_ContractReference.SelectCollectionBy_TotalInventory_ID(totalInventoryReport.ID);
                    foreach (KDT_VNACCS_TotalInventoryReport_ContractReference contractReference in totalInventoryReport.ContractCollection)
                    {
                        contractReference.DetailCollection = KDT_VNACCS_TotalInventoryReport_Detail.SelectCollectionBy_ContractReference_ID(contractReference.ID);
                    }
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ScrapInformation;
                sendXML.master_id = totalInventoryReport.ID;

                if (sendXML.Load())
                {
                    if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                    {
                        ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    totalInventoryReport.GuidString = Guid.NewGuid().ToString();
                    GoodsItems_VNACCS totalInventoryReport_VNACCS = new GoodsItems_VNACCS();
                    totalInventoryReport_VNACCS = Mapper_V4.ToDataTransferTotalInventoryReportNew(totalInventoryReport, totalInventoryReport_Detail, GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = totalInventoryReport.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(totalInventoryReport.MaHaiQuan)),
                              Identity = totalInventoryReport.MaHaiQuan
                          },
                          new SubjectBase()
                          {

                              Type = totalInventoryReport_VNACCS.Issuer,
                              //Function = DeclarationFunction.SUA,
                              Reference = totalInventoryReport.GuidString,
                          },
                          totalInventoryReport_VNACCS
                        );
                    if (totalInventoryReport.TrangThaiXuLy==TrangThaiXuLy.CHUA_KHAI_BAO || totalInventoryReport.TrangThaiXuLy==TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if(IsCancel)
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    }
                    else if (totalInventoryReport.TrangThaiXuLy==TrangThaiXuLy.SUATKDADUYET)
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(totalInventoryReport.ID, MessageTitle.RegisterTotalInventoryReport);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.TotalInventoryReport;
                        sendXML.master_id = totalInventoryReport.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = Convert.ToInt32(totalInventoryReport_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            totalInventoryReport.Update();
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                            FeedBackV5();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            SetCommandStatus();
                            FeedBackV5();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            totalInventoryReport.Update();
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            ShowMessageTQDT(msgInfor, false);
                            FeedBackV5();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        sendForm.Message.XmlSaveMessage(totalInventoryReport.ID, MessageTitle.RegisterTotalInventoryReport);
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        private void BindDataContract()
        {
            try
            {
                dgListHopDong.Refetch();
                dgListHopDong.DataSource = totalInventoryReport.ContractCollection;
                dgListHopDong.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Save()
        {
            try
            {
                if (totalInventoryReport.ContractCollection.Count == 0)
                {
                    ShowMessage("Doanh nghiệp chưa Thêm hợp đồng để đưa vào Báo cáo chốt tồn ",false);
                    return;
                }
                totalInventoryReport.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text);
                totalInventoryReport.NgayTiepNhan = clcNgayTN.Value;
                totalInventoryReport.MaHaiQuan = ctrCoQuanHaiQuan.Code;
                totalInventoryReport.MaDoanhNghiep = txtMaDN.Text;
                if (totalInventoryReport==null || totalInventoryReport.ID==0)
                    totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                totalInventoryReport.NgayChotTon = clcNgayChotTon.Value;
                totalInventoryReport.LoaiBaoCao = Convert.ToDecimal(cbbQuy.SelectedValue.ToString());
                totalInventoryReport.LoaiSua = Convert.ToDecimal(cbbType.SelectedValue.ToString());
                totalInventoryReport.InsertUpdateFull();
                ShowMessage("Lưu thành công ", false);
                BindDataContract();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void BaoCaoChotTonGCForm_Load(object sender, EventArgs e)
        {
            if (totalInventoryReport == null || totalInventoryReport.ID == 0)
            {
                txtMaDN.Text = GlobalSettings.MA_DON_VI;
                txtTenDN.Text = GlobalSettings.TEN_DON_VI;
                txtDiaChi.Text = GlobalSettings.DIA_CHI;
                clcNgayTN.Value = DateTime.Now;
                clcNgayChotTon.Value = DateTime.Now;
                cbbQuy.SelectedValue = "1";
                ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                cbbType.SelectedIndex = 0;
                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                SetCommandStatus();
            }
            else
            {
                txtSoTiepNhan.Text = totalInventoryReport.SoTiepNhan.ToString();
                clcNgayTN.Value = totalInventoryReport.NgayTiepNhan;
                txtMaDN.Text = totalInventoryReport.MaDoanhNghiep;
                txtTenDN.Text = GlobalSettings.TEN_DON_VI;
                txtDiaChi.Text = GlobalSettings.DIA_CHI;
                ctrCoQuanHaiQuan.Code = totalInventoryReport.MaHaiQuan;
                clcNgayChotTon.Value = totalInventoryReport.NgayChotTon;
                cbbQuy.SelectedValue = totalInventoryReport.LoaiBaoCao.ToString();
                cbbType.SelectedValue = totalInventoryReport.LoaiBaoCao.ToString();
                totalInventoryReport.ContractCollection = KDT_VNACCS_TotalInventoryReport_ContractReference.SelectCollectionBy_TotalInventory_ID(totalInventoryReport.ID);
                foreach (KDT_VNACCS_TotalInventoryReport_ContractReference contractReference in totalInventoryReport.ContractCollection)
                {
                    contractReference.DetailCollection = KDT_VNACCS_TotalInventoryReport_Detail.SelectCollectionBy_ContractReference_ID(contractReference.ID);
                }
                BindDataContract();
            }
        }

        private void dgListHopDong_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Add();              
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH HỢP ĐỒNG BÁO CÁO CHỐT TỒN.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgListHopDong;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
