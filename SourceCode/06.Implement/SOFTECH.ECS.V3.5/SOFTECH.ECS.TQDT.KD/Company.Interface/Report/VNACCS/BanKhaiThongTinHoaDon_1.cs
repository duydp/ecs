using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class BanKhaiThongTinHoaDon_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public BanKhaiThongTinHoaDon_1()
        {
            InitializeComponent();
        }
        public void BindReport(VAL0870_IVA Val087)
        {
            #region old
            //lblSoTiepNhanHoaDon.Text = Val087.NIV.GetValue().ToString();
            //lblPhanLoaiXuatNhapKhau.Text = Val087.YNK.GetValue().ToString();
            //lblMaDaiLyHaiQuan.Text = Val087.TII.GetValue().ToString();
            //lblSoHoaDon.Text = Val087.IVN.GetValue().ToString();
            //if (Convert.ToDateTime(Val087.IVD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            //    lblNgayLapHoaDon.Text = Convert.ToDateTime(Val087.IVD.GetValue()).ToString("dd/MM/yyyy");
            //else
            //    lblNgayLapHoaDon.Text = "";
            //lblDiaDiemLapHoaDon.Text = Val087.IVB.GetValue().ToString();
            //lblPhuongThucThanhToan.Text = Val087.HOS.GetValue().ToString();
            //lblMaNguoiXuatNhapKhau.Text = Val087.IMC.GetValue().ToString();
            //lblTenNguoiXuatNhapKhau.Text = Val087.IMN.GetValue().ToString();
            //lblMaBuuChinhXuatNhapKhau.Text = Val087.IMY.GetValue().ToString();
            //lblDiaChiXuatNhapKhau.Text = Val087.IMA.GetValue().ToString();
            //lblSoDienThoaiXuatNhapKhau.Text = Val087.IMT.GetValue().ToString();
            //lblNguoiLapHoaDon.Text = Val087.SAA.GetValue().ToString();
            //lblMaNguoiNhanGui.Text = Val087.EPC.GetValue().ToString();
            //lblTenNguoiNhanGui.Text = Val087.EPN.GetValue().ToString();
            //lblMaBuuChinhNguoiNhanGui.Text = Val087.EP1.GetValue().ToString();
            //lblDiaChiNguoiNhanGui1.Text = Val087.EPA.GetValue().ToString();
            //lblDiaChiNguoiNhanGui2.Text = Val087.EP2.GetValue().ToString();
            //lblDiaChiNguoiNhanGui3.Text = Val087.EP3.GetValue().ToString();
            //lblDiaChiNguoiNhanGui4.Text = Val087.EP4.GetValue().ToString();
            //lblSoDienThoaiNguoiNhanGui.Text = Val087.EP6.GetValue().ToString();
            //lblNuocSoTaiNguoiNhanGui.Text = Val087.EEE.GetValue().ToString();
            //lblMaKyHieu.Text = Val087.KNO.GetValue().ToString();
            ////   lblPhanLoaiVanChuyen.Text = Val087.TTP.GetValue().ToString();
            //lblTenPhuongTienVanChuyen.Text = Val087.VSS.GetValue().ToString();
            ////  lblSoHieuChuyenDi.Text = Val087.VNO.GetValue().ToString();
            //lblMaDiaDiemXepHang.Text = Val087.PSC.GetValue().ToString();
            //lblTenDiaDiemXepHang.Text = Val087.PSN.GetValue().ToString();
            //if (Convert.ToDateTime(Val087.PSD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            //    lblThoiKyXepHang.Text = Convert.ToDateTime(Val087.PSD.GetValue()).ToString("dd/MM/yyyy");
            //else
            //    lblThoiKyXepHang.Text = "";
            //lblMaDiaDiemDoHang.Text = Val087.DST.GetValue().ToString();
            //lblTenDiaDiemDoHang.Text = Val087.DCN.GetValue().ToString();
            //lblMaDiaDiemTrungChuyen.Text = Val087.KYC.GetValue().ToString();
            //lblTenDiaDiemTrungChuyen.Text = Val087.KYN.GetValue().ToString();
            #endregion

            try
            {
                lblTongTrongLuongHangGross.Text = Val087.GWJ.GetValue().ToString();
                lblMaDonViTinhTongTrongLuong.Text = Val087.GWT.GetValue().ToString();
                lblTrongLuongThuan.Text = Val087.JWJ.GetValue().ToString();
                lblMaDonViTinhTrongLuongThuan.Text = Val087.JWT.GetValue().ToString();
                lblTongTheTich.Text = Val087.STJ.GetValue().ToString();
                lblMaDonViTinhTongTheTich.Text = Val087.STT.GetValue().ToString();
                lblTongSoKienHang.Text = Val087.NOJ.GetValue().ToString();
                lblMaDonViTinhTongSoKienHang.Text = Val087.NOT.GetValue().ToString();
                lblGhiChuCuaChuHang.Text = Val087.NT3.GetValue().ToString();
                lblSoPL.Text = Val087.PLN.GetValue().ToString();
                lblNganHangLC.Text = Val087.LCB.GetValue().ToString();
                lblTriGiaFOB.Text = Val087.FON.GetValue().ToString();
                lblMTTCuaFOB.Text = Val087.FOT.GetValue().ToString();
                lblSoTienDieuChinhFOB.Text = Val087.FKK.GetValue().ToString();
                lblMTTSoTienDieuChinhFOB.Text = Val087.FKT.GetValue().ToString();
                lblPhiVanChuyen.Text = Val087.FR3.GetValue().ToString();
                lblMTTPhiVanChuyen.Text = Val087.FR2.GetValue().ToString();
                lblNoiThanhToanPhiVanChuyen.Text = Val087.FR4.GetValue().ToString();
                lblChiPhiXepHang1.Text = Val087.FS1.GetValue().ToString();
                lblMTTChiPhiXepHang1.Text = Val087.FT1.GetValue().ToString();
                lblLoaiChiPhiXepHang1.Text = Val087.FH1.GetValue().ToString();
                lblChiPhiXepHang2.Text = Val087.FS2.GetValue().ToString();
                lblMTTChiPhiXepHang2.Text = Val087.FT2.GetValue().ToString();
                lblLoaiChiPhiXepHang2.Text = Val087.FH2.GetValue().ToString();
                lblPhiVanChuyenDuongBoNoiDia.Text = Val087.NUH.GetValue().ToString();
                lblMTTPhiVanChuyenDuongBoNoiDia.Text = Val087.NTK.GetValue().ToString();
                lblPhiBaoHiem.Text = Val087.IN3.GetValue().ToString();
                lblMTTCuaPhiBaoHiem.Text = Val087.IN2.GetValue().ToString();
                lblSoTienDieuChinhPhiBaoHiem.Text = Val087.IK1.GetValue().ToString();
                lblMTTSoTienDieuChinhPhiBH.Text = Val087.IK2.GetValue().ToString();
                lblSoTienKhauTru.Text = Val087.NBG.GetValue().ToString();
                lblMTTCuaSoTienKhauTru.Text = Val087.NBC.GetValue().ToString();
                lblLoaiKhauTru.Text = Val087.NBS.GetValue().ToString();
                lblSoTienDieuChinhKhac.Text = Val087.SKG.GetValue().ToString();
                lblMTTSoTienDieuChinhKhac.Text = Val087.SKC.GetValue().ToString();
                lblLoaiSoTienDieuChinhKhac.Text = Val087.SKS.GetValue().ToString();
                lblTongTriGiaHoaDon.Text = Val087.IP4.GetValue().ToString();
                lblMTTTongTriGiaHoaDon.Text = Val087.IP3.GetValue().ToString();
                lblDieuKienGiaHoaDon.Text = Val087.IP2.GetValue().ToString();
                lblDiaDiemGiaoHang.Text = Val087.HWT.GetValue().ToString();
                lblGhiChuDacBiet.Text = Val087.NT1.GetValue().ToString();
                lblTongSoDongHang.Text = Val087.SNM.GetValue().ToString();


                if (Val087.HangHoaTrongHoaDon != null && Val087.HangHoaTrongHoaDon.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("SoDong", typeof(string));
                    dt.Columns.Add("MaHangHoa", typeof(string));
                    dt.Columns.Add("MaSoHangHoa", typeof(string));
                    dt.Columns.Add("MoTaHangHoa", typeof(string));
                    dt.Columns.Add("MaNuocXuatXu", typeof(string));
                    dt.Columns.Add("TenNuocXuatXu", typeof(string));
                    dt.Columns.Add("SoKienHang", typeof(string));
                    dt.Columns.Add("SoLuong1", typeof(string));
                    dt.Columns.Add("MaDVTSoLuong1", typeof(string));
                    dt.Columns.Add("SoLuong2", typeof(string));
                    dt.Columns.Add("MaDVTSoLuong2", typeof(string));
                    dt.Columns.Add("DonGiaHoaDon", typeof(string));
                    dt.Columns.Add("MTTDonGiaHoaDon", typeof(string));
                    dt.Columns.Add("DonViDonGiaHoaDon", typeof(string));
                    dt.Columns.Add("TriGiaHoaDon", typeof(string));
                    dt.Columns.Add("MTTGiaTien", typeof(string));
                    dt.Columns.Add("LoaiKhauTru_Hang", typeof(string));
                    dt.Columns.Add("SoTienKhauTru_Hang", typeof(string));
                    dt.Columns.Add("MTTSoTienKhauTru_Hang", typeof(string));


                    for (int i = 0; i < Val087.HangHoaTrongHoaDon.Count; i++)
                    {

                        DataRow dr = dt.NewRow();
                        dr["SoDong"] = Val087.HangHoaTrongHoaDon[i].RNO.Value.ToString().Trim();
                        dr["MaHangHoa"] = Val087.HangHoaTrongHoaDon[i].SNO.Value.ToString().Trim();
                        dr["MaSoHangHoa"] = Val087.HangHoaTrongHoaDon[i].CMD.Value.ToString().Trim();
                        dr["MoTaHangHoa"] = Val087.HangHoaTrongHoaDon[i].CMN.Value.ToString().Trim();
                        dr["MaNuocXuatXu"] = Val087.HangHoaTrongHoaDon[i].ORC.Value.ToString().Trim();
                        dr["TenNuocXuatXu"] = Val087.HangHoaTrongHoaDon[i].ORN.Value.ToString().Trim();
                        dr["SoKienHang"] = Val087.HangHoaTrongHoaDon[i].KBN.Value.ToString().Trim();
                        dr["SoLuong1"] = Val087.HangHoaTrongHoaDon[i].QN1.Value.ToString().Trim();
                        dr["MaDVTSoLuong1"] = Val087.HangHoaTrongHoaDon[i].QT1.Value.ToString().Trim();
                        dr["SoLuong2"] = Val087.HangHoaTrongHoaDon[i].QN2.Value.ToString().Trim();
                        dr["MaDVTSoLuong2"] = Val087.HangHoaTrongHoaDon[i].QT2.Value.ToString().Trim();
                        dr["DonGiaHoaDon"] = Val087.HangHoaTrongHoaDon[i].TNK.Value.ToString().Trim();
                        dr["MTTDonGiaHoaDon"] = Val087.HangHoaTrongHoaDon[i].TNC.Value.ToString().Trim();
                        dr["DonViDonGiaHoaDon"] = Val087.HangHoaTrongHoaDon[i].TSC.Value.ToString().Trim();
                        dr["TriGiaHoaDon"] = Val087.HangHoaTrongHoaDon[i].KKT.Value.ToString().Trim();
                        dr["MTTGiaTien"] = Val087.HangHoaTrongHoaDon[i].KKC.Value.ToString().Trim();
                        dr["LoaiKhauTru_Hang"] = Val087.HangHoaTrongHoaDon[i].NSR.Value.ToString().Trim();
                        dr["SoTienKhauTru_Hang"] = Val087.HangHoaTrongHoaDon[i].NBG.Value.ToString().Trim();
                        dr["MTTSoTienKhauTru_Hang"] = Val087.HangHoaTrongHoaDon[i].NBC.Value.ToString().Trim();
                        dt.Rows.Add(dr);
                    }
                    BindReportHang(dt);

                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }




        }

        private void BindReportHang(DataTable dt)
        {
            DetailReport1.DataSource = dt;
            lblSoDong.DataBindings.Add("Text", DetailReport.DataSource, "SoDong"); ;
            lblMaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaHangHoa");
            lblMaSoHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaSoHangHoa");
            lblMoTaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MoTaHangHoa");
            lblMaNuocXuatXu.DataBindings.Add("Text", DetailReport.DataSource, "MaNuocXuatXu");
            lblTenNuocXuatXu.DataBindings.Add("Text", DetailReport.DataSource, "TenNuocXuatXu");
            lblSoKienHang.DataBindings.Add("Text", DetailReport.DataSource, "SoKienHang");
            lblSoLuong1.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong1");
            lblMaDVTSoLuong1.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTSoLuong1");
            lblSoLuong2.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong2");
            lblMaDVTSoLuong2.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTSoLuong2");
            lblDonGiaHoaDon.DataBindings.Add("Text", DetailReport.DataSource, "DonGiaHoaDon");
            lblMTTDonGiaHoaDon.DataBindings.Add("Text", DetailReport.DataSource, "MTTDonGiaHoaDon");
            lblDonViDonGiaHoaDon.DataBindings.Add("Text", DetailReport.DataSource, "DonViDonGiaHoaDon");
            lblTriGiaHoaDon.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaHoaDon");
            lblMTTGiaTien.DataBindings.Add("Text", DetailReport.DataSource, "MTTGiaTien");
            lblLoaiKhauTru_Hang.DataBindings.Add("Text", DetailReport.DataSource, "LoaiKhauTru_Hang");
            lblSoTienKhauTru_Hang.DataBindings.Add("Text", DetailReport.DataSource, "SoTienKhauTru_Hang");
            lblMTTSoTienKhauTru_Hang.DataBindings.Add("Text", DetailReport.DataSource, "MTTSoTienKhauTru_Hang");

        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
        }
        private void cell_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            if (cell.Text.Trim() == "0")
                cell.Text = "";
        }
    }
}
