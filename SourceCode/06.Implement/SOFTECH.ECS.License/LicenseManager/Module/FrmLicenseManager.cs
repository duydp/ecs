using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmLicenseManager : DevExpress.XtraEditors.XtraForm
    {
        public FrmLicenseManager()
        {
            InitializeComponent();
        }

        private void FrmLicenseManager_Load(object sender, EventArgs e)
        {
            this.RefreshData();
        }

        #region Adding Method
        private void DisplayGridHeader()
        {
            gridData.Columns["ID"].Caption = "Mã bản quyền";
            gridData.Columns["Name"].Caption = "Tên bản quyền";
            gridData.Columns["Description"].Caption = "Mô tả";
            gridData.Columns["NumberClient"].Caption = "Số máy";
            gridData.Columns["Term"].Caption = "Thời hạn";
        }

        private void OpenForm(string id, bool isEdit)
        {
            if (!ClassLib.IsFormShowed("Lic_" + id))
            {
                FrmLicenseInfo obj = new FrmLicenseInfo(id);
                obj.MdiParent = this.MdiParent;
                obj.isEdit = isEdit;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Lic_" + id].Activate();
            }
        }

        public void RefreshData()
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(Activation.Server.License.SelectAll().Tables[0].Copy());
            ds.Tables[0].TableName = "tblLicense";
            ds.Tables.Add(Activation.Server.Serial.SelectAll().Tables[0].Copy());
            ds.Tables[1].TableName = "tblSerial";

            ds.Relations.Add("Danh sách Serial", ds.Tables["tblLicense"].Columns["ID"], ds.Tables["tblSerial"].Columns["LicenseID"]);

            gridLicense.DataSource = ds.Tables[0];
            this.DisplayGridHeader();
        }
        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!ClassLib.IsFormShowed("Lic_New"))
            {
                FrmLicenseInfo obj = new FrmLicenseInfo(null);
                obj.MdiParent = this.MdiParent;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Lic_New"].Activate();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    this.OpenForm(ID, true);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    string Message = "Bạn có muốn xóa bản quyền: " + ID;
                    if (MessageBox.Show(Message, "Xóa bản quyền", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Activation.Server.License.DeleteLicense(ID);
                    }
                }
            }
            this.RefreshData();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            gridData.ClearColumnsFilter();
            gridData.ClearGrouping();
            gridData.ClearSorting();
            this.RefreshData();
        }

        private void gridLicense_DoubleClick(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    this.OpenForm(ID, false);
                }
            }
        }

    }
}