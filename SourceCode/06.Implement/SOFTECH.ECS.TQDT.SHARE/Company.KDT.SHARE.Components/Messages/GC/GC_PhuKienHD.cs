﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    //Phụ kiện hợp đồng
   public class GC_PhuKienHD:DeclarationBase
    {
       //[XmlElement("status")]
       //public string Status { get; set; }
       [XmlElement("ContractReference")]
       public AdditionalDocument ContractReference { get; set; }
       [XmlElement("Subcontract")]
       public Subcontract SubContract { get; set; }
       [XmlElement("AdditionalInformation")]
       public List<AdditionalInformation> AdditionalInformations { get; set; }


    }
}
