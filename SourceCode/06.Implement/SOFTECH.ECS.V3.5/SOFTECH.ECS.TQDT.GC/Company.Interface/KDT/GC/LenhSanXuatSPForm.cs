﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.GC
{
    public partial class LenhSanXuatSPForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public KDT_LenhSanXuat lenhSanXuat = new KDT_LenhSanXuat();
        public KDT_LenhSanXuat_SP lenhSanXuatSP = new KDT_LenhSanXuat_SP();
        public HopDong HD;
        public bool isAdd = true;
        public bool isExits = false;
        public List<GC_DinhMuc> DinhMucCollection = new List<GC_DinhMuc>();
        public String Caption;
        public bool IsChange;
        public LenhSanXuatSPForm()
        {
            InitializeComponent();
        }
        private bool Validate(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaDDSX, errorProvider, "LỆNH SẢN XUẤT", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcTuNgay, errorProvider, "TỪ NGÀY", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcDenNgay, errorProvider, "ĐẾN NGÀY", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbTinhTrang, errorProvider, "TÌNH TRẠNG", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void BindDataSP()
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = lenhSanXuat.SPCollection;
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                this.IsChange = true;
                this.Text = Caption + " * ";
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void Get()
        {
            try
            {
                lenhSanXuat.HopDong_ID = HD.ID;
                lenhSanXuat.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                lenhSanXuat.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                lenhSanXuat.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                lenhSanXuat.SoLenhSanXuat = txtMaDDSX.Text.ToString();
                lenhSanXuat.TuNgay = new DateTime(clcTuNgay.Value.Year, clcTuNgay.Value.Month, clcTuNgay.Value.Day, 00, 00, 00);
                lenhSanXuat.DenNgay = new DateTime(clcDenNgay.Value.Year, clcDenNgay.Value.Month, clcDenNgay.Value.Day, 23, 59, 59);
                lenhSanXuat.SoDonHang = txtPO.Text.ToString();
                lenhSanXuat.TinhTrang = Convert.ToInt32(cbbTinhTrang.SelectedValue.ToString());
                lenhSanXuat.GhiChu = txtGhiChu.Text.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Set()
        {
            try
            {
                txtMaDDSX.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaDDSX.Text = lenhSanXuat.SoLenhSanXuat;
                txtMaDDSX.TextChanged += new EventHandler(txt_TextChanged);

                clcTuNgay.TextChanged -= new EventHandler(txt_TextChanged);
                clcTuNgay.Value = lenhSanXuat.TuNgay;
                clcTuNgay.TextChanged += new EventHandler(txt_TextChanged);

                clcDenNgay.TextChanged -= new EventHandler(txt_TextChanged);
                clcDenNgay.Value = lenhSanXuat.DenNgay;
                clcDenNgay.TextChanged += new EventHandler(txt_TextChanged);

                txtPO.TextChanged -= new EventHandler(txt_TextChanged);
                txtPO.Text = lenhSanXuat.SoDonHang;
                txtPO.TextChanged += new EventHandler(txt_TextChanged);

                cbbTinhTrang.TextChanged -= new EventHandler(txt_TextChanged);
                cbbTinhTrang.SelectedValue = lenhSanXuat.TinhTrang;
                cbbTinhTrang.TextChanged += new EventHandler(txt_TextChanged);

                txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                txtGhiChu.Text = lenhSanXuat.GhiChu;
                txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Validate(false))
                    return;
                Get();
                foreach (KDT_LenhSanXuat item in KDT_LenhSanXuat.SelectCollectionDynamic("HopDong_ID = " + HD.ID + " AND ID NOT IN (" + lenhSanXuat.ID + ")", ""))
                {
                    if (item.SoLenhSanXuat == txtMaDDSX.Text.ToString())
                    {
                        ShowMessageTQDT("Thông báo từ Hệ thống", String.Format("LỆNH SẢN XUẤT NÀY ĐÃ ĐƯỢC NHẬP LIỆU TRƯỚC ĐÓ VỚI THÔNG TIN  \r\nID - TỪ NGÀY - ĐẾN NGÀY \r\n{0} - {1} - {2} \n ", item.ID, item.TuNgay.ToString("dd/MM/yyyy"), item.DenNgay.ToString("dd/MM/yyyy")), false);
                        return;
                    }
                }
                foreach (KDT_LenhSanXuat item in KDT_LenhSanXuat.SelectCollectionDynamic("HopDong_ID = " + HD.ID + "AND ID NOT IN (" + lenhSanXuat.ID + ")", ""))
                {
                    item.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
                    foreach (KDT_LenhSanXuat_SP sp in item.SPCollection)
                    {
                        foreach (KDT_LenhSanXuat_SP temp in lenhSanXuat.SPCollection)
                        {
                            if (sp.MaSanPham == temp.MaSanPham)
                            {
                                if (lenhSanXuat.TuNgay >= item.TuNgay && lenhSanXuat.TuNgay <= item.DenNgay || lenhSanXuat.DenNgay >= item.TuNgay && lenhSanXuat.DenNgay <= item.DenNgay || lenhSanXuat.TuNgay <= item.TuNgay && lenhSanXuat.DenNgay >= item.DenNgay)
                                {
                                    ShowMessageTQDT("Thông báo từ Hệ thống", String.Format("LỆNH SẢN XUẤT CỦA SP NÀY CÓ THÔNG TIN TỪ NGÀY VÀ ĐẾN NGÀY NẰM TRONG KHOẢNG TỪ NGÀY VÀ ĐẾN NGÀY CỦA LỆNH SẢN XUẤT KHÁC CỦA SP NÀY TRƯỚC ĐÓ VỚI THÔNG TIN  \r\n ID - SẢN PHẨM - LỆNH SẢN XUẤT TRƯỚC - TỪ NGÀY - ĐẾN NGÀY - SẢN PHẨM - LỆNH SẢN XUẤT HIỆN TẠI - TỪ NGÀY - ĐẾN NGÀY  \r\n - {0} - {1} - {2} - {3} - {4} - {5} - {6} - {7} - {8} \n ", item.ID, temp.MaSanPham , item.SoLenhSanXuat, item.TuNgay.ToString("dd/MM/yyyy"), item.DenNgay.ToString("dd/MM/yyyy"),sp.MaSanPham ,lenhSanXuat.SoLenhSanXuat, lenhSanXuat.TuNgay.ToString("dd/MM/yyyy"), lenhSanXuat.DenNgay.ToString("dd/MM/yyyy")), false);
                                    return;
                                }
                            }
                        }
                    }
                }
                if (lenhSanXuat.SPCollection.Count == 0)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "DOANH NGHIỆP CHƯA NHẬP DANH SÁCH SẢN PHẨM", false);
                    return;
                }
                lenhSanXuat.InsertUpdateFull();
                // KIỂM TRA VÀ CẬP NHẬT LỆNH SẢN XUẤT ĐỐI VỚI SP ĐÃ CÓ KHAI BÁO ĐỊNH MỨC TRƯỚC ĐÓ
                DinhMucCollection = GC_DinhMuc.SelectCollectionBy_HopDong_ID(HD.ID);
                foreach (GC_DinhMuc item in DinhMucCollection)
                {
                    foreach (KDT_LenhSanXuat_SP sp in lenhSanXuat.SPCollection)
                    {
                        if (sp.MaSanPham == item.MaSanPham)
                        {
                            item.LenhSanXuat_ID = lenhSanXuat.ID;
                            item.UpdateLenhSanXuat();
                        }
                    }
                }
                txtMaDDSX.ReadOnly = true;
                ShowMessageTQDT("Thông báo từ hệ thống", "Lưu thành công", false);
                this.SetChange(false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void LenhSanXuatSPForm_Load(object sender, EventArgs e)
        {
            Caption = this.Text;
            if (lenhSanXuat.ID > 0)
            {
                txtMaDDSX.ReadOnly = true;
                btnGet.Enabled = false;
                Set();
                BindDataSP();
                HD = new HopDong();
                HD = HopDong.Load(Convert.ToInt64(lenhSanXuat.HopDong_ID));
            }
            else
            {
                cbbTinhTrang.TextChanged -= new EventHandler(txt_TextChanged);
                cbbTinhTrang.SelectedValue = "0";
                cbbTinhTrang.TextChanged += new EventHandler(txt_TextChanged);

                clcTuNgay.TextChanged -= new EventHandler(txt_TextChanged);
                clcTuNgay.Value = HD.NgayKy;
                clcTuNgay.TextChanged += new EventHandler(txt_TextChanged);

                clcDenNgay.TextChanged -= new EventHandler(txt_TextChanged);
                clcDenNgay.Value = HD.NgayGiaHan.Year == 1900 || HD.NgayGiaHan.Year == 1 ? HD.NgayHetHan : HD.NgayGiaHan;
                clcDenNgay.TextChanged += new EventHandler(txt_TextChanged);
            }
            txtSoHopDong.Text = HD.SoHopDong;
            clcNgayHĐ.Value = HD.NgayKy;
            clcNgayHH.Value = HD.NgayGiaHan.Year == 1900 || HD.NgayGiaHan.Year == 1 ? HD.NgayHetHan : HD.NgayGiaHan;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(grListSP.RootTable.Columns["MaSanPham"], ConditionOperator.Contains, txtMaSP.Text);
                grListSP.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                String where = " HopDong_ID = " + HD.ID + " AND Ma NOT IN ('',";
                Company.Interface.GC.SanPhamRegistedForm SPRegistedForm = new Company.Interface.GC.SanPhamRegistedForm();
                SPRegistedForm.isBrower = true;
                SPRegistedForm.isDisplayAll = 3;
                SPRegistedForm.SanPhamSelected.HopDong_ID = HD.ID;
                foreach (KDT_LenhSanXuat_SP item in lenhSanXuat.SPCollection)
                {
                    where += "'" + item .MaSanPham + "',";
                }
                where = where.Substring(0, where.Length - 1);
                where += ")";
                SPRegistedForm.whereCondition = where;
                SPRegistedForm.ShowDialog(this);
                if (SPRegistedForm.SanPhamSelectCollection.Count >= 1)
                {
                    foreach (Company.GC.BLL.GC.SanPham item in SPRegistedForm.SanPhamSelectCollection)
                    {
                        KDT_LenhSanXuat_SP lenhSanXuatSP = new KDT_LenhSanXuat_SP();
                        lenhSanXuatSP.MaSanPham = item.Ma;
                        lenhSanXuatSP.TenSanPham = item.Ten;
                        lenhSanXuatSP.MaHS = item.MaHS;
                        lenhSanXuatSP.DVT_ID = item.DVT_ID;
                        lenhSanXuat.SPCollection.Add(lenhSanXuatSP);
                    }
                    this.SetChange(true);
                }
                BindDataSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDeleteSP_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                List<KDT_LenhSanXuat_SP> ItemColl = new List<KDT_LenhSanXuat_SP>();
                if (grListSP.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_LenhSanXuat_SP)i.GetRow().DataRow);
                        }
                    }
                    foreach (KDT_LenhSanXuat_SP item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        lenhSanXuat.SPCollection.Remove(item);
                    }
                    DinhMucCollection = GC_DinhMuc.SelectCollectionBy_HopDong_ID(HD.ID);
                    foreach (GC_DinhMuc item in DinhMucCollection)
                    {
                        foreach (KDT_LenhSanXuat_SP sp in ItemColl)
                        {
                            if (sp.MaSanPham == item.MaSanPham && item.LenhSanXuat_ID == lenhSanXuat.ID)
                            {
                                item.LenhSanXuat_ID = 0;
                                item.UpdateLenhSanXuat();
                            }
                        }
                    }
                    this.SetChange(true);
                    BindDataSP();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grListSP_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private string GetConfigAuto(String TinhTrang)
        {
            try
            {
                String LenhSanXuat = "";
                String FirstStringLenhSanXuat = "";
                String LastStringLenhSanXuat = "";
                int lenghtFirst;
                int lenghtLast;
                int lenghtIndex;
                List<KDT_LenhSanXuat_QuyTac> ConfigCollection = KDT_LenhSanXuat_QuyTac.SelectCollectionBy_HopDong_ID(HD.ID);

                foreach (KDT_LenhSanXuat_QuyTac item in ConfigCollection)
                {
                    string Value = "";
                    switch (TinhTrang)
                    {
                        case "0":
                            Value = "M";
                            break;
                        case "1":
                            Value = "Đ";
                            break;
                        case "2":
                            Value = "H";
                            break;
                        default:
                            break;
                    }
                    if (item.TinhTrang == Value)
                    {
                        lenghtFirst = item.TienTo.Length + item.TinhTrang.Length + item.LoaiHinh.Length + Convert.ToInt32(item.DoDaiSo)-1;
                        lenghtLast = item.HienThi.Length - lenghtFirst;
                        FirstStringLenhSanXuat = item.HienThi.Substring(0, lenghtFirst - 1);
                        LastStringLenhSanXuat = item.HienThi.Substring(lenghtFirst, lenghtLast);
                        string GetLenhSanXuat = KDT_LenhSanXuat_QuyTac.GetLenhSanXuat(TinhTrang);
                        if (!String.IsNullOrEmpty(GetLenhSanXuat))
                        {
                            if (GetLenhSanXuat.Length >= item.HienThi.Length)
                            {
                                if (GetLenhSanXuat.Substring(0, lenghtFirst -1) != FirstStringLenhSanXuat)
                                {
                                    LenhSanXuat = item.HienThi;
                                }
                                else
                                {
                                    string[] Temp = GetLenhSanXuat.Split(new string[] { "-" }, StringSplitOptions.None);
                                    string First = Temp[0].ToString();
                                    int LastIndex = Convert.ToInt32(First.Substring(lenghtFirst - 1, First.Length - (lenghtFirst - 1)));
                                    LenhSanXuat = FirstStringLenhSanXuat + (LastIndex + 1) + LastStringLenhSanXuat;
                                }
                            }
                            else
                            {
                                LenhSanXuat = item.HienThi;
                            }
                        }
                        else
                        {
                            LenhSanXuat = item.HienThi;
                        }
                    }
                }
                return LenhSanXuat;
            }
            catch (Exception ex)
            {
                return "";
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnGet_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(GetConfigAuto(cbbTinhTrang.SelectedValue.ToString())))
            {
                if (ShowMessageTQDT("HỢP ĐỒNG NÀY CHƯA CẤU HÌNH QUY TẮC TẠO LỆNH SẢN XUẤT CHO SẢN PHẨM .BẠN MUỐN CẤU HÌNH QUY TẮC CHO HỢP ĐỒNG NÀY KHÔNG ? ", true) == "Yes")
                {
                    QuyTacTaoLenhSanXuatForm f = new QuyTacTaoLenhSanXuatForm();
                    f.ShowDialog(this);
                }
                else
                {
                    return;
                }
            }
            else
            {
                txtMaDDSX.Text = GetConfigAuto(cbbTinhTrang.SelectedValue.ToString());
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH SẢN PHẨM CỦA LỆNH SẢN XUẤT - " + lenhSanXuat.SoLenhSanXuat.ToString() + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = grListSP;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void LenhSanXuatSPForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Đăng lý lệnh sản xuất có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    btnSave_Click(null,null);
                }
            }
        }
    }
}
