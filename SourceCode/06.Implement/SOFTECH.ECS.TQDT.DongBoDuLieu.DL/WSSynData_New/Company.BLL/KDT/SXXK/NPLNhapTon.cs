﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;

namespace Company.BLL.KDT.SXXK
{
    public partial class NPLNhapTon
    {
        public DataTable SelectNPLTon()
        {
            string spName = "p_KDT_SXXK_NPLNhapTon_SelectNPLTon";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
    }
}
