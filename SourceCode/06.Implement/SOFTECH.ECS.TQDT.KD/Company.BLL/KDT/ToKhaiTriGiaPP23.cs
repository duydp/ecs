﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.KD.BLL.Utils;
using System.Threading;
using Company.KD.BLL.KDT.SXXK;
using System.Collections;
using System.Text;
namespace Company.KD.BLL.KDT
{
    public partial class ToKhaiTriGiaPP23
    {

       
        
        #region Webservice của hải quan

        public XmlNode ConvertToXML()
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            XmlDocument doc = new XmlDocument();

            XmlElement nodeRoot = null;
            if(this.MaToKhaiTriGia==1)
                nodeRoot= doc.CreateElement("TKTG_PP2");            
            else
                nodeRoot = doc.CreateElement("TKTG_PP3");
            XmlAttribute attSTTHang = doc.CreateAttribute("STTHANG");
            attSTTHang.Value = this.STTHang.ToString();
            nodeRoot.Attributes.Append(attSTTHang);  

            XmlAttribute attToSo = doc.CreateAttribute("TO_SO");
            attToSo.Value = this.ToSo.ToString();
            nodeRoot.Attributes.Append(attToSo);                   

            XmlAttribute attNGAY_XK = doc.CreateAttribute("NGAY_XK");
            attNGAY_XK.Value = this.NgayXuat.ToString("MM/dd/yyyy");
            nodeRoot.Attributes.Append(attNGAY_XK);

            XmlAttribute attLOAI_TKTG = doc.CreateAttribute("LOAI_TKTG");
            attLOAI_TKTG.Value = "";
            nodeRoot.Attributes.Append(attLOAI_TKTG);

            XmlAttribute attTEN_HANG_TT = doc.CreateAttribute("TEN_HANG_TT");
            attTEN_HANG_TT.Value = this.TenHangTT;
            nodeRoot.Attributes.Append(attTEN_HANG_TT);

            XmlAttribute attSOTK_TT = doc.CreateAttribute("SOTK_TT");
            attSOTK_TT.Value = this.SoTKHangTT.ToString();
            nodeRoot.Attributes.Append(attSOTK_TT);

            XmlAttribute attNGAY_NK_TT = doc.CreateAttribute("NGAY_NK_TT");
            attNGAY_NK_TT.Value = this.NgayDangKyHangTT.ToString("MM/dd/yyyy");
            nodeRoot.Attributes.Append(attNGAY_NK_TT);

            XmlAttribute attMA_HQ_TT = doc.CreateAttribute("MA_HQ_TT");
            attMA_HQ_TT.Value = this.MaHaiQuanHangTT;
            nodeRoot.Attributes.Append(attMA_HQ_TT);

            XmlAttribute attMA_LH_TT = doc.CreateAttribute("MA_LH_TT");
            attMA_LH_TT.Value = this.MaLoaiHinhHangTT;
            nodeRoot.Attributes.Append(attMA_LH_TT);

            XmlAttribute attSTTHANG_TT = doc.CreateAttribute("STTHANG_TT");
            attSTTHANG_TT.Value = this.SoTKHangTT.ToString();
            nodeRoot.Attributes.Append(attSTTHANG_TT);

            XmlAttribute attNGAY_XK_TT = doc.CreateAttribute("NGAY_XK_TT");
            attNGAY_XK_TT.Value = this.NgayXuatTT.ToString("MM/dd/yyyy");
            nodeRoot.Attributes.Append(attNGAY_XK_TT);

            XmlAttribute attTGGD_TT = doc.CreateAttribute("TGGD_TT");
            attTGGD_TT.Value = this.TriGiaNguyenTeHangTT.ToString();
            nodeRoot.Attributes.Append(attTGGD_TT);

            XmlAttribute attDCC_CD_TM = doc.CreateAttribute("DCC_CD_TM");
            attDCC_CD_TM.Value = this.DieuChinhCongThuongMai.ToString(f);
            nodeRoot.Attributes.Append(attDCC_CD_TM);

            XmlAttribute attDCC_SL = doc.CreateAttribute("DCC_SL");
            attDCC_SL.Value = this.DieuChinhCongSoLuong.ToString(f);
            nodeRoot.Attributes.Append(attDCC_SL);

            XmlAttribute attDCC_CP_BOC_HANG = doc.CreateAttribute("DCC_CP_BOC_HANG");
            attDCC_CP_BOC_HANG.Value = this.DieuChinhCongChiPhiVanTai.ToString(f);
            nodeRoot.Attributes.Append(attDCC_CP_BOC_HANG);

            XmlAttribute attDCTru_CD_TM = doc.CreateAttribute("DCT_CD_TM");
            attDCTru_CD_TM.Value = this.DieuChinhTruCapDoThuongMai.ToString(f);
            nodeRoot.Attributes.Append(attDCTru_CD_TM);

            XmlAttribute attDCTru_SL = doc.CreateAttribute("DCT_SL");
            attDCTru_SL.Value = this.DieuChinhTruSoLuong.ToString(f);
            nodeRoot.Attributes.Append(attDCTru_SL);

            XmlAttribute attDCTru_CP_BOC_HANG = doc.CreateAttribute("DCT_CP_BOC_HANG");
            attDCTru_CP_BOC_HANG.Value = this.DieuChinhTruChiPhiVanTai.ToString(f);
            nodeRoot.Attributes.Append(attDCTru_CP_BOC_HANG);

            XmlAttribute attTG_NGTE = doc.CreateAttribute("TG_NGTE");
            attTG_NGTE.Value = this.TriGiaNguyenTeHang.ToString(f);
            nodeRoot.Attributes.Append(attTG_NGTE);

            XmlAttribute attTG_VND = doc.CreateAttribute("TG_VND");
            attTG_VND.Value = this.TriGiaTTHang.ToString(f);
            nodeRoot.Attributes.Append(attTG_VND);

            XmlAttribute attGIAI_TRINH = doc.CreateAttribute("GIAI_TRINH");
            attGIAI_TRINH.Value = this.GiaiTrinh;
            nodeRoot.Attributes.Append(attGIAI_TRINH);

            XmlAttribute attNGUOI_TN = doc.CreateAttribute("NGUOI_TN");
            if (this.MaToKhaiTriGia == 2)
                attNGUOI_TN.Value = "x";
            else
                attNGUOI_TN.Value = "";
            nodeRoot.Attributes.Append(attNGUOI_TN);

            XmlAttribute attNGAY_KB = doc.CreateAttribute("NGAY_KB");
            if (this.MaToKhaiTriGia == 2)
                attNGAY_KB.Value = DateTime.Today.ToString("MM/dd/yyyy");
            else
                attNGAY_KB.Value = "";
            nodeRoot.Attributes.Append(attNGAY_KB);

            XmlAttribute attNGUOI_KB = doc.CreateAttribute("NGUOI_KB");
            if (this.MaToKhaiTriGia == 2)
                attNGUOI_KB.Value = "x";
            else
                attNGUOI_KB.Value = "";
            nodeRoot.Attributes.Append(attNGUOI_KB);

            XmlAttribute attCHUC_DANH_KB = doc.CreateAttribute("CHUC_DANH_KB");
            if (this.MaToKhaiTriGia == 2)
                attCHUC_DANH_KB.Value = "x";
            else
                attCHUC_DANH_KB.Value = "";
            nodeRoot.Attributes.Append(attCHUC_DANH_KB);

            XmlAttribute attNGAY_TRUYEN = doc.CreateAttribute("NGAY_TRUYEN");
            attNGAY_TRUYEN.Value = "";
            nodeRoot.Attributes.Append(attNGAY_TRUYEN);

            XmlAttribute attMAU_TKTG = doc.CreateAttribute("MAU_TKTG");
            attMAU_TKTG.Value = "";
            if(this.MaToKhaiTriGia==2)
                attMAU_TKTG.Value = 3+"";
            nodeRoot.Attributes.Append(attMAU_TKTG);

            XmlAttribute attLYDO_KAD_PP1 = doc.CreateAttribute("LYDO_KAD_PP1");
            attLYDO_KAD_PP1.Value = this.LyDo;
            nodeRoot.Attributes.Append(attLYDO_KAD_PP1);

            XmlAttribute attLYDO_KAD_PP2 = doc.CreateAttribute("LYDO_KAD_PP2");
            attLYDO_KAD_PP2.Value = "x";
            if (this.MaToKhaiTriGia ==1)
                attMAU_TKTG.Value = "";
            nodeRoot.Attributes.Append(attLYDO_KAD_PP2);

            XmlAttribute attDCC_GIAM_GIA = doc.CreateAttribute("DCC_GIAM_GIA");
            attDCC_GIAM_GIA.Value = this.DieuChinhCongKhoanGiamGiaKhac.ToString(f);
            nodeRoot.Attributes.Append(attDCC_GIAM_GIA);

            XmlAttribute attDCC_BAO_HIEM= doc.CreateAttribute("DCC_BAO_HIEM");
            attDCC_BAO_HIEM.Value = this.DieuChinhCongChiPhiBaoHiem.ToString(f);
            nodeRoot.Attributes.Append(attDCC_BAO_HIEM);

            XmlAttribute attDCT_GIAM_GIA = doc.CreateAttribute("DCT_GIAM_GIA");
            attDCT_GIAM_GIA.Value = this.DieuChinhTruKhoanGiamGiaKhac.ToString(f);
            nodeRoot.Attributes.Append(attDCT_GIAM_GIA);

            XmlAttribute attDCT_BAO_HIEM = doc.CreateAttribute("DCT_BAO_HIEM");
            attDCT_BAO_HIEM.Value = this.DieuChinhTruChiPhiBaoHiem.ToString(f);
            nodeRoot.Attributes.Append(attDCT_BAO_HIEM);

            return nodeRoot;
        }

   
        #endregion Webservice của hải quan
     
    }
}