﻿namespace Company.Interface.KDT.GC
{
    partial class SelectPhuKienForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgPhuKien_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgPhuKien_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectPhuKienForm));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lblHint = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label16 = new System.Windows.Forms.Label();
            this.txtSoPK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.dgPhuKien = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPhuKien)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.dgPhuKien);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Size = new System.Drawing.Size(708, 333);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // lblHint
            // 
            this.lblHint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHint.AutoSize = true;
            this.lblHint.BackColor = System.Drawing.Color.Transparent;
            this.lblHint.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHint.ForeColor = System.Drawing.Color.Blue;
            this.lblHint.Location = new System.Drawing.Point(6, 15);
            this.lblHint.Name = "lblHint";
            this.lblHint.Size = new System.Drawing.Size(167, 14);
            this.lblHint.TabIndex = 31;
            this.lblHint.Text = "Hướng dẫn: Kích đôi để chọn";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(628, 11);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(74, 23);
            this.btnClose.TabIndex = 32;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.btnClose);
            this.uiGroupBox4.Controls.Add(this.lblHint);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 295);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(708, 38);
            this.uiGroupBox4.TabIndex = 338;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnSearch);
            this.uiGroupBox6.Controls.Add(this.label16);
            this.uiGroupBox6.Controls.Add(this.txtSoPK);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(708, 44);
            this.uiGroupBox6.TabIndex = 340;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(445, 16);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(89, 23);
            this.btnSearch.TabIndex = 87;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(33, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 86;
            this.label16.Text = "Số phụ kiện";
            // 
            // txtSoPK
            // 
            this.txtSoPK.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoPK.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoPK.BackColor = System.Drawing.Color.White;
            this.txtSoPK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoPK.Location = new System.Drawing.Point(126, 16);
            this.txtSoPK.Name = "txtSoPK";
            this.txtSoPK.Size = new System.Drawing.Size(313, 21);
            this.txtSoPK.TabIndex = 85;
            this.txtSoPK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoPK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoPK.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // dgPhuKien
            // 
            this.dgPhuKien.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgPhuKien.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgPhuKien.AlternatingColors = true;
            this.dgPhuKien.BorderStyle = Janus.Windows.GridEX.BorderStyle.RaisedLight3D;
            this.dgPhuKien.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgPhuKien.ColumnAutoResize = true;
            dgPhuKien_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgPhuKien_DesignTimeLayout_Reference_0.Instance")));
            dgPhuKien_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgPhuKien_DesignTimeLayout_Reference_0});
            dgPhuKien_DesignTimeLayout.LayoutString = resources.GetString("dgPhuKien_DesignTimeLayout.LayoutString");
            this.dgPhuKien.DesignTimeLayout = dgPhuKien_DesignTimeLayout;
            this.dgPhuKien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgPhuKien.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgPhuKien.GroupByBoxVisible = false;
            this.dgPhuKien.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgPhuKien.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgPhuKien.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgPhuKien.ImageList = this.ImageList1;
            this.dgPhuKien.Location = new System.Drawing.Point(0, 44);
            this.dgPhuKien.Name = "dgPhuKien";
            this.dgPhuKien.RecordNavigator = true;
            this.dgPhuKien.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgPhuKien.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgPhuKien.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgPhuKien.Size = new System.Drawing.Size(708, 251);
            this.dgPhuKien.TabIndex = 341;
            this.dgPhuKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgPhuKien.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgPhuKien_RowDoubleClick);
            // 
            // SelectPhuKienForm
            // 
            this.ClientSize = new System.Drawing.Size(708, 333);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "SelectPhuKienForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Danh sách phụ kiện";
            this.Load += new System.EventHandler(this.SelectPhuKienForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPhuKien)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.Label lblHint;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.GridEX dgPhuKien;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoPK;
    }
}
