﻿namespace Company.Interface.GC
{
    partial class BC02HSTK_GC_TT117_ExportExcelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgListNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BC02HSTK_GC_TT117_ExportExcelForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListNPL = new Janus.Windows.GridEX.GridEX();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnExportPDF = new Janus.Windows.EditControls.UIButton();
            this.btnPrint = new Janus.Windows.EditControls.UIButton();
            this.txtMinRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtMaxrow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.dgListNPL);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(1149, 544);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtMaxrow);
            this.uiGroupBox1.Controls.Add(this.txtMinRow);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnPrint);
            this.uiGroupBox1.Controls.Add(this.btnExportPDF);
            this.uiGroupBox1.Controls.Add(this.btnExportExcel);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 499);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1149, 45);
            this.uiGroupBox1.TabIndex = 9;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListNPL
            // 
            this.dgListNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPL.AlternatingColors = true;
            this.dgListNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPL.ColumnAutoResize = true;
            dgListNPL_DesignTimeLayout.LayoutString = resources.GetString("dgListNPL_DesignTimeLayout.LayoutString");
            this.dgListNPL.DesignTimeLayout = dgListNPL_DesignTimeLayout;
            this.dgListNPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPL.FrozenColumns = 3;
            this.dgListNPL.GroupByBoxVisible = false;
            this.dgListNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPL.Location = new System.Drawing.Point(0, 0);
            this.dgListNPL.Margin = new System.Windows.Forms.Padding(0);
            this.dgListNPL.Name = "dgListNPL";
            this.dgListNPL.RecordNavigator = true;
            this.dgListNPL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPL.Size = new System.Drawing.Size(1149, 499);
            this.dgListNPL.TabIndex = 10;
            this.dgListNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageIndex = 4;
            this.btnExportExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcel.Location = new System.Drawing.Point(929, 12);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(104, 25);
            this.btnExportExcel.TabIndex = 310;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageIndex = 4;
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(1039, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(104, 25);
            this.btnClose.TabIndex = 310;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnExportPDF
            // 
            this.btnExportPDF.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportPDF.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportPDF.Image = ((System.Drawing.Image)(resources.GetObject("btnExportPDF.Image")));
            this.btnExportPDF.ImageIndex = 4;
            this.btnExportPDF.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportPDF.Location = new System.Drawing.Point(819, 12);
            this.btnExportPDF.Name = "btnExportPDF";
            this.btnExportPDF.Size = new System.Drawing.Size(104, 25);
            this.btnExportPDF.TabIndex = 310;
            this.btnExportPDF.Text = "Xuất PDF";
            this.btnExportPDF.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportPDF.Click += new System.EventHandler(this.btnExportPDF_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageIndex = 4;
            this.btnPrint.ImageSize = new System.Drawing.Size(20, 20);
            this.btnPrint.Location = new System.Drawing.Point(12, 14);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(104, 25);
            this.btnPrint.TabIndex = 310;
            this.btnPrint.Text = "Xem";
            this.btnPrint.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // txtMinRow
            // 
            this.txtMinRow.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtMinRow.FormatString = "0";
            this.txtMinRow.Location = new System.Drawing.Point(501, 15);
            this.txtMinRow.Name = "txtMinRow";
            this.txtMinRow.Size = new System.Drawing.Size(68, 21);
            this.txtMinRow.TabIndex = 311;
            this.txtMinRow.Text = "0";
            this.txtMinRow.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtMinRow.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaxrow
            // 
            this.txtMaxrow.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtMaxrow.FormatString = "0";
            this.txtMaxrow.Location = new System.Drawing.Point(643, 15);
            this.txtMaxrow.Name = "txtMaxrow";
            this.txtMaxrow.Size = new System.Drawing.Size(63, 21);
            this.txtMaxrow.TabIndex = 1;
            this.txtMaxrow.Text = "0";
            this.txtMaxrow.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtMaxrow.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(451, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 312;
            this.label1.Text = "Từ Row";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(586, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 312;
            this.label2.Text = "Đến Row";
            // 
            // BC02HSTK_GC_TT117_ExportExcelForm
            // 
            this.ClientSize = new System.Drawing.Size(1149, 544);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BC02HSTK_GC_TT117_ExportExcelForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Báo cáo 02 BKTK";
            this.Load += new System.EventHandler(this.BC02HSTK_GC_TT117_ExportExcelForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX dgListNPL;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
        private Janus.Windows.EditControls.UIButton btnExportPDF;
        private Janus.Windows.EditControls.UIButton btnPrint;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtMaxrow;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtMinRow;

    }
}
