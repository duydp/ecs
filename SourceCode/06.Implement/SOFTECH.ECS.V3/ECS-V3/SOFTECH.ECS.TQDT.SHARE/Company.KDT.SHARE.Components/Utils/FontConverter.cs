﻿using System;

namespace Company.KDT.SHARE.Components.Utils
{
    public class FontConverter
    {
        public static string TCVN2Unicode(string source)
        {
            if (string.IsNullOrEmpty(source)) return string.Empty;
            string _unicodeTemplate = "áàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬÉÈẺẼẸÊẾỀỂỄỆÍÌỈĨỊÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴĐ";
            string _tcvn3Template = "¸µ¶·¹¨¾»¼½Æ©ÊÇÈÉËÐÌÎÏÑªÕÒÓÔÖÝ×ØÜÞãßáâä«èåæçé¬íêëìîóïñòô­øõö÷ùýúûüþ®¸µ¶·¹¡¡¡¡¡¡¢¢¢¢¢¢ÐÌÎÏÑ££££££Ý×ØÜÞãßáâä¤¤¤¤¤¤¥¥¥¥¥¥óïñòô¦¦¦¦¦¦ýúûüþ§";

            string result = String.Empty;
            double pos;
            foreach (char character in source)
            {
                if (character == '\r')
                {
                    result += '\r';
                }
                else if (character == '\n')
                {
                    result += '\n';
                }
                else
                {
                    pos = _tcvn3Template.IndexOf(character);
                    if (pos < 0)
                    {
                        result += character;
                    }
                    else
                    {
                        result += _unicodeTemplate.Substring((int) pos, 1);
                    }
                }
            }
            return result;
        }

        public static string Unicode2TCVN(string source)
        {
            string _unicodeTemplate = "áàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬÉÈẺẼẸÊẾỀỂỄỆÍÌỈĨỊÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴĐ";
            string _tcvn3Template = "¸µ¶·¹¨¾»¼½Æ©ÊÇÈÉËÐÌÎÏÑªÕÒÓÔÖÝ×ØÜÞãßáâä«èåæçé¬íêëìîóïñòô­øõö÷ùýúûüþ®¸µ¶·¹¡¡¡¡¡¡¢¢¢¢¢¢ÐÌÎÏÑ££££££Ý×ØÜÞãßáâä¤¤¤¤¤¤¥¥¥¥¥¥óïñòô¦¦¦¦¦¦ýúûüþ§";

            string result = String.Empty;
            double pos;
            foreach (char character in source)
            {
                if (character == '\r')
                {
                    result += '\r';
                }
                else if (character == '\n')
                {
                    result += '\n';
                }
                else
                {
                    pos = _unicodeTemplate.IndexOf(character);
                    if (pos < 0)
                    {
                        result += character;
                    }
                    else
                    {
                        result += _tcvn3Template.Substring((int)pos, 1);
                    }
                }
            }
            return result;
            //return source;
        }

        public static string UTF8Literal2Unicode(string source)
        {
            string returnString = source;
            // á à ả ã ạ
            returnString = returnString.Replace("Ã¡", "á");
            returnString = returnString.Replace("Ã ", "à");
            returnString = returnString.Replace("áº£", "ả");
            returnString = returnString.Replace("Ã£", "ã");
            returnString = returnString.Replace("áº¡", "ạ");
            // Á À Ả Ã Ạ
            returnString = returnString.Replace("Ã", "Á");
            returnString = returnString.Replace("Ã€", "À");
            returnString = returnString.Replace("áº¢", "Ả");
            returnString = returnString.Replace("Ãƒ", "Ã");
            returnString = returnString.Replace("áº ", "Ạ");
            // ă ắ ằ ẳ ẵ ặ
            returnString = returnString.Replace("Äƒ", "ă");
            returnString = returnString.Replace("áº¯", "ắ");
            returnString = returnString.Replace("áº±", "ắ");
            returnString = returnString.Replace("áº³", "ẳ");
            returnString = returnString.Replace("áºµ", "ẵ");
            returnString = returnString.Replace("áº·", "ặ");
            // Ă Ắ Ằ Ẳ Ẵ Ặ
            returnString = returnString.Replace("Ä‚", "Ă");
            returnString = returnString.Replace("áº®", "Ắ");
            returnString = returnString.Replace("áº°", "Ằ");
            returnString = returnString.Replace("áº²", "Ẳ");
            returnString = returnString.Replace("áº´", "Ẵ");
            returnString = returnString.Replace("áº¶", "Ặ");

            // â ấ ầ ẩ ẫ ậ
            returnString = returnString.Replace("Ã¢", "â");
            returnString = returnString.Replace("áº¥", "ấ");
            returnString = returnString.Replace("áº§", "ầ");
            returnString = returnString.Replace("áº©", "ẩ");
            returnString = returnString.Replace("áº«", "ẫ");
            returnString = returnString.Replace("áº­", "ậ");
            //
            returnString = returnString.Replace("Ã‚", "Â");
            returnString = returnString.Replace("áº¤", "Ấ");
            returnString = returnString.Replace("áº¦", "Ầ");
            returnString = returnString.Replace("áº¨", "Ẩ");
            returnString = returnString.Replace("áºª", "Ẫ");
            returnString = returnString.Replace("áº¬", "Ậ");

            // é è ẻ ẽ ẹ
            returnString = returnString.Replace("Ã©", "é");
            returnString = returnString.Replace("Ã¨", "è");
            returnString = returnString.Replace("áº»", "ẻ");
            returnString = returnString.Replace("áº½", "ẽ");
            returnString = returnString.Replace("áº¹", "ẹ");
            //
            returnString = returnString.Replace("Ã‰", "É");
            returnString = returnString.Replace("Ãˆ", "È");
            returnString = returnString.Replace("áºº", "Ẻ");
            returnString = returnString.Replace("áº¼", "Ẽ");
            returnString = returnString.Replace("áº¸", "Ẹ");

            // ê ế ề ể ễ ệ
            returnString = returnString.Replace("Ãª", "ê");
            returnString = returnString.Replace("áº¿", "ế");
            returnString = returnString.Replace("á»", "ề");
            returnString = returnString.Replace("á»ƒ", "ể");
            returnString = returnString.Replace("á»…", "ễ");
            returnString = returnString.Replace("á»‡", "ệ");
            //
            returnString = returnString.Replace("ÃŠ", "Ê");
            returnString = returnString.Replace("áº¾", "Ế");
            returnString = returnString.Replace("á»€", "Ề");
            returnString = returnString.Replace("á»‚", "Ể");
            returnString = returnString.Replace("á»„", "Ễ");
            returnString = returnString.Replace("á»†", "Ệ");

            // í ì ỉ ĩ ị
            returnString = returnString.Replace("Ã­", "í");
            returnString = returnString.Replace("Ã¬", "ì");
            returnString = returnString.Replace("á»‰", "ỉ");
            returnString = returnString.Replace("Ä©", "ĩ");
            returnString = returnString.Replace("á»‹", "ị");
            //
            returnString = returnString.Replace("Ã", "Í");
            returnString = returnString.Replace("ÃŒ", "Ì");
            returnString = returnString.Replace("á»ˆ", "Ỉ");
            returnString = returnString.Replace("Ä¨", "Ĩ");
            returnString = returnString.Replace("á»Š", "Ị");

            // ó ò ỏ õ ọ
            returnString = returnString.Replace("Ã³", "ó");
            returnString = returnString.Replace("Ã²", "ò");
            returnString = returnString.Replace("á»", "ỏ");
            returnString = returnString.Replace("Ãµ", "õ");
            returnString = returnString.Replace("á»", "ọ");
            //
            returnString = returnString.Replace("Ã“", "Ó");
            returnString = returnString.Replace("Ã’", "Ò");
            returnString = returnString.Replace("á»Ž", "Ỏ");
            returnString = returnString.Replace("Ã•", "Õ");
            returnString = returnString.Replace("á»Œ", "Ọ");

            // ô ố ồ ổ ỗ ộ
            returnString = returnString.Replace("Ã´", "ô");
            returnString = returnString.Replace("á»‘", "ố");
            returnString = returnString.Replace("á»“", "ồ");
            returnString = returnString.Replace("á»•", "ổ");
            returnString = returnString.Replace("á»—", "ỗ");
            returnString = returnString.Replace("á»™", "ộ");
            //
            returnString = returnString.Replace("Ã”", "Ô");
            returnString = returnString.Replace("á»", "Ố");
            returnString = returnString.Replace("á»’", "Ồ");
            returnString = returnString.Replace("á»”", "Ổ");
            returnString = returnString.Replace("á»–", "Ỗ");
            returnString = returnString.Replace("á»˜", "Ộ");

            // ơ ớ ờ ở ỡ ợ
            returnString = returnString.Replace("Æ¡", "ơ");
            returnString = returnString.Replace("á»›", "ớ");
            returnString = returnString.Replace("á»", "ờ");
            returnString = returnString.Replace("á»Ÿ", "ở");
            returnString = returnString.Replace("á»¡", "ỡ");
            returnString = returnString.Replace("á»£", "ợ");
            //
            returnString = returnString.Replace("Æ ", "Ơ");
            returnString = returnString.Replace("á»š", "Ớ");
            returnString = returnString.Replace("á»œ", "Ờ");
            returnString = returnString.Replace("á»ž", "Ở");
            returnString = returnString.Replace("á» ", "Ỡ");
            returnString = returnString.Replace("á»¢", "Ợ");

            // ú ù ủ ũ ụ
            returnString = returnString.Replace("Ãº", "ú");
            returnString = returnString.Replace("Ã¹", "ù");
            returnString = returnString.Replace("á»§", "ủ");
            returnString = returnString.Replace("Å©", "ũ");
            returnString = returnString.Replace("á»¥", "ụ");
            //
            returnString = returnString.Replace("Ãš", "Ú");
            returnString = returnString.Replace("Ã™", "Ù");
            returnString = returnString.Replace("á»¦", "Ủ");
            returnString = returnString.Replace("Å¨", "Ũ");
            returnString = returnString.Replace("á»¤", "Ụ");

            // ư ứ ừ ử ữ ự
            returnString = returnString.Replace("Æ°", "ư");
            returnString = returnString.Replace("á»©", "ứ");
            returnString = returnString.Replace("á»«", "ừ");
            returnString = returnString.Replace("á»­", "ử");
            returnString = returnString.Replace("á»¯", "ữ");
            returnString = returnString.Replace("á»±", "ự");
            //
            returnString = returnString.Replace("Æ¯", "Ư");
            returnString = returnString.Replace("á»¨", "Ứ");
            returnString = returnString.Replace("á»ª", "Ừ");
            returnString = returnString.Replace("á»¬", "Ử");
            returnString = returnString.Replace("á»®", "Ữ");
            returnString = returnString.Replace("á»°", "Ự");

            // ý ỳ ỷ ỹ ỵ đ
            returnString = returnString.Replace("Ã½", "ý");
            returnString = returnString.Replace("á»³", "ỳ");
            returnString = returnString.Replace("á»·", "ỷ");
            returnString = returnString.Replace("á»¹", "ỹ");
            returnString = returnString.Replace("á»µ", "ỵ");
            returnString = returnString.Replace("Ä‘", "đ");
            //
            returnString = returnString.Replace("Ã", "Ý");
            returnString = returnString.Replace("á»²", "Ỳ");
            returnString = returnString.Replace("á»¶", "Ỷ");
            returnString = returnString.Replace("á»¸", "Ỹ");
            returnString = returnString.Replace("á»´", "Ỵ");
            returnString = returnString.Replace("Ä", "Đ");

            return returnString;
        }
    }
}