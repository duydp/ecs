﻿namespace Company.Interface
{
    partial class VNACC_HangMauDichXuatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_HangMauDichXuatForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.linkLabel6 = new System.Windows.Forms.LinkLabel();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label34 = new System.Windows.Forms.Label();
            this.ctrNuocXuatXu = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.linkMaTTCopy = new System.Windows.Forms.LinkLabel();
            this.btnDVT2 = new Janus.Windows.EditControls.UIButton();
            this.btnDVT1 = new Janus.Windows.EditControls.UIButton();
            this.txtSoTTDongHangTKTNTX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.CopyDanhMucMienThue = new System.Windows.Forms.LinkLabel();
            this.CopyMienGiam = new System.Windows.Forms.LinkLabel();
            this.txtSoDMMienThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ctrMaDVTTuyetDoi = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label35 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtDieuKhoanMienGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.ctrMaTTTuyetDoi = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPhanLoai_TS_ThueXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueSuat = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtThueSuatTuyetDoi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ctrMaTTCuaDonGiaTinhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTTriGiaTinhThueS = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaMienGiamThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTMaMienGiam = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTSoTienThueXuatKhau = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTTriGiaTinhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDonGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienMienGiam = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTriGiaTinhThueS = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTriGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDV_SL_TrongDonGiaTinhThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSoDongDMMienThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.editBox2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ucCategory6 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.numericEditBox2 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox6 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label25 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ucCategory5 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.numericEditBox1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox5 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.ctrMaVanBan5 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBan4 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBan3 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBan2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBan1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDVTDonGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTDonGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDVTLuong2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDVTLuong1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaSoHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtMaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaQuanLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTriGiaHoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDonGiaHoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuong2 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuong1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblMaHang = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.linkSuaSoLuong = new System.Windows.Forms.LinkLabel();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 696), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 696);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 672);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 672);
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox7);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(903, 696);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.MistyRose;
            this.uiGroupBox1.Controls.Add(this.grList);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 460);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(903, 236);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Danh sách hàng";
            // 
            // grList
            // 
            this.grList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grList.FrozenColumns = 5;
            this.grList.GroupByBoxVisible = false;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Location = new System.Drawing.Point(0, 17);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            this.grList.Size = new System.Drawing.Size(903, 216);
            this.grList.TabIndex = 0;
            this.grList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.TotalRowPosition = Janus.Windows.GridEX.TotalRowPosition.BottomFixed;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.DoubleClick += new System.EventHandler(this.grList_DoubleClick);
            this.grList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.grList_DeletingRecords);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.MistyRose;
            this.uiGroupBox2.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.uiGroupBox2.Controls.Add(this.linkLabel4);
            this.uiGroupBox2.Controls.Add(this.linkLabel6);
            this.uiGroupBox2.Controls.Add(this.linkLabel5);
            this.uiGroupBox2.Controls.Add(this.linkLabel2);
            this.uiGroupBox2.Controls.Add(this.label34);
            this.uiGroupBox2.Controls.Add(this.ctrNuocXuatXu);
            this.uiGroupBox2.Controls.Add(this.linkLabel3);
            this.uiGroupBox2.Controls.Add(this.linkMaTTCopy);
            this.uiGroupBox2.Controls.Add(this.btnDVT2);
            this.uiGroupBox2.Controls.Add(this.btnDVT1);
            this.uiGroupBox2.Controls.Add(this.txtSoTTDongHangTKTNTX);
            this.uiGroupBox2.Controls.Add(this.linkLabel1);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox2.Controls.Add(this.ctrMaVanBan5);
            this.uiGroupBox2.Controls.Add(this.ctrMaVanBan4);
            this.uiGroupBox2.Controls.Add(this.ctrMaVanBan3);
            this.uiGroupBox2.Controls.Add(this.ctrMaVanBan2);
            this.uiGroupBox2.Controls.Add(this.ctrMaVanBan1);
            this.uiGroupBox2.Controls.Add(this.ctrDVTDonGia);
            this.uiGroupBox2.Controls.Add(this.ctrMaTTDonGia);
            this.uiGroupBox2.Controls.Add(this.ctrDVTLuong2);
            this.uiGroupBox2.Controls.Add(this.ctrDVTLuong1);
            this.uiGroupBox2.Controls.Add(this.ctrMaSoHang);
            this.uiGroupBox2.Controls.Add(this.txtMaHangHoa);
            this.uiGroupBox2.Controls.Add(this.txtMaQuanLy);
            this.uiGroupBox2.Controls.Add(this.txtTriGiaHoaDon);
            this.uiGroupBox2.Controls.Add(this.txtDonGiaHoaDon);
            this.uiGroupBox2.Controls.Add(this.txtSoLuong2);
            this.uiGroupBox2.Controls.Add(this.txtSoLuong1);
            this.uiGroupBox2.Controls.Add(this.txtTenHang);
            this.uiGroupBox2.Controls.Add(this.label19);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label32);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.lblMaHang);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(903, 430);
            this.uiGroupBox2.TabIndex = 1;
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel4.Location = new System.Drawing.Point(310, 127);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(75, 13);
            this.linkLabel4.TabIndex = 82;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "Del SL2+DVT2";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // linkLabel6
            // 
            this.linkLabel6.AutoSize = true;
            this.linkLabel6.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel6.Location = new System.Drawing.Point(256, 50);
            this.linkLabel6.Name = "linkLabel6";
            this.linkLabel6.Size = new System.Drawing.Size(95, 13);
            this.linkLabel6.TabIndex = 82;
            this.linkLabel6.TabStop = true;
            this.linkLabel6.Text = "Chi tiết Mã HS này";
            this.linkLabel6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel6_LinkClicked);
            // 
            // linkLabel5
            // 
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel5.Location = new System.Drawing.Point(407, 103);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(32, 13);
            this.linkLabel5.TabIndex = 82;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "Copy";
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel2.Location = new System.Drawing.Point(256, 127);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(32, 13);
            this.linkLabel2.TabIndex = 82;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Copy";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(7, 127);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(45, 13);
            this.label34.TabIndex = 81;
            this.label34.Text = "Xuất xứ";
            // 
            // ctrNuocXuatXu
            // 
            this.ctrNuocXuatXu.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrNuocXuatXu.Appearance.Options.UseBackColor = true;
            this.ctrNuocXuatXu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ctrNuocXuatXu.Code = "";
            this.ctrNuocXuatXu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNuocXuatXu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrNuocXuatXu.IsOnlyWarning = false;
            this.ctrNuocXuatXu.IsValidate = true;
            this.ctrNuocXuatXu.Location = new System.Drawing.Point(91, 125);
            this.ctrNuocXuatXu.Name = "ctrNuocXuatXu";
            this.ctrNuocXuatXu.Name_VN = "";
            this.ctrNuocXuatXu.SetOnlyWarning = false;
            this.ctrNuocXuatXu.SetValidate = false;
            this.ctrNuocXuatXu.ShowColumnCode = true;
            this.ctrNuocXuatXu.ShowColumnName = true;
            this.ctrNuocXuatXu.Size = new System.Drawing.Size(159, 21);
            this.ctrNuocXuatXu.TabIndex = 80;
            this.ctrNuocXuatXu.TagCode = "";
            this.ctrNuocXuatXu.TagName = "";
            this.ctrNuocXuatXu.Where = null;
            this.ctrNuocXuatXu.WhereCondition = "";
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel3.Location = new System.Drawing.Point(699, 74);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(87, 12);
            this.linkLabel3.TabIndex = 79;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Cấu hình tính TG";
            this.linkLabel3.Click += new System.EventHandler(this.linkLabel2_Click);
            // 
            // linkMaTTCopy
            // 
            this.linkMaTTCopy.AutoSize = true;
            this.linkMaTTCopy.BackColor = System.Drawing.Color.Transparent;
            this.linkMaTTCopy.Location = new System.Drawing.Point(768, 100);
            this.linkMaTTCopy.Name = "linkMaTTCopy";
            this.linkMaTTCopy.Size = new System.Drawing.Size(32, 13);
            this.linkMaTTCopy.TabIndex = 59;
            this.linkMaTTCopy.TabStop = true;
            this.linkMaTTCopy.Text = "Copy";
            this.linkMaTTCopy.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkMaTTCopy_LinkClicked);
            // 
            // btnDVT2
            // 
            this.btnDVT2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDVT2.ImageIndex = 4;
            this.btnDVT2.Location = new System.Drawing.Point(377, 98);
            this.btnDVT2.Name = "btnDVT2";
            this.btnDVT2.Size = new System.Drawing.Size(21, 20);
            this.btnDVT2.TabIndex = 78;
            this.btnDVT2.Text = "...";
            this.btnDVT2.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDVT2.Click += new System.EventHandler(this.btnDVT2_Click);
            // 
            // btnDVT1
            // 
            this.btnDVT1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDVT1.ImageIndex = 4;
            this.btnDVT1.Location = new System.Drawing.Point(377, 71);
            this.btnDVT1.Name = "btnDVT1";
            this.btnDVT1.Size = new System.Drawing.Size(21, 20);
            this.btnDVT1.TabIndex = 77;
            this.btnDVT1.Text = "...";
            this.btnDVT1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDVT1.Click += new System.EventHandler(this.btnDVT1_Click);
            // 
            // txtSoTTDongHangTKTNTX
            // 
            this.txtSoTTDongHangTKTNTX.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoTTDongHangTKTNTX.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoTTDongHangTKTNTX.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoTTDongHangTKTNTX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTTDongHangTKTNTX.Location = new System.Drawing.Point(313, 151);
            this.txtSoTTDongHangTKTNTX.MaxLength = 12;
            this.txtSoTTDongHangTKTNTX.Name = "txtSoTTDongHangTKTNTX";
            this.txtSoTTDongHangTKTNTX.Size = new System.Drawing.Size(78, 21);
            this.txtSoTTDongHangTKTNTX.TabIndex = 14;
            this.txtSoTTDongHangTKTNTX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTTDongHangTKTNTX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTTDongHangTKTNTX.ButtonClick += new System.EventHandler(this.txtSoTTDongHangTKTNTX_ButtonClick);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Location = new System.Drawing.Point(158, 23);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(32, 13);
            this.linkLabel1.TabIndex = 59;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Copy";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.uiTab1);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox4.FrameStyle = Janus.Windows.EditControls.FrameStyle.Top;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 203);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(897, 224);
            this.uiGroupBox4.TabIndex = 41;
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 8);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.PanelFormatStyle.BackColor = System.Drawing.Color.MistyRose;
            this.uiTab1.Size = new System.Drawing.Size(897, 213);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.CopyDanhMucMienThue);
            this.uiTabPage1.Controls.Add(this.CopyMienGiam);
            this.uiTabPage1.Controls.Add(this.txtSoDMMienThue);
            this.uiTabPage1.Controls.Add(this.ctrMaDVTTuyetDoi);
            this.uiTabPage1.Controls.Add(this.label35);
            this.uiTabPage1.Controls.Add(this.label33);
            this.uiTabPage1.Controls.Add(this.txtDieuKhoanMienGiam);
            this.uiTabPage1.Controls.Add(this.label30);
            this.uiTabPage1.Controls.Add(this.label20);
            this.uiTabPage1.Controls.Add(this.ctrMaTTTuyetDoi);
            this.uiTabPage1.Controls.Add(this.label12);
            this.uiTabPage1.Controls.Add(this.txtPhanLoai_TS_ThueXuatKhau);
            this.uiTabPage1.Controls.Add(this.txtThueSuat);
            this.uiTabPage1.Controls.Add(this.txtThueSuatTuyetDoi);
            this.uiTabPage1.Controls.Add(this.ctrMaTTCuaDonGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.ctrMaTTTriGiaTinhThueS);
            this.uiTabPage1.Controls.Add(this.ctrMaMienGiamThue);
            this.uiTabPage1.Controls.Add(this.ctrMaTTMaMienGiam);
            this.uiTabPage1.Controls.Add(this.ctrMaTTSoTienThueXuatKhau);
            this.uiTabPage1.Controls.Add(this.ctrMaTTTriGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.label15);
            this.uiTabPage1.Controls.Add(this.label13);
            this.uiTabPage1.Controls.Add(this.txtDonGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.txtSoTienMienGiam);
            this.uiTabPage1.Controls.Add(this.label14);
            this.uiTabPage1.Controls.Add(this.label17);
            this.uiTabPage1.Controls.Add(this.label10);
            this.uiTabPage1.Controls.Add(this.txtTriGiaTinhThueS);
            this.uiTabPage1.Controls.Add(this.txtSoTienThue);
            this.uiTabPage1.Controls.Add(this.label26);
            this.uiTabPage1.Controls.Add(this.label22);
            this.uiTabPage1.Controls.Add(this.label31);
            this.uiTabPage1.Controls.Add(this.label18);
            this.uiTabPage1.Controls.Add(this.txtTriGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.txtDV_SL_TrongDonGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.label21);
            this.uiTabPage1.Controls.Add(this.txtSoDongDMMienThue);
            this.uiTabPage1.Controls.Add(this.label16);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(895, 191);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thuế xuất khẩu";
            // 
            // CopyDanhMucMienThue
            // 
            this.CopyDanhMucMienThue.AutoSize = true;
            this.CopyDanhMucMienThue.BackColor = System.Drawing.Color.Transparent;
            this.CopyDanhMucMienThue.Location = new System.Drawing.Point(342, 93);
            this.CopyDanhMucMienThue.Name = "CopyDanhMucMienThue";
            this.CopyDanhMucMienThue.Size = new System.Drawing.Size(32, 13);
            this.CopyDanhMucMienThue.TabIndex = 59;
            this.CopyDanhMucMienThue.TabStop = true;
            this.CopyDanhMucMienThue.Text = "Copy";
            this.CopyDanhMucMienThue.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.CopyDanhMuc_LinkkClicked);
            // 
            // CopyMienGiam
            // 
            this.CopyMienGiam.AutoSize = true;
            this.CopyMienGiam.BackColor = System.Drawing.Color.Transparent;
            this.CopyMienGiam.Location = new System.Drawing.Point(342, 144);
            this.CopyMienGiam.Name = "CopyMienGiam";
            this.CopyMienGiam.Size = new System.Drawing.Size(32, 13);
            this.CopyMienGiam.TabIndex = 59;
            this.CopyMienGiam.TabStop = true;
            this.CopyMienGiam.Text = "Copy";
            this.CopyMienGiam.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.CopyMienGiam_LinkkClicked);
            // 
            // txtSoDMMienThue
            // 
            this.txtSoDMMienThue.Location = new System.Drawing.Point(182, 90);
            this.txtSoDMMienThue.Name = "txtSoDMMienThue";
            this.txtSoDMMienThue.Size = new System.Drawing.Size(158, 21);
            this.txtSoDMMienThue.TabIndex = 58;
            this.txtSoDMMienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ctrMaDVTTuyetDoi
            // 
            this.ctrMaDVTTuyetDoi.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDVTTuyetDoi.Appearance.Options.UseBackColor = true;
            this.ctrMaDVTTuyetDoi.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrMaDVTTuyetDoi.Code = "";
            this.ctrMaDVTTuyetDoi.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDVTTuyetDoi.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTTuyetDoi.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTTuyetDoi.IsOnlyWarning = false;
            this.ctrMaDVTTuyetDoi.IsValidate = true;
            this.ctrMaDVTTuyetDoi.Location = new System.Drawing.Point(476, 8);
            this.ctrMaDVTTuyetDoi.Name = "ctrMaDVTTuyetDoi";
            this.ctrMaDVTTuyetDoi.Name_VN = "";
            this.ctrMaDVTTuyetDoi.SetOnlyWarning = false;
            this.ctrMaDVTTuyetDoi.SetValidate = false;
            this.ctrMaDVTTuyetDoi.ShowColumnCode = true;
            this.ctrMaDVTTuyetDoi.ShowColumnName = false;
            this.ctrMaDVTTuyetDoi.Size = new System.Drawing.Size(100, 21);
            this.ctrMaDVTTuyetDoi.TabIndex = 2;
            this.ctrMaDVTTuyetDoi.TagName = "";
            this.ctrMaDVTTuyetDoi.Where = null;
            this.ctrMaDVTTuyetDoi.WhereCondition = "";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(575, 12);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(58, 13);
            this.label35.TabIndex = 57;
            this.label35.Text = "Mã tiền tệ ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(380, 12);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(98, 13);
            this.label33.TabIndex = 56;
            this.label33.Text = "ĐVT thuế tuyệt đối";
            // 
            // txtDieuKhoanMienGiam
            // 
            this.txtDieuKhoanMienGiam.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDieuKhoanMienGiam.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDieuKhoanMienGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDieuKhoanMienGiam.Location = new System.Drawing.Point(328, 164);
            this.txtDieuKhoanMienGiam.MaxLength = 12;
            this.txtDieuKhoanMienGiam.Name = "txtDieuKhoanMienGiam";
            this.txtDieuKhoanMienGiam.ReadOnly = true;
            this.txtDieuKhoanMienGiam.Size = new System.Drawing.Size(307, 21);
            this.txtDieuKhoanMienGiam.TabIndex = 19;
            this.txtDieuKhoanMienGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDieuKhoanMienGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(7, 168);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(315, 13);
            this.label30.TabIndex = 54;
            this.label30.Text = "Điều khoản miễn / Giảm / Không chịu thuế nhập khẩu (Pháp luật)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(7, 144);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(211, 13);
            this.label20.TabIndex = 29;
            this.label20.Text = "Mã miễn/ Giảm/ Không chịu thuế xuất khẩu";
            // 
            // ctrMaTTTuyetDoi
            // 
            this.ctrMaTTTuyetDoi.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTTuyetDoi.Appearance.Options.UseBackColor = true;
            this.ctrMaTTTuyetDoi.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTuyetDoi.Code = "";
            this.ctrMaTTTuyetDoi.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTTuyetDoi.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTuyetDoi.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTTuyetDoi.IsOnlyWarning = false;
            this.ctrMaTTTuyetDoi.IsValidate = true;
            this.ctrMaTTTuyetDoi.Location = new System.Drawing.Point(640, 9);
            this.ctrMaTTTuyetDoi.Name = "ctrMaTTTuyetDoi";
            this.ctrMaTTTuyetDoi.Name_VN = "";
            this.ctrMaTTTuyetDoi.SetOnlyWarning = false;
            this.ctrMaTTTuyetDoi.SetValidate = false;
            this.ctrMaTTTuyetDoi.ShowColumnCode = true;
            this.ctrMaTTTuyetDoi.ShowColumnName = false;
            this.ctrMaTTTuyetDoi.Size = new System.Drawing.Size(69, 20);
            this.ctrMaTTTuyetDoi.TabIndex = 3;
            this.ctrMaTTTuyetDoi.TagName = "";
            this.ctrMaTTTuyetDoi.Where = null;
            this.ctrMaTTTuyetDoi.WhereCondition = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(143, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Mức thuế tuyệt đối";
            // 
            // txtPhanLoai_TS_ThueXuatKhau
            // 
            this.txtPhanLoai_TS_ThueXuatKhau.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtPhanLoai_TS_ThueXuatKhau.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtPhanLoai_TS_ThueXuatKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhanLoai_TS_ThueXuatKhau.Location = new System.Drawing.Point(476, 140);
            this.txtPhanLoai_TS_ThueXuatKhau.MaxLength = 12;
            this.txtPhanLoai_TS_ThueXuatKhau.Name = "txtPhanLoai_TS_ThueXuatKhau";
            this.txtPhanLoai_TS_ThueXuatKhau.ReadOnly = true;
            this.txtPhanLoai_TS_ThueXuatKhau.Size = new System.Drawing.Size(159, 21);
            this.txtPhanLoai_TS_ThueXuatKhau.TabIndex = 18;
            this.txtPhanLoai_TS_ThueXuatKhau.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhanLoai_TS_ThueXuatKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuat
            // 
            this.txtThueSuat.DecimalDigits = 20;
            this.txtThueSuat.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThueSuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuat.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtThueSuat.Location = new System.Drawing.Point(65, 7);
            this.txtThueSuat.MaxLength = 15;
            this.txtThueSuat.Name = "txtThueSuat";
            this.txtThueSuat.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtThueSuat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThueSuat.Size = new System.Drawing.Size(54, 21);
            this.txtThueSuat.TabIndex = 1;
            this.txtThueSuat.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThueSuat.Value = null;
            this.txtThueSuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuatTuyetDoi
            // 
            this.txtThueSuatTuyetDoi.DecimalDigits = 20;
            this.txtThueSuatTuyetDoi.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThueSuatTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuatTuyetDoi.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtThueSuatTuyetDoi.Location = new System.Drawing.Point(247, 8);
            this.txtThueSuatTuyetDoi.MaxLength = 15;
            this.txtThueSuatTuyetDoi.Name = "txtThueSuatTuyetDoi";
            this.txtThueSuatTuyetDoi.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtThueSuatTuyetDoi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThueSuatTuyetDoi.Size = new System.Drawing.Size(121, 21);
            this.txtThueSuatTuyetDoi.TabIndex = 1;
            this.txtThueSuatTuyetDoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThueSuatTuyetDoi.Value = null;
            this.txtThueSuatTuyetDoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ctrMaTTCuaDonGiaTinhThue
            // 
            this.ctrMaTTCuaDonGiaTinhThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTCuaDonGiaTinhThue.Appearance.Options.UseBackColor = true;
            this.ctrMaTTCuaDonGiaTinhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTCuaDonGiaTinhThue.Code = "";
            this.ctrMaTTCuaDonGiaTinhThue.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTCuaDonGiaTinhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTCuaDonGiaTinhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTCuaDonGiaTinhThue.IsOnlyWarning = false;
            this.ctrMaTTCuaDonGiaTinhThue.IsValidate = true;
            this.ctrMaTTCuaDonGiaTinhThue.Location = new System.Drawing.Point(640, 64);
            this.ctrMaTTCuaDonGiaTinhThue.Name = "ctrMaTTCuaDonGiaTinhThue";
            this.ctrMaTTCuaDonGiaTinhThue.Name_VN = "";
            this.ctrMaTTCuaDonGiaTinhThue.SetOnlyWarning = false;
            this.ctrMaTTCuaDonGiaTinhThue.SetValidate = false;
            this.ctrMaTTCuaDonGiaTinhThue.ShowColumnCode = true;
            this.ctrMaTTCuaDonGiaTinhThue.ShowColumnName = false;
            this.ctrMaTTCuaDonGiaTinhThue.Size = new System.Drawing.Size(69, 21);
            this.ctrMaTTCuaDonGiaTinhThue.TabIndex = 11;
            this.ctrMaTTCuaDonGiaTinhThue.TagName = "";
            this.ctrMaTTCuaDonGiaTinhThue.Where = null;
            this.ctrMaTTCuaDonGiaTinhThue.WhereCondition = "";
            // 
            // ctrMaTTTriGiaTinhThueS
            // 
            this.ctrMaTTTriGiaTinhThueS.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTTriGiaTinhThueS.Appearance.Options.UseBackColor = true;
            this.ctrMaTTTriGiaTinhThueS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTriGiaTinhThueS.Code = "";
            this.ctrMaTTTriGiaTinhThueS.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTTriGiaTinhThueS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTriGiaTinhThueS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTTriGiaTinhThueS.IsOnlyWarning = false;
            this.ctrMaTTTriGiaTinhThueS.IsValidate = true;
            this.ctrMaTTTriGiaTinhThueS.Location = new System.Drawing.Point(640, 35);
            this.ctrMaTTTriGiaTinhThueS.Name = "ctrMaTTTriGiaTinhThueS";
            this.ctrMaTTTriGiaTinhThueS.Name_VN = "";
            this.ctrMaTTTriGiaTinhThueS.SetOnlyWarning = false;
            this.ctrMaTTTriGiaTinhThueS.SetValidate = false;
            this.ctrMaTTTriGiaTinhThueS.ShowColumnCode = true;
            this.ctrMaTTTriGiaTinhThueS.ShowColumnName = false;
            this.ctrMaTTTriGiaTinhThueS.Size = new System.Drawing.Size(69, 21);
            this.ctrMaTTTriGiaTinhThueS.TabIndex = 7;
            this.ctrMaTTTriGiaTinhThueS.TagName = "";
            this.ctrMaTTTriGiaTinhThueS.Where = null;
            this.ctrMaTTTriGiaTinhThueS.WhereCondition = "";
            // 
            // ctrMaMienGiamThue
            // 
            this.ctrMaMienGiamThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaMienGiamThue.Appearance.Options.UseBackColor = true;
            this.ctrMaMienGiamThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A520;
            this.ctrMaMienGiamThue.Code = "";
            this.ctrMaMienGiamThue.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaMienGiamThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaMienGiamThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaMienGiamThue.IsOnlyWarning = false;
            this.ctrMaMienGiamThue.IsValidate = true;
            this.ctrMaMienGiamThue.Location = new System.Drawing.Point(218, 140);
            this.ctrMaMienGiamThue.Name = "ctrMaMienGiamThue";
            this.ctrMaMienGiamThue.Name_VN = "";
            this.ctrMaMienGiamThue.SetOnlyWarning = false;
            this.ctrMaMienGiamThue.SetValidate = false;
            this.ctrMaMienGiamThue.ShowColumnCode = true;
            this.ctrMaMienGiamThue.ShowColumnName = false;
            this.ctrMaMienGiamThue.Size = new System.Drawing.Size(122, 21);
            this.ctrMaMienGiamThue.TabIndex = 17;
            this.ctrMaMienGiamThue.TagName = "";
            this.ctrMaMienGiamThue.Where = null;
            this.ctrMaMienGiamThue.WhereCondition = "";
            this.ctrMaMienGiamThue.Leave += new System.EventHandler(this.ctrMaMienGiamThue_Leave);
            // 
            // ctrMaTTMaMienGiam
            // 
            this.ctrMaTTMaMienGiam.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTMaMienGiam.Appearance.Options.UseBackColor = true;
            this.ctrMaTTMaMienGiam.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTMaMienGiam.Code = "";
            this.ctrMaTTMaMienGiam.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTMaMienGiam.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTMaMienGiam.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTMaMienGiam.IsOnlyWarning = false;
            this.ctrMaTTMaMienGiam.IsValidate = true;
            this.ctrMaTTMaMienGiam.Location = new System.Drawing.Point(278, 64);
            this.ctrMaTTMaMienGiam.Name = "ctrMaTTMaMienGiam";
            this.ctrMaTTMaMienGiam.Name_VN = "";
            this.ctrMaTTMaMienGiam.SetOnlyWarning = false;
            this.ctrMaTTMaMienGiam.SetValidate = false;
            this.ctrMaTTMaMienGiam.ShowColumnCode = true;
            this.ctrMaTTMaMienGiam.ShowColumnName = false;
            this.ctrMaTTMaMienGiam.Size = new System.Drawing.Size(78, 21);
            this.ctrMaTTMaMienGiam.TabIndex = 9;
            this.ctrMaTTMaMienGiam.TagName = "";
            this.ctrMaTTMaMienGiam.Where = null;
            this.ctrMaTTMaMienGiam.WhereCondition = "";
            // 
            // ctrMaTTSoTienThueXuatKhau
            // 
            this.ctrMaTTSoTienThueXuatKhau.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTSoTienThueXuatKhau.Appearance.Options.UseBackColor = true;
            this.ctrMaTTSoTienThueXuatKhau.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTSoTienThueXuatKhau.Code = "";
            this.ctrMaTTSoTienThueXuatKhau.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTSoTienThueXuatKhau.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTSoTienThueXuatKhau.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTSoTienThueXuatKhau.IsOnlyWarning = false;
            this.ctrMaTTSoTienThueXuatKhau.IsValidate = true;
            this.ctrMaTTSoTienThueXuatKhau.Location = new System.Drawing.Point(640, 112);
            this.ctrMaTTSoTienThueXuatKhau.Name = "ctrMaTTSoTienThueXuatKhau";
            this.ctrMaTTSoTienThueXuatKhau.Name_VN = "";
            this.ctrMaTTSoTienThueXuatKhau.SetOnlyWarning = false;
            this.ctrMaTTSoTienThueXuatKhau.SetValidate = false;
            this.ctrMaTTSoTienThueXuatKhau.ShowColumnCode = true;
            this.ctrMaTTSoTienThueXuatKhau.ShowColumnName = false;
            this.ctrMaTTSoTienThueXuatKhau.Size = new System.Drawing.Size(69, 21);
            this.ctrMaTTSoTienThueXuatKhau.TabIndex = 16;
            this.ctrMaTTSoTienThueXuatKhau.TagName = "";
            this.ctrMaTTSoTienThueXuatKhau.Where = null;
            this.ctrMaTTSoTienThueXuatKhau.WhereCondition = "";
            // 
            // ctrMaTTTriGiaTinhThue
            // 
            this.ctrMaTTTriGiaTinhThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTTriGiaTinhThue.Appearance.Options.UseBackColor = true;
            this.ctrMaTTTriGiaTinhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTriGiaTinhThue.Code = "";
            this.ctrMaTTTriGiaTinhThue.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTTriGiaTinhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTriGiaTinhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTTriGiaTinhThue.IsOnlyWarning = false;
            this.ctrMaTTTriGiaTinhThue.IsValidate = true;
            this.ctrMaTTTriGiaTinhThue.Location = new System.Drawing.Point(277, 35);
            this.ctrMaTTTriGiaTinhThue.Name = "ctrMaTTTriGiaTinhThue";
            this.ctrMaTTTriGiaTinhThue.Name_VN = "";
            this.ctrMaTTTriGiaTinhThue.SetOnlyWarning = false;
            this.ctrMaTTTriGiaTinhThue.SetValidate = false;
            this.ctrMaTTTriGiaTinhThue.ShowColumnCode = true;
            this.ctrMaTTTriGiaTinhThue.ShowColumnName = false;
            this.ctrMaTTTriGiaTinhThue.Size = new System.Drawing.Size(78, 21);
            this.ctrMaTTTriGiaTinhThue.TabIndex = 5;
            this.ctrMaTTTriGiaTinhThue.TagName = "";
            this.ctrMaTTTriGiaTinhThue.Where = null;
            this.ctrMaTTTriGiaTinhThue.WhereCondition = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(7, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Thuế suất";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(125, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "%";
            // 
            // txtDonGiaTinhThue
            // 
            this.txtDonGiaTinhThue.DecimalDigits = 20;
            this.txtDonGiaTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDonGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaTinhThue.Location = new System.Drawing.Point(476, 64);
            this.txtDonGiaTinhThue.MaxLength = 15;
            this.txtDonGiaTinhThue.Name = "txtDonGiaTinhThue";
            this.txtDonGiaTinhThue.ReadOnly = true;
            this.txtDonGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaTinhThue.Size = new System.Drawing.Size(159, 21);
            this.txtDonGiaTinhThue.TabIndex = 10;
            this.txtDonGiaTinhThue.Text = "0";
            this.txtDonGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienMienGiam
            // 
            this.txtSoTienMienGiam.DecimalDigits = 20;
            this.txtSoTienMienGiam.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienMienGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienMienGiam.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienMienGiam.Location = new System.Drawing.Point(146, 64);
            this.txtSoTienMienGiam.MaxLength = 15;
            this.txtSoTienMienGiam.Name = "txtSoTienMienGiam";
            this.txtSoTienMienGiam.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtSoTienMienGiam.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienMienGiam.Size = new System.Drawing.Size(126, 21);
            this.txtSoTienMienGiam.TabIndex = 8;
            this.txtSoTienMienGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienMienGiam.Value = null;
            this.txtSoTienMienGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(7, 69);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(141, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Số tiền giảm thuế xuất khẩu";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(380, 41);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "Trị giá tính thuế(S)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "Trị giá tính thuế(M)";
            // 
            // txtTriGiaTinhThueS
            // 
            this.txtTriGiaTinhThueS.DecimalDigits = 20;
            this.txtTriGiaTinhThueS.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThueS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThueS.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaTinhThueS.Location = new System.Drawing.Point(476, 35);
            this.txtTriGiaTinhThueS.MaxLength = 15;
            this.txtTriGiaTinhThueS.Name = "txtTriGiaTinhThueS";
            this.txtTriGiaTinhThueS.ReadOnly = true;
            this.txtTriGiaTinhThueS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThueS.Size = new System.Drawing.Size(159, 21);
            this.txtTriGiaTinhThueS.TabIndex = 6;
            this.txtTriGiaTinhThueS.Text = "0";
            this.txtTriGiaTinhThueS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThueS.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTinhThueS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienThue
            // 
            this.txtSoTienThue.DecimalDigits = 20;
            this.txtSoTienThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienThue.Location = new System.Drawing.Point(476, 115);
            this.txtSoTienThue.MaxLength = 15;
            this.txtSoTienThue.Name = "txtSoTienThue";
            this.txtSoTienThue.ReadOnly = true;
            this.txtSoTienThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienThue.Size = new System.Drawing.Size(159, 21);
            this.txtSoTienThue.TabIndex = 15;
            this.txtSoTienThue.Text = "0";
            this.txtSoTienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(380, 144);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(100, 13);
            this.label26.TabIndex = 22;
            this.label26.Text = "Phân loại thuế xuất";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(380, 120);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 13);
            this.label22.TabIndex = 22;
            this.label22.Text = "Số tiền thuế";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(380, 93);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(195, 13);
            this.label31.TabIndex = 22;
            this.label31.Text = "Đơn vị số lượng trong đơn giá tính thuế";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(380, 68);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(90, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "Đơn giá tính thuế";
            // 
            // txtTriGiaTinhThue
            // 
            this.txtTriGiaTinhThue.DecimalDigits = 20;
            this.txtTriGiaTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaTinhThue.Location = new System.Drawing.Point(113, 35);
            this.txtTriGiaTinhThue.MaxLength = 15;
            this.txtTriGiaTinhThue.Name = "txtTriGiaTinhThue";
            this.txtTriGiaTinhThue.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtTriGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThue.Size = new System.Drawing.Size(159, 21);
            this.txtTriGiaTinhThue.TabIndex = 4;
            this.txtTriGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThue.Value = null;
            this.txtTriGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDV_SL_TrongDonGiaTinhThue
            // 
            this.txtDV_SL_TrongDonGiaTinhThue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDV_SL_TrongDonGiaTinhThue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDV_SL_TrongDonGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDV_SL_TrongDonGiaTinhThue.Location = new System.Drawing.Point(579, 89);
            this.txtDV_SL_TrongDonGiaTinhThue.MaxLength = 12;
            this.txtDV_SL_TrongDonGiaTinhThue.Name = "txtDV_SL_TrongDonGiaTinhThue";
            this.txtDV_SL_TrongDonGiaTinhThue.ReadOnly = true;
            this.txtDV_SL_TrongDonGiaTinhThue.Size = new System.Drawing.Size(56, 21);
            this.txtDV_SL_TrongDonGiaTinhThue.TabIndex = 13;
            this.txtDV_SL_TrongDonGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDV_SL_TrongDonGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(7, 93);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(169, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "Số danh mục miễn thuế xuất khẩu";
            // 
            // txtSoDongDMMienThue
            // 
            this.txtSoDongDMMienThue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoDongDMMienThue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoDongDMMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDongDMMienThue.Location = new System.Drawing.Point(247, 113);
            this.txtSoDongDMMienThue.MaxLength = 12;
            this.txtSoDongDMMienThue.Name = "txtSoDongDMMienThue";
            this.txtSoDongDMMienThue.Size = new System.Drawing.Size(93, 21);
            this.txtSoDongDMMienThue.TabIndex = 14;
            this.txtSoDongDMMienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDongDMMienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(7, 117);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(228, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "Số dòng tương ứng trong danh mục miễn thuế";
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.uiGroupBox6);
            this.uiTabPage2.Controls.Add(this.uiGroupBox5);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(895, 191);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Tiền lệ phí và bảo hiểm";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.editBox2);
            this.uiGroupBox6.Controls.Add(this.ucCategory6);
            this.uiGroupBox6.Controls.Add(this.label29);
            this.uiGroupBox6.Controls.Add(this.label28);
            this.uiGroupBox6.Controls.Add(this.numericEditBox2);
            this.uiGroupBox6.Controls.Add(this.numericEditBox6);
            this.uiGroupBox6.Controls.Add(this.label25);
            this.uiGroupBox6.Location = new System.Drawing.Point(323, 3);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(300, 117);
            this.uiGroupBox6.TabIndex = 20;
            this.uiGroupBox6.Text = "Tiền bảo hiểm";
            // 
            // editBox2
            // 
            this.editBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.editBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.editBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editBox2.Location = new System.Drawing.Point(95, 19);
            this.editBox2.MaxLength = 12;
            this.editBox2.Name = "editBox2";
            this.editBox2.Size = new System.Drawing.Size(185, 21);
            this.editBox2.TabIndex = 0;
            this.editBox2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.editBox2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ucCategory6
            // 
            this.ucCategory6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucCategory6.Appearance.Options.UseBackColor = true;
            this.ucCategory6.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucCategory6.Code = "";
            this.ucCategory6.ColorControl = System.Drawing.Color.Empty;
            this.ucCategory6.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategory6.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategory6.IsOnlyWarning = false;
            this.ucCategory6.IsValidate = true;
            this.ucCategory6.Location = new System.Drawing.Point(201, 49);
            this.ucCategory6.Name = "ucCategory6";
            this.ucCategory6.Name_VN = "";
            this.ucCategory6.SetOnlyWarning = false;
            this.ucCategory6.SetValidate = false;
            this.ucCategory6.ShowColumnCode = true;
            this.ucCategory6.ShowColumnName = false;
            this.ucCategory6.Size = new System.Drawing.Size(79, 21);
            this.ucCategory6.TabIndex = 2;
            this.ucCategory6.TagName = "";
            this.ucCategory6.Where = null;
            this.ucCategory6.WhereCondition = "";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(6, 23);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(44, 13);
            this.label29.TabIndex = 18;
            this.label29.Text = "Đơn giá";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(6, 84);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(58, 13);
            this.label28.TabIndex = 18;
            this.label28.Text = "Khoản tiền";
            // 
            // numericEditBox2
            // 
            this.numericEditBox2.DecimalDigits = 20;
            this.numericEditBox2.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox2.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.numericEditBox2.Location = new System.Drawing.Point(95, 80);
            this.numericEditBox2.MaxLength = 15;
            this.numericEditBox2.Name = "numericEditBox2";
            this.numericEditBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox2.Size = new System.Drawing.Size(185, 21);
            this.numericEditBox2.TabIndex = 3;
            this.numericEditBox2.Text = "0";
            this.numericEditBox2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // numericEditBox6
            // 
            this.numericEditBox6.DecimalDigits = 20;
            this.numericEditBox6.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox6.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.numericEditBox6.Location = new System.Drawing.Point(95, 49);
            this.numericEditBox6.MaxLength = 15;
            this.numericEditBox6.Name = "numericEditBox6";
            this.numericEditBox6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox6.Size = new System.Drawing.Size(100, 21);
            this.numericEditBox6.TabIndex = 1;
            this.numericEditBox6.Text = "0";
            this.numericEditBox6.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox6.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox6.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(6, 53);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 13);
            this.label25.TabIndex = 19;
            this.label25.Text = "Số lượng";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.editBox1);
            this.uiGroupBox5.Controls.Add(this.ucCategory5);
            this.uiGroupBox5.Controls.Add(this.label27);
            this.uiGroupBox5.Controls.Add(this.label24);
            this.uiGroupBox5.Controls.Add(this.numericEditBox1);
            this.uiGroupBox5.Controls.Add(this.numericEditBox5);
            this.uiGroupBox5.Controls.Add(this.label23);
            this.uiGroupBox5.Location = new System.Drawing.Point(28, 3);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(274, 117);
            this.uiGroupBox5.TabIndex = 0;
            this.uiGroupBox5.Text = "Tiền lệ phí";
            // 
            // editBox1
            // 
            this.editBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.editBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.editBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editBox1.Location = new System.Drawing.Point(84, 19);
            this.editBox1.MaxLength = 12;
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(185, 21);
            this.editBox1.TabIndex = 0;
            this.editBox1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.editBox1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ucCategory5
            // 
            this.ucCategory5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucCategory5.Appearance.Options.UseBackColor = true;
            this.ucCategory5.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucCategory5.Code = "";
            this.ucCategory5.ColorControl = System.Drawing.Color.Empty;
            this.ucCategory5.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategory5.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategory5.IsOnlyWarning = false;
            this.ucCategory5.IsValidate = true;
            this.ucCategory5.Location = new System.Drawing.Point(190, 49);
            this.ucCategory5.Name = "ucCategory5";
            this.ucCategory5.Name_VN = "";
            this.ucCategory5.SetOnlyWarning = false;
            this.ucCategory5.SetValidate = false;
            this.ucCategory5.ShowColumnCode = true;
            this.ucCategory5.ShowColumnName = true;
            this.ucCategory5.Size = new System.Drawing.Size(79, 21);
            this.ucCategory5.TabIndex = 2;
            this.ucCategory5.TagName = "";
            this.ucCategory5.Where = null;
            this.ucCategory5.WhereCondition = "";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(6, 23);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(44, 13);
            this.label27.TabIndex = 18;
            this.label27.Text = "Đơn giá";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(6, 84);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(58, 13);
            this.label24.TabIndex = 18;
            this.label24.Text = "Khoản tiền";
            // 
            // numericEditBox1
            // 
            this.numericEditBox1.DecimalDigits = 20;
            this.numericEditBox1.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox1.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.numericEditBox1.Location = new System.Drawing.Point(84, 80);
            this.numericEditBox1.MaxLength = 15;
            this.numericEditBox1.Name = "numericEditBox1";
            this.numericEditBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox1.Size = new System.Drawing.Size(185, 21);
            this.numericEditBox1.TabIndex = 3;
            this.numericEditBox1.Text = "0";
            this.numericEditBox1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // numericEditBox5
            // 
            this.numericEditBox5.DecimalDigits = 20;
            this.numericEditBox5.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox5.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.numericEditBox5.Location = new System.Drawing.Point(84, 49);
            this.numericEditBox5.MaxLength = 15;
            this.numericEditBox5.Name = "numericEditBox5";
            this.numericEditBox5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox5.Size = new System.Drawing.Size(100, 21);
            this.numericEditBox5.TabIndex = 1;
            this.numericEditBox5.Text = "0";
            this.numericEditBox5.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox5.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox5.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 53);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 13);
            this.label23.TabIndex = 19;
            this.label23.Text = "Số lượng";
            // 
            // ctrMaVanBan5
            // 
            this.ctrMaVanBan5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBan5.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBan5.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBan5.Code = "";
            this.ctrMaVanBan5.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBan5.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBan5.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBan5.IsOnlyWarning = false;
            this.ctrMaVanBan5.IsValidate = true;
            this.ctrMaVanBan5.Location = new System.Drawing.Point(433, 174);
            this.ctrMaVanBan5.Name = "ctrMaVanBan5";
            this.ctrMaVanBan5.Name_VN = "";
            this.ctrMaVanBan5.SetOnlyWarning = false;
            this.ctrMaVanBan5.SetValidate = false;
            this.ctrMaVanBan5.ShowColumnCode = true;
            this.ctrMaVanBan5.ShowColumnName = false;
            this.ctrMaVanBan5.Size = new System.Drawing.Size(60, 20);
            this.ctrMaVanBan5.TabIndex = 15;
            this.ctrMaVanBan5.TagName = "";
            this.ctrMaVanBan5.Where = null;
            this.ctrMaVanBan5.WhereCondition = "";
            // 
            // ctrMaVanBan4
            // 
            this.ctrMaVanBan4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBan4.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBan4.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBan4.Code = "";
            this.ctrMaVanBan4.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBan4.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBan4.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBan4.IsOnlyWarning = false;
            this.ctrMaVanBan4.IsValidate = true;
            this.ctrMaVanBan4.Location = new System.Drawing.Point(364, 174);
            this.ctrMaVanBan4.Name = "ctrMaVanBan4";
            this.ctrMaVanBan4.Name_VN = "";
            this.ctrMaVanBan4.SetOnlyWarning = false;
            this.ctrMaVanBan4.SetValidate = false;
            this.ctrMaVanBan4.ShowColumnCode = true;
            this.ctrMaVanBan4.ShowColumnName = false;
            this.ctrMaVanBan4.Size = new System.Drawing.Size(60, 20);
            this.ctrMaVanBan4.TabIndex = 14;
            this.ctrMaVanBan4.TagName = "";
            this.ctrMaVanBan4.Where = null;
            this.ctrMaVanBan4.WhereCondition = "";
            // 
            // ctrMaVanBan3
            // 
            this.ctrMaVanBan3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBan3.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBan3.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBan3.Code = "";
            this.ctrMaVanBan3.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBan3.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBan3.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBan3.IsOnlyWarning = false;
            this.ctrMaVanBan3.IsValidate = true;
            this.ctrMaVanBan3.Location = new System.Drawing.Point(292, 174);
            this.ctrMaVanBan3.Name = "ctrMaVanBan3";
            this.ctrMaVanBan3.Name_VN = "";
            this.ctrMaVanBan3.SetOnlyWarning = false;
            this.ctrMaVanBan3.SetValidate = false;
            this.ctrMaVanBan3.ShowColumnCode = true;
            this.ctrMaVanBan3.ShowColumnName = false;
            this.ctrMaVanBan3.Size = new System.Drawing.Size(60, 20);
            this.ctrMaVanBan3.TabIndex = 13;
            this.ctrMaVanBan3.TagName = "";
            this.ctrMaVanBan3.Where = null;
            this.ctrMaVanBan3.WhereCondition = "";
            // 
            // ctrMaVanBan2
            // 
            this.ctrMaVanBan2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBan2.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBan2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBan2.Code = "";
            this.ctrMaVanBan2.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBan2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBan2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBan2.IsOnlyWarning = false;
            this.ctrMaVanBan2.IsValidate = true;
            this.ctrMaVanBan2.Location = new System.Drawing.Point(226, 174);
            this.ctrMaVanBan2.Name = "ctrMaVanBan2";
            this.ctrMaVanBan2.Name_VN = "";
            this.ctrMaVanBan2.SetOnlyWarning = false;
            this.ctrMaVanBan2.SetValidate = false;
            this.ctrMaVanBan2.ShowColumnCode = true;
            this.ctrMaVanBan2.ShowColumnName = false;
            this.ctrMaVanBan2.Size = new System.Drawing.Size(60, 20);
            this.ctrMaVanBan2.TabIndex = 12;
            this.ctrMaVanBan2.TagName = "";
            this.ctrMaVanBan2.Where = null;
            this.ctrMaVanBan2.WhereCondition = "";
            // 
            // ctrMaVanBan1
            // 
            this.ctrMaVanBan1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBan1.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBan1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBan1.Code = "";
            this.ctrMaVanBan1.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBan1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBan1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBan1.IsOnlyWarning = false;
            this.ctrMaVanBan1.IsValidate = true;
            this.ctrMaVanBan1.Location = new System.Drawing.Point(152, 174);
            this.ctrMaVanBan1.Name = "ctrMaVanBan1";
            this.ctrMaVanBan1.Name_VN = "";
            this.ctrMaVanBan1.SetOnlyWarning = false;
            this.ctrMaVanBan1.SetValidate = false;
            this.ctrMaVanBan1.ShowColumnCode = true;
            this.ctrMaVanBan1.ShowColumnName = false;
            this.ctrMaVanBan1.Size = new System.Drawing.Size(60, 20);
            this.ctrMaVanBan1.TabIndex = 11;
            this.ctrMaVanBan1.TagName = "";
            this.ctrMaVanBan1.Where = null;
            this.ctrMaVanBan1.WhereCondition = "";
            // 
            // ctrDVTDonGia
            // 
            this.ctrDVTDonGia.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrDVTDonGia.Appearance.Options.UseBackColor = true;
            this.ctrDVTDonGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTDonGia.Code = "";
            this.ctrDVTDonGia.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVTDonGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTDonGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTDonGia.IsOnlyWarning = false;
            this.ctrDVTDonGia.IsValidate = true;
            this.ctrDVTDonGia.Location = new System.Drawing.Point(550, 120);
            this.ctrDVTDonGia.Name = "ctrDVTDonGia";
            this.ctrDVTDonGia.Name_VN = "";
            this.ctrDVTDonGia.SetOnlyWarning = false;
            this.ctrDVTDonGia.SetValidate = false;
            this.ctrDVTDonGia.ShowColumnCode = true;
            this.ctrDVTDonGia.ShowColumnName = false;
            this.ctrDVTDonGia.Size = new System.Drawing.Size(143, 20);
            this.ctrDVTDonGia.TabIndex = 9;
            this.ctrDVTDonGia.TagName = "";
            this.ctrDVTDonGia.Where = null;
            this.ctrDVTDonGia.WhereCondition = "";
            this.ctrDVTDonGia.Leave += new System.EventHandler(this.ctrDVTDonGia_Leave);
            // 
            // ctrMaTTDonGia
            // 
            this.ctrMaTTDonGia.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTDonGia.Appearance.Options.UseBackColor = true;
            this.ctrMaTTDonGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTDonGia.Code = "";
            this.ctrMaTTDonGia.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTDonGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTDonGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTDonGia.IsOnlyWarning = false;
            this.ctrMaTTDonGia.IsValidate = true;
            this.ctrMaTTDonGia.Location = new System.Drawing.Point(698, 97);
            this.ctrMaTTDonGia.Name = "ctrMaTTDonGia";
            this.ctrMaTTDonGia.Name_VN = "";
            this.ctrMaTTDonGia.SetOnlyWarning = false;
            this.ctrMaTTDonGia.SetValidate = false;
            this.ctrMaTTDonGia.ShowColumnCode = true;
            this.ctrMaTTDonGia.ShowColumnName = false;
            this.ctrMaTTDonGia.Size = new System.Drawing.Size(64, 20);
            this.ctrMaTTDonGia.TabIndex = 8;
            this.ctrMaTTDonGia.TagName = "";
            this.ctrMaTTDonGia.Where = null;
            this.ctrMaTTDonGia.WhereCondition = "";
            // 
            // ctrDVTLuong2
            // 
            this.ctrDVTLuong2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDVTLuong2.Appearance.Options.UseBackColor = true;
            this.ctrDVTLuong2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong2.Code = "";
            this.ctrDVTLuong2.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVTLuong2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong2.IsOnlyWarning = false;
            this.ctrDVTLuong2.IsValidate = true;
            this.ctrDVTLuong2.Location = new System.Drawing.Point(313, 98);
            this.ctrDVTLuong2.Name = "ctrDVTLuong2";
            this.ctrDVTLuong2.Name_VN = "";
            this.ctrDVTLuong2.SetOnlyWarning = false;
            this.ctrDVTLuong2.SetValidate = false;
            this.ctrDVTLuong2.ShowColumnCode = true;
            this.ctrDVTLuong2.ShowColumnName = false;
            this.ctrDVTLuong2.Size = new System.Drawing.Size(65, 21);
            this.ctrDVTLuong2.TabIndex = 17;
            this.ctrDVTLuong2.TagName = "";
            this.ctrDVTLuong2.Where = null;
            this.ctrDVTLuong2.WhereCondition = "";
            // 
            // ctrDVTLuong1
            // 
            this.ctrDVTLuong1.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrDVTLuong1.Appearance.Options.UseBackColor = true;
            this.ctrDVTLuong1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong1.Code = "";
            this.ctrDVTLuong1.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVTLuong1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong1.IsOnlyWarning = false;
            this.ctrDVTLuong1.IsValidate = true;
            this.ctrDVTLuong1.Location = new System.Drawing.Point(313, 71);
            this.ctrDVTLuong1.Name = "ctrDVTLuong1";
            this.ctrDVTLuong1.Name_VN = "";
            this.ctrDVTLuong1.SetOnlyWarning = false;
            this.ctrDVTLuong1.SetValidate = false;
            this.ctrDVTLuong1.ShowColumnCode = true;
            this.ctrDVTLuong1.ShowColumnName = false;
            this.ctrDVTLuong1.Size = new System.Drawing.Size(65, 21);
            this.ctrDVTLuong1.TabIndex = 5;
            this.ctrDVTLuong1.TagName = "";
            this.ctrDVTLuong1.Where = null;
            this.ctrDVTLuong1.WhereCondition = "";
            this.ctrDVTLuong1.Leave += new System.EventHandler(this.ctrDVTLuong1_Leave);
            // 
            // ctrMaSoHang
            // 
            this.ctrMaSoHang.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrMaSoHang.Appearance.Options.UseBackColor = true;
            this.ctrMaSoHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ctrMaSoHang.Code = "";
            this.ctrMaSoHang.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaSoHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaSoHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaSoHang.IsOnlyWarning = false;
            this.ctrMaSoHang.IsValidate = true;
            this.ctrMaSoHang.Location = new System.Drawing.Point(91, 46);
            this.ctrMaSoHang.Name = "ctrMaSoHang";
            this.ctrMaSoHang.Name_VN = "";
            this.ctrMaSoHang.SetOnlyWarning = false;
            this.ctrMaSoHang.SetValidate = false;
            this.ctrMaSoHang.ShowColumnCode = true;
            this.ctrMaSoHang.ShowColumnName = false;
            this.ctrMaSoHang.Size = new System.Drawing.Size(159, 21);
            this.ctrMaSoHang.TabIndex = 3;
            this.ctrMaSoHang.TagName = "";
            this.ctrMaSoHang.Where = null;
            this.ctrMaSoHang.WhereCondition = "";
            // 
            // txtMaHangHoa
            // 
            this.txtMaHangHoa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHangHoa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHangHoa.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangHoa.Location = new System.Drawing.Point(278, 18);
            this.txtMaHangHoa.MaxLength = 48;
            this.txtMaHangHoa.Name = "txtMaHangHoa";
            this.txtMaHangHoa.Size = new System.Drawing.Size(126, 21);
            this.txtMaHangHoa.TabIndex = 1;
            this.txtMaHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHangHoa.ButtonClick += new System.EventHandler(this.txtMaQuanLy_ButtonClick);
            this.txtMaHangHoa.Leave += new System.EventHandler(this.txtMaHangHoa_Leave);
            // 
            // txtMaQuanLy
            // 
            this.txtMaQuanLy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaQuanLy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaQuanLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaQuanLy.Location = new System.Drawing.Point(91, 18);
            this.txtMaQuanLy.MaxLength = 48;
            this.txtMaQuanLy.Name = "txtMaQuanLy";
            this.txtMaQuanLy.Size = new System.Drawing.Size(61, 21);
            this.txtMaQuanLy.TabIndex = 0;
            this.txtMaQuanLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaQuanLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaHoaDon
            // 
            this.txtTriGiaHoaDon.BackColor = System.Drawing.SystemColors.Info;
            this.txtTriGiaHoaDon.DecimalDigits = 20;
            this.txtTriGiaHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaHoaDon.FormatString = "G20";
            this.txtTriGiaHoaDon.Location = new System.Drawing.Point(550, 70);
            this.txtTriGiaHoaDon.Name = "txtTriGiaHoaDon";
            this.txtTriGiaHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaHoaDon.Size = new System.Drawing.Size(143, 21);
            this.txtTriGiaHoaDon.TabIndex = 6;
            this.txtTriGiaHoaDon.Text = "0";
            this.txtTriGiaHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaHoaDon.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTriGiaHoaDon.Leave += new System.EventHandler(this.txtTriGiaHoaDon_Leave);
            // 
            // txtDonGiaHoaDon
            // 
            this.txtDonGiaHoaDon.BackColor = System.Drawing.SystemColors.Info;
            this.txtDonGiaHoaDon.DecimalDigits = 20;
            this.txtDonGiaHoaDon.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDonGiaHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaHoaDon.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaHoaDon.Location = new System.Drawing.Point(550, 97);
            this.txtDonGiaHoaDon.MaxLength = 15;
            this.txtDonGiaHoaDon.Name = "txtDonGiaHoaDon";
            this.txtDonGiaHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaHoaDon.Size = new System.Drawing.Size(143, 21);
            this.txtDonGiaHoaDon.TabIndex = 7;
            this.txtDonGiaHoaDon.Text = "0";
            this.txtDonGiaHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonGiaHoaDon.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDonGiaHoaDon.Leave += new System.EventHandler(this.txtDonGiaHoaDon_Leave);
            // 
            // txtSoLuong2
            // 
            this.txtSoLuong2.BackColor = System.Drawing.SystemColors.Info;
            this.txtSoLuong2.DecimalDigits = 20;
            this.txtSoLuong2.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong2.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong2.Location = new System.Drawing.Point(91, 98);
            this.txtSoLuong2.MaxLength = 15;
            this.txtSoLuong2.Name = "txtSoLuong2";
            this.txtSoLuong2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong2.Size = new System.Drawing.Size(159, 21);
            this.txtSoLuong2.TabIndex = 16;
            this.txtSoLuong2.Text = "0";
            this.txtSoLuong2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuong1
            // 
            this.txtSoLuong1.BackColor = System.Drawing.SystemColors.Info;
            this.txtSoLuong1.DecimalDigits = 20;
            this.txtSoLuong1.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong1.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong1.Location = new System.Drawing.Point(91, 71);
            this.txtSoLuong1.MaxLength = 15;
            this.txtSoLuong1.Name = "txtSoLuong1";
            this.txtSoLuong1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong1.Size = new System.Drawing.Size(159, 21);
            this.txtSoLuong1.TabIndex = 4;
            this.txtSoLuong1.Text = "0";
            this.txtSoLuong1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuong1.Leave += new System.EventHandler(this.txtSoLuong1_Leave);
            // 
            // txtTenHang
            // 
            this.txtTenHang.BackColor = System.Drawing.SystemColors.Info;
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(465, 16);
            this.txtTenHang.MaxLength = 255;
            this.txtTenHang.Multiline = true;
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(302, 49);
            this.txtTenHang.TabIndex = 2;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(7, 177);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(136, 13);
            this.label19.TabIndex = 29;
            this.label19.Text = "Mã văn bản pháp luật khác";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(464, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 34;
            this.label8.Text = "Trị giá hóa đơn";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(252, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Đơn vị tính";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(464, 124);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(83, 13);
            this.label32.TabIndex = 11;
            this.label32.Text = "ĐVT đơn giá HĐ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(464, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Đơn giá hóa đơn";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(252, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Đơn vị tính";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 155);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(307, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Số thứ tự dòng hàng trên tờ khai tạm nhập tái xuất tương ứng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Số lượng (2)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(407, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Tên hàng";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Số lượng (1)";
            // 
            // lblMaHang
            // 
            this.lblMaHang.AutoSize = true;
            this.lblMaHang.BackColor = System.Drawing.Color.Transparent;
            this.lblMaHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaHang.Location = new System.Drawing.Point(211, 22);
            this.lblMaHang.Name = "lblMaHang";
            this.lblMaHang.Size = new System.Drawing.Size(69, 13);
            this.lblMaHang.TabIndex = 27;
            this.lblMaHang.Text = "Mã hàng hóa";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(7, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Mã số hàng hóa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Mã quản lý riêng";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(816, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 25;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(733, 6);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(70, 23);
            this.btnXoa.TabIndex = 24;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = ((System.Drawing.Image)(resources.GetObject("btnGhi.Image")));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGhi.Location = new System.Drawing.Point(652, 6);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(70, 23);
            this.btnGhi.TabIndex = 23;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.BackColor = System.Drawing.Color.MistyRose;
            this.uiGroupBox7.Controls.Add(this.linkSuaSoLuong);
            this.uiGroupBox7.Controls.Add(this.btnClose);
            this.uiGroupBox7.Controls.Add(this.btnXoa);
            this.uiGroupBox7.Controls.Add(this.uiButton1);
            this.uiGroupBox7.Controls.Add(this.btnGhi);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox7.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 430);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(903, 30);
            this.uiGroupBox7.TabIndex = 2;
            // 
            // linkSuaSoLuong
            // 
            this.linkSuaSoLuong.AutoSize = true;
            this.linkSuaSoLuong.BackColor = System.Drawing.Color.Transparent;
            this.linkSuaSoLuong.Location = new System.Drawing.Point(11, 14);
            this.linkSuaSoLuong.Name = "linkSuaSoLuong";
            this.linkSuaSoLuong.Size = new System.Drawing.Size(153, 13);
            this.linkSuaSoLuong.TabIndex = 75;
            this.linkSuaSoLuong.TabStop = true;
            this.linkSuaSoLuong.Text = "Điều chỉnh Thông tin hàng hóa";
            this.linkSuaSoLuong.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkSuaSoLuong_LinkClicked);
            // 
            // uiButton1
            // 
            this.uiButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Image = ((System.Drawing.Image)(resources.GetObject("uiButton1.Image")));
            this.uiButton1.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton1.Location = new System.Drawing.Point(523, 6);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(123, 23);
            this.uiButton1.TabIndex = 23;
            this.uiButton1.Text = "Xuất excel hàng";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // VNACC_HangMauDichXuatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.ClientSize = new System.Drawing.Size(1109, 702);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_HangMauDichXuatForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin hàng hóa";
            this.Load += new System.EventHandler(this.VNACC_HangMauDichXuatForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.uiTabPage1.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        public Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaHoaDon;
        public System.Windows.Forms.Label label8;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaHoaDon;
        public System.Windows.Forms.Label label3;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong2;
        public System.Windows.Forms.Label label7;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong1;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label9;
        public Janus.Windows.GridEX.EditControls.EditBox txtTenHang;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label10;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaQuanLy;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label12;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtThueSuatTuyetDoi;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Label label20;
        public Janus.Windows.GridEX.EditControls.EditBox txtSoDongDMMienThue;
        public System.Windows.Forms.Label label21;
        public Janus.Windows.GridEX.GridEX grList;
        public Janus.Windows.EditControls.UIButton btnClose;
        public Janus.Windows.EditControls.UIButton btnXoa;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienMienGiam;
        public System.Windows.Forms.Label label14;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThue;
        public Janus.Windows.EditControls.UIButton btnGhi;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaSoHang;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTuyetDoi;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTriGiaTinhThue;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTDonGia;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong2;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong1;
        public System.Windows.Forms.Label label16;
        public Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        public Janus.Windows.UI.Tab.UITab uiTab1;
        public Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        public Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTriGiaTinhThueS;
        public System.Windows.Forms.Label label17;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThueS;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTCuaDonGiaTinhThue;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTMaMienGiam;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTSoTienThueXuatKhau;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaTinhThue;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienThue;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.Label label18;
        public Janus.Windows.GridEX.EditControls.EditBox txtDV_SL_TrongDonGiaTinhThue;
        public Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox5;
        public System.Windows.Forms.Label label25;
        public Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox6;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory5;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory6;
        public Janus.Windows.GridEX.EditControls.EditBox txtPhanLoai_TS_ThueXuatKhau;
        public System.Windows.Forms.Label label26;
        public Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        public Janus.Windows.GridEX.EditControls.EditBox editBox2;
        public Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox2;
        public Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        public Janus.Windows.GridEX.EditControls.EditBox editBox1;
        public Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox1;
        public System.Windows.Forms.Label label29;
        public System.Windows.Forms.Label label28;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label label23;
        public Janus.Windows.GridEX.EditControls.EditBox txtDieuKhoanMienGiam;
        public System.Windows.Forms.Label label30;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaMienGiamThue;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTTuyetDoi;
        public System.Windows.Forms.Label label35;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label31;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBan5;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBan4;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBan3;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBan2;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBan1;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTDonGia;
        public System.Windows.Forms.Label label32;
        public Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaHangHoa;
        public System.Windows.Forms.Label lblMaHang;
        public Janus.Windows.GridEX.EditControls.EditBox txtSoDMMienThue;
        public Janus.Windows.GridEX.EditControls.EditBox txtSoTTDongHangTKTNTX;
        public System.Windows.Forms.LinkLabel CopyDanhMucMienThue;
        public System.Windows.Forms.LinkLabel CopyMienGiam;
        public System.Windows.Forms.LinkLabel linkLabel1;
        public Janus.Windows.EditControls.UIButton btnDVT2;
        public Janus.Windows.EditControls.UIButton btnDVT1;
        public System.Windows.Forms.LinkLabel linkMaTTCopy;
        public Janus.Windows.EditControls.UIButton uiButton1;
        public System.Windows.Forms.LinkLabel linkLabel3;
        public System.Windows.Forms.Label label34;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrNuocXuatXu;
        public System.Windows.Forms.LinkLabel linkLabel2;
        public System.Windows.Forms.LinkLabel linkLabel4;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtThueSuat;
        public System.Windows.Forms.LinkLabel linkSuaSoLuong;
        public System.Windows.Forms.LinkLabel linkLabel5;
        public System.Windows.Forms.LinkLabel linkLabel6;
    }
}
