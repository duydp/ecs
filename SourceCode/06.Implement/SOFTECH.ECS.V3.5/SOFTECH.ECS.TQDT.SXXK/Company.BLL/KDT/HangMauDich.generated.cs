using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.BLL.KDT
{
	public partial class HangMauDich : ICloneable
	{

		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public int SoThuTuHang { set; get; }
		public string MaHS { set; get; }
		public string MaPhu { set; get; }
		public string TenHang { set; get; }
		public string NuocXX_ID { set; get; }
		public string DVT_ID { set; get; }
		public decimal SoLuong { set; get; }
		public decimal TrongLuong { set; get; }
		public decimal DonGiaKB { set; get; }
		public decimal DonGiaTT { set; get; }
		public decimal TriGiaKB { set; get; }
		public decimal TriGiaTT { set; get; }
		public decimal TriGiaKB_VND { set; get; }
		public decimal ThueSuatXNK { set; get; }
		public decimal ThueSuatTTDB { set; get; }
		public decimal ThueSuatGTGT { set; get; }
		public decimal ThueXNK { set; get; }
		public decimal ThueTTDB { set; get; }
		public decimal ThueGTGT { set; get; }
		public decimal PhuThu { set; get; }
		public decimal TyLeThuKhac { set; get; }
		public decimal TriGiaThuKhac { set; get; }
		public byte MienThue { set; get; }
		public string Ma_HTS { set; get; }
		public string DVT_HTS { set; get; }
		public decimal SoLuong_HTS { set; get; }
		public string ThueSuatGiam { set; get; }
		public bool FOC { set; get; }
		public double DonGiaTuyetDoi { set; get; }
		public string MaHSMoRong { set; get; }
		public string NhanHieu { set; get; }
		public string QuyCachPhamChat { set; get; }
		public string ThanhPhan { set; get; }
		public string Model { set; get; }
		public string MaHangSX { set; get; }
		public string TenHangSX { set; get; }
		public bool ThueTuyetDoi { set; get; }
		public double ThueSuatXNKGiam { set; get; }
		public double ThueSuatTTDBGiam { set; get; }
		public double ThueSuatVATGiam { set; get; }
		public decimal ThueBVMT { set; get; }
		public double ThueSuatBVMT { set; get; }
		public double ThueSuatBVMTGiam { set; get; }
		public decimal ThueChongPhaGia { set; get; }
		public double ThueSuatChongPhaGia { set; get; }
		public double ThueSuatChongPhaGiaGiam { set; get; }
		public bool isHangCu { set; get; }
		public string BieuThueXNK { set; get; }
		public string BieuThueTTDB { set; get; }
		public string BieuThueBVMT { set; get; }
		public string BieuThueGTGT { set; get; }
		public string BieuThueCBPG { set; get; }
		public string ThongTinKhac { set; get; }
		public bool IsHangDongBo { set; get; }
		public string CheDoUuDai { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<HangMauDich> ConvertToCollection(IDataReader reader)
		{
			IList<HangMauDich> collection = new List<HangMauDich>();
			while (reader.Read())
			{
				HangMauDich entity = new HangMauDich();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGiam"))) entity.ThueSuatGiam = reader.GetString(reader.GetOrdinal("ThueSuatGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) entity.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) entity.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) entity.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) entity.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) entity.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) entity.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Model"))) entity.Model = reader.GetString(reader.GetOrdinal("Model"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) entity.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) entity.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) entity.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatXNKGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatTTDBGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatVATGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueBVMT"))) entity.ThueBVMT = reader.GetDecimal(reader.GetOrdinal("ThueBVMT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMT"))) entity.ThueSuatBVMT = reader.GetDouble(reader.GetOrdinal("ThueSuatBVMT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMTGiam"))) entity.ThueSuatBVMTGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatBVMTGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueChongPhaGia"))) entity.ThueChongPhaGia = reader.GetDecimal(reader.GetOrdinal("ThueChongPhaGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGia"))) entity.ThueSuatChongPhaGia = reader.GetDouble(reader.GetOrdinal("ThueSuatChongPhaGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"))) entity.ThueSuatChongPhaGiaGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("isHangCu"))) entity.isHangCu = reader.GetBoolean(reader.GetOrdinal("isHangCu"));
				if (!reader.IsDBNull(reader.GetOrdinal("BieuThueXNK"))) entity.BieuThueXNK = reader.GetString(reader.GetOrdinal("BieuThueXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("BieuThueTTDB"))) entity.BieuThueTTDB = reader.GetString(reader.GetOrdinal("BieuThueTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("BieuThueBVMT"))) entity.BieuThueBVMT = reader.GetString(reader.GetOrdinal("BieuThueBVMT"));
				if (!reader.IsDBNull(reader.GetOrdinal("BieuThueGTGT"))) entity.BieuThueGTGT = reader.GetString(reader.GetOrdinal("BieuThueGTGT"));
				if (!reader.IsDBNull(reader.GetOrdinal("BieuThueCBPG"))) entity.BieuThueCBPG = reader.GetString(reader.GetOrdinal("BieuThueCBPG"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsHangDongBo"))) entity.IsHangDongBo = reader.GetBoolean(reader.GetOrdinal("IsHangDongBo"));
				if (!reader.IsDBNull(reader.GetOrdinal("CheDoUuDai"))) entity.CheDoUuDai = reader.GetString(reader.GetOrdinal("CheDoUuDai"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
        protected static IList<HangMauDich> ConvertToSmallCollection(IDataReader reader)
        {
            IList<HangMauDich> collection = new List<HangMauDich>();
            while (reader.Read())
            {
                HangMauDich entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));                
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
		public static bool Find(IList<HangMauDich> collection, long id)
        {
            foreach (HangMauDich item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_HangMauDich VALUES(@TKMD_ID, @SoThuTuHang, @MaHS, @MaPhu, @TenHang, @NuocXX_ID, @DVT_ID, @SoLuong, @TrongLuong, @DonGiaKB, @DonGiaTT, @TriGiaKB, @TriGiaTT, @TriGiaKB_VND, @ThueSuatXNK, @ThueSuatTTDB, @ThueSuatGTGT, @ThueXNK, @ThueTTDB, @ThueGTGT, @PhuThu, @TyLeThuKhac, @TriGiaThuKhac, @MienThue, @Ma_HTS, @DVT_HTS, @SoLuong_HTS, @ThueSuatGiam, @FOC, @DonGiaTuyetDoi, @MaHSMoRong, @NhanHieu, @QuyCachPhamChat, @ThanhPhan, @Model, @MaHangSX, @TenHangSX, @ThueTuyetDoi, @ThueSuatXNKGiam, @ThueSuatTTDBGiam, @ThueSuatVATGiam, @ThueBVMT, @ThueSuatBVMT, @ThueSuatBVMTGiam, @ThueChongPhaGia, @ThueSuatChongPhaGia, @ThueSuatChongPhaGiaGiam, @isHangCu, @BieuThueXNK, @BieuThueTTDB, @BieuThueBVMT, @BieuThueGTGT, @BieuThueCBPG, @ThongTinKhac, @IsHangDongBo, @CheDoUuDai)";
            string update = "UPDATE t_KDT_HangMauDich SET TKMD_ID = @TKMD_ID, SoThuTuHang = @SoThuTuHang, MaHS = @MaHS, MaPhu = @MaPhu, TenHang = @TenHang, NuocXX_ID = @NuocXX_ID, DVT_ID = @DVT_ID, SoLuong = @SoLuong, TrongLuong = @TrongLuong, DonGiaKB = @DonGiaKB, DonGiaTT = @DonGiaTT, TriGiaKB = @TriGiaKB, TriGiaTT = @TriGiaTT, TriGiaKB_VND = @TriGiaKB_VND, ThueSuatXNK = @ThueSuatXNK, ThueSuatTTDB = @ThueSuatTTDB, ThueSuatGTGT = @ThueSuatGTGT, ThueXNK = @ThueXNK, ThueTTDB = @ThueTTDB, ThueGTGT = @ThueGTGT, PhuThu = @PhuThu, TyLeThuKhac = @TyLeThuKhac, TriGiaThuKhac = @TriGiaThuKhac, MienThue = @MienThue, Ma_HTS = @Ma_HTS, DVT_HTS = @DVT_HTS, SoLuong_HTS = @SoLuong_HTS, ThueSuatGiam = @ThueSuatGiam, FOC = @FOC, DonGiaTuyetDoi = @DonGiaTuyetDoi, MaHSMoRong = @MaHSMoRong, NhanHieu = @NhanHieu, QuyCachPhamChat = @QuyCachPhamChat, ThanhPhan = @ThanhPhan, Model = @Model, MaHangSX = @MaHangSX, TenHangSX = @TenHangSX, ThueTuyetDoi = @ThueTuyetDoi, ThueSuatXNKGiam = @ThueSuatXNKGiam, ThueSuatTTDBGiam = @ThueSuatTTDBGiam, ThueSuatVATGiam = @ThueSuatVATGiam, ThueBVMT = @ThueBVMT, ThueSuatBVMT = @ThueSuatBVMT, ThueSuatBVMTGiam = @ThueSuatBVMTGiam, ThueChongPhaGia = @ThueChongPhaGia, ThueSuatChongPhaGia = @ThueSuatChongPhaGia, ThueSuatChongPhaGiaGiam = @ThueSuatChongPhaGiaGiam, isHangCu = @isHangCu, BieuThueXNK = @BieuThueXNK, BieuThueTTDB = @BieuThueTTDB, BieuThueBVMT = @BieuThueBVMT, BieuThueGTGT = @BieuThueGTGT, BieuThueCBPG = @BieuThueCBPG, ThongTinKhac = @ThongTinKhac, IsHangDongBo = @IsHangDongBo, CheDoUuDai = @CheDoUuDai WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_HangMauDich WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuTuHang", SqlDbType.Int, "SoThuTuHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuong", SqlDbType.Decimal, "TrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaKB", SqlDbType.Decimal, "DonGiaKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaKB", SqlDbType.Decimal, "TriGiaKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTT", SqlDbType.Decimal, "TriGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaKB_VND", SqlDbType.Decimal, "TriGiaKB_VND", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatXNK", SqlDbType.Decimal, "ThueSuatXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTTDB", SqlDbType.Decimal, "ThueSuatTTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatGTGT", SqlDbType.Decimal, "ThueSuatGTGT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueXNK", SqlDbType.Money, "ThueXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueTTDB", SqlDbType.Money, "ThueTTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueGTGT", SqlDbType.Money, "ThueGTGT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuThu", SqlDbType.Money, "PhuThu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeThuKhac", SqlDbType.Decimal, "TyLeThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaThuKhac", SqlDbType.Money, "TriGiaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MienThue", SqlDbType.TinyInt, "MienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma_HTS", SqlDbType.VarChar, "Ma_HTS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_HTS", SqlDbType.Char, "DVT_HTS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong_HTS", SqlDbType.Decimal, "SoLuong_HTS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatGiam", SqlDbType.VarChar, "ThueSuatGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FOC", SqlDbType.Bit, "FOC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTuyetDoi", SqlDbType.Float, "DonGiaTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHSMoRong", SqlDbType.NVarChar, "MaHSMoRong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhanHieu", SqlDbType.NVarChar, "NhanHieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, "QuyCachPhamChat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThanhPhan", SqlDbType.NVarChar, "ThanhPhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Model", SqlDbType.NVarChar, "Model", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangSX", SqlDbType.NVarChar, "MaHangSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangSX", SqlDbType.NVarChar, "TenHangSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueTuyetDoi", SqlDbType.Bit, "ThueTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatXNKGiam", SqlDbType.Float, "ThueSuatXNKGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTTDBGiam", SqlDbType.Float, "ThueSuatTTDBGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatVATGiam", SqlDbType.Float, "ThueSuatVATGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueBVMT", SqlDbType.Money, "ThueBVMT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatBVMT", SqlDbType.Float, "ThueSuatBVMT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatBVMTGiam", SqlDbType.Float, "ThueSuatBVMTGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueChongPhaGia", SqlDbType.Money, "ThueChongPhaGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatChongPhaGia", SqlDbType.Float, "ThueSuatChongPhaGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatChongPhaGiaGiam", SqlDbType.Float, "ThueSuatChongPhaGiaGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@isHangCu", SqlDbType.Bit, "isHangCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThueXNK", SqlDbType.NVarChar, "BieuThueXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThueTTDB", SqlDbType.NVarChar, "BieuThueTTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThueBVMT", SqlDbType.NVarChar, "BieuThueBVMT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThueGTGT", SqlDbType.NVarChar, "BieuThueGTGT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThueCBPG", SqlDbType.NVarChar, "BieuThueCBPG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThongTinKhac", SqlDbType.NVarChar, "ThongTinKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsHangDongBo", SqlDbType.Bit, "IsHangDongBo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CheDoUuDai", SqlDbType.NVarChar, "CheDoUuDai", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuTuHang", SqlDbType.Int, "SoThuTuHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuong", SqlDbType.Decimal, "TrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaKB", SqlDbType.Decimal, "DonGiaKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaKB", SqlDbType.Decimal, "TriGiaKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTT", SqlDbType.Decimal, "TriGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaKB_VND", SqlDbType.Decimal, "TriGiaKB_VND", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatXNK", SqlDbType.Decimal, "ThueSuatXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTTDB", SqlDbType.Decimal, "ThueSuatTTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatGTGT", SqlDbType.Decimal, "ThueSuatGTGT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueXNK", SqlDbType.Money, "ThueXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueTTDB", SqlDbType.Money, "ThueTTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueGTGT", SqlDbType.Money, "ThueGTGT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuThu", SqlDbType.Money, "PhuThu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeThuKhac", SqlDbType.Decimal, "TyLeThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaThuKhac", SqlDbType.Money, "TriGiaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MienThue", SqlDbType.TinyInt, "MienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma_HTS", SqlDbType.VarChar, "Ma_HTS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_HTS", SqlDbType.Char, "DVT_HTS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong_HTS", SqlDbType.Decimal, "SoLuong_HTS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatGiam", SqlDbType.VarChar, "ThueSuatGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FOC", SqlDbType.Bit, "FOC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTuyetDoi", SqlDbType.Float, "DonGiaTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHSMoRong", SqlDbType.NVarChar, "MaHSMoRong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhanHieu", SqlDbType.NVarChar, "NhanHieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, "QuyCachPhamChat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThanhPhan", SqlDbType.NVarChar, "ThanhPhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Model", SqlDbType.NVarChar, "Model", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangSX", SqlDbType.NVarChar, "MaHangSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangSX", SqlDbType.NVarChar, "TenHangSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueTuyetDoi", SqlDbType.Bit, "ThueTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatXNKGiam", SqlDbType.Float, "ThueSuatXNKGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTTDBGiam", SqlDbType.Float, "ThueSuatTTDBGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatVATGiam", SqlDbType.Float, "ThueSuatVATGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueBVMT", SqlDbType.Money, "ThueBVMT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatBVMT", SqlDbType.Float, "ThueSuatBVMT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatBVMTGiam", SqlDbType.Float, "ThueSuatBVMTGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueChongPhaGia", SqlDbType.Money, "ThueChongPhaGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatChongPhaGia", SqlDbType.Float, "ThueSuatChongPhaGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatChongPhaGiaGiam", SqlDbType.Float, "ThueSuatChongPhaGiaGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@isHangCu", SqlDbType.Bit, "isHangCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThueXNK", SqlDbType.NVarChar, "BieuThueXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThueTTDB", SqlDbType.NVarChar, "BieuThueTTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThueBVMT", SqlDbType.NVarChar, "BieuThueBVMT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThueGTGT", SqlDbType.NVarChar, "BieuThueGTGT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThueCBPG", SqlDbType.NVarChar, "BieuThueCBPG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThongTinKhac", SqlDbType.NVarChar, "ThongTinKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsHangDongBo", SqlDbType.Bit, "IsHangDongBo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CheDoUuDai", SqlDbType.NVarChar, "CheDoUuDai", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_HangMauDich VALUES(@TKMD_ID, @SoThuTuHang, @MaHS, @MaPhu, @TenHang, @NuocXX_ID, @DVT_ID, @SoLuong, @TrongLuong, @DonGiaKB, @DonGiaTT, @TriGiaKB, @TriGiaTT, @TriGiaKB_VND, @ThueSuatXNK, @ThueSuatTTDB, @ThueSuatGTGT, @ThueXNK, @ThueTTDB, @ThueGTGT, @PhuThu, @TyLeThuKhac, @TriGiaThuKhac, @MienThue, @Ma_HTS, @DVT_HTS, @SoLuong_HTS, @ThueSuatGiam, @FOC, @DonGiaTuyetDoi, @MaHSMoRong, @NhanHieu, @QuyCachPhamChat, @ThanhPhan, @Model, @MaHangSX, @TenHangSX, @ThueTuyetDoi, @ThueSuatXNKGiam, @ThueSuatTTDBGiam, @ThueSuatVATGiam, @ThueBVMT, @ThueSuatBVMT, @ThueSuatBVMTGiam, @ThueChongPhaGia, @ThueSuatChongPhaGia, @ThueSuatChongPhaGiaGiam, @isHangCu, @BieuThueXNK, @BieuThueTTDB, @BieuThueBVMT, @BieuThueGTGT, @BieuThueCBPG, @ThongTinKhac, @IsHangDongBo, @CheDoUuDai)";
            string update = "UPDATE t_KDT_HangMauDich SET TKMD_ID = @TKMD_ID, SoThuTuHang = @SoThuTuHang, MaHS = @MaHS, MaPhu = @MaPhu, TenHang = @TenHang, NuocXX_ID = @NuocXX_ID, DVT_ID = @DVT_ID, SoLuong = @SoLuong, TrongLuong = @TrongLuong, DonGiaKB = @DonGiaKB, DonGiaTT = @DonGiaTT, TriGiaKB = @TriGiaKB, TriGiaTT = @TriGiaTT, TriGiaKB_VND = @TriGiaKB_VND, ThueSuatXNK = @ThueSuatXNK, ThueSuatTTDB = @ThueSuatTTDB, ThueSuatGTGT = @ThueSuatGTGT, ThueXNK = @ThueXNK, ThueTTDB = @ThueTTDB, ThueGTGT = @ThueGTGT, PhuThu = @PhuThu, TyLeThuKhac = @TyLeThuKhac, TriGiaThuKhac = @TriGiaThuKhac, MienThue = @MienThue, Ma_HTS = @Ma_HTS, DVT_HTS = @DVT_HTS, SoLuong_HTS = @SoLuong_HTS, ThueSuatGiam = @ThueSuatGiam, FOC = @FOC, DonGiaTuyetDoi = @DonGiaTuyetDoi, MaHSMoRong = @MaHSMoRong, NhanHieu = @NhanHieu, QuyCachPhamChat = @QuyCachPhamChat, ThanhPhan = @ThanhPhan, Model = @Model, MaHangSX = @MaHangSX, TenHangSX = @TenHangSX, ThueTuyetDoi = @ThueTuyetDoi, ThueSuatXNKGiam = @ThueSuatXNKGiam, ThueSuatTTDBGiam = @ThueSuatTTDBGiam, ThueSuatVATGiam = @ThueSuatVATGiam, ThueBVMT = @ThueBVMT, ThueSuatBVMT = @ThueSuatBVMT, ThueSuatBVMTGiam = @ThueSuatBVMTGiam, ThueChongPhaGia = @ThueChongPhaGia, ThueSuatChongPhaGia = @ThueSuatChongPhaGia, ThueSuatChongPhaGiaGiam = @ThueSuatChongPhaGiaGiam, isHangCu = @isHangCu, BieuThueXNK = @BieuThueXNK, BieuThueTTDB = @BieuThueTTDB, BieuThueBVMT = @BieuThueBVMT, BieuThueGTGT = @BieuThueGTGT, BieuThueCBPG = @BieuThueCBPG, ThongTinKhac = @ThongTinKhac, IsHangDongBo = @IsHangDongBo, CheDoUuDai = @CheDoUuDai WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_HangMauDich WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuTuHang", SqlDbType.Int, "SoThuTuHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuong", SqlDbType.Decimal, "TrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaKB", SqlDbType.Decimal, "DonGiaKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaKB", SqlDbType.Decimal, "TriGiaKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTT", SqlDbType.Decimal, "TriGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaKB_VND", SqlDbType.Decimal, "TriGiaKB_VND", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatXNK", SqlDbType.Decimal, "ThueSuatXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTTDB", SqlDbType.Decimal, "ThueSuatTTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatGTGT", SqlDbType.Decimal, "ThueSuatGTGT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueXNK", SqlDbType.Money, "ThueXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueTTDB", SqlDbType.Money, "ThueTTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueGTGT", SqlDbType.Money, "ThueGTGT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuThu", SqlDbType.Money, "PhuThu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeThuKhac", SqlDbType.Decimal, "TyLeThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaThuKhac", SqlDbType.Money, "TriGiaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MienThue", SqlDbType.TinyInt, "MienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma_HTS", SqlDbType.VarChar, "Ma_HTS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_HTS", SqlDbType.Char, "DVT_HTS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong_HTS", SqlDbType.Decimal, "SoLuong_HTS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatGiam", SqlDbType.VarChar, "ThueSuatGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FOC", SqlDbType.Bit, "FOC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTuyetDoi", SqlDbType.Float, "DonGiaTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHSMoRong", SqlDbType.NVarChar, "MaHSMoRong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhanHieu", SqlDbType.NVarChar, "NhanHieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, "QuyCachPhamChat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThanhPhan", SqlDbType.NVarChar, "ThanhPhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Model", SqlDbType.NVarChar, "Model", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangSX", SqlDbType.NVarChar, "MaHangSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangSX", SqlDbType.NVarChar, "TenHangSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueTuyetDoi", SqlDbType.Bit, "ThueTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatXNKGiam", SqlDbType.Float, "ThueSuatXNKGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTTDBGiam", SqlDbType.Float, "ThueSuatTTDBGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatVATGiam", SqlDbType.Float, "ThueSuatVATGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueBVMT", SqlDbType.Money, "ThueBVMT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatBVMT", SqlDbType.Float, "ThueSuatBVMT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatBVMTGiam", SqlDbType.Float, "ThueSuatBVMTGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueChongPhaGia", SqlDbType.Money, "ThueChongPhaGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatChongPhaGia", SqlDbType.Float, "ThueSuatChongPhaGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatChongPhaGiaGiam", SqlDbType.Float, "ThueSuatChongPhaGiaGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@isHangCu", SqlDbType.Bit, "isHangCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThueXNK", SqlDbType.NVarChar, "BieuThueXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThueTTDB", SqlDbType.NVarChar, "BieuThueTTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThueBVMT", SqlDbType.NVarChar, "BieuThueBVMT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThueGTGT", SqlDbType.NVarChar, "BieuThueGTGT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThueCBPG", SqlDbType.NVarChar, "BieuThueCBPG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThongTinKhac", SqlDbType.NVarChar, "ThongTinKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsHangDongBo", SqlDbType.Bit, "IsHangDongBo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CheDoUuDai", SqlDbType.NVarChar, "CheDoUuDai", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuTuHang", SqlDbType.Int, "SoThuTuHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuong", SqlDbType.Decimal, "TrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaKB", SqlDbType.Decimal, "DonGiaKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaKB", SqlDbType.Decimal, "TriGiaKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTT", SqlDbType.Decimal, "TriGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaKB_VND", SqlDbType.Decimal, "TriGiaKB_VND", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatXNK", SqlDbType.Decimal, "ThueSuatXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTTDB", SqlDbType.Decimal, "ThueSuatTTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatGTGT", SqlDbType.Decimal, "ThueSuatGTGT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueXNK", SqlDbType.Money, "ThueXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueTTDB", SqlDbType.Money, "ThueTTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueGTGT", SqlDbType.Money, "ThueGTGT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuThu", SqlDbType.Money, "PhuThu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeThuKhac", SqlDbType.Decimal, "TyLeThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaThuKhac", SqlDbType.Money, "TriGiaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MienThue", SqlDbType.TinyInt, "MienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma_HTS", SqlDbType.VarChar, "Ma_HTS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_HTS", SqlDbType.Char, "DVT_HTS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong_HTS", SqlDbType.Decimal, "SoLuong_HTS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatGiam", SqlDbType.VarChar, "ThueSuatGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FOC", SqlDbType.Bit, "FOC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTuyetDoi", SqlDbType.Float, "DonGiaTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHSMoRong", SqlDbType.NVarChar, "MaHSMoRong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhanHieu", SqlDbType.NVarChar, "NhanHieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, "QuyCachPhamChat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThanhPhan", SqlDbType.NVarChar, "ThanhPhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Model", SqlDbType.NVarChar, "Model", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangSX", SqlDbType.NVarChar, "MaHangSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangSX", SqlDbType.NVarChar, "TenHangSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueTuyetDoi", SqlDbType.Bit, "ThueTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatXNKGiam", SqlDbType.Float, "ThueSuatXNKGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTTDBGiam", SqlDbType.Float, "ThueSuatTTDBGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatVATGiam", SqlDbType.Float, "ThueSuatVATGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueBVMT", SqlDbType.Money, "ThueBVMT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatBVMT", SqlDbType.Float, "ThueSuatBVMT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatBVMTGiam", SqlDbType.Float, "ThueSuatBVMTGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueChongPhaGia", SqlDbType.Money, "ThueChongPhaGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatChongPhaGia", SqlDbType.Float, "ThueSuatChongPhaGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatChongPhaGiaGiam", SqlDbType.Float, "ThueSuatChongPhaGiaGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@isHangCu", SqlDbType.Bit, "isHangCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThueXNK", SqlDbType.NVarChar, "BieuThueXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThueTTDB", SqlDbType.NVarChar, "BieuThueTTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThueBVMT", SqlDbType.NVarChar, "BieuThueBVMT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThueGTGT", SqlDbType.NVarChar, "BieuThueGTGT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThueCBPG", SqlDbType.NVarChar, "BieuThueCBPG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThongTinKhac", SqlDbType.NVarChar, "ThongTinKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsHangDongBo", SqlDbType.Bit, "IsHangDongBo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CheDoUuDai", SqlDbType.NVarChar, "CheDoUuDai", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HangMauDich Load(long id)
		{
			const string spName = "[dbo].[p_KDT_HangMauDich_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<HangMauDich> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<HangMauDich> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<HangMauDich> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}

        public static IList<HangMauDich> SelectDistinctHMDHSTLCollectionDynamic(long BangKeHoSoThanhLy_ID)
        {
            IDataReader reader = SelectReaderDistinctDynamicHMDHSTL(BangKeHoSoThanhLy_ID);
            return ConvertToSmallCollection(reader);
        }
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<HangMauDich> SelectCollectionBy_TKMD_ID(long tKMD_ID)
		{
            IDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_HangMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_HangMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
        public static IDataReader SelectReaderDistinctDynamicHMDHSTL(long BangKeHoSoThanhLy_ID)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_SelectDistinctDynamicHMDHSTL]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, BangKeHoSoThanhLy_ID);

            return db.ExecuteReader(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertHangMauDich(long tKMD_ID, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, decimal trongLuong, decimal donGiaKB, decimal donGiaTT, decimal triGiaKB, decimal triGiaTT, decimal triGiaKB_VND, decimal thueSuatXNK, decimal thueSuatTTDB, decimal thueSuatGTGT, decimal thueXNK, decimal thueTTDB, decimal thueGTGT, decimal phuThu, decimal tyLeThuKhac, decimal triGiaThuKhac, byte mienThue, string ma_HTS, string dVT_HTS, decimal soLuong_HTS, string thueSuatGiam, bool fOC, double donGiaTuyetDoi, string maHSMoRong, string nhanHieu, string quyCachPhamChat, string thanhPhan, string model, string maHangSX, string tenHangSX, bool thueTuyetDoi, double thueSuatXNKGiam, double thueSuatTTDBGiam, double thueSuatVATGiam, decimal thueBVMT, double thueSuatBVMT, double thueSuatBVMTGiam, decimal thueChongPhaGia, double thueSuatChongPhaGia, double thueSuatChongPhaGiaGiam, bool isHangCu, string bieuThueXNK, string bieuThueTTDB, string bieuThueBVMT, string bieuThueGTGT, string bieuThueCBPG, string thongTinKhac, bool isHangDongBo, string cheDoUuDai)
		{
			HangMauDich entity = new HangMauDich();	
			entity.TKMD_ID = tKMD_ID;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaHS = maHS;
			entity.MaPhu = maPhu;
			entity.TenHang = tenHang;
			entity.NuocXX_ID = nuocXX_ID;
			entity.DVT_ID = dVT_ID;
			entity.SoLuong = soLuong;
			entity.TrongLuong = trongLuong;
			entity.DonGiaKB = donGiaKB;
			entity.DonGiaTT = donGiaTT;
			entity.TriGiaKB = triGiaKB;
			entity.TriGiaTT = triGiaTT;
			entity.TriGiaKB_VND = triGiaKB_VND;
			entity.ThueSuatXNK = thueSuatXNK;
			entity.ThueSuatTTDB = thueSuatTTDB;
			entity.ThueSuatGTGT = thueSuatGTGT;
			entity.ThueXNK = thueXNK;
			entity.ThueTTDB = thueTTDB;
			entity.ThueGTGT = thueGTGT;
			entity.PhuThu = phuThu;
			entity.TyLeThuKhac = tyLeThuKhac;
			entity.TriGiaThuKhac = triGiaThuKhac;
			entity.MienThue = mienThue;
			entity.Ma_HTS = ma_HTS;
			entity.DVT_HTS = dVT_HTS;
			entity.SoLuong_HTS = soLuong_HTS;
			entity.ThueSuatGiam = thueSuatGiam;
			entity.FOC = fOC;
			entity.DonGiaTuyetDoi = donGiaTuyetDoi;
			entity.MaHSMoRong = maHSMoRong;
			entity.NhanHieu = nhanHieu;
			entity.QuyCachPhamChat = quyCachPhamChat;
			entity.ThanhPhan = thanhPhan;
			entity.Model = model;
			entity.MaHangSX = maHangSX;
			entity.TenHangSX = tenHangSX;
			entity.ThueTuyetDoi = thueTuyetDoi;
			entity.ThueSuatXNKGiam = thueSuatXNKGiam;
			entity.ThueSuatTTDBGiam = thueSuatTTDBGiam;
			entity.ThueSuatVATGiam = thueSuatVATGiam;
			entity.ThueBVMT = thueBVMT;
			entity.ThueSuatBVMT = thueSuatBVMT;
			entity.ThueSuatBVMTGiam = thueSuatBVMTGiam;
			entity.ThueChongPhaGia = thueChongPhaGia;
			entity.ThueSuatChongPhaGia = thueSuatChongPhaGia;
			entity.ThueSuatChongPhaGiaGiam = thueSuatChongPhaGiaGiam;
			entity.isHangCu = isHangCu;
			entity.BieuThueXNK = bieuThueXNK;
			entity.BieuThueTTDB = bieuThueTTDB;
			entity.BieuThueBVMT = bieuThueBVMT;
			entity.BieuThueGTGT = bieuThueGTGT;
			entity.BieuThueCBPG = bieuThueCBPG;
			entity.ThongTinKhac = thongTinKhac;
			entity.IsHangDongBo = isHangDongBo;
			entity.CheDoUuDai = cheDoUuDai;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_HangMauDich_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, TriGiaKB);
			db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
			db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
			db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
			db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
			db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
			db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
			db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
			db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
			db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
			db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
			db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
			db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
			db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
			db.AddInParameter(dbCommand, "@ThueSuatGiam", SqlDbType.VarChar, ThueSuatGiam);
			db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
			db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);
			db.AddInParameter(dbCommand, "@MaHSMoRong", SqlDbType.NVarChar, MaHSMoRong);
			db.AddInParameter(dbCommand, "@NhanHieu", SqlDbType.NVarChar, NhanHieu);
			db.AddInParameter(dbCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, QuyCachPhamChat);
			db.AddInParameter(dbCommand, "@ThanhPhan", SqlDbType.NVarChar, ThanhPhan);
			db.AddInParameter(dbCommand, "@Model", SqlDbType.NVarChar, Model);
			db.AddInParameter(dbCommand, "@MaHangSX", SqlDbType.NVarChar, MaHangSX);
			db.AddInParameter(dbCommand, "@TenHangSX", SqlDbType.NVarChar, TenHangSX);
			db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
			db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.Float, ThueSuatXNKGiam);
			db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.Float, ThueSuatTTDBGiam);
			db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.Float, ThueSuatVATGiam);
			db.AddInParameter(dbCommand, "@ThueBVMT", SqlDbType.Money, ThueBVMT);
			db.AddInParameter(dbCommand, "@ThueSuatBVMT", SqlDbType.Float, ThueSuatBVMT);
			db.AddInParameter(dbCommand, "@ThueSuatBVMTGiam", SqlDbType.Float, ThueSuatBVMTGiam);
			db.AddInParameter(dbCommand, "@ThueChongPhaGia", SqlDbType.Money, ThueChongPhaGia);
			db.AddInParameter(dbCommand, "@ThueSuatChongPhaGia", SqlDbType.Float, ThueSuatChongPhaGia);
			db.AddInParameter(dbCommand, "@ThueSuatChongPhaGiaGiam", SqlDbType.Float, ThueSuatChongPhaGiaGiam);
			db.AddInParameter(dbCommand, "@isHangCu", SqlDbType.Bit, isHangCu);
			db.AddInParameter(dbCommand, "@BieuThueXNK", SqlDbType.NVarChar, BieuThueXNK);
			db.AddInParameter(dbCommand, "@BieuThueTTDB", SqlDbType.NVarChar, BieuThueTTDB);
			db.AddInParameter(dbCommand, "@BieuThueBVMT", SqlDbType.NVarChar, BieuThueBVMT);
			db.AddInParameter(dbCommand, "@BieuThueGTGT", SqlDbType.NVarChar, BieuThueGTGT);
			db.AddInParameter(dbCommand, "@BieuThueCBPG", SqlDbType.NVarChar, BieuThueCBPG);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			db.AddInParameter(dbCommand, "@IsHangDongBo", SqlDbType.Bit, IsHangDongBo);
			db.AddInParameter(dbCommand, "@CheDoUuDai", SqlDbType.NVarChar, CheDoUuDai);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HangMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangMauDich item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHangMauDich(long id, long tKMD_ID, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, decimal trongLuong, decimal donGiaKB, decimal donGiaTT, decimal triGiaKB, decimal triGiaTT, decimal triGiaKB_VND, decimal thueSuatXNK, decimal thueSuatTTDB, decimal thueSuatGTGT, decimal thueXNK, decimal thueTTDB, decimal thueGTGT, decimal phuThu, decimal tyLeThuKhac, decimal triGiaThuKhac, byte mienThue, string ma_HTS, string dVT_HTS, decimal soLuong_HTS, string thueSuatGiam, bool fOC, double donGiaTuyetDoi, string maHSMoRong, string nhanHieu, string quyCachPhamChat, string thanhPhan, string model, string maHangSX, string tenHangSX, bool thueTuyetDoi, double thueSuatXNKGiam, double thueSuatTTDBGiam, double thueSuatVATGiam, decimal thueBVMT, double thueSuatBVMT, double thueSuatBVMTGiam, decimal thueChongPhaGia, double thueSuatChongPhaGia, double thueSuatChongPhaGiaGiam, bool isHangCu, string bieuThueXNK, string bieuThueTTDB, string bieuThueBVMT, string bieuThueGTGT, string bieuThueCBPG, string thongTinKhac, bool isHangDongBo, string cheDoUuDai)
		{
			HangMauDich entity = new HangMauDich();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaHS = maHS;
			entity.MaPhu = maPhu;
			entity.TenHang = tenHang;
			entity.NuocXX_ID = nuocXX_ID;
			entity.DVT_ID = dVT_ID;
			entity.SoLuong = soLuong;
			entity.TrongLuong = trongLuong;
			entity.DonGiaKB = donGiaKB;
			entity.DonGiaTT = donGiaTT;
			entity.TriGiaKB = triGiaKB;
			entity.TriGiaTT = triGiaTT;
			entity.TriGiaKB_VND = triGiaKB_VND;
			entity.ThueSuatXNK = thueSuatXNK;
			entity.ThueSuatTTDB = thueSuatTTDB;
			entity.ThueSuatGTGT = thueSuatGTGT;
			entity.ThueXNK = thueXNK;
			entity.ThueTTDB = thueTTDB;
			entity.ThueGTGT = thueGTGT;
			entity.PhuThu = phuThu;
			entity.TyLeThuKhac = tyLeThuKhac;
			entity.TriGiaThuKhac = triGiaThuKhac;
			entity.MienThue = mienThue;
			entity.Ma_HTS = ma_HTS;
			entity.DVT_HTS = dVT_HTS;
			entity.SoLuong_HTS = soLuong_HTS;
			entity.ThueSuatGiam = thueSuatGiam;
			entity.FOC = fOC;
			entity.DonGiaTuyetDoi = donGiaTuyetDoi;
			entity.MaHSMoRong = maHSMoRong;
			entity.NhanHieu = nhanHieu;
			entity.QuyCachPhamChat = quyCachPhamChat;
			entity.ThanhPhan = thanhPhan;
			entity.Model = model;
			entity.MaHangSX = maHangSX;
			entity.TenHangSX = tenHangSX;
			entity.ThueTuyetDoi = thueTuyetDoi;
			entity.ThueSuatXNKGiam = thueSuatXNKGiam;
			entity.ThueSuatTTDBGiam = thueSuatTTDBGiam;
			entity.ThueSuatVATGiam = thueSuatVATGiam;
			entity.ThueBVMT = thueBVMT;
			entity.ThueSuatBVMT = thueSuatBVMT;
			entity.ThueSuatBVMTGiam = thueSuatBVMTGiam;
			entity.ThueChongPhaGia = thueChongPhaGia;
			entity.ThueSuatChongPhaGia = thueSuatChongPhaGia;
			entity.ThueSuatChongPhaGiaGiam = thueSuatChongPhaGiaGiam;
			entity.isHangCu = isHangCu;
			entity.BieuThueXNK = bieuThueXNK;
			entity.BieuThueTTDB = bieuThueTTDB;
			entity.BieuThueBVMT = bieuThueBVMT;
			entity.BieuThueGTGT = bieuThueGTGT;
			entity.BieuThueCBPG = bieuThueCBPG;
			entity.ThongTinKhac = thongTinKhac;
			entity.IsHangDongBo = isHangDongBo;
			entity.CheDoUuDai = cheDoUuDai;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_HangMauDich_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, TriGiaKB);
			db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
			db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
			db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
			db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
			db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
			db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
			db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
			db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
			db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
			db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
			db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
			db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
			db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
			db.AddInParameter(dbCommand, "@ThueSuatGiam", SqlDbType.VarChar, ThueSuatGiam);
			db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
			db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);
			db.AddInParameter(dbCommand, "@MaHSMoRong", SqlDbType.NVarChar, MaHSMoRong);
			db.AddInParameter(dbCommand, "@NhanHieu", SqlDbType.NVarChar, NhanHieu);
			db.AddInParameter(dbCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, QuyCachPhamChat);
			db.AddInParameter(dbCommand, "@ThanhPhan", SqlDbType.NVarChar, ThanhPhan);
			db.AddInParameter(dbCommand, "@Model", SqlDbType.NVarChar, Model);
			db.AddInParameter(dbCommand, "@MaHangSX", SqlDbType.NVarChar, MaHangSX);
			db.AddInParameter(dbCommand, "@TenHangSX", SqlDbType.NVarChar, TenHangSX);
			db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
			db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.Float, ThueSuatXNKGiam);
			db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.Float, ThueSuatTTDBGiam);
			db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.Float, ThueSuatVATGiam);
			db.AddInParameter(dbCommand, "@ThueBVMT", SqlDbType.Money, ThueBVMT);
			db.AddInParameter(dbCommand, "@ThueSuatBVMT", SqlDbType.Float, ThueSuatBVMT);
			db.AddInParameter(dbCommand, "@ThueSuatBVMTGiam", SqlDbType.Float, ThueSuatBVMTGiam);
			db.AddInParameter(dbCommand, "@ThueChongPhaGia", SqlDbType.Money, ThueChongPhaGia);
			db.AddInParameter(dbCommand, "@ThueSuatChongPhaGia", SqlDbType.Float, ThueSuatChongPhaGia);
			db.AddInParameter(dbCommand, "@ThueSuatChongPhaGiaGiam", SqlDbType.Float, ThueSuatChongPhaGiaGiam);
			db.AddInParameter(dbCommand, "@isHangCu", SqlDbType.Bit, isHangCu);
			db.AddInParameter(dbCommand, "@BieuThueXNK", SqlDbType.NVarChar, BieuThueXNK);
			db.AddInParameter(dbCommand, "@BieuThueTTDB", SqlDbType.NVarChar, BieuThueTTDB);
			db.AddInParameter(dbCommand, "@BieuThueBVMT", SqlDbType.NVarChar, BieuThueBVMT);
			db.AddInParameter(dbCommand, "@BieuThueGTGT", SqlDbType.NVarChar, BieuThueGTGT);
			db.AddInParameter(dbCommand, "@BieuThueCBPG", SqlDbType.NVarChar, BieuThueCBPG);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			db.AddInParameter(dbCommand, "@IsHangDongBo", SqlDbType.Bit, IsHangDongBo);
			db.AddInParameter(dbCommand, "@CheDoUuDai", SqlDbType.NVarChar, CheDoUuDai);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HangMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangMauDich item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHangMauDich(long id, long tKMD_ID, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, decimal trongLuong, decimal donGiaKB, decimal donGiaTT, decimal triGiaKB, decimal triGiaTT, decimal triGiaKB_VND, decimal thueSuatXNK, decimal thueSuatTTDB, decimal thueSuatGTGT, decimal thueXNK, decimal thueTTDB, decimal thueGTGT, decimal phuThu, decimal tyLeThuKhac, decimal triGiaThuKhac, byte mienThue, string ma_HTS, string dVT_HTS, decimal soLuong_HTS, string thueSuatGiam, bool fOC, double donGiaTuyetDoi, string maHSMoRong, string nhanHieu, string quyCachPhamChat, string thanhPhan, string model, string maHangSX, string tenHangSX, bool thueTuyetDoi, double thueSuatXNKGiam, double thueSuatTTDBGiam, double thueSuatVATGiam, decimal thueBVMT, double thueSuatBVMT, double thueSuatBVMTGiam, decimal thueChongPhaGia, double thueSuatChongPhaGia, double thueSuatChongPhaGiaGiam, bool isHangCu, string bieuThueXNK, string bieuThueTTDB, string bieuThueBVMT, string bieuThueGTGT, string bieuThueCBPG, string thongTinKhac, bool isHangDongBo, string cheDoUuDai)
		{
			HangMauDich entity = new HangMauDich();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaHS = maHS;
			entity.MaPhu = maPhu;
			entity.TenHang = tenHang;
			entity.NuocXX_ID = nuocXX_ID;
			entity.DVT_ID = dVT_ID;
			entity.SoLuong = soLuong;
			entity.TrongLuong = trongLuong;
			entity.DonGiaKB = donGiaKB;
			entity.DonGiaTT = donGiaTT;
			entity.TriGiaKB = triGiaKB;
			entity.TriGiaTT = triGiaTT;
			entity.TriGiaKB_VND = triGiaKB_VND;
			entity.ThueSuatXNK = thueSuatXNK;
			entity.ThueSuatTTDB = thueSuatTTDB;
			entity.ThueSuatGTGT = thueSuatGTGT;
			entity.ThueXNK = thueXNK;
			entity.ThueTTDB = thueTTDB;
			entity.ThueGTGT = thueGTGT;
			entity.PhuThu = phuThu;
			entity.TyLeThuKhac = tyLeThuKhac;
			entity.TriGiaThuKhac = triGiaThuKhac;
			entity.MienThue = mienThue;
			entity.Ma_HTS = ma_HTS;
			entity.DVT_HTS = dVT_HTS;
			entity.SoLuong_HTS = soLuong_HTS;
			entity.ThueSuatGiam = thueSuatGiam;
			entity.FOC = fOC;
			entity.DonGiaTuyetDoi = donGiaTuyetDoi;
			entity.MaHSMoRong = maHSMoRong;
			entity.NhanHieu = nhanHieu;
			entity.QuyCachPhamChat = quyCachPhamChat;
			entity.ThanhPhan = thanhPhan;
			entity.Model = model;
			entity.MaHangSX = maHangSX;
			entity.TenHangSX = tenHangSX;
			entity.ThueTuyetDoi = thueTuyetDoi;
			entity.ThueSuatXNKGiam = thueSuatXNKGiam;
			entity.ThueSuatTTDBGiam = thueSuatTTDBGiam;
			entity.ThueSuatVATGiam = thueSuatVATGiam;
			entity.ThueBVMT = thueBVMT;
			entity.ThueSuatBVMT = thueSuatBVMT;
			entity.ThueSuatBVMTGiam = thueSuatBVMTGiam;
			entity.ThueChongPhaGia = thueChongPhaGia;
			entity.ThueSuatChongPhaGia = thueSuatChongPhaGia;
			entity.ThueSuatChongPhaGiaGiam = thueSuatChongPhaGiaGiam;
			entity.isHangCu = isHangCu;
			entity.BieuThueXNK = bieuThueXNK;
			entity.BieuThueTTDB = bieuThueTTDB;
			entity.BieuThueBVMT = bieuThueBVMT;
			entity.BieuThueGTGT = bieuThueGTGT;
			entity.BieuThueCBPG = bieuThueCBPG;
			entity.ThongTinKhac = thongTinKhac;
			entity.IsHangDongBo = isHangDongBo;
			entity.CheDoUuDai = cheDoUuDai;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_HangMauDich_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, TriGiaKB);
			db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
			db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
			db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
			db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
			db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
			db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
			db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
			db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
			db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
			db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
			db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
			db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
			db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
			db.AddInParameter(dbCommand, "@ThueSuatGiam", SqlDbType.VarChar, ThueSuatGiam);
			db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
			db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);
			db.AddInParameter(dbCommand, "@MaHSMoRong", SqlDbType.NVarChar, MaHSMoRong);
			db.AddInParameter(dbCommand, "@NhanHieu", SqlDbType.NVarChar, NhanHieu);
			db.AddInParameter(dbCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, QuyCachPhamChat);
			db.AddInParameter(dbCommand, "@ThanhPhan", SqlDbType.NVarChar, ThanhPhan);
			db.AddInParameter(dbCommand, "@Model", SqlDbType.NVarChar, Model);
			db.AddInParameter(dbCommand, "@MaHangSX", SqlDbType.NVarChar, MaHangSX);
			db.AddInParameter(dbCommand, "@TenHangSX", SqlDbType.NVarChar, TenHangSX);
			db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
			db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.Float, ThueSuatXNKGiam);
			db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.Float, ThueSuatTTDBGiam);
			db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.Float, ThueSuatVATGiam);
			db.AddInParameter(dbCommand, "@ThueBVMT", SqlDbType.Money, ThueBVMT);
			db.AddInParameter(dbCommand, "@ThueSuatBVMT", SqlDbType.Float, ThueSuatBVMT);
			db.AddInParameter(dbCommand, "@ThueSuatBVMTGiam", SqlDbType.Float, ThueSuatBVMTGiam);
			db.AddInParameter(dbCommand, "@ThueChongPhaGia", SqlDbType.Money, ThueChongPhaGia);
			db.AddInParameter(dbCommand, "@ThueSuatChongPhaGia", SqlDbType.Float, ThueSuatChongPhaGia);
			db.AddInParameter(dbCommand, "@ThueSuatChongPhaGiaGiam", SqlDbType.Float, ThueSuatChongPhaGiaGiam);
			db.AddInParameter(dbCommand, "@isHangCu", SqlDbType.Bit, isHangCu);
			db.AddInParameter(dbCommand, "@BieuThueXNK", SqlDbType.NVarChar, BieuThueXNK);
			db.AddInParameter(dbCommand, "@BieuThueTTDB", SqlDbType.NVarChar, BieuThueTTDB);
			db.AddInParameter(dbCommand, "@BieuThueBVMT", SqlDbType.NVarChar, BieuThueBVMT);
			db.AddInParameter(dbCommand, "@BieuThueGTGT", SqlDbType.NVarChar, BieuThueGTGT);
			db.AddInParameter(dbCommand, "@BieuThueCBPG", SqlDbType.NVarChar, BieuThueCBPG);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			db.AddInParameter(dbCommand, "@IsHangDongBo", SqlDbType.Bit, IsHangDongBo);
			db.AddInParameter(dbCommand, "@CheDoUuDai", SqlDbType.NVarChar, CheDoUuDai);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HangMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangMauDich item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHangMauDich(long id)
		{
			HangMauDich entity = new HangMauDich();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_HangMauDich_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_HangMauDich_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HangMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangMauDich item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}