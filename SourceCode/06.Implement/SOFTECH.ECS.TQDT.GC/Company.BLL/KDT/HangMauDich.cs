using System.Data.SqlClient;
using System.Data;
using System;
using Company.GC.BLL.GC;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT
{
    public partial class HangMauDich
    {
        public string MaHSMoRong { set; get; }
        public bool FOC { set; get; }
        public string NhanHieu { set; get; }
        public string QuyCachPhamChat { set; get; }
        public string ThanhPhan { set; get; }
        public string Model { set; get; }
        public string TenHangSX { set; get; }
        public string MaHangSX { set; get; }
        public bool ThueTuyetDoi { set; get; }
        public double DonGiaTuyetDoi { set; get; }

        public bool Load(SqlTransaction trans)
        {
            string spName = "p_KDT_HangMauDich_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand, trans);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) this._TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) this._SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) this._MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this._TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this._NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this._SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) this._DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) this._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) this._TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) this._TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) this._TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) this._ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) this._ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) this._ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this._ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) this._ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) this._ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) this._PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) this._TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) this._TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) this._MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) this._Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) this._DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) this._SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public HangMauDich Copy()
        {
            HangMauDich entity = new HangMauDich();

            entity.ID = this.ID;
            entity.TKMD_ID = this.TKMD_ID;
            entity.SoThuTuHang = this.SoThuTuHang;
            entity.MaHS = this.MaHS;
            entity.MaPhu = this.MaPhu;
            entity.TenHang = this.TenHang;
            entity.NuocXX_ID = this.NuocXX_ID;
            entity.DVT_ID = this.DVT_ID;
            entity.SoLuong = this.SoLuong;
            entity.TrongLuong = this.TrongLuong;
            entity.DonGiaKB = this.DonGiaKB;
            entity.DonGiaTT = this.DonGiaTT;
            entity.TriGiaKB = this.TriGiaKB;
            entity.TriGiaTT = this.TriGiaTT;
            entity.TriGiaKB_VND = this.TriGiaKB_VND;
            entity.ThueSuatXNK = this.ThueSuatXNK;
            entity.ThueSuatTTDB = this.ThueSuatTTDB;
            entity.ThueSuatGTGT = this.ThueSuatGTGT;
            entity.ThueXNK = this.ThueXNK;
            entity.ThueTTDB = this.ThueTTDB;
            entity.ThueGTGT = this.ThueGTGT;
            entity.PhuThu = this.PhuThu;
            entity.TyLeThuKhac = this.TyLeThuKhac;
            entity.TriGiaThuKhac = this.TriGiaThuKhac;
            entity.MienThue = this.MienThue;
            entity.Ma_HTS = this.Ma_HTS;
            entity.DVT_HTS = this.DVT_HTS;
            entity.SoLuong_HTS = this.SoLuong_HTS;
            return entity;
        }

        public HangMauDichCollection SelectCollectionBy_TKMD_ID(SqlTransaction trans)
        {
            string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);

            HangMauDichCollection collection = new HangMauDichCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand, trans);
            while (reader.Read())
            {
                HangMauDich entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity._Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity._DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity._SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));

                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public void Delete(string LoaiHangHoa, string MaLoaiHinh, long IDHopDong)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    this.Delete(LoaiHangHoa, MaLoaiHinh.Trim(), IDHopDong, transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public void Delete(string LoaiHangHoa, string MaLoaiHinh, long IDHopDong, SqlTransaction transaction)
        {
            if (MaLoaiHinh.StartsWith("NGC"))
            {
                if (LoaiHangHoa == "N")
                {
                    #region Nhap NPL
                    NguyenPhuLieu npl = new NguyenPhuLieu();
                    npl.HopDong_ID = IDHopDong;
                    npl.Ma = this.MaPhu;
                    npl.Load(transaction);
                    npl.SoLuongDaNhap -= this.SoLuong;
                    npl.UpdateTransaction(transaction);
                    #endregion Nhap NPL
                }
                else
                {
                    #region Nhap Thiet BI
                    ThietBi tb = new ThietBi();
                    tb.HopDong_ID = IDHopDong;
                    tb.Ma = this.MaPhu;
                    tb.Load(transaction);
                    tb.SoLuongDaNhap -= this.SoLuong;
                    tb.UpdateTransaction(transaction);
                    #endregion Nhap Thiet BI
                }
            }
            else
            {
                if (LoaiHangHoa == "N")
                {
                    #region Xuat NPL
                    NguyenPhuLieu npl = new NguyenPhuLieu();
                    npl.HopDong_ID = IDHopDong;
                    npl.Ma = this.MaPhu;
                    npl.Load(transaction);
                    npl.SoLuongDaDung -= this.SoLuong;
                    npl.UpdateTransaction(transaction);
                    #endregion Xuat NPL
                }
                else if (LoaiHangHoa == "T")
                {
                    #region Xuat TB
                    ThietBi tb = new ThietBi();
                    tb.HopDong_ID = IDHopDong;
                    tb.Ma = this.MaPhu;
                    tb.Load(transaction);
                    tb.SoLuongDaNhap += this.SoLuong;
                    tb.UpdateTransaction(transaction);
                    #endregion Xuat Thiet bi
                }
                else
                {
                    SanPham sp = new SanPham();
                    sp.HopDong_ID = IDHopDong;
                    sp.Ma = this.MaPhu;
                    sp.Load(transaction);
                    sp.SoLuongDaXuat -= this.SoLuong;
                    sp.UpdateTransaction(transaction);
                    DataSet dsLuongNPL = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(sp.Ma, this.SoLuong, IDHopDong, transaction);
                    foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                    {
                        NguyenPhuLieu npl = new NguyenPhuLieu();
                        npl.HopDong_ID = IDHopDong;
                        npl.Ma = row["MaNguyenPhuLieu"].ToString();
                        npl.Load(transaction);
                        npl.SoLuongDaDung -= Convert.ToDecimal(row["LuongCanDung"]);
                        npl.UpdateTransaction(transaction);
                    }
                }

            }
            this.DeleteTransaction(transaction);
        }

        // Get so hang TK :
        public DataSet GetHangTK(long IDHopDong)
        {
            //string sql = " SELECT * " +
            //          " from   t_KDT_HangMauDich " +
            //          " where  TKMD_ID = @TKMD_ID ";

            string sql = " SELECT T.SoToKhai, T.MaLoaiHinh,T.ID, T.IDHopDong, H.TenHang, H.MaPhu, H.SoLuong,H.DVT_ID "
          + " FROM t_KDT_HangMauDich H  "
          + " inner join t_KDT_ToKhaiMauDich T "
          + " on  H.TKMD_ID = T.ID and T.IDHopDong = @IDHopDong";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        // Get Quantity :
        public DataSet GetSoluongHangTK(long SoTK, string maNPL, string maLoaiHinh, long IDHopDong)
        {
            string sql = " SELECT distinct T.SoToKhai, T.MaLoaiHinh,T.ID, T.IDHopDong, H.TenHang, H.MaPhu, H.SoLuong "
            + " FROM t_KDT_HangMauDich H  "
            + " inner join t_KDT_ToKhaiMauDich T "
            + " on  H.TKMD_ID = T.ID Where T.SoToKhai = @SoToKhai "
            + " And H.MaPhu =@MaPhu "
            + " And T.MaLoaiHinh=@MaLoaiHinh "
            + " And T.IDHopDong=@IDHopDong "
            + " and T.LoaiHangHoa='N'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoTK);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maNPL);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public decimal GetSoluongHangTKMD(long ID, string maPhu)
        {
            string sql = " SELECT SoLuong "
                         + " from   t_KDT_HangMauDich  "
                         + " where  TKMD_ID =@ID AND MaPhu =@MaPhu ";
            decimal temp = 0;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.Char, maPhu);
            try
            {
                temp = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            { }
            return temp;

        }
        public bool CheckMaHangToKhai()
        {
            string sql = " SELECT count(MaPhu) "
                         + " FROM   t_KDT_HangMauDich  "
                         + " WHERE  TKMD_ID =@ID AND MaPhu =@MaPhu ";
            decimal temp = 0;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this.TKMD_ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.Char, this.MaPhu);
            try
            {
                temp = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            { }
            return temp > 0;

        }
        public DataSet GetSoluongHangTKS(long SoTK, string maNPL, string maLoaiHinh, long IDHopDong)
        {
            string sql = " SELECT distinct T.SoToKhai, T.MaLoaiHinh,T.ID, T.IDHopDong, H.TenHang, H.MaPhu, H.SoLuong "
            + " FROM t_KDT_HangMauDich H  "
            + " inner join t_KDT_ToKhaiMauDich T "
            + " on  H.TKMD_ID = T.ID "
            + " Where T.SoToKhai = @SoToKhai "
            + " And H.MaPhu =@MaPhu "
            + " And T.MaLoaiHinh=@MaLoaiHinh "
            + " And T.IDHopDong=@IDHopDong "
            + " And T.LoaiHangHoa='S'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoTK);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maNPL);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetSoluongHangTKT(long SoTK, string maNPL, string maLoaiHinh, long IDHopDong)
        {
            string sql = " SELECT distinct T.SoToKhai, T.MaLoaiHinh,T.ID, T.IDHopDong, H.TenHang, H.MaPhu, H.SoLuong "
            + " FROM t_KDT_HangMauDich H  "
            + " inner join t_KDT_ToKhaiMauDich T "
            + " on  H.TKMD_ID = T.ID Where T.SoToKhai = @SoToKhai "
            + " And H.MaPhu =@MaPhu "
            + " And T.MaLoaiHinh=@MaLoaiHinh "
            + " And T.IDHopDong=@IDHopDong "
            + " and T.LoaiHangHoa='T'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoTK);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maNPL);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public decimal GetDonGiaHangTKMD(long ID, string maPhu)
        {
            string sql = " SELECT DonGiaKB "
                         + " from   t_KDT_HangMauDich  "
                         + " where  TKMD_ID =@ID AND MaPhu =@MaPhu ";
            decimal temp = 0;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.Char, maPhu);
            try
            {
                temp = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            { }
            return temp;

        }

        public long InsertTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_HangMauDich_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, this._SoThuTuHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
            this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Money, this._DonGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Money, this._TriGiaKB);
            this.db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Money, this._TriGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Money, this._TriGiaKB_VND);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
            this.db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
            this.db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
            this.db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
            this.db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);
            this.db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, this._Ma_HTS);
            this.db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, this._DVT_HTS);
            this.db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, this._SoLuong_HTS);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, this._ThueSuatXNKGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, this._ThueSuatTTDBGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, this._ThueSuatVATGiam);
            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_HangMauDich_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, this._SoThuTuHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
            this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Money, this._DonGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Money, this._TriGiaKB);
            this.db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Money, this._TriGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Money, this._TriGiaKB_VND);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
            this.db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
            this.db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
            this.db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
            this.db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);
            this.db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, this._Ma_HTS);
            this.db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, this._DVT_HTS);
            this.db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, this._SoLuong_HTS);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, this._ThueSuatXNKGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, this._ThueSuatTTDBGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, this._ThueSuatVATGiam);
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransaction_V3(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_HangMauDich_InsertUpdate_ByV3";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, this._SoThuTuHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
            this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Money, this._DonGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Money, this._TriGiaKB);
            this.db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Money, this._TriGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Money, this._TriGiaKB_VND);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
            this.db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
            this.db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
            this.db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
            this.db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);
            this.db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, this._Ma_HTS);
            this.db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, this._DVT_HTS);
            this.db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, this._SoLuong_HTS);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, this._ThueSuatXNKGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, this._ThueSuatTTDBGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, this._ThueSuatVATGiam);
            db.AddInParameter(dbCommand, "@MaHSMoRong", SqlDbType.NVarChar, MaHSMoRong);
            db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
            db.AddInParameter(dbCommand, "@NhanHieu", SqlDbType.NVarChar, NhanHieu);
            db.AddInParameter(dbCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, QuyCachPhamChat);
            db.AddInParameter(dbCommand, "@ThanhPhan", SqlDbType.NVarChar, ThanhPhan);
            db.AddInParameter(dbCommand, "@Model", SqlDbType.NVarChar, Model);
            db.AddInParameter(dbCommand, "@TenHangSX", SqlDbType.NVarChar, TenHangSX);
            db.AddInParameter(dbCommand, "@MaHangSX", SqlDbType.NVarChar, MaHangSX);
            db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
            db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_HangMauDich_InsertUpdateByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, this._SoThuTuHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
            this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Money, this._DonGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Money, this._TriGiaKB);
            this.db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Money, this._TriGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Money, this._TriGiaKB_VND);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
            this.db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
            this.db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
            this.db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
            this.db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);
            this.db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, this._Ma_HTS);
            this.db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, this._DVT_HTS);
            this.db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, this._SoLuong_HTS);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, this._ThueSuatXNKGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, this._ThueSuatTTDBGiam);
            this.db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, this._ThueSuatVATGiam);
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public HangMauDichCollection SelectCollectionBy_TKMD_ID(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);

            HangMauDichCollection collection = new HangMauDichCollection();
            SqlDataReader reader = null;
            if (transaction != null)
                reader = (SqlDataReader)this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = (SqlDataReader)this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {

                HangMauDich entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity._Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity._DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity._SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity._TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity._TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity._TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                }
                catch { }
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public HangMauDichCollection SelectCollectionBy_TKMD_ID(string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);

            HangMauDichCollection collection = new HangMauDichCollection();
            SqlDataReader reader = (SqlDataReader)this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {

                HangMauDich entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity._Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity._DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity._SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity._TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity._TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity._TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                }
                catch { }
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

    }
}