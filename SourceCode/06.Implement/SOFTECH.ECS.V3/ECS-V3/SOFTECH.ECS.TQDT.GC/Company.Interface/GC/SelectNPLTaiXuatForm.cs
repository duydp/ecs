using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;

namespace Company.Interface.GC
{
    public partial class SelectNPLTaiXuatForm : BaseForm
    {
        public PhanBoToKhaiXuat pbToKhaiXuat = new PhanBoToKhaiXuat();
        public IList<PhanBoToKhaiXuat> pbToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
        public ToKhaiMauDich TKMD;

        private int SoToKhai;
        private string MaLoaiHinh;
        private DateTime NgayDangKy;
        private string TenNPL;
        private string DVT_ID;
        private decimal SoLuong;
        public bool isPhanBo = false;
        public SelectNPLTaiXuatForm()
        {
            InitializeComponent();
        }

        private void dgList1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
                this.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                this.TenNPL = e.Row.Cells["TenHang"].Text;
                this.SoLuong = Convert.ToDecimal(e.Row.Cells["Ton"].Value);
                this.MaHaiQuan = TKMD.MaHaiQuan;
                txtSoToKhai.Text = e.Row.Cells["SoToKhai"].Text;
                txtMaNPL.Text = e.Row.Cells["MaNPL"].Text;
                txtLuong.Text = this.SoLuong.ToString();
                btnAdd.Enabled = true;
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (cvError.IsValid)
            {
                if (checkExit(this.SoToKhai, this.MaLoaiHinh, Convert.ToInt16(this.NgayDangKy.Year), this.MaHaiQuan, txtMaNPL.Text))
                {
                    //ShowMessage("Nguyên phụ liệu của tờ khai này đã có, bạn hãy chọn nguyên phụ liệu khác.", false);
                    showMsg("MSG_240207");
                    txtSoToKhai.Text = "";
                    txtLuong.Value = 0;
                    txtMaNPL.Text = "";
                    return;
                }
                if (this.SoLuong < Convert.ToDecimal(txtLuong.Value))
                {
                    showMsg("MSG_WRN23");
                    //ShowMessage("Lượng tái xuất phải nhỏ hơn hoặc bằng lượng nguyên phụ liệu tồn của tờ khai.", false);
                    txtLuong.Value = 0;
                    txtLuong.Focus();
                    return;
                }
                PhanBoToKhaiNhap pbTKNhap = new PhanBoToKhaiNhap();
                pbTKNhap.LuongTonDau = Convert.ToDouble(this.SoLuong);
                pbTKNhap.LuongPhanBo = Convert.ToDouble(txtLuong.Text);
                pbTKNhap.LuongTonCuoi = pbTKNhap.LuongTonDau - pbTKNhap.LuongPhanBo;
                pbTKNhap.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                pbTKNhap.MaHaiQuanNhap = TKMD.MaHaiQuan;
                pbTKNhap.MaNPL = txtMaNPL.Text.Trim();
                pbTKNhap.NamDangKyNhap =(short) this.NgayDangKy.Year;
                pbTKNhap.SoToKhaiNhap = this.SoToKhai;
                pbTKNhap.MaLoaiHinhNhap = this.MaLoaiHinh;
                decimal TongLuongCungUng=0;
                foreach (PhanBoToKhaiNhap pbTKN in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                {
                    if (pbTKN.MaNPL.Trim().ToUpper() == cbhang.SelectedValue.ToString())
                    {
                        TongLuongCungUng += (decimal)pbTKN.LuongPhanBo;
                    }
                }
                if ((TongLuongCungUng + (decimal)pbTKNhap.LuongPhanBo) > Convert.ToDecimal(txtLuongTaiXuat.Text))
                {
                    showMsg("MSG_WRN24");
                    //ShowMessage("Tổng lượng chọn trong các tờ khai nhập chưa bằng lượng tái xuất.", false);
                    return;
                }
                pbToKhaiXuat.PhanBoToKhaiNhapCollection.Add(pbTKNhap);
                dgLispbTKX.Refetch();
                txtSoToKhai.Text = "";
                txtMaNPL.Text = "";
                txtLuong.Value = 0;
            }
        }
        private bool checkExit(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, string maNPL)
        {
            foreach (PhanBoToKhaiNhap nplTT in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
            {
                if (nplTT.SoToKhaiNhap == soToKhai && nplTT.MaLoaiHinhNhap == maLoaiHinh && nplTT.NamDangKyNhap == namDangKy && nplTT.MaHaiQuanNhap == maHaiQuan && nplTT.MaNPL == maNPL)
                    return true;
            }
            return false;
        }
        private void SelectPhanBoToKhaiXuatOfMaNPL(string MaNPL)
        {
            foreach (PhanBoToKhaiXuat pbTKX in TKMD.PhanBoToKhaiXuatCollection)
            {
                if (pbTKX.MaSP.Trim().ToUpper() == MaNPL.Trim().ToUpper())
                {
                    pbToKhaiXuat = pbTKX;
                    txtLuongTaiXuat.Text = pbToKhaiXuat.SoLuongXuat.ToString();
                    return;
                }
            }
        }
        private void BK07Form_Load(object sender, EventArgs e)
        {
            
            txtLuongTaiXuat.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            dgListTon.Tables[0].Columns["Ton"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["LuongTonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["LuongPhanBo"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["LuongTonCuoi"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            
            if (TKMD.HMDCollection.Count == 0)
            {
                TKMD.LoadHMDCollection();
            }
            cbhang.Items.Clear();
            foreach (HangMauDich HMD in TKMD.HMDCollection)
            {
                cbhang.Items.Add(HMD.TenHang + " / " + HMD.MaPhu, HMD.MaPhu);
            }
            cbhang.SelectedIndex = 0;
            this.Text = "Phân bổ cho tờ khai tái xuất số : "+TKMD.SoToKhai+"/"+TKMD.MaLoaiHinh+"/"+TKMD.NgayDangKy.Year;
        }
        private DataRow GetRowTon(DataTable dt, int soToKhai, int namDangKy, string maLoaiHinh, string maNPL)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToInt32(dr["SoToKhai"]) == soToKhai && Convert.ToDateTime(dr["NgayDangKy"]).Year == namDangKy && Convert.ToString(dr["MaLoaiHinh"]) == maLoaiHinh && Convert.ToString(dr["MaNPL"]).Trim() == maNPL.Trim())
                    return dr;
            }
            return null;
        }
        private void BindData()
        {
            DataTable NPLNhapTonThucTeCollection = NPLNhapTonThucTe.SelectBy_HopDongAndMaNPLAndTonHonO(TKMD.IDHopDong, cbhang.SelectedValue.ToString(), TKMD.NgayDangKy).Tables[0];
            foreach (PhanBoToKhaiNhap pbTKN in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
            {
                //DataRow rowTon = NPLNhapTonThucTeCollection.Select("SoToKhai=" + pbTKN.SoToKhaiNhap + " and MaLoaiHinh='" + pbTKN.MaLoaiHinhNhap + "' and year(NgayDangKy)=" + pbTKN.NamDangKyNhap + " and MaHaiQuan='" + pbTKN.MaHaiQuanNhap + "' and MaNPL='" + pbTKN.MaNPL.Trim()+ "'")[0];
                DataRow rowTon = GetRowTon(NPLNhapTonThucTeCollection, pbTKN.SoToKhaiNhap, pbTKN.NamDangKyNhap, pbTKN.MaLoaiHinhNhap, pbTKN.MaNPL);
                rowTon["Ton"] = (Convert.ToDouble(rowTon["Ton"]) - pbTKN.LuongPhanBo);                
            }
            dgListTon.DataSource = NPLNhapTonThucTeCollection;
            dgLispbTKX.DataSource = pbToKhaiXuat.PhanBoToKhaiNhapCollection;
        }
        private bool KiemTraLuongTaiXuat(string maNPL, IList<PhanBoToKhaiNhap> PhanBoToKhaiNhapCollection,double LuongTaiXuat)
        {
            decimal TongTaixuat=0;
            foreach (PhanBoToKhaiNhap pbTKN in PhanBoToKhaiNhapCollection)
            {
                TongTaixuat += (decimal) pbTKN.LuongPhanBo;
            }
            if (TongTaixuat != (decimal)LuongTaiXuat)
                return false;
            return true; 
        }
        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                foreach(PhanBoToKhaiXuat pbToKhaiX in TKMD.PhanBoToKhaiXuatCollection)
                {

                    if (pbToKhaiX.PhanBoToKhaiNhapCollection == null || pbToKhaiX.PhanBoToKhaiNhapCollection.Count == 0)
                    {
                        showMsg("MSG_WRN25", pbToKhaiX.MaSP);
                        //ShowMessage("Bạn chưa chọn phân bổ tờ khai nhập cho mặt hàng " + pbToKhaiX.MaSP + ".", false);
                        return;
                    }
                    if (!KiemTraLuongTaiXuat(pbToKhaiX.MaSP, pbToKhaiX.PhanBoToKhaiNhapCollection, Convert.ToDouble(pbToKhaiX.SoLuongXuat)))
                    {
                        showMsg("MSG_WRN26");
                        //ShowMessage("Lượng tái xuất được chọn trong các tờ khai nhập chưa bằng lượng tái xuất.",false);
                        return;
                    }
                }
                this.Cursor = Cursors.WaitCursor;
                PhanBoToKhaiXuat.PhanBoChoToKhaiTaiXuat(TKMD, GlobalSettings.SoThapPhan.LuongNPL);
                isPhanBo = true;
                this.Close();
            }
            catch (Exception ex)
            {
                //showMsg("MSG_2702004", ex.Message);
                ShowMessage("Lỗi: " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgList1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                NguyenPhuLieu NPL = new NguyenPhuLieu();
                NPL.Ma = e.Row.Cells["MaNPL"].Text.Trim();
                NPL.HopDong_ID = TKMD.IDHopDong;
                NPL.Load();
                e.Row.Cells["TenHang"].Text = NPL.Ten;
            }
        }

        private void dgList2_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["SoToKhaiNhap"].Text = e.Row.Cells["SoToKhaiNhap"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinhNhap"].Value) + "/" + e.Row.Cells["NamDangKyNhap"].Text;
        }

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            txtSoToKhai.Text = e.Row.Cells["SoToKhai"].Text;
            this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
            this.MaLoaiHinh = Convert.ToString(e.Row.Cells["MaLoaiHinh"].Value);
            this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
            this.TenNPL = e.Row.Cells["TenNPL"].Text;
            this.DVT_ID = e.Row.Cells["DVT_ID"].Text;
            this.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
            txtMaNPL.Text = e.Row.Cells["MaNPL"].Text;
            txtLuong.Value = Convert.ToDecimal(e.Row.Cells["LuongNopThue"].Value);
            btnAdd.Enabled = false;
            this.SoLuong = Convert.ToDecimal(e.Row.Cells["Ton"].Text);

        }

      
        private void cbhang_SelectedIndexChanged(object sender, EventArgs e)
        {            
            SelectPhanBoToKhaiXuatOfMaNPL(cbhang.SelectedValue.ToString());
            if (this.pbToKhaiXuat.PhanBoToKhaiNhapCollection == null)
                this.pbToKhaiXuat.PhanBoToKhaiNhapCollection = new List<PhanBoToKhaiNhap>();
            BindData();
        }

    }
}