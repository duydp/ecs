
DELETE [dbo].[t_HaiQuan_LoaiChungTu]


INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (200, N'Giấy kiểm tra')
           
           INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (201, N'Giấy kết quả kiểm tra / Miễn kiểm tra')
           
           INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (202, N'Giấy nộp tiền')
           INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (315, N'Hợp đồng thương mại')
           INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (380, N'Hóa đơn thương mại')
           INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (400, N'Giấy đăng ký kiểm tra chất lượng')
           INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (401, N'Giấy thông báo kết quả kiểm tra chất lượng')
           INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (403, N'Chứng thư giám định')
           INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (404, N'Giấy nộp tiền')
           INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (405, N'Đề nghị chuyển cửa khẩu')
           INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (706, N'Vận tải đơn')
           INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (811, N'Giấy phép xuất khẩu')
           INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (861, N'Giấy chứng nhận xuất xứ hàng hóa')
            INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (911, N'Giấy phép nhập khẩu')
            INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (929, N'Tờ khai nhập khẩu')
            INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (930, N'Tờ khai xuất khẩu')
            INSERT INTO [dbo].[t_HaiQuan_LoaiChungTu]
           ([ID]
           ,[Ten])
     VALUES
           (999, N'Chứng từ khác')
           
---------------------------------------------------------------------------------------------------

Go

--Binh duong
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = 'hqbd.gov.vn', ServicePathV4 = 'tqdt/TQDT_ND87/KDTService/CISService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '43'

GO

---------------------------------------------------------------------------------------------------
           

/****** Object:  Table [dbo].[t_KDT_COBoSung]    Script Date: 04/12/2013 12:23:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_COBoSung]') AND type in (N'U'))
DROP TABLE [dbo].[t_KDT_COBoSung]
GO

/****** Object:  Table [dbo].[t_KDT_COBoSung]    Script Date: 04/12/2013 12:23:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_KDT_COBoSung](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[COID] [bigint] NOT NULL,
	[TenCangXepHang] [nvarchar](255) NULL,
	[TenCangDoHang] [nvarchar](255) NULL,
 CONSTRAINT [PK_t_KDT_COBoSung_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_SelectAll]    Script Date: 04/12/2013 12:23:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_COBoSung_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_COBoSung_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_SelectDynamic]    Script Date: 04/12/2013 12:23:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_COBoSung_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_COBoSung_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_SelectBy_COID]    Script Date: 04/12/2013 12:23:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_COBoSung_SelectBy_COID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_COBoSung_SelectBy_COID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_Load]    Script Date: 04/12/2013 12:23:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_COBoSung_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_COBoSung_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_DeleteDynamic]    Script Date: 04/12/2013 12:23:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_COBoSung_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_COBoSung_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_InsertUpdate]    Script Date: 04/12/2013 12:23:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_COBoSung_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_COBoSung_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_DeleteBy_COID]    Script Date: 04/12/2013 12:23:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_COBoSung_DeleteBy_COID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_COBoSung_DeleteBy_COID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_Delete]    Script Date: 04/12/2013 12:23:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_COBoSung_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_COBoSung_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_Update]    Script Date: 04/12/2013 12:23:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_COBoSung_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_COBoSung_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_Insert]    Script Date: 04/12/2013 12:23:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_COBoSung_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_COBoSung_Insert]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_SelectAll]    Script Date: 04/12/2013 12:23:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_COBoSung_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_COBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[COID],
	[TenCangXepHang],
	[TenCangDoHang]
FROM
	[dbo].[t_KDT_COBoSung]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_SelectDynamic]    Script Date: 04/12/2013 12:23:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_COBoSung_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_COBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[COID],
	[TenCangXepHang],
	[TenCangDoHang]
FROM [dbo].[t_KDT_COBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_SelectBy_COID]    Script Date: 04/12/2013 12:23:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_COBoSung_SelectBy_COID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_COBoSung_SelectBy_COID]
	@COID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[COID],
	[TenCangXepHang],
	[TenCangDoHang]
FROM
	[dbo].[t_KDT_COBoSung]
WHERE
	[COID] = @COID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_Load]    Script Date: 04/12/2013 12:23:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_COBoSung_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_COBoSung_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[COID],
	[TenCangXepHang],
	[TenCangDoHang]
FROM
	[dbo].[t_KDT_COBoSung]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_DeleteDynamic]    Script Date: 04/12/2013 12:23:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_COBoSung_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_COBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_COBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_InsertUpdate]    Script Date: 04/12/2013 12:23:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_COBoSung_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_COBoSung_InsertUpdate]
	@ID bigint,
	@COID bigint,
	@TenCangXepHang nvarchar(255),
	@TenCangDoHang nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_COBoSung] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_COBoSung] 
		SET
			[COID] = @COID,
			[TenCangXepHang] = @TenCangXepHang,
			[TenCangDoHang] = @TenCangDoHang
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_COBoSung]
		(
			[COID],
			[TenCangXepHang],
			[TenCangDoHang]
		)
		VALUES 
		(
			@COID,
			@TenCangXepHang,
			@TenCangDoHang
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_DeleteBy_COID]    Script Date: 04/12/2013 12:23:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_COBoSung_DeleteBy_COID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_COBoSung_DeleteBy_COID]
	@COID bigint
AS

DELETE FROM [dbo].[t_KDT_COBoSung]
WHERE
	[COID] = @COID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_Delete]    Script Date: 04/12/2013 12:23:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_COBoSung_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_COBoSung_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_COBoSung]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_Update]    Script Date: 04/12/2013 12:23:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_COBoSung_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_COBoSung_Update]
	@ID bigint,
	@COID bigint,
	@TenCangXepHang nvarchar(255),
	@TenCangDoHang nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_COBoSung]
SET
	[COID] = @COID,
	[TenCangXepHang] = @TenCangXepHang,
	[TenCangDoHang] = @TenCangDoHang
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_COBoSung_Insert]    Script Date: 04/12/2013 12:23:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_COBoSung_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_COBoSung_Insert]
	@COID bigint,
	@TenCangXepHang nvarchar(255),
	@TenCangDoHang nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_COBoSung]
(
	[COID],
	[TenCangXepHang],
	[TenCangDoHang]
)
VALUES 
(
	@COID,
	@TenCangXepHang,
	@TenCangDoHang
)

SET @ID = SCOPE_IDENTITY()


GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '8.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('8.4',GETDATE(), N'Cap nhat CO bo sung V4')
END