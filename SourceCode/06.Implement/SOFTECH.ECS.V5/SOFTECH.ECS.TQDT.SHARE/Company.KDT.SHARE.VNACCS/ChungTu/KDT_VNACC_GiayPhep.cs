using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class KDT_VNACC_GiayPhep_SEA : IKDT_VNACC_GiayPhep
    {
        List<KDT_VNACC_HangGiayPhep> _listHang = new List<KDT_VNACC_HangGiayPhep>();

        public List<KDT_VNACC_HangGiayPhep> HangCollection
        {
            set { this._listHang = value; }
            get { return this._listHang; }
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert();
                    else
                        this.Update();


                    foreach (KDT_VNACC_HangGiayPhep itemHang in this.HangCollection)
                    {
                        if (itemHang.ID == 0)
                        {
                            itemHang.GiayPhepType = EGiayPhep.SEA.ToString();
                            itemHang.GiayPhep_ID = this.ID;
                            itemHang.ID = itemHang.Insert();
                        }
                        else
                        {
                            itemHang.Update();
                        }
                    }
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
    }

    public partial class KDT_VNACC_GiayPhep_SFA : IKDT_VNACC_GiayPhep
    {
        List<KDT_VNACC_HangGiayPhep> _listHang = new List<KDT_VNACC_HangGiayPhep>();

        public List<KDT_VNACC_HangGiayPhep> HangCollection
        {
            set { this._listHang = value; }
            get { return this._listHang; }
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert();
                    else
                        this.Update();


                    foreach (KDT_VNACC_HangGiayPhep itemHang in this.HangCollection)
                    {
                        if (itemHang.ID == 0)
                        {
                            itemHang.GiayPhepType = EGiayPhep.SFA.ToString();
                            itemHang.GiayPhep_ID = this.ID;
                            itemHang.ID = itemHang.Insert();
                        }
                        else
                        {
                            itemHang.Update();
                        }
                    }
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
    }

    public partial class KDT_VNACC_GiayPhep_SAA : IKDT_VNACC_GiayPhep
    {
        List<KDT_VNACC_HangGiayPhep> _listHang = new List<KDT_VNACC_HangGiayPhep>();
        List<KDT_VNACC_HangGiayPhep_CangTrungGian> _listCangTrungGian = new List<KDT_VNACC_HangGiayPhep_CangTrungGian>();
        List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> _listTinhTrangThamTra = new List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra>();

        public List<KDT_VNACC_HangGiayPhep> HangCollection
        {
            set { this._listHang = value; }
            get { return this._listHang; }
        }

        public List<KDT_VNACC_HangGiayPhep_CangTrungGian> CangTrungGianCollection
        {
            set { this._listCangTrungGian = value; }
            get { return this._listCangTrungGian; }
        }

        public List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> TinhTrangThamTraCollection
        {
            set { this._listTinhTrangThamTra = value; }
            get { return this._listTinhTrangThamTra; }
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert();
                    else
                        this.Update();


                    foreach (KDT_VNACC_HangGiayPhep itemHang in this.HangCollection)
                    {
                        if (itemHang.ID == 0)
                        {
                            itemHang.GiayPhepType = EGiayPhep.SAA.ToString();
                            itemHang.GiayPhep_ID = this.ID;
                            itemHang.ID = itemHang.Insert();
                        }
                        else
                        {
                            itemHang.Update();
                        }
                    }
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateFull_CangTrungGian()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert();
                    else
                        this.Update();


                    foreach (KDT_VNACC_HangGiayPhep_CangTrungGian itemCang in this.CangTrungGianCollection)
                    {
                        if (itemCang.ID == 0)
                        {
                            itemCang.GiayPhep_ID = this.ID;
                            itemCang.ID = itemCang.Insert();
                        }
                        else
                        {
                            itemCang.Update();
                        }
                    }
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateFull_TinhTrangThamTra()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert();
                    else
                        this.Update();

                    foreach (KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra item in this.TinhTrangThamTraCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.GiayPhep_ID = this.ID;
                            item.ID = item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
    }

    public partial class KDT_VNACC_GiayPhep_SMA : IKDT_VNACC_GiayPhep
    {
        List<KDT_VNACC_HangGiayPhep_SMA> _listHang = new List<KDT_VNACC_HangGiayPhep_SMA>();

        public List<KDT_VNACC_HangGiayPhep_SMA> HangCollection
        {
            set { this._listHang = value; }
            get { return this._listHang; }
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert();
                    else
                        this.Update();


                    foreach (KDT_VNACC_HangGiayPhep_SMA itemHang in this.HangCollection)
                    {
                        if (itemHang.ID == 0)
                        {
                            itemHang.GiayPhep_ID = this.ID;
                            itemHang.ID = itemHang.Insert();
                        }
                        else
                        {
                            itemHang.Update();
                        }
                    }
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
    }

}