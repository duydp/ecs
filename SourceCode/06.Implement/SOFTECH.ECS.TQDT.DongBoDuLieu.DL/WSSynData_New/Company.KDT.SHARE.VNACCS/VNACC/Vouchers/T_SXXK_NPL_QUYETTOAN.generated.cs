﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class T_SXXK_NPL_QUYETTOAN : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MANPL { set; get; }
		public string TENNPL { set; get; }
		public string DVT { set; get; }
		public decimal LUONGTONDK { set; get; }
		public decimal TRIGIATONDK { set; get; }
		public decimal LUONGNHAPTK { set; get; }
		public decimal TRIGIANHAPTK { set; get; }
		public decimal LUONGXUATTK { set; get; }
		public decimal TRIGIAXUATTK { set; get; }
		public decimal LUONGTONCK { set; get; }
		public decimal TRIGIATONCK { set; get; }
		public DateTime NGAYBATDAU { set; get; }
		public DateTime NGAYKETTHUC { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_SXXK_NPL_QUYETTOAN> ConvertToCollection(IDataReader reader)
		{
			List<T_SXXK_NPL_QUYETTOAN> collection = new List<T_SXXK_NPL_QUYETTOAN>();
			while (reader.Read())
			{
				T_SXXK_NPL_QUYETTOAN entity = new T_SXXK_NPL_QUYETTOAN();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MANPL"))) entity.MANPL = reader.GetString(reader.GetOrdinal("MANPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENNPL"))) entity.TENNPL = reader.GetString(reader.GetOrdinal("TENNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGTONDK"))) entity.LUONGTONDK = reader.GetDecimal(reader.GetOrdinal("LUONGTONDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIATONDK"))) entity.TRIGIATONDK = reader.GetDecimal(reader.GetOrdinal("TRIGIATONDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGNHAPTK"))) entity.LUONGNHAPTK = reader.GetDecimal(reader.GetOrdinal("LUONGNHAPTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIANHAPTK"))) entity.TRIGIANHAPTK = reader.GetDecimal(reader.GetOrdinal("TRIGIANHAPTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGXUATTK"))) entity.LUONGXUATTK = reader.GetDecimal(reader.GetOrdinal("LUONGXUATTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIAXUATTK"))) entity.TRIGIAXUATTK = reader.GetDecimal(reader.GetOrdinal("TRIGIAXUATTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGTONCK"))) entity.LUONGTONCK = reader.GetDecimal(reader.GetOrdinal("LUONGTONCK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIATONCK"))) entity.TRIGIATONCK = reader.GetDecimal(reader.GetOrdinal("TRIGIATONCK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAYBATDAU"))) entity.NGAYBATDAU = reader.GetDateTime(reader.GetOrdinal("NGAYBATDAU"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAYKETTHUC"))) entity.NGAYKETTHUC = reader.GetDateTime(reader.GetOrdinal("NGAYKETTHUC"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_SXXK_NPL_QUYETTOAN> collection, long id)
        {
            foreach (T_SXXK_NPL_QUYETTOAN item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_SXXK_NPL_QUYETTOAN VALUES(@MANPL, @TENNPL, @DVT, @LUONGTONDK, @TRIGIATONDK, @LUONGNHAPTK, @TRIGIANHAPTK, @LUONGXUATTK, @TRIGIAXUATTK, @LUONGTONCK, @TRIGIATONCK, @NGAYBATDAU, @NGAYKETTHUC)";
            string update = "UPDATE T_SXXK_NPL_QUYETTOAN SET MANPL = @MANPL, TENNPL = @TENNPL, DVT = @DVT, LUONGTONDK = @LUONGTONDK, TRIGIATONDK = @TRIGIATONDK, LUONGNHAPTK = @LUONGNHAPTK, TRIGIANHAPTK = @TRIGIANHAPTK, LUONGXUATTK = @LUONGXUATTK, TRIGIAXUATTK = @TRIGIAXUATTK, LUONGTONCK = @LUONGTONCK, TRIGIATONCK = @TRIGIATONCK, NGAYBATDAU = @NGAYBATDAU, NGAYKETTHUC = @NGAYKETTHUC WHERE ID = @ID";
            string delete = "DELETE FROM T_SXXK_NPL_QUYETTOAN WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANPL", SqlDbType.VarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONDK", SqlDbType.Decimal, "LUONGTONDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIATONDK", SqlDbType.Decimal, "TRIGIATONDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGNHAPTK", SqlDbType.Decimal, "LUONGNHAPTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, "TRIGIANHAPTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGXUATTK", SqlDbType.Decimal, "LUONGXUATTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, "TRIGIAXUATTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONCK", SqlDbType.Decimal, "LUONGTONCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIATONCK", SqlDbType.Decimal, "TRIGIATONCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYBATDAU", SqlDbType.DateTime, "NGAYBATDAU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYKETTHUC", SqlDbType.DateTime, "NGAYKETTHUC", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANPL", SqlDbType.VarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONDK", SqlDbType.Decimal, "LUONGTONDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIATONDK", SqlDbType.Decimal, "TRIGIATONDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGNHAPTK", SqlDbType.Decimal, "LUONGNHAPTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, "TRIGIANHAPTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGXUATTK", SqlDbType.Decimal, "LUONGXUATTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, "TRIGIAXUATTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONCK", SqlDbType.Decimal, "LUONGTONCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIATONCK", SqlDbType.Decimal, "TRIGIATONCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYBATDAU", SqlDbType.DateTime, "NGAYBATDAU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYKETTHUC", SqlDbType.DateTime, "NGAYKETTHUC", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_SXXK_NPL_QUYETTOAN VALUES(@MANPL, @TENNPL, @DVT, @LUONGTONDK, @TRIGIATONDK, @LUONGNHAPTK, @TRIGIANHAPTK, @LUONGXUATTK, @TRIGIAXUATTK, @LUONGTONCK, @TRIGIATONCK, @NGAYBATDAU, @NGAYKETTHUC)";
            string update = "UPDATE T_SXXK_NPL_QUYETTOAN SET MANPL = @MANPL, TENNPL = @TENNPL, DVT = @DVT, LUONGTONDK = @LUONGTONDK, TRIGIATONDK = @TRIGIATONDK, LUONGNHAPTK = @LUONGNHAPTK, TRIGIANHAPTK = @TRIGIANHAPTK, LUONGXUATTK = @LUONGXUATTK, TRIGIAXUATTK = @TRIGIAXUATTK, LUONGTONCK = @LUONGTONCK, TRIGIATONCK = @TRIGIATONCK, NGAYBATDAU = @NGAYBATDAU, NGAYKETTHUC = @NGAYKETTHUC WHERE ID = @ID";
            string delete = "DELETE FROM T_SXXK_NPL_QUYETTOAN WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANPL", SqlDbType.VarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONDK", SqlDbType.Decimal, "LUONGTONDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIATONDK", SqlDbType.Decimal, "TRIGIATONDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGNHAPTK", SqlDbType.Decimal, "LUONGNHAPTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, "TRIGIANHAPTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGXUATTK", SqlDbType.Decimal, "LUONGXUATTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, "TRIGIAXUATTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONCK", SqlDbType.Decimal, "LUONGTONCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIATONCK", SqlDbType.Decimal, "TRIGIATONCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYBATDAU", SqlDbType.DateTime, "NGAYBATDAU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYKETTHUC", SqlDbType.DateTime, "NGAYKETTHUC", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANPL", SqlDbType.VarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONDK", SqlDbType.Decimal, "LUONGTONDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIATONDK", SqlDbType.Decimal, "TRIGIATONDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGNHAPTK", SqlDbType.Decimal, "LUONGNHAPTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, "TRIGIANHAPTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGXUATTK", SqlDbType.Decimal, "LUONGXUATTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, "TRIGIAXUATTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONCK", SqlDbType.Decimal, "LUONGTONCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIATONCK", SqlDbType.Decimal, "TRIGIATONCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYBATDAU", SqlDbType.DateTime, "NGAYBATDAU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYKETTHUC", SqlDbType.DateTime, "NGAYKETTHUC", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_SXXK_NPL_QUYETTOAN Load(long id)
		{
			const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_SXXK_NPL_QUYETTOAN> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_SXXK_NPL_QUYETTOAN> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_SXXK_NPL_QUYETTOAN> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_SXXK_NPL_QUYETTOAN(string mANPL, string tENNPL, string dVT, decimal lUONGTONDK, decimal tRIGIATONDK, decimal lUONGNHAPTK, decimal tRIGIANHAPTK, decimal lUONGXUATTK, decimal tRIGIAXUATTK, decimal lUONGTONCK, decimal tRIGIATONCK, DateTime nGAYBATDAU, DateTime nGAYKETTHUC)
		{
			T_SXXK_NPL_QUYETTOAN entity = new T_SXXK_NPL_QUYETTOAN();	
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.DVT = dVT;
			entity.LUONGTONDK = lUONGTONDK;
			entity.TRIGIATONDK = tRIGIATONDK;
			entity.LUONGNHAPTK = lUONGNHAPTK;
			entity.TRIGIANHAPTK = tRIGIANHAPTK;
			entity.LUONGXUATTK = lUONGXUATTK;
			entity.TRIGIAXUATTK = tRIGIAXUATTK;
			entity.LUONGTONCK = lUONGTONCK;
			entity.TRIGIATONCK = tRIGIATONCK;
			entity.NGAYBATDAU = nGAYBATDAU;
			entity.NGAYKETTHUC = nGAYKETTHUC;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.VarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LUONGTONDK", SqlDbType.Decimal, LUONGTONDK);
			db.AddInParameter(dbCommand, "@TRIGIATONDK", SqlDbType.Decimal, TRIGIATONDK);
			db.AddInParameter(dbCommand, "@LUONGNHAPTK", SqlDbType.Decimal, LUONGNHAPTK);
			db.AddInParameter(dbCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, TRIGIANHAPTK);
			db.AddInParameter(dbCommand, "@LUONGXUATTK", SqlDbType.Decimal, LUONGXUATTK);
			db.AddInParameter(dbCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, TRIGIAXUATTK);
			db.AddInParameter(dbCommand, "@LUONGTONCK", SqlDbType.Decimal, LUONGTONCK);
			db.AddInParameter(dbCommand, "@TRIGIATONCK", SqlDbType.Decimal, TRIGIATONCK);
			db.AddInParameter(dbCommand, "@NGAYBATDAU", SqlDbType.DateTime, NGAYBATDAU.Year <= 1753 ? DBNull.Value : (object) NGAYBATDAU);
			db.AddInParameter(dbCommand, "@NGAYKETTHUC", SqlDbType.DateTime, NGAYKETTHUC.Year <= 1753 ? DBNull.Value : (object) NGAYKETTHUC);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_SXXK_NPL_QUYETTOAN> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_SXXK_NPL_QUYETTOAN item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_SXXK_NPL_QUYETTOAN(long id, string mANPL, string tENNPL, string dVT, decimal lUONGTONDK, decimal tRIGIATONDK, decimal lUONGNHAPTK, decimal tRIGIANHAPTK, decimal lUONGXUATTK, decimal tRIGIAXUATTK, decimal lUONGTONCK, decimal tRIGIATONCK, DateTime nGAYBATDAU, DateTime nGAYKETTHUC)
		{
			T_SXXK_NPL_QUYETTOAN entity = new T_SXXK_NPL_QUYETTOAN();			
			entity.ID = id;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.DVT = dVT;
			entity.LUONGTONDK = lUONGTONDK;
			entity.TRIGIATONDK = tRIGIATONDK;
			entity.LUONGNHAPTK = lUONGNHAPTK;
			entity.TRIGIANHAPTK = tRIGIANHAPTK;
			entity.LUONGXUATTK = lUONGXUATTK;
			entity.TRIGIAXUATTK = tRIGIAXUATTK;
			entity.LUONGTONCK = lUONGTONCK;
			entity.TRIGIATONCK = tRIGIATONCK;
			entity.NGAYBATDAU = nGAYBATDAU;
			entity.NGAYKETTHUC = nGAYKETTHUC;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_SXXK_NPL_QUYETTOAN_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.VarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LUONGTONDK", SqlDbType.Decimal, LUONGTONDK);
			db.AddInParameter(dbCommand, "@TRIGIATONDK", SqlDbType.Decimal, TRIGIATONDK);
			db.AddInParameter(dbCommand, "@LUONGNHAPTK", SqlDbType.Decimal, LUONGNHAPTK);
			db.AddInParameter(dbCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, TRIGIANHAPTK);
			db.AddInParameter(dbCommand, "@LUONGXUATTK", SqlDbType.Decimal, LUONGXUATTK);
			db.AddInParameter(dbCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, TRIGIAXUATTK);
			db.AddInParameter(dbCommand, "@LUONGTONCK", SqlDbType.Decimal, LUONGTONCK);
			db.AddInParameter(dbCommand, "@TRIGIATONCK", SqlDbType.Decimal, TRIGIATONCK);
			db.AddInParameter(dbCommand, "@NGAYBATDAU", SqlDbType.DateTime, NGAYBATDAU.Year <= 1753 ? DBNull.Value : (object) NGAYBATDAU);
			db.AddInParameter(dbCommand, "@NGAYKETTHUC", SqlDbType.DateTime, NGAYKETTHUC.Year <= 1753 ? DBNull.Value : (object) NGAYKETTHUC);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_SXXK_NPL_QUYETTOAN> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_SXXK_NPL_QUYETTOAN item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_SXXK_NPL_QUYETTOAN(long id, string mANPL, string tENNPL, string dVT, decimal lUONGTONDK, decimal tRIGIATONDK, decimal lUONGNHAPTK, decimal tRIGIANHAPTK, decimal lUONGXUATTK, decimal tRIGIAXUATTK, decimal lUONGTONCK, decimal tRIGIATONCK, DateTime nGAYBATDAU, DateTime nGAYKETTHUC)
		{
			T_SXXK_NPL_QUYETTOAN entity = new T_SXXK_NPL_QUYETTOAN();			
			entity.ID = id;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.DVT = dVT;
			entity.LUONGTONDK = lUONGTONDK;
			entity.TRIGIATONDK = tRIGIATONDK;
			entity.LUONGNHAPTK = lUONGNHAPTK;
			entity.TRIGIANHAPTK = tRIGIANHAPTK;
			entity.LUONGXUATTK = lUONGXUATTK;
			entity.TRIGIAXUATTK = tRIGIAXUATTK;
			entity.LUONGTONCK = lUONGTONCK;
			entity.TRIGIATONCK = tRIGIATONCK;
			entity.NGAYBATDAU = nGAYBATDAU;
			entity.NGAYKETTHUC = nGAYKETTHUC;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.VarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LUONGTONDK", SqlDbType.Decimal, LUONGTONDK);
			db.AddInParameter(dbCommand, "@TRIGIATONDK", SqlDbType.Decimal, TRIGIATONDK);
			db.AddInParameter(dbCommand, "@LUONGNHAPTK", SqlDbType.Decimal, LUONGNHAPTK);
			db.AddInParameter(dbCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, TRIGIANHAPTK);
			db.AddInParameter(dbCommand, "@LUONGXUATTK", SqlDbType.Decimal, LUONGXUATTK);
			db.AddInParameter(dbCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, TRIGIAXUATTK);
			db.AddInParameter(dbCommand, "@LUONGTONCK", SqlDbType.Decimal, LUONGTONCK);
			db.AddInParameter(dbCommand, "@TRIGIATONCK", SqlDbType.Decimal, TRIGIATONCK);
			db.AddInParameter(dbCommand, "@NGAYBATDAU", SqlDbType.DateTime, NGAYBATDAU.Year <= 1753 ? DBNull.Value : (object) NGAYBATDAU);
			db.AddInParameter(dbCommand, "@NGAYKETTHUC", SqlDbType.DateTime, NGAYKETTHUC.Year <= 1753 ? DBNull.Value : (object) NGAYKETTHUC);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_SXXK_NPL_QUYETTOAN> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_SXXK_NPL_QUYETTOAN item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_SXXK_NPL_QUYETTOAN(long id)
		{
			T_SXXK_NPL_QUYETTOAN entity = new T_SXXK_NPL_QUYETTOAN();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_SXXK_NPL_QUYETTOAN> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_SXXK_NPL_QUYETTOAN item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}