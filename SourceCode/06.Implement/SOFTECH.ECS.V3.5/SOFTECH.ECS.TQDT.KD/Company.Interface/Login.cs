﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;
using System.Collections;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Tab;
using System.Globalization;
using Janus.Windows.GridEX;
using Janus.Windows.ExplorerBar;
using System.Threading;
using System.Resources;
using Janus.Windows.UI.Dock;
using System.Runtime.InteropServices;

namespace Company.Interface
{
    public partial class Login : Form
    {

        protected Company.Controls.MessageBoxControl _MsgBox;
        public Login()
        {
            InitializeComponent();
        }
        public string setText(string vnText, string enText)
        {
            if (Properties.Settings.Default.NgonNgu == "1")
            {
                return enText.Trim();
            }
            else
            {
                return vnText.Trim();
            }
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new Company.Controls.MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog(this);
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.EcsQuanTri = ECSPrincipal.ValidateLogin(txtUser.Text.Trim(), txtMatKhau.Text.Trim());
                if (MainForm.EcsQuanTri == null)
                {
                    ShowMessage(setText("Đăng nhập không thành công.", "Login fail!"), false);
                    MainForm.isLoginSuccess = false;
                }
                else
                {
                    if (MainForm.isUseSettingVNACC)
                    {
                        try
                        {
                            //TODO: Kiem tra bang USER: da co bo sung cac field va store cho VNACC?
                            Company.KDT.SHARE.Components.SQL.CheckUserTableIsHaveVNACCS(GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME, GlobalSettings.USER, GlobalSettings.PASS);
                        }
                        catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("SQL.CheckUserTableIsHaveVNACCS", ex); }

                        //TODO: Cap nhat ten DataBase ket noi cho User login
                        Company.QuanTri.User userVNACC = Company.QuanTri.User.Load_VNACC(((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.ID);
                        userVNACC.DataBase = GlobalSettings.DATABASE_NAME;
                        userVNACC.Update_VNACC();

                        try
                        {
                            //TODO: Kiem tra bang USER: da co bo sung cac field va store cho VNACC?
                            Company.KDT.SHARE.Components.SQL.CheckAndCreateProcedureCheckTerminalOnline(GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME, GlobalSettings.USER, GlobalSettings.PASS);

                            //TODO: Chay store cap nhat trang thai cua Terminal.
                            User.CheckAndUpdateTerminalStatus();
                        }
                        catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("CheckAndCreateProcedureCheckTerminalOnline", ex); }

                        //TODO: Kiem tra cho phep 1 USER duoc phep truy cap duy nhat.
                        //userVNACC = Company.QuanTri.User.Load_VNACC(((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.ID);
                        //if (userVNACC.isOnline && userVNACC.IP != Company.KDT.SHARE.Components.Globals.GetLocalIPAddress())
                        //{
                        //    string msg = string.Format("Tài khoản người sử dụng '{0}' này đang kết nối khai báo.\r\n\n- Máy đang kết nối: {1} - {2}\r\n\nBạn vui lòng kiểm tra lại hoặc đăng nhập với tài khoản người sử dụng khác.", userVNACC.USER_NAME, userVNACC.IP, userVNACC.HostName);
                        //    ShowMessage(msg, false);
                        //    MainForm.isLoginSuccess = false;
                        //    return;
                        //}

                        try
                        {
                            //TODO: Cap nhat IP, HostName cua PC dang nhap vao bang USER
                            userVNACC = Company.QuanTri.User.Load_VNACC(((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.ID);
                            userVNACC.HostName = System.Environment.MachineName;
                            userVNACC.IP = Company.KDT.SHARE.Components.Globals.GetLocalIPAddress();
                            userVNACC.DataBase = GlobalSettings.DATABASE_NAME;
                            userVNACC.Update_VNACC();
                        }
                        catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("userVNACC update", ex); }

                        try
                        {
                            //TODO: Kiem tra Terminal co cung ID co dang su dung khong truoc khi dang nhap nguoi dung. Chi cho phep 1 ternimal ID duoc phep khai bao.                    
                            if (Company.QuanTri.User.CheckVNACCTerminalCountOnline(userVNACC.VNACC_TerminalID) > 1
                                || Company.QuanTri.User.CheckVNACCTerminalCountOnline(userVNACC.VNACC_TerminalID, userVNACC.USER_NAME) > 1)
                            {
                                ShowMessage("Phát hiện có 1 Terminal '" + userVNACC.VNACC_TerminalID + "' khác đang kết nối khai báo. Bạn vui lòng thiết lập lại thông tin cho Terminal.", false);

                                userVNACC.VNACC_TerminalID = string.Empty;
                                userVNACC.VNACC_TerminalPass = string.Empty;
                                userVNACC.VNACC_UserID = string.Empty;
                                userVNACC.VNACC_UserCode = string.Empty;
                                userVNACC.VNACC_UserPass = string.Empty;
                                userVNACC.Update_VNACC();
                            }
                        }
                        catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("Check Terminal", ex); }

                        try
                        {
                            if (userVNACC == null)
                                Logger.LocalLogger.Instance().WriteMessage(new Exception("object userVNACC is null"));

                            //Cap nhat bien toan cuc VNACC cho chuong trinh
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalID = userVNACC.VNACC_TerminalID;
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalAccessKey = userVNACC.VNACC_TerminalPass != null ? userVNACC.VNACC_TerminalPass.Trim() : "";
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_ID = userVNACC.VNACC_UserID;
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Ma = userVNACC.VNACC_UserCode;
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Pass = userVNACC.VNACC_UserPass != null ? userVNACC.VNACC_UserPass.Trim() : "";
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.Url = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("VNACCAddress", @"https://ediconn.vnaccs.customs.gov.vn/test");
                        }
                        catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("Assign GlobalVNACC", ex); }
                    }

                    //Company.KDT.SHARE.Components.Globals.APP_SECRET = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("APP_SECRET");
                    //Company.KDT.SHARE.Components.Globals.APP_ID = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("APP_ID");
                    //Company.KDT.SHARE.Components.Globals.SERVICE_GET_TOKENURL = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SERVICE_GET_TOKENURL");
                    //Company.KDT.SHARE.Components.Globals.URL_CREDENTIALSLIST = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_CREDENTIALSLIST");
                    //Company.KDT.SHARE.Components.Globals.URL_CREDENTIALSINFO = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_CREDENTIALSINFO");
                    //Company.KDT.SHARE.Components.Globals.URL_SIGNHASH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_SIGNHASH");
                    //Company.KDT.SHARE.Components.Globals.URL_SIGN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_SIGN");
                    //Company.KDT.SHARE.Components.Globals.URL_GETTRANINFO = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_GETTRANINFO");
                    //Company.KDT.SHARE.Components.Globals.IsSignSmartCA = System.Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsSignSmartCA", "False"));
                    //Company.KDT.SHARE.Components.Globals.UserNameSmartCA = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("UserNameSmartCA");
                    //Company.KDT.SHARE.Components.Globals.PaswordSmartCA = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PassWordSmartCA");

                    MainForm.isLoginSuccess = true;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Không kết nối được cơ sở dữ liệu : " + ex.Message, false);
                linkLabel1_LinkClicked(null, null);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog(this);
        }
        private void Login_Load(object sender, EventArgs e)
        {
            txtUser.Text = txtMatKhau.Text = string.Empty;
            txtUser.Focus();
            try
            {
                uiButton3.Text = setText("Đăng nhập", "Login");
                uiButton2.Text = setText("Thoát", "Exit");
                linkLabel1.Text = setText("Cấu hình thông số kết nối", "Configuration of connected parameters");
                if (Properties.Settings.Default.NgonNgu == "1")
                {
                    this.BackgroundImage = System.Drawing.Image.FromFile("LOGIN-KD-eng.png");
                }
                else
                {
                    this.BackgroundImage = System.Drawing.Image.FromFile("LOGIN-KD.png");
                }

                // this.BackgroundImageLayout = ImageLayout.Stretch;
            }
            catch { }
            foreach (Control item in this.Controls)
            {
                item.KeyDown += Help_KeyDown;
                OnKeyDown(item);
            }
        }
        #region Help
        private void OnKeyDown(Control ctr)
        {
            if (ctr.Controls.Count > 0)
                foreach (Control item in ctr.Controls)
                {
                    item.KeyDown += Help_KeyDown;
                    OnKeyDown(item);
                }
        }
        // This overload is for passing a single uint value as the dwData parameter.
        [DllImport("hhctrl.ocx", CharSet = CharSet.Unicode, EntryPoint = "HtmlHelpW")]
        protected static extern int HtmlHelp(
            int caller,
            String file,
            uint command,
            uint data
            );
        public static void ShowHelpTopic(string topic)
        {
            HtmlHelp(0, AppDomain.CurrentDomain.BaseDirectory + "Helps\\Help.chm" + "::/" + topic.ToLower() + ".html", HH_DISPLAY_TOC, 0);
        }
        protected const int HH_DISPLAY_TOC = 0x0001;  // [Use HtmlHelp_DisplayTOC()]

        void Help_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    {
                        ShowHelpTopic((this.Tag != null && this.Tag.ToString() != "") ? this.Tag.ToString() : this.Name.ToString());
                    }
                    break;

            }
        }
        #endregion

    }
}
