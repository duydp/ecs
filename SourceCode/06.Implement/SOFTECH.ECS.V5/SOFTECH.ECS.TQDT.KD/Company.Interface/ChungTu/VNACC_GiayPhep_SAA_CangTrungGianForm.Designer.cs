﻿namespace Company.Interface
{
    partial class VNACC_GiayPhep_SAA_CangTrungGianForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grdHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_GiayPhep_SAA_CangTrungGianForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.ucDonViTinhSoLuongHangCangTrungGian = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucDonViTinhKhoiLuongHangCangTrungGian = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSoLuongHangCangTrungGian = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtKhoiLuongHangCangTrungGian = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTenCangTrungGian = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHangCangTrungGian = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grdHang = new Janus.Windows.GridEX.GridEX();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 385), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 385);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 361);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 361);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grdHang);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(580, 385);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Controls.Add(this.ucDonViTinhSoLuongHangCangTrungGian);
            this.uiGroupBox1.Controls.Add(this.ucDonViTinhKhoiLuongHangCangTrungGian);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongHangCangTrungGian);
            this.uiGroupBox1.Controls.Add(this.txtKhoiLuongHangCangTrungGian);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.txtTenCangTrungGian);
            this.uiGroupBox1.Controls.Add(this.txtTenHangCangTrungGian);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(580, 165);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(266, 136);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = global::Company.Interface.Properties.Resources.disk;
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.Location = new System.Drawing.Point(99, 136);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 8;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // ucDonViTinhSoLuongHangCangTrungGian
            // 
            this.ucDonViTinhSoLuongHangCangTrungGian.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ucDonViTinhSoLuongHangCangTrungGian.Code = "";
            this.ucDonViTinhSoLuongHangCangTrungGian.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhSoLuongHangCangTrungGian.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhSoLuongHangCangTrungGian.IsValidate = true;
            this.ucDonViTinhSoLuongHangCangTrungGian.Location = new System.Drawing.Point(256, 106);
            this.ucDonViTinhSoLuongHangCangTrungGian.Name = "ucDonViTinhSoLuongHangCangTrungGian";
            this.ucDonViTinhSoLuongHangCangTrungGian.Name_VN = "";
            this.ucDonViTinhSoLuongHangCangTrungGian.ShowColumnCode = true;
            this.ucDonViTinhSoLuongHangCangTrungGian.ShowColumnName = false;
            this.ucDonViTinhSoLuongHangCangTrungGian.Size = new System.Drawing.Size(53, 26);
            this.ucDonViTinhSoLuongHangCangTrungGian.TabIndex = 5;
            this.ucDonViTinhSoLuongHangCangTrungGian.TagName = "";
            this.ucDonViTinhSoLuongHangCangTrungGian.WhereCondition = "";
            this.ucDonViTinhSoLuongHangCangTrungGian.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucDonViTinhKhoiLuongHangCangTrungGian
            // 
            this.ucDonViTinhKhoiLuongHangCangTrungGian.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucDonViTinhKhoiLuongHangCangTrungGian.Code = "";
            this.ucDonViTinhKhoiLuongHangCangTrungGian.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhKhoiLuongHangCangTrungGian.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhKhoiLuongHangCangTrungGian.IsValidate = true;
            this.ucDonViTinhKhoiLuongHangCangTrungGian.Location = new System.Drawing.Point(519, 106);
            this.ucDonViTinhKhoiLuongHangCangTrungGian.Name = "ucDonViTinhKhoiLuongHangCangTrungGian";
            this.ucDonViTinhKhoiLuongHangCangTrungGian.Name_VN = "";
            this.ucDonViTinhKhoiLuongHangCangTrungGian.ShowColumnCode = true;
            this.ucDonViTinhKhoiLuongHangCangTrungGian.ShowColumnName = false;
            this.ucDonViTinhKhoiLuongHangCangTrungGian.Size = new System.Drawing.Size(53, 26);
            this.ucDonViTinhKhoiLuongHangCangTrungGian.TabIndex = 7;
            this.ucDonViTinhKhoiLuongHangCangTrungGian.TagName = "";
            this.ucDonViTinhKhoiLuongHangCangTrungGian.WhereCondition = "";
            this.ucDonViTinhKhoiLuongHangCangTrungGian.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // btnXoa
            // 
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(183, 136);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 9;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Tên cảng";
            // 
            // txtSoLuongHangCangTrungGian
            // 
            this.txtSoLuongHangCangTrungGian.DecimalDigits = 0;
            this.txtSoLuongHangCangTrungGian.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongHangCangTrungGian.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongHangCangTrungGian.Location = new System.Drawing.Point(99, 109);
            this.txtSoLuongHangCangTrungGian.MaxLength = 8;
            this.txtSoLuongHangCangTrungGian.Name = "txtSoLuongHangCangTrungGian";
            this.txtSoLuongHangCangTrungGian.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongHangCangTrungGian.Size = new System.Drawing.Size(151, 21);
            this.txtSoLuongHangCangTrungGian.TabIndex = 4;
            this.txtSoLuongHangCangTrungGian.Text = "0";
            this.txtSoLuongHangCangTrungGian.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongHangCangTrungGian.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongHangCangTrungGian.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtKhoiLuongHangCangTrungGian
            // 
            this.txtKhoiLuongHangCangTrungGian.DecimalDigits = 3;
            this.txtKhoiLuongHangCangTrungGian.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtKhoiLuongHangCangTrungGian.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKhoiLuongHangCangTrungGian.Location = new System.Drawing.Point(392, 109);
            this.txtKhoiLuongHangCangTrungGian.MaxLength = 10;
            this.txtKhoiLuongHangCangTrungGian.Name = "txtKhoiLuongHangCangTrungGian";
            this.txtKhoiLuongHangCangTrungGian.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtKhoiLuongHangCangTrungGian.Size = new System.Drawing.Size(121, 21);
            this.txtKhoiLuongHangCangTrungGian.TabIndex = 6;
            this.txtKhoiLuongHangCangTrungGian.Text = "0.000";
            this.txtKhoiLuongHangCangTrungGian.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKhoiLuongHangCangTrungGian.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtKhoiLuongHangCangTrungGian.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(44, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Số lượng";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(329, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Khối lượng";
            // 
            // txtTenCangTrungGian
            // 
            this.txtTenCangTrungGian.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenCangTrungGian.Location = new System.Drawing.Point(99, 61);
            this.txtTenCangTrungGian.MaxLength = 255;
            this.txtTenCangTrungGian.Multiline = true;
            this.txtTenCangTrungGian.Name = "txtTenCangTrungGian";
            this.txtTenCangTrungGian.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTenCangTrungGian.Size = new System.Drawing.Size(471, 42);
            this.txtTenCangTrungGian.TabIndex = 3;
            this.txtTenCangTrungGian.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenCangTrungGian.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenHangCangTrungGian
            // 
            this.txtTenHangCangTrungGian.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangCangTrungGian.Location = new System.Drawing.Point(99, 14);
            this.txtTenHangCangTrungGian.MaxLength = 255;
            this.txtTenHangCangTrungGian.Multiline = true;
            this.txtTenHangCangTrungGian.Name = "txtTenHangCangTrungGian";
            this.txtTenHangCangTrungGian.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTenHangCangTrungGian.Size = new System.Drawing.Size(471, 42);
            this.txtTenHangCangTrungGian.TabIndex = 1;
            this.txtTenHangCangTrungGian.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHangCangTrungGian.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHangCangTrungGian.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên hàng";
            // 
            // grdHang
            // 
            this.grdHang.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdHang.CardColumnHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.ColumnAutoResize = true;
            grdHang_DesignTimeLayout.LayoutString = resources.GetString("grdHang_DesignTimeLayout.LayoutString");
            this.grdHang.DesignTimeLayout = grdHang_DesignTimeLayout;
            this.grdHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grdHang.GroupByBoxVisible = false;
            this.grdHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.Location = new System.Drawing.Point(0, 165);
            this.grdHang.Name = "grdHang";
            this.grdHang.RecordNavigator = true;
            this.grdHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdHang.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdHang.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdHang.Size = new System.Drawing.Size(580, 220);
            this.grdHang.TabIndex = 1;
            this.grdHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdHang.VisualStyleManager = this.vsmMain;
            this.grdHang.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grdHang_RowDoubleClick);
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // VNACC_GiayPhep_SAA_CangTrungGianForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 391);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_GiayPhep_SAA_CangTrungGianForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin cảng trung gian";
            this.Load += new System.EventHandler(this.VNACC_GiayPhep_SAA_CangTrungGianForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX grdHang;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtKhoiLuongHangCangTrungGian;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangCangTrungGian;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhKhoiLuongHangCangTrungGian;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhSoLuongHangCangTrungGian;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongHangCangTrungGian;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCangTrungGian;
    }
}