/*
Run this script on:

        192.168.72.100.ECS_TQDT_SXXK_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_SXXK_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 08/01/2012 1:50:31 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_BKToKhaiNhap_DeleteDuplicate]'
GO

CREATE PROC p_KDT_SXXK_BKToKhaiNhap_DeleteDuplicate
(
    @MaHQ VARCHAR(10),
    @MaDN VARCHAR(10)
)
AS

BEGIN
/*
@SOFTECH
HUNGTQ, CREATED 07/27/2012.
XOA THONG TIN TO KHAI BI TRUNG.

HUONG DAN:
	THAY DOI CAC GIA TRI THAM SO BEN DUOI DE XOA THONG TIN NPL TRUNG LAP CUA TO KHAI:
		[Ma hai quan], [Ma doanh nghiep], [So to khai], [Nam dang ky], [Ma loai hinh]
*/

/*----------------------------------------------------*/
/*NGUOI SU DUNG: Thay doi gia tri cac tham so tai day.*/
--SET @MaHQ = 'N34G'
--SET @MaDN = '4000485408'
/*----------------------------------------------------*/

    DECLARE @cnt INT ,
        @r INT ,
        @ID INT ,
        @SoTK INT ,
        @MaLH VARCHAR(10) ,
        @NamDK int 


    DECLARE cur CURSOR FOR
    SELECT k.r, k.ID, k.SoToKhai, k.MaLoaiHinh, k.NamDangKy
    FROM 
    (	
		SELECT RANK() OVER(ORDER BY t_KDT_SXXK_BKToKhaiNhap.SoToKhai, t_KDT_SXXK_BKToKhaiNhap.ID) AS r, t_KDT_SXXK_BKToKhaiNhap.* 
		FROM t_KDT_SXXK_BKToKhaiNhap,
		(
			SELECT COUNT(*) as cnt,BangKeHoSoThanhLy_ID, SoToKhai, MaLoaiHinh, NamDangKy, MaHaiQuan, NgayDangKy, UserName 
			FROM dbo.t_KDT_SXXK_BKToKhaiNhap
			WHERE MaHaiQuan = @MaHQ
			GROUP BY BangKeHoSoThanhLy_ID, SoToKhai, MaLoaiHinh, NamDangKy, MaHaiQuan, NgayDangKy, UserName
			HAVING COUNT(*) > 1
		) AS v
		WHERE t_KDT_SXXK_BKToKhaiNhap.SoToKhai = v.SoToKhai
		AND t_KDT_SXXK_BKToKhaiNhap.MaLoaiHinh = v.MaLoaiHinh
		AND t_KDT_SXXK_BKToKhaiNhap.MaHaiQuan = v.MaHaiQuan
		AND t_KDT_SXXK_BKToKhaiNhap.NamDangKy = v.NamDangKy
    ) AS k


    BEGIN TRANSACTION T1 ;

    SET @cnt = 0

    OPEN cur 
    FETCH NEXT FROM cur INTO @r ,@ID, @SoTK, @MaLH, @NamDK
	--Fetch next record
    WHILE @@FETCH_STATUS = 0 
        BEGIN
            SET @cnt = @cnt + 1
            PRINT STR(@cnt) + ' - ' + STR(@r) + ' - ' + STR(@ID) + ' - ' + STR(@SoTK) + ' - ' + @MaLH + ' - ' + @MaHQ + ' - ' + @MaDN
			-- now move the cursor
			
            IF ( @cnt % 2 = 0 ) 
                BEGIN
                    PRINT 'xoa' + ' -  ID: ' + STR(@ID)
				
				--XOA DU LIEU
				DELETE FROM t_KDT_SXXK_BKToKhaiNhap
				WHERE t_KDT_SXXK_BKToKhaiNhap.ID = @ID
				
                END
            ELSE 
                PRINT 'khong co row nao xoa'
		
            FETCH NEXT FROM cur INTO @r ,@ID, @SoTK, @MaLH, @NamDK
        END

    CLOSE cur --Close cursor
    DEALLOCATE cur --Deallocate cursor
	
    IF @@ERROR != 0 
        BEGIN
            PRINT 'Qua trinh thuc hien xay ra loi.'
            PRINT STR(@@ERROR)
            ROLLBACK TRANSACTION T1 ;
        END
    ELSE 
        COMMIT TRANSACTION T1 ;	

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]'
GO
       
        
ALTER PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]        
@MaDoanhNghiep VARCHAR (14),        
@MaHaiQuan char(6),        
@TuNgay datetime,        
@DenNgay datetime        
AS      
SELECT         
  A.MaHaiQuan,        
     A.SoToKhai,        
  A.MaLoaiHinh,        
  A.NamDangKy,        
  A.NgayDangKy,        
  A.NGAY_THN_THX,       
  A.NgayHoanThanh        
  ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.             
FROM t_SXXK_ToKhaiMauDich A        
 LEFT JOIN (        
   SELECT DISTINCT        
    SoToKhai,        
    MaLoaiHinh,        
    NamDangKy,        
    MaHaiQuan        
   FROM t_KDT_SXXK_BKToKhaiXuat        
   WHERE        
    MaHaiQuan = @MaHaiQuan) D ON        
  A.SoToKhai=D.SoToKhai AND        
  A.MaLoaiHinh=D.MaLoaiHinh AND        
  A.NamDangKy=D.NamDangKy AND        
  A.MaHaiQuan=D.MaHaiQuan      
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.     
  LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy)       
WHERE        
 A.MaDoanhNghiep = @MaDoanhNghiep AND        
 A.MaHaiQuan = @MaHaiQuan AND        
 (A.MaLoaiHinh LIKE 'XSX%' OR A.MaLoaiHinh LIKE 'XGC%')        
 AND A.NgayDangKy between @TuNgay and @DenNgay        
 AND (A.NGAY_THN_THX IS NOT NULL OR year(A.NGAY_THN_THX)!=1900 )        
 AND D.SoToKhai IS NULL       
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.       
 AND CAST(a.SoToKhai AS VARCHAR(50)) + CAST(a.MaLoaiHinh AS VARCHAR(50)) + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (SELECT  CAST(SoToKhai AS VARCHAR(50)) + CAST(MaLoaiHinh AS VARCHAR(50)) + CAST(YEAR(NgayDangKy) AS VARCHAR(50)) FROM dbo.t_KDT_ToKhaiMauDich WHERE MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep AND TrangThaiXuLy != 1 AND SoToKhai != 0)           
ORDER BY A.NgayDangKy , A.SoToKhai, A.MaLoaiHinh      
      
/* Comment Script      
      
SELECT         
 A.MaHaiQuan,        
 A.SoToKhai,        
 A.MaLoaiHinh,        
 A.NamDangKy,        
 A.NgayDangKy,        
 A.NGAY_THN_THX        
FROM       
(      
 --HungTQ Updated 22/06/2011: Bo sung loc du lieu theo TrangThaiXuLy = 1      
 SELECT   DISTINCT      
  A.MaHaiQuan,       
  A.MaDoanhNghiep,       
  A.SoToKhai,        
  A.MaLoaiHinh,        
  A.NamDangKy,        
  A.NgayDangKy,        
  A.NGAY_THN_THX, tkmd.TrangThaiXuLy       
 FROM t_SXXK_ToKhaiMauDich A  LEFT JOIN  dbo.t_KDT_ToKhaiMauDich tkmd      
 ON a.SoToKhai = tkmd.SoToKhai AND a.MaLoaiHinh = tkmd.MaLoaiHinh AND a.NamDangKy = YEAR(tkmd.NgayDangKy) AND A.MaHaiQuan = tkmd.MaHaiQuan AND A.MaDoanhNghiep = tkmd.MaDoanhNghiep      
) A        
 LEFT JOIN (        
   SELECT DISTINCT        
    SoToKhai,        
    MaLoaiHinh,        
    NamDangKy,        
    MaHaiQuan        
   FROM t_KDT_SXXK_BKToKhaiXuat        
   WHERE        
    MaHaiQuan = @MaHaiQuan) D ON        
  A.SoToKhai=D.SoToKhai AND        
  A.MaLoaiHinh=D.MaLoaiHinh AND        
  A.NamDangKy=D.NamDangKy AND        
  A.MaHaiQuan=D.MaHaiQuan        
WHERE        
 A.MaDoanhNghiep = @MaDoanhNghiep AND        
 A.MaHaiQuan = @MaHaiQuan AND        
 (A.MaLoaiHinh LIKE 'XSX%' OR A.MaLoaiHinh LIKE 'XGC%')        
 AND A.NgayDangKy between @TuNgay and @DenNgay        
 AND (A.NGAY_THN_THX IS NOT NULL OR year(A.NGAY_THN_THX)!=1900 )        
 AND D.SoToKhai IS NULL         
 AND A.TrangThaiXuLy = 1      
         
ORDER BY A.NgayDangKy , A.SoToKhai, A.MaLoaiHinh        
*/        
        
        
        
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_GetDSTKNKhongTheThanhKhoanDate]'
GO
CREATE PROCEDURE [dbo].[p_SXXK_GetDSTKNKhongTheThanhKhoanDate]
    @MaDoanhNghiep VARCHAR(14) ,
    @MaHaiQuan CHAR(6) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME ,
    @UserName VARCHAR(50) ,
    @TenChuHang NVARCHAR(200)
AS 
    SELECT  A.MaHaiQuan ,
            A.SoToKhai ,
            A.MaLoaiHinh ,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.        
            ,
            ISNULL(e.TrangThaiXuLy, 404) AS TrangThaiXuLy
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiNhap
                        WHERE   UserName != @UserName
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan  
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.   
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            AND A.MaHaiQuan = @MaHaiQuan
            AND                
-- D.SoToKhai IS NULL AND                
            A.MaLoaiHinh LIKE 'N%'
            AND A.TenChuHang LIKE '%' + @TenChuHang + '%'
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND A.THANHLY <> 'H'       
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy.     
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   MaHaiQuan = @MaHaiQuan
                    AND MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy = 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy DESC ,
            A.SoToKhai ,
            A.MaLoaiHinh  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_GetDSTKXKhongTheThanhKhoanDate]'
GO
       
CREATE PROCEDURE [dbo].[p_SXXK_GetDSTKXKhongTheThanhKhoanDate]
    @MaDoanhNghiep VARCHAR(14) ,
    @MaHaiQuan CHAR(6) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME
AS 
    SELECT  A.MaHaiQuan ,
            A.SoToKhai ,
            A.MaLoaiHinh ,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            e.PhanLuong , --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.             
            ISNULL(e.TrangThaiXuLy, 404) AS TrangThaiXuLy
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiXuat
                        WHERE   MaHaiQuan = @MaHaiQuan
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan      
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.     
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            AND A.MaHaiQuan = @MaHaiQuan
            AND ( A.MaLoaiHinh LIKE 'XSX%'
                  OR A.MaLoaiHinh LIKE 'XGC%'
                )
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND ( A.NGAY_THN_THX IS NOT NULL
                  OR YEAR(A.NGAY_THN_THX) != 1900
                )
            AND D.SoToKhai IS NULL
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho huy, sua to khai, to khai sua cho duyet, khong phe duyet.       
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   MaHaiQuan = @MaHaiQuan
                    AND MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy = 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy ,
            A.SoToKhai ,
            A.MaLoaiHinh      
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='3.6', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('3.6', getdate(), null)
	end 