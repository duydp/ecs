--USE [ECS_TQDT_KD_V3]
GO

/****** Object:  Table [dbo].[t_GopY_CauHoiDoanhNghiep]    Script Date: 09/28/2012 11:12:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_GopY_CauHoiDoanhNghiep](
	[CauHoiID] [bigint] IDENTITY(1,1) NOT NULL,
	[MaDonVi] [nvarchar](50) NOT NULL,
	[TenDonVi] [nvarchar](250) NOT NULL,
	[LinhVuc] [varchar](50) NOT NULL,
	[Phanloai] [varchar](50) NOT NULL,
	[TieuDe] [nvarchar](250) NOT NULL,
	[NoiDungCauHoi] [nvarchar](max) NOT NULL,
	[NgayGui] [datetime] NOT NULL,
	[CauHoiTraLoiID] [bigint] NULL,
	[NgayTraLoi] [datetime] NULL,
 CONSTRAINT [PK_t_Feedback_Question] PRIMARY KEY CLUSTERED 
(
	[CauHoiID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

--USE [ECS_TQDT_KD_V3]
GO

/****** Object:  Table [dbo].[t_GopY_LinhVuc]    Script Date: 09/28/2012 11:12:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_GopY_LinhVuc](
	[Ma] [varchar](50) NOT NULL,
	[Ten] [nvarchar](250) NOT NULL,
	[HienThi] [bit] NOT NULL,
	[MacDinh] [bit] NOT NULL,
	[ThuTu] [int] NOT NULL,
 CONSTRAINT [PK_t_Feedback_LinhVuc] PRIMARY KEY CLUSTERED 
(
	[Ma] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

--USE [ECS_TQDT_KD_V3]
GO

/****** Object:  Table [dbo].[t_GopY_ThuVienCauHoi]    Script Date: 09/28/2012 11:12:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_GopY_ThuVienCauHoi](
	[MaCauHoi] [varchar](50) NOT NULL,
	[LinhVuc] [varchar](50) NOT NULL,
	[Phanloai] [varchar](50) NOT NULL,
	[TieuDe] [nvarchar](250) NOT NULL,
	[NoiDungCauHoi] [nvarchar](max) NOT NULL,
	[TraLoi] [nvarchar](max) NOT NULL,
	[NgayTao] [datetime] NOT NULL,
	[NgayCapNhat] [datetime] NOT NULL,
	[HienThi] [bit] NOT NULL,
	[GhiChu] [nvarchar](255) NULL,
 CONSTRAINT [PK_t_GopY_ThuVienCauHoi_1] PRIMARY KEY CLUSTERED 
(
	[MaCauHoi] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_GopY_CauHoiDoanhNghiep]  WITH CHECK ADD  CONSTRAINT [FK_t_Feedback_CustomerQuestion_t_Feedback_LinhVuc] FOREIGN KEY([LinhVuc])
REFERENCES [dbo].[t_GopY_LinhVuc] ([Ma])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[t_GopY_CauHoiDoanhNghiep] CHECK CONSTRAINT [FK_t_Feedback_CustomerQuestion_t_Feedback_LinhVuc]
GO

ALTER TABLE [dbo].[t_GopY_LinhVuc] ADD  CONSTRAINT [DF_t_Feedback_LinhVuc_HienThi]  DEFAULT ((1)) FOR [HienThi]
GO

ALTER TABLE [dbo].[t_GopY_LinhVuc] ADD  CONSTRAINT [DF_t_Feedback_LinhVuc_MacDinh]  DEFAULT ((0)) FOR [MacDinh]
GO

ALTER TABLE [dbo].[t_GopY_LinhVuc] ADD  CONSTRAINT [DF_t_Feedback_LinhVuc_ThuTu]  DEFAULT ((0)) FOR [ThuTu]
GO

ALTER TABLE [dbo].[t_GopY_ThuVienCauHoi]  WITH CHECK ADD  CONSTRAINT [FK_t_Feedback_QuestionAnswer_t_Feedback_LinhVuc] FOREIGN KEY([LinhVuc])
REFERENCES [dbo].[t_GopY_LinhVuc] ([Ma])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[t_GopY_ThuVienCauHoi] CHECK CONSTRAINT [FK_t_Feedback_QuestionAnswer_t_Feedback_LinhVuc]
GO

ALTER TABLE [dbo].[t_GopY_ThuVienCauHoi] ADD  CONSTRAINT [DF_t_Feedback_QuestionAnswer_HienThi]  DEFAULT ((1)) FOR [HienThi]
GO


