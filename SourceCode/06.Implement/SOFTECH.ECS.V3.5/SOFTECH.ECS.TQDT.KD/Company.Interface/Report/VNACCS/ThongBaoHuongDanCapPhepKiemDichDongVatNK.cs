using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class ThongBaoHuongDanCapPhepKiemDichDongVatNK : DevExpress.XtraReports.UI.XtraReport
    {
        public ThongBaoHuongDanCapPhepKiemDichDongVatNK()
        {
            InitializeComponent();
        }
        public void BindingReport(VAHP060 vahp)
        {
            lblSoDonXinCapPhep.Text = vahp.APN.GetValue().ToString().ToUpper();
            DataTable dt = ConvertListToTable(vahp.D01, 10);
            BindingReportHang(dt);
        }
        private DataTable ConvertListToTable(GroupAttribute group, int loop)
        {
            int STT = 0;
            DataTable gr = new DataTable();
            foreach (PropertiesAttribute attribute in group.listAttribute)
            {
                gr.Columns.Add(attribute.GroupID, attribute.OfType == typeof(int) ? typeof(decimal) : attribute.OfType);
            }
            gr.Columns.Add("STT", typeof(string));
            for (int i = 0; i < loop; i++)
            {
                STT++;
                DataRow dr = gr.NewRow();
                foreach (PropertiesAttribute attribute in group.listAttribute)
                {

                    if (attribute.OfType == typeof(DateTime))
                    {
                        if (System.Convert.ToDateTime(attribute.GetValueCollection(i)).Year > 1900)
                            dr[attribute.GroupID] = attribute.GetValueCollection(i);
                    }
                    else if (attribute.OfType == typeof(decimal) || attribute.OfType == typeof(int))
                    {
                        if (System.Convert.ToDecimal(attribute.GetValueCollection(i)) != 0)
                            dr[attribute.GroupID] = attribute.GetValueCollection(i);
                    }
                    else
                        dr[attribute.GroupID] = attribute.GetValueCollection(i);

                }
                dr["STT"] = STT.ToString().ToUpper();
                gr.Rows.Add(dr);
            }
            return gr;
        }
        public void BindingReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblNgayChiThi.DataBindings.Add("Text", DetailReport.DataSource, "VAHP060_D01");
            lblTenChiThi.DataBindings.Add("Text", DetailReport.DataSource, "VAHP060_T01");
            lblNoiDungChiThi.DataBindings.Add("Text", DetailReport.DataSource, "VAHP060_C01");
        }
    }
}
