-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_SelectBy_Contract_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_SelectBy_Contract_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_DeleteBy_Contract_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_DeleteBy_Contract_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Insert]
	@Contract_ID bigint,
	@ID_HopDong bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@MaHS numeric(12, 0),
	@GhiChu nvarchar(2000),
	@LuongTamNhap numeric(18, 4),
	@LuongTaiXuat numeric(18, 4),
	@LuongChuyenTiep numeric(18, 4),
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQ nvarchar(6),
	@NgayHetHan datetime,
	@LuongConLai numeric(18, 4),
	@DVT nvarchar(4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Details]
(
	[Contract_ID],
	[ID_HopDong],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[MaHS],
	[GhiChu],
	[LuongTamNhap],
	[LuongTaiXuat],
	[LuongChuyenTiep],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[LuongConLai],
	[DVT]
)
VALUES 
(
	@Contract_ID,
	@ID_HopDong,
	@STT,
	@TenHangHoa,
	@MaHangHoa,
	@MaHS,
	@GhiChu,
	@LuongTamNhap,
	@LuongTaiXuat,
	@LuongChuyenTiep,
	@SoHopDong,
	@NgayHopDong,
	@MaHQ,
	@NgayHetHan,
	@LuongConLai,
	@DVT
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Update]
	@ID bigint,
	@Contract_ID bigint,
	@ID_HopDong bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@MaHS numeric(12, 0),
	@GhiChu nvarchar(2000),
	@LuongTamNhap numeric(18, 4),
	@LuongTaiXuat numeric(18, 4),
	@LuongChuyenTiep numeric(18, 4),
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQ nvarchar(6),
	@NgayHetHan datetime,
	@LuongConLai numeric(18, 4),
	@DVT nvarchar(4)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Details]
SET
	[Contract_ID] = @Contract_ID,
	[ID_HopDong] = @ID_HopDong,
	[STT] = @STT,
	[TenHangHoa] = @TenHangHoa,
	[MaHangHoa] = @MaHangHoa,
	[MaHS] = @MaHS,
	[GhiChu] = @GhiChu,
	[LuongTamNhap] = @LuongTamNhap,
	[LuongTaiXuat] = @LuongTaiXuat,
	[LuongChuyenTiep] = @LuongChuyenTiep,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[MaHQ] = @MaHQ,
	[NgayHetHan] = @NgayHetHan,
	[LuongConLai] = @LuongConLai,
	[DVT] = @DVT
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_InsertUpdate]
	@ID bigint,
	@Contract_ID bigint,
	@ID_HopDong bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@MaHS numeric(12, 0),
	@GhiChu nvarchar(2000),
	@LuongTamNhap numeric(18, 4),
	@LuongTaiXuat numeric(18, 4),
	@LuongChuyenTiep numeric(18, 4),
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQ nvarchar(6),
	@NgayHetHan datetime,
	@LuongConLai numeric(18, 4),
	@DVT nvarchar(4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Details] 
		SET
			[Contract_ID] = @Contract_ID,
			[ID_HopDong] = @ID_HopDong,
			[STT] = @STT,
			[TenHangHoa] = @TenHangHoa,
			[MaHangHoa] = @MaHangHoa,
			[MaHS] = @MaHS,
			[GhiChu] = @GhiChu,
			[LuongTamNhap] = @LuongTamNhap,
			[LuongTaiXuat] = @LuongTaiXuat,
			[LuongChuyenTiep] = @LuongChuyenTiep,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[MaHQ] = @MaHQ,
			[NgayHetHan] = @NgayHetHan,
			[LuongConLai] = @LuongConLai,
			[DVT] = @DVT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Details]
		(
			[Contract_ID],
			[ID_HopDong],
			[STT],
			[TenHangHoa],
			[MaHangHoa],
			[MaHS],
			[GhiChu],
			[LuongTamNhap],
			[LuongTaiXuat],
			[LuongChuyenTiep],
			[SoHopDong],
			[NgayHopDong],
			[MaHQ],
			[NgayHetHan],
			[LuongConLai],
			[DVT]
		)
		VALUES 
		(
			@Contract_ID,
			@ID_HopDong,
			@STT,
			@TenHangHoa,
			@MaHangHoa,
			@MaHS,
			@GhiChu,
			@LuongTamNhap,
			@LuongTaiXuat,
			@LuongChuyenTiep,
			@SoHopDong,
			@NgayHopDong,
			@MaHQ,
			@NgayHetHan,
			@LuongConLai,
			@DVT
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_DeleteBy_Contract_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_DeleteBy_Contract_ID]
	@Contract_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Details]
WHERE
	[Contract_ID] = @Contract_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Contract_ID],
	[ID_HopDong],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[MaHS],
	[GhiChu],
	[LuongTamNhap],
	[LuongTaiXuat],
	[LuongChuyenTiep],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[LuongConLai],
	[DVT]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_SelectBy_Contract_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_SelectBy_Contract_ID]
	@Contract_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Contract_ID],
	[ID_HopDong],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[MaHS],
	[GhiChu],
	[LuongTamNhap],
	[LuongTaiXuat],
	[LuongChuyenTiep],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[LuongConLai],
	[DVT]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Details]
WHERE
	[Contract_ID] = @Contract_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Contract_ID],
	[ID_HopDong],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[MaHS],
	[GhiChu],
	[LuongTamNhap],
	[LuongTaiXuat],
	[LuongChuyenTiep],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[LuongConLai],
	[DVT]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail_SelectAll]

















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Contract_ID],
	[ID_HopDong],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[MaHS],
	[GhiChu],
	[LuongTamNhap],
	[LuongTaiXuat],
	[LuongChuyenTiep],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[LuongConLai],
	[DVT]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectBy_GoodItems]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectBy_GoodItems]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_DeleteBy_GoodItems]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_DeleteBy_GoodItems]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Insert]
	@GoodItems bigint,
	@STT numeric(5, 0),
	@ID_HopDong bigint,
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQ nvarchar(6),
	@NgayHetHan datetime,
	@GhiChuKhac nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details]
(
	[GoodItems],
	[STT],
	[ID_HopDong],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[GhiChuKhac]
)
VALUES 
(
	@GoodItems,
	@STT,
	@ID_HopDong,
	@SoHopDong,
	@NgayHopDong,
	@MaHQ,
	@NgayHetHan,
	@GhiChuKhac
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Update]
	@ID bigint,
	@GoodItems bigint,
	@STT numeric(5, 0),
	@ID_HopDong bigint,
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQ nvarchar(6),
	@NgayHetHan datetime,
	@GhiChuKhac nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details]
SET
	[GoodItems] = @GoodItems,
	[STT] = @STT,
	[ID_HopDong] = @ID_HopDong,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[MaHQ] = @MaHQ,
	[NgayHetHan] = @NgayHetHan,
	[GhiChuKhac] = @GhiChuKhac
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_InsertUpdate]
	@ID bigint,
	@GoodItems bigint,
	@STT numeric(5, 0),
	@ID_HopDong bigint,
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQ nvarchar(6),
	@NgayHetHan datetime,
	@GhiChuKhac nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details] 
		SET
			[GoodItems] = @GoodItems,
			[STT] = @STT,
			[ID_HopDong] = @ID_HopDong,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[MaHQ] = @MaHQ,
			[NgayHetHan] = @NgayHetHan,
			[GhiChuKhac] = @GhiChuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details]
		(
			[GoodItems],
			[STT],
			[ID_HopDong],
			[SoHopDong],
			[NgayHopDong],
			[MaHQ],
			[NgayHetHan],
			[GhiChuKhac]
		)
		VALUES 
		(
			@GoodItems,
			@STT,
			@ID_HopDong,
			@SoHopDong,
			@NgayHopDong,
			@MaHQ,
			@NgayHetHan,
			@GhiChuKhac
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_DeleteBy_GoodItems]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_DeleteBy_GoodItems]
	@GoodItems bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details]
WHERE
	[GoodItems] = @GoodItems

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItems],
	[STT],
	[ID_HopDong],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectBy_GoodItems]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectBy_GoodItems]
	@GoodItems bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItems],
	[STT],
	[ID_HopDong],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details]
WHERE
	[GoodItems] = @GoodItems

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GoodItems],
	[STT],
	[ID_HopDong],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[GhiChuKhac]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectAll]









AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItems],
	[STT],
	[ID_HopDong],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@NamQuyetToan numeric(4, 0),
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[NamQuyetToan],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@NamQuyetToan,
	@GhiChuKhac,
	@FileName,
	@Content,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@NamQuyetToan numeric(4, 0),
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[NamQuyetToan] = @NamQuyetToan,
	[GhiChuKhac] = @GhiChuKhac,
	[FileName] = @FileName,
	[Content] = @Content,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@NamQuyetToan numeric(4, 0),
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[NamQuyetToan] = @NamQuyetToan,
			[GhiChuKhac] = @GhiChuKhac,
			[FileName] = @FileName,
			[Content] = @Content,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[NamQuyetToan],
			[GhiChuKhac],
			[FileName],
			[Content],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@NamQuyetToan,
			@GhiChuKhac,
			@FileName,
			@Content,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[NamQuyetToan],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[NamQuyetToan],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[NamQuyetToan],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectBy_GoodItems_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectBy_GoodItems_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_DeleteBy_GoodItems_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_DeleteBy_GoodItems_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Insert]
	@GoodItems_ID bigint,
	@STT numeric(5, 0),
	@LoaiHangHoa numeric(5, 0),
	@TaiKhoan numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@LuongNLVTXuatKhau numeric(18, 4),
	@LuongNLVTTon numeric(18, 4),
	@LuongSPGCNhapKhau numeric(18, 4),
	@LuongSPGCBan numeric(18, 4),
	@LuongTieuHuy numeric(18, 4),
	@LuongBan numeric(18, 4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details]
(
	[GoodItems_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[LuongNLVTXuatKhau],
	[LuongNLVTTon],
	[LuongSPGCNhapKhau],
	[LuongSPGCBan],
	[LuongTieuHuy],
	[LuongBan]
)
VALUES 
(
	@GoodItems_ID,
	@STT,
	@LoaiHangHoa,
	@TaiKhoan,
	@TenHangHoa,
	@MaHangHoa,
	@DVT,
	@LuongNLVTXuatKhau,
	@LuongNLVTTon,
	@LuongSPGCNhapKhau,
	@LuongSPGCBan,
	@LuongTieuHuy,
	@LuongBan
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Update]
	@ID bigint,
	@GoodItems_ID bigint,
	@STT numeric(5, 0),
	@LoaiHangHoa numeric(5, 0),
	@TaiKhoan numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@LuongNLVTXuatKhau numeric(18, 4),
	@LuongNLVTTon numeric(18, 4),
	@LuongSPGCNhapKhau numeric(18, 4),
	@LuongSPGCBan numeric(18, 4),
	@LuongTieuHuy numeric(18, 4),
	@LuongBan numeric(18, 4)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details]
SET
	[GoodItems_ID] = @GoodItems_ID,
	[STT] = @STT,
	[LoaiHangHoa] = @LoaiHangHoa,
	[TaiKhoan] = @TaiKhoan,
	[TenHangHoa] = @TenHangHoa,
	[MaHangHoa] = @MaHangHoa,
	[DVT] = @DVT,
	[LuongNLVTXuatKhau] = @LuongNLVTXuatKhau,
	[LuongNLVTTon] = @LuongNLVTTon,
	[LuongSPGCNhapKhau] = @LuongSPGCNhapKhau,
	[LuongSPGCBan] = @LuongSPGCBan,
	[LuongTieuHuy] = @LuongTieuHuy,
	[LuongBan] = @LuongBan
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_InsertUpdate]
	@ID bigint,
	@GoodItems_ID bigint,
	@STT numeric(5, 0),
	@LoaiHangHoa numeric(5, 0),
	@TaiKhoan numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@LuongNLVTXuatKhau numeric(18, 4),
	@LuongNLVTTon numeric(18, 4),
	@LuongSPGCNhapKhau numeric(18, 4),
	@LuongSPGCBan numeric(18, 4),
	@LuongTieuHuy numeric(18, 4),
	@LuongBan numeric(18, 4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details] 
		SET
			[GoodItems_ID] = @GoodItems_ID,
			[STT] = @STT,
			[LoaiHangHoa] = @LoaiHangHoa,
			[TaiKhoan] = @TaiKhoan,
			[TenHangHoa] = @TenHangHoa,
			[MaHangHoa] = @MaHangHoa,
			[DVT] = @DVT,
			[LuongNLVTXuatKhau] = @LuongNLVTXuatKhau,
			[LuongNLVTTon] = @LuongNLVTTon,
			[LuongSPGCNhapKhau] = @LuongSPGCNhapKhau,
			[LuongSPGCBan] = @LuongSPGCBan,
			[LuongTieuHuy] = @LuongTieuHuy,
			[LuongBan] = @LuongBan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details]
		(
			[GoodItems_ID],
			[STT],
			[LoaiHangHoa],
			[TaiKhoan],
			[TenHangHoa],
			[MaHangHoa],
			[DVT],
			[LuongNLVTXuatKhau],
			[LuongNLVTTon],
			[LuongSPGCNhapKhau],
			[LuongSPGCBan],
			[LuongTieuHuy],
			[LuongBan]
		)
		VALUES 
		(
			@GoodItems_ID,
			@STT,
			@LoaiHangHoa,
			@TaiKhoan,
			@TenHangHoa,
			@MaHangHoa,
			@DVT,
			@LuongNLVTXuatKhau,
			@LuongNLVTTon,
			@LuongSPGCNhapKhau,
			@LuongSPGCBan,
			@LuongTieuHuy,
			@LuongBan
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_DeleteBy_GoodItems_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_DeleteBy_GoodItems_ID]
	@GoodItems_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details]
WHERE
	[GoodItems_ID] = @GoodItems_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItems_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[LuongNLVTXuatKhau],
	[LuongNLVTTon],
	[LuongSPGCNhapKhau],
	[LuongSPGCBan],
	[LuongTieuHuy],
	[LuongBan]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectBy_GoodItems_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectBy_GoodItems_ID]
	@GoodItems_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItems_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[LuongNLVTXuatKhau],
	[LuongNLVTTon],
	[LuongSPGCNhapKhau],
	[LuongSPGCBan],
	[LuongTieuHuy],
	[LuongBan]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details]
WHERE
	[GoodItems_ID] = @GoodItems_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GoodItems_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[LuongNLVTXuatKhau],
	[LuongNLVTTon],
	[LuongSPGCNhapKhau],
	[LuongSPGCBan],
	[LuongTieuHuy],
	[LuongBan]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 25, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Detail_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItems_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[LuongNLVTXuatKhau],
	[LuongNLVTTon],
	[LuongSPGCNhapKhau],
	[LuongSPGCBan],
	[LuongTieuHuy],
	[LuongBan]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NLVTTP_Details]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '24.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('24.4',GETDATE(), N'CẬP NHẬT KHAI BÁO BÁO CÁO QUYẾT TOÁN THIẾT BỊ')
END