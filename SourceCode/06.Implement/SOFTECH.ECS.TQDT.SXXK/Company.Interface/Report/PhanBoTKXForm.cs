﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace Company.Interface.Report
{
   
    public partial class PhanBoTKXForm : BaseForm
    {
        private short NamDangKy;
        private string MaHaiQuan;
        PhanBoTKXReport report;
        public PhanBoTKXForm()
        {
            InitializeComponent();
        }

        private void PhanBoTKNForm_Load(object sender, EventArgs e)
        {
            if (GlobalSettings.GiaoDien.Id == "Office2003")
                this.defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            
        }

        private void txtToKhaiNhap_ButtonClick(object sender, EventArgs e)
        {
            ToKhaiXuatDaPhanBoForm f = new ToKhaiXuatDaPhanBoForm();
            f.ShowDialog();
            txtToKhaiXuat.Text = f.TKMD.SoToKhai + "";
            txtLoaiHinhXuat.Text = f.TKMD.MaLoaiHinh;
            txtNamDKXuat.Text = f.TKMD.NamDangKy + "";
            this.MaHaiQuan = f.TKMD.MaHaiQuan;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (cvError.IsValid)
            {
                this.report = new PhanBoTKXReport();
                report.SoToKhai = Convert.ToInt32(txtToKhaiXuat.Text);
                report.NamDangKy = Convert.ToInt16(txtNamDKXuat.Text);
                report.MaHaiQuan = this.MaHaiQuan.Trim();
                report.MaLoaiHinh = txtLoaiHinhXuat.Text;
                report.BindReport();
                printControl1.PrintingSystem = this.report.PrintingSystem;
                this.report.CreateDocument();

            }
        }

    


    }
}