﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class LoadingLocation
    {
        /// <summary>
        /// Mã địa điểm xếp hàng (Khu vực chịu sự giám sát Hải quan)
        /// </summary>
        [XmlElement("customsCode")]
        public string CustomsCode { get; set; }

        /// <summary>
        /// Tên địa điểm xếp hàng (Khu vực chịu sự giám sát Hải quan)
        /// </summary>
        [XmlElement("customsName")]
        public string CustomsName { get; set; }

        /// <summary>
        /// Mã vị trí xếp hàng (Nơi chất hàng): 
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// Mã cảng/cửa khẩu/ga xếp hàng
        /// </summary>
        [XmlElement("portCode")]
        public string PortCode { get; set; }
    }
}
