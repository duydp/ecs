﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace FixXMLDoc
{
    public partial class Form1 : Form
    {
        public static string GuidePath = AppDomain.CurrentDomain.BaseDirectory + "Help\\guide";
        private static XmlDocument docGuide;
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// minhnd Hàm tách chuỗi 04/10/2014
        /// </summary>
        /// <param name="str">Chuỗi</param>
        /// <param name="separator">Ký tự tách</param>
        /// <returns></returns>
        private string splitString(string str, char separator)
        {
            string[] words = str.Split(separator);
            str = "";
            int stt = 1;
            foreach (var word in words)
            {
                if (stt < words.Length)
                    str += word + separator + "\r\n";
                else
                    str += word;
                stt++;
            }
            return str;
        }
        private string splitString1(string str, char separator)
        {
            str.Replace("...", ".");
            string[] words = str.Split(separator);
            str = "";
            int stt = 1;
            foreach (var word in words)
            {
                if (stt < words.Length)
                    str += word + separator + "\r\n";
                else
                    str += word;
                stt++;
            }
            return str;
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = "Open XML File";
            theDialog.Filter = "XML files|*.xml";
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                txtPart.Text = theDialog.FileName.ToString();
            }
            docGuide = ReadGuideFile(theDialog.FileName);
            XmlNodeList n = docGuide.SelectNodes("/guide/item");
            int count = n.Count;
            string strfix = "";
            string text = "";
            for (int i = 1; i <= count; i++)
            {
                
                text = GetGuideString(docGuide, "no", i.ToString());
                strfix = string.Empty;
                if (text != "") 
                {
                    strfix = text;
                    strfix = splitString1(strfix, '.');
                    docGuide.SelectNodes("guide/item").Item(i-1).InnerText = strfix;
                    
                }
            }
            docGuide.Save(theDialog.FileName);
        }
        private void formatXML(string[] fileParts) 
        {
            try
            {
                for (int i = 0; i < fileParts.Length; i++)
                {
                    docGuide = ReadGuideFile(fileParts[i]);
                    int count = docGuide.SelectNodes("/guide/item").Count;
                    string strfix = "";
                    string textResult = "";
                    for (int j = 1; j <= count; j++)
                    {

                        textResult = GetGuideString(docGuide, "no", j.ToString());
                        strfix = string.Empty;
                        if (textResult != "")
                        {
                            strfix = textResult;
                            strfix = splitString(strfix, '.');
                            docGuide.SelectNodes("guide/item").Item(j - 1).InnerText = strfix;
                        }
                    }
                    docGuide.Save(fileParts[i]);
                }
                MessageBox.Show("Format successful", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                throw;
            }
        }
        // <summary>
        /// Doc file thong tin XML Guide theo Code cua Nghiep vu khai bao
        /// </summary>
        /// <param name="guideCode"></param>
        public static XmlDocument ReadGuideFile(string guideCode)
        {
            XmlDocument docGuide = new XmlDocument();
            try
            {
                string guideFile = string.Format("{0}", guideCode);

                if (System.IO.File.Exists(guideFile))
                    docGuide.Load(guideFile);
                else
                    docGuide = null;
            }
            catch (Exception ex)
            {
                //Logger.LocalLogger.Instance().WriteMessage(ex);
                docGuide = null;
            }

            return docGuide;
        }
        /// <summary>
        /// Tim lay thong tin guide theo No control trong danh sach
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetGuide(XmlDocument docGuide, string no)
        {
            return GetGuideString(docGuide, "no", no);
        }

        public static string GetGuideName(XmlDocument docGuide, string val)
        {
            XmlNode node = GetGuideNode(docGuide, "id", val);

            return node != null ? node.Attributes["name"].InnerXml : "";
        }

        private static string GetGuideString(XmlDocument docGuide, string key, string id)
        {
            if (docGuide != null)
            {
                int length = id.Length;
                StringBuilder builder = new StringBuilder(id);
                while (length > 0)
                {
                    XmlNode node = docGuide.DocumentElement.SelectSingleNode("item[@" + key + "='" + builder.ToString() + "']");
                    if (node != null)
                    {
                        return node.InnerText;
                    }
                    builder[--length] = '_';
                }
            }
            return "";
        }

        private static XmlNode GetGuideNode(XmlDocument docGuide, string key, string id)
        {
            if (docGuide != null)
            {
                int length = id.Length;
                StringBuilder builder = new StringBuilder(id);
                while (length > 0)
                {
                    XmlNode node = docGuide.DocumentElement.SelectSingleNode("item[@" + key + "='" + builder.ToString() + "']");
                    if (node != null)
                    {
                        return node;
                    }
                    builder[--length] = '_';
                }
            }
            return null;
        }

        private void btnBrowseAll_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtPart.Text = folderBrowserDialog1.SelectedPath.ToString();
            }
            //theDialog.Filter = "XML files|*.xml";
            
            if (txtPart.Text != "") 
            {
                string[] fileParts = Directory.GetFiles(string.Format(@"{0}", folderBrowserDialog1.SelectedPath), "*.xml");
                string files = "";
                for (int i = 0; i < fileParts.Length; i++)
                {
                    files += fileParts[i] + "\r\n";
                }
                txtFix.Text = files;
                formatXML(fileParts);
            }
            
        }
    }
}
