using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_Cargo : ICloneable
	{
		#region Properties.
		
		public string ResultCode { set; get; }
		public int PGNumber { set; get; }
		public string TableID { set; get; }
		public string ProcessClassification { set; get; }
		public int CreatorClassification { set; get; }
		public string NumberOfKeyItems { set; get; }
		public string BondedAreaCode { set; get; }
		public string UserCode { set; get; }
		public string BondedAreaName { set; get; }
		public string BondedAreaDemarcation { set; get; }
		public int NecessityIndicationOfBondedAreaName { set; get; }
		public string CustomsOfficeCodeForImportDeclaration { set; get; }
		public string CustomsOfficeCodeForExportDeclaration { set; get; }
		public string CustomsOfficeCodeForDeclarationOnTransportation { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<VNACC_Category_Cargo> ConvertToCollection(IDataReader reader)
		{
			List<VNACC_Category_Cargo> collection = new List<VNACC_Category_Cargo>();
			while (reader.Read())
			{
				VNACC_Category_Cargo entity = new VNACC_Category_Cargo();
				if (!reader.IsDBNull(reader.GetOrdinal("ResultCode"))) entity.ResultCode = reader.GetString(reader.GetOrdinal("ResultCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("PGNumber"))) entity.PGNumber = reader.GetInt32(reader.GetOrdinal("PGNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProcessClassification"))) entity.ProcessClassification = reader.GetString(reader.GetOrdinal("ProcessClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatorClassification"))) entity.CreatorClassification = reader.GetInt32(reader.GetOrdinal("CreatorClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("NumberOfKeyItems"))) entity.NumberOfKeyItems = reader.GetString(reader.GetOrdinal("NumberOfKeyItems"));
				if (!reader.IsDBNull(reader.GetOrdinal("BondedAreaCode"))) entity.BondedAreaCode = reader.GetString(reader.GetOrdinal("BondedAreaCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserCode"))) entity.UserCode = reader.GetString(reader.GetOrdinal("UserCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("BondedAreaName"))) entity.BondedAreaName = reader.GetString(reader.GetOrdinal("BondedAreaName"));
				if (!reader.IsDBNull(reader.GetOrdinal("BondedAreaDemarcation"))) entity.BondedAreaDemarcation = reader.GetString(reader.GetOrdinal("BondedAreaDemarcation"));
				if (!reader.IsDBNull(reader.GetOrdinal("NecessityIndicationOfBondedAreaName"))) entity.NecessityIndicationOfBondedAreaName = reader.GetInt32(reader.GetOrdinal("NecessityIndicationOfBondedAreaName"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomsOfficeCodeForImportDeclaration"))) entity.CustomsOfficeCodeForImportDeclaration = reader.GetString(reader.GetOrdinal("CustomsOfficeCodeForImportDeclaration"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomsOfficeCodeForExportDeclaration"))) entity.CustomsOfficeCodeForExportDeclaration = reader.GetString(reader.GetOrdinal("CustomsOfficeCodeForExportDeclaration"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomsOfficeCodeForDeclarationOnTransportation"))) entity.CustomsOfficeCodeForDeclarationOnTransportation = reader.GetString(reader.GetOrdinal("CustomsOfficeCodeForDeclarationOnTransportation"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
        public static List<VNACC_Category_Cargo> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<VNACC_Category_Cargo> collection = new List<VNACC_Category_Cargo>();
            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                VNACC_Category_Cargo entity = new VNACC_Category_Cargo();
                entity.ResultCode = dataSet.Tables[0].Rows[i]["ResultCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ResultCode"].ToString() : String.Empty;
                entity.PGNumber = dataSet.Tables[0].Rows[i]["PGNumber"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["PGNumber"].ToString()) : 0;
                entity.TableID = dataSet.Tables[0].Rows[i]["TableID"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["TableID"].ToString() : "A202A";
                entity.ProcessClassification = dataSet.Tables[0].Rows[i]["ProcessClassification"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ProcessClassification"].ToString() : String.Empty;
                entity.CreatorClassification = dataSet.Tables[0].Rows[i]["CreatorClassification"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["CreatorClassification"].ToString()) : 0;
                entity.NumberOfKeyItems = dataSet.Tables[0].Rows[i]["NumberOfKeyItems"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["NumberOfKeyItems"].ToString() : String.Empty;
                entity.BondedAreaCode = dataSet.Tables[0].Rows[i]["BondedAreaCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["BondedAreaCode"].ToString() : String.Empty;
                entity.UserCode = dataSet.Tables[0].Rows[i]["UserCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["UserCode"].ToString() : String.Empty;
                entity.BondedAreaName = dataSet.Tables[0].Rows[i]["BondedAreaName"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["BondedAreaName"].ToString() : String.Empty;
                entity.BondedAreaDemarcation = dataSet.Tables[0].Rows[i]["BondedAreaDemarcation"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["BondedAreaDemarcation"].ToString() : String.Empty;
                entity.NecessityIndicationOfBondedAreaName = dataSet.Tables[0].Rows[i]["NecessityIndicationOfBondedAreaName"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["NecessityIndicationOfBondedAreaName"].ToString()) : 0;
                entity.CustomsOfficeCodeForImportDeclaration = dataSet.Tables[0].Rows[i]["CustomsOfficeCodeForImportDeclaration"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["CustomsOfficeCodeForImportDeclaration"].ToString() : String.Empty;
                entity.CustomsOfficeCodeForExportDeclaration = dataSet.Tables[0].Rows[i]["CustomsOfficeCodeForExportDeclaration"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["CustomsOfficeCodeForExportDeclaration"].ToString() : String.Empty;
                entity.CustomsOfficeCodeForDeclarationOnTransportation = dataSet.Tables[0].Rows[i]["CustomsOfficeCodeForDeclarationOnTransportation"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["CustomsOfficeCodeForDeclarationOnTransportation"].ToString() : String.Empty;
                entity.Notes = dataSet.Tables[0].Rows[i]["Notes"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["Notes"].ToString() : String.Empty;
                entity.InputMessageID = dataSet.Tables[0].Rows[i]["InputMessageID"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["InputMessageID"].ToString() : String.Empty;
                entity.MessageTag = dataSet.Tables[0].Rows[i]["MessageTag"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["MessageTag"].ToString() : String.Empty;
                entity.IndexTag = dataSet.Tables[0].Rows[i]["IndexTag"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["IndexTag"].ToString() : String.Empty;

                collection.Add(entity);
            }
            return collection;
        }
		public static bool Find(List<VNACC_Category_Cargo> collection, string bondedAreaCode)
        {
            foreach (VNACC_Category_Cargo item in collection)
            {
                if (item.BondedAreaCode == bondedAreaCode)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_VNACC_Category_Cargo VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @BondedAreaCode, @UserCode, @BondedAreaName, @BondedAreaDemarcation, @NecessityIndicationOfBondedAreaName, @CustomsOfficeCodeForImportDeclaration, @CustomsOfficeCodeForExportDeclaration, @CustomsOfficeCodeForDeclarationOnTransportation, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_Cargo SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, UserCode = @UserCode, BondedAreaName = @BondedAreaName, BondedAreaDemarcation = @BondedAreaDemarcation, NecessityIndicationOfBondedAreaName = @NecessityIndicationOfBondedAreaName, CustomsOfficeCodeForImportDeclaration = @CustomsOfficeCodeForImportDeclaration, CustomsOfficeCodeForExportDeclaration = @CustomsOfficeCodeForExportDeclaration, CustomsOfficeCodeForDeclarationOnTransportation = @CustomsOfficeCodeForDeclarationOnTransportation, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE BondedAreaCode = @BondedAreaCode";
            string delete = "DELETE FROM t_VNACC_Category_Cargo WHERE BondedAreaCode = @BondedAreaCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BondedAreaCode", SqlDbType.VarChar, "BondedAreaCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserCode", SqlDbType.VarChar, "UserCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BondedAreaName", SqlDbType.NVarChar, "BondedAreaName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BondedAreaDemarcation", SqlDbType.Char, "BondedAreaDemarcation", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NecessityIndicationOfBondedAreaName", SqlDbType.Int, "NecessityIndicationOfBondedAreaName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsOfficeCodeForImportDeclaration", SqlDbType.VarChar, "CustomsOfficeCodeForImportDeclaration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsOfficeCodeForExportDeclaration", SqlDbType.VarChar, "CustomsOfficeCodeForExportDeclaration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsOfficeCodeForDeclarationOnTransportation", SqlDbType.VarChar, "CustomsOfficeCodeForDeclarationOnTransportation", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BondedAreaCode", SqlDbType.VarChar, "BondedAreaCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserCode", SqlDbType.VarChar, "UserCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BondedAreaName", SqlDbType.NVarChar, "BondedAreaName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BondedAreaDemarcation", SqlDbType.Char, "BondedAreaDemarcation", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NecessityIndicationOfBondedAreaName", SqlDbType.Int, "NecessityIndicationOfBondedAreaName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsOfficeCodeForImportDeclaration", SqlDbType.VarChar, "CustomsOfficeCodeForImportDeclaration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsOfficeCodeForExportDeclaration", SqlDbType.VarChar, "CustomsOfficeCodeForExportDeclaration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsOfficeCodeForDeclarationOnTransportation", SqlDbType.VarChar, "CustomsOfficeCodeForDeclarationOnTransportation", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@BondedAreaCode", SqlDbType.VarChar, "BondedAreaCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_VNACC_Category_Cargo VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @BondedAreaCode, @UserCode, @BondedAreaName, @BondedAreaDemarcation, @NecessityIndicationOfBondedAreaName, @CustomsOfficeCodeForImportDeclaration, @CustomsOfficeCodeForExportDeclaration, @CustomsOfficeCodeForDeclarationOnTransportation, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_Cargo SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, UserCode = @UserCode, BondedAreaName = @BondedAreaName, BondedAreaDemarcation = @BondedAreaDemarcation, NecessityIndicationOfBondedAreaName = @NecessityIndicationOfBondedAreaName, CustomsOfficeCodeForImportDeclaration = @CustomsOfficeCodeForImportDeclaration, CustomsOfficeCodeForExportDeclaration = @CustomsOfficeCodeForExportDeclaration, CustomsOfficeCodeForDeclarationOnTransportation = @CustomsOfficeCodeForDeclarationOnTransportation, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE BondedAreaCode = @BondedAreaCode";
            string delete = "DELETE FROM t_VNACC_Category_Cargo WHERE BondedAreaCode = @BondedAreaCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BondedAreaCode", SqlDbType.VarChar, "BondedAreaCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserCode", SqlDbType.VarChar, "UserCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BondedAreaName", SqlDbType.NVarChar, "BondedAreaName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BondedAreaDemarcation", SqlDbType.Char, "BondedAreaDemarcation", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NecessityIndicationOfBondedAreaName", SqlDbType.Int, "NecessityIndicationOfBondedAreaName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsOfficeCodeForImportDeclaration", SqlDbType.VarChar, "CustomsOfficeCodeForImportDeclaration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsOfficeCodeForExportDeclaration", SqlDbType.VarChar, "CustomsOfficeCodeForExportDeclaration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsOfficeCodeForDeclarationOnTransportation", SqlDbType.VarChar, "CustomsOfficeCodeForDeclarationOnTransportation", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BondedAreaCode", SqlDbType.VarChar, "BondedAreaCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserCode", SqlDbType.VarChar, "UserCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BondedAreaName", SqlDbType.NVarChar, "BondedAreaName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BondedAreaDemarcation", SqlDbType.Char, "BondedAreaDemarcation", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NecessityIndicationOfBondedAreaName", SqlDbType.Int, "NecessityIndicationOfBondedAreaName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsOfficeCodeForImportDeclaration", SqlDbType.VarChar, "CustomsOfficeCodeForImportDeclaration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsOfficeCodeForExportDeclaration", SqlDbType.VarChar, "CustomsOfficeCodeForExportDeclaration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsOfficeCodeForDeclarationOnTransportation", SqlDbType.VarChar, "CustomsOfficeCodeForDeclarationOnTransportation", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@BondedAreaCode", SqlDbType.VarChar, "BondedAreaCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VNACC_Category_Cargo Load(string bondedAreaCode)
		{
			const string spName = "[dbo].[p_VNACC_Category_Cargo_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BondedAreaCode", SqlDbType.VarChar, bondedAreaCode);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<VNACC_Category_Cargo> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<VNACC_Category_Cargo> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<VNACC_Category_Cargo> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_Cargo_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_Cargo_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_Cargo_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_Cargo_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertVNACC_Category_Cargo(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string userCode, string bondedAreaName, string bondedAreaDemarcation, int necessityIndicationOfBondedAreaName, string customsOfficeCodeForImportDeclaration, string customsOfficeCodeForExportDeclaration, string customsOfficeCodeForDeclarationOnTransportation, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_Cargo entity = new VNACC_Category_Cargo();	
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.UserCode = userCode;
			entity.BondedAreaName = bondedAreaName;
			entity.BondedAreaDemarcation = bondedAreaDemarcation;
			entity.NecessityIndicationOfBondedAreaName = necessityIndicationOfBondedAreaName;
			entity.CustomsOfficeCodeForImportDeclaration = customsOfficeCodeForImportDeclaration;
			entity.CustomsOfficeCodeForExportDeclaration = customsOfficeCodeForExportDeclaration;
			entity.CustomsOfficeCodeForDeclarationOnTransportation = customsOfficeCodeForDeclarationOnTransportation;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_VNACC_Category_Cargo_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@BondedAreaCode", SqlDbType.VarChar, BondedAreaCode);
			db.AddInParameter(dbCommand, "@UserCode", SqlDbType.VarChar, UserCode);
			db.AddInParameter(dbCommand, "@BondedAreaName", SqlDbType.NVarChar, BondedAreaName);
			db.AddInParameter(dbCommand, "@BondedAreaDemarcation", SqlDbType.Char, BondedAreaDemarcation);
			db.AddInParameter(dbCommand, "@NecessityIndicationOfBondedAreaName", SqlDbType.Int, NecessityIndicationOfBondedAreaName);
			db.AddInParameter(dbCommand, "@CustomsOfficeCodeForImportDeclaration", SqlDbType.VarChar, CustomsOfficeCodeForImportDeclaration);
			db.AddInParameter(dbCommand, "@CustomsOfficeCodeForExportDeclaration", SqlDbType.VarChar, CustomsOfficeCodeForExportDeclaration);
			db.AddInParameter(dbCommand, "@CustomsOfficeCodeForDeclarationOnTransportation", SqlDbType.VarChar, CustomsOfficeCodeForDeclarationOnTransportation);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<VNACC_Category_Cargo> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Cargo item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVNACC_Category_Cargo(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string bondedAreaCode, string userCode, string bondedAreaName, string bondedAreaDemarcation, int necessityIndicationOfBondedAreaName, string customsOfficeCodeForImportDeclaration, string customsOfficeCodeForExportDeclaration, string customsOfficeCodeForDeclarationOnTransportation, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_Cargo entity = new VNACC_Category_Cargo();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.BondedAreaCode = bondedAreaCode;
			entity.UserCode = userCode;
			entity.BondedAreaName = bondedAreaName;
			entity.BondedAreaDemarcation = bondedAreaDemarcation;
			entity.NecessityIndicationOfBondedAreaName = necessityIndicationOfBondedAreaName;
			entity.CustomsOfficeCodeForImportDeclaration = customsOfficeCodeForImportDeclaration;
			entity.CustomsOfficeCodeForExportDeclaration = customsOfficeCodeForExportDeclaration;
			entity.CustomsOfficeCodeForDeclarationOnTransportation = customsOfficeCodeForDeclarationOnTransportation;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_VNACC_Category_Cargo_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@BondedAreaCode", SqlDbType.VarChar, BondedAreaCode);
			db.AddInParameter(dbCommand, "@UserCode", SqlDbType.VarChar, UserCode);
			db.AddInParameter(dbCommand, "@BondedAreaName", SqlDbType.NVarChar, BondedAreaName);
			db.AddInParameter(dbCommand, "@BondedAreaDemarcation", SqlDbType.Char, BondedAreaDemarcation);
			db.AddInParameter(dbCommand, "@NecessityIndicationOfBondedAreaName", SqlDbType.Int, NecessityIndicationOfBondedAreaName);
			db.AddInParameter(dbCommand, "@CustomsOfficeCodeForImportDeclaration", SqlDbType.VarChar, CustomsOfficeCodeForImportDeclaration);
			db.AddInParameter(dbCommand, "@CustomsOfficeCodeForExportDeclaration", SqlDbType.VarChar, CustomsOfficeCodeForExportDeclaration);
			db.AddInParameter(dbCommand, "@CustomsOfficeCodeForDeclarationOnTransportation", SqlDbType.VarChar, CustomsOfficeCodeForDeclarationOnTransportation);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<VNACC_Category_Cargo> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Cargo item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVNACC_Category_Cargo(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string bondedAreaCode, string userCode, string bondedAreaName, string bondedAreaDemarcation, int necessityIndicationOfBondedAreaName, string customsOfficeCodeForImportDeclaration, string customsOfficeCodeForExportDeclaration, string customsOfficeCodeForDeclarationOnTransportation, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_Cargo entity = new VNACC_Category_Cargo();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.BondedAreaCode = bondedAreaCode;
			entity.UserCode = userCode;
			entity.BondedAreaName = bondedAreaName;
			entity.BondedAreaDemarcation = bondedAreaDemarcation;
			entity.NecessityIndicationOfBondedAreaName = necessityIndicationOfBondedAreaName;
			entity.CustomsOfficeCodeForImportDeclaration = customsOfficeCodeForImportDeclaration;
			entity.CustomsOfficeCodeForExportDeclaration = customsOfficeCodeForExportDeclaration;
			entity.CustomsOfficeCodeForDeclarationOnTransportation = customsOfficeCodeForDeclarationOnTransportation;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_Cargo_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@BondedAreaCode", SqlDbType.VarChar, BondedAreaCode);
			db.AddInParameter(dbCommand, "@UserCode", SqlDbType.VarChar, UserCode);
			db.AddInParameter(dbCommand, "@BondedAreaName", SqlDbType.NVarChar, BondedAreaName);
			db.AddInParameter(dbCommand, "@BondedAreaDemarcation", SqlDbType.Char, BondedAreaDemarcation);
			db.AddInParameter(dbCommand, "@NecessityIndicationOfBondedAreaName", SqlDbType.Int, NecessityIndicationOfBondedAreaName);
			db.AddInParameter(dbCommand, "@CustomsOfficeCodeForImportDeclaration", SqlDbType.VarChar, CustomsOfficeCodeForImportDeclaration);
			db.AddInParameter(dbCommand, "@CustomsOfficeCodeForExportDeclaration", SqlDbType.VarChar, CustomsOfficeCodeForExportDeclaration);
			db.AddInParameter(dbCommand, "@CustomsOfficeCodeForDeclarationOnTransportation", SqlDbType.VarChar, CustomsOfficeCodeForDeclarationOnTransportation);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<VNACC_Category_Cargo> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Cargo item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVNACC_Category_Cargo(string bondedAreaCode)
		{
			VNACC_Category_Cargo entity = new VNACC_Category_Cargo();
			entity.BondedAreaCode = bondedAreaCode;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_Cargo_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BondedAreaCode", SqlDbType.VarChar, BondedAreaCode);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_VNACC_Category_Cargo_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<VNACC_Category_Cargo> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Cargo item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}