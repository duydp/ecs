﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Xml;
using System.Security.Principal;
using System.Security.AccessControl;

namespace GenLicense
{
    public partial class formMaHoa : Form
    {
        //-- yJSXAArLTPqZ1q1U5L0WOw==  true
        private string passEncryptToString = @"Softech.ECS";
        public formMaHoa()
        {
            InitializeComponent();
        }

        private void btnSinhMa_Click(object sender, EventArgs e)
        {
            if (txtChuoiNhap.Text.Trim() == "")
            {
                MessageBox.Show("Vui lòng nhập chuỗi cần mã hóa");
                return;
            }
            else {
                string strNhap = txtChuoiNhap.Text;
                string strMaHoa = "";
                strMaHoa = EncryptString(strNhap);
                txtChuoiMaHoa.Text = strMaHoa;
            }
        }

        private void btnDoc_Click(object sender, EventArgs e)
        {
            try {
                if (txtChuoiNhap.Text.Trim() == "")
                {
                    MessageBox.Show("Vui lòng nhập chuỗi cần đọc");
                    return;
                }
                else
                {
                    string strNhap = txtChuoiNhap.Text;
                    string strMaHoa = "";
                    strMaHoa = DecryptString(strNhap);
                    txtChuoiMaHoa.Text = strMaHoa;
                }
            }
            catch(Exception ex){
                MessageBox.Show("" + ex.ToString());
            }
        }

        #region Encrypt/Decrypt String
        public string EncryptString(string plainText)
        {
            byte[] text = Encoding.ASCII.GetBytes(plainText);

            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            byte[] salt = Encoding.ASCII.GetBytes(this.passEncryptToString.Length.ToString());

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(this.passEncryptToString, salt);

            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();

            CryptoStream encStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);
            encStream.Write(text, 0, text.Length);

            encStream.FlushFinalBlock();

            byte[] CipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            encStream.Close();

            return Convert.ToBase64String(CipherBytes);
        }

        public string DecryptString(string plainText)
        {
            byte[] text = Convert.FromBase64String(plainText);

            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            byte[] salt = Encoding.ASCII.GetBytes(this.passEncryptToString.Length.ToString());

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(this.passEncryptToString, salt);

            ICryptoTransform decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream(text);

            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            text = new byte[text.Length];
            int DecryptedCount = cryptoStream.Read(text, 0, text.Length);

            memoryStream.Close();
            cryptoStream.Close();


            return Encoding.ASCII.GetString(text);
        }
        #endregion

       
    }
}
