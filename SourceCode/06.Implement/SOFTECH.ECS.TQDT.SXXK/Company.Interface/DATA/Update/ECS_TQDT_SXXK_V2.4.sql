/*
Run this script on:

DIRECTOR-SD\SQLKHANH.ECS_TQDT_KD_V3_DAILY_DEMO    -  This database will be modified

to synchronize it with:

DIRECTOR-SD\SQLKHANH.ECS_TQDT_KD

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 8.1.0 from Red Gate Software Ltd at 06/20/2012 9:20:44 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Update 55 rows in [dbo].[t_HaiQuan_DonViHaiQuan]
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan thành phố Hà Nội' WHERE [ID]='Z01Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan thành phố Hồ Chí Minh' WHERE [ID]='Z02Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan thành phố Hải Phòng' WHERE [ID]='Z03Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Hà Giang' WHERE [ID]='Z10Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Cao Bằng' WHERE [ID]='Z11Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Lai Châu' WHERE [ID]='Z12Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Lào Cai' WHERE [ID]='Z13Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Tuyên Quang' WHERE [ID]='Z14Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Lạng Sơn' WHERE [ID]='Z15Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Bắc Thái' WHERE [ID]='Z16Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Yên Bái' WHERE [ID]='Z17Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Sơn La' WHERE [ID]='Z18Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Vĩnh Phúc' WHERE [ID]='Z19Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Quảng Ninh' WHERE [ID]='Z20Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Hà Bắc' WHERE [ID]='Z21Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Hà Tây' WHERE [ID]='Z22Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Hải Hưng' WHERE [ID]='Z23Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Hòa Bình' WHERE [ID]='Z24Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Nam Hà' WHERE [ID]='Z25Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Thái Bình' WHERE [ID]='Z26Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Thanh Hóa' WHERE [ID]='Z27Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Ninh Bình' WHERE [ID]='Z28Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Nghệ An' WHERE [ID]='Z29Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Hà Tĩnh' WHERE [ID]='Z30Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Quảng Bình' WHERE [ID]='Z31Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Quảng Trị' WHERE [ID]='Z32Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Thừa Thiên - Huế' WHERE [ID]='Z33Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan thành phố đà nẵng' WHERE [ID]='Z34Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Quảng Ngãi' WHERE [ID]='Z35Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Kon Tum' WHERE [ID]='Z36Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Bình Định' WHERE [ID]='Z37Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Gia Lai' WHERE [ID]='Z38Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Phú Yên' WHERE [ID]='Z39Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Đắc Lắc' WHERE [ID]='Z40Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Khánh Hòa' WHERE [ID]='Z41Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Lâm Đồng' WHERE [ID]='Z42Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Bình Dương' WHERE [ID]='Z43Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Ninh Thuận' WHERE [ID]='Z44Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Tây Ninh' WHERE [ID]='Z45Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Bình Thuận' WHERE [ID]='Z46Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Đồng Nai' WHERE [ID]='Z47Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Long An' WHERE [ID]='Z48Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Đồng Tháp' WHERE [ID]='Z49Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh An Giang' WHERE [ID]='Z50Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Bà Rịa - Vũng Tàu' WHERE [ID]='Z51Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Tiền Giang' WHERE [ID]='Z52Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Kiên Giang' WHERE [ID]='Z53Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan thành phố Cần Thơ' WHERE [ID]='Z54Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Bến Tre' WHERE [ID]='Z55Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Vĩnh Long' WHERE [ID]='Z56Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Trà Vinh' WHERE [ID]='Z57Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Sóc Trăng' WHERE [ID]='Z58Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Minh Hải' WHERE [ID]='Z59Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Quảng Nam' WHERE [ID]='Z60Z  '
UPDATE [dbo].[t_HaiQuan_DonViHaiQuan] SET [Ten]=N'Cục hải quan tỉnh Bình Phước' WHERE [ID]='Z61Z  '

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='2.4', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('2.4', getdate(), null)
	end 

COMMIT TRANSACTION
GO
