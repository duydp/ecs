using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace Company.BLL.KDT.SXXK
{
    public partial class BKNPLXuatGiaCong
    {
        public override bool Equals(object obj)
        {
            BKNPLXuatGiaCong item = (BKNPLXuatGiaCong)obj;
            bool isEqual = MaLoaiHinh == item.MaLoaiHinh && SoToKhai == item.SoToKhai &&
                        NamDangKy == item.NamDangKy && item.MaHaiQuan == MaHaiQuan
                        && MaLoaiHinhXuat == item.MaLoaiHinhXuat && SoToKhaiXuat == item.SoToKhaiXuat &&
                        NamDangKyXuat == item.NamDangKyXuat && MaHaiQuanXuat == item.MaHaiQuanXuat;
            return isEqual;
        }
    }
}