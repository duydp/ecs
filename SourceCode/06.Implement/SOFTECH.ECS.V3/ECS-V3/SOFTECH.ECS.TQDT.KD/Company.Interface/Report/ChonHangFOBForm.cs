using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;
using Janus.Windows.GridEX;

namespace Company.Interface.Report
{
    public partial class ChonHangFOBForm : BaseForm
    {
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public ChonHangFOBForm()
        {
            InitializeComponent();
        }

        private void dgList1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
        }

        private void ChonHangFOBForm_Load(object sender, EventArgs e)
        {
            dgList1.DataSource = this.HMDCollection;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            GridEXRow[] rows = dgList1.GetCheckedRows();
            foreach (GridEXRow row in rows)
            {
                HangMauDich hmd = (HangMauDich)row.DataRow;
                hmd.FOC = true;
            }
            this.Close();
        }
    }
}