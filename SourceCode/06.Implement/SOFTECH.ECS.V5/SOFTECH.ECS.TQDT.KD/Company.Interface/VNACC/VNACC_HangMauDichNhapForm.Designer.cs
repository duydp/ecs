﻿namespace Company.Interface
{
    partial class VNACC_HangMauDichNhapForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListThueThuKhac_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_HangMauDichNhapForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoMucKhaiKhoanDC5 = new System.Windows.Forms.NumericUpDown();
            this.txtSoMucKhaiKhoanDC4 = new System.Windows.Forms.NumericUpDown();
            this.txtSoMucKhaiKhoanDC3 = new System.Windows.Forms.NumericUpDown();
            this.txtSoMucKhaiKhoanDC2 = new System.Windows.Forms.NumericUpDown();
            this.txtSoMucKhaiKhoanDC1 = new System.Windows.Forms.NumericUpDown();
            this.ctrNuocXuatXu = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.txtMaHanNgach = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDonGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoDMMienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.ctrDV_SL_TrongDonGiaTinhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDVTDanhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoDongDMMienThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ctrMaTTTriGiaTinhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTTuyetDoi = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label31 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSoLuongTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTTDongHangTKTNTX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMaDVTTuyetDoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueSuatTuyetDoi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaTinhThueS = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtThueSuat = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMaBieuThueNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaMucThueTuyetDoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDieuKhoanMienGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaMienGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtSoTienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienMienGiam = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.grListThueThuKhac = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.ctrMaTTDonGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDVTLuong2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDVTLuong1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaSoHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTriGiaHoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDonGiaHoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuong2 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuong1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDVTDonGia = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtMaQuanLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListThueThuKhac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 616), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 616);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 592);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 592);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(817, 616);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.grList);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 442);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(817, 174);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Danh sách hàng";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grList.FrozenColumns = 4;
            this.grList.GroupByBoxVisible = false;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Location = new System.Drawing.Point(0, 17);
            this.grList.Name = "grList";
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(817, 154);
            this.grList.TabIndex = 1;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.DoubleClick += new System.EventHandler(this.grList_DoubleClick);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtSoMucKhaiKhoanDC5);
            this.uiGroupBox2.Controls.Add(this.txtSoMucKhaiKhoanDC4);
            this.uiGroupBox2.Controls.Add(this.txtSoMucKhaiKhoanDC3);
            this.uiGroupBox2.Controls.Add(this.txtSoMucKhaiKhoanDC2);
            this.uiGroupBox2.Controls.Add(this.txtSoMucKhaiKhoanDC1);
            this.uiGroupBox2.Controls.Add(this.ctrNuocXuatXu);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox2.Controls.Add(this.ctrMaTTDonGia);
            this.uiGroupBox2.Controls.Add(this.ctrDVTLuong2);
            this.uiGroupBox2.Controls.Add(this.ctrDVTLuong1);
            this.uiGroupBox2.Controls.Add(this.ctrMaSoHang);
            this.uiGroupBox2.Controls.Add(this.txtTriGiaHoaDon);
            this.uiGroupBox2.Controls.Add(this.txtDonGiaHoaDon);
            this.uiGroupBox2.Controls.Add(this.txtSoLuong2);
            this.uiGroupBox2.Controls.Add(this.txtSoLuong1);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.txtDVTDonGia);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.label19);
            this.uiGroupBox2.Controls.Add(this.txtMaQuanLy);
            this.uiGroupBox2.Controls.Add(this.txtTenHang);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label18);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(817, 442);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtSoMucKhaiKhoanDC5
            // 
            this.txtSoMucKhaiKhoanDC5.Location = new System.Drawing.Point(575, 134);
            this.txtSoMucKhaiKhoanDC5.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtSoMucKhaiKhoanDC5.Name = "txtSoMucKhaiKhoanDC5";
            this.txtSoMucKhaiKhoanDC5.Size = new System.Drawing.Size(33, 21);
            this.txtSoMucKhaiKhoanDC5.TabIndex = 73;
            // 
            // txtSoMucKhaiKhoanDC4
            // 
            this.txtSoMucKhaiKhoanDC4.Location = new System.Drawing.Point(536, 134);
            this.txtSoMucKhaiKhoanDC4.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtSoMucKhaiKhoanDC4.Name = "txtSoMucKhaiKhoanDC4";
            this.txtSoMucKhaiKhoanDC4.Size = new System.Drawing.Size(33, 21);
            this.txtSoMucKhaiKhoanDC4.TabIndex = 73;
            // 
            // txtSoMucKhaiKhoanDC3
            // 
            this.txtSoMucKhaiKhoanDC3.Location = new System.Drawing.Point(497, 134);
            this.txtSoMucKhaiKhoanDC3.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtSoMucKhaiKhoanDC3.Name = "txtSoMucKhaiKhoanDC3";
            this.txtSoMucKhaiKhoanDC3.Size = new System.Drawing.Size(33, 21);
            this.txtSoMucKhaiKhoanDC3.TabIndex = 73;
            // 
            // txtSoMucKhaiKhoanDC2
            // 
            this.txtSoMucKhaiKhoanDC2.Location = new System.Drawing.Point(458, 134);
            this.txtSoMucKhaiKhoanDC2.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtSoMucKhaiKhoanDC2.Name = "txtSoMucKhaiKhoanDC2";
            this.txtSoMucKhaiKhoanDC2.Size = new System.Drawing.Size(33, 21);
            this.txtSoMucKhaiKhoanDC2.TabIndex = 73;
            // 
            // txtSoMucKhaiKhoanDC1
            // 
            this.txtSoMucKhaiKhoanDC1.Location = new System.Drawing.Point(419, 134);
            this.txtSoMucKhaiKhoanDC1.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtSoMucKhaiKhoanDC1.Name = "txtSoMucKhaiKhoanDC1";
            this.txtSoMucKhaiKhoanDC1.Size = new System.Drawing.Size(33, 21);
            this.txtSoMucKhaiKhoanDC1.TabIndex = 73;
            // 
            // ctrNuocXuatXu
            // 
            this.ctrNuocXuatXu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ctrNuocXuatXu.Code = "";
            this.ctrNuocXuatXu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNuocXuatXu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrNuocXuatXu.Location = new System.Drawing.Point(106, 130);
            this.ctrNuocXuatXu.Name = "ctrNuocXuatXu";
            this.ctrNuocXuatXu.Name_VN = "";
            this.ctrNuocXuatXu.SetValidate = false;
            this.ctrNuocXuatXu.ShowColumnCode = true;
            this.ctrNuocXuatXu.ShowColumnName = true;
            this.ctrNuocXuatXu.Size = new System.Drawing.Size(159, 26);
            this.ctrNuocXuatXu.TabIndex = 76;
            this.ctrNuocXuatXu.TagCode = "";
            this.ctrNuocXuatXu.TagName = "";
            this.ctrNuocXuatXu.WhereCondition = "";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.uiTab1);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox5.Location = new System.Drawing.Point(3, 153);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(811, 256);
            this.uiGroupBox5.TabIndex = 75;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(3, 8);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(805, 245);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab1.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.txtMaHanNgach);
            this.uiTabPage1.Controls.Add(this.txtDonGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.txtSoDMMienThue);
            this.uiTabPage1.Controls.Add(this.label29);
            this.uiTabPage1.Controls.Add(this.ctrDV_SL_TrongDonGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.ctrMaDVTDanhThue);
            this.uiTabPage1.Controls.Add(this.txtSoDongDMMienThue);
            this.uiTabPage1.Controls.Add(this.ctrMaTTTriGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.ctrMaTTTuyetDoi);
            this.uiTabPage1.Controls.Add(this.label31);
            this.uiTabPage1.Controls.Add(this.label9);
            this.uiTabPage1.Controls.Add(this.txtSoLuongTinhThue);
            this.uiTabPage1.Controls.Add(this.txtSoTTDongHangTKTNTX);
            this.uiTabPage1.Controls.Add(this.label12);
            this.uiTabPage1.Controls.Add(this.txtMaDVTTuyetDoi);
            this.uiTabPage1.Controls.Add(this.txtThueSuatTuyetDoi);
            this.uiTabPage1.Controls.Add(this.txtTriGiaTinhThueS);
            this.uiTabPage1.Controls.Add(this.txtTriGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.label27);
            this.uiTabPage1.Controls.Add(this.txtThueSuat);
            this.uiTabPage1.Controls.Add(this.label26);
            this.uiTabPage1.Controls.Add(this.label16);
            this.uiTabPage1.Controls.Add(this.label17);
            this.uiTabPage1.Controls.Add(this.label30);
            this.uiTabPage1.Controls.Add(this.label15);
            this.uiTabPage1.Controls.Add(this.label24);
            this.uiTabPage1.Controls.Add(this.label22);
            this.uiTabPage1.Controls.Add(this.label13);
            this.uiTabPage1.Controls.Add(this.txtMaBieuThueNK);
            this.uiTabPage1.Controls.Add(this.txtMaMucThueTuyetDoi);
            this.uiTabPage1.Controls.Add(this.label23);
            this.uiTabPage1.Controls.Add(this.label25);
            this.uiTabPage1.Controls.Add(this.label21);
            this.uiTabPage1.Controls.Add(this.label10);
            this.uiTabPage1.Controls.Add(this.txtDieuKhoanMienGiam);
            this.uiTabPage1.Controls.Add(this.txtMaMienGiam);
            this.uiTabPage1.Controls.Add(this.label14);
            this.uiTabPage1.Controls.Add(this.label28);
            this.uiTabPage1.Controls.Add(this.label20);
            this.uiTabPage1.Controls.Add(this.txtSoTienThue);
            this.uiTabPage1.Controls.Add(this.txtSoTienMienGiam);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(803, 223);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thuế nhập khẩu";
            // 
            // txtMaHanNgach
            // 
            this.txtMaHanNgach.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHanNgach.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHanNgach.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHanNgach.Location = new System.Drawing.Point(478, 67);
            this.txtMaHanNgach.MaxLength = 12;
            this.txtMaHanNgach.Name = "txtMaHanNgach";
            this.txtMaHanNgach.Size = new System.Drawing.Size(138, 21);
            this.txtMaHanNgach.TabIndex = 9;
            this.txtMaHanNgach.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHanNgach.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDonGiaTinhThue
            // 
            this.txtDonGiaTinhThue.DecimalDigits = 20;
            this.txtDonGiaTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDonGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaTinhThue.Location = new System.Drawing.Point(478, 92);
            this.txtDonGiaTinhThue.MaxLength = 15;
            this.txtDonGiaTinhThue.Name = "txtDonGiaTinhThue";
            this.txtDonGiaTinhThue.ReadOnly = true;
            this.txtDonGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaTinhThue.Size = new System.Drawing.Size(138, 21);
            this.txtDonGiaTinhThue.TabIndex = 24;
            this.txtDonGiaTinhThue.Text = "0";
            this.txtDonGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoDMMienThue
            // 
            this.txtSoDMMienThue.DecimalDigits = 20;
            this.txtSoDMMienThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoDMMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDMMienThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoDMMienThue.Location = new System.Drawing.Point(194, 115);
            this.txtSoDMMienThue.MaxLength = 15;
            this.txtSoDMMienThue.Name = "txtSoDMMienThue";
            this.txtSoDMMienThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoDMMienThue.Size = new System.Drawing.Size(138, 21);
            this.txtSoDMMienThue.TabIndex = 12;
            this.txtSoDMMienThue.Text = "0";
            this.txtSoDMMienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDMMienThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoDMMienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(11, 119);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(169, 13);
            this.label29.TabIndex = 51;
            this.label29.Text = "Số danh mục miễn thuế xuất khẩu";
            // 
            // ctrDV_SL_TrongDonGiaTinhThue
            // 
            this.ctrDV_SL_TrongDonGiaTinhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDV_SL_TrongDonGiaTinhThue.Code = "";
            this.ctrDV_SL_TrongDonGiaTinhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDV_SL_TrongDonGiaTinhThue.Enabled = false;
            this.ctrDV_SL_TrongDonGiaTinhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDV_SL_TrongDonGiaTinhThue.IsValidate = false;
            this.ctrDV_SL_TrongDonGiaTinhThue.Location = new System.Drawing.Point(643, 89);
            this.ctrDV_SL_TrongDonGiaTinhThue.Name = "ctrDV_SL_TrongDonGiaTinhThue";
            this.ctrDV_SL_TrongDonGiaTinhThue.Name_VN = "";
            this.ctrDV_SL_TrongDonGiaTinhThue.ShowColumnCode = true;
            this.ctrDV_SL_TrongDonGiaTinhThue.ShowColumnName = false;
            this.ctrDV_SL_TrongDonGiaTinhThue.Size = new System.Drawing.Size(79, 26);
            this.ctrDV_SL_TrongDonGiaTinhThue.TabIndex = 11;
            this.ctrDV_SL_TrongDonGiaTinhThue.TagName = "";
            this.ctrDV_SL_TrongDonGiaTinhThue.WhereCondition = "";
            // 
            // ctrMaDVTDanhThue
            // 
            this.ctrMaDVTDanhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrMaDVTDanhThue.Code = "";
            this.ctrMaDVTDanhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTDanhThue.Enabled = false;
            this.ctrMaDVTDanhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTDanhThue.IsValidate = false;
            this.ctrMaDVTDanhThue.Location = new System.Drawing.Point(643, 139);
            this.ctrMaDVTDanhThue.Name = "ctrMaDVTDanhThue";
            this.ctrMaDVTDanhThue.Name_VN = "";
            this.ctrMaDVTDanhThue.ShowColumnCode = true;
            this.ctrMaDVTDanhThue.ShowColumnName = false;
            this.ctrMaDVTDanhThue.Size = new System.Drawing.Size(79, 26);
            this.ctrMaDVTDanhThue.TabIndex = 11;
            this.ctrMaDVTDanhThue.TagName = "";
            this.ctrMaDVTDanhThue.WhereCondition = "";
            // 
            // txtSoDongDMMienThue
            // 
            this.txtSoDongDMMienThue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoDongDMMienThue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoDongDMMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDongDMMienThue.Location = new System.Drawing.Point(256, 142);
            this.txtSoDongDMMienThue.MaxLength = 12;
            this.txtSoDongDMMienThue.Name = "txtSoDongDMMienThue";
            this.txtSoDongDMMienThue.Size = new System.Drawing.Size(76, 21);
            this.txtSoDongDMMienThue.TabIndex = 13;
            this.txtSoDongDMMienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDongDMMienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ctrMaTTTriGiaTinhThue
            // 
            this.ctrMaTTTriGiaTinhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTriGiaTinhThue.Code = "";
            this.ctrMaTTTriGiaTinhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTriGiaTinhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTTriGiaTinhThue.IsValidate = false;
            this.ctrMaTTTriGiaTinhThue.Location = new System.Drawing.Point(287, 37);
            this.ctrMaTTTriGiaTinhThue.Name = "ctrMaTTTriGiaTinhThue";
            this.ctrMaTTTriGiaTinhThue.Name_VN = "";
            this.ctrMaTTTriGiaTinhThue.ShowColumnCode = true;
            this.ctrMaTTTriGiaTinhThue.ShowColumnName = false;
            this.ctrMaTTTriGiaTinhThue.Size = new System.Drawing.Size(79, 26);
            this.ctrMaTTTriGiaTinhThue.TabIndex = 6;
            this.ctrMaTTTriGiaTinhThue.TagName = "";
            this.ctrMaTTTriGiaTinhThue.WhereCondition = "";
            // 
            // ctrMaTTTuyetDoi
            // 
            this.ctrMaTTTuyetDoi.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTuyetDoi.Code = "";
            this.ctrMaTTTuyetDoi.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTuyetDoi.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTTuyetDoi.IsValidate = false;
            this.ctrMaTTTuyetDoi.Location = new System.Drawing.Point(620, 10);
            this.ctrMaTTTuyetDoi.Name = "ctrMaTTTuyetDoi";
            this.ctrMaTTTuyetDoi.Name_VN = "";
            this.ctrMaTTTuyetDoi.ShowColumnCode = true;
            this.ctrMaTTTuyetDoi.ShowColumnName = false;
            this.ctrMaTTTuyetDoi.Size = new System.Drawing.Size(79, 26);
            this.ctrMaTTTuyetDoi.TabIndex = 4;
            this.ctrMaTTTuyetDoi.TagName = "";
            this.ctrMaTTTuyetDoi.WhereCondition = "";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(11, 146);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(228, 13);
            this.label31.TabIndex = 72;
            this.label31.Text = "Số dòng tương ứng trong danh mục miễn thuế";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(279, 13);
            this.label9.TabIndex = 43;
            this.label9.Text = "STT dòng hàng trên tờ khai tạm nhập tái xuất tương ứng";
            // 
            // txtSoLuongTinhThue
            // 
            this.txtSoLuongTinhThue.DecimalDigits = 20;
            this.txtSoLuongTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongTinhThue.Location = new System.Drawing.Point(478, 142);
            this.txtSoLuongTinhThue.MaxLength = 15;
            this.txtSoLuongTinhThue.Name = "txtSoLuongTinhThue";
            this.txtSoLuongTinhThue.ReadOnly = true;
            this.txtSoLuongTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongTinhThue.Size = new System.Drawing.Size(138, 21);
            this.txtSoLuongTinhThue.TabIndex = 10;
            this.txtSoLuongTinhThue.Text = "0";
            this.txtSoLuongTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTTDongHangTKTNTX
            // 
            this.txtSoTTDongHangTKTNTX.DecimalDigits = 20;
            this.txtSoTTDongHangTKTNTX.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTTDongHangTKTNTX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTTDongHangTKTNTX.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTTDongHangTKTNTX.Location = new System.Drawing.Point(290, 92);
            this.txtSoTTDongHangTKTNTX.MaxLength = 15;
            this.txtSoTTDongHangTKTNTX.Name = "txtSoTTDongHangTKTNTX";
            this.txtSoTTDongHangTKTNTX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTTDongHangTKTNTX.Size = new System.Drawing.Size(42, 21);
            this.txtSoTTDongHangTKTNTX.TabIndex = 11;
            this.txtSoTTDongHangTKTNTX.Text = "0";
            this.txtSoTTDongHangTKTNTX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTTDongHangTKTNTX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTTDongHangTKTNTX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(353, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Mức thuế tuyệt đối";
            // 
            // txtMaDVTTuyetDoi
            // 
            this.txtMaDVTTuyetDoi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDVTTuyetDoi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDVTTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDVTTuyetDoi.Location = new System.Drawing.Point(569, 13);
            this.txtMaDVTTuyetDoi.MaxLength = 12;
            this.txtMaDVTTuyetDoi.Name = "txtMaDVTTuyetDoi";
            this.txtMaDVTTuyetDoi.Size = new System.Drawing.Size(47, 21);
            this.txtMaDVTTuyetDoi.TabIndex = 3;
            this.txtMaDVTTuyetDoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDVTTuyetDoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuatTuyetDoi
            // 
            this.txtThueSuatTuyetDoi.DecimalDigits = 20;
            this.txtThueSuatTuyetDoi.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThueSuatTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuatTuyetDoi.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtThueSuatTuyetDoi.Location = new System.Drawing.Point(451, 13);
            this.txtThueSuatTuyetDoi.MaxLength = 15;
            this.txtThueSuatTuyetDoi.Name = "txtThueSuatTuyetDoi";
            this.txtThueSuatTuyetDoi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThueSuatTuyetDoi.Size = new System.Drawing.Size(114, 21);
            this.txtThueSuatTuyetDoi.TabIndex = 2;
            this.txtThueSuatTuyetDoi.Text = "0";
            this.txtThueSuatTuyetDoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThueSuatTuyetDoi.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtThueSuatTuyetDoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaTinhThueS
            // 
            this.txtTriGiaTinhThueS.DecimalDigits = 20;
            this.txtTriGiaTinhThueS.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThueS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThueS.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaTinhThueS.Location = new System.Drawing.Point(478, 115);
            this.txtTriGiaTinhThueS.MaxLength = 15;
            this.txtTriGiaTinhThueS.Name = "txtTriGiaTinhThueS";
            this.txtTriGiaTinhThueS.ReadOnly = true;
            this.txtTriGiaTinhThueS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThueS.Size = new System.Drawing.Size(138, 21);
            this.txtTriGiaTinhThueS.TabIndex = 16;
            this.txtTriGiaTinhThueS.Text = "0";
            this.txtTriGiaTinhThueS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThueS.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTinhThueS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaTinhThue
            // 
            this.txtTriGiaTinhThue.DecimalDigits = 20;
            this.txtTriGiaTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaTinhThue.Location = new System.Drawing.Point(117, 40);
            this.txtTriGiaTinhThue.MaxLength = 15;
            this.txtTriGiaTinhThue.Name = "txtTriGiaTinhThue";
            this.txtTriGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThue.Size = new System.Drawing.Size(159, 21);
            this.txtTriGiaTinhThue.TabIndex = 5;
            this.txtTriGiaTinhThue.Text = "0";
            this.txtTriGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(616, 146);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(27, 13);
            this.label27.TabIndex = 68;
            this.label27.Text = "ĐVT";
            // 
            // txtThueSuat
            // 
            this.txtThueSuat.DecimalDigits = 20;
            this.txtThueSuat.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThueSuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuat.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtThueSuat.Location = new System.Drawing.Point(279, 13);
            this.txtThueSuat.MaxLength = 15;
            this.txtThueSuat.Name = "txtThueSuat";
            this.txtThueSuat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThueSuat.Size = new System.Drawing.Size(53, 21);
            this.txtThueSuat.TabIndex = 1;
            this.txtThueSuat.Text = "0";
            this.txtThueSuat.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThueSuat.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtThueSuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(377, 146);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(95, 13);
            this.label26.TabIndex = 65;
            this.label26.Text = "Số lượng tính thuế";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(11, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(122, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "Mã biểu thuế nhập khẩu";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(377, 71);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(103, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Mã ngoài hạn ngạch";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(372, 44);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(160, 13);
            this.label30.TabIndex = 29;
            this.label30.Text = "Mã áp dụng mức thuế tuyệt đối ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(216, 17);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Thuế suất";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(616, 173);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(28, 13);
            this.label24.TabIndex = 20;
            this.label24.Text = "VNĐ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(616, 96);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(28, 13);
            this.label22.TabIndex = 20;
            this.label22.Text = "VNĐ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(337, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "%";
            // 
            // txtMaBieuThueNK
            // 
            this.txtMaBieuThueNK.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaBieuThueNK.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaBieuThueNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBieuThueNK.Location = new System.Drawing.Point(135, 13);
            this.txtMaBieuThueNK.MaxLength = 12;
            this.txtMaBieuThueNK.Name = "txtMaBieuThueNK";
            this.txtMaBieuThueNK.Size = new System.Drawing.Size(75, 21);
            this.txtMaBieuThueNK.TabIndex = 0;
            this.txtMaBieuThueNK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaBieuThueNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaMucThueTuyetDoi
            // 
            this.txtMaMucThueTuyetDoi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaMucThueTuyetDoi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaMucThueTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaMucThueTuyetDoi.Location = new System.Drawing.Point(543, 40);
            this.txtMaMucThueTuyetDoi.MaxLength = 12;
            this.txtMaMucThueTuyetDoi.Name = "txtMaMucThueTuyetDoi";
            this.txtMaMucThueTuyetDoi.Size = new System.Drawing.Size(73, 21);
            this.txtMaMucThueTuyetDoi.TabIndex = 7;
            this.txtMaMucThueTuyetDoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaMucThueTuyetDoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(377, 173);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(65, 13);
            this.label23.TabIndex = 50;
            this.label23.Text = "Số tiền thuế";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(377, 119);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(96, 13);
            this.label25.TabIndex = 67;
            this.label25.Text = "Trị giá tính thuế(S)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(11, 71);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(141, 13);
            this.label21.TabIndex = 50;
            this.label21.Text = "Số tiền giảm thuế xuất khẩu";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(11, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 13);
            this.label10.TabIndex = 67;
            this.label10.Text = "Trị giá tính thuế(M)";
            // 
            // txtDieuKhoanMienGiam
            // 
            this.txtDieuKhoanMienGiam.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDieuKhoanMienGiam.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDieuKhoanMienGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDieuKhoanMienGiam.Location = new System.Drawing.Point(332, 198);
            this.txtDieuKhoanMienGiam.MaxLength = 12;
            this.txtDieuKhoanMienGiam.Name = "txtDieuKhoanMienGiam";
            this.txtDieuKhoanMienGiam.ReadOnly = true;
            this.txtDieuKhoanMienGiam.Size = new System.Drawing.Size(390, 21);
            this.txtDieuKhoanMienGiam.TabIndex = 14;
            this.txtDieuKhoanMienGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDieuKhoanMienGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaMienGiam
            // 
            this.txtMaMienGiam.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaMienGiam.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaMienGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaMienGiam.Location = new System.Drawing.Point(255, 169);
            this.txtMaMienGiam.MaxLength = 12;
            this.txtMaMienGiam.Name = "txtMaMienGiam";
            this.txtMaMienGiam.Size = new System.Drawing.Size(77, 21);
            this.txtMaMienGiam.TabIndex = 14;
            this.txtMaMienGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaMienGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(377, 96);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 13);
            this.label14.TabIndex = 52;
            this.label14.Text = "Đơn giá tính thuế";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(11, 202);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(315, 13);
            this.label28.TabIndex = 52;
            this.label28.Text = "Điều khoản miễn / Giảm / Không chịu thuế nhập khẩu (Pháp luật)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(11, 173);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(211, 13);
            this.label20.TabIndex = 52;
            this.label20.Text = "Mã miễn/ Giảm/ Không chịu thuế xuất khẩu";
            // 
            // txtSoTienThue
            // 
            this.txtSoTienThue.DecimalDigits = 20;
            this.txtSoTienThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienThue.Location = new System.Drawing.Point(478, 169);
            this.txtSoTienThue.MaxLength = 15;
            this.txtSoTienThue.Name = "txtSoTienThue";
            this.txtSoTienThue.ReadOnly = true;
            this.txtSoTienThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienThue.Size = new System.Drawing.Size(138, 21);
            this.txtSoTienThue.TabIndex = 22;
            this.txtSoTienThue.Text = "0";
            this.txtSoTienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienMienGiam
            // 
            this.txtSoTienMienGiam.DecimalDigits = 20;
            this.txtSoTienMienGiam.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienMienGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienMienGiam.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienMienGiam.Location = new System.Drawing.Point(194, 67);
            this.txtSoTienMienGiam.MaxLength = 15;
            this.txtSoTienMienGiam.Name = "txtSoTienMienGiam";
            this.txtSoTienMienGiam.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienMienGiam.Size = new System.Drawing.Size(138, 21);
            this.txtSoTienMienGiam.TabIndex = 10;
            this.txtSoTienMienGiam.Text = "0";
            this.txtSoTienMienGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienMienGiam.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienMienGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.grListThueThuKhac);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(803, 223);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thuế và thu khác";
            // 
            // grListThueThuKhac
            // 
            this.grListThueThuKhac.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListThueThuKhac.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            grListThueThuKhac_DesignTimeLayout.LayoutString = resources.GetString("grListThueThuKhac_DesignTimeLayout.LayoutString");
            this.grListThueThuKhac.DesignTimeLayout = grListThueThuKhac_DesignTimeLayout;
            this.grListThueThuKhac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListThueThuKhac.FrozenColumns = 4;
            this.grListThueThuKhac.GroupByBoxVisible = false;
            this.grListThueThuKhac.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListThueThuKhac.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListThueThuKhac.Location = new System.Drawing.Point(0, 0);
            this.grListThueThuKhac.Name = "grListThueThuKhac";
            this.grListThueThuKhac.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListThueThuKhac.Size = new System.Drawing.Size(803, 223);
            this.grListThueThuKhac.TabIndex = 0;
            this.grListThueThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListThueThuKhac.VisualStyleManager = this.vsmMain;
            this.grListThueThuKhac.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.grListThueThuKhac_DeletingRecord);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.btnClose);
            this.uiGroupBox4.Controls.Add(this.btnXoa);
            this.uiGroupBox4.Controls.Add(this.btnGhi);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox4.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 409);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(811, 30);
            this.uiGroupBox4.TabIndex = 13;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(732, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(649, 4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 1;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.Location = new System.Drawing.Point(565, 4);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 0;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // ctrMaTTDonGia
            // 
            this.ctrMaTTDonGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTDonGia.Code = "";
            this.ctrMaTTDonGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTDonGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTDonGia.IsValidate = false;
            this.ctrMaTTDonGia.Location = new System.Drawing.Point(733, 97);
            this.ctrMaTTDonGia.Name = "ctrMaTTDonGia";
            this.ctrMaTTDonGia.Name_VN = "";
            this.ctrMaTTDonGia.ShowColumnCode = true;
            this.ctrMaTTDonGia.ShowColumnName = false;
            this.ctrMaTTDonGia.Size = new System.Drawing.Size(79, 26);
            this.ctrMaTTDonGia.TabIndex = 11;
            this.ctrMaTTDonGia.TagName = "";
            this.ctrMaTTDonGia.WhereCondition = "";
            // 
            // ctrDVTLuong2
            // 
            this.ctrDVTLuong2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong2.Code = "";
            this.ctrDVTLuong2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong2.IsValidate = false;
            this.ctrDVTLuong2.Location = new System.Drawing.Point(338, 100);
            this.ctrDVTLuong2.Name = "ctrDVTLuong2";
            this.ctrDVTLuong2.Name_VN = "";
            this.ctrDVTLuong2.ShowColumnCode = true;
            this.ctrDVTLuong2.ShowColumnName = false;
            this.ctrDVTLuong2.Size = new System.Drawing.Size(79, 26);
            this.ctrDVTLuong2.TabIndex = 6;
            this.ctrDVTLuong2.TagName = "";
            this.ctrDVTLuong2.WhereCondition = "";
            // 
            // ctrDVTLuong1
            // 
            this.ctrDVTLuong1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong1.Code = "";
            this.ctrDVTLuong1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong1.IsValidate = false;
            this.ctrDVTLuong1.Location = new System.Drawing.Point(338, 73);
            this.ctrDVTLuong1.Name = "ctrDVTLuong1";
            this.ctrDVTLuong1.Name_VN = "";
            this.ctrDVTLuong1.ShowColumnCode = true;
            this.ctrDVTLuong1.ShowColumnName = false;
            this.ctrDVTLuong1.Size = new System.Drawing.Size(79, 26);
            this.ctrDVTLuong1.TabIndex = 4;
            this.ctrDVTLuong1.TagName = "";
            this.ctrDVTLuong1.WhereCondition = "";
            // 
            // ctrMaSoHang
            // 
            this.ctrMaSoHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ctrMaSoHang.Code = "";
            this.ctrMaSoHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaSoHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaSoHang.IsValidate = false;
            this.ctrMaSoHang.Location = new System.Drawing.Point(105, 43);
            this.ctrMaSoHang.Name = "ctrMaSoHang";
            this.ctrMaSoHang.Name_VN = "";
            this.ctrMaSoHang.ShowColumnCode = true;
            this.ctrMaSoHang.ShowColumnName = false;
            this.ctrMaSoHang.Size = new System.Drawing.Size(160, 26);
            this.ctrMaSoHang.TabIndex = 1;
            this.ctrMaSoHang.TagName = "";
            this.ctrMaSoHang.WhereCondition = "";
            // 
            // txtTriGiaHoaDon
            // 
            this.txtTriGiaHoaDon.BackColor = System.Drawing.Color.White;
            this.txtTriGiaHoaDon.DecimalDigits = 20;
            this.txtTriGiaHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaHoaDon.FormatString = "G20";
            this.txtTriGiaHoaDon.Location = new System.Drawing.Point(501, 73);
            this.txtTriGiaHoaDon.Name = "txtTriGiaHoaDon";
            this.txtTriGiaHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaHoaDon.Size = new System.Drawing.Size(159, 21);
            this.txtTriGiaHoaDon.TabIndex = 8;
            this.txtTriGiaHoaDon.Text = "0";
            this.txtTriGiaHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaHoaDon.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTriGiaHoaDon.VisualStyleManager = this.vsmMain;
            // 
            // txtDonGiaHoaDon
            // 
            this.txtDonGiaHoaDon.DecimalDigits = 20;
            this.txtDonGiaHoaDon.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDonGiaHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaHoaDon.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaHoaDon.Location = new System.Drawing.Point(501, 100);
            this.txtDonGiaHoaDon.MaxLength = 15;
            this.txtDonGiaHoaDon.Name = "txtDonGiaHoaDon";
            this.txtDonGiaHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaHoaDon.Size = new System.Drawing.Size(159, 21);
            this.txtDonGiaHoaDon.TabIndex = 9;
            this.txtDonGiaHoaDon.Text = "0";
            this.txtDonGiaHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonGiaHoaDon.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDonGiaHoaDon.VisualStyleManager = this.vsmMain;
            // 
            // txtSoLuong2
            // 
            this.txtSoLuong2.DecimalDigits = 20;
            this.txtSoLuong2.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong2.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong2.Location = new System.Drawing.Point(106, 103);
            this.txtSoLuong2.MaxLength = 15;
            this.txtSoLuong2.Name = "txtSoLuong2";
            this.txtSoLuong2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong2.Size = new System.Drawing.Size(159, 21);
            this.txtSoLuong2.TabIndex = 5;
            this.txtSoLuong2.Text = "0";
            this.txtSoLuong2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuong1
            // 
            this.txtSoLuong1.DecimalDigits = 20;
            this.txtSoLuong1.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong1.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong1.Location = new System.Drawing.Point(106, 76);
            this.txtSoLuong1.MaxLength = 15;
            this.txtSoLuong1.Name = "txtSoLuong1";
            this.txtSoLuong1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong1.Size = new System.Drawing.Size(159, 21);
            this.txtSoLuong1.TabIndex = 3;
            this.txtSoLuong1.Text = "0";
            this.txtSoLuong1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuong1.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(416, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 70;
            this.label8.Text = "Trị giá hóa đơn";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(276, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 69;
            this.label3.Text = "Đơn vị tính";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(416, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 66;
            this.label7.Text = "Đơn giá hóa đơn";
            // 
            // txtDVTDonGia
            // 
            this.txtDVTDonGia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDVTDonGia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDVTDonGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVTDonGia.Location = new System.Drawing.Point(671, 100);
            this.txtDVTDonGia.MaxLength = 12;
            this.txtDVTDonGia.Name = "txtDVTDonGia";
            this.txtDVTDonGia.Size = new System.Drawing.Size(56, 21);
            this.txtDVTDonGia.TabIndex = 10;
            this.txtDVTDonGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDVTDonGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(276, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 68;
            this.label1.Text = "Đơn vị tính";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 65;
            this.label5.Text = "Số lượng (2)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(21, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 64;
            this.label6.Text = "Số lượng (1)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(274, 136);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(144, 13);
            this.label19.TabIndex = 53;
            this.label19.Text = "Số của mục khoản điều chỉnh";
            // 
            // txtMaQuanLy
            // 
            this.txtMaQuanLy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaQuanLy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaQuanLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaQuanLy.Location = new System.Drawing.Point(105, 18);
            this.txtMaQuanLy.MaxLength = 12;
            this.txtMaQuanLy.Name = "txtMaQuanLy";
            this.txtMaQuanLy.Size = new System.Drawing.Size(160, 21);
            this.txtMaQuanLy.TabIndex = 0;
            this.txtMaQuanLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaQuanLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenHang
            // 
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(338, 18);
            this.txtTenHang.MaxLength = 255;
            this.txtTenHang.Multiline = true;
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(474, 49);
            this.txtTenHang.TabIndex = 2;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHang.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(275, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Tên hàng";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 50);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Mã số hàng hóa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Mã quản lý riêng";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(20, 137);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 42;
            this.label18.Text = "Nước xuất xứ";
            // 
            // VNACC_HangMauDichNhapForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 622);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_HangMauDichNhapForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin hàng";
            this.Load += new System.EventHandler(this.VNACC_HangMauDichNhapForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.uiTabPage1.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListThueThuKhac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHang;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaQuanLy;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBieuThueNK;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDVTTuyetDoi;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHanNgach;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThueSuatTuyetDoi;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTTDongHangTKTNTX;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienMienGiam;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoDMMienThue;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMienGiam;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaHoaDon;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThue;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaHoaDon;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTDonGia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMucThueTuyetDoi;
        private System.Windows.Forms.Label label30;
        private Janus.Windows.GridEX.GridEX grList;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTDonGia;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaSoHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTuyetDoi;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTriGiaTinhThue;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThueSuat;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDongDMMienThue;
        private System.Windows.Forms.Label label31;
        private Janus.Windows.GridEX.GridEX grListThueThuKhac;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaTinhThue;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTDanhThue;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongTinhThue;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThueS;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienThue;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDV_SL_TrongDonGiaTinhThue;
        private Janus.Windows.GridEX.EditControls.EditBox txtDieuKhoanMienGiam;
        private System.Windows.Forms.Label label28;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrNuocXuatXu;
        private System.Windows.Forms.NumericUpDown txtSoMucKhaiKhoanDC1;
        private System.Windows.Forms.NumericUpDown txtSoMucKhaiKhoanDC5;
        private System.Windows.Forms.NumericUpDown txtSoMucKhaiKhoanDC4;
        private System.Windows.Forms.NumericUpDown txtSoMucKhaiKhoanDC3;
        private System.Windows.Forms.NumericUpDown txtSoMucKhaiKhoanDC2;
    }
}