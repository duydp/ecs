
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name='IsGiaCongNguoc')
	ALTER TABLE dbo.t_KDT_GC_HopDong
	ADD IsGiaCongNguoc INT DEFAULT (0) NULL

GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HopDong_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HopDong_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HopDong_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HopDong_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HopDong_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HopDong_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HopDong_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HopDong_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HopDong_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HopDong_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HopDong_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HopDong_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HopDong_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HopDong_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HopDong_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HopDong_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HopDong_Insert]
-- Database: ECS_TQDT_GC_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HopDong_Insert]
	@SoTiepNhan bigint,
	@TrangThaiXuLy int,
	@SoHopDong varchar(40),
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaDaiLy varchar(14),
	@NgayKy datetime,
	@NgayDangKy datetime,
	@NgayHetHan datetime,
	@NgayGiaHan datetime,
	@NuocThue_ID char(3),
	@NguyenTe_ID char(3),
	@NgayTiepNhan datetime,
	@DonViDoiTac nvarchar(100),
	@DiaChiDoiTac nvarchar(256),
	@CanBoTheoDoi varchar(40),
	@CanBoDuyet varchar(40),
	@TrangThaiThanhKhoan int,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference nvarchar(500),
	@NamTN int,
	@HUONGDAN nvarchar(500),
	@PhanLuong varchar(50),
	@HuongdanPL nvarchar(500),
	@DiaChiDoanhNghiep nvarchar(256),
	@TenDonViDoiTac nvarchar(80),
	@TenDoanhNghiep nvarchar(80),
	@GhiChu nvarchar(2000),
	@PTTT_ID nvarchar(10),
	@TongTriGiaSP float,
	@TongTriGiaTienCong float,
	@IsGiaCongNguoc int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_HopDong]
(
	[SoTiepNhan],
	[TrangThaiXuLy],
	[SoHopDong],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaDaiLy],
	[NgayKy],
	[NgayDangKy],
	[NgayHetHan],
	[NgayGiaHan],
	[NuocThue_ID],
	[NguyenTe_ID],
	[NgayTiepNhan],
	[DonViDoiTac],
	[DiaChiDoiTac],
	[CanBoTheoDoi],
	[CanBoDuyet],
	[TrangThaiThanhKhoan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamTN],
	[HUONGDAN],
	[PhanLuong],
	[HuongdanPL],
	[DiaChiDoanhNghiep],
	[TenDonViDoiTac],
	[TenDoanhNghiep],
	[GhiChu],
	[PTTT_ID],
	[TongTriGiaSP],
	[TongTriGiaTienCong],
	[IsGiaCongNguoc]
)
VALUES 
(
	@SoTiepNhan,
	@TrangThaiXuLy,
	@SoHopDong,
	@MaHaiQuan,
	@MaDoanhNghiep,
	@MaDaiLy,
	@NgayKy,
	@NgayDangKy,
	@NgayHetHan,
	@NgayGiaHan,
	@NuocThue_ID,
	@NguyenTe_ID,
	@NgayTiepNhan,
	@DonViDoiTac,
	@DiaChiDoiTac,
	@CanBoTheoDoi,
	@CanBoDuyet,
	@TrangThaiThanhKhoan,
	@GUIDSTR,
	@DeXuatKhac,
	@LyDoSua,
	@ActionStatus,
	@GuidReference,
	@NamTN,
	@HUONGDAN,
	@PhanLuong,
	@HuongdanPL,
	@DiaChiDoanhNghiep,
	@TenDonViDoiTac,
	@TenDoanhNghiep,
	@GhiChu,
	@PTTT_ID,
	@TongTriGiaSP,
	@TongTriGiaTienCong,
	@IsGiaCongNguoc
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HopDong_Update]
-- Database: ECS_TQDT_GC_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HopDong_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@TrangThaiXuLy int,
	@SoHopDong varchar(40),
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaDaiLy varchar(14),
	@NgayKy datetime,
	@NgayDangKy datetime,
	@NgayHetHan datetime,
	@NgayGiaHan datetime,
	@NuocThue_ID char(3),
	@NguyenTe_ID char(3),
	@NgayTiepNhan datetime,
	@DonViDoiTac nvarchar(100),
	@DiaChiDoiTac nvarchar(256),
	@CanBoTheoDoi varchar(40),
	@CanBoDuyet varchar(40),
	@TrangThaiThanhKhoan int,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference nvarchar(500),
	@NamTN int,
	@HUONGDAN nvarchar(500),
	@PhanLuong varchar(50),
	@HuongdanPL nvarchar(500),
	@DiaChiDoanhNghiep nvarchar(256),
	@TenDonViDoiTac nvarchar(80),
	@TenDoanhNghiep nvarchar(80),
	@GhiChu nvarchar(2000),
	@PTTT_ID nvarchar(10),
	@TongTriGiaSP float,
	@TongTriGiaTienCong float,
	@IsGiaCongNguoc int
AS

UPDATE
	[dbo].[t_KDT_GC_HopDong]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoHopDong] = @SoHopDong,
	[MaHaiQuan] = @MaHaiQuan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[MaDaiLy] = @MaDaiLy,
	[NgayKy] = @NgayKy,
	[NgayDangKy] = @NgayDangKy,
	[NgayHetHan] = @NgayHetHan,
	[NgayGiaHan] = @NgayGiaHan,
	[NuocThue_ID] = @NuocThue_ID,
	[NguyenTe_ID] = @NguyenTe_ID,
	[NgayTiepNhan] = @NgayTiepNhan,
	[DonViDoiTac] = @DonViDoiTac,
	[DiaChiDoiTac] = @DiaChiDoiTac,
	[CanBoTheoDoi] = @CanBoTheoDoi,
	[CanBoDuyet] = @CanBoDuyet,
	[TrangThaiThanhKhoan] = @TrangThaiThanhKhoan,
	[GUIDSTR] = @GUIDSTR,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua,
	[ActionStatus] = @ActionStatus,
	[GuidReference] = @GuidReference,
	[NamTN] = @NamTN,
	[HUONGDAN] = @HUONGDAN,
	[PhanLuong] = @PhanLuong,
	[HuongdanPL] = @HuongdanPL,
	[DiaChiDoanhNghiep] = @DiaChiDoanhNghiep,
	[TenDonViDoiTac] = @TenDonViDoiTac,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[GhiChu] = @GhiChu,
	[PTTT_ID] = @PTTT_ID,
	[TongTriGiaSP] = @TongTriGiaSP,
	[TongTriGiaTienCong] = @TongTriGiaTienCong,
	[IsGiaCongNguoc] = @IsGiaCongNguoc
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HopDong_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HopDong_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@TrangThaiXuLy int,
	@SoHopDong varchar(40),
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaDaiLy varchar(14),
	@NgayKy datetime,
	@NgayDangKy datetime,
	@NgayHetHan datetime,
	@NgayGiaHan datetime,
	@NuocThue_ID char(3),
	@NguyenTe_ID char(3),
	@NgayTiepNhan datetime,
	@DonViDoiTac nvarchar(100),
	@DiaChiDoiTac nvarchar(256),
	@CanBoTheoDoi varchar(40),
	@CanBoDuyet varchar(40),
	@TrangThaiThanhKhoan int,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference nvarchar(500),
	@NamTN int,
	@HUONGDAN nvarchar(500),
	@PhanLuong varchar(50),
	@HuongdanPL nvarchar(500),
	@DiaChiDoanhNghiep nvarchar(256),
	@TenDonViDoiTac nvarchar(80),
	@TenDoanhNghiep nvarchar(80),
	@GhiChu nvarchar(2000),
	@PTTT_ID nvarchar(10),
	@TongTriGiaSP float,
	@TongTriGiaTienCong float,
	@IsGiaCongNguoc int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_HopDong] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_HopDong] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoHopDong] = @SoHopDong,
			[MaHaiQuan] = @MaHaiQuan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[MaDaiLy] = @MaDaiLy,
			[NgayKy] = @NgayKy,
			[NgayDangKy] = @NgayDangKy,
			[NgayHetHan] = @NgayHetHan,
			[NgayGiaHan] = @NgayGiaHan,
			[NuocThue_ID] = @NuocThue_ID,
			[NguyenTe_ID] = @NguyenTe_ID,
			[NgayTiepNhan] = @NgayTiepNhan,
			[DonViDoiTac] = @DonViDoiTac,
			[DiaChiDoiTac] = @DiaChiDoiTac,
			[CanBoTheoDoi] = @CanBoTheoDoi,
			[CanBoDuyet] = @CanBoDuyet,
			[TrangThaiThanhKhoan] = @TrangThaiThanhKhoan,
			[GUIDSTR] = @GUIDSTR,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua,
			[ActionStatus] = @ActionStatus,
			[GuidReference] = @GuidReference,
			[NamTN] = @NamTN,
			[HUONGDAN] = @HUONGDAN,
			[PhanLuong] = @PhanLuong,
			[HuongdanPL] = @HuongdanPL,
			[DiaChiDoanhNghiep] = @DiaChiDoanhNghiep,
			[TenDonViDoiTac] = @TenDonViDoiTac,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[GhiChu] = @GhiChu,
			[PTTT_ID] = @PTTT_ID,
			[TongTriGiaSP] = @TongTriGiaSP,
			[TongTriGiaTienCong] = @TongTriGiaTienCong,
			[IsGiaCongNguoc] = @IsGiaCongNguoc
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_HopDong]
		(
			[SoTiepNhan],
			[TrangThaiXuLy],
			[SoHopDong],
			[MaHaiQuan],
			[MaDoanhNghiep],
			[MaDaiLy],
			[NgayKy],
			[NgayDangKy],
			[NgayHetHan],
			[NgayGiaHan],
			[NuocThue_ID],
			[NguyenTe_ID],
			[NgayTiepNhan],
			[DonViDoiTac],
			[DiaChiDoiTac],
			[CanBoTheoDoi],
			[CanBoDuyet],
			[TrangThaiThanhKhoan],
			[GUIDSTR],
			[DeXuatKhac],
			[LyDoSua],
			[ActionStatus],
			[GuidReference],
			[NamTN],
			[HUONGDAN],
			[PhanLuong],
			[HuongdanPL],
			[DiaChiDoanhNghiep],
			[TenDonViDoiTac],
			[TenDoanhNghiep],
			[GhiChu],
			[PTTT_ID],
			[TongTriGiaSP],
			[TongTriGiaTienCong],
			[IsGiaCongNguoc]
		)
		VALUES 
		(
			@SoTiepNhan,
			@TrangThaiXuLy,
			@SoHopDong,
			@MaHaiQuan,
			@MaDoanhNghiep,
			@MaDaiLy,
			@NgayKy,
			@NgayDangKy,
			@NgayHetHan,
			@NgayGiaHan,
			@NuocThue_ID,
			@NguyenTe_ID,
			@NgayTiepNhan,
			@DonViDoiTac,
			@DiaChiDoiTac,
			@CanBoTheoDoi,
			@CanBoDuyet,
			@TrangThaiThanhKhoan,
			@GUIDSTR,
			@DeXuatKhac,
			@LyDoSua,
			@ActionStatus,
			@GuidReference,
			@NamTN,
			@HUONGDAN,
			@PhanLuong,
			@HuongdanPL,
			@DiaChiDoanhNghiep,
			@TenDonViDoiTac,
			@TenDoanhNghiep,
			@GhiChu,
			@PTTT_ID,
			@TongTriGiaSP,
			@TongTriGiaTienCong,
			@IsGiaCongNguoc
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HopDong_Delete]
-- Database: ECS_TQDT_GC_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HopDong_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_HopDong]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HopDong_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HopDong_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_HopDong] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HopDong_Load]
-- Database: ECS_TQDT_GC_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HopDong_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[TrangThaiXuLy],
	[SoHopDong],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaDaiLy],
	[NgayKy],
	[NgayDangKy],
	[NgayHetHan],
	[NgayGiaHan],
	[NuocThue_ID],
	[NguyenTe_ID],
	[NgayTiepNhan],
	[DonViDoiTac],
	[DiaChiDoiTac],
	[CanBoTheoDoi],
	[CanBoDuyet],
	[TrangThaiThanhKhoan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamTN],
	[HUONGDAN],
	[PhanLuong],
	[HuongdanPL],
	[DiaChiDoanhNghiep],
	[TenDonViDoiTac],
	[TenDoanhNghiep],
	[GhiChu],
	[PTTT_ID],
	[TongTriGiaSP],
	[TongTriGiaTienCong],
	[IsGiaCongNguoc]
FROM
	[dbo].[t_KDT_GC_HopDong]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HopDong_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HopDong_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[TrangThaiXuLy],
	[SoHopDong],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaDaiLy],
	[NgayKy],
	[NgayDangKy],
	[NgayHetHan],
	[NgayGiaHan],
	[NuocThue_ID],
	[NguyenTe_ID],
	[NgayTiepNhan],
	[DonViDoiTac],
	[DiaChiDoiTac],
	[CanBoTheoDoi],
	[CanBoDuyet],
	[TrangThaiThanhKhoan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamTN],
	[HUONGDAN],
	[PhanLuong],
	[HuongdanPL],
	[DiaChiDoanhNghiep],
	[TenDonViDoiTac],
	[TenDoanhNghiep],
	[GhiChu],
	[PTTT_ID],
	[TongTriGiaSP],
	[TongTriGiaTienCong],
	[IsGiaCongNguoc]
FROM [dbo].[t_KDT_GC_HopDong] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HopDong_SelectAll]
-- Database: ECS_TQDT_GC_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HopDong_SelectAll]




































AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[TrangThaiXuLy],
	[SoHopDong],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaDaiLy],
	[NgayKy],
	[NgayDangKy],
	[NgayHetHan],
	[NgayGiaHan],
	[NuocThue_ID],
	[NguyenTe_ID],
	[NgayTiepNhan],
	[DonViDoiTac],
	[DiaChiDoiTac],
	[CanBoTheoDoi],
	[CanBoDuyet],
	[TrangThaiThanhKhoan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamTN],
	[HUONGDAN],
	[PhanLuong],
	[HuongdanPL],
	[DiaChiDoanhNghiep],
	[TenDonViDoiTac],
	[TenDoanhNghiep],
	[GhiChu],
	[PTTT_ID],
	[TongTriGiaSP],
	[TongTriGiaTienCong],
	[IsGiaCongNguoc]
FROM
	[dbo].[t_KDT_GC_HopDong]	

GO
  
  
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_ToKhaiMauDich_SelectDynamicAndHMD') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_ToKhaiMauDich_SelectDynamicAndHMD
GO
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]    
-- Database: ECS_GC    
-- Author: Dang Phuoc Duy  
-- Time created: Tuesday, October 20, 2015  
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL nvarchar(3250)    
    
SET @SQL = 'SELECT    
  tkmd.ID,    
 [SoTiepNhan],    
 [NgayTiepNhan],    
 [MaHaiQuan],    
 [SoToKhai],    
 [MaLoaiHinh],    
 [NgayDangKy],    
 [MaDoanhNghiep],    
 [TenDoanhNghiep],    
 [MaDaiLyTTHQ],    
 [TenDaiLyTTHQ],    
 [TenDonViDoiTac],    
 [ChiTietDonViDoiTac],    
 [SoGiayPhep],    
 [NgayGiayPhep],    
 [NgayHetHanGiayPhep],    
 [SoHopDong],    
 [NgayHopDong],    
 [NgayHetHanHopDong],    
 [SoHoaDonThuongMai],    
 [NgayHoaDonThuongMai],    
 [PTVT_ID],    
 [SoHieuPTVT],    
 [NgayDenPTVT],    
 [QuocTichPTVT_ID],    
 [LoaiVanDon],    
 [SoVanDon],    
 [NgayVanDon],    
 [NuocXK_ID],    
 [NuocNK_ID],    
 [DiaDiemXepHang],    
 [CuaKhau_ID],    
 [DKGH_ID],    
 [NguyenTe_ID],    
 [TyGiaTinhThue],    
 [TyGiaUSD],    
 [PTTT_ID],    
 [SoHang],    
 [SoLuongPLTK],    
 [TenChuHang],    
 [ChucVu],    
 [SoContainer20],    
 [SoContainer40],    
 [SoKien],    
 [TongTriGiaKhaiBao],    
 [TongTriGiaTinhThue],    
 [LoaiToKhaiGiaCong],    
 [LePhiHaiQuan],    
 [PhiBaoHiem],    
 [PhiVanChuyen],    
 [PhiXepDoHang],    
 [PhiKhac],    
 [CanBoDangKy],    
 [QuanLyMay],    
 [TrangThaiXuLy],    
 [LoaiHangHoa],    
 [GiayTo],    
 [PhanLuong],    
 [MaDonViUT],    
 [TenDonViUT],    
 [TrongLuongNet],    
 [SoTienKhoan],    
 [IDHopDong],    
 [MaMid],    
 [Ngay_THN_THX],    
 [TrangThaiPhanBo],    
 [GUIDSTR],    
 [DeXuatKhac],    
 [LyDoSua],    
 [ActionStatus],    
 [GuidReference],    
 [NamDK],    
 [HUONGDAN] ,  
 hmd.SoThuTuHang,  
 hmd.MaPhu,
 hmd.MaHS,  
 hmd.TenHang,  
 hmd.NuocXX_ID,  
 hmd.SoLuong,  
 hmd.DonGiaKB,  
 hmd.TriGiaKB,  
 hmd.DonGiaTT,  
 hmd.TriGiaTT,  
 CASE WHEN tkmd.ID <>0 THEN (SELECT dvt.Ten FROM dbo.t_HaiQuan_DonViTinh dvt WHERE dvt.ID=hmd.DVT_ID) ELSE NULL END AS DVT  
 FROM [dbo].[t_KDT_ToKhaiMauDich] tkmd INNER JOIN dbo.t_KDT_HangMauDich hmd ON hmd.TKMD_ID = tkmd.ID  WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL    
GO
 
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_GC_PhieuXuatKho_SelectDynamic') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_GC_PhieuXuatKho_SelectDynamic
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_SelectDynamic]  
-- Database: ECS_TQDT_GC_V4_HT  
-- Author: Ngo Thanh Tung  
-- Time created: 26 August, 2014  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_SelectDynamic]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'  SELECT ID ,
         TKMD_ID ,
         NgayXuatKho ,
         SoPhieu ,
         SoHopDong ,
         KhachHang ,
         Style ,
         PO ,
         DonViNhan ,
         XuatTaiKho ,
         SoLuong ,
         DVTSoLuong,
		 ''0'' AS SoInvoice,
		 GETDATE() AS NgayInvoice FROM t_KDT_GC_PhieuXuatKho
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO
  
  
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_GC_ToKhaiChuyenTiep_SelectDynamicAndHCT') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_GC_ToKhaiChuyenTiep_SelectDynamicAndHCT
GO    
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]    
-- Database: ECS_GC    
-- Author: Dang Phuoc Duy  
-- Time created: Tuesday, October 20, 2015  
------------------------------------------------------------------------------------------------------------------------    
CREATE PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectDynamicAndHCT]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL nvarchar(3250)    
    
SET @SQL = 'SELECT TKCT.*,HCT.SoThuTuHang,HCT.MaHS,HCT.MaHang,HCT.TenHang,HCT.ID_NuocXX,HCT.SoLuong,HCT.DonGia,HCT.TriGia,HCT.DonGiaTT,HCT.TriGiaTT,CASE WHEN HCT.ID <>0 THEN (SELECT dvt.Ten FROM dbo.t_HaiQuan_DonViTinh dvt WHERE dvt.ID=HCT.ID_DVT) ELSE NULL END AS DVT FROM dbo.t_KDT_GC_ToKhaiChuyenTiep TKCT INNER JOIN dbo.t_KDT_GC_HangChuyenTiep HCT ON HCT.Master_ID = TKCT.ID WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL    
  
  
  

 
  
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_GC_DinhMucDangKy_SelectDynamic') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_GC_DinhMucDangKy_SelectDynamic
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_SelectDynamic]  
-- Database: ECS_TQDT_GC_V4  
-----------------------------------------------  
GO
CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectDynamic]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT   
 [ID],  
 [SoTiepNhan],  
 [TrangThaiXuLy],  
 [NgayTiepNhan],  
 [ID_HopDong],  
 [MaHaiQuan],  
 [MaDoanhNghiep],  
 [GUIDSTR],  
 [DeXuatKhac],  
 [LyDoSua],  
 [ActionStatus],  
 [GuidReference],  
 [NamTN],  
 [HUONGDAN],  
 [PhanLuong],  
 [Huongdan_PL]
FROM [dbo].[t_KDT_GC_DinhMucDangKy]   
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_GC_DinhMucDangKy_SelectDynamicFull') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_GC_DinhMucDangKy_SelectDynamicFull
GO
CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectDynamicFull]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT t_KDT_GC_DinhMucDangKy.ID ,
       SoTiepNhan ,
	   CASE TrangThaiXuLy WHEN 0 THEN N''Chờ duyệt''
						  WHEN 1 THEN N''Đã duyệt''
						  WHEN 2 THEN N''Không phê duyệt'' 
						  WHEN -1 THEN N''Chưa khai báo''
						  WHEN -2 THEN N''Chờ duyệt đã sửa chữa''
						  WHEN 5 THEN N''Đang sửa''
						  ELSE N''Hủy/Chờ hủy'' END AS TrangThaiXuLy,
       NgayTiepNhan ,
	   CASE WHEN ID_HopDong <> 0 THEN (SELECT TOP 1 SoHopDong FROM dbo.t_KDT_GC_HopDong WHERE ID = ID_HopDong) ELSE NULl END AS SoHopDong,
       MaHaiQuan ,
       MaDoanhNghiep ,
       DeXuatKhac ,
       LyDoSua ,
       NamTN ,
       t_KDT_GC_DinhMuc.ID ,
       MaSanPham ,
	   TenSanPham,
	   MaNguyenPhuLieu,
	   TenNPL,
	   CASE WHEN DVT_ID <> 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID = DVT_ID) ELSE DVT_ID END AS DVT,
       DinhMucSuDung ,
       TyLeHaoHut ,
       GhiChu ,
       STTHang ,
       NPL_TuCungUng 
	    FROM dbo.t_KDT_GC_DinhMucDangKy INNER JOIN dbo.t_KDT_GC_DinhMuc ON t_KDT_GC_DinhMuc.Master_ID = t_KDT_GC_DinhMucDangKy.ID WHERE
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL
GO
 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '25.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('25.0',GETDATE(), N'CẬP NHẬT HỢP ĐỒNG GIA CÔNG VÀ PROCEDURE XUẤT EXCEL ĐỊNH MỨC')
END