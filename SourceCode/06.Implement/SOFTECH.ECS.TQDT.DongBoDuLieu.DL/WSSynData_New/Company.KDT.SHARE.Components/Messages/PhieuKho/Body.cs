﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.PhieuKho
{
    
    [Serializable]
    [XmlRoot("Body")]
    public class Body
    {
        /// <summary>
        /// Nội dung
        /// </summary>
        [XmlElement("Content")]
        public Content Content { get; set; }

    }
}
