﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;

namespace CheckUser.Bll
{
    public class UserInfor
    {
        #region Properties.

        private int _ID;
        public int ID
        {
            set { this._ID = value; }
            get { return this._ID; }

        }
        private string _MaDonVi;
        public string MaDonVi
        {
            set { this._MaDonVi = value; }
            get { return this._MaDonVi; }

        }
        private string _TenDonVi;
        public string TenDonVi
        {
            set { this._TenDonVi = value; }
            get { return this._TenDonVi; }

        }
        private string _DiaChi;
        public string DiaChi
        {
            set { this._DiaChi = value; }
            get { return this._DiaChi; }

        }
        private string _SoDTLienHe;
        public string SoDTLienHe
        {
            set { this._SoDTLienHe = value; }
            get { return this._SoDTLienHe; }

        }
        private string _Email;
        public string Email
        {
            set { this._Email = value; }
            get { return this._Email; }

        }
        private DateTime _NgayKichHoat;
        public DateTime NgayKichHoat
        {
            set { this._NgayKichHoat = value; }
            get { return this._NgayKichHoat; }

        }
        private DateTime _NgayHetHan;
        public DateTime NgayHetHan
        {
            set { this._NgayHetHan = value; }
            get { return this._NgayHetHan; }

        }
        private string _MaMayKichHoat;
        public string MaMayKichHoat
        {
            set { this._MaMayKichHoat = value; }
            get { return this._MaMayKichHoat; }

        }
        private string _TenPhienBan;
        public string TenPhienBan
        {
            set { this._TenPhienBan = value; }
            get { return this._TenPhienBan; }

        }
        private string _TinhTrang;
        public string TinhTrang
        {
            set { this._TinhTrang = value; }
            get { return this._TinhTrang; }

        }
        private string _KeyLicense;
        public string KeyLicense
        {
            set { this._KeyLicense = value; }
            get { return this._KeyLicense; }

        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<UserInfor> ConvertToCollection(DataTable table)
        {
            IList<UserInfor> collection = new List<UserInfor>();
            foreach (DataRow row in table.Rows)
            {
                UserInfor entity = new UserInfor();
                entity.ID = Convert.ToInt32(row["ID"]);
                entity.MaDonVi = row["MaDonVi"].ToString();
                entity.TenDonVi = row["TenDonVi"].ToString();
                entity.DiaChi = row["DiaChi"].ToString();
                entity.SoDTLienHe = row["SoDTLienHe"].ToString();
                entity.Email = row["Email"].ToString();
                entity.NgayKichHoat = Convert.ToDateTime(row["NgayKichHoat"]);
                entity.NgayHetHan = Convert.ToDateTime(row["NgayHetHan"]);
                entity.MaMayKichHoat = row["MaMayKichHoat"].ToString();
                entity.TenPhienBan = row["TenPhienBan"].ToString();
                entity.TinhTrang = row["TinhTrang"].ToString();
                entity.KeyLicense = row["KeyLicense"].ToString();
                collection.Add(entity);
            }
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static UserInfor Load(int id)
        {
            const string spName = "[dbo].[p_User_Load]";
            SqlDataAdapter adapter = new SqlDataAdapter(spName, ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            adapter.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter parameterID = new SqlParameter("@ID", SqlDbType.Int);
            parameterID.Value = id;
            adapter.SelectCommand.Parameters.Add(parameterID);

            DataSet ds = new DataSet();
            adapter.Fill(ds);
            DataTable table = ds.Tables[0];
            IList<UserInfor> collection = ConvertToCollection(table);
            if (collection.Count > 0)
            {
                // Return 1 result.
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------

        public static IList<UserInfor> SelectCollectionAll()
        {
            DataSet ds = SelectAll();
            DataTable table = ds.Tables[0];
            return ConvertToCollection(table);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<UserInfor> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            DataSet ds = SelectDynamic(whereCondition, orderByExpression);
            DataTable table = ds.Tables[0];
            IList<UserInfor> ui = ConvertToCollection(table);
            return ui;
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_User_SelectAll]";
            SqlDataAdapter adapter = new SqlDataAdapter(spName, ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            adapter.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

            DataSet ds = new DataSet();
            adapter.Fill(ds);

            return ds;
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_User_SelectDynamic]";
            SqlDataAdapter adapter = new SqlDataAdapter(spName, ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            adapter.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter parameterWhereCondition = new SqlParameter("@WhereCondition", SqlDbType.NVarChar);
            parameterWhereCondition.Value = whereCondition;
            adapter.SelectCommand.Parameters.Add(parameterWhereCondition);

            SqlParameter parameterOrderByExpression = new SqlParameter("OrderByExpression", SqlDbType.NVarChar);
            parameterOrderByExpression.Value = orderByExpression;
            adapter.SelectCommand.Parameters.Add(parameterOrderByExpression);

            DataSet ds = new DataSet();
            adapter.Fill(ds);

            return ds;
        }

        #endregion
        //---------------------------------------------------------------------------------------------


        #region Insert methods.

        //---------------------------------------------------------------------------------------------

        public int Insert()
        {
            const string spName = "[dbo].[p_User_Insert]";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand command = new SqlCommand(spName, connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            // Parameter: @ID.
            SqlParameter parameterID = new SqlParameter("@ID", SqlDbType.Int, 4);
            parameterID.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameterID);

            // Parameter: @MaDonVi.
            SqlParameter parameterMaDonVi = new SqlParameter("@MaDonVi", SqlDbType.NVarChar);
            parameterMaDonVi.Value = MaDonVi;
            command.Parameters.Add(parameterMaDonVi);

            // Parameter: @TenDonVi.
            SqlParameter parameterTenDonVi = new SqlParameter("@TenDonVi", SqlDbType.NVarChar);
            parameterTenDonVi.Value = TenDonVi;
            command.Parameters.Add(parameterTenDonVi);

            // Parameter: @DiaChi.
            SqlParameter parameterDiaChi = new SqlParameter("@DiaChi", SqlDbType.NVarChar);
            parameterDiaChi.Value = DiaChi;
            command.Parameters.Add(parameterDiaChi);

            // Parameter: @SoDTLienHe.
            SqlParameter parameterSoDTLienHe = new SqlParameter("@SoDTLienHe", SqlDbType.NVarChar);
            parameterSoDTLienHe.Value = SoDTLienHe;
            command.Parameters.Add(parameterSoDTLienHe);

            // Parameter: @Email.
            SqlParameter parameterEmail = new SqlParameter("@Email", SqlDbType.NVarChar);
            parameterEmail.Value = Email;
            command.Parameters.Add(parameterEmail);

            // Parameter: @NgayKichHoat.
            SqlParameter parameterNgayKichHoat = new SqlParameter("@NgayKichHoat", SqlDbType.DateTime);
            parameterNgayKichHoat.Value = NgayKichHoat.Year <= 1753 ? DBNull.Value : (object)NgayKichHoat;
            command.Parameters.Add(parameterNgayKichHoat);

            // Parameter: @NgayHetHan.
            SqlParameter parameterNgayHetHan = new SqlParameter("@NgayHetHan", SqlDbType.DateTime);
            parameterNgayHetHan.Value = NgayHetHan.Year <= 1753 ? DBNull.Value : (object)NgayHetHan;
            command.Parameters.Add(parameterNgayHetHan);

            // Parameter: @MaMayKichHoat.
            SqlParameter parameterMaMayKichHoat = new SqlParameter("@MaMayKichHoat", SqlDbType.NVarChar);
            parameterMaMayKichHoat.Value = MaMayKichHoat;
            command.Parameters.Add(parameterMaMayKichHoat);

            // Parameter: @TenPhienBan.
            SqlParameter parameterTenPhienBan = new SqlParameter("@TenPhienBan", SqlDbType.NVarChar);
            parameterTenPhienBan.Value = TenPhienBan;
            command.Parameters.Add(parameterTenPhienBan);

            // Parameter: @TinhTrang.
            SqlParameter parameterTinhTrang = new SqlParameter("@TinhTrang", SqlDbType.NVarChar);
            parameterTinhTrang.Value = TinhTrang;
            command.Parameters.Add(parameterTinhTrang);

            // Parameter: @KeyLicense.
            SqlParameter parameterKeyLicense = new SqlParameter("@KeyLicense", SqlDbType.NVarChar);
            parameterKeyLicense.Value = KeyLicense;
            command.Parameters.Add(parameterKeyLicense);

            connection.Open();
            int rowAffected = command.ExecuteNonQuery();

            // Return Autonumber ID.
            ID = (int)command.Parameters["@ID"].Value;

            if (connection.State == ConnectionState.Open) connection.Close();

            return rowAffected;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            const string spName = "[dbo].[p_User_Update]";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand command = new SqlCommand(spName, connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            // Parameter: @ID.
            SqlParameter parameterID = new SqlParameter("@ID", SqlDbType.Int);
            parameterID.Value = ID;
            command.Parameters.Add(parameterID);

            // Parameter: @MaDonVi.
            SqlParameter parameterMaDonVi = new SqlParameter("@MaDonVi", SqlDbType.NVarChar);
            parameterMaDonVi.Value = MaDonVi;
            command.Parameters.Add(parameterMaDonVi);

            // Parameter: @TenDonVi.
            SqlParameter parameterTenDonVi = new SqlParameter("@TenDonVi", SqlDbType.NVarChar);
            parameterTenDonVi.Value = TenDonVi;
            command.Parameters.Add(parameterTenDonVi);

            // Parameter: @DiaChi.
            SqlParameter parameterDiaChi = new SqlParameter("@DiaChi", SqlDbType.NVarChar);
            parameterDiaChi.Value = DiaChi;
            command.Parameters.Add(parameterDiaChi);

            // Parameter: @SoDTLienHe.
            SqlParameter parameterSoDTLienHe = new SqlParameter("@SoDTLienHe", SqlDbType.NVarChar);
            parameterSoDTLienHe.Value = SoDTLienHe;
            command.Parameters.Add(parameterSoDTLienHe);

            // Parameter: @Email.
            SqlParameter parameterEmail = new SqlParameter("@Email", SqlDbType.NVarChar);
            parameterEmail.Value = Email;
            command.Parameters.Add(parameterEmail);

            // Parameter: @NgayKichHoat.
            SqlParameter parameterNgayKichHoat = new SqlParameter("@NgayKichHoat", SqlDbType.DateTime);
            parameterNgayKichHoat.Value = NgayKichHoat;
            command.Parameters.Add(parameterNgayKichHoat);

            // Parameter: @NgayHetHan.
            SqlParameter parameterNgayHetHan = new SqlParameter("@NgayHetHan", SqlDbType.DateTime);
            parameterNgayHetHan.Value = NgayHetHan;
            command.Parameters.Add(parameterNgayHetHan);

            // Parameter: @MaMayKichHoat.
            SqlParameter parameterMaMayKichHoat = new SqlParameter("@MaMayKichHoat", SqlDbType.NVarChar);
            parameterMaMayKichHoat.Value = MaMayKichHoat;
            command.Parameters.Add(parameterMaMayKichHoat);

            // Parameter: @TenPhienBan.
            SqlParameter parameterTenPhienBan = new SqlParameter("@TenPhienBan", SqlDbType.NVarChar);
            parameterTenPhienBan.Value = TenPhienBan;
            command.Parameters.Add(parameterTenPhienBan);

            // Parameter: @TinhTrang.
            SqlParameter parameterTinhTrang = new SqlParameter("@TinhTrang", SqlDbType.NVarChar);
            parameterTinhTrang.Value = TinhTrang;
            command.Parameters.Add(parameterTinhTrang);

            // Parameter: @KeyLicense.
            SqlParameter parameterKeyLicense = new SqlParameter("@KeyLicense", SqlDbType.NVarChar);
            parameterKeyLicense.Value = KeyLicense;
            command.Parameters.Add(parameterKeyLicense);

            connection.Open();
            int rowAffected = command.ExecuteNonQuery();

            if (connection.State == ConnectionState.Open) connection.Close();

            return rowAffected;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            const string spName = "[dbo].[p_User_Delete]";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand command = new SqlCommand(spName, connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter parameterID = new SqlParameter("@ID", SqlDbType.Int);
            parameterID.Value = ID;
            command.Parameters.Add(parameterID);

            connection.Open();
            int rowAffected = command.ExecuteNonQuery();

            if (connection.State == ConnectionState.Open) connection.Close();

            return rowAffected;
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_User_DeleteDynamic]";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand command = new SqlCommand(spName, connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter parameterWhereCondition = new SqlParameter("@WhereCondition", SqlDbType.NVarChar);
            parameterWhereCondition.Value = whereCondition;
            command.Parameters.Add(parameterWhereCondition);

            connection.Open();
            int rowAffected = command.ExecuteNonQuery();

            if (connection.State == ConnectionState.Open) connection.Close();

            return rowAffected;
        }

        //---------------------------------------------------------------------------------------------

        #endregion
    }
}
