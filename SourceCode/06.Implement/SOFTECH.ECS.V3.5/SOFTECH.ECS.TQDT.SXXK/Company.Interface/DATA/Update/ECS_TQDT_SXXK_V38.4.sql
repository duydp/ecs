GO
IF COL_LENGTH('t_KDT_VNACCS_TotalInventoryReport', 'LoaiSua') IS NULL
BEGIN
    ALTER TABLE dbo.t_KDT_VNACCS_TotalInventoryReport
	ADD   LoaiSua numeric (1, 0) NULL
END

GO
IF OBJECT_ID('t_KDT_VNACCS_TotalInventoryReport_Log') IS NOT NULL
DROP TABLE  dbo.t_KDT_VNACCS_TotalInventoryReport_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_TotalInventoryReport_Log]
(
[ID] [bigint] NOT NULL ,
[SoTiepNhan] [bigint] NOT NULL,
[NgayTiepNhan] [datetime] NOT NULL,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrangThaiXuLy] [int] NOT NULL,
[GuidString] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayChotTon] [datetime] NOT NULL,
[LoaiBaoCao] [numeric] (5, 0) NOT NULL,
[LoaiSua] [numeric] (1, 0) NULL,
[GhiChuKhac] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACCS_TotalInventoryReport_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACCS_TotalInventoryReport_Log
GO

CREATE TRIGGER trg_t_KDT_VNACCS_TotalInventoryReport_Log
ON t_KDT_VNACCS_TotalInventoryReport
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO t_KDT_VNACCS_TotalInventoryReport_Log
  SELECT *,GETDATE(),'Inserted' FROM Inserted
 END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
 BEGIN
        INSERT INTO t_KDT_VNACCS_TotalInventoryReport_Log
  SELECT *,GETDATE(),'Updated' FROM Inserted
 END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO t_KDT_VNACCS_TotalInventoryReport_Log
  SELECT *,GETDATE(),'Deleted' FROM Deleted
 END
END
GO

IF OBJECT_ID('t_KDT_VNACCS_TotalInventoryReport_Details_Log') IS NOT NULL
DROP TABLE  dbo.t_KDT_VNACCS_TotalInventoryReport_Details_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details_Log]
(
[ID] [bigint] NOT NULL ,
[ContractReference_ID] [bigint] NOT NULL,
[TenHangHoa] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHangHoa] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongTonKhoSoSach] [numeric] (18, 8) NOT NULL,
[SoLuongTonKhoThucTe] [numeric] (18, 4) NOT NULL,
[DVT] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACCS_TotalInventoryReport_Details_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACCS_TotalInventoryReport_Details_Log
GO

CREATE TRIGGER trg_t_KDT_VNACCS_TotalInventoryReport_Details_Log
ON t_KDT_VNACCS_TotalInventoryReport_Details
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO t_KDT_VNACCS_TotalInventoryReport_Details_Log
  SELECT *,GETDATE(),'Inserted' FROM Inserted
 END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
 BEGIN
        INSERT INTO t_KDT_VNACCS_TotalInventoryReport_Details_Log
  SELECT *,GETDATE(),'Updated' FROM Inserted
 END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO t_KDT_VNACCS_TotalInventoryReport_Details_Log
  SELECT *,GETDATE(),'Deleted' FROM Deleted
 END
END
GO

IF OBJECT_ID('t_KDT_VNACCS_ScrapInformation_Log') IS NOT NULL
DROP TABLE  dbo.t_KDT_VNACCS_ScrapInformation_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_ScrapInformation_Log]
(
[ID] [bigint] NOT NULL ,
[SoTiepNhan] [bigint] NOT NULL,
[NgayTiepNhan] [datetime] NOT NULL,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrangThaiXuLy] [int] NOT NULL,
[GuidString] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACCS_ScrapInformation_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACCS_ScrapInformation_Log
GO

CREATE TRIGGER trg_t_KDT_VNACCS_ScrapInformation_Log
ON dbo.t_KDT_VNACCS_ScrapInformation
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO t_KDT_VNACCS_ScrapInformation_Log
  SELECT *,GETDATE(),'Inserted' FROM Inserted
 END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
 BEGIN
        INSERT INTO t_KDT_VNACCS_ScrapInformation_Log
  SELECT *,GETDATE(),'Updated' FROM Inserted
 END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO t_KDT_VNACCS_ScrapInformation_Log
  SELECT *,GETDATE(),'Deleted' FROM Deleted
 END
END
GO

IF OBJECT_ID('t_KDT_VNACCS_ScrapInformation_Details_Log') IS NOT NULL
DROP TABLE  dbo.t_KDT_VNACCS_ScrapInformation_Details_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_ScrapInformation_Details_Log]
(
[ID] [bigint] NOT NULL ,
[Scrap_ID] [bigint] NOT NULL,
[MaHangHoa] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenHangHoa] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoaiHangHoa] [numeric] (2, 0) NOT NULL,
[SoLuong] [numeric] (18, 4) NOT NULL,
[DVT] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACCS_ScrapInformation_Details_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACCS_ScrapInformation_Details_Log
GO

CREATE TRIGGER trg_t_KDT_VNACCS_ScrapInformation_Details_Log
ON dbo.t_KDT_VNACCS_ScrapInformation_Details
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO t_KDT_VNACCS_ScrapInformation_Details_Log
  SELECT *,GETDATE(),'Inserted' FROM Inserted
 END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
 BEGIN
        INSERT INTO t_KDT_VNACCS_ScrapInformation_Details_Log
  SELECT *,GETDATE(),'Updated' FROM Inserted
 END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO t_KDT_VNACCS_ScrapInformation_Details_Log
  SELECT *,GETDATE(),'Deleted' FROM Deleted
 END
END
GO
GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '38.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('38.4',GETDATE(), N'CẬP NHẬT GHI LOG BC CHỐT TỒN - TT TIÊU HỦY')
END