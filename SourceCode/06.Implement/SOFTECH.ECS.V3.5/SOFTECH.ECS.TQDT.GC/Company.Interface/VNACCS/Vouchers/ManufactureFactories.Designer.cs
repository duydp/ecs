﻿namespace Company.Interface.VNACCS.Vouchers
{
    partial class ManufactureFactories
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListThanhVien_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListCongTyMe_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListNganhHangSXChinh_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem38 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem39 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem40 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem41 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem42 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem43 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem44 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem45 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem46 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgListCSSX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListNganhNghe_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListSPNLSX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem15 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem16 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem17 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem18 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem19 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgListSPCKSX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem20 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem21 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem22 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem23 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem24 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem25 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem26 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem27 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem28 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem29 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem30 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem31 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem32 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem33 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem34 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem35 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem36 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem37 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgListDDLGHH_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManufactureFactories));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem13 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem14 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgListAttachFile_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListKiemTra_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem47 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem48 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgListCTM_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDoiTac_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTTCSSX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListSPNLSXDT_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem49 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem50 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem51 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem52 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem53 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgListSPCKSXDT_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem54 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem55 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem56 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem57 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem58 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout grListHD_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSendEdit1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdChuyenTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenTT");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSendEdit = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdChuyenTT = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenTT");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox19 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox20 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListThanhVien = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox28 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteChiNhanh = new Janus.Windows.EditControls.UIButton();
            this.btnAddChiNhanh = new Janus.Windows.EditControls.UIButton();
            this.txtTenDoanhNghiepCN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiCN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtMaDoanhNghiepCN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.uiGroupBox21 = new Janus.Windows.EditControls.UIGroupBox();
            this.label97 = new System.Windows.Forms.Label();
            this.txtSoLuongChiNhanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label46 = new System.Windows.Forms.Label();
            this.uiGroupBox16 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox17 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListCongTyMe = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox27 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteCongTyMe = new Janus.Windows.EditControls.UIButton();
            this.btnAddCongTyMe = new Janus.Windows.EditControls.UIButton();
            this.txtDiaChiCSSXTV = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiepTV = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDoanhNghepTV = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.uiGroupBox18 = new Janus.Windows.EditControls.UIGroupBox();
            this.label96 = new System.Windows.Forms.Label();
            this.txtSoLuongThanhVien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTenCongTyMe = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtMaCongTyMe = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.uiGroupBox45 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTongSoLuongCSSX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongDiThueCSSX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongKhacCSSX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label131 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.txtSoLuongSoHuuCSSX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label137 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.uiGroupBox26 = new Janus.Windows.EditControls.UIGroupBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.txtSoLuongCongNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtBoPhanQuanLy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.uiGroupBox14 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListNganhHangSXChinh = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox15 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteNganhHangSXChinh = new Janus.Windows.EditControls.UIButton();
            this.btnAddNganhHangSXChinh = new Janus.Windows.EditControls.UIButton();
            this.label95 = new System.Windows.Forms.Label();
            this.cbbLoaiNganhNgheSX = new Janus.Windows.EditControls.UIComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox24 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListCSSX = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox12 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListNganhNghe = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox42 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox43 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListSPNLSX = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox44 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteSPNLSX = new Janus.Windows.EditControls.UIButton();
            this.btnAddSPNLSX = new Janus.Windows.EditControls.UIButton();
            this.ctrDVT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaHSNLSX = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtThoiGianSXTG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtMaSPNLSX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label116 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.txtSoLuongSP = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.cbbThoiGianSXDVT = new Janus.Windows.EditControls.UIComboBox();
            this.label130 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.uiGroupBox39 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox41 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListSPCKSX = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox38 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteSPCKSX = new Janus.Windows.EditControls.UIButton();
            this.btnAddSPCKSX = new Janus.Windows.EditControls.UIButton();
            this.ctrMaHSCK = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtMaSPCK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtChuKySXTG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.cbbCKSXDVT = new Janus.Windows.EditControls.UIComboBox();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.uiGroupBox40 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteNganhNghe = new Janus.Windows.EditControls.UIButton();
            this.cbbLoaiNganhNghe = new Janus.Windows.EditControls.UIComboBox();
            this.btnAddNganhNghe = new Janus.Windows.EditControls.UIButton();
            this.label38 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.label88 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.txtTongSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongDiThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongSoHuu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.txtNangLucSX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteCSSX = new Janus.Windows.EditControls.UIButton();
            this.btnAddCSSX = new Janus.Windows.EditControls.UIButton();
            this.label86 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtDiaChiCSSX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label107 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.cbbLoaiCSSX = new Janus.Windows.EditControls.UIComboBox();
            this.cbbDCTruSoCSSX = new Janus.Windows.EditControls.UIComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtSoLuongCN = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label106 = new System.Windows.Forms.Label();
            this.txtDienTichNX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label108 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox35 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox37 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListDDLGHH = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox36 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteDDLGHH = new Janus.Windows.EditControls.UIButton();
            this.btnAddDDLGHH = new Janus.Windows.EditControls.UIButton();
            this.txtTenDiaDiemLGHH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDiaDiemLGHH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.uiGroupBox22 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbKeToan = new Janus.Windows.EditControls.UIComboBox();
            this.cbbTronThue = new Janus.Windows.EditControls.UIComboBox();
            this.cbbBuonLau = new Janus.Windows.EditControls.UIComboBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.dateNgayCapGPGD = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.txtSoDTGD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoCMNDGD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label80 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.txtNoiDKHKGD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNoiCapGPGD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.dateNgayCapGPChuTich = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.txtSoDTChuTich = new Janus.Windows.GridEX.EditControls.EditBox();
            this.SoCMNDChuTich = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNoiDKHKChuTich = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNoiCapGPChuTich = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDoanhNghiepTruoc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLyDoChuyenDoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiepTruoc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayKetThucNTC = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ctrNuocXuatXu = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.cbbLoaiHinhDN = new Janus.Windows.EditControls.UIComboBox();
            this.cbbTruSoChinh = new Janus.Windows.EditControls.UIComboBox();
            this.txtTenDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiTruSoChinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNganhNgheSX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.uiGroupBox25 = new Janus.Windows.EditControls.UIGroupBox();
            this.dateNgayTN = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ctrCoQuanHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.label155 = new System.Windows.Forms.Label();
            this.cbbLoaiSua = new Janus.Windows.EditControls.UIComboBox();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSoTN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox29 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListAttachFile = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox60 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteFile = new Janus.Windows.EditControls.UIButton();
            this.btnViewFile = new Janus.Windows.EditControls.UIButton();
            this.btnAddFile = new Janus.Windows.EditControls.UIButton();
            this.label184 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.lblTotalSize = new System.Windows.Forms.Label();
            this.txtFileSize = new System.Windows.Forms.Label();
            this.lblFileName = new System.Windows.Forms.Label();
            this.uiGroupBox23 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox34 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListKiemTra = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox33 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteKiemTra = new Janus.Windows.EditControls.UIButton();
            this.btnAddKiemTra = new Janus.Windows.EditControls.UIButton();
            this.dateNgayKT = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.txtSoKetLuanKT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoBienBanKT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbDaKiemTra = new Janus.Windows.EditControls.UIComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox30 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListCTM = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox31 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteCTM = new Janus.Windows.EditControls.UIButton();
            this.btnAddCTM = new Janus.Windows.EditControls.UIButton();
            this.txtTenDoanhNghiepCTM = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiCSSXCTM = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label49 = new System.Windows.Forms.Label();
            this.txtMaDoanhNghiepCTM = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.uiGroupBox32 = new Janus.Windows.EditControls.UIGroupBox();
            this.label98 = new System.Windows.Forms.Label();
            this.txtSoLuongThanhVienCTM = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label52 = new System.Windows.Forms.Label();
            this.uiTabPage5 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox46 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox47 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListDoiTac = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox51 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox57 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox61 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListTTCSSX = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox58 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListSPNLSXDT = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox59 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteSPNLSXDT = new Janus.Windows.EditControls.UIButton();
            this.btnAddSPNLSXDT = new Janus.Windows.EditControls.UIButton();
            this.ctrDVTDT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaHSNLSXDT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtMaSPNLSXDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThoiGianSXTGDT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label157 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.txtSoLuongSPDT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.cbbThoiGianSXDVTDT = new Janus.Windows.EditControls.UIComboBox();
            this.label168 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.uiGroupBox54 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox55 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListSPCKSXDT = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox56 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteSPCKSXDT = new Janus.Windows.EditControls.UIButton();
            this.btnAddSPCKSXDT = new Janus.Windows.EditControls.UIButton();
            this.ctrMaHSCKSXDT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtMaSPCKSXDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtChuKySXTGDT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.cbbChuKySXDVTDT = new Janus.Windows.EditControls.UIComboBox();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.uiGroupBox53 = new Janus.Windows.EditControls.UIGroupBox();
            this.label94 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.txtNangLucSXDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTongSoLuongDT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongDiThueDT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongKhacDT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label144 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.txtSoLuongSoHuuDT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label150 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.uiGroupBox52 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteTTCSSX = new Janus.Windows.EditControls.UIButton();
            this.btnAddTTCSSX = new Janus.Windows.EditControls.UIButton();
            this.txtSoLuongCNDT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDienTichNXDT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label132 = new System.Windows.Forms.Label();
            this.txtDiaChiCSSXDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label142 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.uiGroupBox49 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListHD = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox50 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteHD = new Janus.Windows.EditControls.UIButton();
            this.btnAddHD = new Janus.Windows.EditControls.UIButton();
            this.clcNgayHH = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayHD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label177 = new System.Windows.Forms.Label();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label178 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.uiGroupBox48 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteCSSXDT = new Janus.Windows.EditControls.UIButton();
            this.btnAddCSSXDT = new Janus.Windows.EditControls.UIButton();
            this.txtTenDoanhNghiepDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiDoanhNghiepDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiepDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label174 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox19)).BeginInit();
            this.uiGroupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox20)).BeginInit();
            this.uiGroupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListThanhVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox28)).BeginInit();
            this.uiGroupBox28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox21)).BeginInit();
            this.uiGroupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).BeginInit();
            this.uiGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).BeginInit();
            this.uiGroupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListCongTyMe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox27)).BeginInit();
            this.uiGroupBox27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).BeginInit();
            this.uiGroupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox45)).BeginInit();
            this.uiGroupBox45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox26)).BeginInit();
            this.uiGroupBox26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).BeginInit();
            this.uiGroupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNganhHangSXChinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox15)).BeginInit();
            this.uiGroupBox15.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox24)).BeginInit();
            this.uiGroupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListCSSX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).BeginInit();
            this.uiGroupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNganhNghe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox42)).BeginInit();
            this.uiGroupBox42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox43)).BeginInit();
            this.uiGroupBox43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPNLSX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox44)).BeginInit();
            this.uiGroupBox44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox39)).BeginInit();
            this.uiGroupBox39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox41)).BeginInit();
            this.uiGroupBox41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPCKSX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox38)).BeginInit();
            this.uiGroupBox38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox40)).BeginInit();
            this.uiGroupBox40.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox35)).BeginInit();
            this.uiGroupBox35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox37)).BeginInit();
            this.uiGroupBox37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDDLGHH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox36)).BeginInit();
            this.uiGroupBox36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox22)).BeginInit();
            this.uiGroupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox25)).BeginInit();
            this.uiGroupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox29)).BeginInit();
            this.uiGroupBox29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListAttachFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox60)).BeginInit();
            this.uiGroupBox60.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox23)).BeginInit();
            this.uiGroupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox34)).BeginInit();
            this.uiGroupBox34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListKiemTra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox33)).BeginInit();
            this.uiGroupBox33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox30)).BeginInit();
            this.uiGroupBox30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListCTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox31)).BeginInit();
            this.uiGroupBox31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox32)).BeginInit();
            this.uiGroupBox32.SuspendLayout();
            this.uiTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox46)).BeginInit();
            this.uiGroupBox46.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox47)).BeginInit();
            this.uiGroupBox47.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDoiTac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox51)).BeginInit();
            this.uiGroupBox51.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox57)).BeginInit();
            this.uiGroupBox57.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox61)).BeginInit();
            this.uiGroupBox61.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTTCSSX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox58)).BeginInit();
            this.uiGroupBox58.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPNLSXDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox59)).BeginInit();
            this.uiGroupBox59.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox54)).BeginInit();
            this.uiGroupBox54.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox55)).BeginInit();
            this.uiGroupBox55.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPCKSXDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox56)).BeginInit();
            this.uiGroupBox56.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox53)).BeginInit();
            this.uiGroupBox53.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox52)).BeginInit();
            this.uiGroupBox52.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox49)).BeginInit();
            this.uiGroupBox49.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox50)).BeginInit();
            this.uiGroupBox50.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox48)).BeginInit();
            this.uiGroupBox48.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 920), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 35);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 920);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 896);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 896);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Location = new System.Drawing.Point(203, 35);
            this.grbMain.Size = new System.Drawing.Size(1180, 920);
            // 
            // errorProvider
            // 
            this.errorProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider.Icon")));
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSendEdit,
            this.cmdSave,
            this.cmdFeedback,
            this.cmdResult,
            this.cmdUpdateGuidString,
            this.cmdChuyenTT});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("db3f14b4-f472-45b2-95e5-5df0dc360a22");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.LockCommandBars = true;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend1,
            this.cmdSendEdit1,
            this.cmdSave1,
            this.cmdFeedback1,
            this.cmdResult1,
            this.cmdUpdateGuidString1,
            this.cmdChuyenTT1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(794, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdSendEdit1
            // 
            this.cmdSendEdit1.Key = "cmdSendEdit";
            this.cmdSendEdit1.Name = "cmdSendEdit1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdUpdateGuidString1
            // 
            this.cmdUpdateGuidString1.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString1.Name = "cmdUpdateGuidString1";
            this.cmdUpdateGuidString1.Text = "Cập nhật chuỗi phản hồi";
            // 
            // cmdChuyenTT1
            // 
            this.cmdChuyenTT1.Key = "cmdChuyenTT";
            this.cmdChuyenTT1.Name = "cmdChuyenTT1";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdSendEdit
            // 
            this.cmdSendEdit.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendEdit.Image")));
            this.cmdSendEdit.Key = "cmdSendEdit";
            this.cmdSendEdit.Name = "cmdSendEdit";
            this.cmdSendEdit.Text = "Khai báo sửa";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu lại";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback.Image")));
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "Nhận phản hồi";
            // 
            // cmdResult
            // 
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "Kết quả xử lý";
            // 
            // cmdUpdateGuidString
            // 
            this.cmdUpdateGuidString.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateGuidString.Image")));
            this.cmdUpdateGuidString.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Name = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Text = "Cập nhật chuỗi phản hồi";
            // 
            // cmdChuyenTT
            // 
            this.cmdChuyenTT.Image = ((System.Drawing.Image)(resources.GetObject("cmdChuyenTT.Image")));
            this.cmdChuyenTT.Key = "cmdChuyenTT";
            this.cmdChuyenTT.Name = "cmdChuyenTT";
            this.cmdChuyenTT.Text = "Chuyển Khai báo sửa";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1386, 32);
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Enabled = false;
            this.txtSoTiepNhan.Location = new System.Drawing.Point(159, 23);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(149, 20);
            this.txtSoTiepNhan.TabIndex = 24;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.AutoScroll = true;
            this.uiTabPage3.Controls.Add(this.uiGroupBox13);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1178, 898);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Ngàng hàng sản xuất và đơn vị thành viên";
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.AutoScroll = true;
            this.uiGroupBox13.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox13.Controls.Add(this.uiGroupBox19);
            this.uiGroupBox13.Controls.Add(this.uiGroupBox16);
            this.uiGroupBox13.Controls.Add(this.uiGroupBox45);
            this.uiGroupBox13.Controls.Add(this.uiGroupBox26);
            this.uiGroupBox13.Controls.Add(this.uiGroupBox14);
            this.uiGroupBox13.Controls.Add(this.uiGroupBox15);
            this.uiGroupBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox13.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox13.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(1161, 1061);
            this.uiGroupBox13.TabIndex = 19;
            this.uiGroupBox13.Text = "NGÀNH HÀNG SẢN XUẤT CHÍNH (TÍNH CHO TẤT CẢ CÁC CSSX NẾU TỔ CHỨC, CÁ NHÂN CÓ NHIỀU" +
                " CSSX)";
            this.uiGroupBox13.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox19
            // 
            this.uiGroupBox19.AutoScroll = true;
            this.uiGroupBox19.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox19.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(103)))));
            this.uiGroupBox19.Controls.Add(this.uiGroupBox20);
            this.uiGroupBox19.Controls.Add(this.uiGroupBox28);
            this.uiGroupBox19.Controls.Add(this.uiGroupBox21);
            this.uiGroupBox19.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox19.Location = new System.Drawing.Point(3, 828);
            this.uiGroupBox19.Name = "uiGroupBox19";
            this.uiGroupBox19.Size = new System.Drawing.Size(1138, 348);
            this.uiGroupBox19.TabIndex = 0;
            this.uiGroupBox19.Text = "Công ty thành viên nhập khẩu, cung ứng nguyên liệu, vật tư để sản xuất xuất khẩu " +
                "cho các đơn vị trực thuộc Công ty thành viên có CSSX";
            this.uiGroupBox19.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox20
            // 
            this.uiGroupBox20.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox20.Controls.Add(this.dgListThanhVien);
            this.uiGroupBox20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox20.Location = new System.Drawing.Point(3, 172);
            this.uiGroupBox20.Name = "uiGroupBox20";
            this.uiGroupBox20.Size = new System.Drawing.Size(1132, 173);
            this.uiGroupBox20.TabIndex = 15;
            this.uiGroupBox20.Text = "DANH SÁCH CHI NHÁNH";
            this.uiGroupBox20.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListThanhVien
            // 
            this.dgListThanhVien.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListThanhVien.AlternatingColors = true;
            this.dgListThanhVien.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListThanhVien.ColumnAutoResize = true;
            dgListThanhVien_DesignTimeLayout.LayoutString = resources.GetString("dgListThanhVien_DesignTimeLayout.LayoutString");
            this.dgListThanhVien.DesignTimeLayout = dgListThanhVien_DesignTimeLayout;
            this.dgListThanhVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListThanhVien.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListThanhVien.FrozenColumns = 3;
            this.dgListThanhVien.GroupByBoxVisible = false;
            this.dgListThanhVien.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListThanhVien.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListThanhVien.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListThanhVien.Location = new System.Drawing.Point(3, 17);
            this.dgListThanhVien.Margin = new System.Windows.Forms.Padding(0);
            this.dgListThanhVien.Name = "dgListThanhVien";
            this.dgListThanhVien.RecordNavigator = true;
            this.dgListThanhVien.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListThanhVien.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListThanhVien.Size = new System.Drawing.Size(1126, 153);
            this.dgListThanhVien.TabIndex = 14;
            this.dgListThanhVien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListThanhVien.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListThanhVien_RowDoubleClick);
            this.dgListThanhVien.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListThanhVien_LoadingRow);
            // 
            // uiGroupBox28
            // 
            this.uiGroupBox28.AutoScroll = true;
            this.uiGroupBox28.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox28.Controls.Add(this.btnDeleteChiNhanh);
            this.uiGroupBox28.Controls.Add(this.btnAddChiNhanh);
            this.uiGroupBox28.Controls.Add(this.txtTenDoanhNghiepCN);
            this.uiGroupBox28.Controls.Add(this.txtDiaChiCN);
            this.uiGroupBox28.Controls.Add(this.label40);
            this.uiGroupBox28.Controls.Add(this.txtMaDoanhNghiepCN);
            this.uiGroupBox28.Controls.Add(this.label41);
            this.uiGroupBox28.Controls.Add(this.label48);
            this.uiGroupBox28.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox28.Location = new System.Drawing.Point(3, 65);
            this.uiGroupBox28.Name = "uiGroupBox28";
            this.uiGroupBox28.Size = new System.Drawing.Size(1132, 107);
            this.uiGroupBox28.TabIndex = 0;
            this.uiGroupBox28.Text = "CHI NHÁNH";
            this.uiGroupBox28.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteChiNhanh
            // 
            this.btnDeleteChiNhanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteChiNhanh.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteChiNhanh.Image")));
            this.btnDeleteChiNhanh.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteChiNhanh.Location = new System.Drawing.Point(738, 72);
            this.btnDeleteChiNhanh.Name = "btnDeleteChiNhanh";
            this.btnDeleteChiNhanh.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteChiNhanh.TabIndex = 23;
            this.btnDeleteChiNhanh.Text = "Xóa";
            this.btnDeleteChiNhanh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteChiNhanh.Click += new System.EventHandler(this.btnDeleteChiNhanh_Click);
            // 
            // btnAddChiNhanh
            // 
            this.btnAddChiNhanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddChiNhanh.Image = ((System.Drawing.Image)(resources.GetObject("btnAddChiNhanh.Image")));
            this.btnAddChiNhanh.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddChiNhanh.Location = new System.Drawing.Point(642, 72);
            this.btnAddChiNhanh.Name = "btnAddChiNhanh";
            this.btnAddChiNhanh.Size = new System.Drawing.Size(90, 23);
            this.btnAddChiNhanh.TabIndex = 22;
            this.btnAddChiNhanh.Text = "Ghi";
            this.btnAddChiNhanh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddChiNhanh.Click += new System.EventHandler(this.btnAddChiNhanh_Click);
            // 
            // txtTenDoanhNghiepCN
            // 
            this.txtTenDoanhNghiepCN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoanhNghiepCN.Location = new System.Drawing.Point(141, 14);
            this.txtTenDoanhNghiepCN.Name = "txtTenDoanhNghiepCN";
            this.txtTenDoanhNghiepCN.Size = new System.Drawing.Size(465, 21);
            this.txtTenDoanhNghiepCN.TabIndex = 19;
            this.txtTenDoanhNghiepCN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiCN
            // 
            this.txtDiaChiCN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiCN.Location = new System.Drawing.Point(141, 74);
            this.txtDiaChiCN.Name = "txtDiaChiCN";
            this.txtDiaChiCN.Size = new System.Drawing.Size(465, 21);
            this.txtDiaChiCN.TabIndex = 21;
            this.txtDiaChiCN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(11, 82);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(95, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "Địa chỉ chi nhánh :";
            // 
            // txtMaDoanhNghiepCN
            // 
            this.txtMaDoanhNghiepCN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoanhNghiepCN.Location = new System.Drawing.Point(141, 43);
            this.txtMaDoanhNghiepCN.Name = "txtMaDoanhNghiepCN";
            this.txtMaDoanhNghiepCN.Size = new System.Drawing.Size(215, 21);
            this.txtMaDoanhNghiepCN.TabIndex = 20;
            this.txtMaDoanhNghiepCN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(11, 47);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(96, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "Mã doanh nghiệp :";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(10, 18);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(100, 13);
            this.label48.TabIndex = 0;
            this.label48.Text = "Tên doanh nghiệp :";
            // 
            // uiGroupBox21
            // 
            this.uiGroupBox21.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox21.Controls.Add(this.label97);
            this.uiGroupBox21.Controls.Add(this.txtSoLuongChiNhanh);
            this.uiGroupBox21.Controls.Add(this.label46);
            this.uiGroupBox21.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox21.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox21.Name = "uiGroupBox21";
            this.uiGroupBox21.Size = new System.Drawing.Size(1132, 48);
            this.uiGroupBox21.TabIndex = 0;
            this.uiGroupBox21.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Red;
            this.label97.Location = new System.Drawing.Point(362, 21);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(444, 13);
            this.label97.TabIndex = 0;
            this.label97.Text = "Chú ý : Số lượng chi nhánh phải bằng số lượng chi nhánh ở danh sách phía dưới";
            // 
            // txtSoLuongChiNhanh
            // 
            this.txtSoLuongChiNhanh.DecimalDigits = 20;
            this.txtSoLuongChiNhanh.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongChiNhanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongChiNhanh.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongChiNhanh.Location = new System.Drawing.Point(141, 17);
            this.txtSoLuongChiNhanh.MaxLength = 15;
            this.txtSoLuongChiNhanh.Name = "txtSoLuongChiNhanh";
            this.txtSoLuongChiNhanh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongChiNhanh.Size = new System.Drawing.Size(215, 21);
            this.txtSoLuongChiNhanh.TabIndex = 18;
            this.txtSoLuongChiNhanh.Text = "0";
            this.txtSoLuongChiNhanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongChiNhanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongChiNhanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(12, 21);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(105, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "Số lượng chi nhánh :";
            // 
            // uiGroupBox16
            // 
            this.uiGroupBox16.AutoScroll = true;
            this.uiGroupBox16.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox16.Controls.Add(this.uiGroupBox17);
            this.uiGroupBox16.Controls.Add(this.uiGroupBox27);
            this.uiGroupBox16.Controls.Add(this.uiGroupBox18);
            this.uiGroupBox16.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox16.Location = new System.Drawing.Point(3, 388);
            this.uiGroupBox16.Name = "uiGroupBox16";
            this.uiGroupBox16.Size = new System.Drawing.Size(1138, 440);
            this.uiGroupBox16.TabIndex = 0;
            this.uiGroupBox16.Text = "Công ty thành viên trực thuộc Công ty mẹ nhập khẩu, cung ứng nguyên liệu, vật tư " +
                "để sản xuất xuất khẩu cho các đơn vị thành viên khác trực thuộc Công ty mẹ ";
            this.uiGroupBox16.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox17
            // 
            this.uiGroupBox17.AutoScroll = true;
            this.uiGroupBox17.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox17.Controls.Add(this.dgListCongTyMe);
            this.uiGroupBox17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox17.Location = new System.Drawing.Point(3, 226);
            this.uiGroupBox17.Name = "uiGroupBox17";
            this.uiGroupBox17.Size = new System.Drawing.Size(1132, 211);
            this.uiGroupBox17.TabIndex = 15;
            this.uiGroupBox17.Text = "DANH SÁCH THÀNH VIÊN";
            this.uiGroupBox17.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListCongTyMe
            // 
            this.dgListCongTyMe.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListCongTyMe.AlternatingColors = true;
            this.dgListCongTyMe.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListCongTyMe.ColumnAutoResize = true;
            dgListCongTyMe_DesignTimeLayout.LayoutString = resources.GetString("dgListCongTyMe_DesignTimeLayout.LayoutString");
            this.dgListCongTyMe.DesignTimeLayout = dgListCongTyMe_DesignTimeLayout;
            this.dgListCongTyMe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListCongTyMe.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListCongTyMe.FrozenColumns = 3;
            this.dgListCongTyMe.GroupByBoxVisible = false;
            this.dgListCongTyMe.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListCongTyMe.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListCongTyMe.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListCongTyMe.Location = new System.Drawing.Point(3, 17);
            this.dgListCongTyMe.Margin = new System.Windows.Forms.Padding(0);
            this.dgListCongTyMe.Name = "dgListCongTyMe";
            this.dgListCongTyMe.RecordNavigator = true;
            this.dgListCongTyMe.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListCongTyMe.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListCongTyMe.Size = new System.Drawing.Size(1126, 191);
            this.dgListCongTyMe.TabIndex = 14;
            this.dgListCongTyMe.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListCongTyMe.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListCongTyMe_RowDoubleClick);
            // 
            // uiGroupBox27
            // 
            this.uiGroupBox27.AutoScroll = true;
            this.uiGroupBox27.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox27.Controls.Add(this.btnDeleteCongTyMe);
            this.uiGroupBox27.Controls.Add(this.btnAddCongTyMe);
            this.uiGroupBox27.Controls.Add(this.txtDiaChiCSSXTV);
            this.uiGroupBox27.Controls.Add(this.txtMaDoanhNghiepTV);
            this.uiGroupBox27.Controls.Add(this.txtTenDoanhNghepTV);
            this.uiGroupBox27.Controls.Add(this.label33);
            this.uiGroupBox27.Controls.Add(this.label34);
            this.uiGroupBox27.Controls.Add(this.label35);
            this.uiGroupBox27.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox27.Location = new System.Drawing.Point(3, 120);
            this.uiGroupBox27.Name = "uiGroupBox27";
            this.uiGroupBox27.Size = new System.Drawing.Size(1132, 106);
            this.uiGroupBox27.TabIndex = 0;
            this.uiGroupBox27.Text = "ĐƠN VỊ THÀNH VIÊN";
            this.uiGroupBox27.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteCongTyMe
            // 
            this.btnDeleteCongTyMe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteCongTyMe.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteCongTyMe.Image")));
            this.btnDeleteCongTyMe.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteCongTyMe.Location = new System.Drawing.Point(738, 75);
            this.btnDeleteCongTyMe.Name = "btnDeleteCongTyMe";
            this.btnDeleteCongTyMe.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteCongTyMe.TabIndex = 17;
            this.btnDeleteCongTyMe.Text = "Xóa";
            this.btnDeleteCongTyMe.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteCongTyMe.Click += new System.EventHandler(this.btnDeleteCongTyMe_Click);
            // 
            // btnAddCongTyMe
            // 
            this.btnAddCongTyMe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCongTyMe.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCongTyMe.Image")));
            this.btnAddCongTyMe.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddCongTyMe.Location = new System.Drawing.Point(642, 75);
            this.btnAddCongTyMe.Name = "btnAddCongTyMe";
            this.btnAddCongTyMe.Size = new System.Drawing.Size(90, 23);
            this.btnAddCongTyMe.TabIndex = 16;
            this.btnAddCongTyMe.Text = "Ghi";
            this.btnAddCongTyMe.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddCongTyMe.Click += new System.EventHandler(this.btnAddCongTyMe_Click);
            // 
            // txtDiaChiCSSXTV
            // 
            this.txtDiaChiCSSXTV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiCSSXTV.Location = new System.Drawing.Point(141, 76);
            this.txtDiaChiCSSXTV.Name = "txtDiaChiCSSXTV";
            this.txtDiaChiCSSXTV.Size = new System.Drawing.Size(465, 21);
            this.txtDiaChiCSSXTV.TabIndex = 15;
            this.txtDiaChiCSSXTV.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDoanhNghiepTV
            // 
            this.txtMaDoanhNghiepTV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoanhNghiepTV.Location = new System.Drawing.Point(141, 46);
            this.txtMaDoanhNghiepTV.Name = "txtMaDoanhNghiepTV";
            this.txtMaDoanhNghiepTV.Size = new System.Drawing.Size(146, 21);
            this.txtMaDoanhNghiepTV.TabIndex = 14;
            this.txtMaDoanhNghiepTV.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenDoanhNghepTV
            // 
            this.txtTenDoanhNghepTV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoanhNghepTV.Location = new System.Drawing.Point(141, 18);
            this.txtTenDoanhNghepTV.Name = "txtTenDoanhNghepTV";
            this.txtTenDoanhNghepTV.Size = new System.Drawing.Size(606, 21);
            this.txtTenDoanhNghepTV.TabIndex = 13;
            this.txtTenDoanhNghepTV.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(10, 80);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(74, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "Địa chỉ CSSX :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(10, 50);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(96, 13);
            this.label34.TabIndex = 0;
            this.label34.Text = "Mã doanh nghiệp :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(10, 22);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(100, 13);
            this.label35.TabIndex = 0;
            this.label35.Text = "Tên doanh nghiệp :";
            // 
            // uiGroupBox18
            // 
            this.uiGroupBox18.AutoScroll = true;
            this.uiGroupBox18.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox18.Controls.Add(this.label96);
            this.uiGroupBox18.Controls.Add(this.txtSoLuongThanhVien);
            this.uiGroupBox18.Controls.Add(this.txtTenCongTyMe);
            this.uiGroupBox18.Controls.Add(this.label45);
            this.uiGroupBox18.Controls.Add(this.txtMaCongTyMe);
            this.uiGroupBox18.Controls.Add(this.label44);
            this.uiGroupBox18.Controls.Add(this.label39);
            this.uiGroupBox18.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox18.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox18.Name = "uiGroupBox18";
            this.uiGroupBox18.Size = new System.Drawing.Size(1132, 103);
            this.uiGroupBox18.TabIndex = 0;
            this.uiGroupBox18.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Red;
            this.label96.Location = new System.Drawing.Point(305, 76);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(493, 13);
            this.label96.TabIndex = 0;
            this.label96.Text = "Chú ý : Số lượng thành viên phải bằng số lượng đơn vị thành viên ở danh sách phía" +
                " dưới";
            // 
            // txtSoLuongThanhVien
            // 
            this.txtSoLuongThanhVien.DecimalDigits = 20;
            this.txtSoLuongThanhVien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongThanhVien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongThanhVien.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongThanhVien.Location = new System.Drawing.Point(141, 72);
            this.txtSoLuongThanhVien.MaxLength = 15;
            this.txtSoLuongThanhVien.Name = "txtSoLuongThanhVien";
            this.txtSoLuongThanhVien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongThanhVien.Size = new System.Drawing.Size(146, 21);
            this.txtSoLuongThanhVien.TabIndex = 12;
            this.txtSoLuongThanhVien.Text = "0";
            this.txtSoLuongThanhVien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongThanhVien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongThanhVien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenCongTyMe
            // 
            this.txtTenCongTyMe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenCongTyMe.Location = new System.Drawing.Point(141, 13);
            this.txtTenCongTyMe.Name = "txtTenCongTyMe";
            this.txtTenCongTyMe.Size = new System.Drawing.Size(596, 21);
            this.txtTenCongTyMe.TabIndex = 10;
            this.txtTenCongTyMe.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 17);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(88, 13);
            this.label45.TabIndex = 0;
            this.label45.Text = "Tên công ty mẹ :";
            // 
            // txtMaCongTyMe
            // 
            this.txtMaCongTyMe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaCongTyMe.Location = new System.Drawing.Point(141, 42);
            this.txtMaCongTyMe.Name = "txtMaCongTyMe";
            this.txtMaCongTyMe.Size = new System.Drawing.Size(596, 21);
            this.txtMaCongTyMe.TabIndex = 11;
            this.txtMaCongTyMe.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 76);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(110, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "Số lượng thành viên :";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 46);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(84, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "Mã công ty mẹ :";
            // 
            // uiGroupBox45
            // 
            this.uiGroupBox45.AutoScroll = true;
            this.uiGroupBox45.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox45.Controls.Add(this.txtTongSoLuongCSSX);
            this.uiGroupBox45.Controls.Add(this.txtSoLuongDiThueCSSX);
            this.uiGroupBox45.Controls.Add(this.txtSoLuongKhacCSSX);
            this.uiGroupBox45.Controls.Add(this.label131);
            this.uiGroupBox45.Controls.Add(this.label134);
            this.uiGroupBox45.Controls.Add(this.label135);
            this.uiGroupBox45.Controls.Add(this.label136);
            this.uiGroupBox45.Controls.Add(this.txtSoLuongSoHuuCSSX);
            this.uiGroupBox45.Controls.Add(this.label137);
            this.uiGroupBox45.Controls.Add(this.label139);
            this.uiGroupBox45.Controls.Add(this.label140);
            this.uiGroupBox45.Controls.Add(this.label141);
            this.uiGroupBox45.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox45.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox45.Location = new System.Drawing.Point(3, 304);
            this.uiGroupBox45.Name = "uiGroupBox45";
            this.uiGroupBox45.Size = new System.Drawing.Size(1138, 84);
            this.uiGroupBox45.TabIndex = 0;
            this.uiGroupBox45.Text = "SỐ LƯỢNG MÁY MÓC DÂY TRUYỀN TRANG THIẾT BỊ (TÍNH CHO TẤT CẢ CÁC CSSX NẾU TỔ CHỨC," +
                " CÁ NHÂN CÓ NHIỀU CSSX)";
            this.uiGroupBox45.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtTongSoLuongCSSX
            // 
            this.txtTongSoLuongCSSX.DecimalDigits = 20;
            this.txtTongSoLuongCSSX.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongSoLuongCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoLuongCSSX.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongSoLuongCSSX.Location = new System.Drawing.Point(475, 52);
            this.txtTongSoLuongCSSX.MaxLength = 15;
            this.txtTongSoLuongCSSX.Name = "txtTongSoLuongCSSX";
            this.txtTongSoLuongCSSX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoLuongCSSX.Size = new System.Drawing.Size(177, 21);
            this.txtTongSoLuongCSSX.TabIndex = 9;
            this.txtTongSoLuongCSSX.Text = "0";
            this.txtTongSoLuongCSSX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoLuongCSSX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoLuongCSSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongSoLuongCSSX.Leave += new System.EventHandler(this.txtTongSoLuongCSSX_Leave);
            // 
            // txtSoLuongDiThueCSSX
            // 
            this.txtSoLuongDiThueCSSX.DecimalDigits = 20;
            this.txtSoLuongDiThueCSSX.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongDiThueCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongDiThueCSSX.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongDiThueCSSX.Location = new System.Drawing.Point(475, 20);
            this.txtSoLuongDiThueCSSX.MaxLength = 15;
            this.txtSoLuongDiThueCSSX.Name = "txtSoLuongDiThueCSSX";
            this.txtSoLuongDiThueCSSX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongDiThueCSSX.Size = new System.Drawing.Size(177, 21);
            this.txtSoLuongDiThueCSSX.TabIndex = 7;
            this.txtSoLuongDiThueCSSX.Text = "0";
            this.txtSoLuongDiThueCSSX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongDiThueCSSX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongDiThueCSSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongDiThueCSSX.Leave += new System.EventHandler(this.txtSoLuongDiThueCSSX_Leave);
            // 
            // txtSoLuongKhacCSSX
            // 
            this.txtSoLuongKhacCSSX.DecimalDigits = 20;
            this.txtSoLuongKhacCSSX.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongKhacCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongKhacCSSX.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongKhacCSSX.Location = new System.Drawing.Point(141, 52);
            this.txtSoLuongKhacCSSX.MaxLength = 15;
            this.txtSoLuongKhacCSSX.Name = "txtSoLuongKhacCSSX";
            this.txtSoLuongKhacCSSX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongKhacCSSX.Size = new System.Drawing.Size(146, 21);
            this.txtSoLuongKhacCSSX.TabIndex = 8;
            this.txtSoLuongKhacCSSX.Text = "0";
            this.txtSoLuongKhacCSSX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongKhacCSSX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongKhacCSSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongKhacCSSX.Leave += new System.EventHandler(this.txtSoLuongKhacCSSX_Leave);
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.ForeColor = System.Drawing.Color.Red;
            this.label131.Location = new System.Drawing.Point(290, 54);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(16, 16);
            this.label131.TabIndex = 0;
            this.label131.Text = "*";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.ForeColor = System.Drawing.Color.Red;
            this.label134.Location = new System.Drawing.Point(655, 54);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(16, 16);
            this.label134.TabIndex = 0;
            this.label134.Text = "*";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.ForeColor = System.Drawing.Color.Red;
            this.label135.Location = new System.Drawing.Point(655, 22);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(16, 16);
            this.label135.TabIndex = 0;
            this.label135.Text = "*";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.ForeColor = System.Drawing.Color.Red;
            this.label136.Location = new System.Drawing.Point(290, 22);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(16, 16);
            this.label136.TabIndex = 0;
            this.label136.Text = "*";
            // 
            // txtSoLuongSoHuuCSSX
            // 
            this.txtSoLuongSoHuuCSSX.DecimalDigits = 20;
            this.txtSoLuongSoHuuCSSX.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongSoHuuCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongSoHuuCSSX.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongSoHuuCSSX.Location = new System.Drawing.Point(141, 20);
            this.txtSoLuongSoHuuCSSX.MaxLength = 15;
            this.txtSoLuongSoHuuCSSX.Name = "txtSoLuongSoHuuCSSX";
            this.txtSoLuongSoHuuCSSX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongSoHuuCSSX.Size = new System.Drawing.Size(146, 21);
            this.txtSoLuongSoHuuCSSX.TabIndex = 6;
            this.txtSoLuongSoHuuCSSX.Text = "0";
            this.txtSoLuongSoHuuCSSX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongSoHuuCSSX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongSoHuuCSSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongSoHuuCSSX.Leave += new System.EventHandler(this.txtSoLuongSoHuuCSSX_Leave);
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(335, 24);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(92, 13);
            this.label137.TabIndex = 0;
            this.label137.Text = "Số lượng đi thuê :";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(17, 24);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(92, 13);
            this.label139.TabIndex = 0;
            this.label139.Text = "Số lượng sở hữu :";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(335, 56);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(82, 13);
            this.label140.TabIndex = 0;
            this.label140.Text = "Tổng số lượng :";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(17, 56);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(81, 13);
            this.label141.TabIndex = 0;
            this.label141.Text = "Số lượng khác :";
            // 
            // uiGroupBox26
            // 
            this.uiGroupBox26.AutoScroll = true;
            this.uiGroupBox26.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox26.Controls.Add(this.label62);
            this.uiGroupBox26.Controls.Add(this.label56);
            this.uiGroupBox26.Controls.Add(this.txtSoLuongCongNhan);
            this.uiGroupBox26.Controls.Add(this.txtBoPhanQuanLy);
            this.uiGroupBox26.Controls.Add(this.label28);
            this.uiGroupBox26.Controls.Add(this.label29);
            this.uiGroupBox26.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox26.Location = new System.Drawing.Point(3, 256);
            this.uiGroupBox26.Name = "uiGroupBox26";
            this.uiGroupBox26.Size = new System.Drawing.Size(1138, 48);
            this.uiGroupBox26.TabIndex = 0;
            this.uiGroupBox26.Text = "TÌNH HÌNH NHÂN LỰC (TÍNH CHO TẤT CẢ CÁC CSSX NẾU TỔ CHỨC CÁ NHÂN CÓ NHIỀU CSSX)";
            this.uiGroupBox26.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Red;
            this.label62.Location = new System.Drawing.Point(290, 20);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(16, 16);
            this.label62.TabIndex = 0;
            this.label62.Text = "*";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Red;
            this.label56.Location = new System.Drawing.Point(655, 20);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(16, 16);
            this.label56.TabIndex = 0;
            this.label56.Text = "*";
            // 
            // txtSoLuongCongNhan
            // 
            this.txtSoLuongCongNhan.DecimalDigits = 20;
            this.txtSoLuongCongNhan.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongCongNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongCongNhan.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongCongNhan.Location = new System.Drawing.Point(475, 18);
            this.txtSoLuongCongNhan.MaxLength = 15;
            this.txtSoLuongCongNhan.Name = "txtSoLuongCongNhan";
            this.txtSoLuongCongNhan.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongCongNhan.Size = new System.Drawing.Size(177, 21);
            this.txtSoLuongCongNhan.TabIndex = 5;
            this.txtSoLuongCongNhan.Text = "0";
            this.txtSoLuongCongNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongCongNhan.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongCongNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtBoPhanQuanLy
            // 
            this.txtBoPhanQuanLy.DecimalDigits = 20;
            this.txtBoPhanQuanLy.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtBoPhanQuanLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoPhanQuanLy.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtBoPhanQuanLy.Location = new System.Drawing.Point(141, 18);
            this.txtBoPhanQuanLy.MaxLength = 15;
            this.txtBoPhanQuanLy.Name = "txtBoPhanQuanLy";
            this.txtBoPhanQuanLy.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBoPhanQuanLy.Size = new System.Drawing.Size(146, 21);
            this.txtBoPhanQuanLy.TabIndex = 4;
            this.txtBoPhanQuanLy.Text = "0";
            this.txtBoPhanQuanLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtBoPhanQuanLy.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtBoPhanQuanLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(335, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(109, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "Số lượng công nhân :";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(19, 22);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(91, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Bộ phận quản lý :";
            // 
            // uiGroupBox14
            // 
            this.uiGroupBox14.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox14.Controls.Add(this.dgListNganhHangSXChinh);
            this.uiGroupBox14.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox14.Location = new System.Drawing.Point(3, 59);
            this.uiGroupBox14.Name = "uiGroupBox14";
            this.uiGroupBox14.Size = new System.Drawing.Size(1138, 197);
            this.uiGroupBox14.TabIndex = 15;
            this.uiGroupBox14.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListNganhHangSXChinh
            // 
            this.dgListNganhHangSXChinh.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNganhHangSXChinh.AlternatingColors = true;
            this.dgListNganhHangSXChinh.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNganhHangSXChinh.ColumnAutoResize = true;
            dgListNganhHangSXChinh_DesignTimeLayout.LayoutString = resources.GetString("dgListNganhHangSXChinh_DesignTimeLayout.LayoutString");
            this.dgListNganhHangSXChinh.DesignTimeLayout = dgListNganhHangSXChinh_DesignTimeLayout;
            this.dgListNganhHangSXChinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNganhHangSXChinh.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNganhHangSXChinh.FrozenColumns = 3;
            this.dgListNganhHangSXChinh.GroupByBoxVisible = false;
            this.dgListNganhHangSXChinh.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNganhHangSXChinh.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNganhHangSXChinh.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNganhHangSXChinh.Location = new System.Drawing.Point(3, 8);
            this.dgListNganhHangSXChinh.Margin = new System.Windows.Forms.Padding(0);
            this.dgListNganhHangSXChinh.Name = "dgListNganhHangSXChinh";
            this.dgListNganhHangSXChinh.RecordNavigator = true;
            this.dgListNganhHangSXChinh.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNganhHangSXChinh.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNganhHangSXChinh.Size = new System.Drawing.Size(1132, 186);
            this.dgListNganhHangSXChinh.TabIndex = 13;
            this.dgListNganhHangSXChinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListNganhHangSXChinh.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListNganhHangSXChinh_RowDoubleClick);
            this.dgListNganhHangSXChinh.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListNganhHangSXChinh_LoadingRow);
            // 
            // uiGroupBox15
            // 
            this.uiGroupBox15.AutoScroll = true;
            this.uiGroupBox15.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox15.Controls.Add(this.btnDeleteNganhHangSXChinh);
            this.uiGroupBox15.Controls.Add(this.btnAddNganhHangSXChinh);
            this.uiGroupBox15.Controls.Add(this.label95);
            this.uiGroupBox15.Controls.Add(this.cbbLoaiNganhNgheSX);
            this.uiGroupBox15.Controls.Add(this.label37);
            this.uiGroupBox15.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox15.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox15.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox15.Name = "uiGroupBox15";
            this.uiGroupBox15.Size = new System.Drawing.Size(1138, 42);
            this.uiGroupBox15.TabIndex = 0;
            this.uiGroupBox15.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteNganhHangSXChinh
            // 
            this.btnDeleteNganhHangSXChinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteNganhHangSXChinh.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteNganhHangSXChinh.Image")));
            this.btnDeleteNganhHangSXChinh.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteNganhHangSXChinh.Location = new System.Drawing.Point(396, 12);
            this.btnDeleteNganhHangSXChinh.Name = "btnDeleteNganhHangSXChinh";
            this.btnDeleteNganhHangSXChinh.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteNganhHangSXChinh.TabIndex = 3;
            this.btnDeleteNganhHangSXChinh.Text = "Xóa";
            this.btnDeleteNganhHangSXChinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteNganhHangSXChinh.Click += new System.EventHandler(this.btnDeleteNganhHangSXChinh_Click);
            // 
            // btnAddNganhHangSXChinh
            // 
            this.btnAddNganhHangSXChinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNganhHangSXChinh.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNganhHangSXChinh.Image")));
            this.btnAddNganhHangSXChinh.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddNganhHangSXChinh.Location = new System.Drawing.Point(300, 12);
            this.btnAddNganhHangSXChinh.Name = "btnAddNganhHangSXChinh";
            this.btnAddNganhHangSXChinh.Size = new System.Drawing.Size(90, 23);
            this.btnAddNganhHangSXChinh.TabIndex = 2;
            this.btnAddNganhHangSXChinh.Text = "Lưu";
            this.btnAddNganhHangSXChinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddNganhHangSXChinh.Click += new System.EventHandler(this.btnAddNganhHangSXChinh_Click);
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Red;
            this.label95.Location = new System.Drawing.Point(274, 15);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(16, 16);
            this.label95.TabIndex = 0;
            this.label95.Text = "*";
            // 
            // cbbLoaiNganhNgheSX
            // 
            this.cbbLoaiNganhNgheSX.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiNganhNgheSX.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem38.FormatStyle.Alpha = 0;
            uiComboBoxItem38.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem38.IsSeparator = false;
            uiComboBoxItem38.Text = "Da giầy";
            uiComboBoxItem38.Value = "1";
            uiComboBoxItem39.FormatStyle.Alpha = 0;
            uiComboBoxItem39.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem39.IsSeparator = false;
            uiComboBoxItem39.Text = "May mặc";
            uiComboBoxItem39.Value = "2";
            uiComboBoxItem40.FormatStyle.Alpha = 0;
            uiComboBoxItem40.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem40.IsSeparator = false;
            uiComboBoxItem40.Text = "Điện tử, điện lạnh";
            uiComboBoxItem40.Value = "3";
            uiComboBoxItem41.FormatStyle.Alpha = 0;
            uiComboBoxItem41.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem41.IsSeparator = false;
            uiComboBoxItem41.Text = "Chế biến thực phẩm";
            uiComboBoxItem41.Value = "4";
            uiComboBoxItem42.FormatStyle.Alpha = 0;
            uiComboBoxItem42.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem42.IsSeparator = false;
            uiComboBoxItem42.Text = "Cơ khí";
            uiComboBoxItem42.Value = "5";
            uiComboBoxItem43.FormatStyle.Alpha = 0;
            uiComboBoxItem43.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem43.IsSeparator = false;
            uiComboBoxItem43.Text = "Gỗ";
            uiComboBoxItem43.Value = "6";
            uiComboBoxItem44.FormatStyle.Alpha = 0;
            uiComboBoxItem44.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem44.IsSeparator = false;
            uiComboBoxItem44.Text = "Nhựa";
            uiComboBoxItem44.Value = "7";
            uiComboBoxItem45.FormatStyle.Alpha = 0;
            uiComboBoxItem45.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem45.IsSeparator = false;
            uiComboBoxItem45.Text = "Nông sản";
            uiComboBoxItem45.Value = "8";
            uiComboBoxItem46.FormatStyle.Alpha = 0;
            uiComboBoxItem46.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem46.IsSeparator = false;
            uiComboBoxItem46.Text = "Loại khác";
            uiComboBoxItem46.Value = "9";
            this.cbbLoaiNganhNgheSX.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem38,
            uiComboBoxItem39,
            uiComboBoxItem40,
            uiComboBoxItem41,
            uiComboBoxItem42,
            uiComboBoxItem43,
            uiComboBoxItem44,
            uiComboBoxItem45,
            uiComboBoxItem46});
            this.cbbLoaiNganhNgheSX.Location = new System.Drawing.Point(141, 13);
            this.cbbLoaiNganhNgheSX.Name = "cbbLoaiNganhNgheSX";
            this.cbbLoaiNganhNgheSX.Size = new System.Drawing.Size(127, 21);
            this.cbbLoaiNganhNgheSX.TabIndex = 1;
            this.cbbLoaiNganhNgheSX.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(12, 17);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(93, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "Loại ngành nghề :";
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.AutoScroll = true;
            this.uiTabPage2.Controls.Add(this.uiGroupBox8);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1178, 898);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thông tin cơ sở sản xuất và ngành nghề";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.AutoScroll = true;
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(103)))));
            this.uiGroupBox8.Controls.Add(this.uiGroupBox24);
            this.uiGroupBox8.Controls.Add(this.uiGroupBox12);
            this.uiGroupBox8.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox8.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox8.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(1161, 1432);
            this.uiGroupBox8.TabIndex = 0;
            this.uiGroupBox8.Text = "I. Thông tin cơ sở sản xuất";
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox24
            // 
            this.uiGroupBox24.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox24.Controls.Add(this.dgListCSSX);
            this.uiGroupBox24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox24.Location = new System.Drawing.Point(3, 1309);
            this.uiGroupBox24.Name = "uiGroupBox24";
            this.uiGroupBox24.Size = new System.Drawing.Size(1155, 120);
            this.uiGroupBox24.TabIndex = 36;
            this.uiGroupBox24.Tag = "";
            this.uiGroupBox24.Text = "III. DANH SÁCH CƠ SỞ SẢN XUẤT";
            this.uiGroupBox24.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListCSSX
            // 
            this.dgListCSSX.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListCSSX.AlternatingColors = true;
            this.dgListCSSX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListCSSX.ColumnAutoResize = true;
            dgListCSSX_DesignTimeLayout.LayoutString = resources.GetString("dgListCSSX_DesignTimeLayout.LayoutString");
            this.dgListCSSX.DesignTimeLayout = dgListCSSX_DesignTimeLayout;
            this.dgListCSSX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListCSSX.FrozenColumns = 3;
            this.dgListCSSX.GroupByBoxVisible = false;
            this.dgListCSSX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListCSSX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListCSSX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListCSSX.Location = new System.Drawing.Point(3, 17);
            this.dgListCSSX.Margin = new System.Windows.Forms.Padding(0);
            this.dgListCSSX.Name = "dgListCSSX";
            this.dgListCSSX.RecordNavigator = true;
            this.dgListCSSX.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListCSSX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListCSSX.Size = new System.Drawing.Size(1149, 100);
            this.dgListCSSX.TabIndex = 14;
            this.dgListCSSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListCSSX.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListCSSX_RowDoubleClick);
            this.dgListCSSX.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListCSSX_LoadingRow);
            // 
            // uiGroupBox12
            // 
            this.uiGroupBox12.AutoScroll = true;
            this.uiGroupBox12.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox12.Controls.Add(this.uiGroupBox11);
            this.uiGroupBox12.Controls.Add(this.uiGroupBox42);
            this.uiGroupBox12.Controls.Add(this.uiGroupBox39);
            this.uiGroupBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox12.Location = new System.Drawing.Point(3, 297);
            this.uiGroupBox12.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox12.Name = "uiGroupBox12";
            this.uiGroupBox12.Size = new System.Drawing.Size(1155, 1012);
            this.uiGroupBox12.TabIndex = 0;
            this.uiGroupBox12.Text = "3. NGHÀNH NGHỀ";
            this.uiGroupBox12.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.AutoScroll = true;
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.dgListNganhNghe);
            this.uiGroupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox11.Location = new System.Drawing.Point(3, 796);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(1149, 213);
            this.uiGroupBox11.TabIndex = 45;
            this.uiGroupBox11.Text = "II. DANH SÁCH NGÀNH NGHỀ";
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListNganhNghe
            // 
            this.dgListNganhNghe.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNganhNghe.AlternatingColors = true;
            this.dgListNganhNghe.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNganhNghe.ColumnAutoResize = true;
            dgListNganhNghe_DesignTimeLayout.LayoutString = resources.GetString("dgListNganhNghe_DesignTimeLayout.LayoutString");
            this.dgListNganhNghe.DesignTimeLayout = dgListNganhNghe_DesignTimeLayout;
            this.dgListNganhNghe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNganhNghe.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNganhNghe.FrozenColumns = 3;
            this.dgListNganhNghe.GroupByBoxVisible = false;
            this.dgListNganhNghe.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNganhNghe.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNganhNghe.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNganhNghe.Location = new System.Drawing.Point(3, 17);
            this.dgListNganhNghe.Margin = new System.Windows.Forms.Padding(0);
            this.dgListNganhNghe.Name = "dgListNganhNghe";
            this.dgListNganhNghe.RecordNavigator = true;
            this.dgListNganhNghe.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNganhNghe.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNganhNghe.Size = new System.Drawing.Size(1143, 193);
            this.dgListNganhNghe.TabIndex = 12;
            this.dgListNganhNghe.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListNganhNghe.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListNganhNghe_RowDoubleClick);
            this.dgListNganhNghe.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListNganhNghe_LoadingRow);
            // 
            // uiGroupBox42
            // 
            this.uiGroupBox42.AutoScroll = true;
            this.uiGroupBox42.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox42.Controls.Add(this.uiGroupBox43);
            this.uiGroupBox42.Controls.Add(this.uiGroupBox44);
            this.uiGroupBox42.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox42.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox42.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox42.Location = new System.Drawing.Point(3, 416);
            this.uiGroupBox42.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox42.Name = "uiGroupBox42";
            this.uiGroupBox42.Size = new System.Drawing.Size(1149, 380);
            this.uiGroupBox42.TabIndex = 44;
            this.uiGroupBox42.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox43
            // 
            this.uiGroupBox43.AutoScroll = true;
            this.uiGroupBox43.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox43.Controls.Add(this.dgListSPNLSX);
            this.uiGroupBox43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox43.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox43.Location = new System.Drawing.Point(3, 120);
            this.uiGroupBox43.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox43.Name = "uiGroupBox43";
            this.uiGroupBox43.Size = new System.Drawing.Size(1143, 257);
            this.uiGroupBox43.TabIndex = 44;
            this.uiGroupBox43.Text = "3.1.2.1 DANH SÁCH SẢN PHẨM";
            this.uiGroupBox43.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListSPNLSX
            // 
            this.dgListSPNLSX.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListSPNLSX.AlternatingColors = true;
            this.dgListSPNLSX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListSPNLSX.ColumnAutoResize = true;
            dgListSPNLSX_DesignTimeLayout.LayoutString = resources.GetString("dgListSPNLSX_DesignTimeLayout.LayoutString");
            this.dgListSPNLSX.DesignTimeLayout = dgListSPNLSX_DesignTimeLayout;
            this.dgListSPNLSX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListSPNLSX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListSPNLSX.FrozenColumns = 3;
            this.dgListSPNLSX.GroupByBoxVisible = false;
            this.dgListSPNLSX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPNLSX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPNLSX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListSPNLSX.Location = new System.Drawing.Point(3, 17);
            this.dgListSPNLSX.Margin = new System.Windows.Forms.Padding(0);
            this.dgListSPNLSX.Name = "dgListSPNLSX";
            this.dgListSPNLSX.RecordNavigator = true;
            this.dgListSPNLSX.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListSPNLSX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListSPNLSX.Size = new System.Drawing.Size(1137, 237);
            this.dgListSPNLSX.TabIndex = 16;
            this.dgListSPNLSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListSPNLSX.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListSPNLSX_RowDoubleClick);
            this.dgListSPNLSX.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListSPNLSX_LoadingRow);
            // 
            // uiGroupBox44
            // 
            this.uiGroupBox44.AutoScroll = true;
            this.uiGroupBox44.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox44.Controls.Add(this.btnDeleteSPNLSX);
            this.uiGroupBox44.Controls.Add(this.btnAddSPNLSX);
            this.uiGroupBox44.Controls.Add(this.ctrDVT);
            this.uiGroupBox44.Controls.Add(this.ctrMaHSNLSX);
            this.uiGroupBox44.Controls.Add(this.txtThoiGianSXTG);
            this.uiGroupBox44.Controls.Add(this.txtMaSPNLSX);
            this.uiGroupBox44.Controls.Add(this.label116);
            this.uiGroupBox44.Controls.Add(this.label122);
            this.uiGroupBox44.Controls.Add(this.label121);
            this.uiGroupBox44.Controls.Add(this.label120);
            this.uiGroupBox44.Controls.Add(this.label113);
            this.uiGroupBox44.Controls.Add(this.txtSoLuongSP);
            this.uiGroupBox44.Controls.Add(this.cbbThoiGianSXDVT);
            this.uiGroupBox44.Controls.Add(this.label130);
            this.uiGroupBox44.Controls.Add(this.label114);
            this.uiGroupBox44.Controls.Add(this.label119);
            this.uiGroupBox44.Controls.Add(this.label115);
            this.uiGroupBox44.Controls.Add(this.label117);
            this.uiGroupBox44.Controls.Add(this.label118);
            this.uiGroupBox44.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox44.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox44.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox44.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox44.Name = "uiGroupBox44";
            this.uiGroupBox44.Size = new System.Drawing.Size(1143, 112);
            this.uiGroupBox44.TabIndex = 0;
            this.uiGroupBox44.Text = "3.1.2 NĂNG LỰC SẢN XUẤT SẢN PHẨM";
            this.uiGroupBox44.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteSPNLSX
            // 
            this.btnDeleteSPNLSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteSPNLSX.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteSPNLSX.Image")));
            this.btnDeleteSPNLSX.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteSPNLSX.Location = new System.Drawing.Point(774, 83);
            this.btnDeleteSPNLSX.Name = "btnDeleteSPNLSX";
            this.btnDeleteSPNLSX.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteSPNLSX.TabIndex = 29;
            this.btnDeleteSPNLSX.Text = "Xóa";
            this.btnDeleteSPNLSX.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteSPNLSX.Click += new System.EventHandler(this.btnDeleteSPNLSX_Click);
            // 
            // btnAddSPNLSX
            // 
            this.btnAddSPNLSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSPNLSX.Image = ((System.Drawing.Image)(resources.GetObject("btnAddSPNLSX.Image")));
            this.btnAddSPNLSX.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddSPNLSX.Location = new System.Drawing.Point(678, 83);
            this.btnAddSPNLSX.Name = "btnAddSPNLSX";
            this.btnAddSPNLSX.Size = new System.Drawing.Size(90, 23);
            this.btnAddSPNLSX.TabIndex = 28;
            this.btnAddSPNLSX.Text = "Ghi";
            this.btnAddSPNLSX.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddSPNLSX.Click += new System.EventHandler(this.btnAddSPNLSX_Click);
            // 
            // ctrDVT
            // 
            this.ctrDVT.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.ctrDVT.Appearance.Options.UseBackColor = true;
            this.ctrDVT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVT.Code = "";
            this.ctrDVT.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVT.IsOnlyWarning = false;
            this.ctrDVT.IsValidate = true;
            this.ctrDVT.Location = new System.Drawing.Point(166, 84);
            this.ctrDVT.Name = "ctrDVT";
            this.ctrDVT.Name_VN = "";
            this.ctrDVT.SetOnlyWarning = false;
            this.ctrDVT.SetValidate = false;
            this.ctrDVT.ShowColumnCode = true;
            this.ctrDVT.ShowColumnName = false;
            this.ctrDVT.Size = new System.Drawing.Size(139, 21);
            this.ctrDVT.TabIndex = 26;
            this.ctrDVT.TagName = "";
            this.ctrDVT.Where = null;
            this.ctrDVT.WhereCondition = "";
            // 
            // ctrMaHSNLSX
            // 
            this.ctrMaHSNLSX.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.ctrMaHSNLSX.Appearance.Options.UseBackColor = true;
            this.ctrMaHSNLSX.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ctrMaHSNLSX.Code = "";
            this.ctrMaHSNLSX.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaHSNLSX.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaHSNLSX.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaHSNLSX.IsOnlyWarning = false;
            this.ctrMaHSNLSX.IsValidate = true;
            this.ctrMaHSNLSX.Location = new System.Drawing.Point(166, 55);
            this.ctrMaHSNLSX.Name = "ctrMaHSNLSX";
            this.ctrMaHSNLSX.Name_VN = "";
            this.ctrMaHSNLSX.SetOnlyWarning = false;
            this.ctrMaHSNLSX.SetValidate = false;
            this.ctrMaHSNLSX.ShowColumnCode = true;
            this.ctrMaHSNLSX.ShowColumnName = false;
            this.ctrMaHSNLSX.Size = new System.Drawing.Size(139, 21);
            this.ctrMaHSNLSX.TabIndex = 24;
            this.ctrMaHSNLSX.TagName = "";
            this.ctrMaHSNLSX.Where = null;
            this.ctrMaHSNLSX.WhereCondition = "";
            // 
            // txtThoiGianSXTG
            // 
            this.txtThoiGianSXTG.DecimalDigits = 20;
            this.txtThoiGianSXTG.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThoiGianSXTG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThoiGianSXTG.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtThoiGianSXTG.Location = new System.Drawing.Point(458, 55);
            this.txtThoiGianSXTG.MaxLength = 15;
            this.txtThoiGianSXTG.Name = "txtThoiGianSXTG";
            this.txtThoiGianSXTG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThoiGianSXTG.Size = new System.Drawing.Size(177, 21);
            this.txtThoiGianSXTG.TabIndex = 25;
            this.txtThoiGianSXTG.Text = "0";
            this.txtThoiGianSXTG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThoiGianSXTG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtThoiGianSXTG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaSPNLSX
            // 
            this.txtMaSPNLSX.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSPNLSX.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSPNLSX.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaSPNLSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSPNLSX.Location = new System.Drawing.Point(166, 23);
            this.txtMaSPNLSX.MaxLength = 50;
            this.txtMaSPNLSX.Name = "txtMaSPNLSX";
            this.txtMaSPNLSX.Size = new System.Drawing.Size(122, 21);
            this.txtMaSPNLSX.TabIndex = 22;
            this.txtMaSPNLSX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSPNLSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSPNLSX.ButtonClick += new System.EventHandler(this.txtMaSPNLSX_ButtonClick);
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(311, 59);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(156, 13);
            this.label116.TabIndex = 0;
            this.label116.Text = "Thời gian sản xuất (Thời gian) :";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.ForeColor = System.Drawing.Color.Red;
            this.label122.Location = new System.Drawing.Point(293, 25);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(16, 16);
            this.label122.TabIndex = 0;
            this.label122.Text = "*";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.ForeColor = System.Drawing.Color.Red;
            this.label121.Location = new System.Drawing.Point(637, 86);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(16, 16);
            this.label121.TabIndex = 0;
            this.label121.Text = "*";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.ForeColor = System.Drawing.Color.Red;
            this.label120.Location = new System.Drawing.Point(639, 25);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(16, 16);
            this.label120.TabIndex = 0;
            this.label120.Text = "*";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.Red;
            this.label113.Location = new System.Drawing.Point(638, 57);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(16, 16);
            this.label113.TabIndex = 0;
            this.label113.Text = "*";
            // 
            // txtSoLuongSP
            // 
            this.txtSoLuongSP.DecimalDigits = 20;
            this.txtSoLuongSP.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongSP.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongSP.Location = new System.Drawing.Point(458, 84);
            this.txtSoLuongSP.MaxLength = 15;
            this.txtSoLuongSP.Name = "txtSoLuongSP";
            this.txtSoLuongSP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongSP.Size = new System.Drawing.Size(177, 21);
            this.txtSoLuongSP.TabIndex = 27;
            this.txtSoLuongSP.Text = "0";
            this.txtSoLuongSP.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongSP.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // cbbThoiGianSXDVT
            // 
            this.cbbThoiGianSXDVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbThoiGianSXDVT.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbThoiGianSXDVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem15.FormatStyle.Alpha = 0;
            uiComboBoxItem15.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem15.IsSeparator = false;
            uiComboBoxItem15.Text = "Năm";
            uiComboBoxItem15.Value = "1";
            uiComboBoxItem16.FormatStyle.Alpha = 0;
            uiComboBoxItem16.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem16.IsSeparator = false;
            uiComboBoxItem16.Text = "Quý";
            uiComboBoxItem16.Value = "2";
            uiComboBoxItem17.FormatStyle.Alpha = 0;
            uiComboBoxItem17.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem17.IsSeparator = false;
            uiComboBoxItem17.Text = "Tháng";
            uiComboBoxItem17.Value = "3";
            uiComboBoxItem18.FormatStyle.Alpha = 0;
            uiComboBoxItem18.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem18.IsSeparator = false;
            uiComboBoxItem18.Text = "Tuần";
            uiComboBoxItem18.Value = "4";
            uiComboBoxItem19.FormatStyle.Alpha = 0;
            uiComboBoxItem19.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem19.IsSeparator = false;
            uiComboBoxItem19.Text = "Ngày";
            uiComboBoxItem19.Value = "5";
            this.cbbThoiGianSXDVT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem15,
            uiComboBoxItem16,
            uiComboBoxItem17,
            uiComboBoxItem18,
            uiComboBoxItem19});
            this.cbbThoiGianSXDVT.Location = new System.Drawing.Point(458, 23);
            this.cbbThoiGianSXDVT.Name = "cbbThoiGianSXDVT";
            this.cbbThoiGianSXDVT.Size = new System.Drawing.Size(177, 21);
            this.cbbThoiGianSXDVT.TabIndex = 23;
            this.cbbThoiGianSXDVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label130.Location = new System.Drawing.Point(675, 12);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(444, 65);
            this.label130.TabIndex = 0;
            this.label130.Text = resources.GetString("label130.Text");
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(311, 27);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(133, 13);
            this.label114.TabIndex = 0;
            this.label114.Text = "Thời gian sản xuất (ĐVT) :";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(311, 88);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(105, 13);
            this.label119.TabIndex = 0;
            this.label119.Text = "Số lượng sản phẩm :";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(15, 59);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(44, 13);
            this.label115.TabIndex = 0;
            this.label115.Text = "Mã HS :";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(15, 27);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(77, 13);
            this.label117.TabIndex = 0;
            this.label117.Text = "Mã sản phẩm :";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(15, 88);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(34, 13);
            this.label118.TabIndex = 0;
            this.label118.Text = "ĐVT :";
            // 
            // uiGroupBox39
            // 
            this.uiGroupBox39.AutoScroll = true;
            this.uiGroupBox39.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox39.Controls.Add(this.uiGroupBox41);
            this.uiGroupBox39.Controls.Add(this.uiGroupBox38);
            this.uiGroupBox39.Controls.Add(this.uiGroupBox40);
            this.uiGroupBox39.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox39.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox39.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox39.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox39.Name = "uiGroupBox39";
            this.uiGroupBox39.Size = new System.Drawing.Size(1149, 399);
            this.uiGroupBox39.TabIndex = 0;
            this.uiGroupBox39.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox41
            // 
            this.uiGroupBox41.AutoScroll = true;
            this.uiGroupBox41.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox41.Controls.Add(this.dgListSPCKSX);
            this.uiGroupBox41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox41.Location = new System.Drawing.Point(3, 163);
            this.uiGroupBox41.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox41.Name = "uiGroupBox41";
            this.uiGroupBox41.Size = new System.Drawing.Size(1143, 233);
            this.uiGroupBox41.TabIndex = 44;
            this.uiGroupBox41.Text = "3.1.1.1 DANH SÁCH SẢN PHẨM";
            this.uiGroupBox41.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListSPCKSX
            // 
            this.dgListSPCKSX.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListSPCKSX.AlternatingColors = true;
            this.dgListSPCKSX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListSPCKSX.ColumnAutoResize = true;
            dgListSPCKSX_DesignTimeLayout.LayoutString = resources.GetString("dgListSPCKSX_DesignTimeLayout.LayoutString");
            this.dgListSPCKSX.DesignTimeLayout = dgListSPCKSX_DesignTimeLayout;
            this.dgListSPCKSX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListSPCKSX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListSPCKSX.FrozenColumns = 3;
            this.dgListSPCKSX.GroupByBoxVisible = false;
            this.dgListSPCKSX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPCKSX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPCKSX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListSPCKSX.Location = new System.Drawing.Point(3, 17);
            this.dgListSPCKSX.Margin = new System.Windows.Forms.Padding(0);
            this.dgListSPCKSX.Name = "dgListSPCKSX";
            this.dgListSPCKSX.RecordNavigator = true;
            this.dgListSPCKSX.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListSPCKSX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListSPCKSX.Size = new System.Drawing.Size(1137, 213);
            this.dgListSPCKSX.TabIndex = 16;
            this.dgListSPCKSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListSPCKSX.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListSPCKSX_RowDoubleClick);
            this.dgListSPCKSX.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListSPCKSX_LoadingRow);
            // 
            // uiGroupBox38
            // 
            this.uiGroupBox38.AutoScroll = true;
            this.uiGroupBox38.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox38.Controls.Add(this.btnDeleteSPCKSX);
            this.uiGroupBox38.Controls.Add(this.btnAddSPCKSX);
            this.uiGroupBox38.Controls.Add(this.ctrMaHSCK);
            this.uiGroupBox38.Controls.Add(this.txtMaSPCK);
            this.uiGroupBox38.Controls.Add(this.txtChuKySXTG);
            this.uiGroupBox38.Controls.Add(this.label110);
            this.uiGroupBox38.Controls.Add(this.label128);
            this.uiGroupBox38.Controls.Add(this.label127);
            this.uiGroupBox38.Controls.Add(this.label126);
            this.uiGroupBox38.Controls.Add(this.cbbCKSXDVT);
            this.uiGroupBox38.Controls.Add(this.label112);
            this.uiGroupBox38.Controls.Add(this.label111);
            this.uiGroupBox38.Controls.Add(this.label129);
            this.uiGroupBox38.Controls.Add(this.label109);
            this.uiGroupBox38.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox38.Location = new System.Drawing.Point(3, 51);
            this.uiGroupBox38.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox38.Name = "uiGroupBox38";
            this.uiGroupBox38.Size = new System.Drawing.Size(1143, 112);
            this.uiGroupBox38.TabIndex = 0;
            this.uiGroupBox38.Text = "3.1.1 CHU KỲ SẢN XUẤT SẢN PHẨM";
            this.uiGroupBox38.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteSPCKSX
            // 
            this.btnDeleteSPCKSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteSPCKSX.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteSPCKSX.Image")));
            this.btnDeleteSPCKSX.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteSPCKSX.Location = new System.Drawing.Point(420, 79);
            this.btnDeleteSPCKSX.Name = "btnDeleteSPCKSX";
            this.btnDeleteSPCKSX.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteSPCKSX.TabIndex = 21;
            this.btnDeleteSPCKSX.Text = "Xóa";
            this.btnDeleteSPCKSX.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteSPCKSX.Click += new System.EventHandler(this.btnDeleteSPCKSX_Click);
            // 
            // btnAddSPCKSX
            // 
            this.btnAddSPCKSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSPCKSX.Image = ((System.Drawing.Image)(resources.GetObject("btnAddSPCKSX.Image")));
            this.btnAddSPCKSX.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddSPCKSX.Location = new System.Drawing.Point(324, 79);
            this.btnAddSPCKSX.Name = "btnAddSPCKSX";
            this.btnAddSPCKSX.Size = new System.Drawing.Size(90, 23);
            this.btnAddSPCKSX.TabIndex = 20;
            this.btnAddSPCKSX.Text = "Ghi";
            this.btnAddSPCKSX.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddSPCKSX.Click += new System.EventHandler(this.btnAddSPCKSX_Click);
            // 
            // ctrMaHSCK
            // 
            this.ctrMaHSCK.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.ctrMaHSCK.Appearance.Options.UseBackColor = true;
            this.ctrMaHSCK.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ctrMaHSCK.Code = "";
            this.ctrMaHSCK.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaHSCK.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaHSCK.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaHSCK.IsOnlyWarning = false;
            this.ctrMaHSCK.IsValidate = true;
            this.ctrMaHSCK.Location = new System.Drawing.Point(166, 51);
            this.ctrMaHSCK.Name = "ctrMaHSCK";
            this.ctrMaHSCK.Name_VN = "";
            this.ctrMaHSCK.SetOnlyWarning = false;
            this.ctrMaHSCK.SetValidate = false;
            this.ctrMaHSCK.ShowColumnCode = true;
            this.ctrMaHSCK.ShowColumnName = false;
            this.ctrMaHSCK.Size = new System.Drawing.Size(139, 21);
            this.ctrMaHSCK.TabIndex = 18;
            this.ctrMaHSCK.TagName = "";
            this.ctrMaHSCK.Where = null;
            this.ctrMaHSCK.WhereCondition = "";
            // 
            // txtMaSPCK
            // 
            this.txtMaSPCK.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSPCK.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSPCK.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaSPCK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSPCK.Location = new System.Drawing.Point(167, 21);
            this.txtMaSPCK.MaxLength = 50;
            this.txtMaSPCK.Name = "txtMaSPCK";
            this.txtMaSPCK.Size = new System.Drawing.Size(122, 21);
            this.txtMaSPCK.TabIndex = 16;
            this.txtMaSPCK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSPCK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSPCK.ButtonClick += new System.EventHandler(this.txtMaSPCK_ButtonClick);
            // 
            // txtChuKySXTG
            // 
            this.txtChuKySXTG.DecimalDigits = 20;
            this.txtChuKySXTG.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtChuKySXTG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChuKySXTG.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtChuKySXTG.Location = new System.Drawing.Point(458, 51);
            this.txtChuKySXTG.MaxLength = 15;
            this.txtChuKySXTG.Name = "txtChuKySXTG";
            this.txtChuKySXTG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtChuKySXTG.Size = new System.Drawing.Size(177, 21);
            this.txtChuKySXTG.TabIndex = 19;
            this.txtChuKySXTG.Text = "0";
            this.txtChuKySXTG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChuKySXTG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtChuKySXTG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(311, 55);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(146, 13);
            this.label110.TabIndex = 0;
            this.label110.Text = "Chu kỳ sản xuất (Thời gian) :";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.ForeColor = System.Drawing.Color.Red;
            this.label128.Location = new System.Drawing.Point(638, 53);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(16, 16);
            this.label128.TabIndex = 0;
            this.label128.Text = "*";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.ForeColor = System.Drawing.Color.Red;
            this.label127.Location = new System.Drawing.Point(638, 23);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(16, 16);
            this.label127.TabIndex = 0;
            this.label127.Text = "*";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.ForeColor = System.Drawing.Color.Red;
            this.label126.Location = new System.Drawing.Point(292, 23);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(16, 16);
            this.label126.TabIndex = 0;
            this.label126.Text = "*";
            // 
            // cbbCKSXDVT
            // 
            this.cbbCKSXDVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbCKSXDVT.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbCKSXDVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem20.FormatStyle.Alpha = 0;
            uiComboBoxItem20.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem20.IsSeparator = false;
            uiComboBoxItem20.Text = "Năm";
            uiComboBoxItem20.Value = "1";
            uiComboBoxItem21.FormatStyle.Alpha = 0;
            uiComboBoxItem21.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem21.IsSeparator = false;
            uiComboBoxItem21.Text = "Quý";
            uiComboBoxItem21.Value = "2";
            uiComboBoxItem22.FormatStyle.Alpha = 0;
            uiComboBoxItem22.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem22.IsSeparator = false;
            uiComboBoxItem22.Text = "Tháng";
            uiComboBoxItem22.Value = "3";
            uiComboBoxItem23.FormatStyle.Alpha = 0;
            uiComboBoxItem23.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem23.IsSeparator = false;
            uiComboBoxItem23.Text = "Tuần";
            uiComboBoxItem23.Value = "4";
            uiComboBoxItem24.FormatStyle.Alpha = 0;
            uiComboBoxItem24.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem24.IsSeparator = false;
            uiComboBoxItem24.Text = "Ngày";
            uiComboBoxItem24.Value = "5";
            this.cbbCKSXDVT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem20,
            uiComboBoxItem21,
            uiComboBoxItem22,
            uiComboBoxItem23,
            uiComboBoxItem24});
            this.cbbCKSXDVT.Location = new System.Drawing.Point(458, 21);
            this.cbbCKSXDVT.Name = "cbbCKSXDVT";
            this.cbbCKSXDVT.Size = new System.Drawing.Size(177, 21);
            this.cbbCKSXDVT.TabIndex = 17;
            this.cbbCKSXDVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(311, 25);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(123, 13);
            this.label112.TabIndex = 0;
            this.label112.Text = "Chu kỳ sản xuất (ĐVT) :";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(15, 55);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(44, 13);
            this.label111.TabIndex = 0;
            this.label111.Text = "Mã HS :";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label129.Location = new System.Drawing.Point(675, 14);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(410, 52);
            this.label129.TabIndex = 0;
            this.label129.Text = "Hướng dẫn nhập liệu :\r\nNếu doanh nghiệp có Chu kỳ sản xuất sản phẩm A trong 30 ng" +
                "ày thì nhập như sau :\r\nÔ Chu kỳ sản xuất (ĐVT) chọn là : Ngày\r\nÔ Chu kỳ sản xuất" +
                " (Thời gian) nhập vào là : 30\r\n";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(15, 25);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(77, 13);
            this.label109.TabIndex = 0;
            this.label109.Text = "Mã sản phẩm :";
            // 
            // uiGroupBox40
            // 
            this.uiGroupBox40.AutoScroll = true;
            this.uiGroupBox40.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox40.Controls.Add(this.btnDeleteNganhNghe);
            this.uiGroupBox40.Controls.Add(this.cbbLoaiNganhNghe);
            this.uiGroupBox40.Controls.Add(this.btnAddNganhNghe);
            this.uiGroupBox40.Controls.Add(this.label38);
            this.uiGroupBox40.Controls.Add(this.label93);
            this.uiGroupBox40.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox40.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox40.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox40.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox40.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox40.Name = "uiGroupBox40";
            this.uiGroupBox40.Size = new System.Drawing.Size(1143, 43);
            this.uiGroupBox40.TabIndex = 0;
            this.uiGroupBox40.Text = "3.1 Ngành nghề";
            this.uiGroupBox40.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteNganhNghe
            // 
            this.btnDeleteNganhNghe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteNganhNghe.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteNganhNghe.Image")));
            this.btnDeleteNganhNghe.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteNganhNghe.Location = new System.Drawing.Point(420, 12);
            this.btnDeleteNganhNghe.Name = "btnDeleteNganhNghe";
            this.btnDeleteNganhNghe.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteNganhNghe.TabIndex = 15;
            this.btnDeleteNganhNghe.Text = "Xóa";
            this.btnDeleteNganhNghe.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteNganhNghe.Click += new System.EventHandler(this.btnDeleteNganhNghe_Click);
            // 
            // cbbLoaiNganhNghe
            // 
            this.cbbLoaiNganhNghe.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiNganhNghe.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbLoaiNganhNghe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem25.FormatStyle.Alpha = 0;
            uiComboBoxItem25.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem25.IsSeparator = false;
            uiComboBoxItem25.Text = "Da giầy";
            uiComboBoxItem25.Value = "1";
            uiComboBoxItem26.FormatStyle.Alpha = 0;
            uiComboBoxItem26.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem26.IsSeparator = false;
            uiComboBoxItem26.Text = "May mặc";
            uiComboBoxItem26.Value = "2";
            uiComboBoxItem27.FormatStyle.Alpha = 0;
            uiComboBoxItem27.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem27.IsSeparator = false;
            uiComboBoxItem27.Text = "Điện tử, điện lạnh";
            uiComboBoxItem27.Value = "3";
            uiComboBoxItem28.FormatStyle.Alpha = 0;
            uiComboBoxItem28.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem28.IsSeparator = false;
            uiComboBoxItem28.Text = "Chế biến thực phẩm";
            uiComboBoxItem28.Value = "4";
            uiComboBoxItem29.FormatStyle.Alpha = 0;
            uiComboBoxItem29.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem29.IsSeparator = false;
            uiComboBoxItem29.Text = "Cơ khí";
            uiComboBoxItem29.Value = "5";
            uiComboBoxItem30.FormatStyle.Alpha = 0;
            uiComboBoxItem30.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem30.IsSeparator = false;
            uiComboBoxItem30.Text = "Gỗ";
            uiComboBoxItem30.Value = "6";
            uiComboBoxItem31.FormatStyle.Alpha = 0;
            uiComboBoxItem31.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem31.IsSeparator = false;
            uiComboBoxItem31.Text = "Nhựa";
            uiComboBoxItem31.Value = "7";
            uiComboBoxItem32.FormatStyle.Alpha = 0;
            uiComboBoxItem32.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem32.IsSeparator = false;
            uiComboBoxItem32.Text = "Nông sản";
            uiComboBoxItem32.Value = "8";
            uiComboBoxItem33.FormatStyle.Alpha = 0;
            uiComboBoxItem33.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem33.IsSeparator = false;
            uiComboBoxItem33.Text = "Loại khác";
            uiComboBoxItem33.Value = "9";
            this.cbbLoaiNganhNghe.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem25,
            uiComboBoxItem26,
            uiComboBoxItem27,
            uiComboBoxItem28,
            uiComboBoxItem29,
            uiComboBoxItem30,
            uiComboBoxItem31,
            uiComboBoxItem32,
            uiComboBoxItem33});
            this.cbbLoaiNganhNghe.Location = new System.Drawing.Point(166, 13);
            this.cbbLoaiNganhNghe.Name = "cbbLoaiNganhNghe";
            this.cbbLoaiNganhNghe.Size = new System.Drawing.Size(127, 21);
            this.cbbLoaiNganhNghe.TabIndex = 13;
            this.cbbLoaiNganhNghe.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnAddNganhNghe
            // 
            this.btnAddNganhNghe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNganhNghe.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNganhNghe.Image")));
            this.btnAddNganhNghe.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddNganhNghe.Location = new System.Drawing.Point(324, 12);
            this.btnAddNganhNghe.Name = "btnAddNganhNghe";
            this.btnAddNganhNghe.Size = new System.Drawing.Size(90, 23);
            this.btnAddNganhNghe.TabIndex = 14;
            this.btnAddNganhNghe.Text = "Ghi";
            this.btnAddNganhNghe.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddNganhNghe.Click += new System.EventHandler(this.btnAddNganhNghe_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(11, 17);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(93, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "Loại ngành nghề :";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Red;
            this.label93.Location = new System.Drawing.Point(296, 15);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(16, 16);
            this.label93.TabIndex = 0;
            this.label93.Text = "*";
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.label88);
            this.uiGroupBox9.Controls.Add(this.label92);
            this.uiGroupBox9.Controls.Add(this.label91);
            this.uiGroupBox9.Controls.Add(this.label90);
            this.uiGroupBox9.Controls.Add(this.label89);
            this.uiGroupBox9.Controls.Add(this.label87);
            this.uiGroupBox9.Controls.Add(this.txtTongSoLuong);
            this.uiGroupBox9.Controls.Add(this.txtSoLuongDiThue);
            this.uiGroupBox9.Controls.Add(this.txtSoLuongKhac);
            this.uiGroupBox9.Controls.Add(this.txtSoLuongSoHuu);
            this.uiGroupBox9.Controls.Add(this.label60);
            this.uiGroupBox9.Controls.Add(this.label42);
            this.uiGroupBox9.Controls.Add(this.label61);
            this.uiGroupBox9.Controls.Add(this.label43);
            this.uiGroupBox9.Controls.Add(this.label59);
            this.uiGroupBox9.Controls.Add(this.txtNangLucSX);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox9.Location = new System.Drawing.Point(3, 175);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(1155, 122);
            this.uiGroupBox9.TabIndex = 0;
            this.uiGroupBox9.Text = "2. SỐ LƯỢNG MÁY MÓC DÂY TRUYỀN TRANG THIẾT BỊ";
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Red;
            this.label88.Location = new System.Drawing.Point(306, 51);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(16, 16);
            this.label88.TabIndex = 0;
            this.label88.Text = "*";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Red;
            this.label92.Location = new System.Drawing.Point(172, 103);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(400, 13);
            this.label92.TabIndex = 0;
            this.label92.Text = "Nêu rõ Năng lực sản xuất sản phẩm tối đa trong một năm/tháng/ngày";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Red;
            this.label91.Location = new System.Drawing.Point(647, 79);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(16, 16);
            this.label91.TabIndex = 0;
            this.label91.Text = "*";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Red;
            this.label90.Location = new System.Drawing.Point(647, 51);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(16, 16);
            this.label90.TabIndex = 0;
            this.label90.Text = "*";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Red;
            this.label89.Location = new System.Drawing.Point(647, 21);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(16, 16);
            this.label89.TabIndex = 0;
            this.label89.Text = "*";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Red;
            this.label87.Location = new System.Drawing.Point(306, 21);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(16, 16);
            this.label87.TabIndex = 0;
            this.label87.Text = "*";
            // 
            // txtTongSoLuong
            // 
            this.txtTongSoLuong.DecimalDigits = 20;
            this.txtTongSoLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongSoLuong.Location = new System.Drawing.Point(464, 49);
            this.txtTongSoLuong.MaxLength = 15;
            this.txtTongSoLuong.Name = "txtTongSoLuong";
            this.txtTongSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoLuong.Size = new System.Drawing.Size(177, 21);
            this.txtTongSoLuong.TabIndex = 11;
            this.txtTongSoLuong.Text = "0";
            this.txtTongSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuongDiThue
            // 
            this.txtSoLuongDiThue.DecimalDigits = 20;
            this.txtSoLuongDiThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongDiThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongDiThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongDiThue.Location = new System.Drawing.Point(464, 19);
            this.txtSoLuongDiThue.MaxLength = 15;
            this.txtSoLuongDiThue.Name = "txtSoLuongDiThue";
            this.txtSoLuongDiThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongDiThue.Size = new System.Drawing.Size(177, 21);
            this.txtSoLuongDiThue.TabIndex = 9;
            this.txtSoLuongDiThue.Text = "0";
            this.txtSoLuongDiThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongDiThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongDiThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongDiThue.Leave += new System.EventHandler(this.txtSoLuongDiThue_Leave);
            // 
            // txtSoLuongKhac
            // 
            this.txtSoLuongKhac.DecimalDigits = 20;
            this.txtSoLuongKhac.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongKhac.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongKhac.Location = new System.Drawing.Point(172, 49);
            this.txtSoLuongKhac.MaxLength = 15;
            this.txtSoLuongKhac.Name = "txtSoLuongKhac";
            this.txtSoLuongKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongKhac.Size = new System.Drawing.Size(127, 21);
            this.txtSoLuongKhac.TabIndex = 10;
            this.txtSoLuongKhac.Text = "0";
            this.txtSoLuongKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongKhac.Leave += new System.EventHandler(this.txtSoLuongKhac_Leave);
            // 
            // txtSoLuongSoHuu
            // 
            this.txtSoLuongSoHuu.DecimalDigits = 20;
            this.txtSoLuongSoHuu.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongSoHuu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongSoHuu.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongSoHuu.Location = new System.Drawing.Point(172, 19);
            this.txtSoLuongSoHuu.MaxLength = 15;
            this.txtSoLuongSoHuu.Name = "txtSoLuongSoHuu";
            this.txtSoLuongSoHuu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongSoHuu.Size = new System.Drawing.Size(127, 21);
            this.txtSoLuongSoHuu.TabIndex = 8;
            this.txtSoLuongSoHuu.Text = "0";
            this.txtSoLuongSoHuu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongSoHuu.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongSoHuu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongSoHuu.Leave += new System.EventHandler(this.txtSoLuongSoHuu_Leave);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(344, 23);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(92, 13);
            this.label60.TabIndex = 0;
            this.label60.Text = "Số lượng đi thuê :";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(17, 81);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(101, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "Năng lực sản xuất :";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(17, 23);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(92, 13);
            this.label61.TabIndex = 0;
            this.label61.Text = "Số lượng sở hữu :";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(344, 53);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(82, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "Tổng số lượng :";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(17, 53);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(81, 13);
            this.label59.TabIndex = 0;
            this.label59.Text = "Số lượng khác :";
            // 
            // txtNangLucSX
            // 
            this.txtNangLucSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNangLucSX.Location = new System.Drawing.Point(172, 77);
            this.txtNangLucSX.Name = "txtNangLucSX";
            this.txtNangLucSX.Size = new System.Drawing.Size(469, 21);
            this.txtNangLucSX.TabIndex = 12;
            this.txtNangLucSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnDeleteCSSX);
            this.uiGroupBox2.Controls.Add(this.btnAddCSSX);
            this.uiGroupBox2.Controls.Add(this.label86);
            this.uiGroupBox2.Controls.Add(this.label85);
            this.uiGroupBox2.Controls.Add(this.label84);
            this.uiGroupBox2.Controls.Add(this.label32);
            this.uiGroupBox2.Controls.Add(this.txtDiaChiCSSX);
            this.uiGroupBox2.Controls.Add(this.label107);
            this.uiGroupBox2.Controls.Add(this.label105);
            this.uiGroupBox2.Controls.Add(this.label31);
            this.uiGroupBox2.Controls.Add(this.cbbLoaiCSSX);
            this.uiGroupBox2.Controls.Add(this.cbbDCTruSoCSSX);
            this.uiGroupBox2.Controls.Add(this.label30);
            this.uiGroupBox2.Controls.Add(this.txtSoLuongCN);
            this.uiGroupBox2.Controls.Add(this.label106);
            this.uiGroupBox2.Controls.Add(this.txtDienTichNX);
            this.uiGroupBox2.Controls.Add(this.label108);
            this.uiGroupBox2.Controls.Add(this.label104);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1155, 158);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "1. THÔNG TIN CHUNG";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteCSSX
            // 
            this.btnDeleteCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteCSSX.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteCSSX.Image")));
            this.btnDeleteCSSX.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteCSSX.Location = new System.Drawing.Point(426, 127);
            this.btnDeleteCSSX.Name = "btnDeleteCSSX";
            this.btnDeleteCSSX.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteCSSX.TabIndex = 7;
            this.btnDeleteCSSX.Text = "Xóa";
            this.btnDeleteCSSX.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteCSSX.Click += new System.EventHandler(this.btnDeleteCSSX_Click);
            // 
            // btnAddCSSX
            // 
            this.btnAddCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCSSX.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCSSX.Image")));
            this.btnAddCSSX.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddCSSX.Location = new System.Drawing.Point(330, 127);
            this.btnAddCSSX.Name = "btnAddCSSX";
            this.btnAddCSSX.Size = new System.Drawing.Size(90, 23);
            this.btnAddCSSX.TabIndex = 6;
            this.btnAddCSSX.Text = "Ghi";
            this.btnAddCSSX.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddCSSX.Click += new System.EventHandler(this.btnAddCSSX_Click);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Red;
            this.label86.Location = new System.Drawing.Point(819, 71);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(16, 16);
            this.label86.TabIndex = 0;
            this.label86.Text = "*";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Red;
            this.label85.Location = new System.Drawing.Point(302, 42);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(16, 16);
            this.label85.TabIndex = 0;
            this.label85.Text = "*";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Red;
            this.label84.Location = new System.Drawing.Point(303, 15);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(16, 16);
            this.label84.TabIndex = 0;
            this.label84.Text = "*";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(17, 17);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(106, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "Loại cơ sở sản xuất :";
            // 
            // txtDiaChiCSSX
            // 
            this.txtDiaChiCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiCSSX.Location = new System.Drawing.Point(172, 69);
            this.txtDiaChiCSSX.Name = "txtDiaChiCSSX";
            this.txtDiaChiCSSX.Size = new System.Drawing.Size(641, 21);
            this.txtDiaChiCSSX.TabIndex = 3;
            this.txtDiaChiCSSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Red;
            this.label107.Location = new System.Drawing.Point(303, 130);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(16, 16);
            this.label107.TabIndex = 0;
            this.label107.Text = "*";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Red;
            this.label105.Location = new System.Drawing.Point(302, 100);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(16, 16);
            this.label105.TabIndex = 0;
            this.label105.Text = "*";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(17, 73);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(74, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Địa chỉ CSSX :";
            // 
            // cbbLoaiCSSX
            // 
            this.cbbLoaiCSSX.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiCSSX.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbLoaiCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem34.FormatStyle.Alpha = 0;
            uiComboBoxItem34.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem34.IsSeparator = false;
            uiComboBoxItem34.Text = "CSSX trong khu vực nhà máy";
            uiComboBoxItem34.Value = "1";
            uiComboBoxItem35.FormatStyle.Alpha = 0;
            uiComboBoxItem35.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem35.IsSeparator = false;
            uiComboBoxItem35.Text = "CSSX ngoài khu vực nhà máy";
            uiComboBoxItem35.Value = "2";
            this.cbbLoaiCSSX.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem34,
            uiComboBoxItem35});
            this.cbbLoaiCSSX.Location = new System.Drawing.Point(172, 13);
            this.cbbLoaiCSSX.Name = "cbbLoaiCSSX";
            this.cbbLoaiCSSX.Size = new System.Drawing.Size(127, 21);
            this.cbbLoaiCSSX.TabIndex = 1;
            this.cbbLoaiCSSX.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbDCTruSoCSSX
            // 
            this.cbbDCTruSoCSSX.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbDCTruSoCSSX.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbDCTruSoCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem36.FormatStyle.Alpha = 0;
            uiComboBoxItem36.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem36.IsSeparator = false;
            uiComboBoxItem36.Text = "CSSX thuộc quyền sở hữu của DN   ";
            uiComboBoxItem36.Value = "1";
            uiComboBoxItem37.FormatStyle.Alpha = 0;
            uiComboBoxItem37.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem37.IsSeparator = false;
            uiComboBoxItem37.Text = "CSSX thuê ";
            uiComboBoxItem37.Value = "2";
            this.cbbDCTruSoCSSX.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem36,
            uiComboBoxItem37});
            this.cbbDCTruSoCSSX.Location = new System.Drawing.Point(172, 40);
            this.cbbDCTruSoCSSX.Name = "cbbDCTruSoCSSX";
            this.cbbDCTruSoCSSX.Size = new System.Drawing.Size(127, 21);
            this.cbbDCTruSoCSSX.TabIndex = 2;
            this.cbbDCTruSoCSSX.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(17, 44);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(135, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "Địa chỉ trụ sở chính (Loại) :";
            // 
            // txtSoLuongCN
            // 
            this.txtSoLuongCN.DecimalDigits = 20;
            this.txtSoLuongCN.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongCN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongCN.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongCN.Location = new System.Drawing.Point(172, 128);
            this.txtSoLuongCN.MaxLength = 15;
            this.txtSoLuongCN.Name = "txtSoLuongCN";
            this.txtSoLuongCN.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongCN.Size = new System.Drawing.Size(127, 21);
            this.txtSoLuongCN.TabIndex = 5;
            this.txtSoLuongCN.Text = "0";
            this.txtSoLuongCN.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongCN.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongCN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongCN.Leave += new System.EventHandler(this.txtSoLuongSoHuu_Leave);
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(17, 132);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(109, 13);
            this.label106.TabIndex = 0;
            this.label106.Text = "Số lượng công nhân :";
            // 
            // txtDienTichNX
            // 
            this.txtDienTichNX.DecimalDigits = 20;
            this.txtDienTichNX.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDienTichNX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienTichNX.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDienTichNX.Location = new System.Drawing.Point(172, 98);
            this.txtDienTichNX.MaxLength = 15;
            this.txtDienTichNX.Name = "txtDienTichNX";
            this.txtDienTichNX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDienTichNX.Size = new System.Drawing.Size(127, 21);
            this.txtDienTichNX.TabIndex = 4;
            this.txtDienTichNX.Text = "0";
            this.txtDienTichNX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDienTichNX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDienTichNX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDienTichNX.Leave += new System.EventHandler(this.txtSoLuongSoHuu_Leave);
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(328, 102);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(76, 13);
            this.label108.TabIndex = 0;
            this.label108.Text = "Đơn vị tính M2";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(17, 102);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(110, 13);
            this.label104.TabIndex = 0;
            this.label104.Text = "Diện tích nhà xưởng :";
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.AutoScroll = true;
            this.uiTabPage1.Controls.Add(this.uiGroupBox1);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1178, 898);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thông tin chung";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox35);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox22);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox25);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1161, 1120);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "THÔNG TIN CHUNG";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox35
            // 
            this.uiGroupBox35.AutoScroll = true;
            this.uiGroupBox35.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox35.Controls.Add(this.uiGroupBox37);
            this.uiGroupBox35.Controls.Add(this.uiGroupBox36);
            this.uiGroupBox35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox35.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox35.Location = new System.Drawing.Point(3, 716);
            this.uiGroupBox35.Name = "uiGroupBox35";
            this.uiGroupBox35.Size = new System.Drawing.Size(1155, 279);
            this.uiGroupBox35.TabIndex = 0;
            this.uiGroupBox35.Text = "Thông tin địa điểm lưu giữ hàng hóa của DN";
            this.uiGroupBox35.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox37
            // 
            this.uiGroupBox37.AutoScroll = true;
            this.uiGroupBox37.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox37.Controls.Add(this.dgListDDLGHH);
            this.uiGroupBox37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox37.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox37.Location = new System.Drawing.Point(3, 71);
            this.uiGroupBox37.Name = "uiGroupBox37";
            this.uiGroupBox37.Size = new System.Drawing.Size(1149, 205);
            this.uiGroupBox37.TabIndex = 24;
            this.uiGroupBox37.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListDDLGHH
            // 
            this.dgListDDLGHH.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDDLGHH.AlternatingColors = true;
            this.dgListDDLGHH.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDDLGHH.ColumnAutoResize = true;
            dgListDDLGHH_DesignTimeLayout.LayoutString = resources.GetString("dgListDDLGHH_DesignTimeLayout.LayoutString");
            this.dgListDDLGHH.DesignTimeLayout = dgListDDLGHH_DesignTimeLayout;
            this.dgListDDLGHH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDDLGHH.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDDLGHH.FrozenColumns = 3;
            this.dgListDDLGHH.GroupByBoxVisible = false;
            this.dgListDDLGHH.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDDLGHH.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDDLGHH.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDDLGHH.Location = new System.Drawing.Point(3, 8);
            this.dgListDDLGHH.Margin = new System.Windows.Forms.Padding(0);
            this.dgListDDLGHH.Name = "dgListDDLGHH";
            this.dgListDDLGHH.RecordNavigator = true;
            this.dgListDDLGHH.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDDLGHH.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDDLGHH.Size = new System.Drawing.Size(1143, 194);
            this.dgListDDLGHH.TabIndex = 13;
            this.dgListDDLGHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListDDLGHH.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListDDLGHH_RowDoubleClick);
            // 
            // uiGroupBox36
            // 
            this.uiGroupBox36.AutoScroll = true;
            this.uiGroupBox36.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox36.Controls.Add(this.btnDeleteDDLGHH);
            this.uiGroupBox36.Controls.Add(this.btnAddDDLGHH);
            this.uiGroupBox36.Controls.Add(this.txtTenDiaDiemLGHH);
            this.uiGroupBox36.Controls.Add(this.txtMaDiaDiemLGHH);
            this.uiGroupBox36.Controls.Add(this.label100);
            this.uiGroupBox36.Controls.Add(this.label101);
            this.uiGroupBox36.Controls.Add(this.label102);
            this.uiGroupBox36.Controls.Add(this.label103);
            this.uiGroupBox36.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox36.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox36.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox36.Name = "uiGroupBox36";
            this.uiGroupBox36.Size = new System.Drawing.Size(1149, 54);
            this.uiGroupBox36.TabIndex = 0;
            this.uiGroupBox36.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteDDLGHH
            // 
            this.btnDeleteDDLGHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteDDLGHH.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteDDLGHH.Image")));
            this.btnDeleteDDLGHH.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteDDLGHH.Location = new System.Drawing.Point(952, 20);
            this.btnDeleteDDLGHH.Name = "btnDeleteDDLGHH";
            this.btnDeleteDDLGHH.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteDDLGHH.TabIndex = 29;
            this.btnDeleteDDLGHH.Text = "Xóa";
            this.btnDeleteDDLGHH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteDDLGHH.Click += new System.EventHandler(this.btnDeleteDDLGHH_Click);
            // 
            // btnAddDDLGHH
            // 
            this.btnAddDDLGHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddDDLGHH.Image = ((System.Drawing.Image)(resources.GetObject("btnAddDDLGHH.Image")));
            this.btnAddDDLGHH.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddDDLGHH.Location = new System.Drawing.Point(856, 20);
            this.btnAddDDLGHH.Name = "btnAddDDLGHH";
            this.btnAddDDLGHH.Size = new System.Drawing.Size(90, 23);
            this.btnAddDDLGHH.TabIndex = 28;
            this.btnAddDDLGHH.Text = "Ghi";
            this.btnAddDDLGHH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddDDLGHH.Click += new System.EventHandler(this.btnAddDDLGHH_Click);
            // 
            // txtTenDiaDiemLGHH
            // 
            this.txtTenDiaDiemLGHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDiaDiemLGHH.Location = new System.Drawing.Point(409, 21);
            this.txtTenDiaDiemLGHH.Name = "txtTenDiaDiemLGHH";
            this.txtTenDiaDiemLGHH.Size = new System.Drawing.Size(406, 21);
            this.txtTenDiaDiemLGHH.TabIndex = 27;
            this.txtTenDiaDiemLGHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDiaDiemLGHH
            // 
            this.txtMaDiaDiemLGHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDiaDiemLGHH.Location = new System.Drawing.Point(156, 21);
            this.txtMaDiaDiemLGHH.MaxLength = 7;
            this.txtMaDiaDiemLGHH.Name = "txtMaDiaDiemLGHH";
            this.txtMaDiaDiemLGHH.Size = new System.Drawing.Size(150, 21);
            this.txtMaDiaDiemLGHH.TabIndex = 26;
            this.txtMaDiaDiemLGHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Red;
            this.label100.Location = new System.Drawing.Point(815, 23);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(16, 16);
            this.label100.TabIndex = 0;
            this.label100.Text = "*";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Red;
            this.label101.Location = new System.Drawing.Point(306, 23);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(16, 16);
            this.label101.TabIndex = 0;
            this.label101.Text = "*";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(321, 25);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(74, 13);
            this.label102.TabIndex = 0;
            this.label102.Text = "Tên địa điểm :";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(16, 25);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(70, 13);
            this.label103.TabIndex = 0;
            this.label103.Text = "Mã địa điểm :";
            // 
            // uiGroupBox22
            // 
            this.uiGroupBox22.AutoScroll = true;
            this.uiGroupBox22.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox22.Controls.Add(this.cbbKeToan);
            this.uiGroupBox22.Controls.Add(this.cbbTronThue);
            this.uiGroupBox22.Controls.Add(this.cbbBuonLau);
            this.uiGroupBox22.Controls.Add(this.label55);
            this.uiGroupBox22.Controls.Add(this.label54);
            this.uiGroupBox22.Controls.Add(this.label83);
            this.uiGroupBox22.Controls.Add(this.label82);
            this.uiGroupBox22.Controls.Add(this.label81);
            this.uiGroupBox22.Controls.Add(this.label53);
            this.uiGroupBox22.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox22.Location = new System.Drawing.Point(3, 995);
            this.uiGroupBox22.Name = "uiGroupBox22";
            this.uiGroupBox22.Size = new System.Drawing.Size(1155, 122);
            this.uiGroupBox22.TabIndex = 0;
            this.uiGroupBox22.Text = "Tuân thủ pháp luật";
            this.uiGroupBox22.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbKeToan
            // 
            this.cbbKeToan.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbKeToan.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbKeToan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Không";
            uiComboBoxItem1.Value = "0";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Có";
            uiComboBoxItem2.Value = "1";
            this.cbbKeToan.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbbKeToan.Location = new System.Drawing.Point(553, 90);
            this.cbbKeToan.Name = "cbbKeToan";
            this.cbbKeToan.Size = new System.Drawing.Size(129, 21);
            this.cbbKeToan.TabIndex = 32;
            this.cbbKeToan.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbTronThue
            // 
            this.cbbTronThue.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbTronThue.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbTronThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Không";
            uiComboBoxItem3.Value = "0";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Có";
            uiComboBoxItem4.Value = "1";
            this.cbbTronThue.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbbTronThue.Location = new System.Drawing.Point(553, 60);
            this.cbbTronThue.Name = "cbbTronThue";
            this.cbbTronThue.Size = new System.Drawing.Size(129, 21);
            this.cbbTronThue.TabIndex = 31;
            this.cbbTronThue.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbBuonLau
            // 
            this.cbbBuonLau.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbBuonLau.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbBuonLau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Không";
            uiComboBoxItem5.Value = "0";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Có";
            uiComboBoxItem6.Value = "1";
            this.cbbBuonLau.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cbbBuonLau.Location = new System.Drawing.Point(553, 30);
            this.cbbBuonLau.Name = "cbbBuonLau";
            this.cbbBuonLau.Size = new System.Drawing.Size(129, 21);
            this.cbbBuonLau.TabIndex = 30;
            this.cbbBuonLau.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(19, 94);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(357, 13);
            this.label55.TabIndex = 0;
            this.label55.Text = "Bị các cơ quan quản lý nhà nước xử phạt vi phạm trong lĩnh vực kế toán :";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(19, 64);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(233, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "Bị xử phạt về hành vi trốn thuế, gian lận thuế :";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Red;
            this.label83.Location = new System.Drawing.Point(688, 92);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(16, 16);
            this.label83.TabIndex = 0;
            this.label83.Text = "*";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Red;
            this.label82.Location = new System.Drawing.Point(688, 62);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(16, 16);
            this.label82.TabIndex = 0;
            this.label82.Text = "*";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Red;
            this.label81.Location = new System.Drawing.Point(688, 32);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(16, 16);
            this.label81.TabIndex = 0;
            this.label81.Text = "*";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(19, 34);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(418, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "Bị xử lý về hành vi buôn lậu, vận chuyển trái phép hàng hóa qua biên giới, trốn t" +
                "huế :";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.dateNgayCapGPGD);
            this.uiGroupBox6.Controls.Add(this.txtSoDTGD);
            this.uiGroupBox6.Controls.Add(this.txtSoCMNDGD);
            this.uiGroupBox6.Controls.Add(this.label80);
            this.uiGroupBox6.Controls.Add(this.label79);
            this.uiGroupBox6.Controls.Add(this.label78);
            this.uiGroupBox6.Controls.Add(this.label77);
            this.uiGroupBox6.Controls.Add(this.label76);
            this.uiGroupBox6.Controls.Add(this.txtNoiDKHKGD);
            this.uiGroupBox6.Controls.Add(this.txtNoiCapGPGD);
            this.uiGroupBox6.Controls.Add(this.label11);
            this.uiGroupBox6.Controls.Add(this.label12);
            this.uiGroupBox6.Controls.Add(this.label13);
            this.uiGroupBox6.Controls.Add(this.label18);
            this.uiGroupBox6.Controls.Add(this.label19);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 558);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1155, 158);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.Text = "Tổng giám đốc hoặc Giám đốc";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dateNgayCapGPGD
            // 
            this.dateNgayCapGPGD.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.dateNgayCapGPGD.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateNgayCapGPGD.Appearance.Options.UseBackColor = true;
            this.dateNgayCapGPGD.Appearance.Options.UseFont = true;
            this.dateNgayCapGPGD.Location = new System.Drawing.Point(159, 52);
            this.dateNgayCapGPGD.Name = "dateNgayCapGPGD";
            this.dateNgayCapGPGD.ReadOnly = false;
            this.dateNgayCapGPGD.Size = new System.Drawing.Size(127, 21);
            this.dateNgayCapGPGD.TabIndex = 23;
            this.dateNgayCapGPGD.TagName = "";
            this.dateNgayCapGPGD.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateNgayCapGPGD.WhereCondition = "";
            // 
            // txtSoDTGD
            // 
            this.txtSoDTGD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDTGD.Location = new System.Drawing.Point(548, 22);
            this.txtSoDTGD.Name = "txtSoDTGD";
            this.txtSoDTGD.Size = new System.Drawing.Size(270, 21);
            this.txtSoDTGD.TabIndex = 22;
            this.txtSoDTGD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoCMNDGD
            // 
            this.txtSoCMNDGD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCMNDGD.Location = new System.Drawing.Point(159, 22);
            this.txtSoCMNDGD.Name = "txtSoCMNDGD";
            this.txtSoCMNDGD.Size = new System.Drawing.Size(284, 21);
            this.txtSoCMNDGD.TabIndex = 21;
            this.txtSoCMNDGD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Red;
            this.label80.Location = new System.Drawing.Point(824, 119);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(16, 16);
            this.label80.TabIndex = 0;
            this.label80.Text = "*";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Red;
            this.label79.Location = new System.Drawing.Point(824, 87);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(16, 16);
            this.label79.TabIndex = 0;
            this.label79.Text = "*";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Red;
            this.label78.Location = new System.Drawing.Point(293, 54);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(16, 16);
            this.label78.TabIndex = 0;
            this.label78.Text = "*";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Red;
            this.label77.Location = new System.Drawing.Point(824, 24);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(16, 16);
            this.label77.TabIndex = 0;
            this.label77.Text = "*";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Red;
            this.label76.Location = new System.Drawing.Point(445, 24);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(16, 16);
            this.label76.TabIndex = 0;
            this.label76.Text = "*";
            // 
            // txtNoiDKHKGD
            // 
            this.txtNoiDKHKGD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiDKHKGD.Location = new System.Drawing.Point(215, 117);
            this.txtNoiDKHKGD.Name = "txtNoiDKHKGD";
            this.txtNoiDKHKGD.Size = new System.Drawing.Size(603, 21);
            this.txtNoiDKHKGD.TabIndex = 25;
            this.txtNoiDKHKGD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNoiCapGPGD
            // 
            this.txtNoiCapGPGD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiCapGPGD.Location = new System.Drawing.Point(159, 85);
            this.txtNoiCapGPGD.Name = "txtNoiCapGPGD";
            this.txtNoiCapGPGD.Size = new System.Drawing.Size(659, 21);
            this.txtNoiCapGPGD.TabIndex = 24;
            this.txtNoiCapGPGD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(460, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Số điện thoại :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 121);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(166, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Nơi đăng ký hộ khẩu thường trú :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 89);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Nơi cấp giấy phép";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(19, 56);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(109, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Ngày cấp giấy phép :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(21, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(102, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Số CMND/hộ chiếu :";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.label74);
            this.uiGroupBox5.Controls.Add(this.label73);
            this.uiGroupBox5.Controls.Add(this.label75);
            this.uiGroupBox5.Controls.Add(this.label72);
            this.uiGroupBox5.Controls.Add(this.label71);
            this.uiGroupBox5.Controls.Add(this.dateNgayCapGPChuTich);
            this.uiGroupBox5.Controls.Add(this.txtSoDTChuTich);
            this.uiGroupBox5.Controls.Add(this.SoCMNDChuTich);
            this.uiGroupBox5.Controls.Add(this.txtNoiDKHKChuTich);
            this.uiGroupBox5.Controls.Add(this.txtNoiCapGPChuTich);
            this.uiGroupBox5.Controls.Add(this.label5);
            this.uiGroupBox5.Controls.Add(this.label4);
            this.uiGroupBox5.Controls.Add(this.label1);
            this.uiGroupBox5.Controls.Add(this.label2);
            this.uiGroupBox5.Controls.Add(this.label3);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox5.Location = new System.Drawing.Point(3, 409);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1155, 149);
            this.uiGroupBox5.TabIndex = 0;
            this.uiGroupBox5.Text = "Chủ tịch hội đồng quản trị hoặc Chủ tịch hội đồng thành viên";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Red;
            this.label74.Location = new System.Drawing.Point(824, 120);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(16, 16);
            this.label74.TabIndex = 0;
            this.label74.Text = "*";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Red;
            this.label73.Location = new System.Drawing.Point(824, 88);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(16, 16);
            this.label73.TabIndex = 0;
            this.label73.Text = "*";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Red;
            this.label75.Location = new System.Drawing.Point(445, 26);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(16, 16);
            this.label75.TabIndex = 0;
            this.label75.Text = "*";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Red;
            this.label72.Location = new System.Drawing.Point(293, 55);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(16, 16);
            this.label72.TabIndex = 0;
            this.label72.Text = "*";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Red;
            this.label71.Location = new System.Drawing.Point(824, 26);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(16, 16);
            this.label71.TabIndex = 0;
            this.label71.Text = "*";
            // 
            // dateNgayCapGPChuTich
            // 
            this.dateNgayCapGPChuTich.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.dateNgayCapGPChuTich.Appearance.Options.UseBackColor = true;
            this.dateNgayCapGPChuTich.Location = new System.Drawing.Point(160, 53);
            this.dateNgayCapGPChuTich.Name = "dateNgayCapGPChuTich";
            this.dateNgayCapGPChuTich.ReadOnly = false;
            this.dateNgayCapGPChuTich.Size = new System.Drawing.Size(127, 21);
            this.dateNgayCapGPChuTich.TabIndex = 18;
            this.dateNgayCapGPChuTich.TagName = "";
            this.dateNgayCapGPChuTich.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateNgayCapGPChuTich.WhereCondition = "";
            // 
            // txtSoDTChuTich
            // 
            this.txtSoDTChuTich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDTChuTich.Location = new System.Drawing.Point(545, 24);
            this.txtSoDTChuTich.Name = "txtSoDTChuTich";
            this.txtSoDTChuTich.Size = new System.Drawing.Size(273, 21);
            this.txtSoDTChuTich.TabIndex = 17;
            this.txtSoDTChuTich.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // SoCMNDChuTich
            // 
            this.SoCMNDChuTich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SoCMNDChuTich.Location = new System.Drawing.Point(159, 24);
            this.SoCMNDChuTich.Name = "SoCMNDChuTich";
            this.SoCMNDChuTich.Size = new System.Drawing.Size(284, 21);
            this.SoCMNDChuTich.TabIndex = 16;
            this.SoCMNDChuTich.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNoiDKHKChuTich
            // 
            this.txtNoiDKHKChuTich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiDKHKChuTich.Location = new System.Drawing.Point(215, 118);
            this.txtNoiDKHKChuTich.Name = "txtNoiDKHKChuTich";
            this.txtNoiDKHKChuTich.Size = new System.Drawing.Size(603, 21);
            this.txtNoiDKHKChuTich.TabIndex = 20;
            this.txtNoiDKHKChuTich.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNoiCapGPChuTich
            // 
            this.txtNoiCapGPChuTich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiCapGPChuTich.Location = new System.Drawing.Point(160, 86);
            this.txtNoiCapGPChuTich.Name = "txtNoiCapGPChuTich";
            this.txtNoiCapGPChuTich.Size = new System.Drawing.Size(658, 21);
            this.txtNoiCapGPChuTich.TabIndex = 19;
            this.txtNoiCapGPChuTich.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(460, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Số điện thoại :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(166, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nơi đăng ký hộ khẩu thường trú :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nơi cấp giấy phép";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ngày cấp giấy phép :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Số CMND/hộ chiếu :";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtTenDoanhNghiepTruoc);
            this.uiGroupBox4.Controls.Add(this.txtLyDoChuyenDoi);
            this.uiGroupBox4.Controls.Add(this.txtMaDoanhNghiepTruoc);
            this.uiGroupBox4.Controls.Add(this.label21);
            this.uiGroupBox4.Controls.Add(this.label24);
            this.uiGroupBox4.Controls.Add(this.label25);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 287);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1155, 122);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Thông tin doanh nghiệp XNK trước khi thay đổi";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtTenDoanhNghiepTruoc
            // 
            this.txtTenDoanhNghiepTruoc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoanhNghiepTruoc.Location = new System.Drawing.Point(159, 19);
            this.txtTenDoanhNghiepTruoc.Name = "txtTenDoanhNghiepTruoc";
            this.txtTenDoanhNghiepTruoc.Size = new System.Drawing.Size(659, 21);
            this.txtTenDoanhNghiepTruoc.TabIndex = 13;
            this.txtTenDoanhNghiepTruoc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtLyDoChuyenDoi
            // 
            this.txtLyDoChuyenDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLyDoChuyenDoi.Location = new System.Drawing.Point(159, 83);
            this.txtLyDoChuyenDoi.Name = "txtLyDoChuyenDoi";
            this.txtLyDoChuyenDoi.Size = new System.Drawing.Size(659, 21);
            this.txtLyDoChuyenDoi.TabIndex = 15;
            this.txtLyDoChuyenDoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDoanhNghiepTruoc
            // 
            this.txtMaDoanhNghiepTruoc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoanhNghiepTruoc.Location = new System.Drawing.Point(160, 50);
            this.txtMaDoanhNghiepTruoc.Name = "txtMaDoanhNghiepTruoc";
            this.txtMaDoanhNghiepTruoc.Size = new System.Drawing.Size(149, 21);
            this.txtMaDoanhNghiepTruoc.TabIndex = 14;
            this.txtMaDoanhNghiepTruoc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(22, 87);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(95, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Lý do chuyển đổi :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(21, 54);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(96, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Mã doanh nghiệp :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(22, 23);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(100, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Tên doanh nghiệp :";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.clcNgayKetThucNTC);
            this.uiGroupBox3.Controls.Add(this.ctrNuocXuatXu);
            this.uiGroupBox3.Controls.Add(this.label70);
            this.uiGroupBox3.Controls.Add(this.label69);
            this.uiGroupBox3.Controls.Add(this.label68);
            this.uiGroupBox3.Controls.Add(this.label67);
            this.uiGroupBox3.Controls.Add(this.label66);
            this.uiGroupBox3.Controls.Add(this.label65);
            this.uiGroupBox3.Controls.Add(this.cbbLoaiHinhDN);
            this.uiGroupBox3.Controls.Add(this.cbbTruSoChinh);
            this.uiGroupBox3.Controls.Add(this.txtTenDoanhNghiep);
            this.uiGroupBox3.Controls.Add(this.txtDiaChiTruSoChinh);
            this.uiGroupBox3.Controls.Add(this.txtNganhNgheSX);
            this.uiGroupBox3.Controls.Add(this.txtMaDoanhNghiep);
            this.uiGroupBox3.Controls.Add(this.label47);
            this.uiGroupBox3.Controls.Add(this.label151);
            this.uiGroupBox3.Controls.Add(this.label123);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.label10);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Controls.Add(this.label17);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 109);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1155, 178);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "Thông tin doanh nghiệp XNK";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // clcNgayKetThucNTC
            // 
            this.clcNgayKetThucNTC.CustomFormat = "dd/MM/yyyy";
            this.clcNgayKetThucNTC.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayKetThucNTC.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayKetThucNTC.DropDownCalendar.Name = "";
            this.clcNgayKetThucNTC.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayKetThucNTC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayKetThucNTC.Location = new System.Drawing.Point(483, 78);
            this.clcNgayKetThucNTC.Name = "clcNgayKetThucNTC";
            this.clcNgayKetThucNTC.Nullable = true;
            this.clcNgayKetThucNTC.NullButtonText = "Xóa";
            this.clcNgayKetThucNTC.ShowNullButton = true;
            this.clcNgayKetThucNTC.Size = new System.Drawing.Size(131, 21);
            this.clcNgayKetThucNTC.TabIndex = 10;
            this.clcNgayKetThucNTC.TodayButtonText = "Hôm nay";
            this.clcNgayKetThucNTC.Value = new System.DateTime(2018, 7, 27, 0, 0, 0, 0);
            this.clcNgayKetThucNTC.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // ctrNuocXuatXu
            // 
            this.ctrNuocXuatXu.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXuatXu.Appearance.Options.UseBackColor = true;
            this.ctrNuocXuatXu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ctrNuocXuatXu.Code = "";
            this.ctrNuocXuatXu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNuocXuatXu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrNuocXuatXu.IsOnlyWarning = false;
            this.ctrNuocXuatXu.IsValidate = true;
            this.ctrNuocXuatXu.Location = new System.Drawing.Point(156, 78);
            this.ctrNuocXuatXu.Name = "ctrNuocXuatXu";
            this.ctrNuocXuatXu.Name_VN = "";
            this.ctrNuocXuatXu.SetOnlyWarning = false;
            this.ctrNuocXuatXu.SetValidate = false;
            this.ctrNuocXuatXu.ShowColumnCode = true;
            this.ctrNuocXuatXu.ShowColumnName = true;
            this.ctrNuocXuatXu.Size = new System.Drawing.Size(159, 21);
            this.ctrNuocXuatXu.TabIndex = 9;
            this.ctrNuocXuatXu.TagCode = "";
            this.ctrNuocXuatXu.TagName = "";
            this.ctrNuocXuatXu.Where = null;
            this.ctrNuocXuatXu.WhereCondition = "";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Red;
            this.label70.Location = new System.Drawing.Point(824, 140);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(16, 16);
            this.label70.TabIndex = 0;
            this.label70.Text = "*";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Red;
            this.label69.Location = new System.Drawing.Point(824, 110);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(16, 16);
            this.label69.TabIndex = 0;
            this.label69.Text = "*";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Red;
            this.label68.Location = new System.Drawing.Point(824, 20);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(16, 16);
            this.label68.TabIndex = 0;
            this.label68.Text = "*";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Red;
            this.label67.Location = new System.Drawing.Point(311, 49);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(16, 16);
            this.label67.TabIndex = 0;
            this.label67.Text = "*";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Red;
            this.label66.Location = new System.Drawing.Point(620, 51);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(16, 16);
            this.label66.TabIndex = 0;
            this.label66.Text = "*";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Red;
            this.label65.Location = new System.Drawing.Point(313, 80);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(16, 16);
            this.label65.TabIndex = 0;
            this.label65.Text = "*";
            // 
            // cbbLoaiHinhDN
            // 
            this.cbbLoaiHinhDN.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiHinhDN.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbLoaiHinhDN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Doanh nghiệp có vốn đầu tư nước ngoài";
            uiComboBoxItem7.Value = "1";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Doanh nghiệp chế xuất";
            uiComboBoxItem8.Value = "2";
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Doanh nghiệp chế xuất có vốn đầu tư nước ngoài";
            uiComboBoxItem9.Value = "3";
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Doanh nghiệp khác";
            uiComboBoxItem10.Value = "9";
            this.cbbLoaiHinhDN.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem7,
            uiComboBoxItem8,
            uiComboBoxItem9,
            uiComboBoxItem10});
            this.cbbLoaiHinhDN.Location = new System.Drawing.Point(800, 48);
            this.cbbLoaiHinhDN.Name = "cbbLoaiHinhDN";
            this.cbbLoaiHinhDN.Size = new System.Drawing.Size(127, 21);
            this.cbbLoaiHinhDN.TabIndex = 8;
            this.cbbLoaiHinhDN.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbTruSoChinh
            // 
            this.cbbTruSoChinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbTruSoChinh.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbTruSoChinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Trụ sở thuộc quyền sở hữu của DN";
            uiComboBoxItem11.Value = "1";
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.IsSeparator = false;
            uiComboBoxItem12.Text = "Trụ sở thuê";
            uiComboBoxItem12.Value = "2";
            this.cbbTruSoChinh.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem11,
            uiComboBoxItem12});
            this.cbbTruSoChinh.Location = new System.Drawing.Point(483, 47);
            this.cbbTruSoChinh.Name = "cbbTruSoChinh";
            this.cbbTruSoChinh.Size = new System.Drawing.Size(131, 21);
            this.cbbTruSoChinh.TabIndex = 7;
            this.cbbTruSoChinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtTenDoanhNghiep
            // 
            this.txtTenDoanhNghiep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoanhNghiep.Location = new System.Drawing.Point(156, 18);
            this.txtTenDoanhNghiep.Name = "txtTenDoanhNghiep";
            this.txtTenDoanhNghiep.Size = new System.Drawing.Size(662, 21);
            this.txtTenDoanhNghiep.TabIndex = 5;
            this.txtTenDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiTruSoChinh
            // 
            this.txtDiaChiTruSoChinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiTruSoChinh.Location = new System.Drawing.Point(156, 108);
            this.txtDiaChiTruSoChinh.Name = "txtDiaChiTruSoChinh";
            this.txtDiaChiTruSoChinh.Size = new System.Drawing.Size(662, 21);
            this.txtDiaChiTruSoChinh.TabIndex = 11;
            this.txtDiaChiTruSoChinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNganhNgheSX
            // 
            this.txtNganhNgheSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNganhNgheSX.Location = new System.Drawing.Point(156, 138);
            this.txtNganhNgheSX.Name = "txtNganhNgheSX";
            this.txtNganhNgheSX.Size = new System.Drawing.Size(662, 21);
            this.txtNganhNgheSX.TabIndex = 12;
            this.txtNganhNgheSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(156, 47);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(149, 21);
            this.txtMaDoanhNghiep.TabIndex = 6;
            this.txtMaDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(19, 112);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(105, 13);
            this.label47.TabIndex = 0;
            this.label47.Text = "Địa chỉ trụ sở chính :";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(667, 52);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(127, 13);
            this.label151.TabIndex = 0;
            this.label151.Text = "Loại hình doanh nghiệp : ";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(332, 82);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(147, 13);
            this.label123.TabIndex = 0;
            this.label123.Text = "Ngày kết thúc năm tài chính :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(335, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Địa chỉ trụ sở chính :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 142);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Ngành nghề sản xuất :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Nước đầu tư :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(22, 51);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Mã doanh nghiệp :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(22, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Tên doanh nghiệp :";
            // 
            // uiGroupBox25
            // 
            this.uiGroupBox25.AutoScroll = true;
            this.uiGroupBox25.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox25.Controls.Add(this.dateNgayTN);
            this.uiGroupBox25.Controls.Add(this.ctrCoQuanHaiQuan);
            this.uiGroupBox25.Controls.Add(this.label155);
            this.uiGroupBox25.Controls.Add(this.cbbLoaiSua);
            this.uiGroupBox25.Controls.Add(this.lblTrangThai);
            this.uiGroupBox25.Controls.Add(this.label15);
            this.uiGroupBox25.Controls.Add(this.label8);
            this.uiGroupBox25.Controls.Add(this.label14);
            this.uiGroupBox25.Controls.Add(this.label9);
            this.uiGroupBox25.Controls.Add(this.txtSoTN);
            this.uiGroupBox25.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox25.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox25.Name = "uiGroupBox25";
            this.uiGroupBox25.Size = new System.Drawing.Size(1155, 92);
            this.uiGroupBox25.TabIndex = 0;
            this.uiGroupBox25.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dateNgayTN
            // 
            this.dateNgayTN.Location = new System.Drawing.Point(433, 21);
            this.dateNgayTN.Name = "dateNgayTN";
            this.dateNgayTN.ReadOnly = false;
            this.dateNgayTN.Size = new System.Drawing.Size(127, 21);
            this.dateNgayTN.TabIndex = 2;
            this.dateNgayTN.TagName = "";
            this.dateNgayTN.Value = new System.DateTime(2018, 8, 8, 0, 0, 0, 0);
            this.dateNgayTN.WhereCondition = "";
            // 
            // ctrCoQuanHaiQuan
            // 
            this.ctrCoQuanHaiQuan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrCoQuanHaiQuan.Appearance.Options.UseBackColor = true;
            this.ctrCoQuanHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrCoQuanHaiQuan.Code = "";
            this.ctrCoQuanHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrCoQuanHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrCoQuanHaiQuan.IsOnlyWarning = false;
            this.ctrCoQuanHaiQuan.IsValidate = true;
            this.ctrCoQuanHaiQuan.Location = new System.Drawing.Point(149, 59);
            this.ctrCoQuanHaiQuan.Name = "ctrCoQuanHaiQuan";
            this.ctrCoQuanHaiQuan.Name_VN = "";
            this.ctrCoQuanHaiQuan.SetOnlyWarning = false;
            this.ctrCoQuanHaiQuan.SetValidate = false;
            this.ctrCoQuanHaiQuan.ShowColumnCode = true;
            this.ctrCoQuanHaiQuan.ShowColumnName = true;
            this.ctrCoQuanHaiQuan.Size = new System.Drawing.Size(411, 20);
            this.ctrCoQuanHaiQuan.TabIndex = 3;
            this.ctrCoQuanHaiQuan.TagCode = "";
            this.ctrCoQuanHaiQuan.TagName = "";
            this.ctrCoQuanHaiQuan.Where = null;
            this.ctrCoQuanHaiQuan.WhereCondition = "";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(670, 62);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(57, 13);
            this.label155.TabIndex = 0;
            this.label155.Text = "Loại sửa : ";
            // 
            // cbbLoaiSua
            // 
            this.cbbLoaiSua.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiSua.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbLoaiSua.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem13.FormatStyle.Alpha = 0;
            uiComboBoxItem13.IsSeparator = false;
            uiComboBoxItem13.Text = "Bổ sung thông tin";
            uiComboBoxItem13.Value = "1";
            uiComboBoxItem14.FormatStyle.Alpha = 0;
            uiComboBoxItem14.IsSeparator = false;
            uiComboBoxItem14.Text = "Sửa thông tin";
            uiComboBoxItem14.Value = "2";
            this.cbbLoaiSua.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem13,
            uiComboBoxItem14});
            this.cbbLoaiSua.Location = new System.Drawing.Point(800, 57);
            this.cbbLoaiSua.Name = "cbbLoaiSua";
            this.cbbLoaiSua.Size = new System.Drawing.Size(127, 21);
            this.cbbLoaiSua.TabIndex = 4;
            this.cbbLoaiSua.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(707, 25);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(87, 13);
            this.lblTrangThai.TabIndex = 0;
            this.lblTrangThai.Text = "Chưa khai báo";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Số tiếp nhận";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(599, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Trạng thái: ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 63);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Hải quan";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(307, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Ngày tiếp nhận";
            // 
            // txtSoTN
            // 
            this.txtSoTN.Location = new System.Drawing.Point(149, 21);
            this.txtSoTN.Name = "txtSoTN";
            this.txtSoTN.Size = new System.Drawing.Size(149, 21);
            this.txtSoTN.TabIndex = 1;
            this.txtSoTN.Text = "0";
            this.txtSoTN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(1180, 920);
            this.uiTab1.TabIndex = 1;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2,
            this.uiTabPage3,
            this.uiTabPage4,
            this.uiTabPage5});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            this.uiTab1.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.uiTab1_SelectedTabChanged);
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.AutoScroll = true;
            this.uiTabPage4.Controls.Add(this.uiGroupBox29);
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(1178, 898);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "Công ty mẹ nhập khẩu";
            // 
            // uiGroupBox29
            // 
            this.uiGroupBox29.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox29.Controls.Add(this.dgListAttachFile);
            this.uiGroupBox29.Controls.Add(this.uiGroupBox60);
            this.uiGroupBox29.Controls.Add(this.uiGroupBox23);
            this.uiGroupBox29.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox29.Controls.Add(this.uiGroupBox30);
            this.uiGroupBox29.Controls.Add(this.uiGroupBox31);
            this.uiGroupBox29.Controls.Add(this.uiGroupBox32);
            this.uiGroupBox29.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox29.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox29.Name = "uiGroupBox29";
            this.uiGroupBox29.Size = new System.Drawing.Size(1161, 1017);
            this.uiGroupBox29.TabIndex = 0;
            this.uiGroupBox29.Text = "Công ty mẹ nhập khẩu, cung ứng nguyên liệu, vật tư để gia công, sản xuất xuất khẩ" +
                "u cho các đơn vị thành viên trực thuộc";
            this.uiGroupBox29.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListAttachFile
            // 
            this.dgListAttachFile.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListAttachFile.AlternatingColors = true;
            this.dgListAttachFile.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListAttachFile.ColumnAutoResize = true;
            dgListAttachFile_DesignTimeLayout.LayoutString = resources.GetString("dgListAttachFile_DesignTimeLayout.LayoutString");
            this.dgListAttachFile.DesignTimeLayout = dgListAttachFile_DesignTimeLayout;
            this.dgListAttachFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListAttachFile.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListAttachFile.FrozenColumns = 3;
            this.dgListAttachFile.GroupByBoxVisible = false;
            this.dgListAttachFile.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListAttachFile.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListAttachFile.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListAttachFile.Location = new System.Drawing.Point(3, 859);
            this.dgListAttachFile.Margin = new System.Windows.Forms.Padding(0);
            this.dgListAttachFile.Name = "dgListAttachFile";
            this.dgListAttachFile.RecordNavigator = true;
            this.dgListAttachFile.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListAttachFile.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListAttachFile.Size = new System.Drawing.Size(1155, 155);
            this.dgListAttachFile.TabIndex = 73;
            this.dgListAttachFile.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListAttachFile.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListAttachFile_RowDoubleClick);
            // 
            // uiGroupBox60
            // 
            this.uiGroupBox60.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox60.Controls.Add(this.btnDeleteFile);
            this.uiGroupBox60.Controls.Add(this.btnViewFile);
            this.uiGroupBox60.Controls.Add(this.btnAddFile);
            this.uiGroupBox60.Controls.Add(this.label184);
            this.uiGroupBox60.Controls.Add(this.label180);
            this.uiGroupBox60.Controls.Add(this.label181);
            this.uiGroupBox60.Controls.Add(this.label182);
            this.uiGroupBox60.Controls.Add(this.lblTotalSize);
            this.uiGroupBox60.Controls.Add(this.txtFileSize);
            this.uiGroupBox60.Controls.Add(this.lblFileName);
            this.uiGroupBox60.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox60.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox60.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox60.Location = new System.Drawing.Point(3, 765);
            this.uiGroupBox60.Name = "uiGroupBox60";
            this.uiGroupBox60.Size = new System.Drawing.Size(1155, 94);
            this.uiGroupBox60.TabIndex = 0;
            this.uiGroupBox60.Text = "FILE ĐÍNH KÈM";
            this.uiGroupBox60.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteFile
            // 
            this.btnDeleteFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteFile.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteFile.Image")));
            this.btnDeleteFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteFile.Location = new System.Drawing.Point(495, 41);
            this.btnDeleteFile.Name = "btnDeleteFile";
            this.btnDeleteFile.Size = new System.Drawing.Size(125, 23);
            this.btnDeleteFile.TabIndex = 15;
            this.btnDeleteFile.Text = "Xóa File";
            this.btnDeleteFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteFile.Click += new System.EventHandler(this.btnDeleteFile_Click);
            // 
            // btnViewFile
            // 
            this.btnViewFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewFile.Image = ((System.Drawing.Image)(resources.GetObject("btnViewFile.Image")));
            this.btnViewFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnViewFile.Location = new System.Drawing.Point(364, 41);
            this.btnViewFile.Name = "btnViewFile";
            this.btnViewFile.Size = new System.Drawing.Size(125, 23);
            this.btnViewFile.TabIndex = 14;
            this.btnViewFile.Text = "Xem File";
            this.btnViewFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnViewFile.Click += new System.EventHandler(this.btnViewFile_Click);
            // 
            // btnAddFile
            // 
            this.btnAddFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddFile.Image = ((System.Drawing.Image)(resources.GetObject("btnAddFile.Image")));
            this.btnAddFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddFile.Location = new System.Drawing.Point(237, 41);
            this.btnAddFile.Name = "btnAddFile";
            this.btnAddFile.Size = new System.Drawing.Size(125, 23);
            this.btnAddFile.TabIndex = 13;
            this.btnAddFile.Text = "Thêm File";
            this.btnAddFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddFile.Click += new System.EventHandler(this.btnAddFile_Click);
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.BackColor = System.Drawing.Color.Transparent;
            this.label184.Location = new System.Drawing.Point(11, 71);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(98, 13);
            this.label184.TabIndex = 0;
            this.label184.Text = "Tổng dung lượng : ";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.BackColor = System.Drawing.Color.Transparent;
            this.label180.Location = new System.Drawing.Point(11, 46);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(72, 13);
            this.label180.TabIndex = 0;
            this.label180.Text = "Dung lượng : ";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.BackColor = System.Drawing.Color.Transparent;
            this.label181.Location = new System.Drawing.Point(11, 23);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(51, 13);
            this.label181.TabIndex = 0;
            this.label181.Text = "Tên File :";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.BackColor = System.Drawing.Color.Transparent;
            this.label182.ForeColor = System.Drawing.Color.Red;
            this.label182.Location = new System.Drawing.Point(235, 71);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(374, 13);
            this.label182.TabIndex = 0;
            this.label182.Text = "Doanh nghiệp chú ý   : Tổng dung lượng File đính kèm không được quá (3MB)";
            // 
            // lblTotalSize
            // 
            this.lblTotalSize.AutoSize = true;
            this.lblTotalSize.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalSize.ForeColor = System.Drawing.Color.Red;
            this.lblTotalSize.Location = new System.Drawing.Point(114, 71);
            this.lblTotalSize.Name = "lblTotalSize";
            this.lblTotalSize.Size = new System.Drawing.Size(82, 13);
            this.lblTotalSize.TabIndex = 0;
            this.lblTotalSize.Text = "{Total File Size}";
            // 
            // txtFileSize
            // 
            this.txtFileSize.AutoSize = true;
            this.txtFileSize.BackColor = System.Drawing.Color.Transparent;
            this.txtFileSize.ForeColor = System.Drawing.Color.Red;
            this.txtFileSize.Location = new System.Drawing.Point(114, 46);
            this.txtFileSize.Name = "txtFileSize";
            this.txtFileSize.Size = new System.Drawing.Size(55, 13);
            this.txtFileSize.TabIndex = 0;
            this.txtFileSize.Text = "{File Size}";
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.BackColor = System.Drawing.Color.Transparent;
            this.lblFileName.ForeColor = System.Drawing.Color.Blue;
            this.lblFileName.Location = new System.Drawing.Point(114, 24);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(63, 13);
            this.lblFileName.TabIndex = 0;
            this.lblFileName.Text = "{File Name}";
            // 
            // uiGroupBox23
            // 
            this.uiGroupBox23.AutoScroll = true;
            this.uiGroupBox23.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox23.Controls.Add(this.txtGhiChu);
            this.uiGroupBox23.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox23.Location = new System.Drawing.Point(3, 701);
            this.uiGroupBox23.Name = "uiGroupBox23";
            this.uiGroupBox23.Size = new System.Drawing.Size(1155, 64);
            this.uiGroupBox23.TabIndex = 65;
            this.uiGroupBox23.Text = "GHI CHÚ";
            this.uiGroupBox23.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtGhiChu.Location = new System.Drawing.Point(3, 17);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(1149, 44);
            this.txtGhiChu.TabIndex = 70;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.uiGroupBox34);
            this.uiGroupBox7.Controls.Add(this.uiGroupBox33);
            this.uiGroupBox7.Controls.Add(this.uiGroupBox10);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox7.Location = new System.Drawing.Point(3, 341);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(1155, 360);
            this.uiGroupBox7.TabIndex = 0;
            this.uiGroupBox7.Text = "LỊCH SỬ KIỂM TRA CƠ SỞ SẢN XUẤT NĂNG LỰC SẢN XUẤT";
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox34
            // 
            this.uiGroupBox34.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox34.Controls.Add(this.dgListKiemTra);
            this.uiGroupBox34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox34.Location = new System.Drawing.Point(3, 187);
            this.uiGroupBox34.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox34.Name = "uiGroupBox34";
            this.uiGroupBox34.Size = new System.Drawing.Size(1149, 170);
            this.uiGroupBox34.TabIndex = 72;
            this.uiGroupBox34.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListKiemTra
            // 
            this.dgListKiemTra.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListKiemTra.AlternatingColors = true;
            this.dgListKiemTra.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListKiemTra.ColumnAutoResize = true;
            dgListKiemTra_DesignTimeLayout.LayoutString = resources.GetString("dgListKiemTra_DesignTimeLayout.LayoutString");
            this.dgListKiemTra.DesignTimeLayout = dgListKiemTra_DesignTimeLayout;
            this.dgListKiemTra.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListKiemTra.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListKiemTra.FrozenColumns = 3;
            this.dgListKiemTra.GroupByBoxVisible = false;
            this.dgListKiemTra.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListKiemTra.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListKiemTra.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListKiemTra.Location = new System.Drawing.Point(3, 8);
            this.dgListKiemTra.Margin = new System.Windows.Forms.Padding(0);
            this.dgListKiemTra.Name = "dgListKiemTra";
            this.dgListKiemTra.RecordNavigator = true;
            this.dgListKiemTra.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListKiemTra.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListKiemTra.Size = new System.Drawing.Size(1143, 159);
            this.dgListKiemTra.TabIndex = 29;
            this.dgListKiemTra.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListKiemTra.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListKiemTra_RowDoubleClick);
            // 
            // uiGroupBox33
            // 
            this.uiGroupBox33.AutoScroll = true;
            this.uiGroupBox33.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox33.Controls.Add(this.btnDeleteKiemTra);
            this.uiGroupBox33.Controls.Add(this.btnAddKiemTra);
            this.uiGroupBox33.Controls.Add(this.dateNgayKT);
            this.uiGroupBox33.Controls.Add(this.txtSoKetLuanKT);
            this.uiGroupBox33.Controls.Add(this.txtSoBienBanKT);
            this.uiGroupBox33.Controls.Add(this.label26);
            this.uiGroupBox33.Controls.Add(this.label22);
            this.uiGroupBox33.Controls.Add(this.label99);
            this.uiGroupBox33.Controls.Add(this.label23);
            this.uiGroupBox33.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox33.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox33.Location = new System.Drawing.Point(3, 64);
            this.uiGroupBox33.Name = "uiGroupBox33";
            this.uiGroupBox33.Size = new System.Drawing.Size(1149, 123);
            this.uiGroupBox33.TabIndex = 0;
            this.uiGroupBox33.Text = "THÔNG TIN CÁC LẦN KIỂM TRA";
            this.uiGroupBox33.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteKiemTra
            // 
            this.btnDeleteKiemTra.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteKiemTra.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteKiemTra.Image")));
            this.btnDeleteKiemTra.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteKiemTra.Location = new System.Drawing.Point(383, 91);
            this.btnDeleteKiemTra.Name = "btnDeleteKiemTra";
            this.btnDeleteKiemTra.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteKiemTra.TabIndex = 12;
            this.btnDeleteKiemTra.Text = "Xóa";
            this.btnDeleteKiemTra.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteKiemTra.Click += new System.EventHandler(this.btnDeleteKiemTra_Click);
            // 
            // btnAddKiemTra
            // 
            this.btnAddKiemTra.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddKiemTra.Image = ((System.Drawing.Image)(resources.GetObject("btnAddKiemTra.Image")));
            this.btnAddKiemTra.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddKiemTra.Location = new System.Drawing.Point(287, 91);
            this.btnAddKiemTra.Name = "btnAddKiemTra";
            this.btnAddKiemTra.Size = new System.Drawing.Size(90, 23);
            this.btnAddKiemTra.TabIndex = 11;
            this.btnAddKiemTra.Text = "Ghi";
            this.btnAddKiemTra.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddKiemTra.Click += new System.EventHandler(this.btnAddKiemTra_Click);
            // 
            // dateNgayKT
            // 
            this.dateNgayKT.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateNgayKT.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dateNgayKT.Appearance.Options.UseFont = true;
            this.dateNgayKT.Appearance.Options.UseForeColor = true;
            this.dateNgayKT.Location = new System.Drawing.Point(154, 92);
            this.dateNgayKT.Name = "dateNgayKT";
            this.dateNgayKT.ReadOnly = false;
            this.dateNgayKT.Size = new System.Drawing.Size(127, 21);
            this.dateNgayKT.TabIndex = 10;
            this.dateNgayKT.TagName = "";
            this.dateNgayKT.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateNgayKT.WhereCondition = "";
            // 
            // txtSoKetLuanKT
            // 
            this.txtSoKetLuanKT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoKetLuanKT.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoKetLuanKT.Location = new System.Drawing.Point(154, 63);
            this.txtSoKetLuanKT.Name = "txtSoKetLuanKT";
            this.txtSoKetLuanKT.Size = new System.Drawing.Size(663, 21);
            this.txtSoKetLuanKT.TabIndex = 9;
            this.txtSoKetLuanKT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoBienBanKT
            // 
            this.txtSoBienBanKT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoBienBanKT.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoBienBanKT.Location = new System.Drawing.Point(154, 34);
            this.txtSoBienBanKT.Name = "txtSoBienBanKT";
            this.txtSoBienBanKT.Size = new System.Drawing.Size(663, 21);
            this.txtSoBienBanKT.TabIndex = 8;
            this.txtSoBienBanKT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(12, 96);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(80, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Ngày kiểm tra :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 67);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(108, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Số kết luận kiểm tra :";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(12, 17);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(472, 13);
            this.label99.TabIndex = 0;
            this.label99.Text = "Trường hợp đã được cơ quan hải quan kiểm tra trước thời điểm thông báo thì ghi đầ" +
                "y đủ thông tin";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(12, 38);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(111, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Số biên bản kiểm tra :";
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.cbbDaKiemTra);
            this.uiGroupBox10.Controls.Add(this.label27);
            this.uiGroupBox10.Controls.Add(this.label20);
            this.uiGroupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox10.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(1149, 47);
            this.uiGroupBox10.TabIndex = 0;
            this.uiGroupBox10.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbDaKiemTra
            // 
            this.cbbDaKiemTra.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbDaKiemTra.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbDaKiemTra.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbDaKiemTra.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem47.FormatStyle.Alpha = 0;
            uiComboBoxItem47.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem47.IsSeparator = false;
            uiComboBoxItem47.Text = "Chưa được kiểm tra";
            uiComboBoxItem47.Value = "0";
            uiComboBoxItem48.FormatStyle.Alpha = 0;
            uiComboBoxItem48.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem48.IsSeparator = false;
            uiComboBoxItem48.Text = "Đã được kiểm tra";
            uiComboBoxItem48.Value = "1";
            this.cbbDaKiemTra.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem47,
            uiComboBoxItem48});
            this.cbbDaKiemTra.Location = new System.Drawing.Point(280, 14);
            this.cbbDaKiemTra.Name = "cbbDaKiemTra";
            this.cbbDaKiemTra.Size = new System.Drawing.Size(127, 21);
            this.cbbDaKiemTra.TabIndex = 7;
            this.cbbDaKiemTra.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(12, 18);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(211, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Đã/Chưa được cơ quan hải quan kiểm tra :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(413, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 16);
            this.label20.TabIndex = 0;
            this.label20.Text = "*";
            // 
            // uiGroupBox30
            // 
            this.uiGroupBox30.AutoScroll = true;
            this.uiGroupBox30.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox30.Controls.Add(this.dgListCTM);
            this.uiGroupBox30.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox30.Location = new System.Drawing.Point(3, 184);
            this.uiGroupBox30.Name = "uiGroupBox30";
            this.uiGroupBox30.Size = new System.Drawing.Size(1155, 157);
            this.uiGroupBox30.TabIndex = 15;
            this.uiGroupBox30.Text = "DANH SÁCH CHI NHÁNH";
            this.uiGroupBox30.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListCTM
            // 
            this.dgListCTM.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListCTM.AlternatingColors = true;
            this.dgListCTM.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListCTM.ColumnAutoResize = true;
            dgListCTM_DesignTimeLayout.LayoutString = resources.GetString("dgListCTM_DesignTimeLayout.LayoutString");
            this.dgListCTM.DesignTimeLayout = dgListCTM_DesignTimeLayout;
            this.dgListCTM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListCTM.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListCTM.FrozenColumns = 3;
            this.dgListCTM.GroupByBoxVisible = false;
            this.dgListCTM.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListCTM.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListCTM.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListCTM.Location = new System.Drawing.Point(3, 17);
            this.dgListCTM.Margin = new System.Windows.Forms.Padding(0);
            this.dgListCTM.Name = "dgListCTM";
            this.dgListCTM.RecordNavigator = true;
            this.dgListCTM.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListCTM.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListCTM.Size = new System.Drawing.Size(1149, 137);
            this.dgListCTM.TabIndex = 14;
            this.dgListCTM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListCTM.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListCTM_RowDoubleClick);
            // 
            // uiGroupBox31
            // 
            this.uiGroupBox31.AutoScroll = true;
            this.uiGroupBox31.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox31.Controls.Add(this.btnDeleteCTM);
            this.uiGroupBox31.Controls.Add(this.btnAddCTM);
            this.uiGroupBox31.Controls.Add(this.txtTenDoanhNghiepCTM);
            this.uiGroupBox31.Controls.Add(this.txtDiaChiCSSXCTM);
            this.uiGroupBox31.Controls.Add(this.label49);
            this.uiGroupBox31.Controls.Add(this.txtMaDoanhNghiepCTM);
            this.uiGroupBox31.Controls.Add(this.label50);
            this.uiGroupBox31.Controls.Add(this.label51);
            this.uiGroupBox31.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox31.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox31.Location = new System.Drawing.Point(3, 77);
            this.uiGroupBox31.Name = "uiGroupBox31";
            this.uiGroupBox31.Size = new System.Drawing.Size(1155, 107);
            this.uiGroupBox31.TabIndex = 0;
            this.uiGroupBox31.Text = "CHI NHÁNH";
            this.uiGroupBox31.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteCTM
            // 
            this.btnDeleteCTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteCTM.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteCTM.Image")));
            this.btnDeleteCTM.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteCTM.Location = new System.Drawing.Point(712, 75);
            this.btnDeleteCTM.Name = "btnDeleteCTM";
            this.btnDeleteCTM.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteCTM.TabIndex = 6;
            this.btnDeleteCTM.Text = "Xóa";
            this.btnDeleteCTM.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteCTM.Click += new System.EventHandler(this.btnDeleteCTM_Click);
            // 
            // btnAddCTM
            // 
            this.btnAddCTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCTM.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCTM.Image")));
            this.btnAddCTM.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddCTM.Location = new System.Drawing.Point(616, 75);
            this.btnAddCTM.Name = "btnAddCTM";
            this.btnAddCTM.Size = new System.Drawing.Size(90, 23);
            this.btnAddCTM.TabIndex = 5;
            this.btnAddCTM.Text = "Ghi";
            this.btnAddCTM.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddCTM.Click += new System.EventHandler(this.btnAddCTM_Click);
            // 
            // txtTenDoanhNghiepCTM
            // 
            this.txtTenDoanhNghiepCTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoanhNghiepCTM.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtTenDoanhNghiepCTM.Location = new System.Drawing.Point(158, 15);
            this.txtTenDoanhNghiepCTM.Name = "txtTenDoanhNghiepCTM";
            this.txtTenDoanhNghiepCTM.Size = new System.Drawing.Size(448, 21);
            this.txtTenDoanhNghiepCTM.TabIndex = 2;
            this.txtTenDoanhNghiepCTM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiCSSXCTM
            // 
            this.txtDiaChiCSSXCTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiCSSXCTM.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtDiaChiCSSXCTM.Location = new System.Drawing.Point(158, 76);
            this.txtDiaChiCSSXCTM.Name = "txtDiaChiCSSXCTM";
            this.txtDiaChiCSSXCTM.Size = new System.Drawing.Size(448, 21);
            this.txtDiaChiCSSXCTM.TabIndex = 4;
            this.txtDiaChiCSSXCTM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(11, 80);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(74, 13);
            this.label49.TabIndex = 0;
            this.label49.Text = "Địa chỉ CSSX :";
            // 
            // txtMaDoanhNghiepCTM
            // 
            this.txtMaDoanhNghiepCTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoanhNghiepCTM.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMaDoanhNghiepCTM.Location = new System.Drawing.Point(158, 46);
            this.txtMaDoanhNghiepCTM.Name = "txtMaDoanhNghiepCTM";
            this.txtMaDoanhNghiepCTM.Size = new System.Drawing.Size(215, 21);
            this.txtMaDoanhNghiepCTM.TabIndex = 3;
            this.txtMaDoanhNghiepCTM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(11, 50);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(96, 13);
            this.label50.TabIndex = 0;
            this.label50.Text = "Mã doanh nghiệp :";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(10, 19);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(100, 13);
            this.label51.TabIndex = 0;
            this.label51.Text = "Tên doanh nghiệp :";
            // 
            // uiGroupBox32
            // 
            this.uiGroupBox32.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox32.Controls.Add(this.label98);
            this.uiGroupBox32.Controls.Add(this.txtSoLuongThanhVienCTM);
            this.uiGroupBox32.Controls.Add(this.label52);
            this.uiGroupBox32.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox32.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox32.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox32.Name = "uiGroupBox32";
            this.uiGroupBox32.Size = new System.Drawing.Size(1155, 60);
            this.uiGroupBox32.TabIndex = 0;
            this.uiGroupBox32.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Red;
            this.label98.Location = new System.Drawing.Point(12, 38);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(450, 13);
            this.label98.TabIndex = 60;
            this.label98.Text = "Chú ý : Số lượng thành viên phải bằng số lượng chi nhánh ở danh sách phía dưới";
            // 
            // txtSoLuongThanhVienCTM
            // 
            this.txtSoLuongThanhVienCTM.DecimalDigits = 20;
            this.txtSoLuongThanhVienCTM.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongThanhVienCTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongThanhVienCTM.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoLuongThanhVienCTM.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongThanhVienCTM.Location = new System.Drawing.Point(158, 14);
            this.txtSoLuongThanhVienCTM.MaxLength = 15;
            this.txtSoLuongThanhVienCTM.Name = "txtSoLuongThanhVienCTM";
            this.txtSoLuongThanhVienCTM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongThanhVienCTM.Size = new System.Drawing.Size(215, 21);
            this.txtSoLuongThanhVienCTM.TabIndex = 1;
            this.txtSoLuongThanhVienCTM.Text = "0";
            this.txtSoLuongThanhVienCTM.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongThanhVienCTM.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongThanhVienCTM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(12, 18);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(110, 13);
            this.label52.TabIndex = 0;
            this.label52.Text = "Số lượng thành viên :";
            // 
            // uiTabPage5
            // 
            this.uiTabPage5.AutoScroll = true;
            this.uiTabPage5.Controls.Add(this.uiGroupBox46);
            this.uiTabPage5.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage5.Name = "uiTabPage5";
            this.uiTabPage5.Size = new System.Drawing.Size(1178, 898);
            this.uiTabPage5.TabStop = true;
            this.uiTabPage5.Text = "Thông tin Cơ sở sản xuất thuê gia công lại (CSSX)";
            // 
            // uiGroupBox46
            // 
            this.uiGroupBox46.AutoScroll = true;
            this.uiGroupBox46.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox46.Controls.Add(this.uiGroupBox47);
            this.uiGroupBox46.Controls.Add(this.uiGroupBox51);
            this.uiGroupBox46.Controls.Add(this.uiGroupBox49);
            this.uiGroupBox46.Controls.Add(this.uiGroupBox48);
            this.uiGroupBox46.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox46.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox46.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox46.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox46.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox46.Name = "uiGroupBox46";
            this.uiGroupBox46.Size = new System.Drawing.Size(1161, 1323);
            this.uiGroupBox46.TabIndex = 0;
            this.uiGroupBox46.Text = "I. Thông tin Cơ sở sản xuất theo từng đối tác nhận GC";
            this.uiGroupBox46.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox47
            // 
            this.uiGroupBox47.AutoScroll = true;
            this.uiGroupBox47.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox47.Controls.Add(this.dgListDoiTac);
            this.uiGroupBox47.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox47.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox47.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox47.Location = new System.Drawing.Point(3, 1235);
            this.uiGroupBox47.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox47.Name = "uiGroupBox47";
            this.uiGroupBox47.Size = new System.Drawing.Size(1138, 199);
            this.uiGroupBox47.TabIndex = 39;
            this.uiGroupBox47.Text = "II. DANH SÁCH CƠ SỞ SẢN XUẤT";
            this.uiGroupBox47.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListDoiTac
            // 
            this.dgListDoiTac.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDoiTac.AlternatingColors = true;
            this.dgListDoiTac.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDoiTac.ColumnAutoResize = true;
            dgListDoiTac_DesignTimeLayout.LayoutString = resources.GetString("dgListDoiTac_DesignTimeLayout.LayoutString");
            this.dgListDoiTac.DesignTimeLayout = dgListDoiTac_DesignTimeLayout;
            this.dgListDoiTac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDoiTac.FrozenColumns = 3;
            this.dgListDoiTac.GroupByBoxVisible = false;
            this.dgListDoiTac.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDoiTac.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDoiTac.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDoiTac.Location = new System.Drawing.Point(3, 17);
            this.dgListDoiTac.Margin = new System.Windows.Forms.Padding(0);
            this.dgListDoiTac.Name = "dgListDoiTac";
            this.dgListDoiTac.RecordNavigator = true;
            this.dgListDoiTac.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDoiTac.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDoiTac.Size = new System.Drawing.Size(1132, 179);
            this.dgListDoiTac.TabIndex = 40;
            this.dgListDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListDoiTac.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListDoiTac_RowDoubleClick);
            // 
            // uiGroupBox51
            // 
            this.uiGroupBox51.AutoScroll = true;
            this.uiGroupBox51.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox51.Controls.Add(this.uiGroupBox57);
            this.uiGroupBox51.Controls.Add(this.uiGroupBox54);
            this.uiGroupBox51.Controls.Add(this.uiGroupBox53);
            this.uiGroupBox51.Controls.Add(this.uiGroupBox52);
            this.uiGroupBox51.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox51.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox51.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox51.Location = new System.Drawing.Point(3, 311);
            this.uiGroupBox51.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox51.Name = "uiGroupBox51";
            this.uiGroupBox51.Size = new System.Drawing.Size(1138, 927);
            this.uiGroupBox51.TabIndex = 0;
            this.uiGroupBox51.Text = "3. Thông tin Cơ sở sản xuất";
            this.uiGroupBox51.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox57
            // 
            this.uiGroupBox57.AutoScroll = true;
            this.uiGroupBox57.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox57.Controls.Add(this.uiGroupBox61);
            this.uiGroupBox57.Controls.Add(this.uiGroupBox58);
            this.uiGroupBox57.Controls.Add(this.uiGroupBox59);
            this.uiGroupBox57.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox57.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox57.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox57.Location = new System.Drawing.Point(3, 538);
            this.uiGroupBox57.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox57.Name = "uiGroupBox57";
            this.uiGroupBox57.Size = new System.Drawing.Size(1132, 386);
            this.uiGroupBox57.TabIndex = 0;
            this.uiGroupBox57.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox61
            // 
            this.uiGroupBox61.AutoScroll = true;
            this.uiGroupBox61.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox61.Controls.Add(this.dgListTTCSSX);
            this.uiGroupBox61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox61.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox61.Location = new System.Drawing.Point(3, 268);
            this.uiGroupBox61.Name = "uiGroupBox61";
            this.uiGroupBox61.Size = new System.Drawing.Size(1126, 115);
            this.uiGroupBox61.TabIndex = 46;
            this.uiGroupBox61.Text = "Danh sách Thông tin Cơ sở sản xuất";
            this.uiGroupBox61.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListTTCSSX
            // 
            this.dgListTTCSSX.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTTCSSX.AlternatingColors = true;
            this.dgListTTCSSX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTTCSSX.ColumnAutoResize = true;
            dgListTTCSSX_DesignTimeLayout.LayoutString = resources.GetString("dgListTTCSSX_DesignTimeLayout.LayoutString");
            this.dgListTTCSSX.DesignTimeLayout = dgListTTCSSX_DesignTimeLayout;
            this.dgListTTCSSX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTTCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTTCSSX.FrozenColumns = 3;
            this.dgListTTCSSX.GroupByBoxVisible = false;
            this.dgListTTCSSX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTTCSSX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTTCSSX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTTCSSX.Location = new System.Drawing.Point(3, 17);
            this.dgListTTCSSX.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTTCSSX.Name = "dgListTTCSSX";
            this.dgListTTCSSX.RecordNavigator = true;
            this.dgListTTCSSX.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTTCSSX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTTCSSX.Size = new System.Drawing.Size(1120, 95);
            this.dgListTTCSSX.TabIndex = 12;
            this.dgListTTCSSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListTTCSSX.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListTTCSSX_RowDoubleClick);
            // 
            // uiGroupBox58
            // 
            this.uiGroupBox58.AutoScroll = true;
            this.uiGroupBox58.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox58.Controls.Add(this.dgListSPNLSXDT);
            this.uiGroupBox58.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox58.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox58.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox58.Location = new System.Drawing.Point(3, 124);
            this.uiGroupBox58.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox58.Name = "uiGroupBox58";
            this.uiGroupBox58.Size = new System.Drawing.Size(1126, 144);
            this.uiGroupBox58.TabIndex = 44;
            this.uiGroupBox58.Text = "3.4.1 DANH SÁCH SẢN PHẨM";
            this.uiGroupBox58.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListSPNLSXDT
            // 
            this.dgListSPNLSXDT.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListSPNLSXDT.AlternatingColors = true;
            this.dgListSPNLSXDT.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListSPNLSXDT.ColumnAutoResize = true;
            dgListSPNLSXDT_DesignTimeLayout.LayoutString = resources.GetString("dgListSPNLSXDT_DesignTimeLayout.LayoutString");
            this.dgListSPNLSXDT.DesignTimeLayout = dgListSPNLSXDT_DesignTimeLayout;
            this.dgListSPNLSXDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListSPNLSXDT.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListSPNLSXDT.FrozenColumns = 3;
            this.dgListSPNLSXDT.GroupByBoxVisible = false;
            this.dgListSPNLSXDT.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPNLSXDT.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPNLSXDT.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListSPNLSXDT.Location = new System.Drawing.Point(3, 17);
            this.dgListSPNLSXDT.Margin = new System.Windows.Forms.Padding(0);
            this.dgListSPNLSXDT.Name = "dgListSPNLSXDT";
            this.dgListSPNLSXDT.RecordNavigator = true;
            this.dgListSPNLSXDT.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListSPNLSXDT.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListSPNLSXDT.Size = new System.Drawing.Size(1120, 124);
            this.dgListSPNLSXDT.TabIndex = 15;
            this.dgListSPNLSXDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListSPNLSXDT.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListSPNLSXDT_RowDoubleClick);
            this.dgListSPNLSXDT.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListSPNLSXDT_LoadingRow);
            // 
            // uiGroupBox59
            // 
            this.uiGroupBox59.AutoScroll = true;
            this.uiGroupBox59.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox59.Controls.Add(this.btnDeleteSPNLSXDT);
            this.uiGroupBox59.Controls.Add(this.btnAddSPNLSXDT);
            this.uiGroupBox59.Controls.Add(this.ctrDVTDT);
            this.uiGroupBox59.Controls.Add(this.ctrMaHSNLSXDT);
            this.uiGroupBox59.Controls.Add(this.txtMaSPNLSXDT);
            this.uiGroupBox59.Controls.Add(this.txtThoiGianSXTGDT);
            this.uiGroupBox59.Controls.Add(this.label157);
            this.uiGroupBox59.Controls.Add(this.label164);
            this.uiGroupBox59.Controls.Add(this.label165);
            this.uiGroupBox59.Controls.Add(this.label166);
            this.uiGroupBox59.Controls.Add(this.label167);
            this.uiGroupBox59.Controls.Add(this.txtSoLuongSPDT);
            this.uiGroupBox59.Controls.Add(this.cbbThoiGianSXDVTDT);
            this.uiGroupBox59.Controls.Add(this.label168);
            this.uiGroupBox59.Controls.Add(this.label169);
            this.uiGroupBox59.Controls.Add(this.label170);
            this.uiGroupBox59.Controls.Add(this.label171);
            this.uiGroupBox59.Controls.Add(this.label172);
            this.uiGroupBox59.Controls.Add(this.label173);
            this.uiGroupBox59.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox59.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox59.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox59.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox59.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox59.Name = "uiGroupBox59";
            this.uiGroupBox59.Size = new System.Drawing.Size(1126, 116);
            this.uiGroupBox59.TabIndex = 0;
            this.uiGroupBox59.Text = "3.4 NĂNG LỰC SẢN XUẤT SẢN PHẨM";
            this.uiGroupBox59.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteSPNLSXDT
            // 
            this.btnDeleteSPNLSXDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteSPNLSXDT.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteSPNLSXDT.Image")));
            this.btnDeleteSPNLSXDT.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteSPNLSXDT.Location = new System.Drawing.Point(761, 81);
            this.btnDeleteSPNLSXDT.Name = "btnDeleteSPNLSXDT";
            this.btnDeleteSPNLSXDT.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteSPNLSXDT.TabIndex = 34;
            this.btnDeleteSPNLSXDT.Text = "Xóa";
            this.btnDeleteSPNLSXDT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteSPNLSXDT.Click += new System.EventHandler(this.btnDeleteSPNLSXDT_Click);
            // 
            // btnAddSPNLSXDT
            // 
            this.btnAddSPNLSXDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSPNLSXDT.Image = ((System.Drawing.Image)(resources.GetObject("btnAddSPNLSXDT.Image")));
            this.btnAddSPNLSXDT.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddSPNLSXDT.Location = new System.Drawing.Point(665, 81);
            this.btnAddSPNLSXDT.Name = "btnAddSPNLSXDT";
            this.btnAddSPNLSXDT.Size = new System.Drawing.Size(90, 23);
            this.btnAddSPNLSXDT.TabIndex = 33;
            this.btnAddSPNLSXDT.Text = "Ghi";
            this.btnAddSPNLSXDT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddSPNLSXDT.Click += new System.EventHandler(this.btnAddSPNLSXDT_Click);
            // 
            // ctrDVTDT
            // 
            this.ctrDVTDT.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.ctrDVTDT.Appearance.Options.UseBackColor = true;
            this.ctrDVTDT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTDT.Code = "";
            this.ctrDVTDT.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVTDT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTDT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTDT.IsOnlyWarning = false;
            this.ctrDVTDT.IsValidate = true;
            this.ctrDVTDT.Location = new System.Drawing.Point(166, 82);
            this.ctrDVTDT.Name = "ctrDVTDT";
            this.ctrDVTDT.Name_VN = "";
            this.ctrDVTDT.SetOnlyWarning = false;
            this.ctrDVTDT.SetValidate = false;
            this.ctrDVTDT.ShowColumnCode = true;
            this.ctrDVTDT.ShowColumnName = false;
            this.ctrDVTDT.Size = new System.Drawing.Size(139, 21);
            this.ctrDVTDT.TabIndex = 31;
            this.ctrDVTDT.TagName = "";
            this.ctrDVTDT.Where = null;
            this.ctrDVTDT.WhereCondition = "";
            // 
            // ctrMaHSNLSXDT
            // 
            this.ctrMaHSNLSXDT.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.ctrMaHSNLSXDT.Appearance.Options.UseBackColor = true;
            this.ctrMaHSNLSXDT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ctrMaHSNLSXDT.Code = "";
            this.ctrMaHSNLSXDT.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaHSNLSXDT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaHSNLSXDT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaHSNLSXDT.IsOnlyWarning = false;
            this.ctrMaHSNLSXDT.IsValidate = true;
            this.ctrMaHSNLSXDT.Location = new System.Drawing.Point(166, 49);
            this.ctrMaHSNLSXDT.Name = "ctrMaHSNLSXDT";
            this.ctrMaHSNLSXDT.Name_VN = "";
            this.ctrMaHSNLSXDT.SetOnlyWarning = false;
            this.ctrMaHSNLSXDT.SetValidate = false;
            this.ctrMaHSNLSXDT.ShowColumnCode = true;
            this.ctrMaHSNLSXDT.ShowColumnName = false;
            this.ctrMaHSNLSXDT.Size = new System.Drawing.Size(139, 21);
            this.ctrMaHSNLSXDT.TabIndex = 29;
            this.ctrMaHSNLSXDT.TagName = "";
            this.ctrMaHSNLSXDT.Where = null;
            this.ctrMaHSNLSXDT.WhereCondition = "";
            // 
            // txtMaSPNLSXDT
            // 
            this.txtMaSPNLSXDT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSPNLSXDT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSPNLSXDT.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaSPNLSXDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSPNLSXDT.Location = new System.Drawing.Point(169, 18);
            this.txtMaSPNLSXDT.MaxLength = 50;
            this.txtMaSPNLSXDT.Name = "txtMaSPNLSXDT";
            this.txtMaSPNLSXDT.Size = new System.Drawing.Size(122, 21);
            this.txtMaSPNLSXDT.TabIndex = 27;
            this.txtMaSPNLSXDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSPNLSXDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSPNLSXDT.ButtonClick += new System.EventHandler(this.txtMaSPNLSXDT_ButtonClick);
            // 
            // txtThoiGianSXTGDT
            // 
            this.txtThoiGianSXTGDT.DecimalDigits = 20;
            this.txtThoiGianSXTGDT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThoiGianSXTGDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThoiGianSXTGDT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtThoiGianSXTGDT.Location = new System.Drawing.Point(458, 49);
            this.txtThoiGianSXTGDT.MaxLength = 15;
            this.txtThoiGianSXTGDT.Name = "txtThoiGianSXTGDT";
            this.txtThoiGianSXTGDT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThoiGianSXTGDT.Size = new System.Drawing.Size(177, 21);
            this.txtThoiGianSXTGDT.TabIndex = 30;
            this.txtThoiGianSXTGDT.Text = "0";
            this.txtThoiGianSXTGDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThoiGianSXTGDT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtThoiGianSXTGDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(311, 53);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(156, 13);
            this.label157.TabIndex = 0;
            this.label157.Text = "Thời gian sản xuất (Thời gian) :";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label164.ForeColor = System.Drawing.Color.Red;
            this.label164.Location = new System.Drawing.Point(297, 20);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(16, 16);
            this.label164.TabIndex = 0;
            this.label164.Text = "*";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label165.ForeColor = System.Drawing.Color.Red;
            this.label165.Location = new System.Drawing.Point(637, 84);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(16, 16);
            this.label165.TabIndex = 0;
            this.label165.Text = "*";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label166.ForeColor = System.Drawing.Color.Red;
            this.label166.Location = new System.Drawing.Point(639, 20);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(16, 16);
            this.label166.TabIndex = 0;
            this.label166.Text = "*";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label167.ForeColor = System.Drawing.Color.Red;
            this.label167.Location = new System.Drawing.Point(638, 51);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(16, 16);
            this.label167.TabIndex = 0;
            this.label167.Text = "*";
            // 
            // txtSoLuongSPDT
            // 
            this.txtSoLuongSPDT.DecimalDigits = 20;
            this.txtSoLuongSPDT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongSPDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongSPDT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongSPDT.Location = new System.Drawing.Point(458, 82);
            this.txtSoLuongSPDT.MaxLength = 15;
            this.txtSoLuongSPDT.Name = "txtSoLuongSPDT";
            this.txtSoLuongSPDT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongSPDT.Size = new System.Drawing.Size(177, 21);
            this.txtSoLuongSPDT.TabIndex = 32;
            this.txtSoLuongSPDT.Text = "0";
            this.txtSoLuongSPDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongSPDT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongSPDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // cbbThoiGianSXDVTDT
            // 
            this.cbbThoiGianSXDVTDT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbThoiGianSXDVTDT.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbThoiGianSXDVTDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem49.FormatStyle.Alpha = 0;
            uiComboBoxItem49.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem49.IsSeparator = false;
            uiComboBoxItem49.Text = "Năm";
            uiComboBoxItem49.Value = "1";
            uiComboBoxItem50.FormatStyle.Alpha = 0;
            uiComboBoxItem50.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem50.IsSeparator = false;
            uiComboBoxItem50.Text = "Quý";
            uiComboBoxItem50.Value = "2";
            uiComboBoxItem51.FormatStyle.Alpha = 0;
            uiComboBoxItem51.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem51.IsSeparator = false;
            uiComboBoxItem51.Text = "Tháng";
            uiComboBoxItem51.Value = "3";
            uiComboBoxItem52.FormatStyle.Alpha = 0;
            uiComboBoxItem52.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem52.IsSeparator = false;
            uiComboBoxItem52.Text = "Tuần";
            uiComboBoxItem52.Value = "4";
            uiComboBoxItem53.FormatStyle.Alpha = 0;
            uiComboBoxItem53.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem53.IsSeparator = false;
            uiComboBoxItem53.Text = "Ngày";
            uiComboBoxItem53.Value = "5";
            this.cbbThoiGianSXDVTDT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem49,
            uiComboBoxItem50,
            uiComboBoxItem51,
            uiComboBoxItem52,
            uiComboBoxItem53});
            this.cbbThoiGianSXDVTDT.Location = new System.Drawing.Point(458, 18);
            this.cbbThoiGianSXDVTDT.Name = "cbbThoiGianSXDVTDT";
            this.cbbThoiGianSXDVTDT.Size = new System.Drawing.Size(177, 21);
            this.cbbThoiGianSXDVTDT.TabIndex = 28;
            this.cbbThoiGianSXDVTDT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label168.Location = new System.Drawing.Point(662, 11);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(444, 65);
            this.label168.TabIndex = 0;
            this.label168.Text = resources.GetString("label168.Text");
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(311, 22);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(133, 13);
            this.label169.TabIndex = 0;
            this.label169.Text = "Thời gian sản xuất (ĐVT) :";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(311, 86);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(105, 13);
            this.label170.TabIndex = 0;
            this.label170.Text = "Số lượng sản phẩm :";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(15, 53);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(44, 13);
            this.label171.TabIndex = 0;
            this.label171.Text = "Mã HS :";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(15, 22);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(77, 13);
            this.label172.TabIndex = 0;
            this.label172.Text = "Mã sản phẩm :";
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(15, 86);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(34, 13);
            this.label173.TabIndex = 0;
            this.label173.Text = "ĐVT :";
            // 
            // uiGroupBox54
            // 
            this.uiGroupBox54.AutoScroll = true;
            this.uiGroupBox54.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox54.Controls.Add(this.uiGroupBox55);
            this.uiGroupBox54.Controls.Add(this.uiGroupBox56);
            this.uiGroupBox54.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox54.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox54.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox54.Location = new System.Drawing.Point(3, 212);
            this.uiGroupBox54.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox54.Name = "uiGroupBox54";
            this.uiGroupBox54.Size = new System.Drawing.Size(1132, 326);
            this.uiGroupBox54.TabIndex = 44;
            this.uiGroupBox54.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox55
            // 
            this.uiGroupBox55.AutoScroll = true;
            this.uiGroupBox55.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox55.Controls.Add(this.dgListSPCKSXDT);
            this.uiGroupBox55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox55.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox55.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox55.Location = new System.Drawing.Point(3, 128);
            this.uiGroupBox55.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox55.Name = "uiGroupBox55";
            this.uiGroupBox55.Size = new System.Drawing.Size(1126, 195);
            this.uiGroupBox55.TabIndex = 44;
            this.uiGroupBox55.Text = "3.3.1 DANH SÁCH SẢN PHẨM";
            this.uiGroupBox55.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListSPCKSXDT
            // 
            this.dgListSPCKSXDT.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListSPCKSXDT.AlternatingColors = true;
            this.dgListSPCKSXDT.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListSPCKSXDT.ColumnAutoResize = true;
            dgListSPCKSXDT_DesignTimeLayout.LayoutString = resources.GetString("dgListSPCKSXDT_DesignTimeLayout.LayoutString");
            this.dgListSPCKSXDT.DesignTimeLayout = dgListSPCKSXDT_DesignTimeLayout;
            this.dgListSPCKSXDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListSPCKSXDT.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListSPCKSXDT.FrozenColumns = 3;
            this.dgListSPCKSXDT.GroupByBoxVisible = false;
            this.dgListSPCKSXDT.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPCKSXDT.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPCKSXDT.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListSPCKSXDT.Location = new System.Drawing.Point(3, 17);
            this.dgListSPCKSXDT.Margin = new System.Windows.Forms.Padding(0);
            this.dgListSPCKSXDT.Name = "dgListSPCKSXDT";
            this.dgListSPCKSXDT.RecordNavigator = true;
            this.dgListSPCKSXDT.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListSPCKSXDT.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListSPCKSXDT.Size = new System.Drawing.Size(1120, 175);
            this.dgListSPCKSXDT.TabIndex = 15;
            this.dgListSPCKSXDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListSPCKSXDT.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListSPCKSXDT_RowDoubleClick);
            this.dgListSPCKSXDT.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListSPCKSXDT_LoadingRow);
            // 
            // uiGroupBox56
            // 
            this.uiGroupBox56.AutoScroll = true;
            this.uiGroupBox56.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox56.Controls.Add(this.btnDeleteSPCKSXDT);
            this.uiGroupBox56.Controls.Add(this.btnAddSPCKSXDT);
            this.uiGroupBox56.Controls.Add(this.ctrMaHSCKSXDT);
            this.uiGroupBox56.Controls.Add(this.txtMaSPCKSXDT);
            this.uiGroupBox56.Controls.Add(this.txtChuKySXTGDT);
            this.uiGroupBox56.Controls.Add(this.label57);
            this.uiGroupBox56.Controls.Add(this.label58);
            this.uiGroupBox56.Controls.Add(this.label63);
            this.uiGroupBox56.Controls.Add(this.label64);
            this.uiGroupBox56.Controls.Add(this.cbbChuKySXDVTDT);
            this.uiGroupBox56.Controls.Add(this.label124);
            this.uiGroupBox56.Controls.Add(this.label125);
            this.uiGroupBox56.Controls.Add(this.label145);
            this.uiGroupBox56.Controls.Add(this.label146);
            this.uiGroupBox56.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox56.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox56.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox56.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox56.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox56.Name = "uiGroupBox56";
            this.uiGroupBox56.Size = new System.Drawing.Size(1126, 120);
            this.uiGroupBox56.TabIndex = 42;
            this.uiGroupBox56.Text = "3.3 CHU KỲ SẢN XUẤT SẢN PHẨM";
            this.uiGroupBox56.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteSPCKSXDT
            // 
            this.btnDeleteSPCKSXDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteSPCKSXDT.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteSPCKSXDT.Image")));
            this.btnDeleteSPCKSXDT.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteSPCKSXDT.Location = new System.Drawing.Point(761, 70);
            this.btnDeleteSPCKSXDT.Name = "btnDeleteSPCKSXDT";
            this.btnDeleteSPCKSXDT.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteSPCKSXDT.TabIndex = 26;
            this.btnDeleteSPCKSXDT.Text = "Xóa";
            this.btnDeleteSPCKSXDT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteSPCKSXDT.Click += new System.EventHandler(this.btnDeleteSPCKSXDT_Click);
            // 
            // btnAddSPCKSXDT
            // 
            this.btnAddSPCKSXDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSPCKSXDT.Image = ((System.Drawing.Image)(resources.GetObject("btnAddSPCKSXDT.Image")));
            this.btnAddSPCKSXDT.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddSPCKSXDT.Location = new System.Drawing.Point(665, 70);
            this.btnAddSPCKSXDT.Name = "btnAddSPCKSXDT";
            this.btnAddSPCKSXDT.Size = new System.Drawing.Size(90, 23);
            this.btnAddSPCKSXDT.TabIndex = 25;
            this.btnAddSPCKSXDT.Text = "Ghi";
            this.btnAddSPCKSXDT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddSPCKSXDT.Click += new System.EventHandler(this.btnAddSPCKSXDT_Click);
            // 
            // ctrMaHSCKSXDT
            // 
            this.ctrMaHSCKSXDT.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.ctrMaHSCKSXDT.Appearance.Options.UseBackColor = true;
            this.ctrMaHSCKSXDT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ctrMaHSCKSXDT.Code = "";
            this.ctrMaHSCKSXDT.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaHSCKSXDT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaHSCKSXDT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaHSCKSXDT.IsOnlyWarning = false;
            this.ctrMaHSCKSXDT.IsValidate = true;
            this.ctrMaHSCKSXDT.Location = new System.Drawing.Point(166, 71);
            this.ctrMaHSCKSXDT.Name = "ctrMaHSCKSXDT";
            this.ctrMaHSCKSXDT.Name_VN = "";
            this.ctrMaHSCKSXDT.SetOnlyWarning = false;
            this.ctrMaHSCKSXDT.SetValidate = false;
            this.ctrMaHSCKSXDT.ShowColumnCode = true;
            this.ctrMaHSCKSXDT.ShowColumnName = false;
            this.ctrMaHSCKSXDT.Size = new System.Drawing.Size(139, 21);
            this.ctrMaHSCKSXDT.TabIndex = 23;
            this.ctrMaHSCKSXDT.TagName = "";
            this.ctrMaHSCKSXDT.Where = null;
            this.ctrMaHSCKSXDT.WhereCondition = "";
            // 
            // txtMaSPCKSXDT
            // 
            this.txtMaSPCKSXDT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSPCKSXDT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSPCKSXDT.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaSPCKSXDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSPCKSXDT.Location = new System.Drawing.Point(166, 32);
            this.txtMaSPCKSXDT.MaxLength = 50;
            this.txtMaSPCKSXDT.Name = "txtMaSPCKSXDT";
            this.txtMaSPCKSXDT.Size = new System.Drawing.Size(123, 21);
            this.txtMaSPCKSXDT.TabIndex = 21;
            this.txtMaSPCKSXDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSPCKSXDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSPCKSXDT.ButtonClick += new System.EventHandler(this.txtMaSPCKSXDT_ButtonClick);
            // 
            // txtChuKySXTGDT
            // 
            this.txtChuKySXTGDT.DecimalDigits = 20;
            this.txtChuKySXTGDT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtChuKySXTGDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChuKySXTGDT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtChuKySXTGDT.Location = new System.Drawing.Point(458, 71);
            this.txtChuKySXTGDT.MaxLength = 15;
            this.txtChuKySXTGDT.Name = "txtChuKySXTGDT";
            this.txtChuKySXTGDT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtChuKySXTGDT.Size = new System.Drawing.Size(177, 21);
            this.txtChuKySXTGDT.TabIndex = 24;
            this.txtChuKySXTGDT.Text = "0";
            this.txtChuKySXTGDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChuKySXTGDT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtChuKySXTGDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(311, 75);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(146, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "Chu kỳ sản xuất (Thời gian) :";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Red;
            this.label58.Location = new System.Drawing.Point(638, 73);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(16, 16);
            this.label58.TabIndex = 0;
            this.label58.Text = "*";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Red;
            this.label63.Location = new System.Drawing.Point(638, 34);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(16, 16);
            this.label63.TabIndex = 0;
            this.label63.Text = "*";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Red;
            this.label64.Location = new System.Drawing.Point(292, 34);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(16, 16);
            this.label64.TabIndex = 0;
            this.label64.Text = "*";
            // 
            // cbbChuKySXDVTDT
            // 
            this.cbbChuKySXDVTDT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbChuKySXDVTDT.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbChuKySXDVTDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem54.FormatStyle.Alpha = 0;
            uiComboBoxItem54.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem54.IsSeparator = false;
            uiComboBoxItem54.Text = "Năm";
            uiComboBoxItem54.Value = "1";
            uiComboBoxItem55.FormatStyle.Alpha = 0;
            uiComboBoxItem55.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem55.IsSeparator = false;
            uiComboBoxItem55.Text = "Quý";
            uiComboBoxItem55.Value = "2";
            uiComboBoxItem56.FormatStyle.Alpha = 0;
            uiComboBoxItem56.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem56.IsSeparator = false;
            uiComboBoxItem56.Text = "Tháng";
            uiComboBoxItem56.Value = "3";
            uiComboBoxItem57.FormatStyle.Alpha = 0;
            uiComboBoxItem57.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem57.IsSeparator = false;
            uiComboBoxItem57.Text = "Tuần";
            uiComboBoxItem57.Value = "4";
            uiComboBoxItem58.FormatStyle.Alpha = 0;
            uiComboBoxItem58.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem58.IsSeparator = false;
            uiComboBoxItem58.Text = "Ngày";
            uiComboBoxItem58.Value = "5";
            this.cbbChuKySXDVTDT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem54,
            uiComboBoxItem55,
            uiComboBoxItem56,
            uiComboBoxItem57,
            uiComboBoxItem58});
            this.cbbChuKySXDVTDT.Location = new System.Drawing.Point(458, 32);
            this.cbbChuKySXDVTDT.Name = "cbbChuKySXDVTDT";
            this.cbbChuKySXDVTDT.Size = new System.Drawing.Size(177, 21);
            this.cbbChuKySXDVTDT.TabIndex = 22;
            this.cbbChuKySXDVTDT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(311, 36);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(123, 13);
            this.label124.TabIndex = 0;
            this.label124.Text = "Chu kỳ sản xuất (ĐVT) :";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(15, 75);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(44, 13);
            this.label125.TabIndex = 0;
            this.label125.Text = "Mã HS :";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label145.Location = new System.Drawing.Point(663, 12);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(410, 52);
            this.label145.TabIndex = 0;
            this.label145.Text = "Hướng dẫn nhập liệu :\r\nNếu doanh nghiệp có Chu kỳ sản xuất sản phẩm A trong 30 ng" +
                "ày thì nhập như sau :\r\nÔ Chu kỳ sản xuất (ĐVT) chọn là : Ngày\r\nÔ Chu kỳ sản xuất" +
                " (Thời gian) nhập vào là : 30\r\n";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(15, 36);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(77, 13);
            this.label146.TabIndex = 0;
            this.label146.Text = "Mã sản phẩm :";
            // 
            // uiGroupBox53
            // 
            this.uiGroupBox53.AutoScroll = true;
            this.uiGroupBox53.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox53.Controls.Add(this.label94);
            this.uiGroupBox53.Controls.Add(this.label156);
            this.uiGroupBox53.Controls.Add(this.label158);
            this.uiGroupBox53.Controls.Add(this.txtNangLucSXDT);
            this.uiGroupBox53.Controls.Add(this.txtTongSoLuongDT);
            this.uiGroupBox53.Controls.Add(this.txtSoLuongDiThueDT);
            this.uiGroupBox53.Controls.Add(this.txtSoLuongKhacDT);
            this.uiGroupBox53.Controls.Add(this.label144);
            this.uiGroupBox53.Controls.Add(this.label147);
            this.uiGroupBox53.Controls.Add(this.label148);
            this.uiGroupBox53.Controls.Add(this.label149);
            this.uiGroupBox53.Controls.Add(this.txtSoLuongSoHuuDT);
            this.uiGroupBox53.Controls.Add(this.label150);
            this.uiGroupBox53.Controls.Add(this.label152);
            this.uiGroupBox53.Controls.Add(this.label153);
            this.uiGroupBox53.Controls.Add(this.label154);
            this.uiGroupBox53.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox53.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox53.Location = new System.Drawing.Point(3, 87);
            this.uiGroupBox53.Name = "uiGroupBox53";
            this.uiGroupBox53.Size = new System.Drawing.Size(1132, 125);
            this.uiGroupBox53.TabIndex = 39;
            this.uiGroupBox53.Text = "3.2 SỐ LƯỢNG MÁY MÓC DÂY TRUYỀN TRANG THIẾT BỊ";
            this.uiGroupBox53.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Red;
            this.label94.Location = new System.Drawing.Point(172, 105);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(400, 13);
            this.label94.TabIndex = 0;
            this.label94.Text = "Nêu rõ Năng lực sản xuất sản phẩm tối đa trong một năm/tháng/ngày";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.ForeColor = System.Drawing.Color.Red;
            this.label156.Location = new System.Drawing.Point(647, 80);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(16, 16);
            this.label156.TabIndex = 0;
            this.label156.Text = "*";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(17, 82);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(101, 13);
            this.label158.TabIndex = 0;
            this.label158.Text = "Năng lực sản xuất :";
            // 
            // txtNangLucSXDT
            // 
            this.txtNangLucSXDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNangLucSXDT.Location = new System.Drawing.Point(172, 78);
            this.txtNangLucSXDT.Name = "txtNangLucSXDT";
            this.txtNangLucSXDT.Size = new System.Drawing.Size(469, 21);
            this.txtNangLucSXDT.TabIndex = 20;
            this.txtNangLucSXDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongSoLuongDT
            // 
            this.txtTongSoLuongDT.DecimalDigits = 20;
            this.txtTongSoLuongDT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongSoLuongDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoLuongDT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongSoLuongDT.Location = new System.Drawing.Point(464, 50);
            this.txtTongSoLuongDT.MaxLength = 15;
            this.txtTongSoLuongDT.Name = "txtTongSoLuongDT";
            this.txtTongSoLuongDT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoLuongDT.Size = new System.Drawing.Size(177, 21);
            this.txtTongSoLuongDT.TabIndex = 19;
            this.txtTongSoLuongDT.Text = "0";
            this.txtTongSoLuongDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoLuongDT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoLuongDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongSoLuongDT.Leave += new System.EventHandler(this.txtTongSoLuongDT_Leave);
            // 
            // txtSoLuongDiThueDT
            // 
            this.txtSoLuongDiThueDT.DecimalDigits = 20;
            this.txtSoLuongDiThueDT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongDiThueDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongDiThueDT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongDiThueDT.Location = new System.Drawing.Point(464, 19);
            this.txtSoLuongDiThueDT.MaxLength = 15;
            this.txtSoLuongDiThueDT.Name = "txtSoLuongDiThueDT";
            this.txtSoLuongDiThueDT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongDiThueDT.Size = new System.Drawing.Size(177, 21);
            this.txtSoLuongDiThueDT.TabIndex = 17;
            this.txtSoLuongDiThueDT.Text = "0";
            this.txtSoLuongDiThueDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongDiThueDT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongDiThueDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongDiThueDT.Leave += new System.EventHandler(this.txtSoLuongDiThueDT_Leave);
            // 
            // txtSoLuongKhacDT
            // 
            this.txtSoLuongKhacDT.DecimalDigits = 20;
            this.txtSoLuongKhacDT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongKhacDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongKhacDT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongKhacDT.Location = new System.Drawing.Point(172, 50);
            this.txtSoLuongKhacDT.MaxLength = 15;
            this.txtSoLuongKhacDT.Name = "txtSoLuongKhacDT";
            this.txtSoLuongKhacDT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongKhacDT.Size = new System.Drawing.Size(127, 21);
            this.txtSoLuongKhacDT.TabIndex = 18;
            this.txtSoLuongKhacDT.Text = "0";
            this.txtSoLuongKhacDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongKhacDT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongKhacDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongKhacDT.Leave += new System.EventHandler(this.txtSoLuongKhacDT_Leave);
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.ForeColor = System.Drawing.Color.Red;
            this.label144.Location = new System.Drawing.Point(306, 52);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(16, 16);
            this.label144.TabIndex = 34;
            this.label144.Text = "*";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.ForeColor = System.Drawing.Color.Red;
            this.label147.Location = new System.Drawing.Point(647, 52);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(16, 16);
            this.label147.TabIndex = 0;
            this.label147.Text = "*";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.ForeColor = System.Drawing.Color.Red;
            this.label148.Location = new System.Drawing.Point(647, 21);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(16, 16);
            this.label148.TabIndex = 0;
            this.label148.Text = "*";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.ForeColor = System.Drawing.Color.Red;
            this.label149.Location = new System.Drawing.Point(306, 21);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(16, 16);
            this.label149.TabIndex = 0;
            this.label149.Text = "*";
            // 
            // txtSoLuongSoHuuDT
            // 
            this.txtSoLuongSoHuuDT.DecimalDigits = 20;
            this.txtSoLuongSoHuuDT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongSoHuuDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongSoHuuDT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongSoHuuDT.Location = new System.Drawing.Point(172, 19);
            this.txtSoLuongSoHuuDT.MaxLength = 15;
            this.txtSoLuongSoHuuDT.Name = "txtSoLuongSoHuuDT";
            this.txtSoLuongSoHuuDT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongSoHuuDT.Size = new System.Drawing.Size(127, 21);
            this.txtSoLuongSoHuuDT.TabIndex = 16;
            this.txtSoLuongSoHuuDT.Text = "0";
            this.txtSoLuongSoHuuDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongSoHuuDT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongSoHuuDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongSoHuuDT.Leave += new System.EventHandler(this.txtSoLuongSoHuuDT_Leave);
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(344, 23);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(92, 13);
            this.label150.TabIndex = 0;
            this.label150.Text = "Số lượng đi thuê :";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(17, 23);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(92, 13);
            this.label152.TabIndex = 0;
            this.label152.Text = "Số lượng sở hữu :";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(344, 54);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(82, 13);
            this.label153.TabIndex = 0;
            this.label153.Text = "Tổng số lượng :";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(17, 54);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(81, 13);
            this.label154.TabIndex = 0;
            this.label154.Text = "Số lượng khác :";
            // 
            // uiGroupBox52
            // 
            this.uiGroupBox52.AutoScroll = true;
            this.uiGroupBox52.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox52.Controls.Add(this.btnDeleteTTCSSX);
            this.uiGroupBox52.Controls.Add(this.btnAddTTCSSX);
            this.uiGroupBox52.Controls.Add(this.txtSoLuongCNDT);
            this.uiGroupBox52.Controls.Add(this.txtDienTichNXDT);
            this.uiGroupBox52.Controls.Add(this.label132);
            this.uiGroupBox52.Controls.Add(this.txtDiaChiCSSXDT);
            this.uiGroupBox52.Controls.Add(this.label142);
            this.uiGroupBox52.Controls.Add(this.label138);
            this.uiGroupBox52.Controls.Add(this.label133);
            this.uiGroupBox52.Controls.Add(this.label36);
            this.uiGroupBox52.Controls.Add(this.label143);
            this.uiGroupBox52.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox52.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox52.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox52.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox52.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox52.Name = "uiGroupBox52";
            this.uiGroupBox52.Size = new System.Drawing.Size(1132, 70);
            this.uiGroupBox52.TabIndex = 38;
            this.uiGroupBox52.Tag = "";
            this.uiGroupBox52.Text = "3.1 Thông tin chung";
            this.uiGroupBox52.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteTTCSSX
            // 
            this.btnDeleteTTCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteTTCSSX.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteTTCSSX.Image")));
            this.btnDeleteTTCSSX.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteTTCSSX.Location = new System.Drawing.Point(787, 41);
            this.btnDeleteTTCSSX.Name = "btnDeleteTTCSSX";
            this.btnDeleteTTCSSX.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteTTCSSX.TabIndex = 15;
            this.btnDeleteTTCSSX.Text = "Xóa";
            this.btnDeleteTTCSSX.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteTTCSSX.Click += new System.EventHandler(this.btnDeleteTTCSSX_Click);
            // 
            // btnAddTTCSSX
            // 
            this.btnAddTTCSSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddTTCSSX.Image = ((System.Drawing.Image)(resources.GetObject("btnAddTTCSSX.Image")));
            this.btnAddTTCSSX.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddTTCSSX.Location = new System.Drawing.Point(691, 41);
            this.btnAddTTCSSX.Name = "btnAddTTCSSX";
            this.btnAddTTCSSX.Size = new System.Drawing.Size(90, 23);
            this.btnAddTTCSSX.TabIndex = 14;
            this.btnAddTTCSSX.Text = "Ghi";
            this.btnAddTTCSSX.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddTTCSSX.Click += new System.EventHandler(this.btnAddTTCSSX_Click);
            // 
            // txtSoLuongCNDT
            // 
            this.txtSoLuongCNDT.DecimalDigits = 20;
            this.txtSoLuongCNDT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongCNDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongCNDT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongCNDT.Location = new System.Drawing.Point(536, 42);
            this.txtSoLuongCNDT.MaxLength = 15;
            this.txtSoLuongCNDT.Name = "txtSoLuongCNDT";
            this.txtSoLuongCNDT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongCNDT.Size = new System.Drawing.Size(127, 21);
            this.txtSoLuongCNDT.TabIndex = 13;
            this.txtSoLuongCNDT.Text = "0";
            this.txtSoLuongCNDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongCNDT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongCNDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDienTichNXDT
            // 
            this.txtDienTichNXDT.DecimalDigits = 20;
            this.txtDienTichNXDT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDienTichNXDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienTichNXDT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDienTichNXDT.Location = new System.Drawing.Point(172, 42);
            this.txtDienTichNXDT.MaxLength = 15;
            this.txtDienTichNXDT.Name = "txtDienTichNXDT";
            this.txtDienTichNXDT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDienTichNXDT.Size = new System.Drawing.Size(127, 21);
            this.txtDienTichNXDT.TabIndex = 12;
            this.txtDienTichNXDT.Text = "0";
            this.txtDienTichNXDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDienTichNXDT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDienTichNXDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.ForeColor = System.Drawing.Color.Red;
            this.label132.Location = new System.Drawing.Point(669, 44);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(16, 16);
            this.label132.TabIndex = 0;
            this.label132.Text = "*";
            // 
            // txtDiaChiCSSXDT
            // 
            this.txtDiaChiCSSXDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiCSSXDT.Location = new System.Drawing.Point(173, 15);
            this.txtDiaChiCSSXDT.Name = "txtDiaChiCSSXDT";
            this.txtDiaChiCSSXDT.Size = new System.Drawing.Size(490, 21);
            this.txtDiaChiCSSXDT.TabIndex = 11;
            this.txtDiaChiCSSXDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(319, 46);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(76, 13);
            this.label142.TabIndex = 0;
            this.label142.Text = "Đơn vị tính M2";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(407, 46);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(109, 13);
            this.label138.TabIndex = 0;
            this.label138.Text = "Số lượng công nhân :";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.ForeColor = System.Drawing.Color.Red;
            this.label133.Location = new System.Drawing.Point(301, 44);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(16, 16);
            this.label133.TabIndex = 0;
            this.label133.Text = "*";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(8, 19);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(67, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "Địa chỉ CSSX";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(8, 46);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(110, 13);
            this.label143.TabIndex = 0;
            this.label143.Text = "Diện tích nhà xưởng :";
            // 
            // uiGroupBox49
            // 
            this.uiGroupBox49.AutoScroll = true;
            this.uiGroupBox49.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox49.Controls.Add(this.grListHD);
            this.uiGroupBox49.Controls.Add(this.uiGroupBox50);
            this.uiGroupBox49.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox49.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox49.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox49.Location = new System.Drawing.Point(3, 112);
            this.uiGroupBox49.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox49.Name = "uiGroupBox49";
            this.uiGroupBox49.Size = new System.Drawing.Size(1138, 199);
            this.uiGroupBox49.TabIndex = 0;
            this.uiGroupBox49.Text = "2. Hợp đồng";
            this.uiGroupBox49.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListHD
            // 
            this.grListHD.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListHD.AlternatingColors = true;
            this.grListHD.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.grListHD.ColumnAutoResize = true;
            grListHD_DesignTimeLayout.LayoutString = resources.GetString("grListHD_DesignTimeLayout.LayoutString");
            this.grListHD.DesignTimeLayout = grListHD_DesignTimeLayout;
            this.grListHD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListHD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListHD.FrozenColumns = 3;
            this.grListHD.GroupByBoxVisible = false;
            this.grListHD.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHD.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHD.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListHD.Location = new System.Drawing.Point(3, 68);
            this.grListHD.Margin = new System.Windows.Forms.Padding(0);
            this.grListHD.Name = "grListHD";
            this.grListHD.RecordNavigator = true;
            this.grListHD.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListHD.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHD.Size = new System.Drawing.Size(1132, 128);
            this.grListHD.TabIndex = 39;
            this.grListHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListHD.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListHD_RowDoubleClick);
            this.grListHD.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListHD_FormattingRow);
            // 
            // uiGroupBox50
            // 
            this.uiGroupBox50.AutoScroll = true;
            this.uiGroupBox50.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox50.Controls.Add(this.btnDeleteHD);
            this.uiGroupBox50.Controls.Add(this.btnAddHD);
            this.uiGroupBox50.Controls.Add(this.clcNgayHH);
            this.uiGroupBox50.Controls.Add(this.clcNgayHD);
            this.uiGroupBox50.Controls.Add(this.label177);
            this.uiGroupBox50.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox50.Controls.Add(this.label178);
            this.uiGroupBox50.Controls.Add(this.label179);
            this.uiGroupBox50.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox50.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox50.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox50.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox50.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox50.Name = "uiGroupBox50";
            this.uiGroupBox50.Size = new System.Drawing.Size(1132, 51);
            this.uiGroupBox50.TabIndex = 38;
            this.uiGroupBox50.Text = "2.1 Hợp đồng";
            this.uiGroupBox50.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteHD
            // 
            this.btnDeleteHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteHD.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteHD.Image")));
            this.btnDeleteHD.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteHD.Location = new System.Drawing.Point(948, 15);
            this.btnDeleteHD.Name = "btnDeleteHD";
            this.btnDeleteHD.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteHD.TabIndex = 10;
            this.btnDeleteHD.Text = "Xóa";
            this.btnDeleteHD.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteHD.Click += new System.EventHandler(this.btnDeleteHD_Click);
            // 
            // btnAddHD
            // 
            this.btnAddHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddHD.Image = ((System.Drawing.Image)(resources.GetObject("btnAddHD.Image")));
            this.btnAddHD.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddHD.Location = new System.Drawing.Point(852, 15);
            this.btnAddHD.Name = "btnAddHD";
            this.btnAddHD.Size = new System.Drawing.Size(90, 23);
            this.btnAddHD.TabIndex = 9;
            this.btnAddHD.Text = "Ghi";
            this.btnAddHD.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddHD.Click += new System.EventHandler(this.btnAddHD_Click);
            // 
            // clcNgayHH
            // 
            this.clcNgayHH.CustomFormat = "dd/MM/yyyy";
            this.clcNgayHH.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayHH.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayHH.DropDownCalendar.Name = "";
            this.clcNgayHH.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayHH.Location = new System.Drawing.Point(704, 16);
            this.clcNgayHH.Name = "clcNgayHH";
            this.clcNgayHH.Nullable = true;
            this.clcNgayHH.NullButtonText = "Xóa";
            this.clcNgayHH.ShowNullButton = true;
            this.clcNgayHH.Size = new System.Drawing.Size(133, 21);
            this.clcNgayHH.TabIndex = 8;
            this.clcNgayHH.TodayButtonText = "Hôm nay";
            this.clcNgayHH.Value = new System.DateTime(2018, 7, 27, 0, 0, 0, 0);
            this.clcNgayHH.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcNgayHD
            // 
            this.clcNgayHD.CustomFormat = "dd/MM/yyyy";
            this.clcNgayHD.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayHD.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayHD.DropDownCalendar.Name = "";
            this.clcNgayHD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayHD.Location = new System.Drawing.Point(399, 16);
            this.clcNgayHD.Name = "clcNgayHD";
            this.clcNgayHD.Nullable = true;
            this.clcNgayHD.NullButtonText = "Xóa";
            this.clcNgayHD.ShowNullButton = true;
            this.clcNgayHD.Size = new System.Drawing.Size(131, 21);
            this.clcNgayHD.TabIndex = 7;
            this.clcNgayHD.TodayButtonText = "Hôm nay";
            this.clcNgayHD.Value = new System.DateTime(2018, 7, 27, 0, 0, 0, 0);
            this.clcNgayHD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(306, 20);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(87, 13);
            this.label177.TabIndex = 0;
            this.label177.Text = "Ngày hợp đồng :";
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(141, 16);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(149, 21);
            this.txtSoHopDong.TabIndex = 6;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(18, 20);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(74, 13);
            this.label178.TabIndex = 0;
            this.label178.Text = "Số hợp đồng :";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(565, 20);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(127, 13);
            this.label179.TabIndex = 0;
            this.label179.Text = "Ngày hết hạn hợp đồng :";
            // 
            // uiGroupBox48
            // 
            this.uiGroupBox48.AutoScroll = true;
            this.uiGroupBox48.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox48.Controls.Add(this.btnDeleteCSSXDT);
            this.uiGroupBox48.Controls.Add(this.btnAddCSSXDT);
            this.uiGroupBox48.Controls.Add(this.txtTenDoanhNghiepDT);
            this.uiGroupBox48.Controls.Add(this.txtDiaChiDoanhNghiepDT);
            this.uiGroupBox48.Controls.Add(this.txtMaDoanhNghiepDT);
            this.uiGroupBox48.Controls.Add(this.label174);
            this.uiGroupBox48.Controls.Add(this.label175);
            this.uiGroupBox48.Controls.Add(this.label176);
            this.uiGroupBox48.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox48.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox48.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox48.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox48.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox48.Name = "uiGroupBox48";
            this.uiGroupBox48.Size = new System.Drawing.Size(1138, 95);
            this.uiGroupBox48.TabIndex = 0;
            this.uiGroupBox48.Text = "1. Thông tin chung";
            this.uiGroupBox48.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox48.Click += new System.EventHandler(this.uiGroupBox48_Click);
            // 
            // btnDeleteCSSXDT
            // 
            this.btnDeleteCSSXDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteCSSXDT.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteCSSXDT.Image")));
            this.btnDeleteCSSXDT.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteCSSXDT.Location = new System.Drawing.Point(951, 65);
            this.btnDeleteCSSXDT.Name = "btnDeleteCSSXDT";
            this.btnDeleteCSSXDT.Size = new System.Drawing.Size(90, 23);
            this.btnDeleteCSSXDT.TabIndex = 5;
            this.btnDeleteCSSXDT.Text = "Xóa";
            this.btnDeleteCSSXDT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteCSSXDT.Click += new System.EventHandler(this.btnDeleteCSSXDT_Click);
            // 
            // btnAddCSSXDT
            // 
            this.btnAddCSSXDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCSSXDT.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCSSXDT.Image")));
            this.btnAddCSSXDT.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddCSSXDT.Location = new System.Drawing.Point(855, 65);
            this.btnAddCSSXDT.Name = "btnAddCSSXDT";
            this.btnAddCSSXDT.Size = new System.Drawing.Size(90, 23);
            this.btnAddCSSXDT.TabIndex = 4;
            this.btnAddCSSXDT.Text = "Ghi";
            this.btnAddCSSXDT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddCSSXDT.Click += new System.EventHandler(this.btnAddCSSXDT_Click);
            // 
            // txtTenDoanhNghiepDT
            // 
            this.txtTenDoanhNghiepDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoanhNghiepDT.Location = new System.Drawing.Point(144, 38);
            this.txtTenDoanhNghiepDT.Name = "txtTenDoanhNghiepDT";
            this.txtTenDoanhNghiepDT.Size = new System.Drawing.Size(662, 21);
            this.txtTenDoanhNghiepDT.TabIndex = 2;
            this.txtTenDoanhNghiepDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiDoanhNghiepDT
            // 
            this.txtDiaChiDoanhNghiepDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoanhNghiepDT.Location = new System.Drawing.Point(144, 66);
            this.txtDiaChiDoanhNghiepDT.Name = "txtDiaChiDoanhNghiepDT";
            this.txtDiaChiDoanhNghiepDT.Size = new System.Drawing.Size(662, 21);
            this.txtDiaChiDoanhNghiepDT.TabIndex = 3;
            this.txtDiaChiDoanhNghiepDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDoanhNghiepDT
            // 
            this.txtMaDoanhNghiepDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoanhNghiepDT.Location = new System.Drawing.Point(144, 11);
            this.txtMaDoanhNghiepDT.Name = "txtMaDoanhNghiepDT";
            this.txtMaDoanhNghiepDT.Size = new System.Drawing.Size(149, 21);
            this.txtMaDoanhNghiepDT.TabIndex = 1;
            this.txtMaDoanhNghiepDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(21, 70);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(46, 13);
            this.label174.TabIndex = 0;
            this.label174.Text = "Địa chỉ :";
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(21, 15);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(96, 13);
            this.label175.TabIndex = 0;
            this.label175.Text = "Mã doanh nghiệp :";
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(21, 42);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(100, 13);
            this.label176.TabIndex = 0;
            this.label176.Text = "Tên doanh nghiệp :";
            // 
            // ManufactureFactories
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1386, 958);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ManufactureFactories";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông báo cơ sở sản xuất , nơi lưu giữ NL ,MMTB,VT và SP xuất khẩu";
            this.Load += new System.EventHandler(this.ManufactureFactories_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox19)).EndInit();
            this.uiGroupBox19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox20)).EndInit();
            this.uiGroupBox20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListThanhVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox28)).EndInit();
            this.uiGroupBox28.ResumeLayout(false);
            this.uiGroupBox28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox21)).EndInit();
            this.uiGroupBox21.ResumeLayout(false);
            this.uiGroupBox21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).EndInit();
            this.uiGroupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).EndInit();
            this.uiGroupBox17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListCongTyMe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox27)).EndInit();
            this.uiGroupBox27.ResumeLayout(false);
            this.uiGroupBox27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).EndInit();
            this.uiGroupBox18.ResumeLayout(false);
            this.uiGroupBox18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox45)).EndInit();
            this.uiGroupBox45.ResumeLayout(false);
            this.uiGroupBox45.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox26)).EndInit();
            this.uiGroupBox26.ResumeLayout(false);
            this.uiGroupBox26.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).EndInit();
            this.uiGroupBox14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNganhHangSXChinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox15)).EndInit();
            this.uiGroupBox15.ResumeLayout(false);
            this.uiGroupBox15.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox24)).EndInit();
            this.uiGroupBox24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListCSSX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).EndInit();
            this.uiGroupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNganhNghe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox42)).EndInit();
            this.uiGroupBox42.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox43)).EndInit();
            this.uiGroupBox43.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPNLSX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox44)).EndInit();
            this.uiGroupBox44.ResumeLayout(false);
            this.uiGroupBox44.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox39)).EndInit();
            this.uiGroupBox39.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox41)).EndInit();
            this.uiGroupBox41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPCKSX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox38)).EndInit();
            this.uiGroupBox38.ResumeLayout(false);
            this.uiGroupBox38.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox40)).EndInit();
            this.uiGroupBox40.ResumeLayout(false);
            this.uiGroupBox40.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox35)).EndInit();
            this.uiGroupBox35.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox37)).EndInit();
            this.uiGroupBox37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDDLGHH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox36)).EndInit();
            this.uiGroupBox36.ResumeLayout(false);
            this.uiGroupBox36.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox22)).EndInit();
            this.uiGroupBox22.ResumeLayout(false);
            this.uiGroupBox22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox25)).EndInit();
            this.uiGroupBox25.ResumeLayout(false);
            this.uiGroupBox25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox29)).EndInit();
            this.uiGroupBox29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListAttachFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox60)).EndInit();
            this.uiGroupBox60.ResumeLayout(false);
            this.uiGroupBox60.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox23)).EndInit();
            this.uiGroupBox23.ResumeLayout(false);
            this.uiGroupBox23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox34)).EndInit();
            this.uiGroupBox34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListKiemTra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox33)).EndInit();
            this.uiGroupBox33.ResumeLayout(false);
            this.uiGroupBox33.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox30)).EndInit();
            this.uiGroupBox30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListCTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox31)).EndInit();
            this.uiGroupBox31.ResumeLayout(false);
            this.uiGroupBox31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox32)).EndInit();
            this.uiGroupBox32.ResumeLayout(false);
            this.uiGroupBox32.PerformLayout();
            this.uiTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox46)).EndInit();
            this.uiGroupBox46.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox47)).EndInit();
            this.uiGroupBox47.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDoiTac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox51)).EndInit();
            this.uiGroupBox51.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox57)).EndInit();
            this.uiGroupBox57.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox61)).EndInit();
            this.uiGroupBox61.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTTCSSX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox58)).EndInit();
            this.uiGroupBox58.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPNLSXDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox59)).EndInit();
            this.uiGroupBox59.ResumeLayout(false);
            this.uiGroupBox59.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox54)).EndInit();
            this.uiGroupBox54.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox55)).EndInit();
            this.uiGroupBox55.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPCKSXDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox56)).EndInit();
            this.uiGroupBox56.ResumeLayout(false);
            this.uiGroupBox56.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox53)).EndInit();
            this.uiGroupBox53.ResumeLayout(false);
            this.uiGroupBox53.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox52)).EndInit();
            this.uiGroupBox52.ResumeLayout(false);
            this.uiGroupBox52.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox49)).EndInit();
            this.uiGroupBox49.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox50)).EndInit();
            this.uiGroupBox50.ResumeLayout(false);
            this.uiGroupBox50.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox48)).EndInit();
            this.uiGroupBox48.ResumeLayout(false);
            this.uiGroupBox48.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayTN;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label59;
        private Janus.Windows.GridEX.EditControls.EditBox txtNangLucSX;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label32;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiCSSX;
        private System.Windows.Forms.Label label31;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiCSSX;
        private Janus.Windows.EditControls.UIComboBox cbbDCTruSoCSSX;
        private System.Windows.Forms.Label label30;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox19;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox20;
        private Janus.Windows.GridEX.GridEX dgListThanhVien;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox28;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiepCN;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiCN;
        private System.Windows.Forms.Label label40;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiepCN;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label48;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox21;
        private System.Windows.Forms.Label label46;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox13;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox14;
        private Janus.Windows.GridEX.GridEX dgListNganhHangSXChinh;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox15;
        private System.Windows.Forms.Label label37;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiNganhNgheSX;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTN;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox29;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox30;
        private Janus.Windows.GridEX.GridEX dgListCTM;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox31;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiepCTM;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiCSSXCTM;
        private System.Windows.Forms.Label label49;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiepCTM;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox32;
        private System.Windows.Forms.Label label52;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox12;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiNganhNghe;
        private System.Windows.Forms.Label label38;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHaiQuan;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongSoHuu;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoLuong;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongDiThue;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongKhac;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongCongNhan;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtBoPhanQuanLy;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongChiNhanh;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongThanhVienCTM;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString1;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label105;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongCN;
        private System.Windows.Forms.Label label106;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDienTichNX;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label104;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox38;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox39;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox40;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox41;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label109;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtChuKySXTG;
        private Janus.Windows.EditControls.UIComboBox cbbCKSXDVT;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox42;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox43;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox44;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThoiGianSXTG;
        private Janus.Windows.EditControls.UIComboBox cbbThoiGianSXDVT;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label120;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongSP;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox45;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongSoHuuCSSX;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label141;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox46;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox51;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox52;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label143;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox49;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox50;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox48;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox54;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox55;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox56;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox53;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label149;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongSoHuuDT;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox57;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox58;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox59;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThoiGianSXTGDT;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label167;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongSPDT;
        private Janus.Windows.EditControls.UIComboBox cbbThoiGianSXDVTDT;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label177;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label label179;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiepDT;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoanhNghiepDT;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiepDT;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label176;
        private Janus.Windows.GridEX.GridEX dgListSPNLSXDT;
        private Janus.Windows.GridEX.GridEX dgListSPCKSXDT;
        private Janus.Windows.GridEX.GridEX grListHD;
        private Janus.Windows.EditControls.UIButton btnDeleteNganhNghe;
        private Janus.Windows.EditControls.UIButton btnAddNganhNghe;
        private Janus.Windows.EditControls.UIButton btnDeleteCSSX;
        private Janus.Windows.EditControls.UIButton btnAddCSSX;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayHH;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayHD;
        private Janus.Windows.GridEX.GridEX dgListSPNLSX;
        private Janus.Windows.GridEX.GridEX dgListSPCKSX;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaSPCK;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVT;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaHSCK;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaSPNLSX;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaHSNLSX;
        private Janus.Windows.EditControls.UIButton btnDeleteSPNLSX;
        private Janus.Windows.EditControls.UIButton btnAddSPNLSX;
        private Janus.Windows.EditControls.UIButton btnDeleteSPCKSX;
        private Janus.Windows.EditControls.UIButton btnAddSPCKSX;
        private Janus.Windows.EditControls.UIButton btnDeleteNganhHangSXChinh;
        private Janus.Windows.EditControls.UIButton btnAddNganhHangSXChinh;
        private Janus.Windows.EditControls.UIButton btnDeleteChiNhanh;
        private Janus.Windows.EditControls.UIButton btnAddChiNhanh;
        private Janus.Windows.EditControls.UIButton btnDeleteCTM;
        private Janus.Windows.EditControls.UIButton btnAddCTM;
        private Janus.Windows.EditControls.UIButton btnAddHD;
        private Janus.Windows.EditControls.UIButton btnDeleteHD;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaHSCKSXDT;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaSPCKSXDT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtChuKySXTGDT;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private Janus.Windows.EditControls.UIComboBox cbbChuKySXDVTDT;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoLuongDT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongDiThueDT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongKhacDT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongCNDT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDienTichNXDT;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaSPNLSXDT;
        private Janus.Windows.EditControls.UIButton btnDeleteSPCKSXDT;
        private Janus.Windows.EditControls.UIButton btnAddSPCKSXDT;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTDT;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaHSNLSXDT;
        private Janus.Windows.EditControls.UIButton btnDeleteSPNLSXDT;
        private Janus.Windows.EditControls.UIButton btnAddSPNLSXDT;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiSua;
        private System.Windows.Forms.Label label155;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoLuongCSSX;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongDiThueCSSX;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongKhacCSSX;
        private Janus.Windows.EditControls.UIButton btnDeleteCSSXDT;
        private Janus.Windows.EditControls.UIButton btnAddCSSXDT;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox24;
        private Janus.Windows.GridEX.GridEX dgListCSSX;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.GridEX.GridEX dgListNganhNghe;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox47;
        private Janus.Windows.GridEX.GridEX dgListDoiTac;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox34;
        private Janus.Windows.GridEX.GridEX dgListKiemTra;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox33;
        private Janus.Windows.EditControls.UIButton btnDeleteKiemTra;
        private Janus.Windows.EditControls.UIButton btnAddKiemTra;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayKT;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoKetLuanKT;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoBienBanKT;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private Janus.Windows.EditControls.UIComboBox cbbDaKiemTra;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.GridEX dgListAttachFile;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox60;
        private Janus.Windows.EditControls.UIButton btnDeleteFile;
        private Janus.Windows.EditControls.UIButton btnViewFile;
        private Janus.Windows.EditControls.UIButton btnAddFile;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Label lblTotalSize;
        private System.Windows.Forms.Label txtFileSize;
        private System.Windows.Forms.Label lblFileName;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox23;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox25;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayCapGPGD;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDTGD;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCMNDGD;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label76;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiDKHKGD;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiCapGPGD;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayCapGPChuTich;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDTChuTich;
        private Janus.Windows.GridEX.EditControls.EditBox SoCMNDChuTich;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiDKHKChuTich;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiCapGPChuTich;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiepTruoc;
        private Janus.Windows.GridEX.EditControls.EditBox txtLyDoChuyenDoi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiepTruoc;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayKetThucNTC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrNuocXuatXu;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHinhDN;
        private Janus.Windows.EditControls.UIComboBox cbbTruSoChinh;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiep;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiTruSoChinh;
        private Janus.Windows.GridEX.EditControls.EditBox txtNganhNgheSX;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox35;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox37;
        private Janus.Windows.GridEX.GridEX dgListDDLGHH;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox36;
        private Janus.Windows.EditControls.UIButton btnDeleteDDLGHH;
        private Janus.Windows.EditControls.UIButton btnAddDDLGHH;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiemLGHH;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDiaDiemLGHH;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox22;
        private Janus.Windows.EditControls.UIComboBox cbbKeToan;
        private Janus.Windows.EditControls.UIComboBox cbbTronThue;
        private Janus.Windows.EditControls.UIComboBox cbbBuonLau;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label53;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox16;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox17;
        private Janus.Windows.GridEX.GridEX dgListCongTyMe;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox27;
        private Janus.Windows.EditControls.UIButton btnDeleteCongTyMe;
        private Janus.Windows.EditControls.UIButton btnAddCongTyMe;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiCSSXTV;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiepTV;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghepTV;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox18;
        private System.Windows.Forms.Label label96;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongThanhVien;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCongTyMe;
        private System.Windows.Forms.Label label45;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaCongTyMe;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label39;
        private Janus.Windows.EditControls.UIButton btnDeleteTTCSSX;
        private Janus.Windows.EditControls.UIButton btnAddTTCSSX;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox61;
        private Janus.Windows.GridEX.GridEX dgListTTCSSX;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiCSSXDT;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label158;
        private Janus.Windows.GridEX.EditControls.EditBox txtNangLucSXDT;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChuyenTT;
        private Janus.Windows.UI.CommandBars.UICommand cmdChuyenTT1;
    }
}