using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using System.IO;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class HYS
    {
        public List<HYS_AttachData> AttachDatas { get; set; }

        public StringBuilder BuilHYSMsg (StringBuilder BuildEdi, string pathConfig, string inputMSGId)
        {
            StringBuilder str = new StringBuilder();
            string boundary = string.Format("=_MultiPart_H{0}", DateTime.Now.ToString("ddMMyyhhmm"));
            string content_Type = string.Format("Content-Type: multipart/mixed; boundary=\"--{0}\"", boundary);
            str.Append(content_Type).AppendLine().AppendLine();
            str.AppendLine("----" + boundary);
            str.Append("Content-Type: text/plain; charset=UTF-8").AppendLine();
            str.Append("Content-Transfer-Encoding: 8bit").AppendLine();
            str.AppendLine();

            MessagesSend msgEdi = Company.KDT.SHARE.VNACCS.ClassVNACC.MessagesSend.LoadHYS(this, inputMSGId); ;
            //msgEdi.Body = this.BuildEdiMessages(new StringBuilder(),GlobalVNACC.PathConfig);
            str.Append(HelperVNACCS.BuildEdiMessages(msgEdi).ToString());
            str.AppendLine();
            foreach (HYS_AttachData data in this.AttachDatas)
            {
                str.AppendLine("----" + boundary);
                str.AppendLine(string.Format("Content-Type: {0}; name=\"{1}\"", FileNameToContentType(data.FileName), string.Format("=?utf-8?B?{0}?=", Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(data.FileName)))));
               // str.AppendLine(string.Format("name=\"{0}\"", "=?utf-8?B?{0}?=" + Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(data.FileName))));
                str.AppendLine("Content-Transfer-Encoding: base64");
                str.AppendLine("Content-Disposition: attachment; " + string.Format("filename=\"{0}\"", string.Format("=?utf-8?B?{0}?=", Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(data.FileName)))));
               // str.AppendLine(string.Format("filename=\"{0}\"", Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(data.FileName))));
                str.AppendLine();//.AppendLine();

                StringBuilder strData = new StringBuilder();
                List<string> listText = new MIMEPartSign("","").SplitByLength(Convert.ToBase64String(data.Data), 76);
                for (int i = 0; i < listText.Count; i++)
                {
                    strData.Append(listText[i]);
                    strData.AppendLine();
                }

                str.Append(strData);
            }
            str.AppendLine();
            str.AppendLine("----" + boundary + "--");
            return str;
        }
        public StringBuilder BuildEdiMessages(StringBuilder StrBuild, string pathConfig)
        {
            StringBuilder str = StrBuild;
            string IDLoi = string.Empty;
            try
            {

                FileInfo finfo = new FileInfo(pathConfig + "\\HYS.txt");
                if (!finfo.Exists)
                {
                    Exception ex = new Exception("Thiếu file config : " + "HYS.txt");
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw ex;
                }
                string[] lines = System.IO.File.ReadAllLines(pathConfig + "\\" + "HYS.txt");
                string groupName = string.Empty;
                for (int i = 0; i < lines.Length; i++)
                {
                    if (!string.IsNullOrEmpty(lines[i].Split('-')[0].Trim()))
                    {
                        if (lines[i].Split('-')[0].Trim().ToUpper() == groupName.ToUpper())
                            continue;
                        PropertyInfo proterty = typeof(HYS).GetProperty(lines[i].Split('-')[0].Trim());
                        if (proterty != null)
                        {
                            IDLoi = proterty.Name;
                            if (proterty.PropertyType == typeof(PropertiesAttribute))
                            {
                                PropertiesAttribute properties = (PropertiesAttribute)proterty.GetValue(this, null);
                                if (properties != null)
                                    str = properties.ToEDI(StrBuild,true);
                            }
                            else if (proterty.PropertyType == typeof(GroupAttribute))
                            {
                                GroupAttribute group = (GroupAttribute)proterty.GetValue(this, null);
                                str = group.ToEDI(StrBuild);
                                groupName = lines[i].Split('-')[0].Trim();
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Exception e = new Exception("Lỗi tại ID : " + IDLoi + Environment.NewLine + ex.Message);
                Logger.LocalLogger.Instance().WriteMessage(e);
                throw e;
            }
            return str;
        }
        public static string FileNameToContentType(string filename)
        {
            switch (Path.GetExtension(filename).ToLower())
            {
                case ".txt":
                    return "text/plain";

                case ".xml":
                    return "text/xml";

                case ".xsl":
                    return "text/xml";

                case ".bmp":
                    return "image/bmp";

                case ".tif":
                    return "image/tiff";

                case ".jpg":
                    return "image/jpeg";

                case ".gif":
                    return "image/gif";

                case ".wav":
                    return "audio/wav";

                case ".csv":
                    return "application/vnd.ms-excel";

                case ".doc":
                    return "application/msword";

                case ".mdb":
                    return "application/msaccess";

                case ".pdf":
                    return "application/pdf";

                case ".ppt":
                    return "application/vnd.ms-excel";

                case ".xls":
                    return "application/vnd.ms-excel";
            }
            return "application/octet-stream";
        }
    }
}
