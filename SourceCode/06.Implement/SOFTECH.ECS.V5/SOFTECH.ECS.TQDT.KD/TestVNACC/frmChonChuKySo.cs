﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using System.Security.Cryptography.X509Certificates;

namespace TestVNACC
{
    public partial class frmChonChuKySo : Form
    {
        public frmChonChuKySo()
        {
            InitializeComponent();
        }
        private void FrmCauHinhChuKySo_Load(object sender, EventArgs e)
        {
            List<System.Security.Cryptography.X509Certificates.X509Certificate2> items = Cryptography.GetX509CertificatedNames();
            //cbSigns.DataSource = items;
            
            foreach (System.Security.Cryptography.X509Certificates.X509Certificate2 item in items)
            {
                string signName = item.SubjectName.Name.ToString();
                //string name = "";
                //name = GetName(signName);
                cbSigns.Items.Add(signName);
            }
            cbSigns.SelectedItem = Company.KDT.SHARE.Components.Globals.GetX509CertificatedName;

            cbSigns.SelectedValue = Company.KDT.SHARE.Components.Globals.GetX509CertificatedName;
            txtPassword.Text = Company.KDT.SHARE.Components.Globals.PasswordSign;
        }
        private void btnRefersh_Click(object sender, EventArgs e)
        {
            List<System.Security.Cryptography.X509Certificates.X509Certificate2> items = Cryptography.GetX509CertificatedNames();
            cbSigns.Items.Clear();
            foreach (System.Security.Cryptography.X509Certificates.X509Certificate2 item in items)
            {
                string signName = item.SubjectName.Name.ToString();
                cbSigns.Items.Add(signName);
            }
        }
        private void cbChuKySo_SelectedValueChanged(object sender, EventArgs e)
        {
            string signName = cbSigns.SelectedItem.ToString();
            X509Certificate2 x059 = Cryptography.GetStoreX509Certificate2(signName);

            if (x059 != null)
            {
                lblNhaCungCap.Text = GetName(x059.IssuerName.Name);
            }
        }
        private string GetName(string signName)
        {
            string[] infors = signName.Split(',');
            string name = string.Empty;
            string mst = string.Empty;
            foreach (string info in infors)
            {
                int index = info.IndexOf("CN=");
                if (index > -1)
                {
                    name = info.Substring(index + 3);
                }
                index = info.IndexOf("OID");

                if (index > -1)
                {
                    index = info.IndexOf("=");
                    if (index > 0)
                        mst = info.Substring(index + 1);
                }
            }
            if (!string.IsNullOrEmpty(mst)) return string.Format("{0} - [{1}]", name, mst);
            else return string.Format("{0}", name);
        }
        
        private string Check()
        {
            string msg = string.Empty;
            try
            {
                if (cbSigns.SelectedItem == null)
                {
                    return "Chưa chọn chữ ký số";
                }
                Company.KDT.SHARE.Components.Globals.GetX509CertificatedName = cbSigns.SelectedItem.ToString();
                Company.KDT.SHARE.Components.Globals.PasswordSign = txtPassword.Text;
                Company.KDT.SHARE.Components.Globals.IsSignature = true;
                //Helpers.GetSignature("Test");
                checkChuKySo();
                //btnCheckConnect.ImageIndex = 0;
                flag = true;
                return "Mật khẩu được xác nhận là hợp lệ";
            }
            catch (Exception ex)
            {
                //btnCheckConnect.ImageIndex = 1;
                flag = false;
                return ex.Message;
            }
        }
        private bool flag;
        private void btnCheckConnect_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Check());
            if (flag)
            {
                this.Close();
            }
            
        }
        private void checkChuKySo()
        {
            try
            {
                if (!string.IsNullOrEmpty(Globals.GetX509CertificatedName))
                {
                    //ret = new MessageSignature();

                    System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = Cryptography.GetStoreX509Certificate2(Globals.GetX509CertificatedName);
                    if (x509Certificate2 != null)
                    {
                        RSACryptographyHelper rsacryptography = new RSACryptographyHelper(x509Certificate2);
                        string sData = rsacryptography.SignHash("test", Globals.PasswordSign);
                    }
                    else
                        throw new Exception("Bạn đã thiết lập cấu hình chữ ký số\r\nLỗi là do thiết bị (Usb token) này chưa được gắn vào");
                }
                else throw new Exception("Bạn đã thiết lập cấu hình chữ ký số\r\nLỗi là do loại chữ ký số chưa được xác định");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("wrong PIN") || ex.Message.Contains("PIN is incorrect"))
                {
                    throw new Exception("Mật khẩu xác nhận bị sai\r\nVui lòng thử lại");
                }
                else
                {
                    //Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw new Exception("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin");
                }
            }

        }
        private void AdjustWidthComboBox_DropDown(object sender, System.EventArgs e)
        {
            ComboBox senderComboBox = (ComboBox)sender;
            int width = senderComboBox.DropDownWidth;
            Graphics g = senderComboBox.CreateGraphics();
            Font font = senderComboBox.Font;
            int vertScrollBarWidth =
                (senderComboBox.Items.Count > senderComboBox.MaxDropDownItems)
                ? SystemInformation.VerticalScrollBarWidth : 0;

            int newWidth;
            foreach (string s in ((ComboBox)sender).Items)
            {
                newWidth = (int)g.MeasureString(s, font).Width
                    + vertScrollBarWidth;
                if (width < newWidth)
                {
                    width = newWidth;
                }
            }
            senderComboBox.DropDownWidth = width;
        }

    }
}
