﻿namespace Company.Interface.KDT.SXXK
{
    partial class DinhMucSuaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DinhMucSuaForm));
            this.grpTiepNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.btnKetQuaXyLy = new Janus.Windows.EditControls.UIButton();
            this.ccNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.btnKhaiBao = new Janus.Windows.EditControls.UIButton();
            this.btnLayPhanHoi = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnChonNPL = new Janus.Windows.EditControls.UIButton();
            this.btnChonSP = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).BeginInit();
            this.grpTiepNhan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnKhaiBao);
            this.grbMain.Controls.Add(this.btnLayPhanHoi);
            this.grbMain.Controls.Add(this.btnGhi);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnChonSP);
            this.grbMain.Controls.Add(this.btnChonNPL);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.grpTiepNhan);
            this.grbMain.Size = new System.Drawing.Size(756, 303);
            // 
            // grpTiepNhan
            // 
            this.grpTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.grpTiepNhan.Controls.Add(this.btnKetQuaXyLy);
            this.grpTiepNhan.Controls.Add(this.ccNgayTiepNhan);
            this.grpTiepNhan.Controls.Add(this.txtSoTiepNhan);
            this.grpTiepNhan.Controls.Add(this.lblTrangThai);
            this.grpTiepNhan.Controls.Add(this.label9);
            this.grpTiepNhan.Controls.Add(this.label10);
            this.grpTiepNhan.Controls.Add(this.label11);
            this.grpTiepNhan.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTiepNhan.Location = new System.Drawing.Point(0, 0);
            this.grpTiepNhan.Name = "grpTiepNhan";
            this.grpTiepNhan.Size = new System.Drawing.Size(756, 68);
            this.grpTiepNhan.TabIndex = 26;
            this.grpTiepNhan.Text = "Thông tin khai báo";
            this.grpTiepNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grpTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // btnKetQuaXyLy
            // 
            this.btnKetQuaXyLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKetQuaXyLy.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKetQuaXyLy.Icon")));
            this.btnKetQuaXyLy.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnKetQuaXyLy.Location = new System.Drawing.Point(407, 39);
            this.btnKetQuaXyLy.Name = "btnKetQuaXyLy";
            this.btnKetQuaXyLy.Size = new System.Drawing.Size(141, 23);
            this.btnKetQuaXyLy.TabIndex = 23;
            this.btnKetQuaXyLy.Text = "Kết quả xử lý";
            this.btnKetQuaXyLy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKetQuaXyLy.Click += new System.EventHandler(this.btnKetQuaXyLy_Click);
            // 
            // ccNgayTiepNhan
            // 
            // 
            // 
            // 
            this.ccNgayTiepNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayTiepNhan.DropDownCalendar.Name = "";
            this.ccNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTiepNhan.IsNullDate = true;
            this.ccNgayTiepNhan.Location = new System.Drawing.Point(407, 15);
            this.ccNgayTiepNhan.Name = "ccNgayTiepNhan";
            this.ccNgayTiepNhan.Nullable = true;
            this.ccNgayTiepNhan.NullButtonText = "Xóa";
            this.ccNgayTiepNhan.ReadOnly = true;
            this.ccNgayTiepNhan.ShowNullButton = true;
            this.ccNgayTiepNhan.Size = new System.Drawing.Size(141, 21);
            this.ccNgayTiepNhan.TabIndex = 22;
            this.ccNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.ccNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(121, 15);
            this.txtSoTiepNhan.MaxLength = 255;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(142, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.Location = new System.Drawing.Point(83, 49);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(13, 13);
            this.lblTrangThai.TabIndex = 2;
            this.lblTrangThai.Text = "  ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(21, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Trạng thái";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(305, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Ngày tiếp nhận";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Số tiếp nhận";
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.AutomaticSort = false;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            this.dgList.DataMember = "SanPham";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.FocusCellFormatStyle.BackColor = System.Drawing.Color.White;
            this.dgList.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.FocusCellFormatStyle.ForeColor = System.Drawing.Color.Black;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.Location = new System.Drawing.Point(5, 75);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            this.dgList.Size = new System.Drawing.Size(746, 163);
            this.dgList.TabIndex = 30;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.CellEdited += new Janus.Windows.GridEX.ColumnActionEventHandler(this.dgList_CellEdited);
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgList_KeyDown);
            this.dgList.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgList_KeyPress);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // btnKhaiBao
            // 
            this.btnKhaiBao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKhaiBao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKhaiBao.Icon")));
            this.btnKhaiBao.Location = new System.Drawing.Point(121, 273);
            this.btnKhaiBao.Name = "btnKhaiBao";
            this.btnKhaiBao.Size = new System.Drawing.Size(88, 23);
            this.btnKhaiBao.TabIndex = 36;
            this.btnKhaiBao.Text = "Khai  báo";
            this.btnKhaiBao.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKhaiBao.Click += new System.EventHandler(this.btnKhaiBao_Click);
            // 
            // btnLayPhanHoi
            // 
            this.btnLayPhanHoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLayPhanHoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLayPhanHoi.Icon")));
            this.btnLayPhanHoi.Location = new System.Drawing.Point(0, 273);
            this.btnLayPhanHoi.Name = "btnLayPhanHoi";
            this.btnLayPhanHoi.Size = new System.Drawing.Size(109, 23);
            this.btnLayPhanHoi.TabIndex = 35;
            this.btnLayPhanHoi.Text = "Lấy phản hồi";
            this.btnLayPhanHoi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnLayPhanHoi.Click += new System.EventHandler(this.btnLayPhanHoi_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(602, 273);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(69, 23);
            this.btnGhi.TabIndex = 33;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(677, 273);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 34;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnChonNPL
            // 
            this.btnChonNPL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChonNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnChonNPL.Icon")));
            this.btnChonNPL.Location = new System.Drawing.Point(506, 273);
            this.btnChonNPL.Name = "btnChonNPL";
            this.btnChonNPL.Size = new System.Drawing.Size(85, 23);
            this.btnChonNPL.TabIndex = 32;
            this.btnChonNPL.Text = "Chọn NPL";
            this.btnChonNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnChonNPL.VisualStyleManager = this.vsmMain;
            this.btnChonNPL.Click += new System.EventHandler(this.btnChonNPL_Click);
            // 
            // btnChonSP
            // 
            this.btnChonSP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChonSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonSP.Icon = ((System.Drawing.Icon)(resources.GetObject("btnChonSP.Icon")));
            this.btnChonSP.Location = new System.Drawing.Point(424, 273);
            this.btnChonSP.Name = "btnChonSP";
            this.btnChonSP.Size = new System.Drawing.Size(77, 23);
            this.btnChonSP.TabIndex = 32;
            this.btnChonSP.Text = "Chọn SP";
            this.btnChonSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnChonSP.Click += new System.EventHandler(this.btnChonSP_Click);
            // 
            // DinhMucSuaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 303);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "DinhMucSuaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Khai báo sửa định mức";
            this.Load += new System.EventHandler(this.DinhMucSuaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).EndInit();
            this.grpTiepNhan.ResumeLayout(false);
            this.grpTiepNhan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox grpTiepNhan;
        private Janus.Windows.EditControls.UIButton btnKetQuaXyLy;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton btnKhaiBao;
        private Janus.Windows.EditControls.UIButton btnLayPhanHoi;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnChonNPL;
        private Janus.Windows.EditControls.UIButton btnChonSP;
    }
}