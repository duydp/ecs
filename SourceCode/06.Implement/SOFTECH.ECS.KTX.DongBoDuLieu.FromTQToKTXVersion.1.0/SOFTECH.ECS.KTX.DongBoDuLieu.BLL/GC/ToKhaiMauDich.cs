using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Company.KDT.SHARE.Components.Utils;

namespace DongBoDuLieu.BLL.GC
{
    public class TKMD
    {
        public static DataSet GetDataSetALLToKhai(Company.GC.BLL.KDT.GC.HopDong HD, string matKhau)
        {

            StringBuilder queryTK = new StringBuilder();
            queryTK.Append("SELECT * ");
            queryTK.Append("FROM DToKhaiMD TK ");
            queryTK.Append("WHERE TK.Ma_HQ = '" + HD.MaHaiQuan + "' AND Ma_DV = '" + HD.MaDoanhNghiep + "' ");
            queryTK.Append(" AND So_HD = '" + HD.SoHopDong + "'");
            DataSet dsToKhai = WebServiceDB.getDataBySQL(queryTK.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau); ;
            return dsToKhai;

            //TODO: ToKhaiMauDich
            /*
            StringBuilder queryTK = new StringBuilder();
            queryTK.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryTK.Append("SELECT * ");
            queryTK.Append("FROM DToKhaiMD TK ");
            queryTK.Append("WHERE TK.Ma_HQ = ''" + HD.MaHaiQuan + "'' AND Ma_DV = ''" + HD.MaDoanhNghiep + "'' ");
            queryTK.Append(" AND So_HD = ''" + HD.SoHopDong + "''");
            queryTK.Append(" ') AS a;");
            DataSet dsToKhai = WebServiceDB.getDataBySQL(queryTK.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau); ;
            return dsToKhai;
            */
        }

        public static DataSet GetDataSetALLToKhaiByNgay(Company.GC.BLL.KDT.GC.HopDong HD, DateTime NgayBatDau, DateTime NgayKetThuc, string matKhau)
        {
            StringBuilder queryTK = new StringBuilder();
            queryTK.Append("SELECT * ");
            queryTK.Append("FROM DToKhaiMD TK ");
            queryTK.Append("WHERE TK.Ma_HQ = '" + HD.MaHaiQuan + "' AND Ma_DV = '" + HD.MaDoanhNghiep + "' ");
            queryTK.Append(" AND (TK.Ngay_DK) >='" + NgayBatDau.ToString("MM/dd/yyyy") + "'");
            queryTK.Append(" AND (TK.Ngay_DK) <='" + NgayKetThuc.ToString("MM/dd/yyyy") + "'");
            queryTK.Append(" AND So_HD = '" + HD.SoHopDong + "'");
            DataSet dsToKhai = WebServiceDB.getDataBySQL(queryTK.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau); ;
            return dsToKhai;
        }

        /// <summary>
        /// Ghi de tat ca cac du lieu cu
        /// </summary>
        /// <param name="HD"></param>
        /// <param name="dsToKhai"></param>
        /// <param name="dsHang"></param>
        public static void InsertGhiDeALLToKhai(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsToKhai, DataSet dsHang)
        {
            Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection = new Company.GC.BLL.KDT.ToKhaiMauDichCollection();
            foreach (DataRow row in dsToKhai.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.ToKhaiMauDich TKMD = new Company.GC.BLL.KDT.ToKhaiMauDich();
                TKMD.IDHopDong = HD.ID;
                TKMD.CuaKhau_ID = row["Ma_CK"].ToString().Trim();
                TKMD.DiaDiemXepHang = FontConverter.TCVN2Unicode(row["CangNN"].ToString());
                TKMD.DKGH_ID = row["Ma_GH"].ToString();
                TKMD.GiayTo = FontConverter.TCVN2Unicode(row["GiayTo"].ToString().Trim());
                TKMD.SoVanDon = FontConverter.TCVN2Unicode(row["Van_Don"].ToString());
                TKMD.MaDoanhNghiep = HD.MaDoanhNghiep;
                TKMD.MaHaiQuan = HD.MaHaiQuan;
                TKMD.MaLoaiHinh = row["Ma_LH"].ToString();
                TKMD.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"].ToString());
                TKMD.SoToKhai = Convert.ToInt32(row["SoTK"].ToString());
                if (row["NgayDen"].ToString() != "")
                    TKMD.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                if (row["Ngay_GP"].ToString() != "")
                    TKMD.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                if (row["Ngay_HHGP"].ToString() != "")
                    TKMD.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                if (row["Ngay_HHHD"].ToString() != "")
                    TKMD.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                if (row["Ngay_HDTM"].ToString() != "")
                    TKMD.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM"].ToString());
                if (row["Ngay_HD"].ToString() != "")
                    TKMD.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                TKMD.NgayTiepNhan = Convert.ToDateTime(row["Ngay_DK"].ToString());
                if (row["Ngay_VanDon"].ToString() != "")
                    TKMD.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                TKMD.NguyenTe_ID = row["Ma_NT"].ToString();
                if (TKMD.MaLoaiHinh.IndexOf("N") == 0)
                {
                    TKMD.NuocNK_ID = row["Nuoc_NK"].ToString();
                    TKMD.NuocXK_ID = row["Nuoc_XK"].ToString();
                }
                else
                {
                    TKMD.NuocNK_ID = row["Nuoc_XK"].ToString();
                    TKMD.NuocXK_ID = row["Nuoc_NK"].ToString();
                }
                if (row["Phi_BH"].ToString() != "")
                    TKMD.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                if (row["Phi_VC"].ToString() != "")
                    TKMD.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                TKMD.PTTT_ID = row["Ma_PTTT"].ToString();
                TKMD.PTVT_ID = row["Ma_PTVT"].ToString();
                if (row["So_Container"].ToString() != "")
                    TKMD.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                if (row["So_container40"].ToString() != "")
                    TKMD.SoContainer40 = Convert.ToDecimal(row["So_container40"].ToString());
                TKMD.SoGiayPhep = FontConverter.TCVN2Unicode(row["So_GP"].ToString());
                TKMD.SoHieuPTVT = FontConverter.TCVN2Unicode(row["Ten_PTVT"].ToString());
                TKMD.SoHoaDonThuongMai = FontConverter.TCVN2Unicode(row["So_HDTM"].ToString());
                TKMD.SoHopDong = row["So_HD"].ToString();
                if (row["So_Kien"].ToString() != "")
                    TKMD.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                if (row["So_PLTK"].ToString() != "")
                    TKMD.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                TKMD.TenChuHang = FontConverter.TCVN2Unicode(row["TenCH"].ToString().Trim());
                TKMD.TrangThaiXuLy = 1;
                if (row["Tr_Luong"].ToString() != "")
                    TKMD.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                if (row["TyGia_VND"].ToString() != "")
                    TKMD.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                if (row["TyGia_USD"].ToString() != "")
                    TKMD.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                TKMD.TenDonViDoiTac = FontConverter.TCVN2Unicode(row["DV_DT"].ToString().Trim());

                if (dsHang != null)
                {
                    DataRow[] rowHang = dsHang.Tables[0].Select("SoTK=" + TKMD.SoToKhai + " and Ma_LH='" + TKMD.MaLoaiHinh + "' and Ma_HQ='" + TKMD.MaHaiQuan + "' and NamDK=" + TKMD.NgayDangKy.Year);
                    #region BEGIN HANG MD
                    if (rowHang != null && rowHang.Length > 0)
                    {
                        TKMD.HMDCollection = new Company.GC.BLL.KDT.HangMauDichCollection();
                        foreach (DataRow rowIndex in rowHang)
                        {
                            Company.GC.BLL.KDT.HangMauDich hang = new Company.GC.BLL.KDT.HangMauDich();
                            if (rowIndex["DGia_KB"].ToString() != "")
                                hang.DonGiaKB = Convert.ToDecimal(rowIndex["DGia_KB"].ToString());
                            if (rowIndex["DGia_TT"].ToString() != "")
                                hang.DonGiaTT = Convert.ToDecimal(rowIndex["DGia_TT"].ToString());
                            hang.DVT_ID = rowIndex["Ma_DVT"].ToString();
                            hang.MaHS = rowIndex["Ma_HangKB"].ToString().Trim();
                            hang.MaPhu = rowIndex["Ma_Phu"].ToString().Substring(1).Trim();
                            TKMD.LoaiHangHoa = rowIndex["Ma_Phu"].ToString().Substring(0, 1);
                            if (rowIndex["MienThue"].ToString() != "")
                                hang.MienThue = Convert.ToByte(rowIndex["MienThue"].ToString());
                            hang.NuocXX_ID = rowIndex["Nuoc_XX"].ToString();
                            if (rowIndex["Phu_Thu"].ToString() != "")
                                hang.PhuThu = Convert.ToDecimal(rowIndex["Phu_Thu"].ToString());
                            if (rowIndex["Luong"].ToString() != "")
                                hang.SoLuong = Convert.ToDecimal(rowIndex["Luong"].ToString());
                            hang.TenHang = FontConverter.TCVN2Unicode(rowIndex["Ten_Hang"].ToString().Trim());
                            if (rowIndex["Thue_VAT"].ToString() != "")
                                hang.ThueGTGT = Convert.ToDecimal(rowIndex["Thue_VAT"].ToString());
                            if (rowIndex["TS_VAT"].ToString() != "")
                                hang.ThueSuatGTGT = Convert.ToDecimal(rowIndex["TS_VAT"].ToString());
                            if (rowIndex["TS_TTDB"].ToString() != "")
                                hang.ThueSuatTTDB = Convert.ToDecimal(rowIndex["TS_TTDB"].ToString());
                            if (rowIndex["TS_XNK"].ToString() != "")
                                hang.ThueSuatXNK = Convert.ToDecimal(rowIndex["TS_XNK"].ToString());
                            if (rowIndex["Thue_TTDB"].ToString() != "")
                                hang.ThueTTDB = Convert.ToDecimal(rowIndex["Thue_TTDB"].ToString());
                            if (rowIndex["Thue_XNK"].ToString() != "")
                                hang.ThueXNK = Convert.ToDecimal(rowIndex["Thue_XNK"].ToString());
                            if (rowIndex["TriGia_KB"].ToString() != "")
                                hang.TriGiaKB = Convert.ToDecimal(rowIndex["TriGia_KB"].ToString());
                            if (rowIndex["TGKB_VND"].ToString() != "")
                                hang.TriGiaKB_VND = Convert.ToDecimal(rowIndex["TGKB_VND"].ToString());
                            if (rowIndex["TriGia_ThuKhac"].ToString() != "")
                                hang.TriGiaThuKhac = Convert.ToDecimal(rowIndex["TriGia_ThuKhac"].ToString());
                            if (rowIndex["TriGia_TT"].ToString() != "")
                                hang.TriGiaTT = Convert.ToDecimal(rowIndex["TriGia_TT"].ToString());
                            if (rowIndex["TyLe_ThuKhac"].ToString() != "")
                                hang.TyLeThuKhac = Convert.ToDecimal(rowIndex["TyLe_ThuKhac"].ToString());
                            TKMD.HMDCollection.Add(hang);
                        }
                    }
                    #endregion END HANGMD
                }
                TKMDCollection.Add(TKMD);
            }

            string msgTK = "SoToKhai = {0}, MaLoaiHinh = '{1}', NgayDangKy = '{2}'";
            string msgHang = "MaHang = '{0}', TenHang = '{1}', MaHS = '{2}'";
            DataTable dtHangTemp = new DataTable();

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (Company.GC.BLL.KDT.ToKhaiMauDich TKMD in TKMDCollection)
                    {
                        msgTK = string.Format(msgTK, TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NgayDangKy.ToShortDateString());

                        TKMD.IDHopDong = HD.ID;
                        TKMD.InsertTransactionKTX(transaction);

                        int i = 1;
                        foreach (Company.GC.BLL.KDT.HangMauDich hmd in TKMD.HMDCollection)
                        {
                            msgTK += " - " + string.Format(msgHang, hmd.MaPhu, hmd.TenHang, hmd.MaHS);

                            hmd.TKMD_ID = TKMD.ID;
                            hmd.SoThuTuHang = i++;

                            if (!CheckHangDuplicate(TKMD.HMDCollection, hmd.MaPhu))
                                hmd.InsertTransaction(transaction);
                            else
                            {
                                //Trung thong tin hang, khong thuc hien Insert.
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(msgTK, ex));
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public static bool CheckHangDuplicate(Company.GC.BLL.KDT.HangMauDichCollection hmdCollection, string maHang)
        {
            int cnt = 0;

            foreach (Company.GC.BLL.KDT.HangMauDich hmd in hmdCollection)
            {
                if (hmd.MaPhu == maHang)
                {
                    cnt += 1;
                }
            }

            return cnt > 1;
        }

        /// <summary>
        /// Ghi du lieu moi. bo qua du lieu cu
        /// </summary>
        /// <param name="HD"></param>
        /// <param name="dsToKhai"></param>
        /// <param name="dsHang"></param>
        public static void InsertGhiDuLieuToKhaiMoi(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsToKhai, DataSet dsHang)
        {
            Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection = new Company.GC.BLL.KDT.ToKhaiMauDichCollection();
            foreach (DataRow row in dsToKhai.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.ToKhaiMauDich TKMD = new Company.GC.BLL.KDT.ToKhaiMauDich();
                TKMD.IDHopDong = HD.ID;
                TKMD.MaDoanhNghiep = HD.MaDoanhNghiep;
                TKMD.MaHaiQuan = HD.MaHaiQuan;
                TKMD.MaLoaiHinh = row["Ma_LH"].ToString();
                TKMD.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"].ToString());
                TKMD.SoToKhai = Convert.ToInt32(row["SoTK"].ToString());
                TKMD.SetDabaseMoi("MSSQL");
                if (TKMD.LoadBySoToKhai(TKMD.NgayDangKy.Year))
                    continue;
                TKMD.CuaKhau_ID = row["Ma_CK"].ToString().Trim();
                TKMD.DiaDiemXepHang = FontConverter.TCVN2Unicode(row["CangNN"].ToString());
                TKMD.DKGH_ID = row["Ma_GH"].ToString();
                TKMD.GiayTo = FontConverter.TCVN2Unicode(row["GiayTo"].ToString().Trim());
                TKMD.SoVanDon = FontConverter.TCVN2Unicode(row["Van_Don"].ToString());

                if (row["NgayDen"].ToString() != "")
                    TKMD.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                if (row["Ngay_GP"].ToString() != "")
                    TKMD.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                if (row["Ngay_HHGP"].ToString() != "")
                    TKMD.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                if (row["Ngay_HHHD"].ToString() != "")
                    TKMD.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                if (row["Ngay_HDTM"].ToString() != "")
                    TKMD.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM"].ToString());
                if (row["Ngay_HD"].ToString() != "")
                    TKMD.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                TKMD.NgayTiepNhan = Convert.ToDateTime(row["Ngay_DK"].ToString());
                if (row["Ngay_VanDon"].ToString() != "")
                    TKMD.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                TKMD.NguyenTe_ID = row["Ma_NT"].ToString();
                if (TKMD.MaLoaiHinh.IndexOf("N") == 0)
                {
                    TKMD.NuocNK_ID = row["Nuoc_NK"].ToString();
                    TKMD.NuocXK_ID = row["Nuoc_XK"].ToString();
                }
                else
                {
                    TKMD.NuocNK_ID = row["Nuoc_XK"].ToString();
                    TKMD.NuocXK_ID = row["Nuoc_NK"].ToString();
                }
                if (row["Phi_BH"].ToString() != "")
                    TKMD.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                if (row["Phi_VC"].ToString() != "")
                    TKMD.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                TKMD.PTTT_ID = row["Ma_PTTT"].ToString();
                TKMD.PTVT_ID = row["Ma_PTVT"].ToString();
                if (row["So_Container"].ToString() != "")
                    TKMD.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                if (row["So_container40"].ToString() != "")
                    TKMD.SoContainer40 = Convert.ToDecimal(row["So_container40"].ToString());
                TKMD.SoGiayPhep = FontConverter.TCVN2Unicode(row["So_GP"].ToString());
                TKMD.SoHieuPTVT = FontConverter.TCVN2Unicode(row["Ten_PTVT"].ToString());
                TKMD.SoHoaDonThuongMai = FontConverter.TCVN2Unicode(row["So_HDTM"].ToString());
                TKMD.SoHopDong = row["So_HD"].ToString();
                if (row["So_Kien"].ToString() != "")
                    TKMD.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                if (row["So_PLTK"].ToString() != "")
                    TKMD.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                TKMD.TenChuHang = FontConverter.TCVN2Unicode(row["TenCH"].ToString().Trim());
                TKMD.TrangThaiXuLy = 1;
                if (row["Tr_Luong"].ToString() != "")
                    TKMD.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                if (row["TyGia_VND"].ToString() != "")
                    TKMD.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                if (row["TyGia_USD"].ToString() != "")
                    TKMD.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                TKMD.TenDonViDoiTac = FontConverter.TCVN2Unicode(row["DV_DT"].ToString().Trim());

                DataRow[] rowHang = dsHang.Tables[0].Select("SoTK=" + TKMD.SoToKhai + " and Ma_LH='" + TKMD.MaLoaiHinh + "' and Ma_HQ='" + TKMD.MaHaiQuan + "' and NamDK=" + TKMD.NgayDangKy.Year);
                if (rowHang != null && rowHang.Length > 0)
                {
                    TKMD.HMDCollection = new Company.GC.BLL.KDT.HangMauDichCollection();
                    foreach (DataRow rowIndex in rowHang)
                    {
                        Company.GC.BLL.KDT.HangMauDich hang = new Company.GC.BLL.KDT.HangMauDich();
                        if (rowIndex["DGia_KB"].ToString() != "")
                            hang.DonGiaKB = Convert.ToDecimal(rowIndex["DGia_KB"].ToString());
                        if (rowIndex["DGia_TT"].ToString() != "")
                            hang.DonGiaTT = Convert.ToDecimal(rowIndex["DGia_TT"].ToString());
                        hang.DVT_ID = rowIndex["Ma_DVT"].ToString();
                        hang.MaHS = rowIndex["Ma_HangKB"].ToString().Trim();
                        hang.MaPhu = rowIndex["Ma_Phu"].ToString().Substring(1).Trim();
                        TKMD.LoaiHangHoa = rowIndex["Ma_Phu"].ToString().Substring(0, 1);
                        if (rowIndex["MienThue"].ToString() != "")
                            hang.MienThue = Convert.ToByte(rowIndex["MienThue"].ToString());
                        hang.NuocXX_ID = rowIndex["Nuoc_XX"].ToString();
                        if (rowIndex["Phu_Thu"].ToString() != "")
                            hang.PhuThu = Convert.ToDecimal(rowIndex["Phu_Thu"].ToString());
                        if (rowIndex["Luong"].ToString() != "")
                            hang.SoLuong = Convert.ToDecimal(rowIndex["Luong"].ToString());
                        hang.TenHang = FontConverter.TCVN2Unicode(rowIndex["Ten_Hang"].ToString().Trim());
                        if (rowIndex["Thue_VAT"].ToString() != "")
                            hang.ThueGTGT = Convert.ToDecimal(rowIndex["Thue_VAT"].ToString());
                        if (rowIndex["TS_VAT"].ToString() != "")
                            hang.ThueSuatGTGT = Convert.ToDecimal(rowIndex["TS_VAT"].ToString());
                        if (rowIndex["TS_TTDB"].ToString() != "")
                            hang.ThueSuatTTDB = Convert.ToDecimal(rowIndex["TS_TTDB"].ToString());
                        if (rowIndex["TS_XNK"].ToString() != "")
                            hang.ThueSuatXNK = Convert.ToDecimal(rowIndex["TS_XNK"].ToString());
                        if (rowIndex["Thue_TTDB"].ToString() != "")
                            hang.ThueTTDB = Convert.ToDecimal(rowIndex["Thue_TTDB"].ToString());
                        if (rowIndex["Thue_XNK"].ToString() != "")
                            hang.ThueXNK = Convert.ToDecimal(rowIndex["Thue_XNK"].ToString());
                        if (rowIndex["TriGia_KB"].ToString() != "")
                            hang.TriGiaKB = Convert.ToDecimal(rowIndex["TriGia_KB"].ToString());
                        if (rowIndex["TGKB_VND"].ToString() != "")
                            hang.TriGiaKB_VND = Convert.ToDecimal(rowIndex["TGKB_VND"].ToString());
                        if (rowIndex["TriGia_ThuKhac"].ToString() != "")
                            hang.TriGiaThuKhac = Convert.ToDecimal(rowIndex["TriGia_ThuKhac"].ToString());
                        if (rowIndex["TriGia_TT"].ToString() != "")
                            hang.TriGiaTT = Convert.ToDecimal(rowIndex["TriGia_TT"].ToString());
                        if (rowIndex["TyLe_ThuKhac"].ToString() != "")
                            hang.TyLeThuKhac = Convert.ToDecimal(rowIndex["TyLe_ThuKhac"].ToString());
                        TKMD.HMDCollection.Add(hang);
                    }
                    TKMDCollection.Add(TKMD);

                }
            }



            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {



                    foreach (Company.GC.BLL.KDT.ToKhaiMauDich TKMD in TKMDCollection)
                    {
                        TKMD.IDHopDong = HD.ID;
                        TKMD.InsertTransaction(transaction);

                        int i = 1;
                        foreach (Company.GC.BLL.KDT.HangMauDich hmd in TKMD.HMDCollection)
                        {
                            hmd.TKMD_ID = TKMD.ID;
                            hmd.SoThuTuHang = i++;
                            hmd.InsertTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
}
