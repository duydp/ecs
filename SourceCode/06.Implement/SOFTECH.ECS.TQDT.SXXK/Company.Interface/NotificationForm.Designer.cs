﻿namespace Company.Interface
{
    partial class NotificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBarFileDownloaded = new System.Windows.Forms.ProgressBar();
            this.lblNotification = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // progressBarFileDownloaded
            // 
            this.progressBarFileDownloaded.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarFileDownloaded.Location = new System.Drawing.Point(31, 24);
            this.progressBarFileDownloaded.Name = "progressBarFileDownloaded";
            this.progressBarFileDownloaded.Size = new System.Drawing.Size(226, 23);
            this.progressBarFileDownloaded.TabIndex = 4;
            // 
            // lblNotification
            // 
            this.lblNotification.AutoSize = true;
            this.lblNotification.Location = new System.Drawing.Point(28, 64);
            this.lblNotification.Name = "lblNotification";
            this.lblNotification.Size = new System.Drawing.Size(35, 13);
            this.lblNotification.TabIndex = 5;
            this.lblNotification.Text = "label1";
            // 
            // NotificationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 88);
            this.Controls.Add(this.lblNotification);
            this.Controls.Add(this.progressBarFileDownloaded);
            this.Name = "NotificationForm";
            this.Text = "NotificationForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBarFileDownloaded;
        private System.Windows.Forms.Label lblNotification;
    }
}