﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.KD
{
    public class NodeToKhai_XKD
    {
        /// <summary>
        /// Loại chứng từ (= 929) 
        /// </summary>
        public const string issuer = "issuer";
        /// <summary>
        /// Số tham chiếu tờ khai
        /// </summary>
        public const string reference = "reference";
        /// <summary>
        /// Ngày khai báo
        /// </summary>
        public const string issue = "issue";
        /// <summary>
        /// Chức năng (Khai báo = 8, sửa =5)
        /// </summary>
        public const string function = "function";
        /// <summary>
        /// Nơi khai báo
        /// </summary>
        public const string issueLocation = "issueLocation";
        /// <summary>
        /// Trạng thái của chứng từ
        /// </summary>
        public const string status = "status";
        /// <summary>
        /// Số tiếp nhận tờ khai; Dùng trong trường hợp khai sửa bản đăng ký
        /// </summary>
        public const string customsReference = "customsReference";
        /// <summary>
        /// Ngày đăng ký chứng từ 
        /// </summary>
        public const string acceptance = "acceptance";
        /// <summary>
        /// Đơn vị HQ khai báo
        /// </summary>
        public const string declarationOffice = "declarationOffice";
        /// <summary>
        /// Số lượng hàng
        /// </summary>
        public const string goodsItem = "goodsItem";
        /// <summary>
        /// Số lượng chứng từ, phụ lục đính kèm
        /// </summary>
        public const string loadingList = "loadingList";
        /// <summary>
        /// Trọng lượng (kg)
        /// </summary>
        public const string totalGrossMass = "totalGrossMass";
        /// <summary>
        /// Mã loại hình
        /// </summary>
        public const string natureOfTransaction = "natureOfTransaction";
        /// <summary>
        /// Mã phương thức thanh toán
        /// </summary>
        public const string paymentMethod = "paymentMethod";
        /// <summary>
        /// Đại lý
        /// </summary>
        public const string Agent = "Agent";
        /// <summary>
        /// Tên đại lý
        /// </summary>
        public const string name = "name";
        /// <summary>
        /// Mã đại lý
        /// </summary>
        public const string identity = "identity";
        /// <summary>
        /// Nguyên tệ
        /// </summary>
        public const string CurrencyExchange = "CurrencyExchange";
        /// <summary>
        /// Mã nguyên tệ
        /// </summary>
        public const string currencyType = "currencyType";
        /// <summary>
        /// Tỷ giá nguyên tệ
        /// </summary>
        public const string rate = "rate";
        /// <summary>
        /// Số kiện
        /// </summary>
        public const string DeclarationPackaging = "DeclarationPackaging";
        /// <summary>
        /// Số kiện
        /// </summary>
        public const string quantity = "quantity";
        /// <summary>
        /// 	Hợp đồng
        /// </summary>
        public const string AdditionalDocument = "AdditionalDocument";
        /// <summary>
        /// Loại chứng từ (= 315)
        /// </summary>
        public const string type = "type";
        /// <summary>
        /// Ngày hết hạn
        /// </summary>
        public const string expire = "expire";
        /// <summary>
        /// Hóa đơn thương mại
        /// </summary>
        public const string Invoice = "Invoice";
        /// <summary>
        /// Người nhập khẩu
        /// </summary>
        public const string Importer = "Importer";
        /// <summary>
        /// Người đại diện doanh nghiệp
        /// </summary>
        public const string RepresentativePerson = "RepresentativePerson";
        /// <summary>
        /// Chức vụ
        /// </summary>
        public const string contactFunction = "contactFunction";
        /// <summary>
        /// 
        /// </summary>
        public const string AditionalInformation = "AditionalInformation";
        /// <summary>
        /// Mã loại thông tin (=001)
        /// </summary>
        public const string statement = "statement";
        /// <summary>
        /// Ghi chú khác
        /// </summary>
        public const string content = "content";
        /// <summary>
        /// Thông tin về hàng hóa
        /// </summary>
        public const string GoodsShipment = "GoodsShipment";
        /// <summary>
        /// Mã nước xuất khẩu
        /// </summary>
        public const string exportationCountry = "exportationCountry";
        /// <summary>
        /// Người giao hàng
        /// </summary>
        public const string Consignor = "Consignor";
        /// <summary>
        /// Người nhận hàng
        /// </summary>
        public const string Consignee = "Consignee";
        /// <summary>
        /// Người nhận hàng trung gian
        /// </summary>
        public const string NotifyParty = "NotifyParty";
        /// <summary>
        /// Địa điểm giao hàng
        /// </summary>
        public const string DeliveryDestination = "DeliveryDestination";
        /// <summary>
        /// Địa điểm giao hàng
        /// </summary>
        public const string line = "line";
        /// <summary>	
        /// Cửa khẩu nhập
        /// </summary>
        public const string EntryCustomsOffice = "EntryCustomsOffice";
        /// <summary>
        /// Mã cửa khẩu nhập
        /// </summary>
        public const string code = "code";
        /// <summary>
        /// Cửa khẩu xuất
        /// </summary>
        public const string ExitCustomsOffice = "ExitCustomsOffice";
        /// <summary>
        /// Người xuất khẩu
        /// </summary>
        public const string Exporter = "Exporter";
        /// <summary>
        /// Điều kiện giao hàng
        /// </summary>
        public const string TradeTerm = "TradeTerm";
        /// <summary>
        /// Mã điều kiện giao hàng
        /// </summary>
        public const string condition = "condition";
        /// <summary>
        /// Hàng khai báo
        /// </summary>
        public const string CustomsGoodsItem = "CustomsGoodsItem";
        /// <summary>
        /// Trị giá khai báo
        /// </summary>
        public const string customsValue = "customsValue";
        /// <summary>
        /// Số thứ tự hàng
        /// </summary>
        public const string sequence = "sequence";
        /// <summary>
        /// Trị giá tính thuế
        /// </summary>
        public const string statisticalValue = "statisticalValue";
        /// <summary>
        /// Đơn giá nguyên tệ
        /// </summary>
        public const string unitPrice = "unitPrice";
        /// <summary>
        /// 	Hãng sản xuất
        /// </summary>
        public const string Manufacturer = "Manufacturer";
        /// <summary>
        /// Xuất xứ
        /// </summary>
        public const string Origin = "Origin";
        /// <summary>
        /// Mã nước xuất xứ
        /// </summary>
        public const string originCountry = "originCountry";
        /// <summary>
        /// Chi tiết hàng, thuế
        /// </summary>
        public const string Commodity = "Commodity";
        /// <summary>
        /// Tên hàng
        /// </summary>
        public const string description = "description";
        /// <summary>
        /// Mã hàng do nhà sản xuất hoặc doanh nghiệp quy định
        /// </summary>
        public const string identification = "identification";
        /// <summary>
        /// 	Mã HS
        /// </summary>
        public const string tariffClassification = "tariffClassification";
        /// <summary>
        /// Mã HS mở rộng
        /// </summary>
        public const string tariffClassificationExtension = "tariffClassificationExtension";
        /// <summary>
        /// Nhãn hiệu
        /// </summary>
        public const string brand = "brand";
        /// <summary>
        /// Quy cách, phẩm chất
        /// </summary>
        public const string grade = "grade";
        /// <summary>
        /// Thành phần
        /// </summary>
        public const string ingredients = "ingredients";
        /// <summary>
        /// model của hàng hóa
        /// </summary>
        public const string modelNumber = "modelNumber";
        /// <summary>
        /// Thuế
        /// </summary>
        public const string DutyTaxFee = "DutyTaxFee";
        /// <summary>
        /// Số thuế tự tính
        /// </summary>
        public const string adValoremTaxBase = "adValoremTaxBase";
        /// <summary>
        /// 
        /// </summary>
        public const string dutyRegime = "dutyRegime";
        /// <summary>
        /// 
        /// </summary>
        public const string specificTaxBase = "specificTaxBase";
        /// <summary>
        /// Thuế suất
        /// </summary>
        public const string tax = "tax";
        /// <summary>
        /// 
        /// </summary>
        public const string InvoiceLine = "InvoiceLine";
        /// <summary>
        /// 
        /// </summary>
        public const string itemCharge = "itemCharge";
        /// <summary>
        /// 
        /// </summary>
        public const string GoodsMeasure = "GoodsMeasure";
        /// <summary>
        /// Mã đơn vị tính
        /// </summary>
        public const string measureUnit = "measureUnit";
        /// <summary>
        /// Tên, địa chỉ người xuất khẩu trên C/O
        /// </summary>
        public const string exporter = "exporter";
        /// <summary>
        /// Tên, địa chỉ người nhập khẩu
        /// </summary>
        public const string importer = "importer";
        /// <summary>
        /// Mã nước nhập khẩu trên C/O
        /// </summary>
        public const string importationCountry = "importationCountry";
        /// <summary>
        /// Thông tin tờ khai trị giá
        /// </summary>
        public const string AdditionalInformation = "AdditionalInformation";
        /// <summary>
        /// Mô tả thông tin
        /// </summary>
        public const string statementDescription = "statementDescription";
        /// <summary>
        /// Trị giá Hải quan
        /// </summary>
        public const string CustomsValuation = "CustomsValuation";
        /// <summary>
        /// Tổng chi phí khác (vận tải, bảo hiểm và các chi phí khác từ cảng xuất đến cảng nhập)
        /// </summary>
        public const string exitToEntryCharge = "exitToEntryCharge";
        /// <summary>
        /// Phí vận tải
        /// </summary>
        public const string freightCharge = "freightCharge";
        /// <summary>
        /// Phương pháp xác định trị giá
        /// </summary>
        public const string method = "method";
        /// <summary>
        /// Tổng các khoản phải cộng - tổng các khoản được trừ
        /// </summary>
        public const string otherChargeDeduction = "otherChargeDeduction";
        /// <summary>
        /// Các giá trị điều chỉnh
        /// </summary>
        public const string ValuationAdjustment = "ValuationAdjustment";
        /// <summary>
        /// Mã loại giá trị
        /// </summary>
        public const string addition = "addition";
        /// <summary>
        /// giá trị
        /// </summary>
        public const string percentage = "percentage";
        /// <summary>
        /// Số tiền
        /// </summary>
        public const string amount = "amount";
        /// <summary>
        /// Thông tin về hàng quản lý chuyên ngành
        /// </summary>
        public const string SpecializedManagement = "SpecializedManagement";
        /// <summary>
        /// 
        /// </summary>
        public const string Licenses = "Licenses";
        /// <summary>
        /// 
        /// </summary>
        public const string ContractDocuments = "ContractDocuments";
        /// <summary>
        /// 
        /// </summary>
        public const string CommercialInvoices = "CommercialInvoices";
        /// <summary>
        /// 
        /// </summary>
        public const string CertificateOfOrigins = "CertificateOfOrigins";
        /// <summary>
        /// 
        /// </summary>
        public const string BillOfLadings = "BillOfLadings";
        /// <summary>
        /// Đơn xin chuyển cửa khẩu
        /// </summary>
        public const string CustomsOfficeChangedRequest = "CustomsOfficeChangedRequest";
        /// <summary>
        /// Danh sách chứng từ đính kèm, có thể lặp lại
        /// </summary>
        public const string AttachDocuments = "AttachDocuments";
        /// <summary>
        /// 
        /// </summary>
        public const string AttachDocumentItem = "AttachDocumentItem";
        /// <summary>
        /// Chú thích diễn giải
        /// </summary>
        public const string discription = "discription";
        /// <summary>
        /// Danh sách file đính kèm, lặp lại nhiều lần
        /// </summary>
        public const string AttachedFiles = "AttachedFiles";
        /// <summary>
        /// 
        /// </summary>
        public const string AttachedFile = "AttachedFile";
        /// <summary>
        /// Tên file
        /// </summary>
        public const string fileName = "fileName";
        /// <summary>
        /// Chứng từ hải quan trước đó
        /// </summary>
        public const string PreviousCustomsprocedure = "PreviousCustomsprocedure";
        /// <summary>
        /// Thủ tục hải quan trước đó
        /// </summary>
        public const string CustomsProcedure = "CustomsProcedure";
        /// <summary>
        /// 
        /// </summary>
        public const string current = "current";
        /// <summary>
        /// 
        /// </summary>
        public const string previous = "previous";
        /// <summary>
        /// Địa điểm kiểm tra
        /// </summary>
        public const string ExaminationPlace = "ExaminationPlace";

    }
}
