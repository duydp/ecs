﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
  public class TransportEquipment
    {
        [XmlElement("characteristic")]
        public string Characteristic { get; set; }
        [XmlElement("fullness")]
        public string Fullness { get; set; }
        [XmlElement("seal")]
        public string Seal { get; set; }
        /// <summary>
        /// Số hiệu container
        /// </summary>
        [XmlElement("EquipmentIdentification")]
        public EquipmentIdentification EquipmentIdentifications { get; set; }
        /// <summary>
        /// Trọng lượng hàng hóa - không tính thiết bị của nhà vận tải
        /// </summary>
        [XmlElement("grossMass")]
        public string GrossMass { get; set; }
        /// <summary>
        /// Trọng lượng tịnh của hàng hóa - không tính thiết bị của nhà vận tải
        /// </summary>
        [XmlElement("netMass")]
        public string NetMass { get; set; }

        /// <summary>
        /// Địa điểm đóng hàng
        /// </summary>
        [XmlElement("packagingLocation")]
        public string PackagingLocation { get; set; }

        /// <summary>
        /// Số lượng kiện trong container
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }

        /// <summary>
        /// Tính chất cont
        /// </summary>
        [XmlElement("property")]
        public string Property { get; set; }


    }
}
