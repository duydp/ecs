﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.Interface.Report;
using Janus.Windows.GridEX;
using System.Xml.Serialization;
using System.IO;

namespace Company.Interface.SXXK
{
    public partial class HSTLManageForm : Company.Interface.BaseForm
    {
        HoSoThanhLyDangKyCollection HSTLCollection = new HoSoThanhLyDangKyCollection();
        public HSTLManageForm()
        {
            InitializeComponent();

            mniImportHSTK.Visible = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Import"));
        }

        private void HSTLManageForm_Load(object sender, EventArgs e)
        {
            txtNamTiepNhan.Value = DateTime.Today.Year;
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            dgList.Tables[0].Columns["MaDoanhNghiep"].Visible = GlobalSettings.DAI_LY_TTHQ;
            btnSearch_Click(null, null);

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = " MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
            if ((int)txtSoTiepNhan.Value > 0)
            {
                where += " AND SoHoSo = " + txtSoTiepNhan.Value;
            }
            if (txtNamTiepNhan.TextLength > 0)
            {
                where += " AND Year(NgayBatDau) = " + txtNamTiepNhan.Value;
            }

            // Thực hiện tìm kiếm.            
            this.HSTLCollection = new HoSoThanhLyDangKy().SelectCollectionDynamic(where, "");
            dgList.DataSource = this.HSTLCollection;
        }

        private void XemBaoCao_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                HoSoThanhLyDangKy HSTL = (HoSoThanhLyDangKy)row.DataRow;
                if (HSTL.TrangThaiThanhKhoan < 400)
                {
                    //ShowMessage("Hồ sơ chưa chạy thanh khoản thành công, không thể xem báo cáo.", false);
                    MLMessages("Hồ sơ chưa chạy thanh khoản thành công, không thể xem báo cáo.", "MSG_THK86", "", false);
                    return;
                }
                HSTL.LoadBKCollection();
                if (GlobalSettings.SoThapPhan.TachLam2 == 1)
                {
                    ChonToKhaiNhapForm1 f = new ChonToKhaiNhapForm1();
                    f.HSTL = HSTL;
                    f.ShowDialog();
                    PreviewFormITG f1 = new PreviewFormITG();
                    f1.SoHoSo = HSTL.SoHoSo;
                    f1.LanThanhLy = HSTL.LanThanhLy;
                    f1.TKNHoanThueCollection = f.TKNHoanThueCollection;
                    f1.TKNKhongThuCollection = f.TKNKhongThuCollection;
                    f1.Show();
                }
                else
                {
                    PreviewForm f = new PreviewForm();
                    f.SoHoSo = HSTL.SoHoSo;
                    f.LanThanhLy = HSTL.LanThanhLy;
                    f.Show();
                }
            }

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            int TTTK = Convert.ToInt32(e.Row.Cells["TrangThaiThanhKhoan"].Value);
            if (GlobalSettings.NGON_NGU == "0")
            {
                if (TTTK == 0)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đang nhập liệu";
                }
                else if (TTTK > 0 && TTTK < 400)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đang chạy thanh khoản";
                }
                else if (TTTK == 400)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã chạy thanh khoản";
                }
                else if (TTTK == 401)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã đóng hồ sơ";
                }
            }
            else
            {
                if (TTTK == 0)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Inputing data";
                }
                else if (TTTK > 0 && TTTK < 400)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Liquidating";
                }
                else if (TTTK == 400)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Liquidated";
                }
                else if (TTTK == 401)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Liquidation closed";
                }
            }
        }

        private void xemBanrToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                HoSoThanhLyDangKy HSTL = (HoSoThanhLyDangKy)row.DataRow;
                if (HSTL.TrangThaiThanhKhoan < 400)
                {
                    // ShowMessage("Hồ sơ chưa chạy thanh khoản thành công, không thể xem bảng chứng từ thanh toán.", false);
                    MLMessages("Hồ sơ chưa chạy thanh khoản thành công, không thể xem báo cáo.", "MSG_THK86", "", false);
                    return;
                }
                HSTL.LoadBKCollection();
                ChungTuThanhToanForm f = new ChungTuThanhToanForm();
                f.HSTL = HSTL;
                f.Show();

            }
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void ctmSuaHSTL_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                HoSoThanhLyDangKy HSTL = (HoSoThanhLyDangKy)row.DataRow;
                if (HSTL.UserName != MainForm.EcsQuanTri.Identity.Name)
                {
                    //ShowMessage("Bộ hồ sơ này thuộc người khác, bạn không thể sửa!",false);
                    MLMessages("Bộ hồ sơ này thuộc người khác, bạn không thể sửa!", "MSG_THK87", "", false);
                    return;
                }
                int count = HSTL.CheckHSTKTruocDoDaDong();
                if (count > 0)
                {
                    ShowMessage("Có " + count + " bộ hồ sơ trước đó chưa đóng, không thể sửa bộ hiện tại.", false);
                    return;
                }
                HSTL.LoadBKCollection();
                CapNhatHoSoThanhLyForm f = new CapNhatHoSoThanhLyForm();
                f.HSTL = HSTL;
                f.ShowDialog();
                btnSearch_Click(null, null);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void xemBaoCao929_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                HoSoThanhLyDangKy HSTL = (HoSoThanhLyDangKy)row.DataRow;
                if (HSTL.TrangThaiThanhKhoan < 400)
                {
                    //ShowMessage("Hồ sơ chưa chạy thanh khoản thành công, không thể xem báo cáo.", false);
                    MLMessages("Hồ sơ chưa chạy thanh khoản thành công, không thể xem báo cáo.", "MSG_THK86", "", false);
                    return;
                }
                HSTL.LoadBKCollection();
                if (GlobalSettings.SoThapPhan.TachLam2 == 1)
                {
                    ChonToKhaiNhapForm1 f = new ChonToKhaiNhapForm1();
                    f.HSTL = HSTL;
                    f.ShowDialog();
                    PreviewFormBC929 f1 = new PreviewFormBC929();
                    f1.SoHoSo = HSTL.SoHoSo;
                    f1.LanThanhLy = HSTL.LanThanhLy;
                    f1.TKNHoanThueCollection = f.TKNHoanThueCollection;
                    f1.TKNKhongThuCollection = f.TKNKhongThuCollection;
                    f1.Show();
                }
                else
                {
                    PreviewForm929 f = new PreviewForm929();
                    f.SoHoSo = HSTL.SoHoSo;
                    f.LanThanhLy = HSTL.LanThanhLy;
                    f.Show();
                }
            }
        }

        private void HSTLManageForm_Activated(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        private void XuatHSMenuItem_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            DataTable HQ_NPLXuatNhapTon = new DataTable();
            DataTable HQ_ThueXNK = new DataTable();
            DataTable thongtinchung = new DataTable();
            DataRow dr = null;
            if (row.RowType == RowType.Record)
            {
                HoSoThanhLyDangKy HSTL = (HoSoThanhLyDangKy)row.DataRow;
                if (HSTL.TrangThaiThanhKhoan < 400)
                {

                    MLMessages("Hồ sơ chưa chạy thanh khoản thành công, không thể xuất báo cáo.", "MSG_THK100", "", false);
                    return;
                }
                thongtinchung.Columns.Add("MaDoanhNghiep");
                thongtinchung.Columns.Add("LanThanhLy");
                thongtinchung.TableName = "ThongTinChung";
                dr = thongtinchung.NewRow();
                dr["MaDoanhNghiep"] = HSTL.MaDoanhNghiep;
                dr["LanThanhLy"] = HSTL.LanThanhLy;
                thongtinchung.Rows.Add(dr);
                DataTable NPLXuatNhapTon = new Company.BLL.KDT.SXXK.BCXuatNhapTon().SelectDynamic("(MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND LanThanhLy = " + HSTL.LanThanhLy + ")", "SoToKhaiNhap,MaNPL,SoToKhaiXuat").Tables[0];
                DataTable ThueXNK = new Company.BLL.KDT.SXXK.BCThueXNK().SelectDynamic("(a.MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND LanThanhLy = " + HSTL.LanThanhLy + ")", "SoToKhaiNhap,MaNPL,SoToKhaiXuat").Tables[0];
                NPLXuatNhapTon.TableName = "NPLXuatNhapTon";
                ThueXNK.TableName = "Thue_XNK";

                //xuat file

                DataSet ds = new DataSet();
                ds.Tables.Add(thongtinchung.Copy());
                ds.Tables.Add(NPLXuatNhapTon.Copy());
                ds.Tables.Add(ThueXNK.Copy());

                try
                {
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        ds.WriteXml(saveFileDialog1.FileName);
                        MLMessages("Xuất thành công hồ sơ thanh khoản ", "MSG_THK101", "", false);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("Error 1: " + ex.Message, false);
                }

            }
        }

        private void txtSoTiepNhan_Click(object sender, EventArgs e)
        {

        }

        private void mniImportHSTK_Click(object sender, EventArgs e)
        {
            Company.Interface.SXXK.ImportHSTKForm f = new ImportHSTKForm();
            f.ShowDialog();

            Invoke(new MethodInvoker(delegate { btnSearch_Click(sender, e); }));
        }
    }
}

