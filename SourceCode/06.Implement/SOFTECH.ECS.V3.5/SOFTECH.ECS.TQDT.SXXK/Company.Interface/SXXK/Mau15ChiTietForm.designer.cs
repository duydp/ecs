﻿namespace Company.Interface.SXXK
{
    partial class Mau15ChiTietForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mau15ChiTietForm));
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTongNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgChiTietNPLXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTongNPLXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgNPLCU_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgNPLCU_Total_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListNPLXuatDetails_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTotal_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.mnuGridChiTietNPL = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripChiTietNPL = new System.Windows.Forms.ToolStripMenuItem();
            this.gridEXPrintDocument1 = new Janus.Windows.GridEX.GridEXPrintDocument();
            this.gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenChuHang2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTimKiemNPL = new Janus.Windows.EditControls.UIButton();
            this.dgListNPL = new Janus.Windows.GridEX.GridEX();
            this.backgroundHangTon = new System.ComponentModel.BackgroundWorker();
            this.dtpDenNgay = new System.Windows.Forms.DateTimePicker();
            this.dtpNgayChotTon = new System.Windows.Forms.DateTimePicker();
            this.dtpTuNgay = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTrangThaiXuLy = new System.Windows.Forms.Label();
            this.mnnTongNPL = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNPLTheoDM = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTongNPLXuat = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNPL_CU = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTongNPLCU = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdImportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSendEdit1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.cmdSendEdit = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdImportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.lblFile = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.dtpNgayTN = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.cbbDVT = new Janus.Windows.EditControls.UIComboBox();
            this.cbbLoaiHinhBaoCao = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoTN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChuKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamQT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTrangThaiXuLy = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuExportBCQT = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblFileName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dateNgayTN = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnReadExcel = new Janus.Windows.EditControls.UIButton();
            this.btnView = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox17 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnDong = new Janus.Windows.EditControls.UIButton();
            this.btnMau15TriGia = new Janus.Windows.EditControls.UIButton();
            this.btnMau15Luong = new Janus.Windows.EditControls.UIButton();
            this.label14 = new System.Windows.Forms.Label();
            this.btnGetDuLieu = new Janus.Windows.EditControls.UIButton();
            this.btnProcess = new Janus.Windows.EditControls.UIButton();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTongNPL = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgChiTietNPLXuat = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTongNPLXuat = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage6 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNPLCU = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage5 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNPLCU_Total = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage8 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListNPLXuatDetails = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage7 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListTotal = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.mnuGridChiTietNPL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).BeginInit();
            this.mnnTongNPL.SuspendLayout();
            this.mnuNPLTheoDM.SuspendLayout();
            this.mnuTongNPLXuat.SuspendLayout();
            this.mnuNPL_CU.SuspendLayout();
            this.mnuTongNPLCU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).BeginInit();
            this.uiGroupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTongNPL)).BeginInit();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgChiTietNPLXuat)).BeginInit();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTongNPLXuat)).BeginInit();
            this.uiTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCU)).BeginInit();
            this.uiTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCU_Total)).BeginInit();
            this.uiTabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLXuatDetails)).BeginInit();
            this.uiTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox17);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(1090, 726);
            // 
            // mnuGridChiTietNPL
            // 
            this.mnuGridChiTietNPL.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripChiTietNPL});
            this.mnuGridChiTietNPL.Name = "mnuGrid";
            this.mnuGridChiTietNPL.Size = new System.Drawing.Size(128, 26);
            // 
            // toolStripChiTietNPL
            // 
            this.toolStripChiTietNPL.Name = "toolStripChiTietNPL";
            this.toolStripChiTietNPL.Size = new System.Drawing.Size(127, 22);
            this.toolStripChiTietNPL.Text = "Xuất Excel";
            this.toolStripChiTietNPL.Click += new System.EventHandler(this.toolStripChiTietNPL_Click);
            // 
            // gridEXPrintDocument1
            // 
            this.gridEXPrintDocument1.CardColumnsPerPage = 1;
            this.gridEXPrintDocument1.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintDocument1.PageHeaderCenter = "DANH SÁCH NGUYÊN PHỤ LIỆU TỒN";
            this.gridEXPrintDocument1.PageHeaderFormatStyle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // gridEXExporter1
            // 
            this.gridEXExporter1.SheetName = "NPLTon";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.txtTenChuHang2);
            this.uiGroupBox5.Controls.Add(this.label1);
            this.uiGroupBox5.Controls.Add(this.btnTimKiemNPL);
            this.uiGroupBox5.Location = new System.Drawing.Point(12, 19);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1035, 51);
            this.uiGroupBox5.TabIndex = 193;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtTenChuHang2
            // 
            this.txtTenChuHang2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTenChuHang2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenChuHang2.Location = new System.Drawing.Point(136, 18);
            this.txtTenChuHang2.Name = "txtTenChuHang2";
            this.txtTenChuHang2.Size = new System.Drawing.Size(220, 20);
            this.txtTenChuHang2.TabIndex = 17;
            this.txtTenChuHang2.Text = "*";
            this.txtTenChuHang2.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Tên chủ hàng";
            // 
            // btnTimKiemNPL
            // 
            this.btnTimKiemNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiemNPL.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnTimKiemNPL.Location = new System.Drawing.Point(362, 16);
            this.btnTimKiemNPL.Name = "btnTimKiemNPL";
            this.btnTimKiemNPL.Size = new System.Drawing.Size(84, 23);
            this.btnTimKiemNPL.TabIndex = 18;
            this.btnTimKiemNPL.Text = "Tìm kiếm";
            this.btnTimKiemNPL.VisualStyleManager = this.vsmMain;
            this.btnTimKiemNPL.WordWrap = false;
            // 
            // dgListNPL
            // 
            this.dgListNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPL.AlternatingColors = true;
            this.dgListNPL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPL.ColumnAutoResize = true;
            this.dgListNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPL.GroupByBoxVisible = false;
            this.dgListNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPL.Location = new System.Drawing.Point(12, 76);
            this.dgListNPL.Name = "dgListNPL";
            this.dgListNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPL.Size = new System.Drawing.Size(1035, 409);
            this.dgListNPL.TabIndex = 192;
            this.dgListNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListNPL.VisualStyleManager = this.vsmMain;
            // 
            // backgroundHangTon
            // 
            this.backgroundHangTon.WorkerReportsProgress = true;
            // 
            // dtpDenNgay
            // 
            this.dtpDenNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpDenNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDenNgay.Location = new System.Drawing.Point(404, 49);
            this.dtpDenNgay.Name = "dtpDenNgay";
            this.dtpDenNgay.Size = new System.Drawing.Size(139, 21);
            this.dtpDenNgay.TabIndex = 10;
            this.dtpDenNgay.Value = new System.DateTime(2016, 12, 31, 0, 0, 0, 0);
            // 
            // dtpNgayChotTon
            // 
            this.dtpNgayChotTon.CustomFormat = "dd/MM/yyyy";
            this.dtpNgayChotTon.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgayChotTon.Location = new System.Drawing.Point(143, 49);
            this.dtpNgayChotTon.Name = "dtpNgayChotTon";
            this.dtpNgayChotTon.Size = new System.Drawing.Size(140, 21);
            this.dtpNgayChotTon.TabIndex = 10;
            this.dtpNgayChotTon.Value = new System.DateTime(2016, 1, 1, 0, 0, 0, 0);
            // 
            // dtpTuNgay
            // 
            this.dtpTuNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpTuNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTuNgay.Location = new System.Drawing.Point(404, 20);
            this.dtpTuNgay.Name = "dtpTuNgay";
            this.dtpTuNgay.Size = new System.Drawing.Size(139, 21);
            this.dtpTuNgay.TabIndex = 10;
            this.dtpTuNgay.Value = new System.DateTime(2016, 1, 1, 0, 0, 0, 0);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Ngày chốt tồn đầu kỳ :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(324, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Đến ngày :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(323, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Từ ngày :";
            // 
            // txtTrangThaiXuLy
            // 
            this.txtTrangThaiXuLy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtTrangThaiXuLy.AutoSize = true;
            this.txtTrangThaiXuLy.BackColor = System.Drawing.Color.Transparent;
            this.txtTrangThaiXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrangThaiXuLy.Location = new System.Drawing.Point(140, 24);
            this.txtTrangThaiXuLy.Name = "txtTrangThaiXuLy";
            this.txtTrangThaiXuLy.Size = new System.Drawing.Size(60, 13);
            this.txtTrangThaiXuLy.TabIndex = 8;
            this.txtTrangThaiXuLy.Text = "Chưa xử lý";
            // 
            // mnnTongNPL
            // 
            this.mnnTongNPL.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.mnnTongNPL.Name = "mnuGrid";
            this.mnnTongNPL.Size = new System.Drawing.Size(128, 26);
            this.mnnTongNPL.Click += new System.EventHandler(this.mnnTongNPL_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(127, 22);
            this.toolStripMenuItem1.Text = "Xuất Excel";
            // 
            // mnuNPLTheoDM
            // 
            this.mnuNPLTheoDM.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.mnuNPLTheoDM.Name = "mnuGrid";
            this.mnuNPLTheoDM.Size = new System.Drawing.Size(128, 26);
            this.mnuNPLTheoDM.Click += new System.EventHandler(this.mnuNPLTheoDM_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(127, 22);
            this.toolStripMenuItem2.Text = "Xuất Excel";
            // 
            // mnuTongNPLXuat
            // 
            this.mnuTongNPLXuat.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3});
            this.mnuTongNPLXuat.Name = "mnuGrid";
            this.mnuTongNPLXuat.Size = new System.Drawing.Size(128, 26);
            this.mnuTongNPLXuat.Click += new System.EventHandler(this.mnuTongNPLXuat_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(127, 22);
            this.toolStripMenuItem3.Text = "Xuất Excel";
            // 
            // mnuNPL_CU
            // 
            this.mnuNPL_CU.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4});
            this.mnuNPL_CU.Name = "mnuGrid";
            this.mnuNPL_CU.Size = new System.Drawing.Size(128, 26);
            this.mnuNPL_CU.Click += new System.EventHandler(this.mnuNPL_CU_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(127, 22);
            this.toolStripMenuItem4.Text = "Xuất Excel";
            // 
            // mnuTongNPLCU
            // 
            this.mnuTongNPLCU.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5});
            this.mnuTongNPLCU.Name = "mnuGrid";
            this.mnuTongNPLCU.Size = new System.Drawing.Size(128, 26);
            this.mnuTongNPLCU.Click += new System.EventHandler(this.mnuTongNPLCU_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(127, 22);
            this.toolStripMenuItem5.Text = "Xuất Excel";
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSendEdit,
            this.cmdSave,
            this.cmdFeedback,
            this.cmdResult,
            this.cmdImportExcel});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("db3f14b4-f472-45b2-95e5-5df0dc360a22");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportExcel1,
            this.cmdSave1,
            this.cmdSend1,
            this.cmdFeedback1,
            this.cmdResult1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(594, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdImportExcel1
            // 
            this.cmdImportExcel1.Key = "cmdImportExcel";
            this.cmdImportExcel1.Name = "cmdImportExcel1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdSend
            // 
            this.cmdSend.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSendEdit1});
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "KHAI BÁO";
            // 
            // cmdSendEdit1
            // 
            this.cmdSendEdit1.Key = "cmdSendEdit";
            this.cmdSendEdit1.Name = "cmdSendEdit1";
            // 
            // cmdSendEdit
            // 
            this.cmdSendEdit.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendEdit.Image")));
            this.cmdSendEdit.Key = "cmdSendEdit";
            this.cmdSendEdit.Name = "cmdSendEdit";
            this.cmdSendEdit.Text = "KHAI BÁO SỬA";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "LƯU LẠI";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback.Image")));
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "NHẬN PHẢN HỒI";
            // 
            // cmdResult
            // 
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "KẾT QUẢ XỬ LÝ";
            // 
            // cmdImportExcel
            // 
            this.cmdImportExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdImportExcel.Image")));
            this.cmdImportExcel.Key = "cmdImportExcel";
            this.cmdImportExcel.Name = "cmdImportExcel";
            this.cmdImportExcel.Text = "NHẬP HÀNG HÓA TỪ FILE EXCEL";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1090, 28);
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFile.ForeColor = System.Drawing.Color.Blue;
            this.lblFile.Location = new System.Drawing.Point(128, 21);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(0, 13);
            this.lblFile.TabIndex = 0;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(22, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Tên File :";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox2.Controls.Add(this.dtpNgayTN);
            this.uiGroupBox2.Controls.Add(this.cbbDVT);
            this.uiGroupBox2.Controls.Add(this.cbbLoaiHinhBaoCao);
            this.uiGroupBox2.Controls.Add(this.txtSoTN);
            this.uiGroupBox2.Controls.Add(this.txtGhiChuKhac);
            this.uiGroupBox2.Controls.Add(this.txtNamQT);
            this.uiGroupBox2.Controls.Add(this.lblTrangThaiXuLy);
            this.uiGroupBox2.Controls.Add(this.label17);
            this.uiGroupBox2.Controls.Add(this.label18);
            this.uiGroupBox2.Controls.Add(this.label19);
            this.uiGroupBox2.Controls.Add(this.label20);
            this.uiGroupBox2.Controls.Add(this.label21);
            this.uiGroupBox2.Controls.Add(this.label22);
            this.uiGroupBox2.Controls.Add(this.label23);
            this.uiGroupBox2.Controls.Add(this.label24);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.ForeColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1090, 166);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(131, 56);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(412, 22);
            this.donViHaiQuanControl1.TabIndex = 3;
            this.donViHaiQuanControl1.VisualStyleManager = null;
            // 
            // dtpNgayTN
            // 
            this.dtpNgayTN.Appearance.BackColor = System.Drawing.SystemColors.ControlText;
            this.dtpNgayTN.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dtpNgayTN.Appearance.Options.UseBackColor = true;
            this.dtpNgayTN.Appearance.Options.UseForeColor = true;
            this.dtpNgayTN.Location = new System.Drawing.Point(404, 25);
            this.dtpNgayTN.Name = "dtpNgayTN";
            this.dtpNgayTN.ReadOnly = false;
            this.dtpNgayTN.Size = new System.Drawing.Size(127, 21);
            this.dtpNgayTN.TabIndex = 2;
            this.dtpNgayTN.TagName = "";
            this.dtpNgayTN.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpNgayTN.WhereCondition = "";
            // 
            // cbbDVT
            // 
            this.cbbDVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbDVT.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbDVT.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Trị giá";
            uiComboBoxItem7.Value = "1";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Số lượng";
            uiComboBoxItem8.Value = "2";
            this.cbbDVT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem7,
            uiComboBoxItem8});
            this.cbbDVT.Location = new System.Drawing.Point(702, 89);
            this.cbbDVT.Name = "cbbDVT";
            this.cbbDVT.Size = new System.Drawing.Size(138, 21);
            this.cbbDVT.TabIndex = 6;
            this.cbbDVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbLoaiHinhBaoCao
            // 
            this.cbbLoaiHinhBaoCao.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiHinhBaoCao.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbLoaiHinhBaoCao.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Sản xuất xuất khẩu";
            uiComboBoxItem9.Value = "2";
            this.cbbLoaiHinhBaoCao.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem9});
            this.cbbLoaiHinhBaoCao.Location = new System.Drawing.Point(404, 89);
            this.cbbLoaiHinhBaoCao.Name = "cbbLoaiHinhBaoCao";
            this.cbbLoaiHinhBaoCao.Size = new System.Drawing.Size(127, 21);
            this.cbbLoaiHinhBaoCao.TabIndex = 5;
            this.cbbLoaiHinhBaoCao.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtSoTN
            // 
            this.txtSoTN.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoTN.Location = new System.Drawing.Point(131, 25);
            this.txtSoTN.Name = "txtSoTN";
            this.txtSoTN.Size = new System.Drawing.Size(129, 21);
            this.txtSoTN.TabIndex = 1;
            this.txtSoTN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChuKhac
            // 
            this.txtGhiChuKhac.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtGhiChuKhac.Location = new System.Drawing.Point(131, 128);
            this.txtGhiChuKhac.Name = "txtGhiChuKhac";
            this.txtGhiChuKhac.Size = new System.Drawing.Size(917, 21);
            this.txtGhiChuKhac.TabIndex = 7;
            this.txtGhiChuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNamQT
            // 
            this.txtNamQT.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtNamQT.Location = new System.Drawing.Point(131, 89);
            this.txtNamQT.Name = "txtNamQT";
            this.txtNamQT.Size = new System.Drawing.Size(129, 21);
            this.txtNamQT.TabIndex = 4;
            this.txtNamQT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblTrangThaiXuLy
            // 
            this.lblTrangThaiXuLy.AutoSize = true;
            this.lblTrangThaiXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThaiXuLy.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThaiXuLy.Location = new System.Drawing.Point(699, 29);
            this.lblTrangThaiXuLy.Name = "lblTrangThaiXuLy";
            this.lblTrangThaiXuLy.Size = new System.Drawing.Size(87, 13);
            this.lblTrangThaiXuLy.TabIndex = 0;
            this.lblTrangThaiXuLy.Text = "Chưa khai báo";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label17.Location = new System.Drawing.Point(578, 29);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Trạng thái : ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(288, 29);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(90, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Ngày tiếp nhận : ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label19.Location = new System.Drawing.Point(578, 93);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(107, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Đơn vị tính báo cáo :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label20.Location = new System.Drawing.Point(288, 93);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(97, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Loại hình báo cáo :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label21.Location = new System.Drawing.Point(22, 93);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(91, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Năm quyết toán :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label22.Location = new System.Drawing.Point(23, 132);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Ghi chú :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label23.Location = new System.Drawing.Point(22, 61);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Hải quan :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label24.Location = new System.Drawing.Point(22, 29);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(74, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Số tiếp nhận :";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuExportBCQT});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(128, 26);
            // 
            // mnuExportBCQT
            // 
            this.mnuExportBCQT.Image = ((System.Drawing.Image)(resources.GetObject("mnuExportBCQT.Image")));
            this.mnuExportBCQT.Name = "mnuExportBCQT";
            this.mnuExportBCQT.Size = new System.Drawing.Size(127, 22);
            this.mnuExportBCQT.Text = "Xuất Excel";
            this.mnuExportBCQT.Click += new System.EventHandler(this.mnuExportBCQT_Click);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem6});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(128, 26);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem6.Image")));
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(127, 22);
            this.toolStripMenuItem6.Text = "Xuất Excel";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.btnAdd);
            this.groupBox2.Controls.Add(this.lblFileName);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.ForeColor = System.Drawing.Color.DarkGreen;
            this.groupBox2.Location = new System.Drawing.Point(0, 166);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1058, 44);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "FILE ĐÍNH KÈM";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(854, 10);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(135, 28);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "THÊM FILE";
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(156, 22);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(0, 13);
            this.lblFileName.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "FILE NAME :";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(103)))));
            this.uiGroupBox1.Controls.Add(this.dateNgayTN);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.txtGhiChu);
            this.uiGroupBox1.Controls.Add(this.lblTrangThai);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.ForeColor = System.Drawing.Color.DarkGreen;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1058, 166);
            this.uiGroupBox1.TabIndex = 4;
            this.uiGroupBox1.Text = "THÔNG TIN CHUNG";
            // 
            // dateNgayTN
            // 
            this.dateNgayTN.Enabled = false;
            this.dateNgayTN.Location = new System.Drawing.Point(391, 25);
            this.dateNgayTN.Name = "dateNgayTN";
            this.dateNgayTN.ReadOnly = false;
            this.dateNgayTN.Size = new System.Drawing.Size(127, 21);
            this.dateNgayTN.TabIndex = 26;
            this.dateNgayTN.TagName = "";
            this.dateNgayTN.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateNgayTN.WhereCondition = "";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Enabled = false;
            this.txtSoTiepNhan.Location = new System.Drawing.Point(119, 25);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(129, 20);
            this.txtSoTiepNhan.TabIndex = 24;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(119, 131);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(968, 20);
            this.txtGhiChu.TabIndex = 20;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(637, 33);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(87, 13);
            this.lblTrangThai.TabIndex = 19;
            this.lblTrangThai.Text = "Chưa khai báo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(578, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Trạng thái: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(288, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Ngày tiếp nhận";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(578, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Đơn vị tính báo cáo :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(288, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Loại hình báo cáo :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Năm quyết toán :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 135);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Ghi chú :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Hải quan";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 17;
            this.label13.Text = "Số tiếp nhận";
            // 
            // btnReadExcel
            // 
            this.btnReadExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnReadExcel.Image")));
            this.btnReadExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnReadExcel.Location = new System.Drawing.Point(758, 13);
            this.btnReadExcel.Name = "btnReadExcel";
            this.btnReadExcel.Size = new System.Drawing.Size(142, 23);
            this.btnReadExcel.TabIndex = 8;
            this.btnReadExcel.Text = "Chọn File đính kèm";
            this.btnReadExcel.VisualStyleManager = this.vsmMain;
            this.btnReadExcel.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnView
            // 
            this.btnView.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.Image = ((System.Drawing.Image)(resources.GetObject("btnView.Image")));
            this.btnView.ImageSize = new System.Drawing.Size(20, 20);
            this.btnView.Location = new System.Drawing.Point(906, 13);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(142, 23);
            this.btnView.TabIndex = 9;
            this.btnView.Text = "Xem File đính kèm";
            this.btnView.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // uiGroupBox17
            // 
            this.uiGroupBox17.AutoScroll = true;
            this.uiGroupBox17.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox17.Controls.Add(this.btnView);
            this.uiGroupBox17.Controls.Add(this.btnReadExcel);
            this.uiGroupBox17.Controls.Add(this.label15);
            this.uiGroupBox17.Controls.Add(this.lblFile);
            this.uiGroupBox17.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox17.Location = new System.Drawing.Point(0, 166);
            this.uiGroupBox17.Name = "uiGroupBox17";
            this.uiGroupBox17.Size = new System.Drawing.Size(1090, 58);
            this.uiGroupBox17.TabIndex = 0;
            this.uiGroupBox17.Text = "Thông tin File đính kèm";
            this.uiGroupBox17.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox17.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Controls.Add(this.txtTrangThaiXuLy);
            this.uiGroupBox3.Controls.Add(this.btnDong);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.btnMau15TriGia);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.btnMau15Luong);
            this.uiGroupBox3.Controls.Add(this.label14);
            this.uiGroupBox3.Controls.Add(this.btnGetDuLieu);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.btnProcess);
            this.uiGroupBox3.Controls.Add(this.dtpTuNgay);
            this.uiGroupBox3.Controls.Add(this.dtpDenNgay);
            this.uiGroupBox3.Controls.Add(this.dtpNgayChotTon);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 639);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1090, 87);
            this.uiGroupBox3.TabIndex = 26;
            this.uiGroupBox3.Text = "Xử lý dữ liệu";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(953, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 13);
            this.label16.TabIndex = 8;
            // 
            // btnDong
            // 
            this.btnDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Image = ((System.Drawing.Image)(resources.GetObject("btnDong.Image")));
            this.btnDong.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDong.Location = new System.Drawing.Point(986, 47);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(85, 23);
            this.btnDong.TabIndex = 3;
            this.btnDong.Text = "Đóng";
            this.btnDong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnMau15TriGia
            // 
            this.btnMau15TriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMau15TriGia.Image = ((System.Drawing.Image)(resources.GetObject("btnMau15TriGia.Image")));
            this.btnMau15TriGia.ImageSize = new System.Drawing.Size(20, 20);
            this.btnMau15TriGia.Location = new System.Drawing.Point(707, 47);
            this.btnMau15TriGia.Name = "btnMau15TriGia";
            this.btnMau15TriGia.Size = new System.Drawing.Size(135, 23);
            this.btnMau15TriGia.TabIndex = 3;
            this.btnMau15TriGia.Text = "Xem báo cáo (Trị giá)";
            this.btnMau15TriGia.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnMau15TriGia.Click += new System.EventHandler(this.btnMau15TriGia_Click);
            // 
            // btnMau15Luong
            // 
            this.btnMau15Luong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMau15Luong.Image = ((System.Drawing.Image)(resources.GetObject("btnMau15Luong.Image")));
            this.btnMau15Luong.ImageSize = new System.Drawing.Size(20, 20);
            this.btnMau15Luong.Location = new System.Drawing.Point(707, 18);
            this.btnMau15Luong.Name = "btnMau15Luong";
            this.btnMau15Luong.Size = new System.Drawing.Size(135, 23);
            this.btnMau15Luong.TabIndex = 3;
            this.btnMau15Luong.Text = "Xem báo cáo (Lượng)";
            this.btnMau15Luong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnMau15Luong.Click += new System.EventHandler(this.btnMau15Luong_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(18, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "Trạng thái xử lý :";
            // 
            // btnGetDuLieu
            // 
            this.btnGetDuLieu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetDuLieu.Image = ((System.Drawing.Image)(resources.GetObject("btnGetDuLieu.Image")));
            this.btnGetDuLieu.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGetDuLieu.Location = new System.Drawing.Point(566, 47);
            this.btnGetDuLieu.Name = "btnGetDuLieu";
            this.btnGetDuLieu.Size = new System.Drawing.Size(135, 23);
            this.btnGetDuLieu.TabIndex = 3;
            this.btnGetDuLieu.Text = "Lấy dữ liệu";
            this.btnGetDuLieu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGetDuLieu.Click += new System.EventHandler(this.btnGetDuLieu_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.Image = ((System.Drawing.Image)(resources.GetObject("btnProcess.Image")));
            this.btnProcess.ImageSize = new System.Drawing.Size(20, 20);
            this.btnProcess.Location = new System.Drawing.Point(566, 18);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(135, 23);
            this.btnProcess.TabIndex = 3;
            this.btnProcess.Text = "Lấy dữ liệu";
            this.btnProcess.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // uiTab1
            // 
            this.uiTab1.ContextMenuStrip = this.contextMenuStrip1;
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 224);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(1090, 415);
            this.uiTab1.TabIndex = 27;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage3,
            this.uiTabPage2,
            this.uiTabPage4,
            this.uiTabPage6,
            this.uiTabPage5,
            this.uiTabPage8,
            this.uiTabPage7});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.dgList);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1088, 393);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.TabVisible = false;
            this.uiTabPage1.Text = "CHI TIẾT THEO TỜ KHAI";
            // 
            // dgList
            // 
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ContextMenuStrip = this.mnuGridChiTietNPL;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 5;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(1088, 393);
            this.dgList.TabIndex = 1;
            this.dgList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.dgTongNPL);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1058, 165);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.TabVisible = false;
            this.uiTabPage3.Text = "TỔNG NGUYÊN PHỤ LIỆU";
            // 
            // dgTongNPL
            // 
            this.dgTongNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTongNPL.ContextMenuStrip = this.mnuGridChiTietNPL;
            dgTongNPL_DesignTimeLayout.LayoutString = resources.GetString("dgTongNPL_DesignTimeLayout.LayoutString");
            this.dgTongNPL.DesignTimeLayout = dgTongNPL_DesignTimeLayout;
            this.dgTongNPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTongNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTongNPL.FrozenColumns = 4;
            this.dgTongNPL.GroupByBoxVisible = false;
            this.dgTongNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTongNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTongNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTongNPL.Location = new System.Drawing.Point(0, 0);
            this.dgTongNPL.Name = "dgTongNPL";
            this.dgTongNPL.RecordNavigator = true;
            this.dgTongNPL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTongNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTongNPL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTongNPL.Size = new System.Drawing.Size(1058, 165);
            this.dgTongNPL.TabIndex = 2;
            this.dgTongNPL.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTongNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTongNPL.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.dgChiTietNPLXuat);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1058, 165);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.TabVisible = false;
            this.uiTabPage2.Text = "DANH SÁCH NGUYÊN PHỤ LIỆU XUẤT THEO ĐỊNH MỨC SẢN PHẨM";
            // 
            // dgChiTietNPLXuat
            // 
            this.dgChiTietNPLXuat.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgChiTietNPLXuat.ContextMenuStrip = this.mnnTongNPL;
            dgChiTietNPLXuat_DesignTimeLayout.LayoutString = resources.GetString("dgChiTietNPLXuat_DesignTimeLayout.LayoutString");
            this.dgChiTietNPLXuat.DesignTimeLayout = dgChiTietNPLXuat_DesignTimeLayout;
            this.dgChiTietNPLXuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgChiTietNPLXuat.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgChiTietNPLXuat.FrozenColumns = 5;
            this.dgChiTietNPLXuat.GroupByBoxVisible = false;
            this.dgChiTietNPLXuat.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChiTietNPLXuat.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChiTietNPLXuat.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgChiTietNPLXuat.Location = new System.Drawing.Point(0, 0);
            this.dgChiTietNPLXuat.Name = "dgChiTietNPLXuat";
            this.dgChiTietNPLXuat.RecordNavigator = true;
            this.dgChiTietNPLXuat.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowPosition;
            this.dgChiTietNPLXuat.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgChiTietNPLXuat.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgChiTietNPLXuat.Size = new System.Drawing.Size(1058, 165);
            this.dgChiTietNPLXuat.TabIndex = 2;
            this.dgChiTietNPLXuat.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgChiTietNPLXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgChiTietNPLXuat.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.dgTongNPLXuat);
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(1058, 165);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.TabVisible = false;
            this.uiTabPage4.Text = "TỔNG NGUYÊN PHỤ LIỆU XUẤT";
            // 
            // dgTongNPLXuat
            // 
            this.dgTongNPLXuat.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTongNPLXuat.ColumnAutoResize = true;
            this.dgTongNPLXuat.ContextMenuStrip = this.mnnTongNPL;
            dgTongNPLXuat_DesignTimeLayout.LayoutString = resources.GetString("dgTongNPLXuat_DesignTimeLayout.LayoutString");
            this.dgTongNPLXuat.DesignTimeLayout = dgTongNPLXuat_DesignTimeLayout;
            this.dgTongNPLXuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTongNPLXuat.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTongNPLXuat.FrozenColumns = 5;
            this.dgTongNPLXuat.GroupByBoxVisible = false;
            this.dgTongNPLXuat.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTongNPLXuat.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTongNPLXuat.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTongNPLXuat.Location = new System.Drawing.Point(0, 0);
            this.dgTongNPLXuat.Name = "dgTongNPLXuat";
            this.dgTongNPLXuat.RecordNavigator = true;
            this.dgTongNPLXuat.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowPosition;
            this.dgTongNPLXuat.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTongNPLXuat.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTongNPLXuat.Size = new System.Drawing.Size(1058, 165);
            this.dgTongNPLXuat.TabIndex = 3;
            this.dgTongNPLXuat.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTongNPLXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTongNPLXuat.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage6
            // 
            this.uiTabPage6.Controls.Add(this.dgNPLCU);
            this.uiTabPage6.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage6.Name = "uiTabPage6";
            this.uiTabPage6.Size = new System.Drawing.Size(1058, 165);
            this.uiTabPage6.TabStop = true;
            this.uiTabPage6.TabVisible = false;
            this.uiTabPage6.Text = "NGUYÊN PHỤ LIỆU CUNG ỨNG";
            // 
            // dgNPLCU
            // 
            this.dgNPLCU.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNPLCU.ColumnAutoResize = true;
            this.dgNPLCU.ContextMenuStrip = this.mnnTongNPL;
            dgNPLCU_DesignTimeLayout.LayoutString = resources.GetString("dgNPLCU_DesignTimeLayout.LayoutString");
            this.dgNPLCU.DesignTimeLayout = dgNPLCU_DesignTimeLayout;
            this.dgNPLCU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNPLCU.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNPLCU.FrozenColumns = 5;
            this.dgNPLCU.GroupByBoxVisible = false;
            this.dgNPLCU.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCU.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCU.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNPLCU.Location = new System.Drawing.Point(0, 0);
            this.dgNPLCU.Name = "dgNPLCU";
            this.dgNPLCU.RecordNavigator = true;
            this.dgNPLCU.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNPLCU.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLCU.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNPLCU.Size = new System.Drawing.Size(1058, 165);
            this.dgNPLCU.TabIndex = 5;
            this.dgNPLCU.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLCU.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgNPLCU.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage5
            // 
            this.uiTabPage5.Controls.Add(this.dgNPLCU_Total);
            this.uiTabPage5.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage5.Name = "uiTabPage5";
            this.uiTabPage5.Size = new System.Drawing.Size(1058, 165);
            this.uiTabPage5.TabStop = true;
            this.uiTabPage5.TabVisible = false;
            this.uiTabPage5.Text = "TỔNG NPL CUNG ỨNG";
            // 
            // dgNPLCU_Total
            // 
            this.dgNPLCU_Total.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNPLCU_Total.ColumnAutoResize = true;
            this.dgNPLCU_Total.ContextMenuStrip = this.mnnTongNPL;
            dgNPLCU_Total_DesignTimeLayout.LayoutString = resources.GetString("dgNPLCU_Total_DesignTimeLayout.LayoutString");
            this.dgNPLCU_Total.DesignTimeLayout = dgNPLCU_Total_DesignTimeLayout;
            this.dgNPLCU_Total.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNPLCU_Total.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNPLCU_Total.FrozenColumns = 5;
            this.dgNPLCU_Total.GroupByBoxVisible = false;
            this.dgNPLCU_Total.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCU_Total.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCU_Total.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNPLCU_Total.Location = new System.Drawing.Point(0, 0);
            this.dgNPLCU_Total.Name = "dgNPLCU_Total";
            this.dgNPLCU_Total.RecordNavigator = true;
            this.dgNPLCU_Total.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLCU_Total.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNPLCU_Total.Size = new System.Drawing.Size(1058, 165);
            this.dgNPLCU_Total.TabIndex = 4;
            this.dgNPLCU_Total.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLCU_Total.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgNPLCU_Total.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage8
            // 
            this.uiTabPage8.Controls.Add(this.dgListNPLXuatDetails);
            this.uiTabPage8.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage8.Name = "uiTabPage8";
            this.uiTabPage8.Size = new System.Drawing.Size(1088, 393);
            this.uiTabPage8.TabStop = true;
            this.uiTabPage8.Text = "CHI TIẾT NGUYÊN PHỤ LIỆU XUẤT THEO THANH KHOẢN (SỐ LIỆU THEO DÕI KHÔNG GỬI LÊN HQ" +
                ")";
            // 
            // dgListNPLXuatDetails
            // 
            this.dgListNPLXuatDetails.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPLXuatDetails.AlternatingColors = true;
            this.dgListNPLXuatDetails.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPLXuatDetails.ColumnAutoResize = true;
            this.dgListNPLXuatDetails.ContextMenuStrip = this.contextMenuStrip2;
            dgListNPLXuatDetails_DesignTimeLayout.LayoutString = resources.GetString("dgListNPLXuatDetails_DesignTimeLayout.LayoutString");
            this.dgListNPLXuatDetails.DesignTimeLayout = dgListNPLXuatDetails_DesignTimeLayout;
            this.dgListNPLXuatDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPLXuatDetails.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPLXuatDetails.FrozenColumns = 3;
            this.dgListNPLXuatDetails.GroupByBoxVisible = false;
            this.dgListNPLXuatDetails.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLXuatDetails.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLXuatDetails.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPLXuatDetails.Location = new System.Drawing.Point(0, 0);
            this.dgListNPLXuatDetails.Margin = new System.Windows.Forms.Padding(0);
            this.dgListNPLXuatDetails.Name = "dgListNPLXuatDetails";
            this.dgListNPLXuatDetails.RecordNavigator = true;
            this.dgListNPLXuatDetails.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNPLXuatDetails.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPLXuatDetails.Size = new System.Drawing.Size(1088, 393);
            this.dgListNPLXuatDetails.TabIndex = 12;
            this.dgListNPLXuatDetails.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiTabPage7
            // 
            this.uiTabPage7.Controls.Add(this.dgListTotal);
            this.uiTabPage7.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage7.Name = "uiTabPage7";
            this.uiTabPage7.Size = new System.Drawing.Size(1088, 393);
            this.uiTabPage7.TabStop = true;
            this.uiTabPage7.Text = "TỔNG HỢP SỐ LIỆU BÁO CÁO (SỐ LIỆU GỬI LÊN HQ)";
            // 
            // dgListTotal
            // 
            this.dgListTotal.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTotal.AlternatingColors = true;
            this.dgListTotal.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTotal.ColumnAutoResize = true;
            this.dgListTotal.ContextMenuStrip = this.contextMenuStrip1;
            dgListTotal_DesignTimeLayout.LayoutString = resources.GetString("dgListTotal_DesignTimeLayout.LayoutString");
            this.dgListTotal.DesignTimeLayout = dgListTotal_DesignTimeLayout;
            this.dgListTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTotal.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTotal.FrozenColumns = 3;
            this.dgListTotal.GroupByBoxVisible = false;
            this.dgListTotal.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTotal.Location = new System.Drawing.Point(0, 0);
            this.dgListTotal.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTotal.Name = "dgListTotal";
            this.dgListTotal.RecordNavigator = true;
            this.dgListTotal.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTotal.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTotal.Size = new System.Drawing.Size(1088, 393);
            this.dgListTotal.TabIndex = 11;
            this.dgListTotal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // Mau15ChiTietForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1090, 754);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "Mau15ChiTietForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Khai báo báo cáo quyết toán ";
            this.Load += new System.EventHandler(this.XuatNhapTonForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.mnuGridChiTietNPL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).EndInit();
            this.mnnTongNPL.ResumeLayout(false);
            this.mnuNPLTheoDM.ResumeLayout(false);
            this.mnuTongNPLXuat.ResumeLayout(false);
            this.mnuNPL_CU.ResumeLayout(false);
            this.mnuTongNPLCU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).EndInit();
            this.uiGroupBox17.ResumeLayout(false);
            this.uiGroupBox17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTongNPL)).EndInit();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgChiTietNPLXuat)).EndInit();
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTongNPLXuat)).EndInit();
            this.uiTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCU)).EndInit();
            this.uiTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCU_Total)).EndInit();
            this.uiTabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLXuatDetails)).EndInit();
            this.uiTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintDocument1;
        private System.Windows.Forms.ContextMenuStrip mnuGridChiTietNPL;
        private System.Windows.Forms.ToolStripMenuItem toolStripChiTietNPL;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenChuHang2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnTimKiemNPL;
        private Janus.Windows.GridEX.GridEX dgListNPL;
        private System.Windows.Forms.ImageList imageList1;
        private System.ComponentModel.BackgroundWorker backgroundHangTon;
        private System.Windows.Forms.DateTimePicker dtpDenNgay;
        private System.Windows.Forms.DateTimePicker dtpTuNgay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip mnnTongNPL;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Label txtTrangThaiXuLy;
        private System.Windows.Forms.DateTimePicker dtpNgayChotTon;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ContextMenuStrip mnuNPLTheoDM;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ContextMenuStrip mnuTongNPLXuat;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ContextMenuStrip mnuNPL_CU;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ContextMenuStrip mnuTongNPLCU;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dtpNgayTN;
        private Janus.Windows.EditControls.UIComboBox cbbDVT;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHinhBaoCao;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTN;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuKhac;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamQT;
        private System.Windows.Forms.Label lblTrangThaiXuLy;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayTN;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuExportBCQT;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private Janus.Windows.UI.CommandBars.UICommand cmdImportExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdImportExcel;
        private Janus.Windows.EditControls.UIButton btnView;
        private Janus.Windows.EditControls.UIButton btnReadExcel;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox17;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.EditControls.UIButton btnDong;
        private Janus.Windows.EditControls.UIButton btnMau15TriGia;
        private Janus.Windows.EditControls.UIButton btnMau15Luong;
        private Janus.Windows.EditControls.UIButton btnGetDuLieu;
        private Janus.Windows.EditControls.UIButton btnProcess;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.GridEX.GridEX dgTongNPL;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.GridEX.GridEX dgChiTietNPLXuat;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.GridEX.GridEX dgTongNPLXuat;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage6;
        private Janus.Windows.GridEX.GridEX dgNPLCU;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage5;
        private Janus.Windows.GridEX.GridEX dgNPLCU_Total;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage8;
        private Janus.Windows.GridEX.GridEX dgListNPLXuatDetails;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage7;
        private Janus.Windows.GridEX.GridEX dgListTotal;
        private System.Windows.Forms.Label label16;

    }
}