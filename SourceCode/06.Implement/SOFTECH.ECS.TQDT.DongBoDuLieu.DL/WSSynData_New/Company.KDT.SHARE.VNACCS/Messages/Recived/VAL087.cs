﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAL0870 : IVA
    {
        public static int TongSoByte { get { return 3070; } }
        public VAL0870()
        {
        }
        public VAL0870(string strResult)
        {
            try
            {
                this.GetObject<VAL0870>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAL0870, false,VAL0870.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAL0870.TongSoByte);
                while (true)
                {
                    IVA_HANGHOA ivaHang = new IVA_HANGHOA();
                    ivaHang.GetObject<IVA_HANGHOA>(strResult, true, GlobalVNACC.PathConfig, "IVA_HANGHOA", false, IVA_HANGHOA.TotalByte);
                    if (this.HangHoaTrongHoaDon == null) this.HangHoaTrongHoaDon = new List<IVA_HANGHOA>();
                    this.HangHoaTrongHoaDon.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > IVA_HANGHOA.TotalByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - IVA_HANGHOA.TotalByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, IVA_HANGHOA.TotalByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
          

            
        }
        public void LoadVAL0870(string strResult)
        {
            try
            {
                this.GetObject<VAL0870>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAL0870, false, VAL0870.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAL0870.TongSoByte);
                while (true)
                {
                    IVA_HANGHOA ivaHang = new IVA_HANGHOA();
                    ivaHang.GetObject<IVA_HANGHOA>(strResult, true, GlobalVNACC.PathConfig, "IVA_HANGHOA", false, IVA_HANGHOA.TotalByte);
                    if (this.HangHoaTrongHoaDon == null) this.HangHoaTrongHoaDon = new List<IVA_HANGHOA>();
                    this.HangHoaTrongHoaDon.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > IVA_HANGHOA.TotalByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - IVA_HANGHOA.TotalByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, IVA_HANGHOA.TotalByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
