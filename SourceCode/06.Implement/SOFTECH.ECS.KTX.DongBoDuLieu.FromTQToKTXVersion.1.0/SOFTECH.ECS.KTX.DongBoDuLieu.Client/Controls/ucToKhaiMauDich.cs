﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DongBoDuLieu.BLL.SXXK;
using System.IO;

namespace DongBo.Controls
{
    public partial class ucToKhaiMauDich : UserControl
    {
        public ucToKhaiMauDich()
        {
            InitializeComponent();
        }
        DataTable dtTKMD = new DataTable();
        private string condition = string.Empty;
        private void ucToKhaiMauDich_Load(object sender, EventArgs e)
        {
            loadData();
        }
        private void loadData()
        {
            if (GlobalSettings.dsTKMDHQ.Tables.Count > 0)
            {
                dgToKhai.DataSource = GlobalSettings.dsTKMDHQ.Tables["ToKhaiMauDich"];
                dgVanDon.DataSource = GlobalSettings.dsTKMDHQ.Tables["VanDon"];
                dgHoaDon.DataSource = GlobalSettings.dsTKMDHQ.Tables["HoaDon"];
                dgHopDong.DataSource = GlobalSettings.dsTKMDHQ.Tables["HopDong"];
                dgGiayPhep.DataSource = GlobalSettings.dsTKMDHQ.Tables["GiayPhep"];
                dgCo.DataSource = GlobalSettings.dsTKMDHQ.Tables["CO"];
                dgChungTuAnh.DataSource = GlobalSettings.dsTKMDHQ.Tables["ChungTuAnh"];
            }
            string MaLoaiHinh = GlobalSettings.Phanhe == 1 ? "SX" : "GC";
            condition = string.Format(@"  Where Temp_DToKhaiMD.SoTKChinhThuc is not null AND Temp_DToKhaiMD.NgayDKChinhThuc > '{0} 00:00:00' and Temp_DToKhaiMD.NgayDKChinhThuc < '{1} 00:00:00' 
                 AND Temp_DToKhaiMD.MA_HQ = '{2}' AND Temp_DToKhaiMD.MA_DV = '{3}' AND Temp_DToKhaiMD.MA_LH like '%{4}%' ", GlobalSettings.NgayBatDau.ToString("yyyy-MM-dd"), GlobalSettings.NgayKetThuc.ToString("yyyy-MM-dd"), GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, MaLoaiHinh);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //DataSet ds = TK.getToKhaiTQDTFromHQ(txtQuerry.Text, "", "", DateTime.Now, DateTime.Now);
            ////dtTKMD = TK.getToKhaiTQDTFromHQ(txtQuerry.Text,"", "", DateTime.Now, DateTime.Now).Tables[0];
            //grQuerry.DataSource = ds.Tables[0].Copy();   
            frmProcess f = new frmProcess();
            f.Condition = this.condition;
            f.TrangThaiGetDuLieu = DongBo.frmProcess.LoaiDuLieu.ToKhai;
            f.ShowDialog(this);
            string result = f.result;

            if (!string.IsNullOrEmpty(result))
            {
                DataSet ds = new DataSet();
                StringReader reader = new StringReader(f.result);
                //DataTable dt = new DataTable();
                // dt.ReadXml(reader);
                ds.ReadXml(reader, XmlReadMode.ReadSchema);
                DataTable dt = ds.Tables[0].Copy();
                dt.TableName = "ToKhaiMauDich";
                if (GlobalSettings.dsTKMDHQ.Tables["ToKhaiMauDich"] != null)
                    GlobalSettings.dsTKMDHQ.Tables.Remove("ToKhaiMauDich");
                GlobalSettings.dsTKMDHQ.Tables.Add(dt);
                dgToKhai.DataSource = GlobalSettings.dsTKMDHQ.Tables["ToKhaiMauDich"];
            }
        }

        private void GetTTChungTu_Click(object sender, EventArgs e)
        {
            frmProcess f = new frmProcess();
            string result = string.Empty;
            DataSet ds = new DataSet();
            #region Get thông tin vận đơn
            f = new frmProcess();
            f.Condition = this.condition;
            f.TrangThaiGetDuLieu = DongBo.frmProcess.LoaiDuLieu.VanDon;
            f.ShowDialog(this);
            result = f.result;
            if (!string.IsNullOrEmpty(result))
            {
                ds = new DataSet();
                StringReader reader = new StringReader(f.result);
                //DataTable dt = new DataTable();
                // dt.ReadXml(reader);
                ds.ReadXml(reader, XmlReadMode.ReadSchema);
                DataTable dt = ds.Tables[0].Copy();
                dt.TableName = "VanDon";
                if (GlobalSettings.dsTKMDHQ.Tables["VanDon"] != null)
                    GlobalSettings.dsTKMDHQ.Tables.Remove("VanDon");
                GlobalSettings.dsTKMDHQ.Tables.Add(dt);
                dgVanDon.DataSource = GlobalSettings.dsTKMDHQ.Tables["VanDon"];
            }

            #region Container
            f = new frmProcess();
            f.Condition = this.condition;
            f.TrangThaiGetDuLieu = DongBo.frmProcess.LoaiDuLieu.VanDonCT;
            f.ShowDialog(this);
            result = f.result;
            if (!string.IsNullOrEmpty(result))
            {
                ds = new DataSet();
                StringReader reader = new StringReader(f.result);
                //DataTable dt = new DataTable();
                // dt.ReadXml(reader);
                ds.ReadXml(reader, XmlReadMode.ReadSchema);
                DataTable dt = ds.Tables[0].Copy();
                dt.TableName = "VanDonCT";
                if (GlobalSettings.dsTKMDHQ.Tables["VanDonCT"] != null)
                    GlobalSettings.dsTKMDHQ.Tables.Remove("VanDonCT");
                GlobalSettings.dsTKMDHQ.Tables.Add(dt);
                //dgVanDon.DataSource = GlobalSettings.dsTKMDHQ.Tables["VanDon"];
            }
#endregion
            #endregion

            #region Get thông tin hóa đơn thương mại
            f = new frmProcess();
            f.Condition = this.condition;
            f.TrangThaiGetDuLieu = DongBo.frmProcess.LoaiDuLieu.HoaDonThuongMai;
            f.ShowDialog(this);
            result = f.result;
            if (!string.IsNullOrEmpty(result))
            {
                ds = new DataSet();
                StringReader reader = new StringReader(f.result);
                //DataTable dt = new DataTable();
                // dt.ReadXml(reader);
                ds.ReadXml(reader, XmlReadMode.ReadSchema);
                DataTable dt = ds.Tables[0].Copy();
                dt.TableName = "HoaDon";
                if (GlobalSettings.dsTKMDHQ.Tables["HoaDon"] != null)
                    GlobalSettings.dsTKMDHQ.Tables.Remove("HoaDon");
                GlobalSettings.dsTKMDHQ.Tables.Add(dt);
                dgHoaDon.DataSource = GlobalSettings.dsTKMDHQ.Tables["HoaDon"];
            }
            #endregion

            #region Get thông tin Hợp đồng thương mại
            f = new frmProcess();
            f.Condition = this.condition;
            f.TrangThaiGetDuLieu = DongBo.frmProcess.LoaiDuLieu.HopDongThuongMai;
            f.ShowDialog(this);
            result = f.result;
            if (!string.IsNullOrEmpty(result))
            {
                ds = new DataSet();
                StringReader reader = new StringReader(f.result);
                //DataTable dt = new DataTable();
                // dt.ReadXml(reader);
                ds.ReadXml(reader, XmlReadMode.ReadSchema);
                DataTable dt = ds.Tables[0].Copy();
                dt.TableName = "HopDong";
                if (GlobalSettings.dsTKMDHQ.Tables["HopDong"] != null)
                    GlobalSettings.dsTKMDHQ.Tables.Remove("HopDong");
                GlobalSettings.dsTKMDHQ.Tables.Add(dt);
                dgHopDong.DataSource = GlobalSettings.dsTKMDHQ.Tables["HopDong"];
            }
            #endregion

            #region Get thông tin Giấy phép
            f = new frmProcess();
            f.Condition = this.condition;
            f.TrangThaiGetDuLieu = DongBo.frmProcess.LoaiDuLieu.GiayPhep;
            f.ShowDialog(this);
            result = f.result;
            if (!string.IsNullOrEmpty(result))
            {
                ds = new DataSet();
                StringReader reader = new StringReader(f.result);
                //DataTable dt = new DataTable();
                // dt.ReadXml(reader);
                ds.ReadXml(reader, XmlReadMode.ReadSchema);
                DataTable dt = ds.Tables[0].Copy();
                dt.TableName = "GiayPhep";
                if (GlobalSettings.dsTKMDHQ.Tables["GiayPhep"] != null)
                    GlobalSettings.dsTKMDHQ.Tables.Remove("GiayPhep");
                GlobalSettings.dsTKMDHQ.Tables.Add(dt);
                dgGiayPhep.DataSource = GlobalSettings.dsTKMDHQ.Tables["GiayPhep"];
            }
            #endregion

            #region Get thông tin CO
            f = new frmProcess();
            f.Condition = this.condition;
            f.TrangThaiGetDuLieu = DongBo.frmProcess.LoaiDuLieu.CO;
            f.ShowDialog(this);
            result = f.result;
            if (!string.IsNullOrEmpty(result))
            {
                ds = new DataSet();
                StringReader reader = new StringReader(f.result);
                //DataTable dt = new DataTable();
                // dt.ReadXml(reader);
                ds.ReadXml(reader, XmlReadMode.ReadSchema);
                DataTable dt = ds.Tables[0].Copy();
                dt.TableName = "CO";
                if (GlobalSettings.dsTKMDHQ.Tables["CO"] != null)
                    GlobalSettings.dsTKMDHQ.Tables.Remove("CO");
                GlobalSettings.dsTKMDHQ.Tables.Add(dt);
                dgCo.DataSource = GlobalSettings.dsTKMDHQ.Tables["CO"];
            }
            #endregion

            #region Get thông tin Chứng từ dạng ảnh
            f = new frmProcess();
            f.Condition = this.condition;
            f.TrangThaiGetDuLieu = DongBo.frmProcess.LoaiDuLieu.ChungTuDangAnh;
            f.ShowDialog(this);
            result = f.result;
            if (!string.IsNullOrEmpty(result))
            {
                ds = new DataSet();
                StringReader reader = new StringReader(f.result);
                //DataTable dt = new DataTable();
                // dt.ReadXml(reader);
                ds.ReadXml(reader, XmlReadMode.ReadSchema);
                DataTable dt = ds.Tables[0].Copy();
                dt.TableName = "ChungTuAnh";
                if (GlobalSettings.dsTKMDHQ.Tables["ChungTuAnh"] != null)
                    GlobalSettings.dsTKMDHQ.Tables.Remove("ChungTuAnh");
                GlobalSettings.dsTKMDHQ.Tables.Add(dt);
                dgChungTuAnh.DataSource = GlobalSettings.dsTKMDHQ.Tables["ChungTuAnh"];
            }
            #endregion

            #region Get thông tin Chứng từ chuyển cửa khẩu
            f = new frmProcess();
            f.Condition = this.condition;
            f.TrangThaiGetDuLieu = DongBo.frmProcess.LoaiDuLieu.ChuyenCK;
            f.ShowDialog(this);
            result = f.result;
            if (!string.IsNullOrEmpty(result))
            {
                ds = new DataSet();
                StringReader reader = new StringReader(f.result);
                //DataTable dt = new DataTable();
                // dt.ReadXml(reader);
                ds.ReadXml(reader, XmlReadMode.ReadSchema);
                DataTable dt = ds.Tables[0].Copy();
                dt.TableName = "ChuyenCK";
                if (GlobalSettings.dsTKMDHQ.Tables["ChuyenCK"] != null)
                    GlobalSettings.dsTKMDHQ.Tables.Remove("ChuyenCK");
                GlobalSettings.dsTKMDHQ.Tables.Add(dt);
                dgChuyenCK.DataSource = GlobalSettings.dsTKMDHQ.Tables["ChuyenCK"];
            }
            #endregion

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            InsertUpdateTKMD f = new InsertUpdateTKMD();
            f.ShowDialog(this);
        }
        
    }
}
