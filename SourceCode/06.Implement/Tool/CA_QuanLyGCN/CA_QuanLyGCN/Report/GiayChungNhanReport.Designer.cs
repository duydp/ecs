﻿namespace CA_QuanLyGCN.Report
{
    partial class GiayChungNhanReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();


            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GiayChungNhanReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHoSo1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHoSo2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDoanhNghiep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayChungNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThangChungNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNamChungNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoQuanCapMaDoanhNghiep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTruSo1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTruSo2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNguoDaiDien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblQuocTich = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChucDanh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoCMND_HC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayCMND = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThangCMND = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNamCMND = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoQuanCapCMND = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHoKhau1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHoKhau2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblFullName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoSoKD_Ten = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNganhNgheKD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCoSoKD_DiaChi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgay = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNam = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGhiChu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTieuDe_SoGiayCN = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTieuDe_TenPhongBan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTieuDe_TenCQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.lblTieuDe_SoGiayCN,
            this.lblTieuDe_TenPhongBan,
            this.lblTieuDe_TenCQ,
            this.xrPictureBox1});
            this.Detail.HeightF = 1054.042F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(73.96F, 297F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow8,
            this.xrTableRow7,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow13,
            this.xrTableRow12,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow14});
            this.xrTable1.SizeF = new System.Drawing.SizeF(616.67F, 584.3674F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.lblHoSo1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.79166631693111689;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Weight = 1.0852842620116705;
            // 
            // lblHoSo1
            // 
            this.lblHoSo1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblHoSo1.Name = "lblHoSo1";
            this.lblHoSo1.StylePriority.UseFont = false;
            this.lblHoSo1.StylePriority.UseTextAlignment = false;
            this.lblHoSo1.Text = "lblHoSo1";
            this.lblHoSo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblHoSo1.Weight = 1.9147157379883295;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHoSo2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1.0416662521142184;
            // 
            // lblHoSo2
            // 
            this.lblHoSo2.Name = "lblHoSo2";
            this.lblHoSo2.StylePriority.UseTextAlignment = false;
            this.lblHoSo2.Text = "lblHoSo2";
            this.lblHoSo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblHoSo2.Weight = 3;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.lblMaDoanhNghiep});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 2.37500131229989;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 1.9518397457711068;
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.StylePriority.UseTextAlignment = false;
            this.lblMaDoanhNghiep.Text = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblMaDoanhNghiep.Weight = 1.0481602542288933;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.lblNgayChungNhan,
            this.xrTableCell10,
            this.lblThangChungNhan,
            this.xrTableCell11,
            this.lblNamChungNhan,
            this.xrTableCell12,
            this.lblCoQuanCapMaDoanhNghiep});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1.0416661796460256;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 0.17522848019972437;
            // 
            // lblNgayChungNhan
            // 
            this.lblNgayChungNhan.Name = "lblNgayChungNhan";
            this.lblNgayChungNhan.StylePriority.UseTextAlignment = false;
            this.lblNgayChungNhan.Text = "NN";
            this.lblNgayChungNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblNgayChungNhan.Weight = 0.17203354125691814;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Weight = 0.2001888933816523;
            // 
            // lblThangChungNhan
            // 
            this.lblThangChungNhan.Name = "lblThangChungNhan";
            this.lblThangChungNhan.StylePriority.UseTextAlignment = false;
            this.lblThangChungNhan.Text = "NN";
            this.lblThangChungNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblThangChungNhan.Weight = 0.16471610498441927;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.15893257365424734;
            // 
            // lblNamChungNhan
            // 
            this.lblNamChungNhan.Name = "lblNamChungNhan";
            this.lblNamChungNhan.StylePriority.UseTextAlignment = false;
            this.lblNamChungNhan.Text = "NNNN";
            this.lblNamChungNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblNamChungNhan.Weight = 0.36163398833928634;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Weight = 0.44022338196329391;
            // 
            // lblCoQuanCapMaDoanhNghiep
            // 
            this.lblCoQuanCapMaDoanhNghiep.Name = "lblCoQuanCapMaDoanhNghiep";
            this.lblCoQuanCapMaDoanhNghiep.StylePriority.UseTextAlignment = false;
            this.lblCoQuanCapMaDoanhNghiep.Text = "lblCoQuanCapMaDoanhNghiep";
            this.lblCoQuanCapMaDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblCoQuanCapMaDoanhNghiep.Weight = 1.3270430362204584;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.lblTruSo1});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1.0833337325829686;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Weight = 0.361202415000077;
            // 
            // lblTruSo1
            // 
            this.lblTruSo1.Name = "lblTruSo1";
            this.lblTruSo1.StylePriority.UseTextAlignment = false;
            this.lblTruSo1.Text = "lblTruSo1";
            this.lblTruSo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblTruSo1.Weight = 2.6387975849999235;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTruSo2});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1.1249963455419196;
            // 
            // lblTruSo2
            // 
            this.lblTruSo2.Name = "lblTruSo2";
            this.lblTruSo2.StylePriority.UseTextAlignment = false;
            this.lblTruSo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblTruSo2.Weight = 3.0000000000000004;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.lblNguoDaiDien});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1.1666651191822119;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Weight = 1.3444824822767099;
            // 
            // lblNguoDaiDien
            // 
            this.lblNguoDaiDien.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblNguoDaiDien.Name = "lblNguoDaiDien";
            this.lblNguoDaiDien.StylePriority.UseFont = false;
            this.lblNguoDaiDien.StylePriority.UseTextAlignment = false;
            this.lblNguoDaiDien.Text = "lblNguoDaiDien";
            this.lblNguoDaiDien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblNguoDaiDien.Weight = 1.6555175177232906;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.lblQuocTich,
            this.xrTableCell13,
            this.lblChucDanh});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1.0833299980052884;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.36120238438059632;
            // 
            // lblQuocTich
            // 
            this.lblQuocTich.Name = "lblQuocTich";
            this.lblQuocTich.StylePriority.UseTextAlignment = false;
            this.lblQuocTich.Text = "lblQuocTich";
            this.lblQuocTich.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblQuocTich.Weight = 0.87153119183281136;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Weight = 0.37046840776214252;
            // 
            // lblChucDanh
            // 
            this.lblChucDanh.Name = "lblChucDanh";
            this.lblChucDanh.StylePriority.UseTextAlignment = false;
            this.lblChucDanh.Text = "lblChucDanh";
            this.lblChucDanh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblChucDanh.Weight = 1.3967980160244502;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.lblSoCMND_HC,
            this.xrTableCell9,
            this.lblNgayCMND,
            this.xrTableCell8,
            this.lblThangCMND,
            this.xrTableCell6,
            this.lblNamCMND});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1.1666650428882595;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.56638207554066544;
            // 
            // lblSoCMND_HC
            // 
            this.lblSoCMND_HC.Name = "lblSoCMND_HC";
            this.lblSoCMND_HC.StylePriority.UseTextAlignment = false;
            this.lblSoCMND_HC.Text = "lblSoCMND_HC";
            this.lblSoCMND_HC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblSoCMND_HC.Weight = 0.78082893514849216;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 0.18346970656804973;
            // 
            // lblNgayCMND
            // 
            this.lblNgayCMND.Name = "lblNgayCMND";
            this.lblNgayCMND.StylePriority.UseTextAlignment = false;
            this.lblNgayCMND.Text = "NN";
            this.lblNgayCMND.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblNgayCMND.Weight = 0.17333452077489903;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Weight = 0.20555335626351257;
            // 
            // lblThangCMND
            // 
            this.lblThangCMND.Name = "lblThangCMND";
            this.lblThangCMND.StylePriority.UseTextAlignment = false;
            this.lblThangCMND.Text = "NN";
            this.lblThangCMND.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblThangCMND.Weight = 0.18240205831500533;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 0.14617395905669212;
            // 
            // lblNamCMND
            // 
            this.lblNamCMND.Name = "lblNamCMND";
            this.lblNamCMND.StylePriority.UseTextAlignment = false;
            this.lblNamCMND.Text = "NNNN";
            this.lblNamCMND.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblNamCMND.Weight = 0.76185538833268407;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.lblCoQuanCapCMND});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1.0416625900047314;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 0.45569642857317838;
            // 
            // lblCoQuanCapCMND
            // 
            this.lblCoQuanCapCMND.Name = "lblCoQuanCapCMND";
            this.lblCoQuanCapCMND.StylePriority.UseTextAlignment = false;
            this.lblCoQuanCapCMND.Text = "lblCoQuanCapCMND";
            this.lblCoQuanCapCMND.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblCoQuanCapCMND.Weight = 2.5443035714268221;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.lblHoKhau1});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1.1249976348877027;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 1.3269406175311842;
            // 
            // lblHoKhau1
            // 
            this.lblHoKhau1.Name = "lblHoKhau1";
            this.lblHoKhau1.StylePriority.UseTextAlignment = false;
            this.lblHoKhau1.Text = "WWWWWWWWWWWWWWWWWWWWWWWWW";
            this.lblHoKhau1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblHoKhau1.Weight = 1.6730593824688165;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHoKhau2});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1.0833325996511858;
            // 
            // lblHoKhau2
            // 
            this.lblHoKhau2.Name = "lblHoKhau2";
            this.lblHoKhau2.StylePriority.UseTextAlignment = false;
            this.lblHoKhau2.Text = "lblHoKhau2";
            this.lblHoKhau2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblHoKhau2.Weight = 3.0000000000000009;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.lblFullName,
            this.xrTableCell14});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1.7499929067679132;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 0.25047895745680221;
            // 
            // lblFullName
            // 
            this.lblFullName.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.StylePriority.UseFont = false;
            this.lblFullName.StylePriority.UseTextAlignment = false;
            this.lblFullName.Text = "lblFullName";
            this.lblFullName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblFullName.Weight = 2.5636368893892811;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Weight = 0.18588415315391749;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.lblCoSoKD_Ten,
            this.xrTableCell15});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 3.1666651845643314;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Weight = 0.41694500011200325;
            // 
            // lblCoSoKD_Ten
            // 
            this.lblCoSoKD_Ten.Name = "lblCoSoKD_Ten";
            this.lblCoSoKD_Ten.StylePriority.UseTextAlignment = false;
            this.lblCoSoKD_Ten.Text = "lblCoSoKD_Ten";
            this.lblCoSoKD_Ten.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblCoSoKD_Ten.Weight = 2.244167302444533;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 0.33888769744346431;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.lblNganhNgheKD});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1.0415967514235542;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Weight = 2.0919705213277671;
            // 
            // lblNganhNgheKD
            // 
            this.lblNganhNgheKD.Name = "lblNganhNgheKD";
            this.lblNganhNgheKD.StylePriority.UseTextAlignment = false;
            this.lblNganhNgheKD.Text = "lblNganhNgheKD";
            this.lblNganhNgheKD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblNganhNgheKD.Weight = 0.90802947867223371;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCoSoKD_DiaChi});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1.0415992064210087;
            // 
            // lblCoSoKD_DiaChi
            // 
            this.lblCoSoKD_DiaChi.Name = "lblCoSoKD_DiaChi";
            this.lblCoSoKD_DiaChi.StylePriority.UseTextAlignment = false;
            this.lblCoSoKD_DiaChi.Text = "lblCoSoKD_DiaChi";
            this.lblCoSoKD_DiaChi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblCoSoKD_DiaChi.Weight = 3.0000000000000009;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.lblDiaDiem,
            this.xrTableCell41,
            this.lblNgay,
            this.xrTableCell2,
            this.lblThang,
            this.xrTableCell3,
            this.lblNam});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1.0415943176272813;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Weight = 1.3183909262708491;
            // 
            // lblDiaDiem
            // 
            this.lblDiaDiem.Name = "lblDiaDiem";
            this.lblDiaDiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100F);
            this.lblDiaDiem.StylePriority.UsePadding = false;
            this.lblDiaDiem.StylePriority.UseTextAlignment = false;
            this.lblDiaDiem.Text = "Đà Nẵng";
            this.lblDiaDiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblDiaDiem.Weight = 0.38562411926007367;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 0.21571796717317338;
            // 
            // lblNgay
            // 
            this.lblNgay.Name = "lblNgay";
            this.lblNgay.StylePriority.UseTextAlignment = false;
            this.lblNgay.Text = "NN";
            this.lblNgay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblNgay.Weight = 0.25731786016610653;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 0.18153937440117993;
            // 
            // lblThang
            // 
            this.lblThang.Name = "lblThang";
            this.lblThang.StylePriority.UseTextAlignment = false;
            this.lblThang.Text = "NN";
            this.lblThang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblThang.Weight = 0.18153943007484066;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Weight = 0.16152339489912532;
            // 
            // lblNam
            // 
            this.lblNam.Name = "lblNam";
            this.lblNam.StylePriority.UseTextAlignment = false;
            this.lblNam.Text = "NNNN";
            this.lblNam.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.lblNam.Weight = 0.29834692775465166;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.lblGhiChu2});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1.2082654798601971;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Weight = 1.4147166017647994;
            // 
            // lblGhiChu2
            // 
            this.lblGhiChu2.Name = "lblGhiChu2";
            this.lblGhiChu2.Weight = 1.5852833982352013;
            // 
            // lblTieuDe_SoGiayCN
            // 
            this.lblTieuDe_SoGiayCN.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblTieuDe_SoGiayCN.LocationFloat = new DevExpress.Utils.PointFloat(108.3334F, 84.49834F);
            this.lblTieuDe_SoGiayCN.Name = "lblTieuDe_SoGiayCN";
            this.lblTieuDe_SoGiayCN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTieuDe_SoGiayCN.SizeF = new System.Drawing.SizeF(57.29168F, 16.75F);
            this.lblTieuDe_SoGiayCN.StylePriority.UseFont = false;
            this.lblTieuDe_SoGiayCN.StylePriority.UseTextAlignment = false;
            this.lblTieuDe_SoGiayCN.Text = "xrLabel1";
            this.lblTieuDe_SoGiayCN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTieuDe_TenPhongBan
            // 
            this.lblTieuDe_TenPhongBan.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.lblTieuDe_TenPhongBan.LocationFloat = new DevExpress.Utils.PointFloat(95.83334F, 59.49834F);
            this.lblTieuDe_TenPhongBan.Name = "lblTieuDe_TenPhongBan";
            this.lblTieuDe_TenPhongBan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTieuDe_TenPhongBan.SizeF = new System.Drawing.SizeF(311.1778F, 16.75F);
            this.lblTieuDe_TenPhongBan.StylePriority.UseFont = false;
            this.lblTieuDe_TenPhongBan.Text = "xrLabel1";
            // 
            // lblTieuDe_TenCQ
            // 
            this.lblTieuDe_TenCQ.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.lblTieuDe_TenCQ.LocationFloat = new DevExpress.Utils.PointFloat(96.875F, 37.53999F);
            this.lblTieuDe_TenCQ.Name = "lblTieuDe_TenCQ";
            this.lblTieuDe_TenCQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTieuDe_TenCQ.SizeF = new System.Drawing.SizeF(111.4584F, 16.75F);
            this.lblTieuDe_TenCQ.StylePriority.UseFont = false;
            this.lblTieuDe_TenCQ.Text = "lblTieuDe_TenCQ";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(28.92F, 0F);
            this.xrPictureBox1.LockedInUserDesigner = true;
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(703.08F, 1036.25F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox1.StylePriority.UseBackColor = false;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 62F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 43F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GiayChungNhanReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(21, 73, 62, 43);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "10.2";
            this.PrintProgress += new DevExpress.XtraPrinting.PrintProgressEventHandler(this.GiayChungNhanReport_PrintProgress);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel lblTieuDe_TenPhongBan;
        private DevExpress.XtraReports.UI.XRLabel lblTieuDe_TenCQ;
        private DevExpress.XtraReports.UI.XRLabel lblTieuDe_SoGiayCN;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell lblHoSo1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblHoSo2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDoanhNghiep;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblThangChungNhan;
        private DevExpress.XtraReports.UI.XRTableCell lblNamChungNhan;
        private DevExpress.XtraReports.UI.XRTableCell lblCoQuanCapMaDoanhNghiep;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayChungNhan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell lblTruSo1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lblTruSo2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell lblNguoDaiDien;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell lblQuocTich;
        private DevExpress.XtraReports.UI.XRTableCell lblChucDanh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell lblSoCMND_HC;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayCMND;
        private DevExpress.XtraReports.UI.XRTableCell lblThangCMND;
        private DevExpress.XtraReports.UI.XRTableCell lblNamCMND;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell lblCoQuanCapCMND;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell lblHoKhau1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell lblHoKhau2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell lblNganhNgheKD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell lblCoSoKD_DiaChi;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiem;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell lblNgay;
        private DevExpress.XtraReports.UI.XRTableCell lblThang;
        private DevExpress.XtraReports.UI.XRTableCell lblNam;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChu2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell lblCoSoKD_Ten;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell lblFullName;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
    }
}
