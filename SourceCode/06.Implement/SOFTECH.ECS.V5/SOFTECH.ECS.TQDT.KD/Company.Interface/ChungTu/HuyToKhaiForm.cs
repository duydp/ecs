﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
#elif GC_V3 || GC_V4

using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;

#endif

namespace Company.Interface
{
    public partial class HuyToKhaiForm : BaseForm
    {
        static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        public ToKhaiMauDich TKMD = null;
        private FeedBackContent feedbackContent;
        private string msgInfor = string.Empty;
        public HuyToKhai huyTK = null;

        public HuyToKhaiForm()
        {
            InitializeComponent();
        }

        private void HuyToKhaiForm_Load(object sender, EventArgs e)
        {
            string sfmtWhere = string.Format("TKMD_ID={0}", TKMD.ID);
            List<HuyToKhai> listHuyTK = (List<HuyToKhai>)HuyToKhai.SelectCollectionDynamic(sfmtWhere, "");

            if (listHuyTK.Count > 0)
                huyTK = listHuyTK[0];
            if (huyTK != null)
            {

//                 txtLyDoHuy.Text = huyTK.LyDoHuy;
//                 if (huyTK.SoTiepNhan > 0)
//                 {
//                     txtSoTiepNhan.Text = huyTK.SoTiepNhan.ToString();
//                     ccNgayTiepNhan.Text = huyTK.NgayTiepNhan.ToShortDateString();
//                 }
                SetCommandStatus(huyTK);
                txtLyDoHuy.Text = huyTK.LyDoHuy;
                txtSoTiepNhan.Text = huyTK.SoTiepNhan.ToString("N0");
//                 if (huyTK.TrangThai == TrangThaiXuLy.CHO_HUY)
//                 {
//                     btnKhaiBao.Enabled = false;
//                     btnGhi.Enabled = false;
//                     btnLayPhanHoi.Enabled = true;
//                     lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
//                 }
//                 else if (huyTK.TrangThai == TrangThaiXuLy.DA_HUY)
//                 {
//                     lblTrangThai.Text = TrangThaiXuLy.strDAHUY;
//                 }
//                 else if (huyTK.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO)
//                 {
//                     lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
//                 }
            }
            else
            {
                lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                huyTK = new Company.KDT.SHARE.QuanLyChungTu.HuyToKhai();
                huyTK.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                btnKhaiBao.Enabled = true;
                btnGhi.Enabled = true;
                btnLayPhanHoi.Enabled = false;
            }
            lblSoTK.Text = TKMD.SoToKhai.ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (txtLyDoHuy.Text.Trim() == "")
            {
                ShowMessage("Phải nhập lý do hủy!", false);
                return;
            }
            try
            {
                huyTK.LyDoHuy = txtLyDoHuy.Text.Trim();
                huyTK.TKMD_ID = TKMD.ID;
                TKMD.ActionStatus = (int)ActionStatus.ToKhaiXinHuy;
                huyTK.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                if (huyTK.NgayTiepNhan.Year < 1900)
                    huyTK.NgayTiepNhan = new DateTime(1900, 1, 1);
                if (huyTK.ID == 0)
                {
                    huyTK.Insert();
                }
                else
                    huyTK.Update();
                TKMD.Update();
                ShowMessage("Lưu thành công.", false);
                btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            SendV3();
        }
        private void SetCommandStatus(HuyToKhai huytokhai)
        {
            if (huytokhai.SoTiepNhan > 0)
            {
                txtSoTiepNhan.Text = this.huyTK.SoTiepNhan.ToString("N0");
                ccNgayTiepNhan.Value = this.huyTK.NgayTiepNhan;
                ccNgayTiepNhan.Text = this.huyTK.NgayTiepNhan.ToShortDateString();
            }
            if (huytokhai.NgayTiepNhan.Year > 1900)
                ccNgayTiepNhan.Text = huytokhai.NgayTiepNhan.ToLongDateString();
            if (huytokhai.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                lblTrangThai.Text = "Không phê duyệt";
                btnKhaiBao.Enabled = btnGhi.Enabled = true;
                btnLayPhanHoi.Enabled = false;
            }
            else if (huytokhai.TrangThai == TrangThaiXuLy.CHO_DUYET)
            {
                lblTrangThai.Text = "Chờ duyệt";
                btnKhaiBao.Enabled = btnGhi.Enabled = false;
                btnLayPhanHoi.Enabled = true;
            }
            else if (huytokhai.TrangThai == TrangThaiXuLy.DA_DUYET)
            {
                lblTrangThai.Text = "Đã phê duyệt hủy";
                btnKhaiBao.Enabled = btnGhi.Enabled = false;
                btnLayPhanHoi.Enabled = true;
            }
            else if (huytokhai.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                lblTrangThai.Text = "Chưa khai báo";
                btnKhaiBao.Enabled = btnGhi.Enabled = true;
                btnLayPhanHoi.Enabled = false;
            }
            else if (huytokhai.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
            {
                lblTrangThai.Text = "Đã khai báo và đang chờ phản hồi";
                btnKhaiBao.Enabled = btnGhi.Enabled = false;
                btnLayPhanHoi.Enabled = true;
            }
                

        }
        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            FeedBackV3();
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.TKMD.ID;
            bool isToKhaiNhap = TKMD.MaLoaiHinh.StartsWith("N");
#if KD_V3 || KD_V4
                form.DeclarationIssuer = isToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT;
#elif SXXK_V3 || SXXK_V4
                form.DeclarationIssuer = isToKhaiNhap? DeclarationIssuer.SXXK_TOKHAI_NHAP:DeclarationIssuer.SXXK_TOKHAI_XUAT;
#elif GC_V3

            form.DeclarationIssuer = isToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_NHAP : DeclarationIssuer.GC_TOKHAI_XUAT;
#endif
            form.ShowDialog(this);

        }
        #region Khai báo hủy V3
        private void SendV3()
        {
            if (huyTK.ID == 0)
            {
                ShowMessage("Vui lòng lưu thông tin trước khi khai báo",false);
                return;
            }
            if (huyTK.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO || huyTK.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET)
                huyTK.Guid = Guid.NewGuid().ToString();
            ObjectSend msgSend = SingleMessage.CancelMessageV3(TKMD, huyTK.Guid);
            SendMessageForm dlgSend = new SendMessageForm();
            dlgSend.Send += SendMessage;
            bool isSend = dlgSend.DoSend(msgSend, true);
            if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
            {
                TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                huyTK.TrangThai = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                dlgSend.Message.XmlSaveMessage(TKMD.ID, MessageTitle.HuyToKhai);
                TKMD.Update();
                huyTK.Update();
                btnLayPhanHoi_Click(null, null);
                SetCommandStatus(huyTK);
            }
            else if (isSend && feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
            {
                ShowMessageTQDT(msgInfor, false);
            }

        }
        private void FeedBackV3()
        {
              bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBack(TKMD, huyTK.Guid);
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.SetCommandStatus(huyTK);
                }
                //             ObjectSend msgSend = SingleMessage.FeedBackMessageV3(TKMD);
                //             SendMessageForm dlgSendForm = new SendMessageForm();
                //             dlgSendForm.Send += SendMessage;
                //             dlgSendForm.DoSend(msgSend);
            }

        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {
                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                huyTK.TrangThai = TrangThaiXuLy.KHONG_PHE_DUYET;
                                noidung = "Hải quan từ chối tiếp nhận.\r\n" + noidung;
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
//                             btnKhaiBao.Enabled = false;
//                             btnLayPhanHoi.Enabled = true;
//                             btnGhi.Enabled = false;
//                             lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
                            //this.ShowMessage(noidung, false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {

                                string[] vals = noidung.Split('/');

                                huyTK.SoTiepNhan = long.Parse(feedbackContent.CustomsReference);

                                huyTK.NgayTiepNhan = SingleMessage.GetDate(feedbackContent.Acceptance, feedbackContent.Issue, sfmtDateTime);

                                huyTK.NamTiepNhan = huyTK.NgayTiepNhan.Year;

                                noidung = string.Format("Khai báo hủy tờ khai\r\nSố tiếp nhận: {0} \r\nNgày tiếp nhận: {1}\r\nNăm tiếp nhận: {2}\r\nLoại hình: {3}\r\nHải quan: {4}", new object[] {
                                    huyTK.SoTiepNhan, huyTK.NgayTiepNhan.ToString("dd-MM-yyyy HH:mm:ss"), TKMD.NamDK, TKMD.MaLoaiHinh, TKMD.MaHaiQuan });

                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocCapSoTiepNhan, noidung);
                                huyTK.TrangThai = TrangThaiXuLy.CHO_DUYET;
                                TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
//                                 if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
//                                 {
//                                    // noidung = "Tờ khai được cấp số tiếp nhận\r\n" + noidung;
//                                   //  TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
// 
//                                 }
//                                 else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
//                                 {
//                                     noidung = "Tờ khai được cấp số chờ hủy khai báo\r\n" + noidung;
//                                     TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
//                                 }

//                                 txtSoTiepNhan.Text = this.huyTK.SoTiepNhan.ToString("N0");
//                                 ccNgayTiepNhan.Value = this.huyTK.NgayTiepNhan;
//                                 ccNgayTiepNhan.Text = this.huyTK.NgayTiepNhan.ToShortDateString();
//                                 lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;

                                /*this.ShowMessageTQDT(noidung, false);*/
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {

                                TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                noidung = "Tờ khai được chấp nhận hủy\r\n" + noidung;
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.HuyToKhaiThanhCong, noidung);
                                huyTK.TrangThai = TrangThaiXuLy.DA_DUYET;
                                //this.ShowMessage(noidung, false);
                                //e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocPhanLuong, noidung);
                            }
                            break;
                        default:
                            e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.Error, noidung);
                            break;
                    }
                    if (isUpdate)
                    {
                        TKMD.Update();
                        huyTK.Update();
                        SetCommandStatus(huyTK);
                    }
                    msgInfor = noidung;
                }
                else
                {
                    SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(e.Error));
                    msgInfor = e.Error.Message;
                }
                
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    msgInfor = e.FeedBackMessage;
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    msgInfor = e.Error.Message;
                }
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }
        #endregion
    }
}

