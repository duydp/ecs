using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class ChuyenTrangThaiTK : BaseForm
    {
        ToKhaiMauDich tkmd;
        public ChuyenTrangThaiTK(ToKhaiMauDich tk)
        {
            InitializeComponent();
            //tk.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
            this.tkmd = tk;
            this.Text = this.Text + " " + this.tkmd.ID.ToString();
            this.lblCaption.Text = this.lblCaption.Text + " " + this.tkmd.ID.ToString();
            this.lblMaToKhai.Text = this.tkmd.ID.ToString();
            this.lblMaLoaiHinh.Text = this.tkmd.MaLoaiHinh;
        }

        private void btnThuchien_Click(object sender, EventArgs e)
        {
            if (this.validData())
            {
                this.getData();
                //this.tkmd.LoadHMDCollection();

                ToKhaiMauDich tkDaDangKy = new ToKhaiMauDich();
                tkDaDangKy.MaHaiQuan = tkmd.MaHaiQuan;
                tkDaDangKy.NgayDangKy = tkmd.NgayDangKy;
                tkDaDangKy.MaLoaiHinh = tkmd.MaLoaiHinh;
                tkDaDangKy.SoToKhai = tkmd.SoToKhai;
                tkDaDangKy.PhanLuong = tkmd.PhanLuong;
                if (!tkDaDangKy.isHaveWith4Para())
                {
                    this.tkmd.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                    tkmd.TransgferDataToNPLTonThucTe();

                    #region  Ghi vào Kết quả xử lý

                    //Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();

                    //kqxl.ItemID = this.tkmd.ID;
                    //kqxl.ReferenceID = new Guid(this.tkmd.GUIDSTR);
                    //kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                    //kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_ToKhaiChuyenTT;

                    //string tenluong = "Xanh";
                    //if (this.tkmd.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                    //    tenluong = "Vàng";
                    //else if (this.tkmd.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                    //    tenluong = "Đỏ";

                    //kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}", this.tkmd.SoToKhai, this.tkmd.NgayDangKy.ToShortDateString(), this.tkmd.MaLoaiHinh.Trim(), this.tkmd.MaHaiQuan.Trim(), tenluong);
                    //kqxl.Ngay = DateTime.Now;
                    //kqxl.Insert();

                    #endregion 
                    Company.KDT.SHARE.Components.MessageTypes fun = Company.KDT.SHARE.Components.MessageTypes.ToKhaiNhap;
                    if (this.tkmd.MaLoaiHinh.Substring(0,1).Equals("X"))
                        fun = Company.KDT.SHARE.Components.MessageTypes.ToKhaiXuat;
                    Company.KDT.SHARE.Components.Globals.SaveMessage(
                        string.Empty,
                        tkmd.ID,
                        tkmd.GUIDSTR,
                       fun,
                          Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                           Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay,
                         string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}", this.tkmd.SoToKhai, this.tkmd.NgayDangKy.ToShortDateString(), this.tkmd.MaLoaiHinh.Trim(), this.tkmd.MaHaiQuan.Trim(), tkDaDangKy.PhanLuong));                                                  
                    if (showMsg("MSG_240241") == "Cancel")
                    //if (ShowMessage("Chuyển trạng thái thành công", false) == "Cancel")
                    {
                        tkDaDangKy.Update();
                        tkmd.Update();
                        this.Close();
                    }
                }
                else
                {
                    showMsg("MSG_240242");
                    //ShowMessage("Thông tin tờ khai này trùng trong danh sách đăng ký! \nVui lòng kiểm tra lại số tờ khai và ngày đăng ký", false);
                }
            }
            else
            {
                showMsg("MSG_240243");
                //ShowMessage("Dữ liệu nhập vào chưa hợp lệ! \nVui lòng nhập lại.", false);
            }
        }

        private bool validData()
        {
            bool value = true;
            if (txtSoToKhai.Text == "" || int.Parse(txtSoToKhai.Text) < 0) value = false;

            return value;
        }

        private void getData()
        {
            this.tkmd.SoToKhai = int.Parse(txtSoToKhai.Text);
            this.tkmd.NgayDangKy = ccNgayDayKy.Value;
            this.tkmd.PhanLuong = cbPL.SelectedValue.ToString();
            this.tkmd.HUONGDAN = txtHuongDan.Text;

            if (this.tkmd.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                this.tkmd.NgayTiepNhan = this.tkmd.NgayDangKy;
            }
        }

        private void btnHuybo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChuyenTrangThaiTK_Load(object sender, EventArgs e)
        {
            cbPL.SelectedIndex = 1;
        }
    }
}