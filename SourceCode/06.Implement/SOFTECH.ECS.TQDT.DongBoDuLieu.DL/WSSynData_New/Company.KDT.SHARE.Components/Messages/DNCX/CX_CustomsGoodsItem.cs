﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.DNCX
{
    public class CX_CustomsGoodsItem : CustomsGoodsItem
    {
        ///// <summary>
        ///// Thông tin hàng
        ///// </summary>
        //[XmlElement("Commodity")]
        //public Commodity Commodity { get; set; }
        ///// <summary>
        ///// Số lượng và đơn vị tính
        ///// </summary>
        //[XmlElement("GoodsMeasure")]
        //public GoodsMeasure_BC GoodsMeasure { get; set; }

        ///// <summary>
        ///// Thông tin tờ khai nhập khẩu
        ///// </summary>
        [XmlElement("Reference")]
        public CX_Reference Reference { get; set; }
     

    }
}
