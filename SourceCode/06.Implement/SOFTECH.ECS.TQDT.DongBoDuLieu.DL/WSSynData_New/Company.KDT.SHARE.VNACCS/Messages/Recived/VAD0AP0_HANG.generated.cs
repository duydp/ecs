using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAD0AP0_HANG : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute B46 { get; set; }
public PropertiesAttribute B48 { get; set; }
public PropertiesAttribute ADN { get; set; }
public PropertiesAttribute B50 { get; set; }
public PropertiesAttribute B51 { get; set; }
public PropertiesAttribute B52 { get; set; }
public PropertiesAttribute B53 { get; set; }
public GroupAttribute VN1 { get; set; }
public PropertiesAttribute B55 { get; set; }
public PropertiesAttribute B56 { get; set; }
public PropertiesAttribute B69 { get; set; }
public PropertiesAttribute UPR { get; set; }
public PropertiesAttribute UPC { get; set; }
public PropertiesAttribute TSC { get; set; }
public PropertiesAttribute B57 { get; set; }
public PropertiesAttribute B71 { get; set; }
public PropertiesAttribute B72 { get; set; }
public PropertiesAttribute B58 { get; set; }
public PropertiesAttribute B59 { get; set; }
public PropertiesAttribute KKT { get; set; }
public PropertiesAttribute KKS { get; set; }
public PropertiesAttribute B63 { get; set; }
public PropertiesAttribute B64 { get; set; }
public PropertiesAttribute SKB { get; set; }
public PropertiesAttribute SPD { get; set; }
public PropertiesAttribute B68 { get; set; }
public PropertiesAttribute B76 { get; set; }
public PropertiesAttribute B77 { get; set; }
public PropertiesAttribute B78 { get; set; }
public PropertiesAttribute B70 { get; set; }
public PropertiesAttribute KWS { get; set; }
public PropertiesAttribute TDL { get; set; }
public PropertiesAttribute TXN { get; set; }
public PropertiesAttribute TXR { get; set; }
public PropertiesAttribute B79 { get; set; }
public PropertiesAttribute B80 { get; set; }
public GroupAttribute KO1 { get; set; }

public VAD0AP0_HANG()
        {
B46 = new PropertiesAttribute(2, typeof(string));
B48 = new PropertiesAttribute(12, typeof(string));
ADN = new PropertiesAttribute(7, typeof(string));
B50 = new PropertiesAttribute(1, typeof(string));
B51 = new PropertiesAttribute(600, typeof(string));
B52 = new PropertiesAttribute(12, typeof(int));
B53 = new PropertiesAttribute(4, typeof(string));
B55 = new PropertiesAttribute(12, typeof(int));
B56 = new PropertiesAttribute(4, typeof(string));
B69 = new PropertiesAttribute(20, typeof(int));
UPR = new PropertiesAttribute(9, typeof(int));
UPC = new PropertiesAttribute(3, typeof(string));
TSC = new PropertiesAttribute(4, typeof(string));
B57 = new PropertiesAttribute(17, typeof(int));
B71 = new PropertiesAttribute(3, typeof(string));
B72 = new PropertiesAttribute(20, typeof(int));
B58 = new PropertiesAttribute(12, typeof(int));
B59 = new PropertiesAttribute(4, typeof(string));
KKT = new PropertiesAttribute(18, typeof(int));
KKS = new PropertiesAttribute(4, typeof(string));
B63 = new PropertiesAttribute(1, typeof(string));
B64 = new PropertiesAttribute(30, typeof(string));
SKB = new PropertiesAttribute(1, typeof(string));
SPD = new PropertiesAttribute(10, typeof(string));
B68 = new PropertiesAttribute(16, typeof(int));
B76 = new PropertiesAttribute(2, typeof(string));
B77 = new PropertiesAttribute(7, typeof(string));
B78 = new PropertiesAttribute(3, typeof(string));
B70 = new PropertiesAttribute(16, typeof(int));
KWS = new PropertiesAttribute(1, typeof(string));
TDL = new PropertiesAttribute(2, typeof(string));
TXN = new PropertiesAttribute(12, typeof(int));
TXR = new PropertiesAttribute(3, typeof(string));
B79 = new PropertiesAttribute(5, typeof(string));
B80 = new PropertiesAttribute(60, typeof(string));
#region VN1
List<PropertiesAttribute> listVN1 = new List<PropertiesAttribute>();
listVN1.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAD0AP0_HANG_VN1, typeof(int)));
VN1 = new GroupAttribute("VN1", 5, listVN1);
#endregion VN1
#region KO1
List<PropertiesAttribute> listKO1 = new List<PropertiesAttribute>();
listKO1.Add(new PropertiesAttribute(27, 5, EnumGroupID.VAD0AP0_HANG_KO1, typeof(string)));
listKO1.Add(new PropertiesAttribute(10, 5, EnumGroupID.VAD0AP0_HANG_KP1, typeof(string)));
listKO1.Add(new PropertiesAttribute(17, 5, EnumGroupID.VAD0AP0_HANG_KQ1, typeof(int)));
listKO1.Add(new PropertiesAttribute(12, 5, EnumGroupID.VAD0AP0_HANG_KR1, typeof(int)));
listKO1.Add(new PropertiesAttribute(4, 5, EnumGroupID.VAD0AP0_HANG_KS1, typeof(string)));
listKO1.Add(new PropertiesAttribute(25, 5, EnumGroupID.VAD0AP0_HANG_KW1, typeof(string)));
listKO1.Add(new PropertiesAttribute(16, 5, EnumGroupID.VAD0AP0_HANG_KX1, typeof(int)));
listKO1.Add(new PropertiesAttribute(5, 5, EnumGroupID.VAD0AP0_HANG_KY1, typeof(string)));
listKO1.Add(new PropertiesAttribute(60, 5, EnumGroupID.VAD0AP0_HANG_LA1, typeof(string)));
listKO1.Add(new PropertiesAttribute(16, 5, EnumGroupID.VAD0AP0_HANG_LB1, typeof(int)));
KO1 = new GroupAttribute("KO1", 5, listKO1);
#endregion KO1
TongSoByte = 2082;
}

}public partial class EnumGroupID
    {
public static readonly string VAD0AP0_HANG_VN1 = "VAD0AP0_HANG_VN1";
public static readonly string VAD0AP0_HANG_KO1 = "VAD0AP0_HANG_KO1";
public static readonly string VAD0AP0_HANG_KP1 = "VAD0AP0_HANG_KP1";
public static readonly string VAD0AP0_HANG_KQ1 = "VAD0AP0_HANG_KQ1";
public static readonly string VAD0AP0_HANG_KR1 = "VAD0AP0_HANG_KR1";
public static readonly string VAD0AP0_HANG_KS1 = "VAD0AP0_HANG_KS1";
public static readonly string VAD0AP0_HANG_KW1 = "VAD0AP0_HANG_KW1";
public static readonly string VAD0AP0_HANG_KX1 = "VAD0AP0_HANG_KX1";
public static readonly string VAD0AP0_HANG_KY1 = "VAD0AP0_HANG_KY1";
public static readonly string VAD0AP0_HANG_LA1 = "VAD0AP0_HANG_LA1";
public static readonly string VAD0AP0_HANG_LB1 = "VAD0AP0_HANG_LB1";
}

}
