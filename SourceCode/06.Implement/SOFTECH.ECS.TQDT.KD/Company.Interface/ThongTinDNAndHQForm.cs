﻿using System;
using System.Xml;
using System.Windows.Forms;
#if KD_V2
using Company.KD.BLL.SXXK;
using Company.KD.BLL.SXXK.ToKhai;
#elif SXXK_V2
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK.ThanhKhoan;
#endif
using System.Threading;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Net.Sockets;

namespace Company.Interface
{
    public partial class ThongTinDNAndHQForm : BaseForm
    {
        public ThongTinDNAndHQForm()
        {
            InitializeComponent();
        }

        private void SendForm_Load(object sender, EventArgs e)
        {
            cbFont.SelectedValue = Company.KDT.SHARE.Components.Globals.FontName;
            txtDiaChi.Text = GlobalSettings.DIA_CHI;
            txtMaDN.Text = GlobalSettings.MA_DON_VI;
            txtTenDN.Text = GlobalSettings.TEN_DON_VI;
            txtTenNganHQ.Text = GlobalSettings.TEN_HAI_QUAN_NGAN;
            donViHaiQuanControl1.Ma = GlobalSettings.MA_HAI_QUAN.Trim();
            txtTenCucHQ.Text = GlobalSettings.TEN_CUC_HAI_QUAN;
            txtMailHaiQuan.Text = GlobalSettings.MailHaiQuan;
            txtMaMid.Text = GlobalSettings.MaMID;
            txtMaCuc.Text = GlobalSettings.MA_CUC_HAI_QUAN.Trim();
            donViHaiQuanControl1.ReadOnly = false;
            txtMailDoanhNghiep.Text = GlobalSettings.MailDoanhNghiep;
#if KD_V2
            txtDienThoaiDN.Text = GlobalSettings.SoDienThoaiDN;
            txtSoFaxDN.Text = GlobalSettings.SoFaxDN;
#elif SXXK_V2
            txtDienThoaiDN.Text = GlobalSettings.DienThoai;
            txtSoFaxDN.Text = GlobalSettings.Fax;
#endif
            txtNguoiLienHeDN.Text = GlobalSettings.NguoiLienHe;

            txtDiaChiHQ.Text = GlobalSettings.DiaChiWS_Host;
            txtTenDichVu.Text = GlobalSettings.DiaChiWS_Name;

            CultureInfo cultureVN = new CultureInfo("vi-VN");
            if (Thread.CurrentThread.CurrentCulture.Equals(cultureVN))
            {
                opVietNam.Checked = true;
            }
            else
                opTiengAnh.Checked = true;

            donViHaiQuanControl1.Leave += new EventHandler(donViHaiQuanControl1_Leave);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!Company.Interface.Globals.ValidateNull(donViHaiQuanControl1, errorProvider1, "Mã chi cục Hải quan"))
            {
                uiTab1.TabPages[0].Selected = true;
                return;
            }
            containerValidator1.Validate();
            if (!containerValidator1.IsValid)
            {
                uiTab1.TabPages[1].Selected = true;
                return;
            }

           
            if (!Company.Interface.Globals.ValidateNull(txtDienThoaiDN, errorProvider1, "Điện thoại"))
                return;
            if (!Company.Interface.Globals.ValidateNull(txtMailDoanhNghiep, errorProvider1, "Email (Thư điện tử)"))
                return;
            Company.KDT.SHARE.Components.Globals.FontName = cbFont.SelectedValue.ToString();
            #region OldCode
            //Properties.Settings.Default.DIA_CHI = txtDiaChi.Text.Trim();
            //Properties.Settings.Default.MA_DON_VI = txtMaDN.Text.Trim();
            //Properties.Settings.Default.MA_HAI_QUAN = donViHaiQuanControl1.Ma.Trim();
            //Properties.Settings.Default.TEN_HAI_QUAN = donViHaiQuanControl1.Ten;
            //Properties.Settings.Default.TEN_HAI_QUAN_NGAN=txtTenNganHQ.Text;
            //Properties.Settings.Default.TEN_DON_VI=txtTenDN.Text.Trim();
            //Properties.Settings.Default.MA_CUC_HAI_QUAN = txtMaCuc.Text.Trim();
            //Properties.Settings.Default.TEN_CUC_HAI_QUAN = txtTenCucHQ.Text;
            //Properties.Settings.Default.MailDoanhNghiep = txtMailDoanhNghiep.Text;
            //Properties.Settings.Default.MailHaiQuan = txtMailHaiQuan.Text;
            //Properties.Settings.Default.MaMID = txtMaMid.Text;
            //Properties.Settings.Default.Save();
            //GlobalSettings.RefreshKey();

            //CultureInfo culture = null;
            //if (opVietNam.Checked)
            //{
            //    culture = new CultureInfo("vi-VN");
            //}
            //else
            //    culture = new CultureInfo("en-US");

            //CultureInfo cultureVN = new CultureInfo("vi-VN");
            //if (Thread.CurrentThread.CurrentCulture.Equals(culture))
            //{
            //    this.Close();
            //}
            //else
            //{
            //    Thread.CurrentThread.CurrentCulture = culture;
            //    Thread.CurrentThread.CurrentUICulture = culture;
            //    string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            //    ApplicationSettings settings = new ApplicationSettings();
            //    settings.ReadXml(settingFileName);
            //    settings.GiaoDien[0].NgonNgu = culture.Name;
            //    settings.WriteXml(settingFileName);
            //}
            //this.Close();

            //Hungtq 14/01/2011. Luu cau hinh
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DIA_CHI", txtDiaChi.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_DON_VI", txtMaDN.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_HAI_QUAN", donViHaiQuanControl1.Ma.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_HAI_QUAN", donViHaiQuanControl1.Ten);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_HAI_QUAN_NGAN", txtTenNganHQ.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_DON_VI", txtTenDN.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_CUC_HAI_QUAN", txtMaCuc.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_CUC_HAI_QUAN", txtTenCucHQ.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MailHaiQuan", txtMailHaiQuan.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MaMID", txtMaMid.Text);
            //Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ThongBaoHetHan", txtThongBaoHetHan.Text.Trim());
            //Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTienKhoanTKN", txtSoTienKhoanTKN.Text.Trim());
            //Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTienKhoanTKX", txtSoTienKhoanTKX.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MailDoanhNghiep", txtMailDoanhNghiep.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FromMail", txtMailDoanhNghiep.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontName", Company.KDT.SHARE.Components.Globals.FontName);
            #endregion


            /*DATLMQ update Lưu cấu hình vào file config 18/01/2011.*/
            XmlDocument doc = new XmlDocument();
            string path = Company.KDT.SHARE.Components.Globals.GetPathProgram() + "\\ConfigDoanhNghiep";
            //Hungtq update 28/01/2011.
            string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);

            doc.Load(fileName);

            //HUNGTQ Updated 07/06/2011
            //Set thông tin MaCucHQ
            GlobalSettings.MA_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaCucHQ").InnerText = txtMaCuc.Text.Trim();
            //Set thông tin TenCucHQ
            GlobalSettings.TEN_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenCucHQ").InnerText = txtTenCucHQ.Text.Trim();
            //Set thông tin MaChiCucHQ
            GlobalSettings.MA_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaChiCucHQ").InnerText = donViHaiQuanControl1.Ma.Trim();
            //Set thông tin TenChiCucHQ
            GlobalSettings.TEN_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenChiCucHQ").InnerText = donViHaiQuanControl1.Ten.Trim();
            //Set thông tin TenNganChiCucHQ
            GlobalSettings.TEN_HAI_QUAN_NGAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenNganChiCucHQ").InnerText = txtTenNganHQ.Text.Trim();
            //Set thông tin MailHQ
            GlobalSettings.MailHaiQuan = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailHQ").InnerText = txtMailHaiQuan.Text.Trim();
            //Set thông tin MaDoanhNghiep
            GlobalSettings.MA_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaDoanhNghiep").InnerText = txtMaDN.Text.Trim();
            //Set thông tin TenDoanhNghiep
            GlobalSettings.TEN_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenDoanhNghiep").InnerText = txtTenDN.Text.Trim();
            //Set thông tin DiaChiDoanhNghiep
            GlobalSettings.DIA_CHI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DiaChiDoanhNghiep").InnerText = txtDiaChi.Text.Trim();
            //Set thông tin MaMid
            GlobalSettings.MaMID = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaMID").InnerText = txtMaMid.Text.Trim();
            //Set thông tin Email doanh nghiep/ ca nhan
            GlobalSettings.MailDoanhNghiep = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailDoanhNghiep").InnerText = txtMailDoanhNghiep.Text.Trim();
#if KD_V2
            //Set thông tin Dien thoai
            GlobalSettings.SoDienThoaiDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DienThoai").InnerText = txtDienThoaiDN.Text.Trim();
            //Set thông tin Fax
            GlobalSettings.SoFaxDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Fax").InnerText = txtSoFaxDN.Text.Trim();
#elif SXXK_V2
            //Set thông tin Dien thoai
            GlobalSettings.DienThoai = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DienThoai").InnerText = txtDienThoaiDN.Text.Trim();
            //Set thông tin Fax
            GlobalSettings.Fax = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Fax").InnerText = txtSoFaxDN.Text.Trim();
#endif
            //Set thông tin Nguoi lien he
            GlobalSettings.NguoiLienHe = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "NguoiLienHe").InnerText = txtNguoiLienHeDN.Text.Trim();

            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FromMail", txtMailDoanhNghiep.Text.Trim());

            //GlobalSettings.IsOnlyMe = chkOnlyMe.Checked;
            //Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "OnlyMe").InnerText = chkOnlyMe.Checked.ToString();

            //-----------------------------------------------------------------------
            //HUNGTQ Updated 07/06/2011
            string URI = lblWS.Text.Trim();
            if (!URI.Contains("http:"))
                URI = "http://" + lblWS.Text.Trim();
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DiaChiWS", URI);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlSetting("Company_KD_BLL_WS_KhaiDienTu_KDTService", URI);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlSetting("QuanLyChungTu_WS_KDTService", URI);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("WS_URL", URI);

            try
            {
                if (txtHost.Text.Trim().Equals("") || txtPort.Text.Trim().Equals(""))
                {
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsUseProxy", "False");
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Host", "");
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Port", "");
                }
                else
                {
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsUseProxy", "True");
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Host", txtHost.Text);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Port", txtPort.Text);
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi thiết lập Proxy.\nChi tiết lỗi: " + ex.Message, false);
                return;
            }

            //HUNGTQ Updated 07/06/2011
            //Set thong tin WS_Host
            XmlNode nodeWS_Host = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Host");
            if (txtDiaChiHQ.Text.Contains("http:"))
                GlobalSettings.DiaChiWS_Host = nodeWS_Host.InnerText = txtDiaChiHQ.Text.Trim();
            else
                GlobalSettings.DiaChiWS_Host = nodeWS_Host.InnerText = "http://" + txtDiaChiHQ.Text.Trim();
            //Set thong tin WS_Name
            XmlNode nodeWS_Name = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Name");
            GlobalSettings.DiaChiWS_Name = nodeWS_Name.InnerText = txtTenDichVu.Text.Trim();
            //Set thong tin WebService
            XmlNode nodeWebService = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS");
            GlobalSettings.DiaChiWS = nodeWebService.InnerText = URI;

            //Lưu file cấu hình
            doc.Save(fileName);

            try
            {
                CultureInfo infoEN = new CultureInfo("en-US");

                CheckUser.Bll.UserInfor customer = new CheckUser.Bll.UserInfor();
                customer.MaDonVi = GlobalSettings.MA_DON_VI;
                customer.TenDonVi = GlobalSettings.TEN_DON_VI;
#if KD_V2
                customer.SoDTLienHe = GlobalSettings.SoDienThoaiDN;
#elif SXXK_V2
                customer.SoDTLienHe = GlobalSettings.DienThoai;
#endif
                customer.Email = GlobalSettings.MailDoanhNghiep;
                customer.DiaChi = GlobalSettings.DIA_CHI;
                customer.MaMayKichHoat = Program.getProcessorID();
                customer.TenPhienBan = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("AssemblyName");
                customer.TinhTrang = Program.lic.licenseName + "\r\nMã chi cục: " + donViHaiQuanControl1.Ma + "\r\nTên chi cục: " + donViHaiQuanControl1.Ten;
                customer.NgayHetHan = System.Convert.ToDateTime(Program.lic.dayExpires, infoEN);
                customer.KeyLicense = Program.lic.codeActivate;
                WSCustomer.Service1 ws = new Company.Interface.WSCustomer.Service1();
                ws.InsertUser(customer);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("Lưu thông tin Doanh nghiệp qua Web Service.", ex); }

            ShowMessage("Lưu file cấu hình Thông tin Doanh nghiệp và Hải quan thành công.", false);

            GlobalSettings.RefreshKey();
            this.Close();
        }

        private void donViHaiQuanControl1_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(donViHaiQuanControl1.Ma)) return;
            string maCuc = donViHaiQuanControl1.Ma.Substring(1, 2);
            txtMaCuc.Text = maCuc;

            Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan objDonViHQ = Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.Load("Z" + maCuc + "Z");

            txtTenCucHQ.Text = objDonViHQ != null ? objDonViHQ.Ten : "";
            txtTenNganHQ.Text = donViHaiQuanControl1.Ten;
            txtMailHaiQuan.Text = "";
        }

        private void txtTenDichVu_TextChanged(object sender, EventArgs e)
        {
            lblWS.Text = txtDiaChiHQ.Text.Trim() + "/" + txtTenDichVu.Text.Trim();
        }

        private void txtDiaChiHQ_TextChanged(object sender, EventArgs e)
        {
            lblWS.Text = txtDiaChiHQ.Text.Trim() + "/" + txtTenDichVu.Text.Trim();
        }

        private void btnOpenWS_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();

            process.StartInfo.UseShellExecute = true;

            process.StartInfo.FileName = lblWS.Text.Trim();

            process.Start();
        }
    }
}
