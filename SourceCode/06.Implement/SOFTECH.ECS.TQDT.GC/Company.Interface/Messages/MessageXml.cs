﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
namespace Company.KDT.SHARE.Components.Messages
{
    public class MessageXmlEventArgs : EventArgs
    {
        internal MessageXml MessageXml { get; set; }
    }
    public class MessageXml
    {
        XmlDocument doc = null;
        HeaderMessage headermsg = null;
        private string msgXml = string.Empty;
        public string Password { get; set; }
        public MessageXml(string xml)
        {
            msgXml = xml;
            doc = new XmlDocument();
            doc.LoadXml(xml);
            headermsg = new HeaderMessage(doc);
        }
        public MessageXml(XmlDocument doc)
        {
            this.doc = doc;
            msgXml = doc.InnerXml;
            headermsg = new HeaderMessage(doc);
        }
        public XmlDocument XmlDocument
        {
            get
            {
                return doc;
            }
        }
        
        public string Messsage
        {
            get
            {
                return msgXml;
            }
        }
        

        public HeaderMessage HeaderMessage
        {
            get
            {
                return headermsg;
            }
        }
        int? statement = null;
        public int? Statement
        {
            get
            {
                if (statement == null)
                {
                    XmlNode node = doc.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/statement");
                    if (node == null) statement =null;
                    else statement= int.Parse(node.InnerText);
                }
                return statement;
            }
            set
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/statement");
                node.InnerText = value.Value.ToString();
            }
        }

    }
}
