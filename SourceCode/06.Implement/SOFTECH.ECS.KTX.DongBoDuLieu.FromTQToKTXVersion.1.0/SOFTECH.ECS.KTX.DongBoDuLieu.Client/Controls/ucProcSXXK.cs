﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DongBoDuLieu.BLL.SXXK;
using System.Security.Cryptography;

namespace DongBo.Controls
{
    public partial class ucProcSXXK : ucDesign
    {
        frmMain frmParent = null;
        delegate void SetTextCallback(string text);
        delegate void SetButtonXuLyLaiCallback(bool enable);

        public ucProcSXXK()
        {
            InitializeComponent();
            frmParent = (frmMain)Application.OpenForms["frmMain"];
        }

        private void ucProcSXXK_Load(object sender, EventArgs e)
        {
            this.validControl();
            if (!frmParent.btnNext.Enabled)
            {
                btnXulylai.Enabled = false;
                bgWorker.RunWorkerAsync();
            }
            else
            {
                pBar.Style = ProgressBarStyle.Blocks;
                pBar.Value = pBar.Maximum;
                this.setText("Dữ liệu đã được xử lý");
            }
        }

        private void setText(string str)
        {
            if (lblCaption.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(setText);
                this.Invoke(d, new object[] { str });
            }
            else
            {
                lblContent.Text = str;
                toolTip1.SetToolTip(lblContent, str);
            }
        }

        private void setButtonXuLyLai(bool enable)
        {
            if (btnXulylai.InvokeRequired)
            {
                SetButtonXuLyLaiCallback d = new SetButtonXuLyLaiCallback(setButtonXuLyLai);
                this.Invoke(d, new object[] { enable });
            }
            else
            {
                btnXulylai.Enabled = enable;
            }
        }

        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }

        private void QueryData_KTX(string passDN)
        {
            try
            {
                if (GlobalSettings.IsGetNPL)
                {
                    this.setText(GlobalSettings.radSynLocal == true ? "Đang cập nhật danh sách Nguyên Phụ Liệu" : "Đang cập nhật danh sách Nguyên Phụ Liệu từ Hải Quan");
                    if (GlobalSettings.dsHQ[0] == null)
                    {
                        if (GlobalSettings.radSynLocal)
                            GlobalSettings.dsHQ[0] = NPL.GetALLDanhSachNPLLocal(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.IsSoSanhDuLieu, GlobalSettings.DBTarget);
                        else
                            GlobalSettings.dsHQ[0] = NPL.GetALLDanhSachNPL(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, passDN, false);
                        GlobalSettings.dsHQ[0].Tables[0].Columns["MaHaiQuan"].Caption = "Mã HQ";
                        GlobalSettings.dsHQ[0].Tables[0].Columns["Ma"].Caption = "Mã NPL";
                        GlobalSettings.dsHQ[0].Tables[0].Columns["Ten"].Caption = "Tên Nguyên Phụ Liệu";
                        GlobalSettings.dsHQ[0].Tables[0].Columns["MaHS"].Caption = "Mã HS";
                        GlobalSettings.dsHQ[0].Tables[0].Columns["DVT_ID"].Caption = "ĐVT";
                    }
                }

                if (GlobalSettings.IsGetSP)
                {
                    this.setText(GlobalSettings.radSynLocal == true ? "Đang cập nhật danh sách Sản Phẩm" : "Đang cập nhật danh sách Sản Phẩm từ Hải Quan");
                    if (GlobalSettings.dsHQ[1] == null)
                    {
                        if (GlobalSettings.radSynLocal)
                            GlobalSettings.dsHQ[1] = SP.GetALLDanhSachSPLocal(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.IsSoSanhDuLieu, GlobalSettings.DBTarget);
                        else
                            GlobalSettings.dsHQ[1] = SP.GetALLDanhSachSP(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, passDN, GlobalSettings.radSynLocal);
                        GlobalSettings.dsHQ[1].Tables[0].Columns["MaHaiQuan"].Caption = "Mã HQ";
                        GlobalSettings.dsHQ[1].Tables[0].Columns["Ma"].Caption = "Mã SP";
                        GlobalSettings.dsHQ[1].Tables[0].Columns["Ten"].Caption = "Tên Sản Phẩm";
                        GlobalSettings.dsHQ[1].Tables[0].Columns["MaHS"].Caption = "Mã HS";
                        GlobalSettings.dsHQ[1].Tables[0].Columns["DVT_ID"].Caption = "ĐVT";
                    }
                }

                if (GlobalSettings.IsGetDM)
                {
                    this.setText(GlobalSettings.radSynLocal == true ? "Đang cập nhật danh sách Định Mức" : "Đang cập nhật danh sách Định Mức từ Hải Quan");
                    if (GlobalSettings.dsHQ[2] == null)
                    {
                        if (GlobalSettings.radSynLocal)
                            //GlobalSettings.dsHQ[2] = DM.GetALLDanhSachDMLocal(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.IsSoSanhDuLieu, GlobalSettings.DBTarget);
                            GlobalSettings.dsHQ[2] = DM.GetALLDanhSachDMLocalSimple(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.IsSoSanhDuLieu, GlobalSettings.DBTarget);
                        else
                            GlobalSettings.dsHQ[2] = DM.GetALLDanhSachDM(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.radSynLocal, passDN);
                        GlobalSettings.dsHQ[2].Tables[0].Columns["Ma_HQ"].Caption = "Mã HQ";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["Ma_DV"].Caption = "Mã Đơn Vị";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["Ma_SP"].Caption = "Mã SP";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["Ma_NPL"].Caption = "Mã NPL";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["DM_SD"].Caption = "ĐM SD";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["TL_HH"].Caption = "Tỷ lệ HH";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["DM_CHUNG"].Caption = "ĐM Chung";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["GHI_CHU"].Caption = "Ghi Chú";
                    }
                }

                if (GlobalSettings.IsGetTK)
                {
                    this.setText(GlobalSettings.radSynLocal == true ? "Đang cập nhật danh sách Tờ Khai" : "Đang cập nhật danh sách Tờ Khai từ Hải Quan");
                    if (GlobalSettings.dsHQ[3] == null)
                    {
                        if (GlobalSettings.radSynLocal)
                            //GlobalSettings.dsHQ[3] = TK.GetALLDanhSachToKhaiLocal(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.IsChiLayDuLieuDaDuyet, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.IsSoSanhDuLieu, GlobalSettings.DBTarget);
                            GlobalSettings.dsHQ[3] = TK.GetALLDanhSachToKhaiLocalSimple(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.IsChiLayDuLieuDaDuyet, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.IsSoSanhDuLieu, GlobalSettings.DBTarget);
                        else
                            GlobalSettings.dsHQ[3] = TK.GetALLDanhSachToKhai(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.radSynLocal, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, passDN);
                        GlobalSettings.dsHQ[3].Tables[0].Columns["SoTK"].Caption = "Số TK";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_LH"].Caption = "Mã LH";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_HQ"].Caption = "Mã HQ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["NamDK"].Caption = "Năm ĐK";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_DK"].Caption = "Ngày ĐK";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_DV"].Caption = "Mã Đơn Vị";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_DVUT"].Caption = "Mã ĐV Ủy Thác";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["DV_DT"].Caption = "ĐV Đối tác";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_PTVT"].Caption = "Mã PTVT";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ten_PTVT"].Caption = "Tên PTVT";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["NgayDen"].Caption = "Ngay đến PTVT";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Van_Don"].Caption = "Số Vận Đơn";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_CK"].Caption = "Mã Cửa Khẩu";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["CangNN"].Caption = "Cảng NK";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_GP"].Caption = "Ngày Giấy Phép";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_GP"].Caption = "Số Giấy Phép";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_HHGP"].Caption = "Ngày HH Giấy Phép";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_HD"].Caption = "Số HĐ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["TyGia_USD"].Caption = "T/g USD";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_HD"].Caption = "Ngày HĐ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_HHHD"].Caption = "Ngày HH HĐ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Nuoc_XK"].Caption = "Nước XK";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Nuoc_NK"].Caption = "Nước NK";

                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_GH"].Caption = "Mã Giao Hàng";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["SoHang"].Caption = "Số Hàng";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_PTTT"].Caption = "Mã PTTT";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_NT"].Caption = "Mã Nguyên Tệ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["TyGia_VND"].Caption = "T/g VNĐ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["LePhi_HQ"].Caption = "Lệ Phí HQ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["GiayTo"].Caption = "Chứng từ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["TenCH"].Caption = "Tên Chủ Hàng";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Phi_BH"].Caption = "Phí Bảo Hiểm";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Phi_VC"].Caption = "Phí Vận Chuyển";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_TH"].Caption = "Ngày TH";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["User_TH"].Caption = "Người TH";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["TongTGKB"].Caption = "Tổng TGKB";

                        GlobalSettings.dsHQ[3].Tables[0].Columns["TongTGTT"].Caption = "Tổng TGTT";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Tr_luong"].Caption = "Trọng Lượng";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_Kien"].Caption = "Số Kiện";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_Container"].Caption = "Số Container 20";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_Container40"].Caption = "Số Container 40";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_HDTM"].Caption = "Số HĐTM";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_HDTM"].Caption = "Ngày HĐTM";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_VanDon"].Caption = "Ngày Vận Đơn";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_PLTK"].Caption = "Số Phụ Lục";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_MID"].Caption = "Mã MID";
                    }
                    this.setText(GlobalSettings.radSynLocal == true ? "Đang cập nhật danh sách Hàng Mậu Dịch" : "Đang cập nhật danh sách Hàng Mậu Dịch từ Hải Quan");
                    if (GlobalSettings.dsHQ[4] == null)
                    {
                        if (GlobalSettings.radSynLocal)
                            GlobalSettings.dsHQ[4] = HMD.GetALLDanhSachHMDLocal(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.IsSoSanhDuLieu, GlobalSettings.DBTarget);
                        else
                            GlobalSettings.dsHQ[4] = HMD.GetALLDanhSachHMD(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, passDN, GlobalSettings.radSynLocal, 20051);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.setText("Xử lý không thành công " + ex.Message);
                setButtonXuLyLai(true);
            }
        }

        private void QueryData_TQDT(string passDN)
        {
            try
            {
                if (GlobalSettings.IsGetNPL)
                {
                    this.setText(GlobalSettings.radSynLocal == true ? "Đang cập nhật danh sách Nguyên Phụ Liệu" : "Đang cập nhật danh sách Nguyên Phụ Liệu từ Hải Quan");
                    if (GlobalSettings.dsHQ[0] == null)
                    {
                        if (GlobalSettings.radSynLocal)
                            GlobalSettings.dsHQ[0] = NPL.GetALLDanhSachNPLLocalTQDT(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.IsChiLayDuLieuDaDuyet, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.IsSoSanhDuLieu, GlobalSettings.DBTarget);
                        else
                            GlobalSettings.dsHQ[0] = NPL.GetALLDanhSachNPL(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, passDN, false);
                        GlobalSettings.dsHQ[0].Tables[0].Columns["Master_ID"].Caption = "ID";
                        GlobalSettings.dsHQ[0].Tables[0].Columns["Ma"].Caption = "Mã NPL";
                        GlobalSettings.dsHQ[0].Tables[0].Columns["Ten"].Caption = "Tên Nguyên Phụ Liệu";
                        GlobalSettings.dsHQ[0].Tables[0].Columns["MaHS"].Caption = "Mã HS";
                        GlobalSettings.dsHQ[0].Tables[0].Columns["DVT_ID"].Caption = "ĐVT";

                        GlobalSettings.dsHQ[0].Tables[0].Columns["SoTiepNhan"].Caption = "Số tiếp nhận";
                        GlobalSettings.dsHQ[0].Tables[0].Columns["NgayTiepNhan"].Caption = "Ngày tiếp nhận";
                        GlobalSettings.dsHQ[0].Tables[0].Columns["TrangThaiXuLy"].Caption = "Trạng thái xử lý";
                    }
                }

                if (GlobalSettings.IsGetSP)
                {
                    this.setText(GlobalSettings.radSynLocal == true ? "Đang cập nhật danh sách Sản Phẩm" : "Đang cập nhật danh sách Sản Phẩm từ Hải Quan");
                    if (GlobalSettings.dsHQ[1] == null)
                    {
                        if (GlobalSettings.radSynLocal)
                            GlobalSettings.dsHQ[1] = SP.GetALLDanhSachSPLocalTQDT(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.IsChiLayDuLieuDaDuyet, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.IsSoSanhDuLieu, GlobalSettings.DBTarget);
                        else
                            GlobalSettings.dsHQ[1] = SP.GetALLDanhSachSP(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, passDN, GlobalSettings.radSynLocal);
                        GlobalSettings.dsHQ[1].Tables[0].Columns["Master_ID"].Caption = "ID";
                        GlobalSettings.dsHQ[1].Tables[0].Columns["Ma"].Caption = "Mã SP";
                        GlobalSettings.dsHQ[1].Tables[0].Columns["Ten"].Caption = "Tên Sản Phẩm";
                        GlobalSettings.dsHQ[1].Tables[0].Columns["MaHS"].Caption = "Mã HS";
                        GlobalSettings.dsHQ[1].Tables[0].Columns["DVT_ID"].Caption = "ĐVT";

                        GlobalSettings.dsHQ[1].Tables[0].Columns["SoTiepNhan"].Caption = "Số tiếp nhận";
                        GlobalSettings.dsHQ[1].Tables[0].Columns["NgayTiepNhan"].Caption = "Ngày tiếp nhận";
                        GlobalSettings.dsHQ[1].Tables[0].Columns["TrangThaiXuLy"].Caption = "Trạng thái xử lý";
                    }
                }

                if (GlobalSettings.IsGetDM)
                {
                    this.setText(GlobalSettings.radSynLocal == true ? "Đang cập nhật danh sách Định Mức" : "Đang cập nhật danh sách Định Mức từ Hải Quan");
                    if (GlobalSettings.dsHQ[2] == null)
                    {
                        if (GlobalSettings.radSynLocal)
                            GlobalSettings.dsHQ[2] = DM.GetALLDanhSachDMLocalTQDT(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.IsChiLayDuLieuDaDuyet, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.IsSoSanhDuLieu, GlobalSettings.DBTarget);
                        else
                            GlobalSettings.dsHQ[2] = DM.GetALLDanhSachDM(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.radSynLocal, passDN);
                        GlobalSettings.dsHQ[2].Tables[0].Columns["Master_ID"].Caption = "ID";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["Ma_SP"].Caption = "Mã SP";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["Ma_NPL"].Caption = "Mã NPL";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["DM_SD"].Caption = "ĐM SD";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["TL_HH"].Caption = "Tỷ lệ HH";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["GHI_CHU"].Caption = "Ghi Chú";

                        GlobalSettings.dsHQ[2].Tables[0].Columns["SoTiepNhan"].Caption = "Số tiếp nhận";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["NgayTiepNhan"].Caption = "Ngày tiếp nhận";
                        GlobalSettings.dsHQ[2].Tables[0].Columns["TrangThaiXuLy"].Caption = "Trạng thái xử lý";
                    }
                }

                if (GlobalSettings.IsGetTK)
                {
                    this.setText(GlobalSettings.radSynLocal == true ? "Đang cập nhật danh sách Tờ Khai" : "Đang cập nhật danh sách Tờ Khai từ Hải Quan");
                    if (GlobalSettings.dsHQ[3] == null)
                    {
                        if (GlobalSettings.radSynLocal)
                        {
                            if (GlobalSettings.IsChiLayToKhaiSuaBoSung)
                                GlobalSettings.dsHQ[3] = TK.GetALLDanhSachToKhaiLocalTQDT_ChiLayToKhaiSua(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc);
                            else
                                GlobalSettings.dsHQ[3] = TK.GetALLDanhSachToKhaiLocalTQDT(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.IsChiLayDuLieuDaDuyet, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.IsSoSanhDuLieu, GlobalSettings.DBTarget);
                        }
                        else
                            GlobalSettings.dsHQ[3] = TK.GetALLDanhSachToKhai(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.radSynLocal, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, passDN);
                        GlobalSettings.dsHQ[3].Tables[0].Columns["GUIDSTR"].Caption = "GUIDSTR";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["SoTK"].Caption = "Số TK";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_LH"].Caption = "Mã LH";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_HQ"].Caption = "Mã HQ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["NamDK"].Caption = "Năm ĐK";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_DK"].Caption = "Ngày ĐK";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_DV"].Caption = "Mã Đơn Vị";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["PhanLuong"].Caption = "Phân luồng";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["HuongDan"].Caption = "Hướng dẫn";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_DVUT"].Caption = "Mã ĐV Ủy Thác";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["DV_DT"].Caption = "ĐV Đối tác";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_PTVT"].Caption = "Mã PTVT";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ten_PTVT"].Caption = "Tên PTVT";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["NgayDen"].Caption = "Ngay đến PTVT";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Van_Don"].Caption = "Số Vận Đơn";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_CK"].Caption = "Mã Cửa Khẩu";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["CangNN"].Caption = "Cảng NK";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_GP"].Caption = "Ngày Giấy Phép";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_GP"].Caption = "Số Giấy Phép";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_HHGP"].Caption = "Ngày HH Giấy Phép";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_HD"].Caption = "Số HĐ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["TyGia_USD"].Caption = "T/g USD";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_HD"].Caption = "Ngày HĐ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_HHHD"].Caption = "Ngày HH HĐ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Nuoc_XK"].Caption = "Nước XK";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Nuoc_NK"].Caption = "Nước NK";

                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_GH"].Caption = "Mã Giao Hàng";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["SoHang"].Caption = "Số Hàng";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_PTTT"].Caption = "Mã PTTT";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_NT"].Caption = "Mã Nguyên Tệ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["TyGia_VND"].Caption = "T/g VNĐ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["LePhi_HQ"].Caption = "Lệ Phí HQ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["GiayTo"].Caption = "Chứng từ";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["TenCH"].Caption = "Tên Chủ Hàng";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Phi_BH"].Caption = "Phí Bảo Hiểm";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Phi_VC"].Caption = "Phí Vận Chuyển";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_TH"].Caption = "Ngày TH";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["User_TH"].Caption = "Người TH";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["TongTGKB"].Caption = "Tổng TGKB";

                        GlobalSettings.dsHQ[3].Tables[0].Columns["TongTGTT"].Caption = "Tổng TGTT";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Tr_luong"].Caption = "Trọng Lượng";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_Kien"].Caption = "Số Kiện";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_Container"].Caption = "Số Container 20";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_Container40"].Caption = "Số Container 40";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_HDTM"].Caption = "Số HĐTM";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_HDTM"].Caption = "Ngày HĐTM";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ngay_VanDon"].Caption = "Ngày Vận Đơn";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["So_PLTK"].Caption = "Số Phụ Lục";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["Ma_MID"].Caption = "Mã MID";

                        GlobalSettings.dsHQ[3].Tables[0].Columns["SoTN"].Caption = "Số tiếp nhận";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["NgayTN"].Caption = "Ngày tiếp nhận";
                        GlobalSettings.dsHQ[3].Tables[0].Columns["TrangThaiXuLy"].Caption = "Trạng thái xử lý";

                    }
                    this.setText(GlobalSettings.radSynLocal == true ? "Đang cập nhật danh sách Hàng Mậu Dịch" : "Đang cập nhật danh sách Hàng Mậu Dịch từ Hải Quan");
                    if (GlobalSettings.dsHQ[4] == null)
                    {
                        if (GlobalSettings.radSynLocal)
                            GlobalSettings.dsHQ[4] = HMD.GetALLDanhSachHMDLocalTQDT(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.IsChiLayDuLieuDaDuyet, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.IsSoSanhDuLieu, GlobalSettings.DBTarget);
                        else
                            GlobalSettings.dsHQ[4] = HMD.GetALLDanhSachHMD(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, passDN, GlobalSettings.radSynLocal, 20051);
                    }
                }
            }
            catch (Exception ex)
            {
                this.setText("Xử lý không thành công " + ex.Message);

                Logger.LocalLogger.Instance().WriteMessage(ex);

                setButtonXuLyLai(true);
            }
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            string passDN = this.GetMD5Value(GlobalSettings.MaDoanhNghiep + "+" + GlobalSettings.MatKhau);

            if (GlobalSettings.PhienBan == "KTX")
                QueryData_KTX(passDN);
            else if (GlobalSettings.PhienBan == "TQDT")
                QueryData_TQDT(passDN);
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pBar.Style = ProgressBarStyle.Blocks;
            pBar.Value = pBar.Maximum;
            if (frmParent.btnNext.Enabled)
                this.setText("Đã hoàn thành việc xử lý dữ liệu");
            this.validControl();
            btnXulylai.Enabled = true;
        }

        private void btnXulylai_Click(object sender, EventArgs e)
        {
            GlobalSettings.dsHQ = new DataSet[] { null, null, null, null, null };
            GlobalSettings.dsDNNew = new DataSet[] { null, null, null, null, null };
            btnXulylai.Enabled = false;
            if (frmParent.btnNext.Enabled) frmParent.btnNext.Enabled = false;
            pBar.Style = ProgressBarStyle.Marquee;
            bgWorker.RunWorkerAsync();
        }

        private void validControl()
        {
            if (GlobalSettings.dsHQ[3] != null && GlobalSettings.dsHQ[0] != null && GlobalSettings.dsHQ[2] != null && GlobalSettings.dsHQ[1] != null && GlobalSettings.dsHQ[4] != null)
            {
                frmParent.btnNext.Enabled = true;
            }
            else
            {
                frmParent.btnNext.Enabled = false;
            }
        }

        private void btnHistory_Click(object sender, EventArgs e)
        {
            string file = AppDomain.CurrentDomain.BaseDirectory + "Error.log";

            if (System.IO.File.Exists(file))
            {
                System.Diagnostics.Process.Start(file);
            }
        }

    }
}
