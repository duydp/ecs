﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class DeclarationPhuKienToKhai
    {

       #region Sửa thông tin ngày đưa vào thanh khoản (501)
        /// <summary>
        /// Ngày trên hệ thống
        /// </summary>
        [XmlElement("oldClearance")]
        public string OldClearance { get; set; }
       /// <summary>
       /// Ngày cần sửa đổi
       /// </summary>
       [XmlElement("newClearance")]
        public string NewClearance { get; set; }
        #endregion


       #region Phụ kiện sửa thông tin hợp đồng của tờ khai(502)
       /// <summary>
       /// Ngày trên hệ thống
       /// </summary>
       [XmlElement("OldContractDocument")]
       public DeclarationContract OldContractDocument { get; set; }
       /// <summary>
       /// Ngày cần sửa đổi
       /// </summary>
       [XmlElement("NewContractDocument")]
       public DeclarationContract NewContractDocument { get; set; }
       #endregion

       #region Phụ kiện sửa thông tin mã hàng của tờ khai(503), Số lượng hàng của tờ khai (504), Đơn vị tính hàng của tờ khai(505)

       [XmlElement("GoodsShipment")]
       public PhuKienGoodsShipment GoodsShipment { get; set; }

       #endregion   

   
    }


    public class DeclarationContract
    {
        #region Hợp đồng cũ
        /// <summary>
        /// Số hợp đồng (cũ)
        /// </summary>
        [XmlElement("oldReference")]
        public string OldReference { get; set; }
        /// <summary>
        /// Ngày hợp đồng (ngày ký) (cũ)
        /// </summary>
        [XmlElement("oldIssue")]
        public string OldIssue { get; set; }
        #endregion


        #region Hợp đồng mới
        /// <summary>
        /// Số hợp đồng (mới)
        /// </summary>
        [XmlElement("newReference")]
        public string NewReference { get; set; }
        /// <summary>
        /// Ngày hợp đồng (ngày ký) (mới)
        /// </summary>
        [XmlElement("newIssue")]
        public string NewIssue { get; set; }
        #endregion

    }
}
