﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class ProductionInspectionHis
    {
        /// <summary>
        /// Đã/Chưa được cơ quan hải quan kiểm tra
        /// </summary>
        [XmlElement("isInspection")]
        public string IsInspection { get; set; }

        /// <summary>
        /// Thông tin các lần kiểm tra
        /// </summary>
        [XmlElement("ContentInspections")]
        public ContentInspections ContentInspections { get; set; }
    }
}
