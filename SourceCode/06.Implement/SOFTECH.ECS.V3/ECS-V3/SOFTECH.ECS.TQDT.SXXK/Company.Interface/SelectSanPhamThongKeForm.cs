using System;
using System.Data;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.Report.SXXK;

namespace Company.Interface
{
    public partial class SelectSanPhamThongKeForm : BaseForm
    {
        public SanPham SanPhamSelected = new SanPham();
        public DataTable table;

        public SelectSanPhamThongKeForm()
        {
            InitializeComponent();
            
        }

        public void BindData()
        {

            
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            table = new DataTable();
            table.Columns.Add("MaSP");
            table.Columns.Add("TenSP");
            table.Columns.Add("Luong");
        }

        //-----------------------------------------------------------------------------------------

        private void SanPhamRegistedForm_Load(object sender, EventArgs e)
        {

            this.khoitao_DuLieuChuan();
            // Sản phẩm đã đăng ký.
            dgList.Tables[0].Columns["MaDoanhNghiep"].Visible = GlobalSettings.DAI_LY_TTHQ;
            this.search();
            // Doanh nghiệp / Đại lý TTHQ.              

        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------
   
        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string maSP = e.Row.Cells["Ma"].Text;
                table.Rows.Clear();
                table.Rows.Add(new string[] { maSP, e.Row.Cells["Ten"].Text, "0" });
                this.Close();
            }
             
        }

      

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void filterEditor1_Click(object sender, EventArgs e)
        {

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            //this.MuiltiSellect();
            //this.Close();
        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            this.BindData();
            dgList.Refetch();
            this.Cursor = Cursors.Default;
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
             GridEXSelectedItemCollection items = dgList.SelectedItems;
             table.Rows.Clear();
             if (items.Count > 0)
             {
                 foreach (GridEXSelectedItem i in items)
                 {
                     if (i.RowType == RowType.Record)
                     {
                         SanPham sp = (SanPham)i.GetRow().DataRow;                         
                         table.Rows.Add(new string[] { sp.Ma, sp.Ten, "0" });
                     }
                 }
                 this.Close();
             }
             else
             {
               //  ShowMessage("Bạn chưa chọn sản phẩm.", false);
                 MLMessages("Bạn chưa chọn sản phẩm.", "MSG_SP03", "", false);
             }
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            

        }

        private void uiButton2_Click_1(object sender, EventArgs e)
        {
            
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void uiGroupBox4_Click(object sender, EventArgs e)
        {

        }
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "MaDoanhNghiep = '" + MaDoanhNghiep + "' and MaHaiQuan='"+GlobalSettings.MA_HAI_QUAN+"'";
            if (txtMaSP.Text.Trim().Length > 0)
            {
                where += " AND Ma Like '%" + txtMaSP.Text.Trim() + "%'";
            }

            if (txtTenSP.Text.Trim().Length > 0)
            {
                where += " AND Ten Like '%" + txtTenSP.Text.Trim() + "%'";
            }
            dgList.DataSource = new SanPham().SelectCollectionDynamic(where, "");
           
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            search();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
           
            
        }

        //-----------------------------------------------------------------------------------------
    }
}