using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HangMauDich : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
        public decimal SoToKhai { set; get; }
        public Int32 NamDangKy { set; get; }
		public string MaSoHang { set; get; }
		public string MaQuanLy { set; get; }
		public string TenHang { set; get; }
		public decimal ThueSuat { set; get; }
		public decimal ThueSuatTuyetDoi { set; get; }
		public string MaDVTTuyetDoi { set; get; }
		public string MaTTTuyetDoi { set; get; }
		public string NuocXuatXu { set; get; }
		public decimal SoLuong1 { set; get; }
		public string DVTLuong1 { set; get; }
		public decimal SoLuong2 { set; get; }
		public string DVTLuong2 { set; get; }
		public decimal TriGiaHoaDon { set; get; }
		public decimal DonGiaHoaDon { set; get; }
		public string MaTTDonGia { set; get; }
		public string DVTDonGia { set; get; }
		public string MaBieuThueNK { set; get; }
		public string MaHanNgach { set; get; }
		public string MaThueNKTheoLuong { set; get; }
		public string MaMienGiamThue { set; get; }
		public decimal SoTienGiamThue { set; get; }
		public decimal TriGiaTinhThue { set; get; }
		public string MaTTTriGiaTinhThue { set; get; }
		public string SoMucKhaiKhoanDC { set; get; }
		public string SoTTDongHangTKTNTX { set; get; }
		public string SoDMMienThue { set; get; }
		public string SoDongDMMienThue { set; get; }
		public string MaMienGiam { set; get; }
		public decimal SoTienMienGiam { set; get; }
		public string MaTTSoTienMienGiam { set; get; }
		public string MaVanBanPhapQuyKhac1 { set; get; }
		public string MaVanBanPhapQuyKhac2 { set; get; }
		public string MaVanBanPhapQuyKhac3 { set; get; }
		public string MaVanBanPhapQuyKhac4 { set; get; }
		public string MaVanBanPhapQuyKhac5 { set; get; }
		public string SoDong { set; get; }
		public string MaPhanLoaiTaiXacNhanGia { set; get; }
		public string TenNoiXuatXu { set; get; }
		public decimal SoLuongTinhThue { set; get; }
		public string MaDVTDanhThue { set; get; }
		public decimal DonGiaTinhThue { set; get; }
		public string DV_SL_TrongDonGiaTinhThue { set; get; }
		public string MaTTDonGiaTinhThue { set; get; }
		public decimal TriGiaTinhThueS { set; get; }
		public string MaTTTriGiaTinhThueS { set; get; }
		public string MaTTSoTienMienGiam1 { set; get; }
		public string MaPhanLoaiThueSuatThue { set; get; }
		public string ThueSuatThue { set; get; }
		public string PhanLoaiThueSuatThue { set; get; }
		public decimal SoTienThue { set; get; }
		public string MaTTSoTienThueXuatKhau { set; get; }
		public string TienLePhi_DonGia { set; get; }
		public string TienBaoHiem_DonGia { set; get; }
		public decimal TienLePhi_SoLuong { set; get; }
		public string TienLePhi_MaDVSoLuong { set; get; }
		public decimal TienBaoHiem_SoLuong { set; get; }
		public string TienBaoHiem_MaDVSoLuong { set; get; }
		public decimal TienLePhi_KhoanTien { set; get; }
		public decimal TienBaoHiem_KhoanTien { set; get; }
		public string DieuKhoanMienGiam { set; get; }
		public string MaHangHoa { set; get; }
		public string Templ_1 { set; get; }

        public string MaPhanLoaiKiemTra { set; get; }
        public string GhiChu { set; get; }
        public decimal SoLuongCont { set; get; }
        public string SoVanDon { set; get; }
        public string SoGiayPhep { set; get; }
        public DateTime NgayCapPhep { set; get; }
        public List<KDT_VNACC_TK_SoVanDon> VanDonCollection = new List<KDT_VNACC_TK_SoVanDon>();
        public List<KDT_VNACC_TK_GiayPhep> GiayPhepCollection = new List<KDT_VNACC_TK_GiayPhep>();
        public List<KDT_VNACC_TK_PhanHoi_SacThue> SacThueCollection = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_HangMauDich> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_HangMauDich> collection = new List<KDT_VNACC_HangMauDich>();
			while (reader.Read())
			{
				KDT_VNACC_HangMauDich entity = new KDT_VNACC_HangMauDich();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetDecimal(reader.GetOrdinal("SoToKhai"));
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                //try
                //{
                //    if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt32(reader.GetOrdinal("NamDangKy"));
                //}
                //catch (Exception ex)
                //{
                //    Logger.LocalLogger.Instance().WriteMessage(ex);
                //}
				if (!reader.IsDBNull(reader.GetOrdinal("MaSoHang"))) entity.MaSoHang = reader.GetString(reader.GetOrdinal("MaSoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaQuanLy"))) entity.MaQuanLy = reader.GetString(reader.GetOrdinal("MaQuanLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuat"))) entity.ThueSuat = reader.GetDecimal(reader.GetOrdinal("ThueSuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTuyetDoi"))) entity.ThueSuatTuyetDoi = reader.GetDecimal(reader.GetOrdinal("ThueSuatTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVTTuyetDoi"))) entity.MaDVTTuyetDoi = reader.GetString(reader.GetOrdinal("MaDVTTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTTuyetDoi"))) entity.MaTTTuyetDoi = reader.GetString(reader.GetOrdinal("MaTTTuyetDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXuatXu"))) entity.NuocXuatXu = reader.GetString(reader.GetOrdinal("NuocXuatXu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong1"))) entity.SoLuong1 = reader.GetDecimal(reader.GetOrdinal("SoLuong1"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTLuong1"))) entity.DVTLuong1 = reader.GetString(reader.GetOrdinal("DVTLuong1"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong2"))) entity.SoLuong2 = reader.GetDecimal(reader.GetOrdinal("SoLuong2"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTLuong2"))) entity.DVTLuong2 = reader.GetString(reader.GetOrdinal("DVTLuong2"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaHoaDon"))) entity.TriGiaHoaDon = reader.GetDecimal(reader.GetOrdinal("TriGiaHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaHoaDon"))) entity.DonGiaHoaDon = reader.GetDecimal(reader.GetOrdinal("DonGiaHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTDonGia"))) entity.MaTTDonGia = reader.GetString(reader.GetOrdinal("MaTTDonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTDonGia"))) entity.DVTDonGia = reader.GetString(reader.GetOrdinal("DVTDonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBieuThueNK"))) entity.MaBieuThueNK = reader.GetString(reader.GetOrdinal("MaBieuThueNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHanNgach"))) entity.MaHanNgach = reader.GetString(reader.GetOrdinal("MaHanNgach"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaThueNKTheoLuong"))) entity.MaThueNKTheoLuong = reader.GetString(reader.GetOrdinal("MaThueNKTheoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaMienGiamThue"))) entity.MaMienGiamThue = reader.GetString(reader.GetOrdinal("MaMienGiamThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienGiamThue"))) entity.SoTienGiamThue = reader.GetDecimal(reader.GetOrdinal("SoTienGiamThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThue"))) entity.TriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTTriGiaTinhThue"))) entity.MaTTTriGiaTinhThue = reader.GetString(reader.GetOrdinal("MaTTTriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoMucKhaiKhoanDC"))) entity.SoMucKhaiKhoanDC = reader.GetString(reader.GetOrdinal("SoMucKhaiKhoanDC"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTTDongHangTKTNTX"))) entity.SoTTDongHangTKTNTX = reader.GetString(reader.GetOrdinal("SoTTDongHangTKTNTX"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDMMienThue"))) entity.SoDMMienThue = reader.GetString(reader.GetOrdinal("SoDMMienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDongDMMienThue"))) entity.SoDongDMMienThue = reader.GetString(reader.GetOrdinal("SoDongDMMienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaMienGiam"))) entity.MaMienGiam = reader.GetString(reader.GetOrdinal("MaMienGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienMienGiam"))) entity.SoTienMienGiam = reader.GetDecimal(reader.GetOrdinal("SoTienMienGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTSoTienMienGiam"))) entity.MaTTSoTienMienGiam = reader.GetString(reader.GetOrdinal("MaTTSoTienMienGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapQuyKhac1"))) entity.MaVanBanPhapQuyKhac1 = reader.GetString(reader.GetOrdinal("MaVanBanPhapQuyKhac1"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapQuyKhac2"))) entity.MaVanBanPhapQuyKhac2 = reader.GetString(reader.GetOrdinal("MaVanBanPhapQuyKhac2"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapQuyKhac3"))) entity.MaVanBanPhapQuyKhac3 = reader.GetString(reader.GetOrdinal("MaVanBanPhapQuyKhac3"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapQuyKhac4"))) entity.MaVanBanPhapQuyKhac4 = reader.GetString(reader.GetOrdinal("MaVanBanPhapQuyKhac4"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapQuyKhac5"))) entity.MaVanBanPhapQuyKhac5 = reader.GetString(reader.GetOrdinal("MaVanBanPhapQuyKhac5"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDong"))) entity.SoDong = reader.GetString(reader.GetOrdinal("SoDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiTaiXacNhanGia"))) entity.MaPhanLoaiTaiXacNhanGia = reader.GetString(reader.GetOrdinal("MaPhanLoaiTaiXacNhanGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNoiXuatXu"))) entity.TenNoiXuatXu = reader.GetString(reader.GetOrdinal("TenNoiXuatXu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTinhThue"))) entity.SoLuongTinhThue = reader.GetDecimal(reader.GetOrdinal("SoLuongTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVTDanhThue"))) entity.MaDVTDanhThue = reader.GetString(reader.GetOrdinal("MaDVTDanhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTinhThue"))) entity.DonGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("DonGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("DV_SL_TrongDonGiaTinhThue"))) entity.DV_SL_TrongDonGiaTinhThue = reader.GetString(reader.GetOrdinal("DV_SL_TrongDonGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTDonGiaTinhThue"))) entity.MaTTDonGiaTinhThue = reader.GetString(reader.GetOrdinal("MaTTDonGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThueS"))) entity.TriGiaTinhThueS = reader.GetDecimal(reader.GetOrdinal("TriGiaTinhThueS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTTriGiaTinhThueS"))) entity.MaTTTriGiaTinhThueS = reader.GetString(reader.GetOrdinal("MaTTTriGiaTinhThueS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTSoTienMienGiam1"))) entity.MaTTSoTienMienGiam1 = reader.GetString(reader.GetOrdinal("MaTTSoTienMienGiam1"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiThueSuatThue"))) entity.MaPhanLoaiThueSuatThue = reader.GetString(reader.GetOrdinal("MaPhanLoaiThueSuatThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatThue"))) entity.ThueSuatThue = reader.GetString(reader.GetOrdinal("ThueSuatThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiThueSuatThue"))) entity.PhanLoaiThueSuatThue = reader.GetString(reader.GetOrdinal("PhanLoaiThueSuatThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienThue"))) entity.SoTienThue = reader.GetDecimal(reader.GetOrdinal("SoTienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTSoTienThueXuatKhau"))) entity.MaTTSoTienThueXuatKhau = reader.GetString(reader.GetOrdinal("MaTTSoTienThueXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienLePhi_DonGia"))) entity.TienLePhi_DonGia = reader.GetString(reader.GetOrdinal("TienLePhi_DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienBaoHiem_DonGia"))) entity.TienBaoHiem_DonGia = reader.GetString(reader.GetOrdinal("TienBaoHiem_DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienLePhi_SoLuong"))) entity.TienLePhi_SoLuong = reader.GetDecimal(reader.GetOrdinal("TienLePhi_SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienLePhi_MaDVSoLuong"))) entity.TienLePhi_MaDVSoLuong = reader.GetString(reader.GetOrdinal("TienLePhi_MaDVSoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienBaoHiem_SoLuong"))) entity.TienBaoHiem_SoLuong = reader.GetDecimal(reader.GetOrdinal("TienBaoHiem_SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienBaoHiem_MaDVSoLuong"))) entity.TienBaoHiem_MaDVSoLuong = reader.GetString(reader.GetOrdinal("TienBaoHiem_MaDVSoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienLePhi_KhoanTien"))) entity.TienLePhi_KhoanTien = reader.GetDecimal(reader.GetOrdinal("TienLePhi_KhoanTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienBaoHiem_KhoanTien"))) entity.TienBaoHiem_KhoanTien = reader.GetDecimal(reader.GetOrdinal("TienBaoHiem_KhoanTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("DieuKhoanMienGiam"))) entity.DieuKhoanMienGiam = reader.GetString(reader.GetOrdinal("DieuKhoanMienGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangHoa"))) entity.MaHangHoa = reader.GetString(reader.GetOrdinal("MaHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("Templ_1"))) entity.Templ_1 = reader.GetString(reader.GetOrdinal("Templ_1"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_HangMauDich> collection, long id)
        {
            foreach (KDT_VNACC_HangMauDich item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangMauDich VALUES(@TKMD_ID, @MaSoHang, @MaQuanLy, @TenHang, @ThueSuat, @ThueSuatTuyetDoi, @MaDVTTuyetDoi, @MaTTTuyetDoi, @NuocXuatXu, @SoLuong1, @DVTLuong1, @SoLuong2, @DVTLuong2, @TriGiaHoaDon, @DonGiaHoaDon, @MaTTDonGia, @DVTDonGia, @MaBieuThueNK, @MaHanNgach, @MaThueNKTheoLuong, @MaMienGiamThue, @SoTienGiamThue, @TriGiaTinhThue, @MaTTTriGiaTinhThue, @SoMucKhaiKhoanDC, @SoTTDongHangTKTNTX, @SoDMMienThue, @SoDongDMMienThue, @MaMienGiam, @SoTienMienGiam, @MaTTSoTienMienGiam, @MaVanBanPhapQuyKhac1, @MaVanBanPhapQuyKhac2, @MaVanBanPhapQuyKhac3, @MaVanBanPhapQuyKhac4, @MaVanBanPhapQuyKhac5, @SoDong, @MaPhanLoaiTaiXacNhanGia, @TenNoiXuatXu, @SoLuongTinhThue, @MaDVTDanhThue, @DonGiaTinhThue, @DV_SL_TrongDonGiaTinhThue, @MaTTDonGiaTinhThue, @TriGiaTinhThueS, @MaTTTriGiaTinhThueS, @MaTTSoTienMienGiam1, @MaPhanLoaiThueSuatThue, @ThueSuatThue, @PhanLoaiThueSuatThue, @SoTienThue, @MaTTSoTienThueXuatKhau, @TienLePhi_DonGia, @TienBaoHiem_DonGia, @TienLePhi_SoLuong, @TienLePhi_MaDVSoLuong, @TienBaoHiem_SoLuong, @TienBaoHiem_MaDVSoLuong, @TienLePhi_KhoanTien, @TienBaoHiem_KhoanTien, @DieuKhoanMienGiam, @MaHangHoa, @Templ_1)";
            string update = "UPDATE t_KDT_VNACC_HangMauDich SET TKMD_ID = @TKMD_ID, MaSoHang = @MaSoHang, MaQuanLy = @MaQuanLy, TenHang = @TenHang, ThueSuat = @ThueSuat, ThueSuatTuyetDoi = @ThueSuatTuyetDoi, MaDVTTuyetDoi = @MaDVTTuyetDoi, MaTTTuyetDoi = @MaTTTuyetDoi, NuocXuatXu = @NuocXuatXu, SoLuong1 = @SoLuong1, DVTLuong1 = @DVTLuong1, SoLuong2 = @SoLuong2, DVTLuong2 = @DVTLuong2, TriGiaHoaDon = @TriGiaHoaDon, DonGiaHoaDon = @DonGiaHoaDon, MaTTDonGia = @MaTTDonGia, DVTDonGia = @DVTDonGia, MaBieuThueNK = @MaBieuThueNK, MaHanNgach = @MaHanNgach, MaThueNKTheoLuong = @MaThueNKTheoLuong, MaMienGiamThue = @MaMienGiamThue, SoTienGiamThue = @SoTienGiamThue, TriGiaTinhThue = @TriGiaTinhThue, MaTTTriGiaTinhThue = @MaTTTriGiaTinhThue, SoMucKhaiKhoanDC = @SoMucKhaiKhoanDC, SoTTDongHangTKTNTX = @SoTTDongHangTKTNTX, SoDMMienThue = @SoDMMienThue, SoDongDMMienThue = @SoDongDMMienThue, MaMienGiam = @MaMienGiam, SoTienMienGiam = @SoTienMienGiam, MaTTSoTienMienGiam = @MaTTSoTienMienGiam, MaVanBanPhapQuyKhac1 = @MaVanBanPhapQuyKhac1, MaVanBanPhapQuyKhac2 = @MaVanBanPhapQuyKhac2, MaVanBanPhapQuyKhac3 = @MaVanBanPhapQuyKhac3, MaVanBanPhapQuyKhac4 = @MaVanBanPhapQuyKhac4, MaVanBanPhapQuyKhac5 = @MaVanBanPhapQuyKhac5, SoDong = @SoDong, MaPhanLoaiTaiXacNhanGia = @MaPhanLoaiTaiXacNhanGia, TenNoiXuatXu = @TenNoiXuatXu, SoLuongTinhThue = @SoLuongTinhThue, MaDVTDanhThue = @MaDVTDanhThue, DonGiaTinhThue = @DonGiaTinhThue, DV_SL_TrongDonGiaTinhThue = @DV_SL_TrongDonGiaTinhThue, MaTTDonGiaTinhThue = @MaTTDonGiaTinhThue, TriGiaTinhThueS = @TriGiaTinhThueS, MaTTTriGiaTinhThueS = @MaTTTriGiaTinhThueS, MaTTSoTienMienGiam1 = @MaTTSoTienMienGiam1, MaPhanLoaiThueSuatThue = @MaPhanLoaiThueSuatThue, ThueSuatThue = @ThueSuatThue, PhanLoaiThueSuatThue = @PhanLoaiThueSuatThue, SoTienThue = @SoTienThue, MaTTSoTienThueXuatKhau = @MaTTSoTienThueXuatKhau, TienLePhi_DonGia = @TienLePhi_DonGia, TienBaoHiem_DonGia = @TienBaoHiem_DonGia, TienLePhi_SoLuong = @TienLePhi_SoLuong, TienLePhi_MaDVSoLuong = @TienLePhi_MaDVSoLuong, TienBaoHiem_SoLuong = @TienBaoHiem_SoLuong, TienBaoHiem_MaDVSoLuong = @TienBaoHiem_MaDVSoLuong, TienLePhi_KhoanTien = @TienLePhi_KhoanTien, TienBaoHiem_KhoanTien = @TienBaoHiem_KhoanTien, DieuKhoanMienGiam = @DieuKhoanMienGiam, MaHangHoa = @MaHangHoa, Templ_1 = @Templ_1 WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangMauDich WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuanLy", SqlDbType.VarChar, "MaQuanLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuat", SqlDbType.Decimal, "ThueSuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTuyetDoi", SqlDbType.Decimal, "ThueSuatTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTTuyetDoi", SqlDbType.VarChar, "MaDVTTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTuyetDoi", SqlDbType.VarChar, "MaTTTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXuatXu", SqlDbType.VarChar, "NuocXuatXu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTLuong1", SqlDbType.VarChar, "DVTLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong2", SqlDbType.Decimal, "SoLuong2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTLuong2", SqlDbType.VarChar, "DVTLuong2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaHoaDon", SqlDbType.Decimal, "TriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaHoaDon", SqlDbType.Decimal, "DonGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTDonGia", SqlDbType.VarChar, "MaTTDonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTDonGia", SqlDbType.VarChar, "DVTDonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBieuThueNK", SqlDbType.VarChar, "MaBieuThueNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHanNgach", SqlDbType.VarChar, "MaHanNgach", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThueNKTheoLuong", SqlDbType.VarChar, "MaThueNKTheoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaMienGiamThue", SqlDbType.VarChar, "MaMienGiamThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienGiamThue", SqlDbType.Decimal, "SoTienGiamThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThue", SqlDbType.Decimal, "TriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, "MaTTTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoMucKhaiKhoanDC", SqlDbType.VarChar, "SoMucKhaiKhoanDC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTTDongHangTKTNTX", SqlDbType.VarChar, "SoTTDongHangTKTNTX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDMMienThue", SqlDbType.VarChar, "SoDMMienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDongDMMienThue", SqlDbType.VarChar, "SoDongDMMienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaMienGiam", SqlDbType.VarChar, "MaMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienMienGiam", SqlDbType.Decimal, "SoTienMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, "MaTTSoTienMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapQuyKhac1", SqlDbType.VarChar, "MaVanBanPhapQuyKhac1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapQuyKhac2", SqlDbType.VarChar, "MaVanBanPhapQuyKhac2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapQuyKhac3", SqlDbType.VarChar, "MaVanBanPhapQuyKhac3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapQuyKhac4", SqlDbType.VarChar, "MaVanBanPhapQuyKhac4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapQuyKhac5", SqlDbType.VarChar, "MaVanBanPhapQuyKhac5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, "MaPhanLoaiTaiXacNhanGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNoiXuatXu", SqlDbType.VarChar, "TenNoiXuatXu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThue", SqlDbType.Decimal, "SoLuongTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTDanhThue", SqlDbType.VarChar, "MaDVTDanhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTinhThue", SqlDbType.Decimal, "DonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, "DV_SL_TrongDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, "MaTTDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, "TriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, "MaTTTriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTSoTienMienGiam1", SqlDbType.VarChar, "MaTTSoTienMienGiam1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, "MaPhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatThue", SqlDbType.VarChar, "ThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, "PhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThue", SqlDbType.Decimal, "SoTienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, "MaTTSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, "TienLePhi_DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, "TienBaoHiem_DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, "TienLePhi_SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, "TienLePhi_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, "TienBaoHiem_SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, "TienBaoHiem_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, "TienLePhi_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, "TienBaoHiem_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, "DieuKhoanMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Templ_1", SqlDbType.VarChar, "Templ_1", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuanLy", SqlDbType.VarChar, "MaQuanLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuat", SqlDbType.Decimal, "ThueSuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTuyetDoi", SqlDbType.Decimal, "ThueSuatTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTTuyetDoi", SqlDbType.VarChar, "MaDVTTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTuyetDoi", SqlDbType.VarChar, "MaTTTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXuatXu", SqlDbType.VarChar, "NuocXuatXu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTLuong1", SqlDbType.VarChar, "DVTLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong2", SqlDbType.Decimal, "SoLuong2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTLuong2", SqlDbType.VarChar, "DVTLuong2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaHoaDon", SqlDbType.Decimal, "TriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaHoaDon", SqlDbType.Decimal, "DonGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTDonGia", SqlDbType.VarChar, "MaTTDonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTDonGia", SqlDbType.VarChar, "DVTDonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBieuThueNK", SqlDbType.VarChar, "MaBieuThueNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHanNgach", SqlDbType.VarChar, "MaHanNgach", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThueNKTheoLuong", SqlDbType.VarChar, "MaThueNKTheoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaMienGiamThue", SqlDbType.VarChar, "MaMienGiamThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienGiamThue", SqlDbType.Decimal, "SoTienGiamThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThue", SqlDbType.Decimal, "TriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, "MaTTTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoMucKhaiKhoanDC", SqlDbType.VarChar, "SoMucKhaiKhoanDC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTTDongHangTKTNTX", SqlDbType.VarChar, "SoTTDongHangTKTNTX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDMMienThue", SqlDbType.VarChar, "SoDMMienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDongDMMienThue", SqlDbType.VarChar, "SoDongDMMienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaMienGiam", SqlDbType.VarChar, "MaMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienMienGiam", SqlDbType.Decimal, "SoTienMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, "MaTTSoTienMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapQuyKhac1", SqlDbType.VarChar, "MaVanBanPhapQuyKhac1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapQuyKhac2", SqlDbType.VarChar, "MaVanBanPhapQuyKhac2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapQuyKhac3", SqlDbType.VarChar, "MaVanBanPhapQuyKhac3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapQuyKhac4", SqlDbType.VarChar, "MaVanBanPhapQuyKhac4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapQuyKhac5", SqlDbType.VarChar, "MaVanBanPhapQuyKhac5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, "MaPhanLoaiTaiXacNhanGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNoiXuatXu", SqlDbType.VarChar, "TenNoiXuatXu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThue", SqlDbType.Decimal, "SoLuongTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTDanhThue", SqlDbType.VarChar, "MaDVTDanhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTinhThue", SqlDbType.Decimal, "DonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, "DV_SL_TrongDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, "MaTTDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, "TriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, "MaTTTriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTSoTienMienGiam1", SqlDbType.VarChar, "MaTTSoTienMienGiam1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, "MaPhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatThue", SqlDbType.VarChar, "ThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, "PhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThue", SqlDbType.Decimal, "SoTienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, "MaTTSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, "TienLePhi_DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, "TienBaoHiem_DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, "TienLePhi_SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, "TienLePhi_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, "TienBaoHiem_SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, "TienBaoHiem_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, "TienLePhi_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, "TienBaoHiem_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, "DieuKhoanMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Templ_1", SqlDbType.VarChar, "Templ_1", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangMauDich VALUES(@TKMD_ID, @MaSoHang, @MaQuanLy, @TenHang, @ThueSuat, @ThueSuatTuyetDoi, @MaDVTTuyetDoi, @MaTTTuyetDoi, @NuocXuatXu, @SoLuong1, @DVTLuong1, @SoLuong2, @DVTLuong2, @TriGiaHoaDon, @DonGiaHoaDon, @MaTTDonGia, @DVTDonGia, @MaBieuThueNK, @MaHanNgach, @MaThueNKTheoLuong, @MaMienGiamThue, @SoTienGiamThue, @TriGiaTinhThue, @MaTTTriGiaTinhThue, @SoMucKhaiKhoanDC, @SoTTDongHangTKTNTX, @SoDMMienThue, @SoDongDMMienThue, @MaMienGiam, @SoTienMienGiam, @MaTTSoTienMienGiam, @MaVanBanPhapQuyKhac1, @MaVanBanPhapQuyKhac2, @MaVanBanPhapQuyKhac3, @MaVanBanPhapQuyKhac4, @MaVanBanPhapQuyKhac5, @SoDong, @MaPhanLoaiTaiXacNhanGia, @TenNoiXuatXu, @SoLuongTinhThue, @MaDVTDanhThue, @DonGiaTinhThue, @DV_SL_TrongDonGiaTinhThue, @MaTTDonGiaTinhThue, @TriGiaTinhThueS, @MaTTTriGiaTinhThueS, @MaTTSoTienMienGiam1, @MaPhanLoaiThueSuatThue, @ThueSuatThue, @PhanLoaiThueSuatThue, @SoTienThue, @MaTTSoTienThueXuatKhau, @TienLePhi_DonGia, @TienBaoHiem_DonGia, @TienLePhi_SoLuong, @TienLePhi_MaDVSoLuong, @TienBaoHiem_SoLuong, @TienBaoHiem_MaDVSoLuong, @TienLePhi_KhoanTien, @TienBaoHiem_KhoanTien, @DieuKhoanMienGiam, @MaHangHoa, @Templ_1)";
            string update = "UPDATE t_KDT_VNACC_HangMauDich SET TKMD_ID = @TKMD_ID, MaSoHang = @MaSoHang, MaQuanLy = @MaQuanLy, TenHang = @TenHang, ThueSuat = @ThueSuat, ThueSuatTuyetDoi = @ThueSuatTuyetDoi, MaDVTTuyetDoi = @MaDVTTuyetDoi, MaTTTuyetDoi = @MaTTTuyetDoi, NuocXuatXu = @NuocXuatXu, SoLuong1 = @SoLuong1, DVTLuong1 = @DVTLuong1, SoLuong2 = @SoLuong2, DVTLuong2 = @DVTLuong2, TriGiaHoaDon = @TriGiaHoaDon, DonGiaHoaDon = @DonGiaHoaDon, MaTTDonGia = @MaTTDonGia, DVTDonGia = @DVTDonGia, MaBieuThueNK = @MaBieuThueNK, MaHanNgach = @MaHanNgach, MaThueNKTheoLuong = @MaThueNKTheoLuong, MaMienGiamThue = @MaMienGiamThue, SoTienGiamThue = @SoTienGiamThue, TriGiaTinhThue = @TriGiaTinhThue, MaTTTriGiaTinhThue = @MaTTTriGiaTinhThue, SoMucKhaiKhoanDC = @SoMucKhaiKhoanDC, SoTTDongHangTKTNTX = @SoTTDongHangTKTNTX, SoDMMienThue = @SoDMMienThue, SoDongDMMienThue = @SoDongDMMienThue, MaMienGiam = @MaMienGiam, SoTienMienGiam = @SoTienMienGiam, MaTTSoTienMienGiam = @MaTTSoTienMienGiam, MaVanBanPhapQuyKhac1 = @MaVanBanPhapQuyKhac1, MaVanBanPhapQuyKhac2 = @MaVanBanPhapQuyKhac2, MaVanBanPhapQuyKhac3 = @MaVanBanPhapQuyKhac3, MaVanBanPhapQuyKhac4 = @MaVanBanPhapQuyKhac4, MaVanBanPhapQuyKhac5 = @MaVanBanPhapQuyKhac5, SoDong = @SoDong, MaPhanLoaiTaiXacNhanGia = @MaPhanLoaiTaiXacNhanGia, TenNoiXuatXu = @TenNoiXuatXu, SoLuongTinhThue = @SoLuongTinhThue, MaDVTDanhThue = @MaDVTDanhThue, DonGiaTinhThue = @DonGiaTinhThue, DV_SL_TrongDonGiaTinhThue = @DV_SL_TrongDonGiaTinhThue, MaTTDonGiaTinhThue = @MaTTDonGiaTinhThue, TriGiaTinhThueS = @TriGiaTinhThueS, MaTTTriGiaTinhThueS = @MaTTTriGiaTinhThueS, MaTTSoTienMienGiam1 = @MaTTSoTienMienGiam1, MaPhanLoaiThueSuatThue = @MaPhanLoaiThueSuatThue, ThueSuatThue = @ThueSuatThue, PhanLoaiThueSuatThue = @PhanLoaiThueSuatThue, SoTienThue = @SoTienThue, MaTTSoTienThueXuatKhau = @MaTTSoTienThueXuatKhau, TienLePhi_DonGia = @TienLePhi_DonGia, TienBaoHiem_DonGia = @TienBaoHiem_DonGia, TienLePhi_SoLuong = @TienLePhi_SoLuong, TienLePhi_MaDVSoLuong = @TienLePhi_MaDVSoLuong, TienBaoHiem_SoLuong = @TienBaoHiem_SoLuong, TienBaoHiem_MaDVSoLuong = @TienBaoHiem_MaDVSoLuong, TienLePhi_KhoanTien = @TienLePhi_KhoanTien, TienBaoHiem_KhoanTien = @TienBaoHiem_KhoanTien, DieuKhoanMienGiam = @DieuKhoanMienGiam, MaHangHoa = @MaHangHoa, Templ_1 = @Templ_1 WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangMauDich WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuanLy", SqlDbType.VarChar, "MaQuanLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuat", SqlDbType.Decimal, "ThueSuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTuyetDoi", SqlDbType.Decimal, "ThueSuatTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTTuyetDoi", SqlDbType.VarChar, "MaDVTTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTuyetDoi", SqlDbType.VarChar, "MaTTTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXuatXu", SqlDbType.VarChar, "NuocXuatXu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTLuong1", SqlDbType.VarChar, "DVTLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong2", SqlDbType.Decimal, "SoLuong2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTLuong2", SqlDbType.VarChar, "DVTLuong2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaHoaDon", SqlDbType.Decimal, "TriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaHoaDon", SqlDbType.Decimal, "DonGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTDonGia", SqlDbType.VarChar, "MaTTDonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTDonGia", SqlDbType.VarChar, "DVTDonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBieuThueNK", SqlDbType.VarChar, "MaBieuThueNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHanNgach", SqlDbType.VarChar, "MaHanNgach", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThueNKTheoLuong", SqlDbType.VarChar, "MaThueNKTheoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaMienGiamThue", SqlDbType.VarChar, "MaMienGiamThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienGiamThue", SqlDbType.Decimal, "SoTienGiamThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThue", SqlDbType.Decimal, "TriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, "MaTTTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoMucKhaiKhoanDC", SqlDbType.VarChar, "SoMucKhaiKhoanDC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTTDongHangTKTNTX", SqlDbType.VarChar, "SoTTDongHangTKTNTX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDMMienThue", SqlDbType.VarChar, "SoDMMienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDongDMMienThue", SqlDbType.VarChar, "SoDongDMMienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaMienGiam", SqlDbType.VarChar, "MaMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienMienGiam", SqlDbType.Decimal, "SoTienMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, "MaTTSoTienMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapQuyKhac1", SqlDbType.VarChar, "MaVanBanPhapQuyKhac1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapQuyKhac2", SqlDbType.VarChar, "MaVanBanPhapQuyKhac2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapQuyKhac3", SqlDbType.VarChar, "MaVanBanPhapQuyKhac3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapQuyKhac4", SqlDbType.VarChar, "MaVanBanPhapQuyKhac4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapQuyKhac5", SqlDbType.VarChar, "MaVanBanPhapQuyKhac5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, "MaPhanLoaiTaiXacNhanGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNoiXuatXu", SqlDbType.VarChar, "TenNoiXuatXu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThue", SqlDbType.Decimal, "SoLuongTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTDanhThue", SqlDbType.VarChar, "MaDVTDanhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTinhThue", SqlDbType.Decimal, "DonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, "DV_SL_TrongDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, "MaTTDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, "TriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, "MaTTTriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTSoTienMienGiam1", SqlDbType.VarChar, "MaTTSoTienMienGiam1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, "MaPhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatThue", SqlDbType.VarChar, "ThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, "PhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThue", SqlDbType.Decimal, "SoTienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, "MaTTSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, "TienLePhi_DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, "TienBaoHiem_DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, "TienLePhi_SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, "TienLePhi_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, "TienBaoHiem_SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, "TienBaoHiem_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, "TienLePhi_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, "TienBaoHiem_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, "DieuKhoanMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Templ_1", SqlDbType.VarChar, "Templ_1", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuanLy", SqlDbType.VarChar, "MaQuanLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuat", SqlDbType.Decimal, "ThueSuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTuyetDoi", SqlDbType.Decimal, "ThueSuatTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTTuyetDoi", SqlDbType.VarChar, "MaDVTTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTuyetDoi", SqlDbType.VarChar, "MaTTTuyetDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXuatXu", SqlDbType.VarChar, "NuocXuatXu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTLuong1", SqlDbType.VarChar, "DVTLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong2", SqlDbType.Decimal, "SoLuong2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTLuong2", SqlDbType.VarChar, "DVTLuong2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaHoaDon", SqlDbType.Decimal, "TriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaHoaDon", SqlDbType.Decimal, "DonGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTDonGia", SqlDbType.VarChar, "MaTTDonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTDonGia", SqlDbType.VarChar, "DVTDonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBieuThueNK", SqlDbType.VarChar, "MaBieuThueNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHanNgach", SqlDbType.VarChar, "MaHanNgach", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThueNKTheoLuong", SqlDbType.VarChar, "MaThueNKTheoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaMienGiamThue", SqlDbType.VarChar, "MaMienGiamThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienGiamThue", SqlDbType.Decimal, "SoTienGiamThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThue", SqlDbType.Decimal, "TriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, "MaTTTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoMucKhaiKhoanDC", SqlDbType.VarChar, "SoMucKhaiKhoanDC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTTDongHangTKTNTX", SqlDbType.VarChar, "SoTTDongHangTKTNTX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDMMienThue", SqlDbType.VarChar, "SoDMMienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDongDMMienThue", SqlDbType.VarChar, "SoDongDMMienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaMienGiam", SqlDbType.VarChar, "MaMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienMienGiam", SqlDbType.Decimal, "SoTienMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, "MaTTSoTienMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapQuyKhac1", SqlDbType.VarChar, "MaVanBanPhapQuyKhac1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapQuyKhac2", SqlDbType.VarChar, "MaVanBanPhapQuyKhac2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapQuyKhac3", SqlDbType.VarChar, "MaVanBanPhapQuyKhac3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapQuyKhac4", SqlDbType.VarChar, "MaVanBanPhapQuyKhac4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapQuyKhac5", SqlDbType.VarChar, "MaVanBanPhapQuyKhac5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, "MaPhanLoaiTaiXacNhanGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNoiXuatXu", SqlDbType.VarChar, "TenNoiXuatXu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThue", SqlDbType.Decimal, "SoLuongTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTDanhThue", SqlDbType.VarChar, "MaDVTDanhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTinhThue", SqlDbType.Decimal, "DonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, "DV_SL_TrongDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, "MaTTDonGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, "TriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, "MaTTTriGiaTinhThueS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTSoTienMienGiam1", SqlDbType.VarChar, "MaTTSoTienMienGiam1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, "MaPhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatThue", SqlDbType.VarChar, "ThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, "PhanLoaiThueSuatThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThue", SqlDbType.Decimal, "SoTienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, "MaTTSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, "TienLePhi_DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, "TienBaoHiem_DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, "TienLePhi_SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, "TienLePhi_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, "TienBaoHiem_SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, "TienBaoHiem_MaDVSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, "TienLePhi_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, "TienBaoHiem_KhoanTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, "DieuKhoanMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Templ_1", SqlDbType.VarChar, "Templ_1", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_HangMauDich Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_HangMauDich> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_HangMauDich> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_HangMauDich> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		//---------------------------------------------------------------------------------------------

        public static List<KDT_VNACC_HangMauDich> SelectCollectionAllDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderAllDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
        public static DataSet SelectDynamicGroup(DateTime DateFrom, DateTime DateTo)
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_SelectDynamicGroup]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@DateFrom", SqlDbType.DateTime, DateFrom);
            db.AddInParameter(dbCommand, "@DateTo", SqlDbType.DateTime, DateTo);

            return db.ExecuteDataSet(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
        public static IDataReader SelectReaderAllDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_SelectCollectionAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_HangMauDich(long tKMD_ID, string maSoHang, string maQuanLy, string tenHang, decimal thueSuat, decimal thueSuatTuyetDoi, string maDVTTuyetDoi, string maTTTuyetDoi, string nuocXuatXu, decimal soLuong1, string dVTLuong1, decimal soLuong2, string dVTLuong2, decimal triGiaHoaDon, decimal donGiaHoaDon, string maTTDonGia, string dVTDonGia, string maBieuThueNK, string maHanNgach, string maThueNKTheoLuong, string maMienGiamThue, decimal soTienGiamThue, decimal triGiaTinhThue, string maTTTriGiaTinhThue, string soMucKhaiKhoanDC, string soTTDongHangTKTNTX, string soDMMienThue, string soDongDMMienThue, string maMienGiam, decimal soTienMienGiam, string maTTSoTienMienGiam, string maVanBanPhapQuyKhac1, string maVanBanPhapQuyKhac2, string maVanBanPhapQuyKhac3, string maVanBanPhapQuyKhac4, string maVanBanPhapQuyKhac5, string soDong, string maPhanLoaiTaiXacNhanGia, string tenNoiXuatXu, decimal soLuongTinhThue, string maDVTDanhThue, decimal donGiaTinhThue, string dV_SL_TrongDonGiaTinhThue, string maTTDonGiaTinhThue, decimal triGiaTinhThueS, string maTTTriGiaTinhThueS, string maTTSoTienMienGiam1, string maPhanLoaiThueSuatThue, string thueSuatThue, string phanLoaiThueSuatThue, decimal soTienThue, string maTTSoTienThueXuatKhau, string tienLePhi_DonGia, string tienBaoHiem_DonGia, decimal tienLePhi_SoLuong, string tienLePhi_MaDVSoLuong, decimal tienBaoHiem_SoLuong, string tienBaoHiem_MaDVSoLuong, decimal tienLePhi_KhoanTien, decimal tienBaoHiem_KhoanTien, string dieuKhoanMienGiam, string maHangHoa, string templ_1)
		{
			KDT_VNACC_HangMauDich entity = new KDT_VNACC_HangMauDich();	
			entity.TKMD_ID = tKMD_ID;
			entity.MaSoHang = maSoHang;
			entity.MaQuanLy = maQuanLy;
			entity.TenHang = tenHang;
			entity.ThueSuat = thueSuat;
			entity.ThueSuatTuyetDoi = thueSuatTuyetDoi;
			entity.MaDVTTuyetDoi = maDVTTuyetDoi;
			entity.MaTTTuyetDoi = maTTTuyetDoi;
			entity.NuocXuatXu = nuocXuatXu;
			entity.SoLuong1 = soLuong1;
			entity.DVTLuong1 = dVTLuong1;
			entity.SoLuong2 = soLuong2;
			entity.DVTLuong2 = dVTLuong2;
			entity.TriGiaHoaDon = triGiaHoaDon;
			entity.DonGiaHoaDon = donGiaHoaDon;
			entity.MaTTDonGia = maTTDonGia;
			entity.DVTDonGia = dVTDonGia;
			entity.MaBieuThueNK = maBieuThueNK;
			entity.MaHanNgach = maHanNgach;
			entity.MaThueNKTheoLuong = maThueNKTheoLuong;
			entity.MaMienGiamThue = maMienGiamThue;
			entity.SoTienGiamThue = soTienGiamThue;
			entity.TriGiaTinhThue = triGiaTinhThue;
			entity.MaTTTriGiaTinhThue = maTTTriGiaTinhThue;
			entity.SoMucKhaiKhoanDC = soMucKhaiKhoanDC;
			entity.SoTTDongHangTKTNTX = soTTDongHangTKTNTX;
			entity.SoDMMienThue = soDMMienThue;
			entity.SoDongDMMienThue = soDongDMMienThue;
			entity.MaMienGiam = maMienGiam;
			entity.SoTienMienGiam = soTienMienGiam;
			entity.MaTTSoTienMienGiam = maTTSoTienMienGiam;
			entity.MaVanBanPhapQuyKhac1 = maVanBanPhapQuyKhac1;
			entity.MaVanBanPhapQuyKhac2 = maVanBanPhapQuyKhac2;
			entity.MaVanBanPhapQuyKhac3 = maVanBanPhapQuyKhac3;
			entity.MaVanBanPhapQuyKhac4 = maVanBanPhapQuyKhac4;
			entity.MaVanBanPhapQuyKhac5 = maVanBanPhapQuyKhac5;
			entity.SoDong = soDong;
			entity.MaPhanLoaiTaiXacNhanGia = maPhanLoaiTaiXacNhanGia;
			entity.TenNoiXuatXu = tenNoiXuatXu;
			entity.SoLuongTinhThue = soLuongTinhThue;
			entity.MaDVTDanhThue = maDVTDanhThue;
			entity.DonGiaTinhThue = donGiaTinhThue;
			entity.DV_SL_TrongDonGiaTinhThue = dV_SL_TrongDonGiaTinhThue;
			entity.MaTTDonGiaTinhThue = maTTDonGiaTinhThue;
			entity.TriGiaTinhThueS = triGiaTinhThueS;
			entity.MaTTTriGiaTinhThueS = maTTTriGiaTinhThueS;
			entity.MaTTSoTienMienGiam1 = maTTSoTienMienGiam1;
			entity.MaPhanLoaiThueSuatThue = maPhanLoaiThueSuatThue;
			entity.ThueSuatThue = thueSuatThue;
			entity.PhanLoaiThueSuatThue = phanLoaiThueSuatThue;
			entity.SoTienThue = soTienThue;
			entity.MaTTSoTienThueXuatKhau = maTTSoTienThueXuatKhau;
			entity.TienLePhi_DonGia = tienLePhi_DonGia;
			entity.TienBaoHiem_DonGia = tienBaoHiem_DonGia;
			entity.TienLePhi_SoLuong = tienLePhi_SoLuong;
			entity.TienLePhi_MaDVSoLuong = tienLePhi_MaDVSoLuong;
			entity.TienBaoHiem_SoLuong = tienBaoHiem_SoLuong;
			entity.TienBaoHiem_MaDVSoLuong = tienBaoHiem_MaDVSoLuong;
			entity.TienLePhi_KhoanTien = tienLePhi_KhoanTien;
			entity.TienBaoHiem_KhoanTien = tienBaoHiem_KhoanTien;
			entity.DieuKhoanMienGiam = dieuKhoanMienGiam;
			entity.MaHangHoa = maHangHoa;
			entity.Templ_1 = templ_1;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}

        public long InsertSynDataVNACCS()
        {
            return this.InsertSynDataVNACCS(null);
        }	
		//---------------------------------------------------------------------------------------------

        public long InsertSynDataVNACCS(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
            db.AddInParameter(dbCommand, "@MaQuanLy", SqlDbType.VarChar, MaQuanLy);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, ThueSuat);
            db.AddInParameter(dbCommand, "@ThueSuatTuyetDoi", SqlDbType.Decimal, ThueSuatTuyetDoi);
            db.AddInParameter(dbCommand, "@MaDVTTuyetDoi", SqlDbType.VarChar, MaDVTTuyetDoi);
            db.AddInParameter(dbCommand, "@MaTTTuyetDoi", SqlDbType.VarChar, MaTTTuyetDoi);
            db.AddInParameter(dbCommand, "@NuocXuatXu", SqlDbType.VarChar, NuocXuatXu);
            db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
            db.AddInParameter(dbCommand, "@DVTLuong1", SqlDbType.VarChar, DVTLuong1);
            db.AddInParameter(dbCommand, "@SoLuong2", SqlDbType.Decimal, SoLuong2);
            db.AddInParameter(dbCommand, "@DVTLuong2", SqlDbType.VarChar, DVTLuong2);
            db.AddInParameter(dbCommand, "@TriGiaHoaDon", SqlDbType.Decimal, TriGiaHoaDon);
            db.AddInParameter(dbCommand, "@DonGiaHoaDon", SqlDbType.Decimal, DonGiaHoaDon);
            db.AddInParameter(dbCommand, "@MaTTDonGia", SqlDbType.VarChar, MaTTDonGia);
            db.AddInParameter(dbCommand, "@DVTDonGia", SqlDbType.VarChar, DVTDonGia);
            db.AddInParameter(dbCommand, "@MaBieuThueNK", SqlDbType.VarChar, MaBieuThueNK);
            db.AddInParameter(dbCommand, "@MaHanNgach", SqlDbType.VarChar, MaHanNgach);
            db.AddInParameter(dbCommand, "@MaThueNKTheoLuong", SqlDbType.VarChar, MaThueNKTheoLuong);
            db.AddInParameter(dbCommand, "@MaMienGiamThue", SqlDbType.VarChar, MaMienGiamThue);
            db.AddInParameter(dbCommand, "@SoTienGiamThue", SqlDbType.Decimal, SoTienGiamThue);
            db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Decimal, TriGiaTinhThue);
            db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, MaTTTriGiaTinhThue);
            db.AddInParameter(dbCommand, "@SoMucKhaiKhoanDC", SqlDbType.VarChar, SoMucKhaiKhoanDC);
            db.AddInParameter(dbCommand, "@SoTTDongHangTKTNTX", SqlDbType.VarChar, SoTTDongHangTKTNTX);
            db.AddInParameter(dbCommand, "@SoDMMienThue", SqlDbType.VarChar, SoDMMienThue);
            db.AddInParameter(dbCommand, "@SoDongDMMienThue", SqlDbType.VarChar, SoDongDMMienThue);
            db.AddInParameter(dbCommand, "@MaMienGiam", SqlDbType.VarChar, MaMienGiam);
            db.AddInParameter(dbCommand, "@SoTienMienGiam", SqlDbType.Decimal, SoTienMienGiam);
            db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, MaTTSoTienMienGiam);
            db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac1", SqlDbType.VarChar, MaVanBanPhapQuyKhac1);
            db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac2", SqlDbType.VarChar, MaVanBanPhapQuyKhac2);
            db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac3", SqlDbType.VarChar, MaVanBanPhapQuyKhac3);
            db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac4", SqlDbType.VarChar, MaVanBanPhapQuyKhac4);
            db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac5", SqlDbType.VarChar, MaVanBanPhapQuyKhac5);
            db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
            db.AddInParameter(dbCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, MaPhanLoaiTaiXacNhanGia);
            db.AddInParameter(dbCommand, "@TenNoiXuatXu", SqlDbType.VarChar, TenNoiXuatXu);
            db.AddInParameter(dbCommand, "@SoLuongTinhThue", SqlDbType.Decimal, SoLuongTinhThue);
            db.AddInParameter(dbCommand, "@MaDVTDanhThue", SqlDbType.VarChar, MaDVTDanhThue);
            db.AddInParameter(dbCommand, "@DonGiaTinhThue", SqlDbType.Decimal, DonGiaTinhThue);
            db.AddInParameter(dbCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, DV_SL_TrongDonGiaTinhThue);
            db.AddInParameter(dbCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, MaTTDonGiaTinhThue);
            db.AddInParameter(dbCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, TriGiaTinhThueS);
            db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, MaTTTriGiaTinhThueS);
            db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam1", SqlDbType.VarChar, MaTTSoTienMienGiam1);
            db.AddInParameter(dbCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, MaPhanLoaiThueSuatThue);
            db.AddInParameter(dbCommand, "@ThueSuatThue", SqlDbType.VarChar, ThueSuatThue);
            db.AddInParameter(dbCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, PhanLoaiThueSuatThue);
            db.AddInParameter(dbCommand, "@SoTienThue", SqlDbType.Decimal, SoTienThue);
            db.AddInParameter(dbCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, MaTTSoTienThueXuatKhau);
            db.AddInParameter(dbCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, TienLePhi_DonGia);
            db.AddInParameter(dbCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, TienBaoHiem_DonGia);
            db.AddInParameter(dbCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, TienLePhi_SoLuong);
            db.AddInParameter(dbCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, TienLePhi_MaDVSoLuong);
            db.AddInParameter(dbCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, TienBaoHiem_SoLuong);
            db.AddInParameter(dbCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, TienBaoHiem_MaDVSoLuong);
            db.AddInParameter(dbCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, TienLePhi_KhoanTien);
            db.AddInParameter(dbCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, TienBaoHiem_KhoanTien);
            db.AddInParameter(dbCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, DieuKhoanMienGiam);
            db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
            db.AddInParameter(dbCommand, "@Templ_1", SqlDbType.VarChar, Templ_1);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
			db.AddInParameter(dbCommand, "@MaQuanLy", SqlDbType.VarChar, MaQuanLy);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, ThueSuat);
			db.AddInParameter(dbCommand, "@ThueSuatTuyetDoi", SqlDbType.Decimal, ThueSuatTuyetDoi);
			db.AddInParameter(dbCommand, "@MaDVTTuyetDoi", SqlDbType.VarChar, MaDVTTuyetDoi);
			db.AddInParameter(dbCommand, "@MaTTTuyetDoi", SqlDbType.VarChar, MaTTTuyetDoi);
			db.AddInParameter(dbCommand, "@NuocXuatXu", SqlDbType.VarChar, NuocXuatXu);
			db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
			db.AddInParameter(dbCommand, "@DVTLuong1", SqlDbType.VarChar, DVTLuong1);
			db.AddInParameter(dbCommand, "@SoLuong2", SqlDbType.Decimal, SoLuong2);
			db.AddInParameter(dbCommand, "@DVTLuong2", SqlDbType.VarChar, DVTLuong2);
			db.AddInParameter(dbCommand, "@TriGiaHoaDon", SqlDbType.Decimal, TriGiaHoaDon);
			db.AddInParameter(dbCommand, "@DonGiaHoaDon", SqlDbType.Decimal, DonGiaHoaDon);
			db.AddInParameter(dbCommand, "@MaTTDonGia", SqlDbType.VarChar, MaTTDonGia);
			db.AddInParameter(dbCommand, "@DVTDonGia", SqlDbType.VarChar, DVTDonGia);
			db.AddInParameter(dbCommand, "@MaBieuThueNK", SqlDbType.VarChar, MaBieuThueNK);
			db.AddInParameter(dbCommand, "@MaHanNgach", SqlDbType.VarChar, MaHanNgach);
			db.AddInParameter(dbCommand, "@MaThueNKTheoLuong", SqlDbType.VarChar, MaThueNKTheoLuong);
			db.AddInParameter(dbCommand, "@MaMienGiamThue", SqlDbType.VarChar, MaMienGiamThue);
			db.AddInParameter(dbCommand, "@SoTienGiamThue", SqlDbType.Decimal, SoTienGiamThue);
			db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Decimal, TriGiaTinhThue);
			db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, MaTTTriGiaTinhThue);
			db.AddInParameter(dbCommand, "@SoMucKhaiKhoanDC", SqlDbType.VarChar, SoMucKhaiKhoanDC);
			db.AddInParameter(dbCommand, "@SoTTDongHangTKTNTX", SqlDbType.VarChar, SoTTDongHangTKTNTX);
			db.AddInParameter(dbCommand, "@SoDMMienThue", SqlDbType.VarChar, SoDMMienThue);
			db.AddInParameter(dbCommand, "@SoDongDMMienThue", SqlDbType.VarChar, SoDongDMMienThue);
			db.AddInParameter(dbCommand, "@MaMienGiam", SqlDbType.VarChar, MaMienGiam);
			db.AddInParameter(dbCommand, "@SoTienMienGiam", SqlDbType.Decimal, SoTienMienGiam);
			db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, MaTTSoTienMienGiam);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac1", SqlDbType.VarChar, MaVanBanPhapQuyKhac1);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac2", SqlDbType.VarChar, MaVanBanPhapQuyKhac2);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac3", SqlDbType.VarChar, MaVanBanPhapQuyKhac3);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac4", SqlDbType.VarChar, MaVanBanPhapQuyKhac4);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac5", SqlDbType.VarChar, MaVanBanPhapQuyKhac5);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, MaPhanLoaiTaiXacNhanGia);
			db.AddInParameter(dbCommand, "@TenNoiXuatXu", SqlDbType.VarChar, TenNoiXuatXu);
			db.AddInParameter(dbCommand, "@SoLuongTinhThue", SqlDbType.Decimal, SoLuongTinhThue);
			db.AddInParameter(dbCommand, "@MaDVTDanhThue", SqlDbType.VarChar, MaDVTDanhThue);
			db.AddInParameter(dbCommand, "@DonGiaTinhThue", SqlDbType.Decimal, DonGiaTinhThue);
			db.AddInParameter(dbCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, DV_SL_TrongDonGiaTinhThue);
			db.AddInParameter(dbCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, MaTTDonGiaTinhThue);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, TriGiaTinhThueS);
			db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, MaTTTriGiaTinhThueS);
			db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam1", SqlDbType.VarChar, MaTTSoTienMienGiam1);
			db.AddInParameter(dbCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, MaPhanLoaiThueSuatThue);
			db.AddInParameter(dbCommand, "@ThueSuatThue", SqlDbType.VarChar, ThueSuatThue);
			db.AddInParameter(dbCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, PhanLoaiThueSuatThue);
			db.AddInParameter(dbCommand, "@SoTienThue", SqlDbType.Decimal, SoTienThue);
			db.AddInParameter(dbCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, MaTTSoTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, TienLePhi_DonGia);
			db.AddInParameter(dbCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, TienBaoHiem_DonGia);
			db.AddInParameter(dbCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, TienLePhi_SoLuong);
			db.AddInParameter(dbCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, TienLePhi_MaDVSoLuong);
			db.AddInParameter(dbCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, TienBaoHiem_SoLuong);
			db.AddInParameter(dbCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, TienBaoHiem_MaDVSoLuong);
			db.AddInParameter(dbCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, TienLePhi_KhoanTien);
			db.AddInParameter(dbCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, TienBaoHiem_KhoanTien);
			db.AddInParameter(dbCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, DieuKhoanMienGiam);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@Templ_1", SqlDbType.VarChar, Templ_1);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_HangMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_HangMauDich(long id, long tKMD_ID, string maSoHang, string maQuanLy, string tenHang, decimal thueSuat, decimal thueSuatTuyetDoi, string maDVTTuyetDoi, string maTTTuyetDoi, string nuocXuatXu, decimal soLuong1, string dVTLuong1, decimal soLuong2, string dVTLuong2, decimal triGiaHoaDon, decimal donGiaHoaDon, string maTTDonGia, string dVTDonGia, string maBieuThueNK, string maHanNgach, string maThueNKTheoLuong, string maMienGiamThue, decimal soTienGiamThue, decimal triGiaTinhThue, string maTTTriGiaTinhThue, string soMucKhaiKhoanDC, string soTTDongHangTKTNTX, string soDMMienThue, string soDongDMMienThue, string maMienGiam, decimal soTienMienGiam, string maTTSoTienMienGiam, string maVanBanPhapQuyKhac1, string maVanBanPhapQuyKhac2, string maVanBanPhapQuyKhac3, string maVanBanPhapQuyKhac4, string maVanBanPhapQuyKhac5, string soDong, string maPhanLoaiTaiXacNhanGia, string tenNoiXuatXu, decimal soLuongTinhThue, string maDVTDanhThue, decimal donGiaTinhThue, string dV_SL_TrongDonGiaTinhThue, string maTTDonGiaTinhThue, decimal triGiaTinhThueS, string maTTTriGiaTinhThueS, string maTTSoTienMienGiam1, string maPhanLoaiThueSuatThue, string thueSuatThue, string phanLoaiThueSuatThue, decimal soTienThue, string maTTSoTienThueXuatKhau, string tienLePhi_DonGia, string tienBaoHiem_DonGia, decimal tienLePhi_SoLuong, string tienLePhi_MaDVSoLuong, decimal tienBaoHiem_SoLuong, string tienBaoHiem_MaDVSoLuong, decimal tienLePhi_KhoanTien, decimal tienBaoHiem_KhoanTien, string dieuKhoanMienGiam, string maHangHoa, string templ_1)
		{
			KDT_VNACC_HangMauDich entity = new KDT_VNACC_HangMauDich();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.MaSoHang = maSoHang;
			entity.MaQuanLy = maQuanLy;
			entity.TenHang = tenHang;
			entity.ThueSuat = thueSuat;
			entity.ThueSuatTuyetDoi = thueSuatTuyetDoi;
			entity.MaDVTTuyetDoi = maDVTTuyetDoi;
			entity.MaTTTuyetDoi = maTTTuyetDoi;
			entity.NuocXuatXu = nuocXuatXu;
			entity.SoLuong1 = soLuong1;
			entity.DVTLuong1 = dVTLuong1;
			entity.SoLuong2 = soLuong2;
			entity.DVTLuong2 = dVTLuong2;
			entity.TriGiaHoaDon = triGiaHoaDon;
			entity.DonGiaHoaDon = donGiaHoaDon;
			entity.MaTTDonGia = maTTDonGia;
			entity.DVTDonGia = dVTDonGia;
			entity.MaBieuThueNK = maBieuThueNK;
			entity.MaHanNgach = maHanNgach;
			entity.MaThueNKTheoLuong = maThueNKTheoLuong;
			entity.MaMienGiamThue = maMienGiamThue;
			entity.SoTienGiamThue = soTienGiamThue;
			entity.TriGiaTinhThue = triGiaTinhThue;
			entity.MaTTTriGiaTinhThue = maTTTriGiaTinhThue;
			entity.SoMucKhaiKhoanDC = soMucKhaiKhoanDC;
			entity.SoTTDongHangTKTNTX = soTTDongHangTKTNTX;
			entity.SoDMMienThue = soDMMienThue;
			entity.SoDongDMMienThue = soDongDMMienThue;
			entity.MaMienGiam = maMienGiam;
			entity.SoTienMienGiam = soTienMienGiam;
			entity.MaTTSoTienMienGiam = maTTSoTienMienGiam;
			entity.MaVanBanPhapQuyKhac1 = maVanBanPhapQuyKhac1;
			entity.MaVanBanPhapQuyKhac2 = maVanBanPhapQuyKhac2;
			entity.MaVanBanPhapQuyKhac3 = maVanBanPhapQuyKhac3;
			entity.MaVanBanPhapQuyKhac4 = maVanBanPhapQuyKhac4;
			entity.MaVanBanPhapQuyKhac5 = maVanBanPhapQuyKhac5;
			entity.SoDong = soDong;
			entity.MaPhanLoaiTaiXacNhanGia = maPhanLoaiTaiXacNhanGia;
			entity.TenNoiXuatXu = tenNoiXuatXu;
			entity.SoLuongTinhThue = soLuongTinhThue;
			entity.MaDVTDanhThue = maDVTDanhThue;
			entity.DonGiaTinhThue = donGiaTinhThue;
			entity.DV_SL_TrongDonGiaTinhThue = dV_SL_TrongDonGiaTinhThue;
			entity.MaTTDonGiaTinhThue = maTTDonGiaTinhThue;
			entity.TriGiaTinhThueS = triGiaTinhThueS;
			entity.MaTTTriGiaTinhThueS = maTTTriGiaTinhThueS;
			entity.MaTTSoTienMienGiam1 = maTTSoTienMienGiam1;
			entity.MaPhanLoaiThueSuatThue = maPhanLoaiThueSuatThue;
			entity.ThueSuatThue = thueSuatThue;
			entity.PhanLoaiThueSuatThue = phanLoaiThueSuatThue;
			entity.SoTienThue = soTienThue;
			entity.MaTTSoTienThueXuatKhau = maTTSoTienThueXuatKhau;
			entity.TienLePhi_DonGia = tienLePhi_DonGia;
			entity.TienBaoHiem_DonGia = tienBaoHiem_DonGia;
			entity.TienLePhi_SoLuong = tienLePhi_SoLuong;
			entity.TienLePhi_MaDVSoLuong = tienLePhi_MaDVSoLuong;
			entity.TienBaoHiem_SoLuong = tienBaoHiem_SoLuong;
			entity.TienBaoHiem_MaDVSoLuong = tienBaoHiem_MaDVSoLuong;
			entity.TienLePhi_KhoanTien = tienLePhi_KhoanTien;
			entity.TienBaoHiem_KhoanTien = tienBaoHiem_KhoanTien;
			entity.DieuKhoanMienGiam = dieuKhoanMienGiam;
			entity.MaHangHoa = maHangHoa;
			entity.Templ_1 = templ_1;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_HangMauDich_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
			db.AddInParameter(dbCommand, "@MaQuanLy", SqlDbType.VarChar, MaQuanLy);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, ThueSuat);
			db.AddInParameter(dbCommand, "@ThueSuatTuyetDoi", SqlDbType.Decimal, ThueSuatTuyetDoi);
			db.AddInParameter(dbCommand, "@MaDVTTuyetDoi", SqlDbType.VarChar, MaDVTTuyetDoi);
			db.AddInParameter(dbCommand, "@MaTTTuyetDoi", SqlDbType.VarChar, MaTTTuyetDoi);
			db.AddInParameter(dbCommand, "@NuocXuatXu", SqlDbType.VarChar, NuocXuatXu);
			db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
			db.AddInParameter(dbCommand, "@DVTLuong1", SqlDbType.VarChar, DVTLuong1);
			db.AddInParameter(dbCommand, "@SoLuong2", SqlDbType.Decimal, SoLuong2);
			db.AddInParameter(dbCommand, "@DVTLuong2", SqlDbType.VarChar, DVTLuong2);
			db.AddInParameter(dbCommand, "@TriGiaHoaDon", SqlDbType.Decimal, TriGiaHoaDon);
			db.AddInParameter(dbCommand, "@DonGiaHoaDon", SqlDbType.Decimal, DonGiaHoaDon);
			db.AddInParameter(dbCommand, "@MaTTDonGia", SqlDbType.VarChar, MaTTDonGia);
			db.AddInParameter(dbCommand, "@DVTDonGia", SqlDbType.VarChar, DVTDonGia);
			db.AddInParameter(dbCommand, "@MaBieuThueNK", SqlDbType.VarChar, MaBieuThueNK);
			db.AddInParameter(dbCommand, "@MaHanNgach", SqlDbType.VarChar, MaHanNgach);
			db.AddInParameter(dbCommand, "@MaThueNKTheoLuong", SqlDbType.VarChar, MaThueNKTheoLuong);
			db.AddInParameter(dbCommand, "@MaMienGiamThue", SqlDbType.VarChar, MaMienGiamThue);
			db.AddInParameter(dbCommand, "@SoTienGiamThue", SqlDbType.Decimal, SoTienGiamThue);
			db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Decimal, TriGiaTinhThue);
			db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, MaTTTriGiaTinhThue);
			db.AddInParameter(dbCommand, "@SoMucKhaiKhoanDC", SqlDbType.VarChar, SoMucKhaiKhoanDC);
			db.AddInParameter(dbCommand, "@SoTTDongHangTKTNTX", SqlDbType.VarChar, SoTTDongHangTKTNTX);
			db.AddInParameter(dbCommand, "@SoDMMienThue", SqlDbType.VarChar, SoDMMienThue);
			db.AddInParameter(dbCommand, "@SoDongDMMienThue", SqlDbType.VarChar, SoDongDMMienThue);
			db.AddInParameter(dbCommand, "@MaMienGiam", SqlDbType.VarChar, MaMienGiam);
			db.AddInParameter(dbCommand, "@SoTienMienGiam", SqlDbType.Decimal, SoTienMienGiam);
			db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, MaTTSoTienMienGiam);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac1", SqlDbType.VarChar, MaVanBanPhapQuyKhac1);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac2", SqlDbType.VarChar, MaVanBanPhapQuyKhac2);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac3", SqlDbType.VarChar, MaVanBanPhapQuyKhac3);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac4", SqlDbType.VarChar, MaVanBanPhapQuyKhac4);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac5", SqlDbType.VarChar, MaVanBanPhapQuyKhac5);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, MaPhanLoaiTaiXacNhanGia);
			db.AddInParameter(dbCommand, "@TenNoiXuatXu", SqlDbType.VarChar, TenNoiXuatXu);
			db.AddInParameter(dbCommand, "@SoLuongTinhThue", SqlDbType.Decimal, SoLuongTinhThue);
			db.AddInParameter(dbCommand, "@MaDVTDanhThue", SqlDbType.VarChar, MaDVTDanhThue);
			db.AddInParameter(dbCommand, "@DonGiaTinhThue", SqlDbType.Decimal, DonGiaTinhThue);
			db.AddInParameter(dbCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, DV_SL_TrongDonGiaTinhThue);
			db.AddInParameter(dbCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, MaTTDonGiaTinhThue);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, TriGiaTinhThueS);
			db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, MaTTTriGiaTinhThueS);
			db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam1", SqlDbType.VarChar, MaTTSoTienMienGiam1);
			db.AddInParameter(dbCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, MaPhanLoaiThueSuatThue);
			db.AddInParameter(dbCommand, "@ThueSuatThue", SqlDbType.VarChar, ThueSuatThue);
			db.AddInParameter(dbCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, PhanLoaiThueSuatThue);
			db.AddInParameter(dbCommand, "@SoTienThue", SqlDbType.Decimal, SoTienThue);
			db.AddInParameter(dbCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, MaTTSoTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, TienLePhi_DonGia);
			db.AddInParameter(dbCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, TienBaoHiem_DonGia);
			db.AddInParameter(dbCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, TienLePhi_SoLuong);
			db.AddInParameter(dbCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, TienLePhi_MaDVSoLuong);
			db.AddInParameter(dbCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, TienBaoHiem_SoLuong);
			db.AddInParameter(dbCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, TienBaoHiem_MaDVSoLuong);
			db.AddInParameter(dbCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, TienLePhi_KhoanTien);
			db.AddInParameter(dbCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, TienBaoHiem_KhoanTien);
			db.AddInParameter(dbCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, DieuKhoanMienGiam);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@Templ_1", SqlDbType.VarChar, Templ_1);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_HangMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_HangMauDich(long id, long tKMD_ID, string maSoHang, string maQuanLy, string tenHang, decimal thueSuat, decimal thueSuatTuyetDoi, string maDVTTuyetDoi, string maTTTuyetDoi, string nuocXuatXu, decimal soLuong1, string dVTLuong1, decimal soLuong2, string dVTLuong2, decimal triGiaHoaDon, decimal donGiaHoaDon, string maTTDonGia, string dVTDonGia, string maBieuThueNK, string maHanNgach, string maThueNKTheoLuong, string maMienGiamThue, decimal soTienGiamThue, decimal triGiaTinhThue, string maTTTriGiaTinhThue, string soMucKhaiKhoanDC, string soTTDongHangTKTNTX, string soDMMienThue, string soDongDMMienThue, string maMienGiam, decimal soTienMienGiam, string maTTSoTienMienGiam, string maVanBanPhapQuyKhac1, string maVanBanPhapQuyKhac2, string maVanBanPhapQuyKhac3, string maVanBanPhapQuyKhac4, string maVanBanPhapQuyKhac5, string soDong, string maPhanLoaiTaiXacNhanGia, string tenNoiXuatXu, decimal soLuongTinhThue, string maDVTDanhThue, decimal donGiaTinhThue, string dV_SL_TrongDonGiaTinhThue, string maTTDonGiaTinhThue, decimal triGiaTinhThueS, string maTTTriGiaTinhThueS, string maTTSoTienMienGiam1, string maPhanLoaiThueSuatThue, string thueSuatThue, string phanLoaiThueSuatThue, decimal soTienThue, string maTTSoTienThueXuatKhau, string tienLePhi_DonGia, string tienBaoHiem_DonGia, decimal tienLePhi_SoLuong, string tienLePhi_MaDVSoLuong, decimal tienBaoHiem_SoLuong, string tienBaoHiem_MaDVSoLuong, decimal tienLePhi_KhoanTien, decimal tienBaoHiem_KhoanTien, string dieuKhoanMienGiam, string maHangHoa, string templ_1)
		{
			KDT_VNACC_HangMauDich entity = new KDT_VNACC_HangMauDich();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.MaSoHang = maSoHang;
			entity.MaQuanLy = maQuanLy;
			entity.TenHang = tenHang;
			entity.ThueSuat = thueSuat;
			entity.ThueSuatTuyetDoi = thueSuatTuyetDoi;
			entity.MaDVTTuyetDoi = maDVTTuyetDoi;
			entity.MaTTTuyetDoi = maTTTuyetDoi;
			entity.NuocXuatXu = nuocXuatXu;
			entity.SoLuong1 = soLuong1;
			entity.DVTLuong1 = dVTLuong1;
			entity.SoLuong2 = soLuong2;
			entity.DVTLuong2 = dVTLuong2;
			entity.TriGiaHoaDon = triGiaHoaDon;
			entity.DonGiaHoaDon = donGiaHoaDon;
			entity.MaTTDonGia = maTTDonGia;
			entity.DVTDonGia = dVTDonGia;
			entity.MaBieuThueNK = maBieuThueNK;
			entity.MaHanNgach = maHanNgach;
			entity.MaThueNKTheoLuong = maThueNKTheoLuong;
			entity.MaMienGiamThue = maMienGiamThue;
			entity.SoTienGiamThue = soTienGiamThue;
			entity.TriGiaTinhThue = triGiaTinhThue;
			entity.MaTTTriGiaTinhThue = maTTTriGiaTinhThue;
			entity.SoMucKhaiKhoanDC = soMucKhaiKhoanDC;
			entity.SoTTDongHangTKTNTX = soTTDongHangTKTNTX;
			entity.SoDMMienThue = soDMMienThue;
			entity.SoDongDMMienThue = soDongDMMienThue;
			entity.MaMienGiam = maMienGiam;
			entity.SoTienMienGiam = soTienMienGiam;
			entity.MaTTSoTienMienGiam = maTTSoTienMienGiam;
			entity.MaVanBanPhapQuyKhac1 = maVanBanPhapQuyKhac1;
			entity.MaVanBanPhapQuyKhac2 = maVanBanPhapQuyKhac2;
			entity.MaVanBanPhapQuyKhac3 = maVanBanPhapQuyKhac3;
			entity.MaVanBanPhapQuyKhac4 = maVanBanPhapQuyKhac4;
			entity.MaVanBanPhapQuyKhac5 = maVanBanPhapQuyKhac5;
			entity.SoDong = soDong;
			entity.MaPhanLoaiTaiXacNhanGia = maPhanLoaiTaiXacNhanGia;
			entity.TenNoiXuatXu = tenNoiXuatXu;
			entity.SoLuongTinhThue = soLuongTinhThue;
			entity.MaDVTDanhThue = maDVTDanhThue;
			entity.DonGiaTinhThue = donGiaTinhThue;
			entity.DV_SL_TrongDonGiaTinhThue = dV_SL_TrongDonGiaTinhThue;
			entity.MaTTDonGiaTinhThue = maTTDonGiaTinhThue;
			entity.TriGiaTinhThueS = triGiaTinhThueS;
			entity.MaTTTriGiaTinhThueS = maTTTriGiaTinhThueS;
			entity.MaTTSoTienMienGiam1 = maTTSoTienMienGiam1;
			entity.MaPhanLoaiThueSuatThue = maPhanLoaiThueSuatThue;
			entity.ThueSuatThue = thueSuatThue;
			entity.PhanLoaiThueSuatThue = phanLoaiThueSuatThue;
			entity.SoTienThue = soTienThue;
			entity.MaTTSoTienThueXuatKhau = maTTSoTienThueXuatKhau;
			entity.TienLePhi_DonGia = tienLePhi_DonGia;
			entity.TienBaoHiem_DonGia = tienBaoHiem_DonGia;
			entity.TienLePhi_SoLuong = tienLePhi_SoLuong;
			entity.TienLePhi_MaDVSoLuong = tienLePhi_MaDVSoLuong;
			entity.TienBaoHiem_SoLuong = tienBaoHiem_SoLuong;
			entity.TienBaoHiem_MaDVSoLuong = tienBaoHiem_MaDVSoLuong;
			entity.TienLePhi_KhoanTien = tienLePhi_KhoanTien;
			entity.TienBaoHiem_KhoanTien = tienBaoHiem_KhoanTien;
			entity.DieuKhoanMienGiam = dieuKhoanMienGiam;
			entity.MaHangHoa = maHangHoa;
			entity.Templ_1 = templ_1;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
			db.AddInParameter(dbCommand, "@MaQuanLy", SqlDbType.VarChar, MaQuanLy);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, ThueSuat);
			db.AddInParameter(dbCommand, "@ThueSuatTuyetDoi", SqlDbType.Decimal, ThueSuatTuyetDoi);
			db.AddInParameter(dbCommand, "@MaDVTTuyetDoi", SqlDbType.VarChar, MaDVTTuyetDoi);
			db.AddInParameter(dbCommand, "@MaTTTuyetDoi", SqlDbType.VarChar, MaTTTuyetDoi);
			db.AddInParameter(dbCommand, "@NuocXuatXu", SqlDbType.VarChar, NuocXuatXu);
			db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
			db.AddInParameter(dbCommand, "@DVTLuong1", SqlDbType.VarChar, DVTLuong1);
			db.AddInParameter(dbCommand, "@SoLuong2", SqlDbType.Decimal, SoLuong2);
			db.AddInParameter(dbCommand, "@DVTLuong2", SqlDbType.VarChar, DVTLuong2);
			db.AddInParameter(dbCommand, "@TriGiaHoaDon", SqlDbType.Decimal, TriGiaHoaDon);
			db.AddInParameter(dbCommand, "@DonGiaHoaDon", SqlDbType.Decimal, DonGiaHoaDon);
			db.AddInParameter(dbCommand, "@MaTTDonGia", SqlDbType.VarChar, MaTTDonGia);
			db.AddInParameter(dbCommand, "@DVTDonGia", SqlDbType.VarChar, DVTDonGia);
			db.AddInParameter(dbCommand, "@MaBieuThueNK", SqlDbType.VarChar, MaBieuThueNK);
			db.AddInParameter(dbCommand, "@MaHanNgach", SqlDbType.VarChar, MaHanNgach);
			db.AddInParameter(dbCommand, "@MaThueNKTheoLuong", SqlDbType.VarChar, MaThueNKTheoLuong);
			db.AddInParameter(dbCommand, "@MaMienGiamThue", SqlDbType.VarChar, MaMienGiamThue);
			db.AddInParameter(dbCommand, "@SoTienGiamThue", SqlDbType.Decimal, SoTienGiamThue);
			db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Decimal, TriGiaTinhThue);
			db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, MaTTTriGiaTinhThue);
			db.AddInParameter(dbCommand, "@SoMucKhaiKhoanDC", SqlDbType.VarChar, SoMucKhaiKhoanDC);
			db.AddInParameter(dbCommand, "@SoTTDongHangTKTNTX", SqlDbType.VarChar, SoTTDongHangTKTNTX);
			db.AddInParameter(dbCommand, "@SoDMMienThue", SqlDbType.VarChar, SoDMMienThue);
			db.AddInParameter(dbCommand, "@SoDongDMMienThue", SqlDbType.VarChar, SoDongDMMienThue);
			db.AddInParameter(dbCommand, "@MaMienGiam", SqlDbType.VarChar, MaMienGiam);
			db.AddInParameter(dbCommand, "@SoTienMienGiam", SqlDbType.Decimal, SoTienMienGiam);
			db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, MaTTSoTienMienGiam);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac1", SqlDbType.VarChar, MaVanBanPhapQuyKhac1);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac2", SqlDbType.VarChar, MaVanBanPhapQuyKhac2);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac3", SqlDbType.VarChar, MaVanBanPhapQuyKhac3);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac4", SqlDbType.VarChar, MaVanBanPhapQuyKhac4);
			db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac5", SqlDbType.VarChar, MaVanBanPhapQuyKhac5);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, MaPhanLoaiTaiXacNhanGia);
			db.AddInParameter(dbCommand, "@TenNoiXuatXu", SqlDbType.VarChar, TenNoiXuatXu);
			db.AddInParameter(dbCommand, "@SoLuongTinhThue", SqlDbType.Decimal, SoLuongTinhThue);
			db.AddInParameter(dbCommand, "@MaDVTDanhThue", SqlDbType.VarChar, MaDVTDanhThue);
			db.AddInParameter(dbCommand, "@DonGiaTinhThue", SqlDbType.Decimal, DonGiaTinhThue);
			db.AddInParameter(dbCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, DV_SL_TrongDonGiaTinhThue);
			db.AddInParameter(dbCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, MaTTDonGiaTinhThue);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, TriGiaTinhThueS);
			db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, MaTTTriGiaTinhThueS);
			db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam1", SqlDbType.VarChar, MaTTSoTienMienGiam1);
			db.AddInParameter(dbCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, MaPhanLoaiThueSuatThue);
			db.AddInParameter(dbCommand, "@ThueSuatThue", SqlDbType.VarChar, ThueSuatThue);
			db.AddInParameter(dbCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, PhanLoaiThueSuatThue);
			db.AddInParameter(dbCommand, "@SoTienThue", SqlDbType.Decimal, SoTienThue);
			db.AddInParameter(dbCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, MaTTSoTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, TienLePhi_DonGia);
			db.AddInParameter(dbCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, TienBaoHiem_DonGia);
			db.AddInParameter(dbCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, TienLePhi_SoLuong);
			db.AddInParameter(dbCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, TienLePhi_MaDVSoLuong);
			db.AddInParameter(dbCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, TienBaoHiem_SoLuong);
			db.AddInParameter(dbCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, TienBaoHiem_MaDVSoLuong);
			db.AddInParameter(dbCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, TienLePhi_KhoanTien);
			db.AddInParameter(dbCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, TienBaoHiem_KhoanTien);
			db.AddInParameter(dbCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, DieuKhoanMienGiam);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@Templ_1", SqlDbType.VarChar, Templ_1);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
        public int UpdateSynDataVNACCS(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
            db.AddInParameter(dbCommand, "@MaQuanLy", SqlDbType.VarChar, MaQuanLy);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, ThueSuat);
            db.AddInParameter(dbCommand, "@ThueSuatTuyetDoi", SqlDbType.Decimal, ThueSuatTuyetDoi);
            db.AddInParameter(dbCommand, "@MaDVTTuyetDoi", SqlDbType.VarChar, MaDVTTuyetDoi);
            db.AddInParameter(dbCommand, "@MaTTTuyetDoi", SqlDbType.VarChar, MaTTTuyetDoi);
            db.AddInParameter(dbCommand, "@NuocXuatXu", SqlDbType.VarChar, NuocXuatXu);
            db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
            db.AddInParameter(dbCommand, "@DVTLuong1", SqlDbType.VarChar, DVTLuong1);
            db.AddInParameter(dbCommand, "@SoLuong2", SqlDbType.Decimal, SoLuong2);
            db.AddInParameter(dbCommand, "@DVTLuong2", SqlDbType.VarChar, DVTLuong2);
            db.AddInParameter(dbCommand, "@TriGiaHoaDon", SqlDbType.Decimal, TriGiaHoaDon);
            db.AddInParameter(dbCommand, "@DonGiaHoaDon", SqlDbType.Decimal, DonGiaHoaDon);
            db.AddInParameter(dbCommand, "@MaTTDonGia", SqlDbType.VarChar, MaTTDonGia);
            db.AddInParameter(dbCommand, "@DVTDonGia", SqlDbType.VarChar, DVTDonGia);
            db.AddInParameter(dbCommand, "@MaBieuThueNK", SqlDbType.VarChar, MaBieuThueNK);
            db.AddInParameter(dbCommand, "@MaHanNgach", SqlDbType.VarChar, MaHanNgach);
            db.AddInParameter(dbCommand, "@MaThueNKTheoLuong", SqlDbType.VarChar, MaThueNKTheoLuong);
            db.AddInParameter(dbCommand, "@MaMienGiamThue", SqlDbType.VarChar, MaMienGiamThue);
            db.AddInParameter(dbCommand, "@SoTienGiamThue", SqlDbType.Decimal, SoTienGiamThue);
            db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Decimal, TriGiaTinhThue);
            db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, MaTTTriGiaTinhThue);
            db.AddInParameter(dbCommand, "@SoMucKhaiKhoanDC", SqlDbType.VarChar, SoMucKhaiKhoanDC);
            db.AddInParameter(dbCommand, "@SoTTDongHangTKTNTX", SqlDbType.VarChar, SoTTDongHangTKTNTX);
            db.AddInParameter(dbCommand, "@SoDMMienThue", SqlDbType.VarChar, SoDMMienThue);
            db.AddInParameter(dbCommand, "@SoDongDMMienThue", SqlDbType.VarChar, SoDongDMMienThue);
            db.AddInParameter(dbCommand, "@MaMienGiam", SqlDbType.VarChar, MaMienGiam);
            db.AddInParameter(dbCommand, "@SoTienMienGiam", SqlDbType.Decimal, SoTienMienGiam);
            db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam", SqlDbType.VarChar, MaTTSoTienMienGiam);
            db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac1", SqlDbType.VarChar, MaVanBanPhapQuyKhac1);
            db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac2", SqlDbType.VarChar, MaVanBanPhapQuyKhac2);
            db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac3", SqlDbType.VarChar, MaVanBanPhapQuyKhac3);
            db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac4", SqlDbType.VarChar, MaVanBanPhapQuyKhac4);
            db.AddInParameter(dbCommand, "@MaVanBanPhapQuyKhac5", SqlDbType.VarChar, MaVanBanPhapQuyKhac5);
            db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
            db.AddInParameter(dbCommand, "@MaPhanLoaiTaiXacNhanGia", SqlDbType.VarChar, MaPhanLoaiTaiXacNhanGia);
            db.AddInParameter(dbCommand, "@TenNoiXuatXu", SqlDbType.VarChar, TenNoiXuatXu);
            db.AddInParameter(dbCommand, "@SoLuongTinhThue", SqlDbType.Decimal, SoLuongTinhThue);
            db.AddInParameter(dbCommand, "@MaDVTDanhThue", SqlDbType.VarChar, MaDVTDanhThue);
            db.AddInParameter(dbCommand, "@DonGiaTinhThue", SqlDbType.Decimal, DonGiaTinhThue);
            db.AddInParameter(dbCommand, "@DV_SL_TrongDonGiaTinhThue", SqlDbType.VarChar, DV_SL_TrongDonGiaTinhThue);
            db.AddInParameter(dbCommand, "@MaTTDonGiaTinhThue", SqlDbType.VarChar, MaTTDonGiaTinhThue);
            db.AddInParameter(dbCommand, "@TriGiaTinhThueS", SqlDbType.Decimal, TriGiaTinhThueS);
            db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThueS", SqlDbType.VarChar, MaTTTriGiaTinhThueS);
            db.AddInParameter(dbCommand, "@MaTTSoTienMienGiam1", SqlDbType.VarChar, MaTTSoTienMienGiam1);
            db.AddInParameter(dbCommand, "@MaPhanLoaiThueSuatThue", SqlDbType.VarChar, MaPhanLoaiThueSuatThue);
            db.AddInParameter(dbCommand, "@ThueSuatThue", SqlDbType.VarChar, ThueSuatThue);
            db.AddInParameter(dbCommand, "@PhanLoaiThueSuatThue", SqlDbType.VarChar, PhanLoaiThueSuatThue);
            db.AddInParameter(dbCommand, "@SoTienThue", SqlDbType.Decimal, SoTienThue);
            db.AddInParameter(dbCommand, "@MaTTSoTienThueXuatKhau", SqlDbType.VarChar, MaTTSoTienThueXuatKhau);
            db.AddInParameter(dbCommand, "@TienLePhi_DonGia", SqlDbType.VarChar, TienLePhi_DonGia);
            db.AddInParameter(dbCommand, "@TienBaoHiem_DonGia", SqlDbType.VarChar, TienBaoHiem_DonGia);
            db.AddInParameter(dbCommand, "@TienLePhi_SoLuong", SqlDbType.Decimal, TienLePhi_SoLuong);
            db.AddInParameter(dbCommand, "@TienLePhi_MaDVSoLuong", SqlDbType.VarChar, TienLePhi_MaDVSoLuong);
            db.AddInParameter(dbCommand, "@TienBaoHiem_SoLuong", SqlDbType.Decimal, TienBaoHiem_SoLuong);
            db.AddInParameter(dbCommand, "@TienBaoHiem_MaDVSoLuong", SqlDbType.VarChar, TienBaoHiem_MaDVSoLuong);
            db.AddInParameter(dbCommand, "@TienLePhi_KhoanTien", SqlDbType.Decimal, TienLePhi_KhoanTien);
            db.AddInParameter(dbCommand, "@TienBaoHiem_KhoanTien", SqlDbType.Decimal, TienBaoHiem_KhoanTien);
            db.AddInParameter(dbCommand, "@DieuKhoanMienGiam", SqlDbType.VarChar, DieuKhoanMienGiam);
            db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
            db.AddInParameter(dbCommand, "@Templ_1", SqlDbType.VarChar, Templ_1);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }		
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_HangMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_HangMauDich(long id)
		{
			KDT_VNACC_HangMauDich entity = new KDT_VNACC_HangMauDich();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_HangMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}