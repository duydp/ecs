
DELETE FROM t_VNACC_Category_CustomsOffice WHERE TableID='A038A'
---------------------------------BẢNG MÃ CHI CỤC HẢI QUAN DÙNG TRONG VNACCS --------------------------------------
IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '50BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TINHBIENAG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Tịnh Biên' WHERE TableID='A038A' AND CustomsCode = '50BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','50BB','TINHBIENAG',N'Chi cục HQ CK Tịnh Biên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '50BC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HOIDONGAG', CustomsOfficeNameInVietnamese =N'Chi cục HQ Vĩnh Hội Đông' WHERE TableID='A038A' AND CustomsCode = '50BC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','50BC','HOIDONGAG',N'Chi cục HQ Vĩnh Hội Đông')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '50BD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'VXUONGAG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Vĩnh Xương' WHERE TableID='A038A' AND CustomsCode = '50BD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','50BD','VXUONGAG',N'Chi cục HQ CK Vĩnh Xương')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '50BJ')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'BACDAIAG', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Đai' WHERE TableID='A038A' AND CustomsCode = '50BJ'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','50BJ','BACDAIAG',N'Chi cục HQ Bắc Đai')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '50BK')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KBINHAG', CustomsOfficeNameInVietnamese =N'Chi cục HQ Khánh Bình' WHERE TableID='A038A' AND CustomsCode = '50BK'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','50BK','KBINHAG',N'Chi cục HQ Khánh Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '50CE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CMYTHOIAG', CustomsOfficeNameInVietnamese =N'Chi cục HQ Cảng Mỹ Thới' WHERE TableID='A038A' AND CustomsCode = '50CE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','50CE','CMYTHOIAG',N'Chi cục HQ Cảng Mỹ Thới')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '51BE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCATLOVT', CustomsOfficeNameInVietnamese =N'Chi cục HQ Cảng Cát Lở' WHERE TableID='A038A' AND CustomsCode = '51BE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','51BE','CCATLOVT',N'Chi cục HQ Cảng Cát Lở')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '51C1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KNQPMVTAU', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Phú Mỹ' WHERE TableID='A038A' AND CustomsCode = '51C1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','51C1','KNQPMVTAU',N'Chi cục HQ CK Cảng Phú Mỹ')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '51C2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'PSAPMVTAU', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Phú Mỹ' WHERE TableID='A038A' AND CustomsCode = '51C2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','51C2','PSAPMVTAU',N'Chi cục HQ CK Cảng Phú Mỹ')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '51CB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CSANBAYVT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu' WHERE TableID='A038A' AND CustomsCode = '51CB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','51CB','CSANBAYVT',N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '51CH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CONDAOVT', CustomsOfficeNameInVietnamese =N'Chi cục HQ Côn Đảo' WHERE TableID='A038A' AND CustomsCode = '51CH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','51CH','CONDAOVT',N'Chi cục HQ Côn Đảo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '51CI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCAIMEPVT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK cảng Cái Mép' WHERE TableID='A038A' AND CustomsCode = '51CI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','51CI','CCAIMEPVT',N'Chi cục HQ CK cảng Cái Mép')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '18A1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DKCNYPBN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Ninh' WHERE TableID='A038A' AND CustomsCode = '18A1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','18A1','DKCNYPBN',N'Chi cục HQ Bắc Ninh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '18A2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DKCNQVBN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Ninh' WHERE TableID='A038A' AND CustomsCode = '18A2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','18A2','DKCNQVBN',N'Chi cục HQ Bắc Ninh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '18A3')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVCCHQBN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Ninh' WHERE TableID='A038A' AND CustomsCode = '18A3'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','18A3','DNVCCHQBN',N'Chi cục HQ Bắc Ninh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '18B1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVTNBNINH', CustomsOfficeNameInVietnamese =N'Chi cục HQ Thái Nguyên' WHERE TableID='A038A' AND CustomsCode = '18B1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','18B1','DNVTNBNINH',N'Chi cục HQ Thái Nguyên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '18B2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'YBINHTNBN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Thái Nguyên' WHERE TableID='A038A' AND CustomsCode = '18B2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','18B2','YBINHTNBN',N'Chi cục HQ Thái Nguyên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '18BC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'BACGIANGBN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý các KCN Bắc Giang' WHERE TableID='A038A' AND CustomsCode = '18BC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','18BC','BACGIANGBN',N'Chi cục HQ Quản lý các KCN Bắc Giang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '18ID')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TIENSONBN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Cảng nội địa Tiên Sơn' WHERE TableID='A038A' AND CustomsCode = '18ID'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','18ID','TIENSONBN',N'Chi cục HQ Cảng nội địa Tiên Sơn')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '37CB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'QUINHONBD', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Qui Nhơn' WHERE TableID='A038A' AND CustomsCode = '37CB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','37CB','QUINHONBD',N'Chi cục HQ CK Cảng Qui Nhơn')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '37TC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'PHUYENBD', CustomsOfficeNameInVietnamese =N'Chi cục HQ Phú Yên' WHERE TableID='A038A' AND CustomsCode = '37TC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','37TC','PHUYENBD',N'Chi cục HQ Phú Yên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '43CN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CTHOPBD', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng tổng hợp Bình Dương' WHERE TableID='A038A' AND CustomsCode = '43CN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','43CN','CTHOPBD',N'Chi cục HQ CK Cảng tổng hợp Bình Dương')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '43IH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'SONGTHANBD', CustomsOfficeNameInVietnamese =N'Chi cục HQ Sóng Thần' WHERE TableID='A038A' AND CustomsCode = '43IH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','43IH','SONGTHANBD',N'Chi cục HQ Sóng Thần')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '43K1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVCCMPBD', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Mỹ Phước' WHERE TableID='A038A' AND CustomsCode = '43K1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','43K1','DNVCCMPBD',N'Chi cục HQ KCN Mỹ Phước')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '43K2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DKLHCCMPBD', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Mỹ Phước' WHERE TableID='A038A' AND CustomsCode = '43K2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','43K2','DKLHCCMPBD',N'Chi cục HQ KCN Mỹ Phước')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '43K3')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DTDCCMPBD', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Mỹ Phước' WHERE TableID='A038A' AND CustomsCode = '43K3'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','43K3','DTDCCMPBD',N'Chi cục HQ KCN Mỹ Phước')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '43ND')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KCNSTHANBD', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Sóng Thần' WHERE TableID='A038A' AND CustomsCode = '43ND'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','43ND','KCNSTHANBD',N'Chi cục HQ KCN Sóng Thần')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '43NF')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KCNVNSGBD', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Việt Nam - Singapore' WHERE TableID='A038A' AND CustomsCode = '43NF'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','43NF','KCNVNSGBD',N'Chi cục HQ KCN Việt Nam - Singapore')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '43NG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'VHUONGBD', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Việt Hương' WHERE TableID='A038A' AND CustomsCode = '43NG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','43NG','VHUONGBD',N'Chi cục HQ KCN Việt Hương')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '43PB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'NGOAIKCNBD', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng hóa XNK ngoài KCN' WHERE TableID='A038A' AND CustomsCode = '43PB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','43PB','NGOAIKCNBD',N'Chi cục HQ Quản lý hàng hóa XNK ngoài KCN')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '61BA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HOALUBP', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Hoa Lư' WHERE TableID='A038A' AND CustomsCode = '61BA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','61BA','HOALUBP',N'Chi cục HQ CK Quốc tế Hoa Lư')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '61BA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HOALUBP', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Hoa Lư' WHERE TableID='A038A' AND CustomsCode = '61BA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','61BA','HOALUBP',N'Chi cục HQ CK Quốc tế Hoa Lư')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '61PA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CTHANHBP', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chơn Thành' WHERE TableID='A038A' AND CustomsCode = '61PA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','61PA','CTHANHBP',N'Chi cục HQ Chơn Thành')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '61PA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CTHANHBP', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chơn Thành' WHERE TableID='A038A' AND CustomsCode = '61PA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','61PA','CTHANHBP',N'Chi cục HQ Chơn Thành')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '61BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HDIEUBP', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Hoàng Diệu' WHERE TableID='A038A' AND CustomsCode = '61BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','61BB','HDIEUBP',N'Chi cục HQ CK Hoàng Diệu')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '61BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HDIEUBP', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Hoàng Diệu' WHERE TableID='A038A' AND CustomsCode = '61BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','61BB','HDIEUBP',N'Chi cục HQ CK Hoàng Diệu')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '59BD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HOATRUNGCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hòa Trung' WHERE TableID='A038A' AND CustomsCode = '59BD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','59BD','HOATRUNGCM',N'Chi cục HQ Hòa Trung')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '59CB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CNAMCANCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Năm Căn' WHERE TableID='A038A' AND CustomsCode = '59CB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','59CB','CNAMCANCM',N'Chi cục HQ CK Cảng Năm Căn')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '54CB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CANGCANTHO', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Cần Thơ' WHERE TableID='A038A' AND CustomsCode = '54CB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','54CB','CANGCANTHO',N'Chi cục HQ CK Cảng Cần Thơ')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '54CD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'VINHLONGCT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Vĩnh Long' WHERE TableID='A038A' AND CustomsCode = '54CD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','54CD','VINHLONGCT',N'Chi cục HQ CK Vĩnh Long')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '54PH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TAYDOCT', CustomsOfficeNameInVietnamese =N'Chi cục HQ Tây Đô' WHERE TableID='A038A' AND CustomsCode = '54PH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','54PH','TAYDOCT',N'Chi cục HQ Tây Đô')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '54PK')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'SOCTRANGCT', CustomsOfficeNameInVietnamese =N'Chi cục HQ Sóc Trăng' WHERE TableID='A038A' AND CustomsCode = '54PK'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','54PK','SOCTRANGCT',N'Chi cục HQ Sóc Trăng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '11B1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVTLCB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Tà Lùng' WHERE TableID='A038A' AND CustomsCode = '11B1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','11B1','DNVTLCB',N'Chi cục HQ CK Tà Lùng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '11B2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNV2TLCB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Tà Lùng' WHERE TableID='A038A' AND CustomsCode = '11B2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','11B2','DNV2TLCB',N'Chi cục HQ CK Tà Lùng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '11BE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TRALINHCB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Trà Lĩnh' WHERE TableID='A038A' AND CustomsCode = '11BE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','11BE','TRALINHCB',N'Chi cục HQ CK Trà Lĩnh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '11BF')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'SOCGIANGCB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sóc Giang' WHERE TableID='A038A' AND CustomsCode = '11BF'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','11BF','SOCGIANGCB',N'Chi cục HQ CK Sóc Giang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '11BH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'POPEOCB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Pò Peo' WHERE TableID='A038A' AND CustomsCode = '11BH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','11BH','POPEOCB',N'Chi cục HQ CK Pò Peo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '11G1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVBHCBANG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Bí Hà' WHERE TableID='A038A' AND CustomsCode = '11G1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','11G1','DNVBHCBANG',N'Chi cục HQ CK Bí Hà')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '11G2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DLVBHCBANG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Bí Hà' WHERE TableID='A038A' AND CustomsCode = '11G2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','11G2','DLVBHCBANG',N'Chi cục HQ CK Bí Hà')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '11PK')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'BACKANCB', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Kạn' WHERE TableID='A038A' AND CustomsCode = '11PK'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','11PK','BACKANCB',N'Chi cục HQ Bắc Kạn')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '34AB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'SBQTDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng' WHERE TableID='A038A' AND CustomsCode = '34AB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','34AB','SBQTDN',N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '34AB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'SBQTDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng' WHERE TableID='A038A' AND CustomsCode = '34AB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','34AB','SBQTDN',N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '34CC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DTGCDANANG', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE TableID='A038A' AND CustomsCode = '34CC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','34CC','DTGCDANANG',N'Chi cục HQ Quản lý hàng đầu tư - gia công')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '34CE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CANGDANANG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Đà Nẵng' WHERE TableID='A038A' AND CustomsCode = '34CE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','34CE','CANGDANANG',N'Chi cục HQ CK Cảng Đà Nẵng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '34NG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HKHANHDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Hòa Khánh - Liên Chiểu' WHERE TableID='A038A' AND CustomsCode = '34NG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','34NG','HKHANHDN',N'Chi cục HQ KCN Hòa Khánh - Liên Chiểu')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '34NH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KCNDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Đà Nẵng' WHERE TableID='A038A' AND CustomsCode = '34NH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','34NH','KCNDN',N'Chi cục HQ KCN Đà Nẵng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '40B1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVIBRDL', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK BupRăng' WHERE TableID='A038A' AND CustomsCode = '40B1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','40B1','DNVIBRDL',N'Chi cục HQ CK BupRăng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '40BC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'BMTHUOTDL', CustomsOfficeNameInVietnamese =N'Chi cục HQ Buôn Mê Thuột' WHERE TableID='A038A' AND CustomsCode = '40BC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','40BC','BMTHUOTDL',N'Chi cục HQ Buôn Mê Thuột')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '40D1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVDLATDL', CustomsOfficeNameInVietnamese =N'Chi cục HQ Đà Lạt' WHERE TableID='A038A' AND CustomsCode = '40D1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','40D1','DNVDLATDL',N'Chi cục HQ Đà Lạt')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '12B1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVTTRGDB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Tây Trang' WHERE TableID='A038A' AND CustomsCode = '12B1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','12B1','DNVTTRGDB',N'Chi cục HQ CK Quốc tế Tây Trang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '12B2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DHPUOCTTDB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Tây Trang' WHERE TableID='A038A' AND CustomsCode = '12B2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','12B2','DHPUOCTTDB',N'Chi cục HQ CK Quốc tế Tây Trang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '12BE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'LONGSAPDB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Lóng Sập' WHERE TableID='A038A' AND CustomsCode = '12BE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','12BE','LONGSAPDB',N'Chi cục HQ CK Lóng Sập')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '12BI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CKHUONGDB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Chiềng Khương' WHERE TableID='A038A' AND CustomsCode = '12BI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','12BI','CKHUONGDB',N'Chi cục HQ CK Chiềng Khương')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '12F1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HQSONLADB', CustomsOfficeNameInVietnamese =N'Chi cục HQ Sơn La' WHERE TableID='A038A' AND CustomsCode = '12F1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','12F1','HQSONLADB',N'Chi cục HQ Sơn La')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '12F2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'NACAISLDB', CustomsOfficeNameInVietnamese =N'Chi cục HQ Sơn La' WHERE TableID='A038A' AND CustomsCode = '12F2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','12F2','NACAISLDB',N'Chi cục HQ Sơn La')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '12H1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVMLTDB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Ma Lu Thàng' WHERE TableID='A038A' AND CustomsCode = '12H1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','12H1','DNVMLTDB',N'Chi cục HQ CK Ma Lu Thàng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '12H2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DPOTOMLTDB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Ma Lu Thàng' WHERE TableID='A038A' AND CustomsCode = '12H2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','12H2','DPOTOMLTDB',N'Chi cục HQ CK Ma Lu Thàng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '47D1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVCCLTDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Long Thành' WHERE TableID='A038A' AND CustomsCode = '47D1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','47D1','DNVCCLTDN',N'Chi cục HQ Long Thành')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '47D2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNV2CCLTDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Long Thành' WHERE TableID='A038A' AND CustomsCode = '47D2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','47D2','DNV2CCLTDN',N'Chi cục HQ Long Thành')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '47D3')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNV3CCLTDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Long Thành' WHERE TableID='A038A' AND CustomsCode = '47D3'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','47D3','DNV3CCLTDN',N'Chi cục HQ Long Thành')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '47I1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVLBTDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Long Bình Tân' WHERE TableID='A038A' AND CustomsCode = '47I1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','47I1','DNVLBTDN',N'Chi cục HQ Long Bình Tân')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '47I2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNV2LBTDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Long Bình Tân' WHERE TableID='A038A' AND CustomsCode = '47I2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','47I2','DNV2LBTDN',N'Chi cục HQ Long Bình Tân')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '47NB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'BIENHOADN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Biên Hoà' WHERE TableID='A038A' AND CustomsCode = '47NB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','47NB','BIENHOADN',N'Chi cục HQ Biên Hoà')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '47NB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'BIENHOADN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Biên Hoà' WHERE TableID='A038A' AND CustomsCode = '47NB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','47NB','BIENHOADN',N'Chi cục HQ Biên Hoà')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '47NF')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TNHATDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Thống Nhất' WHERE TableID='A038A' AND CustomsCode = '47NF'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','47NF','TNHATDN',N'Chi cục HQ Thống Nhất')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '47NG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'NTRACHDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Nhơn Trạch' WHERE TableID='A038A' AND CustomsCode = '47NG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','47NG','NTRACHDN',N'Chi cục HQ Nhơn Trạch')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '47NM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'BTHUANDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ QL KCN Bình Thuận' WHERE TableID='A038A' AND CustomsCode = '47NM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','47NM','BTHUANDN',N'Chi cục HQ QL KCN Bình Thuận')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '47XE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KCXLBINHDN', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX Long Bình' WHERE TableID='A038A' AND CustomsCode = '47XE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','47XE','KCXLBINHDN',N'Chi cục HQ KCX Long Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '49BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'THPHUOCDT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Thường Phước' WHERE TableID='A038A' AND CustomsCode = '49BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','49BB','THPHUOCDT',N'Chi cục HQ CK Thường Phước')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '49BE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'SOTHUONGDT', CustomsOfficeNameInVietnamese =N'Chi cục HQ Sở Thượng' WHERE TableID='A038A' AND CustomsCode = '49BE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','49BE','SOTHUONGDT',N'Chi cục HQ Sở Thượng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '49BF')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'THBINHDT', CustomsOfficeNameInVietnamese =N'Chi cục HQ Thông Bình' WHERE TableID='A038A' AND CustomsCode = '49BF'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','49BF','THBINHDT',N'Chi cục HQ Thông Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '49BG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DINHBADT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Dinh Bà' WHERE TableID='A038A' AND CustomsCode = '49BG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','49BG','DINHBADT',N'Chi cục HQ CK Dinh Bà')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '49C1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CAOLANHCDT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Đồng Tháp' WHERE TableID='A038A' AND CustomsCode = '49C1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','49C1','CAOLANHCDT',N'Chi cục HQ CK Cảng Đồng Tháp')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '49C2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'SADECCDT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Đồng Tháp' WHERE TableID='A038A' AND CustomsCode = '49C2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','49C2','SADECCDT',N'Chi cục HQ CK Cảng Đồng Tháp')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '38B1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVTHLTGL', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Lệ Thanh' WHERE TableID='A038A' AND CustomsCode = '38B1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','38B1','DNVTHLTGL',N'Chi cục HQ CK Lệ Thanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '38B2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DTTLTGL', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Lệ Thanh' WHERE TableID='A038A' AND CustomsCode = '38B2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','38B2','DTTLTGL',N'Chi cục HQ CK Lệ Thanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '38BC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'BOYGL', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Bờ Y' WHERE TableID='A038A' AND CustomsCode = '38BC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','38BC','BOYGL',N'Chi cục HQ CK Bờ Y')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '38PD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KONTUMGL', CustomsOfficeNameInVietnamese =N'Chi cục HQ Kon Tum' WHERE TableID='A038A' AND CustomsCode = '38PD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','38PD','KONTUMGL',N'Chi cục HQ Kon Tum')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '10BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TTHUYHG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Thanh Thủy' WHERE TableID='A038A' AND CustomsCode = '10BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','10BB','TTHUYHG',N'Chi cục HQ CK Thanh Thủy')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '10BC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'XINMANHG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Xín Mần' WHERE TableID='A038A' AND CustomsCode = '10BC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','10BC','XINMANHG',N'Chi cục HQ CK Xín Mần')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '10BD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'PHOBANGHG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Phó Bảng' WHERE TableID='A038A' AND CustomsCode = '10BD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','10BD','PHOBANGHG',N'Chi cục HQ CK Phó Bảng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '10BF')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'SAMPUNHG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Săm Pun' WHERE TableID='A038A' AND CustomsCode = '10BF'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','10BF','SAMPUNHG',N'Chi cục HQ CK Săm Pun')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '10BI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TQUANGHG', CustomsOfficeNameInVietnamese =N'Chi cục HQ Tuyên Quang' WHERE TableID='A038A' AND CustomsCode = '10BI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','10BI','TQUANGHG',N'Chi cục HQ Tuyên Quang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01B1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DHHXNBHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE TableID='A038A' AND CustomsCode = '01B1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01B1','DHHXNBHN',N'Chi cục HQ CK Sân bay quốc tế Nội Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01B3')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DHHNNBHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE TableID='A038A' AND CustomsCode = '01B3'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01B3','DHHNNBHN',N'Chi cục HQ CK Sân bay quốc tế Nội Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01B6')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DHLNKNBHN ', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE TableID='A038A' AND CustomsCode = '01B6'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01B6','DHLNKNBHN ',N'Chi cục HQ CK Sân bay quốc tế Nội Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01B5')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DHLXKNBHN ', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE TableID='A038A' AND CustomsCode = '01B5'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01B5','DHLXKNBHN ',N'Chi cục HQ CK Sân bay quốc tế Nội Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01BT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'YENBAIHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Yên Bái' WHERE TableID='A038A' AND CustomsCode = '01BT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01BT','YENBAIHN',N'Chi cục HQ Yên Bái')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01E1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVBHNHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Hà Nội' WHERE TableID='A038A' AND CustomsCode = '01E1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01E1','DNVBHNHN',N'Chi cục HQ Bắc Hà Nội')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01E2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DHKBHNHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Hà Nội' WHERE TableID='A038A' AND CustomsCode = '01E2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01E2','DHKBHNHN',N'Chi cục HQ Bắc Hà Nội')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01IK')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'GIATHUYHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Gia Thụy' WHERE TableID='A038A' AND CustomsCode = '01IK'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01IK','GIATHUYHN',N'Chi cục HQ Gia Thụy')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01M1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DHDHTHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hà Tây' WHERE TableID='A038A' AND CustomsCode = '01M1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01M1','DHDHTHN',N'Chi cục HQ Hà Tây')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01M2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CNCHTHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hà Tây' WHERE TableID='A038A' AND CustomsCode = '01M2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01M2','CNCHTHN',N'Chi cục HQ Hà Tây')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01NV')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'BTLONGHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Bắc Thăng Long' WHERE TableID='A038A' AND CustomsCode = '01NV'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01NV','BTLONGHN',N'Chi cục HQ KCN Bắc Thăng Long')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01PJ')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'VIETTRIHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Phú Thọ' WHERE TableID='A038A' AND CustomsCode = '01PJ'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01PJ','VIETTRIHN',N'Chi cục HQ Phú Thọ')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01PL')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HQQLDTGCHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE TableID='A038A' AND CustomsCode = '01PL'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01PL','HQQLDTGCHN',N'Chi cục HQ Quản lý hàng đầu tư - gia công')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01PR')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'VINHPHUCHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Vĩnh Phúc' WHERE TableID='A038A' AND CustomsCode = '01PR'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01PR','VINHPHUCHN',N'Chi cục HQ Vĩnh Phúc')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01DD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCHQCPNHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ chuyển phát nhanh HN' WHERE TableID='A038A' AND CustomsCode = '01DD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01DD','CCHQCPNHN',N'Chi cục HQ chuyển phát nhanh HN')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01DD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCHQCPNHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ chuyển phát nhanh HN' WHERE TableID='A038A' AND CustomsCode = '01DD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01DD','CCHQCPNHN',N'Chi cục HQ chuyển phát nhanh HN')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01SI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'GAYVIENHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Ga đường sắt quốc tế Yên Viên' WHERE TableID='A038A' AND CustomsCode = '01SI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01SI','GAYVIENHN',N'Chi cục HQ Ga đường sắt quốc tế Yên Viên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '01PQ')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCHQHBHN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hòa Bình' WHERE TableID='A038A' AND CustomsCode = '01PQ'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','01PQ','CCHQHBHN',N'Chi cục HQ Hòa Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '30BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CAUTREOHT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Cầu Treo' WHERE TableID='A038A' AND CustomsCode = '30BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','30BB','CAUTREOHT',N'Chi cục HQ CK Quốc tế Cầu Treo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '30BE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HONGLINHHT', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hồng Lĩnh' WHERE TableID='A038A' AND CustomsCode = '30BE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','30BE','HONGLINHHT',N'Chi cục HQ Hồng Lĩnh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '30BI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KKTCTREOHT', CustomsOfficeNameInVietnamese =N'Chi cục HQ khu kinh tế CK Cầu Treo' WHERE TableID='A038A' AND CustomsCode = '30BI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','30BI','KKTCTREOHT',N'Chi cục HQ khu kinh tế CK Cầu Treo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '30CC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CXHAIHT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Xuân Hải' WHERE TableID='A038A' AND CustomsCode = '30CC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','30CC','CXHAIHT',N'Chi cục HQ CK Cảng Xuân Hải')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '30F1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVVANGHT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Vũng Áng' WHERE TableID='A038A' AND CustomsCode = '30F1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','30F1','DNVVANGHT',N'Chi cục HQ CK Cảng Vũng Áng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '30F2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'SDUONGVAHT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Vũng Áng' WHERE TableID='A038A' AND CustomsCode = '30F2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','30F2','SDUONGVAHT',N'Chi cục HQ CK Cảng Vũng Áng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '03CC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CANGHPKVI', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK cảng Hải Phòng KV I' WHERE TableID='A038A' AND CustomsCode = '03CC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','03CC','CANGHPKVI',N'Chi cục HQ CK cảng Hải Phòng KV I')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '03CD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'THAIBINHHP', CustomsOfficeNameInVietnamese =N'Chi cục HQ Thái Bình' WHERE TableID='A038A' AND CustomsCode = '03CD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','03CD','THAIBINHHP',N'Chi cục HQ Thái Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '03CE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CANGHPKVII', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK cảng Hải Phòng KV II' WHERE TableID='A038A' AND CustomsCode = '03CE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','03CE','CANGHPKVII',N'Chi cục HQ CK cảng Hải Phòng KV II')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '03EE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CDINHVUHP', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Đình Vũ' WHERE TableID='A038A' AND CustomsCode = '03EE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','03EE','CDINHVUHP',N'Chi cục HQ CK Cảng Đình Vũ')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '03NK')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KCXKCNHP', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX và KCN' WHERE TableID='A038A' AND CustomsCode = '03NK'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','03NK','KCXKCNHP',N'Chi cục HQ KCX và KCN')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '03PA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DTGCHP', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE TableID='A038A' AND CustomsCode = '03PA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','03PA','DTGCHP',N'Chi cục HQ Quản lý hàng đầu tư - gia công')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '03PA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DTGCHP', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE TableID='A038A' AND CustomsCode = '03PA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','03PA','DTGCHP',N'Chi cục HQ Quản lý hàng đầu tư - gia công')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '03PJ')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HAIDUONGHP', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hải Dương' WHERE TableID='A038A' AND CustomsCode = '03PJ'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','03PJ','HAIDUONGHP',N'Chi cục HQ Hải Dương')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '03PL')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HUNGYENHP', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hưng Yên' WHERE TableID='A038A' AND CustomsCode = '03PL'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','03PL','HUNGYENHP',N'Chi cục HQ Hưng Yên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '03TG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CHPKVIII', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK cảng Hải Phòng KV III' WHERE TableID='A038A' AND CustomsCode = '03TG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','03TG','CHPKVIII',N'Chi cục HQ CK cảng Hải Phòng KV III')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '03TG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CHPKVIII', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK cảng Hải Phòng KV III' WHERE TableID='A038A' AND CustomsCode = '03TG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','03TG','CHPKVIII',N'Chi cục HQ CK cảng Hải Phòng KV III')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '41BH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'NTHUANKH', CustomsOfficeNameInVietnamese =N'Chi cục HQ Ninh Thuận' WHERE TableID='A038A' AND CustomsCode = '41BH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','41BH','NTHUANKH',N'Chi cục HQ Ninh Thuận')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '41CB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'NHATRANGKH', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Nha Trang' WHERE TableID='A038A' AND CustomsCode = '41CB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','41CB','NHATRANGKH',N'Chi cục HQ CK Cảng Nha Trang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '41CC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CAMRANHKH', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Cam Ranh' WHERE TableID='A038A' AND CustomsCode = '41CC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','41CC','CAMRANHKH',N'Chi cục HQ CK Cảng Cam Ranh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '41AB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCHQCKSBCR', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK sân bay quốc tế Cam Ranh' WHERE TableID='A038A' AND CustomsCode = '41AB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','41AB','CCHQCKSBCR',N'Chi cục HQ CK sân bay quốc tế Cam Ranh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '41PE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'VANPHONGKH', CustomsOfficeNameInVietnamese =N'Chi cục HQ Vân Phong' WHERE TableID='A038A' AND CustomsCode = '41PE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','41PE','VANPHONGKH',N'Chi cục HQ Vân Phong')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '53BC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HATIENKG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc Tế Hà Tiên' WHERE TableID='A038A' AND CustomsCode = '53BC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','53BC','HATIENKG',N'Chi cục HQ CK Quốc Tế Hà Tiên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '53BK')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'GTHANHKG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Giang Thành' WHERE TableID='A038A' AND CustomsCode = '53BK'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','53BK','GTHANHKG',N'Chi cục HQ CK Giang Thành')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '53CD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CHCHONGKG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hòn Chông' WHERE TableID='A038A' AND CustomsCode = '53CD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','53CD','CHCHONGKG',N'Chi cục HQ CK Cảng Hòn Chông')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '53CH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'PHUQUOCKG', CustomsOfficeNameInVietnamese =N'Chi cục HQ Phú Quốc' WHERE TableID='A038A' AND CustomsCode = '53CH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','53CH','PHUQUOCKG',N'Chi cục HQ Phú Quốc')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '15BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HUUNGHILS', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Hữu Nghị' WHERE TableID='A038A' AND CustomsCode = '15BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','15BB','HUUNGHILS',N'Chi cục HQ CK Hữu Nghị')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '15BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HUUNGHILS', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Hữu Nghị' WHERE TableID='A038A' AND CustomsCode = '15BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','15BB','HUUNGHILS',N'Chi cục HQ CK Hữu Nghị')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '15BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HUUNGHILS', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Hữu Nghị' WHERE TableID='A038A' AND CustomsCode = '15BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','15BB','HUUNGHILS',N'Chi cục HQ CK Hữu Nghị')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '15BC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CHIMALS', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Chi Ma' WHERE TableID='A038A' AND CustomsCode = '15BC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','15BC','CHIMALS',N'Chi cục HQ CK Chi Ma')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '15BC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CHIMALS', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Chi Ma' WHERE TableID='A038A' AND CustomsCode = '15BC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','15BC','CHIMALS',N'Chi cục HQ CK Chi Ma')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '15BD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'COCNAMLS', CustomsOfficeNameInVietnamese =N'Chi cục HQ Cốc Nam' WHERE TableID='A038A' AND CustomsCode = '15BD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','15BD','COCNAMLS',N'Chi cục HQ Cốc Nam')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '15BE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TANTHANHLS', CustomsOfficeNameInVietnamese =N'Chi cục HQ Tân Thanh' WHERE TableID='A038A' AND CustomsCode = '15BE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','15BE','TANTHANHLS',N'Chi cục HQ Tân Thanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '15BE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TANTHANHLS', CustomsOfficeNameInVietnamese =N'Chi cục HQ Tân Thanh' WHERE TableID='A038A' AND CustomsCode = '15BE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','15BE','TANTHANHLS',N'Chi cục HQ Tân Thanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '15BE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TANTHANHLS', CustomsOfficeNameInVietnamese =N'Chi cục HQ Tân Thanh' WHERE TableID='A038A' AND CustomsCode = '15BE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','15BE','TANTHANHLS',N'Chi cục HQ Tân Thanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '15BE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TANTHANHLS', CustomsOfficeNameInVietnamese =N'Chi cục HQ Tân Thanh' WHERE TableID='A038A' AND CustomsCode = '15BE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','15BE','TANTHANHLS',N'Chi cục HQ Tân Thanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '15SI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DONGDANGLS', CustomsOfficeNameInVietnamese =N'Chi cục HQ Ga Đồng Đăng' WHERE TableID='A038A' AND CustomsCode = '15SI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','15SI','DONGDANGLS',N'Chi cục HQ Ga Đồng Đăng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '13BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCCKLAOCAI', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Lào Cai' WHERE TableID='A038A' AND CustomsCode = '13BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','13BB','CCCKLAOCAI',N'Chi cục HQ CK Quốc tế Lào Cai')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '13BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCCKLAOCAI', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Lào Cai' WHERE TableID='A038A' AND CustomsCode = '13BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','13BB','CCCKLAOCAI',N'Chi cục HQ CK Quốc tế Lào Cai')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '13BC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'M.KHUONGLC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Mường Khương' WHERE TableID='A038A' AND CustomsCode = '13BC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','13BC','M.KHUONGLC',N'Chi cục HQ CK Mường Khương')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '13BD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'BATXATLC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Bát Xát' WHERE TableID='A038A' AND CustomsCode = '13BD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','13BD','BATXATLC',N'Chi cục HQ CK Bát Xát')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '13G1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVDSATLC', CustomsOfficeNameInVietnamese =N'Chi cục HQ Đường sắt LVQT Lào Cai' WHERE TableID='A038A' AND CustomsCode = '13G1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','13G1','DNVDSATLC',N'Chi cục HQ Đường sắt LVQT Lào Cai')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '13G2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'VNLDSATLC', CustomsOfficeNameInVietnamese =N'Chi cục HQ Đường sắt LVQT Lào Cai' WHERE TableID='A038A' AND CustomsCode = '13G2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','13G2','VNLDSATLC',N'Chi cục HQ Đường sắt LVQT Lào Cai')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '48BC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'MQTAYLA', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Mỹ Quý Tây' WHERE TableID='A038A' AND CustomsCode = '48BC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','48BC','MQTAYLA',N'Chi cục HQ CK Mỹ Quý Tây')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '48BD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'BINHHIEPLA', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Bình Hiệp' WHERE TableID='A038A' AND CustomsCode = '48BD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','48BD','BINHHIEPLA',N'Chi cục HQ CK Quốc tế Bình Hiệp')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '48BE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HUNGDIENLA', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hưng Điền' WHERE TableID='A038A' AND CustomsCode = '48BE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','48BE','HUNGDIENLA',N'Chi cục HQ Hưng Điền')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '48BI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DUCHOALA', CustomsOfficeNameInVietnamese =N'Chi cục HQ Đức Hòa' WHERE TableID='A038A' AND CustomsCode = '48BI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','48BI','DUCHOALA',N'Chi cục HQ Đức Hòa')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '48CG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'MYTHOLA', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Mỹ Tho' WHERE TableID='A038A' AND CustomsCode = '48CG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','48CG','MYTHOLA',N'Chi cục HQ CK Cảng Mỹ Tho')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '48CG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'MYTHOLA', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Mỹ Tho' WHERE TableID='A038A' AND CustomsCode = '48CG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','48CG','MYTHOLA',N'Chi cục HQ CK Cảng Mỹ Tho')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '48F1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'LHAUBLLA', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bến Lức' WHERE TableID='A038A' AND CustomsCode = '48F1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','48F1','LHAUBLLA',N'Chi cục HQ Bến Lức')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '48F2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DTTBLLA', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bến Lức' WHERE TableID='A038A' AND CustomsCode = '48F2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','48F2','DTTBLLA',N'Chi cục HQ Bến Lức')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '29BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'NAMCANNA', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Nậm Cắn' WHERE TableID='A038A' AND CustomsCode = '29BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','29BB','NAMCANNA',N'Chi cục HQ CK Quốc tế Nậm Cắn')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '29BH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TTHUYNA', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Thanh Thủy' WHERE TableID='A038A' AND CustomsCode = '29BH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','29BH','TTHUYNA',N'Chi cục HQ CK Thanh Thủy')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '29CC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CANGNGHEAN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng' WHERE TableID='A038A' AND CustomsCode = '29CC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','29CC','CANGNGHEAN',N'Chi cục HQ CK Cảng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '29PF')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'VINHNA', CustomsOfficeNameInVietnamese =N'Chi cục HQ Vinh' WHERE TableID='A038A' AND CustomsCode = '29PF'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','29PF','VINHNA',N'Chi cục HQ Vinh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '31BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CHALOQB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cha Lo' WHERE TableID='A038A' AND CustomsCode = '31BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','31BB','CHALOQB',N'Chi cục HQ CK Cha Lo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '31BF')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CAROONGQB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cà Roòng' WHERE TableID='A038A' AND CustomsCode = '31BF'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','31BF','CAROONGQB',N'Chi cục HQ CK Cà Roòng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '31D1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CHONLAHLQB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hòn La' WHERE TableID='A038A' AND CustomsCode = '31D1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','31D1','CHONLAHLQB',N'Chi cục HQ CK Cảng Hòn La')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '31D2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DHOIHLQB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hòn La' WHERE TableID='A038A' AND CustomsCode = '31D2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','31D2','DHOIHLQB',N'Chi cục HQ CK Cảng Hòn La')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '31D3')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CGIANHHLQB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hòn La' WHERE TableID='A038A' AND CustomsCode = '31D3'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','31D3','CGIANHHLQB',N'Chi cục HQ CK Cảng Hòn La')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '60BD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'NAMGIANGQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Nam Giang' WHERE TableID='A038A' AND CustomsCode = '60BD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','60BD','NAMGIANGQN',N'Chi cục HQ CK Nam Giang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '60C1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNAMDNGCQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Điện Nam - Điện Ngọc' WHERE TableID='A038A' AND CustomsCode = '60C1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','60C1','DNAMDNGCQN',N'Chi cục HQ KCN Điện Nam - Điện Ngọc')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '60C2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNAMDNGCQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Điện Nam - Điện Ngọc' WHERE TableID='A038A' AND CustomsCode = '60C2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','60C2','DNAMDNGCQN',N'Chi cục HQ KCN Điện Nam - Điện Ngọc')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '60CB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KYHAQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Kỳ Hà' WHERE TableID='A038A' AND CustomsCode = '60CB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','60CB','KYHAQN',N'Chi cục HQ CK Cảng Kỳ Hà')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '35CB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CDQUATQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Dung Quất' WHERE TableID='A038A' AND CustomsCode = '35CB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','35CB','CDQUATQN',N'Chi cục HQ CK Cảng Dung Quất')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '35NC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KCNQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ các KCN Quảng Ngãi' WHERE TableID='A038A' AND CustomsCode = '35NC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','35NC','KCNQN',N'Chi cục HQ các KCN Quảng Ngãi')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '20B1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'BLUANMCQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Móng Cái' WHERE TableID='A038A' AND CustomsCode = '20B1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','20B1','BLUANMCQN',N'Chi cục HQ CK Móng Cái')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '20B2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KLONGMCQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Móng Cái' WHERE TableID='A038A' AND CustomsCode = '20B2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','20B2','KLONGMCQN',N'Chi cục HQ CK Móng Cái')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '20BC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HOANHMOQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Hoành Mô' WHERE TableID='A038A' AND CustomsCode = '20BC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','20BC','HOANHMOQN',N'Chi cục HQ CK Hoành Mô')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '20BD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'PSINHQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Phong Sinh' WHERE TableID='A038A' AND CustomsCode = '20BD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','20BD','PSINHQN',N'Chi cục HQ Bắc Phong Sinh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '20CD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CAILANQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Cái Lân' WHERE TableID='A038A' AND CustomsCode = '20CD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','20CD','CAILANQN',N'Chi cục HQ CK Cảng Cái Lân')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '20CE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'VANGIAQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Vạn Gia' WHERE TableID='A038A' AND CustomsCode = '20CE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','20CE','VANGIAQN',N'Chi cục HQ CK Cảng Vạn Gia')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '20CF')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'HONGAIQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hòn Gai' WHERE TableID='A038A' AND CustomsCode = '20CF'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','20CF','HONGAIQN',N'Chi cục HQ CK Cảng Hòn Gai')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '20CG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CAMPHAQN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Cẩm Phả' WHERE TableID='A038A' AND CustomsCode = '20CG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','20CG','CAMPHAQN',N'Chi cục HQ CK Cảng Cẩm Phả')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '32BB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'LAOBAOQT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Lao Bảo' WHERE TableID='A038A' AND CustomsCode = '32BB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','32BB','LAOBAOQT',N'Chi cục HQ CK Lao Bảo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '32BC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'LALAYQT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK La Lay' WHERE TableID='A038A' AND CustomsCode = '32BC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','32BC','LALAYQT',N'Chi cục HQ CK La Lay')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '32BD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KTMAILBQT', CustomsOfficeNameInVietnamese =N'Chi cục HQ Khu thương mại Lao Bảo' WHERE TableID='A038A' AND CustomsCode = '32BD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','32BD','KTMAILBQT',N'Chi cục HQ Khu thương mại Lao Bảo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '32CD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCUAVIETQT', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Cửa Việt' WHERE TableID='A038A' AND CustomsCode = '32CD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','32CD','CCUAVIETQT',N'Chi cục HQ CK Cảng Cửa Việt')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '32VG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KSOATHQQT', CustomsOfficeNameInVietnamese =N'Đội Kiẻm soát HQ Quảng Trị' WHERE TableID='A038A' AND CustomsCode = '32VG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','32VG','KSOATHQQT',N'Đội Kiẻm soát HQ Quảng Trị')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '45B1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVMBAITN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Mộc Bài' WHERE TableID='A038A' AND CustomsCode = '45B1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','45B1','DNVMBAITN',N'Chi cục HQ CK Mộc Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '45B2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KTMCNMBTN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Mộc Bài' WHERE TableID='A038A' AND CustomsCode = '45B2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','45B2','KTMCNMBTN',N'Chi cục HQ CK Mộc Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '45BD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'PHUOCTANTN', CustomsOfficeNameInVietnamese =N'Chi cục HQ Phước Tân' WHERE TableID='A038A' AND CustomsCode = '45BD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','45BD','PHUOCTANTN',N'Chi cục HQ Phước Tân')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '45BE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'KATUMTN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Kà Tum' WHERE TableID='A038A' AND CustomsCode = '45BE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','45BE','KATUMTN',N'Chi cục HQ CK Kà Tum')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '45C1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVXAMATTN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Xa Mát' WHERE TableID='A038A' AND CustomsCode = '45C1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','45C1','DNVXAMATTN',N'Chi cục HQ CK Xa Mát')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '45C2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DCRXAMATTN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Xa Mát' WHERE TableID='A038A' AND CustomsCode = '45C2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','45C2','DCRXAMATTN',N'Chi cục HQ CK Xa Mát')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '45F1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVTBANGTN', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Trảng Bàng' WHERE TableID='A038A' AND CustomsCode = '45F1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','45F1','DNVTBANGTN',N'Chi cục HQ KCN Trảng Bàng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '45F2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'PDONGTBTN', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Trảng Bàng' WHERE TableID='A038A' AND CustomsCode = '45F2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','45F2','PDONGTBTN',N'Chi cục HQ KCN Trảng Bàng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '27B1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DNVNMEOTH', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Na Mèo' WHERE TableID='A038A' AND CustomsCode = '27B1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','27B1','DNVNMEOTH',N'Chi cục HQ CK Quốc tế Na Mèo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '27B2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DTTNMEOTH', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Na Mèo' WHERE TableID='A038A' AND CustomsCode = '27B2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','27B2','DTTNMEOTH',N'Chi cục HQ CK Quốc tế Na Mèo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '27F1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CTHANHHOA', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Thanh Hóa' WHERE TableID='A038A' AND CustomsCode = '27F1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','27F1','CTHANHHOA',N'Chi cục HQ CK Cảng Thanh Hóa')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '27F2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CNGSONTH', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Nghi Sơn' WHERE TableID='A038A' AND CustomsCode = '27F2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','27F2','CNGSONTH',N'Chi cục HQ CK Cảng Nghi Sơn')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '28NJ')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCHQHANAM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý các KCN Hà Nam' WHERE TableID='A038A' AND CustomsCode = '28NJ'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','28NJ','CCHQHANAM',N'Chi cục HQ Quản lý các KCN Hà Nam')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '28PC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCHQNBINH', CustomsOfficeNameInVietnamese =N'Chi cục HQ Ninh Bình' WHERE TableID='A038A' AND CustomsCode = '28PC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','28PC','CCHQNBINH',N'Chi cục HQ Ninh Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '28PC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCHQNBINH', CustomsOfficeNameInVietnamese =N'Chi cục HQ Ninh Bình' WHERE TableID='A038A' AND CustomsCode = '28PC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','28PC','CCHQNBINH',N'Chi cục HQ Ninh Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '28PE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCHQNDINH', CustomsOfficeNameInVietnamese =N'Chi cục HQ Nam Định' WHERE TableID='A038A' AND CustomsCode = '28PE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','28PE','CCHQNDINH',N'Chi cục HQ Nam Định')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '33BA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'ADOTTTH', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK A Đớt' WHERE TableID='A038A' AND CustomsCode = '33BA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','33BA','ADOTTTH',N'Chi cục HQ CK A Đớt')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '33BA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'ADOTTTH', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK A Đớt' WHERE TableID='A038A' AND CustomsCode = '33BA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','33BA','ADOTTTH',N'Chi cục HQ CK A Đớt')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '33CC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CTANTTH', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Thuận An' WHERE TableID='A038A' AND CustomsCode = '33CC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','33CC','CTANTTH',N'Chi cục HQ CK Cảng Thuận An')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '33CF')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCMAYTTH', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Chân Mây' WHERE TableID='A038A' AND CustomsCode = '33CF'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','33CF','CCMAYTTH',N'Chi cục HQ CK Cảng Chân Mây')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '33PD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'THUYANTTH', CustomsOfficeNameInVietnamese =N'Chi cục HQ Thủy An' WHERE TableID='A038A' AND CustomsCode = '33PD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','33PD','THUYANTTH',N'Chi cục HQ Thủy An')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02DS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CPNHANHHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = '02DS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02DS','CPNHANHHCM',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02CV')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CHPHUOCHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hiệp Phước' WHERE TableID='A038A' AND CustomsCode = '02CV'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02CV','CHPHUOCHCM',N'Chi cục HQ CK Cảng Hiệp Phước')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02CV')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CHPHUOCHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hiệp Phước' WHERE TableID='A038A' AND CustomsCode = '02CV'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02CV','CHPHUOCHCM',N'Chi cục HQ CK Cảng Hiệp Phước')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02CI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CSGONKVI', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV I' WHERE TableID='A038A' AND CustomsCode = '02CI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02CI','CSGONKVI',N'Chi cục HQ CK Cảng Sài Gòn KV I')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02CI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CSGONKVI', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV I' WHERE TableID='A038A' AND CustomsCode = '02CI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02CI','CSGONKVI',N'Chi cục HQ CK Cảng Sài Gòn KV I')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02CI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CSGONKVI', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV I' WHERE TableID='A038A' AND CustomsCode = '02CI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02CI','CSGONKVI',N'Chi cục HQ CK Cảng Sài Gòn KV I')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02CC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CSGONKVII', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV II' WHERE TableID='A038A' AND CustomsCode = '02CC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02CC','CSGONKVII',N'Chi cục HQ CK Cảng Sài Gòn KV II')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02CC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CSGONKVII', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV II' WHERE TableID='A038A' AND CustomsCode = '02CC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02CC','CSGONKVII',N'Chi cục HQ CK Cảng Sài Gòn KV II')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02H1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CBNSGKVIII', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV III' WHERE TableID='A038A' AND CustomsCode = '02H1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02H1','CBNSGKVIII',N'Chi cục HQ CK Cảng Sài Gòn KV III')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02H2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'GSXDKVIII', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV III' WHERE TableID='A038A' AND CustomsCode = '02H2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02H2','GSXDKVIII',N'Chi cục HQ CK Cảng Sài Gòn KV III')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02H3')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CVICTKVIII', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV III' WHERE TableID='A038A' AND CustomsCode = '02H3'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02H3','CVICTKVIII',N'Chi cục HQ CK Cảng Sài Gòn KV III')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02IK')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCSGKVIV', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV IV' WHERE TableID='A038A' AND CustomsCode = '02IK'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02IK','CCSGKVIV',N'Chi cục HQ CK Cảng Sài Gòn KV IV')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02IK')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCSGKVIV', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV IV' WHERE TableID='A038A' AND CustomsCode = '02IK'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02IK','CCSGKVIV',N'Chi cục HQ CK Cảng Sài Gòn KV IV')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02B1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'DTCSTSNHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay Quốc tế Tân Sơn Nhất' WHERE TableID='A038A' AND CustomsCode = '02B1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02B1','DTCSTSNHCM',N'Chi cục HQ CK Sân bay Quốc tế Tân Sơn Nhất')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02CX')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CTCANGHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Tân Cảng' WHERE TableID='A038A' AND CustomsCode = '02CX'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02CX','CTCANGHCM',N'Chi cục HQ CK Tân Cảng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02F1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'LTILTHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX Linh Trung' WHERE TableID='A038A' AND CustomsCode = '02F1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02F1','LTILTHCM',N'Chi cục HQ KCX Linh Trung')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02F1')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'LTILTHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX Linh Trung' WHERE TableID='A038A' AND CustomsCode = '02F1'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02F1','LTILTHCM',N'Chi cục HQ KCX Linh Trung')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02F2')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'LTIILTHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX Linh Trung' WHERE TableID='A038A' AND CustomsCode = '02F2'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02F2','LTIILTHCM',N'Chi cục HQ KCX Linh Trung')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02F3')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CNCLTHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX Linh Trung' WHERE TableID='A038A' AND CustomsCode = '02F3'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02F3','CNCLTHCM',N'Chi cục HQ KCX Linh Trung')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02XE')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'TTHUANHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX Tân Thuận' WHERE TableID='A038A' AND CustomsCode = '02XE'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02XE','TTHUANHCM',N'Chi cục HQ KCX Tân Thuận')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02PG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCHQDTHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng đầu tư' WHERE TableID='A038A' AND CustomsCode = '02PG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02PG','CCHQDTHCM',N'Chi cục HQ Quản lý hàng đầu tư')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02PG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCHQDTHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng đầu tư' WHERE TableID='A038A' AND CustomsCode = '02PG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02PG','CCHQDTHCM',N'Chi cục HQ Quản lý hàng đầu tư')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02PJ')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCHQGCHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng gia công' WHERE TableID='A038A' AND CustomsCode = '02PJ'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02PJ','CCHQGCHCM',N'Chi cục HQ Quản lý hàng gia công')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = '02PJ')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = 'CCHQGCHCM', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng gia công' WHERE TableID='A038A' AND CustomsCode = '02PJ'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','02PJ','CCHQGCHCM',N'Chi cục HQ Quản lý hàng gia công')   
END

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '21.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('21.0',GETDATE(), N'BẢNG MÃ CHI CỤC HẢI QUAN DÙNG TRONG VNACCS ')
END
















