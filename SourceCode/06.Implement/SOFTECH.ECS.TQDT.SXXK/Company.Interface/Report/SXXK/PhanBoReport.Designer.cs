namespace Company.Interface.Report.SXXK
{
    partial class PhanBoReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNPL = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDinhMuc1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongSD1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDinhMuc2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongSD2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDinhMuc3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongSD3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDinhMuc4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongSD4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongNhuCau = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanBo = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.lblSanPham1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSanPham2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSanPham3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSanPham4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTieuDe = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Height = 20;
            this.Detail.Name = "Detail";
            // 
            // xrTable1
            // 
            this.xrTable1.Location = new System.Drawing.Point(0, 0);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.Size = new System.Drawing.Size(1125, 20);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTT,
            this.lblTenNPL,
            this.lblDVT,
            this.lblDinhMuc1,
            this.lblLuongSD1,
            this.lblDinhMuc2,
            this.lblLuongSD2,
            this.lblDinhMuc3,
            this.lblLuongSD3,
            this.lblDinhMuc4,
            this.lblLuongSD4,
            this.lblTongNhuCau,
            this.lblPhanBo});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(1125, 20);
            // 
            // lblSTT
            // 
            this.lblSTT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTT.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblSTT.Location = new System.Drawing.Point(0, 0);
            this.lblSTT.Name = "lblSTT";
            this.lblSTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTT.ParentStyleUsing.UseBorders = false;
            this.lblSTT.ParentStyleUsing.UseFont = false;
            this.lblSTT.Size = new System.Drawing.Size(33, 20);
            this.lblSTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSTT_BeforePrint);
            // 
            // lblTenNPL
            // 
            this.lblTenNPL.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTenNPL.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblTenNPL.Location = new System.Drawing.Point(33, 0);
            this.lblTenNPL.Name = "lblTenNPL";
            this.lblTenNPL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNPL.ParentStyleUsing.UseBorders = false;
            this.lblTenNPL.ParentStyleUsing.UseFont = false;
            this.lblTenNPL.Size = new System.Drawing.Size(217, 20);
            this.lblTenNPL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTenNPL.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblMaNPL_BeforePrint);
            // 
            // lblDVT
            // 
            this.lblDVT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVT.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblDVT.Location = new System.Drawing.Point(250, 0);
            this.lblDVT.Name = "lblDVT";
            this.lblDVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDVT.ParentStyleUsing.UseBorders = false;
            this.lblDVT.ParentStyleUsing.UseFont = false;
            this.lblDVT.Size = new System.Drawing.Size(33, 20);
            this.lblDVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblDinhMuc1
            // 
            this.lblDinhMuc1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDinhMuc1.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblDinhMuc1.Location = new System.Drawing.Point(283, 0);
            this.lblDinhMuc1.Name = "lblDinhMuc1";
            this.lblDinhMuc1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDinhMuc1.ParentStyleUsing.UseBorders = false;
            this.lblDinhMuc1.ParentStyleUsing.UseFont = false;
            this.lblDinhMuc1.Size = new System.Drawing.Size(67, 20);
            this.lblDinhMuc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblLuongSD1
            // 
            this.lblLuongSD1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongSD1.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblLuongSD1.Location = new System.Drawing.Point(350, 0);
            this.lblLuongSD1.Name = "lblLuongSD1";
            this.lblLuongSD1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongSD1.ParentStyleUsing.UseBorders = false;
            this.lblLuongSD1.ParentStyleUsing.UseFont = false;
            this.lblLuongSD1.Size = new System.Drawing.Size(67, 20);
            this.lblLuongSD1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblDinhMuc2
            // 
            this.lblDinhMuc2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDinhMuc2.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblDinhMuc2.Location = new System.Drawing.Point(417, 0);
            this.lblDinhMuc2.Name = "lblDinhMuc2";
            this.lblDinhMuc2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDinhMuc2.ParentStyleUsing.UseBorders = false;
            this.lblDinhMuc2.ParentStyleUsing.UseFont = false;
            this.lblDinhMuc2.Size = new System.Drawing.Size(66, 20);
            this.lblDinhMuc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblLuongSD2
            // 
            this.lblLuongSD2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongSD2.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblLuongSD2.Location = new System.Drawing.Point(483, 0);
            this.lblLuongSD2.Name = "lblLuongSD2";
            this.lblLuongSD2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongSD2.ParentStyleUsing.UseBorders = false;
            this.lblLuongSD2.ParentStyleUsing.UseFont = false;
            this.lblLuongSD2.Size = new System.Drawing.Size(67, 20);
            this.lblLuongSD2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblDinhMuc3
            // 
            this.lblDinhMuc3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDinhMuc3.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblDinhMuc3.Location = new System.Drawing.Point(550, 0);
            this.lblDinhMuc3.Name = "lblDinhMuc3";
            this.lblDinhMuc3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDinhMuc3.ParentStyleUsing.UseBorders = false;
            this.lblDinhMuc3.ParentStyleUsing.UseFont = false;
            this.lblDinhMuc3.Size = new System.Drawing.Size(67, 20);
            this.lblDinhMuc3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblLuongSD3
            // 
            this.lblLuongSD3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongSD3.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblLuongSD3.Location = new System.Drawing.Point(617, 0);
            this.lblLuongSD3.Name = "lblLuongSD3";
            this.lblLuongSD3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongSD3.ParentStyleUsing.UseBorders = false;
            this.lblLuongSD3.ParentStyleUsing.UseFont = false;
            this.lblLuongSD3.Size = new System.Drawing.Size(66, 20);
            this.lblLuongSD3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblDinhMuc4
            // 
            this.lblDinhMuc4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDinhMuc4.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblDinhMuc4.Location = new System.Drawing.Point(683, 0);
            this.lblDinhMuc4.Name = "lblDinhMuc4";
            this.lblDinhMuc4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDinhMuc4.ParentStyleUsing.UseBorders = false;
            this.lblDinhMuc4.ParentStyleUsing.UseFont = false;
            this.lblDinhMuc4.Size = new System.Drawing.Size(67, 20);
            this.lblDinhMuc4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblLuongSD4
            // 
            this.lblLuongSD4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongSD4.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblLuongSD4.Location = new System.Drawing.Point(750, 0);
            this.lblLuongSD4.Name = "lblLuongSD4";
            this.lblLuongSD4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongSD4.ParentStyleUsing.UseBorders = false;
            this.lblLuongSD4.ParentStyleUsing.UseFont = false;
            this.lblLuongSD4.Size = new System.Drawing.Size(67, 20);
            this.lblLuongSD4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTongNhuCau
            // 
            this.lblTongNhuCau.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongNhuCau.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblTongNhuCau.Location = new System.Drawing.Point(817, 0);
            this.lblTongNhuCau.Name = "lblTongNhuCau";
            this.lblTongNhuCau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongNhuCau.ParentStyleUsing.UseBorders = false;
            this.lblTongNhuCau.ParentStyleUsing.UseFont = false;
            this.lblTongNhuCau.Size = new System.Drawing.Size(66, 20);
            this.lblTongNhuCau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblPhanBo
            // 
            this.lblPhanBo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPhanBo.Font = new System.Drawing.Font("Tahoma", 6.5F);
            this.lblPhanBo.Location = new System.Drawing.Point(883, 0);
            this.lblPhanBo.Name = "lblPhanBo";
            this.lblPhanBo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanBo.ParentStyleUsing.UseBorders = false;
            this.lblPhanBo.ParentStyleUsing.UseFont = false;
            this.lblPhanBo.Size = new System.Drawing.Size(242, 20);
            this.lblPhanBo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblSanPham1,
            this.xrLabel19,
            this.xrLabel12,
            this.xrLabel9,
            this.xrLabel1,
            this.xrLabel6,
            this.xrLabel8,
            this.xrLabel10,
            this.lblSanPham2,
            this.lblSanPham3,
            this.lblSanPham4,
            this.xrLabel18,
            this.xrLabel22,
            this.xrLabel17,
            this.xrLabel25,
            this.xrLabel28,
            this.xrLabel29});
            this.PageHeader.Height = 75;
            this.PageHeader.Name = "PageHeader";
            // 
            // lblSanPham1
            // 
            this.lblSanPham1.BackColor = System.Drawing.Color.Azure;
            this.lblSanPham1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSanPham1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.lblSanPham1.Location = new System.Drawing.Point(283, 0);
            this.lblSanPham1.Multiline = true;
            this.lblSanPham1.Name = "lblSanPham1";
            this.lblSanPham1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSanPham1.ParentStyleUsing.UseBackColor = false;
            this.lblSanPham1.ParentStyleUsing.UseBorders = false;
            this.lblSanPham1.ParentStyleUsing.UseFont = false;
            this.lblSanPham1.Size = new System.Drawing.Size(134, 58);
            this.lblSanPham1.Text = "ÁD\r\nSDF\r\nSDF\r\nSDF";
            this.lblSanPham1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel19
            // 
            this.xrLabel19.BackColor = System.Drawing.Color.Azure;
            this.xrLabel19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.Location = new System.Drawing.Point(250, 0);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.ParentStyleUsing.UseBackColor = false;
            this.xrLabel19.ParentStyleUsing.UseBorders = false;
            this.xrLabel19.ParentStyleUsing.UseFont = false;
            this.xrLabel19.Size = new System.Drawing.Size(33, 75);
            this.xrLabel19.Text = "ĐVT";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.BackColor = System.Drawing.Color.Azure;
            this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel12.Location = new System.Drawing.Point(33, 0);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.ParentStyleUsing.UseBackColor = false;
            this.xrLabel12.ParentStyleUsing.UseBorders = false;
            this.xrLabel12.ParentStyleUsing.UseFont = false;
            this.xrLabel12.Size = new System.Drawing.Size(217, 75);
            this.xrLabel12.Text = "NGUYÊN PHỤ LIỆU";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel9
            // 
            this.xrLabel9.BackColor = System.Drawing.Color.Azure;
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.Location = new System.Drawing.Point(0, 0);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.ParentStyleUsing.UseBackColor = false;
            this.xrLabel9.ParentStyleUsing.UseBorders = false;
            this.xrLabel9.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel9.ParentStyleUsing.UseFont = false;
            this.xrLabel9.Size = new System.Drawing.Size(33, 75);
            this.xrLabel9.Text = "STT";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.Azure;
            this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.Location = new System.Drawing.Point(283, 58);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.ParentStyleUsing.UseBackColor = false;
            this.xrLabel1.ParentStyleUsing.UseBorders = false;
            this.xrLabel1.ParentStyleUsing.UseFont = false;
            this.xrLabel1.Size = new System.Drawing.Size(67, 17);
            this.xrLabel1.Text = "ĐM";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.BackColor = System.Drawing.Color.Azure;
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.Location = new System.Drawing.Point(350, 58);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.ParentStyleUsing.UseBackColor = false;
            this.xrLabel6.ParentStyleUsing.UseBorders = false;
            this.xrLabel6.ParentStyleUsing.UseFont = false;
            this.xrLabel6.Size = new System.Drawing.Size(67, 17);
            this.xrLabel6.Text = "NC";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.BackColor = System.Drawing.Color.Azure;
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.Location = new System.Drawing.Point(483, 58);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.ParentStyleUsing.UseBackColor = false;
            this.xrLabel8.ParentStyleUsing.UseBorders = false;
            this.xrLabel8.ParentStyleUsing.UseFont = false;
            this.xrLabel8.Size = new System.Drawing.Size(67, 17);
            this.xrLabel8.Text = "NC";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.BackColor = System.Drawing.Color.Azure;
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.Location = new System.Drawing.Point(417, 58);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.ParentStyleUsing.UseBackColor = false;
            this.xrLabel10.ParentStyleUsing.UseBorders = false;
            this.xrLabel10.ParentStyleUsing.UseFont = false;
            this.xrLabel10.Size = new System.Drawing.Size(66, 17);
            this.xrLabel10.Text = "ĐM";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSanPham2
            // 
            this.lblSanPham2.BackColor = System.Drawing.Color.Azure;
            this.lblSanPham2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSanPham2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.lblSanPham2.Location = new System.Drawing.Point(417, 0);
            this.lblSanPham2.Multiline = true;
            this.lblSanPham2.Name = "lblSanPham2";
            this.lblSanPham2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSanPham2.ParentStyleUsing.UseBackColor = false;
            this.lblSanPham2.ParentStyleUsing.UseBorders = false;
            this.lblSanPham2.ParentStyleUsing.UseFont = false;
            this.lblSanPham2.Size = new System.Drawing.Size(133, 58);
            this.lblSanPham2.Text = "ÁD\r\nSDF\r\nSDF\r\nSDF";
            this.lblSanPham2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSanPham3
            // 
            this.lblSanPham3.BackColor = System.Drawing.Color.Azure;
            this.lblSanPham3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSanPham3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.lblSanPham3.Location = new System.Drawing.Point(550, 0);
            this.lblSanPham3.Multiline = true;
            this.lblSanPham3.Name = "lblSanPham3";
            this.lblSanPham3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSanPham3.ParentStyleUsing.UseBackColor = false;
            this.lblSanPham3.ParentStyleUsing.UseBorders = false;
            this.lblSanPham3.ParentStyleUsing.UseFont = false;
            this.lblSanPham3.Size = new System.Drawing.Size(133, 58);
            this.lblSanPham3.Text = "ÁD\r\nSDF\r\nSDF\r\nSDF";
            this.lblSanPham3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSanPham4
            // 
            this.lblSanPham4.BackColor = System.Drawing.Color.Azure;
            this.lblSanPham4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSanPham4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.lblSanPham4.Location = new System.Drawing.Point(683, 0);
            this.lblSanPham4.Multiline = true;
            this.lblSanPham4.Name = "lblSanPham4";
            this.lblSanPham4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSanPham4.ParentStyleUsing.UseBackColor = false;
            this.lblSanPham4.ParentStyleUsing.UseBorders = false;
            this.lblSanPham4.ParentStyleUsing.UseFont = false;
            this.lblSanPham4.Size = new System.Drawing.Size(134, 58);
            this.lblSanPham4.Text = "ÁD\r\nSDF\r\nSDF\r\nSDF";
            this.lblSanPham4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.Azure;
            this.xrLabel18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel18.Location = new System.Drawing.Point(550, 58);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.ParentStyleUsing.UseBackColor = false;
            this.xrLabel18.ParentStyleUsing.UseBorders = false;
            this.xrLabel18.ParentStyleUsing.UseFont = false;
            this.xrLabel18.Size = new System.Drawing.Size(67, 17);
            this.xrLabel18.Text = "ĐM";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel22
            // 
            this.xrLabel22.BackColor = System.Drawing.Color.Azure;
            this.xrLabel22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel22.Location = new System.Drawing.Point(617, 58);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.ParentStyleUsing.UseBackColor = false;
            this.xrLabel22.ParentStyleUsing.UseBorders = false;
            this.xrLabel22.ParentStyleUsing.UseFont = false;
            this.xrLabel22.Size = new System.Drawing.Size(66, 17);
            this.xrLabel22.Text = "NC";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel17
            // 
            this.xrLabel17.BackColor = System.Drawing.Color.Azure;
            this.xrLabel17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel17.Location = new System.Drawing.Point(683, 58);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.ParentStyleUsing.UseBackColor = false;
            this.xrLabel17.ParentStyleUsing.UseBorders = false;
            this.xrLabel17.ParentStyleUsing.UseFont = false;
            this.xrLabel17.Size = new System.Drawing.Size(67, 17);
            this.xrLabel17.Text = "ĐM";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.BackColor = System.Drawing.Color.Azure;
            this.xrLabel25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel25.Location = new System.Drawing.Point(750, 58);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.ParentStyleUsing.UseBackColor = false;
            this.xrLabel25.ParentStyleUsing.UseBorders = false;
            this.xrLabel25.ParentStyleUsing.UseFont = false;
            this.xrLabel25.Size = new System.Drawing.Size(67, 17);
            this.xrLabel25.Text = "NC";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel28
            // 
            this.xrLabel28.BackColor = System.Drawing.Color.Azure;
            this.xrLabel28.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel28.Location = new System.Drawing.Point(817, 0);
            this.xrLabel28.Multiline = true;
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.ParentStyleUsing.UseBackColor = false;
            this.xrLabel28.ParentStyleUsing.UseBorders = false;
            this.xrLabel28.ParentStyleUsing.UseFont = false;
            this.xrLabel28.Size = new System.Drawing.Size(66, 75);
            this.xrLabel28.Text = "TỔNG NHU CẦU";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel29
            // 
            this.xrLabel29.BackColor = System.Drawing.Color.Azure;
            this.xrLabel29.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.Location = new System.Drawing.Point(883, 0);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.ParentStyleUsing.UseBackColor = false;
            this.xrLabel29.ParentStyleUsing.UseBorders = false;
            this.xrLabel29.ParentStyleUsing.UseFont = false;
            this.xrLabel29.Size = new System.Drawing.Size(242, 75);
            this.xrLabel29.Text = "PHÂN BỔ THEO";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2});
            this.ReportFooter.Height = 122;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel2.Location = new System.Drawing.Point(742, 8);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.ParentStyleUsing.UseFont = false;
            this.xrLabel2.Size = new System.Drawing.Size(341, 25);
            this.xrLabel2.Text = "..............., ngày..... tháng ..... năm ........ ";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTieuDe
            // 
            this.lblTieuDe.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lblTieuDe.Location = new System.Drawing.Point(0, 8);
            this.lblTieuDe.Multiline = true;
            this.lblTieuDe.Name = "lblTieuDe";
            this.lblTieuDe.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTieuDe.ParentStyleUsing.UseFont = false;
            this.lblTieuDe.Size = new System.Drawing.Size(1117, 25);
            this.lblTieuDe.Text = "BẢNG PHÂN BỔ NGUYÊN PHỤ LIỆU HÀNG XUẤT KHẨU TỜ KHAI XUẤT KHẨU SỐ ";
            this.lblTieuDe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabel4,
            this.lblTenDoanhNghiep,
            this.lblMaDoanhNghiep,
            this.lblTieuDe});
            this.ReportHeader.Height = 106;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel3.Location = new System.Drawing.Point(0, 50);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.ParentStyleUsing.UseFont = false;
            this.xrLabel3.Size = new System.Drawing.Size(125, 25);
            this.xrLabel3.Text = "Tên Doanh nghiệp:";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel4.Location = new System.Drawing.Point(0, 75);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.ParentStyleUsing.UseFont = false;
            this.xrLabel4.Size = new System.Drawing.Size(125, 25);
            this.xrLabel4.Text = "Mã Doanh nghiệp:";
            // 
            // lblTenDoanhNghiep
            // 
            this.lblTenDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lblTenDoanhNghiep.Location = new System.Drawing.Point(125, 50);
            this.lblTenDoanhNghiep.Name = "lblTenDoanhNghiep";
            this.lblTenDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblTenDoanhNghiep.Size = new System.Drawing.Size(708, 25);
            this.lblTenDoanhNghiep.Text = "lblTenDoanhNghiep";
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lblMaDoanhNghiep.Location = new System.Drawing.Point(125, 75);
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblMaDoanhNghiep.Size = new System.Drawing.Size(141, 25);
            this.lblMaDoanhNghiep.Text = "lblMaDoanhNghiep";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.Name = "xrControlStyle2";
            // 
            // PhanBoReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.ReportFooter,
            this.ReportHeader});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(21, 21, 48, 39);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2});
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PhanBoReport_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel lblTieuDe;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel lblSanPham1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblSanPham2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblSanPham4;
        private DevExpress.XtraReports.UI.XRLabel lblSanPham3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNPL;
        private DevExpress.XtraReports.UI.XRTableCell lblDinhMuc1;
        private DevExpress.XtraReports.UI.XRTableCell lblDVT;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongSD1;
        private DevExpress.XtraReports.UI.XRTableCell lblDinhMuc2;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongSD2;
        private DevExpress.XtraReports.UI.XRTableCell lblDinhMuc3;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongSD3;
        private DevExpress.XtraReports.UI.XRTableCell lblDinhMuc4;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongSD4;
        private DevExpress.XtraReports.UI.XRTableCell lblTongNhuCau;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanBo;
    }
}
