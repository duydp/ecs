using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class VNACC_Category_Station
    {
        protected static List<VNACC_Category_Station> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_Station> collection = new List<VNACC_Category_Station>();
            while (reader.Read())
            {
                VNACC_Category_Station entity = new VNACC_Category_Station();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("StationCode"))) entity.StationCode = reader.GetString(reader.GetOrdinal("StationCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("StationName"))) entity.StationName = reader.GetString(reader.GetOrdinal("StationName"));
                if (!reader.IsDBNull(reader.GetOrdinal("CountryCode"))) entity.CountryCode = reader.GetString(reader.GetOrdinal("CountryCode"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_Station> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [StationCode], StationName, CountryCode FROM [dbo].[t_VNACC_Category_Stations]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_Station> SelectCollectionBy(List<VNACC_Category_Station> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_Station o)
            {
                return o.TableID.Contains(tableID);
            });
        }

        public static List<VNACC_Category_Station> SelectCollectionByCountryCode(List<VNACC_Category_Station> collections, string countryCode)
        {
            return collections.FindAll(delegate(VNACC_Category_Station o)
            {
                return o.CountryCode == countryCode;
            });
        }
    }
}