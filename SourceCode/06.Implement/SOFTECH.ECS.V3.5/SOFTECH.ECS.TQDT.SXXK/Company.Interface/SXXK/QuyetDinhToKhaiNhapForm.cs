﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.Interface.Report;
using Janus.Windows.GridEX;
using System.Xml.Serialization;
using System.IO;
using Company.BLL.SXXK.ToKhai;
using System.Diagnostics;

namespace Company.Interface.SXXK
{
    public partial class QuyetDinhToKhaiNhapForm : Company.Interface.BaseForm
    {

        public QuyetDinhToKhaiNhapForm()
        {
            InitializeComponent();
        }

        private void HSTLManageForm_Load(object sender, EventArgs e)
        {
            txtNamDangKy.Value = DateTime.Today.Year;
            GlobalSettings.KhoiTao_GiaTriMacDinh();

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.SoToKhai = Convert.ToInt32(txtSoToKhai.Value);
            TKMD.NamDangKy = Convert.ToInt16(txtNamDangKy.Value);
            TKMD.MaLoaiHinh = ctrLoaiHinh.Ma;
            TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            if (!TKMD.Load())
            {
                ShowMessage("Không có tờ khai nhập này trong hệ thống.", false);
                return;
            }
            dgList.DataSource = TKMD.GetQuyetDinhToKhaiNhap();
            gridEXExporter1.SheetName = "TK " + TKMD.SoToKhai + "-" + TKMD.MaLoaiHinh + "-" + TKMD.NamDangKy;

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
            HSTL.LanThanhLy = Convert.ToInt32(e.Row.Cells["LanThanhLy"].Value);
            List<HoSoThanhLyDangKy> hsos = HoSoThanhLyDangKy.SelectCollectionDynamic("LanThanhLy=" + HSTL.LanThanhLy, "");
            if (hsos.Count != 0) HSTL = hsos[0];
            e.Row.Cells["SoQuyetDinh"].Text = HSTL.SoQuyetDinh;
            if (HSTL.NgayQuyetDinh.Year > 1900)
                e.Row.Cells["NgayQuyetDinh"].Text = HSTL.NgayQuyetDinh.ToShortDateString();

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.Filter = "Excel files| *.xls";
            sfNPL.ShowDialog(this);
            if (sfNPL.FileName != "")
            {
                Stream str = sfNPL.OpenFile();
                gridEXExporter1.Export(str);
                str.Close();
                if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(sfNPL.FileName);
                }
            }
        }

    }
}

