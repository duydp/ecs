﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Company.GC.BLL.KDT.GC
{
    public partial class LoaiPhuKien
    {
        public HangPhuKienCollection HPKCollection = new HangPhuKienCollection();
        public KDT_GC_PhuKien_TTHopDong PhuKien_TTHopDong;
        public void LoadCollection()
        {
            HangPhuKien hang = new HangPhuKien();
            hang.Master_ID = this.ID;
            HPKCollection = hang.SelectCollectionBy_Master_ID();
        }

        public void LoadCollection(string database)
        {
            HangPhuKien hang = new HangPhuKien();
            hang.SetDabaseMoi(database);
            hang.Master_ID = this.ID;
            HPKCollection = hang.SelectCollectionBy_Master_ID();
        }
        public void LoadCollection(SqlTransaction transaction, string database)
        {
            HangPhuKien hang = new HangPhuKien();
            hang.Master_ID = this.ID;
            HPKCollection = hang.SelectCollectionBy_Master_ID(transaction, database);
        }

        //public bool Delete(long HopDong_ID)
        //{
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
        //        try
        //        {
        //            //if (this.MaPhuKien == "H06")
        //            //{
        //            //    HopDong HD = new HopDong();
        //            //    HD.ID = HopDong_ID;
        //            //    HD.Load();
        //            //    HD.NgayGiaHan = Convert.ToDateTime(this.ThongTinCu);
        //            //    if (HD.NgayGiaHan == HD.NgayHetHan)
        //            //        HD.NgayGiaHan = new DateTime(1900, 1, 1);
        //            //    HD.UpdateTransaction(transaction);
        //            //}
        //            //else if (this.MaPhuKien == "H10")
        //            //{
        //            //    HopDong HD = new HopDong();
        //            //    HD.ID = HopDong_ID;
        //            //    HD.Load();
        //            //    HD.TrangThaiXuLy = 1;
        //            //    HD.UpdateTransaction(transaction);
        //            //}
        //            //else if (this.MaPhuKien == "H11")
        //            //{
        //            //    ;
        //            //}
        //            foreach (HangPhuKien hang in this.HPKCollection)
        //            {
        //                if (this.MaPhuKien == "N01")
        //                {
        //                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //                    npl.HopDong_ID = HopDong_ID;
        //                    npl.Ma = hang.MaHang;
        //                    npl.Load(transaction);
        //                    npl.DeleteTransaction(transaction);
        //                }
        //                //else if (this.MaPhuKien == "N05")//chua
        //                //{
        //                //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //                //    npl.Ma = hang.MaHang;
        //                //    npl.HopDong_ID = HopDong_ID;
        //                //    npl.Load(transaction);
        //                //    npl.SoLuongDangKy = npl.SoLuongDangKy - (hang.SoLuong - Convert.ToDecimal(hang.ThongTinCu));
        //                //    npl.UpdateTransaction(transaction);
        //                //}
        //                //else if (this.MaPhuKien == "N06")
        //                //{
        //                //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //                //    npl.Ma = hang.MaHang;
        //                //    npl.HopDong_ID = HopDong_ID;
        //                //    npl.Load(transaction);
        //                //    npl.DVT_ID =DuLieuChuan.DonViTinh.GetID(hang.ThongTinCu);
        //                //    npl.UpdateTransaction(transaction);
        //                //}
        //                //else if (this.MaPhuKien == "N11")
        //                //{
        //                //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //                //    npl.Ma = hang.MaHang;
        //                //    npl.HopDong_ID = HopDong_ID;
        //                //    npl.Load(transaction);
        //                //    npl.SoLuongCungUng -= hang.SoLuong;
        //                //    npl.UpdateTransaction(transaction);
        //                //}
        //                //else if (this.MaPhuKien == "S06")
        //                //{
        //                //    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //                //    sp.Ma = hang.MaHang;
        //                //    sp.HopDong_ID = HopDong_ID;
        //                //    sp.Load(transaction);
        //                //    sp.DVT_ID = DuLieuChuan.DonViTinh.GetID(hang.ThongTinCu);
        //                //    sp.UpdateTransaction(transaction);
        //                //}
        //                //else if (this.MaPhuKien == "S10")
        //                //{
        //                //    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //                //    sp.Ma = hang.MaHang;
        //                //    sp.HopDong_ID = HopDong_ID;
        //                //    sp.Load(transaction);
        //                //    sp.Ma = hang.ThongTinCu;
        //                //    sp.UpdateMaSPTransaction(transaction, hang.MaHang);
        //                //    //cap nhat lai dinh muc neu co
        //                //    Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
        //                //    dm.HopDong_ID = HopDong_ID;
        //                //    dm.MaSanPham = sp.Ma;
        //                //    dm.UpdateMaDinhMucTransaction(transaction, hang.MaHang);
        //                //}
        //                else if (this.MaPhuKien == "S13")
        //                {
        //                    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //                    sp.Ma = hang.MaHang;
        //                    sp.HopDong_ID = HopDong_ID;
        //                    sp.DeleteTransaction(transaction);
        //                }
        //                else if (this.MaPhuKien == "S15")
        //                {
        //                    Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
        //                    LoaiSP.MaSanPham = hang.MaHang;
        //                    LoaiSP.HopDong_ID = HopDong_ID;
        //                    LoaiSP.DeleteTransaction(transaction);
        //                }
        //                //else if (this.MaPhuKien == "T01")
        //                //{
        //                //    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
        //                //    tb.Ma = hang.MaHang;
        //                //    tb.HopDong_ID = HopDong_ID;
        //                //    tb.TrangThai = 2;
        //                //    tb.Load(transaction);
        //                //    tb.SoLuongDangKy = Convert.ToDecimal(hang.ThongTinCu); ;
        //                //    tb.UpdateTransaction(transaction);
        //                //}
        //                else if (this.MaPhuKien == "T07")
        //                {
        //                    BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
        //                    entity.HopDong_ID = HopDong_ID;
        //                    entity.Ma = hang.MaHang;
        //                    entity.DeleteTransaction(transaction);
        //                }
        //            }
        //            this.DeleteTransaction(transaction);
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //    return true;
        //}

        //public void Delete(long HopDong_ID, SqlTransaction transaction)
        //{            
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {
        //        if (this.MaPhuKien == "N01")
        //        {
        //            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //            npl.HopDong_ID = HopDong_ID;
        //            npl.Ma = hang.MaHang;
        //            npl.Load(transaction);
        //            npl.DeleteTransaction(transaction);
        //        }               
        //        else if (this.MaPhuKien == "S13")
        //        {
        //            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //            sp.Ma = hang.MaHang;
        //            sp.HopDong_ID = HopDong_ID;
        //            sp.DeleteTransaction(transaction);
        //        }
        //        else if (this.MaPhuKien == "S15")
        //        {
        //            Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
        //            LoaiSP.MaSanPham = hang.MaHang;
        //            LoaiSP.HopDong_ID = HopDong_ID;
        //            LoaiSP.DeleteTransaction(transaction);
        //        }               
        //        else if (this.MaPhuKien == "T07")
        //        {
        //            BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
        //            entity.HopDong_ID = HopDong_ID;
        //            entity.Ma = hang.MaHang;
        //            entity.DeleteTransaction(transaction);
        //        }
        //    }
        //    this.DeleteTransaction(transaction);
        //}

        //public void InsertUpdateGiaHanHD(long HopDong_ID, SqlTransaction transaction)
        //{
        //    HopDong HD = new HopDong();
        //    HD.ID = HopDong_ID;
        //    HD.Load(transaction);
        //    HD.NgayGiaHan = Convert.ToDateTime(this.ThongTinMoi);
        //    HD.UpdateTransaction(transaction);
        //}

        //public void InsertUpdateHuyHD(long HopDong_ID, SqlTransaction transaction)
        //{
        //    HopDong HD = new HopDong();
        //    HD.ID = HopDong_ID;
        //    HD.Load(transaction);
        //    HD.TrangThaiXuLy = -2;
        //    HD.UpdateTransaction(transaction);
        //}

        //public void InsertUpdateMuaVN(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {               
        //        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //        npl.Ma = hang.MaHang;
        //        npl.HopDong_ID = HopDong_ID;
        //        npl.Load(transaction);
        //        npl.SoLuongCungUng += hang.SoLuong;
        //        npl.UpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateDieuChinhSoLuongThietBi(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {               
        //        Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
        //        tb.Ma = hang.MaHang;
        //        tb.HopDong_ID = HopDong_ID;
        //        tb.TrangThai = 2;
        //        tb.Load(transaction);
        //        tb.SoLuongDangKy = hang.SoLuong;
        //        tb.UpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateDieuChinhSoLuongNPL(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {               
        //        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //        npl.Ma = hang.MaHang;
        //        npl.HopDong_ID = HopDong_ID;
        //        npl.Load(transaction);
        //        npl.SoLuongDangKy = hang.SoLuong;
        //        npl.UpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateDieuChinhMaSP(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {
        //        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //        sp.Ma = hang.ThongTinCu;
        //        sp.HopDong_ID = HopDong_ID;
        //        sp.Load(transaction);
        //        sp.Ma = hang.MaHang;
        //        sp.UpdateMaSPTransaction(transaction, hang.ThongTinCu);
        //        Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
        //        dm.HopDong_ID = HopDong_ID;
        //        dm.MaSanPham = sp.Ma;
        //        dm.UpdateMaDinhMucTransaction(transaction, hang.ThongTinCu);
        //    }
        //}

        //public void InsertUpdateDieuChinhDVTSP(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {               
        //        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //        sp.Ma = hang.MaHang;
        //        sp.HopDong_ID = HopDong_ID;
        //        sp.Load(transaction);
        //        sp.DVT_ID = hang.DVT_ID;
        //        sp.UpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateDieuChinhDVTNPL(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {               
        //        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //        npl.Ma = hang.MaHang;
        //        npl.HopDong_ID = HopDong_ID;
        //        npl.Load(transaction);
        //        npl.DVT_ID = hang.DVT_ID;
        //        npl.UpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateBoSungNhomSamPhamGiaCong(long HopDong_ID)
        //{
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        long id = this.ID;
        //        try
        //        {
        //            if (this.MaPhuKien == "S15")
        //            {
        //                if (this.ID > 0)
        //                    this.UpdateTransaction(transaction);
        //                else
        //                    this.InsertTransaction(transaction);
        //                int stt = 1;
        //                foreach (HangPhuKien hang in this.HPKCollection)
        //                {
        //                    //hang.STTHang = stt++;
        //                    //hang.Master_ID = this.ID;
        //                    //if (hang.ID == 0)
        //                    //    hang.InsertTransaction(transaction);
        //                    //else
        //                    //    hang.UpdateTransaction(transaction);


        //                    Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
        //                    LoaiSP.MaSanPham = hang.MaHang;
        //                    LoaiSP.HopDong_ID = HopDong_ID;
        //                    LoaiSP.TrangThai = 2;
        //                    LoaiSP.TenSanPham = hang.TenHang;
        //                    LoaiSP.SoLuong = hang.SoLuong;
        //                    LoaiSP.GiaGiaCong = Convert.ToDecimal(hang.DonGia);
        //                    LoaiSP.InsertUpdateTransaction(transaction);
        //                }
        //            }
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            this.ID = id;
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        //public void InsertUpdateBoSungSP(long HopDong_ID)
        //{
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        long id = this.ID;
        //        try
        //        {
        //            if (this.MaPhuKien == "S13")
        //            {
        //                if (this.ID > 0)
        //                    this.UpdateTransaction(transaction);
        //                else
        //                    this.InsertTransaction(transaction);
        //                int stt = 1;
        //                foreach (HangPhuKien hang in this.HPKCollection)
        //                {
        //                    //hang.STTHang = stt++;
        //                    //hang.Master_ID = this.ID;
        //                    //if (hang.ID == 0)
        //                    //    hang.InsertTransaction(transaction);
        //                    //else
        //                    //    hang.UpdateTransaction(transaction);

        //                    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //                    sp.Ma = hang.MaHang;
        //                    sp.HopDong_ID = HopDong_ID;
        //                    sp.DVT_ID = hang.DVT_ID;
        //                    sp.MaHS = hang.MaHS;
        //                    sp.NhomSanPham_ID = hang.NhomSP;
        //                    sp.SoLuongDangKy = hang.SoLuong;
        //                    sp.TrangThai = 2;
        //                    sp.Ten = hang.TenHang;
        //                    sp.InsertUpdateTransaction(transaction);
        //                }
        //            }
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            this.ID = id;
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        //public void InsertUpdateBoSungNPL(long HopDong_ID)
        //{
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        long id = this.ID;
        //        try
        //        {
        //            if (this.MaPhuKien == "N01")
        //            {
        //                if (this.ID > 0)
        //                    this.UpdateTransaction(transaction);
        //                else
        //                    this.InsertTransaction(transaction);
        //                int stt = 1;
        //                foreach (HangPhuKien hang in this.HPKCollection)
        //                {
        //                    //hang.STTHang = stt++;
        //                    //hang.Master_ID = this.ID;
        //                    //if (hang.ID == 0)
        //                    //    hang.InsertTransaction(transaction);
        //                    //else
        //                    //    hang.UpdateTransaction(transaction);

        //                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //                    npl.DVT_ID = hang.DVT_ID;
        //                    npl.HopDong_ID = HopDong_ID;
        //                    npl.Ten = hang.TenHang;
        //                    npl.SoLuongDangKy = hang.SoLuong;
        //                    npl.Ma = hang.MaHang;
        //                    npl.MaHS = hang.MaHS;
        //                    npl.TrangThai = 2;
        //                    npl.InsertUpdateTransaction(transaction);
        //                }
        //            }
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            this.ID = id;
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        //public void InsertUpdateBoSungTB(long HopDong_ID)
        //{
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        long id = this.ID;
        //        try
        //        {
        //            if (this.MaPhuKien == "T07")
        //            {
        //                if (this.ID > 0)
        //                    this.UpdateTransaction(transaction);
        //                else
        //                    this.InsertTransaction(transaction);
        //                int stt = 1;
        //                foreach (HangPhuKien hang in this.HPKCollection)
        //                {
        //                    //hang.STTHang = stt++;
        //                    //hang.Master_ID = this.ID;
        //                    //if (hang.ID == 0)
        //                    //    hang.InsertTransaction(transaction);
        //                    //else
        //                    //    hang.UpdateTransaction(transaction);

        //                    BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
        //                    entity.HopDong_ID = HopDong_ID;
        //                    entity.Ma = hang.MaHang;
        //                    entity.DVT_ID = hang.DVT_ID;
        //                    entity.MaHS = hang.MaHS;
        //                    entity.DonGia = hang.DonGia;
        //                    entity.SoLuongDangKy = hang.SoLuong;
        //                    entity.Ten = hang.TenHang;
        //                    entity.NguyenTe_ID = hang.NguyenTe_ID;
        //                    entity.NuocXX_ID = hang.NuocXX_ID;
        //                    entity.TinhTrang = hang.TinhTrang;
        //                    entity.TriGia = hang.TriGia;
        //                    entity.TrangThai = 2;
        //                    entity.InsertUpdateTransaction(transaction);
        //                }
        //            }
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            this.ID = id;
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        //kiem tra co phu kien cung loai nao duoc mo nhung chua duyet ko
        public long checkPhuKienCungLoai(string MaPhuKien, long idthisDK)
        {
            string sql = "select id from t_View_PhuKienDangKy " +
                        "where MaPhuKien=@MaPhuKien and trangthaixuly<>1 and id<>@idthisDK";

            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@MaPhuKien", SqlDbType.Char, MaPhuKien);
            this.db.AddInParameter(dbCommand, "@idthisDK", SqlDbType.BigInt, idthisDK);

            long id = Convert.ToInt64(db.ExecuteScalar(dbCommand));
            return id;
        }
        public bool Delete(long HopDong_ID)
        {
            this.LoadCollection();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    if (this.MaPhuKien.Trim() == "201")
                    {
                        HopDong HD = new HopDong();
                        HD.ID = HopDong_ID;
                        HD.Load(transaction);
                        HD.NgayGiaHan = Convert.ToDateTime(this.ThongTinCu);
                        if (HD.NgayGiaHan == HD.NgayHetHan)
                            HD.NgayGiaHan = new DateTime(1900, 1, 1);
                        HD.Update(transaction);
                    }
                    else if (this.MaPhuKien.Trim() == "101")
                    {
                        HopDong HD = new HopDong();
                        HD.ID = HopDong_ID;
                        HD.Load(transaction);
                        HD.TrangThaiXuLy = 1;
                        HD.Update(transaction);
                    }
                    else if (this.MaPhuKien == "H11")
                    {
                        ;
                    }
                    foreach (HangPhuKien hang in this.HPKCollection)
                    {
                        if (this.MaPhuKien.Trim() == "N01")
                        {
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.HopDong_ID = HopDong_ID;
                            npl.Ma = hang.MaHang;
                            npl.Load(transaction);
                            npl.DeleteTransaction(transaction);
                        }
                        else if (this.MaPhuKien.Trim() == "N05")//chua
                        {
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.Ma = hang.MaHang;
                            npl.HopDong_ID = HopDong_ID;
                            npl.Load(transaction);
                            npl.SoLuongDangKy = npl.SoLuongDangKy - (hang.SoLuong - Convert.ToDecimal(hang.ThongTinCu));
                            npl.TrangThai = 0;
                            npl.UpdateTransaction(transaction);
                        }
                        else if (this.MaPhuKien.Trim() == "N06")
                        {
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.Ma = hang.MaHang;
                            npl.HopDong_ID = HopDong_ID;
                            npl.Load(transaction);
                            npl.DVT_ID = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetID(hang.ThongTinCu);
                            npl.TrangThai = 0;
                            npl.UpdateTransaction(transaction);
                        }
                        else if (this.MaPhuKien.Trim() == "N11")
                        {
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.Ma = hang.MaHang;
                            npl.HopDong_ID = HopDong_ID;
                            npl.Load(transaction);
                            npl.SoLuongCungUng -= hang.SoLuong;
                            npl.UpdateTransaction(transaction);
                        }
                        else if (this.MaPhuKien.Trim() == "S06")
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hang.MaHang;
                            sp.HopDong_ID = HopDong_ID;
                            sp.Load(transaction);
                            sp.DVT_ID = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetID(hang.ThongTinCu);
                            sp.TrangThai = 0;
                            sp.UpdateTransaction(transaction);
                        }
                        else if (this.MaPhuKien.Trim() == "S10")
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hang.MaHang;
                            sp.HopDong_ID = HopDong_ID;
                            sp.Load(transaction);
                            sp.Ma = hang.ThongTinCu;
                            sp.TrangThai = 0;
                            sp.UpdateMaSPTransaction(transaction, hang.MaHang);
                            //cap nhat lai dinh muc neu co
                            Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                            dm.HopDong_ID = HopDong_ID;
                            dm.MaSanPham = sp.Ma;
                            dm.UpdateMaDinhMucTransaction(transaction, hang.MaHang);
                        }
                        else if (this.MaPhuKien.Trim() == "S13")
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hang.MaHang;
                            sp.HopDong_ID = HopDong_ID;
                            sp.DeleteTransaction(transaction);
                        }
                        else if (this.MaPhuKien.Trim() == "802")
                        {
                            Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
                            LoaiSP.MaSanPham = hang.MaHang;
                            LoaiSP.HopDong_ID = HopDong_ID;
                            LoaiSP.DeleteTransaction(transaction);
                        }
                        else if (this.MaPhuKien.Trim() == "S09")
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hang.MaHang;
                            sp.HopDong_ID = HopDong_ID;
                            sp.Load(transaction);
                            sp.SoLuongDangKy = sp.SoLuongDangKy - (hang.SoLuong - Convert.ToDecimal(hang.ThongTinCu));
                            sp.TrangThai = 0;
                            sp.UpdateTransaction(transaction);
                        }
                        else if (this.MaPhuKien.Trim() == "T01")
                        {
                            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                            tb.Ma = hang.MaHang;
                            tb.HopDong_ID = HopDong_ID;
                            tb.Load(transaction);
                            tb.TrangThai = 0;
                            tb.SoLuongDangKy = Convert.ToDecimal(hang.ThongTinCu); ;
                            tb.UpdateTransaction(transaction);
                        }
                        else if (this.MaPhuKien.Trim() == "T07")
                        {
                            BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                            entity.HopDong_ID = HopDong_ID;
                            entity.Ma = hang.MaHang;
                            entity.DeleteTransaction(transaction);
                        }
                    }
                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }
        public void Delete(long HopDong_ID, SqlTransaction transaction)
        {
            if (this.MaPhuKien.Trim() == "H06")
            {
                HopDong HD = new HopDong();
                HD.ID = HopDong_ID;
                HD.Load(transaction);
                HD.NgayGiaHan = Convert.ToDateTime(this.ThongTinCu);
                if (HD.NgayGiaHan == HD.NgayHetHan)
                    HD.NgayGiaHan = new DateTime(1900, 1, 1);
                HD.Update(transaction);
            }
            else if (this.MaPhuKien.Trim() == "H10")
            {
                HopDong HD = new HopDong();
                HD.ID = HopDong_ID;
                HD.Load(transaction);
                HD.TrangThaiXuLy = 1;
                HD.Update(transaction);
            }
            else if (this.MaPhuKien.Trim() == "H11")
            {
                ;
            }
            foreach (HangPhuKien hang in this.HPKCollection)
            {
                if (this.MaPhuKien.Trim() == "N01")
                {
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HopDong_ID;
                    npl.Ma = hang.MaHang;
                    npl.Load(transaction);
                    npl.DeleteTransaction(transaction);
                }
                else if (this.MaPhuKien.Trim() == "N05")//chua
                {
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.Ma = hang.MaHang;
                    npl.HopDong_ID = HopDong_ID;
                    npl.Load(transaction);
                    npl.SoLuongDangKy = npl.SoLuongDangKy - (hang.SoLuong - Convert.ToDecimal(hang.ThongTinCu));
                    npl.TrangThai = 0;
                    npl.UpdateTransaction(transaction);
                }
                else if (this.MaPhuKien.Trim() == "N06")
                {
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.Ma = hang.MaHang;
                    npl.HopDong_ID = HopDong_ID;
                    npl.Load(transaction);
                    npl.DVT_ID = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetID(hang.ThongTinCu);
                    npl.TrangThai = 0;
                    npl.UpdateTransaction(transaction);
                }
                else if (this.MaPhuKien.Trim() == "N11")
                {
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.Ma = hang.MaHang;
                    npl.HopDong_ID = HopDong_ID;
                    npl.Load(transaction);
                    npl.SoLuongCungUng -= hang.SoLuong;
                    npl.UpdateTransaction(transaction);
                }
                else if (this.MaPhuKien.Trim() == "S06")
                {
                    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                    sp.Ma = hang.MaHang;
                    sp.HopDong_ID = HopDong_ID;
                    sp.Load(transaction);
                    sp.DVT_ID = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetID(hang.ThongTinCu);
                    sp.TrangThai = 0;
                    sp.UpdateTransaction(transaction);
                }
                else if (this.MaPhuKien.Trim() == "S09")//chua
                {
                    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                    sp.Ma = hang.MaHang;
                    sp.HopDong_ID = HopDong_ID;
                    sp.Load(transaction);
                    sp.SoLuongDangKy = sp.SoLuongDangKy - (hang.SoLuong - Convert.ToDecimal(hang.ThongTinCu));
                    sp.TrangThai = 0;
                    sp.UpdateTransaction(transaction);
                }
                else if (this.MaPhuKien.Trim() == "S10")
                {
                    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                    sp.Ma = hang.MaHang;
                    sp.HopDong_ID = HopDong_ID;
                    sp.Load(transaction);
                    sp.Ma = hang.ThongTinCu;
                    sp.TrangThai = 0;
                    sp.UpdateMaSPTransaction(transaction, hang.MaHang);
                    //cap nhat lai dinh muc neu co
                    Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                    dm.HopDong_ID = HopDong_ID;
                    dm.MaSanPham = sp.Ma;
                    dm.UpdateMaDinhMucTransaction(transaction, hang.MaHang);
                }
                else if (this.MaPhuKien.Trim() == "S13")
                {
                    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                    sp.Ma = hang.MaHang;
                    sp.HopDong_ID = HopDong_ID;
                    sp.DeleteTransaction(transaction);
                }
                else if (this.MaPhuKien.Trim() == "802")
                {
                    Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
                    LoaiSP.MaSanPham = hang.MaHang;
                    LoaiSP.HopDong_ID = HopDong_ID;
                    LoaiSP.DeleteTransaction(transaction);
                }
                else if (this.MaPhuKien.Trim() == "T01")
                {
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.Ma = hang.MaHang;
                    tb.HopDong_ID = HopDong_ID;
                    tb.Load(transaction);
                    tb.TrangThai = 0;
                    tb.SoLuongDangKy = Convert.ToDecimal(hang.ThongTinCu); ;
                    tb.UpdateTransaction(transaction);
                }
                else if (this.MaPhuKien.Trim() == "T07")
                {
                    BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                    entity.HopDong_ID = HopDong_ID;
                    entity.Ma = hang.MaHang;
                    entity.DeleteTransaction(transaction);
                }
            }
            this.DeleteTransaction(transaction);
        }
        public void InsertUpdateGiaHanHD(long HopDong_ID)
        {
            HopDong HD = new HopDong();
            HD.ID = HopDong_ID;

            HD = HopDong.Load(HD.ID);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.MaPhuKien.Trim() == "201")
                    {
                        if (this.ID == 0)
                            this.InsertTransaction(transaction);
                        else
                            this.UpdateTransaction(transaction);
                        HD.NgayGiaHan = Convert.ToDateTime(this.ThongTinMoi);
                        HD.Update(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateMuaVN(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "N11")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            if (id == 0)
                                hang.ID = 0;
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;

                            long idHangPK = hang.ID;
                            HangPhuKien HPK = new HangPhuKien();
                            if (idHangPK > 0)
                            {
                                HPK.ID = idHangPK;
                                HPK.Load(transaction);
                            }

                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);

                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.Ma = hang.MaHang;
                            npl.HopDong_ID = HopDong_ID;
                            npl.Load(transaction);
                            if (idHangPK > 0)
                            {
                                npl.SoLuongCungUng -= HPK.SoLuong;
                            }
                            npl.SoLuongCungUng += hang.SoLuong;
                            npl.DonGia = Convert.ToDecimal(hang.DonGia);
                            npl.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDieuChinhSoLuongThietBi(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "T01")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                            tb.Ma = hang.MaHang;
                            tb.HopDong_ID = HopDong_ID;
                            tb.Load(transaction);
                            tb.TrangThai = 5;
                            tb.SoLuongDangKy = hang.SoLuong;
                            tb.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDieuChinhSoLuongThietBiDaDangKy(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "T01")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                            tb.Ma = hang.MaHang;
                            tb.HopDong_ID = HopDong_ID;
                            tb.Load(transaction);
                            tb.TrangThai = 0;
                            tb.SoLuongDangKy = hang.SoLuong;
                            tb.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDieuChinhSoLuongNPL(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "N05")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.Ma = hang.MaHang;
                            npl.HopDong_ID = HopDong_ID;
                            npl.Load(transaction);
                            npl.SoLuongDangKy = hang.SoLuong;
                            npl.TrangThai = 5;
                            npl.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDieuChinhSoLuongNPLDaDangKy(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "N05")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.Ma = hang.MaHang;
                            npl.HopDong_ID = HopDong_ID;
                            npl.Load(transaction);
                            npl.SoLuongDangKy = hang.SoLuong;
                            npl.TrangThai = 0;
                            npl.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDieuChinhSoLuongSPDaDangKy(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "S09")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hang.MaHang;
                            sp.HopDong_ID = HopDong_ID;
                            sp.Load(transaction);
                            sp.SoLuongDangKy = hang.SoLuong;
                            sp.TrangThai = 0;
                            sp.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDieuChinhSoLuongSP(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "S09")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hang.MaHang;
                            sp.HopDong_ID = HopDong_ID;
                            sp.Load(transaction);
                            sp.SoLuongDangKy = hang.SoLuong;
                            sp.TrangThai = 5;
                            sp.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDieuChinhMaSP(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "S10")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);

                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hang.ThongTinCu;
                            sp.HopDong_ID = HopDong_ID;
                            sp.Load(transaction);
                            sp.Ma = hang.MaHang;
                            sp.TrangThai = 4;
                            sp.UpdateMaSPTransaction(transaction, hang.ThongTinCu);
                            //cap nhat lai dinh muc neu co
                            Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                            dm.HopDong_ID = HopDong_ID;
                            dm.MaSanPham = sp.Ma;
                            dm.UpdateMaDinhMucTransaction(transaction, hang.ThongTinCu);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDieuChinhMaSPDaDangKy(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "S10")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);

                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hang.ThongTinCu;
                            sp.HopDong_ID = HopDong_ID;
                            sp.Load(transaction);
                            sp.Ma = hang.MaHang;
                            sp.TrangThai = 0;
                            sp.UpdateMaSPTransaction(transaction, hang.ThongTinCu);
                            //cap nhat lai dinh muc neu co
                            Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                            dm.HopDong_ID = HopDong_ID;
                            dm.MaSanPham = sp.Ma;
                            dm.UpdateMaDinhMucTransaction(transaction, hang.ThongTinCu);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDieuChinhDVTSP(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "S06")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);

                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hang.MaHang;
                            sp.HopDong_ID = HopDong_ID;
                            sp.Load(transaction);
                            sp.DVT_ID = hang.DVT_ID;
                            sp.TrangThai = 3;
                            sp.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDieuChinhDVTSPDaDangKy(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "S06")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);

                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hang.MaHang;
                            sp.HopDong_ID = HopDong_ID;
                            sp.Load(transaction);
                            sp.DVT_ID = hang.DVT_ID;
                            sp.TrangThai = 0;
                            sp.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDieuChinhDVTNPL(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "N06")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);

                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.Ma = hang.MaHang;
                            npl.HopDong_ID = HopDong_ID;
                            npl.Load(transaction);
                            npl.DVT_ID = hang.DVT_ID;
                            npl.TrangThai = 3;
                            npl.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void InsertUpdateDieuChinhDVTNPLDaDangKy(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "N06")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);

                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.Ma = hang.MaHang;
                            npl.HopDong_ID = HopDong_ID;
                            npl.Load(transaction);
                            npl.DVT_ID = hang.DVT_ID;
                            npl.TrangThai = 3;
                            npl.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void InsertUpdateBoSungChiTietLoaiPhuKien(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.ID > 0)
                        this.UpdateTransaction(transaction);
                    else
                        this.InsertTransaction(transaction);
                    int stt = 1;
                    foreach (HangPhuKien hang in this.HPKCollection)
                    {
                        hang.STTHang = stt++;
                        hang.Master_ID = this.ID;
                        if (hang.ID == 0)
                            hang.InsertTransaction(transaction);
                        else
                            hang.UpdateTransaction(transaction);
                        if (hang.ThuTucTruocDo != null)
                        {
                            hang.ThuTucTruocDo.Master_ID = hang.ID;
                            hang.ThuTucTruocDo.InsertUpdateFull(transaction);
                        }
                        //Comment by hungtq, 11/03/2013. Du lieu them sai vi tri. Phai them vao San pham thay vi Nhom san pham
                        if (!string.IsNullOrEmpty(hang.NhomSP))
                        {
                            Company.GC.BLL.GC.NhomSanPham nhomSP = new Company.GC.BLL.GC.NhomSanPham();
                            nhomSP.MaSanPham = hang.MaHang;
                            nhomSP.HopDong_ID = HopDong_ID;
                            nhomSP.TrangThai = 2;
                            nhomSP.TenSanPham = hang.TenHang;
                            nhomSP.SoLuong = hang.SoLuong;
                            nhomSP.GiaGiaCong = Convert.ToDecimal(hang.DonGia);
                            nhomSP.InsertUpdateTransaction(transaction);
                        }

                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void InsertUpdateBoSungDaDangKy(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {

                    if (this.ID > 0)
                        this.UpdateTransaction(transaction);
                    else
                        this.InsertTransaction(transaction);
                    int stt = 1;
                    foreach (HangPhuKien hang in this.HPKCollection)
                    {
                        hang.STTHang = stt++;
                        hang.Master_ID = this.ID;
                        if (hang.ID == 0)
                            hang.InsertTransaction(transaction);
                        else
                            hang.UpdateTransaction(transaction);
                        if (hang.ThuTucTruocDo != null)
                        {
                            hang.ThuTucTruocDo.Master_ID = hang.ID;
                            hang.ThuTucTruocDo.InsertUpdateFull(transaction);
                        }
                        if (!string.IsNullOrEmpty(hang.NhomSP))
                        {
                            Company.GC.BLL.GC.NhomSanPham nhomSP = new Company.GC.BLL.GC.NhomSanPham();
                            nhomSP.MaSanPham = hang.MaHang;
                            nhomSP.HopDong_ID = HopDong_ID;
                            nhomSP.TrangThai = 0;
                            nhomSP.TenSanPham = hang.TenHang;
                            nhomSP.SoLuong = hang.SoLuong;
                            nhomSP.GiaGiaCong = Convert.ToDecimal(hang.DonGia);
                            nhomSP.InsertUpdateTransaction(transaction);
                        }
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        
        public void InsertUpdateBoSungSP(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "S13" || this.MaPhuKien.Trim() == "802" || this.MaPhuKien.Trim() == "502")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            if (hang.ThuTucTruocDo != null)
                            {
                                hang.ThuTucTruocDo.Master_ID = hang.ID;
                                hang.ThuTucTruocDo.InsertUpdateFull(transaction);
                            }
                            //Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            //sp.Ma = hang.MaHang;
                            //sp.HopDong_ID = HopDong_ID;
                            //sp.DVT_ID = hang.DVT_ID;
                            //sp.MaHS = hang.MaHS;
                            //sp.NhomSanPham_ID = hang.NhomSP;
                            //sp.SoLuongDangKy = hang.SoLuong;
                            //sp.DonGia = Convert.ToDecimal(hang.DonGia);
                            //sp.TrangThai = 2;
                            //sp.Ten = hang.TenHang;
                            //sp.InsertUpdateTransaction(transaction);

                            //Company.GC.BLL.KDT.GC.SanPham SP = new SanPham();
                            //SP.Ma = hang.MaHang;
                            //SP.HopDong_ID = HopDong_ID;
                            //SP.DVT_ID = hang.DVT_ID;
                            //SP.MaHS = hang.MaHS;
                            //SP.NhomSanPham_ID = hang.NhomSP;
                            //SP.SoLuongDangKy = hang.SoLuong;
                            //SP.DonGia = Convert.ToDecimal(hang.DonGia);
                            //SP.Ten = hang.TenHang;
                            //SP.InsertUpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateBoSungSPDaDangKy(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "S13" || this.MaPhuKien.Trim() == "802" || this.MaPhuKien.Trim() == "502")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            if (hang.ThuTucTruocDo != null)
                            {
                                hang.ThuTucTruocDo.Master_ID = hang.ID;
                                hang.ThuTucTruocDo.InsertUpdateFull(transaction);
                            }

                            #region cap nhat San Pham vào HD da dangky
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hang.MaHang;
                            sp.HopDong_ID = HopDong_ID;
                            sp.DVT_ID = hang.DVT_ID;
                            sp.MaHS = hang.MaHS;
                            sp.NhomSanPham_ID = hang.NhomSP;
                            sp.SoLuongDangKy = hang.SoLuong;
                            sp.DonGia = Convert.ToDecimal(hang.DonGia);
                            sp.TrangThai = 0;
                            sp.Ten = hang.TenHang;
                            sp.InsertUpdateTransaction(transaction);
                            #endregion

                            #region cap nhat San pham vao HD khai bao
                            Company.GC.BLL.KDT.GC.SanPham sp_td = new Company.GC.BLL.KDT.GC.SanPham();
                            sp_td.Ma = hang.MaHang;
                            sp_td.HopDong_ID = HopDong_ID;
                            sp_td.DVT_ID = hang.DVT_ID;
                            sp_td.MaHS = hang.MaHS;
                            sp_td.NhomSanPham_ID = hang.NhomSP;
                            sp_td.SoLuongDangKy = hang.SoLuong;
                            sp_td.DonGia = Convert.ToDecimal(hang.DonGia);
                            sp_td.Ten = hang.TenHang;
                            sp_td.InsertUpdateTransaction(transaction);
                            #endregion
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateBoSungNPL(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {

                    if (this.ID > 0)
                        this.UpdateTransaction(transaction);
                    else
                        this.InsertTransaction(transaction);
                    int stt = 1;
                    foreach (HangPhuKien hang in this.HPKCollection)
                    {
                        hang.STTHang = stt++;
                        hang.Master_ID = this.ID;
                        if (hang.ID == 0)
                            hang.InsertTransaction(transaction);
                        else
                            hang.UpdateTransaction(transaction);
                        if (hang.ThuTucTruocDo != null)
                        {
                            hang.ThuTucTruocDo.Master_ID = hang.ID;
                            hang.ThuTucTruocDo.InsertUpdateFull(transaction);
                        }
                        //#region cap nhat NPL  vao HD da dangky
                        //Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        //npl.DVT_ID = hang.DVT_ID;
                        //npl.HopDong_ID = HopDong_ID;
                        //npl.Ten = hang.TenHang;
                        //npl.SoLuongDangKy = hang.SoLuong;
                        //npl.DonGia = (decimal)hang.DonGia;
                        //npl.Ma = hang.MaHang;
                        //npl.MaHS = hang.MaHS;
                        //npl.TrangThai = 0;
                        //npl.InsertUpdateTransaction(transaction);
                        //#endregion

                        //#region cap nhat NPL vao HD khai bao
                        //Company.GC.BLL.KDT.GC.NguyenPhuLieu npl_td = new Company.GC.BLL.KDT.GC.NguyenPhuLieu();
                        //npl_td.DVT_ID = hang.DVT_ID;
                        //npl_td.HopDong_ID = HopDong_ID;
                        //npl_td.Ten = hang.TenHang;
                        //npl_td.SoLuongDangKy = hang.SoLuong;
                        //npl_td.DonGia = (decimal)hang.DonGia;
                        //npl_td.Ma = hang.MaHang;
                        //npl_td.MaHS = hang.MaHS;
                        //npl_td.GhiChu = "PK" + ID.ToString();
                        //npl_td.InsertUpdate(transaction);
                        //#endregion
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateBoSungNPLDaDangKy(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.MaPhuKien.Trim() == "N01")
                    {
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            if (hang.ThuTucTruocDo != null)
                            {
                                hang.ThuTucTruocDo.Master_ID = hang.ID;
                                hang.ThuTucTruocDo.InsertUpdateFull(transaction);
                            }
                            #region cap nhat NPL  vao HD da dangky
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.DVT_ID = hang.DVT_ID;
                            npl.HopDong_ID = HopDong_ID;
                            npl.Ten = hang.TenHang;
                            npl.SoLuongDangKy = hang.SoLuong;
                            npl.DonGia = (decimal)hang.DonGia;
                            npl.Ma = hang.MaHang;
                            npl.MaHS = hang.MaHS;
                            npl.TrangThai = 0;
                            npl.InsertUpdateTransaction(transaction);
                            #endregion

                            #region cap nhat NPL vao HD khai bao
                            Company.GC.BLL.KDT.GC.NguyenPhuLieu npl_td = new Company.GC.BLL.KDT.GC.NguyenPhuLieu();
                            npl_td.DVT_ID = hang.DVT_ID;
                            npl_td.HopDong_ID = HopDong_ID;
                            npl_td.Ten = hang.TenHang;
                            npl_td.SoLuongDangKy = hang.SoLuong;
                            npl_td.DonGia = (decimal)hang.DonGia;
                            npl_td.Ma = hang.MaHang;
                            npl_td.MaHS = hang.MaHS;
                            npl_td.GhiChu = "PK" + ID.ToString();
                            npl_td.InsertUpdate(transaction);
                            #endregion
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateBoSungTB(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    //if (this.MaPhuKien.Trim() == "T07")
                    //{
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            if (hang.ThuTucTruocDo != null)
                            {
                                hang.ThuTucTruocDo.Master_ID = hang.ID;
                                hang.ThuTucTruocDo.InsertUpdateFull(transaction);
                            }
                            //#region cap nhat Thiet Bị da dang ky
                            //BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                            //entity.HopDong_ID = HopDong_ID;
                            //entity.Ma = hang.MaHang;
                            //entity.DVT_ID = hang.DVT_ID;
                            //entity.MaHS = hang.MaHS;
                            //entity.DonGia = hang.DonGia;
                            //entity.SoLuongDangKy = hang.SoLuong;
                            //entity.Ten = hang.TenHang;
                            //entity.NguyenTe_ID = hang.NguyenTe_ID;
                            //entity.NuocXX_ID = hang.NuocXX_ID;
                            //entity.TinhTrang = hang.TinhTrang;
                            //entity.TriGia = hang.TriGia;
                            //entity.TrangThai = 0;//Trạng thái đã đăng ký
                            //entity.InsertUpdateTransaction(transaction);
                            //#endregion
                            //#region cap nhat Thiet Bị theo doi HD
                            //Company.GC.BLL.KDT.GC.ThietBi tb = new Company.GC.BLL.KDT.GC.ThietBi();
                            //tb.HopDong_ID = HopDong_ID;
                            //tb.Ma = hang.MaHang;
                            //tb.DVT_ID = hang.DVT_ID;
                            //tb.MaHS = hang.MaHS;
                            //tb.DonGia = hang.DonGia;
                            //tb.SoLuongDangKy = hang.SoLuong;
                            //tb.Ten = hang.TenHang;
                            //tb.NguyenTe_ID = hang.NguyenTe_ID;
                            //tb.NuocXX_ID = hang.NuocXX_ID;
                            //tb.TinhTrang = hang.TinhTrang;
                            //tb.TriGia = hang.TriGia;
                            //tb.GhiChu = "PK" + ID.ToString();
                            //tb.InsertUpdate(transaction);
                            //#endregion
                        }
                    //}
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateBoSungTBDadangKy(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    //if (this.MaPhuKien.Trim() == "804")
                    //{
                        if (this.ID > 0)
                            this.UpdateTransaction(transaction);
                        else
                            this.InsertTransaction(transaction);
                        int stt = 1;
                        foreach (HangPhuKien hang in this.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = this.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            if (hang.ThuTucTruocDo != null)
                            {
                                hang.ThuTucTruocDo.Master_ID = hang.ID;
                                hang.ThuTucTruocDo.InsertUpdateFull(transaction);
                            }
                            #region cap nhat Thiet Bị da dang ky
                            BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                            entity.HopDong_ID = HopDong_ID;
                            entity.Ma = hang.MaHang;
                            entity.DVT_ID = hang.DVT_ID;
                            entity.MaHS = hang.MaHS;
                            entity.DonGia = hang.DonGia;
                            entity.SoLuongDangKy = hang.SoLuong;
                            entity.Ten = hang.TenHang;
                            entity.NguyenTe_ID = hang.NguyenTe_ID;
                            entity.NuocXX_ID = hang.NuocXX_ID;
                            entity.TinhTrang = hang.TinhTrang;
                            entity.TriGia = hang.TriGia;
                            entity.TrangThai = 0;//Trạng thái đã đăng ký
                            entity.InsertUpdateTransaction(transaction);
                            #endregion
                            #region cap nhat Thiet Bị theo doi HD
                            Company.GC.BLL.KDT.GC.ThietBi tb = new Company.GC.BLL.KDT.GC.ThietBi();
                            tb.HopDong_ID = HopDong_ID;
                            tb.Ma = hang.MaHang;
                            tb.DVT_ID = hang.DVT_ID;
                            tb.MaHS = hang.MaHS;
                            tb.DonGia = hang.DonGia;
                            tb.SoLuongDangKy = hang.SoLuong;
                            tb.Ten = hang.TenHang;
                            tb.NguyenTe_ID = hang.NguyenTe_ID;
                            tb.NuocXX_ID = hang.NuocXX_ID;
                            tb.TinhTrang = hang.TinhTrang;
                            tb.TriGia = hang.TriGia;
                            tb.GhiChu = "PK" + ID.ToString();
                            tb.InsertUpdate(transaction);
                            #endregion
                        }
                    //}
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        //public void Delete(long HopDong_ID, SqlTransaction transaction)
        //{            
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {
        //        if (this.MaPhuKien == "N01")
        //        {
        //            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //            npl.HopDong_ID = HopDong_ID;
        //            npl.Ma = hang.MaHang;
        //            npl.Load(transaction);
        //            npl.DeleteTransaction(transaction);
        //        }               
        //        else if (this.MaPhuKien == "S13")
        //        {
        //            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //            sp.Ma = hang.MaHang;
        //            sp.HopDong_ID = HopDong_ID;
        //            sp.DeleteTransaction(transaction);
        //        }
        //        else if (this.MaPhuKien == "S15")
        //        {
        //            Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
        //            LoaiSP.MaSanPham = hang.MaHang;
        //            LoaiSP.HopDong_ID = HopDong_ID;
        //            LoaiSP.DeleteTransaction(transaction);
        //        }               
        //        else if (this.MaPhuKien == "T07")
        //        {
        //            BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
        //            entity.HopDong_ID = HopDong_ID;
        //            entity.Ma = hang.MaHang;
        //            entity.DeleteTransaction(transaction);
        //        }
        //    }
        //    this.DeleteTransaction(transaction);
        //}

        //public void InsertUpdateGiaHanHD(long HopDong_ID, SqlTransaction transaction)
        //{
        //    HopDong HD = new HopDong();
        //    HD.ID = HopDong_ID;
        //    HD.Load(transaction);
        //    HD.NgayGiaHan = Convert.ToDateTime(this.ThongTinMoi);
        //    HD.UpdateTransaction(transaction);
        //}

        //public void InsertUpdateHuyHD(long HopDong_ID, SqlTransaction transaction)
        //{
        //    HopDong HD = new HopDong();
        //    HD.ID = HopDong_ID;
        //    HD.Load(transaction);
        //    HD.TrangThaiXuLy = -2;
        //    HD.UpdateTransaction(transaction);
        //}

        //public void InsertUpdateMuaVN(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {               
        //        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //        npl.Ma = hang.MaHang;
        //        npl.HopDong_ID = HopDong_ID;
        //        npl.Load(transaction);
        //        npl.SoLuongCungUng += hang.SoLuong;
        //        npl.UpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateDieuChinhSoLuongThietBi(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {               
        //        Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
        //        tb.Ma = hang.MaHang;
        //        tb.HopDong_ID = HopDong_ID;
        //        tb.TrangThai = 2;
        //        tb.Load(transaction);
        //        tb.SoLuongDangKy = hang.SoLuong;
        //        tb.UpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateDieuChinhSoLuongNPL(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {               
        //        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //        npl.Ma = hang.MaHang;
        //        npl.HopDong_ID = HopDong_ID;
        //        npl.Load(transaction);
        //        npl.SoLuongDangKy = hang.SoLuong;
        //        npl.UpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateDieuChinhMaSP(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {
        //        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //        sp.Ma = hang.ThongTinCu;
        //        sp.HopDong_ID = HopDong_ID;
        //        sp.Load(transaction);
        //        sp.Ma = hang.MaHang;
        //        sp.UpdateMaSPTransaction(transaction, hang.ThongTinCu);
        //        Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
        //        dm.HopDong_ID = HopDong_ID;
        //        dm.MaSanPham = sp.Ma;
        //        dm.UpdateMaDinhMucTransaction(transaction, hang.ThongTinCu);
        //    }
        //}

        //public void InsertUpdateDieuChinhDVTSP(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {               
        //        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //        sp.Ma = hang.MaHang;
        //        sp.HopDong_ID = HopDong_ID;
        //        sp.Load(transaction);
        //        sp.DVT_ID = hang.DVT_ID;
        //        sp.UpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateDieuChinhDVTNPL(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {               
        //        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //        npl.Ma = hang.MaHang;
        //        npl.HopDong_ID = HopDong_ID;
        //        npl.Load(transaction);
        //        npl.DVT_ID = hang.DVT_ID;
        //        npl.UpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateBoSungNhomSamPhamGiaCong(long HopDong_ID)
        //{
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        long id = this.ID;
        //        try
        //        {
        //            if (this.MaPhuKien == "S15")
        //            {
        //                if (this.ID > 0)
        //                    this.UpdateTransaction(transaction);
        //                else
        //                    this.InsertTransaction(transaction);
        //                int stt = 1;
        //                foreach (HangPhuKien hang in this.HPKCollection)
        //                {
        //                    //hang.STTHang = stt++;
        //                    //hang.Master_ID = this.ID;
        //                    //if (hang.ID == 0)
        //                    //    hang.InsertTransaction(transaction);
        //                    //else
        //                    //    hang.UpdateTransaction(transaction);


        //                    Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
        //                    LoaiSP.MaSanPham = hang.MaHang;
        //                    LoaiSP.HopDong_ID = HopDong_ID;
        //                    LoaiSP.TrangThai = 2;
        //                    LoaiSP.TenSanPham = hang.TenHang;
        //                    LoaiSP.SoLuong = hang.SoLuong;
        //                    LoaiSP.GiaGiaCong = Convert.ToDecimal(hang.DonGia);
        //                    LoaiSP.InsertUpdateTransaction(transaction);
        //                }
        //            }
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            this.ID = id;
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        //public void InsertUpdateBoSungSP(long HopDong_ID)
        //{
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        long id = this.ID;
        //        try
        //        {
        //            if (this.MaPhuKien == "S13")
        //            {
        //                if (this.ID > 0)
        //                    this.UpdateTransaction(transaction);
        //                else
        //                    this.InsertTransaction(transaction);
        //                int stt = 1;
        //                foreach (HangPhuKien hang in this.HPKCollection)
        //                {
        //                    //hang.STTHang = stt++;
        //                    //hang.Master_ID = this.ID;
        //                    //if (hang.ID == 0)
        //                    //    hang.InsertTransaction(transaction);
        //                    //else
        //                    //    hang.UpdateTransaction(transaction);

        //                    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //                    sp.Ma = hang.MaHang;
        //                    sp.HopDong_ID = HopDong_ID;
        //                    sp.DVT_ID = hang.DVT_ID;
        //                    sp.MaHS = hang.MaHS;
        //                    sp.NhomSanPham_ID = hang.NhomSP;
        //                    sp.SoLuongDangKy = hang.SoLuong;
        //                    sp.TrangThai = 2;
        //                    sp.Ten = hang.TenHang;
        //                    sp.InsertUpdateTransaction(transaction);
        //                }
        //            }
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            this.ID = id;
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        //public void InsertUpdateBoSungNPL(long HopDong_ID)
        //{
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        long id = this.ID;
        //        try
        //        {
        //            if (this.MaPhuKien == "N01")
        //            {
        //                if (this.ID > 0)
        //                    this.UpdateTransaction(transaction);
        //                else
        //                    this.InsertTransaction(transaction);
        //                int stt = 1;
        //                foreach (HangPhuKien hang in this.HPKCollection)
        //                {
        //                    //hang.STTHang = stt++;
        //                    //hang.Master_ID = this.ID;
        //                    //if (hang.ID == 0)
        //                    //    hang.InsertTransaction(transaction);
        //                    //else
        //                    //    hang.UpdateTransaction(transaction);

        //                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //                    npl.DVT_ID = hang.DVT_ID;
        //                    npl.HopDong_ID = HopDong_ID;
        //                    npl.Ten = hang.TenHang;
        //                    npl.SoLuongDangKy = hang.SoLuong;
        //                    npl.Ma = hang.MaHang;
        //                    npl.MaHS = hang.MaHS;
        //                    npl.TrangThai = 2;
        //                    npl.InsertUpdateTransaction(transaction);
        //                }
        //            }
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            this.ID = id;
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        //public void InsertUpdateBoSungTB(long HopDong_ID)
        //{
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        long id = this.ID;
        //        try
        //        {
        //            if (this.MaPhuKien == "T07")
        //            {
        //                if (this.ID > 0)
        //                    this.UpdateTransaction(transaction);
        //                else
        //                    this.InsertTransaction(transaction);
        //                int stt = 1;
        //                foreach (HangPhuKien hang in this.HPKCollection)
        //                {
        //                    //hang.STTHang = stt++;
        //                    //hang.Master_ID = this.ID;
        //                    //if (hang.ID == 0)
        //                    //    hang.InsertTransaction(transaction);
        //                    //else
        //                    //    hang.UpdateTransaction(transaction);

        //                    BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
        //                    entity.HopDong_ID = HopDong_ID;
        //                    entity.Ma = hang.MaHang;
        //                    entity.DVT_ID = hang.DVT_ID;
        //                    entity.MaHS = hang.MaHS;
        //                    entity.DonGia = hang.DonGia;
        //                    entity.SoLuongDangKy = hang.SoLuong;
        //                    entity.Ten = hang.TenHang;
        //                    entity.NguyenTe_ID = hang.NguyenTe_ID;
        //                    entity.NuocXX_ID = hang.NuocXX_ID;
        //                    entity.TinhTrang = hang.TinhTrang;
        //                    entity.TriGia = hang.TriGia;
        //                    entity.TrangThai = 2;
        //                    entity.InsertUpdateTransaction(transaction);
        //                }
        //            }
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            this.ID = id;
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        //kiem tra co phu kien cung loai nao duoc mo nhung chua duyet ko


        //public void InsertUpdateBoSungNhomSamPhamGiaCong(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {              
        //        Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
        //        LoaiSP.MaSanPham = hang.MaHang;
        //        LoaiSP.HopDong_ID = HopDong_ID;
        //        LoaiSP.TrangThai = 0;
        //        LoaiSP.TenSanPham = hang.TenHang;
        //        LoaiSP.SoLuong = hang.SoLuong;
        //        LoaiSP.GiaGiaCong = Convert.ToDecimal(hang.DonGia);
        //        LoaiSP.InsertUpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateBoSungSP(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {                
        //        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //        sp.Ma = hang.MaHang;
        //        sp.HopDong_ID = HopDong_ID;
        //        sp.DVT_ID = hang.DVT_ID;
        //        sp.MaHS = hang.MaHS;
        //        sp.NhomSanPham_ID = hang.NhomSP;
        //        sp.SoLuongDangKy = hang.SoLuong;
        //        sp.TrangThai = 0;
        //        sp.Ten = hang.TenHang;
        //        sp.InsertUpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateBoSungNPL(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {               
        //        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //        npl.DVT_ID = hang.DVT_ID;
        //        npl.HopDong_ID = HopDong_ID;
        //        npl.Ten = hang.TenHang;
        //        npl.SoLuongDangKy = hang.SoLuong;
        //        npl.Ma = hang.MaHang;
        //        npl.MaHS = hang.MaHS;
        //        npl.TrangThai = 0;
        //        npl.InsertUpdateTransaction(transaction);
        //    }
        //}

        //public void InsertUpdateBoSungTB(long HopDong_ID, SqlTransaction transaction)
        //{
        //    foreach (HangPhuKien hang in this.HPKCollection)
        //    {              
        //        BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
        //        entity.HopDong_ID = HopDong_ID;
        //        entity.Ma = hang.MaHang;
        //        entity.DVT_ID = hang.DVT_ID;
        //        entity.MaHS = hang.MaHS;
        //        entity.DonGia = hang.DonGia;
        //        entity.SoLuongDangKy = hang.SoLuong;
        //        entity.Ten = hang.TenHang;
        //        entity.NguyenTe_ID = hang.NguyenTe_ID;
        //        entity.NuocXX_ID = hang.NuocXX_ID;
        //        entity.TinhTrang = hang.TinhTrang;
        //        entity.TriGia = hang.TriGia;
        //        entity.TrangThai = 0;
        //        entity.InsertUpdateTransaction(transaction);
        //    }
        //}
        public void InsertUpdateBoSungHM(long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    //if (this.MaPhuKien.Trim() == "T07")
                    //{
                    if (this.ID > 0)
                        this.UpdateTransaction(transaction);
                    else
                        this.InsertTransaction(transaction);
                    int stt = 1;
                    foreach (HangPhuKien hang in this.HPKCollection)
                    {
                        hang.STTHang = stt++;
                        hang.Master_ID = this.ID;
                        if (hang.ID == 0)
                            hang.InsertTransaction(transaction);
                        else
                            hang.UpdateTransaction(transaction);
                        if (hang.ThuTucTruocDo != null)
                        {
                            hang.ThuTucTruocDo.Master_ID = hang.ID;
                            hang.ThuTucTruocDo.InsertUpdateFull(transaction);
                        }
                        //#region cap nhat Thiet Bị da dang ky
                        //BLL.GC.HangMau entity = new Company.GC.BLL.GC.HangMau();
                        //entity.HopDong_ID = HopDong_ID;
                        //entity.Ma = hang.MaHang;
                        //entity.Ten = hang.TenHang;
                        //entity.MaHS = hang.MaHS;
                        //entity.DVT_ID = hang.DVT_ID;
                        //entity.SoLuongDangKy = hang.SoLuong;
                        //entity.SoLuongDaNhap = hang.SoLuong;
                        //entity.SoLuongDaDung = hang.SoLuong;
                        //entity.SoLuongCungUng = hang.SoLuong;
                        //entity.TongNhuCau = hang.SoLuong;
                        //entity.STTHang = hang.STTHang;
                        //entity.TrangThai = 0;//Trạng thái đã đăng ký
                        //entity.DonGia = Convert.ToDecimal(hang.DonGia);
                        //entity.InsertUpdate();
                        //#endregion
                        //#region cap nhat Thiet Bị theo doi HD
                        //Company.GC.BLL.KDT.GC.HangMau hm = new Company.GC.BLL.KDT.GC.HangMau();
                        //hm.HopDong_ID = HopDong_ID;
                        //hm.Ma = hang.MaHang;
                        //hm.Ten = hang.TenHang;
                        //hm.MaHS = hang.MaHS;
                        //hm.DVT_ID = hang.DVT_ID;
                        //hm.SoLuongDangKy = hang.SoLuong;
                        //hm.STTHang = hang.STTHang;
                        //hm.GhiChu = "PK" + ID.ToString();
                        //hm.InsertUpdate(transaction);
                        //#endregion
                    }
                    //}
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public long InsertTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_LoaiPhuKien_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@MaPhuKien", SqlDbType.Char, this._MaPhuKien);
            this.db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, this._NoiDung);
            this.db.AddInParameter(dbCommand, "@ThongTinMoi", SqlDbType.VarChar, this._ThongTinMoi);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.VarChar, this._ThongTinCu);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_LoaiPhuKien_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaPhuKien", SqlDbType.Char, this._MaPhuKien);
            this.db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, this._NoiDung);
            this.db.AddInParameter(dbCommand, "@ThongTinMoi", SqlDbType.VarChar, this._ThongTinMoi);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.VarChar, this._ThongTinCu);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public bool Load(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_LoaiPhuKien_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaPhuKien", SqlDbType.Char, this._MaPhuKien);
            this.db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, this._NoiDung);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            IDataReader reader = null;

            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhuKien"))) this._MaPhuKien = reader.GetString(reader.GetOrdinal("MaPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) this._NoiDung = reader.GetString(reader.GetOrdinal("NoiDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoi"))) this._ThongTinMoi = reader.GetString(reader.GetOrdinal("ThongTinMoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) this._Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) this._ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public LoaiPhuKienCollection SelectCollectionBy_Master_ID(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_LoaiPhuKien_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            LoaiPhuKienCollection collection = new LoaiPhuKienCollection();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                LoaiPhuKien entity = new LoaiPhuKien();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhuKien"))) entity.MaPhuKien = reader.GetString(reader.GetOrdinal("MaPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = reader.GetString(reader.GetOrdinal("NoiDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoi"))) entity.ThongTinMoi = reader.GetString(reader.GetOrdinal("ThongTinMoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) entity.ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

    }
}
