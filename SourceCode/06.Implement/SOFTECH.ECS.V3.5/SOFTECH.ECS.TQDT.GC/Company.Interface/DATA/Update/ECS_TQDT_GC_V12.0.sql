/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_Delete]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_DeleteDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_Insert]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_InsertUpdate]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_Load]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectAll]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_Update]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ApplicationProcedureType_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_Delete]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_Insert]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_InsertUpdate]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_Load]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_SelectAll]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_SelectDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_Update]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_Delete]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_DeleteDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_Insert]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_InsertUpdate]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_Load]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_SelectAll]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_SelectDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_Update]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_Delete]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_Insert]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_Load]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_Update]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_Delete]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Common_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_DeleteDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Common_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_Insert]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Common_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_InsertUpdate]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Common_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_Load]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Common_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_SelectAll]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Common_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_SelectDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Common_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_Update]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Common_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_Delete]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_DeleteDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_Insert]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_InsertUpdate]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_Load]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_SelectAll]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_SelectDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_Update]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_Delete]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_Insert]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_Load]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_Update]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_Delete]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_Insert]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_Load]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_SelectAll]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_Update]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_Delete]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_Insert]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_Load]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_Update]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_Delete]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_DeleteDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_Insert]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_InsertUpdate]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_Load]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_SelectAll]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_SelectDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_Update]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_Delete]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_DeleteDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_Insert]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_InsertUpdate]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_Load]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_SelectAll]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_SelectDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_Update]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_Delete]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_Insert]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_InsertUpdate]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_Load]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_SelectAll]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_SelectDynamic]    Script Date: 11/11/2013 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_Update]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_Delete]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_Insert]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_Load]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_SelectAll]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_Update]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_Delete]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_Insert]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_Load]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_SelectAll]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_Update]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_Delete]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Station_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_DeleteDynamic]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Station_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_Insert]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Station_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_InsertUpdate]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Station_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_Load]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Station_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_SelectAll]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Station_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_SelectDynamic]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Station_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_Update]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Station_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_Delete]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_Insert]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_Load]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_Update]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_Delete]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_DeleteDynamic]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_Insert]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_InsertUpdate]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_Load]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_SelectAll]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_SelectDynamic]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_Update]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_Delete]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Type_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_DeleteDynamic]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Type_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_Insert]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Type_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_InsertUpdate]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Type_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_Load]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Type_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_SelectAll]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Type_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_SelectDynamic]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Type_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_Update]    Script Date: 11/11/2013 16:24:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACC_Category_Type_Update]
GO


/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_Delete]    Script Date: 11/11/2013 16:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Delete]
	@ApplicationProcedureType varchar(5)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_ApplicationProcedureType]
WHERE
	[ApplicationProcedureType] = @ApplicationProcedureType


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_DeleteDynamic]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_ApplicationProcedureType] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_Insert]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@ApplicationProcedureType varchar(5),
	@ApplicationProcedureName nvarchar(250),
	@Section int,
	@IndicationOfGeneralDepartmentOfCustoms int,
	@IndicationOfDisapproval int,
	@IndicationOfCancellation int,
	@IndicationOfCompletionOfExamination int,
	@NumberOfYearsForSavingTheOriginalDocumentData int,
	@ExpiryDate datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_ApplicationProcedureType]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ApplicationProcedureType],
	[ApplicationProcedureName],
	[Section],
	[IndicationOfGeneralDepartmentOfCustoms],
	[IndicationOfDisapproval],
	[IndicationOfCancellation],
	[IndicationOfCompletionOfExamination],
	[NumberOfYearsForSavingTheOriginalDocumentData],
	[ExpiryDate],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@ApplicationProcedureType,
	@ApplicationProcedureName,
	@Section,
	@IndicationOfGeneralDepartmentOfCustoms,
	@IndicationOfDisapproval,
	@IndicationOfCancellation,
	@IndicationOfCompletionOfExamination,
	@NumberOfYearsForSavingTheOriginalDocumentData,
	@ExpiryDate,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_InsertUpdate]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@ApplicationProcedureType varchar(5),
	@ApplicationProcedureName nvarchar(250),
	@Section int,
	@IndicationOfGeneralDepartmentOfCustoms int,
	@IndicationOfDisapproval int,
	@IndicationOfCancellation int,
	@IndicationOfCompletionOfExamination int,
	@NumberOfYearsForSavingTheOriginalDocumentData int,
	@ExpiryDate datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ApplicationProcedureType] FROM [dbo].[t_VNACC_Category_ApplicationProcedureType] WHERE [ApplicationProcedureType] = @ApplicationProcedureType)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_ApplicationProcedureType] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[ApplicationProcedureName] = @ApplicationProcedureName,
			[Section] = @Section,
			[IndicationOfGeneralDepartmentOfCustoms] = @IndicationOfGeneralDepartmentOfCustoms,
			[IndicationOfDisapproval] = @IndicationOfDisapproval,
			[IndicationOfCancellation] = @IndicationOfCancellation,
			[IndicationOfCompletionOfExamination] = @IndicationOfCompletionOfExamination,
			[NumberOfYearsForSavingTheOriginalDocumentData] = @NumberOfYearsForSavingTheOriginalDocumentData,
			[ExpiryDate] = @ExpiryDate,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ApplicationProcedureType] = @ApplicationProcedureType
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_ApplicationProcedureType]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[ApplicationProcedureType],
			[ApplicationProcedureName],
			[Section],
			[IndicationOfGeneralDepartmentOfCustoms],
			[IndicationOfDisapproval],
			[IndicationOfCancellation],
			[IndicationOfCompletionOfExamination],
			[NumberOfYearsForSavingTheOriginalDocumentData],
			[ExpiryDate],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@ApplicationProcedureType,
			@ApplicationProcedureName,
			@Section,
			@IndicationOfGeneralDepartmentOfCustoms,
			@IndicationOfDisapproval,
			@IndicationOfCancellation,
			@IndicationOfCompletionOfExamination,
			@NumberOfYearsForSavingTheOriginalDocumentData,
			@ExpiryDate,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_Load]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Load]
	@ApplicationProcedureType varchar(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ApplicationProcedureType],
	[ApplicationProcedureName],
	[Section],
	[IndicationOfGeneralDepartmentOfCustoms],
	[IndicationOfDisapproval],
	[IndicationOfCancellation],
	[IndicationOfCompletionOfExamination],
	[NumberOfYearsForSavingTheOriginalDocumentData],
	[ExpiryDate],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_ApplicationProcedureType]
WHERE
	[ApplicationProcedureType] = @ApplicationProcedureType

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectAll]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ApplicationProcedureType],
	[ApplicationProcedureName],
	[Section],
	[IndicationOfGeneralDepartmentOfCustoms],
	[IndicationOfDisapproval],
	[IndicationOfCancellation],
	[IndicationOfCompletionOfExamination],
	[NumberOfYearsForSavingTheOriginalDocumentData],
	[ExpiryDate],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_ApplicationProcedureType]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectDynamic]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ApplicationProcedureType],
	[ApplicationProcedureName],
	[Section],
	[IndicationOfGeneralDepartmentOfCustoms],
	[IndicationOfDisapproval],
	[IndicationOfCancellation],
	[IndicationOfCompletionOfExamination],
	[NumberOfYearsForSavingTheOriginalDocumentData],
	[ExpiryDate],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_ApplicationProcedureType] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ApplicationProcedureType_Update]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ApplicationProcedureType_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ApplicationProcedureType_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@ApplicationProcedureType varchar(5),
	@ApplicationProcedureName nvarchar(250),
	@Section int,
	@IndicationOfGeneralDepartmentOfCustoms int,
	@IndicationOfDisapproval int,
	@IndicationOfCancellation int,
	@IndicationOfCompletionOfExamination int,
	@NumberOfYearsForSavingTheOriginalDocumentData int,
	@ExpiryDate datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_ApplicationProcedureType]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[ApplicationProcedureName] = @ApplicationProcedureName,
	[Section] = @Section,
	[IndicationOfGeneralDepartmentOfCustoms] = @IndicationOfGeneralDepartmentOfCustoms,
	[IndicationOfDisapproval] = @IndicationOfDisapproval,
	[IndicationOfCancellation] = @IndicationOfCancellation,
	[IndicationOfCompletionOfExamination] = @IndicationOfCompletionOfExamination,
	[NumberOfYearsForSavingTheOriginalDocumentData] = @NumberOfYearsForSavingTheOriginalDocumentData,
	[ExpiryDate] = @ExpiryDate,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ApplicationProcedureType] = @ApplicationProcedureType


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_Delete]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Delete]
	@BorderGateCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_BorderGate]
WHERE
	[BorderGateCode] = @BorderGateCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_BorderGate] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_Insert]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BorderGateCode varchar(10),
	@BorderGateName nvarchar(250),
	@CustomsOfficeCode varchar(5),
	@ImmigrationBureauUserCode varchar(5),
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@QuarantineOfficeUserCode varchar(5),
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_BorderGate]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BorderGateCode],
	[BorderGateName],
	[CustomsOfficeCode],
	[ImmigrationBureauUserCode],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[QuarantineOfficeUserCode],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@BorderGateCode,
	@BorderGateName,
	@CustomsOfficeCode,
	@ImmigrationBureauUserCode,
	@ArrivalDepartureForImmigration,
	@PassengerForImmigration,
	@QuarantineOfficeUserCode,
	@ArrivalDepartureForQuarantine,
	@PassengerForQuarantine,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_InsertUpdate]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BorderGateCode varchar(10),
	@BorderGateName nvarchar(250),
	@CustomsOfficeCode varchar(5),
	@ImmigrationBureauUserCode varchar(5),
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@QuarantineOfficeUserCode varchar(5),
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [BorderGateCode] FROM [dbo].[t_VNACC_Category_BorderGate] WHERE [BorderGateCode] = @BorderGateCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_BorderGate] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[BorderGateName] = @BorderGateName,
			[CustomsOfficeCode] = @CustomsOfficeCode,
			[ImmigrationBureauUserCode] = @ImmigrationBureauUserCode,
			[ArrivalDepartureForImmigration] = @ArrivalDepartureForImmigration,
			[PassengerForImmigration] = @PassengerForImmigration,
			[QuarantineOfficeUserCode] = @QuarantineOfficeUserCode,
			[ArrivalDepartureForQuarantine] = @ArrivalDepartureForQuarantine,
			[PassengerForQuarantine] = @PassengerForQuarantine,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[BorderGateCode] = @BorderGateCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_BorderGate]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[BorderGateCode],
			[BorderGateName],
			[CustomsOfficeCode],
			[ImmigrationBureauUserCode],
			[ArrivalDepartureForImmigration],
			[PassengerForImmigration],
			[QuarantineOfficeUserCode],
			[ArrivalDepartureForQuarantine],
			[PassengerForQuarantine],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@BorderGateCode,
			@BorderGateName,
			@CustomsOfficeCode,
			@ImmigrationBureauUserCode,
			@ArrivalDepartureForImmigration,
			@PassengerForImmigration,
			@QuarantineOfficeUserCode,
			@ArrivalDepartureForQuarantine,
			@PassengerForQuarantine,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_Load]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Load]
	@BorderGateCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BorderGateCode],
	[BorderGateName],
	[CustomsOfficeCode],
	[ImmigrationBureauUserCode],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[QuarantineOfficeUserCode],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_BorderGate]
WHERE
	[BorderGateCode] = @BorderGateCode

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_SelectAll]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BorderGateCode],
	[BorderGateName],
	[CustomsOfficeCode],
	[ImmigrationBureauUserCode],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[QuarantineOfficeUserCode],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_BorderGate]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_SelectDynamic]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BorderGateCode],
	[BorderGateName],
	[CustomsOfficeCode],
	[ImmigrationBureauUserCode],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[QuarantineOfficeUserCode],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_BorderGate] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_BorderGate_Update]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BorderGateCode varchar(10),
	@BorderGateName nvarchar(250),
	@CustomsOfficeCode varchar(5),
	@ImmigrationBureauUserCode varchar(5),
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@QuarantineOfficeUserCode varchar(5),
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_BorderGate]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[BorderGateName] = @BorderGateName,
	[CustomsOfficeCode] = @CustomsOfficeCode,
	[ImmigrationBureauUserCode] = @ImmigrationBureauUserCode,
	[ArrivalDepartureForImmigration] = @ArrivalDepartureForImmigration,
	[PassengerForImmigration] = @PassengerForImmigration,
	[QuarantineOfficeUserCode] = @QuarantineOfficeUserCode,
	[ArrivalDepartureForQuarantine] = @ArrivalDepartureForQuarantine,
	[PassengerForQuarantine] = @PassengerForQuarantine,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[BorderGateCode] = @BorderGateCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_Delete]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_Delete]
	@BondedAreaCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Cargo]
WHERE
	[BondedAreaCode] = @BondedAreaCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_DeleteDynamic]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Cargo] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_Insert]    Script Date: 11/11/2013 16:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BondedAreaCode varchar(10),
	@UserCode varchar(10),
	@BondedAreaName nvarchar(250),
	@BondedAreaDemarcation char(1),
	@NecessityIndicationOfBondedAreaName int,
	@CustomsOfficeCodeForImportDeclaration varchar(5),
	@CustomsOfficeCodeForExportDeclaration varchar(5),
	@CustomsOfficeCodeForDeclarationOnTransportation varchar(5),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_Cargo]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BondedAreaCode],
	[UserCode],
	[BondedAreaName],
	[BondedAreaDemarcation],
	[NecessityIndicationOfBondedAreaName],
	[CustomsOfficeCodeForImportDeclaration],
	[CustomsOfficeCodeForExportDeclaration],
	[CustomsOfficeCodeForDeclarationOnTransportation],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@BondedAreaCode,
	@UserCode,
	@BondedAreaName,
	@BondedAreaDemarcation,
	@NecessityIndicationOfBondedAreaName,
	@CustomsOfficeCodeForImportDeclaration,
	@CustomsOfficeCodeForExportDeclaration,
	@CustomsOfficeCodeForDeclarationOnTransportation,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_InsertUpdate]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BondedAreaCode varchar(10),
	@UserCode varchar(10),
	@BondedAreaName nvarchar(250),
	@BondedAreaDemarcation char(1),
	@NecessityIndicationOfBondedAreaName int,
	@CustomsOfficeCodeForImportDeclaration varchar(5),
	@CustomsOfficeCodeForExportDeclaration varchar(5),
	@CustomsOfficeCodeForDeclarationOnTransportation varchar(5),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [BondedAreaCode] FROM [dbo].[t_VNACC_Category_Cargo] WHERE [BondedAreaCode] = @BondedAreaCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Cargo] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[UserCode] = @UserCode,
			[BondedAreaName] = @BondedAreaName,
			[BondedAreaDemarcation] = @BondedAreaDemarcation,
			[NecessityIndicationOfBondedAreaName] = @NecessityIndicationOfBondedAreaName,
			[CustomsOfficeCodeForImportDeclaration] = @CustomsOfficeCodeForImportDeclaration,
			[CustomsOfficeCodeForExportDeclaration] = @CustomsOfficeCodeForExportDeclaration,
			[CustomsOfficeCodeForDeclarationOnTransportation] = @CustomsOfficeCodeForDeclarationOnTransportation,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[BondedAreaCode] = @BondedAreaCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_Cargo]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[BondedAreaCode],
			[UserCode],
			[BondedAreaName],
			[BondedAreaDemarcation],
			[NecessityIndicationOfBondedAreaName],
			[CustomsOfficeCodeForImportDeclaration],
			[CustomsOfficeCodeForExportDeclaration],
			[CustomsOfficeCodeForDeclarationOnTransportation],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@BondedAreaCode,
			@UserCode,
			@BondedAreaName,
			@BondedAreaDemarcation,
			@NecessityIndicationOfBondedAreaName,
			@CustomsOfficeCodeForImportDeclaration,
			@CustomsOfficeCodeForExportDeclaration,
			@CustomsOfficeCodeForDeclarationOnTransportation,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_Load]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_Load]
	@BondedAreaCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BondedAreaCode],
	[UserCode],
	[BondedAreaName],
	[BondedAreaDemarcation],
	[NecessityIndicationOfBondedAreaName],
	[CustomsOfficeCodeForImportDeclaration],
	[CustomsOfficeCodeForExportDeclaration],
	[CustomsOfficeCodeForDeclarationOnTransportation],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Cargo]
WHERE
	[BondedAreaCode] = @BondedAreaCode

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_SelectAll]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BondedAreaCode],
	[UserCode],
	[BondedAreaName],
	[BondedAreaDemarcation],
	[NecessityIndicationOfBondedAreaName],
	[CustomsOfficeCodeForImportDeclaration],
	[CustomsOfficeCodeForExportDeclaration],
	[CustomsOfficeCodeForDeclarationOnTransportation],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Cargo]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_SelectDynamic]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BondedAreaCode],
	[UserCode],
	[BondedAreaName],
	[BondedAreaDemarcation],
	[NecessityIndicationOfBondedAreaName],
	[CustomsOfficeCodeForImportDeclaration],
	[CustomsOfficeCodeForExportDeclaration],
	[CustomsOfficeCodeForDeclarationOnTransportation],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_Cargo] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Cargo_Update]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BondedAreaCode varchar(10),
	@UserCode varchar(10),
	@BondedAreaName nvarchar(250),
	@BondedAreaDemarcation char(1),
	@NecessityIndicationOfBondedAreaName int,
	@CustomsOfficeCodeForImportDeclaration varchar(5),
	@CustomsOfficeCodeForExportDeclaration varchar(5),
	@CustomsOfficeCodeForDeclarationOnTransportation varchar(5),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_Cargo]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[UserCode] = @UserCode,
	[BondedAreaName] = @BondedAreaName,
	[BondedAreaDemarcation] = @BondedAreaDemarcation,
	[NecessityIndicationOfBondedAreaName] = @NecessityIndicationOfBondedAreaName,
	[CustomsOfficeCodeForImportDeclaration] = @CustomsOfficeCodeForImportDeclaration,
	[CustomsOfficeCodeForExportDeclaration] = @CustomsOfficeCodeForExportDeclaration,
	[CustomsOfficeCodeForDeclarationOnTransportation] = @CustomsOfficeCodeForDeclarationOnTransportation,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[BondedAreaCode] = @BondedAreaCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_Delete]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_CityUNLOCODE]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_CityUNLOCODE] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_Insert]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@LOCODE varchar(10),
	@CityCode varchar(5),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@AirportPortClassification int,
	@CityNameOrStateName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_CityUNLOCODE]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[LOCODE],
	[CityCode],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[AirportPortClassification],
	[CityNameOrStateName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@LOCODE,
	@CityCode,
	@CountryCode,
	@NecessityIndicationOfInputName,
	@AirportPortClassification,
	@CityNameOrStateName,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@LOCODE varchar(10),
	@CityCode varchar(5),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@AirportPortClassification int,
	@CityNameOrStateName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_CityUNLOCODE] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_CityUNLOCODE] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[LOCODE] = @LOCODE,
			[CityCode] = @CityCode,
			[CountryCode] = @CountryCode,
			[NecessityIndicationOfInputName] = @NecessityIndicationOfInputName,
			[AirportPortClassification] = @AirportPortClassification,
			[CityNameOrStateName] = @CityNameOrStateName,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_CityUNLOCODE]
		(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[LOCODE],
			[CityCode],
			[CountryCode],
			[NecessityIndicationOfInputName],
			[AirportPortClassification],
			[CityNameOrStateName],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@LOCODE,
			@CityCode,
			@CountryCode,
			@NecessityIndicationOfInputName,
			@AirportPortClassification,
			@CityNameOrStateName,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_Load]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[LOCODE],
	[CityCode],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[AirportPortClassification],
	[CityNameOrStateName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CityUNLOCODE]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[LOCODE],
	[CityCode],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[AirportPortClassification],
	[CityNameOrStateName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CityUNLOCODE]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[LOCODE],
	[CityCode],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[AirportPortClassification],
	[CityNameOrStateName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_CityUNLOCODE] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CityUNLOCODE_Update]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Update]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@LOCODE varchar(10),
	@CityCode varchar(5),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@AirportPortClassification int,
	@CityNameOrStateName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_CityUNLOCODE]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[LOCODE] = @LOCODE,
	[CityCode] = @CityCode,
	[CountryCode] = @CountryCode,
	[NecessityIndicationOfInputName] = @NecessityIndicationOfInputName,
	[AirportPortClassification] = @AirportPortClassification,
	[CityNameOrStateName] = @CityNameOrStateName,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_Delete]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Common]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_DeleteDynamic]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Common] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_Insert]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_Insert]
	@ReferenceDB varchar(5),
	@Code varchar(5),
	@Name_VN nvarchar(max),
	@Name_EN varchar(max),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_Common]
(
	[ReferenceDB],
	[Code],
	[Name_VN],
	[Name_EN],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@ReferenceDB,
	@Code,
	@Name_VN,
	@Name_EN,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_InsertUpdate]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_InsertUpdate]
	@ID int,
	@ReferenceDB varchar(5),
	@Code varchar(5),
	@Name_VN nvarchar(max),
	@Name_EN varchar(max),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_Common] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Common] 
		SET
			[ReferenceDB] = @ReferenceDB,
			[Code] = @Code,
			[Name_VN] = @Name_VN,
			[Name_EN] = @Name_EN,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_Common]
		(
			[ReferenceDB],
			[Code],
			[Name_VN],
			[Name_EN],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@ReferenceDB,
			@Code,
			@Name_VN,
			@Name_EN,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_Load]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceDB],
	[Code],
	[Name_VN],
	[Name_EN],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Common]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_SelectAll]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceDB],
	[Code],
	[Name_VN],
	[Name_EN],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Common]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_SelectDynamic]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ReferenceDB],
	[Code],
	[Name_VN],
	[Name_EN],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_Common] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Common_Update]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_Update]
	@ID int,
	@ReferenceDB varchar(5),
	@Code varchar(5),
	@Name_VN nvarchar(max),
	@Name_EN varchar(max),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_Common]
SET
	[ReferenceDB] = @ReferenceDB,
	[Code] = @Code,
	[Name_VN] = @Name_VN,
	[Name_EN] = @Name_EN,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_Delete]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_ContainerSize]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_DeleteDynamic]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_ContainerSize] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_Insert]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@ContainerSizeCode varchar(5),
	@ContainerSizeName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_ContainerSize]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ContainerSizeCode],
	[ContainerSizeName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@ContainerSizeCode,
	@ContainerSizeName,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_InsertUpdate]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_InsertUpdate]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@ContainerSizeCode varchar(5),
	@ContainerSizeName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_ContainerSize] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_ContainerSize] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[ContainerSizeCode] = @ContainerSizeCode,
			[ContainerSizeName] = @ContainerSizeName,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_ContainerSize]
		(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[ContainerSizeCode],
			[ContainerSizeName],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@ContainerSizeCode,
			@ContainerSizeName,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_Load]    Script Date: 11/11/2013 16:24:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ContainerSizeCode],
	[ContainerSizeName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_ContainerSize]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_SelectAll]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ContainerSizeCode],
	[ContainerSizeName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_ContainerSize]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_SelectDynamic]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ContainerSizeCode],
	[ContainerSizeName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_ContainerSize] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_ContainerSize_Update]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Update]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@ContainerSizeCode varchar(5),
	@ContainerSizeName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_ContainerSize]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[ContainerSizeCode] = @ContainerSizeCode,
	[ContainerSizeName] = @ContainerSizeName,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_Delete]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Delete]
	@CurrencyCode char(3)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_CurrencyExchange]
WHERE
	[CurrencyCode] = @CurrencyCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_CurrencyExchange] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_Insert]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Insert]
	@ErrorCode char(25),
	@PGNumber char(2),
	@TableID char(5),
	@ProcessClassification char(1),
	@MakerClassification char(1),
	@NumberOfKeyItems char(2),
	@CurrencyCode char(3),
	@CurrencyName nvarchar(250),
	@GenerationManagementIndication char(1),
	@DateOfMaintenanceUpdated datetime,
	@EndDate datetime,
	@StartDate_1 datetime,
	@ExchangeRate_1 decimal(9, 2),
	@StartDate_2 datetime,
	@ExchangeRate_2 decimal(9, 2),
	@StartDate_3 datetime,
	@ExchangeRate_3 decimal(9, 2),
	@StartDate_4 datetime,
	@ExchangeRate_4 decimal(9, 2),
	@StartDate_5 datetime,
	@ExchangeRate_5 decimal(9, 2),
	@StartDate_6 datetime,
	@ExchangeRate_6 decimal(9, 2),
	@StartDate_7 datetime,
	@ExchangeRate_7 decimal(9, 2),
	@StartDate_8 datetime,
	@ExchangeRate_8 decimal(9, 2),
	@StartDate_9 datetime,
	@ExchangeRate_9 decimal(9, 2),
	@StartDate_10 datetime,
	@ExchangeRate_10 decimal(9, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_CurrencyExchange]
(
	[ErrorCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[MakerClassification],
	[NumberOfKeyItems],
	[CurrencyCode],
	[CurrencyName],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[EndDate],
	[StartDate_1],
	[ExchangeRate_1],
	[StartDate_2],
	[ExchangeRate_2],
	[StartDate_3],
	[ExchangeRate_3],
	[StartDate_4],
	[ExchangeRate_4],
	[StartDate_5],
	[ExchangeRate_5],
	[StartDate_6],
	[ExchangeRate_6],
	[StartDate_7],
	[ExchangeRate_7],
	[StartDate_8],
	[ExchangeRate_8],
	[StartDate_9],
	[ExchangeRate_9],
	[StartDate_10],
	[ExchangeRate_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ErrorCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@MakerClassification,
	@NumberOfKeyItems,
	@CurrencyCode,
	@CurrencyName,
	@GenerationManagementIndication,
	@DateOfMaintenanceUpdated,
	@EndDate,
	@StartDate_1,
	@ExchangeRate_1,
	@StartDate_2,
	@ExchangeRate_2,
	@StartDate_3,
	@ExchangeRate_3,
	@StartDate_4,
	@ExchangeRate_4,
	@StartDate_5,
	@ExchangeRate_5,
	@StartDate_6,
	@ExchangeRate_6,
	@StartDate_7,
	@ExchangeRate_7,
	@StartDate_8,
	@ExchangeRate_8,
	@StartDate_9,
	@ExchangeRate_9,
	@StartDate_10,
	@ExchangeRate_10,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]
	@ErrorCode char(25),
	@PGNumber char(2),
	@TableID char(5),
	@ProcessClassification char(1),
	@MakerClassification char(1),
	@NumberOfKeyItems char(2),
	@CurrencyCode char(3),
	@CurrencyName nvarchar(250),
	@GenerationManagementIndication char(1),
	@DateOfMaintenanceUpdated datetime,
	@EndDate datetime,
	@StartDate_1 datetime,
	@ExchangeRate_1 decimal(9, 2),
	@StartDate_2 datetime,
	@ExchangeRate_2 decimal(9, 2),
	@StartDate_3 datetime,
	@ExchangeRate_3 decimal(9, 2),
	@StartDate_4 datetime,
	@ExchangeRate_4 decimal(9, 2),
	@StartDate_5 datetime,
	@ExchangeRate_5 decimal(9, 2),
	@StartDate_6 datetime,
	@ExchangeRate_6 decimal(9, 2),
	@StartDate_7 datetime,
	@ExchangeRate_7 decimal(9, 2),
	@StartDate_8 datetime,
	@ExchangeRate_8 decimal(9, 2),
	@StartDate_9 datetime,
	@ExchangeRate_9 decimal(9, 2),
	@StartDate_10 datetime,
	@ExchangeRate_10 decimal(9, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [CurrencyCode] FROM [dbo].[t_VNACC_Category_CurrencyExchange] WHERE [CurrencyCode] = @CurrencyCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_CurrencyExchange] 
		SET
			[ErrorCode] = @ErrorCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[MakerClassification] = @MakerClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[CurrencyName] = @CurrencyName,
			[GenerationManagementIndication] = @GenerationManagementIndication,
			[DateOfMaintenanceUpdated] = @DateOfMaintenanceUpdated,
			[EndDate] = @EndDate,
			[StartDate_1] = @StartDate_1,
			[ExchangeRate_1] = @ExchangeRate_1,
			[StartDate_2] = @StartDate_2,
			[ExchangeRate_2] = @ExchangeRate_2,
			[StartDate_3] = @StartDate_3,
			[ExchangeRate_3] = @ExchangeRate_3,
			[StartDate_4] = @StartDate_4,
			[ExchangeRate_4] = @ExchangeRate_4,
			[StartDate_5] = @StartDate_5,
			[ExchangeRate_5] = @ExchangeRate_5,
			[StartDate_6] = @StartDate_6,
			[ExchangeRate_6] = @ExchangeRate_6,
			[StartDate_7] = @StartDate_7,
			[ExchangeRate_7] = @ExchangeRate_7,
			[StartDate_8] = @StartDate_8,
			[ExchangeRate_8] = @ExchangeRate_8,
			[StartDate_9] = @StartDate_9,
			[ExchangeRate_9] = @ExchangeRate_9,
			[StartDate_10] = @StartDate_10,
			[ExchangeRate_10] = @ExchangeRate_10,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[CurrencyCode] = @CurrencyCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_CurrencyExchange]
	(
			[ErrorCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[MakerClassification],
			[NumberOfKeyItems],
			[CurrencyCode],
			[CurrencyName],
			[GenerationManagementIndication],
			[DateOfMaintenanceUpdated],
			[EndDate],
			[StartDate_1],
			[ExchangeRate_1],
			[StartDate_2],
			[ExchangeRate_2],
			[StartDate_3],
			[ExchangeRate_3],
			[StartDate_4],
			[ExchangeRate_4],
			[StartDate_5],
			[ExchangeRate_5],
			[StartDate_6],
			[ExchangeRate_6],
			[StartDate_7],
			[ExchangeRate_7],
			[StartDate_8],
			[ExchangeRate_8],
			[StartDate_9],
			[ExchangeRate_9],
			[StartDate_10],
			[ExchangeRate_10],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ErrorCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@MakerClassification,
			@NumberOfKeyItems,
			@CurrencyCode,
			@CurrencyName,
			@GenerationManagementIndication,
			@DateOfMaintenanceUpdated,
			@EndDate,
			@StartDate_1,
			@ExchangeRate_1,
			@StartDate_2,
			@ExchangeRate_2,
			@StartDate_3,
			@ExchangeRate_3,
			@StartDate_4,
			@ExchangeRate_4,
			@StartDate_5,
			@ExchangeRate_5,
			@StartDate_6,
			@ExchangeRate_6,
			@StartDate_7,
			@ExchangeRate_7,
			@StartDate_8,
			@ExchangeRate_8,
			@StartDate_9,
			@ExchangeRate_9,
			@StartDate_10,
			@ExchangeRate_10,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_Load]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Load]
	@CurrencyCode char(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ErrorCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[MakerClassification],
	[NumberOfKeyItems],
	[CurrencyCode],
	[CurrencyName],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[EndDate],
	[StartDate_1],
	[ExchangeRate_1],
	[StartDate_2],
	[ExchangeRate_2],
	[StartDate_3],
	[ExchangeRate_3],
	[StartDate_4],
	[ExchangeRate_4],
	[StartDate_5],
	[ExchangeRate_5],
	[StartDate_6],
	[ExchangeRate_6],
	[StartDate_7],
	[ExchangeRate_7],
	[StartDate_8],
	[ExchangeRate_8],
	[StartDate_9],
	[ExchangeRate_9],
	[StartDate_10],
	[ExchangeRate_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CurrencyExchange]
WHERE
	[CurrencyCode] = @CurrencyCode

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ErrorCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[MakerClassification],
	[NumberOfKeyItems],
	[CurrencyCode],
	[CurrencyName],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[EndDate],
	[StartDate_1],
	[ExchangeRate_1],
	[StartDate_2],
	[ExchangeRate_2],
	[StartDate_3],
	[ExchangeRate_3],
	[StartDate_4],
	[ExchangeRate_4],
	[StartDate_5],
	[ExchangeRate_5],
	[StartDate_6],
	[ExchangeRate_6],
	[StartDate_7],
	[ExchangeRate_7],
	[StartDate_8],
	[ExchangeRate_8],
	[StartDate_9],
	[ExchangeRate_9],
	[StartDate_10],
	[ExchangeRate_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CurrencyExchange]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ErrorCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[MakerClassification],
	[NumberOfKeyItems],
	[CurrencyCode],
	[CurrencyName],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[EndDate],
	[StartDate_1],
	[ExchangeRate_1],
	[StartDate_2],
	[ExchangeRate_2],
	[StartDate_3],
	[ExchangeRate_3],
	[StartDate_4],
	[ExchangeRate_4],
	[StartDate_5],
	[ExchangeRate_5],
	[StartDate_6],
	[ExchangeRate_6],
	[StartDate_7],
	[ExchangeRate_7],
	[StartDate_8],
	[ExchangeRate_8],
	[StartDate_9],
	[ExchangeRate_9],
	[StartDate_10],
	[ExchangeRate_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_CurrencyExchange] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CurrencyExchange_Update]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Update]
	@ErrorCode char(25),
	@PGNumber char(2),
	@TableID char(5),
	@ProcessClassification char(1),
	@MakerClassification char(1),
	@NumberOfKeyItems char(2),
	@CurrencyCode char(3),
	@CurrencyName nvarchar(250),
	@GenerationManagementIndication char(1),
	@DateOfMaintenanceUpdated datetime,
	@EndDate datetime,
	@StartDate_1 datetime,
	@ExchangeRate_1 decimal(9, 2),
	@StartDate_2 datetime,
	@ExchangeRate_2 decimal(9, 2),
	@StartDate_3 datetime,
	@ExchangeRate_3 decimal(9, 2),
	@StartDate_4 datetime,
	@ExchangeRate_4 decimal(9, 2),
	@StartDate_5 datetime,
	@ExchangeRate_5 decimal(9, 2),
	@StartDate_6 datetime,
	@ExchangeRate_6 decimal(9, 2),
	@StartDate_7 datetime,
	@ExchangeRate_7 decimal(9, 2),
	@StartDate_8 datetime,
	@ExchangeRate_8 decimal(9, 2),
	@StartDate_9 datetime,
	@ExchangeRate_9 decimal(9, 2),
	@StartDate_10 datetime,
	@ExchangeRate_10 decimal(9, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_CurrencyExchange]
SET
	[ErrorCode] = @ErrorCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[MakerClassification] = @MakerClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[CurrencyName] = @CurrencyName,
	[GenerationManagementIndication] = @GenerationManagementIndication,
	[DateOfMaintenanceUpdated] = @DateOfMaintenanceUpdated,
	[EndDate] = @EndDate,
	[StartDate_1] = @StartDate_1,
	[ExchangeRate_1] = @ExchangeRate_1,
	[StartDate_2] = @StartDate_2,
	[ExchangeRate_2] = @ExchangeRate_2,
	[StartDate_3] = @StartDate_3,
	[ExchangeRate_3] = @ExchangeRate_3,
	[StartDate_4] = @StartDate_4,
	[ExchangeRate_4] = @ExchangeRate_4,
	[StartDate_5] = @StartDate_5,
	[ExchangeRate_5] = @ExchangeRate_5,
	[StartDate_6] = @StartDate_6,
	[ExchangeRate_6] = @ExchangeRate_6,
	[StartDate_7] = @StartDate_7,
	[ExchangeRate_7] = @ExchangeRate_7,
	[StartDate_8] = @StartDate_8,
	[ExchangeRate_8] = @ExchangeRate_8,
	[StartDate_9] = @StartDate_9,
	[ExchangeRate_9] = @ExchangeRate_9,
	[StartDate_10] = @StartDate_10,
	[ExchangeRate_10] = @ExchangeRate_10,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[CurrencyCode] = @CurrencyCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_Delete]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Delete]
	@CustomsCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_CustomsOffice]
WHERE
	[CustomsCode] = @CustomsCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_CustomsOffice] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_Insert]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsProvinceCode varchar(5),
	@OfficeIndicationOfExport int,
	@OfficeIndicationOfImport int,
	@OfficeIndicationOfBondedRelated int,
	@OfficeIndicationOfSupervision int,
	@CustomsOfficeName varchar(100),
	@CustomsOfficeNameInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInVietnamese nvarchar(250),
	@Postcode varchar(10),
	@AddressInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInRomanAlphabet varchar(100),
	@CustomsProvinceName nvarchar(250),
	@CustomsBranchName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@InspectAtInspectionSite nvarchar(250),
	@InspectUsingLargeXrayScanner nvarchar(250),
	@TransportationKindCode int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_CustomsOffice]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsProvinceCode],
	[OfficeIndicationOfExport],
	[OfficeIndicationOfImport],
	[OfficeIndicationOfBondedRelated],
	[OfficeIndicationOfSupervision],
	[CustomsOfficeName],
	[CustomsOfficeNameInVietnamese],
	[NameOfHeadOfCustomsOfficeInVietnamese],
	[Postcode],
	[AddressInVietnamese],
	[NameOfHeadOfCustomsOfficeInRomanAlphabet],
	[CustomsProvinceName],
	[CustomsBranchName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[InspectAtInspectionSite],
	[InspectUsingLargeXrayScanner],
	[TransportationKindCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@CustomsCode,
	@CustomsProvinceCode,
	@OfficeIndicationOfExport,
	@OfficeIndicationOfImport,
	@OfficeIndicationOfBondedRelated,
	@OfficeIndicationOfSupervision,
	@CustomsOfficeName,
	@CustomsOfficeNameInVietnamese,
	@NameOfHeadOfCustomsOfficeInVietnamese,
	@Postcode,
	@AddressInVietnamese,
	@NameOfHeadOfCustomsOfficeInRomanAlphabet,
	@CustomsProvinceName,
	@CustomsBranchName,
	@DestinationCode_1,
	@DestinationCode_2,
	@DestinationCode_3,
	@DestinationCode_4,
	@DestinationCode_5,
	@DestinationCode_6,
	@DestinationCode_7,
	@DestinationCode_8,
	@DestinationCode_9,
	@DestinationCode_10,
	@InspectAtInspectionSite,
	@InspectUsingLargeXrayScanner,
	@TransportationKindCode,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsProvinceCode varchar(5),
	@OfficeIndicationOfExport int,
	@OfficeIndicationOfImport int,
	@OfficeIndicationOfBondedRelated int,
	@OfficeIndicationOfSupervision int,
	@CustomsOfficeName varchar(100),
	@CustomsOfficeNameInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInVietnamese nvarchar(250),
	@Postcode varchar(10),
	@AddressInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInRomanAlphabet varchar(100),
	@CustomsProvinceName nvarchar(250),
	@CustomsBranchName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@InspectAtInspectionSite nvarchar(250),
	@InspectUsingLargeXrayScanner nvarchar(250),
	@TransportationKindCode int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [CustomsCode] FROM [dbo].[t_VNACC_Category_CustomsOffice] WHERE [CustomsCode] = @CustomsCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_CustomsOffice] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[CustomsProvinceCode] = @CustomsProvinceCode,
			[OfficeIndicationOfExport] = @OfficeIndicationOfExport,
			[OfficeIndicationOfImport] = @OfficeIndicationOfImport,
			[OfficeIndicationOfBondedRelated] = @OfficeIndicationOfBondedRelated,
			[OfficeIndicationOfSupervision] = @OfficeIndicationOfSupervision,
			[CustomsOfficeName] = @CustomsOfficeName,
			[CustomsOfficeNameInVietnamese] = @CustomsOfficeNameInVietnamese,
			[NameOfHeadOfCustomsOfficeInVietnamese] = @NameOfHeadOfCustomsOfficeInVietnamese,
			[Postcode] = @Postcode,
			[AddressInVietnamese] = @AddressInVietnamese,
			[NameOfHeadOfCustomsOfficeInRomanAlphabet] = @NameOfHeadOfCustomsOfficeInRomanAlphabet,
			[CustomsProvinceName] = @CustomsProvinceName,
			[CustomsBranchName] = @CustomsBranchName,
			[DestinationCode_1] = @DestinationCode_1,
			[DestinationCode_2] = @DestinationCode_2,
			[DestinationCode_3] = @DestinationCode_3,
			[DestinationCode_4] = @DestinationCode_4,
			[DestinationCode_5] = @DestinationCode_5,
			[DestinationCode_6] = @DestinationCode_6,
			[DestinationCode_7] = @DestinationCode_7,
			[DestinationCode_8] = @DestinationCode_8,
			[DestinationCode_9] = @DestinationCode_9,
			[DestinationCode_10] = @DestinationCode_10,
			[InspectAtInspectionSite] = @InspectAtInspectionSite,
			[InspectUsingLargeXrayScanner] = @InspectUsingLargeXrayScanner,
			[TransportationKindCode] = @TransportationKindCode,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[CustomsCode] = @CustomsCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_CustomsOffice]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[CustomsCode],
			[CustomsProvinceCode],
			[OfficeIndicationOfExport],
			[OfficeIndicationOfImport],
			[OfficeIndicationOfBondedRelated],
			[OfficeIndicationOfSupervision],
			[CustomsOfficeName],
			[CustomsOfficeNameInVietnamese],
			[NameOfHeadOfCustomsOfficeInVietnamese],
			[Postcode],
			[AddressInVietnamese],
			[NameOfHeadOfCustomsOfficeInRomanAlphabet],
			[CustomsProvinceName],
			[CustomsBranchName],
			[DestinationCode_1],
			[DestinationCode_2],
			[DestinationCode_3],
			[DestinationCode_4],
			[DestinationCode_5],
			[DestinationCode_6],
			[DestinationCode_7],
			[DestinationCode_8],
			[DestinationCode_9],
			[DestinationCode_10],
			[InspectAtInspectionSite],
			[InspectUsingLargeXrayScanner],
			[TransportationKindCode],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@CustomsCode,
			@CustomsProvinceCode,
			@OfficeIndicationOfExport,
			@OfficeIndicationOfImport,
			@OfficeIndicationOfBondedRelated,
			@OfficeIndicationOfSupervision,
			@CustomsOfficeName,
			@CustomsOfficeNameInVietnamese,
			@NameOfHeadOfCustomsOfficeInVietnamese,
			@Postcode,
			@AddressInVietnamese,
			@NameOfHeadOfCustomsOfficeInRomanAlphabet,
			@CustomsProvinceName,
			@CustomsBranchName,
			@DestinationCode_1,
			@DestinationCode_2,
			@DestinationCode_3,
			@DestinationCode_4,
			@DestinationCode_5,
			@DestinationCode_6,
			@DestinationCode_7,
			@DestinationCode_8,
			@DestinationCode_9,
			@DestinationCode_10,
			@InspectAtInspectionSite,
			@InspectUsingLargeXrayScanner,
			@TransportationKindCode,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_Load]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Load]
	@CustomsCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsProvinceCode],
	[OfficeIndicationOfExport],
	[OfficeIndicationOfImport],
	[OfficeIndicationOfBondedRelated],
	[OfficeIndicationOfSupervision],
	[CustomsOfficeName],
	[CustomsOfficeNameInVietnamese],
	[NameOfHeadOfCustomsOfficeInVietnamese],
	[Postcode],
	[AddressInVietnamese],
	[NameOfHeadOfCustomsOfficeInRomanAlphabet],
	[CustomsProvinceName],
	[CustomsBranchName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[InspectAtInspectionSite],
	[InspectUsingLargeXrayScanner],
	[TransportationKindCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CustomsOffice]
WHERE
	[CustomsCode] = @CustomsCode

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_SelectAll]    Script Date: 11/11/2013 16:24:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsProvinceCode],
	[OfficeIndicationOfExport],
	[OfficeIndicationOfImport],
	[OfficeIndicationOfBondedRelated],
	[OfficeIndicationOfSupervision],
	[CustomsOfficeName],
	[CustomsOfficeNameInVietnamese],
	[NameOfHeadOfCustomsOfficeInVietnamese],
	[Postcode],
	[AddressInVietnamese],
	[NameOfHeadOfCustomsOfficeInRomanAlphabet],
	[CustomsProvinceName],
	[CustomsBranchName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[InspectAtInspectionSite],
	[InspectUsingLargeXrayScanner],
	[TransportationKindCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CustomsOffice]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsProvinceCode],
	[OfficeIndicationOfExport],
	[OfficeIndicationOfImport],
	[OfficeIndicationOfBondedRelated],
	[OfficeIndicationOfSupervision],
	[CustomsOfficeName],
	[CustomsOfficeNameInVietnamese],
	[NameOfHeadOfCustomsOfficeInVietnamese],
	[Postcode],
	[AddressInVietnamese],
	[NameOfHeadOfCustomsOfficeInRomanAlphabet],
	[CustomsProvinceName],
	[CustomsBranchName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[InspectAtInspectionSite],
	[InspectUsingLargeXrayScanner],
	[TransportationKindCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_CustomsOffice] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsOffice_Update]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsProvinceCode varchar(5),
	@OfficeIndicationOfExport int,
	@OfficeIndicationOfImport int,
	@OfficeIndicationOfBondedRelated int,
	@OfficeIndicationOfSupervision int,
	@CustomsOfficeName varchar(100),
	@CustomsOfficeNameInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInVietnamese nvarchar(250),
	@Postcode varchar(10),
	@AddressInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInRomanAlphabet varchar(100),
	@CustomsProvinceName nvarchar(250),
	@CustomsBranchName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@InspectAtInspectionSite nvarchar(250),
	@InspectUsingLargeXrayScanner nvarchar(250),
	@TransportationKindCode int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_CustomsOffice]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[CustomsProvinceCode] = @CustomsProvinceCode,
	[OfficeIndicationOfExport] = @OfficeIndicationOfExport,
	[OfficeIndicationOfImport] = @OfficeIndicationOfImport,
	[OfficeIndicationOfBondedRelated] = @OfficeIndicationOfBondedRelated,
	[OfficeIndicationOfSupervision] = @OfficeIndicationOfSupervision,
	[CustomsOfficeName] = @CustomsOfficeName,
	[CustomsOfficeNameInVietnamese] = @CustomsOfficeNameInVietnamese,
	[NameOfHeadOfCustomsOfficeInVietnamese] = @NameOfHeadOfCustomsOfficeInVietnamese,
	[Postcode] = @Postcode,
	[AddressInVietnamese] = @AddressInVietnamese,
	[NameOfHeadOfCustomsOfficeInRomanAlphabet] = @NameOfHeadOfCustomsOfficeInRomanAlphabet,
	[CustomsProvinceName] = @CustomsProvinceName,
	[CustomsBranchName] = @CustomsBranchName,
	[DestinationCode_1] = @DestinationCode_1,
	[DestinationCode_2] = @DestinationCode_2,
	[DestinationCode_3] = @DestinationCode_3,
	[DestinationCode_4] = @DestinationCode_4,
	[DestinationCode_5] = @DestinationCode_5,
	[DestinationCode_6] = @DestinationCode_6,
	[DestinationCode_7] = @DestinationCode_7,
	[DestinationCode_8] = @DestinationCode_8,
	[DestinationCode_9] = @DestinationCode_9,
	[DestinationCode_10] = @DestinationCode_10,
	[InspectAtInspectionSite] = @InspectAtInspectionSite,
	[InspectUsingLargeXrayScanner] = @InspectUsingLargeXrayScanner,
	[TransportationKindCode] = @TransportationKindCode,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[CustomsCode] = @CustomsCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_Delete]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_CustomsSubSection]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_CustomsSubSection] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_Insert]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsSubSectionCode varchar(5),
	@ImportExportClassification varchar(5),
	@Name nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_CustomsSubSection]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsSubSectionCode],
	[ImportExportClassification],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@CustomsCode,
	@CustomsSubSectionCode,
	@ImportExportClassification,
	@Name,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsSubSectionCode varchar(5),
	@ImportExportClassification varchar(5),
	@Name nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_CustomsSubSection] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_CustomsSubSection] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[CustomsCode] = @CustomsCode,
			[CustomsSubSectionCode] = @CustomsSubSectionCode,
			[ImportExportClassification] = @ImportExportClassification,
			[Name] = @Name,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_CustomsSubSection]
		(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[CustomsCode],
			[CustomsSubSectionCode],
			[ImportExportClassification],
			[Name],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@CustomsCode,
			@CustomsSubSectionCode,
			@ImportExportClassification,
			@Name,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_Load]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsSubSectionCode],
	[ImportExportClassification],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CustomsSubSection]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsSubSectionCode],
	[ImportExportClassification],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CustomsSubSection]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsSubSectionCode],
	[ImportExportClassification],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_CustomsSubSection] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_CustomsSubSection_Update]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Update]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsSubSectionCode varchar(5),
	@ImportExportClassification varchar(5),
	@Name nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_CustomsSubSection]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[CustomsCode] = @CustomsCode,
	[CustomsSubSectionCode] = @CustomsSubSectionCode,
	[ImportExportClassification] = @ImportExportClassification,
	[Name] = @Name,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_Delete]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_Delete]
	@HSCode varchar(12)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_HSCode]
WHERE
	[HSCode] = @HSCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_DeleteDynamic]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_HSCode] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_Insert]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@HSCode varchar(12),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_HSCode]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[HSCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@HSCode,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_InsertUpdate]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@HSCode varchar(12),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [HSCode] FROM [dbo].[t_VNACC_Category_HSCode] WHERE [HSCode] = @HSCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_HSCode] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[HSCode] = @HSCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_HSCode]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[HSCode],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@HSCode,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_Load]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_Load]
	@HSCode varchar(12)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[HSCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_HSCode]
WHERE
	[HSCode] = @HSCode

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_SelectAll]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[HSCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_HSCode]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_SelectDynamic]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[HSCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_HSCode] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_HSCode_Update]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@HSCode varchar(12),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_HSCode]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[HSCode] = @HSCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_Delete]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_Delete]
	@NationCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Nation]
WHERE
	[NationCode] = @NationCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_DeleteDynamic]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Nation] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_Insert]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NationCode varchar(10),
	@GenerationManagementIndication varchar(5),
	@DateOfMaintenanceUpdated datetime,
	@CountryShortName varchar(20),
	@ApplicationStartDate datetime,
	@PlaceOfOriginIsMFNTaxRatesApplicationCountry int,
	@ImportTaxClassificationCodeForFTA1 varchar(10),
	@ImportTaxClassificationCodeForFTA2 varchar(10),
	@ImportTaxClassificationCodeForFTA3 varchar(10),
	@ImportTaxClassificationCodeForFTA4 varchar(10),
	@ImportTaxClassificationCodeForFTA5 varchar(10),
	@ImportTaxClassificationCodeForFTA6 varchar(10),
	@ImportTaxClassificationCodeForFTA7 varchar(10),
	@ImportTaxClassificationCodeForFTA8 varchar(10),
	@ImportTaxClassificationCodeForFTA9 varchar(10),
	@ImportTaxClassificationCodeForFTA10 varchar(10),
	@ImportTaxClassificationCodeForFTA11 varchar(10),
	@ImportTaxClassificationCodeForFTA12 varchar(10),
	@ImportTaxClassificationCodeForFTA13 varchar(10),
	@ImportTaxClassificationCodeForFTA14 varchar(10),
	@ImportTaxClassificationCodeForFTA15 varchar(10),
	@ImportTaxClassificationCodeForFTA16 varchar(10),
	@ImportTaxClassificationCodeForFTA17 varchar(10),
	@ImportTaxClassificationCodeForFTA18 varchar(10),
	@ImportTaxClassificationCodeForFTA19 varchar(10),
	@ImportTaxClassificationCodeForFTA20 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_Nation]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NationCode],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[CountryShortName],
	[ApplicationStartDate],
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
	[ImportTaxClassificationCodeForFTA1],
	[ImportTaxClassificationCodeForFTA2],
	[ImportTaxClassificationCodeForFTA3],
	[ImportTaxClassificationCodeForFTA4],
	[ImportTaxClassificationCodeForFTA5],
	[ImportTaxClassificationCodeForFTA6],
	[ImportTaxClassificationCodeForFTA7],
	[ImportTaxClassificationCodeForFTA8],
	[ImportTaxClassificationCodeForFTA9],
	[ImportTaxClassificationCodeForFTA10],
	[ImportTaxClassificationCodeForFTA11],
	[ImportTaxClassificationCodeForFTA12],
	[ImportTaxClassificationCodeForFTA13],
	[ImportTaxClassificationCodeForFTA14],
	[ImportTaxClassificationCodeForFTA15],
	[ImportTaxClassificationCodeForFTA16],
	[ImportTaxClassificationCodeForFTA17],
	[ImportTaxClassificationCodeForFTA18],
	[ImportTaxClassificationCodeForFTA19],
	[ImportTaxClassificationCodeForFTA20],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@NationCode,
	@GenerationManagementIndication,
	@DateOfMaintenanceUpdated,
	@CountryShortName,
	@ApplicationStartDate,
	@PlaceOfOriginIsMFNTaxRatesApplicationCountry,
	@ImportTaxClassificationCodeForFTA1,
	@ImportTaxClassificationCodeForFTA2,
	@ImportTaxClassificationCodeForFTA3,
	@ImportTaxClassificationCodeForFTA4,
	@ImportTaxClassificationCodeForFTA5,
	@ImportTaxClassificationCodeForFTA6,
	@ImportTaxClassificationCodeForFTA7,
	@ImportTaxClassificationCodeForFTA8,
	@ImportTaxClassificationCodeForFTA9,
	@ImportTaxClassificationCodeForFTA10,
	@ImportTaxClassificationCodeForFTA11,
	@ImportTaxClassificationCodeForFTA12,
	@ImportTaxClassificationCodeForFTA13,
	@ImportTaxClassificationCodeForFTA14,
	@ImportTaxClassificationCodeForFTA15,
	@ImportTaxClassificationCodeForFTA16,
	@ImportTaxClassificationCodeForFTA17,
	@ImportTaxClassificationCodeForFTA18,
	@ImportTaxClassificationCodeForFTA19,
	@ImportTaxClassificationCodeForFTA20,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_InsertUpdate]    Script Date: 11/11/2013 16:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NationCode varchar(10),
	@GenerationManagementIndication varchar(5),
	@DateOfMaintenanceUpdated datetime,
	@CountryShortName varchar(20),
	@ApplicationStartDate datetime,
	@PlaceOfOriginIsMFNTaxRatesApplicationCountry int,
	@ImportTaxClassificationCodeForFTA1 varchar(10),
	@ImportTaxClassificationCodeForFTA2 varchar(10),
	@ImportTaxClassificationCodeForFTA3 varchar(10),
	@ImportTaxClassificationCodeForFTA4 varchar(10),
	@ImportTaxClassificationCodeForFTA5 varchar(10),
	@ImportTaxClassificationCodeForFTA6 varchar(10),
	@ImportTaxClassificationCodeForFTA7 varchar(10),
	@ImportTaxClassificationCodeForFTA8 varchar(10),
	@ImportTaxClassificationCodeForFTA9 varchar(10),
	@ImportTaxClassificationCodeForFTA10 varchar(10),
	@ImportTaxClassificationCodeForFTA11 varchar(10),
	@ImportTaxClassificationCodeForFTA12 varchar(10),
	@ImportTaxClassificationCodeForFTA13 varchar(10),
	@ImportTaxClassificationCodeForFTA14 varchar(10),
	@ImportTaxClassificationCodeForFTA15 varchar(10),
	@ImportTaxClassificationCodeForFTA16 varchar(10),
	@ImportTaxClassificationCodeForFTA17 varchar(10),
	@ImportTaxClassificationCodeForFTA18 varchar(10),
	@ImportTaxClassificationCodeForFTA19 varchar(10),
	@ImportTaxClassificationCodeForFTA20 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [NationCode] FROM [dbo].[t_VNACC_Category_Nation] WHERE [NationCode] = @NationCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Nation] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[GenerationManagementIndication] = @GenerationManagementIndication,
			[DateOfMaintenanceUpdated] = @DateOfMaintenanceUpdated,
			[CountryShortName] = @CountryShortName,
			[ApplicationStartDate] = @ApplicationStartDate,
			[PlaceOfOriginIsMFNTaxRatesApplicationCountry] = @PlaceOfOriginIsMFNTaxRatesApplicationCountry,
			[ImportTaxClassificationCodeForFTA1] = @ImportTaxClassificationCodeForFTA1,
			[ImportTaxClassificationCodeForFTA2] = @ImportTaxClassificationCodeForFTA2,
			[ImportTaxClassificationCodeForFTA3] = @ImportTaxClassificationCodeForFTA3,
			[ImportTaxClassificationCodeForFTA4] = @ImportTaxClassificationCodeForFTA4,
			[ImportTaxClassificationCodeForFTA5] = @ImportTaxClassificationCodeForFTA5,
			[ImportTaxClassificationCodeForFTA6] = @ImportTaxClassificationCodeForFTA6,
			[ImportTaxClassificationCodeForFTA7] = @ImportTaxClassificationCodeForFTA7,
			[ImportTaxClassificationCodeForFTA8] = @ImportTaxClassificationCodeForFTA8,
			[ImportTaxClassificationCodeForFTA9] = @ImportTaxClassificationCodeForFTA9,
			[ImportTaxClassificationCodeForFTA10] = @ImportTaxClassificationCodeForFTA10,
			[ImportTaxClassificationCodeForFTA11] = @ImportTaxClassificationCodeForFTA11,
			[ImportTaxClassificationCodeForFTA12] = @ImportTaxClassificationCodeForFTA12,
			[ImportTaxClassificationCodeForFTA13] = @ImportTaxClassificationCodeForFTA13,
			[ImportTaxClassificationCodeForFTA14] = @ImportTaxClassificationCodeForFTA14,
			[ImportTaxClassificationCodeForFTA15] = @ImportTaxClassificationCodeForFTA15,
			[ImportTaxClassificationCodeForFTA16] = @ImportTaxClassificationCodeForFTA16,
			[ImportTaxClassificationCodeForFTA17] = @ImportTaxClassificationCodeForFTA17,
			[ImportTaxClassificationCodeForFTA18] = @ImportTaxClassificationCodeForFTA18,
			[ImportTaxClassificationCodeForFTA19] = @ImportTaxClassificationCodeForFTA19,
			[ImportTaxClassificationCodeForFTA20] = @ImportTaxClassificationCodeForFTA20,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[NationCode] = @NationCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_Nation]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[NationCode],
			[GenerationManagementIndication],
			[DateOfMaintenanceUpdated],
			[CountryShortName],
			[ApplicationStartDate],
			[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
			[ImportTaxClassificationCodeForFTA1],
			[ImportTaxClassificationCodeForFTA2],
			[ImportTaxClassificationCodeForFTA3],
			[ImportTaxClassificationCodeForFTA4],
			[ImportTaxClassificationCodeForFTA5],
			[ImportTaxClassificationCodeForFTA6],
			[ImportTaxClassificationCodeForFTA7],
			[ImportTaxClassificationCodeForFTA8],
			[ImportTaxClassificationCodeForFTA9],
			[ImportTaxClassificationCodeForFTA10],
			[ImportTaxClassificationCodeForFTA11],
			[ImportTaxClassificationCodeForFTA12],
			[ImportTaxClassificationCodeForFTA13],
			[ImportTaxClassificationCodeForFTA14],
			[ImportTaxClassificationCodeForFTA15],
			[ImportTaxClassificationCodeForFTA16],
			[ImportTaxClassificationCodeForFTA17],
			[ImportTaxClassificationCodeForFTA18],
			[ImportTaxClassificationCodeForFTA19],
			[ImportTaxClassificationCodeForFTA20],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@NationCode,
			@GenerationManagementIndication,
			@DateOfMaintenanceUpdated,
			@CountryShortName,
			@ApplicationStartDate,
			@PlaceOfOriginIsMFNTaxRatesApplicationCountry,
			@ImportTaxClassificationCodeForFTA1,
			@ImportTaxClassificationCodeForFTA2,
			@ImportTaxClassificationCodeForFTA3,
			@ImportTaxClassificationCodeForFTA4,
			@ImportTaxClassificationCodeForFTA5,
			@ImportTaxClassificationCodeForFTA6,
			@ImportTaxClassificationCodeForFTA7,
			@ImportTaxClassificationCodeForFTA8,
			@ImportTaxClassificationCodeForFTA9,
			@ImportTaxClassificationCodeForFTA10,
			@ImportTaxClassificationCodeForFTA11,
			@ImportTaxClassificationCodeForFTA12,
			@ImportTaxClassificationCodeForFTA13,
			@ImportTaxClassificationCodeForFTA14,
			@ImportTaxClassificationCodeForFTA15,
			@ImportTaxClassificationCodeForFTA16,
			@ImportTaxClassificationCodeForFTA17,
			@ImportTaxClassificationCodeForFTA18,
			@ImportTaxClassificationCodeForFTA19,
			@ImportTaxClassificationCodeForFTA20,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_Load]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_Load]
	@NationCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NationCode],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[CountryShortName],
	[ApplicationStartDate],
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
	[ImportTaxClassificationCodeForFTA1],
	[ImportTaxClassificationCodeForFTA2],
	[ImportTaxClassificationCodeForFTA3],
	[ImportTaxClassificationCodeForFTA4],
	[ImportTaxClassificationCodeForFTA5],
	[ImportTaxClassificationCodeForFTA6],
	[ImportTaxClassificationCodeForFTA7],
	[ImportTaxClassificationCodeForFTA8],
	[ImportTaxClassificationCodeForFTA9],
	[ImportTaxClassificationCodeForFTA10],
	[ImportTaxClassificationCodeForFTA11],
	[ImportTaxClassificationCodeForFTA12],
	[ImportTaxClassificationCodeForFTA13],
	[ImportTaxClassificationCodeForFTA14],
	[ImportTaxClassificationCodeForFTA15],
	[ImportTaxClassificationCodeForFTA16],
	[ImportTaxClassificationCodeForFTA17],
	[ImportTaxClassificationCodeForFTA18],
	[ImportTaxClassificationCodeForFTA19],
	[ImportTaxClassificationCodeForFTA20],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Nation]
WHERE
	[NationCode] = @NationCode

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_SelectAll]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NationCode],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[CountryShortName],
	[ApplicationStartDate],
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
	[ImportTaxClassificationCodeForFTA1],
	[ImportTaxClassificationCodeForFTA2],
	[ImportTaxClassificationCodeForFTA3],
	[ImportTaxClassificationCodeForFTA4],
	[ImportTaxClassificationCodeForFTA5],
	[ImportTaxClassificationCodeForFTA6],
	[ImportTaxClassificationCodeForFTA7],
	[ImportTaxClassificationCodeForFTA8],
	[ImportTaxClassificationCodeForFTA9],
	[ImportTaxClassificationCodeForFTA10],
	[ImportTaxClassificationCodeForFTA11],
	[ImportTaxClassificationCodeForFTA12],
	[ImportTaxClassificationCodeForFTA13],
	[ImportTaxClassificationCodeForFTA14],
	[ImportTaxClassificationCodeForFTA15],
	[ImportTaxClassificationCodeForFTA16],
	[ImportTaxClassificationCodeForFTA17],
	[ImportTaxClassificationCodeForFTA18],
	[ImportTaxClassificationCodeForFTA19],
	[ImportTaxClassificationCodeForFTA20],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Nation]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_SelectDynamic]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NationCode],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[CountryShortName],
	[ApplicationStartDate],
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
	[ImportTaxClassificationCodeForFTA1],
	[ImportTaxClassificationCodeForFTA2],
	[ImportTaxClassificationCodeForFTA3],
	[ImportTaxClassificationCodeForFTA4],
	[ImportTaxClassificationCodeForFTA5],
	[ImportTaxClassificationCodeForFTA6],
	[ImportTaxClassificationCodeForFTA7],
	[ImportTaxClassificationCodeForFTA8],
	[ImportTaxClassificationCodeForFTA9],
	[ImportTaxClassificationCodeForFTA10],
	[ImportTaxClassificationCodeForFTA11],
	[ImportTaxClassificationCodeForFTA12],
	[ImportTaxClassificationCodeForFTA13],
	[ImportTaxClassificationCodeForFTA14],
	[ImportTaxClassificationCodeForFTA15],
	[ImportTaxClassificationCodeForFTA16],
	[ImportTaxClassificationCodeForFTA17],
	[ImportTaxClassificationCodeForFTA18],
	[ImportTaxClassificationCodeForFTA19],
	[ImportTaxClassificationCodeForFTA20],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_Nation] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Nation_Update]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NationCode varchar(10),
	@GenerationManagementIndication varchar(5),
	@DateOfMaintenanceUpdated datetime,
	@CountryShortName varchar(20),
	@ApplicationStartDate datetime,
	@PlaceOfOriginIsMFNTaxRatesApplicationCountry int,
	@ImportTaxClassificationCodeForFTA1 varchar(10),
	@ImportTaxClassificationCodeForFTA2 varchar(10),
	@ImportTaxClassificationCodeForFTA3 varchar(10),
	@ImportTaxClassificationCodeForFTA4 varchar(10),
	@ImportTaxClassificationCodeForFTA5 varchar(10),
	@ImportTaxClassificationCodeForFTA6 varchar(10),
	@ImportTaxClassificationCodeForFTA7 varchar(10),
	@ImportTaxClassificationCodeForFTA8 varchar(10),
	@ImportTaxClassificationCodeForFTA9 varchar(10),
	@ImportTaxClassificationCodeForFTA10 varchar(10),
	@ImportTaxClassificationCodeForFTA11 varchar(10),
	@ImportTaxClassificationCodeForFTA12 varchar(10),
	@ImportTaxClassificationCodeForFTA13 varchar(10),
	@ImportTaxClassificationCodeForFTA14 varchar(10),
	@ImportTaxClassificationCodeForFTA15 varchar(10),
	@ImportTaxClassificationCodeForFTA16 varchar(10),
	@ImportTaxClassificationCodeForFTA17 varchar(10),
	@ImportTaxClassificationCodeForFTA18 varchar(10),
	@ImportTaxClassificationCodeForFTA19 varchar(10),
	@ImportTaxClassificationCodeForFTA20 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_Nation]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[GenerationManagementIndication] = @GenerationManagementIndication,
	[DateOfMaintenanceUpdated] = @DateOfMaintenanceUpdated,
	[CountryShortName] = @CountryShortName,
	[ApplicationStartDate] = @ApplicationStartDate,
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry] = @PlaceOfOriginIsMFNTaxRatesApplicationCountry,
	[ImportTaxClassificationCodeForFTA1] = @ImportTaxClassificationCodeForFTA1,
	[ImportTaxClassificationCodeForFTA2] = @ImportTaxClassificationCodeForFTA2,
	[ImportTaxClassificationCodeForFTA3] = @ImportTaxClassificationCodeForFTA3,
	[ImportTaxClassificationCodeForFTA4] = @ImportTaxClassificationCodeForFTA4,
	[ImportTaxClassificationCodeForFTA5] = @ImportTaxClassificationCodeForFTA5,
	[ImportTaxClassificationCodeForFTA6] = @ImportTaxClassificationCodeForFTA6,
	[ImportTaxClassificationCodeForFTA7] = @ImportTaxClassificationCodeForFTA7,
	[ImportTaxClassificationCodeForFTA8] = @ImportTaxClassificationCodeForFTA8,
	[ImportTaxClassificationCodeForFTA9] = @ImportTaxClassificationCodeForFTA9,
	[ImportTaxClassificationCodeForFTA10] = @ImportTaxClassificationCodeForFTA10,
	[ImportTaxClassificationCodeForFTA11] = @ImportTaxClassificationCodeForFTA11,
	[ImportTaxClassificationCodeForFTA12] = @ImportTaxClassificationCodeForFTA12,
	[ImportTaxClassificationCodeForFTA13] = @ImportTaxClassificationCodeForFTA13,
	[ImportTaxClassificationCodeForFTA14] = @ImportTaxClassificationCodeForFTA14,
	[ImportTaxClassificationCodeForFTA15] = @ImportTaxClassificationCodeForFTA15,
	[ImportTaxClassificationCodeForFTA16] = @ImportTaxClassificationCodeForFTA16,
	[ImportTaxClassificationCodeForFTA17] = @ImportTaxClassificationCodeForFTA17,
	[ImportTaxClassificationCodeForFTA18] = @ImportTaxClassificationCodeForFTA18,
	[ImportTaxClassificationCodeForFTA19] = @ImportTaxClassificationCodeForFTA19,
	[ImportTaxClassificationCodeForFTA20] = @ImportTaxClassificationCodeForFTA20,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[NationCode] = @NationCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_Delete]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Delete]
	@OfficeOfApplicationCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_OGAUser]
WHERE
	[OfficeOfApplicationCode] = @OfficeOfApplicationCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_OGAUser] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_Insert]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@OfficeOfApplicationCode varchar(10),
	@OfficeOfApplicationName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_OGAUser]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[OfficeOfApplicationCode],
	[OfficeOfApplicationName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@OfficeOfApplicationCode,
	@OfficeOfApplicationName,
	@DestinationCode_1,
	@DestinationCode_2,
	@DestinationCode_3,
	@DestinationCode_4,
	@DestinationCode_5,
	@DestinationCode_6,
	@DestinationCode_7,
	@DestinationCode_8,
	@DestinationCode_9,
	@DestinationCode_10,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_InsertUpdate]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@OfficeOfApplicationCode varchar(10),
	@OfficeOfApplicationName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [OfficeOfApplicationCode] FROM [dbo].[t_VNACC_Category_OGAUser] WHERE [OfficeOfApplicationCode] = @OfficeOfApplicationCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_OGAUser] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[OfficeOfApplicationName] = @OfficeOfApplicationName,
			[DestinationCode_1] = @DestinationCode_1,
			[DestinationCode_2] = @DestinationCode_2,
			[DestinationCode_3] = @DestinationCode_3,
			[DestinationCode_4] = @DestinationCode_4,
			[DestinationCode_5] = @DestinationCode_5,
			[DestinationCode_6] = @DestinationCode_6,
			[DestinationCode_7] = @DestinationCode_7,
			[DestinationCode_8] = @DestinationCode_8,
			[DestinationCode_9] = @DestinationCode_9,
			[DestinationCode_10] = @DestinationCode_10,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[OfficeOfApplicationCode] = @OfficeOfApplicationCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_OGAUser]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[OfficeOfApplicationCode],
			[OfficeOfApplicationName],
			[DestinationCode_1],
			[DestinationCode_2],
			[DestinationCode_3],
			[DestinationCode_4],
			[DestinationCode_5],
			[DestinationCode_6],
			[DestinationCode_7],
			[DestinationCode_8],
			[DestinationCode_9],
			[DestinationCode_10],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@OfficeOfApplicationCode,
			@OfficeOfApplicationName,
			@DestinationCode_1,
			@DestinationCode_2,
			@DestinationCode_3,
			@DestinationCode_4,
			@DestinationCode_5,
			@DestinationCode_6,
			@DestinationCode_7,
			@DestinationCode_8,
			@DestinationCode_9,
			@DestinationCode_10,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_Load]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Load]
	@OfficeOfApplicationCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[OfficeOfApplicationCode],
	[OfficeOfApplicationName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_OGAUser]
WHERE
	[OfficeOfApplicationCode] = @OfficeOfApplicationCode

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_SelectAll]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[OfficeOfApplicationCode],
	[OfficeOfApplicationName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_OGAUser]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_SelectDynamic]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[OfficeOfApplicationCode],
	[OfficeOfApplicationName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_OGAUser] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_OGAUser_Update]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@OfficeOfApplicationCode varchar(10),
	@OfficeOfApplicationName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_OGAUser]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[OfficeOfApplicationName] = @OfficeOfApplicationName,
	[DestinationCode_1] = @DestinationCode_1,
	[DestinationCode_2] = @DestinationCode_2,
	[DestinationCode_3] = @DestinationCode_3,
	[DestinationCode_4] = @DestinationCode_4,
	[DestinationCode_5] = @DestinationCode_5,
	[DestinationCode_6] = @DestinationCode_6,
	[DestinationCode_7] = @DestinationCode_7,
	[DestinationCode_8] = @DestinationCode_8,
	[DestinationCode_9] = @DestinationCode_9,
	[DestinationCode_10] = @DestinationCode_10,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[OfficeOfApplicationCode] = @OfficeOfApplicationCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_Delete]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Delete]
	@NumberOfPackagesUnitCode varchar(5)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_PackagesUnit]
WHERE
	[NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_PackagesUnit] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_Insert]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NumberOfPackagesUnitCode varchar(5),
	@NumberOfPackagesUnitName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_PackagesUnit]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NumberOfPackagesUnitCode],
	[NumberOfPackagesUnitName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@NumberOfPackagesUnitCode,
	@NumberOfPackagesUnitName,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NumberOfPackagesUnitCode varchar(5),
	@NumberOfPackagesUnitName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [NumberOfPackagesUnitCode] FROM [dbo].[t_VNACC_Category_PackagesUnit] WHERE [NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_PackagesUnit] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[NumberOfPackagesUnitName] = @NumberOfPackagesUnitName,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_PackagesUnit]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[NumberOfPackagesUnitCode],
			[NumberOfPackagesUnitName],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@NumberOfPackagesUnitCode,
			@NumberOfPackagesUnitName,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_Load]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Load]
	@NumberOfPackagesUnitCode varchar(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NumberOfPackagesUnitCode],
	[NumberOfPackagesUnitName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_PackagesUnit]
WHERE
	[NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_SelectAll]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NumberOfPackagesUnitCode],
	[NumberOfPackagesUnitName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_PackagesUnit]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NumberOfPackagesUnitCode],
	[NumberOfPackagesUnitName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_PackagesUnit] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_PackagesUnit_Update]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NumberOfPackagesUnitCode varchar(5),
	@NumberOfPackagesUnitName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_PackagesUnit]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[NumberOfPackagesUnitName] = @NumberOfPackagesUnitName,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_Delete]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Delete]
	@Code varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_QuantityUnit]
WHERE
	[Code] = @Code


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_QuantityUnit] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_Insert]    Script Date: 11/11/2013 16:25:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Insert]
	@TableID varchar(10),
	@Code varchar(10),
	@Name nvarchar(250),
	@Notes nvarchar(150),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_QuantityUnit]
(
	[TableID],
	[Code],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@TableID,
	@Code,
	@Name,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]
	@TableID varchar(10),
	@Code varchar(10),
	@Name nvarchar(250),
	@Notes nvarchar(150),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [Code] FROM [dbo].[t_VNACC_Category_QuantityUnit] WHERE [Code] = @Code)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_QuantityUnit] 
		SET
			[TableID] = @TableID,
			[Name] = @Name,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[Code] = @Code
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_QuantityUnit]
	(
			[TableID],
			[Code],
			[Name],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@TableID,
			@Code,
			@Name,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_Load]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Load]
	@Code varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[TableID],
	[Code],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_QuantityUnit]
WHERE
	[Code] = @Code

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_SelectAll]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[TableID],
	[Code],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_QuantityUnit]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[TableID],
	[Code],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_QuantityUnit] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_QuantityUnit_Update]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Update]
	@TableID varchar(10),
	@Code varchar(10),
	@Name nvarchar(250),
	@Notes nvarchar(150),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_QuantityUnit]
SET
	[TableID] = @TableID,
	[Name] = @Name,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[Code] = @Code


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_Delete]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_Delete]
	@StationCode varchar(5)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Stations]
WHERE
	[StationCode] = @StationCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_DeleteDynamic]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Stations] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_Insert]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@StationCode varchar(5),
	@StationName nvarchar(250),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@CustomsOfficeCode varchar(5),
	@CustomsOfficeCodeRM varchar(5),
	@UserCodeOfImmigrationBureau varchar(5),
	@ManifestForImmigration int,
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@CrewForImmigration int,
	@UserCodeOfQuarantineStation varchar(5),
	@ManifestForQuarantine int,
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@CrewForQuarantine int,
	@UserCodeOfRailwayStationAuthority varchar(5),
	@ManifestForRailwayStationAuthority int,
	@ArrivalDepartureForRailwayStationAuthority int,
	@PassengerForRailwayStationAuthority int,
	@CrewForRailwayStationAuthority int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_Stations]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[StationCode],
	[StationName],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[CustomsOfficeCode],
	[CustomsOfficeCodeRM],
	[UserCodeOfImmigrationBureau],
	[ManifestForImmigration],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[CrewForImmigration],
	[UserCodeOfQuarantineStation],
	[ManifestForQuarantine],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[CrewForQuarantine],
	[UserCodeOfRailwayStationAuthority],
	[ManifestForRailwayStationAuthority],
	[ArrivalDepartureForRailwayStationAuthority],
	[PassengerForRailwayStationAuthority],
	[CrewForRailwayStationAuthority],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@StationCode,
	@StationName,
	@CountryCode,
	@NecessityIndicationOfInputName,
	@CustomsOfficeCode,
	@CustomsOfficeCodeRM,
	@UserCodeOfImmigrationBureau,
	@ManifestForImmigration,
	@ArrivalDepartureForImmigration,
	@PassengerForImmigration,
	@CrewForImmigration,
	@UserCodeOfQuarantineStation,
	@ManifestForQuarantine,
	@ArrivalDepartureForQuarantine,
	@PassengerForQuarantine,
	@CrewForQuarantine,
	@UserCodeOfRailwayStationAuthority,
	@ManifestForRailwayStationAuthority,
	@ArrivalDepartureForRailwayStationAuthority,
	@PassengerForRailwayStationAuthority,
	@CrewForRailwayStationAuthority,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_InsertUpdate]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@StationCode varchar(5),
	@StationName nvarchar(250),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@CustomsOfficeCode varchar(5),
	@CustomsOfficeCodeRM varchar(5),
	@UserCodeOfImmigrationBureau varchar(5),
	@ManifestForImmigration int,
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@CrewForImmigration int,
	@UserCodeOfQuarantineStation varchar(5),
	@ManifestForQuarantine int,
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@CrewForQuarantine int,
	@UserCodeOfRailwayStationAuthority varchar(5),
	@ManifestForRailwayStationAuthority int,
	@ArrivalDepartureForRailwayStationAuthority int,
	@PassengerForRailwayStationAuthority int,
	@CrewForRailwayStationAuthority int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [StationCode] FROM [dbo].[t_VNACC_Category_Stations] WHERE [StationCode] = @StationCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Stations] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[StationName] = @StationName,
			[CountryCode] = @CountryCode,
			[NecessityIndicationOfInputName] = @NecessityIndicationOfInputName,
			[CustomsOfficeCode] = @CustomsOfficeCode,
			[CustomsOfficeCodeRM] = @CustomsOfficeCodeRM,
			[UserCodeOfImmigrationBureau] = @UserCodeOfImmigrationBureau,
			[ManifestForImmigration] = @ManifestForImmigration,
			[ArrivalDepartureForImmigration] = @ArrivalDepartureForImmigration,
			[PassengerForImmigration] = @PassengerForImmigration,
			[CrewForImmigration] = @CrewForImmigration,
			[UserCodeOfQuarantineStation] = @UserCodeOfQuarantineStation,
			[ManifestForQuarantine] = @ManifestForQuarantine,
			[ArrivalDepartureForQuarantine] = @ArrivalDepartureForQuarantine,
			[PassengerForQuarantine] = @PassengerForQuarantine,
			[CrewForQuarantine] = @CrewForQuarantine,
			[UserCodeOfRailwayStationAuthority] = @UserCodeOfRailwayStationAuthority,
			[ManifestForRailwayStationAuthority] = @ManifestForRailwayStationAuthority,
			[ArrivalDepartureForRailwayStationAuthority] = @ArrivalDepartureForRailwayStationAuthority,
			[PassengerForRailwayStationAuthority] = @PassengerForRailwayStationAuthority,
			[CrewForRailwayStationAuthority] = @CrewForRailwayStationAuthority,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[StationCode] = @StationCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_Stations]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[StationCode],
			[StationName],
			[CountryCode],
			[NecessityIndicationOfInputName],
			[CustomsOfficeCode],
			[CustomsOfficeCodeRM],
			[UserCodeOfImmigrationBureau],
			[ManifestForImmigration],
			[ArrivalDepartureForImmigration],
			[PassengerForImmigration],
			[CrewForImmigration],
			[UserCodeOfQuarantineStation],
			[ManifestForQuarantine],
			[ArrivalDepartureForQuarantine],
			[PassengerForQuarantine],
			[CrewForQuarantine],
			[UserCodeOfRailwayStationAuthority],
			[ManifestForRailwayStationAuthority],
			[ArrivalDepartureForRailwayStationAuthority],
			[PassengerForRailwayStationAuthority],
			[CrewForRailwayStationAuthority],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@StationCode,
			@StationName,
			@CountryCode,
			@NecessityIndicationOfInputName,
			@CustomsOfficeCode,
			@CustomsOfficeCodeRM,
			@UserCodeOfImmigrationBureau,
			@ManifestForImmigration,
			@ArrivalDepartureForImmigration,
			@PassengerForImmigration,
			@CrewForImmigration,
			@UserCodeOfQuarantineStation,
			@ManifestForQuarantine,
			@ArrivalDepartureForQuarantine,
			@PassengerForQuarantine,
			@CrewForQuarantine,
			@UserCodeOfRailwayStationAuthority,
			@ManifestForRailwayStationAuthority,
			@ArrivalDepartureForRailwayStationAuthority,
			@PassengerForRailwayStationAuthority,
			@CrewForRailwayStationAuthority,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_Load]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_Load]
	@StationCode varchar(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[StationCode],
	[StationName],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[CustomsOfficeCode],
	[CustomsOfficeCodeRM],
	[UserCodeOfImmigrationBureau],
	[ManifestForImmigration],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[CrewForImmigration],
	[UserCodeOfQuarantineStation],
	[ManifestForQuarantine],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[CrewForQuarantine],
	[UserCodeOfRailwayStationAuthority],
	[ManifestForRailwayStationAuthority],
	[ArrivalDepartureForRailwayStationAuthority],
	[PassengerForRailwayStationAuthority],
	[CrewForRailwayStationAuthority],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Stations]
WHERE
	[StationCode] = @StationCode

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_SelectAll]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[StationCode],
	[StationName],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[CustomsOfficeCode],
	[CustomsOfficeCodeRM],
	[UserCodeOfImmigrationBureau],
	[ManifestForImmigration],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[CrewForImmigration],
	[UserCodeOfQuarantineStation],
	[ManifestForQuarantine],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[CrewForQuarantine],
	[UserCodeOfRailwayStationAuthority],
	[ManifestForRailwayStationAuthority],
	[ArrivalDepartureForRailwayStationAuthority],
	[PassengerForRailwayStationAuthority],
	[CrewForRailwayStationAuthority],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Stations]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_SelectDynamic]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[StationCode],
	[StationName],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[CustomsOfficeCode],
	[CustomsOfficeCodeRM],
	[UserCodeOfImmigrationBureau],
	[ManifestForImmigration],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[CrewForImmigration],
	[UserCodeOfQuarantineStation],
	[ManifestForQuarantine],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[CrewForQuarantine],
	[UserCodeOfRailwayStationAuthority],
	[ManifestForRailwayStationAuthority],
	[ArrivalDepartureForRailwayStationAuthority],
	[PassengerForRailwayStationAuthority],
	[CrewForRailwayStationAuthority],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_Stations] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Station_Update]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@StationCode varchar(5),
	@StationName nvarchar(250),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@CustomsOfficeCode varchar(5),
	@CustomsOfficeCodeRM varchar(5),
	@UserCodeOfImmigrationBureau varchar(5),
	@ManifestForImmigration int,
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@CrewForImmigration int,
	@UserCodeOfQuarantineStation varchar(5),
	@ManifestForQuarantine int,
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@CrewForQuarantine int,
	@UserCodeOfRailwayStationAuthority varchar(5),
	@ManifestForRailwayStationAuthority int,
	@ArrivalDepartureForRailwayStationAuthority int,
	@PassengerForRailwayStationAuthority int,
	@CrewForRailwayStationAuthority int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_Stations]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[StationName] = @StationName,
	[CountryCode] = @CountryCode,
	[NecessityIndicationOfInputName] = @NecessityIndicationOfInputName,
	[CustomsOfficeCode] = @CustomsOfficeCode,
	[CustomsOfficeCodeRM] = @CustomsOfficeCodeRM,
	[UserCodeOfImmigrationBureau] = @UserCodeOfImmigrationBureau,
	[ManifestForImmigration] = @ManifestForImmigration,
	[ArrivalDepartureForImmigration] = @ArrivalDepartureForImmigration,
	[PassengerForImmigration] = @PassengerForImmigration,
	[CrewForImmigration] = @CrewForImmigration,
	[UserCodeOfQuarantineStation] = @UserCodeOfQuarantineStation,
	[ManifestForQuarantine] = @ManifestForQuarantine,
	[ArrivalDepartureForQuarantine] = @ArrivalDepartureForQuarantine,
	[PassengerForQuarantine] = @PassengerForQuarantine,
	[CrewForQuarantine] = @CrewForQuarantine,
	[UserCodeOfRailwayStationAuthority] = @UserCodeOfRailwayStationAuthority,
	[ManifestForRailwayStationAuthority] = @ManifestForRailwayStationAuthority,
	[ArrivalDepartureForRailwayStationAuthority] = @ArrivalDepartureForRailwayStationAuthority,
	[PassengerForRailwayStationAuthority] = @PassengerForRailwayStationAuthority,
	[CrewForRailwayStationAuthority] = @CrewForRailwayStationAuthority,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[StationCode] = @StationCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_Delete]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Delete]
	@TaxCode varchar(5)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_TaxClassificationCode]
WHERE
	[TaxCode] = @TaxCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_TaxClassificationCode] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_Insert]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TaxCode varchar(5),
	@TaxName nvarchar(250),
	@IndicationOfCommonTax decimal(5, 2),
	@IndicationOfPreferantialTax decimal(5, 2),
	@IndicationOfMFNTax decimal(5, 2),
	@IndicationOfFTATax decimal(5, 2),
	@IndicationOfOutOfQuota decimal(5, 2),
	@IndicationOfSpecificDuty decimal(5, 2),
	@IndicationOfSpecificDutyAndAdValoremDuty decimal(5, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_TaxClassificationCode]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TaxCode],
	[TaxName],
	[IndicationOfCommonTax],
	[IndicationOfPreferantialTax],
	[IndicationOfMFNTax],
	[IndicationOfFTATax],
	[IndicationOfOutOfQuota],
	[IndicationOfSpecificDuty],
	[IndicationOfSpecificDutyAndAdValoremDuty],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@TaxCode,
	@TaxName,
	@IndicationOfCommonTax,
	@IndicationOfPreferantialTax,
	@IndicationOfMFNTax,
	@IndicationOfFTATax,
	@IndicationOfOutOfQuota,
	@IndicationOfSpecificDuty,
	@IndicationOfSpecificDutyAndAdValoremDuty,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TaxCode varchar(5),
	@TaxName nvarchar(250),
	@IndicationOfCommonTax decimal(5, 2),
	@IndicationOfPreferantialTax decimal(5, 2),
	@IndicationOfMFNTax decimal(5, 2),
	@IndicationOfFTATax decimal(5, 2),
	@IndicationOfOutOfQuota decimal(5, 2),
	@IndicationOfSpecificDuty decimal(5, 2),
	@IndicationOfSpecificDutyAndAdValoremDuty decimal(5, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [TaxCode] FROM [dbo].[t_VNACC_Category_TaxClassificationCode] WHERE [TaxCode] = @TaxCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_TaxClassificationCode] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[TaxName] = @TaxName,
			[IndicationOfCommonTax] = @IndicationOfCommonTax,
			[IndicationOfPreferantialTax] = @IndicationOfPreferantialTax,
			[IndicationOfMFNTax] = @IndicationOfMFNTax,
			[IndicationOfFTATax] = @IndicationOfFTATax,
			[IndicationOfOutOfQuota] = @IndicationOfOutOfQuota,
			[IndicationOfSpecificDuty] = @IndicationOfSpecificDuty,
			[IndicationOfSpecificDutyAndAdValoremDuty] = @IndicationOfSpecificDutyAndAdValoremDuty,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[TaxCode] = @TaxCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_TaxClassificationCode]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[TaxCode],
			[TaxName],
			[IndicationOfCommonTax],
			[IndicationOfPreferantialTax],
			[IndicationOfMFNTax],
			[IndicationOfFTATax],
			[IndicationOfOutOfQuota],
			[IndicationOfSpecificDuty],
			[IndicationOfSpecificDutyAndAdValoremDuty],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@TaxCode,
			@TaxName,
			@IndicationOfCommonTax,
			@IndicationOfPreferantialTax,
			@IndicationOfMFNTax,
			@IndicationOfFTATax,
			@IndicationOfOutOfQuota,
			@IndicationOfSpecificDuty,
			@IndicationOfSpecificDutyAndAdValoremDuty,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_Load]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Load]
	@TaxCode varchar(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TaxCode],
	[TaxName],
	[IndicationOfCommonTax],
	[IndicationOfPreferantialTax],
	[IndicationOfMFNTax],
	[IndicationOfFTATax],
	[IndicationOfOutOfQuota],
	[IndicationOfSpecificDuty],
	[IndicationOfSpecificDutyAndAdValoremDuty],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_TaxClassificationCode]
WHERE
	[TaxCode] = @TaxCode

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TaxCode],
	[TaxName],
	[IndicationOfCommonTax],
	[IndicationOfPreferantialTax],
	[IndicationOfMFNTax],
	[IndicationOfFTATax],
	[IndicationOfOutOfQuota],
	[IndicationOfSpecificDuty],
	[IndicationOfSpecificDutyAndAdValoremDuty],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_TaxClassificationCode]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TaxCode],
	[TaxName],
	[IndicationOfCommonTax],
	[IndicationOfPreferantialTax],
	[IndicationOfMFNTax],
	[IndicationOfFTATax],
	[IndicationOfOutOfQuota],
	[IndicationOfSpecificDuty],
	[IndicationOfSpecificDutyAndAdValoremDuty],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_TaxClassificationCode] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TaxClassificationCode_Update]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TaxCode varchar(5),
	@TaxName nvarchar(250),
	@IndicationOfCommonTax decimal(5, 2),
	@IndicationOfPreferantialTax decimal(5, 2),
	@IndicationOfMFNTax decimal(5, 2),
	@IndicationOfFTATax decimal(5, 2),
	@IndicationOfOutOfQuota decimal(5, 2),
	@IndicationOfSpecificDuty decimal(5, 2),
	@IndicationOfSpecificDutyAndAdValoremDuty decimal(5, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_TaxClassificationCode]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[TaxName] = @TaxName,
	[IndicationOfCommonTax] = @IndicationOfCommonTax,
	[IndicationOfPreferantialTax] = @IndicationOfPreferantialTax,
	[IndicationOfMFNTax] = @IndicationOfMFNTax,
	[IndicationOfFTATax] = @IndicationOfFTATax,
	[IndicationOfOutOfQuota] = @IndicationOfOutOfQuota,
	[IndicationOfSpecificDuty] = @IndicationOfSpecificDuty,
	[IndicationOfSpecificDutyAndAdValoremDuty] = @IndicationOfSpecificDutyAndAdValoremDuty,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[TaxCode] = @TaxCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_Delete]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Delete]
	@TransportMeansCode varchar(5)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_TransportMeans]
WHERE
	[TransportMeansCode] = @TransportMeansCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_DeleteDynamic]    Script Date: 11/11/2013 16:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_TransportMeans] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_Insert]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TransportMeansCode varchar(5),
	@TransportMeansName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_TransportMeans]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TransportMeansCode],
	[TransportMeansName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@TransportMeansCode,
	@TransportMeansName,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_InsertUpdate]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TransportMeansCode varchar(5),
	@TransportMeansName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [TransportMeansCode] FROM [dbo].[t_VNACC_Category_TransportMeans] WHERE [TransportMeansCode] = @TransportMeansCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_TransportMeans] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[TransportMeansName] = @TransportMeansName,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[TransportMeansCode] = @TransportMeansCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_TransportMeans]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[TransportMeansCode],
			[TransportMeansName],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@TransportMeansCode,
			@TransportMeansName,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_Load]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Load]
	@TransportMeansCode varchar(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TransportMeansCode],
	[TransportMeansName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_TransportMeans]
WHERE
	[TransportMeansCode] = @TransportMeansCode

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_SelectAll]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TransportMeansCode],
	[TransportMeansName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_TransportMeans]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_SelectDynamic]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TransportMeansCode],
	[TransportMeansName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_TransportMeans] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_TransportMean_Update]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TransportMeansCode varchar(5),
	@TransportMeansName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_TransportMeans]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[TransportMeansName] = @TransportMeansName,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[TransportMeansCode] = @TransportMeansCode


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_Delete]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Type]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_DeleteDynamic]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Type] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_Insert]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_Insert]
	@English varchar(250),
	@Vietnamese nvarchar(250),
	@ReferenceDB varchar(4),
	@Notes nvarchar(150),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_Type]
(
	[English],
	[Vietnamese],
	[ReferenceDB],
	[Notes]
)
VALUES 
(
	@English,
	@Vietnamese,
	@ReferenceDB,
	@Notes
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_InsertUpdate]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_InsertUpdate]
	@ID int,
	@English varchar(250),
	@Vietnamese nvarchar(250),
	@ReferenceDB varchar(4),
	@Notes nvarchar(150)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_Type] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Type] 
		SET
			[English] = @English,
			[Vietnamese] = @Vietnamese,
			[ReferenceDB] = @ReferenceDB,
			[Notes] = @Notes
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_Type]
		(
			[English],
			[Vietnamese],
			[ReferenceDB],
			[Notes]
		)
		VALUES 
		(
			@English,
			@Vietnamese,
			@ReferenceDB,
			@Notes
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_Load]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[English],
	[Vietnamese],
	[ReferenceDB],
	[Notes]
FROM
	[dbo].[t_VNACC_Category_Type]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_SelectAll]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[English],
	[Vietnamese],
	[ReferenceDB],
	[Notes]
FROM
	[dbo].[t_VNACC_Category_Type]	


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_SelectDynamic]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[English],
	[Vietnamese],
	[ReferenceDB],
	[Notes]
FROM [dbo].[t_VNACC_Category_Type] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_VNACC_Category_Type_Update]    Script Date: 11/11/2013 16:25:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 11, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_Update]
	@ID int,
	@English varchar(250),
	@Vietnamese nvarchar(250),
	@ReferenceDB varchar(4),
	@Notes nvarchar(150)
AS

UPDATE
	[dbo].[t_VNACC_Category_Type]
SET
	[English] = @English,
	[Vietnamese] = @Vietnamese,
	[ReferenceDB] = @ReferenceDB,
	[Notes] = @Notes
WHERE
	[ID] = @ID


GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '12.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('12.0', GETDATE(), N'Bo sung store cho cac danh muc VNACCS')
END	

