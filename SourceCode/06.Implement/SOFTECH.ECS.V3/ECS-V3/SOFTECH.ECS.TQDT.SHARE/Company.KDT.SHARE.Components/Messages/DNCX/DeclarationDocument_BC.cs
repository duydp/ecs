﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.DNCX
{
    public class DeclarationDocument_BC
    {
        /// <summary>
        /// Số tờ khai NK/ số chứng đưa hàng vào
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }
        /// <summary>
        /// Loại hình NK/ đưa hàng vào
        /// </summary>
        [XmlElement("natureOfTransaction")]
        public string NatureOfTransaction { get; set; }
        /// <summary>
        /// Ngày đăng ký (năm đăng ký)
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }
        /// <summary>
        /// Mã Hải quan làm thủ tục
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }
        /// <summary>
        /// Số lượng NK/ đưa hàng từ nội địa vào DNCX
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }
    }
}
