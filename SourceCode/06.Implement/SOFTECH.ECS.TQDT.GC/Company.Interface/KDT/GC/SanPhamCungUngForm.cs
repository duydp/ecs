﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.GC
{
    public partial class SanPhamCungUngForm : BaseForm
    {
        public ToKhaiMauDich TKMD = null;
        public ToKhaiChuyenTiep TKCT = null;
        private DataTable dt = new DataTable();
        public SanPhanCungUng SPCU = new SanPhanCungUng();
        public BKCungUngDangKy BKCU = new BKCungUngDangKy();
        public SanPhamCungUngForm()
        {
            InitializeComponent();
        }
        private bool CheckExitHang(string MaHang)
        {
            foreach (SanPhanCungUng spCungUng in BKCU.SanPhamCungUngCollection)
            {
                if (spCungUng.MaSanPham.Trim().ToUpper() == MaHang.ToUpper().Trim())
                    return true;
            }
            return false;
        }
        private void txtSoHopDong_ButtonClick(object sender, EventArgs e)
        {
            ChonSPCungUngForm f = new ChonSPCungUngForm();
            f.BKCU = this.BKCU;
            if (TKMD != null)
            {
                f.TKMD = this.TKMD;
                f.TKCT = null;
            }
            else
            {
                f.TKMD = null;
                f.TKCT = this.TKCT;
            }
            f.ShowDialog();
            if (TKMD != null)
            {
                if (f.HMDSelect == null || f.HMDSelect.MaPhu.Trim().Length == 0) return;
                if (CheckExitHang(f.HMDSelect.MaPhu))
                {
                    showMsg("MSG_0203001");
                   //MLMessages("Mặt hàng này đã tồn tại trên lưới.","MSG_WRN06","", false);
                    return;
                }
                txtMaSP.Text = f.HMDSelect.MaPhu;
                txtTenSP.Text = f.HMDSelect.TenHang;
                txtDVT.Text = DonViTinh_GetName(f.HMDSelect.DVT_ID);
                txtLuongXK.Value = f.HMDSelect.SoLuong;
            }
            else
            {
                if (f.HCTSelect == null || f.HCTSelect.MaHang.Trim().Length == 0) return;
                if (CheckExitHang(f.HCTSelect.MaHang))
                {
                    showMsg("MSG_0203001");
                    //MLMessages("Mặt hàng này đã tồn tại trên lưới.", "MSG_WRN06", "", false);
                    return;
                }
                txtMaSP.Text = f.HCTSelect.MaHang;
                txtTenSP.Text = f.HCTSelect.TenHang;
                txtDVT.Text = DonViTinh_GetName(f.HCTSelect.ID_DVT);
                txtLuongXK.Value = f.HCTSelect.SoLuong;
            }
            HopDong HD = new HopDong();
            if (TKMD != null)
                HD.ID = TKMD.IDHopDong;
            else
                HD.ID = TKCT.IDHopDong;
            HD.Load();
            this.dt = new Company.GC.BLL.GC.DinhMuc().getDinhMuc(HD.ID, txtMaSP.Text.Trim());
            if (dt == null || dt.Rows.Count == 0)
            {
                showMsg("MSG_PUB02");
                //MLMessages("Sản phẩm này chưa có định mức.","MSG_WRN17","", false);
            }
            DataColumn[] cols = new DataColumn[4];
            cols[0] = new DataColumn("LuongTCU", typeof(decimal));
            cols[1] = new DataColumn("DonGia", typeof(double));
            cols[2] = new DataColumn("TriGia", typeof(double));
            cols[3] = new DataColumn("HinhThucCU", typeof(string));
            dt.Columns.AddRange(cols);
            foreach (DataRow dr in this.dt.Rows)
            {
                dr["LuongTCU"] = Convert.ToDecimal(dr["DinhMucSuDung"]) * (100 + Convert.ToDecimal(dr["TyLeHaoHut"])) / 100 * Convert.ToDecimal(txtLuongCU.Value);
                dr["DonGia"] = 0;
                dr["HinhThucCU"] = "Mua VN";
            }
            dgList.DataSource = this.dt;
        }

        private void SanPhamCungUngForm_Load(object sender, EventArgs e)
        {
            if (BKCU.TrangThaiXuLy != -1)
            {
                uiButton2.Visible = false;
            }
            txtLuongXK.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
            txtLuongCU.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
            dgList.RootTable.Columns["LuongTCU"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.RootTable.Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
        }
        private void setErro()
        {
            epError.Clear();
            epError.SetError(txtMaSP, null);
            epError.SetError(txtTenSP, null);
            epError.SetError(txtDVT, null);

        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;

            if (Convert.ToDecimal(txtLuongCU.Text) > Convert.ToDecimal(txtLuongXK.Text))
            {
                showMsg("MSG_WRN21");
                //MLMessages("Lượng cung ứng không được lớn hơn lượng xuất khẩu.","MSG_WRN21","", false);
                return;
            }
            this.SPCU.NPLCungUngCollection = new NguyenPhuLieuCungUngCollection();
            this.SPCU.MaSanPham = txtMaSP.Text;
            this.SPCU.LuongCUSanPham = Convert.ToDecimal(txtLuongCU.Value);
            NguyenPhuLieuCungUng nplCungUng = new NguyenPhuLieuCungUng();
            GridEXRow[] rows = dgList.GetCheckedRows();
            foreach (GridEXRow row in rows)
            {
                DataRowView dv = (DataRowView)row.DataRow;
                NguyenPhuLieuCungUng npl = new NguyenPhuLieuCungUng();
                npl.MaNguyenPhuLieu = dv["MaNguyenPhuLieu"].ToString();
                //long id=0;
                //if (TKMD != null)
                //{
                //    id = nplCungUng.checkExitNPLCungUngTKMD(txtMaSP.Text.Trim(), npl.MaNguyenPhuLieu, TKMD.ID, BKCU.ID);
                //}
                //else
                //{
                //    id = nplCungUng.checkExitNPLCungUngTKCT(txtMaSP.Text.Trim(), npl.MaNguyenPhuLieu, TKCT.ID, BKCU.ID);
                //}
                //if (id > 0)
                //{
                //    if (ShowMessage("Nguyên phụ liệu cung ứng có mã " + npl.MaNguyenPhuLieu + " cho sản phẩm " + txtMaSP.Text + " đã được khai báo trong danh sách cung ứng có ID=" + id.ToString() + " nên sẽ được bỏ qua.\n Bạn có muốn tiếp tục không?", true) != "Yes")
                //    {
                //        return;
                //    }
                //    else
                //        continue;
                //}
                npl.LuongCung = Convert.ToDecimal(dv["LuongTCU"]);
                npl.DonGia = Convert.ToDouble(dv["DonGia"]);
                npl.TriGia = npl.DonGia * Convert.ToDouble(npl.LuongCung);
                npl.DinhMucCungUng = Convert.ToDouble(dv["DinhMucSuDung"]);
                npl.TyLeHH = Convert.ToDouble(dv["TyLeHaoHut"]);
                npl.HinhThuCungUng = dv["HinhThucCU"].ToString();
                if (npl.LuongCung > 0){                
                    this.SPCU.NPLCungUngCollection.Add(npl);
                }else
                {
                    string msg = setText(" Nguyên phụ liệu '" + npl.MaNguyenPhuLieu + "' chưa có định mức nên sẽ bỏ qua"," '"+ npl.MaNguyenPhuLieu + "'don't have Norm, so skip it ");
                    ShowMessage(msg,false);
                }

            }
            if (this.SPCU.NPLCungUngCollection.Count <= 0)
                showMsg("MSG_WRN22");
                //MLMessages("Bạn hãy chọn ít nhất 1 NPL cung ứng.","MSG_WRN22","", false);
            else {
                this.setErro();
                this.Close(); }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                // e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
            }
        }

        private void txtLuongCU_Leave(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtLuongCU.Text) > Convert.ToDecimal(txtLuongXK.Text))
            {
                showMsg("MSG_WRN21");
                //MLMessages("Lượng cung ứng không được lớn hơn lượng xuất khẩu.","MSG_WRN21","", false);
                return;
            }
            foreach (DataRow dr in this.dt.Rows)
            {
                if (dr.RowState != DataRowState.Deleted)
                    dr["LuongTCU"] = Convert.ToDecimal(dr["DinhMucSuDung"]) * (100 + Convert.ToDecimal(dr["TyLeHaoHut"])) / 100 * Convert.ToDecimal(txtLuongCU.Value);

            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void txtMaSP_TextChanged(object sender, EventArgs e)
        {

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count < 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {

                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPhanCungUng spcu = (SanPhanCungUng)i.GetRow().DataRow;
                        try
                        {
                            BKCU.SanPhamCungUngCollection.Remove(spcu);
                        }
                        catch { }
                    }
                }
                dgList.DataSource = BKCU.SanPhamCungUngCollection;
                this.setErro();
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
        }

        private void txtTenSP_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

