using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KD.BLL.Utils
{
    public partial class KDT
    {
        #region Private members.

        protected long _ID;
        protected string _SoLuong = string.Empty;
        protected string _TrangThai = string.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public string SoLuong
        {
            set { this._SoLuong = value; }
            get { return this._SoLuong; }
        }

        public string TrangThai
        {
            set { this._TrangThai = value; }
            get { return this._TrangThai; }
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static KDT Load(long iD)
        {
            const string spName = "[dbo].[p_KDT_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
            KDT entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new KDT();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetString(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetString(reader.GetOrdinal("TrangThai"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------
        public static List<KDT> SelectCollectionAll()
        {
            List<KDT> collection = new List<KDT>();
            SqlDataReader reader = SelectReaderAll();
            while (reader.Read())
            {
                KDT entity = new KDT();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetString(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetString(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public static List<KDT> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            List<KDT> collection = new List<KDT>();

            SqlDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                KDT entity = new KDT();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetString(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetString(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		


        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static SqlDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static SqlDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertKDT(string soLuong, string trangThai)
        {
            KDT entity = new KDT();
            entity.SoLuong = soLuong;
            entity.TrangThai = trangThai;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.NVarChar, SoLuong);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.NVarChar, TrangThai);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<KDT> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (KDT item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateKDT(long iD, string soLuong, string trangThai)
        {
            KDT entity = new KDT();
            entity.SoLuong = soLuong;
            entity.TrangThai = trangThai;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.NVarChar, SoLuong);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.NVarChar, TrangThai);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<KDT> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (KDT item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateKDT(long iD, string soLuong, string trangThai)
        {
            KDT entity = new KDT();
            entity.ID = iD;
            entity.SoLuong = soLuong;
            entity.TrangThai = trangThai;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.NVarChar, SoLuong);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.NVarChar, TrangThai);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<KDT> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (KDT item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteKDT(long iD)
        {
            KDT entity = new KDT();
            entity.ID = iD;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<KDT> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (KDT item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}