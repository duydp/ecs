IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP]
GO
CREATE TABLE t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT NOT NULL,
SoTN BIGINT,
NgayTN DATETIME,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
NamQuyetToan NUMERIC(4) NOT NULL,
LoaiHinhBaoCao NUMERIC(1) NOT NULL,
DVTBaoCao NUMERIC(1) NOT NULL,
GhiChuKhac NVARCHAR(2000),
[FileName] NVARCHAR(255),
Content NVARCHAR(MAX) NOT NULL,
GuidStr VARCHAR(MAX)
)

GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
GO
CREATE TABLE t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
GoodItem_ID BIGINT FOREIGN KEY REFERENCES t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP(ID),
STT NUMERIC(5) NOT NULL,
LoaiHangHoa NUMERIC(5) NOT NULL,
TaiKhoan NUMERIC(5) NOT NULL,
TenHangHoa NVARCHAR(255) NOT NULL,
MaHangHoa NVARCHAR(50) ,
DVT NVARCHAR(4),
TonDauKy NUMERIC(18,4) NOT NULL ,
NhapTrongKy NUMERIC(18,4) NOT NULL ,
XuatTrongKy NUMERIC(18,4) NOT NULL ,
TonCuoiKy NUMERIC(18,4) NOT NULL ,
GhiChu NVARCHAR(2000)
)
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@NamQuyetToan numeric(4, 0),
	@LoaiHinhBaoCao numeric(1, 0),
	@DVTBaoCao numeric(1, 0),
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[NamQuyetToan],
	[LoaiHinhBaoCao],
	[DVTBaoCao],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@NamQuyetToan,
	@LoaiHinhBaoCao,
	@DVTBaoCao,
	@GhiChuKhac,
	@FileName,
	@Content,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@NamQuyetToan numeric(4, 0),
	@LoaiHinhBaoCao numeric(1, 0),
	@DVTBaoCao numeric(1, 0),
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[NamQuyetToan] = @NamQuyetToan,
	[LoaiHinhBaoCao] = @LoaiHinhBaoCao,
	[DVTBaoCao] = @DVTBaoCao,
	[GhiChuKhac] = @GhiChuKhac,
	[FileName] = @FileName,
	[Content] = @Content,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@NamQuyetToan numeric(4, 0),
	@LoaiHinhBaoCao numeric(1, 0),
	@DVTBaoCao numeric(1, 0),
	@GhiChuKhac nvarchar(2000),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[NamQuyetToan] = @NamQuyetToan,
			[LoaiHinhBaoCao] = @LoaiHinhBaoCao,
			[DVTBaoCao] = @DVTBaoCao,
			[GhiChuKhac] = @GhiChuKhac,
			[FileName] = @FileName,
			[Content] = @Content,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[NamQuyetToan],
			[LoaiHinhBaoCao],
			[DVTBaoCao],
			[GhiChuKhac],
			[FileName],
			[Content],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@NamQuyetToan,
			@LoaiHinhBaoCao,
			@DVTBaoCao,
			@GhiChuKhac,
			@FileName,
			@Content,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[NamQuyetToan],
	[LoaiHinhBaoCao],
	[DVTBaoCao],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[NamQuyetToan],
	[LoaiHinhBaoCao],
	[DVTBaoCao],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[NamQuyetToan],
	[LoaiHinhBaoCao],
	[DVTBaoCao],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectBy_GoodItem_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectBy_GoodItem_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteBy_GoodItem_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteBy_GoodItem_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Insert]
	@GoodItem_ID bigint,
	@STT numeric(5, 0),
	@LoaiHangHoa numeric(5, 0),
	@TaiKhoan numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@TonDauKy numeric(18, 4),
	@NhapTrongKy numeric(18, 4),
	@XuatTrongKy numeric(18, 4),
	@TonCuoiKy numeric(18, 4),
	@GhiChu nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
(
	[GoodItem_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
)
VALUES 
(
	@GoodItem_ID,
	@STT,
	@LoaiHangHoa,
	@TaiKhoan,
	@TenHangHoa,
	@MaHangHoa,
	@DVT,
	@TonDauKy,
	@NhapTrongKy,
	@XuatTrongKy,
	@TonCuoiKy,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Update]
	@ID bigint,
	@GoodItem_ID bigint,
	@STT numeric(5, 0),
	@LoaiHangHoa numeric(5, 0),
	@TaiKhoan numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@TonDauKy numeric(18, 4),
	@NhapTrongKy numeric(18, 4),
	@XuatTrongKy numeric(18, 4),
	@TonCuoiKy numeric(18, 4),
	@GhiChu nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
SET
	[GoodItem_ID] = @GoodItem_ID,
	[STT] = @STT,
	[LoaiHangHoa] = @LoaiHangHoa,
	[TaiKhoan] = @TaiKhoan,
	[TenHangHoa] = @TenHangHoa,
	[MaHangHoa] = @MaHangHoa,
	[DVT] = @DVT,
	[TonDauKy] = @TonDauKy,
	[NhapTrongKy] = @NhapTrongKy,
	[XuatTrongKy] = @XuatTrongKy,
	[TonCuoiKy] = @TonCuoiKy,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_InsertUpdate]
	@ID bigint,
	@GoodItem_ID bigint,
	@STT numeric(5, 0),
	@LoaiHangHoa numeric(5, 0),
	@TaiKhoan numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@TonDauKy numeric(18, 4),
	@NhapTrongKy numeric(18, 4),
	@XuatTrongKy numeric(18, 4),
	@TonCuoiKy numeric(18, 4),
	@GhiChu nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details] 
		SET
			[GoodItem_ID] = @GoodItem_ID,
			[STT] = @STT,
			[LoaiHangHoa] = @LoaiHangHoa,
			[TaiKhoan] = @TaiKhoan,
			[TenHangHoa] = @TenHangHoa,
			[MaHangHoa] = @MaHangHoa,
			[DVT] = @DVT,
			[TonDauKy] = @TonDauKy,
			[NhapTrongKy] = @NhapTrongKy,
			[XuatTrongKy] = @XuatTrongKy,
			[TonCuoiKy] = @TonCuoiKy,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
		(
			[GoodItem_ID],
			[STT],
			[LoaiHangHoa],
			[TaiKhoan],
			[TenHangHoa],
			[MaHangHoa],
			[DVT],
			[TonDauKy],
			[NhapTrongKy],
			[XuatTrongKy],
			[TonCuoiKy],
			[GhiChu]
		)
		VALUES 
		(
			@GoodItem_ID,
			@STT,
			@LoaiHangHoa,
			@TaiKhoan,
			@TenHangHoa,
			@MaHangHoa,
			@DVT,
			@TonDauKy,
			@NhapTrongKy,
			@XuatTrongKy,
			@TonCuoiKy,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteBy_GoodItem_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteBy_GoodItem_ID]
	@GoodItem_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
WHERE
	[GoodItem_ID] = @GoodItem_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItem_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectBy_GoodItem_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectBy_GoodItem_ID]
	@GoodItem_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItem_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
WHERE
	[GoodItem_ID] = @GoodItem_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GoodItem_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItem_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]	

GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectBy_GoodItem_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectBy_GoodItem_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteBy_GoodItem_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteBy_GoodItem_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Insert]
	@GoodItem_ID bigint,
	@STT numeric(5, 0),
	@LoaiHangHoa numeric(5, 0),
	@TaiKhoan numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@TonDauKy numeric(18, 4),
	@NhapTrongKy numeric(18, 4),
	@XuatTrongKy numeric(18, 4),
	@TonCuoiKy numeric(18, 4),
	@GhiChu nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
(
	[GoodItem_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
)
VALUES 
(
	@GoodItem_ID,
	@STT,
	@LoaiHangHoa,
	@TaiKhoan,
	@TenHangHoa,
	@MaHangHoa,
	@DVT,
	@TonDauKy,
	@NhapTrongKy,
	@XuatTrongKy,
	@TonCuoiKy,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Update]
	@ID bigint,
	@GoodItem_ID bigint,
	@STT numeric(5, 0),
	@LoaiHangHoa numeric(5, 0),
	@TaiKhoan numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@TonDauKy numeric(18, 4),
	@NhapTrongKy numeric(18, 4),
	@XuatTrongKy numeric(18, 4),
	@TonCuoiKy numeric(18, 4),
	@GhiChu nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
SET
	[GoodItem_ID] = @GoodItem_ID,
	[STT] = @STT,
	[LoaiHangHoa] = @LoaiHangHoa,
	[TaiKhoan] = @TaiKhoan,
	[TenHangHoa] = @TenHangHoa,
	[MaHangHoa] = @MaHangHoa,
	[DVT] = @DVT,
	[TonDauKy] = @TonDauKy,
	[NhapTrongKy] = @NhapTrongKy,
	[XuatTrongKy] = @XuatTrongKy,
	[TonCuoiKy] = @TonCuoiKy,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_InsertUpdate]
	@ID bigint,
	@GoodItem_ID bigint,
	@STT numeric(5, 0),
	@LoaiHangHoa numeric(5, 0),
	@TaiKhoan numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@TonDauKy numeric(18, 4),
	@NhapTrongKy numeric(18, 4),
	@XuatTrongKy numeric(18, 4),
	@TonCuoiKy numeric(18, 4),
	@GhiChu nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details] 
		SET
			[GoodItem_ID] = @GoodItem_ID,
			[STT] = @STT,
			[LoaiHangHoa] = @LoaiHangHoa,
			[TaiKhoan] = @TaiKhoan,
			[TenHangHoa] = @TenHangHoa,
			[MaHangHoa] = @MaHangHoa,
			[DVT] = @DVT,
			[TonDauKy] = @TonDauKy,
			[NhapTrongKy] = @NhapTrongKy,
			[XuatTrongKy] = @XuatTrongKy,
			[TonCuoiKy] = @TonCuoiKy,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
		(
			[GoodItem_ID],
			[STT],
			[LoaiHangHoa],
			[TaiKhoan],
			[TenHangHoa],
			[MaHangHoa],
			[DVT],
			[TonDauKy],
			[NhapTrongKy],
			[XuatTrongKy],
			[TonCuoiKy],
			[GhiChu]
		)
		VALUES 
		(
			@GoodItem_ID,
			@STT,
			@LoaiHangHoa,
			@TaiKhoan,
			@TenHangHoa,
			@MaHangHoa,
			@DVT,
			@TonDauKy,
			@NhapTrongKy,
			@XuatTrongKy,
			@TonCuoiKy,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteBy_GoodItem_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteBy_GoodItem_ID]
	@GoodItem_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
WHERE
	[GoodItem_ID] = @GoodItem_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItem_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectBy_GoodItem_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectBy_GoodItem_ID]
	@GoodItem_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItem_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
WHERE
	[GoodItem_ID] = @GoodItem_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GoodItem_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 10, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItem_ID],
	[STT],
	[LoaiHangHoa],
	[TaiKhoan],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '34.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('34.5',GETDATE(), N'CẬP NHẬT KHAI BÁO BÁO CÁO QUYẾT TOÁN VÀ THÔNG BÁO CƠ SỞ SẢN XUẤT')
END