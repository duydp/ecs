﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.Maper;

namespace Company.Interface
{
    public partial class VNACC_ToKhaiMauDichXuatForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        KDT_VNACC_TK_SoVanDon vd = new KDT_VNACC_TK_SoVanDon();

        public VNACC_ToKhaiMauDichXuatForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.EDA.ToString());
            BoSungNghiepVu();
        }

        private void VNACC_ToKhaiMauDichXuatForm_Load(object sender, EventArgs e)
        {
            this.SetEDControl();
            if (TKMD != null)
            {
                SetToKhai();
                cmbMain.Commands["cmdChiThiHQ"].Visible = Janus.Windows.UI.InheritableBoolean.True;
                setCommandStatus();
            }
            LoadGrid();
            ctrDiaDiemXepHang.CategoryType = ECategory.A620;

            ValidateControl(true);

            SetAutoRemoveUnicodeAndUpperCaseControl();
        }

        private void LoadGrid()
        {
            grListTyGia.DataSource = TKMD.TyGiaCollection;
            grListTyGia.Refresh();
        }

        private void GetToKhai()
        {
            errorProvider.Clear();

            //TKMD.SoToKhai = Convert.ToDecimal(txtSoToKhai.Value);
            //TKMD.SoToKhaiDauTien = Convert.ToDecimal(txtSoToKhaiDauTien.Text);
            //TKMD.SoNhanhToKhai = Convert.ToDecimal(txtSoNhanhToKhai.Value);
            //TKMD.TongSoTKChiaNho = Convert.ToDecimal(txtTongSoTKChiaNho.Value);
            TKMD.SoToKhaiTNTX = Convert.ToDecimal(txtSoToKhaiTNTX.Value);
            TKMD.MaLoaiHinh = ctrMaLoaiHinh.Code;
            TKMD.MaPhanLoaiHH = ctrMaPhanLoaiHH.Code;
            TKMD.CoQuanHaiQuan = ctrCoQuanHaiQuan.Code;
            TKMD.NhomXuLyHS = "01"; //ctrNhomXuLyHS.Code;
            TKMD.ThoiHanTaiNhapTaiXuat = clcThoiHanTaiNhap.Value;
            TKMD.NgayDangKy = clcNgayDangKy.Value;
            TKMD.MaPhuongThucVT = ctrMaPhuongThucVT.Code;
            //don vi
            TKMD.MaDonVi = txtMaDonVi.Text;
            TKMD.TenDonVi = txtTenDonVi.Text;
            TKMD.MaBuuChinhDonVi = txtMaBuuChinhDonVi.Text;
            TKMD.DiaChiDonVi = txtDiaChiDonVi.Text;
            TKMD.SoDienThoaiDonVi = txtSoDienThoaiDonVi.Text;
            TKMD.MaUyThac = txtMaUyThac.Text;
            TKMD.TenUyThac = txtTenUyThac.Text;
            // doi tac
            TKMD.MaDoiTac = txtMaDoiTac.Text;
            TKMD.TenDoiTac = txtTenDoiTac.Text;
            TKMD.MaBuuChinhDoiTac = txtMaBuuChinhDoiTac.Text;
            TKMD.DiaChiDoiTac1 = txtDiaChiDoiTac1.Text;
            TKMD.DiaChiDoiTac2 = txtDiaChiDoiTac2.Text;
            TKMD.DiaChiDoiTac3 = txtDiaChiDoiTac3.Text;
            TKMD.DiaChiDoiTac4 = txtDiaChiDoiTac4.Text;
            //uy thac
            TKMD.MaNuocDoiTac = ctrMaNuocDoiTac.Code;
            TKMD.MaDaiLyHQ = txtMaDaiLyHQ.Text;
            //so kien va trong luong
            TKMD.SoLuong = Convert.ToDecimal(txtSoLuong.Value);
            TKMD.MaDVTSoLuong = ctrMaDVTSoLuong.Code;
            TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Value);
            TKMD.MaDVTTrongLuong = ctrMaDVTTrongLuong.Code;
            // van don
            TKMD.MaDDLuuKho = ctrMaDDLuuKho.Code;
            TKMD.SoHieuKyHieu = txtSoHieuKyHieu.Text;
            TKMD.MaPTVC = txtMaPTVC.Text;
            TKMD.TenPTVC = txtTenPTVC.Text;
            TKMD.NgayHangDen = clcNgayHangDen.Value;
            TKMD.MaDiaDiemDoHang = ctrDiaDiemNhanHang.Code;
            TKMD.TenDiaDiemDohang = ctrDiaDiemNhanHang.Name_VN;
            TKMD.MaDiaDiemXepHang = ctrDiaDiemXepHang.Code;
            TKMD.TenDiaDiemXepHang = ctrDiaDiemXepHang.Name_VN;
            //hoa don
            TKMD.PhanLoaiHD = ctrPhanLoaiHD.Code;
            TKMD.SoTiepNhanHD = Convert.ToDecimal(txtSoTiepNhanHD.Value);
            TKMD.SoHoaDon = txtSoHoaDon.Text;
            TKMD.NgayPhatHanhHD = clcNgayPhatHanhHD.Value;
            TKMD.PhuongThucTT = ctrPhuongThucTT.Code;
            TKMD.PhanLoaiGiaHD = ctrPhanLoaiGiaHD.Code;
            TKMD.MaDieuKienGiaHD = ctrMaDieuKienGiaHD.Code;
            TKMD.TongTriGiaHD = Convert.ToDecimal(txtTongTriGiaHD.Value);
            TKMD.MaTTHoaDon = ctrMaTTHoaDon.Code;
            //tri gia tinh thue
            TKMD.TriGiaTinhThue = Convert.ToDecimal(txtTriGiaTinhThue.Value);
            TKMD.MaTTTriGiaTinhThue = ctrMaTTTriGiaTinhThue.Code;
            TKMD.PhanLoaiKhongQDVND = txtPhanLoaiKhongQDVND.Text;
            //han muc
            TKMD.NguoiNopThue = ctrNguoiNopThue.Code;
            TKMD.MaNHTraThueThay = txtMaNHTraThueThay.Text;
            TKMD.NamPhatHanhHM = Convert.ToDecimal(txtNanPhatHanhHM.Value);
            TKMD.KyHieuCTHanMuc = txtKyHieuCTHanMuc.Text;
            TKMD.SoCTHanMuc = txtSoCTHanMuc.Text;
            //bao lanh thue
            TKMD.MaXDThoiHanNopThue = ctrMaXDThoiHanNopThue.Code;
            TKMD.MaNHBaoLanh = txtMaNHBaoLanh.Text;
            TKMD.NamPhatHanhBL = Convert.ToDecimal(txtNamPhatHanhBL.Value);
            TKMD.KyHieuCTBaoLanh = txtKyHieuCTBaoLanh.Text;
            TKMD.SoCTBaoLanh = txtSoCTBaoLanh.Text;
            //
            TKMD.NgayKhoiHanhVC = clcNgayKhoiHanhVC.Value;
            TKMD.DiaDiemDichVC = ctrDiaDiemDichVC.Code;
            TKMD.NgayDen = clcNgayDen.Value;
            TKMD.GhiChu = txtGhiChu.Text;
            TKMD.SoQuanLyNoiBoDN = txtSoQuanLyNoiBoDN.Text;
            //Van don
            if (txtSoVanDon.Text.Trim() != "")
            {
                if (TKMD.VanDonCollection.Count == 0)
                    TKMD.VanDonCollection.Add(vd);
                TKMD.VanDonCollection[0].SoTT = 1;
                TKMD.VanDonCollection[0].SoVanDon = txtSoVanDon.Text;
            }
            else
            {
                if (TKMD.VanDonCollection.Count > 0)
                {
                    TKMD.VanDonCollection[0].Delete();
                    TKMD.VanDonCollection.Clear();
                }
            }
        }

        private void SetToKhai()
        {
            errorProvider.Clear();

            txtSoToKhai.Text = Convert.ToString(TKMD.SoToKhai);
            txtSoToKhaiDauTien.Text = TKMD.SoToKhaiDauTien.ToString();
            txtSoNhanhToKhai.Text = Convert.ToString(TKMD.SoNhanhToKhai);
            txtTongSoTKChiaNho.Text = Convert.ToString(TKMD.TongSoTKChiaNho);
            txtSoToKhaiTNTX.Text = Convert.ToString(TKMD.SoToKhaiTNTX);
            ctrMaLoaiHinh.Code = TKMD.MaLoaiHinh;
            ctrMaPhanLoaiHH.Code = TKMD.MaPhanLoaiHH;
            ctrCoQuanHaiQuan.Code = TKMD.CoQuanHaiQuan;
            //Phan luong
            if (string.IsNullOrEmpty(TKMD.MaPhanLoaiKiemTra))
            {
                lblPhanLuong.Text = string.Empty;
            }
            else
            {
                string pl = TKMD.MaPhanLoaiKiemTra.Trim();
                if (pl == "1")
                {
                    lblPhanLuong.Text = "Luồng Xanh";
                    this.lblPhanLuong.ForeColor = System.Drawing.Color.Green;
                }
                if (pl == "2")
                {
                    lblPhanLuong.Text = "Luồng Vàng";
                    this.lblPhanLuong.ForeColor = System.Drawing.Color.Yellow;
                }
                if (pl == "3")
                {
                    lblPhanLuong.Text = "Luồng Đỏ";
                    this.lblPhanLuong.ForeColor = System.Drawing.Color.Red;
                }
            }
            ctrNhomXuLyHS.Code = TKMD.NhomXuLyHS;
            clcThoiHanTaiNhap.Value = TKMD.ThoiHanTaiNhapTaiXuat;
            clcNgayDangKy.Value = TKMD.NgayDangKy;
            ctrMaPhuongThucVT.Code = TKMD.MaPhuongThucVT;
            //Don vi
            txtMaDonVi.Text = TKMD.MaDonVi;
            txtTenDonVi.Text = TKMD.TenDonVi;
            txtMaBuuChinhDonVi.Text = TKMD.MaBuuChinhDonVi;
            txtDiaChiDonVi.Text = TKMD.DiaChiDonVi;
            txtSoDienThoaiDonVi.Text = TKMD.SoDienThoaiDonVi;
            //uy thac
            txtMaUyThac.Text = TKMD.MaUyThac;
            txtTenUyThac.Text = TKMD.TenUyThac;
            //doi tac
            txtMaDoiTac.Text = TKMD.MaDoiTac;
            txtTenDoiTac.Text = TKMD.TenDoiTac;
            txtMaBuuChinhDoiTac.Text = TKMD.MaBuuChinhDoiTac;
            txtDiaChiDoiTac1.Text = TKMD.DiaChiDoiTac1;
            txtDiaChiDoiTac2.Text = TKMD.DiaChiDoiTac2;
            txtDiaChiDoiTac3.Text = TKMD.DiaChiDoiTac3;
            txtDiaChiDoiTac4.Text = TKMD.DiaChiDoiTac4;
            ctrMaNuocDoiTac.Code = TKMD.MaNuocDoiTac;
            txtMaDaiLyHQ.Text = TKMD.MaDaiLyHQ;
            txtTenDaiLyHaiQuan.Text = TKMD.TenDaiLyHaiQuan;
            //so kien va trong luong
            txtSoLuong.Text = Convert.ToString(TKMD.SoLuong);
            ctrMaDVTSoLuong.Code = TKMD.MaDVTSoLuong;
            txtTrongLuong.Text = Convert.ToString(TKMD.TrongLuong);
            ctrMaDVTTrongLuong.Code = TKMD.MaDVTTrongLuong;
            //van don
            ctrMaDDLuuKho.Code = TKMD.MaDDLuuKho;
            txtSoHieuKyHieu.Text = TKMD.SoHieuKyHieu;
            txtMaPTVC.Text = TKMD.MaPTVC;
            txtTenPTVC.Text = TKMD.TenPTVC;
            clcNgayHangDen.Value = TKMD.NgayHangDen;
            ctrDiaDiemNhanHang.Code = TKMD.MaDiaDiemDoHang;

            ctrDiaDiemXepHang.Code = TKMD.MaDiaDiemXepHang;

            //hoa don
            ctrPhanLoaiHD.Code = TKMD.PhanLoaiHD;
            txtSoTiepNhanHD.Text = Convert.ToString(TKMD.SoTiepNhanHD);
            txtSoHoaDon.Text = TKMD.SoHoaDon;
            clcNgayPhatHanhHD.Value = TKMD.NgayPhatHanhHD;
            ctrPhuongThucTT.Code = TKMD.PhuongThucTT;
            ctrPhanLoaiGiaHD.Code = TKMD.PhanLoaiGiaHD;
            ctrMaDieuKienGiaHD.Code = TKMD.MaDieuKienGiaHD;
            ctrMaTTHoaDon.Code = TKMD.MaTTHoaDon;
            txtTongTriGiaHD.Text = Convert.ToString(TKMD.TongTriGiaHD);
            txtTriGiaTinhThue.Text = Convert.ToString(TKMD.TriGiaTinhThue);
            txtPhanLoaiKhongQDVND.Text = TKMD.PhanLoaiKhongQDVND;
            ctrMaTTTriGiaTinhThue.Code = TKMD.MaTTTriGiaTinhThue;
            txtTongHeSoPhanBoTG.Text = TKMD.TongHeSoPhanBoTG.ToString();
            lblMaPhanLoaiTongGiaCoBan.Text = TKMD.MaPhanLoaiDieuChinhTriGia;
            //han muc
            ctrNguoiNopThue.Code = TKMD.NguoiNopThue;
            txtMaNHTraThueThay.Text = TKMD.MaNHTraThueThay;
            txtNanPhatHanhHM.Text = Convert.ToString(TKMD.NamPhatHanhHM);
            txtKyHieuCTHanMuc.Text = TKMD.KyHieuCTHanMuc;
            txtSoCTHanMuc.Text = TKMD.SoCTHanMuc;
            //bao lanh thue
            ctrMaXDThoiHanNopThue.Code = TKMD.MaXDThoiHanNopThue;
            txtMaNHBaoLanh.Text = TKMD.MaNHBaoLanh;
            txtNamPhatHanhBL.Text = Convert.ToString(TKMD.NamPhatHanhBL);
            txtKyHieuCTBaoLanh.Text = TKMD.KyHieuCTBaoLanh;
            txtSoCTBaoLanh.Text = TKMD.SoCTBaoLanh;
            //----
            clcNgayKhoiHanhVC.Value = TKMD.NgayKhoiHanhVC;
            ctrDiaDiemDichVC.Code = TKMD.DiaDiemDichVC;
            clcNgayDen.Value = TKMD.NgayDen;
            txtGhiChu.Text = TKMD.GhiChu;
            txtSoQuanLyNoiBoDN.Text = TKMD.SoQuanLyNoiBoDN;
            //van don

            if (TKMD.VanDonCollection.Count == 1)
                txtSoVanDon.Text = TKMD.VanDonCollection[0].SoVanDon;
            // thông tin thue và lệ phi
            txtPhanLoaiNopThue.Text = TKMD.PhanLoaiNopThue;
            txtTongSoTienThueXuatKhau.Text = TKMD.TongSoTienThueXuatKhau.ToString();
            ctrMaTTTongTienThueXuatKhau.Code = TKMD.MaTTTongTienThueXuatKhau;
            txtTongSoTienLePhi.Text = TKMD.TongSoTienLePhi.ToString();
            txtSoTienBaoLanh.Text = TKMD.SoTienBaoLanh.ToString();
            ctrMaTTCuaSoTienBaoLanh.Code = TKMD.MaTTCuaSoTienBaoLanh;
            txtTongSoDongHangCuaToKhai.Text = TKMD.TongSoDongHangCuaToKhai.ToString();
            txtTongSoTrangCuaToKhai.Text = TKMD.TongSoTrangCuaToKhai.ToString();

            LoadGrid();
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    showThemHang();
                    break;
                case "cmdChiThiHQ":
                    showChiThiHQ();
                    break;
                case "cmdGiayPhep":
                    showGiayPhep();
                    break;
                case "cmdDinhKemDT":
                    showDinhKem();
                    break;
                case "cmdTrungChuyen":
                    showTrungChuyen();
                    break;
                case "cmdContainer":
                    showContainer();
                    break;
                case "cmdLuu":
                    saveToKhai();
                    break;
                case "cmdEDA":
                    SendVnaccs(false);
                    break;
                case "cmdEDA01":
                    SendVnaccs(true);
                    break;
                case "cmdEDC":
                    SendVnaccsEDC(false);
                    break;
                case "cmdEDE":
                    SendVnaccsEDC(true);
                    break;
                case "cmdKetQuaTraVe":
                    ShowKetQuaTraVe();
                    break;
            }
        }

        private void showThemHang()
        {
            VNACC_HangMauDichXuatForm f = new VNACC_HangMauDichXuatForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
        }

        private void showChiThiHQ()
        {
            if (TKMD.SoToKhai != 0)
            {
                VNACC_ChiThiHaiQuanForm f = new VNACC_ChiThiHaiQuanForm();
                f.Master_ID = this.TKMD.ID;
                f.LoaiThongTin = ELoaiThongTin.TK;
                f.ShowDialog();
            }
        }

        private void showGiayPhep()
        {
            VNACC_TK_GiayPhepForm f = new VNACC_TK_GiayPhepForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
        }

        private void showDinhKem()
        {
            VNACC_TK_DinhKiemDTForm f = new VNACC_TK_DinhKiemDTForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
        }

        private void showTrungChuyen()
        {
            VNACC_TK_TrungChuyenForm f = new VNACC_TK_TrungChuyenForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();

        }

        private void showContainer()
        {
            if (TKMD.ID != 0)
            {
                VNACC_TK_Container f = new VNACC_TK_Container();
                f.TKMD = this.TKMD;
                f.ShowDialog();
            }
            else
                ShowMessage("Lưu tờ khai trước thêm Container", false);
        }

        private void saveToKhai()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (!ValidateControl(false))
                    return;

                GetToKhai();

                TKMD.InsertUpdateFull();
                ShowMessage("Lưu tờ khai thành công", false);
                LoadGrid();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

        #region Bổ sung nghiệp vụ

        private Janus.Windows.UI.CommandBars.UICommand cmdEDA;
        private Janus.Windows.UI.CommandBars.UICommand cmdEDB;
        private Janus.Windows.UI.CommandBars.UICommand cmdEDC;
        private Janus.Windows.UI.CommandBars.UICommand cmdIEX;
        private Janus.Windows.UI.CommandBars.UICommand cmdEDA01;
        private Janus.Windows.UI.CommandBars.UICommand cmdEDD;
        private Janus.Windows.UI.CommandBars.UICommand cmdEDE;
        /* private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaTraVe;*/
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBaoSua;
        private Janus.Windows.UI.CommandBars.UICommand Separator;
        private Janus.Windows.UI.CommandBars.UICommand Separator2;
        private Janus.Windows.UI.CommandBars.UICommand Separator3;
        private void BoSungNghiepVu()
        {


            this.cmdEDA = new Janus.Windows.UI.CommandBars.UICommand("cmdEDA");
            this.cmdEDA.Key = "cmdEDA";
            this.cmdEDA.Name = "cmdEDA";
            this.cmdEDA.Text = "nghiệp vụ EDA (Khai báo tờ khai tạm)";

            this.cmdEDB = new Janus.Windows.UI.CommandBars.UICommand("cmdEDB");
            this.cmdEDB.Key = "cmdEDB";
            this.cmdEDB.Name = "cmdEDB";
            this.cmdEDB.Text = "nghiệp vụ EDB (Gọi tờ khai tạm) ";

            this.cmdEDC = new Janus.Windows.UI.CommandBars.UICommand("cmdEDC");
            this.cmdEDC.Key = "cmdEDC";
            this.cmdEDC.Name = "cmdEDC";
            this.cmdEDC.Text = "Nghiệp vụ EDC (Khai báo chính thức tờ khai)";

            this.cmdEDA01 = new Janus.Windows.UI.CommandBars.UICommand("cmdEDA01");
            this.cmdEDA01.Key = "cmdEDA01";
            this.cmdEDA01.Name = "cmdEDA01";
            this.cmdEDA01.Text = "nghiệp vụ EDA01 (khai báo sửa tạm)";

            this.cmdEDD = new Janus.Windows.UI.CommandBars.UICommand("cmdEDD");
            this.cmdEDD.Key = "cmdEDD";
            this.cmdEDD.Name = "cmdEDD";
            this.cmdEDD.Text = "nghiệp vụ EDD (Gọi tờ khai sửa) ";

            this.cmdIEX = new Janus.Windows.UI.CommandBars.UICommand("cmdIEX");
            this.cmdIEX.Key = "cmdIEX";
            this.cmdIEX.Name = "cmdIEX";
            this.cmdIEX.Text = "nghiệp vụ IEX (tham chiếu tờ khai) ";

            this.cmdEDE = new Janus.Windows.UI.CommandBars.UICommand("cmdEDE");
            this.cmdEDE.Key = "cmdEDE";
            this.cmdEDE.Name = "cmdEDE";
            this.cmdEDE.Text = "nghiệp vụ EDE (Xác nhận sửa tờ khai) ";

            //             this.cmdKetQuaTraVe = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaTraVe");
            //             this.cmdKetQuaTraVe.Key = "cmdKetQuaTraVe";
            //             this.cmdKetQuaTraVe.Name = "cmdKetQuaTraVe";
            //             this.cmdKetQuaTraVe.Text = "Thông tin từ HQ";
            // 
            //             this.cmbMain.Commands.Add(cmdKetQuaTraVe);

            this.cmdKhaiBaoSua = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBaoSua");
            this.cmdKhaiBaoSua.Key = "cmdKhaiBaoSua";
            this.cmdKhaiBaoSua.Name = "cmdKhaiBaoSua";
            this.cmdKhaiBaoSua.Text = "Sửa TK Trong thông quan";

            this.Separator = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            Separator.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;

            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator2");
            Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;

            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator3");
            Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.cmdKhaiBaoSua.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
            {
                this.cmdEDA01,
                this.cmdEDE,
                this.cmdEDD
                ,});


            /*this.cmd.Text = "Cập nhật thông tin ";*/
            this.cmdKhaiBao1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
            {
                this.cmdEDA,
                this.cmdEDB,
                this.Separator,
                this.cmdEDC,
                Separator2,
                this.cmdKhaiBaoSua,
                Separator3,
                this.cmdIEX
            });
        }

        #endregion Bổ sung nghiệp vụ

        #region Send VNACCS
        private void SendVnaccs(bool KhaiBaoSua)
        {
            try
            {
                if (TKMD.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {
                    EDA EDA = VNACCMaperFromObject.EDAMapper(TKMD);
                    if (EDA == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        TKMD.InputMessageID = HelperVNACCS.NewInputMSGID();
                        TKMD.InsertUpdateFull();
                        msg = MessagesSend.Load<EDA>(EDA, TKMD.InputMessageID);

                    }
                    else
                    {
                        msg = MessagesSend.Load<EDA>(EDA, true, EnumNghiepVu.EDA01, TKMD.InputMessageID);
                    }

                    MsgLog.SaveMessages(msg, TKMD.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = false;
                    f.inputMSGID = TKMD.InputMessageID;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.Cancel)
                    {
                        this.ShowMessage(f.msgFeedBack, false);
                    }
                    else if (f.DialogResult == DialogResult.No)
                    {
                    }
                    else
                    {
                        string ketqua = "Khai báo thông tin thành công";
                        CapNhatThongTin(f.feedback.ResponseData);
                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                decimal SoToKhai = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                ketqua = ketqua + Environment.NewLine;
                                ketqua += "Số tờ khai: " + SoToKhai;
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                        this.ShowMessageTQDT(ketqua, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }
        private void SendVnaccsEDC(bool KhaiBaoSua)
        {
            try
            {
                if (TKMD.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {

                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        EDC EDC = VNACCMaperFromObject.EDCMapper(TKMD.SoToKhai, string.Empty);
                        if (EDC == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                        msg = MessagesSend.Load<EDC>(EDC, TKMD.InputMessageID);
                    }
                    else
                    {
                        EDE EDE = VNACCMaperFromObject.EDEMapper(TKMD.SoToKhai, string.Empty);
                        if (EDE == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                        msg = MessagesSend.Load<EDE>(EDE, TKMD.InputMessageID);
                    }

                    MsgLog.SaveMessages(msg, TKMD.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = true;
                    f.inputMSGID = TKMD.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result)
                    {
                        TuDongCapNhatThongTin();
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }
        private void GetIEX()
        {
            try
            {
                if (TKMD.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {
                    IID IEX = VNACCMaperFromObject.IIDMapper(TKMD.SoToKhai);
                    if (IEX == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    MessagesSend msg;
                    msg = MessagesSend.Load<IID>(IEX, TKMD.InputMessageID);

                    MsgLog.SaveMessages(msg, TKMD.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = false;
                    f.inputMSGID = TKMD.InputMessageID;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.Cancel)
                    {
                        this.ShowMessage(f.msgFeedBack, false);
                    }
                    else if (f.DialogResult == DialogResult.No)
                    {
                    }
                    else
                    {
                        TKMD.InputMessageID = msg.Header.InputMessagesID.GetValue().ToString();
                        if (f.feedback.Error == null || f.feedback.Error.Count == 0)
                        {
                            if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
                            {
                                CapNhatThongTin(f.feedback.ResponseData);
                                f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
                            }
                        }
                        this.ShowMessageTQDT(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }
        #endregion

        #region Cập nhật thông tin
        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            ProcessMessages.GetDataResult_TKMD(msgResult, "", TKMD);
            TKMD.InsertUpdateFull();
            SetToKhai();
            setCommandStatus();
        }
        private void TuDongCapNhatThongTin()
        {
            if (TKMD != null && TKMD.SoToKhai > 0 && TKMD.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}' AND MessagesInputID = '{1}'", TKMD.SoToKhai.ToString(), TKMD.InputMessageID), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        //msgPb.Delete();
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }
        #endregion

        private void ShowKetQuaTraVe()
        {
            SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
            f.isSend = false;
            f.isRep = true;
            f.inputMSGID = TKMD.InputMessageID;
            f.ShowDialog(this);
        }

        private void setCommandStatus()
        {
            if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoSua.Visible = cmdEDC.Visible = cmdEDE.Visible = cmdEDA01.Visible = cmdEDD.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdEDA.Visible = cmdEDB.Visible = Janus.Windows.UI.InheritableBoolean.True;

            }
            else if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoSua.Visible = cmdEDA.Visible = cmdEDE.Visible = cmdEDA01.Visible = cmdEDD.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdEDC.Visible = cmdEDB.Visible = Janus.Windows.UI.InheritableBoolean.True;
            }
            else if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEDA.Visible = cmdEDB.Visible = cmdEDC.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBaoSua.Visible = cmdEDA01.Visible = cmdEDE.Visible = cmdEDD.Visible = Janus.Windows.UI.InheritableBoolean.True; cmdKhaiBaoSua.Commands.Clear();
                this.cmdKhaiBaoSua.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
            {
                this.cmdEDA01,
                this.cmdEDE,
                this.cmdEDD
                ,});
            }
            else if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEDA01.Visible = cmdEDE.Visible = cmdEDD.Visible = cmdEDA.Visible = cmdEDB.Visible = cmdEDC.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBaoSua.Visible = cmdIEX.Visible = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoSua.Commands.Clear();
            }
            else if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
            {
                cmdKhaiBaoSua.Visible = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            if (string.IsNullOrEmpty(TKMD.InputMessageID))
                cmdKetQuaTraVe1.Visible = cmdKetQuaTraVe2.Visible = Janus.Windows.UI.InheritableBoolean.False;
            else
                cmdKetQuaTraVe1.Visible = cmdKetQuaTraVe2.Visible = Janus.Windows.UI.InheritableBoolean.True;

        }

        private bool SetEDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtSoToKhai.Tag = "ECN"; //Số tờ khai
                txtSoToKhaiDauTien.Tag = "FIC"; //Số tờ khai đầu tiên
                txtSoNhanhToKhai.Tag = "BNO"; //Số nhánh của tờ khai chia nhỏ
                txtTongSoTKChiaNho.Tag = "DNO"; //Tổng số tờ khai chia nhỏ
                txtSoToKhaiTNTX.Tag = "TDN"; //Số tờ khai tạm nhập tái xuất tương ứng
                ctrMaLoaiHinh.TagName = "ECB"; //Mã loại hình
                ctrMaPhanLoaiHH.TagName = "CCC"; //Mã phân loại hàng hóa
                ctrMaPhuongThucVT.TagName = "MTC"; //Mã hiệu phương thức vận chuyển
                clcThoiHanTaiNhapTaiXuat.TagName = "RID"; //Thời hạn tái nhập khẩu
                ctrCoQuanHaiQuan.TagCode = "CH"; //Cơ quan Hải quan
                ctrNhomXuLyHS.TagName = "CHB"; //Nhóm xử lý hồ sơ
                clcNgayDangKy.TagName = "ECD"; //Ngày khai báo (Dự kiến)
                txtMaDonVi.Tag = "EPC"; //Mã người xuất khẩu
                txtTenDonVi.Tag = "EPN"; //Tên người xuất khẩu
                txtMaBuuChinhDonVi.Tag = "EPP"; //Mã bưu chính
                txtDiaChiDonVi.Tag = "EPA"; //Địa chỉ người xuất khẩu
                txtSoDienThoaiDonVi.Tag = "EPT"; //Số điện thoại người xuất khẩu
                txtMaUyThac.Tag = "EXC"; //Mã người ủy thác xuất khẩu
                txtTenUyThac.Tag = "EXN"; //Tên người ủy thác xuất khẩu
                txtMaDoiTac.Tag = "CGC"; //Mã người nhập khẩu
                txtTenDoiTac.Tag = "CGN"; //Tên người nhập khẩu
                txtMaBuuChinhDoiTac.Tag = "CGP"; //Mã bưu chính
                txtDiaChiDoiTac1.Tag = "CGA"; //Địa chỉ 1(Street and number/P.O.BOX)
                txtDiaChiDoiTac2.Tag = "CAT"; //Địa chỉ 2(Street and number/P.O.BOX)
                txtDiaChiDoiTac3.Tag = "CAC"; //Địa chỉ 3(City name)
                txtDiaChiDoiTac4.Tag = "CAS"; //Địa chỉ 4(Country sub-entity, name)
                ctrMaNuocDoiTac.TagName = "CGK"; //Mã nước(Country, coded)
                txtMaDaiLyHQ.Tag = "ECC"; //Mã đại lý Hải quan

                txtSoLuong.Tag = "NO"; //Số lượng
                ctrMaDVTSoLuong.TagName = "NOT"; //Mã đơn vị tính
                txtTrongLuong.Tag = "GW"; //Tổng trọng lượng hàng (Gross)
                ctrMaDVTTrongLuong.TagName = "GWT"; //Mã đơn vị tính trọng lượng (Gross)
                ctrMaDDLuuKho.TagName = "ST"; //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
                ctrDiaDiemNhanHang.TagCode = "DSC"; //Mã địa điểm nhận hàng cuối cùng
                ctrDiaDiemNhanHang.TagName = "DSN"; //Tên địa điểm nhận hàng cuối cùng
                ctrDiaDiemXepHang.TagCode = "PSC"; //Mã địa điểm xếp hàng
                ctrDiaDiemXepHang.TagName = "PSN"; //Tên địa điểm xếp hàng
                txtMaPTVC.Tag = "VSC"; //Mã phương tiện vận chuyển dự kiến
                txtTenPTVC.Tag = "VSN"; //Tên phương tiện vận chuyển dự kiến
                clcNgayHangDen.TagName = "SYM"; //Ngày hàng đi dự kiến
                txtSoHieuKyHieu.Tag = "MRK"; //Ký hiệu và số hiệu


                ctrPhanLoaiHD.TagName = "IV1"; //Phân loại hình thức hóa đơn
                txtSoTiepNhanHD.Tag = "IV2"; //Số tiếp nhận hóa đơn điện tử
                txtSoHoaDon.Tag = "IV3"; //Số hóa đơn
                clcNgayPhatHanhHD.TagName = "IVD"; //Ngày phát hành
                ctrPhuongThucTT.TagName = "IVP"; //Phương thức thanh toán
                ctrMaDieuKienGiaHD.TagName = "IP2"; //Mã điều kiện giá hóa đơn
                ctrMaTTHoaDon.TagName = "IP3"; //Mã đồng tiền của hóa đơn
                txtTongTriGiaHD.Tag = "IP4"; //Tổng trị giá hóa đơn
                ctrPhanLoaiGiaHD.TagName = "IP1"; //Mã phân loại giá hóa đơn
                ctrNguoiNopThue.TagName = "TPM"; //Người nộp thuế
                txtMaNHTraThueThay.Tag = "BRC"; //Mã ngân hàng trả thuế thay
                txtNanPhatHanhHM.Tag = "BYA"; //Năm phát hành hạn mức
                txtKyHieuCTHanMuc.Tag = "BCM"; //Kí hiệu chứng từ hạn mức
                txtSoCTHanMuc.Tag = "BCN"; //Số chứng từ hạn mức
                ctrMaXDThoiHanNopThue.TagName = "ENC"; //Mã xác định thời hạn nộp thuế
                txtMaNHBaoLanh.Tag = "SBC"; //Mã ngân hàng bảo lãnh
                txtNamPhatHanhBL.Tag = "RYA"; //Năm phát hành bảo lãnh 
                txtSoCTBaoLanh.Tag = "SCN"; //Số chứng từ bảo lãnh
                clcNgayKhoiHanhVC.TagName = "DPD"; //Ngày khởi hành vận chuyển
                ctrDiaDiemDichVC.Tag = "ARP"; //Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
                clcNgayDen.Tag = "ADT"; //Ngày dự kiến đến
                txtGhiChu.Tag = "NT2"; //Phần ghi chú
                txtSoQuanLyNoiBoDN.Tag = "REF"; //Số quản lý của nội bộ doanh nghiệp


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private void ctrMaPhuongThucVT_Leave(object sender, EventArgs e)
        {
            if (ctrMaPhuongThucVT.Code == "4")
                ctrDiaDiemXepHang.CategoryType = ECategory.A601;
            else if (ctrMaPhuongThucVT.Code == "5")
                ctrDiaDiemXepHang.CategoryType = ECategory.A620;
            else
                ctrDiaDiemXepHang.CategoryType = ECategory.A601;
            ctrDiaDiemXepHang.ReLoadData();
        }

        private bool ValidateControl(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                //ctrDiaDiemXepHang.SetValidate = true;
                //isValid &= ctrDiaDiemXepHang.IsValidate; //"Mã địa điểm xếp hàng");
                //ctrMaDVTSoLuong.SetValidate = true;
                //isValid &= ctrMaDVTSoLuong.IsValidate; //"Mã đơn vị tính");
                //ctrMaLoaiHinh.SetValidate = true;
                //isValid &= ctrMaLoaiHinh.IsValidate; //"Mã loại hình");
                //ctrMaPhuongThucVT.SetValidate = true;
                //isValid &= ctrMaPhuongThucVT.IsValidate; //"Mã hiệu phương thức vận chuyển");
                ////isValid &= Globals.ValidateNull(clcNgayHangDen, errorProvider, "Ngày hàng đến");
                //isValid &= Globals.ValidateNull(txtSoLuong, errorProvider, "Số lượng", isOnlyWarning);
                //ctrPhanLoaiHD.SetValidate = true;
                //isValid &= ctrPhanLoaiHD.IsValidate; //"Phân loại hình thức hóa đơn");
                ////ctrPhanLoaiToChuc.SetValidate = true;
                ////isValid &= ctrPhanLoaiToChuc.IsValidate; //"Phân loại cá nhân/tổ chức");

                //ctrDiaDiemNhanHang.SetValidate = true;
                //isValid &= ctrDiaDiemNhanHang.IsValidate; //"Mã địa điểm nhận hàng cuối cùng");
                //ctrMaDieuKienGiaHD.SetValidate = true;
                //isValid &= ctrMaDieuKienGiaHD.IsValidate; //"Mã điều kiện giá hóa đơn");
                ////isValid &= Globals.ValidateNull(txtMaSoHang, errorProvider, "Mã số hàng hóa");
                ////isValid &= Globals.ValidateNull(txtMaTTDonGia, errorProvider, "Mã đồng tiền của đơn giá hóa đơn");
                ////isValid &= Globals.ValidateNull(txtMaTTHoaDon, errorProvider, "Mã đồng tiền của hóa đơn");
                ////isValid &= Globals.ValidateNull(clcNgayHangDen, errorProvider, "Ngày hàng đi dự kiến");
                //ctrPhanLoaiGiaHD.SetValidate = true;
                //isValid &= ctrPhanLoaiGiaHD.IsValidate; //"Mã phân loại giá hóa đơn");
                //isValid &= Globals.ValidateNull(txtSoLuong, errorProvider, "Số lượng", isOnlyWarning);
                //isValid &= Globals.ValidateNull(txtTongTriGiaHD, errorProvider, "Tổng trị giá hóa đơn", isOnlyWarning);
                //isValid &= Globals.ValidateNull(txtTriGiaTinhThue, errorProvider, "Trị giá tính thuế", isOnlyWarning);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            //txtSoToKhaiDauTien.TextChanged += new EventHandler(SetTextChanged_Handler); //Số tờ khai đầu tiên
            ////ctrMaLoaiHinh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã loại hình
            ////ctrMaPhanLoaiHH.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại hàng hóa
            ////ctrMaPhuongThucVT.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã hiệu phương thức vận chuyển
            ////ctrPhanLoaiToChuc.TextChanged += new EventHandler(SetTextChanged_Handler); //Phân loại cá nhân/tổ chức
            ////ctrCoQuanHaiQuan.TextChanged += new EventHandler(SetTextChanged_Handler); //Cơ quan Hải quan
            ////ctrNhomXuLyHS.TextChanged += new EventHandler(SetTextChanged_Handler); //Nhóm xử lý hồ sơ
            //txtMaDonVi.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã người nhập khẩu
            //txtMaBuuChinhDonVi.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã bưu chính
            //txtSoDienThoaiDonVi.TextChanged += new EventHandler(SetTextChanged_Handler); //Số điện thoại người nhập khẩu
            //txtMaUyThac.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã người ủy thác nhập khẩu
            //txtMaDoiTac.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã người xuất khẩu
            //txtTenDoiTac.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên người xuất khẩu
            //txtMaBuuChinhDoiTac.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã bưu chính
            //txtDiaChiDoiTac1.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa chỉ 1(Street and number/P.O.BOX)
            //txtDiaChiDoiTac2.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa chỉ 2(Street and number/P.O.BOX)
            //txtDiaChiDoiTac3.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa chỉ 3(City name)
            //txtDiaChiDoiTac4.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa chỉ 4(Country sub-entity, name)
            ////ctrMaNuocDoiTac.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã nước(Country, coded)
            ////txtNguoiUyThacXK.TextChanged += new EventHandler(SetTextChanged_Handler); //Người ủy thác xuất khẩu
            //txtMaDaiLyHQ.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đại lý Hải quan
            //txtSoVanDon.TextChanged += new EventHandler(SetTextChanged_Handler); //Số vận đơn (Số B/L, số AWB v.v. …)
            ////ctrMaDVTSoLuong.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đơn vị tính
            ////ctrMaDVTTrongLuong.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đơn vị tính trọng lượng (Gross)
            ////ctrMaDDLuuKho.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
            //txtSoHieuKyHieu.TextChanged += new EventHandler(SetTextChanged_Handler); //Ký hiệu và số hiệu
            //txtMaPTVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phương tiện vận chuyển
            //txtTenPTVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên tầu (máy bay) chở hàng
            ////ctrDiaDiemDoHang.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã địa điểm dỡ hàng
            ////txtTenDiaDiemDohang.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên địa điểm dỡ hàng
            ////ctrDiaDiemXepHang.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã địa điểm xếp hàng
            ////txtTenDiaDiemXepHang.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên địa điểm xếp hàng
            ////ctrMaKetQuaKiemTra.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã kết quả kiểm tra nội dung
            ////ctrMaVanbanPhapQuy.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã văn bản pháp quy khác
            ////ctrPhanLoai.TextChanged += new EventHandler(SetTextChanged_Handler); //Phân loại giấy phép nhập khẩu
            ////txtSoGiayPhep.TextChanged += new EventHandler(SetTextChanged_Handler); //Số của giấy phép
            ////ctrPhanLoaiHD.TextChanged += new EventHandler(SetTextChanged_Handler); //Phân loại hình thức hóa đơn
            //txtSoHoaDon.TextChanged += new EventHandler(SetTextChanged_Handler); //Số hóa đơn
            ////ctrPhuongThucTT.TextChanged += new EventHandler(SetTextChanged_Handler); //Phương thức thanh toán
            ////ctrPhanLoaiGiaHD.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại giá hóa đơn
            //ctrMaDieuKienGiaHD.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã điều kiện giá hóa đơn
            //ctrMaTTHoaDon.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đồng tiền của hóa đơn
            ////ctrMaPhanLoaiTriGia.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại khai trị giá
            ////txtSoTiepNhanTKTriGia.TextChanged += new EventHandler(SetTextChanged_Handler); //Số tiếp nhận tờ khai trị giá tổng hợp
            ////txtMaTTHieuChinhTriGia.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã tiền tệ của giá cơ sở hiệu chỉnh trị giá
            ////ctrMaPhanLoaiPhiVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại phí vận chuyển
            ////txtMaTTPhiVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã tiền tệ phí vận chuyển
            ////txtMaPhanLoaiPhiBH.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại bảo hiểm
            ////txtMaTTPhiBH.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã tiền tệ của tiền bảo hiểm
            ////txtSoDangKyBH.TextChanged += new EventHandler(SetTextChanged_Handler); //Số đăng ký bảo hiểm tổng hợp
            ////txtMaTenDieuChinh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã tên khoản điều chỉnh
            ////ctrMaPhanLoaiDieuChinh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại điều chỉnh trị giá
            ////txtMaTTDieuChinhTriGia.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đồng tiền của khoản điều chỉnh trị giá
            ////txtNguoiNopThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Người nộp thuế 
            ////ctrtMaLyDoDeNghiBP.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã lý do đề nghị BP (release before permit)(Phát hành trước giấy phép)
            //txtMaNHTraThueThay.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã ngân hàng trả thuế thay
            //txtKyHieuCTHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler); //Kí hiệu chứng từ hạn mức
            //txtSoCTHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler); //Số chứng từ hạn mức
            ////ctrMaXDThoiHanNopThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã xác định thời hạn nộp thuế
            //txtMaNHBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã ngân hàng bảo lãnh
            //txtKyHieuCTBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Kí hiệu chứng từ bảo lãnh
            //txtSoCTBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Số chứng từ bảo lãnh
            ////ctrDiaDiemDichVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
            //txtSoQuanLyNoiBoDN.TextChanged += new EventHandler(SetTextChanged_Handler); //Số quản lý của nội bộ doanh nghiệp

            ////UpperCase
            //txtSoToKhaiDauTien.CharacterCasing = CharacterCasing.Upper; //Số tờ khai đầu tiên
            //ctrMaLoaiHinh.IsUpperCase = true; //Mã loại hình
            //ctrMaPhanLoaiHH.IsUpperCase = true; //Mã phân loại hàng hóa
            //ctrMaPhuongThucVT.IsUpperCase = true; //Mã hiệu phương thức vận chuyển
            ////ctrPhanLoaiToChuc.IsUpperCase = true; //Phân loại cá nhân/tổ chức
            //ctrCoQuanHaiQuan.IsUpperCase = true; //Cơ quan Hải quan
            ////ctrNhomXuLyHS.IsUpperCase = true; //Nhóm xử lý hồ sơ
            //txtMaDonVi.CharacterCasing = CharacterCasing.Upper; //Mã người nhập khẩu
            //txtMaBuuChinhDonVi.CharacterCasing = CharacterCasing.Upper; //Mã bưu chính
            //txtSoDienThoaiDonVi.CharacterCasing = CharacterCasing.Upper; //Số điện thoại người nhập khẩu
            //txtMaUyThac.CharacterCasing = CharacterCasing.Upper; //Mã người ủy thác nhập khẩu
            //txtMaDoiTac.CharacterCasing = CharacterCasing.Upper; //Mã người xuất khẩu
            //txtTenDoiTac.CharacterCasing = CharacterCasing.Upper; //Tên người xuất khẩu
            //txtMaBuuChinhDoiTac.CharacterCasing = CharacterCasing.Upper; //Mã bưu chính
            //txtDiaChiDoiTac1.CharacterCasing = CharacterCasing.Upper; //Địa chỉ 1(Street and number/P.O.BOX)
            //txtDiaChiDoiTac2.CharacterCasing = CharacterCasing.Upper; //Địa chỉ 2(Street and number/P.O.BOX)
            //txtDiaChiDoiTac3.CharacterCasing = CharacterCasing.Upper; //Địa chỉ 3(City name)
            //txtDiaChiDoiTac4.CharacterCasing = CharacterCasing.Upper; //Địa chỉ 4(Country sub-entity, name)
            //ctrMaNuocDoiTac.IsUpperCase = true; //Mã nước(Country, coded)
            ////txtNguoiUyThacXK.CharacterCasing = CharacterCasing.Upper; //Người ủy thác xuất khẩu
            //txtMaDaiLyHQ.CharacterCasing = CharacterCasing.Upper; //Mã đại lý Hải quan
            //txtSoVanDon.CharacterCasing = CharacterCasing.Upper; //Số vận đơn (Số B/L, số AWB v.v. …)
            //ctrMaDVTSoLuong.IsUpperCase = true; //Mã đơn vị tính
            //ctrMaDVTTrongLuong.IsUpperCase = true; //Mã đơn vị tính trọng lượng (Gross)
            //ctrMaDDLuuKho.IsUpperCase = true; //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
            //txtSoHieuKyHieu.CharacterCasing = CharacterCasing.Upper; //Ký hiệu và số hiệu
            //txtMaPTVC.CharacterCasing = CharacterCasing.Upper; //Mã phương tiện vận chuyển
            //txtTenPTVC.CharacterCasing = CharacterCasing.Upper; //Tên tầu (máy bay) chở hàng
            ////ctrDiaDiemDoHang.IsUpperCase = true; //Mã địa điểm dỡ hàng
            ////txtTenDiaDiemDohang.CharacterCasing = CharacterCasing.Upper; //Tên địa điểm dỡ hàng
            //ctrDiaDiemXepHang.IsUpperCase = true; //Mã địa điểm xếp hàng
            ////txtTenDiaDiemXepHang.CharacterCasing = CharacterCasing.Upper; //Tên địa điểm xếp hàng
            ////ctrMaKetQuaKiemTra.IsUpperCase = true; //Mã kết quả kiểm tra nội dung
            ////ctrMaVanbanPhapQuy.CharacterCasing = CharacterCasing.Upper; //Mã văn bản pháp quy khác
            ////txtPhanLoai.CharacterCasing = CharacterCasing.Upper; //Phân loại giấy phép nhập khẩu
            ////txtSoGiayPhep.CharacterCasing = CharacterCasing.Upper; //Số của giấy phép
            //ctrPhanLoaiHD.IsUpperCase = true; //Phân loại hình thức hóa đơn
            //txtSoHoaDon.CharacterCasing = CharacterCasing.Upper; //Số hóa đơn
            //ctrPhuongThucTT.IsUpperCase = true; //Phương thức thanh toán
            ////txtPhanLoaiGiaHD.CharacterCasing = CharacterCasing.Upper; //Mã phân loại giá hóa đơn
            ////txtMaDieuKienGiaHD.CharacterCasing = CharacterCasing.Upper; //Mã điều kiện giá hóa đơn
            ////txtMaTTHoaDon.CharacterCasing = CharacterCasing.Upper; //Mã đồng tiền của hóa đơn
            ////ctrMaPhanLoaiTriGia.IsUpperCase = true; //Mã phân loại khai trị giá
            ////txtSoTiepNhanTKTriGia.CharacterCasing = CharacterCasing.Upper; //Số tiếp nhận tờ khai trị giá tổng hợp
            ////txtMaTTHieuChinhTriGia.CharacterCasing = CharacterCasing.Upper; //Mã tiền tệ của giá cơ sở hiệu chỉnh trị giá
            ////txtMaPhanLoaiPhiVC.CharacterCasing = CharacterCasing.Upper; //Mã phân loại phí vận chuyển
            ////txtMaTTPhiVC.CharacterCasing = CharacterCasing.Upper; //Mã tiền tệ phí vận chuyển
            ////txtMaPhanLoaiPhiBH.CharacterCasing = CharacterCasing.Upper; //Mã phân loại bảo hiểm
            ////txtMaTTPhiBH.CharacterCasing = CharacterCasing.Upper; //Mã tiền tệ của tiền bảo hiểm
            ////txtSoDangKyBH.CharacterCasing = CharacterCasing.Upper; //Số đăng ký bảo hiểm tổng hợp
            ////txtMaTenDieuChinh.CharacterCasing = CharacterCasing.Upper; //Mã tên khoản điều chỉnh
            ////txtMaPhanLoaiDieuChinh.CharacterCasing = CharacterCasing.Upper; //Mã phân loại điều chỉnh trị giá
            ////txtMaTTDieuChinhTriGia.CharacterCasing = CharacterCasing.Upper; //Mã đồng tiền của khoản điều chỉnh trị giá
            ////txtNguoiNopThue.CharacterCasing = CharacterCasing.Upper; //Người nộp thuế 
            ////txtMaLyDoDeNghiBP.CharacterCasing = CharacterCasing.Upper; //Mã lý do đề nghị BP (release before permit)(Phát hành trước giấy phép)"
            //txtMaNHTraThueThay.CharacterCasing = CharacterCasing.Upper; //Mã ngân hàng trả thuế thay
            //txtKyHieuCTHanMuc.CharacterCasing = CharacterCasing.Upper; //Kí hiệu chứng từ hạn mức
            //txtSoCTHanMuc.CharacterCasing = CharacterCasing.Upper; //Số chứng từ hạn mức
            //ctrMaXDThoiHanNopThue.IsUpperCase = true; //Mã xác định thời hạn nộp thuế
            //txtMaNHBaoLanh.CharacterCasing = CharacterCasing.Upper; //Mã ngân hàng bảo lãnh
            //txtKyHieuCTBaoLanh.CharacterCasing = CharacterCasing.Upper; //Kí hiệu chứng từ bảo lãnh
            //txtSoCTBaoLanh.CharacterCasing = CharacterCasing.Upper; //Số chứng từ bảo lãnh
            ////txtPhanLoai.CharacterCasing = CharacterCasing.Upper; //Phân loại đính kèm khai báo điện tử
            ////txtMaDiaDiem.CharacterCasing = CharacterCasing.Upper; //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
            ////txtDiaDiemDichVC.CharacterCasing = CharacterCasing.Upper; //Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
            //txtSoQuanLyNoiBoDN.CharacterCasing = CharacterCasing.Upper; //Số quản lý của nội bộ doanh nghiệp
            ////txtPhanLoai.CharacterCasing = CharacterCasing.Upper; //Phân loại chỉ thị của Hải quan

        }

    }
}
