using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class VNACC_Category_CustomsOffice
    {
        protected static List<VNACC_Category_CustomsOffice> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_CustomsOffice> collection = new List<VNACC_Category_CustomsOffice>();
            while (reader.Read())
            {
                VNACC_Category_CustomsOffice entity = new VNACC_Category_CustomsOffice();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomsCode"))) entity.CustomsCode = reader.GetString(reader.GetOrdinal("CustomsCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomsOfficeNameInVietnamese"))) entity.CustomsOfficeNameInVietnamese = reader.GetString(reader.GetOrdinal("CustomsOfficeNameInVietnamese"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_CustomsOffice> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [CustomsCode], [CustomsOfficeNameInVietnamese] FROM [dbo].[t_VNACC_Category_CustomsOffice]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_CustomsOffice> SelectCollectionBy(List<VNACC_Category_CustomsOffice> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_CustomsOffice o)
            {
                return o.TableID.Contains(tableID);
            });
        }
    }
}