/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   Ê¥µîÆïÊ¿£¨Knights Warrior£© 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;
using CSMultiThreadedWebDownloader;
namespace ECS.AutoUpdate
{
    public enum ActionStatus
    {
        Download,
        Unzip,
        UpdateDatabase,
        Complete,
        NoThing
    }
    public enum StatusDownload
    {
        HasDownload = 0,
        Download,
        FileNotFound,
        Complete,
        Failed,
        NotFound
    }
    public partial class DownloadProgress : Form
    {
        #region The private fields
        private bool isFinished = false;
        private List<DownloadFileInfo> downloadFileList = null;
        private List<DownloadFileInfo> allFileList = null;
        private ManualResetEvent evtDownload = null;
        private ManualResetEvent evtPerDonwload = null;
        private WebClient clientDownload = null;
        private DataTable dt;
        private bool cancel = false;
        private ActionStatus status;
        #endregion

        MultiThreadedWebDownloader downloader = null;
        DateTime lastNotificationTime;
        #region The constructor of DownloadProgress
        public DownloadProgress(List<DownloadFileInfo> downloadFileListTemp)
        {
            InitializeComponent();
            dt = new DataTable("DataSource");
            DataColumn dc = new DataColumn("FileName", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Size", typeof(string));

            dt.Columns.Add(dc);

            dc = new DataColumn("Status", typeof(string));
            dt.Columns.Add(dc);


            Text = "Auto Version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + " - " + Config.Install.TitleUpdate;
            this.downloadFileList = downloadFileListTemp;
            allFileList = new List<DownloadFileInfo>();
            foreach (DownloadFileInfo file in downloadFileListTemp)
            {
                DataRow dr = dt.NewRow();
                dr[0] = file.FileName;
                dr[1] = file.Size;
                dr[2] = "Chưa tải";
                dt.Rows.Add(dr);
                //allFileList.Add(file);
            }
        }
        #endregion

        #region The method and event
        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            if (status == ActionStatus.Unzip || status == ActionStatus.UpdateDatabase)
            {
                e.Cancel = true;
                return;
            }
            else
                if (!isFinished && DialogResult.No == MessageBox.Show(ConstFile.CANCELORNOT, ConstFile.MESSAGETITLE, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    e.Cancel = true;
                    return;
                }
                else
                {
                    cancel = true;
                    if (clientDownload != null)
                        clientDownload.CancelAsync();

                    if (!evtDownload.SafeWaitHandle.IsClosed)
                    {
                        evtDownload.Set();
                        evtPerDonwload.Set();
                    }
                }
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            this.Height = 326;
            //this.Height = Height - 183;

            dtGridDetail.DataSource = dt;
            if (dtGridDetail.ColumnCount == 3)
            {
                dtGridDetail.Columns[0].Width = 400;
                dtGridDetail.Columns[0].HeaderText = "Tên tệp tin";
                dtGridDetail.Columns[1].Width = 150;
                dtGridDetail.Columns[1].HeaderText = "Kích thước";
                dtGridDetail.Columns[2].Width = 150;
                dtGridDetail.Columns[2].HeaderText = "Trạng thái";

            }
            evtDownload = new ManualResetEvent(true);
            evtDownload.Reset();

            ThreadPool.QueueUserWorkItem(new WaitCallback(this.ProcDownload));
        }

        long total = 0;
        long nDownloadedTotal = 0;
        private void SaveFileComplete(List<DownloadFileInfo> items, string fileName)
        {
            XmlSerializer xs = new XmlSerializer(typeof(List<DownloadFileInfo>));
            StreamWriter sw = new StreamWriter(fileName);
            xs.Serialize(sw, items);
            sw.Close();
        }
        private List<DownloadFileInfo> GetDownloadFiles(string fileName)
        {
            if (!System.IO.File.Exists(fileName)) return new List<DownloadFileInfo>();
            XmlSerializer xs = new XmlSerializer(typeof(List<DownloadFileInfo>));
            StreamReader sr = new StreamReader(fileName);
            List<DownloadFileInfo> items = xs.Deserialize(sr) as List<DownloadFileInfo>;
            sr.Close();
            return items;
        }
        private void ProcDownload(object o)
        {
            try
            {
                string tempFolderPath = Path.Combine(CommonUnitity.SystemBinUrl, ConstFile.TEMPFOLDERNAME);
                if (!Directory.Exists(tempFolderPath))
                {
                    Directory.CreateDirectory(tempFolderPath);
                }

                evtPerDonwload = new ManualResetEvent(false);


                foreach (DownloadFileInfo file in this.downloadFileList)
                {
                    total += file.Size;
                }
                try
                {
                    //TODO: Kiểm tra tệp tin cũ đã tải.
                    ShowCurrentAction("Kiểm tra tệp tin cũ đã tải.");
                    allFileList = GetDownloadFiles(ConstFile.FILEDOWNLOADOLD);
                    foreach (DownloadFileInfo item in allFileList)
                    {
                        this.ShowFile(item.FileName);
                        string path = CommonUnitity.GetZipPath(item);
                        FileInfo fi = new FileInfo(path);
                        if (fi.Exists)
                        {
                            DownloadFileInfo itemHasDownload = downloadFileList.Find(
                                f => f.FileName.EndsWith(item.FileName) && f.LastVer.Equals(item.LastVer)
                                && f.Size.Equals(item.Size) && f.DownloadUrl.Equals(item.DownloadUrl));


                            if (itemHasDownload != null && fi.Length != 0)
                            {
                                SetStausRow(item.FileName, StatusDownload.HasDownload);
                                downloadFileList.Remove(itemHasDownload);
                            }
                            else SetStausRow(item.FileName, StatusDownload.Failed);
                        }
                    }
                    ShowCurrentAction("Tải bản cập nhật");
                    //TODO: ActionStatus.Download;
                    status = ActionStatus.Download;

                    while (!evtDownload.WaitOne(0, false))
                    {
                        if (this.downloadFileList.Count == 0)
                            break;
                        StatusDownload statusDownload = StatusDownload.Complete;
                        DownloadFileInfo file = this.downloadFileList[0];

                        this.ShowFile(file.FileName);
                        SetStausRow(file.FileName, StatusDownload.Download);
                        string fileName = CommonUnitity.GetZipPath(file);

                        if (!CommonUnitity.IsAutoUpdateMultiThread())
                        {
                            #region Cách tải kiểu cũ

                            //Logger.LocalLogger.Instance().WriteMessage("ECS.AutoUpdate.DownloadProgress: AutoUpdate = One thread", new Exception(""));

                            clientDownload = new WebClient();

                            #region For proxy
                            clientDownload.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                            clientDownload.Proxy.Credentials = CredentialCache.DefaultCredentials;
                            clientDownload.Credentials = System.Net.CredentialCache.DefaultCredentials;
                            #endregion

                            clientDownload.DownloadProgressChanged += (object sender, System.Net.DownloadProgressChangedEventArgs e) =>
                            {
                                try
                                {
                                    this.SetProcessBar(e.ProgressPercentage, (int)((nDownloadedTotal + e.BytesReceived) * 100 / total));
                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(file.FileFullName, ex);
                                }
                            };

                            clientDownload.DownloadFileCompleted += (object sender, AsyncCompletedEventArgs e) =>
                            {
                                try
                                {
                                    if (e.Error != null) throw e.Error;
                                    DownloadFileInfo dfile = e.UserState as DownloadFileInfo;
                                    nDownloadedTotal += dfile.Size;
                                    this.SetProcessBar(0, (int)(nDownloadedTotal * 100 / total));
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Message == "The remote server returned an error: (404) Not Found.")
                                        statusDownload = StatusDownload.FileNotFound;
                                    else
                                        statusDownload = StatusDownload.Failed;
                                    Logger.LocalLogger.Instance().WriteMessage(file.FileFullName, ex);
                                }
                                evtPerDonwload.Set();
                            };

                            evtPerDonwload.Reset();

                            clientDownload.DownloadFileAsync(new Uri(file.DownloadUrl), fileName, file);

                            //Wait for the download complete
                            evtPerDonwload.WaitOne();

                            clientDownload.Dispose();
                            clientDownload = null;

                            #endregion
                        }
                        else
                        {
                            #region Tải theo kiểu đa luồng.
                            try
                            {
                                //Logger.LocalLogger.Instance().WriteMessage("ECS.AutoUpdate.DownloadProgress: AutoUpdate = Multi thread", new Exception(""));

                                evtPerDonwload.Reset();
                                downloader = new MultiThreadedWebDownloader(file.DownloadUrl);
                                downloader.Proxy = null;

                                downloader.DownloadCompleted += DownloadCompleted;
                                downloader.DownloadProgressChanged += DownloadProgressChanged;
                                downloader.StatusChanged += StatusChanged;

                                string outFile = string.Empty;
                                downloader.CheckUrl(out outFile);

                                downloader.DownloadPath = fileName + ".tmp";
                                downloader.BeginDownload();

                                evtPerDonwload.WaitOne();
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                            #endregion
                        }

                        try
                        {
                            FileInfo fi = new FileInfo(fileName);
                            if (!cancel && fi.Exists)
                            {
                                if (allFileList.Find(f => f.FileName == file.FileName && f.Size == file.Size) == null)
                                {

                                    if (fi.Length != 0)
                                    {
                                        this.downloadFileList.Remove(file);
                                        allFileList.Add(file);
                                        SaveFileComplete(allFileList, ConstFile.FILEDOWNLOADOLD);
                                    }
                                    else if (statusDownload == StatusDownload.FileNotFound)
                                    {
                                        this.downloadFileList.Remove(file);
                                    }
                                    else statusDownload = StatusDownload.NotFound;
                                }
                                else if (fi.Length != 0 || statusDownload == StatusDownload.FileNotFound)
                                {
                                    this.downloadFileList.Remove(file);
                                }
                            }
                            SetStausRow(file.FileName, statusDownload);
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw;
                }

                //When the files have not downloaded,return.
                if (downloadFileList.Count > 0)
                {
                    return;
                }

                //After dealed with all files, clear the data
                total = allFileList.Count;
                // NeedRestart();
                status = ActionStatus.Unzip;
                ShowCurrentAction("Giải nén tệp tin đã tải");
                //TODO: UnzipFile.BeforeUnzip()
                if (total != 0) UnzipFile.BeforeUnzip();
                for (int i = 0; i < total; i++)
                {
                    DownloadFileInfo fileinfo = allFileList[i];
                    this.ShowFile(fileinfo.FileName);
                    Random r = new Random();

                    this.SetProcessBar(r.Next(1, 99), (int)(i * 100 / total));
                    UnzipFile.Unzip(fileinfo);
                }

                //TODO: UnzipFile.AfterUnzip()
                if (total != 0) UnzipFile.AfterUnzip();
                status = ActionStatus.UpdateDatabase;
                //TODO: UpdateDataBase()
                if (total != 0) UpdateDataBase();
                this.allFileList.Clear();
                status = ActionStatus.Complete;
                evtDownload.Close();
                if (this.downloadFileList.Count == 0)
                    Exit(true);
                else
                    Exit(false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                status = ActionStatus.NoThing;
                Exit(false);
            }
        }

        void Open(string fileName, string directoryUnzip)
        {
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "Restart.bat"))
                File.Delete(AppDomain.CurrentDomain.BaseDirectory + "Restart.bat");
            StreamWriter SW;
            SW = File.CreateText(AppDomain.CurrentDomain.BaseDirectory + "Restart.bat");
            SW.WriteLine("Taskkill /im " + Process.GetCurrentProcess().ProcessName + ".exe");
            SW.WriteLine("unzip -o" + fileName + " -d " + directoryUnzip);
            SW.WriteLine("start " + AppDomain.CurrentDomain.BaseDirectory + Process.GetCurrentProcess().ProcessName + ".exe");
            SW.Close();
            ProcessStartInfo startInfo = new ProcessStartInfo("Restart.bat");
            Exit(true);
        }


        private void NeedRestart()
        {

            foreach (DownloadFileInfo dFileinfo in allFileList)
            {
                if (dFileinfo.NeedRestart)
                {
                    string fileName = CommonUnitity.GetExcuteFile(dFileinfo);

                    if (Path.GetFileName(fileName).Equals(Process.GetCurrentProcess().ProcessName + ".exe", StringComparison.OrdinalIgnoreCase))
                    {
                        Open(fileName, AppDomain.CurrentDomain.BaseDirectory);
                    }
                }
            }
        }
        //To delete or move to old files
        void MoveFolderToOld(string oldPath, string newPath)
        {
            if (File.Exists(oldPath + ".old"))
                File.Delete(oldPath + ".old");

            if (File.Exists(oldPath))
                File.Move(oldPath, oldPath + ".old");



            File.Move(newPath, oldPath);
            //File.Delete(oldPath + ".old");
        }
        delegate void ShowCurrentActionCallBack(string actionName);
        private void ShowCurrentAction(string actionName)
        {
            if (this.lbAction.InvokeRequired)
            {
                ShowCurrentActionCallBack cb = new ShowCurrentActionCallBack(ShowCurrentAction);
                this.Invoke(cb, new object[] { actionName });
            }
            else
                this.lbAction.Text = actionName;
        }

        delegate void ShowFileCallBack(string name);
        private void ShowFile(string name)
        {
            if (this.labelCurrentItem.InvokeRequired)
            {
                ShowFileCallBack cb = new ShowFileCallBack(ShowFile);
                this.Invoke(cb, new object[] { name });
            }
            else
            {
                this.labelCurrentItem.Text = name;
            }
        }

        delegate void SetProcessBarCallBack(int current, int total);
        private void SetProcessBar(int current, int total)
        {
            if (this.progressBarCurrent.InvokeRequired)
            {
                SetProcessBarCallBack cb = new SetProcessBarCallBack(SetProcessBar);
                this.Invoke(cb, new object[] { current, total });
            }
            else
            {
                this.progressBarCurrent.Value = current;
                this.progressBarTotal.Value = total;
                this.lbPercent.Text = string.Format("{0} %", total);
            }
        }
        private void SetStausRow(string fileName, StatusDownload status)
        {
            string statusInfo = string.Empty;
            switch (status)
            {
                case StatusDownload.HasDownload:
                    statusInfo = "Đã tải từ trước";
                    break;
                case StatusDownload.Download:
                    statusInfo = "Đang tải";
                    break;
                case StatusDownload.FileNotFound:
                    statusInfo = "Không tìm thấy";
                    break;
                case StatusDownload.Complete:
                    statusInfo = "Tải thành công";
                    break;
                case StatusDownload.Failed:
                    statusInfo = "Tải thất bại";
                    break;
                case StatusDownload.NotFound:
                    statusInfo = "Không xác định";
                    break;
                default:
                    break;
            }
            DataRow[] dr = dt.Select("FileName='" + fileName + "'");
            if (dr.Length > 0)
                dr[0][2] = statusInfo;
        }
        delegate void ExitCallBack(bool success);
        private void Exit(bool success)
        {
            if (this.InvokeRequired)
            {
                ExitCallBack cb = new ExitCallBack(Exit);
                this.Invoke(cb, new object[] { success });
            }
            else
            {
                this.isFinished = success;
                this.DialogResult = success ? DialogResult.OK : DialogResult.Cancel;
                this.Close();
            }
        }
        private void DealWithDownloadErrors()
        {
            try
            {
                //Test Network is OK or not.
                // Config config = Config.LoadConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConstFile.FILENAME));
                WebClient client = new WebClient();
                client.DownloadString(Config.Install.ServerUrl);

            }
            catch (Exception ex)
            {
                //log the error message,you can use the application's log code
                Logger.LocalLogger.Instance().WriteMessage(ex);

                //ShowErrorAndRestartApplication();
            }
        }

        private void ShowErrorAndRestartApplication()
        {
            MessageBox.Show(ConstFile.NOTNETWORK, ConstFile.MESSAGETITLE, MessageBoxButtons.OK, MessageBoxIcon.Information);
            CommonUnitity.RestartApplication();
        }
        private void UpdateDataBase()
        {
            List<string> sqlFiles = new List<string>();
            foreach (DownloadFileInfo fileInfo in allFileList)
            {
                if (fileInfo.IsDatabase)
                {
                    string[] files = Directory.GetFiles(CommonUnitity.GetSubPath(fileInfo, CommonUnitity.SystemBinUrl), "*.sql");
                    Array.Sort<string>(files);

                    DataTable dtVersion = new DataTable();
                    dtVersion.Columns.Add("Version", typeof(decimal));
                    dtVersion.Columns.Add("Path", typeof(string));

                    if (files.Length > 0)
                    {
                        string vSqlTemp = "";
                        try
                        {
                            //TODO: Update by Hungtq, 14/11/2013
                            foreach (string strV in files)
                            {
                                DataRow drV = dtVersion.NewRow();
                                vSqlTemp = GetSQLFileVersion(strV);
                                drV["Version"] = Convert.ToDecimal(vSqlTemp, new System.Globalization.CultureInfo("en-US"));
                                drV["Path"] = strV;
                                dtVersion.Rows.Add(drV);
                            }
                        }
                        catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(vSqlTemp, ex); }

                        DataView dvV = dtVersion.DefaultView;
                        dvV.Sort = "Version asc";

                        try
                        {
                            foreach (DataRowView item in dvV)
                            {
                                string version = System.Convert.ToString(item["Version"], new System.Globalization.CultureInfo("en-US"));

                                //if (string.Compare(System.Convert.ToDecimal(version).ToString("##0.0#"), System.Convert.ToDecimal(UpdateSql.VersionDataBase()).ToString("##0.0#")) > 0
                                if (System.Convert.ToDecimal(version) > System.Convert.ToDecimal(UpdateSql.VersionDataBase())
                                    && !sqlFiles.Contains(item["Path"].ToString())) //TODO: Hungtq, 14/11/2013
                                    sqlFiles.Add(item["Path"].ToString());
                            }
                        }
                        catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

                        //Comment by Hungtq, 14/11/2013
                        //foreach (string item in files)
                        //{
                        //    int posVersion = item.IndexOf("_V");
                        //    if (posVersion == -1) continue;
                        //    string version = item.Replace(".sql", string.Empty);
                        //    version = version.Remove(0, posVersion + 2);
                        //    //if (string.Compare(version, UpdateSql.VersionDataBase()) > 0 && !sqlFiles.Contains(item))
                        //    if (string.Compare(System.Convert.ToDecimal(version).ToString("000.##"), System.Convert.ToDecimal(UpdateSql.VersionDataBase()).ToString("000.##")) > 0
                        //        && !sqlFiles.Contains(item)) //TODO: Hungtq, 14/11/2013
                        //        sqlFiles.Add(item);
                        //}
                    }
                }
            }
            if (sqlFiles.Count == 0) return;
            ShowCurrentAction("Cập nhật dữ liệu");
            ShowFile("Xử lý cập nhật");
            this.SetProcessBar(0, 0);

            //Hungtq updated, 3/2/2013
            ShowCurrentAction("Sao lưu dữ liệu (database)");
            //TODO: UpdateSql.BeforeUpdate()
            bool isBackup = UpdateSql.BeforeUpdate();
            if (!isBackup)
            {
                ShowCurrentAction("Sao lưu dữ liệu không thành công.");
            }
            else
                ShowCurrentAction("Đã sao lưu dữ liệu thành công.");

            //if (UpdateSql.BeforeUpdate())
            //{
            ShowCurrentAction("Cập nhật dữ liệu (database)");
            int total = sqlFiles.Count;
            for (int i = 0; i < total; i++)
            {
                string inputFile = sqlFiles[i];

                //Lay ten file, bo path
                System.IO.FileInfo sfi = new FileInfo(inputFile);

                ShowFile(sfi != null ? sfi.Name : inputFile);

                Random r = new Random();
                this.SetProcessBar(r.Next(1, 99), (int)(i * 100 / total));

                //TODO: UpdateSql.Update(inputFile)
                bool isUpdate = UpdateSql.Update(inputFile);
                if (isUpdate)
                {
                    try
                    {
                        File.SetAttributes(inputFile, FileAttributes.Normal);
                        //File.Delete(inputFile); //Xoa file .sql sau khi thuc hien cap nhat script xong.
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        ShowCurrentAction("Có lỗi trong quá trình thực hiện cập nhật dữ liệu.");
                    }
                }
                else
                    break;
            }
            //}
            //else
            //    ShowCurrentAction("Có lỗi trong quá trình thực hiện sao lưu dữ liệu.");

            ShowCurrentAction("Kiểm tra dữ liệu sau khi cập nhật");
            //TODO: UpdateSql.AfterUpdate();
            UpdateSql.AfterUpdate();
        }
        private string GetSQLFileVersion(string sqlFile)
        {
            System.IO.FileInfo fi = new FileInfo(sqlFile);

            string sqlFileName = fi != null ? fi.Name : "";

            int posVersion = sqlFileName.IndexOf("_V");
            if (posVersion == -1)
                return "";

            string version = sqlFileName.Replace(".sql", string.Empty);

            version = version.Remove(0, posVersion + 2);

            return version;
        }
        #endregion

        private void btnDetail_Click(object sender, EventArgs e)
        {
            if (Height != 590)
            {
                Height = 590;
                panel3.Visible = true;
                btnDetail.Text = "<< Chi tiết";
                btnDetail.ImageIndex = 1;
            }
            else
            {
                Height = 590 - 264;
                panel3.Visible = false;
                btnDetail.Text = "Chi tiết >>";
                btnDetail.ImageIndex = 2;
            }
        }
        #region Đa luồng LanNT
        /// <summary>
        /// Handle StatusChanged event.
        /// </summary>
        void StatusChanged(object sender, EventArgs e)
        {
            this.Invoke(new EventHandler(StatusChangedHanlder), sender, e);
        }

        void StatusChangedHanlder(object sender, EventArgs e)
        {
            // Refresh the buttons.

            string filename = Path.GetFileName(downloader.Url.ToString());
            switch (downloader.Status)
            {
                case DownloadStatus.Waiting:
                    lbAction.Text = "Kiểm tra kết nối";
                    SetStausRow(filename, StatusDownload.Download);
                    break;
                case DownloadStatus.Canceled:
                case DownloadStatus.Completed:

                    lbAction.Text = "Tải thành công";
                    SetStausRow(filename, StatusDownload.Complete);
                    break;
                case DownloadStatus.Downloading:
                    lbAction.Text = "Đang tải ";
                    SetStausRow(filename, StatusDownload.Download);
                    break;
                case DownloadStatus.Paused:
                    lbAction.Text = "Tạm ngưng";
                    break;
            }
            if (downloader.Status == DownloadStatus.Paused)
            {
                labelCurrentItem.Text =
                   String.Format("Đã tải: {0}KB, Tổng cộng: {1}KB, Thời gian: {2}:{3}:{4}",
                   downloader.DownloadedSize / 1024, downloader.TotalSize / 1024,
                   downloader.TotalUsedTime.Hours, downloader.TotalUsedTime.Minutes,
                   downloader.TotalUsedTime.Seconds);

                //btnPause.Text = "Resume";
            }
            //else
            //{
            //    //btnPause.Text = "Pause";
            //}


        }

        /// <summary>
        /// Handle DownloadProgressChanged event.
        /// </summary>
        void DownloadProgressChanged(object sender, CSMultiThreadedWebDownloader.DownloadProgressChangedEventArgs e)
        {
            this.Invoke(
                new EventHandler<CSMultiThreadedWebDownloader.DownloadProgressChangedEventArgs>(DownloadProgressChangedHandler),
                sender, e);
        }

        void DownloadProgressChangedHandler(object sender, CSMultiThreadedWebDownloader.DownloadProgressChangedEventArgs e)
        {
            // Refresh the summary every second.
            if (DateTime.Now > lastNotificationTime.AddSeconds(1))
            {
                labelCurrentItem.Text = String.Format(
                    "Đã tải: {0}KB Tổng cộng: {1}KB Tốc độ: {2}KB/s  Luồng: {3}",
                    e.ReceivedSize / 1024,
                    e.TotalSize / 1024,
                    e.DownloadSpeed / 1024,
                    downloader.DownloadThreadsCount);
                progressBarCurrent.Value = (int)(e.ReceivedSize * 100 / e.TotalSize);
                lastNotificationTime = DateTime.Now;
            }
        }

        /// <summary>
        /// Handle DownloadCompleted event.
        /// </summary>
        void DownloadCompleted(object sender, DownloadCompletedEventArgs e)
        {
            this.Invoke(
                new EventHandler<DownloadCompletedEventArgs>(DownloadCompletedHandler),
                sender, e);
        }

        void DownloadCompletedHandler(object sender, DownloadCompletedEventArgs e)
        {
            if (e.Error == null)
            {

                labelCurrentItem.Text =
                                String.Format("Đã tải: {0}KB, Tổng cộng: {1}KB, Thời gian: {2}:{3}:{4}",
                                e.DownloadedSize / 1024, e.TotalSize / 1024, e.TotalTime.Hours,
                                e.TotalTime.Minutes, e.TotalTime.Seconds);
                string filename = e.DownloadedFile.FullName.Replace(".tmp", string.Empty);

                if (File.Exists(filename)) File.Delete(filename);
                if (File.Exists(e.DownloadedFile.FullName))
                    File.Move(e.DownloadedFile.FullName, filename);

                progressBarCurrent.Value = 100;
                nDownloadedTotal += e.TotalSize;
                int valPer = (int)((nDownloadedTotal * 100) / total);
                progressBarTotal.Value = valPer;
                lbPercent.Text = string.Format("{0}%", progressBarTotal.Value);
                //lbFileDownload.Items.Add(filename);
                //lbFileDownload.SelectedItem = filename;
                evtPerDonwload.Set();
            }
            else
            {
                labelCurrentItem.Text = e.Error.Message;
                if (File.Exists(e.DownloadedFile.FullName))
                {
                    File.Delete(e.DownloadedFile.FullName);
                }
                string fileDownload = e.DownloadedFile.FullName.Replace(".tmp", string.Empty);
                if (File.Exists(fileDownload))
                {
                    File.Delete(fileDownload);
                }
                progressBarCurrent.Value = 0;
            }
        }
        #endregion
    }
}