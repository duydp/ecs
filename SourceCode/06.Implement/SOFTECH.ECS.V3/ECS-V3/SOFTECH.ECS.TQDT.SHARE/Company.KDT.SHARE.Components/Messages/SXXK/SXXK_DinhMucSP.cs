﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class SXXK_DinhMucSP : DeclarationBase
    {
        /// <summary>
        /// Sản phẩm
        /// </summary>
        [XmlElement("ProductionNorm")]
        public List<ProductionNorm> ProductionNorm { get; set; }
    }
}
