using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraCharts;

namespace Company.Interface.Report
{
    public partial class ChartForm : Company.Interface.BaseForm
    {
       
        public DataTable data;
        public ChartForm()
        {
            InitializeComponent();
        }

        private void ChartForm_Load(object sender, EventArgs e)
        {
          
            ChartTitle charttitle = new ChartTitle();

            charttitle.Text = "Kim Ngach XK";
            charttitle.Font = new Font("Tahoma", 14, FontStyle.Regular);
            int i = chart.Titles.Add(charttitle);
           
            //Create series
            Series serie = new Series("So Luong", ViewType.Bar);
            Series serie2 = new Series("Gia Tri", ViewType.Bar);
             
            serie.DataSource = this.data;
            serie2.DataSource = this.data;
           
            serie.ArgumentScaleType = ScaleType.Qualitative;
            serie.ArgumentDataMember = "TenHang";
            serie.ValueScaleType = ScaleType.Numerical;
            serie.ValueDataMembers.AddRange(new string[] { "TongSoLuong" });
            

            serie2.ArgumentDataMember = "TenHang";
            serie2.ArgumentScaleType = ScaleType.Qualitative;
            serie2.ValueDataMembers.AddRange(new string[] { "TongTriGia" });
            serie2.ValueScaleType = ScaleType.Numerical;
            
            
            ((SideBySideBarSeriesView)serie.View).ColorEach = false;
            ((SideBySideBarSeriesView)serie2.View).ColorEach = false;
            ((SideBySideBarSeriesView)serie.View).BarWidth = 0.3;
            ((SideBySideBarSeriesView)serie2.View).BarWidth = 0.3;
            //add series into chart
            chart.Series.AddRange(new Series[] { serie, serie2 });
                        
            //should  at here not before
            //((XYDiagram)chart.Diagram).AxisY.Visible = false;
            ((XYDiagram)chart.Diagram).AxisY.Range.AlwaysShowZeroLevel = false;
            ((XYDiagram)chart.Diagram).AxisY.Range.MinValue = (double)10000;
            ((XYDiagram)chart.Diagram).AxisX.Range.Auto = true;
            ((XYDiagram)chart.Diagram).AxisX.GridSpacing = 25000;
            ((XYDiagram)this.chart.Diagram).Rotated = false;
            
        }

        
    }
}

