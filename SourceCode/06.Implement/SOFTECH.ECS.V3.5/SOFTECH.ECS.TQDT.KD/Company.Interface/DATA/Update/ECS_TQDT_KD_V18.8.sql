  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVA11')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVA11',N'Nhập kinh doanh tiêu dùng','A11',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVA12')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVA12',N'Nhập kinh doanh sản xuất','A12',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVA21')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVA21',N'Chuyển tiêu thụ nội địa từ nguồn tạm nhập','A21',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVA31')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVA31',N'Nhập hàng XK bị trả lại','A31',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVA41')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVA41',N'Nhập kinh doanh của doanh nghiệp đầu tư','A41',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVA42')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVA42',N'Chuyển tiêu thụ nội địa khác','A42',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVA43')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVA43',N'Không dùng/dự phòng','A43',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVA44')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVA44',N'Nhập vào khu phi thuế quan từ nội địa','A44',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVAEO')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVAEO',N'Loại hình dành cho doanh nghiệp ưu tiên','AEO',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVC11')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVC11',N'Hàng gửi kho ngoại quan','C11',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVC21')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVC21',N'Hàng đưa vào khu phi thuế quan','C21',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVE11')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVE11',N'Nhập nguyên liệu của doanh nghiệp chế xuất','E11',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVE13')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVE13',N'Nhập tạo tài sản cố định của doanh nghiệp chế xuất','E13',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVE15')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVE15',N'Nhập NL của DNCX từ nội địa','E15',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVE21')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVE21',N'Nhập NL để GC cho  nước ngoài','E21',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVE23')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVE23',N'Nhập NL GC từ hợp đồng khác chuyển sang','E23',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVE25')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVE25',N'Không dùng/dự phòng','E25',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVE31')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVE31',N'Nhập nguyên liệu sản xuất xuất khẩu','E31',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVE33')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVE33',N'Không dùng/dự phòng','E33',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVE41')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVE41',N'Nhập sản phẩm thuê gia công ở nước ngoài','E41',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVG11')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVG11',N'Tạm nhập hàng kinh doanh tạm nhập tái xuất','G11',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVG12')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVG12',N'Tạm nhập máy móc, thiết bị cho dự án có thời hạn','G12',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVG13')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVG13',N'Tạm nhập miễn thuế','G13',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVG14')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVG14',N'Tạm nhập khác','G14',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVG51')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVG51',N'Tái nhập hàng đã tạm xuất','G51',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='NVH11')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('NVH11',N'Các loại nhập khẩu khác','H11',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVAEO')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVAEO',N'Xuất khẩu AEO','AEO',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVB11')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVB11',N'Xuất khẩu thương mại','B11',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVB12')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVB12',N'Xuất khẩu hàng hoá tạm xuất khẩu','B12',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVB13')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVB13',N'Xuất khẩu hàng hoá nhập khẩu (reshipment)','B13',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVC12')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVC12',N'Mang hàng ra khỏi kho ngoại quan để xuất khẩu','C12',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVC22')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVC22',N'Mang hàng ra khỏi khu miễn thuế khác để xuất khẩu','C22',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVE42')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVE42',N'Xuất khẩu các sản phẩm từ nhà máy CX','E42',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVE44')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVE44',N'Xuất khẩu các sản phẩm từ CX nhà máy được ủy quyền','E44',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVE46')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVE46',N'Mang hàng hóa từ nhà máy CX vào nội địa','E46',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVE52')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVE52',N'Xuất khẩu các sản phẩm từ nhà máy GC','E52',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVE54')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVE54',N'Xuất khẩu các sản phẩm từ GC nhà máy được ủy quyền','E54',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVE56')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVE56',N'Mang hàng hóa từ nhà máy GC vào nội địa','E56',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVE62')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVE62',N'Xuất khẩu các sản phẩm từ nhà máy sản xuất SXXK','E62',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVE64')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVE64',N'Xuất khẩu SP từ nhà máy SXXK có thẩm quyền','E64',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVE82')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVE82',N'Xuất khẩu nguyên liệu để chế biến nước ngoài','E82',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVG21')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVG21',N'Tái xuất hàng hoá thương mại ','G21',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVG22')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVG22',N'Tái xuất máy móc, TB cho  các dự án giới hạn','G22',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVG23')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVG23',N'Tái xuất khẩu hàng hóa không chịu thuế tạm nhập','G23',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVG24')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVG24',N'Khác tái xuất','G24',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVG61')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVG61',N'Xuất khẩu tạm thời hàng hóa','G61',null,null)
  IF NOT EXISTS (Select * from t_HaiQuan_LoaiHinhMauDich where ID='XVH21')
  INSERT INTO t_HaiQuan_LoaiHinhMauDich VALUES ('XVH21',N'Xuất khẩu khác','H21',null,null)
GO
alter table t_kdt_tokhaimaudich 
alter column CuaKhau_ID varchar(6)

go

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Insert]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 30, 2011
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(6),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@ChucVu nvarchar(20),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@GUIDSTR varchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference varchar(50),
	@NamDK int,
	@HUONGDAN nvarchar(max),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_ToKhaiMauDich]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[ChucVu],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[CanBoDangKy],
	[QuanLyMay],
	[TrangThaiXuLy],
	[LoaiHangHoa],
	[GiayTo],
	[PhanLuong],
	[MaDonViUT],
	[TenDonViUT],
	[TrongLuongNet],
	[SoTienKhoan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayDangKy,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@MaDaiLyTTHQ,
	@TenDaiLyTTHQ,
	@TenDonViDoiTac,
	@ChiTietDonViDoiTac,
	@SoGiayPhep,
	@NgayGiayPhep,
	@NgayHetHanGiayPhep,
	@SoHopDong,
	@NgayHopDong,
	@NgayHetHanHopDong,
	@SoHoaDonThuongMai,
	@NgayHoaDonThuongMai,
	@PTVT_ID,
	@SoHieuPTVT,
	@NgayDenPTVT,
	@QuocTichPTVT_ID,
	@LoaiVanDon,
	@SoVanDon,
	@NgayVanDon,
	@NuocXK_ID,
	@NuocNK_ID,
	@DiaDiemXepHang,
	@CuaKhau_ID,
	@DKGH_ID,
	@NguyenTe_ID,
	@TyGiaTinhThue,
	@TyGiaUSD,
	@PTTT_ID,
	@SoHang,
	@SoLuongPLTK,
	@TenChuHang,
	@ChucVu,
	@SoContainer20,
	@SoContainer40,
	@SoKien,
	@TrongLuong,
	@TongTriGiaKhaiBao,
	@TongTriGiaTinhThue,
	@LoaiToKhaiGiaCong,
	@LePhiHaiQuan,
	@PhiBaoHiem,
	@PhiVanChuyen,
	@PhiXepDoHang,
	@PhiKhac,
	@CanBoDangKy,
	@QuanLyMay,
	@TrangThaiXuLy,
	@LoaiHangHoa,
	@GiayTo,
	@PhanLuong,
	@MaDonViUT,
	@TenDonViUT,
	@TrongLuongNet,
	@SoTienKhoan,
	@GUIDSTR,
	@DeXuatKhac,
	@LyDoSua,
	@ActionStatus,
	@GuidReference,
	@NamDK,
	@HUONGDAN
)

SET @ID = SCOPE_IDENTITY()

Go


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, January 23, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(255),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(6),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@ChucVu nvarchar(20),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 4),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference nvarchar(500),
	@NamDK int,
	@HUONGDAN nvarchar(max),
	@IDHopDong bigint,
	@MaMid varchar(100),
	@Ngay_THN_THX datetime,
	@TrangThaiPhanBo int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiMauDich] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
			[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
			[TenDonViDoiTac] = @TenDonViDoiTac,
			[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
			[SoGiayPhep] = @SoGiayPhep,
			[NgayGiayPhep] = @NgayGiayPhep,
			[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[NgayHetHanHopDong] = @NgayHetHanHopDong,
			[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
			[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
			[PTVT_ID] = @PTVT_ID,
			[SoHieuPTVT] = @SoHieuPTVT,
			[NgayDenPTVT] = @NgayDenPTVT,
			[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
			[LoaiVanDon] = @LoaiVanDon,
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[NuocXK_ID] = @NuocXK_ID,
			[NuocNK_ID] = @NuocNK_ID,
			[DiaDiemXepHang] = @DiaDiemXepHang,
			[CuaKhau_ID] = @CuaKhau_ID,
			[DKGH_ID] = @DKGH_ID,
			[NguyenTe_ID] = @NguyenTe_ID,
			[TyGiaTinhThue] = @TyGiaTinhThue,
			[TyGiaUSD] = @TyGiaUSD,
			[PTTT_ID] = @PTTT_ID,
			[SoHang] = @SoHang,
			[SoLuongPLTK] = @SoLuongPLTK,
			[TenChuHang] = @TenChuHang,
			[ChucVu] = @ChucVu,
			[SoContainer20] = @SoContainer20,
			[SoContainer40] = @SoContainer40,
			[SoKien] = @SoKien,
			[TrongLuong] = @TrongLuong,
			[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
			[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
			[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
			[LePhiHaiQuan] = @LePhiHaiQuan,
			[PhiBaoHiem] = @PhiBaoHiem,
			[PhiVanChuyen] = @PhiVanChuyen,
			[PhiXepDoHang] = @PhiXepDoHang,
			[PhiKhac] = @PhiKhac,
			[CanBoDangKy] = @CanBoDangKy,
			[QuanLyMay] = @QuanLyMay,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[LoaiHangHoa] = @LoaiHangHoa,
			[GiayTo] = @GiayTo,
			[PhanLuong] = @PhanLuong,
			[MaDonViUT] = @MaDonViUT,
			[TenDonViUT] = @TenDonViUT,
			[TrongLuongNet] = @TrongLuongNet,
			[SoTienKhoan] = @SoTienKhoan,
			[GUIDSTR] = @GUIDSTR,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua,
			[ActionStatus] = @ActionStatus,
			[GuidReference] = @GuidReference,
			[NamDK] = @NamDK,
			[HUONGDAN] = @HUONGDAN,
			[IDHopDong] = @IDHopDong,
			[MaMid] = @MaMid,
			[Ngay_THN_THX] = @Ngay_THN_THX,
			[TrangThaiPhanBo] = @TrangThaiPhanBo
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiMauDich]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayDangKy],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[MaDaiLyTTHQ],
			[TenDaiLyTTHQ],
			[TenDonViDoiTac],
			[ChiTietDonViDoiTac],
			[SoGiayPhep],
			[NgayGiayPhep],
			[NgayHetHanGiayPhep],
			[SoHopDong],
			[NgayHopDong],
			[NgayHetHanHopDong],
			[SoHoaDonThuongMai],
			[NgayHoaDonThuongMai],
			[PTVT_ID],
			[SoHieuPTVT],
			[NgayDenPTVT],
			[QuocTichPTVT_ID],
			[LoaiVanDon],
			[SoVanDon],
			[NgayVanDon],
			[NuocXK_ID],
			[NuocNK_ID],
			[DiaDiemXepHang],
			[CuaKhau_ID],
			[DKGH_ID],
			[NguyenTe_ID],
			[TyGiaTinhThue],
			[TyGiaUSD],
			[PTTT_ID],
			[SoHang],
			[SoLuongPLTK],
			[TenChuHang],
			[ChucVu],
			[SoContainer20],
			[SoContainer40],
			[SoKien],
			[TrongLuong],
			[TongTriGiaKhaiBao],
			[TongTriGiaTinhThue],
			[LoaiToKhaiGiaCong],
			[LePhiHaiQuan],
			[PhiBaoHiem],
			[PhiVanChuyen],
			[PhiXepDoHang],
			[PhiKhac],
			[CanBoDangKy],
			[QuanLyMay],
			[TrangThaiXuLy],
			[LoaiHangHoa],
			[GiayTo],
			[PhanLuong],
			[MaDonViUT],
			[TenDonViUT],
			[TrongLuongNet],
			[SoTienKhoan],
			[GUIDSTR],
			[DeXuatKhac],
			[LyDoSua],
			[ActionStatus],
			[GuidReference],
			[NamDK],
			[HUONGDAN],
			[IDHopDong],
			[MaMid],
			[Ngay_THN_THX],
			[TrangThaiPhanBo]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayDangKy,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@MaDaiLyTTHQ,
			@TenDaiLyTTHQ,
			@TenDonViDoiTac,
			@ChiTietDonViDoiTac,
			@SoGiayPhep,
			@NgayGiayPhep,
			@NgayHetHanGiayPhep,
			@SoHopDong,
			@NgayHopDong,
			@NgayHetHanHopDong,
			@SoHoaDonThuongMai,
			@NgayHoaDonThuongMai,
			@PTVT_ID,
			@SoHieuPTVT,
			@NgayDenPTVT,
			@QuocTichPTVT_ID,
			@LoaiVanDon,
			@SoVanDon,
			@NgayVanDon,
			@NuocXK_ID,
			@NuocNK_ID,
			@DiaDiemXepHang,
			@CuaKhau_ID,
			@DKGH_ID,
			@NguyenTe_ID,
			@TyGiaTinhThue,
			@TyGiaUSD,
			@PTTT_ID,
			@SoHang,
			@SoLuongPLTK,
			@TenChuHang,
			@ChucVu,
			@SoContainer20,
			@SoContainer40,
			@SoKien,
			@TrongLuong,
			@TongTriGiaKhaiBao,
			@TongTriGiaTinhThue,
			@LoaiToKhaiGiaCong,
			@LePhiHaiQuan,
			@PhiBaoHiem,
			@PhiVanChuyen,
			@PhiXepDoHang,
			@PhiKhac,
			@CanBoDangKy,
			@QuanLyMay,
			@TrangThaiXuLy,
			@LoaiHangHoa,
			@GiayTo,
			@PhanLuong,
			@MaDonViUT,
			@TenDonViUT,
			@TrongLuongNet,
			@SoTienKhoan,
			@GUIDSTR,
			@DeXuatKhac,
			@LyDoSua,
			@ActionStatus,
			@GuidReference,
			@NamDK,
			@HUONGDAN,
			@IDHopDong,
			@MaMid,
			@Ngay_THN_THX,
			@TrangThaiPhanBo
		)		
	END

Go
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Update]
-- Database: ECS_SXXKTQ
-- Author: Ngo Thanh Tung
-- Time created: Thursday, January 07, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(255),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(500),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(6),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@ChucVu nvarchar(20),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 5),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@HeSoNhan float,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference nvarchar(500),
	@NamDK int,
	@HUONGDAN nvarchar(max)
AS
UPDATE
	[dbo].[t_KDT_ToKhaiMauDich]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
	[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
	[TenDonViDoiTac] = @TenDonViDoiTac,
	[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
	[SoGiayPhep] = @SoGiayPhep,
	[NgayGiayPhep] = @NgayGiayPhep,
	[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[NgayHetHanHopDong] = @NgayHetHanHopDong,
	[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
	[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
	[PTVT_ID] = @PTVT_ID,
	[SoHieuPTVT] = @SoHieuPTVT,
	[NgayDenPTVT] = @NgayDenPTVT,
	[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
	[LoaiVanDon] = @LoaiVanDon,
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[NuocXK_ID] = @NuocXK_ID,
	[NuocNK_ID] = @NuocNK_ID,
	[DiaDiemXepHang] = @DiaDiemXepHang,
	[CuaKhau_ID] = @CuaKhau_ID,
	[DKGH_ID] = @DKGH_ID,
	[NguyenTe_ID] = @NguyenTe_ID,
	[TyGiaTinhThue] = @TyGiaTinhThue,
	[TyGiaUSD] = @TyGiaUSD,
	[PTTT_ID] = @PTTT_ID,
	[SoHang] = @SoHang,
	[SoLuongPLTK] = @SoLuongPLTK,
	[TenChuHang] = @TenChuHang,
	[ChucVu] = @ChucVu,
	[SoContainer20] = @SoContainer20,
	[SoContainer40] = @SoContainer40,
	[SoKien] = @SoKien,
	[TrongLuong] = @TrongLuong,
	[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
	[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
	[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
	[LePhiHaiQuan] = @LePhiHaiQuan,
	[PhiBaoHiem] = @PhiBaoHiem,
	[PhiVanChuyen] = @PhiVanChuyen,
	[PhiXepDoHang] = @PhiXepDoHang,
	[PhiKhac] = @PhiKhac,
	[CanBoDangKy] = @CanBoDangKy,
	[QuanLyMay] = @QuanLyMay,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[LoaiHangHoa] = @LoaiHangHoa,
	[GiayTo] = @GiayTo,
	[PhanLuong] = @PhanLuong,
	[MaDonViUT] = @MaDonViUT,
	[TenDonViUT] = @TenDonViUT,
	[TrongLuongNet] = @TrongLuongNet,
	[SoTienKhoan] = @SoTienKhoan,
	[GUIDSTR] = @GUIDSTR,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua,
	[ActionStatus] = @ActionStatus,
	[GuidReference] = @GuidReference,
	[NamDK] = @NamDK,
	[HUONGDAN] = @HUONGDAN
WHERE
	[ID] = @ID


GO
IF NOT EXISTS (select * from t_HaiQuan_CuaKhau where ID='VNZZZ')
  INSERT INTO t_HaiQuan_CuaKhau VALUES ('VNZZZ',N'Việt Nam','1',null,null)

Go
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.8',GETDATE(), N' Cap nhat danh muc V5 khai tu xa')
END	
