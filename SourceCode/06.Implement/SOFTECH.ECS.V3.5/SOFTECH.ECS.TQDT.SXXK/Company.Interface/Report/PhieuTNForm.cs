﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.SXXK;
using Company.BLL.KDT.SXXK;
using DevExpress.XtraPrinting;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report
{
    public partial class PhieuTNForm : BaseForm
    {
        PhieuTN Report = new PhieuTN();
        PhieuTN ReportAll = new PhieuTN();
        public string maHaiQuan;
        public string TenPhieu;
        public string [,] phieuTN;
        
        public PhieuTNForm()
        {
            InitializeComponent();
        }

        private void PreviewForm_Load(object sender, EventArgs e)
        {
            this.ViewPhanBo();
        }
        private void ViewPhanBo()
        {
            this.Report.phieu = this.TenPhieu;
            this.Report.soTN = this.phieuTN[0,0];
            this.Report.ngayTN = this.phieuTN[0,1];
            this.Report.maHaiQuan = maHaiQuan;
            printControl1.PrintingSystem = this.Report.PrintingSystem;
            this.Report.CreateDocument();
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            } 
        }
        private void cbPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

    }
}