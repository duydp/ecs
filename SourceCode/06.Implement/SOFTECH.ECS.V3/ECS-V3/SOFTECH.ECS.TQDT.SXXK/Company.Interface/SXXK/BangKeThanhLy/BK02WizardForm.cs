using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK02WizardForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public BKToKhaiXuatCollection bkTKXCollection = new BKToKhaiXuatCollection();
        public ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        public BK02WizardForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (this.bkTKXCollection.Count == 0)
            {
                // ShowMessage("Bạn phải nhập danh sách tờ khai xuất", false);
                MLMessages("Bạn phải nhập danh sách tờ khai xuất.", "MSG_THK82", "", false);
            }
            else
            {
                int i = this.HSTL.getBKToKhaiXuat();
                if (i < 0)
                {
                    BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                    bk.MaBangKe = "DTLTKX";
                    bk.TenBangKe = "DTLTKX";
                    bk.STTHang = this.HSTL.BKCollection.Count + 1;
                    bk.bkTKXColletion = this.bkTKXCollection;
                    this.HSTL.BKCollection.Add(bk);
                }
                else
                {
                    this.HSTL.BKCollection[i].bkTKXColletion = this.bkTKXCollection;
                }
                BK01WizardForm bk01 = new BK01WizardForm();
                bk01.MdiParent = this.ParentForm;
                bk01.HSTL = this.HSTL;
                bk01.Show();
                this.Close();
            }
        }

        private void BK02WizardForm_Load(object sender, EventArgs e)
        {
            dgTKX.RootTable.Columns["NGAY_THN_THX"].DefaultValue = DateTime.Today;
            dgTKXTL.RootTable.Columns["NgayThucXuat"].DefaultValue = DateTime.Today;

            DateTime fDay = new DateTime(DateTime.Now.Year - 1, 1, 1);
            ccFromDate.Value = fDay;

            DateTime fromDate = ccFromDate.Value;
            DateTime toDate = ccToDate.Value;

            Company.KDT.SHARE.Components.Globals.TuNgay_HSTK = ccFromDate.Value;
            Company.KDT.SHARE.Components.Globals.DenNgay_HSTK = ccToDate.Value;

            this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKXChuaThanhLyByDate(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate);
            int i = this.HSTL.getBKToKhaiXuat();
            if (i >= 0)
            {
                this.bkTKXCollection = this.HSTL.BKCollection[i].bkTKXColletion;
                foreach (BKToKhaiXuat bk in this.bkTKXCollection)
                {
                    this.RemoveTKMD(bk.SoToKhai, bk.MaLoaiHinh, bk.NamDangKy, bk.MaHaiQuan);
                }
            }
            try
            {
                dgTKX.DataSource = this.tkmdCollection;
            }
            catch
            { }
            try
            {
                dgTKXTL.DataSource = this.bkTKXCollection;
            }
            catch
            { }
        }
        private void RemoveTKMD(ToKhaiMauDich tkmd)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == tkmd.SoToKhai && this.tkmdCollection[i].MaLoaiHinh == tkmd.MaLoaiHinh && this.tkmdCollection[i].NamDangKy == tkmd.NamDangKy && this.tkmdCollection[i].MaHaiQuan == tkmd.MaHaiQuan)
                {
                    this.tkmdCollection.Remove(tkmdCollection[i]);
                    break;
                }
            }
        }
        private void RemoveTKMD(int SoToKhai, string MaLoaiHinh, short NamDangKy, string MaHaiQuan)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == SoToKhai && this.tkmdCollection[i].MaLoaiHinh == MaLoaiHinh && this.tkmdCollection[i].NamDangKy == NamDangKy && this.tkmdCollection[i].MaHaiQuan == MaHaiQuan)
                {
                    this.tkmdCollection.Remove(tkmdCollection[i]);
                    break;
                }
            }
        }
        private void RemoveBKTKX(BKToKhaiXuat bk)
        {
            for (int i = 0; i < this.bkTKXCollection.Count; i++)
            {
                if (this.bkTKXCollection[i].SoToKhai == bk.SoToKhai && this.bkTKXCollection[i].MaLoaiHinh == bk.MaLoaiHinh && this.bkTKXCollection[i].NamDangKy == bk.NamDangKy && this.bkTKXCollection[i].MaHaiQuan == bk.MaHaiQuan)
                {
                    this.bkTKXCollection.RemoveAt(i);
                    break;
                }
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            ToKhaiMauDichCollection tkmds = new ToKhaiMauDichCollection();
            foreach (GridEXRow row in dgTKX.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    ToKhaiMauDich tkmd = (ToKhaiMauDich)row.DataRow;
                    BKToKhaiXuat bk = new BKToKhaiXuat();
                    bk.MaHaiQuan = tkmd.MaHaiQuan;
                    bk.MaLoaiHinh = tkmd.MaLoaiHinh;
                    bk.NamDangKy = tkmd.NamDangKy;
                    bk.NgayDangKy = tkmd.NgayDangKy;
                    bk.NgayThucXuat = tkmd.NGAY_THN_THX;
                    bk.SoToKhai = tkmd.SoToKhai;
                    this.bkTKXCollection.Add(bk);
                    tkmds.Add(tkmd);
                }
            }
            foreach (ToKhaiMauDich tkmd in tkmds)
                this.RemoveTKMD(tkmd);
            dgTKX.DataSource = this.tkmdCollection;
            try
            {
                dgTKX.Refetch();
            }
            catch
            {
                dgTKX.Refresh();
            }

            try
            {
                dgTKXTL.Refetch();
            }
            catch
            {
                dgTKXTL.Refresh();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            BKToKhaiXuatCollection bks = new BKToKhaiXuatCollection();
            foreach (GridEXRow row in dgTKXTL.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    BKToKhaiXuat bk = (BKToKhaiXuat)row.DataRow;
                    ToKhaiMauDich tkmd = new ToKhaiMauDich();
                    tkmd.MaHaiQuan = bk.MaHaiQuan;
                    tkmd.SoToKhai = bk.SoToKhai;
                    tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                    tkmd.NgayDangKy = bk.NgayDangKy;
                    tkmd.NamDangKy = bk.NamDangKy;
                    tkmd.NGAY_THN_THX = bk.NgayThucXuat;
                    this.tkmdCollection.Add(tkmd);
                    bks.Add(bk);
                }
            }
            foreach (BKToKhaiXuat bk in bks)
                this.RemoveBKTKX(bk);
            dgTKX.DataSource = this.tkmdCollection;
            try
            {
                dgTKXTL.Refetch();
            }
            catch
            {
                dgTKXTL.Refresh();
            }
            try
            {
                dgTKX.Refetch();
            }
            catch
            {
                dgTKX.Refresh();
            }

        }
        private void dgTKX_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {

                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog();
            }
        }

        private void dgTKXTL_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog();
            }
        }

        private void dgTKXTL_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                BKToKhaiXuat bk = (BKToKhaiXuat)e.Row.DataRow;
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.MaHaiQuan = bk.MaHaiQuan;
                tkmd.SoToKhai = bk.SoToKhai;
                tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                tkmd.NgayDangKy = bk.NgayDangKy;
                tkmd.NamDangKy = bk.NamDangKy;
                tkmd.NGAY_THN_THX = bk.NgayThucXuat;
                this.tkmdCollection.Add(tkmd);
            }
            try
            {
                dgTKX.Refetch();
            }
            catch
            {
                dgTKX.Refresh();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            DateTime fromDate = ccFromDate.Value;
            DateTime toDate = ccToDate.Value;
            if (fromDate > toDate)
            {
                MLMessages("Từ ngày phải nhỏ hơn hoặc bằng đến ngày.", "MSG_SEA01", "", false);
                return;
            }
            this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKXChuaThanhLyByDate(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate);
            foreach (BKToKhaiXuat bk in this.bkTKXCollection)
            {
                this.RemoveTKMD(bk.SoToKhai, bk.MaLoaiHinh, bk.NamDangKy, bk.MaHaiQuan);
            }
            dgTKX.DataSource = this.tkmdCollection;
            try
            {
                dgTKX.Refetch();
            }
            catch
            {
                dgTKX.Refresh();
            }
        }

        private void dgTKX_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (Convert.ToDateTime(e.Row.Cells["Ngay_THN_THX"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["Ngay_THN_THX"].Text = "";
        }

        private void dgTKXTL_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (Convert.ToDateTime(e.Row.Cells["NgayThucXuat"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayThucXuat"].Text = "";
        }
        private void dgTKXTL_RecordUpdated(object sender, EventArgs e)
        {
            GridEXRow row = dgTKXTL.GetRow();
            BKToKhaiXuat bk = (BKToKhaiXuat)row.DataRow;
            bk.Update();
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.SoToKhai = bk.SoToKhai;
            TKMD.MaLoaiHinh = bk.MaLoaiHinh;
            TKMD.MaHaiQuan = bk.MaHaiQuan;
            TKMD.NamDangKy = bk.NamDangKy;
            TKMD.Load();
            TKMD.NGAY_THN_THX = bk.NgayThucXuat;
            TKMD.Update();
        }

        private void dgTKX_RecordUpdated(object sender, EventArgs e)
        {
            GridEXRow row = dgTKX.GetRow();
            ToKhaiMauDich TKMD = (ToKhaiMauDich)row.DataRow;
            DateTime temp = TKMD.NGAY_THN_THX;
            TKMD.Load();
            TKMD.NGAY_THN_THX = temp;
            TKMD.Update();
        }

        private void btnGoTo_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTKX.FindAll(dgTKX.Tables[0].Columns["SoToKhai"], ConditionOperator.Equal, Convert.ToInt64(txtSoToKhai.Value)) > 0)
                {

                    foreach (GridEXSelectedItem item in dgTKX.SelectedItems)
                        item.GetRow().IsChecked = true;
                }
                else
                {
                    MLMessages("Không có tờ khai này trong hệ thống", "MSG_THK79", "", false);
                }
            }
            catch
            { }


        }
    }
}