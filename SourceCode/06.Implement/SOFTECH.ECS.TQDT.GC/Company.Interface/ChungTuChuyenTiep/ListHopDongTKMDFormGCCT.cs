﻿using System;
using System.Drawing;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.KDT.GC;
using Company.Interface.GC;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class ListHopDongTKMDFormGCCT : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT;
        //-----------------------------------------------------------------------------------------

        public ListHopDongTKMDFormGCCT()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NguyenTe_ID"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value.ToString());
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = TKCT.CTKHopDongThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            if (TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                //btnTaoMoi.Enabled = false;
                //btnXoa.Enabled = false;
            }

            //HUNGTQ, Uopdate 07/06/2010
            SetButtonStateHOPDONG(TKCT, isKhaiBoSung);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong> listHopDong = new List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong hd = (Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong)i.GetRow().DataRow;
                        listHopDong.Add(hd);
                    }
                }
            }

            foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong hd in listHopDong)
            {
                hd.Delete();
                TKCT.CTKHopDongThuongMaiCollection.Remove(hd);
            }

            dgList.DataSource = TKCT.CTKHopDongThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            HopDongFormGCCT f = new HopDongFormGCCT();
            f.TKCT = TKCT;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.ShowDialog();
            dgList.DataSource = TKCT.CTKHopDongThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            HopDongFormGCCT f = new HopDongFormGCCT();
            f.TKCT = TKCT;
            f.HopDongTM = (Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong)e.Row.DataRow;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.ShowDialog();
            dgList.DataSource = TKCT.CTKHopDongThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form.
        /// </summary>
        /// <param name="tkct"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateHOPDONG(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct, bool isKhaiBoSung)
        {
            bool status = false;
            if (TKCT.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                //Khai bao moi
                if (isKhaiBoSung == false)
                {
                    //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                    status = (tkct.SoTiepNhan == 0);

                    btnXoa.Enabled = status;
                    btnTaoMoi.Enabled = status;
                }
                //Khai bao bo sung
                else
                {
                    //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                    if (tkct.SoToKhai == 0)
                    {
                        //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                        //Globals.ShowMessageTQDT(msg, false);

                        //return false;
                    }
                    else
                    {
                        status = true;

                        btnXoa.Enabled = status;
                        btnTaoMoi.Enabled = status;
                    }
                }
            }
            return true;
        }

        #endregion

    }
}
