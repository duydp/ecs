﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.KDT.ExportToExcel;
using System.Globalization;
//using Company.KDT.SHARE.Components.DuLieuChuan;


namespace Company.Interface
{
    public partial class ImportFromExcelForm : BaseForm
    {
        //Nguyên phụ liệu : 
        public NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
        public NguyenPhuLieuCollection NPLReadCollection = new NguyenPhuLieuCollection();
        public NguyenPhuLieuCollection NPLChuaCoCollection = new NguyenPhuLieuCollection();

        //Sản phẩm :
        public SanPhamCollection SPCollection = new SanPhamCollection();
        public SanPhamCollection SPReadCollection = new SanPhamCollection();
        public SanPhamCollection SPChuaCoCollection = new SanPhamCollection();

        //Định mức :
        public DinhMucCollection DMCollection = new DinhMucCollection();
        public DinhMucCollection DMReadCollection = new DinhMucCollection();
        public DinhMucCollection DMChuaCoCollection = new DinhMucCollection();

        //Thông tin định mức :
        public ThongTinDinhMucCollection TTDMReadCollection = new ThongTinDinhMucCollection();
        public ThongTinDinhMucCollection TTDMChuaCoCollection = new ThongTinDinhMucCollection();
        public ThongTinDinhMucCollection TTDMCollection = new ThongTinDinhMucCollection();

        //Hàng Mậu dịch :
        public HangMauDichCollection HMDCollection = new HangMauDichCollection();
        //Hàng mậu dịch điều chỉnh :
        public HangMauDichDieuChinhCollection HMDDCCollection = new HangMauDichDieuChinhCollection();
        //Nhập tồn :
        public NPLNhapTonCollection NPLTonCollection = new NPLNhapTonCollection();

        //Tờ khai mậu dịch :
        public ToKhaiMauDichCollection TKCollection = new ToKhaiMauDichCollection();
        public ThongTinDieuChinhCollection TTDCCollection = new ThongTinDieuChinhCollection();


        public ImportFromExcelForm()
        {
            InitializeComponent();
        }
        private void ExportToExcelForm_Load(object sender, EventArgs e)
        {
            txtMaDN.Text = GlobalSettings.MA_DON_VI;
            txtTenDN.Text = GlobalSettings.TEN_DON_VI;
            btnImport.Enabled = false;
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uiCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            ccFromDate.Enabled = true;
            ccToDate.Enabled = false;
        }
        //Ghi dữ liệu :
        private void btnImport_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            btnImport.Enabled = false;
            try
            {
                if (chkOverwrite.Checked == true)
                {
                    if (chkNPL.Checked == true)
                        new NguyenPhuLieu().InsertUpdate(this.NPLReadCollection);

                    if (chkSanPham.Checked == true)
                        new SanPham().InsertUpdate(this.SPReadCollection);

                    if (chkDMMoiKhai.Checked == true)
                    {
                        new DinhMuc().InsertUpdate(this.DMReadCollection);
                        new ThongTinDinhMuc().InsertUpdate(this.TTDMReadCollection);
                    }
                    if (chkTTDM.Checked == true)
                    {
                        new ThongTinDinhMuc().InsertUpdate(this.TTDMReadCollection);
                    }
                    if (chkToKhaiMauDich.Checked == true)
                    {
                        new ToKhaiMauDich().InsertUpdate(this.TKCollection);
                        //new ThongTinDieuChinh().InsertUpdate(this.TTDCCollection);
                    }

                    if (chkHMD.Checked == true)
                    {
                        new HangMauDich().InsertUpdate(this.HMDCollection);
                        //new HangMauDichDieuChinh().InsertUpdate(this.HMDDCCollection);
                        new NPLNhapTon().InsertUpdate(this.NPLTonCollection);
                    }
                }
                else
                {
                    if (chkNPL.Checked == true)
                        new NguyenPhuLieu().InsertUpdate(this.NPLChuaCoCollection);

                    if (chkSanPham.Checked == true)
                        new SanPham().InsertUpdate(this.SPChuaCoCollection);

                    if (chkDMMoiKhai.Checked == true)
                    {

                        new ThongTinDinhMuc().InsertUpdate(this.TTDMChuaCoCollection);
                        new DinhMuc().InsertUpdate(this.DMChuaCoCollection);
                    }

                    if (chkTTDM.Checked == true)
                    {
                        new ThongTinDinhMuc().InsertUpdate(this.TTDMChuaCoCollection);
                    }

                    if (chkToKhaiMauDich.Checked == true)
                    {
                        new ToKhaiMauDich().InsertUpdate(this.TKCollection);
                        //new ThongTinDieuChinh().InsertUpdate(this.TTDCCollection);
                    }

                    if (chkHMD.Checked == true)
                    {
                        new HangMauDich().InsertUpdate(this.HMDCollection);
                        //new HangMauDichDieuChinh().InsertUpdate(this.HMDDCCollection);
                        new NPLNhapTon().InsertUpdate(this.NPLTonCollection);
                    }
                    // ShowMessage("Nhập dữ liệu thành công.", false);
                    MLMessages("Nhập thành công.", "MSG_PUB02", "", false);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
            //Nguyên Phụ liệu :
            this.NPLCollection.AddRange(this.NPLChuaCoCollection);

            //Sản phẩm :
            this.SPCollection.AddRange(this.SPChuaCoCollection);

            //Định mức :
            this.DMCollection.AddRange(this.DMChuaCoCollection);

            //Thông tin định mức :
            this.TTDMCollection.AddRange(this.TTDMChuaCoCollection);

            //Tờ khai :
            //Hàng mậu dịch :

            btnImport.Enabled = true;
            this.Cursor = Cursors.Default;
            //this.Close();

        }
        // kiểm tra NPL đã tồn tại chưa.
        private int checkNPLExit1(string maNPL)
        {
            for (int i = 0; i < this.NPLCollection.Count; i++)
            {
                if (this.NPLCollection[i].Ma.ToUpper() == maNPL.ToUpper()) return i;
            }
            return -1;
        }

        // Kiểm tra tồn tại của sản phẩm :
        private int checkNPLExitSP(string maSP)
        {
            for (int i = 0; i < this.SPCollection.Count; i++)
            {
                if (this.SPCollection[i].Ma.ToUpper() == maSP.ToUpper()) return i;
            }
            return -1;
        }
        // Kiểm tra tồn tại Dịnh mức :
        private int checkDMExit1(string maSP, string maNPL)
        {
            for (int i = 0; i < this.DMCollection.Count; i++)
            {
                if (this.DMCollection[i].MaSanPHam.ToUpper() == maSP.ToUpper() && this.DMCollection[i].MaNguyenPhuLieu.ToUpper() == maNPL.ToUpper()) return i;
            }
            return -1;

        }
        private int checkTTDMExit1(string maSP)
        {
            for (int i = 0; i < this.TTDMCollection.Count; i++)
            {
                if (this.TTDMCollection[i].MaSanPham.ToUpper() == maSP.ToUpper()) return i;
            }
            return -1;
        }
        //

        // Kiểm tra tồn tại Tờ khai :

        // Kiểm tra tồn tại HMD :

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog(this);
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void btnReadFile_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = 2;
            this.Cursor = Cursors.WaitCursor;
            btnReadFile.Enabled = false;
            btnImport.Enabled = false;
            Workbook wb = new Workbook();
            Worksheet wsNPL = null;
            Worksheet wsSP = null;
            Worksheet wsDM = null;
            Worksheet wsTTDM = null;
            Worksheet wsTKMD = null;
            Worksheet wsHMD = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                this.Cursor = Cursors.Default;
                btnReadFile.Enabled = true;
                return;
            }
            this.Cursor = Cursors.WaitCursor;
            btnReadFile.Enabled = false;

            if (chkNPL.Checked)
            {
                try
                {
                    wsNPL = wb.Worksheets["NPL_D"];
                }
                catch
                {
                    ShowMessage("Không tồn tại sheet NPL_D tương ứng", false);
                    this.Cursor = Cursors.Default;
                    btnReadFile.Enabled = true;
                    return;
                }
                this.NPLReadCollection.Clear();
                WorksheetRowCollection wsrcNPL = wsNPL.Rows;
                foreach (WorksheetRow wsr in wsrcNPL)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            NguyenPhuLieu npl = new NguyenPhuLieu();
                            npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            npl.Ma = Convert.ToString(wsr.Cells[0].Value).Trim();
                            npl.Ten = Convert.ToString(wsr.Cells[1].Value).Trim();
                            npl.MaHS = Convert.ToString(wsr.Cells[2].Value).Trim();
                            //npl.DVT_ID = DonViTinh_GetID(Convert.ToString(wsr.Cells[dvtCol].Value));
                            npl.DVT_ID = Convert.ToString(wsr.Cells[3].Value);
                            if (checkNPLExit1(npl.Ma) >= 0)
                            {
                                npl.IsExist = 1;

                            }
                            else this.NPLChuaCoCollection.Add(npl);
                            this.NPLReadCollection.Add(npl);

                        }
                        catch
                        {
                            if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " thuôc Sheet TTDM_D không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                            {
                                this.NPLReadCollection.Clear();
                                this.Cursor = Cursors.Default;
                                btnReadFile.Enabled = true;
                                return;
                            }
                        }
                    }
                }

            }

            //Đọc Sheet SP
            if (chkSanPham.Checked == true)
            {

                try
                {
                    wsSP = wb.Worksheets["SP_D"];
                }
                catch
                {
                    ShowMessage("Không tồn tại sheet  SP_D", false);
                    this.Cursor = Cursors.Default;
                    // uiButton1.Enabled = true;
                    return;
                }
                this.SPReadCollection.Clear();
                WorksheetRowCollection wsrc = wsSP.Rows;
                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            SanPham SP = new SanPham();
                            SP.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            SP.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            SP.Ma = Convert.ToString(wsr.Cells[0].Value).Trim();
                            //SP.Ten = Convert.ToString(wsr.Cells[1].Value).Trim();
                            SP.Ten = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(Convert.ToString(wsr.Cells[1].Value).Trim());
                            SP.MaHS = Convert.ToString(wsr.Cells[2].Value).Trim();
                            SP.DVT_ID = Convert.ToString(wsr.Cells[3].Value);
                            if (checkNPLExitSP(SP.Ma) >= 0)
                            {
                                SP.IsExist = 1;
                            }
                            else
                                this.SPChuaCoCollection.Add(SP);
                            this.SPReadCollection.Add(SP);
                        }
                        catch
                        {
                            if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " thuôc Sheet TTDM_D không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                            {
                                this.SPReadCollection.Clear();
                                this.Cursor = Cursors.Default;
                                // uiButton1.Enabled = true;
                                return;
                            }
                        }
                    }
                }

            }

            //Đọc Sheet DM
            if (chkDMMoiKhai.Checked == true)
            {

                try
                {
                    wsDM = wb.Worksheets["DM_D"];
                }
                catch
                {
                    ShowMessage("Không tồn tại sheet  DM ", false);
                    this.btnReadFile.Enabled = true;
                    this.Cursor = Cursors.Default;
                    return;
                }
                this.DMReadCollection.Clear();
                WorksheetRowCollection wsrc = wsDM.Rows;

                this.DMReadCollection.Clear();
                string maSP = "";
                ThongTinDinhMuc TTDM = new ThongTinDinhMuc();
                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            if (maSP != Convert.ToString(wsr.Cells[0].Value).Trim())
                            {
                                TTDM = new ThongTinDinhMuc();
                                TTDM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                TTDM.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                                TTDM.MaSanPham = Convert.ToString(wsr.Cells[0].Value).Trim();
                                maSP = TTDM.MaSanPham;
                                if (checkTTDMExit1(TTDM.MaSanPham) >= 0)
                                {
                                    TTDM.IsExist = 1;
                                }
                                else
                                {
                                    this.TTDMChuaCoCollection.Add(TTDM);

                                }
                                this.TTDMReadCollection.Add(TTDM);
                            }
                            DinhMuc DM = new DinhMuc();
                            DM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            DM.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            DM.MaSanPHam = Convert.ToString(wsr.Cells[0].Value).Trim();
                            DM.MaNguyenPhuLieu = Convert.ToString(wsr.Cells[1].Value).Trim();
                            DM.DinhMucSuDung = Convert.ToDecimal(wsr.Cells[2].Value);
                            DM.TyLeHaoHut = Convert.ToDecimal(wsr.Cells[3].Value);
                            DM.DinhMucChung = Convert.ToDecimal(wsr.Cells[4].Value);
                            if (checkDMExit1(DM.MaSanPHam, DM.MaNguyenPhuLieu) >= 0)
                            {
                                DM.IsExist = 1;
                            }
                            else
                            {
                                this.DMChuaCoCollection.Add(DM);

                            }
                            this.DMReadCollection.Add(DM);
                        }
                        catch
                        {
                            if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " thuộc sheet DM_D không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                            {
                                this.DMReadCollection.Clear();
                                this.btnReadFile.Enabled = true;
                                this.Cursor = Cursors.Default;
                                return;
                            }
                        }
                    }
                }

            }
            // Đọc sheet TTDM :
            if (chkTTDM.Checked == true)
            {
                try
                {
                    wsTKMD = wb.Worksheets["TTDM_D"];
                }
                catch
                {
                    ShowMessage("Không tồn tại sheet TTDM_  ", false);
                    this.Cursor = Cursors.Default;
                    btnReadFile.Enabled = true;
                    return;
                }
                WorksheetRowCollection wsrc = wsTKMD.Rows;
                this.TTDMReadCollection.Clear();
                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            ThongTinDinhMuc TTDM = new ThongTinDinhMuc();
                            TTDM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            TTDM.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            TTDM.MaSanPham = Convert.ToString(wsr.Cells[0].Value).Trim();
                            TTDM.SoDinhMuc = Convert.ToInt32(wsr.Cells[1].Value);
                            TTDM.NgayDangKy = DateTime.FromOADate((double)(wsr.Cells[2].Value));

                            if ((wsr.Cells[3].Value) != null)
                            {
                                TTDM.NgayApDung = DateTime.FromOADate((double)(wsr.Cells[3].Value));
                            }
                            if (checkTTDMExit1(TTDM.MaSanPham) >= 0)
                            {
                                TTDM.IsExist = 1;
                            }
                            else this.TTDMChuaCoCollection.Add(TTDM);
                            this.TTDMReadCollection.Add(TTDM);
                        }
                        catch
                        {
                            if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " thuôc Sheet TTDM_D  không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                            {
                                this.TTDMReadCollection.Clear();
                                this.Cursor = Cursors.Default;
                                btnReadFile.Enabled = true;
                                return;
                            }
                        }
                    }
                }
            }
            //Đọc Sheet TKMD
            if (chkToKhaiMauDich.Checked == true)
            {
                try
                {
                    wsTKMD = wb.Worksheets["TKMD_D"];
                }
                catch
                {
                    ShowMessage("Không tồn tại sheet  TKMD_D ", false);
                    this.btnReadFile.Enabled = true;
                    this.Cursor = Cursors.Default;
                    return;
                }

                WorksheetRowCollection wsrc = wsTKMD.Rows;

                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            //if (Convert.ToInt32(wsr.Cells[ConvertStringToInt(txtSoToKhai.Text)].Value).Length == 0) break;
                            ToKhaiMauDich TK = new ToKhaiMauDich();
                            ThongTinDieuChinh TTDC = new ThongTinDieuChinh();
                            //5
                            TK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            //2
                            TTDC.MaHaiQuan = TK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;

                            TTDC.SoToKhai = TK.SoToKhai = Convert.ToInt32(wsr.Cells[0].Value);
                            TTDC.MaLoaiHinh = TK.MaLoaiHinh = Convert.ToString(wsr.Cells[1].Value);
                            try
                            {
                                TK.NgayDangKy = DateTime.FromOADate((double)wsr.Cells[4].Value);
                            }
                            catch { }
                            TK.MaDonViUT = Convert.ToString(wsr.Cells[6].Value);
                            //3
                            TTDC.NamDangKy = TK.NamDangKy = Convert.ToInt16(TK.NgayDangKy.Year);
                            TTDC.LanDieuChinh = 0;
                            TK.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                            TK.TenDonViDoiTac = Convert.ToString(wsr.Cells[7].Value);
                            TK.PTVT_ID = Convert.ToString(wsr.Cells[8].Value);
                            TK.ChiTietDonViDoiTac = "";
                            TK.SoHieuPTVT = Convert.ToString(wsr.Cells[9].Value);
                            try
                            {
                                TK.NgayDenPTVT = DateTime.FromOADate((double)wsr.Cells[10].Value);
                            }
                            catch
                            { }
                            TK.LoaiVanDon = Convert.ToString(wsr.Cells[11].Value);
                            TK.SoGiayPhep = Convert.ToString(wsr.Cells[12].Value);
                            try
                            {
                                TK.NgayGiayPhep = DateTime.FromOADate((double)wsr.Cells[13].Value);
                            }
                            catch
                            { }
                            try
                            {
                                TK.NgayHetHanGiayPhep = Convert.ToDateTime((double)wsr.Cells[14].Value);
                            }
                            catch
                            { }
                            TK.SoHopDong = Convert.ToString(wsr.Cells[15].Value);
                            TK.TyGiaUSD = Convert.ToDecimal(wsr.Cells[16].Value);
                            try
                            {
                                TK.NgayHopDong = DateTime.FromOADate((double)wsr.Cells[17].Value);
                            }
                            catch
                            { }
                            try
                            {
                                TK.NgayHetHanHopDong = DateTime.FromOADate((double)wsr.Cells[18].Value);
                            }
                            catch
                            { }
                            TK.NuocNK_ID = Convert.ToString(wsr.Cells[19].Value);
                            TK.NuocXK_ID = Convert.ToString(wsr.Cells[20].Value);
                            TK.SoHang = Convert.ToInt16(wsr.Cells[21].Value);
                            TK.PTTT_ID = Convert.ToString(wsr.Cells[22].Value);
                            TK.NguyenTe_ID = Convert.ToString(wsr.Cells[23].Value);
                            TK.TyGiaTinhThue = Convert.ToDecimal(wsr.Cells[24].Value);
                            TK.LePhiHaiQuan = Convert.ToDecimal(wsr.Cells[25].Value);
                            TK.TenChuHang = Convert.ToString(wsr.Cells[26].Value);
                            TK.DiaDiemXepHang = Convert.ToString(wsr.Cells[27].Value);
                            TK.PhiBaoHiem = Convert.ToDecimal(wsr.Cells[28].Value);
                            TK.PhiVanChuyen = Convert.ToDecimal(wsr.Cells[29].Value);
                            TK.TongTriGiaKhaiBao = Convert.ToDecimal(wsr.Cells[30].Value);
                            TK.TongTriGiaTinhThue = Convert.ToDecimal(wsr.Cells[31].Value);
                            TK.TrongLuong = Convert.ToDecimal(wsr.Cells[32].Value);
                            TK.SoKien = Convert.ToDecimal(wsr.Cells[33].Value);
                            TK.SoContainer20 = Convert.ToDecimal(wsr.Cells[34].Value);
                            TK.PhanLuong = Convert.ToString(wsr.Cells[35].Value);
                            TK.ThanhLy = Convert.ToString(wsr.Cells[36].Value);
                            try
                            {
                                TK.NGAY_THN_THX = DateTime.FromOADate((double)wsr.Cells[37].Value);
                            }
                            catch { }
                            TK.SoContainer40 = Convert.ToDecimal(wsr.Cells[38].Value);
                            TK.SoHoaDonThuongMai = Convert.ToString(wsr.Cells[39].Value);
                            try
                            {
                                TK.NgayHoaDonThuongMai = DateTime.FromOADate((double)wsr.Cells[40].Value);
                            }
                            catch
                            { }
                            try
                            {
                                TK.NgayVanDon = DateTime.FromOADate((double)wsr.Cells[41].Value);
                            }
                            catch { }
                            TK.SoLuongPLTK = Convert.ToInt16(wsr.Cells[42].Value);
                            TK.Xuat_NPL_SP = Convert.ToString(wsr.Cells[43].Value);
                            try
                            {
                                TK.NgayHoanThanh = DateTime.FromOADate((double)wsr.Cells[44].Value);
                            }
                            catch { }
                            TK.SoVanDon = Convert.ToString(wsr.Cells[45].Value);
                            TK.MaDaiLyTTHQ = Convert.ToString(wsr.Cells[46].Value);
                            TK.TenDaiLyTTHQ = Convert.ToString(wsr.Cells[47].Value);
                            TK.ChiTietDonViDoiTac = Convert.ToString(wsr.Cells[48].Value);
                            TK.QuocTichPTVT_ID = Convert.ToString(wsr.Cells[49].Value);
                            TK.CuaKhau_ID = Convert.ToString(wsr.Cells[50].Value);
                            TK.DKGH_ID = Convert.ToString(wsr.Cells[51].Value);
                            TK.LoaiToKhaiGiaCong = Convert.ToString(wsr.Cells[52].Value);
                            TK.PhiXepDoHang = Convert.ToDecimal(wsr.Cells[53].Value);
                            TK.PhiKhac = Convert.ToDecimal(wsr.Cells[54].Value);
                            TK.TrangThaiThanhKhoan = Convert.ToString(wsr.Cells[55].Value);
                            TK.ChungTu = Convert.ToString(wsr.Cells[56].Value);
                            TK.TrangThai = Convert.ToInt32(wsr.Cells[57].Value);
                            //if (TK.MaLoaiHinh.Contains("NSX")) TK.NgayHoanThanh = TK.NGAY_THN_THX = TK.NgayDangKy;
                            this.TKCollection.Add(TK);
                            this.TTDCCollection.Add(TTDC);
                        }
                        catch
                        {
                            if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " thuộc Sheet TKMD_D không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                            {
                                this.btnReadFile.Enabled = true;
                                this.Cursor = Cursors.Default;
                                return;
                            }
                        }
                    }
                }

            }

            //Đọc Sheet HMD
            if (chkHMD.Checked == true)
            {
                try
                {
                    wsHMD = wb.Worksheets["HMD_D"];
                }
                catch
                {
                    ShowMessage("Không tồn tại sheet  HMD_", false);
                    this.btnReadFile.Enabled = true;
                    this.Cursor = Cursors.Default;
                    return;
                }
                WorksheetRowCollection wsrc = wsHMD.Rows;

                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            NumberFormatInfo format = new NumberFormatInfo();
                            format.NumberDecimalSeparator = ".";
                            format.NumberGroupSeparator = ",";
                            HangMauDich HMD = new HangMauDich();
                            HangMauDichDieuChinh HMDDC = new HangMauDichDieuChinh();
                            NPLNhapTon NPLNT = new NPLNhapTon();
                            //2
                            HMD.MaHaiQuan = HMDDC.MaHaiQuan = NPLNT.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            HMDDC.LanDieuChinh = 0;
                            HMDDC.SoToKhai = HMD.SoToKhai = NPLNT.SoToKhai = Convert.ToInt32(wsr.Cells[0].Value);
                            HMDDC.MaLoaiHinh = HMD.MaLoaiHinh = NPLNT.MaLoaiHinh = Convert.ToString(wsr.Cells[1].Value);
                            HMDDC.NamDangKy = HMD.NamDangKy = NPLNT.NamDangKy = Convert.ToInt16(wsr.Cells[3].Value);
                            HMDDC.STTHang = HMD.SoThuTuHang = Convert.ToInt16(wsr.Cells[4].Value);
                            HMDDC.MaHS = HMDDC.MaHSKB = HMD.MaHS = Convert.ToString(wsr.Cells[5].Value);
                            HMDDC.MaHang = HMDDC.MA_NPL_SP = HMD.MaPhu = NPLNT.MaNPL = Convert.ToString(wsr.Cells[6].Value);

                            //HMDDC.TenHang = HMD.TenHang = Convert.ToString(wsr.Cells[7].Value);
                            HMDDC.TenHang = HMD.TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(Convert.ToString(wsr.Cells[7].Value));
                            HMDDC.DinhMuc = 0;// Convert.ToInt32(wsr.Cells[10].Value);

                            //
                            HMDDC.NuocXX_ID = HMD.NuocXX_ID = Convert.ToString(wsr.Cells[8].Value);
                            HMDDC.DVT_ID = HMD.DVT_ID = Convert.ToString(wsr.Cells[9].Value);
                            HMDDC.Luong = HMD.SoLuong = NPLNT.Luong = NPLNT.TonDau = NPLNT.TonCuoi = (decimal)Convert.ToDouble(wsr.Cells[10].Value, format);

                            HMDDC.DGIA_KB = Convert.ToDouble(wsr.Cells[11].Value, format);

                            try
                            {
                                HMDDC.DGIA_TT = Convert.ToDouble(wsr.Cells[12].Value, format);
                            }
                            catch { }
                            HMDDC.MA_DG = "HD";// Convert.ToString(wsr.Cells[16].Value);

                            HMDDC.TRIGIA_KB = Convert.ToDouble(wsr.Cells[13].Value, format);
                            HMDDC.TRIGIA_TT = Convert.ToDouble(wsr.Cells[14].Value, format);
                            HMDDC.TGKB_VND = Convert.ToDouble(wsr.Cells[15].Value, format);
                            // HMDDC.LOAITSXNK = Convert.ToByte(wsr.Cells[20].Value);

                            try
                            {
                                HMDDC.TS_XNK = HMD.ThueSuatXNK = Convert.ToDecimal(wsr.Cells[16].Value, format);
                            }
                            catch { }
                            try
                            {
                                HMDDC.TS_TTDB = HMD.ThueSuatTTDB = Convert.ToDecimal(wsr.Cells[17].Value, format);
                            }
                            catch { }
                            try
                            {
                                HMDDC.TS_VAT = HMD.ThueSuatGTGT = Convert.ToDecimal(wsr.Cells[18].Value, format);
                            }
                            catch { }
                            try
                            {
                                HMDDC.THUE_XNK = Convert.ToDouble(wsr.Cells[19].Value, format);
                            }
                            catch { }
                            try
                            {
                                HMDDC.THUE_TTDB = Convert.ToDouble(wsr.Cells[20].Value, format);
                            }
                            catch { }
                            try
                            {
                                HMDDC.THUE_VAT = Convert.ToDouble(wsr.Cells[21].Value, format);
                            }
                            catch { }
                            try
                            {
                                HMDDC.PHU_THU = Convert.ToDouble(wsr.Cells[22].Value, format);
                            }
                            catch { }
                            try
                            {
                                HMD.MienThue = HMDDC.MIENTHUE = Convert.ToByte(wsr.Cells[25].Value, format);
                            }
                            catch { }
                            //HMDDC.TL_QUYDOI = Convert.ToDecimal(wsr.Cells[29].Value);
                            /// HMDDC.MA_THKE = Convert.ToString(wsr.Cells[30].Value);
                            //HMDDC.CHOXULY = Convert.ToByte(wsr.Cells[31].Value);
                            try
                            {
                                HMDDC.TYLE_THUKHAC = Convert.ToDecimal(wsr.Cells[24].Value, format);
                            }
                            catch { }
                            HMD.DonGiaKB = (decimal)Convert.ToDouble(wsr.Cells[11].Value, format);
                            try
                            {
                                HMD.DonGiaTT = (decimal)Convert.ToDouble(wsr.Cells[12].Value, format);
                            }
                            catch { }
                            // HMDDC.MA_DG = "HD";                           

                            HMD.TriGiaKB = (decimal)Convert.ToDouble(wsr.Cells[13].Value, format);
                            HMD.TriGiaTT = (decimal)Convert.ToDouble(wsr.Cells[14].Value, format);
                            HMD.TriGiaKB_VND = (decimal)Convert.ToDouble(wsr.Cells[15].Value, format);
                            try
                            {
                                HMD.ThueXNK = (decimal)Convert.ToDouble(wsr.Cells[19].Value, format);
                                NPLNT.ThueXNK = Convert.ToDouble(wsr.Cells[19].Value, format);
                            }
                            catch { }
                            try
                            {
                                HMD.ThueTTDB = (decimal)Convert.ToDouble(wsr.Cells[20].Value, format);
                                NPLNT.ThueTTDB = Convert.ToDouble(wsr.Cells[20].Value, format);
                            }
                            catch
                            { }
                            try
                            {
                                HMD.ThueGTGT = (decimal)Convert.ToDouble(wsr.Cells[21].Value, format);
                                NPLNT.ThueVAT = Convert.ToDouble(wsr.Cells[21].Value, format);
                            }
                            catch { }
                            try
                            {
                                HMD.PhuThu = (decimal)Convert.ToDouble(wsr.Cells[22].Value, format);
                                NPLNT.PhuThu = Convert.ToDouble(wsr.Cells[22].Value, format);
                            }
                            catch { }
                            try
                            {
                                HMD.TriGiaThuKhac = Convert.ToDecimal(wsr.Cells[23].Value, format);
                            }
                            catch { }
                            try
                            {
                                HMD.TyLeThuKhac = Convert.ToDecimal(wsr.Cells[24].Value, format);
                            }
                            catch { }

                            try
                            {
                                HMDDC.TRIGIA_THUKHAC = Convert.ToDouble(wsr.Cells[23].Value, format);
                            }
                            catch { }
                            NPLNT.ThueCLGia = 0;

                            this.HMDCollection.Add(HMD);
                            this.HMDDCCollection.Add(HMDDC);
                            this.NPLTonCollection.Add(NPLNT);
                        }
                        catch
                        {
                            if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                            {
                                this.btnReadFile.Enabled = true;
                                this.Cursor = Cursors.Default;
                                return;
                            }
                        }
                    }
                }
            }
            ShowMessage("Đọc file thành công ", false);
            this.btnImport.Enabled = true;
            this.btnReadFile.Enabled = true;
            this.Cursor = Cursors.Default;

        }
    }
}