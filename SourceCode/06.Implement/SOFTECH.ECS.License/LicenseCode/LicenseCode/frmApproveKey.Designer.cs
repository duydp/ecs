﻿namespace LicenseCode
{
    partial class frmApproveKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmApproveKey));
            this.listView1 = new System.Windows.Forms.ListView();
            this.lvRequestBy = new System.Windows.Forms.ColumnHeader();
            this.lvProductID = new System.Windows.Forms.ColumnHeader();
            this.lvSerialNumber = new System.Windows.Forms.ColumnHeader();
            this.lvDays = new System.Windows.Forms.ColumnHeader();
            this.lvCustomerCode = new System.Windows.Forms.ColumnHeader();
            this.lvCustomerName = new System.Windows.Forms.ColumnHeader();
            this.lvCustomerDes = new System.Windows.Forms.ColumnHeader();
            this.lvRequestDate = new System.Windows.Forms.ColumnHeader();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Approve = new System.Windows.Forms.ToolStripMenuItem();
            this.DisApprove = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvRequestBy,
            this.lvProductID,
            this.lvSerialNumber,
            this.lvDays,
            this.lvCustomerCode,
            this.lvCustomerName,
            this.lvCustomerDes,
            this.lvRequestDate});
            this.listView1.ContextMenuStrip = this.contextMenuStrip1;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(820, 337);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseClick);
            // 
            // lvRequestBy
            // 
            this.lvRequestBy.Text = "Người Lấy Key";
            this.lvRequestBy.Width = 85;
            // 
            // lvProductID
            // 
            this.lvProductID.Text = "Mã sản phẩm";
            this.lvProductID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lvProductID.Width = 77;
            // 
            // lvSerialNumber
            // 
            this.lvSerialNumber.Text = "Serial";
            this.lvSerialNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lvSerialNumber.Width = 83;
            // 
            // lvDays
            // 
            this.lvDays.Text = "Thời hạn";
            this.lvDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lvDays.Width = 54;
            // 
            // lvCustomerCode
            // 
            this.lvCustomerCode.Text = "Mã doanh nghiệp";
            this.lvCustomerCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lvCustomerCode.Width = 95;
            // 
            // lvCustomerName
            // 
            this.lvCustomerName.Text = "Tên doanh nghiệp";
            this.lvCustomerName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lvCustomerName.Width = 118;
            // 
            // lvCustomerDes
            // 
            this.lvCustomerDes.Text = "Mô tả";
            this.lvCustomerDes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lvCustomerDes.Width = 195;
            // 
            // lvRequestDate
            // 
            this.lvRequestDate.Text = "Ngày yêu cầu";
            this.lvRequestDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lvRequestDate.Width = 106;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Approve,
            this.DisApprove});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(136, 48);
            this.contextMenuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuStrip1_ItemClicked);
            // 
            // Approve
            // 
            this.Approve.Name = "Approve";
            this.Approve.Size = new System.Drawing.Size(135, 22);
            this.Approve.Text = "Chấp nhận ";
            // 
            // DisApprove
            // 
            this.DisApprove.Name = "DisApprove";
            this.DisApprove.Size = new System.Drawing.Size(135, 22);
            this.DisApprove.Text = "Từ chối";
            // 
            // frmApproveKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(820, 337);
            this.Controls.Add(this.listView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmApproveKey";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Danh sách key chờ xác nhận";
            this.Load += new System.EventHandler(this.frmApproveKey_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader lvRequestBy;
        private System.Windows.Forms.ColumnHeader lvProductID;
        private System.Windows.Forms.ColumnHeader lvDays;
        private System.Windows.Forms.ColumnHeader lvCustomerCode;
        private System.Windows.Forms.ColumnHeader lvCustomerName;
        private System.Windows.Forms.ColumnHeader lvCustomerDes;
        private System.Windows.Forms.ColumnHeader lvRequestDate;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Approve;
        private System.Windows.Forms.ToolStripMenuItem DisApprove;
        private System.Windows.Forms.ColumnHeader lvSerialNumber;
    }
}