﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Threading;
using System.ComponentModel;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class DownloadUpdate
    {
        private ManualResetEvent evtDownload = null;
        private ManualResetEvent evtPerDonwload = null;
        private WebClient clientDownload = null;
        private const string dirTemp = "";
        private const string cfg = "Autoupdater.config";
        private string args = string.Empty;
        public DownloadUpdate(string args)
        {
            this.args = args;
        }
        public void DoDownload()
        {
            evtDownload = new ManualResetEvent(true);
            evtDownload.Reset();
            ThreadPool.QueueUserWorkItem(new WaitCallback(this.ProcDownload));
        }
        private void StartUpdate(string arg)
        {
            if (File.Exists("AppAutoUpdate.exe"))
            {
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("AppAutoUpdate.exe", arg);
                psi.CreateNoWindow = true;
                psi.ErrorDialog = true;
                psi.UseShellExecute = false;
                psi.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
                System.Diagnostics.Process.Start(psi);
            }

#if DEBUG
            //bool bHasError = false;
            //ECS.AutoUpdate.IAutoUpdater autoUpdater = new ECS.AutoUpdate.AutoUpdater();
            //try
            //{

            //    DirectoryInfo dInfo = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            //    System.Security.AccessControl.DirectorySecurity dSecurity = dInfo.GetAccessControl();
            //    dSecurity.AddAccessRule(new System.Security.AccessControl.FileSystemAccessRule("everyone", System.Security.AccessControl.FileSystemRights.FullControl, System.Security.AccessControl.InheritanceFlags.ObjectInherit | System.Security.AccessControl.InheritanceFlags.ContainerInherit, System.Security.AccessControl.PropagationFlags.InheritOnly, System.Security.AccessControl.AccessControlType.Allow));
            //    dInfo.SetAccessControl(dSecurity);

            //    autoUpdater.Update("MSG");
            //}
            //catch (WebException exp)
            //{
            //    System.Windows.Forms.MessageBox.Show("Không thể kết nối với hệ thống máy chủ Softech\nVui lòng kiểm tra lại kết nối của bạn.", "Cập nhật phần mềm");
            //    bHasError = true;
            //    Logger.LocalLogger.Instance().WriteMessage(exp);
            //}
#endif
        }

        private void CreateNode(XmlDocument doc, XmlNode serverNode)
        {
            try
            {
                string sfmtXml = serverNode.OuterXml;
                sfmtXml = sfmtXml.Replace("RemoteFile", "LocalFile");
                XmlNode parentNode = doc.SelectSingleNode("Config/UpdateFileList");
                XmlNode nodeChild = parentNode.SelectSingleNode("LocalFile[@path='" + serverNode.Attributes["path"].InnerText + "']");
                if (nodeChild != null)
                    parentNode.RemoveChild(nodeChild);

                XmlDocument docNew = new XmlDocument();
                docNew.LoadXml(sfmtXml);
                if (docNew.FirstChild.Attributes != null)
                {
                    docNew.FirstChild.Attributes.Remove(docNew.FirstChild.Attributes["fullSize"]);
                    docNew.FirstChild.Attributes.Remove(docNew.FirstChild.Attributes["isDatabase"]);
                    docNew.FirstChild.Attributes.Remove(docNew.FirstChild.Attributes["needRestart"]);

                }

                XmlNode copiedNode = doc.ImportNode(docNew.FirstChild, true);
                parentNode.AppendChild(copiedNode);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private string getPath(XmlDocument docVersionOld, XmlDocument docVersionNew, string FileName)
        {
            try
            {
                XmlNode nodeVersionOld = docVersionOld.SelectSingleNode("Config/UpdateFileList/LocalFile[@path=\"" + FileName + "\"]");
                XmlNode nodeVersionNew = docVersionNew.SelectSingleNode("UpdateFileList/RemoteFile[@path=\"" + FileName + "\"]");

                if (nodeVersionOld == null)
                {

                    CreateNode(docVersionOld, nodeVersionNew);
                    return FileName;
                }
                else
                    /* string.Compare(frist, second) = 0:
                     * frist = second: 0
                     * frist > second: 1
                     * frist < second: -1
                     */
                    if (string.Compare(nodeVersionOld.Attributes["lastver"].Value, nodeVersionNew.Attributes["lastver"].Value) == -1
                        || string.Compare(nodeVersionOld.Attributes["size"].Value, nodeVersionNew.Attributes["size"].Value) == -1)
                    {
                        CreateNode(docVersionOld, nodeVersionNew);
                        return FileName;
                    }
                    else
                    {
                        return string.Empty;
                    }
            }
            catch
            {
                return FileName;
            }
        }

        private bool UnZip(string fileName)
        {
            if (!File.Exists("unzip.exe")) return false;
            try
            {
                //Logger.LocalLogger.Instance().WriteMessage("Tên file: " + fileName, new Exception(""));

                System.IO.FileInfo fi = new FileInfo(fileName);

                string directoryUnzip = AppDomain.CurrentDomain.BaseDirectory;
                string pathFile = directoryUnzip + fi.Name.Replace(".zip", "");

                //Don't Delete data file
                try
                {
                    if (fileName.Equals("AppAutoUpdate.exe")
                        || fileName.Equals("ECS.AutoUpdater.dll"))
                    {
                        if (System.IO.File.Exists(pathFile))
                        {
                            System.IO.File.Delete(pathFile);
                        }
                    }
                }
                catch (Exception e) { Logger.LocalLogger.Instance().WriteMessage("Delete file in Company.KDT.SHARE.Components.DownloadUpdate.Unzip:\r\n" + pathFile, e); }


                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("unzip.exe",
                                                            "-o \"" + fi.FullName + "\" -d \"" + directoryUnzip);
                psi.CreateNoWindow = true;
                psi.ErrorDialog = true;
                psi.UseShellExecute = false;
                System.Diagnostics.Process p = System.Diagnostics.Process.Start(psi);
                p.WaitForExit();
                File.SetAttributes(fileName, FileAttributes.Archive);
                File.Delete(fi.FullName);
                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return false; }
        }

        private void ProcDownload(object o)
        {
            try
            {
                //Kiểm tra trong TempFolderAutoUpdate nếu có file AppAutoUpdate.exe.zip, ECS.AutoUpdater.dll.zip thì copy và giải nén thay thế file cũ.                
                string appA = AppDomain.CurrentDomain.BaseDirectory + "TempFolderAutoUpdate\\AppAutoUpdate.exe.zip";
                if (System.IO.File.Exists(appA))
                {
                    UnZip(appA);
                }

                appA = AppDomain.CurrentDomain.BaseDirectory + "TempFolderAutoUpdate\\ECS.AutoUpdater.dll.zip";
                if (System.IO.File.Exists(appA))
                {
                    UnZip(appA);
                }

                //TODO: Delete TempFolderAutoUpdate
                if (System.IO.Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TempFolderAutoUpdate")))
                    Directory.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TempFolderAutoUpdate"), true);

                /*=========================================================================================================================*/
                if (!System.IO.File.Exists(cfg)) Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("Không tồn tại tệp tin {0}", cfg)));
                XmlDocument docVersionOld = new XmlDocument();
                XmlDocument docVersionNew = new XmlDocument();
                string serverLink = string.Empty;

                docVersionOld.Load(cfg);

                List<string> filesDownload = new List<string>();

                XmlNode nodeVersionOld = docVersionOld.SelectSingleNode("Config/ServerUrl");
                if (nodeVersionOld == null)
                {
                    Logger.LocalLogger.Instance().WriteMessage("Chưa có Server link AutoUpdate", new Exception(""));
                    return;
                }

                serverLink = nodeVersionOld.InnerText;

                WebClient client = new WebClient();
                client.Proxy = WebRequest.DefaultWebProxy;
                client.Proxy.Credentials = CredentialCache.DefaultCredentials;
                client.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string sfmtXml = string.Empty;
                string serverInf = "ServerInfo.pdb";
                if (File.Exists(serverInf))
                    File.Delete(serverInf);
                client.DownloadFile(serverLink, serverInf);
                docVersionNew.Load(serverInf);
                if (File.Exists(serverInf))
                    File.Delete(serverInf);
                XmlNode nodeVersionNew = docVersionNew.SelectSingleNode("UpdateFileList");
                if (nodeVersionNew == null) return;
                serverLink = nodeVersionNew.Attributes["url"].Value;

                nodeVersionOld = docVersionOld.SelectSingleNode("Config/UpdateFileList");

                if (nodeVersionOld == null || nodeVersionOld.InnerText == "")
                {
                    //Logger.LocalLogger.Instance().WriteMessage("file auto chua co trong config", new Exception(""));
                    filesDownload.Add("AppAutoUpdate.exe.zip");
                    filesDownload.Add("ECS.AutoUpdater.dll.zip");
                    filesDownload.Add("Logger.dll.zip");
                }
                else
                {
                    string file = getPath(docVersionOld, docVersionNew, "AppAutoUpdate.exe.zip");
                    if (!string.IsNullOrEmpty(file))
                    {
                        filesDownload.Add(file);
                        Logger.LocalLogger.Instance().WriteMessage("Thêm file: " + file, new Exception(""));
                    }
                    file = getPath(docVersionOld, docVersionNew, "ECS.AutoUpdater.dll.zip");
                    if (!string.IsNullOrEmpty(file))
                    {
                        filesDownload.Add(file);
                        Logger.LocalLogger.Instance().WriteMessage("Thêm file: " + file, new Exception(""));
                    }
                    file = getPath(docVersionOld, docVersionNew, "Logger.dll.zip");
                    if (!string.IsNullOrEmpty(file))
                        filesDownload.Add(file);
                }
                docVersionOld.Save(cfg);
                if (!string.IsNullOrEmpty(dirTemp) && !Directory.Exists(dirTemp))
                    Directory.CreateDirectory(dirTemp);
                evtPerDonwload = new ManualResetEvent(false);

                bool unzipSuccess = false;
                while (!evtDownload.WaitOne(0, false))
                {
                    if (filesDownload.Count == 0) break;
                    string filename = filesDownload[0];
                    clientDownload = new WebClient();
                    clientDownload.Proxy = WebRequest.DefaultWebProxy;
                    clientDownload.Proxy.Credentials = CredentialCache.DefaultCredentials;
                    clientDownload.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    clientDownload.DownloadFileCompleted += (object sender, AsyncCompletedEventArgs e) =>
                    {
                        try
                        {
                            evtPerDonwload.Set();
                        }
                        catch { }
                    };
                    evtPerDonwload.Reset();
                    clientDownload.DownloadFileAsync(new Uri(serverLink + "/" + filename), filename, filename);
                    //Logger.LocalLogger.Instance().WriteMessage("Tải file: " + filename, new Exception(""));
                    evtPerDonwload.WaitOne();
                    clientDownload.Dispose();
                    clientDownload = null;
                    unzipSuccess = UnZip(filename);
                    if (!unzipSuccess)
                        Logger.LocalLogger.Instance().WriteMessage("Lỗi giải nén file: " + filename, new Exception(""));
                    filesDownload.Remove(filename);
                }

                StartUpdate(args);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Lỗi tại method Company.KDT.SHARE.Components.ProcDownload()", ex);
            }
        }

        public static string GetDateRelease()
        {
            try
            {
                if (!System.IO.File.Exists(cfg)) Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("Không tồn tại tệp tin {0}", cfg)));
                XmlDocument localVersion = new XmlDocument();
                localVersion.Load(cfg);

                XmlNode nodeDescription = localVersion.SelectSingleNode("Config/DateRelease");
                if (nodeDescription == null)
                    return "";

                nodeDescription.InnerText = nodeDescription.InnerText.Replace("\r\n", "|");
                string[] arrSlpit = nodeDescription.InnerText.Split(new char[] { '|' });

                string dateRelease = arrSlpit[0]; //.Substring(arrSlpit[0].IndexOf("("), (arrSlpit[0].LastIndexOf(")") + 1) - (arrSlpit[0].IndexOf("(")));

                return Convert.ToDateTime(dateRelease).Year > 1900 ? Convert.ToDateTime(dateRelease).ToString("dd/MM/yyyy") : "";
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return "";
            }
        }
    }
}
