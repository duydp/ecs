/****** Object:  StoredProcedure [dbo].[p_NPLCungUngDaDangKy]    Script Date: 04/02/2013 09:14:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_NPLCungUngDaDangKy]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_NPLCungUngDaDangKy]
GO


/****** Object:  StoredProcedure [dbo].[p_NPLCungUngDaDangKy]    Script Date: 04/02/2013 09:14:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Author,,Huynh Ngoc Khanh>  
-- Create date: <Create Date,,01/09/2012>  
-- Description: <Description,,Theo doi NPL cung ung>  
-- =============================================  
CREATE PROCEDURE [dbo].[p_NPLCungUngDaDangKy]  
@HopDongID int
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 -- p_NPLTuCungUng '0400101556',23,676  
 SET NOCOUNT ON;   
  
--Declare @MaDoanhNghiep varchar(50);  
--set @MaDoanhNghiep='0100101308'  
SELECT Ma,MAX(TenNPL)AS TenNPL,SUM(LuongCung)LuongCung,MAX(DonViTinh) AS DonViTinh,STK,MAX(TKMD_ID) AS TKMD_ID ,MAX(TKCT_ID)AS TKCT_ID
FROM
(SELECT     t_KDT_GC_NguyenPhuLieu.Ma, t_KDT_GC_NguyenPhuLieu.Ten AS TenNPL, t_KDT_GC_NguyenPhuLieu.MaHS, 
                       t_View_KDT_NPLCungUng.LuongCung,
                          (SELECT     Ten
                            FROM          t_HaiQuan_DonViTinh
                            WHERE      (ID = t_KDT_GC_NguyenPhuLieu.DVT_ID)) AS DonViTinh, 
                           CASE WHEN t_View_KDT_NPLCungUng.TKMD_ID > 0 THEN
									(SELECT   
										CASE	WHEN  t_KDT_ToKhaiMauDich.SoToKhai > 0 THEN
												CONVERT(VARCHAR(7), SoToKhai) + '/' + MaLoaiHinh + '/' + CONVERT(VARCHAR(10), NamDK)
												ELSE
												N'Tờ khai chưa được cấp số (ID = '+CONVERT(VARCHAR(6),t_KDT_ToKhaiMauDich.ID)+')'
												END
									FROM    dbo.t_KDT_ToKhaiMauDich
									WHERE      ID = t_View_KDT_NPLCungUng.TKMD_ID) 
							     WHEN	t_View_KDT_NPLCungUng.TKMD_ID = 0 AND t_View_KDT_NPLCungUng.TKCT_ID > 0 THEN
									(SELECT     
										CASE	WHEN tkct.SoToKhai > 0 THEN
												CONVERT(VARCHAR(7), tkct.SoToKhai) + '/' + tkct.MaLoaiHinh + '/' + CONVERT(VARCHAR(4), tkct.NamDK)
												ELSE
												N'Tờ khai chuyển tiếp xuất chưa được cấp số (ID = '+CONVERT(VARCHAR(6),tkct.ID)+')'
												END
									FROM          dbo.t_KDT_GC_ToKhaiChuyenTiep tkct
									WHERE      tkct.ID = t_View_KDT_NPLCungUng.TKCT_ID)
                            END AS STK,
                             t_View_KDT_NPLCungUng.TKMD_ID AS TKMD_ID, t_View_KDT_NPLCungUng.TKCT_ID AS TKCT_ID
FROM         t_KDT_GC_NguyenPhuLieu INNER JOIN
                      t_View_KDT_NPLCungUng ON t_KDT_GC_NguyenPhuLieu.Ma = t_View_KDT_NPLCungUng.MaNguyenPhuLieu
WHERE     (t_KDT_GC_NguyenPhuLieu.HopDong_ID = @HopDongID )) AS t_view_NPLCungUng
GROUP BY STK,Ma
ORDER BY Ma


END  

GO


/****** Object:  Table [dbo].[t_KDT_ToKhaiMauDichBoSung]    Script Date: 02/06/2013 11:32:22 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_ToKhaiMauDichBoSung]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[t_KDT_ToKhaiMauDichBoSung](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[PhienBan] [int] NOT NULL,
	[ChuKySo] [bit] NOT NULL,
	[GhiChu] [nvarchar](250) NULL,
 CONSTRAINT [PK_t_KDT_ToKhaiMauDich_BoSung] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]    Script Date: 02/06/2013 13:45:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]    Script Date: 02/06/2013 13:45:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]    Script Date: 02/06/2013 13:45:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate_ByTKMD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate_ByTKMD]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
FROM
	[dbo].[t_KDT_ToKhaiMauDichBoSung]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
FROM
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[TKMD_ID] = @TKMD_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
FROM
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[TKMD_ID] = @TKMD_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiMauDichBoSung] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[PhienBan] = @PhienBan,
			[ChuKySo] = @ChuKySo,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiMauDichBoSung]
		(
			[TKMD_ID],
			[PhienBan],
			[ChuKySo],
			[GhiChu]
		)
		VALUES 
		(
			@TKMD_ID,
			@PhienBan,
			@ChuKySo,
			@GhiChu
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250)
AS

UPDATE
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
SET
	[TKMD_ID] = @TKMD_ID,
	[PhienBan] = @PhienBan,
	[ChuKySo] = @ChuKySo,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ToKhaiMauDichBoSung]
(
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
)
VALUES 
(
	@TKMD_ID,
	@PhienBan,
	@ChuKySo,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

--Bo sung them
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate_ByTKMD]
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] WHERE [TKMD_ID] = @TKMD_ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiMauDichBoSung] 
		SET
			[PhienBan] = @PhienBan,
			[ChuKySo] = @ChuKySo,
			[GhiChu] = @GhiChu
		WHERE
			[TKMD_ID] = @TKMD_ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiMauDichBoSung]
		(
			[TKMD_ID],
			[PhienBan],
			[ChuKySo],
			[GhiChu]
		)
		VALUES 
		(
			@TKMD_ID,
			@PhienBan,
			@ChuKySo,
			@GhiChu
		)		
	END


GO

UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '8.6'
      ,[Date] = getdate()
      ,[Notes] = ''
      GO
