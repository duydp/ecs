﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.GC
{
    class NodeHopDong
    {	        
        /// <summary>
        /// Số tiếp nhận chứng từ
        /// </summary>
        public const string customsReference = "customsReference";        
        /// <summary>
        /// Loại chứng từ (= 601) 
        /// </summary>        
        public const string issuer = "issuer";
        /// <summary>
        /// Đơn vị khai báo
        /// </summary>
        public const string Agent = "Agent";
        /// <summary>
        /// Đơn vị HQ khai báo
        /// </summary>
        public const string declarationOffice = "declarationOffice";
        /// <summary>
        /// Ngày tiếp nhận chứng từ
        /// </summary>
        public const string acceptance = "acceptance";
        /// <summary>
        /// Chức năng (khai báo = 8, sửa =5)
        /// </summary>
        public const string function = "function";
        /// <summary>
        /// Nơi khai báo
        /// </summary>
        public const string issueLocation = "issueLocation";	
        /// <summary>
        /// Số hợp đồng || Số tham chiếu tờ khai
        /// </summary>
        public const string reference = "reference";
        /// <summary>
        /// Ngày hợp đồng || Ngày khai báo
        /// </summary>
        public const string issue = "issue";
        /// <summary>
        /// Trạng thái (mới = 0; cũ =1) || Trạng thái chứng từ || Loại (xem AgentStauts)
        /// </summary>
        public const string status = "status";
        /// <summary>
        /// Tên người nhận gia công || Tên người thuê gia công || Tên nhóm sản phẩm || Tên đơn vi khai báo || Tên đơn vị XNK
        /// </summary>
        public const string name = "name";
        /// <summary>
        /// Mã người nhận gia công || Mã người thuê gia công || Mã nhóm sản phẩm || Mã đơn vị khai báo || Mã đơn vị XNK
        /// </summary>
        public const string identity = "identity";
        /// <summary>
        /// Người nhận gia công || Đơn vị XNK
        /// </summary>
        public const string Importer = "Importer";
        /// <summary>
        /// Ngày hết hạn hợp đồng
        /// </summary>
        public const string expire = "expire";
        /// <summary>
        /// Ghi chú khác về hợp đồng
        /// </summary>
        public const string AdditionalInformation = "AdditionalInformation";
        /// <summary>
        /// Ghi chú khác về hợp đồng
        /// </summary>
        public const string content = "content";
        /// <summary>
        /// Ghi chu mo ta seqquen
        /// </summary>
        public const string sequence = "sequence";
        /// <summary>
        /// Đồng tiền thanh toán || Nguyên tệ 
        /// </summary>
        public const string CurrencyExchange = "CurrencyExchange";
        /// <summary>
        /// Thông tin sản phẩm || Thông tin nguyên phụ liệu || Thông tin thiết bị || Thông tin hàng mẫu
        /// </summary>
        public const string Commodity = "Commodity";
        /// <summary>
        /// Tên/Mô tả sản phẩm || Tên/Mô tả nguyên phụ liệu || Tên/Mô tả thiết bị || Tên/Mô tả hàng mẫu
        /// </summary>
        public const string description = "description";
        /// <summary>
        /// Mã sản phẩm || Mã nguyên phụ liệu || Mã thiết bị || Mã hàng mẫu
        /// </summary>
        public const string identification = "identification";
        /// <summary>
        /// Mã HS
        /// </summary>
        public const string tariffClassification = "tariffClassification";
        /// <summary>
        /// Thông tin sản phẩm || Thông tin đơn vị tính || Thông tin thiết bị
        /// </summary>
        public const string GoodsMeasure = "GoodsMeasure";
        /// <summary>
        /// Số lượng
        /// </summary>
        public const string quantity = "quantity";
        /// <summary>
        /// Đơn vị tính của sản phẩm || Đơn vị tính của nguyên phụ liệu || Đơn vị tính
        /// </summary>
        public const string measureUnit = "measureUnit";
        /// <summary>
        /// Nước thuê gia công
        /// </summary>
        public const string exportationCountry = "exportationCountry";
        public const string code = "code";
        /// <summary>
        /// Nước nhận gia công
        /// </summary>
        public const string importationCountry = "importationCountry";
        /// <summary>
        /// Nguyên tệ 
        /// </summary>
        public const string CurrencyType = "CurrencyType";
        /// <summary>
        /// Thông tin nước xuất xứ
        /// </summary>
        public const string Origin = "Origin";
        /// <summary>
        /// Nước xuất xứ
        /// </summary>
        public const string originCountry = "originCountry";
        /// <summary>
        /// Phương thức thanh toán
        /// </summary>
        public const string Payment = "Payment";
        /// <summary>
        /// Phương thức thanh toán
        /// </summary>
        public const string method = "method";
        /// <summary>
        /// Trị giá hải quan
        /// </summary>
        public const string unitPrice = "unitPrice";
        /// <summary>
        /// Hợp đồng
        /// </summary>
        public const string ContractDocument = "ContractDocument";
        /// <summary>
        /// Người thuê gia công
        /// </summary>
        public const string Exporter = "Exporter";
        /// <summary>
        /// Trị giá hải quan || Tổng trị giá
        /// </summary>
        public const string customsValue = "customsValue";
        /// <summary>
        /// Địa chỉ người nhận gia công || Địa chỉ người thuê gia công
        /// </summary>
        public const string address = "address";
        /// <summary>
        /// Nhóm sản phẩm
        /// </summary>
        public const string ContractItems = "ContractItems";										
        /// <summary>
        /// Nhóm sản phẩm gia công
        /// </summary>
        public const string Item = "Item";
		/// <summary>
		/// Trị giá sản phẩm
		/// </summary>
        public const string productValue = "productValue";
        /// <summary>
        /// Trị giá tiền công
        /// </summary>
        public const string paymentValue = "paymentValue";
        /// <summary>
        /// Sản phẩm gia công
        /// </summary>
        public const string Products = "Products";
        /// <summary>
        /// Danh mục sản phẩm Gia công
        /// </summary>
        public const string Product = "Product";
        /// <summary>
        /// Mã nhóm sản phẩm
        /// </summary>
        public const string productGroup = "productGroup";
        /// <summary>
        /// Nguyên phụ liệu
        /// </summary>
        public const string Materials = "Materials";
        /// <summary>
        /// Danh mục nguyên phụ liệu
        /// </summary>
        public const string Material = "Material";
        /// <summary>
        /// Thiết bị gia công
        /// </summary>
        public const string Equipments = "Equipments";
        /// <summary>
        /// Danh mục thiết bị tạm nhập Gia công
        /// </summary>
        public const string Equipment = "Equipment";
        /// <summary>
        /// Hàng mẫu
        /// </summary>
        public const string SampleProducts = "SampleProducts";
        /// <summary>
        /// Danh mục hàng mẫu 
        /// </summary>
        public const string SampleProduct = "SampleProduct";
        /// <summary>
        /// Tổng trị giá tiền công
        /// </summary>
        public const string totalPaymentValue = "totalPaymentValue";
        /// <summary>
        /// Tổng trị giá sản phẩm
        /// </summary>
        public const string totalProductValue = "totalProductValue";
    }
}
