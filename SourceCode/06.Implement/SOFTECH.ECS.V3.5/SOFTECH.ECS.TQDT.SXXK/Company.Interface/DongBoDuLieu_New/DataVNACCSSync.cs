﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.BLL.SXXK;

namespace Company.Interface.DongBoDuLieu_New
{
    public class DataVNACCSSync
    {
        public MsgPhanBo msgPB { get; set; }
        public MsgLog msgLog { get; set; }
        public NguyenPhuLieuCollection NplCollection { get; set; }
        public SanPhamCollection SPCollection { get; set; }
        public DinhMucCollection DMCollection { get; set; }


        public DataVNACCSSync (string SoToKhai)
        {
            try
            {
                GetDataLog(SoToKhai);
                LoadNPL(SoToKhai);
                LoadSanPham(SoToKhai);
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        public void LoadNPL( string SoToKhai)
        {
            this.NplCollection = new NguyenPhuLieu().SelectCollectionDynamic(@"Ma IN ( SELECT DISTINCT MaHangHoa FROM t_kdt_vnacc_hangmaudich
  WHERE TKMD_ID = (SELECT TOP 1 ID FROM t_kdt_vnacc_ToKhaiMauDich WHERE SoToKhai like ' "+SoToKhai+"%'))", null);
        }
        public void LoadSanPham(string SoToKhai)
        {
            this.SPCollection = new SanPham().SelectCollectionDynamic(@"Ma IN ( SELECT DISTINCT MaHangHoa FROM t_kdt_vnacc_hangmaudich
  WHERE TKMD_ID = (SELECT TOP 1 ID FROM t_kdt_vnacc_ToKhaiMauDich WHERE SoToKhai like ' " + SoToKhai + "%'))", null);
            if (this.SPCollection != null && this.SPCollection.Count > 0)
            {
                this.DMCollection = new DinhMuc().SelectCollectionDynamic(@"MaSanPham IN ( SELECT DISTINCT MaHangHoa FROM t_kdt_vnacc_hangmaudich
  WHERE TKMD_ID = (SELECT TOP 1 ID FROM t_kdt_vnacc_ToKhaiMauDich WHERE SoToKhai like ' " + SoToKhai + "%'))", null);
            }
        }
       
        private void GetDataLog(string SoToKhai)
        {
            try
            {
                MsgPhanBo phanbo = MsgPhanBo.GetMessThongQuan(SoToKhai);
                if (phanbo != null)
                {
                    MsgLog log = MsgLog.Load(phanbo.Master_ID);
                    if (log != null)
                    {
                        this.msgPB = phanbo;
                        this.msgLog = msgLog;
                    }
                }
                throw new Exception("Không tìm thấy log messages thông quan của tờ khai " + SoToKhai);
            }
            catch (System.Exception ex)
            {
                throw new Exception("Lỗi tìm kiếm Msg Thông quan tờ khai " + SoToKhai + ".(" + ex.Message + ")");
            }

        }
    }
}
