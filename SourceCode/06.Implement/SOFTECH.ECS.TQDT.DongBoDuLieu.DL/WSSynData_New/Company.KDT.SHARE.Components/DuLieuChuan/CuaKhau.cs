using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public class CuaKhau : BaseClass
    {
        public static DataSet SelectAll()
        {
            string query = "SELECT LTRIM(RTRIM(ID)) as ID,Ten,MaCuc,DateCreated,DateModified FROM t_HaiQuan_CuaKhau";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }
        public static void Update(DataSet ds)
        {
            string insert = "Insert INTO t_HaiQuan_CuaKhau VALUES(@Id,@Ten,@MaCuc,@DateCreated,@DateModified)";
            string update = "UPDATE t_HaiQuan_CuaKhau SET Ten = @Ten, MaCuc = @MaCuc, DateModified = @DateModified WHERE Id=@Id";
            string delete = "DELETE FROM t_HaiQuan_CuaKhau WHERE ID = @Id";

            DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaCuc", SqlDbType.NVarChar, "MaCuc", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaCuc", SqlDbType.NVarChar, "MaCuc", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
        public static string GetName(string id)
        {
            try
            {
                //string query = string.Format("SELECT Ten FROM t_HaiQuan_CuaKhau WHERE [ID] = '{0}'", id.PadRight(4));
                //DbCommand dbCommand = db.GetSqlStringCommand(query);
                //IDataReader reader = db.ExecuteReader(dbCommand);
                //if (reader.Read())
                //{
                //    return reader["Ten"].ToString();
                //}

                string query = string.Format("[ID] = '{0}'", id.PadRight(4));

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["CuaKhau"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["Ten"].ToString();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }


    }
}