﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.Messages.GoodsItem;
using Company.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.QuanLyChungTu.CX;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class ThongTinTieuHuySendForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_ScrapInformation scrapInformation;
        public KDT_VNACCS_ScrapInformation_Detail scrapInformation_Detail = new KDT_VNACCS_ScrapInformation_Detail();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
        public bool isAdd = true;
        public ThongTinTieuHuySendForm()
        {
            InitializeComponent();
        }
        private void SetCommandStatus()
        {
            if (scrapInformation.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnSave.Enabled = false;
                btnDelete.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTiepNhan.Text = scrapInformation.SoTiepNhan.ToString();
                dtpNgayTN.Value = scrapInformation.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (scrapInformation.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || scrapInformation.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnSave.Enabled = true;
                btnDelete.Enabled = true;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                dtpNgayTN.Value = DateTime.Now;
                lblTrangThai.Text = scrapInformation.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (scrapInformation.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnSave.Enabled = false;
                btnDelete.Enabled = false;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTiepNhan.Text = scrapInformation.SoTiepNhan.ToString();
                dtpNgayTN.Value = scrapInformation.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (scrapInformation.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || scrapInformation.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || scrapInformation.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnSave.Enabled = false;
                btnDelete.Enabled = false;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = scrapInformation.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã khai báo Sửa / Hủy", "Wait for approval");
                this.OpenType = OpenFormType.View;
            }
            else if (scrapInformation.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnSave.Enabled = true;
                btnDelete.Enabled = true;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;


                txtSoTiepNhan.Text = scrapInformation.SoTiepNhan.ToString();
                dtpNgayTN.Value = scrapInformation.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (scrapInformation.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || scrapInformation.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA || scrapInformation.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {

                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnSave.Enabled = false;
                btnDelete.Enabled = false;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = scrapInformation.SoTiepNhan.ToString();
                dtpNgayTN.Value = scrapInformation.NgayTiepNhan;
                lblTrangThai.Text = scrapInformation.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Not declared") : setText("Chờ duyệt Sửa / Hủy", "Wait for approval");
                this.OpenType = OpenFormType.View;
            }


        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdAdd":
                    this.Add();
                    break;
                case "cmdAddExcel":
                    this.AddExcel();
                    break;
                case "cmdSend":
                    this.SendV5(false);
                    break;
                case "cmdCancel":
                    if (ShowMessage("DOANH NGHIỆP CÓ CHẮC CHẮN MUỐN KHAI BÁO HỦY THÔNG TIN TIÊU HỦY NÀY ĐẾN HQ KHÔNG ? ", true) == "Yes")
                    {
                        scrapInformation.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                        SetCommandStatus();
                        scrapInformation.Update();
                        this.SendV5(true);
                    }
                    break;
                case "cmdFeedback":
                    this.FeedBackV5();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
            }
        }

        private void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_ScrapInformation", "", Convert.ToInt32(scrapInformation.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Result()
        {
            try
            {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.scrapInformation.ID;
            form.DeclarationIssuer = DeclarationIssuer.ScrapInformation;
            form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = scrapInformation.GuidString;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.ScrapInformation,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.ScrapInformation,
                };
                subjectBase.Type = DeclarationIssuer.ScrapInformation;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = scrapInformation.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(scrapInformation.MaHaiQuan.Trim())),
                                                  Identity = scrapInformation.MaHaiQuan
                                              }, subjectBase, null);
                if (scrapInformation.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(scrapInformation.MaHaiQuan));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                    {
                        scrapInformation.Update();
                        isFeedBack = true;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        scrapInformation.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        scrapInformation.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG", true) == "Yes";
                    }
                }
            }
        }

        private void SendV5(bool IsCancel)
        {
            if (ShowMessage("DOANH NGHIỆP CÓ MUỐN KHAI PHỤ KIỆN NÀY ĐẾN HQ KHÔNG ?", true) == "Yes")
            {
                if (scrapInformation.ID == 0)
                {
                    ShowMessage("DOANH NGHIỆP HÃY LƯU THÔNG TIN TRƯỚC KHI KHAI BÁO ", false);
                    return;
                }
                else
                {
                    scrapInformation.ScrapInformationCollection = KDT_VNACCS_ScrapInformation_Detail.SelectCollectionBy_Scrap_ID(scrapInformation.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ScrapInformation;
                sendXML.master_id = scrapInformation.ID;

                if (sendXML.Load())
                {
                    if (scrapInformation.TrangThaiXuLy==TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                    {
                        ShowMessage("THÔNG TIN ĐÃ ĐƯỢC GỬI ĐẾN HẢI QUAN. NHẤN [LẤY PHẢN HỒI] ĐỂ LẤY THÔNG TIN", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    scrapInformation.GuidString = Guid.NewGuid().ToString();
                    ScrapInformation_VNACCS scrapInformation_VNACCS = new ScrapInformation_VNACCS();
                    scrapInformation_VNACCS = Mapper_V4.ToDataTransferScrapInformation(scrapInformation,scrapInformation_Detail , GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = scrapInformation.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(scrapInformation.MaHaiQuan)),
                              Identity = scrapInformation.MaHaiQuan
                          },
                          new SubjectBase()
                          {

                              Type = scrapInformation_VNACCS.Issuer,
                              //Function = DeclarationFunction.KHAI_BAO,
                              Reference = scrapInformation.GuidString,
                          },
                          scrapInformation_VNACCS
                        );
                    if (!IsCancel)
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        scrapInformation.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        scrapInformation.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(scrapInformation.ID, MessageTitle.RegisterScrapInformation);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.ScrapInformation;
                        sendXML.master_id = scrapInformation.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            scrapInformation.Update();
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                            FeedBackV5();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            SetCommandStatus();
                            FeedBackV5();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            scrapInformation.Update();
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            ShowMessageTQDT(msgInfor, false);
                            FeedBackV5();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        sendForm.Message.XmlSaveMessage(scrapInformation.ID, MessageTitle.RegisterScrapInformation);
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.ScrapInformationSendHandler(scrapInformation, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void AddExcel()
        {
            try
            {
                ThongTinTieuHuyReadExcelForm form = new ThongTinTieuHuyReadExcelForm();
                form.scrapInformation = scrapInformation;
                form.ShowDialog(this);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Add()
        {
            throw new NotImplementedException();
        }

        private void Save()
        {
            try
            {
                if (scrapInformation.ID == 0)
                    scrapInformation.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                scrapInformation.MaDoanhNghiep = txtMaDoanhNghiep.Text;
                scrapInformation.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text);
                scrapInformation.NgayTiepNhan = dtpNgayTN.Value;
                scrapInformation.MaHaiQuan = ctrCoQuanHaiQuan.Code;
                scrapInformation.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                scrapInformation.InsertUpdateFull();
                ShowMessage("LƯU THÀNH CÔNG ",false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void ThongTinTieuHuySendForm_Load(object sender, EventArgs e)
        {
            if (scrapInformation==null || scrapInformation.ID==0)
            {
                txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
                txtTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
                dtpThoiGian.Value = DateTime.Now;
                ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                scrapInformation.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            }
            else
            {
                txtSoTiepNhan.Text = scrapInformation.SoTiepNhan.ToString();
                dtpNgayTN.Value = scrapInformation.NgayTiepNhan;
                txtMaDoanhNghiep.Text = scrapInformation.MaDoanhNghiep;
                txtTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
                ctrCoQuanHaiQuan.Code = scrapInformation.MaHaiQuan;
                scrapInformation.ScrapInformationCollection = KDT_VNACCS_ScrapInformation_Detail.SelectCollectionBy_Scrap_ID(scrapInformation.ID);
                BindData();
                //dtpThoiGian.Value = scrapInformation.th
            }
            SetCommandStatus();
        }

        private void txtMaHangHoa_Leave(object sender, EventArgs e)
        {
            try
            {
                if (cbbLoaiHangHoa.SelectedValue.ToString() == "1" && Company.KDT.SHARE.Components.Globals.LaDNCX)
                {
                    IList<HangDuaVao> listTB = HangDuaVao.SelectCollectionDynamic(string.Format("Ma = '{0}' and LoaiNPL = '3'", txtMaHangHoa.Text.Trim()), null);
                    if (listTB != null && listTB.Count > 0)
                    {
                        HangDuaVao tb = listTB[0];
                        txtTenHangHoa.Text = tb.Ten;
                        //ctrMaSoHang.Code = tb.MaHS;
                    }
                    else
                    {
                    }
                }
                else if (cbbLoaiHangHoa.SelectedValue.ToString() == "1")
                {
                    Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                    npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    npl.Ma = txtMaHangHoa.Text;
                    if (npl.Load())
                    {
                        txtMaHangHoa.Text = npl.Ma;
                        ctrDVTLuong1.Code = DVT_VNACC(npl.DVT_ID.PadRight(3));
                        txtTenHangHoa.Text = npl.Ten;


                    }
                }
                else if (cbbLoaiHangHoa.SelectedValue.ToString() == "2")
                {
                    Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                    sp.Ma = txtMaHangHoa.Text;
                    sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    if (sp.Load())
                    {
                        txtMaHangHoa.Text = sp.Ma;
                        ctrDVTLuong1.Code = DVT_VNACC(sp.DVT_ID.PadRight(3));
                        txtTenHangHoa.Text = sp.Ten;

                    }
                }
                else
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN LOẠI HÀNG HÓA", false);
                }
            }
            catch (Exception ex)
            {
                ShowMessage("DOANH NGHIỆP CHƯA CHỌN LOẠI HÀNG HÓA", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;
                if (cbbLoaiHangHoa.SelectedValue.ToString() == "1" && Company.KDT.SHARE.Components.Globals.LaDNCX)
                {

                    if (this.NPLRegistedForm_CX == null)
                        this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                    this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                    this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.NPLRegistedForm_CX.LoaiNPL = "3";
                    this.NPLRegistedForm_CX.ShowDialog(this);
                    if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                    {
                        txtMaHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                        txtTenHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                        //ctrMaSoHang.Code = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

                    }

                }
                else if (cbbLoaiHangHoa.SelectedValue.ToString() == "1")
                {
                    if (this.NPLRegistedForm == null)
                        this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.NPLRegistedForm.isBrowser = true;
                    this.NPLRegistedForm.ShowDialog(this);
                    if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                    {
                        txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtTenHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        //ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

                    }
                }
                else if (cbbLoaiHangHoa.SelectedValue.ToString() == "2")
                {
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.CalledForm = "HangMauDichForm";
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                    {
                        txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtTenHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        //ctrMaSoHang.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                    }
                }
                else
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN LOẠI LOẠI HÀNG HÓA ", false);
                }
            }
            catch (Exception ex)
            {
                ShowMessage("DOANH NGHIỆP CHƯA CHỌN LOẠI LOẠI HÀNG HÓA ", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMaHangHoa, errorProvider, "MÃ HÀNG HÓA ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenHangHoa, errorProvider, "TÊN HÀNG HÓA ", isOnlyWarning);
                ctrDVTLuong1.SetValidate = !isOnlyWarning; ctrDVTLuong1.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrDVTLuong1.IsValidate;
                isValid &= ValidateControl.ValidateNull(txtSoLuong1, errorProvider, "SỐ LƯỢNG TIÊU HỦY", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHangHoa, errorProvider, "LOẠI HÀNG HÓA", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void BindData()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = scrapInformation.ScrapInformationCollection;
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (scrapInformation==null)
                {
                    scrapInformation = new KDT_VNACCS_ScrapInformation();
                }
                scrapInformation_Detail.TenHangHoa = txtTenHangHoa.Text;
                scrapInformation_Detail.MaHangHoa = txtMaHangHoa.Text;
                scrapInformation_Detail.DVT = ctrDVTLuong1.Code;
                scrapInformation_Detail.LoaiHangHoa = Convert.ToDecimal(cbbLoaiHangHoa.SelectedValue);
                scrapInformation_Detail.SoLuong = Convert.ToDecimal(txtSoLuong1.Text);
                if(isAdd)
                    scrapInformation.ScrapInformationCollection.Add(scrapInformation_Detail);
                BindData();
                isAdd = true;
                scrapInformation_Detail = new KDT_VNACCS_ScrapInformation_Detail();
                txtTenHangHoa.Text = String.Empty;
                txtMaHangHoa.Text = String.Empty;
                txtSoLuong1.Text = String.Empty;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            scrapInformation_Detail = (KDT_VNACCS_ScrapInformation_Detail)i.GetRow().DataRow;
                            scrapInformation.ScrapInformationCollection.Remove(scrapInformation_Detail);
                            if (scrapInformation_Detail.ID > 0)
                            {
                                scrapInformation_Detail.Delete();
                            }
                        }
                    }
                    scrapInformation_Detail = new KDT_VNACCS_ScrapInformation_Detail();
                    ShowMessage("XÓA THÀNH CÔNG", false);
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                string LoaiHangHoa = e.Row.Cells["LoaiHangHoa"].Value.ToString();
                if (LoaiHangHoa == "1")
                {
                    e.Row.Cells["LoaiHangHoa"].Text = "Nguyên phụ liệu";
                }
                else
                {
                    e.Row.Cells["LoaiHangHoa"].Text = "Sản phẩm";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                scrapInformation_Detail = new KDT_VNACCS_ScrapInformation_Detail();
                scrapInformation_Detail = (KDT_VNACCS_ScrapInformation_Detail)dgList.GetRow().DataRow;
                txtMaHangHoa.Text = scrapInformation_Detail.MaHangHoa;
                txtTenHangHoa.Text = scrapInformation_Detail.TenHangHoa;
                ctrDVTLuong1.Code = scrapInformation_Detail.DVT;
                cbbLoaiHangHoa.SelectedValue = scrapInformation_Detail.LoaiHangHoa.ToString();
                txtSoLuong1.Text = scrapInformation_Detail.SoLuong.ToString();
                isAdd = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            scrapInformation_Detail = new KDT_VNACCS_ScrapInformation_Detail();
                            scrapInformation_Detail = (KDT_VNACCS_ScrapInformation_Detail)i.GetRow().DataRow;
                            txtMaHangHoa.Text = scrapInformation_Detail.MaHangHoa;
                            txtTenHangHoa.Text = scrapInformation_Detail.TenHangHoa;
                            ctrDVTLuong1.Code = scrapInformation_Detail.DVT;
                            cbbLoaiHangHoa.SelectedValue = scrapInformation_Detail.LoaiHangHoa.ToString();
                            txtSoLuong1.Text = scrapInformation_Detail.SoLuong.ToString();
                            isAdd = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            } 
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH HÀNG HÓA KHAI BÁO TIÊU HỦY.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
