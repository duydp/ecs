/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
 * Changed and add new LanNT@Softech.vn 15/09/2011
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using ECS.AutoUpdater;

namespace ECS.AutoUpdate
{
    #region The delegate
    public delegate void ShowHandler();
    #endregion

    public class AutoUpdater : IAutoUpdater
    {
        #region The private fields
        private Config config = null;
        List<DownloadFileInfo> downloadFileListTemp = null;
        #endregion

        #region The public event
        public event ShowHandler OnShow;
        #endregion

        #region The constructor of AutoUpdater
        public AutoUpdater()
        {
            config = Config.LoadConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConstFile.FILENAME));
        }
        #endregion
        private void KillProcess(string[] arrProc)
        {
            foreach (string strProcname in arrProc)
            {
                string strTemp = strProcname.ToLower().Trim().Replace(".exe", string.Empty);
                foreach (Process process in Process.GetProcesses())
                {
                    if (process.ProcessName.ToLower() == strTemp)
                    {
                        process.Kill();
                        break;
                    }
                }
            }
        }
        private bool ExistProcess(string procName)
        {

            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName(procName.Replace(".exe", string.Empty));
            return process.Length > 0;

        }
        #region The public method
        public void Update(string arg)
        {
            if (!Config.Install.Enabled)
            {
                if (!string.IsNullOrEmpty(arg))
                {
                    DialogResult dlgRet = MessageBox.Show("Chức năng tự động cập nhật đã bị tắt\nBạn có muốn kích hoạt không?", "Xác nhận", MessageBoxButtons.YesNo);
                    if (dlgRet == DialogResult.Yes)
                    {
                        Config.Install.Enabled = true;
                        config.Save();
                    }
                    else return;
                }
                else
                {
                    //MessageBox.Show("Chức năng tự động cập nhật phần mềm đã bị tắt\nHãy thiết lập lại cấu hình", "Thông báo", MessageBoxButtons.OK);
                    //return;
                    DialogResult dlgRet = MessageBox.Show("Chức năng tự động cập nhật đã bị tắt\nBạn có muốn kích hoạt không?", "Xác nhận", MessageBoxButtons.YesNo);
                    if (dlgRet == DialogResult.Yes)
                    {
                        Config.Install.Enabled = true;
                        config.Save();
                    }
                    else return;
                }

            }
            // Kill Process
            //string[] arrProc = new string[]{
            //        "SOFTECH.ECS.TQDT.SXXK.exe",
            //        "SOFTECH.ECS.TQDT.GC.exe",
            //        "SOFTECH.ECS.TQDT.KD.exe",
            //        "MiniSqlQuery.exe"
            //    };
            //foreach (string strProcName in arrProc)
            //{
            //    if (ExistProcess(strProcName))
            //    {
            //        KillProcess(arrProc);
            //        break;
            //    }
            //}
            //if (string.IsNullOrEmpty(arg))
            //{
            //    List<string> fileNames = new List<string>();
            //    fileNames.Add("Company.Controls.CustomValidation.dll");
            //    fileNames.Add("Company.BLL.dll");
            //    fileNames.Add("Company.GC.BLL.dll");
            //    fileNames.Add("Company.KD.BLL.dll");
            //    fileNames.Add("Company.KDT.SHARE.Components.dll");
            //    fileNames.Add("Company.KDT.SHARE.QuanLyChungTu.dll");
            //    fileNames.Add("Company.KDT.SHARE.VNACCS.Controls.dll");
            //    fileNames.Add("Company.KDT.SHARE.VNACCS.dll");
            //    fileNames.Add("SOFTECH.ECS.TQDT.GC.exe");
            //    fileNames.Add("SOFTECH.ECS.TQDT.KD.exe");
            //    fileNames.Add("SOFTECH.ECS.TQDT.SXXK.exe");
            //    foreach (var item in fileNames)
            //    {
            //        string path = System.Windows.Forms.Application.StartupPath + "\\" + item;
            //        try
            //        {
            //            System.IO.File.Delete(path);
            //        }
            //        catch (Exception ex)
            //        {
            //            Logger.LocalLogger.Instance().WriteMessage(ex);
            //        }
            //    }
            //}
            string strNote = string.Empty;
            //wf.SetCaption("Kiểm tra kết nối máy chủ");
            Dictionary<string, RemoteFile> listRemotFile = ParseRemoteXml(config.ServerUrl);

            //wf.SetCaption("Phân tích kiểm tra tệp tin máy bạn");
            List<DownloadFileInfo> downloadList = new List<DownloadFileInfo>();
            List<LocalFile> localRemove = new List<LocalFile>();
            foreach (LocalFile file in config.UpdateFileList)
            {
                // wf.SetCaption(file.Path);
                if (listRemotFile.ContainsKey(file.Path))
                {
                    RemoteFile rf = listRemotFile[file.Path];
                    Version v1 = new Version(rf.LastVer);
                    Version v2 = new Version(file.LastVer);
                    FileInfo fi = new FileInfo(file.Path.Replace(".zip", string.Empty));
                    string fiLocal = file.Path.Replace(".zip", string.Empty);
                    //string[] fiNotIns = new string[2]{
                    //                                "AppAutoUpdate.exe","ECS.AutoUpdater.dll"
                    //                                };
                    //if(fiNotIns.Cou
                    if (File.Exists(fiLocal))
                    {
                        FileVersionInfo fvInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(fiLocal);
                        if (!string.IsNullOrEmpty(fvInfo.ProductVersion) && fiLocal.ToLower() != "Logger.dll".ToLower())
                        {
                            Version vCurrent = new Version(fvInfo.ProductVersion);
                            if (vCurrent != v2)
                            {
                                //Neu version local > server
                                if (vCurrent.Build <= v2.Build && vCurrent.Revision < v2.Revision)
                                {
                                    v2 = vCurrent;
                                    strNote += "\r\nTệp tin lỗi thời:  " + fiLocal;
                                }
                            }
                        }
                    }
                    if (v1 != v2 || rf.Size != file.Size)
                    {
                        if (rf.IsDatabase && !string.IsNullOrEmpty(UpdateSql.VersionDataBase()))
                        {
                            GetDataBaseVersion(downloadList, file, rf);
                        }
                        else
                        {
                            downloadList.Add(new DownloadFileInfo(rf.Url, file.Path, rf.LastVer, rf.Size, rf.IsDatabase, rf.NeedRestart));
                            file.LastVer = rf.LastVer;
                            file.Size = rf.Size;
                        }
                    }
                    else if (rf.IsDatabase)
                    {
                        GetDataBaseVersion(downloadList, file, rf);
                    }
                    else
                    {
                        DownloadFileInfo fileInfo = new DownloadFileInfo(rf.Url, file.Path, rf.LastVer, rf.Size, rf.IsDatabase, rf.NeedRestart);
                        string dir = CommonUnitity.GetSubPath(fileInfo, CommonUnitity.SystemBinUrl);
                        if (!File.Exists(dir + fileInfo.FileName.Replace(".zip", string.Empty)))
                        {
                            strNote += "\r\nMáy bạn thiếu tệp tin: " + fileInfo.FileName.Replace(".zip", string.Empty);
                            downloadList.Add(fileInfo);
                        }
                    }
                    if (rf.NeedRestart)
                        file.NeedRestart = rf.NeedRestart;
                    listRemotFile.Remove(file.Path);
                }
                else
                    localRemove.Add(file);

            }
            // wf.SetCaption("Rà soát những thông tin còn thiếu");
            foreach (LocalFile item in localRemove)
            {
                config.UpdateFileList.Remove(item);
            }
            //  wf.SetCaption("Bổ sung những thông tin còn thiếu");
            foreach (RemoteFile file in listRemotFile.Values)
            {
                config.UpdateFileList.Add(new LocalFile(file.Path, file.LastVer, file.Size, file.NeedRestart));
                downloadList.Add(new DownloadFileInfo(file.Url, file.Path, file.LastVer, file.Size, file.IsDatabase, file.NeedRestart));
            }

            downloadFileListTemp = downloadList;
            // wf.Close();
            if (downloadList.Count > 0)
            {
                //StartDownload(downloadList);
                //NeedRestart();
                DownloadConfirm dc = new DownloadConfirm(downloadList, strNote);

                if (this.OnShow != null)
                    this.OnShow();
                //bool isDownload = true;
                dc.DialogResult = DialogResult.OK;
                bool isDownload = (!string.IsNullOrEmpty(arg) && arg == "AutoRun")
                    || (DialogResult.OK == dc.ShowDialog());
                if (isDownload && StartDownload(downloadList))
                    NeedRestart();
            }
            else if (arg != string.Empty)
            {
                new DownloadComplete("Bạn đang dùng phiên bản mới nhất.\nVui lòng cập nhật phần mềm sau").ShowDialog();
            }

        }

        private void GetDataBaseVersion(List<DownloadFileInfo> downloadList, LocalFile file, RemoteFile rf)
        {
            try
            {
                //Note: Format version database is FileName_V1.2.3.sql.zip
                //Reasion: File database don't check assembly version
                //Version database from file is 1.2.3

                int posVersion = rf.Path.IndexOf("_V");
                string version = rf.Path.Replace(".sql", string.Empty).Replace(".zip", string.Empty);
                version = version.Remove(0, posVersion + 2);
                //if (string.Compare(UpdateSql.VersionDataBase(), version) < 0)
                //if (string.Compare(System.Convert.ToDecimal(UpdateSql.VersionDataBase()).ToString("##0.0#"), System.Convert.ToDecimal(version).ToString("##0.0#")) < 0) //TODO: Hungtq, 14/11/2013
                if (System.Convert.ToDecimal(version) > System.Convert.ToDecimal(UpdateSql.VersionDataBase()))
                    downloadList.Add(new DownloadFileInfo(rf.Url, file.Path, rf.LastVer, rf.Size, rf.IsDatabase, rf.NeedRestart));

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("ECS.AutoUpdate.AutoUpdater.GetDataBaseVersion", ex); }
        }
        private void NeedRestart()
        {
            foreach (LocalFile file in config.UpdateFileList)
            {
                if (file.NeedRestart)
                {
                    try
                    {
                        System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(AppDomain.CurrentDomain.BaseDirectory + file.Path.Replace("/", "\\").Replace(".zip", string.Empty), string.Empty);
                        psi.CreateNoWindow = false;
                        psi.ErrorDialog = true;
                        psi.UseShellExecute = false;
                        System.Diagnostics.Process.Start(psi);
                    }
                    catch { }
                }
            }
        }
        public void RollBack()
        {
            foreach (DownloadFileInfo file in downloadFileListTemp)
            {
                string tempUrlPath = CommonUnitity.GetFolderUrl(file);
                string oldPath = string.Empty;
                try
                {
                    if (!string.IsNullOrEmpty(tempUrlPath))
                    {
                        oldPath = Path.Combine(CommonUnitity.SystemBinUrl + tempUrlPath.Substring(1), file.FileName);
                    }
                    else
                    {
                        oldPath = Path.Combine(CommonUnitity.SystemBinUrl, file.FileName);
                    }

                    if (oldPath.EndsWith("_"))
                        oldPath = oldPath.Substring(0, oldPath.Length - 1);

                    MoveFolderToOld(oldPath + ".old", oldPath);

                }
                catch
                {
                }
            }
        }


        #endregion

        #region The private method
        string newfilepath = string.Empty;
        private void MoveFolderToOld(string oldPath, string newPath)
        {
            if (File.Exists(oldPath) && File.Exists(newPath))
            {
                System.IO.File.Copy(oldPath, newPath, true);
            }
        }

        private bool StartDownload(List<DownloadFileInfo> downloadList)
        {
            DownloadProgress dp = new DownloadProgress(downloadList);
            dp.DialogResult = DialogResult.OK;
            if (dp.ShowDialog() == DialogResult.OK)
            {

                //Update successfully
                config.DateRelease = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                config.SaveConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConstFile.FILENAME));

                //Kiểm tra trong FolderTemp nếu có file AppAutoUpdate.exe.zip, ECS.AutoUpdater.dll.zip thì copy và giải nén thay thế file cũ.
                //Copy 2 file sang thu muc luu tam                
                if (!System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "TempFolderAutoUpdate"))
                    System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "TempFolderAutoUpdate");

                string sourceFile = AppDomain.CurrentDomain.BaseDirectory + ConstFile.TEMPFOLDERNAME + "\\AppAutoUpdate.exe.zip";
                string destinateFile = AppDomain.CurrentDomain.BaseDirectory + "TempFolderAutoUpdate\\AppAutoUpdate.exe.zip";

                if (System.IO.File.Exists(destinateFile))
                {
                    System.IO.File.Delete(destinateFile);
                    System.IO.File.Copy(sourceFile, destinateFile);
                    //Logger.LocalLogger.Instance().WriteMessage("Copy file: " + sourceFile, new Exception(""));
                }

                sourceFile = AppDomain.CurrentDomain.BaseDirectory + ConstFile.TEMPFOLDERNAME + "\\AppAutoUpdate.exe.zip";
                destinateFile = AppDomain.CurrentDomain.BaseDirectory + "TempFolderAutoUpdate\\ECS.AutoUpdater.dll.zip";
                if (System.IO.File.Exists(destinateFile))
                {
                    System.IO.File.Delete(destinateFile);
                    System.IO.File.Copy(sourceFile, destinateFile);
                    //Logger.LocalLogger.Instance().WriteMessage("Copy file: " + sourceFile, new Exception(""));
                }

                //TODO: Delete TEMPFOLDERNAME
                if (System.IO.Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConstFile.TEMPFOLDERNAME)))
                    Directory.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConstFile.TEMPFOLDERNAME), true);

                if (System.IO.File.Exists(ConstFile.FILEDOWNLOADOLD))
                    System.IO.File.Delete(ConstFile.FILEDOWNLOADOLD);

                return DialogResult.OK == new DownloadComplete().ShowDialog();
            }
            return false;
        }

        private Dictionary<string, RemoteFile> ParseRemoteXml(string xml)
        {

            string sfmtXml = string.Empty;
            try
            {
                WebClient client = new WebClient();
                client.Proxy = WebRequest.DefaultWebProxy;
                client.Proxy.Credentials = CredentialCache.DefaultCredentials;
                client.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string serverInf = "ServerInfo.pdb";
                if (File.Exists(serverInf))
                    File.Delete(serverInf);
                client.DownloadFile(xml, serverInf);
                XmlDocument doc = new XmlDocument();
                doc.Load(serverInf);
                sfmtXml = doc.InnerXml;
                if (File.Exists(serverInf))
                    File.Delete(serverInf);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
            if (sfmtXml == string.Empty)
            {
                return new Dictionary<string, RemoteFile>();
            }

            XmlDocument document = new XmlDocument();
            document.Load(xml);
            string defaultPath = "http://ecs.softech.cloud/autoupdate/SOFTECH_ECS_TQDT_GC_V4/ListUpdate.xml";
            if (document.DocumentElement.Attributes["url"] != null)
                defaultPath = document.DocumentElement.Attributes["url"].Value;
            Dictionary<string, RemoteFile> list = new Dictionary<string, RemoteFile>();
            XmlNode nodeDescription = document.DocumentElement.FirstChild;

            Config.Install.Description = nodeDescription.InnerText.Replace("\n", "\r\n");
            Config.Install.TitleUpdate = nodeDescription.Attributes["Title"].Value;
            /*if (nodeDescription.Name.Equals("Desctiption"))*/
            document.DocumentElement.RemoveChild(nodeDescription);

            foreach (XmlNode node in document.DocumentElement.ChildNodes)
            {
                list.Add(node.Attributes["path"].Value, new RemoteFile(node, defaultPath));
            }

            return list;
        }
        #endregion

    }

}
