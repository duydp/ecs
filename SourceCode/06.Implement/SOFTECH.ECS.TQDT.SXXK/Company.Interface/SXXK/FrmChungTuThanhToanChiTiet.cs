﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;
using Company.Interface.SXXK;
using Company.BLL.KDT.SXXK.CTTT;

namespace Company.Interface
{
    public partial class FrmChungTuThanhToanChiTiet : Company.Interface.BaseForm
    {
        public ChungTu ChungTuThanhToan;

        public FrmChungTuThanhToanChiTiet()
        {
            InitializeComponent();
        }

        private void FrmChungTuThanhToanChiTiet_Load(object sender, EventArgs e)
        {
            try
            {
                numLanThanhLy.Enabled = false;

                // Phương thức thanh toán.
                cboHinhThucThanhToan.DataSource = Company.BLL.DuLieuChuan.PhuongThucThanhToan.SelectAll().Tables[0];
                cboHinhThucThanhToan.DisplayMember = cboHinhThucThanhToan.ValueMember = "ID";
                cboHinhThucThanhToan.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

                if (cboHinhThucThanhToan.Items.Count > 0)
                    cboHinhThucThanhToan.SelectedIndex = 0;

                // Loại .
                DataTable dtChiPhiKhac = Company.BLL.DuLieuChuan.LoaiPhiChungTuThanhToan.SelectAll().Tables[0];
                dgChiPhi.DataSource = dtChiPhiKhac;
                DataTable dtChiPhi = dtChiPhiKhac.Clone();


                DataColumn triGia = new DataColumn("TriGia");
                triGia.DataType = typeof(decimal);
                dtChiPhi.Columns.Add(triGia);


                foreach (ChiPhiKhac item in ChungTuThanhToan.ChiPhiKhacCollections)
                {
                    DataRow dr = dtChiPhi.NewRow();
                    dr[0] = item.MaChiPhi;
                    dr[2] = item.TriGia;
                    dtChiPhi.Rows.Add(dr);
                    DataRow[] arrDr = dtChiPhiKhac.Select("ID='" + item.MaChiPhi + "'");
                    foreach (DataRow drItem in arrDr)
                    {
                        dr[1] = drItem[1];
                        dtChiPhiKhac.Rows.Remove(drItem);
                    }
                }

                dgChiPhiDuocChon.Tables[0].Columns["TriGia"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
                dgChiPhiDuocChon.Tables[0].Columns["TriGia"].InputMask = "Number" + GlobalSettings.SoThapPhan.DinhMuc;
                dgChiPhiDuocChon.Tables[0].Columns["TriGia"].TotalFormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;


                //
                //ChungTuThanhToan.ChungTuChiTietCollections;
                dgChiPhiDuocChon.DataSource = dtChiPhi;


                numLanThanhLy.Value = HSTL.LanThanhLy;

                InitialDSTK();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void BK02Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.bkTKXCollection = this.bkTKXLucDauCollection;
            this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].bkTKXColletion = this.bkTKXCollection;
        }

        #region Chon to khai

        public BKToKhaiXuatCollection bkTKXLucDauCollection = new BKToKhaiXuatCollection();
        public BKToKhaiXuatCollection bkTKXCollection = new BKToKhaiXuatCollection();
        public ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();

        private void InitialDSTK()
        {
            try
            {

                dgTKXTL.DeletingRecord += new RowActionCancelEventHandler(dgTKXTL_DeletingRecord);
                dgTKXTL.DeletingRecords += new CancelEventHandler(dgTKXTL_DeletingRecords);
                dgTKXTL.LoadingRow += new RowLoadEventHandler(dgTKXTL_LoadingRow);
                dgTKXTL.RowDoubleClick += new RowActionEventHandler(dgTKXTL_RowDoubleClick);

                dgTKX.LoadingRow += new RowLoadEventHandler(dgTKX_LoadingRow);
                dgTKX.RowDoubleClick += new RowActionEventHandler(dgTKX_RowDoubleClick);

                //dgTKXTL.RootTable.Columns["NgayThucXuat"].DefaultValue = DateTime.Today;

                dgTKXTL.ColumnAutoResize = true;
                dgTKXTL.ColumnAutoSizeMode = ColumnAutoSizeMode.Default;
                dgTKXTL.Tables[0].Columns["TriGia"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
                dgTKXTL.Tables[0].Columns["TriGia"].InputMask = "Number" + GlobalSettings.SoThapPhan.DinhMuc;
                dgTKXTL.Tables[0].Columns["TriGia"].TotalFormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;



                if (this.HSTL.TrangThaiThanhKhoan == 401 || this.HSTL.SoTiepNhan == 1)
                {
                    //btnAdd.Enabled = btnDelete.Enabled = false;
                    //btnSave.Enabled = false;
                    dgTKXTL.AllowDelete = InheritableBoolean.False;
                }

                int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);
                DateTime fromDate = dtFromDate.Value = new DateTime(System.DateTime.Today.Year, 1, 1);
                DateTime toDate = dtToDate.Value = System.DateTime.Today;

                //if (fromDate < this.getMaxDateBKTKN().AddDays(chenhLech))
                //    fromDate = this.getMaxDateBKTKN().AddDays(0 - chenhLech);

                this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKXChuaThanhLyByDate(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate);

                int i = this.HSTL.getBKToKhaiXuat();

                if (i >= 0)
                {
                    this.bkTKXCollection = this.HSTL.BKCollection[i].bkTKXColletion;
                    foreach (BKToKhaiXuat bk in this.bkTKXCollection)
                    {
                        this.RemoveTKMD(bk.SoToKhai, bk.MaLoaiHinh, bk.NamDangKy, bk.MaHaiQuan);
                    }
                }
                //this.dgTKXTL.DataSource = null;

                List<BKToKhaiXuat> tokhaiXuatSource = new List<BKToKhaiXuat>();
                List<BKToKhaiXuat> tokhaiXuatDest = new List<BKToKhaiXuat>();

                foreach (BKToKhaiXuat item in this.bkTKXCollection)
                {

                    ChungTuChiTiet chungtuchitiet = ChungTuThanhToan.ChungTuChiTietCollections.Find(c => c.SoToKhai == item.SoToKhai && item.NgayDangKy == c.NgayDangKy);
                    if (chungtuchitiet != null)
                    {
                        item.TriGia = chungtuchitiet.TriGia;
                        tokhaiXuatDest.Add(item);
                    }
                    else tokhaiXuatSource.Add(item);
                }



                this.bkTKXLucDauCollection = (BKToKhaiXuatCollection)this.bkTKXCollection.Clone();
                this.dgTKX.DataSource = tokhaiXuatSource;
                dgTKXTL.DataSource = tokhaiXuatDest;
                if (ChungTuThanhToan.ID != 0)
                {
                    numTongtriGia.Value = ChungTuThanhToan.TongTriGia;
                    txtSoChungTu.Text = ChungTuThanhToan.SoChungTu;
                    dtNgayChungTu.Value = ChungTuThanhToan.NgayChungTu;
                    cboHinhThucThanhToan.Text = ChungTuThanhToan.HinhThucThanhToan;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private DateTime getMaxDateBKTKN()
        {
            int i = this.HSTL.getBKToKhaiNhap();

            this.HSTL.BKCollection[i].LoadChiTietBangKe();

            BKToKhaiNhapCollection bkTKNCollection = this.HSTL.BKCollection[i].bkTKNCollection;

            DateTime dt = bkTKNCollection[0].NgayDangKy;

            for (int j = 1; j < bkTKNCollection.Count; j++)
            {
                if (dt > bkTKNCollection[j].NgayDangKy) dt = bkTKNCollection[j].NgayDangKy;
            }

            return dt;
        }

        private void RemoveBKTKX(BKToKhaiXuat bk)
        {
            for (int i = 0; i < this.bkTKXCollection.Count; i++)
            {
                if (this.bkTKXCollection[i].SoToKhai == bk.SoToKhai && this.bkTKXCollection[i].MaLoaiHinh == bk.MaLoaiHinh && this.bkTKXCollection[i].NamDangKy == bk.NamDangKy && this.bkTKXCollection[i].MaHaiQuan == bk.MaHaiQuan)
                {
                    this.bkTKXCollection.RemoveAt(i);
                    break;
                }
            }
        }
        private void RemoveTKMD(ToKhaiMauDich tkmd)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == tkmd.SoToKhai && this.tkmdCollection[i].MaLoaiHinh == tkmd.MaLoaiHinh && this.tkmdCollection[i].NamDangKy == tkmd.NamDangKy && this.tkmdCollection[i].MaHaiQuan == tkmd.MaHaiQuan)
                {
                    this.tkmdCollection.Remove(tkmdCollection[i]);
                    break;
                }
            }
        }
        private void RemoveTKMD(int SoToKhai, string MaLoaiHinh, short NamDangKy, string MaHaiQuan)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == SoToKhai && this.tkmdCollection[i].MaLoaiHinh == MaLoaiHinh && this.tkmdCollection[i].NamDangKy == NamDangKy && this.tkmdCollection[i].MaHaiQuan == MaHaiQuan)
                {
                    this.tkmdCollection.Remove(tkmdCollection[i]);
                    break;
                }
            }
        }

        private void dgTKXTL_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                //if (ShowMessage("Khi bạn xóa tờ khai xuất này thì dữ liệu liên quan đến tờ khai xuất trong các bảng kê: BK05, BK06, BK08 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
                if (MLMessages("Khi bạn xóa tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
                {
                    BKToKhaiXuat bk = (BKToKhaiXuat)e.Row.DataRow;
                    bk.DeleteFull();
                    ToKhaiMauDich tkmd = new ToKhaiMauDich();
                    tkmd.MaHaiQuan = bk.MaHaiQuan;
                    tkmd.SoToKhai = bk.SoToKhai;
                    tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                    tkmd.NgayDangKy = bk.NgayDangKy;
                    tkmd.NamDangKy = bk.NamDangKy;
                    tkmd.NGAY_THN_THX = bk.NgayThucXuat;
                    this.tkmdCollection.Add(tkmd);
                    try
                    {
                        dgTKX.Refetch();
                    }
                    catch
                    {
                        dgTKX.Refresh();
                    }
                }
                else return;
            }
        }
        private void dgTKXTL_DeletingRecords(object sender, CancelEventArgs e)
        {
            //if (ShowMessage("Khi bạn xóa các tờ khai xuất này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê:  BK05, BK06, BK08 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
            if (MLMessages("Khi bạn xóa tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
            {
                this.Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = dgTKXTL.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        BKToKhaiXuat bk = (BKToKhaiXuat)i.GetRow().DataRow;
                        //if (bk.ID > 0)
                        //    bk.DeleteFull();
                        ToKhaiMauDich tkmd = new ToKhaiMauDich();
                        tkmd.MaHaiQuan = bk.MaHaiQuan;
                        tkmd.SoToKhai = bk.SoToKhai;
                        tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                        tkmd.NgayDangKy = bk.NgayDangKy;
                        tkmd.NamDangKy = bk.NamDangKy;
                        tkmd.NGAY_THN_THX = bk.NgayThucXuat;
                        this.tkmdCollection.Add(tkmd);
                    }
                }
                try
                {
                    dgTKX.Refetch();
                }
                catch
                {
                    dgTKX.Refresh();
                }
                this.Cursor = Cursors.Default;
            }
        }
        private void dgTKXTL_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();

                tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog();
            }
        }
        private void dgTKXTL_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //if (Convert.ToDateTime(e.Row.Cells["NgayThucXuat"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayThucXuat"].Text = "";
            if (Convert.ToDateTime(e.Row.Cells["NgayHoanThanh"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayHoanThanh"].Text = "";
        }

        private void dgTKX_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (Convert.ToDateTime(e.Row.Cells["NgayHoanThanh"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayHoanThanh"].Text = "";
        }
        private void dgTKX_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog();
            }
        }

        private void btnGoTo_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTKX.FindAll(dgTKX.Tables[0].Columns["SoToKhai"], ConditionOperator.Equal, Convert.ToInt64(txtSoToKhai.Value)) > 0)
                {
                    foreach (GridEXSelectedItem item in dgTKX.SelectedItems)
                        item.GetRow().IsChecked = true;
                }
                else
                {
                    MLMessages("Không có tờ khai này trong hệ thống", "MSG_THK79", "", false);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);
                DateTime fromDate = dtFromDate.Value;
                DateTime toDate = dtToDate.Value;
                if (fromDate > toDate)
                {
                    MLMessages("Từ ngày phải nhỏ hơn hoặc bằng đến ngày.", "MSG_SEA01", "", false);
                    return;
                }
                if (fromDate < this.getMaxDateBKTKN().AddDays(chenhLech)) fromDate = this.getMaxDateBKTKN().AddDays(0 - chenhLech);
                this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKXChuaThanhLyByDate(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate);
                foreach (BKToKhaiXuat bk in this.bkTKXCollection)
                {
                    this.RemoveTKMD(bk.SoToKhai, bk.MaLoaiHinh, bk.NamDangKy, bk.MaHaiQuan);
                }
                dgTKX.DataSource = this.tkmdCollection;
                try
                {
                    dgTKX.Refetch();
                }
                catch
                {
                    dgTKX.Refresh();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                List<BKToKhaiXuat> tkxDich = (List<BKToKhaiXuat>)dgTKXTL.DataSource;

                foreach (GridEXRow row in dgTKX.GetCheckedRows())
                {
                    if (row.RowType == RowType.Record)
                    {

                        BKToKhaiXuat tkmd = (BKToKhaiXuat)row.DataRow;

                        //BKToKhaiXuat bk = new BKToKhaiXuat();
                        //bk.MaHaiQuan = tkmd.MaHaiQuan;
                        //bk.MaLoaiHinh = tkmd.MaLoaiHinh;
                        //bk.NamDangKy = tkmd.NamDangKy;
                        //bk.NgayDangKy = tkmd.NgayDangKy;
                        //bk.NgayThucXuat = tkmd.NGAY_THN_THX;
                        //bk.SoToKhai = tkmd.SoToKhai;
                        //bk.NgayHoanThanh = tkmd.NgayHoanThanh;
                        //this.bkTKXCollection.Add(bk);
                        //tkmds.Add(tkmd);

                        tkxDich.Add(tkmd);
                    }
                }
                List<BKToKhaiXuat> tkxNguon = (List<BKToKhaiXuat>)dgTKX.DataSource;
                foreach (BKToKhaiXuat item in tkxDich)
                {
                    tkxNguon.Remove(item);
                }
                try
                {
                    dgTKX.Refetch();
                }
                catch
                {
                    dgTKX.Refresh();
                }
                try
                {
                    dgTKXTL.Refetch();
                }
                catch
                {
                    dgTKXTL.Refresh();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //if (ShowMessage("Khi bạn xóa các tờ khai xuất này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK05, BK06, BK08 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
            //if (MLMessages("Bạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
            //{
            this.Cursor = Cursors.WaitCursor;

            List<BKToKhaiXuat> tkxDich = (List<BKToKhaiXuat>)dgTKXTL.DataSource;
            List<BKToKhaiXuat> tkxNguon = (List<BKToKhaiXuat>)dgTKX.DataSource;
            List<BKToKhaiXuat> tkItems = new List<BKToKhaiXuat>();

            foreach (GridEXRow row in dgTKXTL.GetCheckedRows())
            {

                if (row.RowType == RowType.Record)
                {

                    BKToKhaiXuat bk = (BKToKhaiXuat)row.DataRow;
                    ChungTuChiTiet ctct = ChungTuThanhToan.ChungTuChiTietCollections.Find(c => c.SoToKhai == bk.SoToKhai && bk.NgayDangKy == c.NgayDangKy);
                    if (ctct != null) ChungTuThanhToan.ChungTuChiTietCollections.Remove(ctct);
                    tkItems.Add(bk);
                }
            }
            foreach (BKToKhaiXuat item in tkItems)
            {
                tkxDich.Remove(item);
                tkxNguon.Add(item);
            }
            //foreach (BKToKhaiXuat bk in tkxDich)
            //    this.RemoveBKTKX(bk);
            try
            {
                dgTKXTL.Refetch();
            }
            catch
            {
                dgTKXTL.Refresh();
            }
            try
            {
                dgTKX.Refetch();
            }
            catch
            {
                dgTKX.Refresh();
            }
            this.Cursor = Cursors.Default;
            //}
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.bkTKXCollection.Count == 0)
                {
                    // ShowMessage("Danh sách tờ khai xuất cần thanh lý chưa có!", false);
                    MLMessages("Danh sách tờ khai xuất cần thanh lý chưa có!", "MSG_THK78", "", false);
                    return;
                }
                this.Cursor = Cursors.WaitCursor;
                this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].bkTKXColletion = this.bkTKXCollection;
                foreach (BKToKhaiXuat bk in this.bkTKXLucDauCollection)
                {
                    if (!this.bkTKXCollection.Contains(bk)) bk.DeleteFull();
                }
                this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].InsertUpdate_BKTKX(this.bkTKXCollection);
                this.Close();
            }
            catch (Exception ex)
            {
                ShowMessage("Quá trình lưu thông tin không thành công.\r\nChi tiết: " + ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        /// <summary>
        /// Lưu thông tin chứng từ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool valid = Company.Interface.Globals.ValidateNumber(numLanThanhLy, errorProvider1, "Lần thanh lý.");

                valid = Company.Interface.Globals.ValidateNull(txtSoChungTu, errorProvider1, "Số chứng từ.");

                valid = Company.Interface.Globals.ValidateNull(cboHinhThucThanhToan, errorProvider1, "Hình thức thanh toán.");

                //valid = Company.Interface.Globals.ValidateNumber(numTongtriGia, errorProvider1, "Tổng trị giá.");
                errorProvider1.SetError(numTongtriGia, "");
                if (System.Convert.ToInt64(numTongtriGia.Value) <= 0)
                {
                    errorProvider1.SetError(numTongtriGia, "Tổng trị giá phải > 0");
                    valid = false;
                }

                if (!valid) return;

                //ChungTu.DeleteChungTu(ChungTuThanhToan.ID);
                //Tao doi tuong Chung tu thanh toan
                ChungTu cttt = ChungTuThanhToan;//new ChungTu();
                cttt.LanThanhLy = (int)numLanThanhLy.Value;
                cttt.SoChungTu = txtSoChungTu.Text;
                cttt.NgayChungTu = dtNgayChungTu.Value;
                cttt.HinhThucThanhToan = cboHinhThucThanhToan.SelectedValue.ToString();
                cttt.TongTriGia = numTongtriGia.Value;
                cttt.ConLai = numTongtriGia.Value - getSum();
                //Kiem tra co thong tin to khai

                if (dgTKXTL.RowCount > 0)
                {
                    //Tao danh sach to khai cua chung tu thanh toan -- Tạo mới                    

                    cttt.ChungTuChiTietCollections = new List<ChungTuChiTiet>();

                    for (int i = 0; i < dgTKXTL.RowCount; i++)
                    {
                        ChungTuChiTiet chiTiet = new ChungTuChiTiet();
                        chiTiet.IDCT = cttt.ID;
                        chiTiet.SoToKhai = Convert.ToInt32(dgTKXTL.GetRow(i).Cells["SoToKhai"].Value);
                        chiTiet.MaLoaiHinh = dgTKXTL.GetRow(i).Cells["MaLoaiHinh"].Value.ToString();
                        chiTiet.NgayDangKy = Convert.ToDateTime(dgTKXTL.GetRow(i).Cells["NgayDangKy"].Value);
                        chiTiet.TriGia = Convert.ToDecimal(dgTKXTL.GetRow(i).Cells["TriGia"].Value);

                        cttt.ChungTuChiTietCollections.Add(chiTiet);
                    }

                }
                if (dgChiPhiDuocChon.RowCount > 0)
                {
                    cttt.ChiPhiKhacCollections = new List<ChiPhiKhac>();
                    for (int i = 0; i < dgChiPhiDuocChon.RowCount; i++)
                    {
                        ChiPhiKhac chiphikhac = new ChiPhiKhac();
                        chiphikhac.IDCT = cttt.ID;
                        chiphikhac.MaChiPhi = dgChiPhiDuocChon.GetRow(i).Cells["ID"].Value.ToString();
                        object value = dgChiPhiDuocChon.GetRow(i).Cells["TriGia"].Value;
                        if (!value.Equals(DBNull.Value))
                            chiphikhac.TriGia = (decimal)value;
                        cttt.ChiPhiKhacCollections.Add(chiphikhac);
                    }
                }
                List<ChungTuChiTiet> chungtuchitiets = (List<ChungTuChiTiet>)ChungTuChiTiet.SelectCollectionBy_IDCT(cttt.ID);
                List<ChiPhiKhac> chiphikhacs = (List<ChiPhiKhac>)ChiPhiKhac.SelectCollectionBy_IDCT(cttt.ID);

                ChungTuChiTiet.DeleteCollection(chungtuchitiets);
                ChiPhiKhac.DeleteCollection(chiphikhacs);

                int id = 0;
                if (cttt.ID == 0) id = cttt.Insert();
                else
                {
                    cttt.Update();
                    id = cttt.ID;
                }
                foreach (ChungTuChiTiet item in cttt.ChungTuChiTietCollections)
                {
                    item.IDCT = id;
                }
                foreach (ChiPhiKhac item in cttt.ChiPhiKhacCollections)
                {
                    item.IDCT = id;
                }

                ChungTuChiTiet.InsertCollection(cttt.ChungTuChiTietCollections);
                ChiPhiKhac.InsertCollection(cttt.ChiPhiKhacCollections);


                //if (cttt.ID == 0)
                //{
                //    //Them moi Chung tu thanh toan va lay ID
                //    cttt.Insert();                                      
                //}
                Globals.ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage("Quá trình lưu không thành công.\r\nChi tiết: " + ex.Message, false);
            }
        }

        private void btnToRight_Click(object sender, EventArgs e)
        {

            try
            {
                DataTable dtNguon = (DataTable)dgChiPhi.DataSource;
                DataTable dtDich = (DataTable)dgChiPhiDuocChon.DataSource;
                List<DataRow> drItems = new List<DataRow>();

                foreach (GridEXRow row in dgChiPhi.GetCheckedRows())
                {
                    if (row.RowType == RowType.Record)
                    {
                        DataRowView drView = (DataRowView)row.DataRow;
                        drItems.Add(drView.Row);
                    }
                }

                foreach (DataRow dr in drItems)
                {
                    DataRow myRow = dtDich.NewRow();
                    myRow[0] = dr[0];
                    myRow[1] = dr[1];
                    dtDich.Rows.Add(myRow);
                    dtNguon.Rows.Remove(dr);
                }

                try
                {
                    dgChiPhi.Refetch();
                }
                catch
                {
                    dgChiPhi.Refresh();
                }
                try
                {

                    dgChiPhiDuocChon.Refetch();
                }
                catch
                {
                    dgChiPhiDuocChon.Refresh();
                }
            }
            catch (Exception ex)
            { }
        }

        private void btnToLeft_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtNguon = (DataTable)dgChiPhi.DataSource;
                DataTable dtDich = (DataTable)dgChiPhiDuocChon.DataSource;
                List<DataRow> drItems = new List<DataRow>();

                foreach (GridEXRow row in dgChiPhiDuocChon.GetCheckedRows())
                {
                    if (row.RowType == RowType.Record)
                    {

                        DataRowView drView = (DataRowView)row.DataRow;
                        drItems.Add(drView.Row);
                        ChiPhiKhac cpk = ChungTuThanhToan.ChiPhiKhacCollections.Find(cp => cp.MaChiPhi == drView.Row[0].ToString());
                        if (cpk != null) ChungTuThanhToan.ChiPhiKhacCollections.Remove(cpk);
                    }
                }

                foreach (DataRow dr in drItems)
                {
                    DataRow myRow = dtNguon.NewRow();
                    myRow[0] = dr[0];
                    myRow[1] = dr[1];
                    dtNguon.Rows.Add(myRow);
                    dtDich.Rows.Remove(dr);
                }

                try
                {
                    dgChiPhi.Refetch();
                }
                catch
                {
                    dgChiPhi.Refresh();
                }
                try
                {

                    dgChiPhiDuocChon.Refetch();
                }
                catch
                {
                    dgChiPhiDuocChon.Refresh();
                }
            }
            catch (Exception ex)
            { }
        }
        private decimal getSum()
        {
            decimal tongNhap = 0M;
            try
            {

                tongNhap = (decimal)dgTKXTL.GetTotalRow().GetSubTotal(dgTKXTL.Tables[0].Columns["TriGia"], AggregateFunction.Sum);
                tongNhap += (decimal)dgChiPhiDuocChon.GetTotalRow().GetSubTotal(dgChiPhiDuocChon.Tables[0].Columns["TriGia"], AggregateFunction.Sum);
            }
            catch { }
            return tongNhap;
        }

        private void numConLai_ValueChanged(object sender, EventArgs e)
        {
            label16.Text = numConLai.Value.ToString("N" + GlobalSettings.SoThapPhan.DinhMuc);
            lblConLai.Text = Company.BLL.Utils.VNCurrency.ToString(numConLai.Value);
        }

        private void numTongtriGia_ValueChanged(object sender, EventArgs e)
        {
            decimal tongnhap = getSum();
            numConLai.Value = numTongtriGia.Value - decimal.Parse(lbTong.Text);
            lblTongTriGia.Text = Company.BLL.Utils.VNCurrency.ToString(numTongtriGia.Value);
        }
        private void dgTKXTL_SelectionChanged(object sender, EventArgs e)
        {
            //dgTKXTL_CellValueChanged(null, null);
        }
        private void dgTKXTL_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "TriGia")
            {
                try
                {

                    decimal tongNhap = getSum();
                    decimal init = 0;
                    if (e.InitialValue.ToString() != "") init = (decimal)e.InitialValue;
                    tongNhap += (decimal)e.Value-init;
                    lbTong.Text = tongNhap.ToString("N" + GlobalSettings.SoThapPhan.DinhMuc);
                    numConLai.Value = numTongtriGia.Value - tongNhap;
                }
                catch { }

            }

        }

    }
}
