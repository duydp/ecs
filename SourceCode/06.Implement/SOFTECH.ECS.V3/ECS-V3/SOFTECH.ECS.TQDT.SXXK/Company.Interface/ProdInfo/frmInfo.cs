using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace Company.Interface.ProdInfo
{
    public partial class frmInfo : BaseForm
    {
        public frmInfo()
        {
            InitializeComponent();
        }

        private void frmInfo_Load(object sender, EventArgs e)
        {
            try
            {
                lblVersion.Text = Application.ProductVersion;
                this.fillInfo();
                lblLicense.Text = Program.lic.licenseName;
                string licenseInfo = setText("Được sử dụng ", "Can be used");
                licenseInfo += Program.lic.numberClient + setText(" máy", "computer(s)"); ;
                if (Program.isActivated)
                {
                    btnRegister.Enabled = false;

                }
                else
                {
                    licenseInfo += setText("\nNgày hết hạn: ", "Expire date:");
                    int i = int.Parse(Program.lic.dateTrial);
                    licenseInfo += DateTime.Now.Date.AddDays(i).ToShortDateString();
                    licenseInfo += " (" + i.ToString() + setText(" ngày)", "day(s))");
                }
                lblLicenseInfo.Text = licenseInfo;
            }
            catch { }
        }
        
        private void fillInfo()
        {
            lblTenDN.Text = GlobalSettings.TEN_DON_VI;
            lblDiaChiDN.Text = GlobalSettings.DIA_CHI;

            if (GlobalSettings.MA_DON_VI == "3600942745")
            {
                label10.Text = "Người sở hữu : ";
                lblTenDN.Text = "NGUYỄN TUẤN HÒA";
                lblDiaChiDN.Text = "ẤP HÒA BÌNH XÃ VĨNH THANH HUYỆN NHƠN TRẠCH TỈNH ĐỒNG NAI";
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            this.Hide();
            //this.Close();
            frmRegister_old obj = new frmRegister_old();
            obj.ShowDialog(this);
        }

        private void linkSoftech_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://softech.vn");
        }
    }
}