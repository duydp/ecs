﻿using System;
using System.Text;
using System.Collections;
using System.ComponentModel;

namespace CA_QuanLyGCN
{
    public class LoiPhatCollections : CollectionBase
    {
        #region Constructor

        public LoiPhatCollections() { }
        #endregion Constructor

        #region Indexer Methods

        public LoiPhat this[int index]
        {
            get { return (LoiPhat)List[index]; }
            set { List[index] = value; }
        }

        #endregion Indexer Methods

        #region Private Methods

        public int Add(LoiPhat value)
        {
            return List.Add(value);
        }

        public void Insert(int index, LoiPhat value)
        {
            List.Insert(index, value);
        }

        public void Remove(LoiPhat value)
        {
            List.Remove(value);
        }

        public bool Contains(string id)
        {
            for (int i = 0; i < List.Count; i++)
            {
                LoiPhat LoiPhatEntity = (LoiPhat)List[i];
                if (LoiPhatEntity.Id.Equals(id) && (LoiPhatEntity.EntityState != EntityState.Deleted))
                {
                    return true;
                }
            }
            return false;
        }

        public LoiPhat SearchById(int id)
        {
            for (int i = 0; i < List.Count; i++)
            {
                LoiPhat LoiPhatEntity = (LoiPhat)List[i];
                if ((LoiPhatEntity.Id.Equals(id)) && (LoiPhatEntity.EntityState != EntityState.Deleted))
                {
                    return LoiPhatEntity;
                }
            }
            return null;
        }
/*
        public LoiPhat SearchByTen(string name)
        {
            for (int i = 0; i < List.Count; i++)
            {
                LoiPhat LoiPhatEntity = (LoiPhat)List[i];
                if ((LoiPhatEntity.Name.ToLower().Trim().Equals(name.ToLower().Trim())) && (LoiPhatEntity.EntityState != EntityState.Deleted))
                {
                    return LoiPhatEntity;
                }
            }
            return null;
        }
*/
        public void Save()
        {
            for (int i = 0; i < List.Count; i++)
            {
                LoiPhat LoiPhatEntity = (LoiPhat)List[i];
                switch (LoiPhatEntity.EntityState)
                {
                    case EntityState.Deleted:
                        LoiPhatEntity.Delete();
                        break;
                    case EntityState.Added:
                        LoiPhatEntity.Insert();
                        break;
                    case EntityState.Updated:
                        LoiPhatEntity.Update();
                        break;
                }
                LoiPhatEntity.EntityState = EntityState.Unchanged;
            }

        }

        #endregion Private Methods
						
		#region Clone Method

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public LoiPhatCollections Clone()
        {
            LoiPhatCollections _tmpCollections = new LoiPhatCollections();

            foreach (LoiPhat item in this)
            {
                _tmpCollections.Add((LoiPhat)item.Clone());
            }

            return _tmpCollections;
        }

        #endregion

    }
}

