using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.BLL.SXXK.ThanhKhoan;

namespace Company.Interface.Report.SXXK
{
    public partial class BangKeToKhaiXuat_929 : DevExpress.XtraReports.UI.XtraReport
    {
        public int SoHoSo;
        public BangKeToKhaiXuat_929()
        {
            InitializeComponent();
        }
        public void BindReport(int lanThanhLy)
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            DataTable dt = new Company.BLL.KDT.SXXK.BKToKhaiXuat().getBKToKhaiXuatForReport(lanThanhLy,GlobalSettings.MA_DON_VI).Tables[0];
            dt.TableName = "BKToKhaiXuat";
            this.DataSource = dt;
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (SoHoSo > 0)
                lblSHSTK.Text = SoHoSo + "";
            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblSoToKhai.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhai");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKy","{0:dd/MM/yyyy}");
            lblNgayThucXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKy", "{0:dd/MM/yyyy}");
            lblDVHQ.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ten");
            xrLabel9.Text = xrLabel77.Text = GlobalSettings.TieudeNgay;
        }
    }
}
