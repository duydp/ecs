using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Data;
using ThoughtWorks.QRCode.Codec;
//using BarcodeLib.Barcode.WinForms;
using System.Windows.Forms;

namespace Company.Interface.Report.VNACCS
{
    public partial class 
        BangKeSoContainerQuaKVGS_M29_TT38 : DevExpress.XtraReports.UI.XtraReport
    {

        //BarcodeLib.Barcode.QRCode qrcode = new BarcodeLib.Barcode.QRCode();

        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
        string encodeBase64String = string.Empty;

        string sotokhai = "";

        public BangKeSoContainerQuaKVGS_M29_TT38()
        {
            InitializeComponent();
        }
        public void BindReport(KDT_ContainerDangKy cont)
        {
            //KDT_ContainerDangKy contDK = new KDT_ContainerDangKy();
            //List<KDT_ContainerBS> listCont = cont.ListCont;
            lblNgayThangNam.Text = "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            lblMaNguoiKhaiHQ.Text = cont.MaHQ;
            //lblCucHQ.Text = GlobalSettings.TEN_CUC_HAI_QUAN;
            lblChiCucHQ.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
            
            long id = cont.TKMD_ID;

            KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(id);
            string maCC = TKMD.MaDDLuuKho.ToString();
            string temp = maCC.Substring(0, 4);

            VNACC_Category_CustomsOffice cus = VNACC_Category_CustomsOffice.SelectCollectionAllMinimize().Find(delegate(VNACC_Category_CustomsOffice c)
            {
                return c.CustomsCode == temp;
            });

            lblChiCucHQGiamSat.Text = cus.CustomsOfficeNameInVietnamese;
            lblCucHQ.Text = cus.CustomsOfficeNameInVietnamese;
            VNACC_Category_Common com = VNACC_Category_Common.SelectCollectionAllMinimize().Find(delegate(VNACC_Category_Common c)
            {
                return c.Code == TKMD.MaLoaiHinh;
            });

            lblLoaiHinh.Text = com.Name_VN;

            sotokhai = TKMD.SoToKhai.ToString();
            lblTenNguoiKhaiHQ.Text = GlobalSettings.TEN_DON_VI;
            lblMaNguoiKhaiHQ.Text = GlobalSettings.MA_DON_VI;
            lblSoToKhai.Text = Convert.ToString(TKMD.SoToKhai);
            lblNgayKhai.Text = TKMD.NgayDangKy.ToString("dd/MM/yyyy hh:mm:ss");
            //lblGioKhai.Text = TKMD.NgayDangKy.Hour.;
            //lblTrangThaiTK.Text = Convert.ToString(TKMD.TrangThaiXuLy);
            int luong = 0;
            luong = Convert.ToInt32(TKMD.MaPhanLoaiKiemTra);
            if (luong == 1)
            {
                lblLuong.Text = "Luồng xanh";
            }
            else if (luong == 2)
            {
                lblLuong.Text = "Luồng vàng";
            }
            else if (luong == 3)
            {
                lblLuong.Text = "Luồng đỏ";
            }
            int trangthai = 0;
            trangthai = Convert.ToInt32(TKMD.TrangThaiXuLy);
            if (trangthai == 1)
            {
                lblTrangThaiTK.Text = "Khai báo tạm";
            }
            else if (trangthai == 2)
            {
                lblTrangThaiTK.Text = "Đã xác nhận khai báo";
            }
            else if (trangthai == 3)
            {
                lblTrangThaiTK.Text = "Thông quan";
            }
            else if (trangthai == 4)
            {
                lblTrangThaiTK.Text = "Khai báo sửa";
            }
            else if (trangthai == 5)
            {
                lblTrangThaiTK.Text = "Từ chối";
            }
            DataTable dt = new DataTable();
            this.DataSource = cont.ListCont;
            lblSoHieuKienCont.DataBindings.Add("Text", this.DataSource, "SoContainer");
            lblSoSeal.DataBindings.Add("Text", this.DataSource, "SoSeal");
            
            lblMaVach.DataBindings.Add("Text", this.DataSource, "Code");
            
            //lblNgayGio.Text = "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + " " + DateTime.Now.ToString("hh:mm:ss");
        }
        int stt = 0;
        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            stt += 1;
            lblSTT.Text = stt.ToString();
            //lblMaVach.Text = "aHR0cHM6Ly93d3cuZmFjZWJvb2suY29tLw==";
        }

        private void lblMaVach_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            #region General Old

            ////qrcode.ModuleSize = 2;
            ////qrcode.LeftMargin = 1;
            ////qrcode.RightMargin = 1;
            ////qrcode.BottomMargin = 1;
            ////qrcode.TopMargin = 1;
            ////qrcode.Version = BarcodeLib.Barcode.QRCodeVersion.V7;
            ////qrcode.Encoding = BarcodeLib.Barcode.QRCodeEncoding.Auto;
            ////qrcode.ECL = BarcodeLib.Barcode.QRCodeErrorCorrectionLevel.L;
            ////qrcode.ImageFormat = System.Drawing.Imaging.ImageFormat.Png;

            ////qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            //////Scale
            ////qrCodeEncoder.QRCodeScale = 3;
            //////Version
            ////qrCodeEncoder.QRCodeVersion = 8;

            ////qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;

            //Image image = null;
            ////String data = "100605558060WHLU4278161                        WHL6338143                         BD32215090269-01                   34NH  34CE  0400100104   2TQ";
            //String data = lblMaVach.Text;
            //if (!string.IsNullOrEmpty(data))
            //{
            //    try
            //    {
            //        //encodeBase64String = qrCodeEncoder.EncodeToBase64String(data);
            //        //encodeBase64String = qrcode.d
            //        //qrcode.Data = data;
            //        ////image = qrCodeEncoder.Encode(data,System.Text.Encoding.Unicode);
            //        //image = qrcode.drawBarcode();
            //    }
            //    catch (Exception ex)
            //    {

            //        //throw;
            //    }

            //}

            ////PictureBox pic = new PictureBox();
            ////pic.Visible = true;
            //pictureBox1.Image = image;
            #endregion            
            try
            {
                Zen.Barcode.CodeQrBarcodeDraw qrCode = Zen.Barcode.BarcodeDrawFactory.CodeQr;
                picQrCode.Image = qrCode.Draw(lblMaVach.Text, 50);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtBarCode_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            txtBarCode.Text = sotokhai;
        }

    }
}
