﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
#if KD_V3
using Company.KD.BLL.KDT;
#elif SXXK_V3
using Company.BLL.KDT;
#elif GC_V3
using Company.GC.BLL.KDT;
#endif
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using System.IO;

namespace Company.Interface
{
    public partial class ThongDiepForm : Company.Interface.BaseForm
    {
        public long ItemID { set; get; }
        public string DeclarationIssuer { get; set; }
        public ThongDiepForm()
        {
            InitializeComponent();
        }

        private void KetQuaXuLyForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = Company.KDT.SHARE.Components.Message.SelectCollectionBy_ItemID(this.ItemID, DeclarationIssuer);
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                Company.KDT.SHARE.Components.Message message = Company.KDT.SHARE.Components.Message.Load(id);

                string content = "ID: [" + e.Row.Cells[1].Value + "]\r\n" + message.NoiDungThongBao;

                Company.Controls.KDTMessageBoxControl msg = new Company.Controls.KDTMessageBoxControl();
                msg.HQMessageString = "Kết quả xử lý thông tin";
                msg.ShowYesNoButton = false;
                msg.ShowErrorButton = true;
                msg.MaHQ = GlobalSettings.MA_HAI_QUAN;
                //string path = Company.KDT.SHARE.Components.Globals.Message2File(message.MessageContent);
                //msg.XmlFiles.Add(path);
                msg.MessageString = content;
                msg.MessageID = id;
                //msg.exceptionString = message.MessageContent;
                msg.ShowDialog(this);

            }
        }
    }
}
