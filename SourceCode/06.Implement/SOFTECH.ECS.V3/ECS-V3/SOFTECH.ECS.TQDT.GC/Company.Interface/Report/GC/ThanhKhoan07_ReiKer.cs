﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
 

namespace Company.Interface.Report.GC
{
    public partial class ThanhKhoan07_RieKer : DevExpress.XtraReports.UI.XtraReport
    {
        public Report.ReportViewBC07FormRieKer report = new ReportViewBC07FormRieKer();
        public HopDong HD = new HopDong();
        public bool First = false;
        public bool Last = false;


        public ThanhKhoan07_RieKer()
        {
            InitializeComponent();
        }

        public void BindReport(DataTable dt)
        {
            this.ReportHeader.Visible = this.First;
            ReportFooter1.Visible = this.Last;

            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = dt;
            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamDangKyInHopDong(this.HD.ID);
            lblH_SoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + " - " + dr["TenSanPham"].ToString();
                }
            }

            lblH_MatHang.Text = loaiSP;
            lblH_DiaChiBN.Text = GlobalSettings.DIA_CHI;
            lblH_BenNhan.Text = GlobalSettings.TEN_DON_VI;
            lblH_DVHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN;

            lblH_NgayHD.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lblH_ThoiHanHD.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblH_BenThue.Text = this.HD.DonViDoiTac;
            lblH_DiaChiBT.Text = this.HD.DiaChiDoiTac;
            lblH_HDGCS.Text = this.HD.SoHopDong;
            lblPH_SoToKhai_3.Text = dt.Columns[2].Caption;
            lblPH_SoToKhai_4.Text = dt.Columns[3].Caption;
            lblPH_SoToKhai_5.Text = dt.Columns[4].Caption;
            lblPH_SoToKhai_6.Text = dt.Columns[5].Caption;
            lblPH_SoToKhai_7.Text = dt.Columns[6].Caption;
            lblPD_1.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[0].ColumnName);
            lblPD_2.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[1].ColumnName);
            lblPD_3.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[2].ColumnName, "{0:N0}");
            lblPD_4.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[3].ColumnName, "{0:N0}");
            lblPD_5.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[4].ColumnName, "{0:N0}");
            lblPD_6.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[5].ColumnName, "{0:N0}");
            lblPD_7.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[6].ColumnName, "{0:N0}");
            lblPD_8.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[7].ColumnName, "{0:N0}");
            xrLbl_6.Text = GlobalSettings.TieudeNgay;
        }

        private void lblH_HDGCS_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel label = (XRLabel)sender;
            report.Label = label;
            report.txtName.Text = label.Text;
            report.lblName.Text = label.Tag.ToString();
        }
        public void setText(XRLabel label, string text)
        {
            label.Text = text;
        }
    }
}
