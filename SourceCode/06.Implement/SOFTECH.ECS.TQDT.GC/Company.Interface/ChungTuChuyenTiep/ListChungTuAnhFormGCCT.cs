﻿using System;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class ListChungTuAnhFormGCCT : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiChuyenTiep TKCT;
        //-----------------------------------------------------------------------------------------

        public ListChungTuAnhFormGCCT()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            if (TKCT.ID > 0)
            {
                // dgList.DataSource = ChungTuKemAnh.SelectCollectionBy_ID_TK(TKCT.ID);
                TKCT.CTKAnhCollection = (List<ChungTuKemAnh>)ChungTuKemAnh.SelectCollectionBy_ID_TK(TKCT.ID);
                dgList.DataSource = TKCT.CTKAnhCollection;
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            //HUNGTQ, Uopdate 07/06/2010            
            SetButtonStateCHUNGTUKEM(TKCT, isKhaiBoSung);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<ChungTuKemAnh> listChungTuKemAnh = new List<ChungTuKemAnh>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuKemAnh ctk = (ChungTuKemAnh)i.GetRow().DataRow;
                        listChungTuKemAnh.Add(ctk);
                    }
                }
            }

            foreach (ChungTuKemAnh ctk in listChungTuKemAnh)
            {
                if (ctk.ID > 0)
                {
                    ctk.Delete();
                }
                //TKCT.GiayPhepCollection.Remove(gp);
                TKCT.CTKAnhCollection.Remove(ctk);
            }

            dgList.DataSource = TKCT.CTKAnhCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            ChungTuKemFormGCCT ctForm = new ChungTuKemFormGCCT();
            ctForm.TKCT = TKCT;
            ctForm.isKhaiBoSung = this.isKhaiBoSung;
            ctForm.ShowDialog();
            dgList.DataSource = TKCT.CTKAnhCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ChungTuKemFormGCCT f = new ChungTuKemFormGCCT();
            f.TKCT = TKCT;
            f.CTK = (ChungTuKemAnh)e.Row.DataRow;
            f.isKhaiBoSung = this.isKhaiBoSung;


            if (f.CTK.ID > 0)
                f.CTK.LoadListChungTuKemAnhChiTiet();

            f.ShowDialog();
            dgList.DataSource = TKCT.CTKAnhCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateCHUNGTUKEM(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkmd, bool isKhaiBoSung)
        {
            bool status = false;
            if (TKCT.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                //Khai bao moi
                if (isKhaiBoSung == false)
                {
                    //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                    status = (tkmd.SoTiepNhan == 0);

                    btnXoa.Enabled = status;
                    btnTaoMoi.Enabled = status;
                }
                //Khai bao bo sung
                else
                {
                    //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                    if (tkmd.SoToKhai == 0)
                    {
                        //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                        //Globals.ShowMessageTQDT(msg, false);

                        //return false;
                    }
                    else
                    {
                        status = true;

                        btnXoa.Enabled = status;
                        btnTaoMoi.Enabled = status;
                    }
                }
            }
            return true;
        }

        #endregion

    }
}
