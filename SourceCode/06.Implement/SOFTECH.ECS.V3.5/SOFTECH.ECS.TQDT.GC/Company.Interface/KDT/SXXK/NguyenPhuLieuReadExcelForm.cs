using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.Interface.CauHinh;
using Company.KDT.SHARE.Components;
namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuReadExcelForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public List<NguyenPhuLieu> NPLCollection = new List<NguyenPhuLieu>();
        public string FormImport = "";
        public int FormatSoLuong = GlobalSettings.SoThapPhan.LuongNPL;
        public int FormatDonGia = GlobalSettings.SoThapPhan.LuongNPL;
        public bool ImportExcelSucces = false;
        public NguyenPhuLieuReadExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog();
                txtFilePath.Text = openFileDialog1.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private List<String>  GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    MLMessages("Lỗi khi chọn file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return null;
                }
                List<String> Collection = new List<string>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi đọc sheet. Doanh nghiệp hãy kiểm tra lại tên sheet trước khi chọn file.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtFilePath, errorProvider, "ĐƯỜNG DẪN FILE EXCEL", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbSheetName, errorProvider, "TÊN SHEET", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHangColumn, errorProvider, "MÃ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenHangColumn, errorProvider, "TÊN/MÔ TẢ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHSColumn, errorProvider, "MÃ HS", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDVTColumn, errorProvider, "ĐƠN VỊ TÍNH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuong, errorProvider, "SỐ LƯỢNG", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDonGia, errorProvider, "ĐƠN GIÁ", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtRow, errorProvider, "DÒNG BẮT ĐẦU");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                int beginRow = Convert.ToInt32(txtRow.Value) - 1;
                Workbook wb = new Workbook();
                Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    MLMessages("Lỗi khi đọc file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                    return;
                }
                try
                {
                    ws = wb.Worksheets[cbbSheetName.Text];
                }
                catch
                {
                    MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"", "MSG_EXC01", cbbSheetName.Text, false);
                    return;
                }
                WorksheetRowCollection wsrc = ws.Rows;
                char maHangColumn;
                int maHangCol = 0;
                char tenHangColumn;
                int tenHangCol = 0;
                char maHSColumn;
                int maHSCol = 0;
                char dvtColumn;
                int dvtCol = 0;
                char SoLuongColumn;
                int SoLuongCol = 0;

                char DonGiaColumn;
                int DonGiaCol = 0;
                int j = 0;
                maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
                maHangCol = ConvertCharToInt(maHangColumn);
                tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
                tenHangCol = ConvertCharToInt(tenHangColumn);
                maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
                maHSCol = ConvertCharToInt(maHSColumn);
                dvtColumn = Convert.ToChar(txtDVTColumn.Text);
                dvtCol = ConvertCharToInt(dvtColumn);
                SoLuongColumn = Convert.ToChar(txtSoLuong.Text);
                SoLuongCol = ConvertCharToInt(SoLuongColumn);

                DonGiaColumn = Convert.ToChar(txtDonGia.Text);
                DonGiaCol = ConvertCharToInt(DonGiaColumn);

                string errorTotal = "";
                string errorMaHangHoa = "";
                string errorMaHangHoaExits = "";
                string errorMaHangValid = "";
                string errorMaHangSpecialChar = "";
                string errorTenHangHoa = "";
                string errorMaHS = "";
                string errorMaHSExits = "";
                string errorDVT = "";
                string errorDVTExits = "";
                string errorXuatXu = "";
                string errorXuatXuExits = "";
                string errorSoLuong = "";
                string errorSoluongValid = "";
                string errorDonGia = "";
                string errorDonGiaValid = "";

                List<VNACC_Category_Nation> XuatXuCollection = VNACC_Category_Nation.SelectCollectionAll();
                List<VNACC_Category_HSCode> HSCollection = VNACC_Category_HSCode.SelectCollectionAll();
                List<HaiQuan_DonViTinh> DVTCollection = HaiQuan_DonViTinh.SelectCollectionAll();
                List<NguyenPhuLieu> NPLCollectionAdd = new List<NguyenPhuLieu>();
                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            NguyenPhuLieu npl = new NguyenPhuLieu();
                            bool isAdd = true;
                            bool isExits = false;
                            try
                            {
                                npl.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                                if (npl.Ma.Trim().Length == 0)
                                {
                                    errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                    isAdd = false;
                                }
                                else if (Helpers.ValidateSpecialChar(npl.Ma))
                                {
                                    errorMaHangSpecialChar += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    foreach (NguyenPhuLieu item in HD.NPLCollection)
                                    {
                                        if (npl.Ma == item.Ma)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                                if (isExits)
                                {
                                    errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    isExits = false;
                                    foreach (NguyenPhuLieu item in NPLCollectionAdd)
                                    {
                                        if (npl.Ma == item.Ma)
                                        {
                                            isExits = true;
                                            break;                                            
                                        }
                                    }
                                    if (isExits)
                                    {
                                        errorMaHangValid += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                npl.Ten = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                                if (npl.Ten.Trim().Length == 0)
                                {
                                    errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + npl.Ten + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + npl.Ten + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                isExits = false;
                                npl.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                                if (npl.MaHS.Trim().Length == 0)
                                {
                                    errorMaHS += "[" + (wsr.Index + 1) + "]-[" + npl.MaHS + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    foreach (VNACC_Category_HSCode item in HSCollection)
                                    {
                                        if (item.HSCode == npl.MaHS)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                                if (!isExits)
                                {
                                    errorMaHSExits += "[" + (wsr.Index + 1) + "]-[" + npl.MaHS + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + npl.MaHS + "]\n";
                                isAdd = false;
                            }
                            string DonViTinh = String.Empty;
                            try
                            {
                                isExits = false;
                                DonViTinh = Convert.ToString(wsr.Cells[dvtCol].Value.ToString().ToUpper()).Trim();
                                if (DonViTinh.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    foreach (HaiQuan_DonViTinh item in DVTCollection)
                                    {
                                        if (item.Ten == DonViTinh)
                                        {
                                            npl.DVT_ID = DonViTinh_GetID(DonViTinh);
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                                if (!isExits)
                                {
                                    errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                isExits = false;
                                npl.SoLuongDangKy = Convert.ToDecimal(wsr.Cells[SoLuongCol].Value);
                                if (npl.SoLuongDangKy.ToString().Trim().Length == 0)
                                {
                                    errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + npl.SoLuongDangKy + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(npl.SoLuongDangKy, FormatSoLuong) != npl.SoLuongDangKy)
                                    {
                                        errorSoluongValid += "[" + (wsr.Index + 1) + "]-[" + npl.SoLuongDangKy + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + npl.SoLuongDangKy + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                npl.DonGia = Convert.ToDecimal(wsr.Cells[DonGiaCol].Value);
                                if (npl.DonGia.ToString().Trim().Length == 0)
                                {
                                    errorDonGia += "[" + (wsr.Index + 1) + "]-[" + npl.DonGia + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorDonGia += "[" + (wsr.Index + 1) + "]-[" + npl.DonGia + "]\n";
                                isAdd = false;
                            }
                            npl.NguonCungCap = txtNguonCungCap.Text;
                            npl.TuCungUng = chkTCU.Checked;

                            if (isAdd)
                                NPLCollectionAdd.Add(npl);

                        }
                        catch (Exception ex)
                        {
                            this.SaveDefault();
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            return;
                        }
                    }
                }
                if (!String.IsNullOrEmpty(errorMaHangHoa))
                    errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorMaHangSpecialChar))
                    errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangSpecialChar + " CHỨA CÁC KÝ TỰ ĐẶC BIỆT HOẶC UNICODE HOẶC XUỐNG DÒNG \n";
                if (!String.IsNullOrEmpty(errorMaHangHoaExits))
                    errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaExits + " ĐÃ ĐƯỢC ĐĂNG KÝ \n";
                if (!String.IsNullOrEmpty(errorTenHangHoa))
                    errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorDVT))
                    errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVT + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorDVTExits))
                    errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVTExits + " KHÔNG HỢP LỆ .VÍ DỤ : CHIEC \n";
                if (!String.IsNullOrEmpty(errorMaHS))
                    errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHS + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorMaHSExits))
                    errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHSExits + " KHÔNG HỢP LỆ\n";
                if (!String.IsNullOrEmpty(errorSoluongValid))
                    errorTotal += "\n - [STT] -[SỐ LƯỢNG] : \n" + errorSoluongValid + " DOANH NGHIỆP CẤU HÌNH  CHỈ ĐƯỢC NHẬP TỐI ĐA " + FormatSoLuong + " SỐ THẬP PHÂN .ĐỂ CẤU HÌNH : VÀO MENU HỆ THỐNG - CẤU HÌNH THÔNG SỐ ĐỌC EXCEL\n";
                if (!String.IsNullOrEmpty(errorSoLuong))
                    errorTotal += "\n - [STT] -[SỐ LƯỢNG] : " + errorSoLuong + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorDonGia))
                    errorTotal += "\n - [STT] -[ĐƠN GIÁ] : \n" + errorDonGia + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (String.IsNullOrEmpty(errorTotal))
                {
                    foreach (NguyenPhuLieu item in NPLCollectionAdd)
                    {
                        if (FormImport=="PK")
                        {
                            NPLCollection.Add(item);
                        }
                        else
                        {
                            HD.NPLCollection.Add(item);
                        }
                    }
                    ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP HÀNG TỪ EXCEL THÀNH CÔNG ", false);
                    ImportExcelSucces = true;
                }
                else
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG ." + errorTotal, false);
                    ImportExcelSucces = false;
                    return;
                }
                this.SaveDefault();
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void NguyenPhuLieuReadExcelForm_Load(object sender, EventArgs e)
        {
            try
            {
                FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
                f.ReadDefaultNPL(cbbSheetName, txtRow,txtMaHangColumn,txtTenHangColumn,txtMaHSColumn,txtDVTColumn,txtSoLuong,txtDonGia,chkTCU);
                FormatSoLuong = f.FormatLuongNPL;
                FormatDonGia = f.FormatDonGiaNPL;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SaveDefault()
        {
            try
            {
                FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
                f.SaveConfigNPL(txtRow.Text, txtMaHangColumn.Text, txtTenHangColumn.Text, txtMaHSColumn.Text, txtDVTColumn.Text, txtSoLuong.Text, txtDonGia.Text, chkTCU.Checked);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void lblLinkExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_GC("NPL_HD");
            }
            catch (Exception ex) 
            {                
                Logger.LocalLogger.Instance().WriteMessage(ex); 
            }
        }

        private void chkTCU_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                txtNguonCungCap.Text = chkTCU.Checked == false ? "Bên thuê gia công cung cấp" : "Tự cung ứng";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}