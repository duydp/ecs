﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DongBoDuLieu.BLL.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using System.Globalization;
using System.Security.Cryptography;

namespace DongBo.Controls
{
    public partial class ucGC_TQ : ucDesign
    {
        frmMain frmParent = null;

        public Company.GC.BLL.KDT.GC.HopDong HD1 = new Company.GC.BLL.KDT.GC.HopDong();
        //Collection Hai Quan :
        public DataSet HQLoaiSPColection;
        public DataSet HQNPLColection;
        public DataSet HQSPColection;
        public DataSet HQTBColection;
        public DataSet HQDMColection;
        public DataSet HQGTKCTColection;
        public DataSet HQHCTColection;
        public DataSet HQGTKMDColection;
        public DataSet HQGHMDColection;
        public DataSet HQGBKCUColection;
        public DataSet HQGPKDKColection;

        //public DataSet HQLoaiSPDS;
        //public DataSet HQNPLDS;
        //public DataSet HQSPDS;
        //public DataSet HQTBDS;

        public DataSet HQDMDS;
        public DataSet HQGTKCTDS;
        public DataSet HQHCTDS;
        public DataSet HQGTKMDDS;
        public DataSet HQGHMDDS;
        public DataSet HQGBKCUDS;
        public DataSet HQGPKDKDS;

        public DataSet DNLoaiSPColection;
        public DataSet DNNPLColection;
        public DataSet DNSPColection;
        public DataSet DNTBColection;
        public DataSet DNDMColection;
        public DataSet DNGTKCTColection;
        public DataSet DNHCTColection;
        public DataSet DNGTKMDColection;
        public DataSet DNGHMDColection;
        public DataSet DNGBKCUColection;
        public DataSet DNGPKDKColection;

        public DataSet HQHopDong;
        public DataSet DNHopDong;
        public DataTable dtHQHopDong;
        public DataTable dtDNHopDong;

        public int trangthaidongbo = 0;
        public string PassAccess = "";
        //public Company.GC.BLL.GC.NhomSanPhamCollection HQLoaiSPColection;
        //public Company.GC.BLL.GC.NguyenPhuLieuCollection HQNPLColection;
        //public Company.GC.BLL.GC.SanPhamCollection HQSPColection;
        //public Company.GC.BLL.GC.ThietBiCollection HQTBColection;
        //public Company.GC.BLL.GC.DinhMucCollection HQDMColection;
        //public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection HQGTKCTColection;
        //public Company.GC.BLL.KDT.GC.HangChuyenTiepCollection HQHCTColection;
        //public Company.GC.BLL.KDT.ToKhaiMauDichCollection HQGTKMDColection;
        //public Company.GC.BLL.KDT.HangMauDichCollection HQGHMDColection;
        //public Company.GC.BLL.KDT.GC.BKCungUngDangKyCollection HQGBKCUColection;
        //public Company.GC.BLL.KDT.GC.PhuKienDangKyCollection HQGPKDKColection;

        //Collection Doanh nghiep :
        //public Company.GC.BLL.GC.NhomSanPhamCollection DNLoaiSPColection;
        //public Company.GC.BLL.GC.NguyenPhuLieuCollection DNNPLColection;
        //public Company.GC.BLL.GC.SanPhamCollection DNSPColection;
        //public Company.GC.BLL.GC.ThietBiCollection DNTBColection;
        //public Company.GC.BLL.GC.DinhMucCollection DNDMColection;
        //public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection DNGTKCTColection;
        //public Company.GC.BLL.KDT.GC.HangChuyenTiepCollection DNHCTColection;
        //public Company.GC.BLL.KDT.ToKhaiMauDichCollection DNGTKMDColection;
        //public Company.GC.BLL.KDT.HangMauDichCollection DNGHMDColection;
        //public Company.GC.BLL.KDT.GC.BKCungUngDangKyCollection DNGBKCUColection;
        //public Company.GC.BLL.KDT.GC.PhuKienDangKyCollection DNGPKDKColection;

        public ucGC_TQ()
        {
            InitializeComponent();
            frmParent = (frmMain)Application.OpenForms["frmMain"];
            MatKhauDN = GlobalSettings.MatKhau;
            GlobalSettings.MatKhau = GetMD5Value(GlobalSettings.MaDoanhNghiep + "+" + GlobalSettings.MatKhau);
            PassAccess = GlobalSettings.MatKhau;
        }
        public int namdk = 2008;
        public string sohopdong = "";
        private string MatKhauDN = "";
        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }

        #region Get method
        // Loai san pham :
        private DataSet GetLoaiSanPham(Company.GC.BLL.KDT.GC.HopDong HD)
        {

            DataSet ds = null;
            //DataSet dsHopDong = GetHopDong();
            try
            {
                ds = LoaiSP.GetDataSetLoaiSanPham(HD, GlobalSettings.MatKhau);
            }
            catch { }
            return ds;
        }
        // Nguyen phu lieu :
        private DataSet GetNguyenPhuLieu(Company.GC.BLL.KDT.GC.HopDong HD)
        {

            DataSet ds = null;
            //DataSet dsHopDong = GetHopDong();
            try
            {
                ds = NPL.GetDataSetNguyenPhuLieu(HD, GlobalSettings.MatKhau);
            }
            catch { }
            return ds;
        }
        // San pham :
        private DataSet GetSanPham(Company.GC.BLL.KDT.GC.HopDong HD)
        {

            DataSet ds = null;
            try
            {
                ds = SP.GetDataSetSanPham(HD, GlobalSettings.MatKhau);
            }
            catch { }
            return ds;
        }
        // Thiet Bi :
        private DataSet GetThietBi(Company.GC.BLL.KDT.GC.HopDong HD)
        {

            DataSet ds = null;
            try
            {
                ds = TB.GetDataSetThietBi(HD, GlobalSettings.MatKhau);
            }
            catch { }
            return ds;
        }
        private DataSet GetDinhMucDS(Company.GC.BLL.KDT.GC.HopDong HD)
        {

            DataSet ds = null;
            try
            {
                ds = DM.GetDataSetDinhMuc(HD, GlobalSettings.MatKhau);
                HQDMDS = ds;
            }
            catch { }
            return ds;
        }
        private DataSet GetToKhaiMD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;

            try
            {
                ds = TKMD.GetDataSetALLToKhaiByNgay(HD, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.MatKhau);
                HQGTKMDDS = ds;
            }
            catch { }
            return ds;
        }
        private DataSet GetHangMD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;

            try
            {
                ds = HMD.GetDataSetHangByNgay(HD, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.MatKhau);
                HQGHMDDS = ds;
            }
            catch { }
            return ds;
        }
        private DataSet GetToKhaiCT(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;

            try
            {
                ds = TKCT.GetDataSetToKhaiChuyenTiepByNgay(HD, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.MatKhau);
                HQGTKCTDS = ds;
            }
            catch { }
            return ds;
        }
        private DataSet GetHangCT(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;
            try
            {
                ds = HCT.GetDataSetHangChuyenTiepByNgay(HD, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.MatKhau);
                HQHCTDS = ds;
            }
            catch { }
            return ds;
        }
        private DataSet GetBangKeCU(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;

            try
            {
                ds = BKCU.GetDataSetBangKeCungUngByNgay(HD, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.MatKhau);
                HQGBKCUDS = ds;

            }
            catch { }
            return ds;
        }
        private DataSet GetSanPhamBangKeCU(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;
            try
            {
                ds = BKCU.GetDataSetSanPhamCungUngBy(HD, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.MatKhau);
            }
            catch { }
            return ds;
        }
        private DataSet GetNPLDSBangKeCU(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;
            try
            {
                ds = BKCU.GetDataSetNPLCungUngByNgay(HD, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.MatKhau);
            }
            catch { }
            return ds;
        }
        private DataSet GetSoPhuKien(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;
            try
            {
                ds = PK.GetDataSetPhuKien(HD, GlobalSettings.MatKhau);
                HQGPKDKDS = ds;
            }
            catch { }
            return ds;
        }
        private DataSet GetLoaiPhuKien(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;
            try
            {
                ds = PK.GetDataSetLoaiPhuKienByNgay(HD, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.MatKhau);
            }
            catch { }
            return ds;
        }
        private DataSet GetHangPhuKien(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;
            try
            {
                ds = PK.GetDataSetHangPhuKienByNgay(HD, GlobalSettings.NgayBatDau, GlobalSettings.NgayKetThuc, GlobalSettings.MatKhau);
            }
            catch { }
            return ds;
        }
        private DataSet GetHopDong()
        {
            DataSet dsHopDong = null;
            try
            {
                dsHopDong = HD.GetDataSetHopDong(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, namdk, GlobalSettings.MatKhau);
                HQHopDong = dsHopDong;
            }
            catch (Exception ex) { throw ex; }
            return dsHopDong;
        }
        #endregion
        private void GetHopDongAtDN(int t)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Company.GC.BLL.KDT.GC.HopDong hopdong = new Company.GC.BLL.KDT.GC.HopDong();
                if (t == 1)//Nguon
                {
                    hopdong.SetDabaseMoi("MSSQL");
                }

                DataTable dt;
                string where = string.Format(" [MaHaiQuan] LIKE '{0}' and MaDoanhNghiep='{1}'", GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep);
                if (GlobalSettings.IsChiLayDuLieuDaDuyet)
                    where += " AND TrangThaiXuLy = 1";
                if (GlobalSettings.NgayBatDau > DateTime.MinValue)
                    where += " and (NgayKy >= '" + GlobalSettings.NgayBatDau.ToString("yyyy-MM-dd 00:00:00") + "' AND NgayKy <= '" + GlobalSettings.NgayKetThuc.ToString("yyyy-MM-dd 23:59:59") + "')";
                if (t == 1)
                    if (GlobalSettings.IsSoSanhDuLieu)
                        where += "  AND NOT EXISTS ( SELECT SoHopDong ,NgayKy ,MaHaiQuan ,MaDoanhNghiep FROM [" + GlobalSettings.DBTarget + "].dbo.t_KDT_GC_HopDong b WHERE  t_kdt_gc_hopdong.SoHopDong = b.SoHopDong AND t_kdt_gc_hopdong.NgayKy = b.NgayKy AND t_kdt_gc_hopdong.MaHaiQuan = b.MaHaiQuan AND t_kdt_gc_hopdong.MaDoanhNghiep = b.MaDoanhNghiep )";
                where += " ORDER BY SoHopdong";

                if (t == 2)//Dich
                {
                    hopdong.SetDabaseMoi("MSSQLTarget");
                }

                dt = hopdong.SelectDynamic(where, "").Tables[0];

                if (t == 1)
                {
                    dtHQHopDong = dt;

                    cbHopDong.DataSource = dt;
                    cbHopDong.DisplayMember = "SoHopDong";
                    cbHopDong.ValueMember = "ID";

                    cbHopDong.SelectedIndex = cbHopDong.Items.Count > 0 ? 0 : -1;

                    lblHQ.Text = "DỮ LIỆU NGUỒN SERVER: [" + GlobalSettings.DiaChiSQLSource + "].[" + GlobalSettings.DBSource.ToUpper() + "]" + " - " + dtHQHopDong.Rows.Count + " Hợp đồng.";

                    if (cbHopDong.SelectedIndex == -1)
                        cbHopDong_SelectedIndexChanged(cbHopDong, EventArgs.Empty);
                }
                else
                {
                    dtDNHopDong = dt;

                    cbHopDongDN.DataSource = dt;
                    cbHopDongDN.DisplayMember = "SoHopDong";
                    cbHopDongDN.ValueMember = "ID";

                    cbHopDongDN.SelectedIndex = cbHopDongDN.Items.Count > 0 ? 0 : -1;

                    lblDN.Text = "DỮ LIỆU ĐÍCH SERVER: [" + GlobalSettings.DiaChiSQLTarget + "].[" + GlobalSettings.DBTarget.ToUpper() + "]" + " - " + dtDNHopDong.Rows.Count + " Hợp đồng.";

                    if (cbHopDongDN.SelectedIndex == -1)
                        cbHopDongDN_SelectedIndexChanged(cbHopDongDN, EventArgs.Empty);
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
            finally { Cursor = Cursors.Default; }
        }
        private DataSet GetHopDongDN()
        {
            DataSet ds = null;
            try
            {
                Company.GC.BLL.KDT.GC.HopDong hopdong = new Company.GC.BLL.KDT.GC.HopDong();

                string where = string.Format("[MaHaiQuan] LIKE '%{0}%' and MaDoanhNghiep='{1}'", GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep);
                ds = hopdong.SelectDynamic(where, "");
                DNHopDong = ds;
            }
            catch { }
            return ds;
        }

        #region DongBo Method
        public void LoaiSPOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsLoaiSP)
        {
            HD.NhomSPCollection = new Company.GC.BLL.KDT.GC.NhomSanPhamCollection();
            DataRow[] rowSelect = dsLoaiSP.Tables[0].Select("SoHopDong='" + HD.SoHopDong + "' and MaHaiQuan='" + HD.MaHaiQuan + "' and MaDoanhNghiep='" + HD.MaDoanhNghiep + "'");
            foreach (DataRow row in rowSelect)
            {
                Company.GC.BLL.KDT.GC.NhomSanPham NhomSP = new Company.GC.BLL.KDT.GC.NhomSanPham();
                if (row["Gia"].ToString() != "")
                    NhomSP.GiaGiaCong = Convert.ToDecimal(row["Gia"]);
                NhomSP.HopDong_ID = HD.ID;
                NhomSP.MaSanPham = row["Ma"].ToString().Trim();
                if (row["SoLuong"].ToString() != "")
                    NhomSP.SoLuong = Convert.ToDecimal(row["SoLuong"]);
                NhomSP.TenSanPham = Company.GC.BLL.DuLieuChuan.NhomSanPham.getTenSanPham(row["Ma"].ToString().Trim());
                HD.NhomSPCollection.Add(NhomSP);
            }
            //  HQLoaiSPColection = HD.nhomSPCollection;
        }
        public void DongBoLoaiSPFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsLoaiSP)
        {
            HD.NhomSPCollection = new Company.GC.BLL.KDT.GC.NhomSanPhamCollection();
            foreach (DataRow row in dsLoaiSP.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.NhomSanPham NhomSP = new Company.GC.BLL.KDT.GC.NhomSanPham();
                NhomSP.GiaGiaCong = Convert.ToDecimal(row["GiaGiaCong"]);
                NhomSP.HopDong_ID = HD.ID;
                NhomSP.MaSanPham = row["MaSanPham"].ToString().Trim();
                NhomSP.SoLuong = Convert.ToDecimal(row["SoLuong"]);
                NhomSP.TenSanPham = Company.GC.BLL.DuLieuChuan.NhomSanPham.getTenSanPham(row["MaSanPham"].ToString().Trim());
                HD.NhomSPCollection.Add(NhomSP);
            }
        }
        public void SanPhamOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsSP)
        {
            HD.SPCollection = new Company.GC.BLL.KDT.GC.SanPhamCollection();
            DataRow[] rowSelect = dsSP.Tables[0].Select("SoHopDong='" + HD.SoHopDong + "' and MaHaiQuan='" + HD.MaHaiQuan + "' and MaDoanhNghiep='" + HD.MaDoanhNghiep + "'");
            foreach (DataRow row in rowSelect)
            {
                Company.GC.BLL.KDT.GC.SanPham SP = new Company.GC.BLL.KDT.GC.SanPham();
                SP.DVT_ID = row["DVT_ID"].ToString();
                SP.HopDong_ID = HD.ID;
                SP.Ma = row["Ma"].ToString().Substring(1).Trim();
                SP.MaHS = row["MaHS"].ToString().Trim();
                SP.NhomSanPham_ID = row["Nhom_SP"].ToString();
                if (row["SoLuongDangKy"].ToString() != "")
                    SP.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"]);
                SP.Ten = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Ten"].ToString().Trim());
                HD.SPCollection.Add(SP);
            }
            //HQSPColection = HD.SPCollection;
        }
        public void DongBoSPFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsSP)
        {
            HD.SPCollection = new Company.GC.BLL.KDT.GC.SanPhamCollection();
            foreach (DataRow row in dsSP.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.SanPham SP = new Company.GC.BLL.KDT.GC.SanPham();
                SP.DVT_ID = row["DVT_ID"].ToString();
                SP.HopDong_ID = HD.ID;
                SP.Ma = row["Ma"].ToString().Trim();
                SP.MaHS = row["MaHS"].ToString().Trim();
                SP.NhomSanPham_ID = row["NhomSanPham_ID"].ToString();
                if (row["SoLuongDangKy"].ToString() != "")
                    SP.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"]);
                SP.Ten = (row["Ten"].ToString().Trim());
                HD.SPCollection.Add(SP);
            }
        }
        public void NguyenPhuLieuOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsNPL)
        {
            HD.NPLCollection = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCollection();
            DataRow[] rowSelect = dsNPL.Tables[0].Select("SoHopDong='" + HD.SoHopDong + "' and MaHaiQuan='" + HD.MaHaiQuan + "' and MaDoanhNghiep='" + HD.MaDoanhNghiep + "'");
            foreach (DataRow row in rowSelect)
            {
                Company.GC.BLL.KDT.GC.NguyenPhuLieu npl = new Company.GC.BLL.KDT.GC.NguyenPhuLieu();
                npl.DVT_ID = row["DVT_ID"].ToString();
                npl.HopDong_ID = HD.ID;
                npl.Ma = row["Ma"].ToString().Substring(1).Trim();
                npl.MaHS = row["MaHS"].ToString().Trim();
                npl.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"].ToString());
                npl.Ten = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Ten"].ToString().Trim());
                HD.NPLCollection.Add(npl);
            }
            //HQNPLColection = HD.NPLCollection;
        }
        public void DongBoNPLFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsNPL)
        {
            HD.NPLCollection = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCollection();
            foreach (DataRow row in dsNPL.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.NguyenPhuLieu npl = new Company.GC.BLL.KDT.GC.NguyenPhuLieu();
                npl.DVT_ID = row["DVT_ID"].ToString();
                npl.HopDong_ID = HD.ID;
                npl.Ma = row["Ma"].ToString().Trim();
                npl.MaHS = row["MaHS"].ToString().Trim();
                npl.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"].ToString());
                npl.Ten = (row["Ten"].ToString().Trim());
                HD.NPLCollection.Add(npl);
            }
        }
        public void ThietBiOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsTB)
        {
            HD.TBCollection = new Company.GC.BLL.KDT.GC.ThietBiCollection();
            DataRow[] rowSelect = dsTB.Tables[0].Select("SoHopDong='" + HD.SoHopDong + "' and MaHaiQuan='" + HD.MaHaiQuan + "' and MaDoanhNghiep='" + HD.MaDoanhNghiep + "'");
            foreach (DataRow row in rowSelect)
            {
                Company.GC.BLL.KDT.GC.ThietBi tb = new Company.GC.BLL.KDT.GC.ThietBi();
                tb.DonGia = Convert.ToDouble(row["DonGia"]);
                tb.DVT_ID = row["DVT_ID"].ToString();
                tb.HopDong_ID = HD.ID;
                tb.Ma = row["Ma"].ToString().Trim().Substring(1);
                tb.MaHS = row["MaHS"].ToString().Trim();
                tb.NguyenTe_ID = row["NguyenTe_ID"].ToString();
                tb.NuocXX_ID = row["NuocXX_ID"].ToString();
                tb.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"]);
                tb.Ten = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Ten"].ToString().Trim());
                tb.TinhTrang = row["TinhTrang"].ToString().Trim();
                tb.TriGia = Convert.ToDouble(row["TriGia"]);
                HD.TBCollection.Add(tb);
            }
            //TBColection = HD.TBCollection;
        }
        public void DongBoTBFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsTB)
        {
            HD.TBCollection = new Company.GC.BLL.KDT.GC.ThietBiCollection();
            foreach (DataRow row in dsTB.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.ThietBi tb = new Company.GC.BLL.KDT.GC.ThietBi();
                tb.DonGia = Convert.ToDouble(row["DonGia"]);
                tb.DVT_ID = row["DVT_ID"].ToString();
                tb.HopDong_ID = HD.ID;
                tb.Ma = row["Ma"].ToString().Trim();
                tb.MaHS = row["MaHS"].ToString().Trim();
                tb.NguyenTe_ID = row["NguyenTe_ID"].ToString();
                tb.NuocXX_ID = row["NuocXX_ID"].ToString();
                tb.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"]);
                tb.Ten = (row["Ten"].ToString().Trim());
                tb.TinhTrang = row["TinhTrang"].ToString().Trim();
                tb.TriGia = Convert.ToDouble(row["TriGia"]);
                HD.TBCollection.Add(tb);
            }
            //TBColection = HD.TBCollection;
        }
        public void DinhMucOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM, int dk)
        {
            Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
            foreach (DataRow row in dsDM.Tables[0].Rows)
            {
                Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                DM.DinhMucSuDung = Convert.ToDecimal(row["DinhMucSuDung"]);
                DM.HopDong_ID = HD.ID;
                DM.MaNguyenPhuLieu = row["MaNguyenPhuLieu"].ToString().Trim();
                DM.MaSanPham = row["MaSanPham"].ToString().Trim();
                DM.NPL_TuCungUng = Convert.ToDecimal(row["NPL_TuCungUng"]);
                DM.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = DM.MaNguyenPhuLieu;
                npl.Load();
                DM.TenNPL = npl.Ten;
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = DM.MaSanPham;
                sp.Load();
                DM.TenSanPham = sp.Ten;
                dmCollection.Add(DM);
            }
            if (dmCollection.Count > 0)
            {
                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                dm.InsertUpdate(dmCollection, HD);
            }

        }
        public void DinhMucOfHDKTX(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM, int dk)
        {
            Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
            foreach (DataRow row in dsDM.Tables[0].Rows)
            {
                Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                DM.DinhMucSuDung = Convert.ToDecimal(row["DinhMucSuDung"]);
                DM.HopDong_ID = HD.ID;
                DM.MaNguyenPhuLieu = row["MaNguyenPhuLieu"].ToString().Trim();
                DM.MaSanPham = row["MaSanPham"].ToString().Trim();
                DM.NPL_TuCungUng = Convert.ToDecimal(row["NPL_TuCungUng"]);
                DM.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);

                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = DM.MaNguyenPhuLieu;
                npl.Load(); //"MSSQLTARGET"
                DM.TenNPL = npl.Ten;

                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = DM.MaSanPham;
                sp.LoadKTX();
                DM.TenSanPham = sp.Ten;
                dmCollection.Add(DM);
            }
            if (dmCollection.Count > 0)
            {
                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                dm.InsertUpdate(dmCollection, HD);
            }

        }
        public void DinhMucOfHDKTX(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM, int dk, string dbName)
        {
            Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
            foreach (DataRow row in dsDM.Tables[0].Rows)
            {
                Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                DM.DinhMucSuDung = Convert.ToDecimal(row["DinhMucSuDung"]);
                DM.HopDong_ID = HD.ID;
                DM.MaNguyenPhuLieu = row["MaNguyenPhuLieu"].ToString().Trim();
                DM.MaSanPham = row["MaSanPham"].ToString().Trim();
                DM.NPL_TuCungUng = Convert.ToDecimal(row["NPL_TuCungUng"]);
                DM.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);

                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = DM.MaNguyenPhuLieu;
                npl.Load(dbName);
                DM.TenNPL = npl.Ten;

                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = DM.MaSanPham;
                sp.LoadKTX(dbName);
                DM.TenSanPham = sp.Ten;
                dmCollection.Add(DM);
            }
            if (dmCollection.Count > 0)
            {
                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                dm.InsertUpdate(dmCollection, HD, dbName);
            }

        }
        public void DinhMucOfHDTQDT(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM, int dk, string dbName)
        {
            Company.GC.BLL.KDT.GC.DinhMucDangKyCollection dmdkCollection = new DinhMucDangKyCollection();

            Company.GC.BLL.KDT.GC.DinhMucDangKy dmdk = new DinhMucDangKy();
            dmdk.ID_HopDong = dk; // HD.ID;
            dmdkCollection = dmdk.SelectCollectionBy_ID_HopDong("MSSQL");

            foreach (DinhMucDangKy item in dmdkCollection)
            {
                item.ID_HopDong = HD.ID;
                item.InsertUpdateTQDT(dbName);
            }

        }
        public void DongBoDinhMucMoiFromDBTamToDBOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM)
        {
            Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
            foreach (DataRow row in dsDM.Tables[0].Rows)
            {
                Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                DM.DinhMucSuDung = Convert.ToDecimal(row["DinhMucSuDung"]);
                DM.HopDong_ID = HD.ID;
                DM.MaNguyenPhuLieu = row["MaNguyenPhuLieu"].ToString().Trim();
                DM.MaSanPham = row["MaSanPham"].ToString().Trim();
                DM.NPL_TuCungUng = Convert.ToDecimal(row["NPL_TuCungUng"]);
                DM.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);
                if (DM.Load())
                    continue;

                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = DM.MaNguyenPhuLieu;
                npl.Load();

                DM.TenNPL = npl.Ten;
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = DM.MaSanPham;
                sp.Load();

                DM.TenSanPham = sp.Ten;
                dmCollection.Add(DM);
            }
            if (dmCollection.Count > 0)
            {
                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                dm.InsertUpdate(dmCollection, HD);
            }

        }
        public void DongBoDinhMucMoiFromDBTamToDBOfHDKTX(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM)
        {
            Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
            foreach (DataRow row in dsDM.Tables[0].Rows)
            {
                Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                DM.DinhMucSuDung = Convert.ToDecimal(row["DinhMucSuDung"]);
                DM.HopDong_ID = HD.ID;
                DM.MaNguyenPhuLieu = row["MaNguyenPhuLieu"].ToString().Trim();
                DM.MaSanPham = row["MaSanPham"].ToString().Trim();
                DM.NPL_TuCungUng = Convert.ToDecimal(row["NPL_TuCungUng"]);
                DM.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);
                if (DM.Load())
                    continue;

                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = DM.MaNguyenPhuLieu;
                npl.Load();

                DM.TenNPL = npl.Ten;
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = DM.MaSanPham;
                sp.LoadKTX();

                DM.TenSanPham = sp.Ten;
                dmCollection.Add(DM);
            }
            if (dmCollection.Count > 0)
            {
                string databaseName = "MSSQLTARGET";
                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                dm.InsertUpdate(dmCollection, HD, databaseName);
            }

        }
        public void DongBoDinhMucMoiFromDBTamToDBOfHDKTX(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM, string databaseName)
        {
            Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
            foreach (DataRow row in dsDM.Tables[0].Rows)
            {
                Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                DM.DinhMucSuDung = Convert.ToDecimal(row["DinhMucSuDung"]);
                DM.HopDong_ID = HD.ID;
                DM.MaNguyenPhuLieu = row["MaNguyenPhuLieu"].ToString().Trim();
                DM.MaSanPham = row["MaSanPham"].ToString().Trim();
                DM.NPL_TuCungUng = Convert.ToDecimal(row["NPL_TuCungUng"]);
                DM.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);
                if (DM.Load(databaseName))
                    continue;

                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = DM.MaNguyenPhuLieu;
                npl.Load(databaseName);
                DM.TenNPL = npl.Ten;

                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = DM.MaSanPham;
                sp.LoadKTX(databaseName);
                DM.TenSanPham = sp.Ten;

                dmCollection.Add(DM);
            }
            if (dmCollection.Count > 0)
            {
                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                dm.InsertUpdate(dmCollection, HD, databaseName);
            }

        }
        public void LayThongTinTKMD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet dsToKhai = GetToKhaiMD(HD);
            DataSet dsHang = GetHangMD(HD);
            TKMD.InsertGhiDeALLToKhai(HD, dsToKhai, dsHang);
        }
        public void LayThongTinBKCU(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet dsBangKe = GetBangKeCU(HD);
            DataSet dsSanPhamBangKe = GetSanPhamBangKeCU(HD);
            DataSet dsNPLBangKe = GetNPLDSBangKeCU(HD);

            BKCU.InsertGhiDeALLBangKeCungUng(HD, dsBangKe, dsSanPhamBangKe, dsNPLBangKe);
        }
        public void LayThongTinPhuKien(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet dsPhuKienDK = GetSoPhuKien(HD);
            DataSet dsLoaiPK = GetLoaiPhuKien(HD);
            DataSet dsHangPK = GetHangPhuKien(HD);
            PK.InsertGhiDeALLPhuKien(HD, dsPhuKienDK, dsLoaiPK, dsHangPK);

        }
        public void DongBoTKMDFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP)
        {
            ToKhaiMauDich TKMDDBTamp = new ToKhaiMauDich();
            TKMDDBTamp.IDHopDong = id_HDTMP;
            TKMDDBTamp.SetDabaseMoi("MSSQL");
            ToKhaiMauDichCollection TKMDCollection = TKMDDBTamp.SelectCollectionBy_IDHopDong();
            foreach (ToKhaiMauDich TKitem in TKMDCollection)
            {
                TKitem.LoadHMDCollection("MSSQL");
                TKitem.ID = 0;
                TKitem.TrangThaiPhanBo = 0;
                TKitem.IDHopDong = HD.ID;
                foreach (HangMauDich HMD in TKitem.HMDCollection)
                    HMD.ID = 0;
            }
            if (TKMDCollection.Count > 0)
            {
                try
                {
                    ToKhaiMauDich tk = new ToKhaiMauDich();
                    tk.InsertUpdateDongBoDuLieu(TKMDCollection, HD);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi khi đồng bộ dữ liệu TKMD : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
                }
            }
        }
        public void DongBoTKMDFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP, string dbName)
        {
            ToKhaiMauDich TKMDDBTamp = new ToKhaiMauDich();
            TKMDDBTamp.IDHopDong = id_HDTMP;
            TKMDDBTamp.SetDabaseMoi("MSSQL");
            ToKhaiMauDichCollection TKMDCollection = TKMDDBTamp.SelectCollectionBy_IDHopDong("MSSQL");
            foreach (ToKhaiMauDich TKitem in TKMDCollection)
            {
                TKitem.LoadHMDCollection("MSSQl");
                TKitem.ID = 0;
                TKitem.TrangThaiPhanBo = 0;
                TKitem.IDHopDong = HD.ID;
                foreach (HangMauDich HMD in TKitem.HMDCollection)
                    HMD.ID = 0;
            }
            if (TKMDCollection.Count > 0)
            {
                try
                {
                    ToKhaiMauDich tk = new ToKhaiMauDich();
                    tk.InsertUpdateDongBoDuLieu(TKMDCollection, HD, dbName);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi khi đồng bộ dữ liệu TKMD : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
                }
            }
        }
        public void DongBoTKMDFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP)
        {
            ToKhaiMauDich TKMDDBTamp = new ToKhaiMauDich();
            TKMDDBTamp.IDHopDong = id_HDTMP;
            TKMDDBTamp.SetDabaseMoi("MSSQL");
            ToKhaiMauDichCollection TKMDCollection = TKMDDBTamp.SelectCollectionBy_IDHopDong_KTX();
            foreach (ToKhaiMauDich TKitem in TKMDCollection)
            {
                TKitem.LoadHMDCollection("MSSQL");
                TKitem.ID = 0;
                TKitem.TrangThaiPhanBo = 0;
                TKitem.IDHopDong = HD.ID;
                foreach (HangMauDich HMD in TKitem.HMDCollection)
                    HMD.ID = 0;
            }
            if (TKMDCollection.Count > 0)
            {
                try
                {
                    ToKhaiMauDich tk = new ToKhaiMauDich();
                    tk.InsertUpdateDongBoDuLieuKTX(TKMDCollection, HD);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi khi đồng bộ dữ liệu TKMD : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
                }
            }
        }
        public void DongBoTKMDFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP, string dbName)
        {
            ToKhaiMauDich TKMDDBTamp = new ToKhaiMauDich();
            TKMDDBTamp.IDHopDong = id_HDTMP;
            TKMDDBTamp.SetDabaseMoi("MSSQL");
            ToKhaiMauDichCollection TKMDCollection = TKMDDBTamp.SelectCollectionBy_IDHopDong_KTX();
            foreach (ToKhaiMauDich TKitem in TKMDCollection)
            {
                TKitem.LoadHMDCollection("MSSQL");
                TKitem.ID = 0;
                TKitem.TrangThaiPhanBo = 0;
                TKitem.IDHopDong = HD.ID;
                foreach (HangMauDich HMD in TKitem.HMDCollection)
                    HMD.ID = 0;
            }
            if (TKMDCollection.Count > 0)
            {
                try
                {
                    ToKhaiMauDich tk = new ToKhaiMauDich();
                    tk.InsertUpdateDongBoDuLieuKTX(TKMDCollection, HD, dbName);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi khi đồng bộ dữ liệu TKMD : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
                }
            }
        }
        public void DongBoTKChuyenTiepFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP)
        {
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
            ToKhaiChuyenTiep TKCTDBTam = new ToKhaiChuyenTiep();
            TKCTDBTam.IDHopDong = id_HDTMP;
            TKCTDBTam.SetDabaseMoi("MSSQL");
            TKCTCollection = TKCTDBTam.SelectCollectionBy_IDHopDong();
            foreach (ToKhaiChuyenTiep TKCTItem in TKCTCollection)
            {
                //TKCTItem.LoadHCTCollection("MSSQL");
                TKCTItem.ID = 0;
                TKCTItem.IDHopDong = HD.ID;
                foreach (HangChuyenTiep HCT in TKCTItem.HCTCollection)
                    HCT.ID = 0;
            }
            try
            {
                ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                tkct.InsertUpdateDongBoDuLieu(TKCTCollection, HD);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi khi đồng bộ dữ liệu TKCT : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
            }
        }
        public void DongBoTKChuyenTiepFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP, string dbName)
        {
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
            ToKhaiChuyenTiep TKCTDBTam = new ToKhaiChuyenTiep();
            TKCTDBTam.IDHopDong = id_HDTMP;
            TKCTDBTam.SetDabaseMoi("MSSQL");
            TKCTCollection = TKCTDBTam.SelectCollectionBy_IDHopDong("MSSQL");

            foreach (ToKhaiChuyenTiep TKCTItem in TKCTCollection)
            {
                //TKCTItem.LoadHCTCollection("MSSQL");
                TKCTItem.ID = 0;
                TKCTItem.IDHopDong = HD.ID;
                foreach (HangChuyenTiep HCT in TKCTItem.HCTCollection)
                    HCT.ID = 0;
            }
            try
            {
                ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                tkct.InsertUpdateDongBoDuLieu(TKCTCollection, HD, dbName);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi khi đồng bộ dữ liệu TKCT : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
            }
        }
        public void DongBoTKChuyenTiepFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP)
        {
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
            ToKhaiChuyenTiep TKCTDBTam = new ToKhaiChuyenTiep();
            TKCTDBTam.IDHopDong = id_HDTMP;
            TKCTDBTam.SetDabaseMoi("MSSQL");
            TKCTCollection = TKCTDBTam.SelectCollectionBy_IDHopDong_KTX();
            foreach (ToKhaiChuyenTiep TKCTItem in TKCTCollection)
            {
                //TKCTItem.LoadHCTCollection("MSSQL");
                TKCTItem.ID = 0;
                TKCTItem.IDHopDong = HD.ID;
                foreach (HangChuyenTiep HCT in TKCTItem.HCTCollection)
                    HCT.ID = 0;
            }
            try
            {
                ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                tkct.InsertUpdateDongBoDuLieuKTX(TKCTCollection, HD);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi khi đồng bộ dữ liệu TKCT : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
            }
        }
        public void DongBoTKChuyenTiepFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP, string dbName)
        {
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
            ToKhaiChuyenTiep TKCTDBTam = new ToKhaiChuyenTiep();
            TKCTDBTam.IDHopDong = id_HDTMP;
            TKCTDBTam.SetDabaseMoi("MSSQL");
            TKCTCollection = TKCTDBTam.SelectCollectionBy_IDHopDong_KTX();
            foreach (ToKhaiChuyenTiep TKCTItem in TKCTCollection)
            {
                //TKCTItem.LoadHCTCollection("MSSQL");
                TKCTItem.ID = 0;
                TKCTItem.IDHopDong = HD.ID;
                foreach (HangChuyenTiep HCT in TKCTItem.HCTCollection)
                    HCT.ID = 0;
            }
            try
            {
                ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                tkct.InsertUpdateDongBoDuLieuKTX(TKCTCollection, HD, dbName);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi khi đồng bộ dữ liệu TKCT : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
            }
        }
        public void DongBoPhuKienFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP)
        {
            PhuKienDangKy pkdk = new PhuKienDangKy();
            pkdk.HopDong_ID = ID_HDTMP;
            pkdk.SetDabaseMoi("MSSQL");
            PhuKienDangKyCollection pkdkCollection = pkdk.SelectCollectionBy_HopDong_ID();
            foreach (PhuKienDangKy pkdkItem in pkdkCollection)
            {
                pkdkItem.LoadCollection("MSSQL");
                pkdkItem.ID = 0;
                pkdkItem.HopDong_ID = HD.ID;
                foreach (LoaiPhuKien loaiPK in pkdkItem.PKCollection)
                {
                    loaiPK.LoadCollection("MSSQL");
                    loaiPK.ID = 0;
                    foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                    {
                        hangPK.ID = 0;

                    }
                }
            }


            try
            {
                PhuKienDangKy pk = new PhuKienDangKy();
                pk.InsertUpdateDongBoDuLieu(pkdkCollection, HD);
            }
            catch { }

        }
        public void DongBoPhuKienFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP, string dbName)
        {
            PhuKienDangKy pkdk = new PhuKienDangKy();
            pkdk.HopDong_ID = ID_HDTMP;
            pkdk.SetDabaseMoi("MSSQL");
            PhuKienDangKyCollection pkdkCollection = pkdk.SelectCollectionBy_HopDong_ID("MSSQL");
            foreach (PhuKienDangKy pkdkItem in pkdkCollection)
            {
                pkdkItem.LoadCollection("MSSQL");
                pkdkItem.ID = 0;
                pkdkItem.HopDong_ID = HD.ID;
                foreach (LoaiPhuKien loaiPK in pkdkItem.PKCollection)
                {
                    loaiPK.LoadCollection("MSSQL");
                    loaiPK.ID = 0;
                    foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                    {
                        hangPK.ID = 0;

                    }
                }
            }


            try
            {
                PhuKienDangKy pk = new PhuKienDangKy();
                pk.InsertUpdateDongBoDuLieu(pkdkCollection, HD, dbName);
            }
            catch (Exception ex) { throw ex; }

        }
        public void DongBoPhuKienFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP)
        {
            PhuKienDangKy pkdk = new PhuKienDangKy();
            pkdk.HopDong_ID = ID_HDTMP;
            pkdk.SetDabaseMoi("MSSQL");
            PhuKienDangKyCollection pkdkCollection = pkdk.SelectCollectionBy_HopDong_ID_KTX();
            foreach (PhuKienDangKy pkdkItem in pkdkCollection)
            {
                pkdkItem.LoadCollection("MSSQL");
                pkdkItem.ID = 0;
                pkdkItem.HopDong_ID = HD.ID;
                foreach (LoaiPhuKien loaiPK in pkdkItem.PKCollection)
                {
                    loaiPK.LoadCollection("MSSQL");
                    loaiPK.ID = 0;
                    foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                    {
                        hangPK.ID = 0;

                    }
                }
            }


            try
            {
                PhuKienDangKy pk = new PhuKienDangKy();
                pk.InsertUpdateDongBoDuLieuKTX(pkdkCollection, HD);
            }
            catch { }

        }
        public void DongBoPhuKienFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP, string dbName)
        {
            PhuKienDangKy pkdk = new PhuKienDangKy();
            pkdk.HopDong_ID = ID_HDTMP;
            pkdk.SetDabaseMoi("MSSQL");
            PhuKienDangKyCollection pkdkCollection = pkdk.SelectCollectionBy_HopDong_ID_KTX();
            foreach (PhuKienDangKy pkdkItem in pkdkCollection)
            {
                pkdkItem.LoadCollection("MSSQL");
                pkdkItem.ID = 0;
                pkdkItem.HopDong_ID = HD.ID;
                foreach (LoaiPhuKien loaiPK in pkdkItem.PKCollection)
                {
                    loaiPK.LoadCollection("MSSQL");
                    loaiPK.ID = 0;
                    foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                    {
                        hangPK.ID = 0;

                    }
                }
            }


            try
            {
                PhuKienDangKy pk = new PhuKienDangKy();
                pk.InsertUpdateDongBoDuLieuKTX(pkdkCollection, HD, dbName);
            }
            catch { }

        }
        private void DongBoBKCUFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP)
        {
            BKCungUngDangKy bkTMP = new BKCungUngDangKy();
            bkTMP.SetDabaseMoi("MSSQL");
            BKCungUngDangKyCollection BKCUDangKyCollection = bkTMP.SelectBangKeCUByHopDong(ID_HDTMP);

            foreach (BKCungUngDangKy BKCU in BKCUDangKyCollection)
            {
                BKCU.LoadSanPhamCungUngCollection("MSSQL");
                BKCU.ID = 0;
                BKCU.TKCT_ID = 0;
                BKCU.TKMD_ID = 0;
                if (BKCU.isTKMD == 0)
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.SoToKhai = BKCU.SoToKhai;
                    TKMD.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKMD.MaHaiQuan = Properties.Settings.Default.MaHaiQuan;
                    //TKMD.SetDabaseMoi("MSSQL");
                    if (TKMD.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKMD.ID;
                    else
                        continue;
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.SoToKhai = BKCU.SoToKhai;
                    TKCT.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKCT.MaHaiQuanTiepNhan = Properties.Settings.Default.MaHaiQuan;
                    //TKCT.SetDabaseMoi("MSSQL");
                    if (TKCT.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKCT.ID;
                    else
                        continue;
                }
                foreach (SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                {
                    spCU.LoadNPLCungUngCollection("MSSQL");
                    spCU.ID = 0;
                    foreach (NguyenPhuLieuCungUng NPLCu in spCU.NPLCungUngCollection)
                    {
                        NPLCu.ID = 0;
                    }
                }
            }
            BKCungUngDangKy BKCUUpdate = new BKCungUngDangKy();
            BKCUUpdate.InsertUpdateDongBoDuLieu(BKCUDangKyCollection, HD);
        }
        private void DongBoBKCUFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP, string dbName)
        {
            BKCungUngDangKy bkTMP = new BKCungUngDangKy();
            bkTMP.SetDabaseMoi("MSSQL");
            BKCungUngDangKyCollection BKCUDangKyCollection = bkTMP.SelectBangKeCUByHopDong(ID_HDTMP, "MSSQL");

            foreach (BKCungUngDangKy BKCU in BKCUDangKyCollection)
            {
                BKCU.LoadSanPhamCungUngCollection("MSSQL");
                BKCU.ID = 0;
                BKCU.TKCT_ID = 0;
                BKCU.TKMD_ID = 0;
                if (BKCU.isTKMD == 0)
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.SoToKhai = BKCU.SoToKhai;
                    TKMD.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKMD.MaHaiQuan = Properties.Settings.Default.MaHaiQuan;
                    TKMD.SetDabaseMoi("MSSQL");
                    if (TKMD.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKMD.ID;
                    else
                        continue;
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.SoToKhai = BKCU.SoToKhai;
                    TKCT.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKCT.MaHaiQuanTiepNhan = Properties.Settings.Default.MaHaiQuan;
                    TKCT.SetDabaseMoi("MSSQL");
                    if (TKCT.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKCT.ID;
                    else
                        continue;
                }
                foreach (SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                {
                    spCU.LoadNPLCungUngCollection("MSSQL");
                    spCU.ID = 0;
                    foreach (NguyenPhuLieuCungUng NPLCu in spCU.NPLCungUngCollection)
                    {
                        NPLCu.ID = 0;
                    }
                }
            }
            BKCungUngDangKy BKCUUpdate = new BKCungUngDangKy();
            BKCUUpdate.InsertUpdateDongBoDuLieu(BKCUDangKyCollection, HD, dbName);
        }
        private void DongBoBKCUFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP)
        {
            BKCungUngDangKy bkTMP = new BKCungUngDangKy();
            bkTMP.SetDabaseMoi("MSSQL");
            BKCungUngDangKyCollection BKCUDangKyCollection = bkTMP.SelectBangKeCUByHopDong(ID_HDTMP);

            foreach (BKCungUngDangKy BKCU in BKCUDangKyCollection)
            {
                BKCU.LoadSanPhamCungUngCollection("MSSQL");
                BKCU.ID = 0;
                BKCU.TKCT_ID = 0;
                BKCU.TKMD_ID = 0;
                if (BKCU.isTKMD == 0)
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.SoToKhai = BKCU.SoToKhai;
                    TKMD.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKMD.MaHaiQuan = Properties.Settings.Default.MaHaiQuan;
                    //TKMD.SetDabaseMoi("MSSQL");
                    if (TKMD.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKMD.ID;
                    else
                        continue;
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.SoToKhai = BKCU.SoToKhai;
                    TKCT.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKCT.MaHaiQuanTiepNhan = Properties.Settings.Default.MaHaiQuan;
                    //TKCT.SetDabaseMoi("MSSQL");
                    if (TKCT.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKCT.ID;
                    else
                        continue;
                }
                foreach (SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                {
                    spCU.LoadNPLCungUngCollection("MSSQL");
                    spCU.ID = 0;
                    foreach (NguyenPhuLieuCungUng NPLCu in spCU.NPLCungUngCollection)
                    {
                        NPLCu.ID = 0;
                    }
                }
            }
            BKCungUngDangKy BKCUUpdate = new BKCungUngDangKy();
            BKCUUpdate.InsertUpdateDongBoDuLieuKTX(BKCUDangKyCollection, HD);
        }
        private void DongBoBKCUFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP, string dbName)
        {
            BKCungUngDangKy bkTMP = new BKCungUngDangKy();
            bkTMP.SetDabaseMoi("MSSQL");
            BKCungUngDangKyCollection BKCUDangKyCollection = bkTMP.SelectBangKeCUByHopDong(ID_HDTMP);

            foreach (BKCungUngDangKy BKCU in BKCUDangKyCollection)
            {
                BKCU.LoadSanPhamCungUngCollection("MSSQL");
                BKCU.ID = 0;
                BKCU.TKCT_ID = 0;
                BKCU.TKMD_ID = 0;
                if (BKCU.isTKMD == 0)
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.SoToKhai = BKCU.SoToKhai;
                    TKMD.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKMD.MaHaiQuan = Properties.Settings.Default.MaHaiQuan;
                    TKMD.SetDabaseMoi("MSSQL");
                    if (TKMD.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKMD.ID;
                    else
                        continue;
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.SoToKhai = BKCU.SoToKhai;
                    TKCT.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKCT.MaHaiQuanTiepNhan = Properties.Settings.Default.MaHaiQuan;
                    TKCT.SetDabaseMoi("MSSQL");
                    if (TKCT.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKCT.ID;
                    else
                        continue;
                }
                foreach (SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                {
                    spCU.LoadNPLCungUngCollection("MSSQL");
                    spCU.ID = 0;
                    foreach (NguyenPhuLieuCungUng NPLCu in spCU.NPLCungUngCollection)
                    {
                        NPLCu.ID = 0;
                    }
                }
            }
            BKCungUngDangKy BKCUUpdate = new BKCungUngDangKy();
            BKCUUpdate.InsertUpdateDongBoDuLieuKTX(BKCUDangKyCollection, HD, dbName);
        }
        private void DongBoBKCUMoiFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP)
        {
            BKCungUngDangKy bkTMP = new BKCungUngDangKy();
            bkTMP.SetDabaseMoi("MSSQL");
            BKCungUngDangKyCollection BKCUDangKyCollection = bkTMP.SelectBangKeCUByHopDong(ID_HDTMP);
            BKCungUngDangKyCollection BKCUDangKyMoiCollection = new BKCungUngDangKyCollection();

            foreach (BKCungUngDangKy BKCU in BKCUDangKyCollection)
            {
                if (BKCU.LoadBySoBangKe())
                {
                    continue;
                }
                BKCU.LoadSanPhamCungUngCollection("MSSQL");
                BKCU.ID = 0;
                BKCU.TKCT_ID = 0;
                BKCU.TKMD_ID = 0;
                if (BKCU.isTKMD == 0)
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.SoToKhai = BKCU.SoToKhai;
                    TKMD.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKMD.MaHaiQuan = Properties.Settings.Default.MaHaiQuan;
                    //TKMD.SetDabaseMoi("MSSQL");
                    if (TKMD.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKMD.ID;
                    else
                        continue;
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.SoToKhai = BKCU.SoToKhai;
                    TKCT.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKCT.MaHaiQuanTiepNhan = Properties.Settings.Default.MaHaiQuan;
                    //TKCT.SetDabaseMoi("MSSQL");
                    if (TKCT.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKCT.ID;
                    else
                        continue;
                }
                foreach (SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                {
                    spCU.LoadNPLCungUngCollection("MSSQL");
                    spCU.ID = 0;
                    foreach (NguyenPhuLieuCungUng NPLCu in spCU.NPLCungUngCollection)
                    {
                        NPLCu.ID = 0;
                    }
                }
                BKCUDangKyMoiCollection.Add(BKCU);
            }
            if (BKCUDangKyMoiCollection.Count > 0)
            {
                BKCungUngDangKy BKCUUpdate = new BKCungUngDangKy();
                BKCUUpdate.InsertUpdateDongBoDuLieuMOi(BKCUDangKyMoiCollection, HD);
            }

        }
        private void DongBoBKCUMoiFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP)
        {
            BKCungUngDangKy bkTMP = new BKCungUngDangKy();
            bkTMP.SetDabaseMoi("MSSQL");
            BKCungUngDangKyCollection BKCUDangKyCollection = bkTMP.SelectBangKeCUByHopDong(ID_HDTMP);
            BKCungUngDangKyCollection BKCUDangKyMoiCollection = new BKCungUngDangKyCollection();

            foreach (BKCungUngDangKy BKCU in BKCUDangKyCollection)
            {
                if (BKCU.LoadBySoBangKe())
                {
                    continue;
                }
                BKCU.LoadSanPhamCungUngCollection("MSSQL");
                BKCU.ID = 0;
                BKCU.TKCT_ID = 0;
                BKCU.TKMD_ID = 0;
                if (BKCU.isTKMD == 0)
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.SoToKhai = BKCU.SoToKhai;
                    TKMD.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKMD.MaHaiQuan = Properties.Settings.Default.MaHaiQuan;
                    //TKMD.SetDabaseMoi("MSSQL");
                    if (TKMD.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKMD.ID;
                    else
                        continue;
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.SoToKhai = BKCU.SoToKhai;
                    TKCT.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKCT.MaHaiQuanTiepNhan = Properties.Settings.Default.MaHaiQuan;
                    //TKCT.SetDabaseMoi("MSSQL");
                    if (TKCT.LoadBySoToKhaiKTX(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKCT.ID;
                    else
                        continue;
                }
                foreach (SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                {
                    spCU.LoadNPLCungUngCollection("MSSQL");
                    spCU.ID = 0;
                    foreach (NguyenPhuLieuCungUng NPLCu in spCU.NPLCungUngCollection)
                    {
                        NPLCu.ID = 0;
                    }
                }
                BKCUDangKyMoiCollection.Add(BKCU);
            }
            if (BKCUDangKyMoiCollection.Count > 0)
            {
                BKCungUngDangKy BKCUUpdate = new BKCungUngDangKy();
                BKCUUpdate.InsertUpdateDongBoDuLieuMOiKTX(BKCUDangKyMoiCollection, HD);
            }

        }
        private void DongBoBKCUMoiFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP, string databaseName)
        {
            BKCungUngDangKy bkTMP = new BKCungUngDangKy();
            bkTMP.SetDabaseMoi("MSSQL");
            BKCungUngDangKyCollection BKCUDangKyCollection = bkTMP.SelectBangKeCUByHopDong(ID_HDTMP);
            BKCungUngDangKyCollection BKCUDangKyMoiCollection = new BKCungUngDangKyCollection();

            foreach (BKCungUngDangKy BKCU in BKCUDangKyCollection)
            {
                if (BKCU.LoadBySoBangKe())
                {
                    continue;
                }
                BKCU.LoadSanPhamCungUngCollection("MSSQL");
                BKCU.ID = 0;
                BKCU.TKCT_ID = 0;
                BKCU.TKMD_ID = 0;
                if (BKCU.isTKMD == 0)
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.SoToKhai = BKCU.SoToKhai;
                    TKMD.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKMD.MaHaiQuan = Properties.Settings.Default.MaHaiQuan;
                    //TKMD.SetDabaseMoi("MSSQL");
                    if (TKMD.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKMD.ID;
                    else
                        continue;
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.SoToKhai = BKCU.SoToKhai;
                    TKCT.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKCT.MaHaiQuanTiepNhan = Properties.Settings.Default.MaHaiQuan;
                    //TKCT.SetDabaseMoi("MSSQL");
                    if (TKCT.LoadBySoToKhaiKTX(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKCT.ID;
                    else
                        continue;
                }
                foreach (SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                {
                    spCU.LoadNPLCungUngCollection("MSSQL");
                    spCU.ID = 0;
                    foreach (NguyenPhuLieuCungUng NPLCu in spCU.NPLCungUngCollection)
                    {
                        NPLCu.ID = 0;
                    }
                }
                BKCUDangKyMoiCollection.Add(BKCU);
            }
            if (BKCUDangKyMoiCollection.Count > 0)
            {
                BKCungUngDangKy BKCUUpdate = new BKCungUngDangKy();
                BKCUUpdate.InsertUpdateDongBoDuLieuMOiKTX(BKCUDangKyMoiCollection, HD, databaseName);
            }

        }

        public void DongBoDinhMucDangKyFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP, string dbName)
        {
            try
            {
                DinhMucDangKy dmdk = new DinhMucDangKy();
                dmdk.ID_HopDong = ID_HDTMP;
                dmdk.SetDabaseMoi("MSSQL");
                DinhMucDangKyCollection dmdkCollection = dmdk.SelectCollectionBy_ID_HopDong("MSSQL");
                foreach (DinhMucDangKy dmdkItem in dmdkCollection)
                {
                    dmdkItem.LoadCollection("MSSQL");
                    dmdkItem.ID = 0;
                    dmdkItem.ID_HopDong = HD.ID;
                    foreach (DinhMuc dm in dmdkItem.DMCollection)
                    {
                        dm.ID = 0;
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion DongBo Method

        void BindHopDong()
        {
            DataTable dt;
            dt = GetHopDong().Tables[0];
            cbHopDong.DataSource = dt;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "SoHopDong";
            try
            {
                cbHopDong.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Loi : " + ex.Message);

            }
        }

        private void BindDataMain()
        {
            //Luu index so hop dong dang chon
            int index = -1;
            string soHDSelect = cbHopDong.Text;

            GetHopDongAtDN(2);
            HD1.ID = Convert.ToInt64(cbHopDongDN.SelectedValue);

            string dbName = "MSSQLTARGET";

            DNNPLColection = HD1.GetNPLDS(dbName);
            dgListDNNPL.DataSource = DNNPLColection.Tables[0];// HQNPLColection;

            DNLoaiSPColection = HD1.GetLoaiSPDS(dbName);
            dgListDNLSP.DataSource = DNLoaiSPColection.Tables[0];

            HQSPColection = HD1.GetSPDS(dbName);
            dgListDNSP.DataSource = DNNPLColection.Tables[0];

            DNTBColection = HD1.GetTBDS(dbName);
            dgListDNTB.DataSource = DNTBColection.Tables[0];

            DNDMColection = HD1.GetDinhMucDS(dbName);
            try
            {
                dgListDNDM.DataSource = DNGHMDColection.Tables[0];
            }
            catch { }

            DNGPKDKColection = HD1.GetPKDS(dbName);
            dgListDNPK.DataSource = DNGPKDKColection.Tables[0];

            DNGTKMDColection = HD1.GetTKMDDS(dbName);
            dgListDNTKMD.DataSource = DNGTKMDColection.Tables[0];

            DNGTKCTColection = HD1.GetTKCTDS(dbName);
            dgListDNTKCT.DataSource = DNGTKCTColection.Tables[0];

            DNGBKCUColection = HD1.GetNPLCUDS(dbName);
            dgListDNBKCU.DataSource = DNGBKCUColection.Tables[0];

            for (int i = 0; i < cbHopDongDN.Items.Count; i++)
            {
                if (cbHopDongDN.Items[i].Text == soHDSelect)
                {
                    index = i;
                    break;
                }
            }

            cbHopDongDN.SelectedIndex = index;

            cbHopDong_SelectedIndexChanged(cbHopDong, EventArgs.Empty);

            cbHopDongDN_SelectedIndexChanged(cbHopDongDN, EventArgs.Empty);
        }
        private void BindDataTemp()
        {
            GetHopDongAtDN(1);
            HD1.ID = Convert.ToInt64(cbHopDong.SelectedValue);
            HQNPLColection = HD1.GetNPLDST();
            dgListHQNPL.DataSource = HQNPLColection.Tables[0];// HQNPLColection;

            HQLoaiSPColection = HD1.GetLoaiSPDST();
            dgListHQLSP.DataSource = HQLoaiSPColection.Tables[0];

            HQSPColection = HD1.GetSPDST();
            dgListHQSP.DataSource = HQNPLColection.Tables[0];

            HQTBColection = HD1.GetTBDST();
            dgListHQTB.DataSource = HQTBColection.Tables[0];

            HQDMColection = HD1.GetDinhMucDST();
            dgListHQDM.DataSource = HQGHMDColection.Tables[0];

            HQGPKDKColection = HD1.GetPKDST();
            dgListHQPK.DataSource = HQGPKDKColection.Tables[0];

            HQGTKMDColection = HD1.GetTKMDDST();
            dgListHQTKMD.DataSource = HQGTKMDColection.Tables[0];

            HQGTKCTColection = HD1.GetTKCTDST();
            dgListHQTKCT.DataSource = HQHCTColection.Tables[0];

            HQGBKCUColection = HD1.GetNPLCUDST();
            dgListHQBKCU.DataSource = HQGBKCUColection.Tables[0];

        }

        private void btnMoveSelected_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string st1 = cbHopDong.Text;
                string st2 = cbHopDongDN.Text;
                if (!st1.Equals(st2))
                {
                    MessageBox.Show("Chọn sai hợp đồng so sánh");
                    return;
                }
                DataTable dtNPLTempNPL = null;
                DataTable dtNPLMainNPL = null;

                DataTable dtNPLTempLSP = null;
                DataTable dtNPLMainLSP = null;

                DataTable dtNPLTempSP = null;
                DataTable dtNPLMainSP = null;

                DataTable dtNPLTempTB = null;
                DataTable dtNPLMainTB = null;

                DataTable dtNPLTempDM = null;
                DataTable dtNPLMainDM = null;

                DataTable dtNPLTempPK = null;
                DataTable dtNPLMainPK = null;

                DataTable dtNPLTempTKMD = null;
                DataTable dtNPLMainTKMD = null;

                DataTable dtNPLTempTKCT = null;
                DataTable dtNPLMainTKCT = null;

                DataTable dtNPLTempBKCU = null;
                DataTable dtNPLMainBKCU = null;

                try
                {
                    dtNPLTempNPL = HQNPLColection.Tables[0];
                    dtNPLMainNPL = DNNPLColection.Tables[0];

                    dtNPLTempLSP = HQLoaiSPColection.Tables[0];
                    dtNPLMainLSP = DNLoaiSPColection.Tables[0];

                    dtNPLTempSP = HQSPColection.Tables[0];
                    dtNPLMainSP = DNSPColection.Tables[0];

                    dtNPLTempTB = HQTBColection.Tables[0];
                    dtNPLMainTB = DNTBColection.Tables[0];

                    dtNPLTempDM = HQDMColection.Tables[0];
                    dtNPLMainDM = DNDMColection.Tables[0];

                    dtNPLTempPK = HQGPKDKColection.Tables[0];
                    dtNPLMainPK = DNGPKDKColection.Tables[0];

                    dtNPLTempTKMD = HQGTKMDColection.Tables[0];
                    dtNPLMainTKMD = DNGTKMDColection.Tables[0];

                    dtNPLTempTKCT = HQGTKCTColection.Tables[0];
                    dtNPLMainTKCT = DNGTKCTColection.Tables[0];

                    dtNPLTempBKCU = HQGBKCUColection.Tables[0];
                    dtNPLMainBKCU = DNGBKCUColection.Tables[0];



                }
                catch { }
                CompareDataNPL(dtNPLTempNPL, dtNPLMainNPL, HD1);
                CompareDataLSP(dtNPLTempLSP, dtNPLMainLSP, HD1);
                CompareDataSP(dtNPLTempSP, dtNPLMainSP, HD1);
                CompareDataTB(dtNPLTempTB, dtNPLMainTB, HD1);
                CompareDataDM(dtNPLTempDM, dtNPLMainDM, HD1);
                CompareDataPK(dtNPLTempPK, dtNPLMainPK, HD1);
                CompareDataTKMD(dtNPLTempTKMD, dtNPLMainTKMD, HD1);
                CompareDataTKCT(dtNPLTempTKCT, dtNPLMainTKCT, HD1);
                CompareDataBKCU(dtNPLTempBKCU, dtNPLMainBKCU, HD1);

                MessageBox.Show("Tiến trình so sánh 'Hợp đồng' đã hoàn thành.");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { Cursor = Cursors.Default; }
        }

        private void btnSearchHDDN_Click(object sender, EventArgs e)
        {
            GetHopDongAtDN(2);
        }

        private void cbHopDongDN_SelectedIndexChanged(object sender, EventArgs e)
        {
            LayDuLieuHopDongServerDich();
        }

        private void btnDongBoALL_Click(object sender, EventArgs e)
        {
            // this.Cursor = Cursors.WaitCursor;      
            // DongBoAll(1);
            TrangThaiDongBo tt = new TrangThaiDongBo();
            // HQHopDong = GetHopDong();
            tt.maDN = GlobalSettings.MaDoanhNghiep;
            tt.maHQ = GlobalSettings.MaHaiQuan;
            tt.matKhau = PassAccess;
            // tt.dsHopDongTTDB = HQHopDong;
            tt.ngayBatDau = GlobalSettings.NgayBatDau;
            tt.ngayKetThuc = GlobalSettings.NgayKetThuc;
            namdk = GlobalSettings.NgayBatDau.Year;
            tt.namdk = namdk;
            tt.ShowDialog();
            trangthaidongbo = 1;
            this.Cursor = Cursors.Default;
        }

        private void cbHopDong_SelectedIndexChanged(object sender, EventArgs e)
        {
            LayDuLieuHopDongServerNguon();
        }

        private void LayDuLieuHopDongServerNguon()
        {
            HD1.ID = Convert.ToInt64(cbHopDong.SelectedValue);

            string dbName = "MSSQL";

            dgListHQHopDong.DataSource = new Company.GC.BLL.KDT.GC.HopDong().LoadHopDongTQDTDataSet(HD1.ID, dbName).Tables[0];

            HQNPLColection = HD1.GetNPLDST_TQDT(dbName);

            try
            {
                dgListHQNPL.DataSource = HQNPLColection.Tables[0];// HQNPLColection;
            }
            catch { }
            HQLoaiSPColection = HD1.GetLoaiSPDST_TQDT(dbName);

            try
            {
                dgListHQLSP.DataSource = HQLoaiSPColection.Tables[0];
            }
            catch { }

            HQSPColection = HD1.GetSPDST_TQDT(dbName);

            try
            {
                dgListHQSP.DataSource = HQSPColection.Tables[0];
            }
            catch { }
            HQTBColection = HD1.GetTBDST_TQDT(dbName);

            try
            {
                dgListHQTB.DataSource = HQTBColection.Tables[0];
            }
            catch { }
            HQDMColection = HD1.GetDinhMucDST(dbName);
            try
            {
                dgListHQDM.DataSource = HQDMColection.Tables[0];
            }
            catch { }


            HQGPKDKColection = HD1.GetPKDST(dbName);
            try
            {
                dgListHQPK.DataSource = HQGPKDKColection.Tables[0];
            }
            catch { }


            HQGTKMDColection = HD1.GetTKMDDST(dbName, GlobalSettings.IsChiLayDuLieuDaDuyet);
            HQGTKMDColection.Tables[0].TableName = "TK";
            try
            {
                DataTable tableHMD = HD1.GetHMDHD(dbName).Tables[0];
                HQGTKMDColection.Tables.Add(tableHMD.Copy());

                //DataRelation rel = new DataRelation("TKMDRel", HQGTKMDColection.Tables[0].Columns["ID"], HQGTKMDColection.Tables[1].Columns["TKMD_ID"]);
                //HQGTKMDColection.Tables[1].TableName = "HMD";
                //HQGTKMDColection.Relations.Add(rel);
                dgListHQTKMD.DataSource = HQGTKMDColection.Tables[0];
            }
            catch (Exception ex) { }


            HQGTKCTColection = HD1.GetTKCTDST(dbName, GlobalSettings.IsChiLayDuLieuDaDuyet);
            try
            {
                dgListHQTKCT.DataSource = HQGTKCTColection.Tables[0];
            }
            catch { }


            HQGBKCUColection = HD1.GetNPLCUDST(dbName);
            try
            {
                dgListHQBKCU.DataSource = HQGBKCUColection.Tables[0];
            }
            catch { }

            btnDongBo1HD.Text = "Đồng bộ hợp đồng \"" + (cbHopDong.Text != "" ? cbHopDong.Text : "?") + "\"";

            //dgListHQNPL.DataSource = HD1.GetNPLDS();
            //dgListHQLSP.DataSource = HD1.GetLoaiSPDS();
            //dgListHQTB.DataSource = HD1.GetTBDS();
            //dgListHQDM.DataSource = HD1.GetDinhMucDS();
            //dgListHQPK.DataSource = HD1.GetPKDS();
            //dgListHQTKMD.DataSource = HD1.GetTKMDDS();
            //dgListHQTKCT.DataSource = HD1.GetTKCTDS();
        }

        private void LayDuLieuHopDongServerDich()
        {
            HD1.ID = Convert.ToInt64(cbHopDongDN.SelectedValue);

            string databseName = "MSSQLTARGET";

            dgListDNHopDong.DataSource = new Company.GC.BLL.KDT.GC.HopDong().LoadHopDongTQDTDataSet(HD1.ID, databseName).Tables[0];

            DNNPLColection = HD1.GetNPLDST_TQDT(databseName);
            dgListDNNPL.DataSource = DNNPLColection.Tables[0];

            DNLoaiSPColection = HD1.GetLoaiSPDST_TQDT(databseName);
            dgListDNLSP.DataSource = DNLoaiSPColection.Tables[0];

            DNSPColection = HD1.GetSPDST_TQDT(databseName);
            dgListDNSP.DataSource = DNSPColection.Tables[0];

            DNTBColection = HD1.GetTBDST_TQDT(databseName);
            dgListDNTB.DataSource = DNTBColection.Tables[0];

            DNDMColection = HD1.GetDinhMucDS(databseName);
            dgListDNDM.DataSource = DNDMColection.Tables[0];

            DNGPKDKColection = HD1.GetPKDS(databseName);
            dgListDNPK.DataSource = DNGPKDKColection.Tables[0];

            DNGTKMDColection = HD1.GetTKMDDS(databseName, GlobalSettings.IsChiLayDuLieuDaDuyet);
            dgListDNTKMD.DataSource = DNGTKMDColection.Tables[0];

            DNGTKCTColection = HD1.GetTKCTDS(databseName);
            dgListDNTKCT.DataSource = DNGTKCTColection.Tables[0];

            DNGBKCUColection = HD1.GetNPLCUDS(databseName);
            dgListDNBKCU.DataSource = DNGBKCUColection.Tables[0];
        }

        #region SoSanhDuLieu
        private void CompareDataNPL(DataTable dtTemp, DataTable dtMain, HopDong HD)
        {
            try
            {
                dtMain.Columns.Add("DNColor", typeof(string));
            }
            catch { }
            try
            {
                dtTemp.Columns.Add("HQColor", typeof(string));

            }
            catch { }
            int ok = 0;

            DataColumnCollection colleciton = dtTemp.Columns;
            //foreach (DataColumn dtCol in colleciton)
            //{
            //    if (dtCol.ColumnName == "HopDong_ID")
            //    {
            //        dtTemp.Columns.Remove("HopDong_ID");
            //    }
            //}

            foreach (DataRow row in dtTemp.Rows)
            {
                DataRow[] rowNPL = dtMain.Select("Ma='" + row["Ma"].ToString() + "'");
                if (rowNPL.Length > 0)
                {
                    DataRow rowMainNPL = rowNPL[0];
                    foreach (DataColumn col in colleciton)
                    {
                        if (!col.ColumnName.Equals("HopDong_ID") && !col.ColumnName.Equals("HQColor"))
                        {
                            if (!row[col.ColumnName].Equals(rowMainNPL[col.ColumnName]))
                            {
                                row["HQColor"] = "1";
                                rowMainNPL["DNColor"] = "1";
                            }
                        }
                    }
                }
                else
                {
                    row["HQColor"] = "2";
                }
            }

            foreach (DataRow row in dtMain.Rows)
            {
                DataRow[] rowNPL = dtTemp.Select("Ma='" + row["Ma"].ToString() + "'");
                if (rowNPL.Length == 0)
                    row["DNColor"] = "2";
            }
        }
        private void CompareDataLSP(DataTable dtTemp, DataTable dtMain, HopDong HD)
        {
            try
            {
                dtMain.Columns.Add("DNColor", typeof(string));
            }
            catch { }
            try
            {
                dtTemp.Columns.Add("HQColor", typeof(string));

            }
            catch { }

            DataColumnCollection colleciton = dtTemp.Columns;
            foreach (DataRow row in dtTemp.Rows)
            {
                //Loai SP
                DataRow[] rowLSP = dtMain.Select(" MaSanPham='" + row["MaSanPham"].ToString() + "'");
                if (rowLSP.Length > 0)
                {
                    DataRow rowMainLSP = rowLSP[0];
                    foreach (DataColumn col in colleciton)
                    {
                        if (!col.ColumnName.Equals("HopDong_ID"))
                        {
                            try
                            {
                                if (rowMainLSP.Table.Columns.Contains(col.ColumnName) && !row[col.ColumnName].Equals(rowMainLSP[col.ColumnName]))
                                {
                                    row["HQColor"] = "1";
                                    rowMainLSP["DNColor"] = "1";
                                }
                            }
                            catch { }
                        }
                    }
                }
                else
                {
                    row["HQColor"] = "2";
                }
            }
            foreach (DataRow row in dtMain.Rows)
            {
                //Loai SP
                DataRow[] rowLSP = dtTemp.Select(" MaSanPham='" + row["MaSanPham"].ToString() + "'");
                if (rowLSP.Length == 0)
                {
                    row["DNColor"] = "2";
                }
            }
        }
        private void CompareDataSP(DataTable dtTemp, DataTable dtMain, HopDong HD)
        {
            try
            {
                dtMain.Columns.Add("DNColor", typeof(string));
            }
            catch { }
            try
            {
                dtTemp.Columns.Add("HQColor", typeof(string));

            }
            catch { }


            DataColumnCollection colleciton = dtTemp.Columns;
            //SP
            foreach (DataRow row in dtTemp.Rows)
            {
                //Loai SP
                DataRow[] rowSP = dtMain.Select(" Ma='" + row["Ma"].ToString() + "'");
                if (rowSP.Length > 0)
                {
                    DataRow rowMainSP = rowSP[0];
                    foreach (DataColumn col in colleciton)
                    {
                        if (!col.ColumnName.Equals("HopDong_ID") && !col.ColumnName.Equals("STTHang"))
                        {
                            try
                            {
                                if (rowMainSP.Table.Columns.Contains(col.ColumnName) && !row[col.ColumnName].Equals(rowMainSP[col.ColumnName]))
                                {
                                    row["HQColor"] = "1";
                                    rowMainSP["DNColor"] = "1";
                                }
                            }
                            catch { }
                        }
                    }
                }
                else
                {
                    row["HQColor"] = "2";
                }
            }

            foreach (DataRow row in dtMain.Rows)
            {
                //Loai SP
                DataRow[] rowSP = dtTemp.Select(" Ma='" + row["Ma"].ToString() + "'");
                if (rowSP.Length == 0)
                {
                    row["DNColor"] = "2";
                }
            }
        }
        private void CompareDataTB(DataTable dtTemp, DataTable dtMain, HopDong HD)
        {
            try
            {
                dtMain.Columns.Add("DNColor", typeof(string));
            }
            catch { }
            try
            {
                dtTemp.Columns.Add("HQColor", typeof(string));

            }
            catch { }
            int ok = 0;

            DataColumnCollection colleciton = dtTemp.Columns;
            foreach (DataRow row in dtTemp.Rows)
            {
                //TB
                DataRow[] rowTB = dtMain.Select(" Ma='" + row["Ma"].ToString() + "'");
                if (rowTB.Length > 0)
                {
                    DataRow rowMainTB = rowTB[0];
                    foreach (DataColumn col in colleciton)
                    {
                        if (col.ColumnName.Equals("HopDong_ID"))
                        {
                            if (!row[col.ColumnName].Equals(rowMainTB[col.ColumnName]))
                            {
                                row["HQColor"] = "1";
                                rowMainTB["DNColor"] = "1";
                            }
                        }
                    }
                }
                else
                {
                    row["HQColor"] = "2";
                }
            }
            foreach (DataRow row in dtMain.Rows)
            {
                //TB
                DataRow[] rowTB = dtTemp.Select(" Ma='" + row["Ma"].ToString() + "'");
                if (rowTB.Length == 0)
                {
                    row["DNColor"] = "2";
                }

            }
        }
        private void CompareDataDM(DataTable dtTemp, DataTable dtMain, HopDong HD)
        {
            try
            {
                dtMain.Columns.Add("DNColor", typeof(string));
            }
            catch { }
            try
            {
                dtTemp.Columns.Add("HQColor", typeof(string));

            }
            catch { }
            int ok = 0;

            DataColumnCollection colleciton = dtTemp.Columns;
            try
            {
                //
                foreach (DataRow row in dtTemp.Rows)
                {
                    DataRow[] rowDM = dtMain.Select(" MaSanPham='" + row["MaSanPham"].ToString() + "' and MaNguyenPhuLieu='" + row["MaNguyenPhuLieu"].ToString() + "'");
                    if (rowDM.Length > 0)
                    {
                        DataRow rowMainDM = rowDM[0];
                        foreach (DataColumn col in colleciton)
                        {
                            if (!col.ColumnName.Equals("HopDong_ID"))
                            {
                                if (rowMainDM.Table.Columns.Contains(col.ColumnName) && !row[col.ColumnName].Equals(rowMainDM[col.ColumnName]))
                                {
                                    row["HQColor"] = "1";
                                    rowMainDM["DNColor"] = "1";
                                }
                            }
                        }
                    }
                    else
                    {
                        row["HQColor"] = "2";
                    }
                }
                foreach (DataRow row in dtMain.Rows)
                {
                    DataRow[] rowDM = dtTemp.Select(" MaSanPham='" + row["MaSanPham"].ToString() + "' and MaNguyenPhuLieu='" + row["MaNguyenPhuLieu"].ToString() + "'");
                    if (rowDM.Length == 0)
                    {
                        row["DNColor"] = "2";
                    }
                }
            }
            catch (Exception ex) { throw ex; }

        }

        private void CompareDataPK(DataTable dtTemp, DataTable dtMain, HopDong HD)
        {
            try
            {
                dtMain.Columns.Add("DNColor", typeof(string));
            }
            catch { }
            try
            {
                dtTemp.Columns.Add("HQColor", typeof(string));

            }
            catch { }
            int ok = 0;

            DataColumnCollection colleciton = dtTemp.Columns;
            foreach (DataRow row in dtTemp.Rows)
            {
                DataRow[] rowPK = dtMain.Select(" SoPhuKien='" + row["SoPhuKien"].ToString() + "'");
                if (rowPK.Length > 0)
                {
                    DataRow rowMainPK = rowPK[0];
                    foreach (DataColumn col in colleciton)
                    {
                        if (!col.ColumnName.Equals("HopDong_ID") && !col.ColumnName.Equals("ID"))
                        {
                            try
                            {
                                if (!row[col.ColumnName].Equals(rowMainPK[col.ColumnName]))
                                {
                                    row["HQColor"] = "1";
                                    rowMainPK["DNColor"] = "1";
                                }
                            }
                            catch { }
                        }
                    }
                }
                else
                {
                    row["HQColor"] = "2";
                }
            }

            foreach (DataRow row in dtMain.Rows)
            {
                DataRow[] rowPK = dtTemp.Select(" SoPhuKien='" + row["SoPhuKien"].ToString() + "'");
                if (rowPK.Length == 0)
                {
                    row["DNColor"] = "2";
                }
            }
        }
        private void CompareDataTKMD(DataTable dtTemp, DataTable dtMain, HopDong HD)
        {
            try
            {
                dtMain.Columns.Remove("DNColor");
            }
            catch { }
            try
            {
                dtTemp.Columns.Remove("HQColor");

            }
            catch { }

            try
            {
                dtMain.Columns.Add("DNColor", typeof(string));
            }
            catch { }
            try
            {
                dtTemp.Columns.Add("HQColor", typeof(string));

            }
            catch { }
            DataColumnCollection colleciton = dtTemp.Columns;
            foreach (DataRow row in dtTemp.Rows)
            {
                DataRow[] rowTK = dtMain.Select(" SoToKhai=" + Convert.ToInt32(row["SoToKhai"]) + " and NamDK=" + Convert.ToDateTime(row["NgayDangKy"]).Year + " and MaLoaiHinh='" + row["MaLoaiHinh"].ToString() + "'");
                if (rowTK.Length > 0)
                {
                    DataRow rowMainTK = rowTK[0];
                    foreach (DataColumn col in colleciton)
                    {
                        if (!col.ColumnName.Equals("IDHopDong") && !col.ColumnName.Equals("ID"))
                        {
                            try
                            {
                                if (!row[col.ColumnName].Equals(rowMainTK[col.ColumnName]))
                                {
                                    row["HQColor"] = "1";
                                    rowMainTK["DNColor"] = "1";
                                }
                            }
                            catch { }
                        }
                    }
                }
                else
                {
                    row["HQColor"] = "2";
                }
            }

            foreach (DataRow row in dtMain.Rows)
            {
                DataRow[] rowTK = dtTemp.Select(" SoToKhai=" + Convert.ToInt32(row["SoToKhai"]) + " and NamDK=" + Convert.ToDateTime(row["NgayDangKy"]).Year + " and MaLoaiHinh='" + row["MaLoaiHinh"].ToString() + "'");
                if (rowTK.Length == 0)
                {
                    row["DNColor"] = "2";
                }
            }

        }
        private void CompareDataTKCT(DataTable dtTemp, DataTable dtMain, HopDong HD)
        {
            try
            {
                dtMain.Columns.Add("DNColor", typeof(string));
            }
            catch { }
            try
            {
                dtTemp.Columns.Add("HQColor", typeof(string));

            }
            catch { }
            int ok = 0;

            DataColumnCollection colleciton = dtTemp.Columns;
            //TKCT
            foreach (DataRow row in dtTemp.Rows)
            {
                DataRow[] rowTKCT = dtMain.Select(" SoToKhai=" + Convert.ToInt32(row["SoToKhai"]) + " and NamDK=" + Convert.ToInt32(row["NamDK"]) + " and MaLoaiHinh='" + row["MaLoaiHinh"].ToString() + "'");
                if (rowTKCT.Length > 0)
                {
                    DataRow rowMainTKCT = rowTKCT[0];
                    foreach (DataColumn col in colleciton)
                    {
                        if (!col.ColumnName.Equals("IDHopDong") && !col.ColumnName.Equals("ID"))
                        {
                            if (!dtMain.Columns.Contains("HQColor"))
                            {
                                //Khong hieu
                                //if (!row[col.ColumnName].Equals(rowMainTKCT[col.ColumnName]))
                                //{
                                row["HQColor"] = "1";
                                rowMainTKCT["DNColor"] = "1";
                                //}
                            }
                        }
                    }
                }
                else
                {
                    row["HQColor"] = "2";
                }
            }

            //TKCT
            foreach (DataRow row in dtMain.Rows)
            {
                DataRow[] rowTKCT = dtTemp.Select(" SoToKhai=" + Convert.ToInt32(row["SoToKhai"]) + " and NamDK=" + Convert.ToInt32(row["NamDK"]) + " and MaLoaiHinh='" + row["MaLoaiHinh"].ToString() + "'");
                if (rowTKCT.Length == 0)
                {
                    row["DNColor"] = "2";
                }
            }


        }
        private void CompareDataBKCU(DataTable dtTemp, DataTable dtMain, HopDong HD)
        {
            try
            {
                dtMain.Columns.Add("DNColor", typeof(string));
            }
            catch { }
            try
            {
                dtTemp.Columns.Add("HQColor", typeof(string));

            }
            catch { }
            DataColumnCollection colleciton = dtTemp.Columns;
            foreach (DataRow row in dtTemp.Rows)
            {
                DataRow[] rowBK = dtTemp.Select(" SoBangKe=" + Convert.ToInt64(row["SoBangKe"].ToString()) + " and NamTN=" + Convert.ToInt32(row["NamTN"]));
                if (rowBK.Length > 0)
                {
                    DataRow rowMainBK = rowBK[0];
                    foreach (DataColumn col in colleciton)
                    {
                        if (!col.ColumnName.Equals("ID") && !col.ColumnName.Equals("TKMD_ID") && !col.ColumnName.Equals("TKCT_ID"))
                        {
                            if (!row[col.ColumnName].Equals(rowMainBK[col.ColumnName]))
                            {
                                row["HQColor"] = "1";
                                rowMainBK["DNColor"] = "1";
                            }
                        }
                    }
                }
                else
                {
                    row["HQColor"] = "2";
                }
            }

            foreach (DataRow row in dtMain.Rows)
            {
                DataRow[] rowBK = dtTemp.Select(" SoBangKe=" + Convert.ToInt64(row["SoBangKe"].ToString()) + " and (NamTN)=" + Convert.ToInt32(row["NamTN"]));
                if (rowBK.Length == 0)
                {
                    row["DNColor"] = "2";
                }
            }

        }

        #endregion SoSanhDuLieu

        public void DinhMucMoiOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM)
        {
            Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
            DataRow[] rowSelect = dsDM.Tables[0].Select("SoHopDong='" + HD.SoHopDong + "' and MaHaiQuan='" + HD.MaHaiQuan + "' and MaDoanhNghiep='" + HD.MaDoanhNghiep + "'");
            foreach (DataRow row in rowSelect)
            {
                Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                DM.DinhMucSuDung = Convert.ToDecimal(row["DinhMuc"]);
                DM.HopDong_ID = HD.ID;
                DM.MaNguyenPhuLieu = row["MaNPL"].ToString().Substring(1);
                DM.MaSanPham = row["MaSP"].ToString().Substring(1);
                DM.NPL_TuCungUng = Convert.ToDecimal(row["NPL_TuCungUng"]);
                DM.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);
                if (DM.CheckExitsDinhMucSanPham())
                {
                    continue;
                }
                dmCollection.Add(DM);
            }
            if (dmCollection.Count > 0)
                try
                {
                    new Company.GC.BLL.GC.DinhMuc().InsertUpdate(dmCollection);
                }
                catch
                {
                    ;
                }

        }
        public void ToKhaiMDMoiOfHD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet dsToKhai = HQGTKMDDS;// GetDataSetToKhai(HD);GetToKhaiMD(HD);//
            DataSet dsHang = HQGHMDDS; // GetHangMD(HD);
            foreach (DataRow row in dsToKhai.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.ToKhaiMauDich TKMD = new Company.GC.BLL.KDT.ToKhaiMauDich();
                TKMD.MaDoanhNghiep = GlobalSettings.MaDoanhNghiep;
                TKMD.MaHaiQuan = GlobalSettings.MaHaiQuan;
                TKMD.SoToKhai = Convert.ToInt32(row["SoTK"].ToString());
                TKMD.MaLoaiHinh = row["Ma_LH"].ToString();
                TKMD.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"].ToString());

                if (Company.GC.BLL.KDT.ToKhaiMauDich.GetIDToKhaiMauDichBySoToKhaiAndNamDangKy(GlobalSettings.MaHaiQuan, TKMD.MaLoaiHinh, TKMD.SoToKhai, TKMD.NgayDangKy.Year) > 0)
                {
                    continue;
                }

                TKMD.CuaKhau_ID = row["Ma_CK"].ToString().Trim();
                TKMD.DiaDiemXepHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["CangNN"].ToString());
                TKMD.DKGH_ID = row["Ma_GH"].ToString();
                TKMD.GiayTo = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["GiayTo"].ToString().Trim());
                TKMD.SoVanDon = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Van_Don"].ToString());
                if (row["NgayDen"].ToString() != "")
                    TKMD.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                if (row["Ngay_GP"].ToString() != "")
                    TKMD.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                if (row["Ngay_HHGP"].ToString() != "")
                    TKMD.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                if (row["Ngay_HHHD"].ToString() != "")
                    TKMD.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                if (row["Ngay_HDTM"].ToString() != "")
                    TKMD.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM"].ToString());
                if (row["Ngay_HD"].ToString() != "")
                    TKMD.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                TKMD.NgayTiepNhan = Convert.ToDateTime(row["Ngay_DK"].ToString());
                if (row["Ngay_VanDon"].ToString() != "")
                    TKMD.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                TKMD.NguyenTe_ID = row["Ma_NT"].ToString();
                if (TKMD.MaLoaiHinh.IndexOf("N") == 0)
                {
                    TKMD.NuocNK_ID = row["Nuoc_NK"].ToString();
                    TKMD.NuocXK_ID = row["Nuoc_XK"].ToString();
                }
                else
                {
                    TKMD.NuocNK_ID = row["Nuoc_XK"].ToString();
                    TKMD.NuocXK_ID = row["Nuoc_NK"].ToString();
                }
                if (row["Phi_BH"].ToString() != "")
                    TKMD.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                if (row["Phi_VC"].ToString() != "")
                    TKMD.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                TKMD.PTTT_ID = row["Ma_PTTT"].ToString();
                TKMD.PTVT_ID = row["Ma_PTVT"].ToString();
                if (row["So_Container"].ToString() != "")
                    TKMD.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                if (row["So_container40"].ToString() != "")
                    TKMD.SoContainer40 = Convert.ToDecimal(row["So_container40"].ToString());
                TKMD.SoGiayPhep = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["So_GP"].ToString());
                TKMD.SoHieuPTVT = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Ten_PTVT"].ToString());
                TKMD.SoHoaDonThuongMai = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["So_HDTM"].ToString());
                TKMD.SoHopDong = row["So_HD"].ToString();
                if (row["So_Kien"].ToString() != "")
                    TKMD.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                if (row["So_PLTK"].ToString() != "")
                    TKMD.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                TKMD.TenChuHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["TenCH"].ToString().Trim());
                TKMD.TrangThaiXuLy = 1;
                if (row["Tr_Luong"].ToString() != "")
                    TKMD.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                if (row["TyGia_VND"].ToString() != "")
                    TKMD.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                if (row["TyGia_USD"].ToString() != "")
                    TKMD.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                TKMD.TenDonViDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DV_DT"].ToString().Trim());

                DataRow[] rowHang = dsHang.Tables[0].Select("SoTK=" + TKMD.SoToKhai + " and Ma_LH='" + TKMD.MaLoaiHinh + "' and Ma_HQ='" + TKMD.MaHaiQuan + "' and NamDK=" + TKMD.NgayDangKy.Year);
                if (rowHang != null && rowHang.Length > 0)
                {
                    TKMD.HMDCollection = new Company.GC.BLL.KDT.HangMauDichCollection();
                    foreach (DataRow rowIndex in rowHang)
                    {
                        Company.GC.BLL.KDT.HangMauDich hang = new Company.GC.BLL.KDT.HangMauDich();
                        if (rowIndex["DGia_KB"].ToString() != "")
                            hang.DonGiaKB = Convert.ToDecimal(rowIndex["DGia_KB"].ToString());
                        if (rowIndex["DGia_TT"].ToString() != "")
                            hang.DonGiaTT = Convert.ToDecimal(rowIndex["DGia_TT"].ToString());
                        hang.DVT_ID = rowIndex["Ma_DVT"].ToString();
                        hang.MaHS = rowIndex["Ma_HangKB"].ToString().Trim();
                        hang.MaPhu = rowIndex["Ma_Phu"].ToString().Substring(1).Trim();
                        TKMD.LoaiHangHoa = rowIndex["Ma_Phu"].ToString().Substring(0, 1);
                        if (rowIndex["MienThue"].ToString() != "")
                            hang.MienThue = Convert.ToByte(rowIndex["MienThue"].ToString());
                        hang.NuocXX_ID = rowIndex["Nuoc_XX"].ToString();
                        if (rowIndex["Phu_Thu"].ToString() != "")
                            hang.PhuThu = Convert.ToDecimal(rowIndex["Phu_Thu"].ToString());
                        if (rowIndex["Luong"].ToString() != "")
                            hang.SoLuong = Convert.ToDecimal(rowIndex["Luong"].ToString());
                        hang.TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(rowIndex["Ten_Hang"].ToString().Trim());
                        if (rowIndex["Thue_VAT"].ToString() != "")
                            hang.ThueGTGT = Convert.ToDecimal(rowIndex["Thue_VAT"].ToString());
                        if (rowIndex["TS_VAT"].ToString() != "")
                            hang.ThueSuatGTGT = Convert.ToDecimal(rowIndex["TS_VAT"].ToString());
                        if (rowIndex["TS_TTDB"].ToString() != "")
                            hang.ThueSuatTTDB = Convert.ToDecimal(rowIndex["TS_TTDB"].ToString());
                        if (rowIndex["TS_XNK"].ToString() != "")
                            hang.ThueSuatXNK = Convert.ToDecimal(rowIndex["TS_XNK"].ToString());
                        if (rowIndex["Thue_TTDB"].ToString() != "")
                            hang.ThueTTDB = Convert.ToDecimal(rowIndex["Thue_TTDB"].ToString());
                        if (rowIndex["Thue_XNK"].ToString() != "")
                            hang.ThueXNK = Convert.ToDecimal(rowIndex["Thue_XNK"].ToString());
                        if (rowIndex["TriGia_KB"].ToString() != "")
                            hang.TriGiaKB = Convert.ToDecimal(rowIndex["TriGia_KB"].ToString());
                        if (rowIndex["TGKB_VND"].ToString() != "")
                            hang.TriGiaKB_VND = Convert.ToDecimal(rowIndex["TGKB_VND"].ToString());
                        if (rowIndex["TriGia_ThuKhac"].ToString() != "")
                            hang.TriGiaThuKhac = Convert.ToDecimal(rowIndex["TriGia_ThuKhac"].ToString());
                        if (rowIndex["TriGia_TT"].ToString() != "")
                            hang.TriGiaTT = Convert.ToDecimal(rowIndex["TriGia_TT"].ToString());
                        if (rowIndex["TyLe_ThuKhac"].ToString() != "")
                            hang.TyLeThuKhac = Convert.ToDecimal(rowIndex["TyLe_ThuKhac"].ToString());
                        TKMD.HMDCollection.Add(hang);
                    }
                    if (TKMD.HMDCollection.Count > 0)
                    {
                        try
                        {
                            TKMD.IDHopDong = HD.ID;
                            TKMD.InsertUpdateDongBoDuLieu();
                        }
                        catch
                        {
                            ;
                        }
                    }
                }
            }

        }
        public void LayThongTinTKCT(Company.GC.BLL.KDT.GC.HopDong HD)
        {

            DataSet dsToKhai = GetToKhaiCT(HD);
            DataSet dsHang = GetHangCT(HD);

            TKCT.InsertGhiDeALLToKhai(HD, dsToKhai, dsHang);
        }
        public void ToKhaiChuyenTiepMoiOfHD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet dsToKhai = HQGTKCTDS;// GetDataSetToKhaiChuyenTiep(HD);
            DataSet dsHang = HQHCTDS; //GetHangCT(HD);
            foreach (DataRow row in dsToKhai.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep();
                tkct.MaDoanhNghiep = GlobalSettings.MaHaiQuan;
                tkct.MaLoaiHinh = row["Ma_CT"].ToString().Trim();
                tkct.MaHaiQuanTiepNhan = GlobalSettings.MaHaiQuan;
                tkct.SoToKhai = Convert.ToInt64(row["So_CT"].ToString());
                tkct.NgayDangKy = Convert.ToDateTime(row["Ngay_CT"]);
                if (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.GetIDToKhaiChuyenTiepBySoToKhaiAndNamDangKy(GlobalSettings.MaHaiQuan, tkct.MaLoaiHinh, tkct.SoToKhai, tkct.NgayDangKy.Year) > 0)
                {
                    continue;
                }
                tkct.ChungTu = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["CTKT"].ToString());
                if (row["LePhiHQ"].ToString() != "")
                    tkct.LePhiHQ = Convert.ToDecimal(row["LePhiHQ"].ToString());
                string sohopdong = "";
                sohopdong = row["So_HD"].ToString();
                tkct.SoHopDongDV = row["So_HD"].ToString();
                tkct.SoHDKH = row["So_HDCh"].ToString();
                if (tkct.MaLoaiHinh.EndsWith("N"))
                {
                    tkct.DiaDiemXepHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["NguoiHQNhan"].ToString());

                }
                else
                {
                    tkct.DiaDiemXepHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["NguoiHQGiao"].ToString());
                }

                tkct.MaHaiQuanKH = row["Ma_HQKH"].ToString();
                tkct.MaKhachHang = row["Ma_DoiTac"].ToString();
                tkct.NgayHDDV = Convert.ToDateTime(row["Ngay_Ky"].ToString());
                tkct.NgayHDKH = Convert.ToDateTime(row["Ngay_KyCh"].ToString());
                tkct.NgayHetHanHDDV = HD.NgayHetHan;
                tkct.NguoiChiDinhDV = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["NguoiNhan"].ToString());
                tkct.NguoiChiDinhKH = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["NguoiGiao"].ToString());
                tkct.NgayTiepNhan = tkct.NgayDangKy;
                tkct.NguyenTe_ID = row["Ma_NT"].ToString();
                tkct.SoTiepNhan = 0;
                tkct.TrangThaiXuLy = 1;
                tkct.IDHopDong = HD.ID;
                if (row["TyGiaVND"].ToString() != "")
                    tkct.TyGiaVND = Convert.ToDecimal(row["TyGiaVND"].ToString());
                DataRow[] rowHang = dsHang.Tables[0].Select("So_CT=" + tkct.SoToKhai + " and Ma_CT='" + tkct.MaLoaiHinh + "' and Ma_HQ='" + tkct.MaHaiQuanTiepNhan + "' and Nam_CT=" + tkct.NgayDangKy.Year);
                if (rowHang != null && rowHang.Length > 0)
                {
                    tkct.HCTCollection = new Company.GC.BLL.KDT.GC.HangChuyenTiepCollection();
                    foreach (DataRow rowIndex in rowHang)
                    {
                        Company.GC.BLL.KDT.GC.HangChuyenTiep hangCT = new Company.GC.BLL.KDT.GC.HangChuyenTiep();
                        hangCT.DonGia = Convert.ToDecimal(rowIndex["DonGia"]);
                        hangCT.ID_DVT = rowIndex["Ma_DVT"].ToString();
                        hangCT.ID_NuocXX = rowIndex["NuocXX"].ToString();
                        hangCT.MaHang = rowIndex["P_Code"].ToString().Substring(1).Trim();
                        hangCT.MaHS = rowIndex["HS_Code"].ToString().Trim();
                        hangCT.SoLuong = Convert.ToDecimal(rowIndex["SoLuong"]);
                        hangCT.TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(rowIndex["TenHang"].ToString().Trim());
                        hangCT.TriGia = Convert.ToDecimal(rowIndex["TriGia"]);
                        tkct.HCTCollection.Add(hangCT);
                    }
                    try
                    {
                        tkct.InsertUpdateDongBoDuLieu();
                    }
                    catch
                    {
                        ;
                    }
                }
            }

        }
        public void PhuKienMoiOfHD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            // DataSet dsPhuKienDK = GetDataSetDistinctSoPhuKien(HD);
            DataSet dsPhuKienDK = HQGPKDKDS;// GetSoPhuKien(HD);// HQGPKDKColection;
            if (dsPhuKienDK.Tables[0].Rows.Count > 0)
            {
                DataSet dsLoaiPK = GetLoaiPhuKien(HD);
                DataSet dsHangPK = GetHangPhuKien(HD);
                NumberFormatInfo infoNumber = new CultureInfo("vi-VN").NumberFormat;
                foreach (DataRow row in dsPhuKienDK.Tables[0].Rows)
                {
                    string sohopdong = row["So_HD"].ToString().Trim();
                    Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();
                    pkdk.HopDong_ID = HD.ID;
                    pkdk.SoPhuKien = row["So_PK"].ToString().Trim();
                    if (Company.GC.BLL.KDT.GC.PhuKienDangKy.GetIDPhuKienByHopDongAndSoPhuKien(pkdk.SoPhuKien, pkdk.HopDong_ID) > 0)
                        continue;

                    // pkdk.GhiChu = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["GhiChu"].ToString().Trim());
                    pkdk.MaDoanhNghiep = GlobalSettings.MaHaiQuan;
                    pkdk.MaHaiQuan = GlobalSettings.MaDoanhNghiep;
                    pkdk.NgayPhuKien = Convert.ToDateTime(row["Ngay_PK"]);
                    pkdk.NgayTiepNhan = Convert.ToDateTime(row["Ngay_TN"]);
                    pkdk.NguoiDuyet = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Nguoi_Duyet"].ToString());
                    pkdk.SoTiepNhan = Convert.ToInt64(row["So_TN"].ToString());
                    pkdk.TrangThaiXuLy = 1;
                    pkdk.VanBanChoPhep = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["VBCP"].ToString().Trim());
                    pkdk.TrangThaiXuLy = 1;
                    string st = "DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and Ngay_Ky='" + HD.NgayKy.ToString("dd/MM/yyyy") + "' and So_PK='" + pkdk.SoPhuKien + "'";
                    DataRow[] rowLoaiPK = dsLoaiPK.Tables[0].Select(st);
                    foreach (DataRow rowLoai in rowLoaiPK)
                    {
                        Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                        LoaiPK.MaPhuKien = rowLoai["Ma_PK"].ToString();
                        LoaiPK.NoiDung = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(rowLoai["Noi_Dung"].ToString());
                        LoaiPK.ThongTinCu = rowLoai["Old_Info"].ToString();
                        if (LoaiPK.MaPhuKien != "H06")
                            LoaiPK.ThongTinMoi = rowLoai["New_Info"].ToString();
                        else
                        {
                            DateTimeFormatInfo infoDate = new CultureInfo("en-US").DateTimeFormat;
                            LoaiPK.ThongTinMoi = Convert.ToDateTime(rowLoai["New_Info"].ToString(), infoDate).ToString("dd/MM/yyyy");
                        }
                        LoaiPK.HPKCollection = new Company.GC.BLL.KDT.GC.HangPhuKienCollection();
                        DataRow[] rowHangLoaiPK = dsHangPK.Tables[0].Select("DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and Ngay_Ky='" + HD.NgayKy.ToString("dd/MM/yyyy") + "' and So_PK='" + pkdk.SoPhuKien + "' and Ma_PK='" + LoaiPK.MaPhuKien + "'");
                        foreach (DataRow rowHang in rowHangLoaiPK)
                        {
                            Company.GC.BLL.KDT.GC.HangPhuKien hangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                            if (rowHang["DonGia"].ToString() != "")
                                hangPK.DonGia = Convert.ToDouble(rowHang["DonGia"]);
                            hangPK.DVT_ID = rowHang["Ma_DVT"].ToString();
                            if (LoaiPK.MaPhuKien != "S15")
                                hangPK.MaHang = rowHang["P_Code"].ToString().Substring(1).Trim();
                            else
                                hangPK.MaHang = rowHang["P_Code"].ToString().Trim();
                            hangPK.MaHS = rowHang["HS_Code"].ToString().Trim();
                            hangPK.NguyenTe_ID = rowHang["NGTe"].ToString();
                            hangPK.NhomSP = rowHang["Ma_SP"].ToString();
                            hangPK.NuocXX_ID = rowHang["Xuat_xu"].ToString();
                            hangPK.ThongTinCu = rowHang["Old_Info"].ToString();
                            try
                            {
                                if (rowHang["So_Luong"].ToString() != "")
                                {
                                    if (LoaiPK.MaPhuKien.Trim() == "N05")
                                        hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                    else if (LoaiPK.MaPhuKien.Trim() == "N11")
                                        hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                    else if (LoaiPK.MaPhuKien.Trim() == "T01")
                                        hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                    else
                                        hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                }
                            }
                            catch { }
                            hangPK.TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(rowHang["Ten_SP"].ToString().Trim());
                            hangPK.TinhTrang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(rowHang["TinhTrang"].ToString().Trim());
                            if (rowHang["TriGia"].ToString() != "")
                                hangPK.TriGia = Convert.ToDouble(rowHang["TriGia"]);
                            LoaiPK.HPKCollection.Add(hangPK);
                        }
                        pkdk.PKCollection.Add(LoaiPK);
                    }
                    try
                    {

                        pkdk.InsertUpDatePhuKienDongBoDuLieu();
                    }
                    catch { }
                }
            }

        }
        public void BangKeCungUngMoiOfHD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet dsBangKe = HQGBKCUDS;// GetDataSetBangKeCungUng(HD);
            DataSet dsSanPhamBangKe = GetSanPhamBangKeCU(HD);
            DataSet dsNPLBangKe = GetNPLDSBangKeCU(HD);
            foreach (DataRow rowBangKe in dsBangKe.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.BKCungUngDangKy BKCungUng = new Company.GC.BLL.KDT.GC.BKCungUngDangKy();
                BKCungUng.MaDoanhNghiep = GlobalSettings.MaHaiQuan;
                BKCungUng.MaHaiQuan = GlobalSettings.MaDoanhNghiep;
                BKCungUng.NgayTiepNhan = Convert.ToDateTime(rowBangKe["Ngay_TN"]);
                BKCungUng.SoBangKe = Convert.ToInt64(rowBangKe["So_TN"]);
                BKCungUng.SoTiepNhan = 0;
                BKCungUng.TrangThaiXuLy = 1;
                string MaLoaiHinh = rowBangKe["Ma_LH"].ToString();
                int NamDK = Convert.ToInt32(rowBangKe["NamDK"].ToString());
                long SoToKhai = Convert.ToInt64(rowBangKe["SoTK"].ToString());

                if (Company.GC.BLL.KDT.GC.BKCungUngDangKy.GetIDBangKeBySoBangKeAndNamDangKy(GlobalSettings.MaHaiQuan, BKCungUng.SoBangKe, NamDK) > 0)
                    continue;

                if (MaLoaiHinh.StartsWith("XGC"))
                {
                    BKCungUng.TKMD_ID = Company.GC.BLL.KDT.ToKhaiMauDich.GetIDToKhaiMauDichBySoToKhaiAndNamDangKy(GlobalSettings.MaHaiQuan, MaLoaiHinh, SoToKhai, NamDK);
                }
                else
                {
                    BKCungUng.TKCT_ID = Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.GetIDToKhaiChuyenTiepBySoToKhaiAndNamDangKy(GlobalSettings.MaHaiQuan, MaLoaiHinh, SoToKhai, NamDK);
                }
                if (BKCungUng.TKMD_ID > 0 || BKCungUng.TKCT_ID > 0)
                {
                    BKCungUng.SanPhamCungUngCollection = new Company.GC.BLL.KDT.GC.SanPhanCungUngCollection();
                    DataRow[] rowSPCungUngCollection = dsSanPhamBangKe.Tables[0].Select("DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and Ngay_Ky='" + HD.NgayKy.ToString("dd/MM/yyyy") + "' and So_TN=" + BKCungUng.SoBangKe + " and NamTN=" + NamDK);
                    foreach (DataRow SPCungUng in rowSPCungUngCollection)
                    {
                        Company.GC.BLL.KDT.GC.SanPhanCungUng spCungUngEntity = new Company.GC.BLL.KDT.GC.SanPhanCungUng();
                        string maSP = SPCungUng["SPP_Code"].ToString();
                        spCungUngEntity.LuongCUSanPham = Convert.ToDecimal(SPCungUng["SoLuongSP"]);
                        spCungUngEntity.MaSanPham = maSP.Substring(1).Trim();
                        spCungUngEntity.NPLCungUngCollection = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUngCollection();
                        DataRow[] rowNPLOfSanPhamCollection = dsNPLBangKe.Tables[0].Select("DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and Ngay_Ky='" + HD.NgayKy.ToString("dd/MM/yyyy") + "' and So_TN=" + BKCungUng.SoBangKe + " and NamTN=" + NamDK + " and SPP_Code='" + maSP + "'");
                        foreach (DataRow rowNPL in rowNPLOfSanPhamCollection)
                        {
                            Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng nplCungUng = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng();
                            nplCungUng.DinhMucCungUng = Convert.ToDouble(rowNPL["DMGC"]);
                            nplCungUng.DonGia = Convert.ToDouble(rowNPL["DonGia"]);
                            nplCungUng.HinhThuCungUng = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(rowNPL["HinhThucCU"].ToString());
                            nplCungUng.LuongCung = Convert.ToDecimal(rowNPL["LuongCU"]);
                            nplCungUng.MaNguyenPhuLieu = rowNPL["NPLP_Code"].ToString().Substring(1).Trim();
                            nplCungUng.TriGia = Convert.ToDouble(rowNPL["TriGia"]);
                            nplCungUng.TyLeHH = Convert.ToDouble(rowNPL["TLHH"]);
                            spCungUngEntity.NPLCungUngCollection.Add(nplCungUng);
                        }
                        BKCungUng.SanPhamCungUngCollection.Add(spCungUngEntity);
                    }
                }
                try
                {
                    BKCungUng.InsertUpdateDongBoDuLieu();
                }
                catch { }
            }
        }

        private void btnDongBoDuLieu_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(" Để tránh mất dữ liệu trong một số trường hợp đặc biệt, người dùng nên backup dữ liệu trước khi thực hiện chức năng đồng bộ dữ liệu để có dữ liệu đối chiếu khi cần. Chọn Yes sẽ đồng bộ dữ liệu mới của các hợp đồng từ hệ thống Hải quan về csdl doanh nghiệp.\r\n\n-Lưu ý: Chức năng này cho phép \n\tTHÊM MỚI 'Hợp đồng' chưa có bên CSDL đích.\n\tHoặc CẬP NHẬT 'Hợp đồng' (nếu có) bên CSDL đích.\r\n\nBạn có muốn tiếp tục không ?", " WARNNING ", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DongBoDuLieuMoiOfHD();
            }
        }

        private void btnHopDongNew_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(" Để tránh mất dữ liệu trong một số trường hợp đặc biệt, người dùng nên backup dữ liệu trước khi thực hiện chức năng đồng bộ dữ liệu để có dữ liệu đối chiếu khi cần. Chọn Yes sẽ đồng bộ dữ liệu của hợp đồng mới từ hệ thống Hải quan về csdl doanh nghiệp.\r\n\n-Lưu ý: Chức năng này chỉ đồng bộ 'Hợp đồng' MỚI chưa có bên CSDL đích.\r\n\nBạn có muốn tiếp tục không ?", " WARNNING ", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.DongBoHopDongMoi();
            }
        }
        private void DisplayMessages(string st1)
        {
            // MessageBox.Show(st1,"Cảnh báo",MessageBoxButtons.YesNo,MessageBoxIcon.Warning) ;
            MessageBox.Show(st1, "Thông báo");
        }

        private void btnLayDuLieuNguon_Click(object sender, EventArgs e)
        {
            GetHopDongAtDN(1);

            btnDongBo1HD.Text = "Đồng bộ hợp đồng \"" + (cbHopDong.Text != "" ? cbHopDong.Text : "?") + "\"";

        }

        #region DongBoDuLieu
        private void DongBoDuLieuMoiOfHD()
        {
            try
            {
                foreach (DataRow row in dtHQHopDong.Rows)
                {
                    long ID_HDTMP = Convert.ToInt64(row["ID"]);
                    Company.GC.BLL.KDT.GC.HopDong HDG = new Company.GC.BLL.KDT.GC.HopDong();
                    HDG.LoadDataHopDong(ID_HDTMP, "MSSQL");

                    /*
                    HDG.DiaChiDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DiaChiDoiTac"].ToString()).Trim();
                    HDG.DonViDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DonViDoiTac"].ToString()).Trim();
                    HDG.MaDoanhNghiep = GlobalSettings.MaDoanhNghiep;
                    HDG.MaHaiQuan = GlobalSettings.MaHaiQuan;
                    if (row["NgayGiaHan"].ToString() != "")
                        HDG.NgayGiaHan = Convert.ToDateTime(row["NgayGiaHan"]);
                    HDG.NgayHetHan = Convert.ToDateTime(row["NgayHetHan"]);
                    HDG.NgayKy = Convert.ToDateTime(row["NgayKy"]);
                    HDG.NguyenTe_ID = row["NguyenTe_ID"].ToString();
                    HDG.NuocThue_ID = row["NuocThue_ID"].ToString();
                    HDG.SoHopDong = row["SoHopDong"].ToString().Trim();
                    HDG.SoTiepNhan = 0;
                    HDG.TrangThaiXuLy = 1;
                    */

                    try
                    {
                        string databseName = "MSSQLTARGET";
                        HDG.SetDabaseMoi(databseName);

                        long ID = HDG.GetIDHopDongExit(HDG.SoHopDong, HDG.MaHaiQuan, HDG.MaDoanhNghiep, HDG.NgayKy);

                        if (ID == 0)
                        {
                            if (GlobalSettings.PhienBan == "KTX")
                            {
                                HDG.InsertUpdateHopDongDongBoDuLieuKTX(databseName);
                            }
                            else if (GlobalSettings.PhienBan == "TQDT")
                            {
                                HDG.InsertUpdateHopDongDongBoDuLieuTQDT(databseName, chkV3.Checked);
                            }
                        }
                        else
                        {
                            HDG.ID = ID;
                        }

                        if (GlobalSettings.PhienBan == "KTX")
                        {
                            DongBoDinhMucMoiFromDBTamToDBOfHDKTX(HDG, DM.GetDMAtTMP(ID_HDTMP), databseName);
                            DongBoPhuKienMoiFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                            DongBoTKMDMoiFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                            DongBoTKChuyenTiepMoiFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                            DongBoBKCUMoiFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                        }
                        else if (GlobalSettings.PhienBan == "TQDT")
                        {
                            //DongBoDinhMucMoiFromDBTamToDBOfHD(HDG, DM.GetDMAtTMP(ID_HDTMP));
                            //DongBoPhuKienMoiFromDBTamToDB(HDG, ID_HDTMP);
                            //DongBoTKMDMoiFromDBTamToDB(HDG, ID_HDTMP);
                            //DongBoTKChuyenTiepMoiFromDBTamToDB(HDG, ID_HDTMP);
                            //DongBoBKCUMoiFromDBTamToDB(HDG, ID_HDTMP);
                        }
                        //HDG.XuLyHopDong();
                    }
                    catch (Exception exxx)
                    {
                        DisplayMessages(" Lỗi : " + exxx.Message.ToString());
                        BindDataMain();
                        //return;
                    }
                }
                MessageBox.Show(" Hoàn thành việc 'Đồng bộ dữ liệu mới'");
                BindDataMain();
            }
            catch (Exception exe) { DisplayMessages("Lỗi : " + exe.Message.ToString()); }
        }

        private void DongBoHopDongMoi()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                foreach (DataRow row in dtHQHopDong.Rows)
                {
                    long ID_HDTMP = Convert.ToInt64(row["ID"]);
                    Company.GC.BLL.KDT.GC.HopDong HDG = new Company.GC.BLL.KDT.GC.HopDong();
                    HDG.LoadDataHopDong(ID_HDTMP, "MSSQL");

                    /*
                    HDG.DiaChiDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DiaChiDoiTac"].ToString()).Trim();
                    HDG.DonViDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DonViDoiTac"].ToString()).Trim();
                    HDG.MaDoanhNghiep = GlobalSettings.MaDoanhNghiep;
                    HDG.MaHaiQuan = GlobalSettings.MaHaiQuan;
                    if (row["NgayGiaHan"].ToString() != "")
                        HDG.NgayGiaHan = Convert.ToDateTime(row["NgayGiaHan"]);
                    HDG.NgayHetHan = Convert.ToDateTime(row["NgayHetHan"]);
                    HDG.NgayKy = Convert.ToDateTime(row["NgayKy"]);
                    HDG.NguyenTe_ID = row["NguyenTe_ID"].ToString();
                    HDG.NuocThue_ID = row["NuocThue_ID"].ToString();
                    HDG.SoHopDong = row["SoHopDong"].ToString().Trim();
                    HDG.SoTiepNhan = 0;
                    HDG.TrangThaiXuLy = 1;
                    DongBoLoaiSPFromDBTamToDB(HDG, LoaiSP.GetLoaiSPAtTMP(ID_HDTMP));
                    //Nguyen phu lieu :
                    DongBoNPLFromDBTamToDB(HDG, NPL.GetNguyenPhuLieuAtTMP(ID_HDTMP));
                    //San pham :
                    DongBoSPFromDBTamToDB(HDG, SP.GetSanPhamAtTMP(ID_HDTMP));
                    //Thiet bi :
                    DongBoTBFromDBTamToDB(HDG, TB.GetTBAtTMP(ID_HDTMP));
                    */
                    try
                    {
                        string databseName = "MSSQLTARGET";
                        HDG.SetDabaseMoi(databseName);

                        long ID = HDG.GetIDHopDongExit(HDG.SoHopDong, HDG.MaHaiQuan, HDG.MaDoanhNghiep, HDG.NgayKy, databseName);
                        HDG.ID = ID;

                        if (GlobalSettings.PhienBan == "KTX")
                        {
                            HDG.InsertUpdateHopDongDongBoDuLieuKTX(databseName);

                            DinhMucOfHDKTX(HDG, DM.GetDMAtTMP(ID_HDTMP), 2, databseName);
                            DongBoPhuKienFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                            DongBoTKMDFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                            DongBoTKChuyenTiepFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                            DongBoBKCUFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                        }
                        else if (GlobalSettings.PhienBan == "TQDT")
                        {
                            HDG.InsertUpdateHopDongDongBoDuLieuTQDT(databseName, chkV3.Checked);

                            //DinhMucOfHD(HDG, DM.GetDMAtTMP(ID_HDTMP), 2);
                            //DongBoPhuKienFromDBTamToDB(HDG, ID_HDTMP);
                            //DongBoTKMDFromDBTamToDB(HDG, ID_HDTMP);
                            //DongBoTKChuyenTiepFromDBTamToDB(HDG, ID_HDTMP);
                            //DongBoBKCUFromDBTamToDB(HDG, ID_HDTMP);
                        }
                    }
                    catch (Exception ex1) { MessageBox.Show(" Lỗi : " + ex1.Message); }
                }
                MessageBox.Show(" Hoàn thành việc 'Đồng bộ hợp đồng mới'");
            }
            catch (Exception exx) { MessageBox.Show(" Lỗi : " + exx.Message.ToString()); }
            finally { Cursor = Cursors.Default; }
        }

        private void DongBoAll(int dk)
        {

            //TrangThaiDongBo tt = new TrangThaiDongBo();
            //HQHopDong = GetHopDong();
            //tt.maDN = GlobalSettings.MaDoanhNghiep;
            //tt.maHQ = GlobalSettings.MaHaiQuan;
            //tt.dsHopDongTTDB = HQHopDong ;
            //tt.ShowDialog();

            ////xoa toan bo hop dong cu :
            //HopDong hdong = new HopDong();
            //hdong.SetDabaseMoi("MSSQL");
            //hdong.DeleteAllHopDong(); //Luu y

            //// Xoa toan bo bang ke cung ung :
            //// Set lai default chuoi ket noi trong file config :

            //// Dong bo vao database tam :
            //try
            //{
            //    //  DongBo.TrangThaiDongBo tt = new TrangThaiDongBo();
            //    // tt.ShowDialog();
            //    DataSet dsHopDong = GetHopDong();
            //    // DongBo.EndTrangThaiDongBo ett = new EndTrangThaiDongBo();
            //    //  ett.ShowDialog();
            //    foreach (DataRow row in dsHopDong.Tables[0].Rows)
            //    {
            //        Company.GC.BLL.KDT.GC.HopDong HDG = new Company.GC.BLL.KDT.GC.HopDong();
            //        HDG.DiaChiDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DiaChi_DT"].ToString()).Trim();
            //        HDG.DonViDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DVDT"].ToString()).Trim();
            //        HDG.MaDoanhNghiep = GlobalSettings.MaDoanhNghiep;
            //        HDG.MaHaiQuan = GlobalSettings.MaHaiQuan;
            //        if (row["NgayGiaHan"].ToString() != "")
            //            HDG.NgayGiaHan = Convert.ToDateTime(row["NgayGiaHan"]);
            //        HDG.NgayHetHan = Convert.ToDateTime(row["NgayHetHan"]);
            //        HDG.NgayKy = Convert.ToDateTime(row["NgayKy"]);
            //        HDG.NguyenTe_ID = row["NguyenTe_ID"].ToString();
            //        HDG.NuocThue_ID = row["NuocThue_ID"].ToString();
            //        HDG.SoHopDong = row["SoHopDong"].ToString().Trim();
            //        HDG.SoTiepNhan = 0;
            //        HDG.TrangThaiXuLy = 1;
            //       LoaiSPOfHD(HDG, GetLoaiSanPham(HDG));
            //        //Nguyen phu lieu :
            //        NguyenPhuLieuOfHD(HDG, GetNguyenPhuLieu(HDG));
            //        //San pham :
            //        SanPhamOfHD(HDG, GetSanPham(HDG));
            //        //Thiet bi :
            //        ThietBiOfHD(HDG, GetThietBi(HDG));

            //        try
            //        {
            //            if (dk == 1)
            //            {
            //                HDG.SetDabaseMoi("MSSQL");
            //            }
            //            HDG.InsertUpdateHopDongDongBoDuLieu();
            //            DM.InsertGhiDeDuLieu(HDG, GetDinhMucDS(HDG),"");
            //            LayThongTinPhuKien(HDG);
            //            LayThongTinTKMD(HDG);
            //            LayThongTinTKCT(HDG);
            //            LayThongTinBKCU(HDG);

            //        }
            //        catch (Exception ex1) { this.Cursor = Cursors.Default; MessageBox.Show("Lỗi : " + ex1.Message); }

            //    }

            //    // Lay ra tu database temp :                
            //    if (dk == 1)
            //    {
            //        GetHopDongAtDN(1);                   
            //    }
            //    else
            //    {
            //        GetHopDongAtDN(2);                   
            //    }

            //}
            //catch (Exception ex) { this.Cursor = Cursors.Default; MessageBox.Show(ex.Message); }

        }
        #endregion DongBoDuLieu

        #region ChuyenDuLieuMoi

        public void DongBoTKMDMoiFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP)
        {
            ToKhaiMauDich TKMDDBTamp = new ToKhaiMauDich();
            TKMDDBTamp.IDHopDong = id_HDTMP;
            TKMDDBTamp.SetDabaseMoi("MSSQL");
            ToKhaiMauDichCollection TKMDCollection = TKMDDBTamp.SelectCollectionBy_IDHopDong();
            ToKhaiMauDichCollection TKMDCollectionMoi = new ToKhaiMauDichCollection();

            foreach (ToKhaiMauDich TKitem in TKMDCollection)
            {
                if (TKitem.LoadBySoToKhai(TKitem.NgayDangKy.Year))
                    continue;
                TKitem.LoadHMDCollection("MSSQL");
                TKitem.ID = 0;
                TKitem.TrangThaiPhanBo = 0;
                TKitem.IDHopDong = HD.ID;
                foreach (HangMauDich HMD in TKitem.HMDCollection)
                    HMD.ID = 0;
                TKMDCollectionMoi.Add(TKitem);
            }
            if (TKMDCollectionMoi.Count > 0)
            {
                try
                {
                    ToKhaiMauDich tk = new ToKhaiMauDich();
                    tk.InsertUpdateDongBoDuLieuMoi(TKMDCollection, HD);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi khi đồng bộ dữ liệu TKMD : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
                }
            }
        }

        public void DongBoTKMDMoiFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP)
        {
            ToKhaiMauDich TKMDDBTamp = new ToKhaiMauDich();
            TKMDDBTamp.IDHopDong = id_HDTMP;
            TKMDDBTamp.SetDabaseMoi("MSSQL");
            ToKhaiMauDichCollection TKMDCollection = TKMDDBTamp.SelectCollectionBy_IDHopDong_KTX();
            ToKhaiMauDichCollection TKMDCollectionMoi = new ToKhaiMauDichCollection();

            foreach (ToKhaiMauDich TKitem in TKMDCollection)
            {
                if (TKitem.LoadBySoToKhai(TKitem.NgayDangKy.Year))
                    continue;
                TKitem.LoadHMDCollection("MSSQL");
                TKitem.ID = 0;
                TKitem.TrangThaiPhanBo = 0;
                TKitem.IDHopDong = HD.ID;
                foreach (HangMauDich HMD in TKitem.HMDCollection)
                    HMD.ID = 0;
                TKMDCollectionMoi.Add(TKitem);
            }
            if (TKMDCollectionMoi.Count > 0)
            {
                try
                {
                    ToKhaiMauDich tk = new ToKhaiMauDich();
                    tk.InsertUpdateDongBoDuLieuMoiKTX(TKMDCollection, HD);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi khi đồng bộ dữ liệu TKMD : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
                }
            }
        }

        public void DongBoTKMDMoiFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP, string databaseName)
        {
            ToKhaiMauDich TKMDDBTamp = new ToKhaiMauDich();
            TKMDDBTamp.IDHopDong = id_HDTMP;
            TKMDDBTamp.SetDabaseMoi("MSSQL");
            ToKhaiMauDichCollection TKMDCollection = TKMDDBTamp.SelectCollectionBy_IDHopDong_KTX();
            ToKhaiMauDichCollection TKMDCollectionMoi = new ToKhaiMauDichCollection();

            foreach (ToKhaiMauDich TKitem in TKMDCollection)
            {
                if (TKitem.LoadBySoToKhai(TKitem.NgayDangKy.Year))
                    continue;
                TKitem.LoadHMDCollection("MSSQL");
                TKitem.ID = 0;
                TKitem.TrangThaiPhanBo = 0;
                TKitem.IDHopDong = HD.ID;
                foreach (HangMauDich HMD in TKitem.HMDCollection)
                    HMD.ID = 0;
                TKMDCollectionMoi.Add(TKitem);
            }
            if (TKMDCollectionMoi.Count > 0)
            {
                try
                {
                    ToKhaiMauDich tk = new ToKhaiMauDich();
                    tk.InsertUpdateDongBoDuLieuMoiKTX(TKMDCollection, HD, databaseName);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi khi đồng bộ dữ liệu TKMD : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
                }
            }
        }

        public void DongBoTKChuyenTiepMoiFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP)
        {
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollectionMoi = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
            ToKhaiChuyenTiep TKCTDBTam = new ToKhaiChuyenTiep();
            TKCTDBTam.IDHopDong = id_HDTMP;
            TKCTDBTam.SetDabaseMoi("MSSQL");
            TKCTCollection = TKCTDBTam.SelectCollectionBy_IDHopDong();
            foreach (ToKhaiChuyenTiep TKCTItem in TKCTCollection)
            {
                if (TKCTItem.LoadBySoToKhai())
                    continue;
                //TKCTItem.LoadHCTCollection("MSSQL");
                TKCTItem.ID = 0;
                TKCTItem.IDHopDong = HD.ID;
                foreach (HangChuyenTiep HCT in TKCTItem.HCTCollection)
                    HCT.ID = 0;
                TKCTCollectionMoi.Add(TKCTItem);
            }
            try
            {
                ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                tkct.InsertUpdateDongBoDuLieuMoi(TKCTCollectionMoi, HD);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi khi đồng bộ dữ liệu TKCT : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
            }
        }

        public void DongBoTKChuyenTiepMoiFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP)
        {
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollectionMoi = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
            ToKhaiChuyenTiep TKCTDBTam = new ToKhaiChuyenTiep();
            TKCTDBTam.IDHopDong = id_HDTMP;
            TKCTDBTam.SetDabaseMoi("MSSQL");
            TKCTCollection = TKCTDBTam.SelectCollectionBy_IDHopDong_KTX();
            foreach (ToKhaiChuyenTiep TKCTItem in TKCTCollection)
            {
                if (TKCTItem.LoadBySoToKhaiKTX())
                    continue;
                //TKCTItem.LoadHCTCollection("MSSQL");
                TKCTItem.ID = 0;
                TKCTItem.IDHopDong = HD.ID;
                foreach (HangChuyenTiep HCT in TKCTItem.HCTCollection)
                    HCT.ID = 0;
                TKCTCollectionMoi.Add(TKCTItem);
            }
            try
            {
                ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                tkct.InsertUpdateDongBoDuLieuMoiKTX(TKCTCollectionMoi, HD, "MSSQLTARGET");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi khi đồng bộ dữ liệu TKCT : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
            }
        }

        public void DongBoTKChuyenTiepMoiFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP, string databaseName)
        {
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollectionMoi = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
            ToKhaiChuyenTiep TKCTDBTam = new ToKhaiChuyenTiep();
            TKCTDBTam.IDHopDong = id_HDTMP;
            TKCTDBTam.SetDabaseMoi("MSSQL");
            TKCTCollection = TKCTDBTam.SelectCollectionBy_IDHopDong_KTX();
            foreach (ToKhaiChuyenTiep TKCTItem in TKCTCollection)
            {
                if (TKCTItem.LoadBySoToKhaiKTX())
                    continue;
                //TKCTItem.LoadHCTCollection("MSSQL");
                TKCTItem.ID = 0;
                TKCTItem.IDHopDong = HD.ID;
                foreach (HangChuyenTiep HCT in TKCTItem.HCTCollection)
                    HCT.ID = 0;
                TKCTCollectionMoi.Add(TKCTItem);
            }
            try
            {
                ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                tkct.InsertUpdateDongBoDuLieuMoiKTX(TKCTCollectionMoi, HD, databaseName);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi khi đồng bộ dữ liệu TKCT : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
            }
        }

        public void DongBoPhuKienMoiFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP)
        {
            PhuKienDangKy pkdk = new PhuKienDangKy();
            pkdk.HopDong_ID = ID_HDTMP;
            pkdk.SetDabaseMoi("MSSQL");
            PhuKienDangKyCollection pkdkCollection = pkdk.SelectCollectionBy_HopDong_ID();
            PhuKienDangKyCollection pkdkCollectionMoi = new PhuKienDangKyCollection();
            foreach (PhuKienDangKy pkdkItem in pkdkCollection)
            {
                if (pkdkItem.LoadBySoPhuKien())
                    continue;
                pkdkItem.LoadCollection("MSSQL");
                pkdkItem.ID = 0;
                pkdkItem.HopDong_ID = HD.ID;
                foreach (LoaiPhuKien loaiPK in pkdkItem.PKCollection)
                {
                    loaiPK.LoadCollection("MSSQL");
                    loaiPK.ID = 0;
                    foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                    {
                        hangPK.ID = 0;

                    }
                }
                pkdkCollectionMoi.Add(pkdkItem);
            }


            try
            {
                PhuKienDangKy pk = new PhuKienDangKy();
                pk.InsertUpdateDongBoDuLieuMoi(pkdkCollectionMoi, HD);
            }
            catch { }

        }

        public void DongBoPhuKienMoiFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP)
        {
            PhuKienDangKy pkdk = new PhuKienDangKy();
            pkdk.HopDong_ID = ID_HDTMP;
            pkdk.SetDabaseMoi("MSSQL");
            PhuKienDangKyCollection pkdkCollection = pkdk.SelectCollectionBy_HopDong_ID_KTX();
            PhuKienDangKyCollection pkdkCollectionMoi = new PhuKienDangKyCollection();
            foreach (PhuKienDangKy pkdkItem in pkdkCollection)
            {
                if (pkdkItem.LoadBySoPhuKien())
                    continue;
                pkdkItem.LoadCollection("MSSQL");
                pkdkItem.ID = 0;
                pkdkItem.HopDong_ID = HD.ID;
                foreach (LoaiPhuKien loaiPK in pkdkItem.PKCollection)
                {
                    loaiPK.LoadCollection("MSSQL");
                    loaiPK.ID = 0;
                    foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                    {
                        hangPK.ID = 0;

                    }
                }
                pkdkCollectionMoi.Add(pkdkItem);
            }


            try
            {
                PhuKienDangKy pk = new PhuKienDangKy();
                pk.InsertUpdateDongBoDuLieuMoiKTX(pkdkCollectionMoi, HD);
            }
            catch { }

        }

        public void DongBoPhuKienMoiFromDBTamToDBKTX(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP, string databaseName)
        {
            PhuKienDangKy pkdk = new PhuKienDangKy();
            pkdk.HopDong_ID = ID_HDTMP;
            pkdk.SetDabaseMoi("MSSQL");
            PhuKienDangKyCollection pkdkCollection = pkdk.SelectCollectionBy_HopDong_ID_KTX();
            PhuKienDangKyCollection pkdkCollectionMoi = new PhuKienDangKyCollection();
            foreach (PhuKienDangKy pkdkItem in pkdkCollection)
            {
                if (pkdkItem.LoadBySoPhuKien())
                    continue;
                pkdkItem.LoadCollection("MSSQL");
                pkdkItem.ID = 0;
                pkdkItem.HopDong_ID = HD.ID;
                foreach (LoaiPhuKien loaiPK in pkdkItem.PKCollection)
                {
                    loaiPK.LoadCollection("MSSQL");
                    loaiPK.ID = 0;
                    foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                    {
                        hangPK.ID = 0;

                    }
                }
                pkdkCollectionMoi.Add(pkdkItem);
            }


            try
            {
                PhuKienDangKy pk = new PhuKienDangKy();
                pk.InsertUpdateDongBoDuLieuMoiKTX(pkdkCollectionMoi, HD, databaseName);
            }
            catch (Exception ex) { throw ex; }

        }

        #endregion

        private void btnGHiDeALL_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(" Để tránh mất hoặc sai lệch dữ liệu trong một số trường hợp đặc biệt,người dùng nên backup dữ liệu trước khi thực hiện chức năng đồng bộ dữ liệu để có dữ liệu so sánh khi cần. Nếu chọn Yes sẽ ghi đè tất cả các dữ liệu đã tồn tại.\r\n\n-Lưu ý: Chức năng này cho phép \n\tTHÊM MỚI 'Hợp đồng' chưa có bên CSDL đích.\n\tHoặc CẬP NHẬT 'Hợp đồng' (nếu có) bên CSDL đích.\r\n\nBạn có muốn tiếp tục không ?", " WARNNING ", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                bgw.RunWorkerAsync();
            }
            else
            {
                return;
            }
        }

        private void ucGC_TQ_Load(object sender, EventArgs e)
        {
            dgListHQSP.RootTable.Columns["NhomSanPham_ID"].Visible = false;
            dgListHQTKMD.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            dgListDNTKMD.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;

            lblCaption.Text = "Đồng bộ  dữ liệu hợp đồng  Gia Công" + (GlobalSettings.PhienBan == "KTX" ? " - KHAI TỪ XA" : " - THÔNG QUAN ĐIỆN TỬ") + (GlobalSettings.radSynLocal == true ? " - Trong Nội mạng" : " - Từ Hải quan");

            lblHQ.Text = "DỮ LIỆU NGUỒN SERVER: [" + GlobalSettings.DiaChiSQLSource + "].[" + GlobalSettings.DBSource.ToUpper() + "]";
            lblDN.Text = "DỮ LIỆU ĐÍCH SERVER:  [" + GlobalSettings.DiaChiSQLTarget + "].[" + GlobalSettings.DBTarget.ToUpper() + "]";

            //Neu chon dong bo trong noi mang
            if (!GlobalSettings.IsSynFromHQ)
            {
                btnDongBoALL.Enabled = false;

                btnLayDuLieuNguon.Text = "Lấy dữ liệu từ '" + GlobalSettings.DBSource + "'";
            }
            else
            {
                btnDongBoALL.Enabled = true;
                btnLayDuLieuNguon.Text = "Lấy dữ liệu từ hệ thống tạm '" + GlobalSettings.DBSource + "'";
            }
        }

        private void btnDongBo1HD_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                DataRow[] rows = dtHQHopDong.Select("SohopDong = '" + cbHopDong.Text + "'");

                bool choPhepTrungHD = false;
                if (rows.Length > 1)
                {
                    if (DialogResult.Cancel == MessageBox.Show("Có 2 số hợp đồng '" + cbHopDong.Text + "' trùng nhau.\nBạn có muốn tiếp tục không?", this.Text, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
                        return;
                    else
                        choPhepTrungHD = true;
                }

                foreach (DataRow row in rows)
                {
                    long ID_HDTMP = Convert.ToInt64(row["ID"]);
                    Company.GC.BLL.KDT.GC.HopDong HDG = new Company.GC.BLL.KDT.GC.HopDong();
                    HDG.LoadDataHopDong(ID_HDTMP, "MSSQL");

                    /*
                    HDG.DiaChiDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DiaChiDoiTac"].ToString()).Trim();
                    HDG.DonViDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DonViDoiTac"].ToString()).Trim();
                    HDG.MaDoanhNghiep = GlobalSettings.MaDoanhNghiep;
                    HDG.MaHaiQuan = GlobalSettings.MaHaiQuan;
                    if (row["NgayGiaHan"].ToString() != "")
                        HDG.NgayGiaHan = Convert.ToDateTime(row["NgayGiaHan"]);
                    HDG.NgayHetHan = Convert.ToDateTime(row["NgayHetHan"]);
                    HDG.NgayKy = Convert.ToDateTime(row["NgayKy"]);
                    HDG.NguyenTe_ID = row["NguyenTe_ID"].ToString();
                    HDG.NuocThue_ID = row["NuocThue_ID"].ToString();
                    HDG.SoHopDong = row["SoHopDong"].ToString().Trim();
                    //HDG.SoTiepNhan = 0;
                    //HDG.TrangThaiXuLy = 1;
                    DongBoLoaiSPFromDBTamToDB(HDG, LoaiSP.GetLoaiSPAtTMP(ID_HDTMP));
                    //Nguyen phu lieu :
                    DongBoNPLFromDBTamToDB(HDG, NPL.GetNguyenPhuLieuAtTMP(ID_HDTMP));
                    //San pham :
                    DongBoSPFromDBTamToDB(HDG, SP.GetSanPhamAtTMP(ID_HDTMP));
                    //Thiet bi :
                    DongBoTBFromDBTamToDB(HDG, TB.GetTBAtTMP(ID_HDTMP));

                    DongBoDinhMucDangKyFromDBTamToDB(HDG, ID_HDTMP, "MSSQL");
                    */

                    try
                    {
                        string databseName = "MSSQLTARGET";

                        long ID = 0;
                        if (!choPhepTrungHD)
                            ID = HDG.GetIDHopDongExit(HDG.SoHopDong, HDG.MaHaiQuan, HDG.MaDoanhNghiep, HDG.NgayKy, databseName);
                        else
                            ID = HDG.GetIDHopDongExit(HDG.SoHopDong, HDG.MaHaiQuan, HDG.MaDoanhNghiep, databseName);

                        HDG.ID = ID;

                        if (GlobalSettings.PhienBan == "KTX")
                        {
                            HDG.InsertUpdateHopDongDongBoDuLieuKTX(databseName);

                            DinhMucOfHDKTX(HDG, DM.GetDMAtTMP(ID_HDTMP), 2, databseName);
                            DongBoPhuKienFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                            DongBoTKMDFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                            DongBoTKChuyenTiepFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                            DongBoBKCUFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                        }
                        else if (GlobalSettings.PhienBan == "TQDT")
                        {
                            HDG.InsertUpdateHopDongDongBoDuLieuTQDT(databseName, chkV3.Checked);

                            HDG.TinhToanCanDoiHopDong(5, 3, 6, 4, databseName, chkV3.Checked);

                            //DinhMucOfHDTQDT(HDG, DM.GetDMDKAtTMP_TQDT(ID_HDTMP), (int)ID_HDTMP, databseName);
                            //DongBoPhuKienFromDBTamToDB(HDG, ID_HDTMP, databseName);
                            //DongBoTKMDFromDBTamToDB(HDG, ID_HDTMP, databseName);
                            //DongBoTKChuyenTiepFromDBTamToDB(HDG, ID_HDTMP, databseName);
                            //DongBoBKCUFromDBTamToDB(HDG, ID_HDTMP, databseName);
                        }
                    }
                    catch (Exception ex1)
                    {
                        //MessageBox.Show(" Lỗi : " + ex1.Message + " hợp đồng : " + HDG.SoHopDong); 
                        throw ex1;
                    }
                }

                MessageBox.Show(" Hoàn thành việc đồng bộ hợp đồng ");
                BindDataMain();
            }
            catch (Exception exe) { DisplayMessages("Lỗi : " + exe.Message.ToString()); }
            finally { Cursor = Cursors.Default; }
        }

        #region BackgroundWorker
        delegate void SetTextCallback(System.Windows.Forms.Label control, string message);
        private void SetText(System.Windows.Forms.Label lblControl, string message)
        {
            if (lblControl.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { lblControl, message });
            }
            else
            {
                lblControl.Text = message;
            }
        }

        delegate void SetProgessBarCallback(System.Windows.Forms.ProgressBar pbControl, int percent);
        private void SetProgessBar(System.Windows.Forms.ProgressBar pbControl, int percent)
        {
            if (pbControl.InvokeRequired)
            {
                SetProgessBarCallback d = new SetProgessBarCallback(SetProgessBar);
                this.Invoke(d, new object[] { pbControl, percent });
            }
            else
            {
                pbControl.Value = percent;

                pbControl.Text = percent + " %";
            }
        }

        private void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int cnt = 0;
                foreach (DataRow row in dtHQHopDong.Rows)
                {
                    long ID_HDTMP = Convert.ToInt64(row["ID"]);
                    Company.GC.BLL.KDT.GC.HopDong HDG = new Company.GC.BLL.KDT.GC.HopDong();

                    SetText(lblMsgSoHopDong, "Hợp đồng: " + row["SoHopDong"].ToString() + " đang tải dữ liệu...");

                    cnt += 1;
                    HDG.LoadDataHopDong(ID_HDTMP, "MSSQL");

                    /*
                    HDG.DiaChiDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DiaChiDoiTac"].ToString()).Trim();
                    HDG.DonViDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DonViDoiTac"].ToString()).Trim();
                    HDG.MaDoanhNghiep = GlobalSettings.MaDoanhNghiep;
                    HDG.MaHaiQuan = GlobalSettings.MaHaiQuan;
                    if (row["NgayGiaHan"].ToString() != "")
                        HDG.NgayGiaHan = Convert.ToDateTime(row["NgayGiaHan"]);
                    HDG.NgayHetHan = Convert.ToDateTime(row["NgayHetHan"]);
                    HDG.NgayKy = Convert.ToDateTime(row["NgayKy"]);
                    HDG.NguyenTe_ID = row["NguyenTe_ID"].ToString();
                    HDG.NuocThue_ID = row["NuocThue_ID"].ToString();
                    HDG.SoHopDong = row["SoHopDong"].ToString().Trim();
                    HDG.SoTiepNhan = 0;
                    HDG.TrangThaiXuLy = 1;
                    DongBoLoaiSPFromDBTamToDB(HDG, LoaiSP.GetLoaiSPAtTMP(ID_HDTMP));
                    //Nguyen phu lieu :
                    DongBoNPLFromDBTamToDB(HDG, NPL.GetNguyenPhuLieuAtTMP(ID_HDTMP));
                    //San pham :
                    DongBoSPFromDBTamToDB(HDG, SP.GetSanPhamAtTMP(ID_HDTMP));
                    //Thiet bi :
                    DongBoTBFromDBTamToDB(HDG, TB.GetTBAtTMP(ID_HDTMP));
                    */
                    try
                    {
                        string databseName = "MSSQLTARGET";

                        long ID = HDG.GetIDHopDongExit(HDG.SoHopDong, HDG.MaHaiQuan, HDG.MaDoanhNghiep, HDG.NgayKy, databseName);
                        HDG.ID = ID;

                        if (GlobalSettings.PhienBan == "KTX")
                        {
                            //HDG.InsertUpdateHopDongDongBoDuLieuKTX(databseName);

                            //DinhMucOfHDKTX(HDG, DM.GetDMAtTMP(ID_HDTMP), 2, databseName);
                            //DongBoPhuKienFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                            //DongBoTKMDFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                            //DongBoTKChuyenTiepFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                            //DongBoBKCUFromDBTamToDBKTX(HDG, ID_HDTMP, databseName);
                        }
                        else if (GlobalSettings.PhienBan == "TQDT")
                        {
                            SetText(lblMsgSoHopDong, "Hợp đồng: " + row["SoHopDong"].ToString() + " đang chuyển dữ liệu...");

                            HDG.InsertUpdateHopDongDongBoDuLieuTQDT(databseName, chkV3.Checked);

                            SetText(lblMsgSoHopDong, "Hợp đồng: " + row["SoHopDong"].ToString() + " đang xử lý dữ liệu...");
                            HDG.TinhToanCanDoiHopDong(5, 3, 6, 4, databseName, chkV3.Checked);

                            if (!bgw.CancellationPending)
                                bgw.ReportProgress((int)(((double)(cnt) / (double)dtHQHopDong.Rows.Count) * 100));

                            SetText(lblMsgPercent, cnt + "/" + dtHQHopDong.Rows.Count + " (" + (int)(((double)(cnt) / (double)dtHQHopDong.Rows.Count) * 100) + "%)");

                            //DinhMucOfHD(HDG, DM.GetDMAtTMP(ID_HDTMP), 2);
                            //DongBoPhuKienFromDBTamToDB(HDG, ID_HDTMP);
                            //DongBoTKMDFromDBTamToDB(HDG, ID_HDTMP);
                            //DongBoTKChuyenTiepFromDBTamToDB(HDG, ID_HDTMP);
                            //DongBoBKCUFromDBTamToDB(HDG, ID_HDTMP);
                        }
                    }
                    catch (Exception ex1) { MessageBox.Show(" Lỗi : " + ex1.Message + " hợp đồng : " + HDG.SoHopDong); }
                }
            }
            catch (Exception exx) { e.Result = " Lỗi : " + exx.Message.ToString(); e.Cancel = true; }
        }

        private void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (bgw.CancellationPending == true) return;

            SetProgessBar(progressBar, e.ProgressPercentage);
        }

        private void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                this.SetText(lblMsgSoHopDong, "Lỗi: " + e.Error);
                return;
            }

            this.SetText(lblMsgSoHopDong, "Hoàn thành việc đồng bộ hợp đồng");
            BindDataMain();
        }
        #endregion
    }
}
