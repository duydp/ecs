namespace LicenseCode
{
    partial class frmKeysApprove
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Approve = new System.Windows.Forms.ToolStripMenuItem();
            this.DisApprove = new System.Windows.Forms.ToolStripMenuItem();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.requetBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ProductId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Days = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CustomerCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CustomerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CustomerDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RequestedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.ContextMenuStrip = this.contextMenuStrip1;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(903, 399);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Approve,
            this.DisApprove});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(133, 48);
            this.contextMenuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuStrip1_ItemClicked);
            // 
            // Approve
            // 
            this.Approve.Name = "Approve";
            this.Approve.Size = new System.Drawing.Size(132, 22);
            this.Approve.Text = "Chấp nhận";
            // 
            // DisApprove
            // 
            this.DisApprove.Name = "DisApprove";
            this.DisApprove.Size = new System.Drawing.Size(132, 22);
            this.DisApprove.Text = "Từ chối";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.requetBy,
            this.ProductId,
            this.SerialNumber,
            this.Days,
            this.CustomerCode,
            this.CustomerName,
            this.CustomerDescription,
            this.RequestedDate});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoSelectAllInEditor = false;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
            // 
            // requetBy
            // 
            this.requetBy.Caption = "Người lấy key";
            this.requetBy.FieldName = "RequestedBy";
            this.requetBy.Name = "requetBy";
            this.requetBy.OptionsColumn.AllowEdit = false;
            this.requetBy.Visible = true;
            this.requetBy.VisibleIndex = 0;
            this.requetBy.Width = 85;
            // 
            // ProductId
            // 
            this.ProductId.Caption = "Sản phẩm";
            this.ProductId.FieldName = "ProductId";
            this.ProductId.Name = "ProductId";
            this.ProductId.OptionsColumn.AllowEdit = false;
            this.ProductId.Visible = true;
            this.ProductId.VisibleIndex = 1;
            this.ProductId.Width = 113;
            // 
            // SerialNumber
            // 
            this.SerialNumber.Caption = "SerialNumber";
            this.SerialNumber.FieldName = "SerialNumber";
            this.SerialNumber.Name = "SerialNumber";
            this.SerialNumber.OptionsColumn.AllowEdit = false;
            this.SerialNumber.Visible = true;
            this.SerialNumber.VisibleIndex = 2;
            this.SerialNumber.Width = 113;
            // 
            // Days
            // 
            this.Days.Caption = "Thời hạn (Ngày)";
            this.Days.FieldName = "Days";
            this.Days.Name = "Days";
            this.Days.OptionsColumn.AllowEdit = false;
            this.Days.Visible = true;
            this.Days.VisibleIndex = 3;
            this.Days.Width = 113;
            // 
            // CustomerCode
            // 
            this.CustomerCode.Caption = "Mã doanh nghiệp";
            this.CustomerCode.FieldName = "CustomerCode";
            this.CustomerCode.Name = "CustomerCode";
            this.CustomerCode.OptionsColumn.AllowEdit = false;
            this.CustomerCode.Visible = true;
            this.CustomerCode.VisibleIndex = 4;
            this.CustomerCode.Width = 113;
            // 
            // CustomerName
            // 
            this.CustomerName.Caption = "Tên Doanh Nghiệp";
            this.CustomerName.FieldName = "CustomerName";
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.OptionsColumn.AllowEdit = false;
            this.CustomerName.Visible = true;
            this.CustomerName.VisibleIndex = 5;
            this.CustomerName.Width = 113;
            // 
            // CustomerDescription
            // 
            this.CustomerDescription.Caption = "Mô tả";
            this.CustomerDescription.FieldName = "CustomerDescription";
            this.CustomerDescription.Name = "CustomerDescription";
            this.CustomerDescription.OptionsColumn.AllowEdit = false;
            this.CustomerDescription.Visible = true;
            this.CustomerDescription.VisibleIndex = 6;
            this.CustomerDescription.Width = 113;
            // 
            // RequestedDate
            // 
            this.RequestedDate.Caption = "Ngày lấy key";
            this.RequestedDate.DisplayFormat.FormatString = "dd-MM-yyyy";
            this.RequestedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.RequestedDate.FieldName = "RequestedDate";
            this.RequestedDate.Name = "RequestedDate";
            this.RequestedDate.OptionsColumn.AllowEdit = false;
            this.RequestedDate.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            this.RequestedDate.Visible = true;
            this.RequestedDate.VisibleIndex = 7;
            this.RequestedDate.Width = 119;
            // 
            // frmKeysApprove
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 399);
            this.Controls.Add(this.gridControl1);
            this.Name = "frmKeysApprove";
            this.Text = "Danh sách key chờ được xác nhận";
            this.Load += new System.EventHandler(this.frmKeysApprove_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Approve;
        private System.Windows.Forms.ToolStripMenuItem DisApprove;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn requetBy;
        private DevExpress.XtraGrid.Columns.GridColumn ProductId;
        private DevExpress.XtraGrid.Columns.GridColumn SerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn Days;
        private DevExpress.XtraGrid.Columns.GridColumn CustomerCode;
        private DevExpress.XtraGrid.Columns.GridColumn CustomerName;
        private DevExpress.XtraGrid.Columns.GridColumn CustomerDescription;
        private DevExpress.XtraGrid.Columns.GridColumn RequestedDate;

    }
}