﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface.SXXK
{
    public partial class NPLSaiLechForm : BaseForm
    {
        public NPLSaiLechForm()
        {
            InitializeComponent();
        }
        public DataTable dt = new DataTable();

        private void NPLSaiLechForm_Load(object sender, EventArgs e)
        {
            if (dt != null && dt.Rows.Count > 0)
                gridEX1.DataSource = dt;
            else
                this.Close();

            gridEX1.Tables[0].Columns["SoLuong"].FormatString = Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan.LuongNPL);
            gridEX1.Tables[0].Columns["SoLuongKB"].FormatString = Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan.LuongNPL);
        }

    }
}
