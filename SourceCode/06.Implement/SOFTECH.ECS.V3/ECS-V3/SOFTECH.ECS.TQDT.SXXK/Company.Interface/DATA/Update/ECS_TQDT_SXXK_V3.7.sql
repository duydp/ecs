﻿/*
Run this script on:

192.168.72.100.ECS_TQDT_SXXK_V2_VERSION    -  This database will be modified

to synchronize it with:

192.168.72.100.ECS_TQDT_SXXK_V2

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 8.0.2 from Red Gate Software Ltd at 07/09/2012 12:13:21 PM

*/
		
SET XACT_ABORT ON
GO
SET ARITHABORT ON
GO
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)
BEGIN TRANSACTION

DELETE FROM [dbo].[t_HaiQuan_CuaKhau] WHERE ID IN ('D007', 'D02S')

-- Add rows to [dbo].[t_HaiQuan_CuaKhau]
INSERT INTO [dbo].[t_HaiQuan_CuaKhau] ([ID], [Ten]) VALUES ('D007', N'Chi Cục HQ Chuyển Phát Nhanh')
-- Operation applied to 5 rows out of 5
COMMIT TRANSACTION
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='3.7', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('3.7', getdate(), null)
	end 


