﻿namespace Company.Interface.Report.SXXK
{
    partial class ToKhaiXuatA4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiXuatA4));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable58 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable57 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable55 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable46 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable44 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable42 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable40 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable39 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable38 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable37 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDaiLyTTHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable35 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable34 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoBanChinh5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable32 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTenChungTu6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTenChungTu5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDaiLyTTHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgoaiTe = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTyGiaTT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPTTT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiUyThac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiUyThac = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaDiemDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDKGH = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDoiTac = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHHHD = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHD = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNuoc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNuoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBanLuuHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTiepNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChiCucHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCucHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoPLTK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoPLTK1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTrangthai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable41 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable43 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDoanhNghiep1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDoanhNghiep1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrKhongThue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrGC = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTNK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCoThue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSXXK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrKD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHHGP = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayGP = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoGP = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoKienTrongLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine8,
            this.xrLine7,
            this.xrLine6,
            this.xrLine5,
            this.xrLine4,
            this.xrLine3,
            this.xrLine2,
            this.xrLine1,
            this.xrTable58,
            this.xrTable57,
            this.xrTable55,
            this.xrTable46,
            this.xrTable44,
            this.xrTable42,
            this.xrLabel29,
            this.xrLabel27,
            this.xrLabel26,
            this.xrLabel25,
            this.xrLabel24,
            this.xrLabel22,
            this.xrLabel21,
            this.xrTable40,
            this.xrLabel20,
            this.xrTable39,
            this.xrTable38,
            this.xrTable37,
            this.lblTenDaiLyTTHQ,
            this.xrTable36,
            this.xrTable35,
            this.xrTable34,
            this.lblSoBanChinh5,
            this.lblSoBanSao5,
            this.xrTable33,
            this.xrTable32,
            this.xrTable31,
            this.xrTable30,
            this.xrTable29,
            this.xrTable28,
            this.xrTable27,
            this.xrTable26,
            this.xrTable16,
            this.xrTable14,
            this.xrTable9,
            this.xrTable7,
            this.lblBanLuuHaiQuan,
            this.xrLabel2,
            this.xrLabel1,
            this.xrLabel23,
            this.lblSoBanChinh6,
            this.lblSoBanSao6,
            this.lblSoBanChinh4,
            this.lblSoBanChinh3,
            this.lblSoBanChinh2,
            this.lblSoBanChinh1,
            this.lblSoBanSao1,
            this.lblSoBanSao2,
            this.lblSoBanSao3,
            this.lblSoBanSao4,
            this.lblSoTiepNhan,
            this.xrTable4,
            this.xrTable41,
            this.xrTable43,
            this.xrTable5,
            this.lblSoKienTrongLuong,
            this.xrTable1});
            this.Detail.Height = 2268;
            this.Detail.Name = "Detail";
            this.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLine8
            // 
            this.xrLine8.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine8.Location = new System.Drawing.Point(25, 858);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.Size = new System.Drawing.Size(767, 10);
            // 
            // xrLine7
            // 
            this.xrLine7.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine7.Location = new System.Drawing.Point(25, 821);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.Size = new System.Drawing.Size(767, 10);
            // 
            // xrLine6
            // 
            this.xrLine6.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine6.Location = new System.Drawing.Point(25, 784);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.Size = new System.Drawing.Size(767, 10);
            // 
            // xrLine5
            // 
            this.xrLine5.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine5.Location = new System.Drawing.Point(25, 750);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.Size = new System.Drawing.Size(767, 10);
            // 
            // xrLine4
            // 
            this.xrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine4.Location = new System.Drawing.Point(25, 716);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.Size = new System.Drawing.Size(767, 10);
            // 
            // xrLine3
            // 
            this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine3.Location = new System.Drawing.Point(25, 675);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.Size = new System.Drawing.Size(767, 10);
            // 
            // xrLine2
            // 
            this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine2.Location = new System.Drawing.Point(25, 644);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Size = new System.Drawing.Size(767, 10);
            // 
            // xrLine1
            // 
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine1.Location = new System.Drawing.Point(25, 606);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Size = new System.Drawing.Size(767, 10);
            // 
            // xrTable58
            // 
            this.xrTable58.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable58.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable58.Location = new System.Drawing.Point(25, 2075);
            this.xrTable58.Name = "xrTable58";
            this.xrTable58.ParentStyleUsing.UseBorders = false;
            this.xrTable58.ParentStyleUsing.UseFont = false;
            this.xrTable58.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow75});
            this.xrTable58.Size = new System.Drawing.Size(767, 170);
            // 
            // xrTableRow75
            // 
            this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell216,
            this.xrTableCell217,
            this.xrTableCell218});
            this.xrTableRow75.Name = "xrTableRow75";
            this.xrTableRow75.Size = new System.Drawing.Size(767, 170);
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell216.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell216.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell216.Multiline = true;
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell216.ParentStyleUsing.UseBorders = false;
            this.xrTableCell216.ParentStyleUsing.UseFont = false;
            this.xrTableCell216.Size = new System.Drawing.Size(267, 170);
            this.xrTableCell216.Text = "25. Ghi chép khác của hải quan.";
            this.xrTableCell216.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell217.Location = new System.Drawing.Point(267, 0);
            this.xrTableCell217.Multiline = true;
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell217.ParentStyleUsing.UseFont = false;
            this.xrTableCell217.Size = new System.Drawing.Size(309, 170);
            this.xrTableCell217.Text = "26. Xác nhận đã làm thủ tục hải quan\r\n (Ký, đóng dấu, ghi rõ họ tên).";
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell218.Location = new System.Drawing.Point(576, 0);
            this.xrTableCell218.Multiline = true;
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell218.ParentStyleUsing.UseFont = false;
            this.xrTableCell218.Size = new System.Drawing.Size(191, 170);
            this.xrTableCell218.Text = "27. Xác nhận thực xuất\r\n (Ký, đóng dấu, ghi rõ họ tên).";
            // 
            // xrTable57
            // 
            this.xrTable57.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable57.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable57.Location = new System.Drawing.Point(25, 2033);
            this.xrTable57.Name = "xrTable57";
            this.xrTable57.ParentStyleUsing.UseBorders = false;
            this.xrTable57.ParentStyleUsing.UseFont = false;
            this.xrTable57.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow74});
            this.xrTable57.Size = new System.Drawing.Size(767, 45);
            // 
            // xrTableRow74
            // 
            this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell215});
            this.xrTableRow74.Name = "xrTableRow74";
            this.xrTableRow74.Size = new System.Drawing.Size(767, 45);
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell215.CanGrow = false;
            this.xrTableCell215.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell215.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell215.Multiline = true;
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell215.ParentStyleUsing.UseBorders = false;
            this.xrTableCell215.ParentStyleUsing.UseFont = false;
            this.xrTableCell215.Size = new System.Drawing.Size(767, 45);
            this.xrTableCell215.Text = "Biên lai thu lệ phí số: ..................................................Ngày: ." +
                "..............................................";
            this.xrTableCell215.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable55
            // 
            this.xrTable55.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable55.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable55.Location = new System.Drawing.Point(25, 1992);
            this.xrTable55.Name = "xrTable55";
            this.xrTable55.ParentStyleUsing.UseBorders = false;
            this.xrTable55.ParentStyleUsing.UseFont = false;
            this.xrTable55.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow72});
            this.xrTable55.Size = new System.Drawing.Size(767, 45);
            // 
            // xrTableRow72
            // 
            this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell212});
            this.xrTableRow72.Name = "xrTableRow72";
            this.xrTableRow72.Size = new System.Drawing.Size(767, 45);
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell212.CanGrow = false;
            this.xrTableCell212.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell212.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell212.Multiline = true;
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell212.ParentStyleUsing.UseBorders = false;
            this.xrTableCell212.ParentStyleUsing.UseFont = false;
            this.xrTableCell212.Size = new System.Drawing.Size(767, 45);
            this.xrTableCell212.Text = resources.GetString("xrTableCell212.Text");
            this.xrTableCell212.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable46
            // 
            this.xrTable46.Location = new System.Drawing.Point(25, 1875);
            this.xrTable46.Name = "xrTable46";
            this.xrTable46.ParentStyleUsing.UseBorders = false;
            this.xrTable46.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow59});
            this.xrTable46.Size = new System.Drawing.Size(767, 118);
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell166,
            this.xrTableCell167});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Size = new System.Drawing.Size(767, 118);
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell166.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 5, 0, 100F);
            this.xrTableCell166.ParentStyleUsing.UseBorders = false;
            this.xrTableCell166.Size = new System.Drawing.Size(267, 118);
            this.xrTableCell166.Text = "22. Đại diện doanh nghiệp (Ký ghi rõ họ tên).";
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell167.CanGrow = false;
            this.xrTableCell167.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell167.Location = new System.Drawing.Point(267, 0);
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 5, 0, 100F);
            this.xrTableCell167.ParentStyleUsing.UseBorders = false;
            this.xrTableCell167.ParentStyleUsing.UseFont = false;
            this.xrTableCell167.Size = new System.Drawing.Size(500, 118);
            this.xrTableCell167.Text = "23.Cán bộ kiểm hóa (Ký , ghi rõ họ tên). ";
            // 
            // xrTable44
            // 
            this.xrTable44.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable44.Location = new System.Drawing.Point(25, 1210);
            this.xrTable44.Name = "xrTable44";
            this.xrTable44.ParentStyleUsing.UseBorders = false;
            this.xrTable44.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow57});
            this.xrTable44.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell164});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell164.CanGrow = false;
            this.xrTableCell164.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell164.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell164.ParentStyleUsing.UseBorders = false;
            this.xrTableCell164.ParentStyleUsing.UseFont = false;
            this.xrTableCell164.Size = new System.Drawing.Size(767, 25);
            this.xrTableCell164.Text = "21. Phần ghi kết quả kiểm tra của Hải quan";
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable42
            // 
            this.xrTable42.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable42.Location = new System.Drawing.Point(25, 1235);
            this.xrTable42.Name = "xrTable42";
            this.xrTable42.ParentStyleUsing.UseBorders = false;
            this.xrTable42.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow55});
            this.xrTable42.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell162});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell162.CanGrow = false;
            this.xrTableCell162.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell162.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell162.ParentStyleUsing.UseBorders = false;
            this.xrTableCell162.ParentStyleUsing.UseFont = false;
            this.xrTableCell162.Size = new System.Drawing.Size(767, 25);
            this.xrTableCell162.Text = "Người quyết định hình thức kiểm tra: (ghi rõ họ tên): ";
            this.xrTableCell162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.BorderWidth = 0;
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.Location = new System.Drawing.Point(175, 1260);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel29.ParentStyleUsing.UseFont = false;
            this.xrLabel29.Size = new System.Drawing.Size(92, 23);
            this.xrLabel29.Text = "Miễn kiểm tra";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.Location = new System.Drawing.Point(158, 1267);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.ParentStyleUsing.UseBorders = false;
            this.xrLabel27.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel27.ParentStyleUsing.UseFont = false;
            this.xrLabel27.Size = new System.Drawing.Size(10, 10);
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel26
            // 
            this.xrLabel26.BorderWidth = 0;
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.Location = new System.Drawing.Point(350, 1260);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel26.ParentStyleUsing.UseFont = false;
            this.xrLabel26.Size = new System.Drawing.Size(133, 23);
            this.xrLabel26.Text = "Kiểm tra xác suất. Tỷ lệ :";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.BorderWidth = 0;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.Location = new System.Drawing.Point(483, 1260);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel25.ParentStyleUsing.UseFont = false;
            this.xrLabel25.Size = new System.Drawing.Size(59, 23);
            this.xrLabel25.Text = ".............%";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.BorderWidth = 0;
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.Location = new System.Drawing.Point(608, 1260);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel24.ParentStyleUsing.UseFont = false;
            this.xrLabel24.Size = new System.Drawing.Size(92, 23);
            this.xrLabel24.Text = "Kiểm tra toàn bộ";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.Location = new System.Drawing.Point(333, 1267);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.ParentStyleUsing.UseBorders = false;
            this.xrLabel22.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel22.ParentStyleUsing.UseFont = false;
            this.xrLabel22.Size = new System.Drawing.Size(10, 10);
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.Location = new System.Drawing.Point(592, 1267);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.ParentStyleUsing.UseBorders = false;
            this.xrLabel21.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel21.ParentStyleUsing.UseFont = false;
            this.xrLabel21.Size = new System.Drawing.Size(10, 10);
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable40
            // 
            this.xrTable40.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable40.Location = new System.Drawing.Point(25, 1283);
            this.xrTable40.Name = "xrTable40";
            this.xrTable40.ParentStyleUsing.UseBorders = false;
            this.xrTable40.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow53});
            this.xrTable40.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell160});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell160.CanGrow = false;
            this.xrTableCell160.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell160.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell160.ParentStyleUsing.UseBorders = false;
            this.xrTableCell160.ParentStyleUsing.UseFont = false;
            this.xrTableCell160.Size = new System.Drawing.Size(767, 25);
            this.xrTableCell160.Text = "Địa điểm kiểm tra :";
            this.xrTableCell160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.BorderWidth = 0;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.Location = new System.Drawing.Point(333, 1283);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel20.ParentStyleUsing.UseFont = false;
            this.xrLabel20.Size = new System.Drawing.Size(459, 23);
            this.xrLabel20.Text = "Thời gian kiểm tra: Từ:                           giờ,ngày                       " +
                "  Đến:                giờ, ngày";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable39
            // 
            this.xrTable39.BorderColor = System.Drawing.Color.DarkGray;
            this.xrTable39.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable39.Location = new System.Drawing.Point(33, 1342);
            this.xrTable39.Name = "xrTable39";
            this.xrTable39.ParentStyleUsing.UseBorderColor = false;
            this.xrTable39.ParentStyleUsing.UseBorders = false;
            this.xrTable39.ParentStyleUsing.UseBorderWidth = false;
            this.xrTable39.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51,
            this.xrTableRow52,
            this.xrTableRow26,
            this.xrTableRow58,
            this.xrTableRow60,
            this.xrTableRow61,
            this.xrTableRow62,
            this.xrTableRow63,
            this.xrTableRow64,
            this.xrTableRow65,
            this.xrTableRow66,
            this.xrTableRow67,
            this.xrTableRow68,
            this.xrTableRow69});
            this.xrTable39.Size = new System.Drawing.Size(751, 525);
            this.xrTable39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell151});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell151.Size = new System.Drawing.Size(751, 25);
            this.xrTableCell151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell152});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell152.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell153.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell154});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell154.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell155.Size = new System.Drawing.Size(751, 25);
            this.xrTableCell155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell156});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell156.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell157});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell157.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell158});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell158.Size = new System.Drawing.Size(751, 25);
            this.xrTableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell159});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell159.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell165});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell165.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell168});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell168.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell169});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell169.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell170});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell170.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell171});
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell171.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell172});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell172.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell173});
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell173.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell174});
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell174.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell175});
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell175.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell176});
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell176.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell177});
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell177.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTable38
            // 
            this.xrTable38.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable38.Location = new System.Drawing.Point(25, 1153);
            this.xrTable38.Name = "xrTable38";
            this.xrTable38.ParentStyleUsing.UseBorders = false;
            this.xrTable38.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow43});
            this.xrTable38.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell150});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell150.CanGrow = false;
            this.xrTableCell150.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell150.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell150.ParentStyleUsing.UseBorders = false;
            this.xrTableCell150.ParentStyleUsing.UseFont = false;
            this.xrTableCell150.Size = new System.Drawing.Size(767, 30);
            this.xrTableCell150.Text = "B-PHẦN DÀNH CHO KIỂM TRA CỦA HẢI QUAN";
            this.xrTableCell150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable37
            // 
            this.xrTable37.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable37.Location = new System.Drawing.Point(25, 1183);
            this.xrTable37.Name = "xrTable37";
            this.xrTable37.ParentStyleUsing.UseBorders = false;
            this.xrTable37.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow42});
            this.xrTable37.Size = new System.Drawing.Size(767, 27);
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell149});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Size = new System.Drawing.Size(767, 27);
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell149.CanGrow = false;
            this.xrTableCell149.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell149.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell149.ParentStyleUsing.UseBorders = false;
            this.xrTableCell149.ParentStyleUsing.UseFont = false;
            this.xrTableCell149.Size = new System.Drawing.Size(767, 27);
            this.xrTableCell149.Text = "I-PHẦN KIỂM TRA HÀNG HÓA ";
            this.xrTableCell149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTenDaiLyTTHQ
            // 
            this.lblTenDaiLyTTHQ.BorderWidth = 0;
            this.lblTenDaiLyTTHQ.CanGrow = false;
            this.lblTenDaiLyTTHQ.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblTenDaiLyTTHQ.Location = new System.Drawing.Point(33, 500);
            this.lblTenDaiLyTTHQ.Name = "lblTenDaiLyTTHQ";
            this.lblTenDaiLyTTHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDaiLyTTHQ.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenDaiLyTTHQ.ParentStyleUsing.UseFont = false;
            this.lblTenDaiLyTTHQ.Size = new System.Drawing.Size(375, 33);
            this.lblTenDaiLyTTHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable36
            // 
            this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable36.Location = new System.Drawing.Point(408, 1088);
            this.xrTable36.Name = "xrTable36";
            this.xrTable36.ParentStyleUsing.UseBorders = false;
            this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow41});
            this.xrTable36.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell142});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell142.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell142.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 8, 0, 0, 100F);
            this.xrTableCell142.ParentStyleUsing.UseBorders = false;
            this.xrTableCell142.ParentStyleUsing.UseFont = false;
            this.xrTableCell142.Size = new System.Drawing.Size(384, 23);
            this.xrTableCell142.Text = " (Người khai báo ghi rõ họ tên, chức danh, ký tên và đóng dấu)";
            this.xrTableCell142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable35
            // 
            this.xrTable35.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable35.Location = new System.Drawing.Point(408, 930);
            this.xrTable35.Name = "xrTable35";
            this.xrTable35.ParentStyleUsing.UseBorders = false;
            this.xrTable35.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow40});
            this.xrTable35.Size = new System.Drawing.Size(384, 159);
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell144});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Size = new System.Drawing.Size(384, 159);
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell144.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell144.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell144.Multiline = true;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 10, 0, 100F);
            this.xrTableCell144.ParentStyleUsing.UseBorders = false;
            this.xrTableCell144.ParentStyleUsing.UseFont = false;
            this.xrTableCell144.Size = new System.Drawing.Size(384, 159);
            this.xrTableCell144.Text = "20. Tôi xin cam đoan, chụi trách nhiệm trước pháp luật về những nội dung khai báo" +
                " trên tờ khai này.\r\n                                                       Ngày." +
                "........tháng.........năm........\r\n\r\n\r\n";
            // 
            // xrTable34
            // 
            this.xrTable34.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable34.Location = new System.Drawing.Point(25, 133);
            this.xrTable34.Name = "xrTable34";
            this.xrTable34.ParentStyleUsing.UseBorders = false;
            this.xrTable34.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39});
            this.xrTable34.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell148});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell148.CanGrow = false;
            this.xrTableCell148.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell148.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell148.ParentStyleUsing.UseBorders = false;
            this.xrTableCell148.ParentStyleUsing.UseFont = false;
            this.xrTableCell148.Size = new System.Drawing.Size(767, 30);
            this.xrTableCell148.Text = "A-PHẦN DÀNH CHO NGƯỜI KHAI HẢI QUAN KÊ KHAI";
            this.xrTableCell148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanChinh5
            // 
            this.lblSoBanChinh5.Location = new System.Drawing.Point(208, 1046);
            this.lblSoBanChinh5.Name = "lblSoBanChinh5";
            this.lblSoBanChinh5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh5.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao5
            // 
            this.lblSoBanSao5.Location = new System.Drawing.Point(317, 1046);
            this.lblSoBanSao5.Name = "lblSoBanSao5";
            this.lblSoBanSao5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao5.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable33
            // 
            this.xrTable33.Location = new System.Drawing.Point(25, 1088);
            this.xrTable33.Name = "xrTable33";
            this.xrTable33.ParentStyleUsing.UseBorders = false;
            this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38});
            this.xrTable33.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell146,
            this.xrTableCell147});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell146.CanGrow = false;
            this.xrTableCell146.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell146.Multiline = true;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell146.ParentStyleUsing.UseBorders = false;
            this.xrTableCell146.Size = new System.Drawing.Size(145, 23);
            this.xrTableCell146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell147.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell147.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell147.Multiline = true;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell147.ParentStyleUsing.UseBorders = false;
            this.xrTableCell147.ParentStyleUsing.UseFont = false;
            this.xrTableCell147.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell147.Text = ":         ......................             ...................";
            this.xrTableCell147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable32
            // 
            this.xrTable32.Location = new System.Drawing.Point(25, 1065);
            this.xrTable32.Name = "xrTable32";
            this.xrTable32.ParentStyleUsing.UseBorders = false;
            this.xrTable32.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37});
            this.xrTable32.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTenChungTu6,
            this.xrTableCell145});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Size = new System.Drawing.Size(384, 23);
            // 
            // lblTenChungTu6
            // 
            this.lblTenChungTu6.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.lblTenChungTu6.CanGrow = false;
            this.lblTenChungTu6.Location = new System.Drawing.Point(0, 0);
            this.lblTenChungTu6.Multiline = true;
            this.lblTenChungTu6.Name = "lblTenChungTu6";
            this.lblTenChungTu6.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.lblTenChungTu6.ParentStyleUsing.UseBorders = false;
            this.lblTenChungTu6.Size = new System.Drawing.Size(145, 23);
            this.lblTenChungTu6.Text = "-";
            this.lblTenChungTu6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell145.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell145.Multiline = true;
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell145.ParentStyleUsing.UseFont = false;
            this.xrTableCell145.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell145.Text = ":         ......................             ...................";
            this.xrTableCell145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable31
            // 
            this.xrTable31.Location = new System.Drawing.Point(25, 1043);
            this.xrTable31.Name = "xrTable31";
            this.xrTable31.ParentStyleUsing.UseBorders = false;
            this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable31.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTenChungTu5,
            this.xrTableCell143});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Size = new System.Drawing.Size(384, 23);
            // 
            // lblTenChungTu5
            // 
            this.lblTenChungTu5.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.lblTenChungTu5.CanGrow = false;
            this.lblTenChungTu5.Location = new System.Drawing.Point(0, 0);
            this.lblTenChungTu5.Multiline = true;
            this.lblTenChungTu5.Name = "lblTenChungTu5";
            this.lblTenChungTu5.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.lblTenChungTu5.ParentStyleUsing.UseBorders = false;
            this.lblTenChungTu5.Size = new System.Drawing.Size(145, 23);
            this.lblTenChungTu5.Text = "-";
            this.lblTenChungTu5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell143.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell143.Multiline = true;
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell143.ParentStyleUsing.UseFont = false;
            this.xrTableCell143.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell143.Text = ":         ......................             ...................";
            this.xrTableCell143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable30
            // 
            this.xrTable30.Location = new System.Drawing.Point(25, 1021);
            this.xrTable30.Name = "xrTable30";
            this.xrTable30.ParentStyleUsing.UseBorders = false;
            this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35});
            this.xrTable30.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell140,
            this.xrTableCell141});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell140.CanGrow = false;
            this.xrTableCell140.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell140.Multiline = true;
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell140.ParentStyleUsing.UseBorders = false;
            this.xrTableCell140.Size = new System.Drawing.Size(145, 23);
            this.xrTableCell140.Text = "- Vân tải đơn";
            this.xrTableCell140.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell141.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell141.Multiline = true;
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell141.ParentStyleUsing.UseFont = false;
            this.xrTableCell141.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell141.Text = ":         ......................             ...................";
            this.xrTableCell141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable29
            // 
            this.xrTable29.Location = new System.Drawing.Point(25, 999);
            this.xrTable29.Name = "xrTable29";
            this.xrTable29.ParentStyleUsing.UseBorders = false;
            this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
            this.xrTable29.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell138,
            this.xrTableCell139});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell138.CanGrow = false;
            this.xrTableCell138.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell138.Multiline = true;
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell138.ParentStyleUsing.UseBorders = false;
            this.xrTableCell138.Size = new System.Drawing.Size(145, 23);
            this.xrTableCell138.Text = "- Bản kê chi tiết";
            this.xrTableCell138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell139.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell139.Multiline = true;
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell139.ParentStyleUsing.UseFont = false;
            this.xrTableCell139.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell139.Text = ":         ......................             ...................";
            this.xrTableCell139.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable28
            // 
            this.xrTable28.Location = new System.Drawing.Point(25, 978);
            this.xrTable28.Name = "xrTable28";
            this.xrTable28.ParentStyleUsing.UseBorders = false;
            this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
            this.xrTable28.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell136,
            this.xrTableCell137});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell136.CanGrow = false;
            this.xrTableCell136.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell136.Multiline = true;
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell136.ParentStyleUsing.UseBorders = false;
            this.xrTableCell136.Size = new System.Drawing.Size(145, 23);
            this.xrTableCell136.Text = "- Hóa đơn thương mại";
            this.xrTableCell136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell137.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell137.Multiline = true;
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell137.ParentStyleUsing.UseFont = false;
            this.xrTableCell137.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell137.Text = ":         ......................             ...................";
            this.xrTableCell137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable27
            // 
            this.xrTable27.Location = new System.Drawing.Point(25, 956);
            this.xrTable27.Name = "xrTable27";
            this.xrTable27.ParentStyleUsing.UseBorders = false;
            this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32});
            this.xrTable27.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell132,
            this.xrTableCell135});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell132.CanGrow = false;
            this.xrTableCell132.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell132.Multiline = true;
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell132.ParentStyleUsing.UseBorders = false;
            this.xrTableCell132.Size = new System.Drawing.Size(145, 23);
            this.xrTableCell132.Text = "- Hợp đồng thương mại";
            this.xrTableCell132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell135.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell135.Multiline = true;
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell135.ParentStyleUsing.UseFont = false;
            this.xrTableCell135.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell135.Text = ":         ......................             ...................";
            this.xrTableCell135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable26
            // 
            this.xrTable26.Location = new System.Drawing.Point(25, 930);
            this.xrTable26.Name = "xrTable26";
            this.xrTable26.ParentStyleUsing.UseBorders = false;
            this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTable26.Size = new System.Drawing.Size(384, 27);
            this.xrTable26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell131,
            this.xrTableCell134});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Size = new System.Drawing.Size(384, 27);
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell131.CanGrow = false;
            this.xrTableCell131.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell131.Multiline = true;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell131.ParentStyleUsing.UseBorders = false;
            this.xrTableCell131.Size = new System.Drawing.Size(142, 27);
            this.xrTableCell131.Text = "19. Chứng từ kèm:                                                     \r\n         " +
                "                                              ";
            this.xrTableCell131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Location = new System.Drawing.Point(142, 0);
            this.xrTableCell134.Multiline = true;
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell134.Size = new System.Drawing.Size(242, 27);
            this.xrTableCell134.Text = "Bản chính                  Bản sao\r\n";
            this.xrTableCell134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable16.Location = new System.Drawing.Point(25, 533);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.ParentStyleUsing.UseBorders = false;
            this.xrTable16.ParentStyleUsing.UseFont = false;
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable16.Size = new System.Drawing.Size(767, 41);
            this.xrTable16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell88,
            this.xrTableCell80,
            this.xrTableCell89,
            this.xrTableCell83,
            this.xrTableCell91,
            this.xrTableCell90,
            this.xrTableCell92});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Size = new System.Drawing.Size(767, 41);
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell88.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell88.ParentStyleUsing.UseFont = false;
            this.xrTableCell88.Size = new System.Drawing.Size(25, 41);
            this.xrTableCell88.Text = "SỐ TT";
            this.xrTableCell88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell80.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell80.Multiline = true;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell80.ParentStyleUsing.UseFont = false;
            this.xrTableCell80.Size = new System.Drawing.Size(283, 41);
            this.xrTableCell80.Text = "13. TÊN HÀNG \r\n QUY CÁCH PHẨM CHẤT";
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell89.Location = new System.Drawing.Point(308, 0);
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell89.ParentStyleUsing.UseFont = false;
            this.xrTableCell89.Size = new System.Drawing.Size(84, 41);
            this.xrTableCell89.Text = "14. MÃ SỐ HÀNG HÓA";
            this.xrTableCell89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell83.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell83.Multiline = true;
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell83.ParentStyleUsing.UseFont = false;
            this.xrTableCell83.Size = new System.Drawing.Size(91, 41);
            this.xrTableCell83.Text = "15.LƯỢNG ";
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell91.Location = new System.Drawing.Point(483, 0);
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell91.ParentStyleUsing.UseFont = false;
            this.xrTableCell91.Size = new System.Drawing.Size(75, 41);
            this.xrTableCell91.Text = "16. ĐƠN VỊ TÍNH";
            this.xrTableCell91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell90.Location = new System.Drawing.Point(558, 0);
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell90.ParentStyleUsing.UseFont = false;
            this.xrTableCell90.Size = new System.Drawing.Size(114, 41);
            this.xrTableCell90.Text = "17. ĐƠN GIÁ NGUYÊN TỆ";
            this.xrTableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell92.Location = new System.Drawing.Point(672, 0);
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell92.ParentStyleUsing.UseFont = false;
            this.xrTableCell92.Size = new System.Drawing.Size(95, 41);
            this.xrTableCell92.Text = "18. TRỊ GIÁ NGUYÊN TỆ ";
            this.xrTableCell92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable14
            // 
            this.xrTable14.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable14.Location = new System.Drawing.Point(25, 442);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.ParentStyleUsing.UseBorders = false;
            this.xrTable14.ParentStyleUsing.UseFont = false;
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.xrTable14.Size = new System.Drawing.Size(767, 92);
            this.xrTable14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.xrTableCell82,
            this.xrTableCell87});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Size = new System.Drawing.Size(767, 92);
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell65.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable15,
            this.lblMaDaiLyTTHQ,
            this.xrLabel41});
            this.xrTableCell65.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell65.ParentStyleUsing.UseBorders = false;
            this.xrTableCell65.Size = new System.Drawing.Size(392, 92);
            // 
            // xrTable15
            // 
            this.xrTable15.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable15.Location = new System.Drawing.Point(133, 1);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.ParentStyleUsing.UseBorders = false;
            this.xrTable15.ParentStyleUsing.UseFont = false;
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.xrTable15.Size = new System.Drawing.Size(260, 25);
            this.xrTable15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Size = new System.Drawing.Size(260, 25);
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell66.ParentStyleUsing.UseBorders = false;
            this.xrTableCell66.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell66.Text = " ";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell67.ParentStyleUsing.UseBorders = false;
            this.xrTableCell67.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell67.Text = " ";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell68.ParentStyleUsing.UseBorders = false;
            this.xrTableCell68.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell68.Text = " ";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell69.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell69.ParentStyleUsing.UseBorders = false;
            this.xrTableCell69.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell69.Text = " ";
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell70.Location = new System.Drawing.Point(80, 0);
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell70.ParentStyleUsing.UseBorders = false;
            this.xrTableCell70.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell70.Text = " ";
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell71.Location = new System.Drawing.Point(100, 0);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell71.ParentStyleUsing.UseBorders = false;
            this.xrTableCell71.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell71.Text = " ";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell72.Location = new System.Drawing.Point(120, 0);
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell72.ParentStyleUsing.UseBorders = false;
            this.xrTableCell72.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell72.Text = " ";
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell73.Location = new System.Drawing.Point(140, 0);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell73.ParentStyleUsing.UseBorders = false;
            this.xrTableCell73.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell73.Text = " ";
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell74.Location = new System.Drawing.Point(160, 0);
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell74.ParentStyleUsing.UseBorders = false;
            this.xrTableCell74.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell74.Text = " ";
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell75.Location = new System.Drawing.Point(180, 0);
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell75.ParentStyleUsing.UseBorders = false;
            this.xrTableCell75.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell75.Text = " ";
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell76.Location = new System.Drawing.Point(200, 0);
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell76.ParentStyleUsing.UseBorders = false;
            this.xrTableCell76.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell76.Text = " ";
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell77.Location = new System.Drawing.Point(220, 0);
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell77.ParentStyleUsing.UseBorders = false;
            this.xrTableCell77.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell77.Text = " ";
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell78.Location = new System.Drawing.Point(240, 0);
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell78.ParentStyleUsing.UseBorders = false;
            this.xrTableCell78.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell78.Text = " ";
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDaiLyTTHQ
            // 
            this.lblMaDaiLyTTHQ.BorderWidth = 0;
            this.lblMaDaiLyTTHQ.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDaiLyTTHQ.Location = new System.Drawing.Point(133, 0);
            this.lblMaDaiLyTTHQ.Name = "lblMaDaiLyTTHQ";
            this.lblMaDaiLyTTHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaDaiLyTTHQ.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaDaiLyTTHQ.ParentStyleUsing.UseFont = false;
            this.lblMaDaiLyTTHQ.Size = new System.Drawing.Size(260, 24);
            this.lblMaDaiLyTTHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel41
            // 
            this.xrLabel41.BorderWidth = 0;
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.Location = new System.Drawing.Point(8, 0);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel41.ParentStyleUsing.UseFont = false;
            this.xrLabel41.Size = new System.Drawing.Size(117, 41);
            this.xrLabel41.Text = "4. Đại lý làm thủ tục hải quan";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell82.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgoaiTe,
            this.xrLabel39,
            this.lblTyGiaTT,
            this.xrTable17,
            this.xrLabel46});
            this.xrTableCell82.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell82.ParentStyleUsing.UseBorders = false;
            this.xrTableCell82.Size = new System.Drawing.Size(200, 92);
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgoaiTe
            // 
            this.lblNgoaiTe.BorderWidth = 0;
            this.lblNgoaiTe.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgoaiTe.Location = new System.Drawing.Point(108, 0);
            this.lblNgoaiTe.Multiline = true;
            this.lblNgoaiTe.Name = "lblNgoaiTe";
            this.lblNgoaiTe.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.lblNgoaiTe.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgoaiTe.ParentStyleUsing.UseFont = false;
            this.lblNgoaiTe.Size = new System.Drawing.Size(90, 25);
            this.lblNgoaiTe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel39
            // 
            this.xrLabel39.BorderWidth = 0;
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.Location = new System.Drawing.Point(5, 52);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel39.ParentStyleUsing.UseFont = false;
            this.xrLabel39.Size = new System.Drawing.Size(58, 33);
            this.xrLabel39.Text = "Tỷ giá tính thuế:";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTyGiaTT
            // 
            this.lblTyGiaTT.BorderWidth = 0;
            this.lblTyGiaTT.Location = new System.Drawing.Point(58, 50);
            this.lblTyGiaTT.Name = "lblTyGiaTT";
            this.lblTyGiaTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTyGiaTT.ParentStyleUsing.UseBorderWidth = false;
            this.lblTyGiaTT.Size = new System.Drawing.Size(125, 33);
            this.lblTyGiaTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable17
            // 
            this.xrTable17.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable17.Location = new System.Drawing.Point(110, 0);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.ParentStyleUsing.UseBorders = false;
            this.xrTable17.ParentStyleUsing.UseFont = false;
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTable17.Size = new System.Drawing.Size(90, 25);
            this.xrTable17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell84,
            this.xrTableCell85,
            this.xrTableCell86});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Size = new System.Drawing.Size(90, 25);
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell84.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell84.ParentStyleUsing.UseBorders = false;
            this.xrTableCell84.Size = new System.Drawing.Size(30, 25);
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell85.Location = new System.Drawing.Point(30, 0);
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell85.ParentStyleUsing.UseBorders = false;
            this.xrTableCell85.Size = new System.Drawing.Size(30, 25);
            this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell86.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell86.ParentStyleUsing.UseBorders = false;
            this.xrTableCell86.Size = new System.Drawing.Size(30, 25);
            // 
            // xrLabel46
            // 
            this.xrLabel46.BorderWidth = 0;
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel46.Location = new System.Drawing.Point(5, 0);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel46.ParentStyleUsing.UseFont = false;
            this.xrLabel46.Size = new System.Drawing.Size(73, 33);
            this.xrLabel46.Text = "11. Đồng tiền thanh toán";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell87.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblPTTT,
            this.xrLabel49});
            this.xrTableCell87.Location = new System.Drawing.Point(592, 0);
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell87.ParentStyleUsing.UseBorders = false;
            this.xrTableCell87.Size = new System.Drawing.Size(175, 92);
            this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblPTTT
            // 
            this.lblPTTT.BackColor = System.Drawing.Color.Ivory;
            this.lblPTTT.BorderWidth = 0;
            this.lblPTTT.Location = new System.Drawing.Point(8, 33);
            this.lblPTTT.Name = "lblPTTT";
            this.lblPTTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPTTT.ParentStyleUsing.UseBackColor = false;
            this.lblPTTT.ParentStyleUsing.UseBorderWidth = false;
            this.lblPTTT.Size = new System.Drawing.Size(158, 50);
            this.lblPTTT.Tag = "PTTT";
            this.lblPTTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblPTTT.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrLabel49
            // 
            this.xrLabel49.BorderWidth = 0;
            this.xrLabel49.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel49.Location = new System.Drawing.Point(5, 0);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel49.ParentStyleUsing.UseFont = false;
            this.xrLabel49.Size = new System.Drawing.Size(192, 25);
            this.xrLabel49.Text = "12. Phương thức thanh toán:";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable9
            // 
            this.xrTable9.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable9.Location = new System.Drawing.Point(25, 350);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.ParentStyleUsing.UseBorders = false;
            this.xrTable9.ParentStyleUsing.UseFont = false;
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable9.Size = new System.Drawing.Size(767, 94);
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell52,
            this.xrTableCell54});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Size = new System.Drawing.Size(767, 94);
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell38.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10,
            this.lblMaNguoiUyThac,
            this.lblNguoiUyThac,
            this.xrLabel36});
            this.xrTableCell38.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell38.ParentStyleUsing.UseBorders = false;
            this.xrTableCell38.Size = new System.Drawing.Size(392, 94);
            // 
            // xrTable10
            // 
            this.xrTable10.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable10.Location = new System.Drawing.Point(133, 0);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.ParentStyleUsing.UseBorders = false;
            this.xrTable10.ParentStyleUsing.UseFont = false;
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable10.Size = new System.Drawing.Size(260, 25);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Size = new System.Drawing.Size(260, 25);
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell39.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell39.ParentStyleUsing.UseBorders = false;
            this.xrTableCell39.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell39.Text = " ";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell40.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell40.ParentStyleUsing.UseBorders = false;
            this.xrTableCell40.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell40.Text = " ";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell41.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell41.ParentStyleUsing.UseBorders = false;
            this.xrTableCell41.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell41.Text = " ";
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell42.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell42.ParentStyleUsing.UseBorders = false;
            this.xrTableCell42.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell42.Text = " ";
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell43.Location = new System.Drawing.Point(80, 0);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell43.ParentStyleUsing.UseBorders = false;
            this.xrTableCell43.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell43.Text = " ";
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.Location = new System.Drawing.Point(100, 0);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell44.ParentStyleUsing.UseBorders = false;
            this.xrTableCell44.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell44.Text = " ";
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell45.Location = new System.Drawing.Point(120, 0);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell45.ParentStyleUsing.UseBorders = false;
            this.xrTableCell45.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell45.Text = " ";
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell46.Location = new System.Drawing.Point(140, 0);
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell46.ParentStyleUsing.UseBorders = false;
            this.xrTableCell46.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell46.Text = " ";
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell47.Location = new System.Drawing.Point(160, 0);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell47.ParentStyleUsing.UseBorders = false;
            this.xrTableCell47.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell47.Text = " ";
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell48.Location = new System.Drawing.Point(180, 0);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell48.ParentStyleUsing.UseBorders = false;
            this.xrTableCell48.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell48.Text = " ";
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell49.Location = new System.Drawing.Point(200, 0);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell49.ParentStyleUsing.UseBorders = false;
            this.xrTableCell49.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell49.Text = " ";
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell50.Location = new System.Drawing.Point(220, 0);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell50.ParentStyleUsing.UseBorders = false;
            this.xrTableCell50.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell50.Text = " ";
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell51.Location = new System.Drawing.Point(240, 0);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell51.ParentStyleUsing.UseBorders = false;
            this.xrTableCell51.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell51.Text = " ";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblMaNguoiUyThac
            // 
            this.lblMaNguoiUyThac.BorderWidth = 0;
            this.lblMaNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiUyThac.Location = new System.Drawing.Point(133, 0);
            this.lblMaNguoiUyThac.Name = "lblMaNguoiUyThac";
            this.lblMaNguoiUyThac.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaNguoiUyThac.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaNguoiUyThac.ParentStyleUsing.UseFont = false;
            this.lblMaNguoiUyThac.Size = new System.Drawing.Size(260, 24);
            this.lblMaNguoiUyThac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNguoiUyThac
            // 
            this.lblNguoiUyThac.BorderWidth = 0;
            this.lblNguoiUyThac.CanGrow = false;
            this.lblNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblNguoiUyThac.Location = new System.Drawing.Point(8, 33);
            this.lblNguoiUyThac.Name = "lblNguoiUyThac";
            this.lblNguoiUyThac.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNguoiUyThac.ParentStyleUsing.UseBorderWidth = false;
            this.lblNguoiUyThac.ParentStyleUsing.UseFont = false;
            this.lblNguoiUyThac.Size = new System.Drawing.Size(375, 58);
            this.lblNguoiUyThac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel36
            // 
            this.xrLabel36.BorderWidth = 0;
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.Location = new System.Drawing.Point(8, 0);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel36.ParentStyleUsing.UseFont = false;
            this.xrLabel36.Size = new System.Drawing.Size(102, 25);
            this.xrLabel36.Text = "3. Người ủy thác";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell52.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMaDiaDiemDoHang,
            this.xrTable13,
            this.xrLabel38,
            this.lblDiaDiemDoHang});
            this.xrTableCell52.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell52.ParentStyleUsing.UseBorders = false;
            this.xrTableCell52.Size = new System.Drawing.Size(200, 94);
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDiaDiemDoHang
            // 
            this.lblMaDiaDiemDoHang.BorderWidth = 0;
            this.lblMaDiaDiemDoHang.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDiaDiemDoHang.Location = new System.Drawing.Point(82, 67);
            this.lblMaDiaDiemDoHang.Multiline = true;
            this.lblMaDiaDiemDoHang.Name = "lblMaDiaDiemDoHang";
            this.lblMaDiaDiemDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.lblMaDiaDiemDoHang.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaDiaDiemDoHang.ParentStyleUsing.UseFont = false;
            this.lblMaDiaDiemDoHang.Size = new System.Drawing.Size(120, 25);
            this.lblMaDiaDiemDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable13
            // 
            this.xrTable13.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable13.Location = new System.Drawing.Point(80, 68);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.ParentStyleUsing.UseBorders = false;
            this.xrTable13.ParentStyleUsing.UseFont = false;
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTable13.Size = new System.Drawing.Size(120, 25);
            this.xrTable13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Size = new System.Drawing.Size(120, 25);
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell61.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell61.ParentStyleUsing.UseBorders = false;
            this.xrTableCell61.Size = new System.Drawing.Size(30, 25);
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell62.Location = new System.Drawing.Point(30, 0);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell62.ParentStyleUsing.UseBorders = false;
            this.xrTableCell62.Size = new System.Drawing.Size(30, 25);
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell63.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell63.ParentStyleUsing.UseBorders = false;
            this.xrTableCell63.Size = new System.Drawing.Size(30, 25);
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell64.Location = new System.Drawing.Point(90, 0);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell64.ParentStyleUsing.UseBorders = false;
            this.xrTableCell64.Size = new System.Drawing.Size(30, 25);
            // 
            // xrLabel38
            // 
            this.xrLabel38.BorderWidth = 0;
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.Location = new System.Drawing.Point(8, 0);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel38.ParentStyleUsing.UseFont = false;
            this.xrLabel38.Size = new System.Drawing.Size(150, 17);
            this.xrLabel38.Text = "9. Cửa khẩu xuất hàng";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblDiaDiemDoHang
            // 
            this.lblDiaDiemDoHang.BorderWidth = 0;
            this.lblDiaDiemDoHang.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaDiemDoHang.Location = new System.Drawing.Point(8, 25);
            this.lblDiaDiemDoHang.Multiline = true;
            this.lblDiaDiemDoHang.Name = "lblDiaDiemDoHang";
            this.lblDiaDiemDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaDiemDoHang.ParentStyleUsing.UseBorderWidth = false;
            this.lblDiaDiemDoHang.ParentStyleUsing.UseFont = false;
            this.lblDiaDiemDoHang.Size = new System.Drawing.Size(184, 35);
            this.lblDiaDiemDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell54.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblDKGH,
            this.xrLabel43});
            this.xrTableCell54.Location = new System.Drawing.Point(592, 0);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell54.ParentStyleUsing.UseBorders = false;
            this.xrTableCell54.Size = new System.Drawing.Size(175, 94);
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblDKGH
            // 
            this.lblDKGH.BackColor = System.Drawing.Color.Ivory;
            this.lblDKGH.BorderWidth = 0;
            this.lblDKGH.Location = new System.Drawing.Point(8, 33);
            this.lblDKGH.Name = "lblDKGH";
            this.lblDKGH.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDKGH.ParentStyleUsing.UseBackColor = false;
            this.lblDKGH.ParentStyleUsing.UseBorderWidth = false;
            this.lblDKGH.Size = new System.Drawing.Size(156, 50);
            this.lblDKGH.Tag = "DKGH";
            this.lblDKGH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDKGH.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrLabel43
            // 
            this.xrLabel43.BorderWidth = 0;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.Location = new System.Drawing.Point(8, 0);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel43.ParentStyleUsing.UseFont = false;
            this.xrLabel43.Size = new System.Drawing.Size(150, 25);
            this.xrLabel43.Text = "10. Điều kiện giao hàng:";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable7
            // 
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable7.Location = new System.Drawing.Point(25, 256);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.ParentStyleUsing.UseBorders = false;
            this.xrTable7.ParentStyleUsing.UseFont = false;
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable7.Size = new System.Drawing.Size(767, 94);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell6,
            this.xrTableCell7});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Size = new System.Drawing.Size(767, 94);
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8,
            this.lblTenDoiTac,
            this.xrLabel31});
            this.xrTableCell24.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell24.ParentStyleUsing.UseBorders = false;
            this.xrTableCell24.Size = new System.Drawing.Size(392, 94);
            // 
            // xrTable8
            // 
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable8.Location = new System.Drawing.Point(133, 0);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.ParentStyleUsing.UseBorders = false;
            this.xrTable8.ParentStyleUsing.UseFont = false;
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable8.Size = new System.Drawing.Size(260, 25);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Size = new System.Drawing.Size(260, 25);
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell25.ParentStyleUsing.UseBorders = false;
            this.xrTableCell25.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell26.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell26.ParentStyleUsing.UseBorders = false;
            this.xrTableCell26.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell27.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell27.ParentStyleUsing.UseBorders = false;
            this.xrTableCell27.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell28.ParentStyleUsing.UseBorders = false;
            this.xrTableCell28.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Location = new System.Drawing.Point(80, 0);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell29.ParentStyleUsing.UseBorders = false;
            this.xrTableCell29.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell29.Text = " ";
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Location = new System.Drawing.Point(100, 0);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell30.ParentStyleUsing.UseBorders = false;
            this.xrTableCell30.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell30.Text = " ";
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Location = new System.Drawing.Point(120, 0);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell31.ParentStyleUsing.UseBorders = false;
            this.xrTableCell31.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell31.Text = " ";
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell32.Location = new System.Drawing.Point(140, 0);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell32.ParentStyleUsing.UseBorders = false;
            this.xrTableCell32.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell32.Text = " ";
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell33.Location = new System.Drawing.Point(160, 0);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell33.ParentStyleUsing.UseBorders = false;
            this.xrTableCell33.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell33.Text = " ";
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Location = new System.Drawing.Point(180, 0);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell34.ParentStyleUsing.UseBorders = false;
            this.xrTableCell34.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell34.Text = " ";
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell35.Location = new System.Drawing.Point(200, 0);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell35.ParentStyleUsing.UseBorders = false;
            this.xrTableCell35.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell35.Text = " ";
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell36.Location = new System.Drawing.Point(220, 0);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell36.ParentStyleUsing.UseBorders = false;
            this.xrTableCell36.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell36.Text = " ";
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Location = new System.Drawing.Point(240, 0);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell37.ParentStyleUsing.UseBorders = false;
            this.xrTableCell37.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell37.Text = " ";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTenDoiTac
            // 
            this.lblTenDoiTac.BorderColor = System.Drawing.Color.Black;
            this.lblTenDoiTac.BorderWidth = 0;
            this.lblTenDoiTac.CanGrow = false;
            this.lblTenDoiTac.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoiTac.Location = new System.Drawing.Point(17, 33);
            this.lblTenDoiTac.Multiline = true;
            this.lblTenDoiTac.Name = "lblTenDoiTac";
            this.lblTenDoiTac.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoiTac.ParentStyleUsing.UseBorderColor = false;
            this.lblTenDoiTac.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenDoiTac.ParentStyleUsing.UseFont = false;
            this.lblTenDoiTac.Size = new System.Drawing.Size(366, 58);
            this.lblTenDoiTac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel31
            // 
            this.xrLabel31.BorderWidth = 0;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.Location = new System.Drawing.Point(8, 0);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel31.ParentStyleUsing.UseFont = false;
            this.xrLabel31.Size = new System.Drawing.Size(109, 25);
            this.xrLabel31.Text = "2. Người nhập khẩu";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgayHHHD,
            this.lblNgayHD,
            this.lblSoHD,
            this.xrLabel30});
            this.xrTableCell6.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.ParentStyleUsing.UseBorders = false;
            this.xrTableCell6.Size = new System.Drawing.Size(200, 94);
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHHHD
            // 
            this.lblNgayHHHD.BorderWidth = 0;
            this.lblNgayHHHD.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHHHD.Location = new System.Drawing.Point(6, 69);
            this.lblNgayHHHD.Name = "lblNgayHHHD";
            this.lblNgayHHHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHHHD.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHHHD.ParentStyleUsing.UseFont = false;
            this.lblNgayHHHD.Size = new System.Drawing.Size(184, 17);
            this.lblNgayHHHD.Text = "Ngày hết hạn:";
            this.lblNgayHHHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNgayHD
            // 
            this.lblNgayHD.BorderWidth = 0;
            this.lblNgayHD.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHD.Location = new System.Drawing.Point(8, 52);
            this.lblNgayHD.Name = "lblNgayHD";
            this.lblNgayHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHD.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHD.ParentStyleUsing.UseFont = false;
            this.lblNgayHD.Size = new System.Drawing.Size(184, 15);
            this.lblNgayHD.Text = "Ngày:";
            this.lblNgayHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoHD
            // 
            this.lblSoHD.BorderWidth = 0;
            this.lblSoHD.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHD.Location = new System.Drawing.Point(8, 19);
            this.lblSoHD.Multiline = true;
            this.lblSoHD.Name = "lblSoHD";
            this.lblSoHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoHD.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoHD.ParentStyleUsing.UseFont = false;
            this.lblSoHD.Size = new System.Drawing.Size(184, 33);
            this.lblSoHD.Text = "Số  :";
            this.lblSoHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.BorderWidth = 0;
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.Location = new System.Drawing.Point(8, 0);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel30.ParentStyleUsing.UseFont = false;
            this.xrLabel30.Size = new System.Drawing.Size(113, 17);
            this.xrLabel30.Text = "7. Hợp đồng:";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMaNuoc,
            this.lblTenNuoc,
            this.xrLabel35,
            this.xrTable11});
            this.xrTableCell7.Location = new System.Drawing.Point(592, 0);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.ParentStyleUsing.UseBorders = false;
            this.xrTableCell7.Size = new System.Drawing.Size(175, 94);
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaNuoc
            // 
            this.lblMaNuoc.BorderWidth = 0;
            this.lblMaNuoc.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNuoc.Location = new System.Drawing.Point(117, 69);
            this.lblMaNuoc.Name = "lblMaNuoc";
            this.lblMaNuoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.lblMaNuoc.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaNuoc.ParentStyleUsing.UseFont = false;
            this.lblMaNuoc.Size = new System.Drawing.Size(60, 25);
            this.lblMaNuoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTenNuoc
            // 
            this.lblTenNuoc.BorderWidth = 0;
            this.lblTenNuoc.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNuoc.Location = new System.Drawing.Point(8, 25);
            this.lblTenNuoc.Name = "lblTenNuoc";
            this.lblTenNuoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNuoc.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenNuoc.ParentStyleUsing.UseFont = false;
            this.lblTenNuoc.Size = new System.Drawing.Size(158, 35);
            // 
            // xrLabel35
            // 
            this.xrLabel35.BorderWidth = 0;
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.Location = new System.Drawing.Point(8, 0);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel35.ParentStyleUsing.UseFont = false;
            this.xrLabel35.Size = new System.Drawing.Size(120, 17);
            this.xrLabel35.Text = "8. Nước nhập khẩu:";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable11
            // 
            this.xrTable11.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable11.Location = new System.Drawing.Point(117, 67);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.ParentStyleUsing.UseBorders = false;
            this.xrTable11.ParentStyleUsing.UseFont = false;
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable11.Size = new System.Drawing.Size(59, 25);
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell56});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Size = new System.Drawing.Size(59, 25);
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell55.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell55.ParentStyleUsing.UseBorders = false;
            this.xrTableCell55.Size = new System.Drawing.Size(29, 25);
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell56.Location = new System.Drawing.Point(29, 0);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell56.ParentStyleUsing.UseBorders = false;
            this.xrTableCell56.Size = new System.Drawing.Size(30, 25);
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblBanLuuHaiQuan
            // 
            this.lblBanLuuHaiQuan.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBanLuuHaiQuan.Location = new System.Drawing.Point(316, 33);
            this.lblBanLuuHaiQuan.Name = "lblBanLuuHaiQuan";
            this.lblBanLuuHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanLuuHaiQuan.ParentStyleUsing.UseFont = false;
            this.lblBanLuuHaiQuan.Size = new System.Drawing.Size(216, 17);
            this.lblBanLuuHaiQuan.Text = "Bản lưu người khai Hải quan";
            this.lblBanLuuHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.Location = new System.Drawing.Point(718, 33);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.ParentStyleUsing.UseFont = false;
            this.xrLabel2.Size = new System.Drawing.Size(74, 17);
            this.xrLabel2.Text = "HQ/2002-XK";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.Location = new System.Drawing.Point(261, 8);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.ParentStyleUsing.UseFont = false;
            this.xrLabel1.Size = new System.Drawing.Size(317, 25);
            this.xrLabel1.Text = "TỜ KHAI HÀNG HÓA XUẤT KHẨU ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.Location = new System.Drawing.Point(25, 8);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.ParentStyleUsing.UseFont = false;
            this.xrLabel23.Size = new System.Drawing.Size(175, 25);
            this.xrLabel23.Text = "HẢI QUAN VIỆT NAM";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoBanChinh6
            // 
            this.lblSoBanChinh6.Location = new System.Drawing.Point(208, 1069);
            this.lblSoBanChinh6.Name = "lblSoBanChinh6";
            this.lblSoBanChinh6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh6.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao6
            // 
            this.lblSoBanSao6.Location = new System.Drawing.Point(317, 1069);
            this.lblSoBanSao6.Name = "lblSoBanSao6";
            this.lblSoBanSao6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao6.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanChinh4
            // 
            this.lblSoBanChinh4.Location = new System.Drawing.Point(208, 1024);
            this.lblSoBanChinh4.Name = "lblSoBanChinh4";
            this.lblSoBanChinh4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh4.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanChinh3
            // 
            this.lblSoBanChinh3.Location = new System.Drawing.Point(208, 1002);
            this.lblSoBanChinh3.Name = "lblSoBanChinh3";
            this.lblSoBanChinh3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh3.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanChinh2
            // 
            this.lblSoBanChinh2.Location = new System.Drawing.Point(208, 981);
            this.lblSoBanChinh2.Name = "lblSoBanChinh2";
            this.lblSoBanChinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh2.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanChinh1
            // 
            this.lblSoBanChinh1.Location = new System.Drawing.Point(208, 959);
            this.lblSoBanChinh1.Name = "lblSoBanChinh1";
            this.lblSoBanChinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh1.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao1
            // 
            this.lblSoBanSao1.Location = new System.Drawing.Point(317, 959);
            this.lblSoBanSao1.Name = "lblSoBanSao1";
            this.lblSoBanSao1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao1.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao2
            // 
            this.lblSoBanSao2.Location = new System.Drawing.Point(317, 981);
            this.lblSoBanSao2.Name = "lblSoBanSao2";
            this.lblSoBanSao2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao2.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao3
            // 
            this.lblSoBanSao3.Location = new System.Drawing.Point(317, 1002);
            this.lblSoBanSao3.Name = "lblSoBanSao3";
            this.lblSoBanSao3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao3.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao4
            // 
            this.lblSoBanSao4.Location = new System.Drawing.Point(317, 1024);
            this.lblSoBanSao4.Name = "lblSoBanSao4";
            this.lblSoBanSao4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao4.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoTiepNhan
            // 
            this.lblSoTiepNhan.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblSoTiepNhan.Location = new System.Drawing.Point(650, 8);
            this.lblSoTiepNhan.Name = "lblSoTiepNhan";
            this.lblSoTiepNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTiepNhan.ParentStyleUsing.UseFont = false;
            this.lblSoTiepNhan.Size = new System.Drawing.Size(142, 25);
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.Location = new System.Drawing.Point(25, 50);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.ParentStyleUsing.UseBorders = false;
            this.xrTable4.ParentStyleUsing.UseFont = false;
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable4.Size = new System.Drawing.Size(767, 84);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.lblTrangthai});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Size = new System.Drawing.Size(767, 84);
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblChiCucHaiQuan,
            this.lblCucHaiQuan,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8});
            this.xrTableCell1.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.Size = new System.Drawing.Size(245, 84);
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblChiCucHaiQuan
            // 
            this.lblChiCucHaiQuan.BorderWidth = 0;
            this.lblChiCucHaiQuan.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChiCucHaiQuan.Location = new System.Drawing.Point(108, 58);
            this.lblChiCucHaiQuan.Multiline = true;
            this.lblChiCucHaiQuan.Name = "lblChiCucHaiQuan";
            this.lblChiCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHaiQuan.ParentStyleUsing.UseBorderWidth = false;
            this.lblChiCucHaiQuan.ParentStyleUsing.UseFont = false;
            this.lblChiCucHaiQuan.Size = new System.Drawing.Size(134, 16);
            this.lblChiCucHaiQuan.Text = "chi cuc";
            this.lblChiCucHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblCucHaiQuan
            // 
            this.lblCucHaiQuan.BorderWidth = 0;
            this.lblCucHaiQuan.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCucHaiQuan.Location = new System.Drawing.Point(92, 34);
            this.lblCucHaiQuan.Name = "lblCucHaiQuan";
            this.lblCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCucHaiQuan.ParentStyleUsing.UseBorderWidth = false;
            this.lblCucHaiQuan.ParentStyleUsing.UseFont = false;
            this.lblCucHaiQuan.Size = new System.Drawing.Size(150, 16);
            this.lblCucHaiQuan.Text = "cuc";
            this.lblCucHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.BorderWidth = 0;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.Location = new System.Drawing.Point(8, 58);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel10.ParentStyleUsing.UseFont = false;
            this.xrLabel10.Size = new System.Drawing.Size(100, 16);
            this.xrLabel10.Text = "Chi cục Hải quan :";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.BorderWidth = 0;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.Location = new System.Drawing.Point(9, 34);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel9.ParentStyleUsing.UseFont = false;
            this.xrLabel9.Size = new System.Drawing.Size(84, 16);
            this.xrLabel9.Text = "Cục Hải quan :";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.BorderWidth = 0;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.Location = new System.Drawing.Point(10, 10);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel8.ParentStyleUsing.UseFont = false;
            this.xrLabel8.Size = new System.Drawing.Size(150, 16);
            this.xrLabel8.Text = "TỔNG CỤC HẢI QUAN";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMaHaiQuan,
            this.xrLabel3,
            this.lblSoPLTK,
            this.lblNgayDangKy,
            this.lblSoPLTK1,
            this.lblNgayDangKy1,
            this.lblSoToKhai,
            this.xrLabel7});
            this.xrTableCell2.Location = new System.Drawing.Point(245, 0);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.Size = new System.Drawing.Size(260, 84);
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaHaiQuan
            // 
            this.lblMaHaiQuan.BackColor = System.Drawing.Color.Ivory;
            this.lblMaHaiQuan.BorderWidth = 0;
            this.lblMaHaiQuan.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblMaHaiQuan.Location = new System.Drawing.Point(200, 8);
            this.lblMaHaiQuan.Name = "lblMaHaiQuan";
            this.lblMaHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.lblMaHaiQuan.ParentStyleUsing.UseBackColor = false;
            this.lblMaHaiQuan.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaHaiQuan.ParentStyleUsing.UseFont = false;
            this.lblMaHaiQuan.Size = new System.Drawing.Size(42, 16);
            this.lblMaHaiQuan.Tag = "Hải quan";
            this.lblMaHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaHaiQuan.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrLabel3
            // 
            this.xrLabel3.BorderWidth = 0;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.Location = new System.Drawing.Point(8, 10);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel3.ParentStyleUsing.UseFont = false;
            this.xrLabel3.Size = new System.Drawing.Size(75, 16);
            this.xrLabel3.Text = "Tờ khai số : ";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoPLTK
            // 
            this.lblSoPLTK.BorderWidth = 0;
            this.lblSoPLTK.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoPLTK.Location = new System.Drawing.Point(155, 58);
            this.lblSoPLTK.Name = "lblSoPLTK";
            this.lblSoPLTK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoPLTK.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoPLTK.ParentStyleUsing.UseFont = false;
            this.lblSoPLTK.Size = new System.Drawing.Size(83, 16);
            this.lblSoPLTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.BorderWidth = 0;
            this.lblNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDangKy.Location = new System.Drawing.Point(100, 35);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayDangKy.ParentStyleUsing.UseFont = false;
            this.lblNgayDangKy.Size = new System.Drawing.Size(150, 16);
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoPLTK1
            // 
            this.lblSoPLTK1.BorderWidth = 0;
            this.lblSoPLTK1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoPLTK1.Location = new System.Drawing.Point(9, 59);
            this.lblSoPLTK1.Name = "lblSoPLTK1";
            this.lblSoPLTK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoPLTK1.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoPLTK1.ParentStyleUsing.UseFont = false;
            this.lblSoPLTK1.Size = new System.Drawing.Size(150, 16);
            this.lblSoPLTK1.Text = "Số lượng phụ lục tờ khai :";
            this.lblSoPLTK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNgayDangKy1
            // 
            this.lblNgayDangKy1.BorderWidth = 0;
            this.lblNgayDangKy1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDangKy1.Location = new System.Drawing.Point(9, 36);
            this.lblNgayDangKy1.Name = "lblNgayDangKy1";
            this.lblNgayDangKy1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy1.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayDangKy1.ParentStyleUsing.UseFont = false;
            this.lblNgayDangKy1.Size = new System.Drawing.Size(92, 16);
            this.lblNgayDangKy1.Text = "Ngày đăng ký : ";
            this.lblNgayDangKy1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.BorderWidth = 0;
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoToKhai.Location = new System.Drawing.Point(92, 8);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoToKhai.ParentStyleUsing.UseFont = false;
            this.lblSoToKhai.Size = new System.Drawing.Size(49, 16);
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.BorderWidth = 0;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.Location = new System.Drawing.Point(92, 11);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel7.ParentStyleUsing.UseFont = false;
            this.xrLabel7.Size = new System.Drawing.Size(117, 17);
            this.xrLabel7.Text = ".................../XK/GC/";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTrangthai
            // 
            this.lblTrangthai.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel6,
            this.xrLabel5});
            this.lblTrangthai.Location = new System.Drawing.Point(505, 0);
            this.lblTrangthai.Name = "lblTrangthai";
            this.lblTrangthai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrangthai.Size = new System.Drawing.Size(262, 84);
            this.lblTrangthai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.BorderWidth = 0;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.Location = new System.Drawing.Point(95, 12);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel6.ParentStyleUsing.UseFont = false;
            this.xrLabel6.Size = new System.Drawing.Size(92, 17);
            this.xrLabel6.Text = "(Ký, ghi rõ họ tên)";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.BorderWidth = 0;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.Location = new System.Drawing.Point(9, 12);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel5.ParentStyleUsing.UseFont = false;
            this.xrLabel5.Size = new System.Drawing.Size(83, 17);
            this.xrLabel5.Text = "Cán bộ đăng ký";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable41
            // 
            this.xrTable41.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable41.Location = new System.Drawing.Point(25, 1260);
            this.xrTable41.Name = "xrTable41";
            this.xrTable41.ParentStyleUsing.UseBorders = false;
            this.xrTable41.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow54});
            this.xrTable41.Size = new System.Drawing.Size(767, 23);
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell161});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Size = new System.Drawing.Size(767, 23);
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell161.CanGrow = false;
            this.xrTableCell161.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell161.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell161.ParentStyleUsing.UseBorders = false;
            this.xrTableCell161.ParentStyleUsing.UseFont = false;
            this.xrTableCell161.Size = new System.Drawing.Size(767, 23);
            this.xrTableCell161.Text = "Hình thức kiểm tra : ";
            this.xrTableCell161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable43
            // 
            this.xrTable43.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable43.Location = new System.Drawing.Point(25, 1308);
            this.xrTable43.Name = "xrTable43";
            this.xrTable43.ParentStyleUsing.UseBorders = false;
            this.xrTable43.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow56});
            this.xrTable43.Size = new System.Drawing.Size(767, 566);
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell163});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Size = new System.Drawing.Size(767, 566);
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell163.CanGrow = false;
            this.xrTableCell163.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell163.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 2, 0, 100F);
            this.xrTableCell163.ParentStyleUsing.UseBorders = false;
            this.xrTableCell163.ParentStyleUsing.UseFont = false;
            this.xrTableCell163.Size = new System.Drawing.Size(767, 566);
            this.xrTableCell163.Text = "Kết quả kiểm tra: ";
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.Location = new System.Drawing.Point(25, 162);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.ParentStyleUsing.UseBorders = false;
            this.xrTable5.ParentStyleUsing.UseFont = false;
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable5.Size = new System.Drawing.Size(767, 94);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell4});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Size = new System.Drawing.Size(767, 94);
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTenDoanhNghiep1,
            this.lblMaDoanhNghiep1,
            this.xrTable6,
            this.xrLabel11});
            this.xrTableCell8.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.ParentStyleUsing.UseBorders = false;
            this.xrTableCell8.Size = new System.Drawing.Size(392, 94);
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTenDoanhNghiep1
            // 
            this.lblTenDoanhNghiep1.BorderWidth = 0;
            this.lblTenDoanhNghiep1.CanGrow = false;
            this.lblTenDoanhNghiep1.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoanhNghiep1.Location = new System.Drawing.Point(8, 33);
            this.lblTenDoanhNghiep1.Multiline = true;
            this.lblTenDoanhNghiep1.Name = "lblTenDoanhNghiep1";
            this.lblTenDoanhNghiep1.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoanhNghiep1.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenDoanhNghiep1.ParentStyleUsing.UseFont = false;
            this.lblTenDoanhNghiep1.Size = new System.Drawing.Size(384, 58);
            this.lblTenDoanhNghiep1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDoanhNghiep1
            // 
            this.lblMaDoanhNghiep1.BorderWidth = 0;
            this.lblMaDoanhNghiep1.CanGrow = false;
            this.lblMaDoanhNghiep1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDoanhNghiep1.Location = new System.Drawing.Point(135, 0);
            this.lblMaDoanhNghiep1.Name = "lblMaDoanhNghiep1";
            this.lblMaDoanhNghiep1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.lblMaDoanhNghiep1.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaDoanhNghiep1.ParentStyleUsing.UseFont = false;
            this.lblMaDoanhNghiep1.Size = new System.Drawing.Size(259, 26);
            this.lblMaDoanhNghiep1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable6
            // 
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable6.Location = new System.Drawing.Point(133, 0);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.ParentStyleUsing.UseBorders = false;
            this.xrTable6.ParentStyleUsing.UseFont = false;
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable6.Size = new System.Drawing.Size(260, 25);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell12,
            this.xrTableCell17,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell14,
            this.xrTableCell18,
            this.xrTableCell20,
            this.xrTableCell19,
            this.xrTableCell22,
            this.xrTableCell21,
            this.xrTableCell5,
            this.xrTableCell13});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Size = new System.Drawing.Size(260, 25);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.ParentStyleUsing.UseBorders = false;
            this.xrTableCell10.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell12.ParentStyleUsing.UseBorders = false;
            this.xrTableCell12.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell17.ParentStyleUsing.UseBorders = false;
            this.xrTableCell17.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell15.ParentStyleUsing.UseBorders = false;
            this.xrTableCell15.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.Location = new System.Drawing.Point(80, 0);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell16.ParentStyleUsing.UseBorders = false;
            this.xrTableCell16.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.Location = new System.Drawing.Point(100, 0);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell14.ParentStyleUsing.UseBorders = false;
            this.xrTableCell14.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell18.Location = new System.Drawing.Point(120, 0);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell18.ParentStyleUsing.UseBorders = false;
            this.xrTableCell18.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.Location = new System.Drawing.Point(140, 0);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell20.ParentStyleUsing.UseBorders = false;
            this.xrTableCell20.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Location = new System.Drawing.Point(160, 0);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell19.ParentStyleUsing.UseBorders = false;
            this.xrTableCell19.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell22.Location = new System.Drawing.Point(180, 0);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell22.ParentStyleUsing.UseBorders = false;
            this.xrTableCell22.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell21.Location = new System.Drawing.Point(200, 0);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell21.ParentStyleUsing.UseBorders = false;
            this.xrTableCell21.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Location = new System.Drawing.Point(220, 0);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.ParentStyleUsing.UseBorders = false;
            this.xrTableCell5.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Location = new System.Drawing.Point(240, 0);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell13.ParentStyleUsing.UseBorders = false;
            this.xrTableCell13.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel11
            // 
            this.xrLabel11.BorderWidth = 0;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.Location = new System.Drawing.Point(6, 0);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel11.ParentStyleUsing.UseFont = false;
            this.xrLabel11.Size = new System.Drawing.Size(117, 26);
            this.xrLabel11.Text = "1. Người xuất khẩu";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel37,
            this.xrLabel34,
            this.xrKhongThue,
            this.xrLabel32,
            this.xrLabel4,
            this.xrLabel16,
            this.xrGC,
            this.xrLabel18,
            this.xrTNK,
            this.xrCoThue,
            this.xrLabel19,
            this.xrTN,
            this.xrSXXK,
            this.xrLabel17,
            this.xrLabel15,
            this.xrLabel14,
            this.xrKD,
            this.xrLabel13});
            this.xrTableCell9.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell9.ParentStyleUsing.UseBorders = false;
            this.xrTableCell9.Size = new System.Drawing.Size(200, 94);
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel37.CanGrow = false;
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.Location = new System.Drawing.Point(8, 75);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel37.ParentStyleUsing.UseBorders = false;
            this.xrLabel37.ParentStyleUsing.UseFont = false;
            this.xrLabel37.Size = new System.Drawing.Size(13, 13);
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel34
            // 
            this.xrLabel34.BorderWidth = 0;
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.Location = new System.Drawing.Point(108, 26);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel34.ParentStyleUsing.UseFont = false;
            this.xrLabel34.Size = new System.Drawing.Size(67, 9);
            this.xrLabel34.Text = "Không thuế";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrKhongThue
            // 
            this.xrKhongThue.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrKhongThue.CanGrow = false;
            this.xrKhongThue.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrKhongThue.Location = new System.Drawing.Point(92, 25);
            this.xrKhongThue.Name = "xrKhongThue";
            this.xrKhongThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrKhongThue.ParentStyleUsing.UseBorders = false;
            this.xrKhongThue.ParentStyleUsing.UseFont = false;
            this.xrKhongThue.Size = new System.Drawing.Size(13, 13);
            this.xrKhongThue.Text = "X";
            this.xrKhongThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel32
            // 
            this.xrLabel32.BorderWidth = 0;
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.Location = new System.Drawing.Point(25, 26);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel32.ParentStyleUsing.UseFont = false;
            this.xrLabel32.Size = new System.Drawing.Size(50, 10);
            this.xrLabel32.Text = "Có thuế";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel4.CanGrow = false;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.Location = new System.Drawing.Point(67, 58);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel4.ParentStyleUsing.UseBorders = false;
            this.xrLabel4.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel4.ParentStyleUsing.UseFont = false;
            this.xrLabel4.Size = new System.Drawing.Size(13, 13);
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.BorderWidth = 0;
            this.xrLabel16.CanGrow = false;
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.Location = new System.Drawing.Point(25, 58);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel16.ParentStyleUsing.UseFont = false;
            this.xrLabel16.Size = new System.Drawing.Size(25, 11);
            this.xrLabel16.Text = "GC";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrGC
            // 
            this.xrGC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrGC.CanGrow = false;
            this.xrGC.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrGC.Location = new System.Drawing.Point(125, 42);
            this.xrGC.Name = "xrGC";
            this.xrGC.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrGC.ParentStyleUsing.UseBorders = false;
            this.xrGC.ParentStyleUsing.UseFont = false;
            this.xrGC.Size = new System.Drawing.Size(13, 13);
            this.xrGC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BorderWidth = 0;
            this.xrLabel18.CanGrow = false;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.Location = new System.Drawing.Point(142, 42);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel18.ParentStyleUsing.UseFont = false;
            this.xrLabel18.Size = new System.Drawing.Size(28, 11);
            this.xrLabel18.Text = "XTN";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTNK
            // 
            this.xrTNK.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTNK.CanGrow = false;
            this.xrTNK.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTNK.Location = new System.Drawing.Point(67, 42);
            this.xrTNK.Name = "xrTNK";
            this.xrTNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTNK.ParentStyleUsing.UseBorders = false;
            this.xrTNK.ParentStyleUsing.UseFont = false;
            this.xrTNK.Size = new System.Drawing.Size(13, 13);
            this.xrTNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrCoThue
            // 
            this.xrCoThue.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrCoThue.CanGrow = false;
            this.xrCoThue.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCoThue.Location = new System.Drawing.Point(8, 25);
            this.xrCoThue.Name = "xrCoThue";
            this.xrCoThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrCoThue.ParentStyleUsing.UseBorders = false;
            this.xrCoThue.ParentStyleUsing.UseFont = false;
            this.xrCoThue.Size = new System.Drawing.Size(13, 13);
            this.xrCoThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel19
            // 
            this.xrLabel19.BorderWidth = 0;
            this.xrLabel19.CanGrow = false;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.Location = new System.Drawing.Point(142, 58);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel19.ParentStyleUsing.UseFont = false;
            this.xrLabel19.Size = new System.Drawing.Size(25, 11);
            this.xrLabel19.Text = "TX";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTN
            // 
            this.xrTN.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTN.CanGrow = false;
            this.xrTN.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTN.Location = new System.Drawing.Point(125, 58);
            this.xrTN.Name = "xrTN";
            this.xrTN.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTN.ParentStyleUsing.UseBorders = false;
            this.xrTN.ParentStyleUsing.UseFont = false;
            this.xrTN.Size = new System.Drawing.Size(13, 13);
            this.xrTN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrSXXK
            // 
            this.xrSXXK.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrSXXK.CanGrow = false;
            this.xrSXXK.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSXXK.Location = new System.Drawing.Point(8, 58);
            this.xrSXXK.Name = "xrSXXK";
            this.xrSXXK.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrSXXK.ParentStyleUsing.UseBorders = false;
            this.xrSXXK.ParentStyleUsing.UseFont = false;
            this.xrSXXK.Size = new System.Drawing.Size(13, 13);
            this.xrSXXK.Text = "X";
            this.xrSXXK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel17
            // 
            this.xrLabel17.BorderWidth = 0;
            this.xrLabel17.CanGrow = false;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.Location = new System.Drawing.Point(83, 58);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel17.ParentStyleUsing.UseFont = false;
            this.xrLabel17.Size = new System.Drawing.Size(41, 11);
            this.xrLabel17.Text = "SXXK";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.BorderWidth = 0;
            this.xrLabel15.CanGrow = false;
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.Location = new System.Drawing.Point(83, 42);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel15.ParentStyleUsing.UseFont = false;
            this.xrLabel15.Size = new System.Drawing.Size(25, 11);
            this.xrLabel15.Text = "ĐT";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.BorderWidth = 0;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.Location = new System.Drawing.Point(25, 42);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel14.ParentStyleUsing.UseFont = false;
            this.xrLabel14.Size = new System.Drawing.Size(25, 11);
            this.xrLabel14.Text = "KD";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrKD
            // 
            this.xrKD.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrKD.CanGrow = false;
            this.xrKD.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrKD.Location = new System.Drawing.Point(8, 42);
            this.xrKD.Name = "xrKD";
            this.xrKD.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrKD.ParentStyleUsing.UseBorders = false;
            this.xrKD.ParentStyleUsing.UseBorderWidth = false;
            this.xrKD.ParentStyleUsing.UseFont = false;
            this.xrKD.Size = new System.Drawing.Size(13, 13);
            this.xrKD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel13
            // 
            this.xrLabel13.BorderWidth = 0;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.Location = new System.Drawing.Point(8, 0);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel13.ParentStyleUsing.UseFont = false;
            this.xrLabel13.Size = new System.Drawing.Size(102, 17);
            this.xrLabel13.Text = "5. Loại hình:";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgayHHGP,
            this.lblNgayGP,
            this.lblSoGP,
            this.xrLabel28});
            this.xrTableCell4.Location = new System.Drawing.Point(592, 0);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.ParentStyleUsing.UseBorders = false;
            this.xrTableCell4.Size = new System.Drawing.Size(175, 94);
            // 
            // lblNgayHHGP
            // 
            this.lblNgayHHGP.BorderWidth = 0;
            this.lblNgayHHGP.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHHGP.Location = new System.Drawing.Point(8, 75);
            this.lblNgayHHGP.Name = "lblNgayHHGP";
            this.lblNgayHHGP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHHGP.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHHGP.ParentStyleUsing.UseFont = false;
            this.lblNgayHHGP.Size = new System.Drawing.Size(165, 17);
            this.lblNgayHHGP.Text = "Ngày hết hạn:";
            this.lblNgayHHGP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNgayGP
            // 
            this.lblNgayGP.BorderWidth = 0;
            this.lblNgayGP.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayGP.Location = new System.Drawing.Point(8, 55);
            this.lblNgayGP.Name = "lblNgayGP";
            this.lblNgayGP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayGP.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayGP.ParentStyleUsing.UseFont = false;
            this.lblNgayGP.Size = new System.Drawing.Size(165, 15);
            this.lblNgayGP.Text = "Ngày: ";
            this.lblNgayGP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoGP
            // 
            this.lblSoGP.BorderWidth = 0;
            this.lblSoGP.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoGP.Location = new System.Drawing.Point(8, 26);
            this.lblSoGP.Multiline = true;
            this.lblSoGP.Name = "lblSoGP";
            this.lblSoGP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoGP.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoGP.ParentStyleUsing.UseFont = false;
            this.lblSoGP.Size = new System.Drawing.Size(158, 27);
            this.lblSoGP.Text = "Số  : \r\n";
            this.lblSoGP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.BorderWidth = 0;
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.Location = new System.Drawing.Point(5, 0);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel28.ParentStyleUsing.UseFont = false;
            this.xrLabel28.Size = new System.Drawing.Size(184, 17);
            this.xrLabel28.Text = "6. Giấy phép(nếu có)";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoKienTrongLuong
            // 
            this.lblSoKienTrongLuong.Location = new System.Drawing.Point(25, 900);
            this.lblSoKienTrongLuong.Name = "lblSoKienTrongLuong";
            this.lblSoKienTrongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoKienTrongLuong.Size = new System.Drawing.Size(558, 33);
            this.lblSoKienTrongLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable1.Location = new System.Drawing.Point(25, 574);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.ParentStyleUsing.UseBorderColor = false;
            this.xrTable1.ParentStyleUsing.UseBorders = false;
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow4});
            this.xrTable1.Size = new System.Drawing.Size(767, 358);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.TenHang1,
            this.MaHS1,
            this.Luong1,
            this.DVT1,
            this.DonGiaNT1,
            this.TriGiaNT1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(767, 36);
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell93.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell93.ParentStyleUsing.UseBorders = false;
            this.xrTableCell93.Size = new System.Drawing.Size(25, 36);
            this.xrTableCell93.Text = "1";
            this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang1
            // 
            this.TenHang1.BackColor = System.Drawing.Color.Ivory;
            this.TenHang1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang1.CanGrow = false;
            this.TenHang1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TenHang1.Location = new System.Drawing.Point(25, 0);
            this.TenHang1.Multiline = true;
            this.TenHang1.Name = "TenHang1";
            this.TenHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.TenHang1.ParentStyleUsing.UseBackColor = false;
            this.TenHang1.ParentStyleUsing.UseBorders = false;
            this.TenHang1.ParentStyleUsing.UseFont = false;
            this.TenHang1.Size = new System.Drawing.Size(283, 36);
            this.TenHang1.Tag = "Tên hàng 1";
            this.TenHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS1
            // 
            this.MaHS1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MaHS1.Location = new System.Drawing.Point(308, 0);
            this.MaHS1.Name = "MaHS1";
            this.MaHS1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS1.ParentStyleUsing.UseBorders = false;
            this.MaHS1.ParentStyleUsing.UseFont = false;
            this.MaHS1.Size = new System.Drawing.Size(84, 36);
            this.MaHS1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong1
            // 
            this.Luong1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.Luong1.Location = new System.Drawing.Point(392, 0);
            this.Luong1.Multiline = true;
            this.Luong1.Name = "Luong1";
            this.Luong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong1.ParentStyleUsing.UseBorders = false;
            this.Luong1.ParentStyleUsing.UseFont = false;
            this.Luong1.Size = new System.Drawing.Size(91, 36);
            this.Luong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT1
            // 
            this.DVT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DVT1.Location = new System.Drawing.Point(483, 0);
            this.DVT1.Multiline = true;
            this.DVT1.Name = "DVT1";
            this.DVT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT1.ParentStyleUsing.UseBorders = false;
            this.DVT1.ParentStyleUsing.UseFont = false;
            this.DVT1.Size = new System.Drawing.Size(75, 36);
            this.DVT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT1
            // 
            this.DonGiaNT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DonGiaNT1.Location = new System.Drawing.Point(558, 0);
            this.DonGiaNT1.Name = "DonGiaNT1";
            this.DonGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT1.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT1.ParentStyleUsing.UseFont = false;
            this.DonGiaNT1.Size = new System.Drawing.Size(114, 36);
            this.DonGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT1
            // 
            this.TriGiaNT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TriGiaNT1.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT1.Name = "TriGiaNT1";
            this.TriGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT1.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT1.ParentStyleUsing.UseFont = false;
            this.TriGiaNT1.Size = new System.Drawing.Size(95, 36);
            this.TriGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell94,
            this.TenHang2,
            this.MaHS2,
            this.Luong2,
            this.DVT2,
            this.DonGiaNT2,
            this.TriGiaNT2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Size = new System.Drawing.Size(767, 36);
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell94.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell94.ParentStyleUsing.UseBorders = false;
            this.xrTableCell94.Size = new System.Drawing.Size(25, 36);
            this.xrTableCell94.Text = "2";
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang2
            // 
            this.TenHang2.BackColor = System.Drawing.Color.Ivory;
            this.TenHang2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang2.CanGrow = false;
            this.TenHang2.Location = new System.Drawing.Point(25, 0);
            this.TenHang2.Name = "TenHang2";
            this.TenHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.TenHang2.ParentStyleUsing.UseBackColor = false;
            this.TenHang2.ParentStyleUsing.UseBorders = false;
            this.TenHang2.Size = new System.Drawing.Size(283, 36);
            this.TenHang2.Tag = "Tên hàng 2";
            this.TenHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang2.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS2
            // 
            this.MaHS2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MaHS2.Location = new System.Drawing.Point(308, 0);
            this.MaHS2.Name = "MaHS2";
            this.MaHS2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS2.ParentStyleUsing.UseBorders = false;
            this.MaHS2.ParentStyleUsing.UseFont = false;
            this.MaHS2.Size = new System.Drawing.Size(84, 36);
            this.MaHS2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong2
            // 
            this.Luong2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.Luong2.Location = new System.Drawing.Point(392, 0);
            this.Luong2.Multiline = true;
            this.Luong2.Name = "Luong2";
            this.Luong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong2.ParentStyleUsing.UseBorders = false;
            this.Luong2.ParentStyleUsing.UseFont = false;
            this.Luong2.Size = new System.Drawing.Size(91, 36);
            this.Luong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT2
            // 
            this.DVT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DVT2.Location = new System.Drawing.Point(483, 0);
            this.DVT2.Multiline = true;
            this.DVT2.Name = "DVT2";
            this.DVT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT2.ParentStyleUsing.UseBorders = false;
            this.DVT2.ParentStyleUsing.UseFont = false;
            this.DVT2.Size = new System.Drawing.Size(75, 36);
            this.DVT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT2
            // 
            this.DonGiaNT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DonGiaNT2.Location = new System.Drawing.Point(558, 0);
            this.DonGiaNT2.Name = "DonGiaNT2";
            this.DonGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT2.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT2.ParentStyleUsing.UseFont = false;
            this.DonGiaNT2.Size = new System.Drawing.Size(114, 36);
            this.DonGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT2
            // 
            this.TriGiaNT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TriGiaNT2.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT2.Name = "TriGiaNT2";
            this.TriGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT2.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT2.ParentStyleUsing.UseFont = false;
            this.TriGiaNT2.Size = new System.Drawing.Size(95, 36);
            this.TriGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell95,
            this.TenHang3,
            this.MaHS3,
            this.Luong3,
            this.DVT3,
            this.DonGiaNT3,
            this.TriGiaNT3});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Size = new System.Drawing.Size(767, 36);
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell95.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell95.ParentStyleUsing.UseBorders = false;
            this.xrTableCell95.Size = new System.Drawing.Size(25, 36);
            this.xrTableCell95.Text = "3";
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang3
            // 
            this.TenHang3.BackColor = System.Drawing.Color.Ivory;
            this.TenHang3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang3.CanGrow = false;
            this.TenHang3.Location = new System.Drawing.Point(25, 0);
            this.TenHang3.Name = "TenHang3";
            this.TenHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.TenHang3.ParentStyleUsing.UseBackColor = false;
            this.TenHang3.ParentStyleUsing.UseBorders = false;
            this.TenHang3.Size = new System.Drawing.Size(283, 36);
            this.TenHang3.Tag = "Tên hàng 3";
            this.TenHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS3
            // 
            this.MaHS3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MaHS3.Location = new System.Drawing.Point(308, 0);
            this.MaHS3.Name = "MaHS3";
            this.MaHS3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS3.ParentStyleUsing.UseBorders = false;
            this.MaHS3.ParentStyleUsing.UseFont = false;
            this.MaHS3.Size = new System.Drawing.Size(84, 36);
            this.MaHS3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong3
            // 
            this.Luong3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.Luong3.Location = new System.Drawing.Point(392, 0);
            this.Luong3.Multiline = true;
            this.Luong3.Name = "Luong3";
            this.Luong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong3.ParentStyleUsing.UseBorders = false;
            this.Luong3.ParentStyleUsing.UseFont = false;
            this.Luong3.Size = new System.Drawing.Size(91, 36);
            this.Luong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT3
            // 
            this.DVT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DVT3.Location = new System.Drawing.Point(483, 0);
            this.DVT3.Multiline = true;
            this.DVT3.Name = "DVT3";
            this.DVT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT3.ParentStyleUsing.UseBorders = false;
            this.DVT3.ParentStyleUsing.UseFont = false;
            this.DVT3.Size = new System.Drawing.Size(75, 36);
            this.DVT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT3
            // 
            this.DonGiaNT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DonGiaNT3.Location = new System.Drawing.Point(558, 0);
            this.DonGiaNT3.Name = "DonGiaNT3";
            this.DonGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT3.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT3.ParentStyleUsing.UseFont = false;
            this.DonGiaNT3.Size = new System.Drawing.Size(114, 36);
            this.DonGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT3
            // 
            this.TriGiaNT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TriGiaNT3.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT3.Name = "TriGiaNT3";
            this.TriGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT3.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT3.ParentStyleUsing.UseFont = false;
            this.TriGiaNT3.Size = new System.Drawing.Size(95, 36);
            this.TriGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104,
            this.TenHang4,
            this.MaHS4,
            this.Luong4,
            this.DVT4,
            this.DonGiaNT4,
            this.TriGiaNT4});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Size = new System.Drawing.Size(767, 36);
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell104.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell104.ParentStyleUsing.UseBorders = false;
            this.xrTableCell104.Size = new System.Drawing.Size(25, 36);
            this.xrTableCell104.Text = "4";
            this.xrTableCell104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang4
            // 
            this.TenHang4.BackColor = System.Drawing.Color.Ivory;
            this.TenHang4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang4.Location = new System.Drawing.Point(25, 0);
            this.TenHang4.Name = "TenHang4";
            this.TenHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang4.ParentStyleUsing.UseBackColor = false;
            this.TenHang4.ParentStyleUsing.UseBorders = false;
            this.TenHang4.Size = new System.Drawing.Size(283, 36);
            this.TenHang4.Tag = "Tên hàng 4";
            this.TenHang4.Text = " ";
            this.TenHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang4.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS4
            // 
            this.MaHS4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MaHS4.Location = new System.Drawing.Point(308, 0);
            this.MaHS4.Name = "MaHS4";
            this.MaHS4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS4.ParentStyleUsing.UseBorders = false;
            this.MaHS4.ParentStyleUsing.UseFont = false;
            this.MaHS4.Size = new System.Drawing.Size(84, 36);
            this.MaHS4.Text = " ";
            this.MaHS4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong4
            // 
            this.Luong4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.Luong4.Location = new System.Drawing.Point(392, 0);
            this.Luong4.Multiline = true;
            this.Luong4.Name = "Luong4";
            this.Luong4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong4.ParentStyleUsing.UseBorders = false;
            this.Luong4.ParentStyleUsing.UseFont = false;
            this.Luong4.Size = new System.Drawing.Size(91, 36);
            this.Luong4.Text = " ";
            this.Luong4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT4
            // 
            this.DVT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DVT4.Location = new System.Drawing.Point(483, 0);
            this.DVT4.Multiline = true;
            this.DVT4.Name = "DVT4";
            this.DVT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT4.ParentStyleUsing.UseBorders = false;
            this.DVT4.ParentStyleUsing.UseFont = false;
            this.DVT4.Size = new System.Drawing.Size(75, 36);
            this.DVT4.Text = " ";
            this.DVT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT4
            // 
            this.DonGiaNT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DonGiaNT4.Location = new System.Drawing.Point(558, 0);
            this.DonGiaNT4.Name = "DonGiaNT4";
            this.DonGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT4.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT4.ParentStyleUsing.UseFont = false;
            this.DonGiaNT4.Size = new System.Drawing.Size(114, 36);
            this.DonGiaNT4.Text = " ";
            this.DonGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT4
            // 
            this.TriGiaNT4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TriGiaNT4.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT4.Name = "TriGiaNT4";
            this.TriGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT4.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT4.ParentStyleUsing.UseFont = false;
            this.TriGiaNT4.Size = new System.Drawing.Size(95, 36);
            this.TriGiaNT4.Text = " ";
            this.TriGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell112,
            this.TenHang5,
            this.MaHS5,
            this.Luong5,
            this.DVT5,
            this.DonGiaNT5,
            this.TriGiaNT5});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Size = new System.Drawing.Size(767, 36);
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell112.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell112.ParentStyleUsing.UseBorders = false;
            this.xrTableCell112.Size = new System.Drawing.Size(25, 36);
            this.xrTableCell112.Text = "5";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang5
            // 
            this.TenHang5.BackColor = System.Drawing.Color.Ivory;
            this.TenHang5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang5.Location = new System.Drawing.Point(25, 0);
            this.TenHang5.Name = "TenHang5";
            this.TenHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang5.ParentStyleUsing.UseBackColor = false;
            this.TenHang5.ParentStyleUsing.UseBorders = false;
            this.TenHang5.Size = new System.Drawing.Size(283, 36);
            this.TenHang5.Tag = "Tên hàng 5";
            this.TenHang5.Text = " ";
            this.TenHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang5.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS5
            // 
            this.MaHS5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS5.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MaHS5.Location = new System.Drawing.Point(308, 0);
            this.MaHS5.Name = "MaHS5";
            this.MaHS5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS5.ParentStyleUsing.UseBorders = false;
            this.MaHS5.ParentStyleUsing.UseFont = false;
            this.MaHS5.Size = new System.Drawing.Size(84, 36);
            this.MaHS5.Text = " ";
            this.MaHS5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong5
            // 
            this.Luong5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong5.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.Luong5.Location = new System.Drawing.Point(392, 0);
            this.Luong5.Multiline = true;
            this.Luong5.Name = "Luong5";
            this.Luong5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong5.ParentStyleUsing.UseBorders = false;
            this.Luong5.ParentStyleUsing.UseFont = false;
            this.Luong5.Size = new System.Drawing.Size(91, 36);
            this.Luong5.Text = " ";
            this.Luong5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT5
            // 
            this.DVT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT5.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DVT5.Location = new System.Drawing.Point(483, 0);
            this.DVT5.Multiline = true;
            this.DVT5.Name = "DVT5";
            this.DVT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT5.ParentStyleUsing.UseBorders = false;
            this.DVT5.ParentStyleUsing.UseFont = false;
            this.DVT5.Size = new System.Drawing.Size(75, 36);
            this.DVT5.Text = " ";
            this.DVT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT5
            // 
            this.DonGiaNT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT5.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DonGiaNT5.Location = new System.Drawing.Point(558, 0);
            this.DonGiaNT5.Name = "DonGiaNT5";
            this.DonGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT5.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT5.ParentStyleUsing.UseFont = false;
            this.DonGiaNT5.Size = new System.Drawing.Size(114, 36);
            this.DonGiaNT5.Text = " ";
            this.DonGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT5
            // 
            this.TriGiaNT5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT5.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TriGiaNT5.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT5.Name = "TriGiaNT5";
            this.TriGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT5.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT5.ParentStyleUsing.UseFont = false;
            this.TriGiaNT5.Size = new System.Drawing.Size(95, 36);
            this.TriGiaNT5.Text = " ";
            this.TriGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell120,
            this.TenHang6,
            this.MaHS6,
            this.Luong6,
            this.DVT6,
            this.DonGiaNT6,
            this.TriGiaNT6});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Size = new System.Drawing.Size(767, 36);
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell120.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell120.ParentStyleUsing.UseBorders = false;
            this.xrTableCell120.Size = new System.Drawing.Size(25, 36);
            this.xrTableCell120.Text = "6";
            this.xrTableCell120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang6
            // 
            this.TenHang6.BackColor = System.Drawing.Color.Ivory;
            this.TenHang6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang6.Location = new System.Drawing.Point(25, 0);
            this.TenHang6.Name = "TenHang6";
            this.TenHang6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang6.ParentStyleUsing.UseBackColor = false;
            this.TenHang6.ParentStyleUsing.UseBorders = false;
            this.TenHang6.Size = new System.Drawing.Size(283, 36);
            this.TenHang6.Tag = "Tên hàng 6";
            this.TenHang6.Text = " ";
            this.TenHang6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang6.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS6
            // 
            this.MaHS6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MaHS6.Location = new System.Drawing.Point(308, 0);
            this.MaHS6.Name = "MaHS6";
            this.MaHS6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS6.ParentStyleUsing.UseBorders = false;
            this.MaHS6.ParentStyleUsing.UseFont = false;
            this.MaHS6.Size = new System.Drawing.Size(84, 36);
            this.MaHS6.Text = " ";
            this.MaHS6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong6
            // 
            this.Luong6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.Luong6.Location = new System.Drawing.Point(392, 0);
            this.Luong6.Multiline = true;
            this.Luong6.Name = "Luong6";
            this.Luong6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong6.ParentStyleUsing.UseBorders = false;
            this.Luong6.ParentStyleUsing.UseFont = false;
            this.Luong6.Size = new System.Drawing.Size(91, 36);
            this.Luong6.Text = " ";
            this.Luong6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT6
            // 
            this.DVT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DVT6.Location = new System.Drawing.Point(483, 0);
            this.DVT6.Multiline = true;
            this.DVT6.Name = "DVT6";
            this.DVT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT6.ParentStyleUsing.UseBorders = false;
            this.DVT6.ParentStyleUsing.UseFont = false;
            this.DVT6.Size = new System.Drawing.Size(75, 36);
            this.DVT6.Text = " ";
            this.DVT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT6
            // 
            this.DonGiaNT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DonGiaNT6.Location = new System.Drawing.Point(558, 0);
            this.DonGiaNT6.Name = "DonGiaNT6";
            this.DonGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT6.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT6.ParentStyleUsing.UseFont = false;
            this.DonGiaNT6.Size = new System.Drawing.Size(114, 36);
            this.DonGiaNT6.Text = " ";
            this.DonGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT6
            // 
            this.TriGiaNT6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TriGiaNT6.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT6.Name = "TriGiaNT6";
            this.TriGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT6.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT6.ParentStyleUsing.UseFont = false;
            this.TriGiaNT6.Size = new System.Drawing.Size(95, 36);
            this.TriGiaNT6.Text = " ";
            this.TriGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell128,
            this.TenHang7,
            this.MaHS7,
            this.Luong7,
            this.DVT7,
            this.DonGiaNT7,
            this.TriGiaNT7});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Size = new System.Drawing.Size(767, 36);
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell128.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell128.ParentStyleUsing.UseBorders = false;
            this.xrTableCell128.Size = new System.Drawing.Size(25, 36);
            this.xrTableCell128.Text = "7";
            this.xrTableCell128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang7
            // 
            this.TenHang7.BackColor = System.Drawing.Color.Ivory;
            this.TenHang7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang7.Location = new System.Drawing.Point(25, 0);
            this.TenHang7.Name = "TenHang7";
            this.TenHang7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang7.ParentStyleUsing.UseBackColor = false;
            this.TenHang7.ParentStyleUsing.UseBorders = false;
            this.TenHang7.Size = new System.Drawing.Size(283, 36);
            this.TenHang7.Tag = "Tên hàng 7";
            this.TenHang7.Text = " ";
            this.TenHang7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang7.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS7
            // 
            this.MaHS7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS7.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MaHS7.Location = new System.Drawing.Point(308, 0);
            this.MaHS7.Name = "MaHS7";
            this.MaHS7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS7.ParentStyleUsing.UseBorders = false;
            this.MaHS7.ParentStyleUsing.UseFont = false;
            this.MaHS7.Size = new System.Drawing.Size(84, 36);
            this.MaHS7.Text = " ";
            this.MaHS7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong7
            // 
            this.Luong7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong7.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.Luong7.Location = new System.Drawing.Point(392, 0);
            this.Luong7.Multiline = true;
            this.Luong7.Name = "Luong7";
            this.Luong7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong7.ParentStyleUsing.UseBorders = false;
            this.Luong7.ParentStyleUsing.UseFont = false;
            this.Luong7.Size = new System.Drawing.Size(91, 36);
            this.Luong7.Text = " ";
            this.Luong7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT7
            // 
            this.DVT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT7.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DVT7.Location = new System.Drawing.Point(483, 0);
            this.DVT7.Multiline = true;
            this.DVT7.Name = "DVT7";
            this.DVT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT7.ParentStyleUsing.UseBorders = false;
            this.DVT7.ParentStyleUsing.UseFont = false;
            this.DVT7.Size = new System.Drawing.Size(75, 36);
            this.DVT7.Text = " ";
            this.DVT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT7
            // 
            this.DonGiaNT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT7.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DonGiaNT7.Location = new System.Drawing.Point(558, 0);
            this.DonGiaNT7.Name = "DonGiaNT7";
            this.DonGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT7.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT7.ParentStyleUsing.UseFont = false;
            this.DonGiaNT7.Size = new System.Drawing.Size(114, 36);
            this.DonGiaNT7.Text = " ";
            this.DonGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT7
            // 
            this.TriGiaNT7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT7.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TriGiaNT7.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT7.Name = "TriGiaNT7";
            this.TriGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT7.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT7.ParentStyleUsing.UseFont = false;
            this.TriGiaNT7.Size = new System.Drawing.Size(95, 36);
            this.TriGiaNT7.Text = " ";
            this.TriGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell226,
            this.TenHang8,
            this.MaHS8,
            this.Luong8,
            this.DVT8,
            this.DonGiaNT8,
            this.TriGiaNT8});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Size = new System.Drawing.Size(767, 36);
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell226.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell226.ParentStyleUsing.UseBorders = false;
            this.xrTableCell226.Size = new System.Drawing.Size(25, 36);
            this.xrTableCell226.Text = "8";
            this.xrTableCell226.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang8
            // 
            this.TenHang8.BackColor = System.Drawing.Color.Ivory;
            this.TenHang8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang8.Location = new System.Drawing.Point(25, 0);
            this.TenHang8.Name = "TenHang8";
            this.TenHang8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang8.ParentStyleUsing.UseBackColor = false;
            this.TenHang8.ParentStyleUsing.UseBorders = false;
            this.TenHang8.Size = new System.Drawing.Size(283, 36);
            this.TenHang8.Tag = "Tên hàng 8";
            this.TenHang8.Text = " ";
            this.TenHang8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang8.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS8
            // 
            this.MaHS8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MaHS8.Location = new System.Drawing.Point(308, 0);
            this.MaHS8.Name = "MaHS8";
            this.MaHS8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS8.ParentStyleUsing.UseBorders = false;
            this.MaHS8.ParentStyleUsing.UseFont = false;
            this.MaHS8.Size = new System.Drawing.Size(84, 36);
            this.MaHS8.Text = " ";
            this.MaHS8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong8
            // 
            this.Luong8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.Luong8.Location = new System.Drawing.Point(392, 0);
            this.Luong8.Multiline = true;
            this.Luong8.Name = "Luong8";
            this.Luong8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong8.ParentStyleUsing.UseBorders = false;
            this.Luong8.ParentStyleUsing.UseFont = false;
            this.Luong8.Size = new System.Drawing.Size(91, 36);
            this.Luong8.Text = " ";
            this.Luong8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT8
            // 
            this.DVT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DVT8.Location = new System.Drawing.Point(483, 0);
            this.DVT8.Multiline = true;
            this.DVT8.Name = "DVT8";
            this.DVT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT8.ParentStyleUsing.UseBorders = false;
            this.DVT8.ParentStyleUsing.UseFont = false;
            this.DVT8.Size = new System.Drawing.Size(75, 36);
            this.DVT8.Text = " ";
            this.DVT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT8
            // 
            this.DonGiaNT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DonGiaNT8.Location = new System.Drawing.Point(558, 0);
            this.DonGiaNT8.Name = "DonGiaNT8";
            this.DonGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT8.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT8.ParentStyleUsing.UseFont = false;
            this.DonGiaNT8.Size = new System.Drawing.Size(114, 36);
            this.DonGiaNT8.Text = " ";
            this.DonGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT8
            // 
            this.TriGiaNT8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TriGiaNT8.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT8.Name = "TriGiaNT8";
            this.TriGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT8.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT8.ParentStyleUsing.UseFont = false;
            this.TriGiaNT8.Size = new System.Drawing.Size(95, 36);
            this.TriGiaNT8.Text = " ";
            this.TriGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell234,
            this.TenHang9,
            this.MaHS9,
            this.Luong9,
            this.DVT9,
            this.DonGiaNT9,
            this.TriGiaNT9});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Size = new System.Drawing.Size(767, 36);
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell234.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell234.ParentStyleUsing.UseBorders = false;
            this.xrTableCell234.Size = new System.Drawing.Size(25, 36);
            this.xrTableCell234.Text = "9";
            this.xrTableCell234.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang9
            // 
            this.TenHang9.BackColor = System.Drawing.Color.Ivory;
            this.TenHang9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang9.Location = new System.Drawing.Point(25, 0);
            this.TenHang9.Name = "TenHang9";
            this.TenHang9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang9.ParentStyleUsing.UseBackColor = false;
            this.TenHang9.ParentStyleUsing.UseBorders = false;
            this.TenHang9.Size = new System.Drawing.Size(283, 36);
            this.TenHang9.Tag = "Tên hàng 9";
            this.TenHang9.Text = " ";
            this.TenHang9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang9.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS9
            // 
            this.MaHS9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS9.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.MaHS9.Location = new System.Drawing.Point(308, 0);
            this.MaHS9.Name = "MaHS9";
            this.MaHS9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS9.ParentStyleUsing.UseBorders = false;
            this.MaHS9.ParentStyleUsing.UseFont = false;
            this.MaHS9.Size = new System.Drawing.Size(84, 36);
            this.MaHS9.Text = " ";
            this.MaHS9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong9
            // 
            this.Luong9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong9.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.Luong9.Location = new System.Drawing.Point(392, 0);
            this.Luong9.Multiline = true;
            this.Luong9.Name = "Luong9";
            this.Luong9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong9.ParentStyleUsing.UseBorders = false;
            this.Luong9.ParentStyleUsing.UseFont = false;
            this.Luong9.Size = new System.Drawing.Size(91, 36);
            this.Luong9.Text = " ";
            this.Luong9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT9
            // 
            this.DVT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT9.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DVT9.Location = new System.Drawing.Point(483, 0);
            this.DVT9.Multiline = true;
            this.DVT9.Name = "DVT9";
            this.DVT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT9.ParentStyleUsing.UseBorders = false;
            this.DVT9.ParentStyleUsing.UseFont = false;
            this.DVT9.Size = new System.Drawing.Size(75, 36);
            this.DVT9.Text = " ";
            this.DVT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT9
            // 
            this.DonGiaNT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT9.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.DonGiaNT9.Location = new System.Drawing.Point(558, 0);
            this.DonGiaNT9.Name = "DonGiaNT9";
            this.DonGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT9.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT9.ParentStyleUsing.UseFont = false;
            this.DonGiaNT9.Size = new System.Drawing.Size(114, 36);
            this.DonGiaNT9.Text = " ";
            this.DonGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT9
            // 
            this.TriGiaNT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT9.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TriGiaNT9.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT9.Name = "TriGiaNT9";
            this.TriGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT9.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT9.ParentStyleUsing.UseFont = false;
            this.TriGiaNT9.Size = new System.Drawing.Size(95, 36);
            this.TriGiaNT9.Text = " ";
            this.TriGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.xrTableCell23,
            this.xrTableCell53,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60,
            this.lblTongTriGiaNT});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Size = new System.Drawing.Size(767, 34);
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell11.ParentStyleUsing.UseBorders = false;
            this.xrTableCell11.Size = new System.Drawing.Size(25, 34);
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell23.ParentStyleUsing.UseBackColor = false;
            this.xrTableCell23.Size = new System.Drawing.Size(283, 34);
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell53.Location = new System.Drawing.Point(308, 0);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell53.ParentStyleUsing.UseFont = false;
            this.xrTableCell53.Size = new System.Drawing.Size(84, 34);
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell57.Size = new System.Drawing.Size(66, 34);
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Location = new System.Drawing.Point(458, 0);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell58.Size = new System.Drawing.Size(67, 34);
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Location = new System.Drawing.Point(525, 0);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell59.Size = new System.Drawing.Size(67, 34);
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell60.Location = new System.Drawing.Point(592, 0);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell60.ParentStyleUsing.UseBorders = false;
            this.xrTableCell60.Size = new System.Drawing.Size(80, 34);
            this.xrTableCell60.Text = "Cộng: ";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTongTriGiaNT
            // 
            this.lblTongTriGiaNT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTriGiaNT.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTongTriGiaNT.Location = new System.Drawing.Point(672, 0);
            this.lblTongTriGiaNT.Name = "lblTongTriGiaNT";
            this.lblTongTriGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaNT.ParentStyleUsing.UseBorders = false;
            this.lblTongTriGiaNT.ParentStyleUsing.UseFont = false;
            this.lblTongTriGiaNT.Size = new System.Drawing.Size(95, 34);
            this.lblTongTriGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblSoHopDong
            // 
            this.lblSoHopDong.CanGrow = false;
            this.lblSoHopDong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoHopDong.Location = new System.Drawing.Point(186, 18);
            this.lblSoHopDong.Multiline = true;
            this.lblSoHopDong.Name = "lblSoHopDong";
            this.lblSoHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoHopDong.ParentStyleUsing.UseFont = false;
            this.lblSoHopDong.Size = new System.Drawing.Size(100, 33);
            this.lblSoHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHHHopDong
            // 
            this.lblNgayHHHopDong.Location = new System.Drawing.Point(186, 58);
            this.lblNgayHHHopDong.Name = "lblNgayHHHopDong";
            this.lblNgayHHHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayHHHopDong.Size = new System.Drawing.Size(66, 17);
            this.lblNgayHHHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayHopDong
            // 
            this.lblNgayHopDong.Location = new System.Drawing.Point(183, 42);
            this.lblNgayHopDong.Name = "lblNgayHopDong";
            this.lblNgayHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayHopDong.Size = new System.Drawing.Size(83, 16);
            // 
            // lblSoGiayPhep
            // 
            this.lblSoGiayPhep.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoGiayPhep.Location = new System.Drawing.Point(3, 33);
            this.lblSoGiayPhep.Name = "lblSoGiayPhep";
            this.lblSoGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoGiayPhep.ParentStyleUsing.UseFont = false;
            this.lblSoGiayPhep.Size = new System.Drawing.Size(91, 33);
            // 
            // lblNgayGiayPhep
            // 
            this.lblNgayGiayPhep.Location = new System.Drawing.Point(95, 33);
            this.lblNgayGiayPhep.Name = "lblNgayGiayPhep";
            this.lblNgayGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayGiayPhep.Size = new System.Drawing.Size(91, 17);
            // 
            // lblNgayHHGiayPhep
            // 
            this.lblNgayHHGiayPhep.Location = new System.Drawing.Point(103, 58);
            this.lblNgayHHGiayPhep.Name = "lblNgayHHGiayPhep";
            this.lblNgayHHGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayHHGiayPhep.Size = new System.Drawing.Size(75, 17);
            this.lblNgayHHGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTenDoanhNghiep
            // 
            this.lblTenDoanhNghiep.BorderWidth = 0;
            this.lblTenDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoanhNghiep.Location = new System.Drawing.Point(17, 30);
            this.lblTenDoanhNghiep.Multiline = true;
            this.lblTenDoanhNghiep.Name = "lblTenDoanhNghiep";
            this.lblTenDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoanhNghiep.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblTenDoanhNghiep.Size = new System.Drawing.Size(353, 58);
            this.lblTenDoanhNghiep.Text = "CÔNG TY CỔ PHẦN DỆT MAY 19/3\r\n60 MẸ NHU THANH KHÊ ĐÀ NẴNG\r\nMID CODE: VNMARTEX478D" +
                "AN";
            this.lblTenDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.BorderWidth = 0;
            this.lblMaDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDoanhNghiep.Location = new System.Drawing.Point(111, 2);
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaDoanhNghiep.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblMaDoanhNghiep.Size = new System.Drawing.Size(260, 24);
            // 
            // xrLabel12
            // 
            this.xrLabel12.BorderWidth = 0;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.Location = new System.Drawing.Point(8, 0);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel12.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel12.ParentStyleUsing.UseFont = false;
            this.xrLabel12.Size = new System.Drawing.Size(102, 17);
            this.xrLabel12.Text = "5. Loại hình: ";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ToKhaiXuatA4
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.Margins = new System.Drawing.Printing.Margins(5, 15, 12, 8);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            ((System.ComponentModel.ISupportInitialize)(this.xrTable58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRLabel lblCucHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy1;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblSoPLTK1;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiUyThac;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoiTac;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblTenDaiLyTTHQ;
        private DevExpress.XtraReports.UI.XRLabel lblSoHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblSoGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblDKGH;
        private DevExpress.XtraReports.UI.XRLabel lblPTTT;
        private DevExpress.XtraReports.UI.XRLabel lblTyGiaTT;
        private DevExpress.XtraReports.UI.XRLabel lblMaDaiLyTTHQ;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiUyThac;
        private DevExpress.XtraReports.UI.XRLabel lblMaHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell TenHang1;
        private DevExpress.XtraReports.UI.XRTableCell MaHS1;
        private DevExpress.XtraReports.UI.XRTableCell Luong1;
        private DevExpress.XtraReports.UI.XRTableCell DVT1;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell TenHang2;
        private DevExpress.XtraReports.UI.XRTableCell MaHS2;
        private DevExpress.XtraReports.UI.XRTableCell Luong2;
        private DevExpress.XtraReports.UI.XRTableCell DVT2;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell TenHang3;
        private DevExpress.XtraReports.UI.XRTableCell MaHS3;
        private DevExpress.XtraReports.UI.XRTableCell Luong3;
        private DevExpress.XtraReports.UI.XRTableCell DVT3;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT3;
        private DevExpress.XtraReports.UI.XRLabel lblSoTiepNhan;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh6;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao6;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao5;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh5;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh4;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh3;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh2;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh1;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao1;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao2;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao3;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblBanLuuHaiQuan;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell lblTrangthai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHGP;
        private DevExpress.XtraReports.UI.XRLabel lblNgayGP;
        private DevExpress.XtraReports.UI.XRLabel lblSoGP;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTable xrTable26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTable xrTable28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTable xrTable27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTable xrTable32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell lblTenChungTu6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTable xrTable31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblTenChungTu5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTable xrTable30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTable xrTable29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTable xrTable33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTable xrTable34;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTable xrTable35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTable xrTable36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiep1;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiep1;
        private DevExpress.XtraReports.UI.XRTable xrTable38;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTable xrTable37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTable xrTable44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTable xrTable43;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTable xrTable42;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTable xrTable41;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRTable xrTable40;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRTable xrTable39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTable xrTable46;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;

        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell TenHang4;
        private DevExpress.XtraReports.UI.XRTableCell MaHS4;
        private DevExpress.XtraReports.UI.XRTableCell Luong4;
        private DevExpress.XtraReports.UI.XRTableCell DVT4;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT4;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell TenHang5;
        private DevExpress.XtraReports.UI.XRTableCell MaHS5;
        private DevExpress.XtraReports.UI.XRTableCell Luong5;
        private DevExpress.XtraReports.UI.XRTableCell DVT5;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT5;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell TenHang6;
        private DevExpress.XtraReports.UI.XRTableCell MaHS6;
        private DevExpress.XtraReports.UI.XRTableCell Luong6;
        private DevExpress.XtraReports.UI.XRTableCell DVT6;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT6;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell TenHang7;
        private DevExpress.XtraReports.UI.XRTableCell MaHS7;
        private DevExpress.XtraReports.UI.XRTableCell Luong7;
        private DevExpress.XtraReports.UI.XRTableCell DVT7;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT7;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell TenHang8;
        private DevExpress.XtraReports.UI.XRTableCell MaHS8;
        private DevExpress.XtraReports.UI.XRTableCell Luong8;
        private DevExpress.XtraReports.UI.XRTableCell DVT8;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT8;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell TenHang9;
        private DevExpress.XtraReports.UI.XRTableCell MaHS9;
        private DevExpress.XtraReports.UI.XRTableCell Luong9;
        private DevExpress.XtraReports.UI.XRTableCell DVT9;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT9;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT9;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblSoPLTK;
        private DevExpress.XtraReports.UI.XRTable xrTable58;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTable xrTable57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTable xrTable55;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel lblSoHD;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHD;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHHD;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRLabel lblMaNuoc;
        private DevExpress.XtraReports.UI.XRLabel lblTenNuoc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRLabel lblMaDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaNT;
        private DevExpress.XtraReports.UI.XRLabel xrKD;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrSXXK;
        private DevExpress.XtraReports.UI.XRLabel xrTN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrCoThue;
        private DevExpress.XtraReports.UI.XRLabel xrTNK;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrGC;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrKhongThue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel lblNgoaiTe;
        private DevExpress.XtraReports.UI.XRLabel lblSoKienTrongLuong;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
    }
}
