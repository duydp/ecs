using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.ExportToExcel 
{
    public partial class ToKhaiMauDich:EntityBase
    {

        #region Select

        public DataSet SelectDynamic(string maHaiQuan, string maDoanhNghiep)
        {
            string spName = "SELECT * FROM t_SXXK_ToKhaiMauDich WHERE MaHaiQuan=@MaHaiQuan AND MaDoanhNghiep=@MaDoanhNghiep";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar , maDoanhNghiep);

            return this.db.ExecuteDataSet(dbCommand);
        }

        public DataSet SelectDynamic1(string maHaiQuan, string maDoanhNghiep)
        {
           // string spName = "SELECT * FROM t_SXXK_ToKhaiMauDich WHERE MaHaiQuan=@MaHaiQuan AND MaDoanhNghiep=@MaDoanhNghiep";
            string spName = " SELECT  t_SXXK_ToKhaiMauDich.* ,  LanDieuChinh,SoChungTu ,  NGAY_HL , SNAHAN , THANH_LY " +
                
                                  " FROM t_SXXK_ThongTinDieuChinh   inner join   t_SXXK_ToKhaiMauDich " +
                                  " ON " +
                                  " t_SXXK_ToKhaiMauDich.SoToKhai = t_SXXK_ThongTinDieuChinh.SoToKhai " +
                                  " AND " +
                                  " t_SXXK_ToKhaiMauDich.MaLoaiHinh = t_SXXK_ThongTinDieuChinh.MaLoaiHinh " +
                                  " AND  " +
                                  " t_SXXK_ToKhaiMauDich.MaHaiQuan = t_SXXK_ThongTinDieuChinh.MaHaiQuan " +
                                  "  AND " +
                                  "  t_SXXK_ToKhaiMauDich.NamDangKy = t_SXXK_ThongTinDieuChinh.NamDangKy " +
                                  " WHERE t_SXXK_ToKhaiMauDich.MaHaiQuan = '" + maHaiQuan + "'" + " AND MaDoanhNghiep ='" + maDoanhNghiep + "'";

            
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);

            return this.db.ExecuteDataSet(dbCommand);
        }


       

        //---------------------------------------------------------------------------------------------

        

        #endregion

      
       
    }
}