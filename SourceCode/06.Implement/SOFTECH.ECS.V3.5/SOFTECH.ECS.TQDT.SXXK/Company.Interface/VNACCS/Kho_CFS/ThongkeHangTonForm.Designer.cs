﻿namespace Company.Interface.GC
{
    partial class ThongkeHangTonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongkeHangTonForm));
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListNPLTK_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout cbbToKhai_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListPhanBo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTKChuaPhanBo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgToKhaiDaPhanBo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uicmdThongke = new Janus.Windows.EditControls.UIButton();
            this.uicmdSelectTK = new Janus.Windows.EditControls.UIButton();
            this.uiTabSanPham = new Janus.Windows.UI.Tab.UITab();
            this.tabQLNPLTon = new Janus.Windows.UI.Tab.UITabPage();
            this.btnChinhDL = new Janus.Windows.EditControls.UIButton();
            this.btnXuatExcelNPLTon = new Janus.Windows.EditControls.UIButton();
            this.btnInNPLTon = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.filterEditor1 = new Janus.Windows.FilterEditor.FilterEditor();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.btnLayDuLieuCungUng = new Janus.Windows.EditControls.UIButton();
            this.btnChuyenTonSXXK = new Janus.Windows.EditControls.UIButton();
            this.uiButton4 = new Janus.Windows.EditControls.UIButton();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.ccTo = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label9 = new System.Windows.Forms.Label();
            this.ccFrom = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.uiButton3 = new Janus.Windows.EditControls.UIButton();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dgListNPLTK = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.btnXuatExcelNPLCungUng = new Janus.Windows.EditControls.UIButton();
            this.cmdXemBangNPLCungUng = new Janus.Windows.EditControls.UIButton();
            this.cmdViewBangPhanBoNPL = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbToKhai = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.label10 = new System.Windows.Forms.Label();
            this.filterEditor2 = new Janus.Windows.FilterEditor.FilterEditor();
            this.dgListPhanBo = new Janus.Windows.GridEX.GridEX();
            this.label1 = new System.Windows.Forms.Label();
            this.cbSanPham = new Janus.Windows.EditControls.UIComboBox();
            this.uiButton8 = new Janus.Windows.EditControls.UIButton();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.progressBarPhanBo = new System.Windows.Forms.ProgressBar();
            this.btnPhanBo = new Janus.Windows.EditControls.UIButton();
            this.dgListTKChuaPhanBo = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStripPhanBo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemPhanBo = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.progressBarDaPhanBo = new System.Windows.Forms.ProgressBar();
            this.label15 = new System.Windows.Forms.Label();
            this.btnDeletePhanBo = new Janus.Windows.EditControls.UIButton();
            this.dgToKhaiDaPhanBo = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStripDaPhanBo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemBangPhanBo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemBangNPLCungUng = new System.Windows.Forms.ToolStripMenuItem();
            this.xuatExcelNPLCungUngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.xemBangNplCungUngTKMuaVNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrDonViHaiQuan = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.ccToDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label7 = new System.Windows.Forms.Label();
            this.ccFromDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label6 = new System.Windows.Forms.Label();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnChonTK = new Janus.Windows.EditControls.UIButton();
            this.dgTKX = new Janus.Windows.GridEX.GridEX();
            this.gridEXPrintDocument1 = new Janus.Windows.GridEX.GridEXPrintDocument();
            this.gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.gridEXExporterNPL = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.gridEXPrintDocumentNPL = new Janus.Windows.GridEX.GridEXPrintDocument();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTabSanPham)).BeginInit();
            this.uiTabSanPham.SuspendLayout();
            this.tabQLNPLTon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLTK)).BeginInit();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToKhai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListPhanBo)).BeginInit();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKChuaPhanBo)).BeginInit();
            this.contextMenuStripPhanBo.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgToKhaiDaPhanBo)).BeginInit();
            this.contextMenuStripDaPhanBo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.grbMain.Controls.Add(this.uiTabSanPham);
            this.grbMain.Size = new System.Drawing.Size(829, 434);
            // 
            // uicmdThongke
            // 
            this.uicmdThongke.Location = new System.Drawing.Point(698, 27);
            this.uicmdThongke.Name = "uicmdThongke";
            this.uicmdThongke.Size = new System.Drawing.Size(84, 40);
            this.uicmdThongke.TabIndex = 196;
            this.uicmdThongke.Text = "Thống kê";
            this.uicmdThongke.VisualStyleManager = this.vsmMain;
            // 
            // uicmdSelectTK
            // 
            this.uicmdSelectTK.Location = new System.Drawing.Point(592, 27);
            this.uicmdSelectTK.Name = "uicmdSelectTK";
            this.uicmdSelectTK.Size = new System.Drawing.Size(84, 40);
            this.uicmdSelectTK.TabIndex = 195;
            this.uicmdSelectTK.Text = "Chọn tờ khai";
            this.uicmdSelectTK.VisualStyleManager = this.vsmMain;
            // 
            // uiTabSanPham
            // 
            this.uiTabSanPham.BackColor = System.Drawing.Color.Transparent;
            this.uiTabSanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTabSanPham.FlatBorderColor = System.Drawing.Color.Transparent;
            this.uiTabSanPham.Location = new System.Drawing.Point(0, 0);
            this.uiTabSanPham.Name = "uiTabSanPham";
            this.uiTabSanPham.Size = new System.Drawing.Size(829, 434);
            this.uiTabSanPham.TabIndex = 0;
            this.uiTabSanPham.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.tabQLNPLTon,
            this.uiTabPage2,
            this.uiTabPage3,
            this.uiTabPage4,
            this.uiTabPage1});
            this.uiTabSanPham.VisualStyleManager = this.vsmMain;
            this.uiTabSanPham.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.uiTabSanPham_SelectedTabChanged_1);
            // 
            // tabQLNPLTon
            // 
            this.tabQLNPLTon.Controls.Add(this.btnChinhDL);
            this.tabQLNPLTon.Controls.Add(this.btnXuatExcelNPLTon);
            this.tabQLNPLTon.Controls.Add(this.btnInNPLTon);
            this.tabQLNPLTon.Controls.Add(this.uiGroupBox5);
            this.tabQLNPLTon.Controls.Add(this.dgList);
            this.tabQLNPLTon.Key = "tpQLNPLTon";
            this.tabQLNPLTon.Location = new System.Drawing.Point(1, 21);
            this.tabQLNPLTon.Name = "tabQLNPLTon";
            this.tabQLNPLTon.Size = new System.Drawing.Size(827, 412);
            this.tabQLNPLTon.TabStop = true;
            this.tabQLNPLTon.Text = "Quản lý nguyên phụ liệu tồn";
            // 
            // btnChinhDL
            // 
            this.btnChinhDL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChinhDL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChinhDL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnChinhDL.Icon")));
            this.btnChinhDL.Location = new System.Drawing.Point(11, 386);
            this.btnChinhDL.Name = "btnChinhDL";
            this.btnChinhDL.Size = new System.Drawing.Size(128, 23);
            this.btnChinhDL.TabIndex = 192;
            this.btnChinhDL.Text = "Chỉnh dữ liệu lệch";
            this.btnChinhDL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnChinhDL.Click += new System.EventHandler(this.btnChinhDL_Click);
            // 
            // btnXuatExcelNPLTon
            // 
            this.btnXuatExcelNPLTon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuatExcelNPLTon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatExcelNPLTon.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXuatExcelNPLTon.Icon")));
            this.btnXuatExcelNPLTon.Location = new System.Drawing.Point(633, 386);
            this.btnXuatExcelNPLTon.Name = "btnXuatExcelNPLTon";
            this.btnXuatExcelNPLTon.Size = new System.Drawing.Size(102, 23);
            this.btnXuatExcelNPLTon.TabIndex = 192;
            this.btnXuatExcelNPLTon.Text = "Xuất Excel";
            this.btnXuatExcelNPLTon.VisualStyleManager = this.vsmMain;
            this.btnXuatExcelNPLTon.Click += new System.EventHandler(this.btnXuatExcelNPLTon_Click);
            // 
            // btnInNPLTon
            // 
            this.btnInNPLTon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInNPLTon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInNPLTon.Icon = ((System.Drawing.Icon)(resources.GetObject("btnInNPLTon.Icon")));
            this.btnInNPLTon.Location = new System.Drawing.Point(741, 386);
            this.btnInNPLTon.Name = "btnInNPLTon";
            this.btnInNPLTon.Size = new System.Drawing.Size(75, 23);
            this.btnInNPLTon.TabIndex = 191;
            this.btnInNPLTon.Text = "In";
            this.btnInNPLTon.VisualStyleManager = this.vsmMain;
            this.btnInNPLTon.Click += new System.EventHandler(this.btnInNPLTon_Click);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.filterEditor1);
            this.uiGroupBox5.Location = new System.Drawing.Point(11, 12);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(805, 76);
            this.uiGroupBox5.TabIndex = 189;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // filterEditor1
            // 
            this.filterEditor1.AutoApply = true;
            this.filterEditor1.BackColor = System.Drawing.Color.Transparent;
            this.filterEditor1.DefaultConditionOperator = Janus.Data.ConditionOperator.BeginsWith;
            this.filterEditor1.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.UseFormatStyle;
            this.filterEditor1.Location = new System.Drawing.Point(39, 11);
            this.filterEditor1.MinSize = new System.Drawing.Size(465, 44);
            this.filterEditor1.Name = "filterEditor1";
            this.filterEditor1.Office2007ColorScheme = Janus.Windows.Common.Office2007ColorScheme.Default;
            this.filterEditor1.ScrollMode = Janus.Windows.UI.Dock.ScrollMode.Both;
            this.filterEditor1.ScrollStep = 15;
            this.filterEditor1.Size = new System.Drawing.Size(555, 65);
            this.filterEditor1.SourceControl = this.dgList;
            this.filterEditor1.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(11, 94);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(805, 286);
            this.dgList.TabIndex = 187;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            this.dgList.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_FormattingRow);
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.btnLayDuLieuCungUng);
            this.uiTabPage2.Controls.Add(this.btnChuyenTonSXXK);
            this.uiTabPage2.Controls.Add(this.uiButton4);
            this.uiTabPage2.Controls.Add(this.uiButton2);
            this.uiTabPage2.Controls.Add(this.uiGroupBox3);
            this.uiTabPage2.Controls.Add(this.dgListNPLTK);
            this.uiTabPage2.Key = "tpThongKeLuongTon";
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(827, 412);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thống kê lượng tồn theo tờ khai";
            // 
            // btnLayDuLieuCungUng
            // 
            this.btnLayDuLieuCungUng.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLayDuLieuCungUng.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayDuLieuCungUng.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLayDuLieuCungUng.Icon")));
            this.btnLayDuLieuCungUng.Location = new System.Drawing.Point(327, 385);
            this.btnLayDuLieuCungUng.Name = "btnLayDuLieuCungUng";
            this.btnLayDuLieuCungUng.Size = new System.Drawing.Size(154, 23);
            this.btnLayDuLieuCungUng.TabIndex = 5;
            this.btnLayDuLieuCungUng.Text = "Lấy dữ liệu cung ứng";
            this.btnLayDuLieuCungUng.VisualStyleManager = this.vsmMain;
            this.btnLayDuLieuCungUng.Click += new System.EventHandler(this.btnLayDuLieuCungUng_Click);
            // 
            // btnChuyenTonSXXK
            // 
            this.btnChuyenTonSXXK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChuyenTonSXXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChuyenTonSXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("btnChuyenTonSXXK.Icon")));
            this.btnChuyenTonSXXK.Location = new System.Drawing.Point(487, 385);
            this.btnChuyenTonSXXK.Name = "btnChuyenTonSXXK";
            this.btnChuyenTonSXXK.Size = new System.Drawing.Size(140, 23);
            this.btnChuyenTonSXXK.TabIndex = 4;
            this.btnChuyenTonSXXK.Text = "Chuyển tồn SXXK";
            this.btnChuyenTonSXXK.VisualStyleManager = this.vsmMain;
            this.btnChuyenTonSXXK.Click += new System.EventHandler(this.btnChuyenTonSXXK_Click);
            // 
            // uiButton4
            // 
            this.uiButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton4.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton4.Icon")));
            this.uiButton4.Location = new System.Drawing.Point(633, 385);
            this.uiButton4.Name = "uiButton4";
            this.uiButton4.Size = new System.Drawing.Size(102, 23);
            this.uiButton4.TabIndex = 2;
            this.uiButton4.Text = "Xuất Excel";
            this.uiButton4.VisualStyleManager = this.vsmMain;
            this.uiButton4.Click += new System.EventHandler(this.uiButton4_Click);
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(741, 385);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 3;
            this.uiButton2.Text = "In";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            this.uiButton2.Click += new System.EventHandler(this.inDanhSachTon_Click_1);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.ccTo);
            this.uiGroupBox3.Controls.Add(this.label9);
            this.uiGroupBox3.Controls.Add(this.ccFrom);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtMaNPL);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Controls.Add(this.uiButton3);
            this.uiGroupBox3.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Location = new System.Drawing.Point(11, 3);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(805, 81);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // ccTo
            // 
            // 
            // 
            // 
            this.ccTo.DropDownCalendar.Name = "";
            this.ccTo.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccTo.Location = new System.Drawing.Point(351, 47);
            this.ccTo.Name = "ccTo";
            this.ccTo.Size = new System.Drawing.Size(81, 21);
            this.ccTo.TabIndex = 12;
            this.ccTo.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccTo.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(281, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Đến ngày";
            // 
            // ccFrom
            // 
            // 
            // 
            // 
            this.ccFrom.DropDownCalendar.Name = "";
            this.ccFrom.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccFrom.Location = new System.Drawing.Point(175, 47);
            this.ccFrom.Name = "ccFrom";
            this.ccFrom.Size = new System.Drawing.Size(81, 21);
            this.ccFrom.TabIndex = 10;
            this.ccFrom.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccFrom.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(106, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Từ ngày";
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtMaNPL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNPL.Location = new System.Drawing.Point(351, 19);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(177, 21);
            this.txtMaNPL.TabIndex = 5;
            this.txtMaNPL.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(281, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mã NPL";
            // 
            // uiButton3
            // 
            this.uiButton3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton3.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton3.Icon")));
            this.uiButton3.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.uiButton3.Location = new System.Drawing.Point(444, 47);
            this.uiButton3.Name = "uiButton3";
            this.uiButton3.Size = new System.Drawing.Size(84, 23);
            this.uiButton3.TabIndex = 8;
            this.uiButton3.Text = "Tìm kiếm";
            this.uiButton3.VisualStyleManager = this.vsmMain;
            this.uiButton3.WordWrap = false;
            this.uiButton3.Click += new System.EventHandler(this.TimToKhaiTon_Click);
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.DecimalDigits = 0;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.FormatString = "#####";
            this.txtSoTiepNhan.Location = new System.Drawing.Point(175, 19);
            this.txtSoTiepNhan.MaxLength = 12;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(81, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoTiepNhan.Value = ((ulong)(0ul));
            this.txtSoTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(106, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Số tờ khai";
            // 
            // dgListNPLTK
            // 
            this.dgListNPLTK.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPLTK.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListNPLTK.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPLTK.ColumnAutoResize = true;
            dgListNPLTK_DesignTimeLayout.LayoutString = resources.GetString("dgListNPLTK_DesignTimeLayout.LayoutString");
            this.dgListNPLTK.DesignTimeLayout = dgListNPLTK_DesignTimeLayout;
            this.dgListNPLTK.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPLTK.GroupByBoxVisible = false;
            this.dgListNPLTK.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLTK.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLTK.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPLTK.Location = new System.Drawing.Point(11, 90);
            this.dgListNPLTK.Name = "dgListNPLTK";
            this.dgListNPLTK.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPLTK.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPLTK.Size = new System.Drawing.Size(805, 289);
            this.dgListNPLTK.TabIndex = 1;
            this.dgListNPLTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListNPLTK.VisualStyleManager = this.vsmMain;
            this.dgListNPLTK.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListNPLTK_LoadingRow);
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.btnXuatExcelNPLCungUng);
            this.uiTabPage3.Controls.Add(this.cmdXemBangNPLCungUng);
            this.uiTabPage3.Controls.Add(this.cmdViewBangPhanBoNPL);
            this.uiTabPage3.Controls.Add(this.uiGroupBox6);
            this.uiTabPage3.Controls.Add(this.dgListPhanBo);
            this.uiTabPage3.Key = "tpTheoDoiPhanBo";
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(827, 412);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Theo dõi phân bổ";
            // 
            // btnXuatExcelNPLCungUng
            // 
            this.btnXuatExcelNPLCungUng.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuatExcelNPLCungUng.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatExcelNPLCungUng.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXuatExcelNPLCungUng.Icon")));
            this.btnXuatExcelNPLCungUng.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnXuatExcelNPLCungUng.Location = new System.Drawing.Point(140, 381);
            this.btnXuatExcelNPLCungUng.Name = "btnXuatExcelNPLCungUng";
            this.btnXuatExcelNPLCungUng.Size = new System.Drawing.Size(221, 23);
            this.btnXuatExcelNPLCungUng.TabIndex = 1;
            this.btnXuatExcelNPLCungUng.Text = "Xuất Excel bảng NPL cung ứng";
            this.btnXuatExcelNPLCungUng.VisualStyleManager = this.vsmMain;
            this.btnXuatExcelNPLCungUng.WordWrap = false;
            this.btnXuatExcelNPLCungUng.Click += new System.EventHandler(this.btnXuatExcelNPLCungUng_Click);
            // 
            // cmdXemBangNPLCungUng
            // 
            this.cmdXemBangNPLCungUng.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdXemBangNPLCungUng.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdXemBangNPLCungUng.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdXemBangNPLCungUng.Icon")));
            this.cmdXemBangNPLCungUng.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.cmdXemBangNPLCungUng.Location = new System.Drawing.Point(368, 381);
            this.cmdXemBangNPLCungUng.Name = "cmdXemBangNPLCungUng";
            this.cmdXemBangNPLCungUng.Size = new System.Drawing.Size(221, 23);
            this.cmdXemBangNPLCungUng.TabIndex = 2;
            this.cmdXemBangNPLCungUng.Text = "Xem bảng tổng hợp NPL cung ứng";
            this.cmdXemBangNPLCungUng.VisualStyleManager = this.vsmMain;
            this.cmdXemBangNPLCungUng.WordWrap = false;
            this.cmdXemBangNPLCungUng.Click += new System.EventHandler(this.cmdXemBangNPLCungUng_Click);
            // 
            // cmdViewBangPhanBoNPL
            // 
            this.cmdViewBangPhanBoNPL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdViewBangPhanBoNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdViewBangPhanBoNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdViewBangPhanBoNPL.Icon")));
            this.cmdViewBangPhanBoNPL.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.cmdViewBangPhanBoNPL.Location = new System.Drawing.Point(595, 381);
            this.cmdViewBangPhanBoNPL.Name = "cmdViewBangPhanBoNPL";
            this.cmdViewBangPhanBoNPL.Size = new System.Drawing.Size(221, 23);
            this.cmdViewBangPhanBoNPL.TabIndex = 3;
            this.cmdViewBangPhanBoNPL.Text = "Xem bảng tổng hợp phân bổ NPL";
            this.cmdViewBangPhanBoNPL.VisualStyleManager = this.vsmMain;
            this.cmdViewBangPhanBoNPL.WordWrap = false;
            this.cmdViewBangPhanBoNPL.Click += new System.EventHandler(this.cmdViewBangPhanBoNPL_Click);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.cbbToKhai);
            this.uiGroupBox6.Controls.Add(this.label10);
            this.uiGroupBox6.Controls.Add(this.filterEditor2);
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.cbSanPham);
            this.uiGroupBox6.Controls.Add(this.uiButton8);
            this.uiGroupBox6.Location = new System.Drawing.Point(11, 3);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(805, 91);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // cbbToKhai
            // 
            cbbToKhai_DesignTimeLayout.LayoutString = resources.GetString("cbbToKhai_DesignTimeLayout.LayoutString");
            this.cbbToKhai.DesignTimeLayout = cbbToKhai_DesignTimeLayout;
            this.cbbToKhai.Location = new System.Drawing.Point(97, 14);
            this.cbbToKhai.Name = "cbbToKhai";
            this.cbbToKhai.SelectedIndex = -1;
            this.cbbToKhai.SelectedItem = null;
            this.cbbToKhai.Size = new System.Drawing.Size(296, 21);
            this.cbbToKhai.TabIndex = 1;
            this.cbbToKhai.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(28, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "ID tờ khai";
            // 
            // filterEditor2
            // 
            this.filterEditor2.AutoApply = true;
            this.filterEditor2.BackColor = System.Drawing.Color.Transparent;
            this.filterEditor2.DefaultConditionOperator = Janus.Data.ConditionOperator.Equal;
            this.filterEditor2.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.UseFormatStyle;
            this.filterEditor2.Location = new System.Drawing.Point(22, 37);
            this.filterEditor2.MinSize = new System.Drawing.Size(403, 44);
            this.filterEditor2.Name = "filterEditor2";
            this.filterEditor2.Office2007ColorScheme = Janus.Windows.Common.Office2007ColorScheme.Default;
            this.filterEditor2.ScrollMode = Janus.Windows.UI.Dock.ScrollMode.Both;
            this.filterEditor2.ScrollStep = 15;
            this.filterEditor2.Size = new System.Drawing.Size(562, 50);
            this.filterEditor2.SourceControl = this.dgListPhanBo;
            this.filterEditor2.VisualStyleManager = this.vsmMain;
            // 
            // dgListPhanBo
            // 
            this.dgListPhanBo.AlternatingColors = true;
            this.dgListPhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListPhanBo.AutomaticSort = false;
            this.dgListPhanBo.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListPhanBo_DesignTimeLayout.LayoutString = resources.GetString("dgListPhanBo_DesignTimeLayout.LayoutString");
            this.dgListPhanBo.DesignTimeLayout = dgListPhanBo_DesignTimeLayout;
            this.dgListPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListPhanBo.GroupByBoxVisible = false;
            this.dgListPhanBo.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListPhanBo.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListPhanBo.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListPhanBo.Location = new System.Drawing.Point(11, 100);
            this.dgListPhanBo.Name = "dgListPhanBo";
            this.dgListPhanBo.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListPhanBo.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListPhanBo.Size = new System.Drawing.Size(805, 272);
            this.dgListPhanBo.TabIndex = 4;
            this.dgListPhanBo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListPhanBo.VisualStyleManager = this.vsmMain;
            this.dgListPhanBo.EditingCell += new Janus.Windows.GridEX.EditingCellEventHandler(this.dgListPhanBo_EditingCell);
            this.dgListPhanBo.RecordUpdated += new System.EventHandler(this.dgListPhanBo_RecordUpdated);
            this.dgListPhanBo.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListPhanBo_LoadingRow);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(412, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sản phẩm";
            // 
            // cbSanPham
            // 
            this.cbSanPham.Location = new System.Drawing.Point(481, 14);
            this.cbSanPham.Name = "cbSanPham";
            this.cbSanPham.Size = new System.Drawing.Size(206, 21);
            this.cbSanPham.TabIndex = 3;
            this.cbSanPham.VisualStyleManager = this.vsmMain;
            // 
            // uiButton8
            // 
            this.uiButton8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton8.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton8.Icon")));
            this.uiButton8.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.uiButton8.Location = new System.Drawing.Point(590, 46);
            this.uiButton8.Name = "uiButton8";
            this.uiButton8.Size = new System.Drawing.Size(97, 23);
            this.uiButton8.TabIndex = 4;
            this.uiButton8.Text = "Tìm kiếm";
            this.uiButton8.VisualStyleManager = this.vsmMain;
            this.uiButton8.WordWrap = false;
            this.uiButton8.Click += new System.EventHandler(this.XemPhanBo_Click);
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.progressBarPhanBo);
            this.uiTabPage4.Controls.Add(this.btnPhanBo);
            this.uiTabPage4.Controls.Add(this.dgListTKChuaPhanBo);
            this.uiTabPage4.Key = "tpPhanBoToKhaiXuat";
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(827, 412);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "Thực hiện phân bổ cho tờ khai xuất";
            // 
            // progressBarPhanBo
            // 
            this.progressBarPhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarPhanBo.Location = new System.Drawing.Point(11, 384);
            this.progressBarPhanBo.Name = "progressBarPhanBo";
            this.progressBarPhanBo.Size = new System.Drawing.Size(100, 23);
            this.progressBarPhanBo.TabIndex = 282;
            // 
            // btnPhanBo
            // 
            this.btnPhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhanBo.Icon = ((System.Drawing.Icon)(resources.GetObject("btnPhanBo.Icon")));
            this.btnPhanBo.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnPhanBo.Location = new System.Drawing.Point(732, 382);
            this.btnPhanBo.Name = "btnPhanBo";
            this.btnPhanBo.Size = new System.Drawing.Size(81, 23);
            this.btnPhanBo.TabIndex = 278;
            this.btnPhanBo.Text = "Phân bổ";
            this.btnPhanBo.VisualStyleManager = this.vsmMain;
            this.btnPhanBo.WordWrap = false;
            this.btnPhanBo.Click += new System.EventHandler(this.PhanBo_Click);
            // 
            // dgListTKChuaPhanBo
            // 
            this.dgListTKChuaPhanBo.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTKChuaPhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListTKChuaPhanBo.AutomaticSort = false;
            this.dgListTKChuaPhanBo.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListTKChuaPhanBo.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTKChuaPhanBo.ColumnAutoResize = true;
            this.dgListTKChuaPhanBo.ContextMenuStrip = this.contextMenuStripPhanBo;
            dgListTKChuaPhanBo_DesignTimeLayout.LayoutString = resources.GetString("dgListTKChuaPhanBo_DesignTimeLayout.LayoutString");
            this.dgListTKChuaPhanBo.DesignTimeLayout = dgListTKChuaPhanBo_DesignTimeLayout;
            this.dgListTKChuaPhanBo.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKChuaPhanBo.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTKChuaPhanBo.GroupByBoxVisible = false;
            this.dgListTKChuaPhanBo.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListTKChuaPhanBo.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTKChuaPhanBo.Hierarchical = true;
            this.dgListTKChuaPhanBo.ImageList = this.ImageList1;
            this.dgListTKChuaPhanBo.Location = new System.Drawing.Point(11, 5);
            this.dgListTKChuaPhanBo.Name = "dgListTKChuaPhanBo";
            this.dgListTKChuaPhanBo.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTKChuaPhanBo.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTKChuaPhanBo.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKChuaPhanBo.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTKChuaPhanBo.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListTKChuaPhanBo.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTKChuaPhanBo.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListTKChuaPhanBo.Size = new System.Drawing.Size(802, 373);
            this.dgListTKChuaPhanBo.TabIndex = 205;
            this.dgListTKChuaPhanBo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListTKChuaPhanBo.VisualStyleManager = this.vsmMain;
            this.dgListTKChuaPhanBo.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListTKChuaPhanBo_LoadingRow);
            this.dgListTKChuaPhanBo.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListTKChuaPhanBo_FormattingRow);
            // 
            // contextMenuStripPhanBo
            // 
            this.contextMenuStripPhanBo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemPhanBo});
            this.contextMenuStripPhanBo.Name = "contextMenuStrip2";
            this.contextMenuStripPhanBo.Size = new System.Drawing.Size(119, 26);
            // 
            // toolStripMenuItemPhanBo
            // 
            this.toolStripMenuItemPhanBo.Name = "toolStripMenuItemPhanBo";
            this.toolStripMenuItemPhanBo.Size = new System.Drawing.Size(118, 22);
            this.toolStripMenuItemPhanBo.Text = "Phân bổ";
            this.toolStripMenuItemPhanBo.Click += new System.EventHandler(this.toolStripMenuItemPhanBo_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.progressBarDaPhanBo);
            this.uiTabPage1.Controls.Add(this.label15);
            this.uiTabPage1.Controls.Add(this.btnDeletePhanBo);
            this.uiTabPage1.Controls.Add(this.dgToKhaiDaPhanBo);
            this.uiTabPage1.Key = "tpDanhSachToKhaiDaPhanBo";
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(827, 412);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Danh sách tờ khai đã phân bổ";
            // 
            // progressBarDaPhanBo
            // 
            this.progressBarDaPhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarDaPhanBo.Location = new System.Drawing.Point(524, 381);
            this.progressBarDaPhanBo.Name = "progressBarDaPhanBo";
            this.progressBarDaPhanBo.Size = new System.Drawing.Size(100, 23);
            this.progressBarDaPhanBo.TabIndex = 281;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label15.Location = new System.Drawing.Point(18, 381);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(500, 30);
            this.label15.TabIndex = 280;
            this.label15.Text = "Liệt kê danh sách tờ khai đã phân bổ theo thứ tự phân bổ.\r\nChọn tờ khai xuất để r" +
                "ồi bấm nút xóa phân bổ để xóa phân bổ từ tờ khai đó trở về sau.";
            // 
            // btnDeletePhanBo
            // 
            this.btnDeletePhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeletePhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeletePhanBo.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDeletePhanBo.Icon")));
            this.btnDeletePhanBo.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnDeletePhanBo.Location = new System.Drawing.Point(699, 381);
            this.btnDeletePhanBo.Name = "btnDeletePhanBo";
            this.btnDeletePhanBo.Size = new System.Drawing.Size(111, 23);
            this.btnDeletePhanBo.TabIndex = 279;
            this.btnDeletePhanBo.Text = "Xóa phân bổ";
            this.btnDeletePhanBo.VisualStyleManager = this.vsmMain;
            this.btnDeletePhanBo.WordWrap = false;
            this.btnDeletePhanBo.Click += new System.EventHandler(this.btnDeletePhanBo_Click);
            // 
            // dgToKhaiDaPhanBo
            // 
            this.dgToKhaiDaPhanBo.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgToKhaiDaPhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgToKhaiDaPhanBo.AutomaticSort = false;
            this.dgToKhaiDaPhanBo.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgToKhaiDaPhanBo.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgToKhaiDaPhanBo.ColumnAutoResize = true;
            this.dgToKhaiDaPhanBo.ContextMenuStrip = this.contextMenuStripDaPhanBo;
            dgToKhaiDaPhanBo_DesignTimeLayout.LayoutString = resources.GetString("dgToKhaiDaPhanBo_DesignTimeLayout.LayoutString");
            this.dgToKhaiDaPhanBo.DesignTimeLayout = dgToKhaiDaPhanBo_DesignTimeLayout;
            this.dgToKhaiDaPhanBo.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgToKhaiDaPhanBo.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiDaPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgToKhaiDaPhanBo.GroupByBoxVisible = false;
            this.dgToKhaiDaPhanBo.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiDaPhanBo.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgToKhaiDaPhanBo.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiDaPhanBo.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgToKhaiDaPhanBo.Hierarchical = true;
            this.dgToKhaiDaPhanBo.ImageList = this.ImageList1;
            this.dgToKhaiDaPhanBo.Location = new System.Drawing.Point(17, 17);
            this.dgToKhaiDaPhanBo.Name = "dgToKhaiDaPhanBo";
            this.dgToKhaiDaPhanBo.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgToKhaiDaPhanBo.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgToKhaiDaPhanBo.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgToKhaiDaPhanBo.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiDaPhanBo.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgToKhaiDaPhanBo.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgToKhaiDaPhanBo.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiDaPhanBo.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgToKhaiDaPhanBo.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgToKhaiDaPhanBo.Size = new System.Drawing.Size(793, 358);
            this.dgToKhaiDaPhanBo.TabIndex = 206;
            this.dgToKhaiDaPhanBo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgToKhaiDaPhanBo.VisualStyleManager = this.vsmMain;
            this.dgToKhaiDaPhanBo.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListTKChuaPhanBo_FormattingRow);
            // 
            // contextMenuStripDaPhanBo
            // 
            this.contextMenuStripDaPhanBo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemBangPhanBo,
            this.toolStripMenuItemBangNPLCungUng,
            this.xuatExcelNPLCungUngToolStripMenuItem,
            this.toolStripMenuItem1,
            this.xemBangNplCungUngTKMuaVNToolStripMenuItem});
            this.contextMenuStripDaPhanBo.Name = "contextMenuStrip1";
            this.contextMenuStripDaPhanBo.Size = new System.Drawing.Size(229, 114);
            // 
            // toolStripMenuItemBangPhanBo
            // 
            this.toolStripMenuItemBangPhanBo.Name = "toolStripMenuItemBangPhanBo";
            this.toolStripMenuItemBangPhanBo.Size = new System.Drawing.Size(228, 22);
            this.toolStripMenuItemBangPhanBo.Text = "Xem bảng phân bổ";
            this.toolStripMenuItemBangPhanBo.Click += new System.EventHandler(this.toolStripMenuItemBangPhanBo_Click);
            // 
            // toolStripMenuItemBangNPLCungUng
            // 
            this.toolStripMenuItemBangNPLCungUng.Name = "toolStripMenuItemBangNPLCungUng";
            this.toolStripMenuItemBangNPLCungUng.Size = new System.Drawing.Size(228, 22);
            this.toolStripMenuItemBangNPLCungUng.Text = "Xem bảng npl cung ứng";
            this.toolStripMenuItemBangNPLCungUng.Click += new System.EventHandler(this.toolStripMenuItemBangNPLCungUng_Click);
            // 
            // xuatExcelNPLCungUngToolStripMenuItem
            // 
            this.xuatExcelNPLCungUngToolStripMenuItem.Name = "xuatExcelNPLCungUngToolStripMenuItem";
            this.xuatExcelNPLCungUngToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.xuatExcelNPLCungUngToolStripMenuItem.Text = "Xuất Excel NPL cung ứng";
            this.xuatExcelNPLCungUngToolStripMenuItem.Click += new System.EventHandler(this.xuatExcelNPLCungUngToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(228, 22);
            this.toolStripMenuItem1.Text = "Xóa phân bổ và chỉnh dữ liệu";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // xemBangNplCungUngTKMuaVNToolStripMenuItem
            // 
            this.xemBangNplCungUngTKMuaVNToolStripMenuItem.Name = "xemBangNplCungUngTKMuaVNToolStripMenuItem";
            this.xemBangNplCungUngTKMuaVNToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.ctrDonViHaiQuan);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.btnSearch);
            this.uiGroupBox4.Controls.Add(this.ccToDate);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.ccFromDate);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(53, 5);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(686, 96);
            this.uiGroupBox4.TabIndex = 304;
            this.uiGroupBox4.Text = "Tìm kiếm thông tin tờ khai";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // ctrDonViHaiQuan
            // 
            this.ctrDonViHaiQuan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ctrDonViHaiQuan.BackColor = System.Drawing.Color.Transparent;
            this.ctrDonViHaiQuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrDonViHaiQuan.Location = new System.Drawing.Point(161, 19);
            this.ctrDonViHaiQuan.Ma = "C34C";
            this.ctrDonViHaiQuan.MaCuc = "";
            this.ctrDonViHaiQuan.Name = "ctrDonViHaiQuan";
            this.ctrDonViHaiQuan.ReadOnly = true;
            this.ctrDonViHaiQuan.Size = new System.Drawing.Size(366, 39);
            this.ctrDonViHaiQuan.TabIndex = 274;
            this.ctrDonViHaiQuan.VisualStyleManager = null;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(60, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 273;
            this.label3.Text = "Mã hải quan";
            // 
            // btnSearch
            // 
            this.btnSearch.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSearch.Icon")));
            this.btnSearch.Location = new System.Drawing.Point(546, 50);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(93, 40);
            this.btnSearch.TabIndex = 272;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyleManager = this.vsmMain;
            // 
            // ccToDate
            // 
            this.ccToDate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccToDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccToDate.DropDownCalendar.Name = "";
            this.ccToDate.DropDownCalendar.Visible = false;
            this.ccToDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccToDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccToDate.Location = new System.Drawing.Point(392, 52);
            this.ccToDate.Name = "ccToDate";
            this.ccToDate.Nullable = true;
            this.ccToDate.NullButtonText = "Xóa";
            this.ccToDate.ShowNullButton = true;
            this.ccToDate.Size = new System.Drawing.Size(120, 21);
            this.ccToDate.TabIndex = 271;
            this.ccToDate.TodayButtonText = "Hôm nay";
            this.ccToDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccToDate.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(325, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 270;
            this.label7.Text = "Đến ngày";
            // 
            // ccFromDate
            // 
            this.ccFromDate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccFromDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccFromDate.DropDownCalendar.Name = "";
            this.ccFromDate.DropDownCalendar.Visible = false;
            this.ccFromDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccFromDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccFromDate.Location = new System.Drawing.Point(190, 52);
            this.ccFromDate.Name = "ccFromDate";
            this.ccFromDate.Nullable = true;
            this.ccFromDate.NullButtonText = "Xóa";
            this.ccFromDate.ShowNullButton = true;
            this.ccFromDate.Size = new System.Drawing.Size(120, 21);
            this.ccFromDate.TabIndex = 269;
            this.ccFromDate.TodayButtonText = "Hôm nay";
            this.ccFromDate.Value = new System.DateTime(2008, 1, 1, 0, 0, 0, 0);
            this.ccFromDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccFromDate.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(60, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 13);
            this.label6.TabIndex = 269;
            this.label6.Text = "Chọn tờ khai từ ngày";
            // 
            // uiButton1
            // 
            this.uiButton1.Location = new System.Drawing.Point(625, 120);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(84, 40);
            this.uiButton1.TabIndex = 303;
            this.uiButton1.Text = "Chọn tờ khai";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            // 
            // btnChonTK
            // 
            this.btnChonTK.Location = new System.Drawing.Point(718, 120);
            this.btnChonTK.Name = "btnChonTK";
            this.btnChonTK.Size = new System.Drawing.Size(74, 40);
            this.btnChonTK.TabIndex = 302;
            this.btnChonTK.Text = "Thống kê";
            this.btnChonTK.VisualStyleManager = this.vsmMain;
            // 
            // dgTKX
            // 
            this.dgTKX.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgTKX.AlternatingColors = true;
            this.dgTKX.AutomaticSort = false;
            this.dgTKX.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKX.ColumnAutoResize = true;
            dgTKX_DesignTimeLayout.LayoutString = resources.GetString("dgTKX_DesignTimeLayout.LayoutString");
            this.dgTKX.DesignTimeLayout = dgTKX_DesignTimeLayout;
            this.dgTKX.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgTKX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKX.GroupByBoxVisible = false;
            this.dgTKX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKX.Location = new System.Drawing.Point(0, 172);
            this.dgTKX.Name = "dgTKX";
            this.dgTKX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKX.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKX.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKX.Size = new System.Drawing.Size(795, 165);
            this.dgTKX.TabIndex = 195;
            this.dgTKX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTKX.VisualStyleManager = this.vsmMain;
            // 
            // gridEXPrintDocument1
            // 
            this.gridEXPrintDocument1.CardColumnsPerPage = 1;
            this.gridEXPrintDocument1.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintDocument1.GridEX = this.dgListNPLTK;
            this.gridEXPrintDocument1.PageHeaderCenter = "DANH SÁCH NGUYÊN PHỤ LIỆU TỒN THỰC TẾ";
            this.gridEXPrintDocument1.PageHeaderFormatStyle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // gridEXExporter1
            // 
            this.gridEXExporter1.GridEX = this.dgListNPLTK;
            this.gridEXExporter1.IncludeFormatStyle = false;
            this.gridEXExporter1.SheetName = "LuongTonToKhai";
            // 
            // gridEXExporterNPL
            // 
            this.gridEXExporterNPL.GridEX = this.dgList;
            this.gridEXExporterNPL.IncludeFormatStyle = false;
            this.gridEXExporterNPL.SheetName = "Nguyen phu lieu ton";
            // 
            // gridEXPrintDocumentNPL
            // 
            this.gridEXPrintDocumentNPL.CardColumnsPerPage = 1;
            this.gridEXPrintDocumentNPL.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintDocumentNPL.GridEX = this.dgList;
            this.gridEXPrintDocumentNPL.PageHeaderCenter = "THỐNG KÊ NGUYÊN PHỤ LIỆU TỒN";
            this.gridEXPrintDocumentNPL.PageHeaderFormatStyle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Excel files|*.xls";
            // 
            // ThongkeHangTonForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(829, 434);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ThongkeHangTonForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Quản lý phân bổ";
            this.Load += new System.EventHandler(this.ThongkeHangTonForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTabSanPham)).EndInit();
            this.uiTabSanPham.ResumeLayout(false);
            this.tabQLNPLTon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLTK)).EndInit();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToKhai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListPhanBo)).EndInit();
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKChuaPhanBo)).EndInit();
            this.contextMenuStripPhanBo.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgToKhaiDaPhanBo)).EndInit();
            this.contextMenuStripDaPhanBo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton uicmdSelectTK;
        private Janus.Windows.EditControls.UIButton uicmdThongke;
        private Janus.Windows.UI.Tab.UITab uiTabSanPham;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.CalendarCombo.CalendarCombo ccToDate;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.CalendarCombo.CalendarCombo ccFromDate;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton btnChonTK;
        private Janus.Windows.GridEX.GridEX dgTKX;
        private Company.Interface.Controls.DonViHaiQuanControl ctrDonViHaiQuan;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.UI.Tab.UITabPage tabQLNPLTon;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton uiButton3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.GridEX dgListNPLTK;
        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintDocument1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.FilterEditor.FilterEditor filterEditor1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.GridEX.GridEX dgListPhanBo;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton uiButton8;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cbSanPham;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.EditControls.UIButton btnPhanBo;
        private Janus.Windows.GridEX.GridEX dgListTKChuaPhanBo;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.FilterEditor.FilterEditor filterEditor2;
        private Janus.Windows.EditControls.UIButton cmdViewBangPhanBoNPL;
        private Janus.Windows.EditControls.UIButton cmdXemBangNPLCungUng;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.GridEX.GridEX dgToKhaiDaPhanBo;
        private Janus.Windows.EditControls.UIButton btnDeletePhanBo;
        private Janus.Windows.EditControls.UIButton uiButton4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbToKhai;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripDaPhanBo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemBangPhanBo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemBangNPLCungUng;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripPhanBo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemPhanBo;
        private Janus.Windows.EditControls.UIButton btnXuatExcelNPLTon;
        private Janus.Windows.EditControls.UIButton btnInNPLTon;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporterNPL;
        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintDocumentNPL;
        private Janus.Windows.EditControls.UIButton btnXuatExcelNPLCungUng;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem xuatExcelNPLCungUngToolStripMenuItem;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.CalendarCombo.CalendarCombo ccTo;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.CalendarCombo.CalendarCombo ccFrom;
        private Janus.Windows.EditControls.UIButton btnChuyenTonSXXK;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private Janus.Windows.EditControls.UIButton btnLayDuLieuCungUng;
        private System.Windows.Forms.ProgressBar progressBarDaPhanBo;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ProgressBar progressBarPhanBo;
        private System.Windows.Forms.ToolStripMenuItem xemBangNplCungUngTKMuaVNToolStripMenuItem;
        private Janus.Windows.EditControls.UIButton btnChinhDL;
    }
}