﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Data.SqlClient;

namespace ToolCreateAccount
{
    public partial class ConfigurationDatabase : Form
    {
        string path = "";
        string MaChiCucCurrent = "";
        string Database = "";
        public ConfigurationDatabase()
        {
            InitializeComponent();
        }

        private void ConfigurationDatabase_Load(object sender, EventArgs e)
        {
            BindData();            
        }

        private void BindData()
        {
            string path = Properties.Settings.Default.PathFileDatabase;
            if (path == "")
            {
                DialogResult rel = MessageBox.Show("Đường dẫn tới file lưu trữ database chưa có.Bạn có muốn chọn đường dẫn không?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (rel == DialogResult.Yes)
                {
                    DialogResult ok = openFileDialog1.ShowDialog();
                    if (ok == DialogResult.OK)
                    {
                        path = openFileDialog1.FileName;
                        DialogResult luuTHongTin = MessageBox.Show("Bạn có muốn lưu đường dẫn file lại không ?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (luuTHongTin == DialogResult.Yes)
                        {
                            Properties.Settings.Default.PathFileDatabase = path;
                            Properties.Settings.Default.Save();
                        }
                    }
                }
                else
                    return;
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            treeView1.Nodes.Clear();
            foreach (XmlNode node in doc.SelectSingleNode("Root").ChildNodes)
            {
                TreeNode nodeTree = new TreeNode(node.Attributes[0].Value);
                TreeNode nodeTreeCon1 = new TreeNode("SXXK");
                TreeNode nodeTreeCon2 = new TreeNode("SLXNK");
                nodeTree.Nodes.Add(nodeTreeCon1);
                nodeTree.Nodes.Add(nodeTreeCon2);
                treeView1.Nodes.Add(nodeTree);
            }
            treeView1.Refresh();

        }
      

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Level > 0)
            {
                string path = Properties.Settings.Default.PathFileDatabase;
                if (path == "")
                {
                    DialogResult rel = MessageBox.Show("Đường dẫn tới file lưu trữ database chưa có.Bạn có muốn chọn đường dẫn không?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (rel == DialogResult.Yes)
                    {
                        DialogResult ok = openFileDialog1.ShowDialog();
                        if (ok == DialogResult.OK)
                        {
                            path = openFileDialog1.FileName;
                            DialogResult luuTHongTin = MessageBox.Show("Bạn có muốn lưu đường dẫn file lại không ?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (luuTHongTin == DialogResult.Yes)
                            {
                                Properties.Settings.Default.PathFileDatabase = path;
                                Properties.Settings.Default.Save();
                            }
                        }
                    }
                    else
                        return;
                }
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                XmlNode node = doc.SelectSingleNode("Root/ChiCuc[@MaChiCuc=\"" + e.Node.Parent.Text + "\"]");
                txtConnectStr.Text = node.SelectSingleNode(e.Node.Text).InnerText.Trim();
                MaChiCucCurrent = e.Node.Parent.Text.Trim();
                Database = e.Node.Text.Trim();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CreateHQ f = new CreateHQ();
            f.ShowDialog();
            if (f.isCreate)
            {
                BindData();
                txtConnectStr.Clear();
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (MaChiCucCurrent == "")
            {
                MessageBox.Show("Bạn chưa chọn chi cục để sửa.");
                return;
            }
            string path = Properties.Settings.Default.PathFileDatabase;
            if (path == "")
            {
                DialogResult rel = MessageBox.Show("Đường dẫn tới file lưu trữ database chưa có.Bạn có muốn chọn đường dẫn không?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (rel == DialogResult.Yes)
                {
                    DialogResult ok = openFileDialog1.ShowDialog();
                    if (ok == DialogResult.OK)
                    {
                        path = openFileDialog1.FileName;
                        DialogResult luuTHongTin = MessageBox.Show("Bạn có muốn lưu đường dẫn file lại không ?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (luuTHongTin == DialogResult.Yes)
                        {
                            Properties.Settings.Default.PathFileDatabase = path;
                            Properties.Settings.Default.Save();
                        }
                    }
                }
                else
                    return;
            }

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(path);
            }
            catch
            {
                MessageBox.Show("Có lỗi khi đọc file.Hãy kiểm tra lại file");
                return;
            }
            XmlNode node = doc.SelectSingleNode("Root/ChiCuc[@MaChiCuc=\"" + MaChiCucCurrent + "\"]");

            node.SelectSingleNode(Database).InnerText = txtConnectStr.Text.Trim();

            doc.Save(path);
            BindData();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult ok = openFileDialog1.ShowDialog();
            if (ok == DialogResult.OK)
            {
                string path = openFileDialog1.FileName;
                DialogResult luuTHongTin = MessageBox.Show("Bạn có muốn lưu đường dẫn file lại không ?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (luuTHongTin == DialogResult.Yes)
                {
                    Properties.Settings.Default.PathFileDatabase = path;
                    Properties.Settings.Default.Save();
                }
            }
        }

    }
}