﻿/*
Run this script on:

192.168.72.100.ECS_TQDT_KD_V3    -  This database will be modified

to synchronize it with:

192.168.72.100.ECS_TQDT_DANHMUC

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 8.0.2 from Red Gate Software Ltd at 09/26/2012 3:43:35 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)
BEGIN TRANSACTION

delete  FROM [dbo].[t_HaiQuan_LoaiHinhMauDich]

-- Add 179 rows to [dbo].[t_HaiQuan_LoaiHinhMauDich]
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('CNC01', N'CT Nhập chế xuất sản xuất', 'CT-NCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('CNC02', N'CT Nhập chế xuất đầu tư', 'CT-NCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('CNC03', N'CT Nhập chế xuất tiêu dùng', 'CT-NCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('CNC04', N'CT Nhập chế xuất cho mục đích khác', 'CT-NCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('CXC01', N'CT Xuất chế xuất sản xuất', 'CT-XCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('CXC02', N'CT Xuất chế xuất đầu tư', 'CT-XCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('CXC04', N'CT Xuất chế xuất cho mục đích khác', 'CT-XCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('N0003', N'Nhap co quan', 'NCQ', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('N0006', N'Nhap qua canh', 'NQC-PMD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NCX01', N'Nhập chế xuất sản xuất', 'NCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NCX02', N'Nhập chế xuất đầu tư', 'NCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NCX03', N'Nhập chế xuất tiêu dùng', 'NCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NCX04', N'Nhập chế xuất cho mục đích khác', 'NCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT01', N'Nhập đầu tư', 'NDT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT02', N'Nhập Đầu Tư gắn máy', 'NDTG', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT03', N'Nhập Đầu Tư ô tô', 'NDTO', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT04', N'Nhập chuyển khẩu', 'NCK', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT05', N'Nhập Đầu Tư sửa chữa tái chế', 'NDT-TNST', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT06', N'Nhập Đầu Tư Kho Bảo Thuế', 'NDT-KBT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT07', N'Nhập Đầu Tư nhập viện trợ', 'NDT-NVT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT08', N'Nhập Đầu Tư nộp thuế', 'NDT-NPT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT09', N'Nhập Đầu Tư từ Việt Nam', 'NDTV', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT10', N'Nhap Kinh Doanh Đầu Tư (Trong nước)', 'NKD-DT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT11', N'Nhập đầu tư khu chế xuất', 'NDT-KCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT12', N'Nhập đầu tư tái xuất', 'NDT-TX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT13', N'Nhập đầu tư tạm xuất', 'NDT-TMX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT14', N'Nhập đầu tư khu công nghiệp', 'NDT-KCN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT15', N'Nhập đầu tư gia công khu công nghiệp', 'NDT-GCKCN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT16', N'Nhập đầu tư tại chỗ', 'NDT-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT17', N'Nhập Đầu tư liên doanh', 'NDT-LD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT18', N'Nhập Đầu tư nộp thuế tại chỗ', 'NDTNT-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC01', N'Nhập Gia Công', 'NGC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC02', N'Nhập Đầu Tư Gia công', 'NDT-GC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC03', N'Nhập Gia Công Kinh doanh', 'NGC-KD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC04', N'Nhập Gia Công Tạm nhập', 'NGC-TN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC05', N'Nhập Kinh Doanh Kho Bảo Thuế', 'NKD-KBT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC06', N'Hàng hóa tái nhập vào KCX', 'NTKCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC07', N'Hàng hóa tạm nhập vào KCX', 'NTXKCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC08', N'Nội địa mua hàng của Khu chế xuất', 'NKD-BND', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC09', N'Nhập khu chế xuất', 'NCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC10', N'Nhập chuyển tiếp', 'NCT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC11', N'Nhập gia công để SXXK', 'NGC-SXXK', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC12', N'Nhap TP  tu ND vao KCX', 'NGC-CX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC13', N'Nhập gia công tại chổ', 'NGC-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC14', N'Nhập chế xuất tại chỗ', 'NCX-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC15', N'Nhập kho bảo thuế tại chỗ', 'NBT-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC16', N'Nhập Gia công từ KTM về nội địa', 'NGC/KTM-N§', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC18', N'Nhập gia công chuyển tiếp NPL', 'NCT-NPL', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC19', N'Nhập gia công chuyển tiếp SP', 'NCT-SP', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC20', N'Nhập gia công chuyển tiếp TB', 'NCT-TB', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC21', N'Nhập gia công tự cung ứng', 'NGC-CU', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC99', N'Nhập Gia công Tạm nhập Tái chế', 'NGC-TNTC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD01', N'Nhập Kinh Doanh', 'NKD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD02', N'Nhập Dầu khí', 'NDK', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD03', N'Nhập Kinh Doanh Đá Quí', 'NKD-DQ', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD04', N'Nhập Kinh Doanh Gắn máy', 'NKDG', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD05', N'Nhập Đầu Tư Kinh doanh', 'NDT-KD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD06', N'Nhập Kinh Doanh Ô tô', 'NKDO', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD07', N'Nhập hợp đồng đại lý', 'NHD-DL', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD08', N'Nhập Kinh doanh hàng đổi hàng', 'NKD-HDH', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD09', N'Nhập Kinh doanh Nội địa hóa', 'NKD-NDH', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD10', N'Nhập Kinh doanh Nội địa hóa - Gắn máy', 'NKD-NDHG', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD11', N'Nhập Kinh doanh-Tại Chỗ', 'NKD-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD12', N'Nhập Biên giới', 'NBG', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD13', N'Nhập Kinh doanh từ KTM về nội địa', 'NKD/KTM-N§', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NSX01', N'Nhập Để Sản Xuất Hàng Xuất Khẩu', 'NSXX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NSX02', N'Nhập Đầu Tư Sản xuất xuất khẩu', 'NDT-SXX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NSX03', N'Nhập san xuat xuat khau vao KCX', 'NSX-CX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NSX04', N'KCX mua hang noi dia de SXXK', 'NKD-MND-CX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NSX05', N'Tam nhap hang SXXK', 'TNSXXK', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NSX06', N'Nhập sản xuất xuất khẩu tại chỗ', 'NSX-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NSX07', N'Nhập NPL vào kho bảo thuế để SXXK', 'NSX-KBT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA01', N'Tạm Nhập Tái Xuất (Nhập Phải Tái Xuất)', 'NTX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA02', N'Tái Nhập', 'NT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA03', N'Tạm Nhập Tàu Biển', 'NTTB', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA04', N'Nhập Đầu Tư Tái nhập', 'NDT-TAN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA05', N'Tái Nhập Hàng Xuất Triển Lãm', 'NTTL', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA06', N'Nhập kho ngoại quan', 'NKNQ', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA07', N'Nhập Uỷ Thác', 'NUT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA08', N'Nhập Viện Trợ', 'NVT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA09', N'Tái Nhập Thành Phẩm GC vào KCX', 'NGCT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA10', N'Tạm Nhập NPL vào KCX để Gia công', 'NGCN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA11', N'Nhập Đầu Tư Tạm nhập thi công', 'NDT-TNTC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA12', N'Mua Hàng Của Nội địa (Xí nghiệp KCX)', 'NKD-MND', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA13', N'Nhập Quá Cảnh', 'NQC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA14', N'Nhập Triển Lãm,Hàng mẫu,Quảng cáo ...', 'NTL', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA15', N'Nhập Đầu Tư Tạm nhập', 'NDT-TN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA16', N'Nhập Đầu Tư Kinh Doanh Cửa hàng M/Thuế', 'NDT-TNKD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA17', N'Nhập Địa Phương Ôtô', 'NDPO', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA18', N'Nhập Địa Phương Xe Máy', 'NDPG', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA19', N'Nhập hàng bán tại cửa hàng miễn thuế', 'NBMT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA20', N'Nhập Trung Ương Ôtô', 'NTWO', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA21', N'Nhập Trung Ương Xe Máy', 'NTWG', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA22', N'Nhập Viện Trợ Ôtô', 'NVTO', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA23', N'Nhập Viện Trợ Xe Máy', 'NVTG', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA24', N'Tạm nhập xăng dầu', 'TNXD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA25', N'Tạm nhập tái chế', 'NTX-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA26', N'Tạm nhập Tái xuất tại chỗ', 'TNTX-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NTA27', N'Tái nhập tại chỗ', 'TN-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('PHPLN', N'Nhận nguyên liệu dư từ HĐGC khác', 'NPLV', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('PHPLX', N'Giao nguyên liệu dư cho HĐGC khác', 'NPLD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('PHSPN', N'Nhận sản phẩm GCCT từ HĐGC khác', 'SPV', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('PHSPX', N'Giao sản phẩm GCCT cho HĐGC khác', 'SPD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('PHTBN', N'Nhận máy móc thiết bị từ HĐGC khác', 'TBV', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('PHTBX', N'Giao máy móc thiết bị cho HĐGC khác', 'TBD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('X0003', N'Tai xuat', 'XTX-PMD ', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XCX01', N'Xuất chế xuất sản xuất', 'XCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XCX02', N'Xuất chế xuất đầu tư', 'XCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XCX04', N'Xuất chế xuất cho mục đích khác', 'XCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XDT01', N'Xuất Đầu Tư', 'XDT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XDT02', N'Xuất Đầu Tư Gắn máy', 'XDTG', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XDT03', N'Xuất Đầu Tư Ô tô', 'XDTO', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XDT04', N'Xuất Chuyển khẩu', 'XCK', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XDT05', N'Xuất Đầu Tư đã sửa chữa tái chế', 'XDT-TXST', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XDT06', N'Xuất Đầu Tư Kho Bảo Thuế', 'XDT-KBT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XDT07', N'Xuất đầu tư khu công nghiệp', 'XDT-KCN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XDT08', N'Xuất đầu tư gia công khu công nghiệp', 'XDT-GCKCN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XDT09', N'Xuất đầu tư kinh doanh khu công nghiệp', 'XDT-KDKCN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XDT10', N'Xuất đầu tư tại chỗ', 'XDT-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC01', N'Xuất Gia Công', 'XGC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC02', N'Xuất Đầu Tư Gia công', 'XDT-GC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC03', N'Xuất Gia Công Kinh doanh', 'XGC-KD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC04', N'Xuất Gia Công Tái xuất', 'XGC-TX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC05', N'Xuất Kinh Doanh Kho Bảo Thuế', 'XKD-KBT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC06', N'Hàng hóa tái xuất ra nước ngoài từ KCX', 'XTKCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC07', N'Hàng hóa tạm xuất ra nước ngoài từ KCX', 'XTNKCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC08', N'Hàng trong nội địa bán cho KCX', 'XKD-MND', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC09', N'Sản Phẩm KCX xuất ra nước ngoài', 'XDTKCX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC10', N'Xuất đặt gia công hàng hoá ở nước ngoài', 'XGCNN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC12', N'Xuat NL tu KCX vao noi dia de GC', 'XGC-CX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC13', N'Xuất gia công tại chổ', 'XGC-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC14', N'Xuất chế xuất tại chỗ', 'XCX-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC15', N'Xuất kho bảo thuế tại chỗ', 'XBT-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC16', N'Xuất Gia công từ nội địa vào KTM', 'XGC/N§-KTM', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC18', N'Xuất gia công chuyển tiếp NPL', 'XCT-NPL', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC19', N'Xuất gia công chuyển tiếp SP', 'XCT-SP', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC20', N'Xuất gia công chuyển tiếp TB', 'XCT-TB', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD01', N'Xuất Kinh Doanh', 'XKD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD02', N'Xuất Dầu khí', 'XDK', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD03', N'Xuất Kinh Doanh Đá Quí', 'XKD-DQ', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD04', N'Xuất Kinh Doanh Gắn máy', 'XKDG', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD05', N'Xuất Đầu Tư Kinh doanh', 'XDT-KD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD06', N'Xuất Kinh Doanh Ô tô', 'XKDO', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD07', N'Xuất kinh doanh hàng đổi hàng', 'XKD-HDH', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD08', N'Xuất kinh doanh tại chỗ', 'XKD-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD09', N'Xuất Kinh doanh từ nội địa vào KTM', 'XKD/ND-KTM', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD10', N'Xuất Biên giới', 'XKD-BG', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD11', N'Xuất Kinh doanh phục vụ Đầu tư', 'XKD-DT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XSX01', N'Xuất khẩu hàng SX từ hàng NK', 'XSXN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XSX02', N'Xuất Đầu Tư Sản xuất xuất khẩu', 'XDT-SXX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XSX03', N'Xuat san xuat xuat khau tu KCX', 'XSX-CX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XSX04', N'KCX ban hang noi dia de SXXK', 'XKD-BND-CX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XSX05', N'Tai xuat hang SXXK', 'TXSXXK', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XSX06', N'Xuất sản xuất tại chỗ', 'XSX-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XSX07', N'Xuất SXXK vào kho bảo thuế', 'XSX-KBT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA01', N'Tạm Xuất Tái Nhập (Xuất Phải Tái Nhập)', 'XTN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA02', N'Tái Xuất', 'XT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA03', N'Tái Xuất Tàu Biển', 'XTTB', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA04', N'Xuất Đầu Tư Tái xuất', 'XDT-TX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA05', N'Tạm Xuất  Triển Lãm', 'XTTL', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA06', N'Xuất kho ngoại quan', 'XKNQ', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA07', N'Xuất Uỷ Thác', 'XUT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA08', N'Xuất Viện Trợ', 'XVT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA09', N'Tái Xuất Thành Phẩm GC vào Nội địa', 'XGCT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA10', N'Tạm Xuất NPL vào Nội địa để Gia công', 'XGCN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA11', N'Xuất Đầu Tư Tái xuất thi công', 'XDT-TXTC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA12', N'Bán Hàng cho Nội địa (Xí nghiệp KCX)', 'XKD-BND', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA13', N'Xuất Quá Cảnh', 'XQC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA14', N'Xuất Triển Lãm,Hàng mẫu,Quảng cáo ...', 'XTL', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA15', N'Xuất Đầu Tư Tạm xuất', 'XDT-TX', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA16', N'Tái Xuất Xăng Dầu', 'XTXD', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA17', N'Xuất hàng bán tại cửa hàng miễn thuế', 'XBMT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA18', N'Tái xuất hàng bán miễn thuế', 'XT-BMT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA19', N'Tái xuất Tái chế', 'XT-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA20', N'Tái xuất ( Hàng tạm nhập tái xuất)', 'XTX-HTN', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA21', N'Xuất Tại chỗ Tái xuất', 'XTA-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA22', N'Tạm xuất Tái nhập tại chỗ', 'TXTN-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA23', N'Tái xuất tại chỗ', 'TX-TC', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XTA24', N'TáI xuất Đầu tư Kho Bảo Thuế', 'TXDT-KBT', '2012-09-26 14:33:06.097', '2012-09-26 14:33:06.097')
COMMIT TRANSACTION
GO

if( (select count(*) from dbo.t_HaiQuan_Version where [Version] ='4.7') = 0)
	begin
		insert into dbo.t_HaiQuan_Version values('4.7', getdate(), N'')
	end 