using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.BLL.SXXK.ThanhKhoan
{
	public partial class NPLTaiXuatTon : EntityBase
	{
		#region Private members.
		
		protected int _SoToKhai;
		protected string _MaLoaiHinh = String.Empty;
		protected short _NamDangKy;
		protected string _MaHaiQuan = String.Empty;
		protected string _MaNPL = String.Empty;
		protected string _MaDoanhNghiep = String.Empty;
		protected decimal _LuongTaiXuat;
		protected decimal _Ton;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public int SoToKhai
		{
			set {this._SoToKhai = value;}
			get {return this._SoToKhai;}
		}
		public string MaLoaiHinh
		{
			set {this._MaLoaiHinh = value;}
			get {return this._MaLoaiHinh;}
		}
		public short NamDangKy
		{
			set {this._NamDangKy = value;}
			get {return this._NamDangKy;}
		}
		public string MaHaiQuan
		{
			set {this._MaHaiQuan = value;}
			get {return this._MaHaiQuan;}
		}
		public string MaNPL
		{
			set {this._MaNPL = value;}
			get {return this._MaNPL;}
		}
		public string MaDoanhNghiep
		{
			set {this._MaDoanhNghiep = value;}
			get {return this._MaDoanhNghiep;}
		}
		public decimal LuongTaiXuat
		{
			set {this._LuongTaiXuat = value;}
			get {return this._LuongTaiXuat;}
		}
		public decimal Ton
		{
			set {this._Ton = value;}
			get {return this._Ton;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_SXXK_ThanhLy_NPLTaiXuatTon_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTaiXuat"))) this._LuongTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) this._Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
				return true;
			}
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public NPLTaiXuatTonCollection SelectCollectionBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy()
		{
			string spName = "p_SXXK_ThanhLy_NPLTaiXuatTon_SelectBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			
			NPLTaiXuatTonCollection collection = new NPLTaiXuatTonCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				NPLTaiXuatTon entity = new NPLTaiXuatTon();
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTaiXuat"))) entity.LuongTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
				collection.Add(entity);
			}
			return collection;
		}

		//---------------------------------------------------------------------------------------------

		public DataSet SelectBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy()
		{
			string spName = "p_SXXK_ThanhLy_NPLTaiXuatTon_SelectBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
						
            return this.db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public DataSet SelectAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLTaiXuatTon_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLTaiXuatTon_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_ThanhLy_NPLTaiXuatTon_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_ThanhLy_NPLTaiXuatTon_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public NPLTaiXuatTonCollection SelectCollectionAll()
		{
			NPLTaiXuatTonCollection collection = new NPLTaiXuatTonCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				NPLTaiXuatTon entity = new NPLTaiXuatTon();
				
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTaiXuat"))) entity.LuongTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public NPLTaiXuatTonCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			NPLTaiXuatTonCollection collection = new NPLTaiXuatTonCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				NPLTaiXuatTon entity = new NPLTaiXuatTon();
				
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTaiXuat"))) entity.LuongTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_ThanhLy_NPLTaiXuatTon_Insert";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@LuongTaiXuat", SqlDbType.Decimal, this._LuongTaiXuat);
			this.db.AddInParameter(dbCommand, "@Ton", SqlDbType.Decimal, this._Ton);
			
			if (transaction != null)
			{
				return this.db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return this.db.ExecuteNonQuery(dbCommand);
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(NPLTaiXuatTonCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (NPLTaiXuatTon item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, NPLTaiXuatTonCollection collection)
        {
            foreach (NPLTaiXuatTon item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_ThanhLy_NPLTaiXuatTon_InsertUpdate";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@LuongTaiXuat", SqlDbType.Decimal, this._LuongTaiXuat);
			this.db.AddInParameter(dbCommand, "@Ton", SqlDbType.Decimal, this._Ton);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(NPLTaiXuatTonCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (NPLTaiXuatTon item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_ThanhLy_NPLTaiXuatTon_Update";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@LuongTaiXuat", SqlDbType.Decimal, this._LuongTaiXuat);
			this.db.AddInParameter(dbCommand, "@Ton", SqlDbType.Decimal, this._Ton);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(NPLTaiXuatTonCollection collection, SqlTransaction transaction)
        {
            foreach (NPLTaiXuatTon item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_ThanhLy_NPLTaiXuatTon_Delete";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(NPLTaiXuatTonCollection collection, SqlTransaction transaction)
        {
            foreach (NPLTaiXuatTon item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(NPLTaiXuatTonCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (NPLTaiXuatTon item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion

        public static void Updatebase(DataSet ds, SqlTransaction transaction)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NPLTaiXuatTon nplTXTon = new NPLTaiXuatTon();
                nplTXTon.LuongTaiXuat = Convert.ToDecimal(row["LuongXuat"]);
                nplTXTon.MaDoanhNghiep = row["MA_DV"].ToString();
                nplTXTon.MaHaiQuan = row["MA_HQ"].ToString();
                nplTXTon.MaLoaiHinh = row["MA_LH"].ToString();
                nplTXTon.MaNPL = row["MA_NPL"].ToString();
                nplTXTon.NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                nplTXTon.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                nplTXTon.Ton = Convert.ToDecimal(row["TonXuat"]);                
                nplTXTon.InsertUpdateTransaction(transaction);
            }
        }

	}	
}