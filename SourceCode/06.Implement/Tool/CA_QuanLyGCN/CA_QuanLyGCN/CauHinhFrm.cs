﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CA_QuanLyGCN
{
    public partial class CauHinhFrm : Form
    {
        public CauHinhFrm()
        {
            InitializeComponent();
        }

        private void CauHinhFrm_Load(object sender, EventArgs e)
        {
            try
            {
                txtTenCoQuan.Text = CauHinh.Load("CoQuan").KeyValue.ToString();
                txtTenPhong.Text = CauHinh.Load("Phong").KeyValue.ToString();
                txtTenPhongFull.Text = CauHinh.Load("FullName").KeyValue.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show("Lưu thông lỗi:  " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
           
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {

            try
            {

                CauHinh cauhinh = new CauHinh();

                cauhinh.Key = "CoQuan";
                cauhinh.KeyValue = txtTenCoQuan.Text;
                cauhinh.IsUse = true;
                cauhinh.Update();
                cauhinh.Key = "Phong";
                cauhinh.KeyValue = txtTenPhong.Text;
                cauhinh.IsUse = true;
                cauhinh.Update();

                cauhinh.Key = "FullName";
                cauhinh.KeyValue = txtTenPhongFull.Text;
                cauhinh.IsUse = true;
                cauhinh.Update();
                MessageBox.Show("Lưu cấu hình thành công");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show("Lưu thông lỗi:  " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
           
        }

    }
}
