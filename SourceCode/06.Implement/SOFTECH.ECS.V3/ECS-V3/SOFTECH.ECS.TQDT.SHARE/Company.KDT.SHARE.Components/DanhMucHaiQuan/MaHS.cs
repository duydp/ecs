﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
    public partial class MaHS
    {
        public static List<MaHS> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<MaHS> collection = new List<MaHS>();

            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                MaHS entity = new MaHS();
                entity.Nhom = dataSet.Tables[0].Rows[i]["Nhom"].ToString();
                entity.PN1 = dataSet.Tables[0].Rows[i]["PN1"].ToString();
                entity.PN2 = dataSet.Tables[0].Rows[i]["PN2"].ToString();
                entity.Pn3 = dataSet.Tables[0].Rows[i]["Pn3"].ToString();
                entity.Mo_ta = dataSet.Tables[0].Rows[i]["Mo_ta"].ToString();
                entity.HS10SO = dataSet.Tables[0].Rows[i]["HS10SO"].ToString();
                entity.DateCreated = dataSet.Tables[0].Rows[i]["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateCreated"]) : DateTime.Today;
                entity.DateModified = dataSet.Tables[0].Rows[i]["DateModified"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateModified"]) : DateTime.Today;
                collection.Add(entity);
            }

            return collection;
        }
    }
}
