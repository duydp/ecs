using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmLogin : DevExpress.XtraEditors.XtraForm
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            lblError.Text = "";
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                Program.LoginID = Employee.DoLogin(txtUserName.Text, txtPassword.Text);
                if (txtUserName.Text != "")
                {
                    if (Program.LoginID != "")
                    {
                        lblError.Text = "Đăng nhập thành công!";
                        this.Hide();
                        Program.isLoginSuccess = true;
                    }
                    else
                    {
                        lblError.Text = "Đăng nhập thất bại!";
                    }
                }
                else
                {
                    lblError.Text = "Tên đăng nhập không được bỏ trống!";
                }
            }
            catch
            {
                lblError.Text = "Có lỗi kết nối xảy ra!";
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}