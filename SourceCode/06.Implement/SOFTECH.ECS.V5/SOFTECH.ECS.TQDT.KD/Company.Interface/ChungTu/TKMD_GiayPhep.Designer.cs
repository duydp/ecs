﻿namespace Company.Interface
{
    partial class TKMD_GiayPhep
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbMaPhanLoaiGP = new Janus.Windows.EditControls.UIComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoVanDon4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.uiComboBox1 = new Janus.Windows.EditControls.UIComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.uiComboBox2 = new Janus.Windows.EditControls.UIComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.editBox2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.uiComboBox3 = new Janus.Windows.EditControls.UIComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.editBox3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.uiComboBox4 = new Janus.Windows.EditControls.UIComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.editBox4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnGhi);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(577, 242);
            // 
            // cbMaPhanLoaiGP
            // 
            this.cbMaPhanLoaiGP.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbMaPhanLoaiGP.DisplayMember = "ID";
            this.cbMaPhanLoaiGP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMaPhanLoaiGP.Location = new System.Drawing.Point(143, 20);
            this.cbMaPhanLoaiGP.Name = "cbMaPhanLoaiGP";
            this.cbMaPhanLoaiGP.Size = new System.Drawing.Size(104, 21);
            this.cbMaPhanLoaiGP.TabIndex = 8;
            this.cbMaPhanLoaiGP.ValueMember = "ID";
            this.cbMaPhanLoaiGP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(11, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(126, 13);
            this.label14.TabIndex = 7;
            this.label14.Text = "Mã phân loại giấy phép 1";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(264, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Số giấy phép 1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtSoVanDon4
            // 
            this.txtSoVanDon4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon4.Location = new System.Drawing.Point(348, 20);
            this.txtSoVanDon4.MaxLength = 255;
            this.txtSoVanDon4.Name = "txtSoVanDon4";
            this.txtSoVanDon4.Size = new System.Drawing.Size(210, 21);
            this.txtSoVanDon4.TabIndex = 9;
            this.txtSoVanDon4.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDon4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.editBox4);
            this.uiGroupBox1.Controls.Add(this.editBox3);
            this.uiGroupBox1.Controls.Add(this.editBox2);
            this.uiGroupBox1.Controls.Add(this.editBox1);
            this.uiGroupBox1.Controls.Add(this.txtSoVanDon4);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.uiComboBox4);
            this.uiGroupBox1.Controls.Add(this.uiComboBox3);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.uiComboBox2);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.uiComboBox1);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.cbMaPhanLoaiGP);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(571, 196);
            this.uiGroupBox1.TabIndex = 10;
            this.uiGroupBox1.Text = "Danh sách giấy phép";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(264, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Số giấy phép 2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uiComboBox1
            // 
            this.uiComboBox1.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.uiComboBox1.DisplayMember = "ID";
            this.uiComboBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiComboBox1.Location = new System.Drawing.Point(143, 57);
            this.uiComboBox1.Name = "uiComboBox1";
            this.uiComboBox1.Size = new System.Drawing.Size(104, 21);
            this.uiComboBox1.TabIndex = 8;
            this.uiComboBox1.ValueMember = "ID";
            this.uiComboBox1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Mã phân loại giấy phép 2";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // editBox1
            // 
            this.editBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editBox1.Location = new System.Drawing.Point(348, 57);
            this.editBox1.MaxLength = 255;
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(210, 21);
            this.editBox1.TabIndex = 9;
            this.editBox1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.editBox1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(264, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Số giấy phép 3";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uiComboBox2
            // 
            this.uiComboBox2.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.uiComboBox2.DisplayMember = "ID";
            this.uiComboBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiComboBox2.Location = new System.Drawing.Point(143, 93);
            this.uiComboBox2.Name = "uiComboBox2";
            this.uiComboBox2.Size = new System.Drawing.Size(104, 21);
            this.uiComboBox2.TabIndex = 8;
            this.uiComboBox2.ValueMember = "ID";
            this.uiComboBox2.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Mã phân loại giấy phép 3";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // editBox2
            // 
            this.editBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editBox2.Location = new System.Drawing.Point(348, 93);
            this.editBox2.MaxLength = 255;
            this.editBox2.Name = "editBox2";
            this.editBox2.Size = new System.Drawing.Size(210, 21);
            this.editBox2.TabIndex = 9;
            this.editBox2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.editBox2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(264, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Số giấy phép 4";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uiComboBox3
            // 
            this.uiComboBox3.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.uiComboBox3.DisplayMember = "ID";
            this.uiComboBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiComboBox3.Location = new System.Drawing.Point(143, 129);
            this.uiComboBox3.Name = "uiComboBox3";
            this.uiComboBox3.Size = new System.Drawing.Size(104, 21);
            this.uiComboBox3.TabIndex = 8;
            this.uiComboBox3.ValueMember = "ID";
            this.uiComboBox3.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Mã phân loại giấy phép 4";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // editBox3
            // 
            this.editBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editBox3.Location = new System.Drawing.Point(348, 129);
            this.editBox3.MaxLength = 255;
            this.editBox3.Name = "editBox3";
            this.editBox3.Size = new System.Drawing.Size(210, 21);
            this.editBox3.TabIndex = 9;
            this.editBox3.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.editBox3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(264, 170);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Số giấy phép 5";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uiComboBox4
            // 
            this.uiComboBox4.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.uiComboBox4.DisplayMember = "ID";
            this.uiComboBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiComboBox4.Location = new System.Drawing.Point(143, 165);
            this.uiComboBox4.Name = "uiComboBox4";
            this.uiComboBox4.Size = new System.Drawing.Size(104, 21);
            this.uiComboBox4.TabIndex = 8;
            this.uiComboBox4.ValueMember = "ID";
            this.uiComboBox4.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 170);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Mã phân loại giấy phép 5";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // editBox4
            // 
            this.editBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editBox4.Location = new System.Drawing.Point(348, 165);
            this.editBox4.MaxLength = 255;
            this.editBox4.Name = "editBox4";
            this.editBox4.Size = new System.Drawing.Size(210, 21);
            this.editBox4.TabIndex = 9;
            this.editBox4.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.editBox4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // btnGhi
            // 
            this.btnGhi.Location = new System.Drawing.Point(499, 214);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 11;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyleManager = this.vsmMain;
            // 
            // TKMD_GiayPhep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 242);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "TKMD_GiayPhep";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Giấy phép của tờ khai";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIComboBox cbMaPhanLoaiGP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox editBox4;
        private Janus.Windows.GridEX.EditControls.EditBox editBox3;
        private Janus.Windows.GridEX.EditControls.EditBox editBox2;
        private Janus.Windows.GridEX.EditControls.EditBox editBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIComboBox uiComboBox4;
        private Janus.Windows.EditControls.UIComboBox uiComboBox3;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIComboBox uiComboBox2;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIComboBox uiComboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIButton btnGhi;
    }
}