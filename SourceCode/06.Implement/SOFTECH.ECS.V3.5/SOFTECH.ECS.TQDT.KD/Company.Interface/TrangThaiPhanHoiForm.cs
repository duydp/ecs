﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Company.Interface
{
    public partial class TrangThaiPhanHoiForm : DevExpress.XtraEditors.XtraForm
    {

        public TrangThaiPhanHoiForm()
        {
           
            InitializeComponent();
        }
        private void TrangThaiPhanHoiForm_Click(object sender, EventArgs e)
        {
            var mainForm = Application.OpenForms["MainForm"] as MainForm;
            //mainForm.ShowFormThongBao(null, null);
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { mainForm.frmThongBao.Show(); }));

            }
            else
                mainForm.frmThongBao.Show();
        }
        private void TrangThaiPhanHoiForm_Load(object sender, EventArgs e)
        {
            var mainForm = Application.OpenForms["MainForm"] as MainForm;
            this.TopMost = true;
            this.DesktopLocation = new System.Drawing.Point(mainForm.Width - 300, 3);
            picError.Dock = DockStyle.Fill;
            pnWaiting.Dock = DockStyle.Fill;
            picStop.Dock = DockStyle.Fill;
            picDownload.Dock = DockStyle.Fill;
            lblNew.Visible = false;
            ShowStop();
            picError.DoubleClick += new EventHandler(TrangThaiPhanHoiForm_Click);
            pnWaiting.DoubleClick += new EventHandler(TrangThaiPhanHoiForm_Click);
            picStop.DoubleClick += new EventHandler(TrangThaiPhanHoiForm_Click);
            picDownload.DoubleClick += new EventHandler(TrangThaiPhanHoiForm_Click);
            lblNew.DoubleClick += new EventHandler(TrangThaiPhanHoiForm_Click);


            picDownload.MouseDown += new MouseEventHandler(TrangThaiPhanHoiForm_MouseDown);
            picDownload.MouseUp += new MouseEventHandler(TrangThaiPhanHoiForm_MouseUp);
            picDownload.MouseMove += new MouseEventHandler(TrangThaiPhanHoiForm_MouseMove);

            pnWaiting.MouseDown += new MouseEventHandler(TrangThaiPhanHoiForm_MouseDown);
            pnWaiting.MouseUp += new MouseEventHandler(TrangThaiPhanHoiForm_MouseUp);
            pnWaiting.MouseMove += new MouseEventHandler(TrangThaiPhanHoiForm_MouseMove);

            picStop.MouseDown += new MouseEventHandler(TrangThaiPhanHoiForm_MouseDown);
            picStop.MouseUp += new MouseEventHandler(TrangThaiPhanHoiForm_MouseUp);
            picStop.MouseMove += new MouseEventHandler(TrangThaiPhanHoiForm_MouseMove);

            picError.MouseDown += new MouseEventHandler(TrangThaiPhanHoiForm_MouseDown);
            picError.MouseUp += new MouseEventHandler(TrangThaiPhanHoiForm_MouseUp);
            picError.MouseMove += new MouseEventHandler(TrangThaiPhanHoiForm_MouseMove);

            lblNew.MouseDown += new MouseEventHandler(TrangThaiPhanHoiForm_MouseDown);
            lblNew.MouseUp += new MouseEventHandler(TrangThaiPhanHoiForm_MouseUp);
            lblNew.MouseMove += new MouseEventHandler(TrangThaiPhanHoiForm_MouseMove);

            label1.MouseDown += new MouseEventHandler(TrangThaiPhanHoiForm_MouseDown);
            label1.MouseUp += new MouseEventHandler(TrangThaiPhanHoiForm_MouseUp);
            label1.MouseMove += new MouseEventHandler(TrangThaiPhanHoiForm_MouseMove);

        }
        public void ShowWaiting(int Time)
        {
            picError.Visible = false;
            pnWaiting.Visible = true;
            picStop.Visible = false;
            picDownload.Visible = false;
            TimeSpan time = new TimeSpan(0, 0, Time);
            label1.Text = time.ToString();
        }
        public void ShowStop()
        {
            picError.Visible = false;
            pnWaiting.Visible = false;
            picStop.Visible = true;
            picDownload.Visible = false;
            picStop.BringToFront();
        }
        public void ShowError()
        {
            picError.Visible = true;
            pnWaiting.Visible = false;
            picStop.Visible = false;
            picDownload.Visible = false;
        }
        public void ShowDownloading()
        {
            picError.Visible = false;
            pnWaiting.Visible = false;
            picStop.Visible = false;
            picDownload.Visible = true;
        }
        public void ShowNew(bool isNew)
        {
            lblNew.Visible = isNew;
        }
        int TogMove; int MValX; int MValY;
        private void TrangThaiPhanHoiForm_MouseDown(object sender, MouseEventArgs e)
        {
            TogMove = 1;
            MValX = e.X;
            MValY = e.Y;
        }

        private void TrangThaiPhanHoiForm_MouseUp(object sender, MouseEventArgs e)
        {
            TogMove = 0;
        }

        private void TrangThaiPhanHoiForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (TogMove == 1) 
            {
                this.SetDesktopLocation(MousePosition.X - MValX, MousePosition.Y - MValY); 
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }

        private void ẩnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notifyIcon1.Visible = true;
            notifyIcon1.ShowBalloonTip(500);
            this.Hide();
        }


    }
}