using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class GiayNopTien
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public string SoLenh { set; get; }
		public DateTime NgayPhatLenh { set; get; }
		public string TenNguoiNop { set; get; }
		public string SoCMNDNguoiNop { set; get; }
		public string DiaChi { set; get; }
		public string MaDonViNop { set; get; }
		public string TenDonViNop { set; get; }
		public string SoTKNop { set; get; }
		public string MaNganHangNop { set; get; }
		public string TenNganHangNop { set; get; }
		public string MaDonViNhan { set; get; }
		public string TenDonViNhan { set; get; }
		public string SoTKNhan { set; get; }
		public string MaNganHangNhan { set; get; }
		public string TenNganHangNhan { set; get; }
		public string GhiChu { set; get; }
		public byte[] FileDinhKem { set; get; }
		public string GuidStr { set; get; }
		public int LoaiKB { set; get; }
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public int TrangThai { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static GiayNopTien Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GiayNopTien_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			GiayNopTien entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new GiayNopTien();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLenh"))) entity.SoLenh = reader.GetString(reader.GetOrdinal("SoLenh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPhatLenh"))) entity.NgayPhatLenh = reader.GetDateTime(reader.GetOrdinal("NgayPhatLenh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNop"))) entity.TenNguoiNop = reader.GetString(reader.GetOrdinal("TenNguoiNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoCMNDNguoiNop"))) entity.SoCMNDNguoiNop = reader.GetString(reader.GetOrdinal("SoCMNDNguoiNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViNop"))) entity.MaDonViNop = reader.GetString(reader.GetOrdinal("MaDonViNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViNop"))) entity.TenDonViNop = reader.GetString(reader.GetOrdinal("TenDonViNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKNop"))) entity.SoTKNop = reader.GetString(reader.GetOrdinal("SoTKNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangNop"))) entity.MaNganHangNop = reader.GetString(reader.GetOrdinal("MaNganHangNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNganHangNop"))) entity.TenNganHangNop = reader.GetString(reader.GetOrdinal("TenNganHangNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViNhan"))) entity.MaDonViNhan = reader.GetString(reader.GetOrdinal("MaDonViNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViNhan"))) entity.TenDonViNhan = reader.GetString(reader.GetOrdinal("TenDonViNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKNhan"))) entity.SoTKNhan = reader.GetString(reader.GetOrdinal("SoTKNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangNhan"))) entity.MaNganHangNhan = reader.GetString(reader.GetOrdinal("MaNganHangNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNganHangNhan"))) entity.TenNganHangNhan = reader.GetString(reader.GetOrdinal("TenNganHangNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileDinhKem"))) entity.FileDinhKem = (byte[])reader.GetValue(reader.GetOrdinal("FileDinhKem"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<GiayNopTien> SelectCollectionAll()
		{
			List<GiayNopTien> collection = new List<GiayNopTien>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				GiayNopTien entity = new GiayNopTien();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLenh"))) entity.SoLenh = reader.GetString(reader.GetOrdinal("SoLenh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPhatLenh"))) entity.NgayPhatLenh = reader.GetDateTime(reader.GetOrdinal("NgayPhatLenh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNop"))) entity.TenNguoiNop = reader.GetString(reader.GetOrdinal("TenNguoiNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoCMNDNguoiNop"))) entity.SoCMNDNguoiNop = reader.GetString(reader.GetOrdinal("SoCMNDNguoiNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViNop"))) entity.MaDonViNop = reader.GetString(reader.GetOrdinal("MaDonViNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViNop"))) entity.TenDonViNop = reader.GetString(reader.GetOrdinal("TenDonViNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKNop"))) entity.SoTKNop = reader.GetString(reader.GetOrdinal("SoTKNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangNop"))) entity.MaNganHangNop = reader.GetString(reader.GetOrdinal("MaNganHangNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNganHangNop"))) entity.TenNganHangNop = reader.GetString(reader.GetOrdinal("TenNganHangNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViNhan"))) entity.MaDonViNhan = reader.GetString(reader.GetOrdinal("MaDonViNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViNhan"))) entity.TenDonViNhan = reader.GetString(reader.GetOrdinal("TenDonViNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKNhan"))) entity.SoTKNhan = reader.GetString(reader.GetOrdinal("SoTKNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangNhan"))) entity.MaNganHangNhan = reader.GetString(reader.GetOrdinal("MaNganHangNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNganHangNhan"))) entity.TenNganHangNhan = reader.GetString(reader.GetOrdinal("TenNganHangNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileDinhKem"))) entity.FileDinhKem = (byte[])reader.GetValue(reader.GetOrdinal("FileDinhKem"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<GiayNopTien> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<GiayNopTien> collection = new List<GiayNopTien>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				GiayNopTien entity = new GiayNopTien();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLenh"))) entity.SoLenh = reader.GetString(reader.GetOrdinal("SoLenh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPhatLenh"))) entity.NgayPhatLenh = reader.GetDateTime(reader.GetOrdinal("NgayPhatLenh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNop"))) entity.TenNguoiNop = reader.GetString(reader.GetOrdinal("TenNguoiNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoCMNDNguoiNop"))) entity.SoCMNDNguoiNop = reader.GetString(reader.GetOrdinal("SoCMNDNguoiNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViNop"))) entity.MaDonViNop = reader.GetString(reader.GetOrdinal("MaDonViNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViNop"))) entity.TenDonViNop = reader.GetString(reader.GetOrdinal("TenDonViNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKNop"))) entity.SoTKNop = reader.GetString(reader.GetOrdinal("SoTKNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangNop"))) entity.MaNganHangNop = reader.GetString(reader.GetOrdinal("MaNganHangNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNganHangNop"))) entity.TenNganHangNop = reader.GetString(reader.GetOrdinal("TenNganHangNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViNhan"))) entity.MaDonViNhan = reader.GetString(reader.GetOrdinal("MaDonViNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViNhan"))) entity.TenDonViNhan = reader.GetString(reader.GetOrdinal("TenDonViNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKNhan"))) entity.SoTKNhan = reader.GetString(reader.GetOrdinal("SoTKNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangNhan"))) entity.MaNganHangNhan = reader.GetString(reader.GetOrdinal("MaNganHangNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNganHangNhan"))) entity.TenNganHangNhan = reader.GetString(reader.GetOrdinal("TenNganHangNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileDinhKem"))) entity.FileDinhKem = (byte[])reader.GetValue(reader.GetOrdinal("FileDinhKem"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GiayNopTien_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GiayNopTien_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GiayNopTien_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GiayNopTien_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertGiayNopTien(long tKMD_ID, string soLenh, DateTime ngayPhatLenh, string tenNguoiNop, string soCMNDNguoiNop, string diaChi, string maDonViNop, string tenDonViNop, string soTKNop, string maNganHangNop, string tenNganHangNop, string maDonViNhan, string tenDonViNhan, string soTKNhan, string maNganHangNhan, string tenNganHangNhan, string ghiChu, byte[] fileDinhKem, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai)
		{
			GiayNopTien entity = new GiayNopTien();	
			entity.TKMD_ID = tKMD_ID;
			entity.SoLenh = soLenh;
			entity.NgayPhatLenh = ngayPhatLenh;
			entity.TenNguoiNop = tenNguoiNop;
			entity.SoCMNDNguoiNop = soCMNDNguoiNop;
			entity.DiaChi = diaChi;
			entity.MaDonViNop = maDonViNop;
			entity.TenDonViNop = tenDonViNop;
			entity.SoTKNop = soTKNop;
			entity.MaNganHangNop = maNganHangNop;
			entity.TenNganHangNop = tenNganHangNop;
			entity.MaDonViNhan = maDonViNhan;
			entity.TenDonViNhan = tenDonViNhan;
			entity.SoTKNhan = soTKNhan;
			entity.MaNganHangNhan = maNganHangNhan;
			entity.TenNganHangNhan = tenNganHangNhan;
			entity.GhiChu = ghiChu;
			entity.FileDinhKem = fileDinhKem;
			entity.GuidStr = guidStr;
			entity.LoaiKB = loaiKB;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThai = trangThai;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GiayNopTien_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoLenh", SqlDbType.NVarChar, SoLenh);
			db.AddInParameter(dbCommand, "@NgayPhatLenh", SqlDbType.DateTime, NgayPhatLenh.Year <= 1753 ? DBNull.Value : (object) NgayPhatLenh);
			db.AddInParameter(dbCommand, "@TenNguoiNop", SqlDbType.NVarChar, TenNguoiNop);
			db.AddInParameter(dbCommand, "@SoCMNDNguoiNop", SqlDbType.NVarChar, SoCMNDNguoiNop);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@MaDonViNop", SqlDbType.VarChar, MaDonViNop);
			db.AddInParameter(dbCommand, "@TenDonViNop", SqlDbType.NVarChar, TenDonViNop);
			db.AddInParameter(dbCommand, "@SoTKNop", SqlDbType.NVarChar, SoTKNop);
			db.AddInParameter(dbCommand, "@MaNganHangNop", SqlDbType.NVarChar, MaNganHangNop);
			db.AddInParameter(dbCommand, "@TenNganHangNop", SqlDbType.NVarChar, TenNganHangNop);
			db.AddInParameter(dbCommand, "@MaDonViNhan", SqlDbType.VarChar, MaDonViNhan);
			db.AddInParameter(dbCommand, "@TenDonViNhan", SqlDbType.NVarChar, TenDonViNhan);
			db.AddInParameter(dbCommand, "@SoTKNhan", SqlDbType.NVarChar, SoTKNhan);
			db.AddInParameter(dbCommand, "@MaNganHangNhan", SqlDbType.NVarChar, MaNganHangNhan);
			db.AddInParameter(dbCommand, "@TenNganHangNhan", SqlDbType.NVarChar, TenNganHangNhan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@FileDinhKem", SqlDbType.Image, FileDinhKem);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.NVarChar, GuidStr);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<GiayNopTien> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiayNopTien item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateGiayNopTien(long id, long tKMD_ID, string soLenh, DateTime ngayPhatLenh, string tenNguoiNop, string soCMNDNguoiNop, string diaChi, string maDonViNop, string tenDonViNop, string soTKNop, string maNganHangNop, string tenNganHangNop, string maDonViNhan, string tenDonViNhan, string soTKNhan, string maNganHangNhan, string tenNganHangNhan, string ghiChu, byte[] fileDinhKem, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai)
		{
			GiayNopTien entity = new GiayNopTien();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoLenh = soLenh;
			entity.NgayPhatLenh = ngayPhatLenh;
			entity.TenNguoiNop = tenNguoiNop;
			entity.SoCMNDNguoiNop = soCMNDNguoiNop;
			entity.DiaChi = diaChi;
			entity.MaDonViNop = maDonViNop;
			entity.TenDonViNop = tenDonViNop;
			entity.SoTKNop = soTKNop;
			entity.MaNganHangNop = maNganHangNop;
			entity.TenNganHangNop = tenNganHangNop;
			entity.MaDonViNhan = maDonViNhan;
			entity.TenDonViNhan = tenDonViNhan;
			entity.SoTKNhan = soTKNhan;
			entity.MaNganHangNhan = maNganHangNhan;
			entity.TenNganHangNhan = tenNganHangNhan;
			entity.GhiChu = ghiChu;
			entity.FileDinhKem = fileDinhKem;
			entity.GuidStr = guidStr;
			entity.LoaiKB = loaiKB;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThai = trangThai;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GiayNopTien_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoLenh", SqlDbType.NVarChar, SoLenh);
			db.AddInParameter(dbCommand, "@NgayPhatLenh", SqlDbType.DateTime, NgayPhatLenh.Year == 1753 ? DBNull.Value : (object) NgayPhatLenh);
			db.AddInParameter(dbCommand, "@TenNguoiNop", SqlDbType.NVarChar, TenNguoiNop);
			db.AddInParameter(dbCommand, "@SoCMNDNguoiNop", SqlDbType.NVarChar, SoCMNDNguoiNop);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@MaDonViNop", SqlDbType.VarChar, MaDonViNop);
			db.AddInParameter(dbCommand, "@TenDonViNop", SqlDbType.NVarChar, TenDonViNop);
			db.AddInParameter(dbCommand, "@SoTKNop", SqlDbType.NVarChar, SoTKNop);
			db.AddInParameter(dbCommand, "@MaNganHangNop", SqlDbType.NVarChar, MaNganHangNop);
			db.AddInParameter(dbCommand, "@TenNganHangNop", SqlDbType.NVarChar, TenNganHangNop);
			db.AddInParameter(dbCommand, "@MaDonViNhan", SqlDbType.VarChar, MaDonViNhan);
			db.AddInParameter(dbCommand, "@TenDonViNhan", SqlDbType.NVarChar, TenDonViNhan);
			db.AddInParameter(dbCommand, "@SoTKNhan", SqlDbType.NVarChar, SoTKNhan);
			db.AddInParameter(dbCommand, "@MaNganHangNhan", SqlDbType.NVarChar, MaNganHangNhan);
			db.AddInParameter(dbCommand, "@TenNganHangNhan", SqlDbType.NVarChar, TenNganHangNhan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@FileDinhKem", SqlDbType.Image, FileDinhKem);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.NVarChar, GuidStr);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<GiayNopTien> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiayNopTien item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateGiayNopTien(long id, long tKMD_ID, string soLenh, DateTime ngayPhatLenh, string tenNguoiNop, string soCMNDNguoiNop, string diaChi, string maDonViNop, string tenDonViNop, string soTKNop, string maNganHangNop, string tenNganHangNop, string maDonViNhan, string tenDonViNhan, string soTKNhan, string maNganHangNhan, string tenNganHangNhan, string ghiChu, byte[] fileDinhKem, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai)
		{
			GiayNopTien entity = new GiayNopTien();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoLenh = soLenh;
			entity.NgayPhatLenh = ngayPhatLenh;
			entity.TenNguoiNop = tenNguoiNop;
			entity.SoCMNDNguoiNop = soCMNDNguoiNop;
			entity.DiaChi = diaChi;
			entity.MaDonViNop = maDonViNop;
			entity.TenDonViNop = tenDonViNop;
			entity.SoTKNop = soTKNop;
			entity.MaNganHangNop = maNganHangNop;
			entity.TenNganHangNop = tenNganHangNop;
			entity.MaDonViNhan = maDonViNhan;
			entity.TenDonViNhan = tenDonViNhan;
			entity.SoTKNhan = soTKNhan;
			entity.MaNganHangNhan = maNganHangNhan;
			entity.TenNganHangNhan = tenNganHangNhan;
			entity.GhiChu = ghiChu;
			entity.FileDinhKem = fileDinhKem;
			entity.GuidStr = guidStr;
			entity.LoaiKB = loaiKB;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThai = trangThai;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GiayNopTien_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoLenh", SqlDbType.NVarChar, SoLenh);
			db.AddInParameter(dbCommand, "@NgayPhatLenh", SqlDbType.DateTime, NgayPhatLenh.Year == 1753 ? DBNull.Value : (object) NgayPhatLenh);
			db.AddInParameter(dbCommand, "@TenNguoiNop", SqlDbType.NVarChar, TenNguoiNop);
			db.AddInParameter(dbCommand, "@SoCMNDNguoiNop", SqlDbType.NVarChar, SoCMNDNguoiNop);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@MaDonViNop", SqlDbType.VarChar, MaDonViNop);
			db.AddInParameter(dbCommand, "@TenDonViNop", SqlDbType.NVarChar, TenDonViNop);
			db.AddInParameter(dbCommand, "@SoTKNop", SqlDbType.NVarChar, SoTKNop);
			db.AddInParameter(dbCommand, "@MaNganHangNop", SqlDbType.NVarChar, MaNganHangNop);
			db.AddInParameter(dbCommand, "@TenNganHangNop", SqlDbType.NVarChar, TenNganHangNop);
			db.AddInParameter(dbCommand, "@MaDonViNhan", SqlDbType.VarChar, MaDonViNhan);
			db.AddInParameter(dbCommand, "@TenDonViNhan", SqlDbType.NVarChar, TenDonViNhan);
			db.AddInParameter(dbCommand, "@SoTKNhan", SqlDbType.NVarChar, SoTKNhan);
			db.AddInParameter(dbCommand, "@MaNganHangNhan", SqlDbType.NVarChar, MaNganHangNhan);
			db.AddInParameter(dbCommand, "@TenNganHangNhan", SqlDbType.NVarChar, TenNganHangNhan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@FileDinhKem", SqlDbType.Image, FileDinhKem);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.NVarChar, GuidStr);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<GiayNopTien> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiayNopTien item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteGiayNopTien(long id)
		{
			GiayNopTien entity = new GiayNopTien();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GiayNopTien_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GiayNopTien_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<GiayNopTien> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiayNopTien item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}