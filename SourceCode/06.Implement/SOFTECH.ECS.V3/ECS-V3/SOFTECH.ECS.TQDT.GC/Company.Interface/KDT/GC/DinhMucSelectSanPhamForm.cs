﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;
using Janus.Windows.GridEX;
using System.Windows.Forms;

namespace Company.Interface.KDT.GC
{
    public partial class DinhMucSelectSanPhamForm : BaseForm
    {
        public Company.GC.BLL.GC.SanPham SPDetail;        
        public HopDong HD = new HopDong();     
        public bool isBrower = false;
        public DinhMucSelectSanPhamForm()
        {
            InitializeComponent();
            SPDetail = new  Company.GC.BLL.GC.SanPham();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.SPDetail = null;
            this.Close();
        }



        private void SanPhamGCEditForm_Load(object sender, EventArgs e)
        {            
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            Company.GC.BLL.GC.SanPham sp = new  Company.GC.BLL.GC.SanPham();
            sp.HopDong_ID = HD.ID;
            //dgList.DataSource = sp.SelectCollectionBy_HopDong_ID_ChuaCoDM();
            //KhanhHn 23/06/2012
            dgList.DataSource = sp.SelectCollectionBy_HopDong_ID();
        }

  
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            SPDetail = (Company.GC.BLL.GC.SanPham)e.Row.DataRow;
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
        }

        private void filterEditor1_Click(object sender, EventArgs e)
        {

        }

        
    }
}