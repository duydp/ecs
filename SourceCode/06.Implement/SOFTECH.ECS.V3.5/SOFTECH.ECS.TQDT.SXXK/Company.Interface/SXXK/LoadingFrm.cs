﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface.SXXK
{
    public partial class LoadingFrm : Form
    {
        public LoadingFrm()
        {
            InitializeComponent();
        }
        public  void ShowForm(object form)
        {
            Form f = form as Form;
            this.ShowDialog(f);
        }
        public void CloseForm(object obj)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { this.Close(); }));
            }
            else
                this.Close();
        }
    }
}
