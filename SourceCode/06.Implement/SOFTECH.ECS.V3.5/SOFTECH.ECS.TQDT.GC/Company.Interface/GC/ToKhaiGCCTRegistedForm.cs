﻿using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.SXXK;
using System.IO;
using Company.Interface.KDT.GC;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.GC
{
	public partial class ToKhaiGCCTRegistedForm : BaseForm
	{
        private List<ToKhaiChuyenTiep> tkctCollection = new List<ToKhaiChuyenTiep>();
        private string xmlCurrent = "";
        public ToKhaiGCCTRegistedForm()
		{
			InitializeComponent();
		}

		//-----------------------------------------------------------------------------------------
		
	    private void khoitao_DuLieuChuan()
	    {
	        //string where = string.Format("[ID] LIKE '{0}'", GlobalSettings.MA_CUC_HAI_QUAN);
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
	    }
	   	 

        private void ToKhaiGCCTManagerForm_Load(object sender, EventArgs e)
		{
            txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
            this.Cursor = Cursors.WaitCursor;
            this.khoitao_DuLieuChuan();
            BindHopDong();
		    this.search();
            this.Cursor = Cursors.Default;
        }
        private void BindHopDong()
        {
            Company.GC.BLL.KDT.GC.HopDong hopdong = new Company.GC.BLL.KDT.GC.HopDong();
            DataTable dt;

            string where = string.Format("MaDoanhNghiep='{0}' and TrangThaiXuLy=1", GlobalSettings.MA_DON_VI);
            dt = Company.GC.BLL.KDT.GC.HopDong.SelectDynamic(where, "").Tables[0];

            cbHopDong.DataSource = dt;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            try
            {
                cbHopDong.SelectedIndex = 0;
            }
            catch { }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       
        private ToKhaiChuyenTiep getTKCTByID(long id)
        {
            foreach (ToKhaiChuyenTiep tk in this.tkctCollection)
            {
                if (tk.ID == id) return tk;
            }
            return null;
        }
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Contains("XV"))
                    {
                        VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
                        f.TKMD = this.getTKMDVNACCS(System.Convert.ToInt32(e.Row.Cells["sotokhai"].Value.ToString().Trim()));
                        f.ShowDialog();
                    }
                else if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Contains("NV"))
                {
                    VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
                    f.TKMD = this.getTKMDVNACCS(System.Convert.ToInt32(e.Row.Cells["sotokhai"].Value.ToString().Trim()));
                    f.ShowDialog();
                }
                else
                {
                    ToKhaiGCChuyenTiepNhapForm f = new ToKhaiGCChuyenTiepNhapForm();
                    f.OpenType = OpenFormType.Edit;
                    f.TKCT = (ToKhaiChuyenTiep)e.Row.DataRow;
                    f.TKCT = ToKhaiChuyenTiep.Load(f.TKCT.ID);
                    f.TKCT.LoadHCTCollection();
                    if (f.TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                        f.OpenType = OpenFormType.View;
                    f.ShowDialog();
                }
                search();
            }
        }
        private KDT_VNACC_ToKhaiMauDich getTKMDVNACCS(int Sotokhai)
        {
            KDT_VNACC_ToKhaiMauDich TKVNACC = new KDT_VNACC_ToKhaiMauDich();
            decimal sotokhaiVnacc = CapSoToKhai.GetSoTKVNACCS(Sotokhai);
            TKVNACC = KDT_VNACC_ToKhaiMauDich.Load(KDT_VNACC_ToKhaiMauDich.GetID(sotokhaiVnacc));
            TKVNACC.LoadFull();
            return TKVNACC;
        }
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "1 = 1";
            where += string.Format(" and MaDoanhNghiep='{0}' and TrangThaiXuLy=1", GlobalSettings.MA_DON_VI);

            if (txtSoTiepNhan.TextLength > 0)
            {
                if (txtSoTiepNhan.TextLength==12)
                    where += " AND Huongdan_PL = " + txtSoTiepNhan.Value;
                else
                    where += " AND SoToKhai = " + txtSoTiepNhan.Value;
            }

            if (txtNamTiepNhan.TextLength > 0)
            {
                where += " AND YEAR(NgayDangKy) = " + txtNamTiepNhan.Value;
            }

            where += " AND TrangThaiXuLy = 1";
            long idHopDong = 0;
            try
            {
                idHopDong = Convert.ToInt64(cbHopDong.Value);
            }
            catch
            {
                showMsg("MSG_240205");
                //ShowMessage("Không có hợp đồng này trong hệ thống.", false);
                return;
            }
            if (cbHopDong.Text != "")
                where += string.Format(" And IDHopDong = {0} ", idHopDong);

            // Thực hiện tìm kiếm.            
            this.tkctCollection = ToKhaiChuyenTiep.SelectCollectionDynamic(where, "");
            dgList.DataSource = this.tkctCollection;
            this.setCommandStatus();

            //this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
        }

        private void setCommandStatus()
        {
            
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }
     



        private ToKhaiChuyenTiep search(object id)
        {
            foreach (ToKhaiChuyenTiep tkct in this.tkctCollection)
            {
                if (tkct.ID == Convert.ToInt64(id))
                {
                    return tkct;
                }
            }
            return null;
        }




   
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetName(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
                if (e.Row.Cells["NgayTiepNhan"].Text != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Value);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                }
                //if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim().Contains("V"))
                //{
                //    e.Row.Cells["SoToKhai"].Text = e.Row.Cells["HuongDan_PL"].Value.ToString();
                //}
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //if (showMsg("MSG_DEL01", true) == "Yes")
            ////if (ShowMessage("Bạn có muốn xóa tờ khai này không?", true) == "Yes")
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)i.GetRow().DataRow;
            //            if (tkct.ID > 0)
            //            {
            //                if (tkct.ID_Relation > 0)
            //                {
            //                    tkct.Delete();
            //                }                            

            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    e.Cancel = true;
            //}
        }


        private void inToKhaiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                Report.ReportViewTKCTForm f = new Company.Interface.Report.ReportViewTKCTForm();
                f.TKCT = (ToKhaiChuyenTiep)row.DataRow;
                f.TKCT.LoadHCTCollection();
                f.Show();
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            //if (showMsg("MSG_DEL01", true) == "Yes")
            ////if (ShowMessage("Bạn có muốn xóa tờ khai này không?", true) == "Yes")
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)i.GetRow().DataRow;
            //            if (tkct.ID > 0)
            //            {
            //                if (tkct.ID_Relation > 0)
            //                {
            //                    tkct.Delete();
            //                }

            //            }
            //        }
            //    }
            //    this.search();
            //}
        }


	}
}