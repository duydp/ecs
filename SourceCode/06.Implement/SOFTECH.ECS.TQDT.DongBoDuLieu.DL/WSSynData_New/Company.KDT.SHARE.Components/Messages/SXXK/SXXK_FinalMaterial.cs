﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.SXXK
{
    public class SXXK_FinalMaterial
    {

        [XmlElement("Material")]
        public List<SXXK_Material> Materials { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("FinalGoodsItem")]
        public List<SXXK_Material> FinalGoodsItem { get; set; }



    }
}
