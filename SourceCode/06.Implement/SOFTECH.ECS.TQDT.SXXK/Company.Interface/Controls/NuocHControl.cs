using System;
using System.Data;
using System.Windows.Forms;
using Company.BLL.DuLieuChuan;

namespace Company.Interface.Controls
{
    public partial class NuocHControl : UserControl
    {        
        public NuocHControl()
        {
            InitializeComponent();
        }

        public string Ma
        {
            set
            {
                this.txtMa.Text = value;
                this.cbTen.Value = this.txtMa.Text.PadRight(3);
            }
            get { return this.txtMa.Text; }
        }
        public bool ReadOnly
        {
            set
            {
                this.txtMa.ReadOnly = value;
                this.cbTen.ReadOnly = value;
            }
            get { return this.cbTen.ReadOnly; }
        }
        public string ErrorMessage
        {
            get { return this.rfvTen.ErrorMessage; }
            set { this.rfvTen.ErrorMessage = value; }
        }
        public Janus.Windows.Common.VisualStyleManager VisualStyleManager
        {
            set
            {
                this.txtMa.VisualStyleManager = value;
                this.cbTen.VisualStyleManager = value;
            }
            get
            {
                return this.txtMa.VisualStyleManager;
            }
        }
        public event ValueChangedEventHandler ValueChanged;
        public delegate void ValueChangedEventHandler(object sender, EventArgs e);

        private void loadData()
        {
            if (this.txtMa.Text.Trim().Length == 0) this.txtMa.Text = GlobalSettings.NUOC;
            DataTable dt = Nuoc.SelectAll().Tables[0];
            dtNuoc.Rows.Clear();
            foreach (DataRow row in dt.Rows)
            {
                this.dtNuoc.ImportRow(row);
            }
            this.cbTen.Value = this.txtMa.Text.PadRight(3);
        }

        private void txtMa_Leave(object sender, EventArgs e)
        {
            this.txtMa.Text = this.txtMa.Text.Trim();
            if (this.dtNuoc.Select("ID='" + txtMa.Text.Trim() + "'").Length == 0)
            {
                this.txtMa.Text = GlobalSettings.NUOC;
            }
            this.cbTen.Value = this.txtMa.Text.PadRight(3);
        }

        private void NuocControl_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.loadData();
            }
        }

        private void cbTen_ValueChanged(object sender, EventArgs e)
        {
            this.txtMa.Text = this.cbTen.Value.ToString().Trim();
            if (this.ValueChanged != null) this.ValueChanged(sender, e);
        }
    }
}