namespace Company.Interface.Report.VNACCS
{
    partial class BanXacNhanNoiDungToKhaiHangHoaXuatKhau_3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lblDieukhoanmien = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMamiengiam = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMavanvanphapquy5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMavanvanphapquy4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMavanvanphapquy3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMavanvanphapquy2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMavanvanphapquy1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadvtienbaohiem = new DevExpress.XtraReports.UI.XRLabel();
            this.lblKhoantienbaohiem = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoluongtienbaohiem = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadvtienlephi = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblKhoantienlephi = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoluongtienlephi = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTienbaohiem = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTienlephi = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSodongmienthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDanhmucmienthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSodonghangtaixuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMatientesotiengiamthue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMatientesotienthue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotienmiengiam = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotienthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloainhapthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThuesuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMatientedongiathue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDongiathue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDonvisoluongthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadvtinhchuanthue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoluongthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadongtienM = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTrigiathueM = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadongtienS = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTrigiathueS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTrigia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDonvidongia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadongtien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDongia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadvtinh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadvtinh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoluong2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoluong1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMotahanghoa = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMataixacnhangia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaquanlyrieng = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMasohanghoa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSodong1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.lblGiothaydoidangky = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaythaydoidangky = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNhomxulyhoso = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMahieuphuongthucvanchuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaphanloaihanghoa = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaloaihinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMasothuedaidien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongsotokhaichianho = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSonhanhtokhaichianho = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBieuthitruonghophethan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotokhaidautien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGiodangky = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThoihantainhapxuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaydangky = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTencoquanhaiquantiepnhantokhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotokhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotokhaitamnhaptaixuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaphanloaikiemtra = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDieukhoanmien
            // 
            this.lblDieukhoanmien.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDieukhoanmien.LocationFloat = new DevExpress.Utils.PointFloat(118.9121F, 238.3557F);
            this.lblDieukhoanmien.Name = "lblDieukhoanmien";
            this.lblDieukhoanmien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDieukhoanmien.SizeF = new System.Drawing.SizeF(511.907F, 11.00002F);
            this.lblDieukhoanmien.StylePriority.UseFont = false;
            this.lblDieukhoanmien.StylePriority.UseTextAlignment = false;
            this.lblDieukhoanmien.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieukhoanmien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMamiengiam
            // 
            this.lblMamiengiam.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMamiengiam.LocationFloat = new DevExpress.Utils.PointFloat(81.08405F, 238.3557F);
            this.lblMamiengiam.Name = "lblMamiengiam";
            this.lblMamiengiam.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMamiengiam.SizeF = new System.Drawing.SizeF(37.82806F, 11.00002F);
            this.lblMamiengiam.StylePriority.UseFont = false;
            this.lblMamiengiam.StylePriority.UseTextAlignment = false;
            this.lblMamiengiam.Text = "XXXE";
            this.lblMamiengiam.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel49
            // 
            this.xrLabel49.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(9.999719F, 227.3557F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(260.5471F, 10.99998F);
            this.xrLabel49.StylePriority.UseFont = false;
            this.xrLabel49.StylePriority.UseTextAlignment = false;
            this.xrLabel49.Text = "Miễn / Giảm / Không chịu thuế xuất khẩu";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMavanvanphapquy5
            // 
            this.lblMavanvanphapquy5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMavanvanphapquy5.LocationFloat = new DevExpress.Utils.PointFloat(453.4862F, 216.3558F);
            this.lblMavanvanphapquy5.Name = "lblMavanvanphapquy5";
            this.lblMavanvanphapquy5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMavanvanphapquy5.SizeF = new System.Drawing.SizeF(38.34088F, 11.00002F);
            this.lblMavanvanphapquy5.StylePriority.UseFont = false;
            this.lblMavanvanphapquy5.StylePriority.UseTextAlignment = false;
            this.lblMavanvanphapquy5.Text = "XE";
            this.lblMavanvanphapquy5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel58
            // 
            this.xrLabel58.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(431.7321F, 216.3558F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(21.75414F, 11.00003F);
            this.xrLabel58.StylePriority.UseFont = false;
            this.xrLabel58.StylePriority.UseTextAlignment = false;
            this.xrLabel58.Text = "5";
            this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMavanvanphapquy4
            // 
            this.lblMavanvanphapquy4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMavanvanphapquy4.LocationFloat = new DevExpress.Utils.PointFloat(393.3911F, 216.3556F);
            this.lblMavanvanphapquy4.Name = "lblMavanvanphapquy4";
            this.lblMavanvanphapquy4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMavanvanphapquy4.SizeF = new System.Drawing.SizeF(38.34088F, 11.00002F);
            this.lblMavanvanphapquy4.StylePriority.UseFont = false;
            this.lblMavanvanphapquy4.StylePriority.UseTextAlignment = false;
            this.lblMavanvanphapquy4.Text = "XE";
            this.lblMavanvanphapquy4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(371.637F, 216.3558F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(21.75414F, 11.00003F);
            this.xrLabel56.StylePriority.UseFont = false;
            this.xrLabel56.StylePriority.UseTextAlignment = false;
            this.xrLabel56.Text = "4";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMavanvanphapquy3
            // 
            this.lblMavanvanphapquy3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMavanvanphapquy3.LocationFloat = new DevExpress.Utils.PointFloat(333.2961F, 216.3556F);
            this.lblMavanvanphapquy3.Name = "lblMavanvanphapquy3";
            this.lblMavanvanphapquy3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMavanvanphapquy3.SizeF = new System.Drawing.SizeF(38.34088F, 11.00002F);
            this.lblMavanvanphapquy3.StylePriority.UseFont = false;
            this.lblMavanvanphapquy3.StylePriority.UseTextAlignment = false;
            this.lblMavanvanphapquy3.Text = "XE";
            this.lblMavanvanphapquy3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(311.5419F, 216.3557F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(21.75414F, 11.00003F);
            this.xrLabel54.StylePriority.UseFont = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = "3";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMavanvanphapquy2
            // 
            this.lblMavanvanphapquy2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMavanvanphapquy2.LocationFloat = new DevExpress.Utils.PointFloat(273.2011F, 216.3557F);
            this.lblMavanvanphapquy2.Name = "lblMavanvanphapquy2";
            this.lblMavanvanphapquy2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMavanvanphapquy2.SizeF = new System.Drawing.SizeF(38.34088F, 11.00002F);
            this.lblMavanvanphapquy2.StylePriority.UseFont = false;
            this.lblMavanvanphapquy2.StylePriority.UseTextAlignment = false;
            this.lblMavanvanphapquy2.Text = "XE";
            this.lblMavanvanphapquy2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(251.4469F, 216.3556F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(21.75414F, 11.00003F);
            this.xrLabel52.StylePriority.UseFont = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "2";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMavanvanphapquy1
            // 
            this.lblMavanvanphapquy1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMavanvanphapquy1.LocationFloat = new DevExpress.Utils.PointFloat(213.106F, 216.3556F);
            this.lblMavanvanphapquy1.Name = "lblMavanvanphapquy1";
            this.lblMavanvanphapquy1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMavanvanphapquy1.SizeF = new System.Drawing.SizeF(38.34088F, 11.00002F);
            this.lblMavanvanphapquy1.StylePriority.UseFont = false;
            this.lblMavanvanphapquy1.StylePriority.UseTextAlignment = false;
            this.lblMavanvanphapquy1.Text = "XE";
            this.lblMavanvanphapquy1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(191.3518F, 216.3557F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(21.75414F, 11.00003F);
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.StylePriority.UseTextAlignment = false;
            this.xrLabel47.Text = "1";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(9.999984F, 216.3557F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(181.3519F, 11.00002F);
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.Text = "Mã văn bản pháp luật khác";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(590.712F, 205.3557F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(55.22192F, 11F);
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.Text = "VND";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel50
            // 
            this.xrLabel50.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(270.5767F, 205.356F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(38.34088F, 11.00002F);
            this.xrLabel50.StylePriority.UseFont = false;
            this.xrLabel50.StylePriority.UseTextAlignment = false;
            this.xrLabel50.Text = "VND";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel48
            // 
            this.xrLabel48.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(405.6929F, 205.3556F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(63.90463F, 11F);
            this.xrLabel48.StylePriority.UseFont = false;
            this.xrLabel48.StylePriority.UseTextAlignment = false;
            this.xrLabel48.Text = "Khoản tiền";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblMadvtienbaohiem
            // 
            this.lblMadvtienbaohiem.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadvtienbaohiem.LocationFloat = new DevExpress.Utils.PointFloat(590.712F, 194.3559F);
            this.lblMadvtienbaohiem.Name = "lblMadvtienbaohiem";
            this.lblMadvtienbaohiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadvtienbaohiem.SizeF = new System.Drawing.SizeF(55.22192F, 10.99997F);
            this.lblMadvtienbaohiem.StylePriority.UseFont = false;
            this.lblMadvtienbaohiem.StylePriority.UseTextAlignment = false;
            this.lblMadvtienbaohiem.Text = "XXXE";
            this.lblMadvtienbaohiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblKhoantienbaohiem
            // 
            this.lblKhoantienbaohiem.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKhoantienbaohiem.LocationFloat = new DevExpress.Utils.PointFloat(469.5976F, 205.3556F);
            this.lblKhoantienbaohiem.Name = "lblKhoantienbaohiem";
            this.lblKhoantienbaohiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblKhoantienbaohiem.SizeF = new System.Drawing.SizeF(121.1147F, 11.00005F);
            this.lblKhoantienbaohiem.StylePriority.UseFont = false;
            this.lblKhoantienbaohiem.StylePriority.UseTextAlignment = false;
            this.lblKhoantienbaohiem.Text = "1,234,567,890,123,456";
            this.lblKhoantienbaohiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel44
            // 
            this.xrLabel44.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(418.2586F, 194.3558F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(50.9938F, 11F);
            this.xrLabel44.StylePriority.UseFont = false;
            this.xrLabel44.StylePriority.UseTextAlignment = false;
            this.xrLabel44.Text = "Số lượng";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblSoluongtienbaohiem
            // 
            this.lblSoluongtienbaohiem.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoluongtienbaohiem.LocationFloat = new DevExpress.Utils.PointFloat(469.2524F, 194.3558F);
            this.lblSoluongtienbaohiem.Name = "lblSoluongtienbaohiem";
            this.lblSoluongtienbaohiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoluongtienbaohiem.SizeF = new System.Drawing.SizeF(121.4598F, 11.00005F);
            this.lblSoluongtienbaohiem.StylePriority.UseFont = false;
            this.lblSoluongtienbaohiem.StylePriority.UseTextAlignment = false;
            this.lblSoluongtienbaohiem.Text = "123,456,789,012";
            this.lblSoluongtienbaohiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMadvtienlephi
            // 
            this.lblMadvtienlephi.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadvtienlephi.LocationFloat = new DevExpress.Utils.PointFloat(270.5468F, 194.3559F);
            this.lblMadvtienlephi.Name = "lblMadvtienlephi";
            this.lblMadvtienlephi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadvtienlephi.SizeF = new System.Drawing.SizeF(60.59863F, 11.00002F);
            this.lblMadvtienlephi.StylePriority.UseFont = false;
            this.lblMadvtienlephi.StylePriority.UseTextAlignment = false;
            this.lblMadvtienlephi.Text = "XXXE";
            this.lblMadvtienlephi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(53.17029F, 205.3557F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(83.15601F, 11.00002F);
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "Khoản tiền";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblKhoantienlephi
            // 
            this.lblKhoantienlephi.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKhoantienlephi.LocationFloat = new DevExpress.Utils.PointFloat(136.3263F, 205.3557F);
            this.lblKhoantienlephi.Name = "lblKhoantienlephi";
            this.lblKhoantienlephi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblKhoantienlephi.SizeF = new System.Drawing.SizeF(134.2205F, 11.00005F);
            this.lblKhoantienlephi.StylePriority.UseFont = false;
            this.lblKhoantienlephi.StylePriority.UseTextAlignment = false;
            this.lblKhoantienlephi.Text = "1,234,567,890,123,456";
            this.lblKhoantienlephi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblSoluongtienlephi
            // 
            this.lblSoluongtienlephi.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoluongtienlephi.LocationFloat = new DevExpress.Utils.PointFloat(136.3262F, 194.3556F);
            this.lblSoluongtienlephi.Name = "lblSoluongtienlephi";
            this.lblSoluongtienlephi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoluongtienlephi.SizeF = new System.Drawing.SizeF(134.2206F, 11.00005F);
            this.lblSoluongtienlephi.StylePriority.UseFont = false;
            this.lblSoluongtienlephi.StylePriority.UseTextAlignment = false;
            this.lblSoluongtienlephi.Text = "123,456,789,012";
            this.lblSoluongtienlephi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(85.33252F, 194.3556F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(50.99378F, 11.00003F);
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.Text = "Số lượng";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblTienbaohiem
            // 
            this.lblTienbaohiem.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTienbaohiem.LocationFloat = new DevExpress.Utils.PointFloat(469.2524F, 183.3558F);
            this.lblTienbaohiem.Name = "lblTienbaohiem";
            this.lblTienbaohiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienbaohiem.SizeF = new System.Drawing.SizeF(176.6816F, 11.00003F);
            this.lblTienbaohiem.StylePriority.UseFont = false;
            this.lblTienbaohiem.StylePriority.UseTextAlignment = false;
            this.lblTienbaohiem.Text = "XXXXXXXXX1XXXXXXXXX2E";
            this.lblTienbaohiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel42
            // 
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(418.2586F, 183.3555F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(50.99378F, 11.00003F);
            this.xrLabel42.StylePriority.UseFont = false;
            this.xrLabel42.StylePriority.UseTextAlignment = false;
            this.xrLabel42.Text = "Đơn giá";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel41
            // 
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(321.8345F, 183.3558F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(96.42413F, 11.00005F);
            this.xrLabel41.StylePriority.UseFont = false;
            this.xrLabel41.StylePriority.UseTextAlignment = false;
            this.xrLabel41.Text = "Tiền bảo hiểm";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTienlephi
            // 
            this.lblTienlephi.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTienlephi.LocationFloat = new DevExpress.Utils.PointFloat(136.3263F, 183.3556F);
            this.lblTienlephi.Name = "lblTienlephi";
            this.lblTienlephi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienlephi.SizeF = new System.Drawing.SizeF(176.6816F, 11.00003F);
            this.lblTienlephi.StylePriority.UseFont = false;
            this.lblTienlephi.StylePriority.UseTextAlignment = false;
            this.lblTienlephi.Text = "XXXXXXXXX1XXXXXXXXX2E";
            this.lblTienlephi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(85.33252F, 183.3556F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(50.99378F, 11.00003F);
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "Đơn giá";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(9.999719F, 183.3556F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(75.33281F, 11.00002F);
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "Tiền lệ phí";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblSodongmienthue
            // 
            this.lblSodongmienthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSodongmienthue.LocationFloat = new DevExpress.Utils.PointFloat(310.8087F, 172.3557F);
            this.lblSodongmienthue.Name = "lblSodongmienthue";
            this.lblSodongmienthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSodongmienthue.SizeF = new System.Drawing.SizeF(33.33453F, 10.99996F);
            this.lblSodongmienthue.StylePriority.UseFont = false;
            this.lblSodongmienthue.StylePriority.UseTextAlignment = false;
            this.lblSodongmienthue.Text = "XXE";
            this.lblSodongmienthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(299.4907F, 172.3556F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(11.31796F, 11.00001F);
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "-";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblDanhmucmienthue
            // 
            this.lblDanhmucmienthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDanhmucmienthue.LocationFloat = new DevExpress.Utils.PointFloat(193.5422F, 172.3556F);
            this.lblDanhmucmienthue.Name = "lblDanhmucmienthue";
            this.lblDanhmucmienthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDanhmucmienthue.SizeF = new System.Drawing.SizeF(105.9485F, 11.00002F);
            this.lblDanhmucmienthue.StylePriority.UseFont = false;
            this.lblDanhmucmienthue.StylePriority.UseTextAlignment = false;
            this.lblDanhmucmienthue.Text = "XXXXXXXXX1XE";
            this.lblDanhmucmienthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(9.999719F, 172.3556F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(183.5424F, 11F);
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "Danh mục miễn thuế xuất khẩu";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblSodonghangtaixuat
            // 
            this.lblSodonghangtaixuat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSodonghangtaixuat.LocationFloat = new DevExpress.Utils.PointFloat(373.6062F, 161.814F);
            this.lblSodonghangtaixuat.Name = "lblSodonghangtaixuat";
            this.lblSodonghangtaixuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSodonghangtaixuat.SizeF = new System.Drawing.SizeF(44.65247F, 10.54161F);
            this.lblSodonghangtaixuat.StylePriority.UseFont = false;
            this.lblSodonghangtaixuat.StylePriority.UseTextAlignment = false;
            this.lblSodonghangtaixuat.Text = "XE";
            this.lblSodonghangtaixuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(9.999984F, 161.3556F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(363.6065F, 11.00002F);
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMatientesotiengiamthue
            // 
            this.lblMatientesotiengiamthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatientesotiengiamthue.LocationFloat = new DevExpress.Utils.PointFloat(373.6062F, 150.3556F);
            this.lblMatientesotiengiamthue.Name = "lblMatientesotiengiamthue";
            this.lblMatientesotiengiamthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMatientesotiengiamthue.SizeF = new System.Drawing.SizeF(44.65247F, 11.45833F);
            this.lblMatientesotiengiamthue.StylePriority.UseFont = false;
            this.lblMatientesotiengiamthue.StylePriority.UseTextAlignment = false;
            this.lblMatientesotiengiamthue.Text = "XXE";
            this.lblMatientesotiengiamthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMatientesotienthue
            // 
            this.lblMatientesotienthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatientesotienthue.LocationFloat = new DevExpress.Utils.PointFloat(373.6062F, 138.8973F);
            this.lblMatientesotienthue.Name = "lblMatientesotienthue";
            this.lblMatientesotienthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMatientesotienthue.SizeF = new System.Drawing.SizeF(44.65247F, 11.45833F);
            this.lblMatientesotienthue.StylePriority.UseFont = false;
            this.lblMatientesotienthue.StylePriority.UseTextAlignment = false;
            this.lblMatientesotienthue.Text = "XXE";
            this.lblMatientesotienthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSotienmiengiam
            // 
            this.lblSotienmiengiam.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotienmiengiam.LocationFloat = new DevExpress.Utils.PointFloat(161.3563F, 150.3556F);
            this.lblSotienmiengiam.Name = "lblSotienmiengiam";
            this.lblSotienmiengiam.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotienmiengiam.SizeF = new System.Drawing.SizeF(212.2499F, 11F);
            this.lblSotienmiengiam.StylePriority.UseFont = false;
            this.lblSotienmiengiam.StylePriority.UseTextAlignment = false;
            this.lblSotienmiengiam.Text = "1,234,567,890,123,456";
            this.lblSotienmiengiam.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblSotienthue
            // 
            this.lblSotienthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotienthue.LocationFloat = new DevExpress.Utils.PointFloat(161.3562F, 138.8973F);
            this.lblSotienthue.Name = "lblSotienthue";
            this.lblSotienthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotienthue.SizeF = new System.Drawing.SizeF(212.2499F, 11.45833F);
            this.lblSotienthue.StylePriority.UseFont = false;
            this.lblSotienthue.StylePriority.UseTextAlignment = false;
            this.lblSotienthue.Text = "1,234,567,890,123,456";
            this.lblSotienthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(9.999984F, 150.3556F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(151.3562F, 11F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "Số tiền miễn giảm";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(9.999984F, 139.3557F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(151.3562F, 11.00002F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "Số tiền thuế";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblPhanloainhapthue
            // 
            this.lblPhanloainhapthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloainhapthue.LocationFloat = new DevExpress.Utils.PointFloat(384.9241F, 127.8974F);
            this.lblPhanloainhapthue.Name = "lblPhanloainhapthue";
            this.lblPhanloainhapthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloainhapthue.SizeF = new System.Drawing.SizeF(33.33453F, 10.99996F);
            this.lblPhanloainhapthue.StylePriority.UseFont = false;
            this.lblPhanloainhapthue.StylePriority.UseTextAlignment = false;
            this.lblPhanloainhapthue.Text = "[X]";
            this.lblPhanloainhapthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(373.6062F, 127.8973F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(11.31796F, 11.00001F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "-";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblThuesuat
            // 
            this.lblThuesuat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThuesuat.LocationFloat = new DevExpress.Utils.PointFloat(115.6703F, 127.8973F);
            this.lblThuesuat.Name = "lblThuesuat";
            this.lblThuesuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThuesuat.SizeF = new System.Drawing.SizeF(257.9361F, 11F);
            this.lblThuesuat.StylePriority.UseFont = false;
            this.lblThuesuat.StylePriority.UseTextAlignment = false;
            this.lblThuesuat.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXXE";
            this.lblThuesuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(9.999984F, 127.8973F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(105.6704F, 11F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "Thuế suất";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMatientedongiathue
            // 
            this.lblMatientedongiathue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatientedongiathue.LocationFloat = new DevExpress.Utils.PointFloat(545.6934F, 116.8973F);
            this.lblMatientedongiathue.Name = "lblMatientedongiathue";
            this.lblMatientedongiathue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMatientedongiathue.SizeF = new System.Drawing.SizeF(36.34955F, 11F);
            this.lblMatientedongiathue.StylePriority.UseFont = false;
            this.lblMatientedongiathue.StylePriority.UseTextAlignment = false;
            this.lblMatientedongiathue.Text = "XXE";
            this.lblMatientedongiathue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblDongiathue
            // 
            this.lblDongiathue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDongiathue.LocationFloat = new DevExpress.Utils.PointFloat(422.4385F, 116.8973F);
            this.lblDongiathue.Name = "lblDongiathue";
            this.lblDongiathue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDongiathue.SizeF = new System.Drawing.SizeF(123.2549F, 11.00005F);
            this.lblDongiathue.StylePriority.UseFont = false;
            this.lblDongiathue.StylePriority.UseTextAlignment = false;
            this.lblDongiathue.Text = "123,456,789,012,345,678";
            this.lblDongiathue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblDonvisoluongthue
            // 
            this.lblDonvisoluongthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDonvisoluongthue.LocationFloat = new DevExpress.Utils.PointFloat(582.0429F, 116.8973F);
            this.lblDonvisoluongthue.Name = "lblDonvisoluongthue";
            this.lblDonvisoluongthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonvisoluongthue.SizeF = new System.Drawing.SizeF(55.59216F, 10.99998F);
            this.lblDonvisoluongthue.StylePriority.UseFont = false;
            this.lblDonvisoluongthue.StylePriority.UseTextAlignment = false;
            this.lblDonvisoluongthue.Text = "XXXE";
            this.lblDonvisoluongthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(316.7681F, 116.8973F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(105.6704F, 11F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "Đơn giá tính thuế";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMadvtinhchuanthue
            // 
            this.lblMadvtinhchuanthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadvtinhchuanthue.LocationFloat = new DevExpress.Utils.PointFloat(238.1704F, 116.8973F);
            this.lblMadvtinhchuanthue.Name = "lblMadvtinhchuanthue";
            this.lblMadvtinhchuanthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadvtinhchuanthue.SizeF = new System.Drawing.SizeF(54.18147F, 10.99999F);
            this.lblMadvtinhchuanthue.StylePriority.UseFont = false;
            this.lblMadvtinhchuanthue.StylePriority.UseTextAlignment = false;
            this.lblMadvtinhchuanthue.Text = "XXXE";
            this.lblMadvtinhchuanthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblSoluongthue
            // 
            this.lblSoluongthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoluongthue.LocationFloat = new DevExpress.Utils.PointFloat(115.6703F, 116.8973F);
            this.lblSoluongthue.Name = "lblSoluongthue";
            this.lblSoluongthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoluongthue.SizeF = new System.Drawing.SizeF(122.5F, 11F);
            this.lblSoluongthue.StylePriority.UseFont = false;
            this.lblSoluongthue.StylePriority.UseTextAlignment = false;
            this.lblSoluongthue.Text = "123,456,789,012";
            this.lblSoluongthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(9.999984F, 116.8973F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(105.6704F, 11F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Số lượng tính thuế";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(451.7319F, 105.8973F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(11.31796F, 11.00001F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "-";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMadongtienM
            // 
            this.lblMadongtienM.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadongtienM.LocationFloat = new DevExpress.Utils.PointFloat(422.4385F, 105.8973F);
            this.lblMadongtienM.Name = "lblMadongtienM";
            this.lblMadongtienM.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadongtienM.SizeF = new System.Drawing.SizeF(29.29349F, 11F);
            this.lblMadongtienM.StylePriority.UseFont = false;
            this.lblMadongtienM.StylePriority.UseTextAlignment = false;
            this.lblMadongtienM.Text = "XXE";
            this.lblMadongtienM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblTrigiathueM
            // 
            this.lblTrigiathueM.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrigiathueM.LocationFloat = new DevExpress.Utils.PointFloat(463.0499F, 105.8973F);
            this.lblTrigiathueM.Name = "lblTrigiathueM";
            this.lblTrigiathueM.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrigiathueM.SizeF = new System.Drawing.SizeF(174.5852F, 11.00001F);
            this.lblTrigiathueM.StylePriority.UseFont = false;
            this.lblTrigiathueM.StylePriority.UseTextAlignment = false;
            this.lblTrigiathueM.Text = "12,345,678,901,234,567,890";
            this.lblTrigiathueM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(316.7681F, 105.8973F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(105.6704F, 11F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Trị giá tính thuế(M)";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMadongtienS
            // 
            this.lblMadongtienS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadongtienS.LocationFloat = new DevExpress.Utils.PointFloat(238.1704F, 105.8973F);
            this.lblMadongtienS.Name = "lblMadongtienS";
            this.lblMadongtienS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadongtienS.SizeF = new System.Drawing.SizeF(54.18147F, 10.99999F);
            this.lblMadongtienS.StylePriority.UseFont = false;
            this.lblMadongtienS.StylePriority.UseTextAlignment = false;
            this.lblMadongtienS.Text = "XXE";
            this.lblMadongtienS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblTrigiathueS
            // 
            this.lblTrigiathueS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrigiathueS.LocationFloat = new DevExpress.Utils.PointFloat(115.6703F, 105.8973F);
            this.lblTrigiathueS.Name = "lblTrigiathueS";
            this.lblTrigiathueS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrigiathueS.SizeF = new System.Drawing.SizeF(122.5F, 11F);
            this.lblTrigiathueS.StylePriority.UseFont = false;
            this.lblTrigiathueS.StylePriority.UseTextAlignment = false;
            this.lblTrigiathueS.Text = "12,345,678,901,234,567";
            this.lblTrigiathueS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(9.999984F, 105.8973F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(105.6704F, 11F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Trị giá tính thuế(S)";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 94.89731F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(108.6268F, 11.00001F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Thuế xuất khẩu";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblTrigia
            // 
            this.lblTrigia.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrigia.LocationFloat = new DevExpress.Utils.PointFloat(108.6268F, 83.89729F);
            this.lblTrigia.Name = "lblTrigia";
            this.lblTrigia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrigia.SizeF = new System.Drawing.SizeF(207.9933F, 10.99998F);
            this.lblTrigia.StylePriority.UseFont = false;
            this.lblTrigia.StylePriority.UseTextAlignment = false;
            this.lblTrigia.Text = "12,345,678,901,234,567,890";
            this.lblTrigia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(0.1591555F, 83.60275F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(108.4677F, 10.99998F);
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "Trị giá hóa đơn";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblDonvidongia
            // 
            this.lblDonvidongia.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDonvidongia.LocationFloat = new DevExpress.Utils.PointFloat(566.7152F, 83.89726F);
            this.lblDonvidongia.Name = "lblDonvidongia";
            this.lblDonvidongia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonvidongia.SizeF = new System.Drawing.SizeF(59.35175F, 11.00002F);
            this.lblDonvidongia.StylePriority.UseFont = false;
            this.lblDonvidongia.StylePriority.UseTextAlignment = false;
            this.lblDonvidongia.Text = "XXXE";
            this.lblDonvidongia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(547.7272F, 83.89729F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(18.98816F, 11.00002F);
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "-";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMadongtien
            // 
            this.lblMadongtien.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadongtien.LocationFloat = new DevExpress.Utils.PointFloat(501.322F, 83.89735F);
            this.lblMadongtien.Name = "lblMadongtien";
            this.lblMadongtien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadongtien.SizeF = new System.Drawing.SizeF(46.40524F, 11.00002F);
            this.lblMadongtien.StylePriority.UseFont = false;
            this.lblMadongtien.StylePriority.UseTextAlignment = false;
            this.lblMadongtien.Text = "XXE";
            this.lblMadongtien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(482.3338F, 83.89729F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(18.98819F, 11.00002F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "-";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblDongia
            // 
            this.lblDongia.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDongia.LocationFloat = new DevExpress.Utils.PointFloat(407.63F, 83.89729F);
            this.lblDongia.Name = "lblDongia";
            this.lblDongia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDongia.SizeF = new System.Drawing.SizeF(74.70377F, 11.00002F);
            this.lblDongia.StylePriority.UseFont = false;
            this.lblDongia.StylePriority.UseTextAlignment = false;
            this.lblDongia.Text = "123,456,789";
            this.lblDongia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(316.9386F, 83.60269F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(90.69141F, 11F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "Đơn giá hóa đơn";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMadvtinh2
            // 
            this.lblMadvtinh2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadvtinh2.LocationFloat = new DevExpress.Utils.PointFloat(526.5977F, 72.60268F);
            this.lblMadvtinh2.Name = "lblMadvtinh2";
            this.lblMadvtinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadvtinh2.SizeF = new System.Drawing.SizeF(55.61572F, 10.99998F);
            this.lblMadvtinh2.StylePriority.UseFont = false;
            this.lblMadvtinh2.StylePriority.UseTextAlignment = false;
            this.lblMadvtinh2.Text = "XXXE";
            this.lblMadvtinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMadvtinh1
            // 
            this.lblMadvtinh1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadvtinh1.LocationFloat = new DevExpress.Utils.PointFloat(526.5977F, 61.6027F);
            this.lblMadvtinh1.Name = "lblMadvtinh1";
            this.lblMadvtinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadvtinh1.SizeF = new System.Drawing.SizeF(55.61572F, 10.99999F);
            this.lblMadvtinh1.StylePriority.UseFont = false;
            this.lblMadvtinh1.StylePriority.UseTextAlignment = false;
            this.lblMadvtinh1.Text = "XXXE";
            this.lblMadvtinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblSoluong2
            // 
            this.lblSoluong2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoluong2.LocationFloat = new DevExpress.Utils.PointFloat(407.63F, 72.60268F);
            this.lblSoluong2.Name = "lblSoluong2";
            this.lblSoluong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoluong2.SizeF = new System.Drawing.SizeF(118.9676F, 11.00001F);
            this.lblSoluong2.StylePriority.UseFont = false;
            this.lblSoluong2.StylePriority.UseTextAlignment = false;
            this.lblSoluong2.Text = "123,456,789,012";
            this.lblSoluong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblSoluong1
            // 
            this.lblSoluong1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoluong1.LocationFloat = new DevExpress.Utils.PointFloat(407.6297F, 61.6027F);
            this.lblSoluong1.Name = "lblSoluong1";
            this.lblSoluong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoluong1.SizeF = new System.Drawing.SizeF(118.968F, 11.00001F);
            this.lblSoluong1.StylePriority.UseFont = false;
            this.lblSoluong1.StylePriority.UseTextAlignment = false;
            this.lblSoluong1.Text = "123,456,789,012";
            this.lblSoluong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(316.9386F, 72.60268F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(90.69141F, 11.00001F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "Số lượng (2)";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(316.9386F, 61.6027F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(90.69141F, 11.00001F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "Số lượng (1)";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMotahanghoa
            // 
            this.lblMotahanghoa.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMotahanghoa.LocationFloat = new DevExpress.Utils.PointFloat(125.8408F, 27.52508F);
            this.lblMotahanghoa.Multiline = true;
            this.lblMotahanghoa.Name = "lblMotahanghoa";
            this.lblMotahanghoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMotahanghoa.SizeF = new System.Drawing.SizeF(577.1592F, 34.07761F);
            this.lblMotahanghoa.StylePriority.UseFont = false;
            this.lblMotahanghoa.StylePriority.UseTextAlignment = false;
            this.lblMotahanghoa.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6X" +
                "XXXXXXXX7XXXXXXXXX8XXXXXXXXX9XXXXXXXXXE";
            this.lblMotahanghoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMataixacnhangia
            // 
            this.lblMataixacnhangia.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMataixacnhangia.LocationFloat = new DevExpress.Utils.PointFloat(590.8826F, 15.44752F);
            this.lblMataixacnhangia.Name = "lblMataixacnhangia";
            this.lblMataixacnhangia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMataixacnhangia.SizeF = new System.Drawing.SizeF(33.33453F, 12.07759F);
            this.lblMataixacnhangia.StylePriority.UseFont = false;
            this.lblMataixacnhangia.StylePriority.UseTextAlignment = false;
            this.lblMataixacnhangia.Text = "[X]";
            this.lblMataixacnhangia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(432.8038F, 15.44752F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(158.0789F, 12.07759F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "Mã phân loại tái xác nhận giá";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMaquanlyrieng
            // 
            this.lblMaquanlyrieng.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaquanlyrieng.LocationFloat = new DevExpress.Utils.PointFloat(351.5492F, 15.44752F);
            this.lblMaquanlyrieng.Name = "lblMaquanlyrieng";
            this.lblMaquanlyrieng.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaquanlyrieng.SizeF = new System.Drawing.SizeF(81.25454F, 12.07759F);
            this.lblMaquanlyrieng.StylePriority.UseFont = false;
            this.lblMaquanlyrieng.StylePriority.UseTextAlignment = false;
            this.lblMaquanlyrieng.Text = "XXXXXXE";
            this.lblMaquanlyrieng.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(250.7084F, 15.44752F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(100.8408F, 12.07759F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Mã quản lý riêng";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMasohanghoa
            // 
            this.lblMasohanghoa.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMasohanghoa.LocationFloat = new DevExpress.Utils.PointFloat(125.8408F, 15.44752F);
            this.lblMasohanghoa.Name = "lblMasohanghoa";
            this.lblMasohanghoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMasohanghoa.SizeF = new System.Drawing.SizeF(124.8676F, 12.07759F);
            this.lblMasohanghoa.StylePriority.UseFont = false;
            this.lblMasohanghoa.StylePriority.UseTextAlignment = false;
            this.lblMasohanghoa.Text = "XXXXXXXXX1XE";
            this.lblMasohanghoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(27.37229F, 27.52508F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(98.46851F, 12.07759F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Mô tả hàng hóa";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(27.37229F, 15.44752F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(98.46853F, 12.07759F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Mã số hàng hóa";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblSodong1
            // 
            this.lblSodong1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSodong1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.369931F);
            this.lblSodong1.Name = "lblSodong1";
            this.lblSodong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSodong1.SizeF = new System.Drawing.SizeF(39.25454F, 12.07759F);
            this.lblSodong1.StylePriority.UseFont = false;
            this.lblSodong1.StylePriority.UseTextAlignment = false;
            this.lblSodong1.Text = "<XE>";
            this.lblSodong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblGiothaydoidangky,
            this.lblNgaythaydoidangky,
            this.lblNhomxulyhoso,
            this.lblMahieuphuongthucvanchuyen,
            this.lblMaphanloaihanghoa,
            this.lblMaloaihinh,
            this.lblMasothuedaidien,
            this.xrLabel15,
            this.xrLabel11,
            this.xrLabel12,
            this.xrLabel14,
            this.lblTongsotokhaichianho,
            this.xrLabel10,
            this.lblSonhanhtokhaichianho,
            this.xrLabel13,
            this.lblBieuthitruonghophethan,
            this.lblSotokhaidautien,
            this.xrLabel8,
            this.lblGiodangky,
            this.lblThoihantainhapxuat,
            this.xrLabel7,
            this.lblNgaydangky,
            this.xrLabel6,
            this.lblTencoquanhaiquantiepnhantokhai,
            this.lblSotokhai,
            this.lblSotokhaitamnhaptaixuat,
            this.xrLabel5,
            this.xrLabel9,
            this.xrLabel4,
            this.xrLabel1,
            this.xrLabel3,
            this.lblMaphanloaikiemtra,
            this.xrLabel2});
            this.TopMargin.HeightF = 148.1921F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGiothaydoidangky
            // 
            this.lblGiothaydoidangky.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiothaydoidangky.LocationFloat = new DevExpress.Utils.PointFloat(620.2838F, 92.73134F);
            this.lblGiothaydoidangky.Name = "lblGiothaydoidangky";
            this.lblGiothaydoidangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiothaydoidangky.SizeF = new System.Drawing.SizeF(60.71606F, 11F);
            this.lblGiothaydoidangky.StylePriority.UseFont = false;
            this.lblGiothaydoidangky.Text = "hh:mm:ss";
            // 
            // lblNgaythaydoidangky
            // 
            this.lblNgaythaydoidangky.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaythaydoidangky.LocationFloat = new DevExpress.Utils.PointFloat(545.8638F, 92.73134F);
            this.lblNgaythaydoidangky.Name = "lblNgaythaydoidangky";
            this.lblNgaythaydoidangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaythaydoidangky.SizeF = new System.Drawing.SizeF(74.4201F, 11F);
            this.lblNgaythaydoidangky.StylePriority.UseFont = false;
            this.lblNgaythaydoidangky.Text = "dd/MM/yyyy";
            // 
            // lblNhomxulyhoso
            // 
            this.lblNhomxulyhoso.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhomxulyhoso.LocationFloat = new DevExpress.Utils.PointFloat(545.8638F, 81.73132F);
            this.lblNhomxulyhoso.Name = "lblNhomxulyhoso";
            this.lblNhomxulyhoso.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNhomxulyhoso.SizeF = new System.Drawing.SizeF(74.42004F, 11.00001F);
            this.lblNhomxulyhoso.StylePriority.UseFont = false;
            this.lblNhomxulyhoso.Text = "XE";
            // 
            // lblMahieuphuongthucvanchuyen
            // 
            this.lblMahieuphuongthucvanchuyen.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMahieuphuongthucvanchuyen.LocationFloat = new DevExpress.Utils.PointFloat(352.1301F, 70.73135F);
            this.lblMahieuphuongthucvanchuyen.Name = "lblMahieuphuongthucvanchuyen";
            this.lblMahieuphuongthucvanchuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMahieuphuongthucvanchuyen.SizeF = new System.Drawing.SizeF(18.52283F, 11.00001F);
            this.lblMahieuphuongthucvanchuyen.StylePriority.UseFont = false;
            this.lblMahieuphuongthucvanchuyen.Text = "X";
            // 
            // lblMaphanloaihanghoa
            // 
            this.lblMaphanloaihanghoa.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaphanloaihanghoa.LocationFloat = new DevExpress.Utils.PointFloat(333.2961F, 70.73135F);
            this.lblMaphanloaihanghoa.Name = "lblMaphanloaihanghoa";
            this.lblMaphanloaihanghoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaphanloaihanghoa.SizeF = new System.Drawing.SizeF(18.83395F, 11.00001F);
            this.lblMaphanloaihanghoa.StylePriority.UseFont = false;
            this.lblMaphanloaihanghoa.Text = "X";
            // 
            // lblMaloaihinh
            // 
            this.lblMaloaihinh.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaloaihinh.LocationFloat = new DevExpress.Utils.PointFloat(292.5225F, 70.73131F);
            this.lblMaloaihinh.Name = "lblMaloaihinh";
            this.lblMaloaihinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaloaihinh.SizeF = new System.Drawing.SizeF(40.77365F, 11F);
            this.lblMaloaihinh.StylePriority.UseFont = false;
            this.lblMaloaihinh.Text = "XXE";
            // 
            // lblMasothuedaidien
            // 
            this.lblMasothuedaidien.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMasothuedaidien.LocationFloat = new DevExpress.Utils.PointFloat(545.8638F, 70.73135F);
            this.lblMasothuedaidien.Name = "lblMasothuedaidien";
            this.lblMasothuedaidien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMasothuedaidien.SizeF = new System.Drawing.SizeF(74.42029F, 11F);
            this.lblMasothuedaidien.StylePriority.UseFont = false;
            this.lblMasothuedaidien.Text = "XXXE";
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(405.8634F, 92.73129F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(140.0005F, 11.00001F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.Text = "Ngày thay đổi đăng ký";
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(543.2618F, 48.73132F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(25.84082F, 11F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "-";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(607.4435F, 48.73132F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(13.34082F, 11F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "/";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(405.8634F, 81.7313F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(140.0005F, 11.00001F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.Text = "Mã bộ phận xử lý tờ khai";
            // 
            // lblTongsotokhaichianho
            // 
            this.lblTongsotokhaichianho.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongsotokhaichianho.LocationFloat = new DevExpress.Utils.PointFloat(620.7842F, 48.73132F);
            this.lblTongsotokhaichianho.Name = "lblTongsotokhaichianho";
            this.lblTongsotokhaichianho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongsotokhaichianho.SizeF = new System.Drawing.SizeF(60.21582F, 11F);
            this.lblTongsotokhaichianho.StylePriority.UseFont = false;
            this.lblTongsotokhaichianho.StylePriority.UseTextAlignment = false;
            this.lblTongsotokhaichianho.Text = "NE";
            this.lblTongsotokhaichianho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(237.3676F, 103.148F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(13.34079F, 11F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "-";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblSonhanhtokhaichianho
            // 
            this.lblSonhanhtokhaichianho.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSonhanhtokhaichianho.LocationFloat = new DevExpress.Utils.PointFloat(569.1026F, 48.73132F);
            this.lblSonhanhtokhaichianho.Name = "lblSonhanhtokhaichianho";
            this.lblSonhanhtokhaichianho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSonhanhtokhaichianho.SizeF = new System.Drawing.SizeF(38.34076F, 11F);
            this.lblSonhanhtokhaichianho.StylePriority.UseFont = false;
            this.lblSonhanhtokhaichianho.StylePriority.UseTextAlignment = false;
            this.lblSonhanhtokhaichianho.Text = "NE";
            this.lblSonhanhtokhaichianho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(405.8634F, 70.73131F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(140.0005F, 11.00001F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.Text = "Mã số thuế đại diện";
            // 
            // lblBieuthitruonghophethan
            // 
            this.lblBieuthitruonghophethan.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBieuthitruonghophethan.LocationFloat = new DevExpress.Utils.PointFloat(250.7083F, 103.148F);
            this.lblBieuthitruonghophethan.Name = "lblBieuthitruonghophethan";
            this.lblBieuthitruonghophethan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBieuthitruonghophethan.SizeF = new System.Drawing.SizeF(48.95282F, 11F);
            this.lblBieuthitruonghophethan.StylePriority.UseFont = false;
            this.lblBieuthitruonghophethan.StylePriority.UseTextAlignment = false;
            this.lblBieuthitruonghophethan.Text = "X";
            this.lblBieuthitruonghophethan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblSotokhaidautien
            // 
            this.lblSotokhaidautien.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotokhaidautien.LocationFloat = new DevExpress.Utils.PointFloat(405.8634F, 48.7313F);
            this.lblSotokhaidautien.Name = "lblSotokhaidautien";
            this.lblSotokhaidautien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotokhaidautien.SizeF = new System.Drawing.SizeF(137.3985F, 11F);
            this.lblSotokhaidautien.StylePriority.UseFont = false;
            this.lblSotokhaidautien.Text = "XXXXXXXXX1XE";
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(292.5224F, 48.7313F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(113.341F, 11F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.Text = "Số tờ khai đầu tiên";
            // 
            // lblGiodangky
            // 
            this.lblGiodangky.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiodangky.LocationFloat = new DevExpress.Utils.PointFloat(237.3676F, 92.73129F);
            this.lblGiodangky.Name = "lblGiodangky";
            this.lblGiodangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiodangky.SizeF = new System.Drawing.SizeF(62.29362F, 10.41669F);
            this.lblGiodangky.StylePriority.UseFont = false;
            this.lblGiodangky.Text = "hh:mm:ss";
            // 
            // lblThoihantainhapxuat
            // 
            this.lblThoihantainhapxuat.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThoihantainhapxuat.LocationFloat = new DevExpress.Utils.PointFloat(161.5267F, 103.148F);
            this.lblThoihantainhapxuat.Name = "lblThoihantainhapxuat";
            this.lblThoihantainhapxuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThoihantainhapxuat.SizeF = new System.Drawing.SizeF(75.84079F, 11F);
            this.lblThoihantainhapxuat.StylePriority.UseFont = false;
            this.lblThoihantainhapxuat.StylePriority.UseTextAlignment = false;
            this.lblThoihantainhapxuat.Text = "dd/MM/yyyy";
            this.lblThoihantainhapxuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(10.15919F, 103.148F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(151.3675F, 11.00003F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblNgaydangky
            // 
            this.lblNgaydangky.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaydangky.LocationFloat = new DevExpress.Utils.PointFloat(161.5267F, 92.73129F);
            this.lblNgaydangky.Name = "lblNgaydangky";
            this.lblNgaydangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaydangky.SizeF = new System.Drawing.SizeF(75.84079F, 10.41669F);
            this.lblNgaydangky.StylePriority.UseFont = false;
            this.lblNgaydangky.Text = "dd/MM/yyyy";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(10.31839F, 92.73129F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(151.2083F, 10.41668F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "Ngày đăng ký";
            // 
            // lblTencoquanhaiquantiepnhantokhai
            // 
            this.lblTencoquanhaiquantiepnhantokhai.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTencoquanhaiquantiepnhantokhai.LocationFloat = new DevExpress.Utils.PointFloat(238.3409F, 81.73132F);
            this.lblTencoquanhaiquantiepnhantokhai.Name = "lblTencoquanhaiquantiepnhantokhai";
            this.lblTencoquanhaiquantiepnhantokhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTencoquanhaiquantiepnhantokhai.SizeF = new System.Drawing.SizeF(94.95517F, 11.00001F);
            this.lblTencoquanhaiquantiepnhantokhai.StylePriority.UseFont = false;
            this.lblTencoquanhaiquantiepnhantokhai.Text = "XXXXXXXXXE";
            // 
            // lblSotokhai
            // 
            this.lblSotokhai.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotokhai.LocationFloat = new DevExpress.Utils.PointFloat(108.6268F, 48.7313F);
            this.lblSotokhai.Name = "lblSotokhai";
            this.lblSotokhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotokhai.SizeF = new System.Drawing.SizeF(183.8956F, 11F);
            this.lblSotokhai.StylePriority.UseFont = false;
            this.lblSotokhai.Text = "NNNNNNNNN1NE";
            // 
            // lblSotokhaitamnhaptaixuat
            // 
            this.lblSotokhaitamnhaptaixuat.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotokhaitamnhaptaixuat.LocationFloat = new DevExpress.Utils.PointFloat(292.5223F, 59.73132F);
            this.lblSotokhaitamnhaptaixuat.Name = "lblSotokhaitamnhaptaixuat";
            this.lblSotokhaitamnhaptaixuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotokhaitamnhaptaixuat.SizeF = new System.Drawing.SizeF(189.8114F, 11F);
            this.lblSotokhaitamnhaptaixuat.StylePriority.UseFont = false;
            this.lblSotokhaitamnhaptaixuat.StylePriority.UseTextAlignment = false;
            this.lblSotokhaitamnhaptaixuat.Text = "NNNNNNNNN1NE";
            this.lblSotokhaitamnhaptaixuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(9.999996F, 81.7313F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(228.1703F, 11F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(201.6816F, 70.73131F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(90.84074F, 11F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Mã loại hình";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(10.00004F, 70.73131F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(115.8408F, 11F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "Mã phân loại kiểm tra";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(97.78814F, 12.75F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(492.9241F, 35.9813F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Bản xác nhận nội dung tờ khai hàng hóa xuất khẩu";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(9.999996F, 59.73132F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(282.5225F, 11F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            // 
            // lblMaphanloaikiemtra
            // 
            this.lblMaphanloaikiemtra.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaphanloaikiemtra.LocationFloat = new DevExpress.Utils.PointFloat(125.8408F, 70.73131F);
            this.lblMaphanloaikiemtra.Name = "lblMaphanloaikiemtra";
            this.lblMaphanloaikiemtra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaphanloaikiemtra.SizeF = new System.Drawing.SizeF(75.84079F, 11F);
            this.lblMaphanloaikiemtra.StylePriority.UseFont = false;
            this.lblMaphanloaikiemtra.Text = "XXE ";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(10.00003F, 48.7313F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(98.62683F, 11.00001F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Số tờ khai";
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(702.8408F, 2F);
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel17,
            this.lblSodong1,
            this.xrLabel18,
            this.lblMasohanghoa,
            this.xrLabel20,
            this.lblMaquanlyrieng,
            this.xrLabel22,
            this.lblMataixacnhangia,
            this.lblMotahanghoa,
            this.xrLabel24,
            this.xrLabel25,
            this.lblSoluong1,
            this.lblSoluong2,
            this.lblMadvtinh1,
            this.lblMadvtinh2,
            this.xrLabel30,
            this.lblDongia,
            this.xrLabel32,
            this.lblMadongtien,
            this.xrLabel34,
            this.lblDonvidongia,
            this.xrLabel36,
            this.lblTrigia,
            this.xrLabel16,
            this.xrLabel19,
            this.lblTrigiathueS,
            this.lblMadongtienS,
            this.xrLabel21,
            this.lblTrigiathueM,
            this.lblMadongtienM,
            this.xrLabel27,
            this.xrLabel23,
            this.lblSoluongthue,
            this.lblMadvtinhchuanthue,
            this.xrLabel26,
            this.lblDonvisoluongthue,
            this.lblDongiathue,
            this.lblMatientedongiathue,
            this.xrLabel28,
            this.lblThuesuat,
            this.xrLabel31,
            this.lblPhanloainhapthue,
            this.xrLabel29,
            this.xrLabel33,
            this.lblSotienthue,
            this.lblSotienmiengiam,
            this.lblMatientesotienthue,
            this.lblMatientesotiengiamthue,
            this.xrLabel35,
            this.lblSodonghangtaixuat,
            this.xrLabel37,
            this.lblDanhmucmienthue,
            this.xrLabel38,
            this.lblSodongmienthue,
            this.xrLabel39,
            this.xrLabel40,
            this.lblTienlephi,
            this.xrLabel41,
            this.xrLabel42,
            this.lblTienbaohiem,
            this.xrLabel43,
            this.lblSoluongtienlephi,
            this.lblKhoantienlephi,
            this.xrLabel46,
            this.lblMadvtienlephi,
            this.lblSoluongtienbaohiem,
            this.xrLabel44,
            this.lblKhoantienbaohiem,
            this.lblMadvtienbaohiem,
            this.xrLabel48,
            this.xrLabel50,
            this.xrLabel51,
            this.xrLabel45,
            this.xrLabel47,
            this.lblMavanvanphapquy1,
            this.xrLabel52,
            this.lblMavanvanphapquy2,
            this.xrLabel54,
            this.lblMavanvanphapquy3,
            this.xrLabel56,
            this.lblMavanvanphapquy4,
            this.xrLabel58,
            this.lblMavanvanphapquy5,
            this.xrLabel49,
            this.lblMamiengiam,
            this.lblDieukhoanmien,
            this.xrLine1});
            this.Detail1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Detail1.HeightF = 252.875F;
            this.Detail1.Name = "Detail1";
            this.Detail1.StylePriority.UseFont = false;
            // 
            // BanXacNhanNoiDungToKhaiHangHoaXuatKhau_3
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(71, 76, 148, 100);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel lblGiothaydoidangky;
        private DevExpress.XtraReports.UI.XRLabel lblNgaythaydoidangky;
        private DevExpress.XtraReports.UI.XRLabel lblNhomxulyhoso;
        private DevExpress.XtraReports.UI.XRLabel lblMahieuphuongthucvanchuyen;
        private DevExpress.XtraReports.UI.XRLabel lblMaphanloaihanghoa;
        private DevExpress.XtraReports.UI.XRLabel lblMaloaihinh;
        private DevExpress.XtraReports.UI.XRLabel lblMasothuedaidien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lblTongsotokhaichianho;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lblSonhanhtokhaichianho;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel lblBieuthitruonghophethan;
        private DevExpress.XtraReports.UI.XRLabel lblSotokhaidautien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel lblGiodangky;
        private DevExpress.XtraReports.UI.XRLabel lblThoihantainhapxuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel lblNgaydangky;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel lblTencoquanhaiquantiepnhantokhai;
        private DevExpress.XtraReports.UI.XRLabel lblSotokhai;
        private DevExpress.XtraReports.UI.XRLabel lblSotokhaitamnhaptaixuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblMaphanloaikiemtra;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel lblSodong1;
        private DevExpress.XtraReports.UI.XRLabel lblMotahanghoa;
        private DevExpress.XtraReports.UI.XRLabel lblMataixacnhangia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel lblMaquanlyrieng;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel lblMasohanghoa;
        private DevExpress.XtraReports.UI.XRLabel lblSoluong2;
        private DevExpress.XtraReports.UI.XRLabel lblSoluong1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel lblMadvtinh2;
        private DevExpress.XtraReports.UI.XRLabel lblMadvtinh1;
        private DevExpress.XtraReports.UI.XRLabel lblDonvidongia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel lblMadongtien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel lblDongia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel lblTrigia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lblMadongtienS;
        private DevExpress.XtraReports.UI.XRLabel lblTrigiathueS;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel lblMadvtinhchuanthue;
        private DevExpress.XtraReports.UI.XRLabel lblSoluongthue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel lblMadongtienM;
        private DevExpress.XtraReports.UI.XRLabel lblTrigiathueM;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel lblMatientedongiathue;
        private DevExpress.XtraReports.UI.XRLabel lblDongiathue;
        private DevExpress.XtraReports.UI.XRLabel lblDonvisoluongthue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloainhapthue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel lblThuesuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel lblMatientesotiengiamthue;
        private DevExpress.XtraReports.UI.XRLabel lblMatientesotienthue;
        private DevExpress.XtraReports.UI.XRLabel lblSotienmiengiam;
        private DevExpress.XtraReports.UI.XRLabel lblSotienthue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel lblSodongmienthue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel lblDanhmucmienthue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel lblSodonghangtaixuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel lblTienbaohiem;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel lblTienlephi;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel lblMadvtienlephi;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel lblKhoantienlephi;
        private DevExpress.XtraReports.UI.XRLabel lblSoluongtienlephi;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel lblMadvtienbaohiem;
        private DevExpress.XtraReports.UI.XRLabel lblKhoantienbaohiem;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel lblSoluongtienbaohiem;
        private DevExpress.XtraReports.UI.XRLabel lblMavanvanphapquy5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel lblMavanvanphapquy4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel lblMavanvanphapquy3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel lblMavanvanphapquy2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel lblMavanvanphapquy1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel lblMamiengiam;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel lblDieukhoanmien;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
    }
}
