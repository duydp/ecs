﻿using System.Net;
using System;
using System.Xml;
using System.Text;
using System.Security.Cryptography;
namespace Company.KD.BLL
{
    public class WebServiceConnection
    {
        //public static bool WSCheck()
        //{
        //    KDTService kdtService = new KDTService();
        //    try
        //    {
        //        return kdtService.KDT_CheckConnection();
        //    }
        //    catch 
        //    {
        //        return false;
        //    }
        //}

        //public static string GetURLWebServiceKhaiBao()
        //{
        //    Uri url = new Uri(Properties.Settings.Default.Company_KD_BLL_WS_KhaiDienTu_KDTService);
        //    return url.Host;
        //    //return Properties.Settings.Default.Company_KD_BLL_WS_KhaiDienTu_KDTService;
        //}

        //public static void SetURLWebServiceKhaiBao(string ip)
        //{
        //    string urlCu = Properties.Settings.Default.Company_KD_BLL_WS_KhaiDienTu_KDTService;
        //    try
        //    {                
        //        //Properties.Settings.Default.Company_KD_BLL_WS_KhaiDienTu_KDTService = "Http://" + ip + "/kdtservice/kdtservice.asmx";                
        //        Properties.Settings.Default.Save();
        //        Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS_KD();
        //    }
        //    catch
        //    {
        //        //Properties.Settings.Default.Company_KD_BLL_WS_KhaiDienTu_KDTService = urlCu;
        //        Properties.Settings.Default.Save();
        //        throw new Exception("Địa chỉ webservice kích hoạt không hợp lệ");
        //    }
        //}

        //public static string GetURLWebServiceKichHoat()
        //{
        //    Uri url = new Uri(Properties.Settings.Default.Company_KD_BLL_WS_AS_ActivateService);
        //    return url.Host;
        //}

        //public static void SetURLWebServiceKichHoat(string ip)
        //{
        //    string urlCu = Properties.Settings.Default.Company_KD_BLL_WS_KhaiDienTu_KDTService;
        //    try
        //    {
        //        //Properties.Settings.Default.Company_KD_BLL_WS_AS_ActivateService = "Http://" + ip + "/ActivateOnline/ActivateService.asmx";                
        //        Properties.Settings.Default.Save();
        //        WS.AS.ActivateService ac = new Company.KD.BLL.WS.AS.ActivateService();                
        //    }
        //    catch
        //    {
        //        //Properties.Settings.Default.Company_KD_BLL_WS_AS_ActivateService = urlCu;
        //        Properties.Settings.Default.Save();
        //        throw new Exception("Địa chỉ webservice kích hoạt không hợp lệ");
        //    }
        //}
        public static string GetMD5Value(string data)
        {
            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();
        }
        public static void sendThongTinDN(string MaDoanhNghiep, string TenDoanhNghiep, string DiaChi, string GhiChu, string LoaiHinh)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("Root");
                XmlElement madn = doc.CreateElement("MaDoanhNghiep");
                madn.InnerText = MaDoanhNghiep;
                XmlElement tendn = doc.CreateElement("TenDoanhNghiep");
                tendn.InnerText = TenDoanhNghiep;
                XmlElement diachidn = doc.CreateElement("DiaChi");
                diachidn.InnerText = DiaChi;
                XmlElement loaihinhdn = doc.CreateElement("LoaiHinh");
                loaihinhdn.InnerText = LoaiHinh;
                XmlElement ghichudn = doc.CreateElement("GhiChu");
                ghichudn.InnerText = GhiChu;

                root.AppendChild(madn);
                root.AppendChild(tendn);
                root.AppendChild(diachidn);
                root.AppendChild(loaihinhdn);
                root.AppendChild(ghichudn);
                doc.AppendChild(root);
                WS.AutoUpdate.Update a = new Company.KD.BLL.WS.AutoUpdate.Update();
                a.Activate(doc.InnerXml, GetMD5Value("ECSHCM12345ABCD"));
            }
            catch { }
        }
        public static string checkKhaiBao(string MaDoanhNghiep, string tenDoanhNghiep,string diaChi,string maHQ)
        {
            WS.AutoUpdate.Update u = new Company.KD.BLL.WS.AutoUpdate.Update();
            string st = u.CheckDoanhNghiepKhaiBao(MaDoanhNghiep, tenDoanhNghiep,diaChi,maHQ, GetMD5Value("ECSHCM12345ABCD"));
            return st;
        }

        public static void AddNodeDeclarationType(XmlDocument xmlDoc)
        {

        }
    }
}