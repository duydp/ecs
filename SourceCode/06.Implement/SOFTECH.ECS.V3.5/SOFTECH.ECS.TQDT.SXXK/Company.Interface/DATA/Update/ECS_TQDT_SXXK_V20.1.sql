
/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy]    Script Date: 10/24/2014 09:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
----------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy]  
 @MaDoanhNghiep NVarchar(14),
 @MaHaiQuan varchar (6),  
 @LanThanhLy int  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  

Create table #BangKeNPLTon 
		    ( MaNPL nvarchar(80),
			 TenNPL nvarchar(255),
			 LuongTonDau numeric(24,8),
			 LuongNhap	numeric(24,8),
			LuongNPLSuDung numeric(24,8),
			LuongNPLTaiXuat numeric(24,8),
			LuongNopThue numeric(24,8),
			LuongXuatTheoHD numeric(24,8),
			LuongTonCuoi numeric(24,8),
			DVT_ID varchar(10)
			)

insert into #BangKeNPLTon (MaNPL ,
			 TenNPL ,
			 LuongTonDau ,
			 LuongNhap	,
			LuongNPLSuDung ,
			LuongNPLTaiXuat ,
			LuongNopThue,
			LuongXuatTheoHD ,
			LuongTonCuoi,
			DVT_ID )

Select   
 A.MaNPL,  
 MAX(B.Ten) as TenNPL,   
 (SUM(A.LuongTonDau) - Sum(LuongNhap)) as LuongTonDau,  
 Sum(LuongNhap) as LuongNhap,  
 Sum(LuongNPLSuDung) as LuongNPLSuDung,  
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,  
 Sum(LuongNopThue) as LuongNopThue,  
 Sum(LuongXuatTheoHD) as LuongXuatTheoHD,  
 Sum(LuongTonCuoi) as LuongTonCuoi,  
 B.DVT_ID --MAX(ID_DVT_NPL) as ID_DVT_NPL,  

 From   
(   
SELECT  
 SoToKhaiNhap,  
 NgayDangKyNhap,  
 MaNPL,  
 TenNPL,   
 LuongTonDau,  
 CASE when  LuongNhap = LuongTonDau  then (Case when  MaLoaiHinhNhap not like '%V%' then 0 else LuongNhap end) else 0 end as LuongNhap,   
 Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung,   
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,   
 0 as LuongNopThue,  
 0 as LuongXuatTheoHD,  
 Case When Min(LuongTonCuoi) < 0 then 0 else Min(LuongTonCuoi) end as LuongTonCuoi,  
 (select ID from t_HaiQuan_DonViTinh where ten=TenDVT_NPL)as ID_DVT_NPL  

    
FROM  
 [dbo].[t_KDT_SXXK_BCXuatNhapTon]  
WHERE MaDoanhNghiep =@MaDoanhNghiep AND LanThanhLy =@LanThanhLy  
GROUP BY NgayDangKyNhap, SoToKhaiNhap, MaLoaiHinhNhap, MaNPL,TenNPL, TenDVT_NPL,DonGiaTT, LuongTonDau,LuongNhap  
) as A inner join t_SXXK_NguyenPhuLieu B  
on A.MaNPL = B.Ma  AND B.MaDoanhNghiep = @MaDoanhNghiep And B.MaHaiQuan= @MaHaiQuan
-- inner join (select MaNPL,TenNPL, Luong as LuongTonDau from [t_KDT_SXXK_BKNPLChuaThanhLy] where BangKeHoSoThanhLy_ID=2476 )D
--on A.MaNPL =  D.MaNPL   
Group by A.MaNPL,B.DVT_ID  
order by A.MaNPL  

 if(exists( select * from t_KDT_SXXK_BangKeHoSoThanhLy where MaterID = (select top 1 ID from  t_KDT_SXXK_HoSoThanhLyDangKy where LanThanhLy = @LanThanhLy AND MaBangKe = 'DTLNPLCHUATL')))
 begin 
  update #BangKeNPLTon set LuongTonCuoi = LuongTonCuoi + b.Luong , LuongTonDau = LuongTonDau + b.Luong
  from #BangKeNPLTon a inner join (select sum(Luong)as Luong,MaNPL,DVT_ID  from t_KDT_SXXK_BKNPLChuaThanhLy where BangKeHoSoThanhLy_ID = (select top 1 ID from t_KDT_SXXK_BangKeHoSoThanhLy where MaterID = (select top 1 ID from  t_KDT_SXXK_HoSoThanhLyDangKy where LanThanhLy = @LanThanhLY AND MaBangKe = 'DTLNPLCHUATL')) group by MaNPL,DVT_ID) b
  on a.MaNPL = b.MaNPL and a.DVT_ID = b.DVT_ID
end

select * from #BangKeNPLTon
drop table #BangKeNPLTon

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '20.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('20.1',GETDATE(), N' cap nhat bao cao 07 CX')
END