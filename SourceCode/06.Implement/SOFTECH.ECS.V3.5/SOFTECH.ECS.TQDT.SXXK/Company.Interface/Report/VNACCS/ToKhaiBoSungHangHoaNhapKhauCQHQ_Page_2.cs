using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;
namespace Company.Interface.Report.VNACCS
{
    public partial class ToKhaiBoSungHangHoaNhapKhauCQHQ_Page_2 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiBoSungHangHoaNhapKhauCQHQ_Page_2()
        {
            InitializeComponent();
        }
        public void BindingReport(VAD2AY0 vad2ay)
        {
            lblSoTrang.Text = "2/" + vad2ay.B12.GetValue().ToString().ToUpper();
            lblSoToKhai.Text = vad2ay.ICN.GetValue().ToString().ToUpper();

            lblSoToKhaiDauTien.Text = vad2ay.FIC.GetValue().ToString().ToUpper();
            lblSoNhanhToKhaiChiaNho.Text = vad2ay.BNO.GetValue().ToString().ToUpper();
            lblTongSoToKhaiChiaNho.Text = vad2ay.DNO.GetValue().ToString().ToUpper();
            lblSoToKhaiTamNhapTaiXuat.Text = vad2ay.TDN.GetValue().ToString().ToUpper();
            lblMaPhanLoaiKiemTra.Text = vad2ay.A06.GetValue().ToString().ToUpper();
            lblMaLoaiHinh.Text = vad2ay.ICB.GetValue().ToString().ToUpper();
            lblMaPhanLoaiHangHoa.Text = vad2ay.CCC.GetValue().ToString().ToUpper();
            lblMaHieuPhuongThucVanChuyen.Text = vad2ay.MTC.GetValue().ToString().ToUpper();
            lblPhanLoaiCaNhanToChuc.Text = vad2ay.SKB.GetValue().ToString().ToUpper();
            lblMaSoHangHoaDaiDienToKhai.Text = vad2ay.A00.GetValue().ToString().ToUpper();
            lblTenCoQuanHaiQuanTiepNhanToKhai.Text = vad2ay.A07.GetValue().ToString().ToUpper();
            lblMaBoPhanXuLyToKhai.Text = vad2ay.CHB.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad2ay.A09.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDangKy.Text = Convert.ToDateTime(vad2ay.A09.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDangKy.Text = "";
            }
            if (vad2ay.AD1.GetValue().ToString().ToUpper() != "" && vad2ay.AD1.GetValue().ToString().ToUpper() != "0")
            {
                lblGioDangKy.Text = vad2ay.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad2ay.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad2ay.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad2ay.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayThayDoiDangKy.Text = Convert.ToDateTime(vad2ay.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayThayDoiDangKy.Text = "";
            }
            if (vad2ay.AD3.GetValue().ToString().ToUpper() != "" && vad2ay.AD3.GetValue().ToString().ToUpper() != "0")
            {
                lblGioThayDoiDangKy.Text = vad2ay.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad2ay.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad2ay.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioThayDoiDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad2ay.RED.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiHanTaiNhapTaiXuat.Text = Convert.ToDateTime(vad2ay.RED.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiHanTaiNhapTaiXuat.Text = "";
            }
            lblBieuThiTHHetHan.Text = vad2ay.AAA.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad2ay.EA_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblPhanLoaiDinhKemKhaiBaoDienTu1.Text = vad2ay.EA_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoDinhKemKhaiBaoDienTu1.Text = vad2ay.EA_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblPhanLoaiDinhKemKhaiBaoDienTu2.Text = vad2ay.EA_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoDinhKemKhaiBaoDienTu2.Text = vad2ay.EA_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblPhanLoaiDinhKemKhaiBaoDienTu3.Text = vad2ay.EA_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoDinhKemKhaiBaoDienTu3.Text = vad2ay.EA_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblPhanGhiChu.Text = vad2ay.NT2.GetValue().ToString().ToUpper();
            lblSoQuanLyNoiBoDN.Text = vad2ay.REF.GetValue().ToString().ToUpper();
            lblSoQuanLyNSD.Text = vad2ay.B16.GetValue().ToString().ToUpper();
            lblPhanLoaiChiThiHQ.Text = vad2ay.CCM.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad2ay.ADY.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBaoNopThue.Text = Convert.ToDateTime(vad2ay.ADY.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBaoNopThue.Text = "";
            }
            if (vad2ay.ADZ.GetValue().ToString().ToUpper() != "" && vad2ay.ADZ.GetValue().ToString().ToUpper() != "0")
            {
                lblGioKhaiBaoNT.Text = vad2ay.ADZ.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad2ay.ADZ.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad2ay.ADZ.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioKhaiBaoNT.Text = "";
            }
            lblTieuDe.Text = vad2ay.B23.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad2ay.KN1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaSacThueAnHan1.Text = vad2ay.KN1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThueAnHan1.Text = vad2ay.KN1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad2ay.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblHanNopThue1.Text = Convert.ToDateTime(vad2ay.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblHanNopThue1.Text = "";
                        }
                        break;
                    case 1:
                        lblMaSacThueAnHan2.Text = vad2ay.KN1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThueAnHan2.Text = vad2ay.KN1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad2ay.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblHanNopThue2.Text = Convert.ToDateTime(vad2ay.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblHanNopThue2.Text = "";
                        }
                        break;
                    case 2:
                        lblMaSacThueAnHan3.Text = vad2ay.KN1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThueAnHan3.Text = vad2ay.KN1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad2ay.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblHanNopThue3.Text = Convert.ToDateTime(vad2ay.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblHanNopThue3.Text = "";
                        }
                        break;
                    case 3:
                        lblMaSacThueAnHan4.Text = vad2ay.KN1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThueAnHan4.Text = vad2ay.KN1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad2ay.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblHanNopThue4.Text = Convert.ToDateTime(vad2ay.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblHanNopThue4.Text = "";
                        }
                        break;
                    case 4:
                        lblMaSacThueAnHan5.Text = vad2ay.KN1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThueAnHan5.Text = vad2ay.KN1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad2ay.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblHanNopThue5.Text = Convert.ToDateTime(vad2ay.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblHanNopThue5.Text = "";
                        }
                        break;
                    case 5:
                        lblMaSacThueAnHan6.Text = vad2ay.KN1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThueAnHan6.Text = vad2ay.KN1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad2ay.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblHanNopThue6.Text = Convert.ToDateTime(vad2ay.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblHanNopThue6.Text = "";
                        }
                        break;
                }
            }
            if (Convert.ToDateTime(vad2ay.DPD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhoiHanhVanChuyen.Text = Convert.ToDateTime(vad2ay.DPD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhoiHanhVanChuyen.Text = "";
            }
            for (int i = 0; i < vad2ay.ST_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblDiaDiemTrungChuyen1.Text = vad2ay.ST_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad2ay.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayDuKienDenDDTC1.Text = Convert.ToDateTime(vad2ay.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayDuKienDenDDTC1.Text = "";
                        }
                        if (Convert.ToDateTime(vad2ay.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayKhoiHanh1.Text = Convert.ToDateTime(vad2ay.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayKhoiHanh1.Text = "";
                        }
                        break;
                    case 1:
                        lblDiaDiemTrungChuyen2.Text = vad2ay.ST_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad2ay.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayDuKienDenDDTC2.Text = Convert.ToDateTime(vad2ay.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayDuKienDenDDTC2.Text = "";
                        }
                        if (Convert.ToDateTime(vad2ay.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayKhoiHanh2.Text = Convert.ToDateTime(vad2ay.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayKhoiHanh2.Text = "";
                        }
                        break;
                    case 2:
                        lblDiaDiemTrungChuyen3.Text = vad2ay.ST_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad2ay.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayDuKienDenDDTC3.Text = Convert.ToDateTime(vad2ay.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayDuKienDenDDTC3.Text = "";
                        }
                        if (Convert.ToDateTime(vad2ay.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayKhoiHanh3.Text = Convert.ToDateTime(vad2ay.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayKhoiHanh3.Text = "";
                        }
                        break;
                }
            }
            lblDiaDiemDichVanChuyen.Text = vad2ay.ARP.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad2ay.ADT.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDuKienDen.Text = Convert.ToDateTime(vad2ay.ADT.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDuKienDen.Text = "";
            }
            BindReportChiThiHQ(vad2ay);
        }
        public void BindReportChiThiHQ(VAD2AY0 vad2ay)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Date", typeof(string));
                dt.Columns.Add("Ten", typeof(string));
                dt.Columns.Add("NoiDung", typeof(string));
                dt.Columns.Add("STT", typeof(string));
                int STT = 0;

                for (int i = 0; i < vad2ay.D__.listAttribute[0].ListValue.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    if (Convert.ToDateTime(vad2ay.D__.listAttribute[0].GetValueCollection(i)).ToString("dd/MM/yyyy") != ("01/01/1900"))
                    {
                        dr["Date"] = Convert.ToDateTime(vad2ay.D__.listAttribute[0].GetValueCollection(i)).ToString("dd/MM/yyyy");
                    }
                    dr["Ten"] = vad2ay.D__.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                    dr["STT"] = STT.ToString().ToUpper();
                    dr["NoiDung"] = vad2ay.D__.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                    dt.Rows.Add(dr);
                }

                while (dt.Rows.Count < 10)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    dr["Date"] = "";
                    dr["Ten"] = "";
                    dr["NoiDung"] = "";
                    dr["STT"] = STT.ToString().ToUpper();
                    dt.Rows.Add(dr);
                }
                DetailReport.DataSource = dt;
                lblNgaychithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "Date");
                lblTenchithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "Ten");
                lblNoidungchithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "NoiDung");
                lblSTTChiThiHQ.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            }
            catch (Exception e)
            {
            }
        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
        }
    }
}
