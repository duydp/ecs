using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class IDA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute ICN { get; set; }
public PropertiesAttribute FIC { get; set; }
public PropertiesAttribute BNO { get; set; }
public PropertiesAttribute DNO { get; set; }
public PropertiesAttribute TDN { get; set; }
public PropertiesAttribute ICB { get; set; }
public PropertiesAttribute CCC { get; set; }
public PropertiesAttribute MTC { get; set; }
public PropertiesAttribute SKB { get; set; }
public PropertiesAttribute CH { get; set; }
public PropertiesAttribute CHB { get; set; }
public PropertiesAttribute RED { get; set; }
public PropertiesAttribute ICD { get; set; }
public PropertiesAttribute IMC { get; set; }
public PropertiesAttribute IMN { get; set; }
public PropertiesAttribute IMY { get; set; }
public PropertiesAttribute IMA { get; set; }
public PropertiesAttribute IMT { get; set; }
public PropertiesAttribute NMC { get; set; }
public PropertiesAttribute NMN { get; set; }
public PropertiesAttribute EPC { get; set; }
public PropertiesAttribute EPN { get; set; }
public PropertiesAttribute EPY { get; set; }
public PropertiesAttribute EPA { get; set; }
public PropertiesAttribute EP2 { get; set; }
public PropertiesAttribute EP3 { get; set; }
public PropertiesAttribute EP4 { get; set; }
public PropertiesAttribute EPO { get; set; }
public PropertiesAttribute ENM { get; set; }
public PropertiesAttribute ICC { get; set; }
public GroupAttribute BL_ { get; set; }
public PropertiesAttribute NO { get; set; }
public PropertiesAttribute NOT { get; set; }
public PropertiesAttribute GW { get; set; }
public PropertiesAttribute GWT { get; set; }
public PropertiesAttribute ST { get; set; }
public PropertiesAttribute MRK { get; set; }
public PropertiesAttribute VSC { get; set; }
public PropertiesAttribute VSN { get; set; }
public PropertiesAttribute ARR { get; set; }
public PropertiesAttribute DST { get; set; }
public PropertiesAttribute DSN { get; set; }
public PropertiesAttribute PSC { get; set; }
public PropertiesAttribute PSN { get; set; }
public PropertiesAttribute COC { get; set; }
public PropertiesAttribute N4 { get; set; }
public GroupAttribute OL_ { get; set; }
public GroupAttribute SS_ { get; set; }
public PropertiesAttribute IV1 { get; set; }
public PropertiesAttribute IV2 { get; set; }
public PropertiesAttribute IV3 { get; set; }
public PropertiesAttribute IVD { get; set; }
public PropertiesAttribute IVP { get; set; }
public PropertiesAttribute IP1 { get; set; }
public PropertiesAttribute IP2 { get; set; }
public PropertiesAttribute IP3 { get; set; }
public PropertiesAttribute IP4 { get; set; }
public PropertiesAttribute VD1 { get; set; }
public PropertiesAttribute VD2 { get; set; }
public PropertiesAttribute VCC { get; set; }
public PropertiesAttribute VPC { get; set; }
public PropertiesAttribute FR1 { get; set; }
public PropertiesAttribute FR2 { get; set; }
public PropertiesAttribute FR3 { get; set; }
public PropertiesAttribute IN1 { get; set; }
public PropertiesAttribute IN2 { get; set; }
public PropertiesAttribute IN3 { get; set; }
public PropertiesAttribute IN4 { get; set; }
public GroupAttribute VR_ { get; set; }
public PropertiesAttribute VLD { get; set; }
public PropertiesAttribute TP { get; set; }
public PropertiesAttribute TPM { get; set; }
public PropertiesAttribute BP { get; set; }
public PropertiesAttribute BRC { get; set; }
public PropertiesAttribute BYA { get; set; }
public PropertiesAttribute BCM { get; set; }
public PropertiesAttribute BCN { get; set; }
public PropertiesAttribute ENC { get; set; }
public PropertiesAttribute SBC { get; set; }
public PropertiesAttribute RYA { get; set; }
public PropertiesAttribute SCM { get; set; }
public PropertiesAttribute SCN { get; set; }
public GroupAttribute EA_ { get; set; }
public PropertiesAttribute ISD { get; set; }
public PropertiesAttribute DPD { get; set; }
public GroupAttribute ST_ { get; set; }
public PropertiesAttribute ARP { get; set; }
public PropertiesAttribute ADT { get; set; }
public PropertiesAttribute NT2 { get; set; }
public PropertiesAttribute REF { get; set; }
public PropertiesAttribute CCM { get; set; }
public GroupAttribute DTI { get; set; }

public IDA()
        {
ICN = new PropertiesAttribute(12, typeof(int));
FIC = new PropertiesAttribute(12, typeof(string));
BNO = new PropertiesAttribute(2, typeof(int));
DNO = new PropertiesAttribute(2, typeof(int));
TDN = new PropertiesAttribute(12, typeof(int));
ICB = new PropertiesAttribute(3, typeof(string));
CCC = new PropertiesAttribute(1, typeof(string));
MTC = new PropertiesAttribute(1, typeof(string));
SKB = new PropertiesAttribute(1, typeof(string));
CH = new PropertiesAttribute(6, typeof(string));
CHB = new PropertiesAttribute(2, typeof(string));
RED = new PropertiesAttribute(8, typeof(DateTime));
ICD = new PropertiesAttribute(8, typeof(DateTime));
IMC = new PropertiesAttribute(13, typeof(string));
IMN = new PropertiesAttribute(300, typeof(string));
IMY = new PropertiesAttribute(7, typeof(string));
IMA = new PropertiesAttribute(300, typeof(string));
IMT = new PropertiesAttribute(20, typeof(string));
NMC = new PropertiesAttribute(13, typeof(string));
NMN = new PropertiesAttribute(300, typeof(string));
EPC = new PropertiesAttribute(13, typeof(string));
EPN = new PropertiesAttribute(70, typeof(string));
EPY = new PropertiesAttribute(9, typeof(string));
EPA = new PropertiesAttribute(35, typeof(string));
EP2 = new PropertiesAttribute(35, typeof(string));
EP3 = new PropertiesAttribute(35, typeof(string));
EP4 = new PropertiesAttribute(35, typeof(string));
EPO = new PropertiesAttribute(2, typeof(string));
ENM = new PropertiesAttribute(70, typeof(string));
ICC = new PropertiesAttribute(5, typeof(string));
NO = new PropertiesAttribute(8, typeof(int));
NOT = new PropertiesAttribute(3, typeof(string));
GW = new PropertiesAttribute(10, typeof(int));
GWT = new PropertiesAttribute(3, typeof(string));
ST = new PropertiesAttribute(7, typeof(string));
MRK = new PropertiesAttribute(140, typeof(string));
VSC = new PropertiesAttribute(9, typeof(string));
VSN = new PropertiesAttribute(35, typeof(string));
ARR = new PropertiesAttribute(8, typeof(DateTime));
DST = new PropertiesAttribute(6, typeof(string));
DSN = new PropertiesAttribute(35, typeof(string));
PSC = new PropertiesAttribute(5, typeof(string));
PSN = new PropertiesAttribute(35, typeof(string));
COC = new PropertiesAttribute(3, typeof(int));
N4 = new PropertiesAttribute(1, typeof(string));
IV1 = new PropertiesAttribute(1, typeof(string));
IV2 = new PropertiesAttribute(12, typeof(int));
IV3 = new PropertiesAttribute(35, typeof(string));
IVD = new PropertiesAttribute(8, typeof(DateTime));
IVP = new PropertiesAttribute(7, typeof(string));
IP1 = new PropertiesAttribute(1, typeof(string));
IP2 = new PropertiesAttribute(3, typeof(string));
IP3 = new PropertiesAttribute(3, typeof(string));
IP4 = new PropertiesAttribute(20, typeof(int));
VD1 = new PropertiesAttribute(1, typeof(string));
VD2 = new PropertiesAttribute(9, typeof(string));
VCC = new PropertiesAttribute(3, typeof(string));
VPC = new PropertiesAttribute(20, typeof(int));
FR1 = new PropertiesAttribute(1, typeof(string));
FR2 = new PropertiesAttribute(3, typeof(string));
FR3 = new PropertiesAttribute(18, typeof(int));
IN1 = new PropertiesAttribute(1, typeof(string));
IN2 = new PropertiesAttribute(3, typeof(string));
IN3 = new PropertiesAttribute(16, typeof(int));
IN4 = new PropertiesAttribute(6, typeof(string));
VLD = new PropertiesAttribute(840, typeof(string));
TP = new PropertiesAttribute(20, typeof(int));
TPM = new PropertiesAttribute(1, typeof(string));
BP = new PropertiesAttribute(1, typeof(string));
BRC = new PropertiesAttribute(11, typeof(string));
BYA = new PropertiesAttribute(4, typeof(int));
BCM = new PropertiesAttribute(10, typeof(string));
BCN = new PropertiesAttribute(10, typeof(string));
ENC = new PropertiesAttribute(1, typeof(string));
SBC = new PropertiesAttribute(11, typeof(string));
RYA = new PropertiesAttribute(4, typeof(int));
SCM = new PropertiesAttribute(10, typeof(string));
SCN = new PropertiesAttribute(10, typeof(string));
ISD = new PropertiesAttribute(8, typeof(DateTime));
DPD = new PropertiesAttribute(8, typeof(DateTime));
ARP = new PropertiesAttribute(7, typeof(string));
ADT = new PropertiesAttribute(8, typeof(DateTime));
NT2 = new PropertiesAttribute(300, typeof(string));
REF = new PropertiesAttribute(20, typeof(string));
CCM = new PropertiesAttribute(1, typeof(string));
#region BL_
List<PropertiesAttribute> listBL_ = new List<PropertiesAttribute>();
listBL_.Add(new PropertiesAttribute(35, 5, EnumGroupID.IDA_BL_, typeof(string)));
BL_ = new GroupAttribute("BL_", 5, listBL_);
#endregion BL_
#region OL_
List<PropertiesAttribute> listOL_ = new List<PropertiesAttribute>();
listOL_.Add(new PropertiesAttribute(2, 5, EnumGroupID.IDA_OL_, typeof(string)));
OL_ = new GroupAttribute("OL_", 5, listOL_);
#endregion OL_
#region SS_
List<PropertiesAttribute> listSS_ = new List<PropertiesAttribute>();
listSS_.Add(new PropertiesAttribute(4, 5, EnumGroupID.IDA_SS_, typeof(string)));
listSS_.Add(new PropertiesAttribute(20, 5, EnumGroupID.IDA_SN_, typeof(string)));
SS_ = new GroupAttribute("SS_", 5, listSS_);
#endregion SS_
#region VR_
List<PropertiesAttribute> listVR_ = new List<PropertiesAttribute>();
listVR_.Add(new PropertiesAttribute(1, 5, EnumGroupID.IDA_VR_, typeof(string)));
listVR_.Add(new PropertiesAttribute(3, 5, EnumGroupID.IDA_VI_, typeof(string)));
listVR_.Add(new PropertiesAttribute(3, 5, EnumGroupID.IDA_VC_, typeof(string)));
listVR_.Add(new PropertiesAttribute(20, 5, EnumGroupID.IDA_VP_, typeof(int)));
listVR_.Add(new PropertiesAttribute(20, 5, EnumGroupID.IDA_VT_, typeof(int)));
VR_ = new GroupAttribute("VR_", 5, listVR_);
#endregion VR_
#region EA_
List<PropertiesAttribute> listEA_ = new List<PropertiesAttribute>();
listEA_.Add(new PropertiesAttribute(3, 3, EnumGroupID.IDA_EA_, typeof(string)));
listEA_.Add(new PropertiesAttribute(12, 3, EnumGroupID.IDA_EB_, typeof(int)));
EA_ = new GroupAttribute("EA_", 3, listEA_);
#endregion EA_
#region ST_
List<PropertiesAttribute> listST_ = new List<PropertiesAttribute>();
listST_.Add(new PropertiesAttribute(7, 3, EnumGroupID.IDA_ST_, typeof(string)));
listST_.Add(new PropertiesAttribute(8, 3, EnumGroupID.IDA_AD_, typeof(DateTime)));
listST_.Add(new PropertiesAttribute(8, 3, EnumGroupID.IDA_SD_, typeof(DateTime)));
ST_ = new GroupAttribute("ST_", 3, listST_);
#endregion ST_
#region DTI
List<PropertiesAttribute> listDTI = new List<PropertiesAttribute>();
listDTI.Add(new PropertiesAttribute(8, 10, EnumGroupID.IDA_D__, typeof(DateTime)));
listDTI.Add(new PropertiesAttribute(120, 10, EnumGroupID.IDA_T__, typeof(string)));
listDTI.Add(new PropertiesAttribute(420, 10, EnumGroupID.IDA_I__, typeof(string)));
DTI = new GroupAttribute("DTI", 10, listDTI);
#endregion DTI
TongSoByte = 9580;
}

}public partial class EnumGroupID
    {
public static readonly string IDA_BL_ = "IDA_BL_";
public static readonly string IDA_OL_ = "IDA_OL_";
public static readonly string IDA_SS_ = "IDA_SS_";
public static readonly string IDA_SN_ = "IDA_SN_";
public static readonly string IDA_VR_ = "IDA_VR_";
public static readonly string IDA_VI_ = "IDA_VI_";
public static readonly string IDA_VC_ = "IDA_VC_";
public static readonly string IDA_VP_ = "IDA_VP_";
public static readonly string IDA_VT_ = "IDA_VT_";
public static readonly string IDA_EA_ = "IDA_EA_";
public static readonly string IDA_EB_ = "IDA_EB_";
public static readonly string IDA_ST_ = "IDA_ST_";
public static readonly string IDA_AD_ = "IDA_AD_";
public static readonly string IDA_SD_ = "IDA_SD_";
public static readonly string IDA_D__ = "IDA_D__";
public static readonly string IDA_T__ = "IDA_T__";
public static readonly string IDA_I__ = "IDA_I__";
}

}
