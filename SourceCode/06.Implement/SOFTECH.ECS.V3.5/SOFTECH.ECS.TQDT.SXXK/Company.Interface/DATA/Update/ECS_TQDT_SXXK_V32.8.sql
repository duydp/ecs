SET QUOTED_IDENTIFIER ON;
SET ANSI_NULLS ON;
GO

/*----------------------------------------------------------------------*/
CREATE VIEW [dbo].[v_t_KDT_SXXK_BCXuatNhapTon]
AS
SELECT ID ,
       STT ,
       LanThanhLy ,
       NamThanhLy ,
       MaDoanhNghiep ,
       MaNPL ,
       TenNPL ,
	   CASE WHEN YEAR(NgayDangKyNhap) >=2015  THEN (SELECT TOP 1 SoTKVNACCS FROM dbo.t_VNACCS_CapSoToKhai TKMD WHERE TKMD.SoTK=t_KDT_SXXK_BCXuatNhapTon.SoToKhaiNhap AND TKMD.NamDangKy = YEAR(t_KDT_SXXK_BCXuatNhapTon.NgayDangKyNhap)) ELSE SoToKhaiNhap END AS SoTKNhapVNACCS,
       SoToKhaiNhap ,
       NgayDangKyNhap ,
       NgayHoanThanhNhap ,
       MaLoaiHinhNhap ,
       LuongNhap ,
       LuongTonDau ,
       TenDVT_NPL ,
       MaSP ,
       TenSP ,
	   CASE WHEN YEAR(NgayDangKyNhap) >=2015 THEN (SELECT TOP 1 SoTKVNACCS FROM dbo.t_VNACCS_CapSoToKhai TKMD WHERE TKMD.SoTK=t_KDT_SXXK_BCXuatNhapTon.SoToKhaiXuat AND TKMD.NamDangKy = YEAR(t_KDT_SXXK_BCXuatNhapTon.NgayDangKyXuat)) ELSE SoToKhaiXuat END AS SoTKXuatVNACCS,
       SoToKhaiXuat ,
       NgayDangKyXuat ,
       NgayHoanThanhXuat ,
       MaLoaiHinhXuat ,
       LuongSPXuat ,
       TenDVT_SP ,
       DinhMuc ,
       LuongNPLSuDung ,
       SoToKhaiTaiXuat ,
       NgayTaiXuat ,
       LuongNPLTaiXuat ,
       LuongTonCuoi ,
       ThanhKhoanTiep ,
       ChuyenMucDichKhac ,
       DonGiaTT ,
       TyGiaTT ,
       ThueSuat ,
       ThueXNK ,
       ThueXNKTon ,
       NgayThucXuat FROM dbo.t_KDT_SXXK_BCXuatNhapTon
GO

SET QUOTED_IDENTIFIER ON;
SET ANSI_NULLS ON;
GO

/*----------------------------------------------------------------------*/
ALTER VIEW [dbo].[v_KDT_SXXK_NPLXuatTon]
AS
    SELECT TOP ( 100 ) PERCENT
        CASE WHEN a.SoToKhai <> 0
             THEN ( SELECT TOP 1
                            tkmd.SoTKVNACCS
                    FROM    dbo.t_VNACCS_CapSoToKhai tkmd
                    WHERE   tkmd.SoTK = a.SoToKhai
                            AND tkmd.NamDangKy = a.NamDangKy
                  )
             ELSE a.SoToKhai
        END AS SoToKhaiVNACCS ,
			a.SoToKhai,
            a.MaLoaiHinh ,
            a.NamDangKy ,
            a.MaHaiQuan ,
            c.MaSanPham AS MaSP ,
            b.TenHang AS TenSP ,
            e.Ten AS TenDVT_SP ,
            b.SoLuong AS LuongSP ,
            c.MaNguyenPhuLieu AS MaNPL ,
            c.DinhMucChung AS DinhMuc ,
            b.SoLuong * c.DinhMucChung AS LuongNPL ,
            b.SoLuong * c.DinhMucChung AS TonNPL ,
            a.BangKeHoSoThanhLy_ID ,
            CASE WHEN YEAR(d.NGAY_THN_THX) > 1900 THEN d.NGAY_THN_THX
                 ELSE d.NgayDangKy
            END AS NgayThucXuat ,
            CASE WHEN YEAR(d.NgayHoanThanh) > 1900 THEN d.NgayHoanThanh
                 ELSE d.NgayDangKy
            END AS NgayHoanThanhXuat ,
            d.NgayHoanThanh ,
            a.NgayDangKy ,
            b.MaHaiQuan AS Expr1
    FROM    dbo.t_KDT_SXXK_BKToKhaiXuat AS a
            INNER JOIN dbo.t_SXXK_ToKhaiMauDich AS d ON a.SoToKhai = d.SoToKhai
                                                        AND a.MaLoaiHinh = d.MaLoaiHinh
                                                        AND a.NamDangKy = d.NamDangKy
                                                        AND a.MaHaiQuan = d.MaHaiQuan
            INNER JOIN dbo.t_SXXK_HangMauDich AS b ON a.SoToKhai = b.SoToKhai
                                                      AND a.MaLoaiHinh = b.MaLoaiHinh
                                                      AND a.NamDangKy = b.NamDangKy
                                                      AND a.MaHaiQuan = b.MaHaiQuan
            INNER JOIN dbo.t_SXXK_DinhMuc AS c ON b.MaPhu = c.MaSanPham
                                                  AND b.MaHaiQuan = c.MaHaiQuan
                                                  AND d.MaDoanhNghiep = c.MaDoanhNghiep
            INNER JOIN dbo.t_HaiQuan_DonViTinh AS e ON b.DVT_ID = e.ID
    WHERE   ( a.MaLoaiHinh LIKE 'XSX%'
              OR a.MaLoaiHinh LIKE 'XV%'
              OR a.MaLoaiHinh LIKE 'XCX%'
            )
            AND ( d.Xuat_NPL_SP = 'S' )
    ORDER BY d.NgayHoanThanh ,
            a.SoToKhai;


GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '32.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('32.8',GETDATE(), N'Cập nhật nguyên phụ liệu tồn')
END