﻿namespace Company.Interface
{
    partial class FrmQuanLyHopDongXuatKhau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grdHopDongXuatKhau_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference grdHopDongXuatKhau_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmQuanLyHopDongXuatKhau));
            this.lblTriGia = new System.Windows.Forms.Label();
            this.grpSoHopDong = new System.Windows.Forms.GroupBox();
            this.cboNam = new Janus.Windows.EditControls.UIComboBox();
            this.cboSoHopDong = new Janus.Windows.EditControls.UIComboBox();
            this.btnCapNhat = new Janus.Windows.EditControls.UIButton();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.txtGhiChu = new System.Windows.Forms.TextBox();
            this.btnThemMoi = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.numTriGia = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNgayDK = new System.Windows.Forms.Label();
            this.lblSoHD = new System.Windows.Forms.Label();
            this.dtNgayKyHopDong = new System.Windows.Forms.DateTimePicker();
            this.imgSmall = new System.Windows.Forms.ImageList(this.components);
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox17 = new Janus.Windows.EditControls.UIGroupBox();
            this.grdHopDongXuatKhau = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.grpSoHopDong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTriGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).BeginInit();
            this.uiGroupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHopDongXuatKhau)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grdHopDongXuatKhau);
            this.grbMain.Controls.Add(this.uiGroupBox17);
            this.grbMain.Controls.Add(this.grpSoHopDong);
            this.grbMain.Size = new System.Drawing.Size(778, 368);
            // 
            // lblTriGia
            // 
            this.lblTriGia.AutoSize = true;
            this.lblTriGia.BackColor = System.Drawing.Color.Transparent;
            this.lblTriGia.Location = new System.Drawing.Point(363, 48);
            this.lblTriGia.Name = "lblTriGia";
            this.lblTriGia.Size = new System.Drawing.Size(40, 13);
            this.lblTriGia.TabIndex = 0;
            this.lblTriGia.Text = "Trị giá:";
            // 
            // grpSoHopDong
            // 
            this.grpSoHopDong.BackColor = System.Drawing.Color.Transparent;
            this.grpSoHopDong.Controls.Add(this.cboNam);
            this.grpSoHopDong.Controls.Add(this.cboSoHopDong);
            this.grpSoHopDong.Controls.Add(this.btnCapNhat);
            this.grpSoHopDong.Controls.Add(this.txtGhiChu);
            this.grpSoHopDong.Controls.Add(this.btnThemMoi);
            this.grpSoHopDong.Controls.Add(this.label1);
            this.grpSoHopDong.Controls.Add(this.numTriGia);
            this.grpSoHopDong.Controls.Add(this.label3);
            this.grpSoHopDong.Controls.Add(this.lblNgayDK);
            this.grpSoHopDong.Controls.Add(this.lblSoHD);
            this.grpSoHopDong.Controls.Add(this.lblTriGia);
            this.grpSoHopDong.Controls.Add(this.dtNgayKyHopDong);
            this.grpSoHopDong.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpSoHopDong.Location = new System.Drawing.Point(0, 0);
            this.grpSoHopDong.Name = "grpSoHopDong";
            this.grpSoHopDong.Size = new System.Drawing.Size(778, 127);
            this.grpSoHopDong.TabIndex = 0;
            this.grpSoHopDong.TabStop = false;
            this.grpSoHopDong.Text = "Nhập thông tin";
            // 
            // cboNam
            // 
            this.cboNam.Location = new System.Drawing.Point(57, 16);
            this.cboNam.Name = "cboNam";
            this.cboNam.Size = new System.Drawing.Size(128, 21);
            this.cboNam.TabIndex = 0;
            this.cboNam.VisualStyleManager = this.vsmMain;
            this.cboNam.SelectedIndexChanged += new System.EventHandler(this.cboNam_SelectedIndexChanged);
            // 
            // cboSoHopDong
            // 
            this.cboSoHopDong.Location = new System.Drawing.Point(57, 43);
            this.cboSoHopDong.Name = "cboSoHopDong";
            this.cboSoHopDong.Size = new System.Drawing.Size(128, 21);
            this.cboSoHopDong.TabIndex = 1;
            this.cboSoHopDong.VisualStyleManager = this.vsmMain;
            this.cboSoHopDong.SelectedIndexChanged += new System.EventHandler(this.cboSoHopDong_SelectedIndexChanged);
            this.cboSoHopDong.Leave += new System.EventHandler(this.cboSoHopDong_Leave);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Enabled = false;
            this.btnCapNhat.Image = ((System.Drawing.Image)(resources.GetObject("btnCapNhat.Image")));
            this.btnCapNhat.ImageList = this.imageList;
            this.btnCapNhat.Location = new System.Drawing.Point(138, 97);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(75, 23);
            this.btnCapNhat.TabIndex = 6;
            this.btnCapNhat.Text = "Cập Nhật";
            this.btnCapNhat.VisualStyleManager = this.vsmMain;
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click_1);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "83.png");
            this.imageList.Images.SetKeyName(1, "disk.png");
            this.imageList.Images.SetKeyName(2, "edit.png");
            this.imageList.Images.SetKeyName(3, "erase.png");
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(57, 70);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(452, 21);
            this.txtGhiChu.TabIndex = 4;
            // 
            // btnThemMoi
            // 
            this.btnThemMoi.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnThemMoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemMoi.Image = ((System.Drawing.Image)(resources.GetObject("btnThemMoi.Image")));
            this.btnThemMoi.ImageList = this.imageList;
            this.btnThemMoi.Location = new System.Drawing.Point(57, 97);
            this.btnThemMoi.Name = "btnThemMoi";
            this.btnThemMoi.Size = new System.Drawing.Size(75, 23);
            this.btnThemMoi.TabIndex = 5;
            this.btnThemMoi.Text = "Thêm";
            this.btnThemMoi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnThemMoi.Click += new System.EventHandler(this.btnThemMoi_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ghi Chú:";
            // 
            // numTriGia
            // 
            this.numTriGia.DecimalPlaces = 5;
            this.numTriGia.Location = new System.Drawing.Point(409, 43);
            this.numTriGia.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.numTriGia.Name = "numTriGia";
            this.numTriGia.Size = new System.Drawing.Size(100, 21);
            this.numTriGia.TabIndex = 3;
            this.numTriGia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numTriGia.ThousandsSeparator = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Năm:";
            // 
            // lblNgayDK
            // 
            this.lblNgayDK.AutoSize = true;
            this.lblNgayDK.Location = new System.Drawing.Point(191, 47);
            this.lblNgayDK.Name = "lblNgayDK";
            this.lblNgayDK.Size = new System.Drawing.Size(50, 13);
            this.lblNgayDK.TabIndex = 1;
            this.lblNgayDK.Text = "Ngày ký:";
            // 
            // lblSoHD
            // 
            this.lblSoHD.AutoSize = true;
            this.lblSoHD.Location = new System.Drawing.Point(10, 48);
            this.lblSoHD.Name = "lblSoHD";
            this.lblSoHD.Size = new System.Drawing.Size(41, 13);
            this.lblSoHD.TabIndex = 0;
            this.lblSoHD.Text = "Số HĐ:";
            // 
            // dtNgayKyHopDong
            // 
            this.dtNgayKyHopDong.CustomFormat = "dd/MM/yyyy";
            this.dtNgayKyHopDong.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNgayKyHopDong.Location = new System.Drawing.Point(246, 43);
            this.dtNgayKyHopDong.Name = "dtNgayKyHopDong";
            this.dtNgayKyHopDong.Size = new System.Drawing.Size(100, 21);
            this.dtNgayKyHopDong.TabIndex = 2;
            // 
            // imgSmall
            // 
            this.imgSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgSmall.ImageStream")));
            this.imgSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.imgSmall.Images.SetKeyName(0, "");
            this.imgSmall.Images.SetKeyName(1, "");
            this.imgSmall.Images.SetKeyName(2, "");
            this.imgSmall.Images.SetKeyName(3, "");
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(696, 11);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageList = this.imageList;
            this.btnXoa.Location = new System.Drawing.Point(615, 11);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 3;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(5, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(231, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Double Click dòng trên lưới để chỉnh sửa";
            // 
            // uiGroupBox17
            // 
            this.uiGroupBox17.AutoScroll = true;
            this.uiGroupBox17.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox17.Controls.Add(this.btnClose);
            this.uiGroupBox17.Controls.Add(this.label2);
            this.uiGroupBox17.Controls.Add(this.btnXoa);
            this.uiGroupBox17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox17.Location = new System.Drawing.Point(0, 328);
            this.uiGroupBox17.Name = "uiGroupBox17";
            this.uiGroupBox17.Size = new System.Drawing.Size(778, 40);
            this.uiGroupBox17.TabIndex = 25;
            this.uiGroupBox17.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox17.VisualStyleManager = this.vsmMain;
            // 
            // grdHopDongXuatKhau
            // 
            this.grdHopDongXuatKhau.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdHopDongXuatKhau.CardCaptionFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHopDongXuatKhau.CardColumnHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHopDongXuatKhau.ColumnAutoResize = true;
            grdHopDongXuatKhau_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("grdHopDongXuatKhau_DesignTimeLayout_Reference_0.Instance")));
            grdHopDongXuatKhau_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            grdHopDongXuatKhau_DesignTimeLayout_Reference_0});
            grdHopDongXuatKhau_DesignTimeLayout.LayoutString = resources.GetString("grdHopDongXuatKhau_DesignTimeLayout.LayoutString");
            this.grdHopDongXuatKhau.DesignTimeLayout = grdHopDongXuatKhau_DesignTimeLayout;
            this.grdHopDongXuatKhau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdHopDongXuatKhau.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic;
            this.grdHopDongXuatKhau.FilterRowButtonStyle = Janus.Windows.GridEX.FilterRowButtonStyle.ConditionOperatorDropDown;
            this.grdHopDongXuatKhau.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grdHopDongXuatKhau.GroupByBoxVisible = false;
            this.grdHopDongXuatKhau.ImageList = this.imgSmall;
            this.grdHopDongXuatKhau.IncrementalSearchMode = Janus.Windows.GridEX.IncrementalSearchMode.FirstCharacter;
            this.grdHopDongXuatKhau.Location = new System.Drawing.Point(0, 127);
            this.grdHopDongXuatKhau.Name = "grdHopDongXuatKhau";
            this.grdHopDongXuatKhau.RecordNavigator = true;
            this.grdHopDongXuatKhau.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.grdHopDongXuatKhau.Size = new System.Drawing.Size(778, 201);
            this.grdHopDongXuatKhau.TabIndex = 26;
            this.grdHopDongXuatKhau.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdHopDongXuatKhau.TotalRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdHopDongXuatKhau.TotalRowPosition = Janus.Windows.GridEX.TotalRowPosition.BottomFixed;
            this.grdHopDongXuatKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grdHopDongXuatKhau.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grdHopDongXuatKhau_RowDoubleClick);
            // 
            // FrmQuanLyHopDongXuatKhau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 368);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "FrmQuanLyHopDongXuatKhau";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Quản lý hợp đồng xuất khẩu";
            this.Load += new System.EventHandler(this.FrmQuanLyHopDongXuatKhau_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.grpSoHopDong.ResumeLayout(false);
            this.grpSoHopDong.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTriGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).EndInit();
            this.uiGroupBox17.ResumeLayout(false);
            this.uiGroupBox17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHopDongXuatKhau)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTriGia;
        private System.Windows.Forms.GroupBox grpSoHopDong;
        private System.Windows.Forms.Label lblNgayDK;
        private System.Windows.Forms.Label lblSoHD;
        private Janus.Windows.EditControls.UIButton btnThemMoi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.DateTimePicker dtNgayKyHopDong;
        private System.Windows.Forms.NumericUpDown numTriGia;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.TextBox txtGhiChu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private Janus.Windows.EditControls.UIButton btnCapNhat;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIComboBox cboSoHopDong;
        private System.Windows.Forms.ImageList imgSmall;
        private Janus.Windows.EditControls.UIComboBox cboNam;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox17;
        private Janus.Windows.GridEX.GridEX grdHopDongXuatKhau;
    }
}
