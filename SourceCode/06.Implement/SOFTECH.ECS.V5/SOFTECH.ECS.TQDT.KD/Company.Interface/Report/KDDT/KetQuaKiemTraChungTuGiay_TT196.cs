﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KD.BLL;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.GC.BLL;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.BLL;
#endif
//using Company.BLL.KDT;


namespace Company.Interface.Report.SXXK
{
    public partial class KetQuaKiemTraChungTuGiay_TT196 : DevExpress.XtraReports.UI.XtraReport
    {
        public    ToKhaiMauDich TKMD = new ToKhaiMauDich();

        public KetQuaKiemTraChungTuGiay_TT196()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            DateTime minDate = new DateTime(1900, 1, 1);
            //Cuc HQ
            string maCuc = TKMD.MaHaiQuan.Substring(1, 2);
            lblChiCucHQ.Text = DonViHaiQuan.GetName("Z" + maCuc + "Z");
            lblChiCucHQ.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
            
            //Chi cuc Hai quan cua khau
            lblChiCucHDCK.Text = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
            lblChiCucHDCK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

            //Số tờ khai
            if (this.TKMD.SoToKhai > 0)
                lblSoToKhai.Text = this.TKMD.SoToKhai + "";

            //Ngày giờ đăng ký
            if (this.TKMD.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy HH:mm");
            else
                lblNgayDangKy.Text = "";
            
            //01. Nguoi nhap/xuat khau
            lblMSTNguoiNhapSuat.Font = lblNguoiNhapSuat.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
            lblMSTNguoiNhapSuat.Text = TKMD.MaDoanhNghiep;
            lblNguoiNhapSuat.Text = TKMD.TenDoanhNghiep;

            //02.Nguoi Dai Dien
            lblNguoiDaiDien.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
            lblNguoiDaiDien.Text = TKMD.TenChuHang;
            //03. Loai hinh
            string stlh = "";
            stlh = LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
            lblLoaiHinh.Text = TKMD.MaLoaiHinh + " - " + stlh;
            lblLoaiHinh.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

            //04. Hoa Don thuong mai
            lblSoHoaDon.Text = "" + this.TKMD.SoHoaDonThuongMai;
            lblSoHoaDon.Font =new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

            //05. Hop dong
            lblSoHD.Text = "" + this.TKMD.SoHopDong;
            lblSoHD.Font = lblNgayHD.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
            if (this.TKMD.NgayHopDong > minDate)
                lblNgayHD.Text = this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
            else
                lblNgayHD.Text = " ";

            //06. GiayPhep
            if (TKMD.SoGiayPhep != "")
                lblSoGiayPhep.Text = "" + this.TKMD.SoGiayPhep;
            else
                lblSoGiayPhep.Text = "";
            lblSoGiayPhep.Font = lblNgayGiayPhep.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
            if (this.TKMD.NgayGiayPhep > minDate)
                lblNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
            else
                lblNgayGiayPhep.Text = "";

            //07. VanTaiDon
            lblSoVanDon.Text = this.TKMD.SoVanDon;
            lblSoVanDon.Font = lblNgayVanDon.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
            if (this.TKMD.NgayVanDon > minDate)
                lblNgayVanDon.Text = this.TKMD.NgayVanDon.ToString("dd/MM/yyyy");
            else
                lblNgayVanDon.Text = "";
            

        }

            
    }
}
