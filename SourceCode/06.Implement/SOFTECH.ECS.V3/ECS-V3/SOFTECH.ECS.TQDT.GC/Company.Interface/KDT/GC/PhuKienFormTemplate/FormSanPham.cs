﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
namespace Company.Interface.KDT.GC
{
    public partial class FormSanPham : BaseForm
    {
        public HopDong HD = new HopDong();

        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
        public string MaLoaiPK = string.Empty;
        public bool boolFlag;
        private bool isEdit = false;
        public FormSanPham()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void reset()
        {
            txtMa.Text = "";
            txtTen.Text = "";
            txtMaHS.Text = "";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            error.SetError(txtMaHS, null);
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
            isEdit = false;
            HangPK = new HangPhuKien();
        }
        private void setErro()
        {
            error.Clear();
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);
            error.SetError(txtMaHS, null);

        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            txtMa.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;

            checkExitsSanPhamAndSTTHang();
        }
        private void LoadLoaiSanPham()
        {
        }
        private void SanPhamGCEditForm_Load(object sender, EventArgs e)
        {
            bool isSuaSP = false;
            isSuaSP = MaLoaiPK == "502";
            dgList.RootTable.Columns["ThongTinCu"].Visible = txtMaCu.ReadOnly =
            lblCu.Visible = txtMaCu.Visible = lblTenCu.Visible = isSuaSP;
            btnChon.Visible = MaLoaiPK != "802";
            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            dgList.DataSource = HD.SPCollection;
            if (pkdk.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                btnAdd.Enabled = false;
                btnXoa.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
            if (LoaiPK.HPKCollection.Count == 0) LoaiPK.LoadCollection();

            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == MaLoaiPK)
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (boolFlag == true)
            {
                btnAdd.Enabled = true;
            }
            LoaiPK.MaPhuKien = MaLoaiPK;
            if (LoaiPK.HPKCollection.Count == 0)
                LoaiPK.LoadCollection();
            dgList.DataSource = LoaiPK.HPKCollection;

        }

        private void checkExitsSanPhamAndSTTHang()
        {
            if (string.IsNullOrEmpty(MaLoaiPK))
            {
                ShowMessage("Mã loại phụ kiện chưa thiết lập", false);
                DialogResult = DialogResult.Cancel;
            }

            Company.GC.BLL.GC.SanPham sanpham   = new Company.GC.BLL.GC.SanPham();
            sanpham.HopDong_ID = this.HD.ID;
            sanpham.Ma = txtMa.Text;
            if (!isEdit && sanpham.Load())
            {
                showMsg("MSG_240214");
                //ShowMessage("Loại sản phẩm này đã được khai báo.", false);
                cbTen.Focus();
                return;
            }
            //if (MaLoaiPK == "802" && !Globals.ValidateNull(txtMaSpCu, error, "Tên sản phẩm mới"))
            //    return;
            LoaiPK.HPKCollection.Remove(HangPK);
            foreach (HangPhuKien p in LoaiPK.HPKCollection)
            {
                if (p.MaHang.Trim() == cbTen.Ma)
                {
                    //ShowMessage("Đã có loại sản phẩm này trong danh sách.", false);
                    showMsg("MSG_240215");
                    if (HangPK.MaHang.Trim() != "")
                        LoaiPK.HPKCollection.Add(HangPK);
                    txtMa.Focus();
                    return;
                }
            }
            HangPK.MaHang = txtMa.Text;
            HangPK.TenHang = txtTen.Text;
            HangPK.NhomSP = cbTen.Ma;
            HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            HangPK.MaHS = txtMaHS.Text;
            HangPK.ThongTinCu = txtMaCu.Text;
            LoaiPK.HPKCollection.Add(HangPK);
            LoaiPK.Master_ID = pkdk.ID;
            LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
            if (boolFlag == true)
            {
                LoaiPK.InsertUpdateBoSungDaDangKy(pkdk.HopDong_ID);
                boolFlag = false;
            }
            else
            {
                LoaiPK.InsertUpdateBoSungChiTietLoaiPhuKien(pkdk.HopDong_ID);
            }

            Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
            if (lpk == null)
                pkdk.PKCollection.Add(LoaiPK);        
            dgList.DataSource = LoaiPK.HPKCollection;
            reset();
            this.setErro();

        }
        private void txtMaHS_Leave(object sender, EventArgs e)
        {

        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            HangPK = (Company.GC.BLL.KDT.GC.HangPhuKien)e.Row.DataRow;
            if (HangPK != null)
            {
                isEdit = true;
                cbDonViTinh.SelectedValue = HangPK.DVT_ID;
                txtMa.Text = HangPK.MaHang;
                txtTen.Text = HangPK.TenHang;
                txtMaHS.Text = HangPK.MaHS;
                cbTen.Ma = HangPK.NhomSP;
                txtMaCu.Text = HangPK.ThongTinCu;
                cbDonViTinh.SelectedValue = HangPK.DVT_ID;
                error.Clear();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenNhomSanPham"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSanPham_ID"].Value);
                e.Row.Cells["NhomSanPham_ID"].Text = e.Row.Cells["TenNhomSanPham"].Text;
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (!btnAdd.Enabled) return;            
            HangPhuKienCollection hpkColl = new HangPhuKienCollection();
            if (LoaiPK.HPKCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;

            if (showMsg("MSG_DEL01", true) == "Yes")           
            {
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            pkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                        }
                        hpkColl.Add(pkDelete);
                    }
                }
                foreach (Company.GC.BLL.KDT.GC.HangPhuKien hpkk in hpkColl)
                {
                    try
                    {
                        LoaiPK.HPKCollection.Remove(hpkk);
                    }
                    catch { }

                }
                dgList.DataSource = LoaiPK.HPKCollection;
                this.reset();
                this.setErro();
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }

            }
        }

        private void btnChonSP_Click(object sender, EventArgs e)
        {
            FormChonSanPham frm = new FormChonSanPham();
            frm.HD = HD;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    foreach (HangPhuKien hangPK in frm.HPKCollection)
                    {
                        hangPK.Master_ID = LoaiPK.ID;
                        LoaiPK.HPKCollection.Add(hangPK);
                    }
                    LoaiPK.Master_ID = pkdk.ID;
                    LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                    if (boolFlag == true)
                    {
                        LoaiPK.InsertUpdateBoSungDaDangKy(pkdk.HopDong_ID);
                        boolFlag = false;
                    }
                    else
                    {
                        LoaiPK.InsertUpdateBoSungChiTietLoaiPhuKien(pkdk.HopDong_ID);
                    }
                    Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
                    if (lpk == null)
                        pkdk.PKCollection.Add(LoaiPK);
                    dgList.DataSource = LoaiPK.HPKCollection;
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message, false);
                }
                reset();
                this.setErro();
            }

        }


    }
}