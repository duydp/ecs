
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_GC_NguyenPhuLieuBoSung]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_KDT_GC_NguyenPhuLieuBoSung](
	[HopDong_ID] [bigint] NOT NULL,
	[Ma] [varchar](30) NOT NULL,
	[NguonCungCap] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](256) NULL,
 CONSTRAINT [PK_t_KDT_GC_NguyenPhuLieuBoSung_1] PRIMARY KEY CLUSTERED 
(
	[HopDong_ID] ASC,
	[Ma] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Delete]    Script Date: 03/13/2013 17:15:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteBy_HopDong_ID_Ma]    Script Date: 03/13/2013 17:15:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteBy_HopDong_ID_Ma]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteBy_HopDong_ID_Ma]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteDynamic]    Script Date: 03/13/2013 17:15:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Insert]    Script Date: 03/13/2013 17:15:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_InsertUpdate]    Script Date: 03/13/2013 17:15:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Load]    Script Date: 03/13/2013 17:15:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectAll]    Script Date: 03/13/2013 17:15:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectBy_HopDong_ID_Ma]    Script Date: 03/13/2013 17:15:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectBy_HopDong_ID_Ma]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectBy_HopDong_ID_Ma]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectDynamic]    Script Date: 03/13/2013 17:15:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Update]    Script Date: 03/13/2013 17:15:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Update]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Delete]    Script Date: 03/13/2013 17:15:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Delete]
	@HopDong_ID bigint,
	@Ma varchar(30)
AS

DELETE FROM 
	[dbo].[t_KDT_GC_NguyenPhuLieuBoSung]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteBy_HopDong_ID_Ma]    Script Date: 03/13/2013 17:15:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteBy_HopDong_ID_Ma]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteBy_HopDong_ID_Ma]
	@HopDong_ID bigint,
	@Ma varchar(30)
AS

DELETE FROM [dbo].[t_KDT_GC_NguyenPhuLieuBoSung]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteDynamic]    Script Date: 03/13/2013 17:15:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_NguyenPhuLieuBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Insert]    Script Date: 03/13/2013 17:15:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Insert]
	@HopDong_ID bigint,
	@Ma varchar(30),
	@NguonCungCap nvarchar(250),
	@GhiChu nvarchar(256),
	@TuCungUng bit
AS
INSERT INTO [dbo].[t_KDT_GC_NguyenPhuLieuBoSung]
(
	[HopDong_ID],
	[Ma],
	[NguonCungCap],
	[GhiChu],
	[TuCungUng]
)
VALUES
(
	@HopDong_ID,
	@Ma,
	@NguonCungCap,
	@GhiChu,
	@TuCungUng
)


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_InsertUpdate]    Script Date: 03/13/2013 17:15:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_InsertUpdate]
	@HopDong_ID bigint,
	@Ma varchar(30),
	@NguonCungCap nvarchar(250),
	@GhiChu nvarchar(256),
	@TuCungUng bit
AS
IF EXISTS(SELECT [HopDong_ID], [Ma] FROM [dbo].[t_KDT_GC_NguyenPhuLieuBoSung] WHERE [HopDong_ID] = @HopDong_ID AND [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_NguyenPhuLieuBoSung] 
		SET
			[NguonCungCap] = @NguonCungCap,
			[GhiChu] = @GhiChu,
			[TuCungUng] = @TuCungUng
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_GC_NguyenPhuLieuBoSung]
	(
			[HopDong_ID],
			[Ma],
			[NguonCungCap],
			[GhiChu],
			[TuCungUng]
	)
	VALUES
	(
			@HopDong_ID,
			@Ma,
			@NguonCungCap,
			@GhiChu,
			@TuCungUng
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Load]    Script Date: 03/13/2013 17:15:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Load]
	@HopDong_ID bigint,
	@Ma varchar(30)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[NguonCungCap],
	[GhiChu],
	[TuCungUng]
FROM
	[dbo].[t_KDT_GC_NguyenPhuLieuBoSung]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectAll]    Script Date: 03/13/2013 17:15:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[NguonCungCap],
	[GhiChu],
	[TuCungUng]
FROM
	[dbo].[t_KDT_GC_NguyenPhuLieuBoSung]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectBy_HopDong_ID_Ma]    Script Date: 03/13/2013 17:15:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectBy_HopDong_ID_Ma]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectBy_HopDong_ID_Ma]
	@HopDong_ID bigint,
	@Ma varchar(30)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[NguonCungCap],
	[GhiChu],
	[TuCungUng]
FROM
	[dbo].[t_KDT_GC_NguyenPhuLieuBoSung]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectDynamic]    Script Date: 03/13/2013 17:15:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[Ma],
	[NguonCungCap],
	[GhiChu],
	[TuCungUng]
FROM [dbo].[t_KDT_GC_NguyenPhuLieuBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Update]    Script Date: 03/13/2013 17:15:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Update]
	@HopDong_ID bigint,
	@Ma varchar(30),
	@NguonCungCap nvarchar(250),
	@GhiChu nvarchar(256),
	@TuCungUng bit
AS

UPDATE
	[dbo].[t_KDT_GC_NguyenPhuLieuBoSung]
SET
	[NguonCungCap] = @NguonCungCap,
	[GhiChu] = @GhiChu,
	[TuCungUng] = @TuCungUng
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma


GO



UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '8.1'
      ,[Date] = getdate()
      ,[Notes] = ''
      GO	


