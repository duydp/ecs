﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.BLL.Utils;
using System.Threading;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;
using System.Collections.Generic;
namespace Company.BLL.KDT.SXXK
{
    public partial class SanPhamDangKy
    {
        private List<SanPham> _SPCollection = new List<SanPham>();

        public List<SanPham> SPCollection
        {
            set { this._SPCollection = value; }
            get { return this._SPCollection; }
        }

        //-----------------------------------------------------------------------------------------
        public void LoadSPCollection()
        {
            this._SPCollection = (List<SanPham>)SanPham.SelectCollectionBy_Master_ID(this.ID);
        }

        public void LoadSPCollection(SqlTransaction trans)
        {
            this._SPCollection = SanPham.SelectCollectionBy_Master_ID(this.ID, trans);
        }
        //-----------------------------------------------------------------------------------------

        public bool InsertUpdateFull(string connectionString)
        {
            bool ret;
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    //Kiem tra ton tai NPL DK tren Database Target
                    SanPhamDangKy spTemp = SanPhamDangKy.Load(this._SoTiepNhan, this._NgayTiepNhan, this._MaHaiQuan, this._MaDoanhNghiep, this._TrangThaiXuLy, connectionString);

                    //KDT San pham dang ky
                    if (spTemp == null || spTemp._ID == 0)
                        this._ID = this.InsertTransaction(transaction, db);
                    else
                    {
                        this._ID = spTemp.ID;
                        this.UpdateTransaction(transaction, db);
                    }
                    //KDT San pham
                    foreach (SanPham spDetail in this._SPCollection)
                    {
                        spDetail.Master_ID = this._ID;

                        spDetail.ID = spDetail.InsertUpdateTransactionBy(transaction, db);
                    }
                    //SXXK San pham
                    TransgferDataToSXXK(transaction, db);

                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool CloneToDB(SqlTransaction transaction)
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand("p_KDT_SXXK_SanPhamDangKy_Copy");
                db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
                if (transaction != null)
                    db.ExecuteNonQuery(dbCommand, transaction);
                else
                    db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool InsertUpdateFull(string connectionString, int i)
        {
            bool ret;
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    //Kiem tra ton tai NPL DK tren Database Target
                    SanPhamDangKy spTemp = SanPhamDangKy.Load(this._SoTiepNhan, this._NgayTiepNhan, this._MaHaiQuan, this._MaDoanhNghiep, this._TrangThaiXuLy, connectionString);

                    //KDT San pham dang ky
                    if (spTemp == null || spTemp._ID == 0)
                        this._ID = this.InsertTransaction(transaction, db);
                    else
                    {
                        this._ID = spTemp.ID;
                        this.UpdateTransaction(transaction, db);
                    }
                    //KDT San pham
                    foreach (SanPham spDetail in this._SPCollection)
                    {
                        spDetail.Master_ID = this._ID;

                        spDetail.ID = spDetail.InsertUpdateTransactionBy(transaction, db);
                    }
                    //SXXK San pham
                    TransgferDataToSXXK(transaction, db, i);

                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);

                    foreach (SanPham spDetail in this._SPCollection)
                    {
                        if (spDetail.ID == 0)
                        {
                            spDetail.Master_ID = this._ID;
                            spDetail.ID = spDetail.Insert(transaction);
                        }
                        else
                        {
                            SanPham sp = SanPham.Load(spDetail.ID);
                            if (sp == null) throw new Exception("Mã hàng id =" + spDetail.ID + " không tồn tại");
                            BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                            spSXXK.Ma = sp.Ma;
                            spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                            spSXXK.MaHaiQuan = this.MaHaiQuan;
                            spSXXK.LoaiHang_ID = spDetail.LoaiHang_ID;
                            spSXXK.DeleteTransaction(transaction);
                            spDetail.Update(transaction);
                        }
                    }
                    TransgferDataToSXXK(transaction);
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public void InsertUpdateFull(SqlTransaction transaction)
        {
            if (this._ID == 0)
                this._ID = this.InsertTransaction(transaction);
            else
                this.UpdateTransaction(transaction);

            foreach (SanPham spDetail in this._SPCollection)
            {
                if (spDetail.ID == 0)
                {
                    spDetail.Master_ID = this._ID;
                    spDetail.ID = spDetail.Insert(transaction);
                }
                else
                {
                    SanPham sp = SanPham.Load(spDetail.ID);
                    BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                    spSXXK.Ma = sp.Ma;
                    spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                    spSXXK.MaHaiQuan = this.MaHaiQuan;
                    spSXXK.DeleteTransaction(transaction);

                    spDetail.Update(transaction);
                }
            }
            TransgferDataToSXXK(transaction);
        }

        public int Delete()
        {

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    DeleteSPCollection(transaction);
                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }

            }
            return 1;

        }

        public void DeleteSPCollection(SqlTransaction transaction)
        {
            if (SPCollection.Count == 0)
                this.LoadSPCollection(transaction);
            string sql = "delete from t_KDT_SXXK_SanPham where master_id=@ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                db.ExecuteNonQuery(dbCommand, transaction);
            else
                db.ExecuteNonQuery(dbCommand);
            foreach (SanPham sp in this.SPCollection)
            {
                BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                spSXXK.Ma = sp.Ma;
                spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                spSXXK.MaHaiQuan = this.MaHaiQuan;
                spSXXK.DeleteTransaction(transaction);
            }
        }

        public static void DongBoDuLieuPhongKhai(SanPhamDangKyCollection spDKCollection)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (SanPhamDangKy spDangKy in spDKCollection)
                    {
                        foreach (SanPham sp in spDangKy.SPCollection)
                        {
                            sp.ID = 0;
                        }
                        MsgSend msg = new MsgSend();
                        msg.master_id = spDangKy.ID;
                        msg.LoaiHS = "SP";
                        msg.DeleteTransaction(transaction);
                        spDangKy.DeleteSPCollection(transaction);
                        spDangKy.InsertUpdateFull(transaction);
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }


        }

        public void TransgferDataToSXXK()
        {
            if (this.SPCollection.Count == 0)
            {
                this.LoadSPCollection();
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction);

                    foreach (SanPham sp in this.SPCollection)
                    {
                        BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                        spSXXK.DVT_ID = sp.DVT_ID;
                        spSXXK.Ma = sp.Ma;
                        spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                        spSXXK.MaHaiQuan = this.MaHaiQuan;
                        spSXXK.MaHS = sp.MaHS;
                        spSXXK.Ten = sp.Ten;
                        spSXXK.InsertUpdateTransaction(transaction, db);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void TransgferDataToSXXK(SqlTransaction transaction)
        {
            if (SPCollection.Count == 0)
                this.LoadSPCollection(transaction);
            foreach (SanPham sp in this.SPCollection)
            {
                BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                spSXXK.DVT_ID = sp.DVT_ID;
                spSXXK.Ma = sp.Ma;
                spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                spSXXK.MaHaiQuan = this.MaHaiQuan;
                spSXXK.MaHS = sp.MaHS;
                spSXXK.Ten = sp.Ten;
                spSXXK.InsertUpdateTransaction(transaction, db);
            }
        }

        public void TransgferDataToSXXK(SqlTransaction transaction, SqlDatabase db)
        {
            if (SPCollection.Count == 0)
                this.LoadSPCollection(transaction);
            foreach (SanPham sp in this.SPCollection)
            {
                BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                spSXXK.DVT_ID = sp.DVT_ID;
                spSXXK.Ma = sp.Ma;
                spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                spSXXK.MaHaiQuan = this.MaHaiQuan;
                spSXXK.MaHS = sp.MaHS;
                spSXXK.Ten = sp.Ten;
                spSXXK.InsertUpdateTransaction(transaction, db);

                //TODO: HungTQ, Update 27/04/2010.
                Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("KDT San pham: MaHaiQuan={5}, MaDoanhNghiep={6}, Ma={1}, Ten={2}, DVT={3}, MaHS={4} ", spSXXK.Ma, spSXXK.Ten, spSXXK.DVT_ID, spSXXK.MaHS, spSXXK.MaHaiQuan, spSXXK.MaDoanhNghiep)));

            }
        }

        public void TransgferDataToSXXK(SqlTransaction transaction, SqlDatabase db, int i)
        {
            if (SPCollection.Count == 0)
                this.LoadSPCollection(transaction);
            foreach (SanPham sp in this.SPCollection)
            {
                BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                spSXXK.DVT_ID = sp.DVT_ID;
                spSXXK.Ma = sp.Ma;
                spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                spSXXK.MaHaiQuan = this.MaHaiQuan;
                spSXXK.MaHS = sp.MaHS;
                spSXXK.Ten = sp.Ten;
                spSXXK.InsertUpdateTransaction(transaction, db);

                //TODO: HungTQ, Update 27/04/2010.
                Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("{6}.KDT San pham: MaHaiQuan={4}, MaDoanhNghiep={5}, Ma={0}, Ten={1}, DVT={2}, MaHS={3} ", spSXXK.Ma, spSXXK.Ten, spSXXK.DVT_ID, spSXXK.MaHS, spSXXK.MaHaiQuan, spSXXK.MaDoanhNghiep, i)));

            }
        }

        #region WebService Methods của Softech
        ////-----------------------------------------------------------------------------------------
        ///// <summary>
        ///// Kiểm tra sự tồn tại của các mã sản phẩm. 
        ///// </summary>
        ///// <returns>Danh sách sản phẩm đã tồn tại.</returns>
        //public SanPhamCollection WSCheckExist()
        //{
        //    // Chuẩn bị dữ liệu để gửi qua WEB SERVICE.
        //    SXXKService sxxkService = new SXXKService();
        //    string[] danhsachSP = new string[this.SPCollection.Count];
        //    for (int i = 0; i < this.SPCollection.Count; i++)
        //    {
        //        danhsachSP[i] = this.SPCollection[i].Ma;
        //    }
        //    string[] danhsachSPDaDangKy = sxxkService.SanPham_CheckExist(this.MaHaiQuan, this.MaDoanhNghiep, danhsachSP);

        //    // Dữ liệu trả về.
        //    SanPhamCollection collection = new SanPhamCollection();
        //    foreach (string s in danhsachSPDaDangKy)
        //    {
        //        foreach (SanPham sp in this.SPCollection)
        //        {
        //            if (sp.Ma == s)
        //            {
        //                collection.Add(sp);
        //                break;
        //            }
        //        }
        //    }
        //    return collection;
        //}

        ////-----------------------------------------------------------------------------------------
        //public long WSSend(ref string[] DanhSachDaDangKy)
        //{
        //    // Chuẩn bị dữ liệu để gửi qua WEB SERVICE.
        //    // Lấy thông tin đăng ký.
        //    KDT_SXXK_Service kdtService = new KDT_SXXK_Service();
        //    SanPhamDangKyInfo webSPDangKyInfo = new SanPhamDangKyInfo();
        //    webSPDangKyInfo.MaHaiQuan = this._MaHaiQuan;
        //    webSPDangKyInfo.MaDoanhNghiep = this._MaDoanhNghiep;
        //    webSPDangKyInfo.MaDaiLy = this.MaDaiLy;
        //    webSPDangKyInfo.SoTiepNhan = this.SoTiepNhan;
        //    webSPDangKyInfo.NamTiepNhan =(short)this.NgayTiepNhan.Year;
        //    webSPDangKyInfo.NgayTiepNhan = this.NgayTiepNhan;
        //    // Lấy danh sách sản phẩm.
        //    int count = this._SPCollection.Count;
        //    webSPDangKyInfo.SPInfoCollection = new SanPhamInfo[count];
        //    int i = 0;
        //    foreach (SanPham spDetail in this._SPCollection)
        //    {
        //        SanPhamInfo webSPInfo = new SanPhamInfo();
        //        webSPInfo.MaHaiQuan = this._MaHaiQuan;
        //        webSPInfo.MaDoanhNghiep = this._MaDoanhNghiep;
        //        webSPInfo.Ma = spDetail.Ma;
        //        webSPInfo.Ten = spDetail.Ten;
        //        webSPInfo.MaHS = spDetail.MaHS;
        //        webSPInfo.DVT_ID = spDetail.DVT_ID;
        //        webSPDangKyInfo.SPInfoCollection[i] = webSPInfo;
        //        i++;
        //    }

        //    // Thực hiện gửi dữ liệu.
        //    this._SoTiepNhan = kdtService.SanPham_Send(webSPDangKyInfo, ref DanhSachDaDangKy);
        //    if (this._SoTiepNhan > 0)
        //    {
        //        this._TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
        //        this._NgayTiepNhan = DateTime.Today;
        //        this.InsertUpdateFull();
        //    }
        //    return this._SoTiepNhan;
        //}

        ////-----------------------------------------------------------------------------------------
        ///// <summary>
        ///// Hủy thông tin khai báo.
        ///// </summary>
        //public bool WSCancel()
        //{
        //    KDT_SXXK_Service kdtService = new KDT_SXXK_Service();
        //    bool result = kdtService.SanPham_Cancel(this._SoTiepNhan, this._MaHaiQuan, this._MaDoanhNghiep, this._NgayTiepNhan.Year);
        //    if (result)
        //    {
        //        // Trả lại trạng thái chưa khai báo.
        //        this.SoTiepNhan = 0;
        //        this.NgayTiepNhan = new DateTime(1900, 1, 1);
        //        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
        //        this.Update();
        //    }
        //    return result;
        //}

        ////-----------------------------------------------------------------------------------------
        //public void WSDownload()
        //{
        //    // Các bước thực hiện:
        //    // 1. Lấy thông tin từ Web Service.
        //    // 2. Cập nhật thông tin đăng ký.
        //    // 3. Xóa danh sách sản phẩm cũ.
        //    // 4. Cập nhật lại danh sách sản phẩm mới.
        //    // 5. Nếu trạng thái: Đã duyệt chính thức thì chuyển danh sách sản phẩm đã duyệt sang phân hệ SXXK.

        //    // 1. Lấy danh sách từ Web Service.
        //    KDT_SXXK_Service kdtService = new KDT_SXXK_Service();
        //    DataSet ds = kdtService.SanPham_RequestStatus(this._SoTiepNhan, this._MaHaiQuan, this._MaDoanhNghiep, Convert.ToInt16(this._NgayTiepNhan.Year));
        //    DataTable dt = ds.Tables[0];
        //    if (dt.Rows.Count > 0)
        //    {
        //        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //        using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //        {
        //            connection.Open();
        //            SqlTransaction transaction = connection.BeginTransaction();
        //            try
        //            {
        //                // 2. Cập nhật thông tin đăng ký.
        //                this._TrangThaiXuLy = Convert.ToInt32(dt.Rows[0]["TrangThaiXL"]);

        //                // 3. Xóa danh sách sản phẩm cũ.
        //                this.LoadSPCollection();
        //                SanPham.DeleteCollection(this._SPCollection, transaction);
        //                this.SPCollection.Clear();

        //                // 4. Cập nhật lại danh sách sản phẩm mới.
        //                int stt = 1;
        //                foreach (DataRow row in dt.Rows)
        //                {
        //                    SanPham sp = new SanPham();
        //                    sp.Master_ID = this._ID;
        //                    sp.Ma = row["Ma_SP"].ToString();
        //                    sp.Ten = row["Ten_SP"].ToString();
        //                    sp.MaHS = row["Ma_HS"].ToString();
        //                    sp.DVT_ID = row["Ma_DVT"].ToString();
        //                    sp.STTHang = stt++;
        //                    this._SPCollection.Add(sp);
        //                }

        //                // Cập nhật vào CSDL.
        //                this.InsertUpdateFull(transaction);

        //                // 5. Nếu trạng thái là đã duyệt chính thức thì chuyển danh sách sản phẩm đã duyệt sang phân hệ SXXK.
        //                if (this._TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
        //                {
        //                    foreach (DataRow row in dt.Rows)
        //                    {
        //                        BLL.SXXK.SanPham sp = new BLL.SXXK.SanPham();
        //                        sp.MaHaiQuan = this._MaHaiQuan;
        //                        sp.MaDoanhNghiep = this._MaDoanhNghiep;
        //                        sp.Ma = row["Ma_SP"].ToString();
        //                        sp.Ten = row["Ten_SP"].ToString();
        //                        sp.MaHS = row["Ma_HS"].ToString();
        //                        sp.DVT_ID = row["Ma_DVT"].ToString();

        //                        sp.InsertUpdateTransaction(transaction);
        //                    }
        //                }

        //                transaction.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                throw new Exception(ex.Message);
        //            }
        //            finally
        //            {
        //                connection.Close();
        //            }
        //        }
        //    }
        //    else
        //    {
        //        this.Update();
        //    }
        //}

        //public void WSRequestStatus()
        //{
        //    // Các bước thực hiện:
        //    // 1. Lấy thông tin từ Web Service.
        //    // 2. Cập nhật thông tin đăng ký trạng thái.
        //    // 3. Nếu trạng thái: Đã duyệt chính thức thì chuyển danh sách sản phẩm đã duyệt sang phân hệ SXXK.

        //    // 1. Lấy danh sách từ Web Service.
        //    KDT_SXXK_Service kdtService = new KDT_SXXK_Service();
        //    DataSet ds = kdtService.SanPham_RequestStatus(this._SoTiepNhan, this._MaHaiQuan, this._MaDoanhNghiep, Convert.ToInt16(this._NgayTiepNhan.Year));
        //    DataTable dt = ds.Tables[0];
        //    if (dt.Rows.Count > 0)
        //    {
        //        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //        using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //        {
        //            connection.Open();
        //            SqlTransaction transaction = connection.BeginTransaction();
        //            try
        //            {
        //                // 2. Cập nhật thông tin đăng ký.
        //                this._TrangThaiXuLy = Convert.ToInt32(dt.Rows[0]["TrangThaiXL"]);

        //                // 3. Xóa danh sách sản phẩm cũ.
        //                //this.LoadSPCollection();
        //                //SanPham.DeleteCollection(this._SPCollection, transaction);
        //                //this.SPCollection.Clear();

        //                //// 4. Cập nhật lại danh sách sản phẩm mới.
        //                //int stt = 1;
        //                //foreach (DataRow row in dt.Rows)
        //                //{
        //                //    SanPham sp = new SanPham();
        //                //    sp.Master_ID = this._ID;
        //                //    sp.Ma = row["Ma_SP"].ToString();
        //                //    sp.Ten = row["Ten_SP"].ToString();
        //                //    sp.MaHS = row["Ma_HS"].ToString();
        //                //    sp.DVT_ID = row["Ma_DVT"].ToString();
        //                //    sp.STTHang = stt++;
        //                //    this._SPCollection.Add(sp);
        //                //}

        //                // Cập nhật vào CSDL.
        //                this.UpdateTransaction(transaction);

        //                // 5. Nếu trạng thái là đã duyệt chính thức thì chuyển danh sách sản phẩm đã duyệt sang phân hệ SXXK.
        //                if (this._TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
        //                {
        //                    foreach (DataRow row in dt.Rows)
        //                    {
        //                        BLL.SXXK.SanPham sp = new BLL.SXXK.SanPham();
        //                        sp.MaHaiQuan = this._MaHaiQuan;
        //                        sp.MaDoanhNghiep = this._MaDoanhNghiep;
        //                        sp.Ma = row["Ma_SP"].ToString();
        //                        sp.Ten = row["Ten_SP"].ToString();
        //                        sp.MaHS = row["Ma_HS"].ToString();
        //                        sp.DVT_ID = row["Ma_DVT"].ToString();
        //                        sp.InsertUpdateTransaction(transaction);
        //                    }
        //                }

        //                transaction.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                throw new Exception(ex.Message);
        //            }
        //            finally
        //            {
        //                connection.Close();
        //            }
        //        }
        //    }
        //    else
        //    {
        //        this.Update();
        //    }
        //}
        //#endregion WebService Methods của Softech

        #region WebService Methods của Hải quan
        private string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            //set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            //LannT khong cho phep them moi GuidStr o day
            //this.GUIDSTR = (System.Guid.NewGuid().ToString());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            this.Update();
            return doc.InnerXml;
        }
        private string ConfigPhongBiPhanHoi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;

            return doc.InnerXml;
        }

        #region Khai báo danh sách sản phẩm
        public string WSSendXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucSanPham, MessgaseFunction.KhaiBao));
            XmlDocument docSP = new XmlDocument();
            docSP.LoadXml(ConvertCollectionToXML());

            //luu vao string
            XmlNode root = doc.ImportNode(docSP.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {

                kq = kdt.Send(doc.InnerXml, pass);
                Globals.SaveMessage(doc.InnerXml, this.ID, MessageTitle.KhaiBaoSanPham);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    //XmlNode node = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    //XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";
        }
        private string ConvertCollectionToXML()
        {
            //load du lieu
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\KhaiBaoSanPham.xml");
            XmlNode nplNode = docNPL.GetElementsByTagName("SP")[0];

            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);


            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            if (this.SPCollection == null || this.SPCollection.Count == 0)
            {
                this.SPCollection = (List<SanPham>)SanPham.SelectCollectionBy_Master_ID(this.ID);
            }
            foreach (SanPham sp in this.SPCollection)
            {
                XmlNode node = docNPL.CreateElement("SP.ITEM");
                XmlAttribute sttAtt = docNPL.CreateAttribute("STT");
                sttAtt.Value = sp.STTHang.ToString();
                node.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docNPL.CreateAttribute("MA_SP");
                maAtt.Value = sp.Ma;
                node.Attributes.Append(maAtt);

                XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
                MaHSAtt.Value = sp.MaHS;
                node.Attributes.Append(MaHSAtt);

                XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_SP");
                //Hungtq updated 18/01/2012. Chuyển mã font từ Unicde -> TCVN3. San pham khong chuyen, chi chuyen cho NPL thoi.
                //TenAtt.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(sp.Ten);
                TenAtt.Value = sp.Ten;
                node.Attributes.Append(TenAtt);

                XmlAttribute DVTAtt = docNPL.CreateAttribute("MA_DVT");
                DVTAtt.Value = sp.DVT_ID;
                node.Attributes.Append(DVTAtt);

                nplNode.AppendChild(node);
            }
            return docNPL.InnerXml;
        }
        #endregion Khai báo danh sách nguyên phụ liệu

        #region Huy khai báo danh mục nguyên phụ liệu

        public string WSCancelXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucSanPham, MessgaseFunction.HuyKhaiBao));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\HuyKhaiBaoSanPham.xml");

            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);


            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Root/SXXK/DU_LIEU");
            nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {

                kq = kdt.Send(doc.InnerXml, pass);
                doc.InnerXml.XmlSaveMessage(ID, MessageTitle.KhaiBaoHuySanPham);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";

        }

        #endregion Huy khai báo danh mục nguyên phụ liệu

        #region Lấy phản hồi

        public string WSRequestXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucSanPham, MessgaseFunction.HoiTrangThai));

            XmlDocument docSP = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docSP.Load(path + "\\TemplateXML\\LayPhanHoiSanPham.xml");


            XmlNode nodeHQNhan = docSP.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            XmlNode nodeDN = docSP.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            XmlNode nodeDuLieu = docSP.SelectSingleNode("Root/SXXK/DU_LIEU");
            nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docSP.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";

        }

        #endregion Lấy phản hồi

        public string LayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            //doc.GetElementsByTagName("function")[0].InnerText = "5";
            doc.GetElementsByTagName("function")[0].InnerText = MessgaseFunction.LayPhanHoi.ToString();
            //doc.GetElementsByTagName("type")[0].InnerText = MessgaseType.ThongTin.ToString();

            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);

            //HungTQ, Update 21/04/2010.
            //doc.GetElementsByTagName("declarationType")[0].InnerText = "1";

            string kq = "";
            int i = 0;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                    {
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 1)
                return doc.InnerXml;

            try
            {
                /*XU LY THONG TIN MESSAGE PHAN HOI TU HAI QUAN*/
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
                {
                    XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                    if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.SP)
                    {
                        #region Lấy số tiếp nhận của danh sách SP

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                            this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                            this.NamDK = short.Parse(nodeDuLieu.Attributes["NAM_TN"].Value);
                            this.NgayTiepNhan = DateTime.Today;
                            this.TrangThaiXuLy = 0;
                            this.Update();
                        }
                        #endregion Lấy số tiếp nhận của danh sách SP
                        Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoLayPhanHoiSanPham, string.Format("Số tiếp nhận {0},năm đăng ký {1},ngày tiếp nhận {2}", this.SoTiepNhan, this.NamDK, this.NgayTiepNhan));

                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.SP)
                    {
                        #region Hủy khai báo NPL

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            this.SoTiepNhan = 0;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            this.NgayTiepNhan = new DateTime(1900, 1, 1);
                            this.Update();
                        }

                        #endregion Hủy khai báo SP
                        Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQHuySanPham);

                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == LAY_THONG_TIN.SP)
                    {
                        #region Nhận trạng thái hồ sơ NPL
                        XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                        if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "DA_XU_LY")
                        {

                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                            TransgferDataToSXXK();
                            Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQDaDuyetSanPham, docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);


                        }
                        else if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "TU_CHOI")
                        {
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                            this.Update();
                            Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQKhongDuyetSanPham, docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                        }
                        else if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");

                            if (nodeDuLieu.Attributes["TRANG_THAI"].Value != null)
                            {
                                this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                                this.NgayTiepNhan = DateTime.Today;
                                this.NamDK = Convert.ToInt16(nodeDuLieu.Attributes["NAM_TN"].Value);
                                this.TrangThaiXuLy = 1;
                                this.Update();
                            }
                        }
                        #endregion Nhận trạng thái hồ sơ SP
                    }
                }
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
                {
                    XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                    XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                    string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
                    string errorSt = "";
                    if (stMucLoi == "XML_LEVEL")
                        errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                    else if (stMucLoi == "DATA_LEVEL")
                        errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                    else if (stMucLoi == "SERVICE_LEVEL")
                        errorSt = "Lỗi do Web service trả về ";
                    else if (stMucLoi == "DOTNET_LEVEL")
                        errorSt = "Lỗi do hệ thống của hải quan ";
                    Globals.SaveMessage(kq, this.ID, errorSt, nodeMota.InnerText);
                    throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
                }
                //DATLMQ update nhận thông tin đã duyệt theo chuẩn TNTT 30/11/2010
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("TuChoi") != null &&
                        docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("TuChoi").Value.Equals("yes"))
                    {
                        this.SoTiepNhan = 0;
                        this.NgayTiepNhan = new DateTime(1900, 01, 01);
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        this.HUONGDAN = docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText.ToString();
                        this.Update();
                        Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQTuChoiSanPham, this.HUONGDAN);

                    }
                    else
                    {
                        XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                        this.SoTiepNhan = Convert.ToInt64(nodeRoot.Attributes["SOTN"].Value);
                        this.NgayTiepNhan = DateTime.Today;
                        this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMTN"].Value);
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                        this.Update();
                        Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQDaDuyetSanPham);

                    }
                }
                //DATLMQ bổ sung nhận thông tin Từ chối từ Hải quan ngày 08/08/2011
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("TuChoi") != null)
                {
                    this.SoTiepNhan = 0;
                    this.NgayTiepNhan = new DateTime(1900, 01, 01);
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.HUONGDAN = docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText.ToString();
                    this.Update();
                    Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQKhongDuyetSanPham, this.HUONGDAN);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Không thể xử lý message kết quả", new Exception(kq));

                throw ex;
            }
            return "";
        }
        #endregion của hải quan


        //#region Webservice của FPT

        //#region Khai báo danh sách sản phẩm        
        ////cap nhat
        //public string WSUpdateXMLFPT(string pass, SanPhamCollection spCollection)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_SP_CAPNHAT.xml");

        //    //luu thong tin gui
        //    XmlNode hq = doc.GetElementsByTagName("Root/SXXK/THONG_TIN/HQ_NHAN")[0];
        //    hq.Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    hq.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);

        //    XmlNode data = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DU_LIEU");
        //    data.Attributes["SO_DK"].Value = this.SoTiepNhan.ToString();


        //    XmlNode spNode = doc.GetElementsByTagName("SP")[0];
        //    foreach (SanPham sp in spCollection)
        //    {
        //        XmlNode node = doc.CreateElement("SP.ITEM");
        //        XmlAttribute sttAtt = doc.CreateAttribute("STT");
        //        sttAtt.Value = sp.STTHang.ToString();
        //        node.Attributes.Append(sttAtt);

        //        XmlAttribute maAtt = doc.CreateAttribute("MA_SP");
        //        maAtt.Value = sp.Ma;
        //        node.Attributes.Append(maAtt);

        //        XmlAttribute MaHSAtt = doc.CreateAttribute("MA_HS");
        //        MaHSAtt.Value = sp.MaHS;
        //        node.Attributes.Append(MaHSAtt);

        //        XmlAttribute TenAtt = doc.CreateAttribute("TEN_SP");
        //        TenAtt.Value = sp.Ten;
        //        node.Attributes.Append(TenAtt);                

        //        XmlAttribute DVTAtt = doc.CreateAttribute("MA_DVT");
        //        DVTAtt.Value = sp.DVT_ID;
        //        node.Attributes.Append(DVTAtt);
        //        spNode.AppendChild(node);
        //    }
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    return kq;
        //}

        ////dang ky
        //public bool WSSendXMLFPT(string pass)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_SP_DK.xml");

        //    //luu thong tin gui
        //    XmlNode hq = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hq.Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    hq.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);


        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode spNode = doc.SelectSingleNode("Root/SXXK/DU_LIEU/SP");
        //    if (this.SPCollection == null || this.SPCollection.Count == 0)
        //    {
        //        this.SPCollection = SanPham.SelectCollectionBy_Master_ID(this.ID);
        //    }
        //    foreach (SanPham sp in this.SPCollection)
        //    {
        //        XmlNode node = doc.CreateElement("SP.ITEM");
        //        XmlAttribute sttAtt = doc.CreateAttribute("STT");
        //        sttAtt.Value = sp.STTHang.ToString();
        //        node.Attributes.Append(sttAtt);

        //        XmlAttribute maAtt = doc.CreateAttribute("MA_SP");
        //        maAtt.Value = sp.Ma;
        //        node.Attributes.Append(maAtt);

        //        XmlAttribute MaHSAtt = doc.CreateAttribute("MA_HS");
        //        MaHSAtt.Value = sp.MaHS;
        //        node.Attributes.Append(MaHSAtt);

        //        XmlAttribute TenAtt = doc.CreateAttribute("TEN_SP");
        //        TenAtt.Value = sp.Ten;
        //        node.Attributes.Append(TenAtt);

        //        XmlAttribute DVTAtt = doc.CreateAttribute("MA_DVT");
        //        DVTAtt.Value = sp.DVT_ID;
        //        node.Attributes.Append(DVTAtt);
        //        spNode.AppendChild(node);
        //    }
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        XmlNode nodeData = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //        this.SoTiepNhan = Convert.ToInt64(nodeData.Attributes["SO_TN"].Value);
        //        this.NgayTiepNhan = DateTime.Today;
        //        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
        //        this.Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //    return true;

        //}

        //#endregion Khai báo danh sách sản phẩm

        //#region Hủy khai báo danh sách sản phẩm

        ////huy dang ky
        //public bool WSHuyXMLFPT(string pass)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_SP_HUY.xml");

        //    //luu thong tin gui

        //    XmlNode hq = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hq.Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    hq.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode data = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    data.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
        //    data.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        XmlNode nodeData = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //        this.SoTiepNhan = 0;
        //        this.NgayTiepNhan = new DateTime(1900, 1, 1);
        //        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
        //        this.Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //    return true;
        //}

        //#endregion Hủy khai báo danh sách sản phẩm

        //#region Lấy thông tin trả lời từ hải quan

        ////lây thong tin dang ky
        //public bool WSRequestXMLFPT(string pass)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_LAY_TT.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode data = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    data.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
        //    data.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

        //    XmlNode nodeYeuCau = doc.SelectSingleNode("Root/SXXK");
        //    nodeYeuCau.Attributes["YEU_CAU"].Value = LAY_THONG_TIN.SP;

        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();          
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        XmlNode nodeData = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //        string stTT = nodeData.Attributes["TRANG_THAI"].Value;
        //        if (stTT == "DA_XU_LY")
        //        {
        //            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
        //            TransgferDataToSXXK();                    
        //        }
        //        else if (stTT == "TU_CHOI")
        //        {
        //            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
        //            this.Update();
        //        }

        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //    return true;
        //}


        //#endregion Lấy thông tin trả lời từ hải quan
        #endregion Webservice của FPT

        #region TQDT :
        public string TQDTWSLayPhanHoi(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucSanPham, MessgaseFunction.HoiTrangThai));

            XmlDocument docSP = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docSP.Load(path + "\\TemplateXML\\LayPhanHoiDaDuyet.xml");


            //XmlNode nodeHQNhan = docSP.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            //nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            //nodeHQNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            //XmlNode nodeDN = docSP.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            //nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            //nodeDN.Attributes["TEN_DV"].Value = "";

            //XmlNode nodeDuLieu = docSP.SelectSingleNode("Root/SXXK/DU_LIEU");
            //nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            //nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docSP.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = "";
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";

        }

        public string TQDTLayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            doc.GetElementsByTagName("function")[0].InnerText = "5";

            //HungTQ, Update 21/04/2010.
            doc.GetElementsByTagName("declarationType")[0].InnerText = "1";

            string kq = "";
            int i = 0;
            for (i = 1; i <= 3; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                    {
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
                    }
                }
                catch
                {
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 3)
                return doc.InnerXml;
            try
            {

                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
                {
                    XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                    if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.SP)
                    {
                        #region Lấy số tiếp nhận của danh sách SP

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                            this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                            this.NamDK = short.Parse(nodeDuLieu.Attributes["NAM_TN"].Value);
                            this.NgayTiepNhan = DateTime.Today;
                            this.TrangThaiXuLy = 0;
                            this.Update();
                        }
                        #endregion Lấy số tiếp nhận của danh sách SP
                        kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoLayPhanHoiSanPham, string.Format("Số tiếp nhận {0}, ngày tiếp nhận {1}, năm tiếp nhận {2}", SoTiepNhan, NgayTiepNhan, NamDK));
                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.SP)
                    {
                        #region Hủy khai báo NPL
                        kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHuySanPham);
                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            this.SoTiepNhan = 0;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            this.NgayTiepNhan = new DateTime(1900, 1, 1);
                            this.Update();
                        }

                        #endregion Hủy khai báo SP
                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == LAY_THONG_TIN.SP)
                    {
                        #region Nhận trạng thái hồ sơ NPL
                        XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                        if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "DA_XU_LY")
                        {
                            string phanHoi = TQDTWSLayPhanHoi(pass);

                            if (phanHoi.Length != 0)
                            {
                                XmlDocument docPH = new XmlDocument();
                                docPH.LoadXml(phanHoi);

                                XmlNode nodeTuChoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                                if (nodeTuChoi.Attributes.GetNamedItem("TuChoi") != null
                                    && nodeTuChoi.Attributes["TuChoi"].Value == "yes")
                                {
                                    //nodeTuChoi.InnerText;
                                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                                    this.Update();
                                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoHQKhongDuyetSanPham);
                                }

                            }
                            else
                            {
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                                TransgferDataToSXXK();
                                this.Update();
                                kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoHQDaDuyetSanPham);

                            }
                        }
                        else if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "TU_CHOI")
                        {
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                            this.Update();
                            kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoHQKhongDuyetSanPham);

                        }
                        #endregion Nhận trạng thái hồ sơ SP
                    }
                }
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
                {
                    XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                    XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                    string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
                    string errorSt = "";
                    if (stMucLoi == "XML_LEVEL")
                        errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                    else if (stMucLoi == "DATA_LEVEL")
                        errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                    else if (stMucLoi == "SERVICE_LEVEL")
                        errorSt = "Lỗi do Web service trả về ";
                    else if (stMucLoi == "DOTNET_LEVEL")
                        errorSt = "Lỗi do hệ thống của hải quan ";
                    throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
                }
                /*Kiem tra PHAN LUONG*/
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTN") != null)
                {
                    #region Lấy số tiếp nhận của NPL & Thong tin phan luong

                    XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                    this.SoTiepNhan = Convert.ToInt64(nodeRoot.Attributes["SOTN"].Value);
                    this.NgayTiepNhan = DateTime.Today;
                    this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMTN"].Value);
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;

                    /*Lay thong tin phan luong*/
                    if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                    {
                        XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");

                        this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                        this.Huongdan_PL = nodePhanLuong.Attributes["HUONGDAN"].Value;
                    }

                    this.Update();
                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoHQDaDuyetSanPham);

                    #endregion Lấy số tiếp nhận của danh sách NPL
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Không thể xử lý message kết quả", new Exception(kq));
                throw ex;
            }


            return "";

        }
        #endregion

        public static SanPhamDangKy Load(long iD)
        {
            string spName = "[dbo].p_KDT_SXXK_SanPhamDangKy_Load";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
            SanPhamDangKy entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new SanPhamDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
            }
            reader.Close();
            dbCommand.Connection.Close();

            return entity;
        }

        public static SanPhamDangKy Load(long SoTiepNhan, DateTime NgayTiepNhan, string MaHaiQuan, string MaDoanhNghiep, int TrangThaiXuLy, string connectionString)
        {
            SqlDatabase db = new SqlDatabase(connectionString);
            string spName = "[dbo].p_KDT_SXXK_SanPhamDangKy_LoadBy";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);

            SanPhamDangKy entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new SanPhamDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
            }
            reader.Close();
            dbCommand.Connection.Close();

            return entity;
        }

        public bool Load(long SoTiepNhan, DateTime NgayTiepNhan, string MaHaiQuan, string MaDoanhNghiep, int TrangThaiXuLy)
        {
            string spName = "[dbo].p_KDT_SXXK_SanPhamDangKy_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);

            SanPhamDangKy entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new SanPhamDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public int DeleteDynamicTransaction(string where, SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_SanPhamDangKy_DeleteDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, where);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_SXXK_SanPhamDangKy_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, this._MaDaiLy);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_SXXK_SanPhamDangKy_Update";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, this._MaDaiLy);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public bool InsertUpdateFullDaiLy()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    SanPhamDangKy objTemp = new SanPhamDangKy();
                    objTemp.Load(SoTiepNhan, NgayTiepNhan, MaHaiQuan, MaDoanhNghiep, TrangThaiXuLy);

                    if (objTemp.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.ID = objTemp.ID;
                        this.UpdateTransaction(transaction);
                    }

                    foreach (SanPham spDetail in this._SPCollection)
                    {
                        spDetail.Master_ID = this.ID;
                        NguyenPhuLieu tmp = NguyenPhuLieu.Load(spDetail.Ma);
                        if (tmp == null)
                        {
                            spDetail.Master_ID = this._ID;
                            spDetail.ID = spDetail.Insert(transaction);
                        }
                        else
                        {
                            SanPham sp = SanPham.Load(spDetail.ID);
                            BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                            spSXXK.Ma = sp.Ma;
                            spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                            spSXXK.MaHaiQuan = this.MaHaiQuan;
                            spSXXK.DeleteTransaction(transaction);
                            spDetail.Update(transaction);
                        }
                    }
                    TransgferDataToSXXK(transaction);
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

    }
}
