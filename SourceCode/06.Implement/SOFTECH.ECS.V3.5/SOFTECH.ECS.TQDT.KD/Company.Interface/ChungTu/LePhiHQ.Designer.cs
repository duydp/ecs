﻿namespace Company.Interface
{
    partial class LePhiHQ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgLePhi_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LePhiHQ));
            Janus.Windows.Common.JanusColorScheme janusColorScheme4 = new Janus.Windows.Common.JanusColorScheme();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgLePhi = new Janus.Windows.GridEX.GridEX();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgLePhi)).BeginInit();
            this.SuspendLayout();
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.uiGroupBox1.Controls.Add(this.dgLePhi);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(481, 211);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // dgLePhi
            // 
            this.dgLePhi.ColumnAutoResize = true;
            dgLePhi_DesignTimeLayout.LayoutString = resources.GetString("dgLePhi_DesignTimeLayout.LayoutString");
            this.dgLePhi.DesignTimeLayout = dgLePhi_DesignTimeLayout;
            this.dgLePhi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgLePhi.GroupByBoxVisible = false;
            this.dgLePhi.Location = new System.Drawing.Point(3, 8);
            this.dgLePhi.Name = "dgLePhi";
            this.dgLePhi.Size = new System.Drawing.Size(475, 200);
            this.dgLePhi.TabIndex = 0;
            this.dgLePhi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgLePhi.VisualStyleManager = this.visualStyleManager1;
            // 
            // visualStyleManager1
            // 
            janusColorScheme4.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme4.Name = "office2007";
            janusColorScheme4.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme4.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme4);
            // 
            // LePhiHQ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 211);
            this.Controls.Add(this.uiGroupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LePhiHQ";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Lệ phí hải quan";
            this.Load += new System.EventHandler(this.LePhiHQ_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LePhiHQ_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgLePhi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.GridEX dgLePhi;
        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
    }
}