﻿namespace Company.Interface.Feedback
{
    partial class TraLoiDoanhNghiepForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.Common.JanusColorScheme janusColorScheme2 = new Janus.Windows.Common.JanusColorScheme();
            this.visualStyleManager = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.grMain = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cauHoiUserControl1 = new Company.Interface.Feedback.CauHoiUserControl();
            this.label3 = new System.Windows.Forms.Label();
            this.linhVucCauHoi1 = new Company.Interface.Feedback.LinhVucCauHoi();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.cauhoiDoanhNghiep1 = new Company.Interface.Feedback.CauhoiDoanhNghiep();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNoiDungCauHoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnChapNhan = new Janus.Windows.EditControls.UIButton();
            this.btnThoat = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grMain)).BeginInit();
            this.grMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(882, 623);
            // 
            // visualStyleManager
            // 
            janusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme1.Name = "Office 2003";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme2.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme2.Name = "Office 2007";
            janusColorScheme2.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme2.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager.ColorSchemes.Add(janusColorScheme1);
            this.visualStyleManager.ColorSchemes.Add(janusColorScheme2);
            this.visualStyleManager.DefaultColorScheme = "Office 2003";
            // 
            // grMain
            // 
            this.grMain.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.grMain.Controls.Add(this.uiGroupBox3);
            this.grMain.Controls.Add(this.uiGroupBox2);
            this.grMain.Controls.Add(this.uiGroupBox1);
            this.grMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grMain.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.grMain.Location = new System.Drawing.Point(0, 0);
            this.grMain.Name = "grMain";
            this.grMain.Size = new System.Drawing.Size(882, 623);
            this.grMain.TabIndex = 0;
            this.grMain.VisualStyleManager = this.visualStyleManager;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.cauHoiUserControl1);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.linhVucCauHoi1);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Location = new System.Drawing.Point(433, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(449, 588);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.Text = "Trả lời Doanh nghiệp";
            this.uiGroupBox3.VisualStyleManager = this.visualStyleManager;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 328);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Chọn câu trả lời";
            // 
            // cauHoiUserControl1
            // 
            this.cauHoiUserControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cauHoiUserControl1.Location = new System.Drawing.Point(3, 338);
            this.cauHoiUserControl1.Name = "cauHoiUserControl1";
            this.cauHoiUserControl1.Size = new System.Drawing.Size(443, 247);
            this.cauHoiUserControl1.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Lĩnh vực";
            // 
            // linhVucCauHoi1
            // 
            this.linhVucCauHoi1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.linhVucCauHoi1.Location = new System.Drawing.Point(3, 36);
            this.linhVucCauHoi1.Name = "linhVucCauHoi1";
            this.linhVucCauHoi1.Size = new System.Drawing.Size(443, 285);
            this.linhVucCauHoi1.TabIndex = 2;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cauhoiDoanhNghiep1);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.txtNoiDungCauHoi);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(433, 588);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Danh sách câu hỏi doanh nghiệp";
            this.uiGroupBox2.VisualStyleManager = this.visualStyleManager;
            // 
            // cauhoiDoanhNghiep1
            // 
            this.cauhoiDoanhNghiep1.Dock = System.Windows.Forms.DockStyle.Top;
            this.cauhoiDoanhNghiep1.Location = new System.Drawing.Point(3, 17);
            this.cauhoiDoanhNghiep1.Name = "cauhoiDoanhNghiep1";
            this.cauhoiDoanhNghiep1.Size = new System.Drawing.Size(427, 305);
            this.cauhoiDoanhNghiep1.TabIndex = 3;
            this.cauhoiDoanhNghiep1.CauHoiDNSelectionChanged += new Company.Interface.Feedback.CauhoiDoanhNghiep.CauHoiDNSelectionChangedEventHandler(this.cauhoiDoanhNghiep1_CauHoiDNSelectionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 328);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nội dung câu hỏi";
            // 
            // txtNoiDungCauHoi
            // 
            this.txtNoiDungCauHoi.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtNoiDungCauHoi.Location = new System.Drawing.Point(3, 344);
            this.txtNoiDungCauHoi.Multiline = true;
            this.txtNoiDungCauHoi.Name = "txtNoiDungCauHoi";
            this.txtNoiDungCauHoi.Size = new System.Drawing.Size(427, 241);
            this.txtNoiDungCauHoi.TabIndex = 0;
            this.txtNoiDungCauHoi.VisualStyleManager = this.visualStyleManager;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.btnChapNhan);
            this.uiGroupBox1.Controls.Add(this.btnThoat);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 588);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(882, 35);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.visualStyleManager;
            // 
            // btnChapNhan
            // 
            this.btnChapNhan.Location = new System.Drawing.Point(714, 12);
            this.btnChapNhan.Name = "btnChapNhan";
            this.btnChapNhan.Size = new System.Drawing.Size(75, 23);
            this.btnChapNhan.TabIndex = 0;
            this.btnChapNhan.Text = "Chấp nhận";
            this.btnChapNhan.VisualStyleManager = this.visualStyleManager;
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(795, 12);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(75, 23);
            this.btnThoat.TabIndex = 0;
            this.btnThoat.Text = "Đóng";
            this.btnThoat.VisualStyleManager = this.visualStyleManager;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // TraLoiDoanhNghiepForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 623);
            this.Controls.Add(this.grMain);
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "TraLoiDoanhNghiepForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "TraLoiDoanhNghiepForm";
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.grMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grMain)).EndInit();
            this.grMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.Common.VisualStyleManager visualStyleManager;
        private Janus.Windows.EditControls.UIGroupBox grMain;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIButton btnChapNhan;
        private Janus.Windows.EditControls.UIButton btnThoat;
        private CauHoiUserControl cauHoiUserControl1;
        private LinhVucCauHoi linhVucCauHoi1;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiDungCauHoi;
        private CauhoiDoanhNghiep cauhoiDoanhNghiep1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}