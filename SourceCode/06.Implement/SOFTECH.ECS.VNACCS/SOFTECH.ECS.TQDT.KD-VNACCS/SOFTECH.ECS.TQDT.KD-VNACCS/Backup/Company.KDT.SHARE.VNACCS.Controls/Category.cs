﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.VNACCS.Controls
{
    public class CategoryItem
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public CategoryItem(string code, string name)
        {
            Code = code;
            Name = name;
        }
    }
}
