﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.DuLieuChuan;

namespace Company.Interface.DanhMucChuan
{
    public partial class CuaKhauForm : Company.Interface.BaseForm
    {
        DataSet ds = new DataSet();
        public CuaKhauForm()
        {
            InitializeComponent();
        }

        private void CuaKhauForm_Load(object sender, EventArgs e)
        {
            ds = CuaKhau.SelectAll();
            dgList.DataSource = ds;
            dgList.DataMember = ds.Tables[0].TableName;
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            try
            {
                CuaKhau.Update(ds);
                ShowMessage(setText("Cập nhật thành công.","Save successfully"), false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            if (ShowMessage(setText("Bạn có muốn xóa các cửa khẩu này không?","Do you want to delete ?"),true) != "Yes") e.Cancel = true;
        }
        private bool CheckID(string Id)
        {
            if (ds.Tables[0].Select(" ID= '" + Id.Trim() + "'").Length == 0) return false;
            else return true;
            //foreach (DataRow dr in ds.Tables[0].Rows)
            //{
            //    if (dr["Id"].ToString().Trim().ToUpper() == Id.Trim().ToUpper()) return true;
            //}
            //return false;
        }
        private void dgList_UpdatingCell(object sender, Janus.Windows.GridEX.UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "ID")
            {
                string s = e.Value.ToString();
                if (CheckID(s))
                {
                    ShowMessage(setText("Mã cửa khẩu này đã có.","This code is exist"), false);
                    e.Cancel = true;
                }
                if (s.Trim().Length == 0)
                {
                    ShowMessage(setText("Mã cửa khẩu không được rỗng.", "Border-Gate code must be filled"), false);
                    e.Cancel = true;
                }
            }
        }


    }
}

