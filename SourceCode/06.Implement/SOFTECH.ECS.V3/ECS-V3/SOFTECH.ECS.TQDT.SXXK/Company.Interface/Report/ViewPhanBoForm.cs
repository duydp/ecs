﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.SXXK;
using Company.BLL.KDT.SXXK;
using DevExpress.XtraPrinting;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report
{
    public partial class ViewPhanBoForm : BaseForm
    {
        PhanBoReport Report = new PhanBoReport();
        public ToKhaiMauDich TKMD;
        public DataSet ds = new DataSet();
        public ViewPhanBoForm()
        {
            InitializeComponent();
        }

        private void explorerBar1_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            
        }

        private void PreviewForm_Load(object sender, EventArgs e)
        {

            this.ViewPhanBo();
        }
        private void ViewPhanBo()
        {
            this.ds = CreatBangPhanBo();
            for (int i = 0; i < this.ds.Tables.Count; i++)
            {
                cbPage.Items.Add("Trang " + (i + 1), i);
            }
            cbPage.SelectedIndex = 0;
            this.Report.dt = ds.Tables[0];
            this.Report.BindReport(this.TKMD.SoToKhai,this.TKMD.MaLoaiHinh,this.TKMD.NamDangKy, this.TKMD.MaHaiQuan);
            printControl1.PrintingSystem = this.Report.PrintingSystem;
            this.Report.CreateDocument();
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            } 
        }

       
        public DataSet CreatBangPhanBo()
        {
    
            this.TKMD.LoadHMDCollection();
            int soLuongSP = this.TKMD.HMDCollection.Count;

            // Create new DataSet to Storage :
            int soTable = 0;
            soTable = (soLuongSP - 1 )  / 4 + 1;
            DataSet dsBangPhanBo = new DataSet();
            //Create DataTables :
            DataTable dttemp;
            for (int z = 0; z < soTable; z++)
            {
                dttemp = new DataTable("dtBangPhanBo" + z.ToString());
                DataColumn[] dcCol = new DataColumn[17];
                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                int t = 0;
                for (int k = z * 4; k < (z + 1) * 4; k++)
                {

                    if (k < soLuongSP)
                    {
                        HangMauDich hmd = this.TKMD.HMDCollection[k];
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "DinhMuc" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = hmd.MaPhu;
                        t++;
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "LuongSD" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = hmd.SoLuong + "";
                        t++;
                    }
                    else
                    {
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "DinhMuc" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = " ";
                        t++;
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "LuongSD" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = " ";
                        t++;
                        
                    }
                }
                dcCol[4 + t] = new DataColumn();
                dcCol[4 + t].ColumnName = "TongNhuCau";
                dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                dcCol[4 + t].Caption = "TongNhuCau";
                t++;
                dcCol[4 + t] = new DataColumn();
                dcCol[4 + t].ColumnName = "PhanBo";
                dcCol[4 + t].DataType = Type.GetType("System.String");
                dcCol[4 + t].Caption = "PhanBo";
                dttemp.Columns.AddRange(dcCol);

                int stt = 0;
                DataSet ds = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoToKhai(this.TKMD.MaHaiQuan, this.TKMD.MaLoaiHinh,this.TKMD.SoToKhai, this.TKMD.NamDangKy);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    try
                    {
                        DataRow drData = dttemp.NewRow();
                        drData[0] = ++stt;
                        drData[1] = dr["MaNPL"].ToString();
                        drData[2] = dr["TenNPL"].ToString();
                        drData[3] = DonViTinh.GetName(dr["DVT_ID"].ToString());
                        for(int n = 0; n< 4; n++)
                        {
                            if (dttemp.Columns["DinhMuc" + n].Caption != " ")
                            {
                                Company.BLL.SXXK.DinhMuc dm = new Company.BLL.SXXK.DinhMuc();
                                dm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                dm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                                dm.MaSanPHam = dttemp.Columns["DinhMuc" + n].Caption;
                                dm.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                                dm.Load();
                                drData["DinhMuc"+n] = dm.DinhMucSuDung;
                                drData["LuongSD"+n] = Convert.ToDecimal(dttemp.Columns["LuongSD"+n].Caption) * dm.DinhMucSuDung;
                            }
                        }
                        dttemp.Rows.Add(drData);
                    }
                    catch
                    {

                    }


                }
                dsBangPhanBo.Tables.Add(dttemp);
            }

            if (dsBangPhanBo.Tables.Count > 0)
            {
                for (int t = 0; t < dsBangPhanBo.Tables[0].Rows.Count; t++)
                {
                    decimal tongTungNPL = 0;
                    for (int i = 0; i < dsBangPhanBo.Tables.Count; i++)
                    {
                        try
                        {
                            DataRow dr = dsBangPhanBo.Tables[i].Rows[t];
                            for (int n = 0; n < 4; n++)
                            {
                                if (dr["LuongSD"+n] != DBNull.Value)
                                    tongTungNPL += Convert.ToDecimal(dr["LuongSD"+n]);

                            }
                        }
                        catch { }
                    }
                    try
                    {
                        dsBangPhanBo.Tables[dsBangPhanBo.Tables.Count - 1].Rows[t]["TongNhuCau"] = tongTungNPL;
                        dsBangPhanBo.Tables[dsBangPhanBo.Tables.Count - 1].Rows[t]["PhanBo"] = GetPhanBoToKhaiXuat(dsBangPhanBo.Tables[dsBangPhanBo.Tables.Count - 1].Rows[t]["MaNPL"].ToString(), tongTungNPL);
                    }
                    catch { }
                }
            }
            return dsBangPhanBo;
        }

        private string GetPhanBoToKhaiXuat(string maNPL, decimal tongNhuCau)
        {
            string temp = "";
            DataSet ds = PhanBoToKhaiXuat.SelectViewPhanBoToKhaiXuatOfTKMDAndMaNPL(this.TKMD.MaHaiQuan, this.TKMD.MaLoaiHinh, this.TKMD.SoToKhai, this.TKMD.NamDangKy, maNPL);
            decimal tongPhanBo = 0;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr["SoToKhaiNhap"].ToString() != "0")
                {
                    temp += "TK" + dr["SoToKhaiNhap"].ToString() + "/" + dr["NamDangKyNhap"].ToString() + ": " + dr["LuongPhanBo"].ToString() + "; ";
                    tongPhanBo += Convert.ToDecimal(dr["LuongPhanBo"]);
                }
            }
            decimal muaVN = tongNhuCau - tongPhanBo;
            if (muaVN > 0)
                temp += "Mua VN: " + muaVN.ToString("N"+GlobalSettings.SoThapPhan.LuongNPL);
            return temp;

 
        }

        private void cbPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Report.First = (cbPage.SelectedIndex == 0);
            this.Report.Last = (cbPage.SelectedIndex == (ds.Tables.Count - 1));
            this.Report.dt = this.ds.Tables[cbPage.SelectedIndex];
            this.Report.BindReport(this.TKMD.SoToKhai, this.TKMD.MaLoaiHinh, this.TKMD.NamDangKy, this.TKMD.MaHaiQuan);
            printControl1.PrintingSystem = this.Report.PrintingSystem;
            this.Report.CreateDocument();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            } 
        }

    }
}