using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Linq;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.Report.SXXK
{
    public partial class BCNPLXuatNhapTon_TT128_2013 : DevExpress.XtraReports.UI.XtraReport
    {
        private int STT = 0;
        private DataTable DtGhiChu = new DataTable();
        private DataTable dt = new DataTable();
        private decimal SoToKhaiNhap = 0;
        private decimal SoToKhaiXuat = 0;
        private decimal SoToKhaiXuatTemp = 0;
        private string MaNPLGroup = "";
        private string MaNPLTempGroup = "";
        private string MaNPLKeTiepGroup = "";
        private string MaSPGroup = "";
        private string MaNPL2TempGroup = "";
        private decimal SumTongLuongNhap = 0;
        private decimal SumTongLuongTonDau = 0;
        private decimal SumTongLuongTonCuoi = 0;
        private decimal SumTongLuongSuDung = 0;
        private decimal temp = 0;
        public DateTime DateFrom;
        public DateTime DateTo;
        public long HopDong_ID;
 
        public BCNPLXuatNhapTon_TT128_2013()
        {
            InitializeComponent();
        }

        public void BindReport(string where)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            string order = "";
            int sapxeptkx = 0;
            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                if (sapxeptkx == 2)
                    order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    //Dùng hiển thị đúng thứ tự tờ khai âm kế tiếp. Ngày của tờ khai âm kế tiếp có thế < tờ khai trước đó.
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
                else
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
            }
            else
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";

            }
            string whereCondition = " MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "' AND TuNgay ='" + DateTo.ToString("yyyy-MM-dd 00:00:000") + "' AND  DenNgay='" + DateFrom.ToString("yyyy-MM-dd 00:00:000") + "' AND HopDong_ID = " + HopDong_ID;
            dt = KDT_GC_BCXuatNhapTon.SelectDynamic(whereCondition,order).Tables[0];
            int i = 1;
            string maNPL = "";
            DateTime ngayDangKyNhap = DateTime.Today;
            long soToKhaiNhap = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (maNPL == dr["MaNPL"].ToString() && ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == Convert.ToInt64(dr["SoToKhaiNhap"]))
                {
                    dr["STT"] = i;
                }
                else
                {

                    maNPL = dr["MaNPL"].ToString();
                    ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                    soToKhaiNhap = Convert.ToInt64(dr["SoToKhaiNhap"]);
                    i++;
                    dr["STT"] = i;
                }
            }
            dt.TableName = "BCXuatNhapTon";
            this.DataSource = dt;
            lblTongLuongNPLSuDung.Summary.Running = SummaryRunning.Group;
            lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Sum;
            lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL"),
                new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});
            }
            else
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL"),
                new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});

            }
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblTuNgay.Text = DateTo.ToString("dd-MM-yyyy");
            lblDenNgay.Text = DateFrom.ToString("dd-MM-yyyy");
            lblSoHopDong.Text = HopDong.GetNameHDRent(HopDong_ID).ToString();
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            lblNgayHoanThanhNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhNhap", "{0:dd/MM/yy}");
            lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            lblLuongSPXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSPXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblTenDVT_SP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_SP");
            lblDinhMuc.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMuc", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblSoToKhaiTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiTaiXuat");
            lblNgayTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayTaiXuat", "{0:dd/MM/yy}");
            lblLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongNPLTaiXuat1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongNPLTaiXuat1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");;
            lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep");

            //HQ
            xrLabel76.Text = Properties.Settings.Default.TieudeNgay;
            //DN
            xrLabel77.Text = GlobalSettings.TieudeNgay;

            //Bo sung them cot Index tren table
            DataColumn colIndex = new DataColumn("Index", typeof(int));
            dt.Columns.Add(colIndex);

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                dt.Rows[j]["Index"] = j;
            }
        }

        public void BindReportByMaNPL(string where, string MaNPL)
        {
            try
            {
                GlobalSettings.KhoiTao_GiaTriMacDinh();
                this.PrintingSystem.ShowMarginsWarning = false;
                string order = "";
                int sapxeptkx = 0;
                if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
                {
                    if (sapxeptkx == 2)
                        order = " ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                    else if (sapxeptkx == 0)
                        order = " ngayhoanthanhnhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                    else
                        order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                }
                else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
                {
                    if (sapxeptkx == 2)
                        order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                    else if (sapxeptkx == 0)
                        //order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                        //TODO: Update by Hungtq 08/06/2012. Bỏ sắp xếp theo "ngayhoanthanhxuat" 
                        //dùng hiển thị đúng thứ tự tờ khai âm kế tiếp. Ngày của tờ khai âm kế tiếp có thế < tờ khai trước đó.
                        order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
                    else
                        order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
                }
                else
                {
                    if (sapxeptkx == 2)
                        order = " ngayhoanthanhnhap,ngaydangkynhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                    else if (sapxeptkx == 0)
                        order = " ngayhoanthanhnhap,ngaydangkynhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                    else
                        order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";

                }


                string whereCondition = " MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "' AND TuNgay ='" + DateFrom + "' DenNgay='" + DateTo + "'";
                dt = KDT_GC_BCXuatNhapTon.SelectDynamic(whereCondition, order).Tables[0];

                int i = 1;
                string maNPL = "";
                DateTime ngayDangKyNhap = DateTime.Today;
                long soToKhaiNhap = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (maNPL == dr["MaNPL"].ToString() && ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == Convert.ToInt64(dr["SoToKhaiNhap"]))
                    {
                        dr["STT"] = i;
                    }
                    else
                    {
                        maNPL = dr["MaNPL"].ToString();
                        ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                        soToKhaiNhap = Convert.ToInt64(dr["SoToKhaiNhap"]);
                        i++;
                        dr["STT"] = i;
                    }
                }
                dt.TableName = "BCXuatNhapTon";
                this.DataSource = dt;
                lblTongLuongNPLSuDung.Summary.Running = SummaryRunning.Group;
                lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Sum;
                lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
                {
                    this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});
                }
                else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
                {
                    this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap")});
                }
                else
                {
                    this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});

                }
                lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
                lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
                lblDiaChi.Text = GlobalSettings.DIA_CHI;
                lblTuNgay.Width = 300;
                lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
                lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
                lblNgayHoanThanhNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhNhap", "{0:dd/MM/yy}");
                lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTongLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
                lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
                lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
                lblLuongSPXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSPXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongSP + "}");
                lblTenDVT_SP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_SP");
                lblDinhMuc.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMuc", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
                lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblSoToKhaiTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiTaiXuat");
                lblNgayTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayTaiXuat", "{0:dd/MM/yy}");
                lblLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblLuongNPLTaiXuat1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblLuongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblLuongNPLTaiXuat1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTongThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep");

                //HQ
                xrLabel76.Text = Properties.Settings.Default.TieudeNgay;
                //DN
                xrLabel77.Text = GlobalSettings.TieudeNgay;

                //Bo sung them cot Index tren table
                DataColumn colIndex = new DataColumn("Index", typeof(int));
                dt.Columns.Add(colIndex);

                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    dt.Rows[j]["Index"] = j;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void lblMaSP_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

            lblMaSP.Text = GetCurrentColumnValue("TenSP").ToString();
            if (GetCurrentColumnValue("MaSP").ToString().Trim().Length > 0)
                lblMaSP.Text = lblMaSP.Text + " / " + GetCurrentColumnValue("MaSP").ToString();
        }

        private void lblSoToKhaiTaiXuat_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiTaiXuat")) == 0)
                lblSoToKhaiTaiXuat1.Text = "";
        }

        private void lblLuongNPLTaiXuat_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) == 0)
                lblLuongNPLTaiXuat1.Text = "";
        }

        private void lblToKhaiNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblToKhaiNhap.Text = "Tổng lượng tờ khai " + Convert.ToString(GetCurrentColumnValue("SoToKhaiNhap")) + "/" + GetCurrentColumnValue("MaLoaiHinhNhap").ToString() + "/" + Convert.ToDateTime(GetCurrentColumnValue("NgayDangKyNhap")).Year;
        }

        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblMaNPL.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();
        }

        private void BCNPLXuatNhapTon_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
            this.DtGhiChu = new DataTable();
            DataColumn[] cols = new DataColumn[3];
            cols[0] = new DataColumn("MaNPL", typeof(string));
            cols[1] = new DataColumn("From", typeof(decimal));
            cols[2] = new DataColumn("To", typeof(decimal));
            this.DtGhiChu.Columns.AddRange(cols);
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                this.STT++;
                temp = Convert.ToDecimal(GetCurrentColumnValue("LuongTonDau"));
                if (lblSoToKhaiNhap.Text != "")
                    SoToKhaiNhap = Convert.ToDecimal(lblSoToKhaiNhap.Text);
                MaNPLGroup = GetCurrentColumnValue("MaNPL").ToString();

                if (MaNPLTempGroup == "") MaNPLTempGroup = MaNPLGroup;

                if (MaNPLGroup != MaNPL2TempGroup)
                {
                    MaNPL2TempGroup = MaNPLGroup;
                    SumTongLuongSuDung = 0;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblSTT.Text = this.STT + "";
        }

        private void lblSoToKhaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) > 0) ((XRTableCell)sender).Text = "";
            if (((XRTableCell)sender).Name == "lblLuongSPXuat")
            {
                if (Convert.ToDecimal(GetCurrentColumnValue("LuongSPXuat")) == 0) ((XRTableCell)sender).Text = "";
            }
        }

        private void lblSoToKhaiTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiTaiXuat")) == 0) lblSoToKhaiTaiXuat.Text = "";
        }

        private void lblNgayTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (Convert.ToDateTime(GetCurrentColumnValue("NgayTaiXuat")).Year == 1900) lblNgayTaiXuat.Text = "";
        }

        private void lblLuongNPLTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) == 0) lblLuongNPLTaiXuat.Text = "";
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (GetCurrentColumnValue("MaSP").ToString() == "") e.Cancel = true;

            SoToKhaiXuat = System.Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiTaiXuat"));

            if (GetCurrentColumnValue("MaSP").ToString() != MaSPGroup
                && SoToKhaiXuat != SoToKhaiXuatTemp)
            {
                MaSPGroup = GetCurrentColumnValue("MaSP").ToString();
                SoToKhaiXuatTemp = SoToKhaiXuat;
                SumTongLuongSuDung += System.Convert.ToDecimal(GetCurrentColumnValue("LuongNPLSuDung"));
            }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
              
            }
            
        }

        private void lblThanhKhoanTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if ((temp - Convert.ToDecimal(GetCurrentColumnValue("LuongNPLSuDung"))) != Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi")))
            {
                decimal from = GetFrom(GetCurrentColumnValue("MaNPL").ToString(), Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiNhap")));
                if (from > 0)
                    lblThanhKhoanTiep.Text = "Chuyển từ TK " + from;
                else
                    lblThanhKhoanTiep.Text = "";
            }
            else
                lblThanhKhoanTiep.Text = "";
            temp = Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
        }

        private decimal GetFrom(string maNPL, decimal to)
        {
            foreach (DataRow dr in this.DtGhiChu.Rows)
            {
                if (dr["MaNPL"].ToString() == maNPL && to == Convert.ToDecimal(dr["To"]))
                    return Convert.ToDecimal(dr["From"]); 
            }
            return 0;
        }

        private void lblTongThanhKhoanTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTongThanhKhoanTiep.Text.Contains("Chuyển TK"))
            {
                DataRow dr = this.DtGhiChu.NewRow();
                dr["MaNPL"] = GetCurrentColumnValue("MaNPL").ToString();
                dr["From"] = Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiNhap"));
                dr["To"] = Convert.ToDecimal(lblTongThanhKhoanTiep.Text.Replace("Chuyển TK ", ""));
                this.DtGhiChu.Rows.Add(dr);
            }
        }

        private void lblDinhMuc_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDinhMuc.Text = GetCurrentColumnValue("DinhMuc").ToString() + " / " + GetCurrentColumnValue("TenDVT_SP").ToString();
        }

        private void SetTextLabel(string maNPL, string tenNPL, decimal luongNhap, decimal luongTonDau, decimal luongTonCuoi, decimal luongSuDung)
        {
            lblTongTonCuoi.Text = (luongTonDau-luongSuDung).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.LuongNPL));
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                DataRow[] rowsFilter = dt.Select(string.Format("Index = {0}", System.Convert.ToInt64(GetCurrentColumnValue("Index")) + 1));

                if (rowsFilter.Length > 0)
                    MaNPLKeTiepGroup = rowsFilter[0]["MaNPL"].ToString();

                if (MaNPLTempGroup != "" && MaNPLGroup != "" && MaNPLGroup != MaNPLTempGroup)
                {
                    AssignSumGroup();

                    MaNPLTempGroup = MaNPLGroup;

                    goto Update;
                }
                else if (MaNPLTempGroup != "" && MaNPLGroup != ""
                    && MaNPLGroup == MaNPLTempGroup
                    && GetCurrentColumnValue("SoToKhaiNhap").ToString() != SoToKhaiNhap.ToString())
                {
                    CalculateSumGroup();

                    goto Update;
                }
                else
                {
                    MaNPLTempGroup = MaNPLGroup;

                    AssignSumGroup();
                }

            Update:
                SetTextLabel(
                    GetCurrentColumnValue("MaNPL").ToString(),
                    GetCurrentColumnValue("TenNPL").ToString(),
                    SumTongLuongNhap,
                    SumTongLuongTonDau,
                    SumTongLuongTonCuoi,
                    SumTongLuongSuDung);
                lblTongTonCuoi.Text = (SumTongLuongTonDau - SumTongLuongSuDung).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void AssignSumGroup()
        {
            SumTongLuongNhap = System.Convert.ToDecimal(GetCurrentColumnValue("LuongNhap"));
            SumTongLuongTonDau = System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonDau"));
            SumTongLuongTonCuoi = System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
        }

        private void CalculateSumGroup()
        {
            SumTongLuongNhap += System.Convert.ToDecimal(GetCurrentColumnValue("LuongNhap"));
            SumTongLuongTonDau += System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonDau"));
            SumTongLuongTonCuoi = System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
        }
    }
}
