namespace Company.Interface.SXXK
{
    partial class ImportHMDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportHMDForm));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMienThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTGTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtTLTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtPhuThu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtThueGTGT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueTTDB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtThueXNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtTSGTGT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTSTTDB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtTSXNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtTGKBVND = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTGTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTGKB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtDGTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDGKB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNuocXX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMaHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMaHS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSoThuTuHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamDangKy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaLoaiHinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSheet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.chkSaveSetting = new Janus.Windows.EditControls.UICheckBox();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvTenSheet = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvFilePath = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.chkOverwrite = new Janus.Windows.EditControls.UICheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.chkOverwrite);
            this.grbMain.Controls.Add(this.chkSaveSetting);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(707, 257);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Excel 97 - 2003 files (*.xls)|*.xls";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(447, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 188;
            this.label2.Text = "Cột số TK";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(553, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 190;
            this.label3.Text = "Cột mã LH";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 194;
            this.label5.Text = "Cột năm ĐK";
            // 
            // txtRow
            // 
            this.txtRow.Location = new System.Drawing.Point(417, 17);
            this.txtRow.MaxLength = 2;
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(23, 21);
            this.txtRow.TabIndex = 196;
            this.txtRow.Text = "2";
            this.txtRow.Value = 2;
            this.txtRow.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtRow.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 199;
            this.label6.Text = "Tên Sheet";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(339, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 208;
            this.label11.Text = "Dòng đầu";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtMienThue);
            this.uiGroupBox1.Controls.Add(this.label25);
            this.uiGroupBox1.Controls.Add(this.txtTGTK);
            this.uiGroupBox1.Controls.Add(this.label26);
            this.uiGroupBox1.Controls.Add(this.txtTLTK);
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Controls.Add(this.txtPhuThu);
            this.uiGroupBox1.Controls.Add(this.label28);
            this.uiGroupBox1.Controls.Add(this.label29);
            this.uiGroupBox1.Controls.Add(this.txtThueGTGT);
            this.uiGroupBox1.Controls.Add(this.txtThueTTDB);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Controls.Add(this.txtThueXNK);
            this.uiGroupBox1.Controls.Add(this.label19);
            this.uiGroupBox1.Controls.Add(this.txtTSGTGT);
            this.uiGroupBox1.Controls.Add(this.label20);
            this.uiGroupBox1.Controls.Add(this.txtTSTTDB);
            this.uiGroupBox1.Controls.Add(this.label21);
            this.uiGroupBox1.Controls.Add(this.txtTSXNK);
            this.uiGroupBox1.Controls.Add(this.label22);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Controls.Add(this.txtTGKBVND);
            this.uiGroupBox1.Controls.Add(this.txtTGTT);
            this.uiGroupBox1.Controls.Add(this.label16);
            this.uiGroupBox1.Controls.Add(this.txtTGKB);
            this.uiGroupBox1.Controls.Add(this.label17);
            this.uiGroupBox1.Controls.Add(this.txtDGTT);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Controls.Add(this.txtDGKB);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.txtSoLuong);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.txtNuocXX);
            this.uiGroupBox1.Controls.Add(this.txtDVT);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.txtTenHang);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.txtMaHang);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.txtMaHS);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.txtSoThuTuHang);
            this.uiGroupBox1.Controls.Add(this.txtNamDangKy);
            this.uiGroupBox1.Controls.Add(this.txtMaLoaiHinh);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtSheet);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.txtRow);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(680, 152);
            this.uiGroupBox1.TabIndex = 212;
            this.uiGroupBox1.Text = "Thông số cấu hình";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            this.uiGroupBox1.Click += new System.EventHandler(this.uiGroupBox1_Click);
            // 
            // txtMienThue
            // 
            this.txtMienThue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMienThue.Location = new System.Drawing.Point(525, 124);
            this.txtMienThue.MaxLength = 2;
            this.txtMienThue.Name = "txtMienThue";
            this.txtMienThue.Size = new System.Drawing.Size(23, 21);
            this.txtMienThue.TabIndex = 265;
            this.txtMienThue.Text = "AD";
            this.txtMienThue.VisualStyleManager = this.vsmMain;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(447, 129);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(46, 13);
            this.label25.TabIndex = 264;
            this.label25.Text = "Cột MT";
            // 
            // txtTGTK
            // 
            this.txtTGTK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTGTK.Location = new System.Drawing.Point(417, 125);
            this.txtTGTK.MaxLength = 2;
            this.txtTGTK.Name = "txtTGTK";
            this.txtTGTK.Size = new System.Drawing.Size(23, 21);
            this.txtTGTK.TabIndex = 263;
            this.txtTGTK.Text = "AI";
            this.txtTGTK.VisualStyleManager = this.vsmMain;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(339, 130);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(58, 13);
            this.label26.TabIndex = 262;
            this.label26.Text = "Cột TGTK";
            // 
            // txtTLTK
            // 
            this.txtTLTK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTLTK.Location = new System.Drawing.Point(308, 124);
            this.txtTLTK.MaxLength = 2;
            this.txtTLTK.Name = "txtTLTK";
            this.txtTLTK.Size = new System.Drawing.Size(23, 21);
            this.txtTLTK.TabIndex = 261;
            this.txtTLTK.Text = "AH";
            this.txtTLTK.VisualStyleManager = this.vsmMain;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(233, 129);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(56, 13);
            this.label27.TabIndex = 260;
            this.label27.Text = "Cột TLTK";
            // 
            // txtPhuThu
            // 
            this.txtPhuThu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPhuThu.Location = new System.Drawing.Point(204, 124);
            this.txtPhuThu.MaxLength = 2;
            this.txtPhuThu.Name = "txtPhuThu";
            this.txtPhuThu.Size = new System.Drawing.Size(23, 21);
            this.txtPhuThu.TabIndex = 259;
            this.txtPhuThu.Text = "AC";
            this.txtPhuThu.VisualStyleManager = this.vsmMain;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(127, 129);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(72, 13);
            this.label28.TabIndex = 258;
            this.label28.Text = "Cột phụ thu";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(11, 129);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(88, 13);
            this.label29.TabIndex = 257;
            this.label29.Text = "Cột thuế GTGT";
            // 
            // txtThueGTGT
            // 
            this.txtThueGTGT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueGTGT.Location = new System.Drawing.Point(100, 124);
            this.txtThueGTGT.MaxLength = 2;
            this.txtThueGTGT.Name = "txtThueGTGT";
            this.txtThueGTGT.Size = new System.Drawing.Size(23, 21);
            this.txtThueGTGT.TabIndex = 256;
            this.txtThueGTGT.Text = "AB";
            this.txtThueGTGT.VisualStyleManager = this.vsmMain;
            // 
            // txtThueTTDB
            // 
            this.txtThueTTDB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueTTDB.Location = new System.Drawing.Point(642, 98);
            this.txtThueTTDB.MaxLength = 2;
            this.txtThueTTDB.Name = "txtThueTTDB";
            this.txtThueTTDB.Size = new System.Drawing.Size(23, 21);
            this.txtThueTTDB.TabIndex = 255;
            this.txtThueTTDB.Text = "AA";
            this.txtThueTTDB.VisualStyleManager = this.vsmMain;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(552, 103);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(88, 13);
            this.label18.TabIndex = 254;
            this.label18.Text = "Cột thuế TTĐB";
            // 
            // txtThueXNK
            // 
            this.txtThueXNK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueXNK.Location = new System.Drawing.Point(525, 97);
            this.txtThueXNK.MaxLength = 2;
            this.txtThueXNK.Name = "txtThueXNK";
            this.txtThueXNK.Size = new System.Drawing.Size(23, 21);
            this.txtThueXNK.TabIndex = 253;
            this.txtThueXNK.Text = "Z";
            this.txtThueXNK.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(446, 102);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(79, 13);
            this.label19.TabIndex = 252;
            this.label19.Text = "Cột thuế XNK";
            // 
            // txtTSGTGT
            // 
            this.txtTSGTGT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTSGTGT.Location = new System.Drawing.Point(417, 98);
            this.txtTSGTGT.MaxLength = 2;
            this.txtTSGTGT.Name = "txtTSGTGT";
            this.txtTSGTGT.Size = new System.Drawing.Size(23, 21);
            this.txtTSGTGT.TabIndex = 251;
            this.txtTSGTGT.Text = "Y";
            this.txtTSGTGT.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(337, 103);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 13);
            this.label20.TabIndex = 250;
            this.label20.Text = "Cột TS GTGT";
            // 
            // txtTSTTDB
            // 
            this.txtTSTTDB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTSTTDB.Location = new System.Drawing.Point(308, 97);
            this.txtTSTTDB.MaxLength = 2;
            this.txtTSTTDB.Name = "txtTSTTDB";
            this.txtTSTTDB.Size = new System.Drawing.Size(23, 21);
            this.txtTSTTDB.TabIndex = 249;
            this.txtTSTTDB.Text = "X";
            this.txtTSTTDB.VisualStyleManager = this.vsmMain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(233, 103);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(76, 13);
            this.label21.TabIndex = 248;
            this.label21.Text = "Cột TS TTĐB";
            // 
            // txtTSXNK
            // 
            this.txtTSXNK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTSXNK.Location = new System.Drawing.Point(204, 97);
            this.txtTSXNK.MaxLength = 2;
            this.txtTSXNK.Name = "txtTSXNK";
            this.txtTSXNK.Size = new System.Drawing.Size(23, 21);
            this.txtTSXNK.TabIndex = 247;
            this.txtTSXNK.Text = "W";
            this.txtTSXNK.VisualStyleManager = this.vsmMain;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(127, 102);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 13);
            this.label22.TabIndex = 246;
            this.label22.Text = "Cột TS XNK";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(10, 102);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(80, 13);
            this.label23.TabIndex = 245;
            this.label23.Text = "Cột TGKBVND";
            // 
            // txtTGKBVND
            // 
            this.txtTGKBVND.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTGKBVND.Location = new System.Drawing.Point(100, 97);
            this.txtTGKBVND.MaxLength = 2;
            this.txtTGKBVND.Name = "txtTGKBVND";
            this.txtTGKBVND.Size = new System.Drawing.Size(23, 21);
            this.txtTGKBVND.TabIndex = 244;
            this.txtTGKBVND.Text = "U";
            this.txtTGKBVND.VisualStyleManager = this.vsmMain;
            // 
            // txtTGTT
            // 
            this.txtTGTT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTGTT.Location = new System.Drawing.Point(642, 71);
            this.txtTGTT.MaxLength = 2;
            this.txtTGTT.Name = "txtTGTT";
            this.txtTGTT.Size = new System.Drawing.Size(23, 21);
            this.txtTGTT.TabIndex = 243;
            this.txtTGTT.Text = "T";
            this.txtTGTT.VisualStyleManager = this.vsmMain;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(552, 76);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 13);
            this.label16.TabIndex = 242;
            this.label16.Text = "Cột TGTT";
            // 
            // txtTGKB
            // 
            this.txtTGKB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTGKB.Location = new System.Drawing.Point(525, 70);
            this.txtTGKB.MaxLength = 2;
            this.txtTGKB.Name = "txtTGKB";
            this.txtTGKB.Size = new System.Drawing.Size(23, 21);
            this.txtTGKB.TabIndex = 241;
            this.txtTGKB.Text = "S";
            this.txtTGKB.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(446, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 13);
            this.label17.TabIndex = 240;
            this.label17.Text = "Cột TGKB";
            // 
            // txtDGTT
            // 
            this.txtDGTT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDGTT.Location = new System.Drawing.Point(417, 71);
            this.txtDGTT.MaxLength = 2;
            this.txtDGTT.Name = "txtDGTT";
            this.txtDGTT.Size = new System.Drawing.Size(23, 21);
            this.txtDGTT.TabIndex = 239;
            this.txtDGTT.Text = "Q";
            this.txtDGTT.VisualStyleManager = this.vsmMain;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(337, 76);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 13);
            this.label15.TabIndex = 238;
            this.label15.Text = "Cột ĐGTT";
            // 
            // txtDGKB
            // 
            this.txtDGKB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDGKB.Location = new System.Drawing.Point(308, 70);
            this.txtDGKB.MaxLength = 2;
            this.txtDGKB.Name = "txtDGKB";
            this.txtDGKB.Size = new System.Drawing.Size(23, 21);
            this.txtDGKB.TabIndex = 237;
            this.txtDGKB.Text = "P";
            this.txtDGKB.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(233, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 13);
            this.label14.TabIndex = 236;
            this.label14.Text = "Cột ĐGKB";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuong.Location = new System.Drawing.Point(204, 70);
            this.txtSoLuong.MaxLength = 2;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Size = new System.Drawing.Size(23, 21);
            this.txtSoLuong.TabIndex = 235;
            this.txtSoLuong.Text = "O";
            this.txtSoLuong.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(127, 75);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 13);
            this.label13.TabIndex = 234;
            this.label13.Text = "Cột số lượng";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(11, 75);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 233;
            this.label12.Text = "Cột ĐVT";
            // 
            // txtNuocXX
            // 
            this.txtNuocXX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNuocXX.Location = new System.Drawing.Point(642, 45);
            this.txtNuocXX.MaxLength = 2;
            this.txtNuocXX.Name = "txtNuocXX";
            this.txtNuocXX.Size = new System.Drawing.Size(23, 21);
            this.txtNuocXX.TabIndex = 232;
            this.txtNuocXX.Text = "M";
            this.txtNuocXX.VisualStyleManager = this.vsmMain;
            this.txtNuocXX.TextChanged += new System.EventHandler(this.txtNuocXX_TextChanged);
            // 
            // txtDVT
            // 
            this.txtDVT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVT.Location = new System.Drawing.Point(100, 70);
            this.txtDVT.MaxLength = 2;
            this.txtDVT.Name = "txtDVT";
            this.txtDVT.Size = new System.Drawing.Size(23, 21);
            this.txtDVT.TabIndex = 230;
            this.txtDVT.Text = "N";
            this.txtDVT.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(554, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 229;
            this.label10.Text = "Cột nước XX";
            // 
            // txtTenHang
            // 
            this.txtTenHang.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenHang.Location = new System.Drawing.Point(525, 44);
            this.txtTenHang.MaxLength = 2;
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(23, 21);
            this.txtTenHang.TabIndex = 228;
            this.txtTenHang.Text = "K";
            this.txtTenHang.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(446, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 227;
            this.label9.Text = "Cột tên hàng";
            // 
            // txtMaHang
            // 
            this.txtMaHang.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHang.Location = new System.Drawing.Point(417, 44);
            this.txtMaHang.MaxLength = 2;
            this.txtMaHang.Name = "txtMaHang";
            this.txtMaHang.Size = new System.Drawing.Size(23, 21);
            this.txtMaHang.TabIndex = 226;
            this.txtMaHang.Text = "E";
            this.txtMaHang.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(337, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 225;
            this.label8.Text = "Cột mã hàng";
            // 
            // txtMaHS
            // 
            this.txtMaHS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHS.Location = new System.Drawing.Point(308, 44);
            this.txtMaHS.MaxLength = 2;
            this.txtMaHS.Name = "txtMaHS";
            this.txtMaHS.Size = new System.Drawing.Size(23, 21);
            this.txtMaHS.TabIndex = 224;
            this.txtMaHS.Text = "G";
            this.txtMaHS.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(233, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 223;
            this.label7.Text = "Cột mã HS";
            // 
            // txtSoThuTuHang
            // 
            this.txtSoThuTuHang.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoThuTuHang.Location = new System.Drawing.Point(204, 44);
            this.txtSoThuTuHang.MaxLength = 2;
            this.txtSoThuTuHang.Name = "txtSoThuTuHang";
            this.txtSoThuTuHang.Size = new System.Drawing.Size(23, 21);
            this.txtSoThuTuHang.TabIndex = 222;
            this.txtSoThuTuHang.Text = "F";
            this.txtSoThuTuHang.VisualStyleManager = this.vsmMain;
            // 
            // txtNamDangKy
            // 
            this.txtNamDangKy.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNamDangKy.Location = new System.Drawing.Point(100, 44);
            this.txtNamDangKy.MaxLength = 2;
            this.txtNamDangKy.Name = "txtNamDangKy";
            this.txtNamDangKy.Size = new System.Drawing.Size(23, 21);
            this.txtNamDangKy.TabIndex = 221;
            this.txtNamDangKy.Text = "D";
            this.txtNamDangKy.VisualStyleManager = this.vsmMain;
            // 
            // txtMaLoaiHinh
            // 
            this.txtMaLoaiHinh.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaLoaiHinh.Location = new System.Drawing.Point(642, 18);
            this.txtMaLoaiHinh.MaxLength = 2;
            this.txtMaLoaiHinh.Name = "txtMaLoaiHinh";
            this.txtMaLoaiHinh.Size = new System.Drawing.Size(23, 21);
            this.txtMaLoaiHinh.TabIndex = 220;
            this.txtMaLoaiHinh.Text = "B";
            this.txtMaLoaiHinh.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(127, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 217;
            this.label1.Text = "Cột STTH";
            // 
            // txtSheet
            // 
            this.txtSheet.Location = new System.Drawing.Point(100, 18);
            this.txtSheet.Name = "txtSheet";
            this.txtSheet.Size = new System.Drawing.Size(231, 21);
            this.txtSheet.TabIndex = 216;
            this.txtSheet.VisualStyleManager = this.vsmMain;
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoToKhai.Location = new System.Drawing.Point(524, 17);
            this.txtSoToKhai.MaxLength = 2;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(23, 21);
            this.txtSoToKhai.TabIndex = 211;
            this.txtSoToKhai.Text = "A";
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiButton1);
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 170);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(680, 53);
            this.uiGroupBox2.TabIndex = 213;
            this.uiGroupBox2.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(594, 18);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(71, 23);
            this.uiButton1.TabIndex = 216;
            this.uiButton1.Text = "Đọc file";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtFilePath.Location = new System.Drawing.Point(13, 18);
            this.txtFilePath.Multiline = true;
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(575, 23);
            this.txtFilePath.TabIndex = 188;
            this.txtFilePath.VisualStyleManager = this.vsmMain;
            this.txtFilePath.ButtonClick += new System.EventHandler(this.txtFilePath_ButtonClick);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(622, 230);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 214;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(546, 230);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(70, 23);
            this.btnSave.TabIndex = 215;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // chkSaveSetting
            // 
            this.chkSaveSetting.BackColor = System.Drawing.Color.Transparent;
            this.chkSaveSetting.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSaveSetting.Location = new System.Drawing.Point(18, 229);
            this.chkSaveSetting.Name = "chkSaveSetting";
            this.chkSaveSetting.Size = new System.Drawing.Size(104, 23);
            this.chkSaveSetting.TabIndex = 216;
            this.chkSaveSetting.Text = "Lưu cấu hình";
            this.chkSaveSetting.VisualStyleManager = this.vsmMain;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this.grbMain;
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvTenSheet
            // 
            this.rfvTenSheet.ControlToValidate = this.txtSheet;
            this.rfvTenSheet.ErrorMessage = "\"Tên sheet\" bắt buộc phải nhập.";
            this.rfvTenSheet.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSheet.Icon")));
            // 
            // rfvFilePath
            // 
            this.rfvFilePath.ControlToValidate = this.txtFilePath;
            this.rfvFilePath.ErrorMessage = "\"Cột đơn vị tính\" bắt buộc phải chọn.";
            this.rfvFilePath.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvFilePath.Icon")));
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // chkOverwrite
            // 
            this.chkOverwrite.BackColor = System.Drawing.Color.Transparent;
            this.chkOverwrite.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOverwrite.Location = new System.Drawing.Point(117, 229);
            this.chkOverwrite.Name = "chkOverwrite";
            this.chkOverwrite.Size = new System.Drawing.Size(104, 23);
            this.chkOverwrite.TabIndex = 218;
            this.chkOverwrite.Text = "Ghi đè";
            this.chkOverwrite.VisualStyleManager = this.vsmMain;
            // 
            // ImportHMDForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(707, 257);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ImportHMDForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Import hàng mậu dịch từ Excel";
            this.Load += new System.EventHandler(this.ImportHMDForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRow;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UICheckBox chkSaveSetting;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSheet;
        private Janus.Windows.GridEX.EditControls.EditBox txtSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvFilePath;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UICheckBox chkOverwrite;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamDangKy;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaLoaiHinh;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHang;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHang;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHS;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoThuTuHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVT;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuong;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtNuocXX;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueTTDB;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueXNK;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.GridEX.EditControls.EditBox txtTSGTGT;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.EditBox txtTSTTDB;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.EditBox txtTSXNK;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.EditBox txtTGKBVND;
        private Janus.Windows.GridEX.EditControls.EditBox txtTGTT;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtTGKB;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.GridEX.EditControls.EditBox txtDGTT;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.GridEX.EditControls.EditBox txtDGKB;
        private Janus.Windows.GridEX.EditControls.EditBox txtMienThue;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.GridEX.EditControls.EditBox txtTGTK;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.GridEX.EditControls.EditBox txtTLTK;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhuThu;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueGTGT;
    }
}