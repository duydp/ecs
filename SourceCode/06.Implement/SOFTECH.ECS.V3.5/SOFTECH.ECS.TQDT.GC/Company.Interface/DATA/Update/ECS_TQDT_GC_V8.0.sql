/*
Run this script on:

192.168.72.100.ECS_TQDT_GC_V4_VERSION    -  This database will be modified

to synchronize it with:

192.168.72.100.ECS_TQDT_GC_V4

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 8.1.0 from Red Gate Software Ltd at 03/06/2013 9:19:45 AM

*/
		
SET XACT_ABORT ON
GO
SET ARITHABORT ON
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

DELETE FROM [dbo].[t_HaiQuan_LoaiPhuKien]

-- Add rows to [dbo].[t_HaiQuan_LoaiPhuKien]
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('101', N'Hủy Hợp đồng')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('102', N'Hủy đăng ký Sản phẩm')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('103', N'Hủy đăng ký Nguyên liệu')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('104', N'Hủy đăng ký Thiết bị')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('105', N'Hủy đăng ký Hàng mẫu')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('201', N'Gia hạn Hợp đồng')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('501', N'Sửa thông tin chung Hợp đồng')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('502', N'Sửa Sản phẩm')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('503', N'Sửa Nguyên liệu')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('504', N'Sửa Thiết bị')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('505', N'Sửa Hàng mẫu')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('802', N'Bổ sung Sản phẩm')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('803', N'Bổ sung Nguyên liệu')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('804', N'Bổ sung Thiết bị')
INSERT INTO [dbo].[t_HaiQuan_LoaiPhuKien] ([ID_LoaiPhuKien], [TenLoaiPhuKien]) VALUES ('805', N'Bổ sung Hàng mẫu')
-- Operation applied to 15 rows out of 15
COMMIT TRANSACTION
GO


UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '8.0'
      ,[Date] = getdate()
      ,[Notes] = ''
      GO	


