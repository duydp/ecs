﻿namespace CreateUpdateFile
{
    partial class FrmCreateUpdateFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCreateUpdateFile));
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.Common.JanusColorScheme janusColorScheme2 = new Janus.Windows.Common.JanusColorScheme();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.dsConfig = new System.Data.DataSet();
            this.dtConfig = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lbxFileOtherFolder = new System.Windows.Forms.ListBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.txtGroup = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.txtFileNeedRestart = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtFolderDatabase = new System.Windows.Forms.TextBox();
            this.txtFooter = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.numVersion = new System.Windows.Forms.NumericUpDown();
            this.chkClientZip = new System.Windows.Forms.CheckBox();
            this.lblClientCount = new System.Windows.Forms.Label();
            this.rtxClientListFile = new System.Windows.Forms.RichTextBox();
            this.txtClientFolderPath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPathCompare = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cboGroup = new System.Windows.Forms.ComboBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClientSave = new Janus.Windows.EditControls.UIButton();
            this.btnOpenfolder = new Janus.Windows.EditControls.UIButton();
            this.btnClientClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnCheckLink = new Janus.Windows.EditControls.UIButton();
            this.btnClientOpen = new Janus.Windows.EditControls.UIButton();
            this.btnCancel = new Janus.Windows.EditControls.UIButton();
            this.btnPause = new Janus.Windows.EditControls.UIButton();
            this.btnClientExecute = new Janus.Windows.EditControls.UIButton();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.linkGetContentFromServer = new System.Windows.Forms.LinkLabel();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtHeader = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnConfigSave = new Janus.Windows.EditControls.UIButton();
            this.btnTaoMoi = new Janus.Windows.EditControls.UIButton();
            this.btnConfigExit = new Janus.Windows.EditControls.UIButton();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnTaoDuLieuMacDinh = new Janus.Windows.EditControls.UIButton();
            this.vsmMain = new Janus.Windows.Common.VisualStyleManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dsConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // dsConfig
            // 
            this.dsConfig.DataSetName = "dsConfig";
            this.dsConfig.Tables.AddRange(new System.Data.DataTable[] {
            this.dtConfig});
            // 
            // dtConfig
            // 
            this.dtConfig.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10});
            this.dtConfig.TableName = "dtConfig";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "Url";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Group";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "Path";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "Compare";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "Title";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "Description";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "Header";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "Footer";
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "DateRelease";
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "Version";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
            // 
            // lbxFileOtherFolder
            // 
            this.lbxFileOtherFolder.FormattingEnabled = true;
            this.lbxFileOtherFolder.ItemHeight = 14;
            this.lbxFileOtherFolder.Location = new System.Drawing.Point(187, 62);
            this.lbxFileOtherFolder.Name = "lbxFileOtherFolder";
            this.lbxFileOtherFolder.Size = new System.Drawing.Size(799, 564);
            this.lbxFileOtherFolder.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(187, 31);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(799, 22);
            this.textBox1.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(158, 14);
            this.label10.TabIndex = 3;
            this.label10.Text = "Path server thư mục chung";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(29, 62);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(124, 14);
            this.label11.TabIndex = 3;
            this.label11.Text = "Tệp tin dùng tại host";
            // 
            // txtUrl
            // 
            this.txtUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUrl.Location = new System.Drawing.Point(172, 61);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(952, 22);
            this.txtUrl.TabIndex = 2;
            // 
            // txtGroup
            // 
            this.txtGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGroup.Location = new System.Drawing.Point(172, 26);
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.Size = new System.Drawing.Size(807, 22);
            this.txtGroup.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 14);
            this.label4.TabIndex = 1;
            this.label4.Text = "Đường dẫn mặc định:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(29, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 14);
            this.label15.TabIndex = 3;
            this.label15.Text = "Tên nhóm:";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(985, 30);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 14);
            this.label16.TabIndex = 3;
            this.label16.Text = "Phiên bản:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 14);
            this.label5.TabIndex = 2;
            this.label5.Text = "Tiêu đề hộp thoại:";
            // 
            // txtTitle
            // 
            this.txtTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTitle.Location = new System.Drawing.Point(172, 92);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(952, 22);
            this.txtTitle.TabIndex = 3;
            this.txtTitle.Text = "Cập nhật phiên bản ?";
            // 
            // txtFileNeedRestart
            // 
            this.txtFileNeedRestart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFileNeedRestart.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFileNeedRestart.Location = new System.Drawing.Point(172, 172);
            this.txtFileNeedRestart.Multiline = true;
            this.txtFileNeedRestart.Name = "txtFileNeedRestart";
            this.txtFileNeedRestart.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFileNeedRestart.Size = new System.Drawing.Size(952, 69);
            this.txtFileNeedRestart.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 172);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 14);
            this.label7.TabIndex = 7;
            this.label7.Text = "Tệp tin khởi động:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(29, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(137, 14);
            this.label8.TabIndex = 8;
            this.label8.Text = "Tên thư mục database:";
            // 
            // txtFolderDatabase
            // 
            this.txtFolderDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFolderDatabase.Location = new System.Drawing.Point(172, 130);
            this.txtFolderDatabase.Name = "txtFolderDatabase";
            this.txtFolderDatabase.Size = new System.Drawing.Size(952, 22);
            this.txtFolderDatabase.TabIndex = 4;
            // 
            // txtFooter
            // 
            this.txtFooter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFooter.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFooter.Location = new System.Drawing.Point(172, 480);
            this.txtFooter.Multiline = true;
            this.txtFooter.Name = "txtFooter";
            this.txtFooter.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFooter.Size = new System.Drawing.Size(952, 100);
            this.txtFooter.TabIndex = 8;
            this.txtFooter.TextChanged += new System.EventHandler(this.txtFooter_TextChanged);
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(29, 480);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 14);
            this.label14.TabIndex = 15;
            this.label14.Text = "Tiều để chân :";
            // 
            // numVersion
            // 
            this.numVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numVersion.Location = new System.Drawing.Point(1057, 26);
            this.numVersion.Name = "numVersion";
            this.numVersion.Size = new System.Drawing.Size(67, 22);
            this.numVersion.TabIndex = 1;
            this.numVersion.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chkClientZip
            // 
            this.chkClientZip.AutoSize = true;
            this.chkClientZip.Checked = true;
            this.chkClientZip.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkClientZip.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkClientZip.ForeColor = System.Drawing.Color.Blue;
            this.chkClientZip.Location = new System.Drawing.Point(14, 21);
            this.chkClientZip.Name = "chkClientZip";
            this.chkClientZip.Size = new System.Drawing.Size(188, 18);
            this.chkClientZip.TabIndex = 0;
            this.chkClientZip.Text = "Bao gồm nén tệp tin  \'.zip\'";
            this.chkClientZip.UseVisualStyleBackColor = true;
            // 
            // lblClientCount
            // 
            this.lblClientCount.AutoSize = true;
            this.lblClientCount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClientCount.ForeColor = System.Drawing.Color.Red;
            this.lblClientCount.Location = new System.Drawing.Point(224, 23);
            this.lblClientCount.Name = "lblClientCount";
            this.lblClientCount.Size = new System.Drawing.Size(110, 14);
            this.lblClientCount.TabIndex = 4;
            this.lblClientCount.Text = "Tổng số file(s): 0";
            // 
            // rtxClientListFile
            // 
            this.rtxClientListFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxClientListFile.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxClientListFile.Location = new System.Drawing.Point(3, 8);
            this.rtxClientListFile.Name = "rtxClientListFile";
            this.rtxClientListFile.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtxClientListFile.Size = new System.Drawing.Size(1162, 415);
            this.rtxClientListFile.TabIndex = 0;
            this.rtxClientListFile.Text = "";
            // 
            // txtClientFolderPath
            // 
            this.txtClientFolderPath.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClientFolderPath.Location = new System.Drawing.Point(173, 25);
            this.txtClientFolderPath.Multiline = true;
            this.txtClientFolderPath.Name = "txtClientFolderPath";
            this.txtClientFolderPath.Size = new System.Drawing.Size(919, 25);
            this.txtClientFolderPath.TabIndex = 0;
            this.txtClientFolderPath.TextChanged += new System.EventHandler(this.txtClientFolderPath_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Đường dẫn:";
            // 
            // txtPathCompare
            // 
            this.txtPathCompare.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPathCompare.Location = new System.Drawing.Point(173, 59);
            this.txtPathCompare.Multiline = true;
            this.txtPathCompare.Name = "txtPathCompare";
            this.txtPathCompare.Size = new System.Drawing.Size(919, 25);
            this.txtPathCompare.TabIndex = 2;
            this.txtPathCompare.Text = "http://ecs.softech.cloud/autoupdate/SOFTECH_ECS_TQDT_SXXK_V4/ListUpdate.xml";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 14);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tệp tin cấu hình so sánh:";
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBar1.Location = new System.Drawing.Point(3, 133);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1162, 18);
            this.progressBar1.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 14);
            this.label2.TabIndex = 8;
            this.label2.Text = "Tiến trình:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label9.Location = new System.Drawing.Point(91, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 14);
            this.label9.TabIndex = 9;
            this.label9.Text = "Tệp tin đang nén";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 14);
            this.label12.TabIndex = 12;
            this.label12.Text = "Loại hình:";
            // 
            // cboGroup
            // 
            this.cboGroup.FormattingEnabled = true;
            this.cboGroup.Location = new System.Drawing.Point(76, 14);
            this.cboGroup.Name = "cboGroup";
            this.cboGroup.Size = new System.Drawing.Size(294, 22);
            this.cboGroup.TabIndex = 0;
            this.cboGroup.SelectedIndexChanged += new System.EventHandler(this.cboGroup_SelectedIndexChanged);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.uiTab1);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1176, 719);
            this.uiGroupBox1.TabIndex = 15;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(3, 57);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(1170, 659);
            this.uiTab1.TabIndex = 16;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2,
            this.uiTabPage3});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.uiGroupBox5);
            this.uiTabPage1.Controls.Add(this.uiGroupBox4);
            this.uiTabPage1.Controls.Add(this.uiGroupBox3);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 22);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1168, 636);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Tạo tập tin Update";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.rtxClientListFile);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 154);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1168, 426);
            this.uiGroupBox5.TabIndex = 3;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.btnClientSave);
            this.uiGroupBox4.Controls.Add(this.btnOpenfolder);
            this.uiGroupBox4.Controls.Add(this.lblClientCount);
            this.uiGroupBox4.Controls.Add(this.chkClientZip);
            this.uiGroupBox4.Controls.Add(this.btnClientClose);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 580);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1168, 56);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnClientSave
            // 
            this.btnClientSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClientSave.Image = ((System.Drawing.Image)(resources.GetObject("btnClientSave.Image")));
            this.btnClientSave.Location = new System.Drawing.Point(967, 18);
            this.btnClientSave.Name = "btnClientSave";
            this.btnClientSave.Size = new System.Drawing.Size(118, 25);
            this.btnClientSave.TabIndex = 15;
            this.btnClientSave.Text = "&Lưu";
            this.btnClientSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClientSave.Click += new System.EventHandler(this.btnClientSave_Click);
            // 
            // btnOpenfolder
            // 
            this.btnOpenfolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenfolder.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenfolder.Image")));
            this.btnOpenfolder.Location = new System.Drawing.Point(843, 18);
            this.btnOpenfolder.Name = "btnOpenfolder";
            this.btnOpenfolder.Size = new System.Drawing.Size(118, 25);
            this.btnOpenfolder.TabIndex = 15;
            this.btnOpenfolder.Text = "Mở thư mục zip";
            this.btnOpenfolder.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnOpenfolder.Click += new System.EventHandler(this.btnOpenfolder_Click);
            // 
            // btnClientClose
            // 
            this.btnClientClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClientClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClientClose.Image")));
            this.btnClientClose.Location = new System.Drawing.Point(1091, 18);
            this.btnClientClose.Name = "btnClientClose";
            this.btnClientClose.Size = new System.Drawing.Size(68, 25);
            this.btnClientClose.TabIndex = 15;
            this.btnClientClose.Text = "Đón&g";
            this.btnClientClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClientClose.Click += new System.EventHandler(this.btnClientClose_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnCheckLink);
            this.uiGroupBox3.Controls.Add(this.btnClientOpen);
            this.uiGroupBox3.Controls.Add(this.btnCancel);
            this.uiGroupBox3.Controls.Add(this.btnPause);
            this.uiGroupBox3.Controls.Add(this.btnClientExecute);
            this.uiGroupBox3.Controls.Add(this.progressBar1);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.txtClientFolderPath);
            this.uiGroupBox3.Controls.Add(this.label9);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.txtPathCompare);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1168, 154);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "1. Thư mục chứa tệp tin cần tạo danh sách";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnCheckLink
            // 
            this.btnCheckLink.Image = ((System.Drawing.Image)(resources.GetObject("btnCheckLink.Image")));
            this.btnCheckLink.Location = new System.Drawing.Point(1105, 59);
            this.btnCheckLink.Name = "btnCheckLink";
            this.btnCheckLink.Size = new System.Drawing.Size(42, 25);
            this.btnCheckLink.TabIndex = 15;
            this.btnCheckLink.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCheckLink.Click += new System.EventHandler(this.btnCheckLink_Click);
            // 
            // btnClientOpen
            // 
            this.btnClientOpen.Image = ((System.Drawing.Image)(resources.GetObject("btnClientOpen.Image")));
            this.btnClientOpen.Location = new System.Drawing.Point(1105, 25);
            this.btnClientOpen.Name = "btnClientOpen";
            this.btnClientOpen.Size = new System.Drawing.Size(42, 25);
            this.btnClientOpen.TabIndex = 15;
            this.btnClientOpen.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClientOpen.Click += new System.EventHandler(this.btnClientOpen_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(987, 97);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(104, 25);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnPause
            // 
            this.btnPause.Image = ((System.Drawing.Image)(resources.GetObject("btnPause.Image")));
            this.btnPause.Location = new System.Drawing.Point(877, 97);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(104, 25);
            this.btnPause.TabIndex = 15;
            this.btnPause.Text = "Tạm dừng";
            this.btnPause.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnClientExecute
            // 
            this.btnClientExecute.Image = ((System.Drawing.Image)(resources.GetObject("btnClientExecute.Image")));
            this.btnClientExecute.Location = new System.Drawing.Point(767, 97);
            this.btnClientExecute.Name = "btnClientExecute";
            this.btnClientExecute.Size = new System.Drawing.Size(104, 25);
            this.btnClientExecute.TabIndex = 15;
            this.btnClientExecute.Text = "Đóng gói";
            this.btnClientExecute.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClientExecute.Click += new System.EventHandler(this.btnClientExecute_Click);
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.uiGroupBox8);
            this.uiTabPage2.Controls.Add(this.uiGroupBox6);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 22);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1168, 636);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Cấu hình";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.AutoScroll = true;
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.linkGetContentFromServer);
            this.uiGroupBox8.Controls.Add(this.label15);
            this.uiGroupBox8.Controls.Add(this.numVersion);
            this.uiGroupBox8.Controls.Add(this.txtUrl);
            this.uiGroupBox8.Controls.Add(this.label14);
            this.uiGroupBox8.Controls.Add(this.txtGroup);
            this.uiGroupBox8.Controls.Add(this.label4);
            this.uiGroupBox8.Controls.Add(this.txtFooter);
            this.uiGroupBox8.Controls.Add(this.label16);
            this.uiGroupBox8.Controls.Add(this.label5);
            this.uiGroupBox8.Controls.Add(this.txtTitle);
            this.uiGroupBox8.Controls.Add(this.txtDescription);
            this.uiGroupBox8.Controls.Add(this.txtHeader);
            this.uiGroupBox8.Controls.Add(this.txtFileNeedRestart);
            this.uiGroupBox8.Controls.Add(this.label13);
            this.uiGroupBox8.Controls.Add(this.txtFolderDatabase);
            this.uiGroupBox8.Controls.Add(this.label6);
            this.uiGroupBox8.Controls.Add(this.label7);
            this.uiGroupBox8.Controls.Add(this.label8);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox8.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(1168, 586);
            this.uiGroupBox8.TabIndex = 6;
            this.uiGroupBox8.Text = "Cấu hình ";
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // linkGetContentFromServer
            // 
            this.linkGetContentFromServer.AutoSize = true;
            this.linkGetContentFromServer.Location = new System.Drawing.Point(32, 336);
            this.linkGetContentFromServer.Name = "linkGetContentFromServer";
            this.linkGetContentFromServer.Size = new System.Drawing.Size(134, 14);
            this.linkGetContentFromServer.TabIndex = 16;
            this.linkGetContentFromServer.TabStop = true;
            this.linkGetContentFromServer.Text = "Lấy Nội dung từ Server";
            this.linkGetContentFromServer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkGetContentFromServer_LinkClicked);
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescription.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescription.Location = new System.Drawing.Point(172, 283);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(952, 181);
            this.txtDescription.TabIndex = 5;
            this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
            // 
            // txtHeader
            // 
            this.txtHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHeader.Location = new System.Drawing.Point(172, 255);
            this.txtHeader.Name = "txtHeader";
            this.txtHeader.Size = new System.Drawing.Size(952, 22);
            this.txtHeader.TabIndex = 4;
            this.txtHeader.TextChanged += new System.EventHandler(this.txtHeader_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(29, 300);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 14);
            this.label13.TabIndex = 7;
            this.label13.Text = "Nội dung :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 259);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 14);
            this.label6.TabIndex = 8;
            this.label6.Text = "Tiêu đề đầu :";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnConfigSave);
            this.uiGroupBox6.Controls.Add(this.btnTaoMoi);
            this.uiGroupBox6.Controls.Add(this.btnConfigExit);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 586);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1168, 50);
            this.uiGroupBox6.TabIndex = 5;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnConfigSave
            // 
            this.btnConfigSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfigSave.Image = ((System.Drawing.Image)(resources.GetObject("btnConfigSave.Image")));
            this.btnConfigSave.Location = new System.Drawing.Point(919, 14);
            this.btnConfigSave.Name = "btnConfigSave";
            this.btnConfigSave.Size = new System.Drawing.Size(110, 25);
            this.btnConfigSave.TabIndex = 17;
            this.btnConfigSave.Text = "&Lưu";
            this.btnConfigSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnConfigSave.Click += new System.EventHandler(this.btnConfigSave_Click);
            // 
            // btnTaoMoi
            // 
            this.btnTaoMoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTaoMoi.Image = ((System.Drawing.Image)(resources.GetObject("btnTaoMoi.Image")));
            this.btnTaoMoi.Location = new System.Drawing.Point(803, 14);
            this.btnTaoMoi.Name = "btnTaoMoi";
            this.btnTaoMoi.Size = new System.Drawing.Size(110, 25);
            this.btnTaoMoi.TabIndex = 17;
            this.btnTaoMoi.Text = "Xóa thông tin";
            this.btnTaoMoi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTaoMoi.Click += new System.EventHandler(this.btnTaoMoi_Click);
            // 
            // btnConfigExit
            // 
            this.btnConfigExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfigExit.Image = ((System.Drawing.Image)(resources.GetObject("btnConfigExit.Image")));
            this.btnConfigExit.Location = new System.Drawing.Point(1035, 14);
            this.btnConfigExit.Name = "btnConfigExit";
            this.btnConfigExit.Size = new System.Drawing.Size(89, 25);
            this.btnConfigExit.TabIndex = 16;
            this.btnConfigExit.Text = "Đón&g";
            this.btnConfigExit.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnConfigExit.Click += new System.EventHandler(this.btnClientClose_Click);
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.uiGroupBox11);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 22);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1168, 636);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Map Path";
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.AutoScroll = true;
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.uiButton1);
            this.uiGroupBox11.Controls.Add(this.textBox1);
            this.uiGroupBox11.Controls.Add(this.label11);
            this.uiGroupBox11.Controls.Add(this.lbxFileOtherFolder);
            this.uiGroupBox11.Controls.Add(this.label10);
            this.uiGroupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox11.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(1168, 636);
            this.uiGroupBox11.TabIndex = 3;
            this.uiGroupBox11.Text = "Map Path";
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiButton1
            // 
            this.uiButton1.Image = ((System.Drawing.Image)(resources.GetObject("uiButton1.Image")));
            this.uiButton1.Location = new System.Drawing.Point(998, 31);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(115, 25);
            this.uiButton1.TabIndex = 16;
            this.uiButton1.Text = "Chọn tệp tin";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cboGroup);
            this.uiGroupBox2.Controls.Add(this.btnTaoDuLieuMacDinh);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1170, 49);
            this.uiGroupBox2.TabIndex = 15;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnTaoDuLieuMacDinh
            // 
            this.btnTaoDuLieuMacDinh.Image = ((System.Drawing.Image)(resources.GetObject("btnTaoDuLieuMacDinh.Image")));
            this.btnTaoDuLieuMacDinh.Location = new System.Drawing.Point(376, 13);
            this.btnTaoDuLieuMacDinh.Name = "btnTaoDuLieuMacDinh";
            this.btnTaoDuLieuMacDinh.Size = new System.Drawing.Size(151, 25);
            this.btnTaoDuLieuMacDinh.TabIndex = 15;
            this.btnTaoDuLieuMacDinh.Text = "Tạo dữ liệu mặc định";
            this.btnTaoDuLieuMacDinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTaoDuLieuMacDinh.Click += new System.EventHandler(this.btnTaoDuLieuMacDinh_Click);
            // 
            // vsmMain
            // 
            janusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme1.Name = "Office2003";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme2.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme2.Name = "Office2007";
            janusColorScheme2.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme2.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.vsmMain.ColorSchemes.Add(janusColorScheme1);
            this.vsmMain.ColorSchemes.Add(janusColorScheme2);
            this.vsmMain.DefaultColorScheme = "Office2003";
            // 
            // FrmCreateUpdateFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1176, 719);
            this.Controls.Add(this.uiGroupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmCreateUpdateFile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tạo danh sách tệp tin cập nhật cho phiên bản mới";
            this.Load += new System.EventHandler(this.FrmCreateUpdateFile_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Data.DataSet dsConfig;
        private System.Data.DataTable dtConfig;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Data.DataColumn dataColumn10;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox cboGroup;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPathCompare;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtClientFolderPath;
        private System.Windows.Forms.RichTextBox rtxClientListFile;
        private System.Windows.Forms.Label lblClientCount;
        private System.Windows.Forms.CheckBox chkClientZip;
        private System.Windows.Forms.NumericUpDown numVersion;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtFooter;
        private System.Windows.Forms.TextBox txtFolderDatabase;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFileNeedRestart;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtGroup;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ListBox lbxFileOtherFolder;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        public Janus.Windows.Common.VisualStyleManager vsmMain;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIButton btnCheckLink;
        private Janus.Windows.EditControls.UIButton btnClientOpen;
        private Janus.Windows.EditControls.UIButton btnClientClose;
        private Janus.Windows.EditControls.UIButton btnTaoDuLieuMacDinh;
        private Janus.Windows.EditControls.UIButton btnClientSave;
        private Janus.Windows.EditControls.UIButton btnOpenfolder;
        private Janus.Windows.EditControls.UIButton btnCancel;
        private Janus.Windows.EditControls.UIButton btnPause;
        private Janus.Windows.EditControls.UIButton btnClientExecute;
        private Janus.Windows.EditControls.UIButton btnConfigSave;
        private Janus.Windows.EditControls.UIButton btnTaoMoi;
        private Janus.Windows.EditControls.UIButton btnConfigExit;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtHeader;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.LinkLabel linkGetContentFromServer;
    }
}

