﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class ChonHopDongForm : BaseForm
    {
        public List<HopDong>  HDCol;
        public List<HopDong>  HDColRet = new List<HopDong> ();
        public ChonHopDongForm()
        {
            InitializeComponent();
        }

        private void ChonHopDongForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = HDCol;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            dgList.Tables["HopDong"].FormatConditions[0].Value1 = DateTime.Now.ToShortDateString();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GridEXRow []rows = dgList.GetCheckedRows();
            if (rows.Length == 0)
                if (ShowMessage("Chưa có hợp đồng cần xuất. Bạn có muốn thay đổi", true) == "Yes")
                    return;
                else this.Dispose();
            int soHDHetHan = 0;
            foreach (GridEXRow row in rows)
            {
                HopDong HD = (HopDong)row.DataRow;
                HDColRet.Add(HD);
                if (HD.NgayHetHan < DateTime.Now)
                    soHDHetHan++;
            }
            if (soHDHetHan > 0)
            {
                if (ShowMessage("Có " + soHDHetHan + " hợp đồng hết hạn được chọn.\nBạn có muốn tiếp tục ?", true) == "Yes")
                    this.Close();
            }
            else this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }


    }
}