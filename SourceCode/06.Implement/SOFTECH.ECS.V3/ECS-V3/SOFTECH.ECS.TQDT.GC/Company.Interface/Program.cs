﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.Management;
using System.Configuration;
using System.Text;
using System.Security.Cryptography;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.Utils;

namespace Company.Interface
{
    internal static class Program
    {
        public static bool isActivated = false;
       // public static License lic = null;
        [STAThread]
        private static void Main()
        {

           /* try*/
//             {
//                 lic = new License();
//                 lic.EcsSys = "ECS.TQDT.GC";
//                 loadLicense();
//             }
//             catch (Exception e)
//             {
//                 MessageBox.Show(e.Message + "\nChương trình có lỗi. Vui lòng cài đặt lại.");
//                 return;
//             }
            //isActivated = checkRegistered();

            //isActivated = true;
            //DevExpress.UserSkins.OfficeSkins.Register();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (GlobalSettings.NgayHeThong)
            {
                if (GlobalSettings.DiaPhuong.Trim() == "")
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieudeNgay", "................." + ", ngày " + DateTime.Now.Day.ToString("00") + " tháng " + DateTime.Now.Month.ToString("00") + " năm " + DateTime.Now.Year.ToString("0000"));
                else
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieudeNgay", GlobalSettings.DiaPhuong + ", ngày " + DateTime.Now.Day.ToString("00") + " tháng " + DateTime.Now.Month.ToString("00") + " năm " + DateTime.Now.Year.ToString("0000"));
            }
            GlobalSettings.RefreshKey();

            CultureInfo culture = null;
            if (GlobalSettings.NGON_NGU == "0")
                culture = new CultureInfo("vi-VN");
            else
                culture = new CultureInfo("en-US");

            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
            try
            {
                Application.Run(new MainForm());
            }
            catch { }
        }

//         #region Activate Software
//         private static void loadLicense()
//         {
//             try
//             {
//                 Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//                 int numInConf = int.Parse(lic.DecryptString(config.AppSettings.Settings["DateTr"].Value));
//                 if (!lic.checkExistsLicense())
//                 {
//                     lic.generTrial();
//                     lic.Load();
//                     if (numInConf < int.Parse(lic.dateTrial))
//                     {
//                         lic.dateTrial = numInConf.ToString();
//                         lic.Save();
//                     }
//                 }
//                 else lic.Load();
//                 //if (numInConf > int.Parse(lic.dateTrial))
//                 //{
//                 //    config.AppSettings.Settings.Remove("DateTr");
//                 //    config.AppSettings.Settings.Add("DateTr", lic.EncryptString(lic.dateTrial));
//                 //    config.Save(ConfigurationSaveMode.Full);
//                 //    ConfigurationManager.RefreshSection("appSettings");
//                 //}
//                 isActivated = !string.IsNullOrEmpty(lic.codeActivate);
//             }
//             catch (Exception e)
//             {
//                 throw e;
//             }
//         }
// 
//         public static string getProcessorID()
//         {
//             string CPUID = "BoardID:";
//             ManagementObjectSearcher srch = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
//             foreach (ManagementObject obj in srch.Get())
//             {
//                 CPUID = obj.Properties["SerialNumber"].Value.ToString();
//             }
//             return License.md5String(CPUID);
//         }
// 
//         public static string getCodeToActivate()
//         {
//             return License.md5String(getProcessorID() + "SOFTECH.ECS.GC");
//         }
// 
//         private static bool checkRegistered()
//         {
//             bool value = false;
//             Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//             if (lic.codeActivate == getCodeToActivate())
//             {
//                 CultureInfo infoVN = new CultureInfo("vi-VN");
//                 if (DateTime.Parse(lic.dayExpires, infoVN.DateTimeFormat).Date > DateTime.Now.Date)
//                 {
//                     value = true;
//                 }
//             }
//             return value;
//         }
//         #endregion

    }
}