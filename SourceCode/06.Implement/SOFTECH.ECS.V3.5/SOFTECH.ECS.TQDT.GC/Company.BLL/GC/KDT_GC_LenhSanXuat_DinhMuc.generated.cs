﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
	public partial class KDT_GC_LenhSanXuat_DinhMuc : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long LenhSanXuatSP_ID { set; get; }
		public int STT { set; get; }
		public string MaSanPham { set; get; }
		public string TenSanPham { set; get; }
		public string DVT_SP { set; get; }
		public string MaNPL { set; get; }
		public string TenNPL { set; get; }
		public string DVT_NPL { set; get; }
		public string MaHS { set; get; }
		public decimal DinhMucSuDung { set; get; }
		public decimal TyLeHaoHut { set; get; }
		public decimal NPL_TuCungUng { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_LenhSanXuat_DinhMuc> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_LenhSanXuat_DinhMuc> collection = new List<KDT_GC_LenhSanXuat_DinhMuc>();
			while (reader.Read())
			{
				KDT_GC_LenhSanXuat_DinhMuc entity = new KDT_GC_LenhSanXuat_DinhMuc();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LenhSanXuatSP_ID"))) entity.LenhSanXuatSP_ID = reader.GetInt64(reader.GetOrdinal("LenhSanXuatSP_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt32(reader.GetOrdinal("STT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_SP"))) entity.DVT_SP = reader.GetString(reader.GetOrdinal("DVT_SP"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_NPL"))) entity.DVT_NPL = reader.GetString(reader.GetOrdinal("DVT_NPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("NPL_TuCungUng"))) entity.NPL_TuCungUng = reader.GetDecimal(reader.GetOrdinal("NPL_TuCungUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_LenhSanXuat_DinhMuc> collection, long id)
        {
            foreach (KDT_GC_LenhSanXuat_DinhMuc item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_LenhSanXuat_DinhMuc VALUES(@LenhSanXuatSP_ID, @STT, @MaSanPham, @TenSanPham, @DVT_SP, @MaNPL, @TenNPL, @DVT_NPL, @MaHS, @DinhMucSuDung, @TyLeHaoHut, @NPL_TuCungUng, @GhiChu)";
            string update = "UPDATE t_KDT_GC_LenhSanXuat_DinhMuc SET LenhSanXuatSP_ID = @LenhSanXuatSP_ID, STT = @STT, MaSanPham = @MaSanPham, TenSanPham = @TenSanPham, DVT_SP = @DVT_SP, MaNPL = @MaNPL, TenNPL = @TenNPL, DVT_NPL = @DVT_NPL, MaHS = @MaHS, DinhMucSuDung = @DinhMucSuDung, TyLeHaoHut = @TyLeHaoHut, NPL_TuCungUng = @NPL_TuCungUng, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_LenhSanXuat_DinhMuc WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LenhSanXuatSP_ID", SqlDbType.BigInt, "LenhSanXuatSP_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Int, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSanPham", SqlDbType.VarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSanPham", SqlDbType.NVarChar, "TenSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_SP", SqlDbType.Char, "DVT_SP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_NPL", SqlDbType.Char, "DVT_NPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NPL_TuCungUng", SqlDbType.Decimal, "NPL_TuCungUng", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LenhSanXuatSP_ID", SqlDbType.BigInt, "LenhSanXuatSP_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Int, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSanPham", SqlDbType.VarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSanPham", SqlDbType.NVarChar, "TenSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_SP", SqlDbType.Char, "DVT_SP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_NPL", SqlDbType.Char, "DVT_NPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NPL_TuCungUng", SqlDbType.Decimal, "NPL_TuCungUng", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_LenhSanXuat_DinhMuc VALUES(@LenhSanXuatSP_ID, @STT, @MaSanPham, @TenSanPham, @DVT_SP, @MaNPL, @TenNPL, @DVT_NPL, @MaHS, @DinhMucSuDung, @TyLeHaoHut, @NPL_TuCungUng, @GhiChu)";
            string update = "UPDATE t_KDT_GC_LenhSanXuat_DinhMuc SET LenhSanXuatSP_ID = @LenhSanXuatSP_ID, STT = @STT, MaSanPham = @MaSanPham, TenSanPham = @TenSanPham, DVT_SP = @DVT_SP, MaNPL = @MaNPL, TenNPL = @TenNPL, DVT_NPL = @DVT_NPL, MaHS = @MaHS, DinhMucSuDung = @DinhMucSuDung, TyLeHaoHut = @TyLeHaoHut, NPL_TuCungUng = @NPL_TuCungUng, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_LenhSanXuat_DinhMuc WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LenhSanXuatSP_ID", SqlDbType.BigInt, "LenhSanXuatSP_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Int, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSanPham", SqlDbType.VarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSanPham", SqlDbType.NVarChar, "TenSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_SP", SqlDbType.Char, "DVT_SP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_NPL", SqlDbType.Char, "DVT_NPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NPL_TuCungUng", SqlDbType.Decimal, "NPL_TuCungUng", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LenhSanXuatSP_ID", SqlDbType.BigInt, "LenhSanXuatSP_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Int, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSanPham", SqlDbType.VarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSanPham", SqlDbType.NVarChar, "TenSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_SP", SqlDbType.Char, "DVT_SP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_NPL", SqlDbType.Char, "DVT_NPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NPL_TuCungUng", SqlDbType.Decimal, "NPL_TuCungUng", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_LenhSanXuat_DinhMuc Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_LenhSanXuat_DinhMuc_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_LenhSanXuat_DinhMuc> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_LenhSanXuat_DinhMuc> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_LenhSanXuat_DinhMuc> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_GC_LenhSanXuat_DinhMuc> SelectCollectionBy_LenhSanXuatSP_ID(long lenhSanXuatSP_ID)
		{
            IDataReader reader = SelectReaderBy_LenhSanXuatSP_ID(lenhSanXuatSP_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_LenhSanXuatSP_ID(long lenhSanXuatSP_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_LenhSanXuat_DinhMuc_SelectBy_LenhSanXuatSP_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LenhSanXuatSP_ID", SqlDbType.BigInt, lenhSanXuatSP_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_LenhSanXuat_DinhMuc_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_LenhSanXuat_DinhMuc_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_LenhSanXuat_DinhMuc_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_LenhSanXuat_DinhMuc_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_LenhSanXuatSP_ID(long lenhSanXuatSP_ID)
		{
			const string spName = "p_KDT_GC_LenhSanXuat_DinhMuc_SelectBy_LenhSanXuatSP_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LenhSanXuatSP_ID", SqlDbType.BigInt, lenhSanXuatSP_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_LenhSanXuat_DinhMuc(long lenhSanXuatSP_ID, int sTT, string maSanPham, string tenSanPham, string dVT_SP, string maNPL, string tenNPL, string dVT_NPL, string maHS, decimal dinhMucSuDung, decimal tyLeHaoHut, decimal nPL_TuCungUng, string ghiChu)
		{
			KDT_GC_LenhSanXuat_DinhMuc entity = new KDT_GC_LenhSanXuat_DinhMuc();	
			entity.LenhSanXuatSP_ID = lenhSanXuatSP_ID;
			entity.STT = sTT;
			entity.MaSanPham = maSanPham;
			entity.TenSanPham = tenSanPham;
			entity.DVT_SP = dVT_SP;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.DVT_NPL = dVT_NPL;
			entity.MaHS = maHS;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.NPL_TuCungUng = nPL_TuCungUng;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_LenhSanXuat_DinhMuc_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@LenhSanXuatSP_ID", SqlDbType.BigInt, LenhSanXuatSP_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, STT);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
			db.AddInParameter(dbCommand, "@DVT_SP", SqlDbType.Char, DVT_SP);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@DVT_NPL", SqlDbType.Char, DVT_NPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, NPL_TuCungUng);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_LenhSanXuat_DinhMuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_LenhSanXuat_DinhMuc item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_LenhSanXuat_DinhMuc(long id, long lenhSanXuatSP_ID, int sTT, string maSanPham, string tenSanPham, string dVT_SP, string maNPL, string tenNPL, string dVT_NPL, string maHS, decimal dinhMucSuDung, decimal tyLeHaoHut, decimal nPL_TuCungUng, string ghiChu)
		{
			KDT_GC_LenhSanXuat_DinhMuc entity = new KDT_GC_LenhSanXuat_DinhMuc();			
			entity.ID = id;
			entity.LenhSanXuatSP_ID = lenhSanXuatSP_ID;
			entity.STT = sTT;
			entity.MaSanPham = maSanPham;
			entity.TenSanPham = tenSanPham;
			entity.DVT_SP = dVT_SP;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.DVT_NPL = dVT_NPL;
			entity.MaHS = maHS;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.NPL_TuCungUng = nPL_TuCungUng;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_LenhSanXuat_DinhMuc_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LenhSanXuatSP_ID", SqlDbType.BigInt, LenhSanXuatSP_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, STT);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
			db.AddInParameter(dbCommand, "@DVT_SP", SqlDbType.Char, DVT_SP);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@DVT_NPL", SqlDbType.Char, DVT_NPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, NPL_TuCungUng);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_LenhSanXuat_DinhMuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_LenhSanXuat_DinhMuc item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_LenhSanXuat_DinhMuc(long id, long lenhSanXuatSP_ID, int sTT, string maSanPham, string tenSanPham, string dVT_SP, string maNPL, string tenNPL, string dVT_NPL, string maHS, decimal dinhMucSuDung, decimal tyLeHaoHut, decimal nPL_TuCungUng, string ghiChu)
		{
			KDT_GC_LenhSanXuat_DinhMuc entity = new KDT_GC_LenhSanXuat_DinhMuc();			
			entity.ID = id;
			entity.LenhSanXuatSP_ID = lenhSanXuatSP_ID;
			entity.STT = sTT;
			entity.MaSanPham = maSanPham;
			entity.TenSanPham = tenSanPham;
			entity.DVT_SP = dVT_SP;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.DVT_NPL = dVT_NPL;
			entity.MaHS = maHS;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.NPL_TuCungUng = nPL_TuCungUng;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_LenhSanXuat_DinhMuc_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LenhSanXuatSP_ID", SqlDbType.BigInt, LenhSanXuatSP_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, STT);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
			db.AddInParameter(dbCommand, "@DVT_SP", SqlDbType.Char, DVT_SP);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@DVT_NPL", SqlDbType.Char, DVT_NPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, NPL_TuCungUng);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_LenhSanXuat_DinhMuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_LenhSanXuat_DinhMuc item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_LenhSanXuat_DinhMuc(long id)
		{
			KDT_GC_LenhSanXuat_DinhMuc entity = new KDT_GC_LenhSanXuat_DinhMuc();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_LenhSanXuat_DinhMuc_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_LenhSanXuatSP_ID(long lenhSanXuatSP_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_LenhSanXuat_DinhMuc_DeleteBy_LenhSanXuatSP_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LenhSanXuatSP_ID", SqlDbType.BigInt, lenhSanXuatSP_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_LenhSanXuat_DinhMuc_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_LenhSanXuat_DinhMuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_LenhSanXuat_DinhMuc item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}