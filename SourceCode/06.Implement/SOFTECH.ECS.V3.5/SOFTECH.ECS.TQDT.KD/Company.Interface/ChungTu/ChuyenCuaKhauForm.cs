﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif
namespace Company.Interface
{
    public partial class ChuyenCuaKhauForm : Company.Interface.BaseForm
    {
        public bool isKhaiBoSung = false;

        public ToKhaiMauDich TKMD;
        public DeNghiChuyenCuaKhau deNghiChuyen;
        public List<DeNghiChuyenCuaKhau> ListDeNghiChuyen = new List<DeNghiChuyenCuaKhau>();
        private string msgInfor;
        public ChuyenCuaKhauForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            //cvError.Validate();
            //if (!cvError.IsValid)
            //    return;

            error.Clear();
            if (string.IsNullOrEmpty(txtDiaDiemKiemTra.Text) && string.IsNullOrEmpty(txtDiaDiemKiemTraHQChapNhan.Text))
            {
                error.SetError(txtDiaDiemKiemTra, "Không được để trống Địa điểm kiểm tra");
                return;
            }
            if (string.IsNullOrEmpty(txtTuyenDuong.Text))
            {
                error.SetError(txtTuyenDuong, "Không được để trống Tuyến đường");
                return;
            }

            bool isValidate = ValidateControl.ValidateNull(txtSoVanDon, error, "Số vận đơn");
            isValidate &= ValidateControl.ValidateDate(ccNgayVanDon, new DateTime(1900, 1, 1), DateTime.Now, error, "Ngày vận đơn");
            isValidate &= ValidateControl.ValidateDate(ccThoiHanDen, ccNgayVanDon.Value, new DateTime(9999, 1, 1), error, "Thời gian đến");
            isValidate &= ValidateControl.ValidateNull(cbPTVT, error, "Tên phương thức vận tải");
            if (!isValidate) return;
            deNghiChuyen.DiaDiemKiemTra = txtDiaDiemKiemTra.Text.Trim();
            deNghiChuyen.DiaDiemKiemTraCucHQThanhPho = txtDiaDiemKiemTraHQChapNhan.Text.Trim();
            if (isKhaiBoSung)
                deNghiChuyen.LoaiKB = 1;
            else
                deNghiChuyen.LoaiKB = 0;

            //deNghiChuyen.

            deNghiChuyen.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            deNghiChuyen.NgayVanDon = ccNgayVanDon.Value;
            deNghiChuyen.SoVanDon = txtSoVanDon.Text;
            deNghiChuyen.ThoiGianDen = ccThoiHanDen.Value;
            deNghiChuyen.ThongTinKhac = txtThongTinKhac.Text.Trim();
            deNghiChuyen.TuyenDuong = txtTuyenDuong.Text.Trim();
            deNghiChuyen.TKMD_ID = TKMD.ID;
            deNghiChuyen.PTVT_ID = cbPTVT.SelectedValue.ToString();
            if (string.IsNullOrEmpty(deNghiChuyen.GuidStr))
                deNghiChuyen.GuidStr = Guid.NewGuid().ToString();
            bool isUpdate = false;
            try
            {
                if (deNghiChuyen.ID == 0)
                    deNghiChuyen.Insert();
                else
                {
                    deNghiChuyen.Update();
                    isUpdate = true;
                }
                if (!isUpdate)
                    TKMD.listChuyenCuaKhau.Add(deNghiChuyen);
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (deNghiChuyen.ID > 0)
            {
                deNghiChuyen.Delete();
                TKMD.listChuyenCuaKhau.Remove(deNghiChuyen);
                this.Close();
            }
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (deNghiChuyen.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            #region V3

            try
            {
                if ((deNghiChuyen.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO) || (deNghiChuyen.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET))
                    deNghiChuyen.GuidStr = Guid.NewGuid().ToString();
                else if (deNghiChuyen.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    return;
                }

                ObjectSend msgSend = SingleMessage.BoSungXinChuyenCuaKhau(TKMD, deNghiChuyen);

                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    deNghiChuyen.TrangThai = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    sendMessageForm.Message.XmlSaveMessage(deNghiChuyen.ID, MessageTitle.KhaiBaoBoSungDeNghiChuyenCK);
                    deNghiChuyen.Update();
                    this.SetButtonStateCHUYENCUAKHAU(TKMD, isKhaiBoSung, deNghiChuyen);
                    btnLayPhanHoi_Click(null, null);
                }
                else if (isSend && feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    ShowMessageTQDT(msgInfor, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
                //deNghiChuyen.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;

            }
            SetButtonStateCHUYENCUAKHAU(TKMD, isKhaiBoSung, deNghiChuyen);


            #endregion

        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBack(TKMD, deNghiChuyen.GuidStr);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                isFeedBack = sendMessageForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.SetButtonStateCHUYENCUAKHAU(TKMD, isKhaiBoSung, deNghiChuyen);
                }


            }
        }

        //         private void LayPhanHoiKhaiBao(string password)
        //         {
        //         //StartInvoke:
        //         //    try
        //         //    {
        //         //        bool thanhdeNghiChuyenng = deNghiChuyen.WSLaySoTiepNhan(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
        // 
        //         //        // Thực hiện kiểm tra.  
        //         //        if (thanhdeNghiChuyenng == false)
        //         //        {
        //         //            if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
        //         //            {
        //         //                this.Refresh();
        //         //                goto StartInvoke;
        //         //            }
        // 
        //         //            btnKhaiBao.Enabled = true;
        //         //            btnLayPhanHoi.Enabled = true;
        //         //        }
        //         //        else
        //         //        {
        //         //            string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.deNghiChuyen.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungDeNghiChuyenCKThanhCong);
        //         //            this.ShowMessage(message, false);
        // 
        //         //            btnKhaiBao.Enabled = false;
        //         //            txtSoTiepNhan.Text = this.deNghiChuyen.SoTiepNhan.ToString("N0");
        //         //            ccNgayTiepNhan.Value = this.deNghiChuyen.NgayTiepNhan;
        //         //            ccNgayTiepNhan.Text = this.deNghiChuyen.NgayTiepNhan.ToShortDateString();
        //         //        }
        //         //    }
        //         //    catch (Exception ex)
        //         //    {
        //         //        this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
        //         //    }
        //         }

        //         private void LayPhanHoiDuyet(string password)
        //         {
        //         StartInvoke:
        //             try
        //             {
        //                 bool thanhdeNghiChuyenng = deNghiChuyen.WSLayPhanHoi(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
        // 
        //                 // Thực hiện kiểm tra.  
        //                 if (thanhdeNghiChuyenng == false)
        //                 {
        //                     if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
        //                     {
        //                         this.Refresh();
        //                         goto StartInvoke;
        //                     }
        // 
        //                     btnLayPhanHoi.Enabled = true;
        //                 }
        //                 else
        //                 {
        //                     string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.deNghiChuyen.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungDeNghiChuyenCKDuocChapNhan);
        //                     if (message.Length == 0)
        //                     {
        //                         message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.deNghiChuyen.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
        //                         txtSoTiepNhan.Text = "";
        //                         ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
        //                         ccNgayTiepNhan.Text = "";
        //                     }
        //                     else
        //                         btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
        //                     this.ShowMessage(message, false);
        // 
        //                 }
        //             }
        //             catch (Exception ex)
        //             {
        //                 this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
        //             }
        //         }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form CHUYENCUAKHAU.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateCHUYENCUAKHAU(ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau chuyenCuaKhau)
        {
            if (chuyenCuaKhau == null)
                return false;

            if (chuyenCuaKhau.LoaiKB == 1)
            {

                switch (chuyenCuaKhau.TrangThai)
                {
                    case -1:
                        lbTrangThai.Text = "Chưa Khai Báo";
                        break;
                    case -3:
                        lbTrangThai.Text = "Đã Khai báo nhưng chưa có phản hồi từ Hải quan";
                        break;
                    case 0:
                        lbTrangThai.Text = "Chờ Duyệt";
                        break;
                    case 1:
                        lbTrangThai.Text = "Đã Duyệt";
                        break;
                    case 2:
                        lbTrangThai.Text = "Hải quan không phê duyệt";
                        break;
                    default:
                        lbTrangThai.Text = string.Empty;
                        break;
                }
            }
            bool status = false;


            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else if (tkmd.SoToKhai > 0 && TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {

                bool khaiBaoEnable = (deNghiChuyen.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     deNghiChuyen.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET);

                btnKhaiBao.Enabled = btnXoa.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = deNghiChuyen.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   deNghiChuyen.TrangThai == TrangThaiXuLy.DA_DUYET ||
                                   deNghiChuyen.TrangThai == TrangThaiXuLy.CHO_DUYET || deNghiChuyen.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;
            }
            txtSoTiepNhan.Text = chuyenCuaKhau.SoTiepNhan > 0 ? chuyenCuaKhau.SoTiepNhan.ToString() : string.Empty;
            if (chuyenCuaKhau.NgayTiepNhan.Year > 1900)
                ccNgayTiepNhan.Value = chuyenCuaKhau.NgayTiepNhan;
            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (deNghiChuyen.GuidStr != null && deNghiChuyen.GuidStr != "")
            {
                ThongDiepForm f = new ThongDiepForm();
                f.DeclarationIssuer = AdditionalDocumentType.DE_NGHI_CHUYEN_CUA_KHAU;
                f.ItemID = deNghiChuyen.ID;
                f.ShowDialog(this);
            }
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        private void ChuyenCuaKhauForm_Load(object sender, EventArgs e)
        {
            //DATLMQ bổ sung Số Vận Đơn 17/01/2011
            //Edit by KhanhHN
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll().Tables[0];
            cbPTVT.DisplayMember = "Ten";
            cbPTVT.ValueMember = "ID";
            if (deNghiChuyen != null && deNghiChuyen.ID != 0)
            {
                txtSoVanDon.Text = deNghiChuyen.SoVanDon;
                ccNgayVanDon.Text = deNghiChuyen.NgayVanDon.ToLongDateString();
                ccThoiHanDen.Text = deNghiChuyen.ThoiGianDen.ToLongDateString();
                if (!string.IsNullOrEmpty(deNghiChuyen.PTVT_ID))
                    cbPTVT.SelectedValue = deNghiChuyen.PTVT_ID;
                else
                    cbPTVT.SelectedValue = !string.IsNullOrEmpty(TKMD.PTVT_ID) ? TKMD.PTVT_ID : "005";
                txtDiaDiemKiemTra.Text = deNghiChuyen.DiaDiemKiemTra;
                txtDiaDiemKiemTraHQChapNhan.Text = deNghiChuyen.DiaDiemKiemTraCucHQThanhPho;
                txtTuyenDuong.Text = deNghiChuyen.TuyenDuong;
                txtThongTinKhac.Text = deNghiChuyen.ThongTinKhac;
            }
            else
            {
                deNghiChuyen = new DeNghiChuyenCuaKhau { TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO };
                txtSoVanDon.Text = "";
                ccNgayVanDon.Enabled = true;
                ccThoiHanDen.Enabled = true;
                txtSoVanDon.Text = TKMD.SoVanDon;
                ccNgayVanDon.Text = ccThoiHanDen.Text = DateTime.Now.ToLongDateString();
                cbPTVT.SelectedValue = "005";
            }

            SetButtonStateCHUYENCUAKHAU(TKMD, isKhaiBoSung, deNghiChuyen);

            txtSoVanDon.Focus();
        }


        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public FeedBackContent feedbackContent;
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {
                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(deNghiChuyen.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                deNghiChuyen.TrangThai = TrangThaiXuLy.KHONG_PHE_DUYET;
                                btnKhaiBao.Enabled = true;
                                btnLayPhanHoi.Enabled = true;
                                noidung = "Hải quan từ chối tiếp nhận.\r\nLý do: " + noidung;
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            //noidung = noidung.Replace(string.Format("Message [{0}]", deNghiChuyen.GuidStr), string.Empty);
                            //this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                deNghiChuyen.SoTiepNhan = long.Parse(feedbackContent.CustomsReference);

                                if (feedbackContent.Acceptance.Length == 10)
                                    deNghiChuyen.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd", null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    deNghiChuyen.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                else
                                    deNghiChuyen.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);

                                deNghiChuyen.NamTiepNhan = deNghiChuyen.NgayTiepNhan.Year;

                                e.FeedBackMessage.XmlSaveMessage(deNghiChuyen.ID, MessageTitle.KhaiBaoBoSungDeNghiChuyenCKDuocChapNhan, noidung);
                                deNghiChuyen.TrangThai = TrangThaiXuLy.CHO_DUYET;
                                noidung = "Hải quan cấp số tiếp nhận\r\nSố tiếp nhận: " + deNghiChuyen.SoTiepNhan + "\r\nNgày tiếp nhận:" + deNghiChuyen.NgayTiepNhan.ToLongTimeString();
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(deNghiChuyen.ID, MessageTitle.KhaiBaoBoSungDeNghiChuyenCKThanhCong, noidung);
                                deNghiChuyen.TrangThai = TrangThaiXuLy.DA_DUYET;
                                noidung = "Đề nghị chuyển cửa khẩu đã được duyệt.\n Thông tin trả về từ hải quan: " + noidung;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(deNghiChuyen.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }
                    if (isUpdate)
                    {
                        deNghiChuyen.Update();
                        SetButtonStateCHUYENCUAKHAU(TKMD, isKhaiBoSung, deNghiChuyen);
                    }
                    msgInfor = noidung;
                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        private void txtDiaDiemKiemTraHQChapNhan_TextChanged(object sender, EventArgs e)
        {
            txtDiaDiemKiemTra.Enabled = !(txtDiaDiemKiemTraHQChapNhan.Text.Trim().Length > 0);
        }

        private void txtDiaDiemKiemTra_TextChanged(object sender, EventArgs e)
        {
            txtDiaDiemKiemTraHQChapNhan.Enabled = !(txtDiaDiemKiemTra.Text.Trim().Length > 0);
        }


    }
}

