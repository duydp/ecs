﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{

    public class ErrorDescription
    {
        /// <summary>
        /// Mô tả lỗi
        /// </summary>
        [XmlElement("DESCRIPTION")]
        public string Description { get; set; }

        /// <summary>
        /// Đường dẫn
        /// </summary>
        [XmlElement("PATH")]
        public string Path { get; set; }

        /// <summary>
        /// Node
        /// </summary>
        [XmlElement("NODE")]
        public ErrorNode Node { get; set; }

        /// <summary>
        /// Trình tự
        /// </summary>
        [XmlElement("SEQUENCE")]
        public string SeQuence { get; set; }   
    }
}
