﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KeyServer;
using KeySecurity;
using LicenseCode.Module;
using System.Xml;
using System.IO;
using System.Diagnostics;
using Janus.Windows.GridEX;
using IWshRuntimeLibrary;

namespace LicenseCode
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private Management manage = new Management();
        public static KeyServer.User.User User = new KeyServer.User.User();
        public static List<KeyServer.User.User> listUser = new List<KeyServer.User.User>();
        public static bool isLoginSuccess = false;
        private ProductKey.ProductKey productServices = new ProductKey.ProductKey();
        public static string UserName = string.Empty;
        public static string Pass = string.Empty;

        private void btnGen_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(cbProduct.Text))
            {
                errorProvider1.SetError(cbProduct, "Mã sản phẩm không được để trống");
                return;
            }
            if (string.IsNullOrEmpty(cbPrefix1.Text))
            {
                errorProvider1.SetError(cbPrefix1, "Tiền tố không được để trống");
                return;
            }
            else if (cbPrefix1.Text.Length != 3)
            {
                errorProvider1.SetError(cbPrefix1, "Tiền tố phải có 3 ký tự");
                return;
            }

            if (!manage.IsExitProducts(cbProduct.Text))
            {
                errorProvider1.SetError(cbProduct, "Mã sản phẩm chưa được tạo");
                return;
            }
            if (!manage.IsExitPrefix(cbProduct.SelectedIndex, cbPrefix1.Text))
            {
                errorProvider1.SetError(cbPrefix1, "Tiền tố chưa được tạo");
                return;
            }
            int soNgay = 0;
            int soLuong = 0;
            try
            {
                if (string.IsNullOrEmpty(cmbSoNgay1.Text))
                {
                    errorProvider1.SetError(cmbSoNgay1, "Số ngày không được để trống");
                    return;
                }
                else
                    soNgay = Convert.ToInt32(cmbSoNgay1.Text);
            }
            catch
            {
                errorProvider1.SetError(cmbSoNgay1, "Số ngày không hợp lệ");
                return;

            }
            try
            {
                if (string.IsNullOrEmpty(txtSoLuong.Text))
                {
                    errorProvider1.SetError(txtSoLuong, "Số lượng không được để trống");
                    return;
                }
                else
                    soLuong = Convert.ToInt32(txtSoLuong.Text);
            }
            catch
            {
                errorProvider1.SetError(txtSoLuong, "Số lượng không hợp lệ");
                return;
            }
            try
            {
                string productID = cbProduct.Text.Trim();
                string sfmtKeyNumber = cbPrefix1.Text.Trim();
                string createBy = User.UserName;
                bool status = cmbLoaiKey1.Text.Trim().Equals("Trial");
                string Created = productServices.GenKey(UserName, Pass, productID, cbPrefix1.Text, soNgay, soLuong, status, createBy);
                if (string.IsNullOrEmpty(Created))
                {
                    MessageBoxControl.ShowMessage("Tạo thành công");
                    if (cmbLoaiKey1.Text == "Trial")
                    {
                        if (!manage.IsExitDays(cbProduct.SelectedIndex, true, soNgay))
                            manage.Products[cbProduct.SelectedIndex].Trial.Days.Add(soNgay);
                    }
                    else if (cmbLoaiKey1.Text == "License")
                    {
                        if (!manage.IsExitDays(cbProduct.SelectedIndex, false, soNgay))
                            manage.Products[cbProduct.SelectedIndex].License.Days.Add(soNgay);
                    }
                }
                else
                    MessageBoxControl.ShowMessage(Created);
                //int startID = 0;
                //List<Key> keyItems = Key.SelectCollectionDynamic("ProductId='" + productID + "'", "SerialNumber Desc");
                //if (keyItems.Count > 0)
                //{
                //    string maxKey = keyItems[0].SerialNumber;
                //    startID = int.Parse(maxKey.Substring(7, maxKey.Length - 7));
                //}
                //DateTime dateTrials = new DateTime(1900, 1, 1); //DateTime.Now.Date.AddDays(soNgay);
                //List<Key> keys = new List<Key>();

                //for (int i = 0; i < soLuong; i++)
                //{

                //    Key k = new Key()
                //    {
                //        SerialNumber = string.Format("{0}{1}{2:00000}", sfmtKeyNumber, DateTime.Now.Year, ++startID),
                //        ProductId = productID,
                //        ActivedKey = Guid.NewGuid().ToString().ToUpper(),
                //        Days = soNgay,
                //        Status = status,
                //        ActivedDate = new DateTime(1900, 1, 1),
                //        CreatedDate = DateTime.Now,
                //        ExpiredDate = dateTrials,
                //        LastCheckStatus = new DateTime(1900, 1, 1),
                //        CreatedBy = createBy,
                //    };
                //    keys.Add(k);
                //}
                //Key.InsertCollection(keys);

            }
            catch (Exception ex)
            {
                MessageBoxControl.ShowMessage(ex.Message);
            }
        }
        private void btnCapNhatTB_Click_1(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            try
            {
                if (string.IsNullOrEmpty(cbProduct3.Text))
                {
                    errorProvider1.SetError(cbProduct3, "Mã sản phẩm không được để trống");
                    return;
                }
                if (string.IsNullOrEmpty(txtTBFileNames.Text))
                {
                    errorProvider1.SetError(txtTBFileNames, "Bạn chưa chọn đường dẫn chứa file");
                    return;
                }
                if (string.IsNullOrEmpty(cmbLoaiKey3.Text))
                {
                    errorProvider1.SetError(cmbLoaiKey3, "Bạn chưa chọn loại key cần cập nhật");
                    return;
                }
                string text = KeyCode.ReadFile(txtTBFileNames.Text.Trim());
                //List<Key> keys = Key.SelectCollectionDynamic("SerialNumber like '" + cbPrefix1.Text.Trim() + "%' and ProductId='" + cbProduct.Text.Trim() + "'", "");
                //foreach (Key key in keys)
                //{
                //    key.Notice = text;
                //    key.CustomerField10 = null;
                //    if (key.LastCheckStatus.Year < 1900)
                //        key.LastCheckStatus = new DateTime(1900, 1, 1);
                //    if (key.RequestedDate.Year < 1900)
                //        key.RequestedDate = new DateTime(1900, 1, 1);
                //    key.Update();
                //}
                string update = string.Empty;

                string result = productServices.UpdateNotices(UserName, Pass, text, cbProduct.Text, cmbLoaiKey3.Text.Trim().Equals("Trial"), cmbLoaiKey3.Text.Trim().Equals("All"));
                if (!string.IsNullOrEmpty(result))
                {
                    MessageBoxControl.ShowMessage("Lỗi cập nhật thông báo: " + result);
                    return;
                }

            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Lỗi cập nhật thông báo: " + ex.Message);
                return;
            }
            MessageBoxControl.ShowMessage("Cập nhật thông báo thành công");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            frmLogin flogin = new frmLogin();
            flogin.ShowDialog();
            if (isLoginSuccess)
            {
                KhoiTaoDuLieu();
                LoadUser();
                KhoiTaoGiaoDien();
                CreateShorcut();
                cbProduct.DataSource = cbProduct2.DataSource = cbProduct3.DataSource = txtProductID.DataSource = manage.Products;
                cbProduct.DisplayMember = cbProduct2.DisplayMember = cbProduct3.DisplayMember = txtProductID.DisplayMember = "Id";

                if (manage.Products.Count > 0)
                    cbPrefix1.DataSource = manage.Products[0].Prefix;
                productServices.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                productServices.Timeout = 120000;

                btnSearch_Click(null,null);
            }
            else
                Application.Exit();
        }
        private void CreateShorcut()
        {
            string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                + Path.DirectorySeparatorChar + Application.ProductName.Replace("v4", "V5") + ".lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = Application.ExecutablePath;
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Save();
        } 
        private void KhoiTaoDuLieu()
        {
            #region Products

            try
            {
                manage = new Management() { Products = new List<Products>() };
                string result = productServices.GetInfoProduct();
                result = KeyCode.ConvertFromBase64(result);
                List<Key> keys = KeyCode.Deserialize<List<Key>>(result);
                Products product = new Products()
                {
                    Prefix = new List<string>(),
                    Trial = new TypeKey { Days = new List<int>() },
                    License = new TypeKey { Days = new List<int>() },

                };
                if (keys == null && keys.Count == 0)
                    return;
                product.Id = keys[0].ProductId;
                product.Prefix = new List<string>();
                product.Prefix.Add(keys[0].SerialNumber);
                if (keys[0].Status == "0")
                    product.Trial.Days.Add(keys[0].Days);
                else if (keys[0].Status == "1")
                    product.License.Days.Add(keys[0].Days);

                for (int i = 1; i < keys.Count; i++)
                {
                    if (keys[i].ProductId == keys[i - 1].ProductId)
                    {
                        if (product.Prefix[product.Prefix.Count - 1] != keys[i].SerialNumber)
                            product.Prefix.Add(keys[i].SerialNumber);
                        if (keys[i].Status == "0")
                        {
                            if (product.Trial.Days.Count == 0)
                                product.Trial.Days.Add(keys[i].Days);
                            else if (product.Trial.Days[product.Trial.Days.Count - 1] != keys[i].Days)
                                product.Trial.Days.Add(keys[i].Days);
                        }
                        else if (keys[i].Status == "1")
                        {
                            if (product.License.Days.Count == 0)
                                product.License.Days.Add(keys[i].Days);
                            if (product.License.Days[product.License.Days.Count - 1] != keys[i].Days)
                                product.License.Days.Add(keys[i].Days);
                        }
                    }
                    else
                    {
                        manage.Products.Add(product);
                        product = new Products()
                        {
                            Prefix = new List<string>(),
                            Trial = new TypeKey { Days = new List<int>() },
                            License = new TypeKey { Days = new List<int>() },

                        };
                        product.Id = keys[i].ProductId;
                        product.Prefix = new List<string>();
                        product.Prefix.Add(keys[i].SerialNumber);
                        if (keys[i].Status == "0")
                            product.Trial.Days.Add(keys[i].Days);
                        else if (keys[i].Status == "1")
                            product.License.Days.Add(keys[i].Days);
                    }
                }
                manage.Products.Add(product);
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Lỗi load dữ liệu chuẩn:  " + ex.Message);
            }
            #endregion

            GetUser();
        }


        #region Quản lý User/ Dành cho User quản trị
        private void GetUser()
        {
            try
            {
                if (User.Permission.Admin)
                {
                    listUser = new List<KeyServer.User.User>();
                    string result = productServices.GetInfoUser(UserName, Pass);
                    if (string.IsNullOrEmpty(result)) return;
                    result = KeyCode.ConvertFromBase64(result);
                    listUser = KeyCode.Deserialize<List<KeyServer.User.User>>(result);
                }
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Lỗi Services: " + ex.Message);
            }

        }
        #endregion


        private void LoadUser()
        {
            if (User != null)
            {
                lblUser.Text = User.UserName;
                lblTenUser.Text = User.FirstName + " " + User.LastName;
                lblPhongBan.Text = User.Department;
                lblChucVu.Text = User.position;
                lblEmail.Text = User.Email;
                lblQuyenHan.Text = User.Permission.Admin ? "  + Quản trị\r\n\n" : string.Empty;
                lblQuyenHan.Text += User.Permission.GenKey ? "  + Tạo key\r\n\n" : string.Empty;
                lblQuyenHan.Text += User.Permission.GetTrial ? "  + Lấy key Trial\r\n\n" : string.Empty;
                lblQuyenHan.Text += User.Permission.GetLicense ? "  + Lấy key License\r\n\n" : string.Empty;
                lblQuyenHan.Text += User.Permission.UpdateNotices ? "  + Tạo thông báo" : string.Empty;
            }
        }

        private void KhoiTaoGiaoDien()
        {
            try
            {
                if (User != null)
                {
                    if (!User.Permission.Admin) uiTab1.TabPages.Remove(uiTabPageUserManagement); 
                    if (!User.Permission.GenKey) uiTab1.TabPages.Remove(uiTabPageCreateCDKey); 
                    if (!User.Permission.GetTrial && !User.Permission.GetLicense) uiTab1.TabPages.Remove(uiTabPageGetCDKey) ;
                    if (!User.Permission.UpdateNotices) uiTab1.TabPages.Remove(uiTabPageUpdateNotification);
                }
                else
                {
                    MessageBoxControl.ShowMessage("Không kết nối được server");
                    Application.Exit();
                }
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage(ex.Message);
                Application.Exit();
            }
        }

        private void btnTaoSanPham_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cbProduct.Text))
            {
                MessageBoxControl.ShowMessage("Không được để trống mã sản phẩm");
                return;
            }
            if (manage.IsExitProducts(cbProduct.Text.Trim()))
                MessageBoxControl.ShowMessage("Mã sản phẩm đã tồn tại");
            else
            {
                if (manage.CreateProduct(cbProduct.Text.Trim()))
                {
                    MessageBoxControl.ShowMessage("Thêm mã mới thành công");
                }
                else
                    MessageBoxControl.ShowMessage("Không thêm được mã mới thành công");
            }
            cbProduct.DataSource = cbProduct2.DataSource = null;
            cbProduct.DataSource = cbProduct2.DataSource = manage.Products;
            cbProduct.DisplayMember = cbProduct2.DisplayMember = "Id";
        }

        private void btnTaoTienTo_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cbProduct.Text))
            {
                MessageBoxControl.ShowMessage("Không được để trống mã sản phẩm");
                return;
            }
            else if (string.IsNullOrEmpty(cbProduct.Text))
            {
                MessageBoxControl.ShowMessage("Không được để trống tiền tố");
                return;
            }
            if (manage.IsExitPrefix(cbProduct.SelectedIndex, cbPrefix1.Text.Trim()))
                MessageBoxControl.ShowMessage("Tiền tố đã tồn tại");
            else
            {
                if (manage.CreatePrefix(cbProduct.SelectedIndex, cbPrefix1.Text))
                {
                    MessageBoxControl.ShowMessage("Thêm  mới thành công");
                }
                else
                    MessageBoxControl.ShowMessage("Không thêm được tiền tố  mới ");
            }
            int index = cbProduct.SelectedIndex;
            cbPrefix1.DataSource = null;
            cbPrefix1.DataSource = manage.Products[index].Prefix;
            cbPrefix1.DisplayMember = "Prefix";
        }

        private void cbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {

            int index = cbProduct.SelectedIndex;
            if (index >= 0)
            {
                cbPrefix1.DataSource = null;
                cbPrefix1.DataSource = manage.Products[index].Prefix;
                cmbSoNgay1.DataSource = cmbSoNgay2.DataSource = null;
                if (!string.IsNullOrEmpty(cmbLoaiKey1.Text) || !string.IsNullOrEmpty(cmbloaiKey2.Text))
                {
                    if (cmbLoaiKey1.Text == "Trial" || cmbloaiKey2.Text == "Trial")
                        cmbSoNgay1.DataSource = cmbSoNgay2.DataSource = manage.Products[index].Trial.Days;
                    else if (cmbLoaiKey1.Text == "License" || cmbloaiKey2.Text == "License")
                        cmbSoNgay1.DataSource = cmbSoNgay2.DataSource = manage.Products[index].License.Days;

                }
            }
        }

        private void btnLayKey_Click(object sender, EventArgs e)
        {
            List<Key> key = new List<Key>();
            int soNgay;
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(cbProduct2.Text))
            {
                errorProvider1.SetError(cbProduct, "Mã sản phẩm không được để trống");
                return;
            }
            //if (string.IsNullOrEmpty(txtNguoiLayKey.Text))
            //{
            //    errorProvider1.SetError(txtNguoiLayKey, "Người lấy key không được để trống");
            //    return;
            //}
            try
            {
                if (string.IsNullOrEmpty(cmbSoNgay2.Text))
                {
                    errorProvider1.SetError(cmbSoNgay2, "Số lượng không được để trống");
                    return;
                }
                else
                    soNgay = Convert.ToInt32(cmbSoNgay2.Text);
            }
            catch
            {
                errorProvider1.SetError(cmbSoNgay2, "Số lượng không hợp lệ");
                return;
            }
            int SoLuongKey = 1;
            try
            {
                if (string.IsNullOrEmpty(txtSoLuongKey.Text))
                {
                    errorProvider1.SetError(txtSoLuongKey, "Số lượng key không được để trống");
                    return;
                }
                else
                    SoLuongKey = Convert.ToInt32(txtSoLuongKey.Text);
            }
            catch
            {
                errorProvider1.SetError(txtSoLuongKey, "Số lượng key không hợp lệ");
                return;
            }
            string result = string.Empty;



            try
            {
                if (cmbloaiKey2.Text == "License")
                {
                    if (string.IsNullOrEmpty(txtMaDoanhNghiep.Text))
                    {
                        errorProvider1.SetError(txtMaDoanhNghiep, "Mã doanh nghiệp không được để trống");
                        return;
                    }
                    if (string.IsNullOrEmpty(txtTenDoanhNghiep.Text))
                    {
                        errorProvider1.SetError(txtTenDoanhNghiep, "Tên doanh nghiệp không được để trống");
                        return;
                    }
                    if (User.Permission.GetLicense)
                        result = KeyCode.ConvertFromBase64(productServices.GetKeyLicense(UserName, Pass, cbProduct2.Text.Trim(), UserName, soNgay, SoLuongKey, txtMaDoanhNghiep.Text, txtTenDoanhNghiep.Text, txtCustomerDes.Text));
                    else
                    {
                        result = productServices.ProposalKeyLicense(UserName, Pass, txtMaDoanhNghiep.Text, txtTenDoanhNghiep.Text, txtCustomerDes.Text, cbProduct2.Text, soNgay, SoLuongKey);
                        if (string.IsNullOrEmpty(result))
                        {
                            MessageBoxControl.ShowMessage("Đề nghị nhận key license đã được gửi đi. Vui lòng chờ đợi xác nhận. Vào 'Quản lý key' để nhận key sau khi được xác nhận ");
                            return;
                        }
                    }
                }
                else
                    result = KeyCode.ConvertFromBase64(productServices.GetKeyTrial(UserName, Pass, cbProduct2.Text.Trim(), soNgay, SoLuongKey));
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Lỗi kết nối: " + ex.Message);
                return;
            }

            try
            {
                key = KeyCode.Deserialize<List<Key>>(result);
                if (key.Count < SoLuongKey)
                    MessageBoxControl.ShowMessage("Số lượng key trên server không đáp ứng đủ. Số lượng key lấy được là: " + key.Count);
                frmKeyManager f = new frmKeyManager();
                f.listKey = key;
                f.user = User;
                f.productId = manage.Products;
                f.ShowDialog();
            }
            catch
            {
                MessageBoxControl.ShowMessage("Lỗi khi get key: " + result);
                return;
            }
        }

        private void btnCheckInfo_Click(object sender, EventArgs e)
        {
            try
            {
                Key key = new Key();
                string result = productServices.GetInfoKey(txtActiveCode.Text.Trim());
                try
                {
                    result = KeyCode.ConvertFromBase64(result);
                    key = KeyCode.Deserialize<Key>(result);
                }
                catch (System.Exception ex)
                {
                    MessageBoxControl.ShowMessage(result);
                    return;
                }
                if (key == null)
                    MessageBoxControl.ShowMessage("Key không hợp lệ");
                else
                {
                    lblMaDoanhNghiep.Text = key.CustomerCode;
                    lblTenDoanhNghiep.Text = key.CustomerName;
                    lblProductID.Text = key.ProductId;
                    lblNgayCuoi.Value = key.LastCheckStatus;
                    lblNgayActive.Value = key.ActivedDate;
                    lblNgayHetHan.Value = key.ExpiredDate;
                    lblNguoiCapKey.Text = key.RequestedBy;
                    lblNgayCapKey.Value = key.RequestedDate;
                }
                if (key.ActivedDate.Year > 1900)
                {
                    lblTrangThai.ForeColor = Color.Blue;
                    lblTrangThai.Text = "Key đã được kích hoạt";
                }
                else
                {
                    lblTrangThai.ForeColor = Color.Red;
                    lblTrangThai.Text = "Key chưa được kích hoạt";
                }

            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Không thể tìm kiếm thông tin key");
                return;
            }
        }

        private void cmbloaiKey2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbloaiKey2.Text.Trim().Equals("Trial"))
            {
                txtMaDoanhNghiep.ReadOnly = true;
                txtTenDoanhNghiep.ReadOnly = true;
                txtCustomerDes.ReadOnly = true;
                txtCustomerDes.BackColor = txtMaDoanhNghiep.BackColor = txtTenDoanhNghiep.BackColor = Color.LightGoldenrodYellow;
            }
            else
            {
                txtMaDoanhNghiep.ReadOnly = false;
                txtTenDoanhNghiep.ReadOnly = false;
                txtCustomerDes.ReadOnly = false;
                txtCustomerDes.BackColor = txtMaDoanhNghiep.BackColor = txtTenDoanhNghiep.BackColor = Color.White;
            }
            cmbSoNgay2.DataSource = null;
            int index = cbProduct2.SelectedIndex;
            if (index >= 0)
            {
                if (!string.IsNullOrEmpty(cmbloaiKey2.Text))
                {
                    if (cmbloaiKey2.Text == "Trial")
                        cmbSoNgay2.DataSource = manage.Products[index].Trial.Days;
                    else if (cmbloaiKey2.Text == "License")
                        cmbSoNgay2.DataSource = manage.Products[index].License.Days;
                }
            }
        }

        private void cmbLoaiKey1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = cbProduct.SelectedIndex;
            cmbSoNgay1.DataSource = null;
            if (index >= 0)
            {
                if (!string.IsNullOrEmpty(cmbLoaiKey1.Text))
                {
                    if (cmbLoaiKey1.Text == "Trial")
                        cmbSoNgay1.DataSource = manage.Products[index].Trial.Days;
                    else if (cmbLoaiKey1.Text == "License")
                        cmbSoNgay1.DataSource = manage.Products[index].License.Days;
                }
            }
        }

        private void btnDoiMatKhau_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(txtMatKhauCu.Text))
            {
                errorProvider1.SetError(txtMatKhauCu, "Không được để trống mật khẩu");
                return;
            }
            if (string.IsNullOrEmpty(txtMatKhauMoi.Text))
            {
                errorProvider1.SetError(txtMatKhauCu, "Không được để trống mật khẩu");
                return;
            }
            if (!txtMatKhauMoi.Text.Trim().Equals(txtNhapLaiMatKhau.Text.Trim()))
            {
                errorProvider1.SetError(txtNhapLaiMatKhau, "Mật khẩu nhập lại không chính xác");
                return;
            }
            try
            {
                string _pass = txtMatKhauMoi.Text.Trim();
                _pass = KeyCode.HashMD5(KeyCode.Encrypt(_pass));
                User.PassWord = _pass;
                string _user = KeyCode.ConvertToBase64(KeyCode.Serializer(User));
                string result = productServices.UpdateUser(UserName, Pass, _user);
                if (string.IsNullOrEmpty(result))
                {
                    MessageBoxControl.ShowMessage("Cập nhật thành công");
                    Pass = KeyCode.ConvertToBase64(_pass);
                }
                else
                    MessageBoxControl.ShowMessage("Lỗi: " + result + " .Không thể cập nhật");
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Lỗi: " + ex.Message);
            }


        }

        //private void LoadListView()
        //{
        //    lvUser.Items.Clear();
        //    foreach (KeyServer.User.User userItem in listUser)
        //    {
        //        ListViewItem lvitem = new ListViewItem();
        //        lvitem.SubItems[0].Text = userItem.ID.ToString();
        //        lvitem.SubItems.Add(userItem.FirstName + " " + userItem.LastName);
        //        lvitem.SubItems.Add(userItem.UserName);
        //        lvitem.SubItems.Add(userItem.Department);
        //        lvitem.SubItems.Add(userItem.Email);
        //        lvUser.Items.Add(lvitem);

        //    }
        //}

        //private void lvUser_DoubleClick(object sender, EventArgs e)
        //{
        //    if (lvUser.SelectedItems.Count == 1)
        //    {
        //        frmUser f = new frmUser();
        //        f.UserName = UserName;
        //        f.Pass = Pass;
        //        int index = lvUser.SelectedItems[0].Index;
        //        f.User = listUser[index];
        //        f.isEdit = true;
        //        f.ShowDialog();
        //        if (f.isSuccess)
        //        {
        //            listUser[index] = f.User;
        //            LoadListView();
        //            MessageBoxControl.ShowMessage("Cập nhật thành công");
        //        }
        //    }
        //}

        private void btnNewUser_Click(object sender, EventArgs e)
        {
            frmUser f = new frmUser();
            f.UserName = UserName;
            f.Pass = Pass;
            f.isEdit = false;
            f.ShowDialog();
            if (f.isSuccess)
            {
                GetUser();
                MessageBoxControl.ShowMessage("Cập nhật thành công");
            }
        }

        //private void btnXoa_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (lvUser.SelectedItems.Count == 1)
        //        {
        //            int index = lvUser.SelectedItems[0].Index;
        //            if (listUser[index].UserName == UserName)
        //            {
        //                MessageBoxControl.ShowMessage("Không thể xóa User đang đăng nhập");
        //                return;
        //            }
        //            string _user = KeyCode.ConvertToBase64(KeyCode.Serializer(listUser[index]));
        //            string result = productServices.DeleteUser(UserName, Pass, _user);
        //            if (string.IsNullOrEmpty(result))
        //            {
        //                listUser.RemoveAt(index);
        //                LoadListView();
        //                MessageBoxControl.ShowMessage("Xóa thành công");
        //            }
        //            else
        //                MessageBoxControl.ShowMessage("lỗi :" + result);

        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        MessageBoxControl.ShowMessage("Lỗi : " + ex.Message);
        //    }

        //}

        private void btnQuanLyKey_Click(object sender, EventArgs e)
        {
            frmKeyManager f = new frmKeyManager();
            f.user = User;
            f.productId = manage.Products;
            f.ShowDialog();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "RTF File (*.rtf)| *.rtf";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtTBFileNames.Text = openFileDialog1.FileName;
            }

        }

        private void btnHienThiTB_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtTBFileNames.Text))
                {
                    errorProvider1.SetError(txtTBFileNames, "Đường dẫn file không được để trống");
                    return;
                }
                InfoOnline f = new InfoOnline();
                f.SetRTF(KeyCode.ReadFile(txtTBFileNames.Text.Trim()));
                f.ShowDialog();
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage(ex.Message);
            }

        }

        #region Active offline
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Setconfig(txtProductID.Text, txtFileName.Text, txtActiveUrl.Text);
                KeySecurity.License lic = KeySecurity.Active.Install.ActiveKey(txtSerial.Text.Trim(), txtMachineCode.Text.Trim(), txtProductID.Text.Trim());
                if (!string.IsNullOrEmpty(lic.Message))
                {
                    MessageBoxControl.ShowMessage(lic.Message);
                }
                else
                {
                    SaveLicense(txtMachineCode.Text, txtFileName.Text);
                    MessageBoxControl.ShowMessage("Kích hoạt thành công. Ngày hết hạn Key: " + lic.KeyInfo.ExpiredDate.ToShortDateString());
                    btnOpenFolder_Click(null, null);
                }
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Lỗi kích hoạt offline:  " + ex.Message);
            }
          
            
        }

        private void Setconfig(string ProductID, string fileName, string ActiveUrl)
        {
            string configName = "Active.cfg";
            
            if (System.IO.File.Exists(configName))
            {
                Config configActive;
                string content = KeyCode.ReadFile(configName);
                configActive = KeyCode.Deserialize<Config>(content);
                configActive.ActiveUrl = ActiveUrl;
                configActive.ProductID = ProductID;
                configActive.LicenseFile = fileName;
                content = KeyCode.Serializer(configActive);
                KeyCode.WriteToFile(content, configName);
            }
            else
            {
                Config configActive = new Config()
                {
                    ActiveUrl = @"http://tfs.softech.vn/{0}.asmx",
                    ProductKeyUrl = string.Empty,
                    ProductID = ProductID,
                    LicenseFile = fileName
                };
                string content = KeyCode.Serializer(configActive);
                KeyCode.WriteToFile(content, configName);
            }
        }

        private void SaveLicense(string MachineCode, string fileName)
        {
            string tagerPath = Application.StartupPath + "\\" + MachineCode;
            string sourcePath = Application.StartupPath;

            string destLicenseFile = System.IO.Path.Combine(tagerPath, fileName);
            string sourceLicenseFile = System.IO.Path.Combine(sourcePath, fileName);

            string configName = "Active.cfg";
            string destConfigFile = System.IO.Path.Combine(tagerPath, configName);
            string sourceConfigFile = System.IO.Path.Combine(sourcePath, configName);

            if (!System.IO.Directory.Exists(tagerPath))
            {
                System.IO.Directory.CreateDirectory(tagerPath);
            }
            System.IO.File.Copy(sourceLicenseFile, destLicenseFile, true);
            System.IO.File.Copy(sourceConfigFile, destConfigFile, true);
        }

       
        private void btnOpenFolder_Click(object sender, EventArgs e)
        {
            string tagerPath = Application.StartupPath + "\\" + txtMachineCode.Text;
            if (!System.IO.Directory.Exists(tagerPath))
            {
                System.IO.Directory.CreateDirectory(tagerPath);
            }
            Process.Start(tagerPath);
        }
        
 #endregion

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                gridList.Refetch();
                gridList.DataSource = listUser;
                gridList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnCreateAccount_Click(object sender, EventArgs e)
        {
            try
            {
                frmUser f = new frmUser();
                f.UserName = UserName;
                f.Pass = Pass;
                f.isEdit = false;
                f.ShowDialog();
                if (f.isSuccess)
                {
                    GetUser();
                    MessageBoxControl.ShowMessage("Cập nhật thành công");
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDeleteAccount_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = gridList.SelectedItems;
                if (gridList.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (MessageBoxControl.ShowMessageConfirm("Bạn có muốn xóa Tài khoản này không?") == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        KeyServer.User.User user = (KeyServer.User.User)i.GetRow().DataRow;
                        productServices.DeleteUser(user.UserName, KeyCode.ConvertToBase64(user.PassWord), user.UserName);
                    }
                }
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void gridList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    KeyServer.User.User user = (KeyServer.User.User)gridList.GetRow().DataRow;
                    frmUser f = new frmUser();
                    f.User = user;
                    f.isEdit = true;
                    f.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void gridList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

    }
}
