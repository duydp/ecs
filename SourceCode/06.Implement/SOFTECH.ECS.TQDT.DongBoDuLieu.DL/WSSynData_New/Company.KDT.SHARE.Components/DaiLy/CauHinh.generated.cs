using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components
{
	public partial class CauHinh
	{
		#region Properties.
		
		public string MaDaiLy { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string TenDoanhNghiep { set; get; }
		public int Value { set; get; }
		public string GhiChu { set; get; }
		public int IsUse { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static CauHinh Load(string maDaiLy)
		{
			const string spName = "[dbo].[p_DaiLy_CauHinh_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.NChar, maDaiLy);
			CauHinh entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new CauHinh();
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("Value"))) entity.Value = reader.GetInt32(reader.GetOrdinal("Value"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsUse"))) entity.IsUse = reader.GetInt32(reader.GetOrdinal("IsUse"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_DaiLy_CauHinh_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_DaiLy_CauHinh_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_DaiLy_CauHinh_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_DaiLy_CauHinh_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertCauHinh(string maDoanhNghiep, string tenDoanhNghiep, int value, string ghiChu, int isUse)
		{
			CauHinh entity = new CauHinh();	
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.Value = value;
			entity.GhiChu = ghiChu;
			entity.IsUse = isUse;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_DaiLy_CauHinh_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.NChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.Char, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@Value", SqlDbType.Int, Value);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@IsUse", SqlDbType.Int, IsUse);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateCauHinh(string maDaiLy, string maDoanhNghiep, string tenDoanhNghiep, int value, string ghiChu, int isUse)
		{
			CauHinh entity = new CauHinh();			
			entity.MaDaiLy = maDaiLy;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.Value = value;
			entity.GhiChu = ghiChu;
			entity.IsUse = isUse;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_DaiLy_CauHinh_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.NChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.Char, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@Value", SqlDbType.Int, Value);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@IsUse", SqlDbType.Int, IsUse);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateCauHinh(string maDaiLy, string maDoanhNghiep, string tenDoanhNghiep, int value, string ghiChu, int isUse)
		{
			CauHinh entity = new CauHinh();			
			entity.MaDaiLy = maDaiLy;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.Value = value;
			entity.GhiChu = ghiChu;
			entity.IsUse = isUse;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_DaiLy_CauHinh_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.NChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.Char, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@Value", SqlDbType.Int, Value);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@IsUse", SqlDbType.Int, IsUse);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteCauHinh(string maDaiLy)
		{
			CauHinh entity = new CauHinh();
			entity.MaDaiLy = maDaiLy;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_DaiLy_CauHinh_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.NChar, MaDaiLy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_DaiLy_CauHinh_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		#endregion
	}	
}