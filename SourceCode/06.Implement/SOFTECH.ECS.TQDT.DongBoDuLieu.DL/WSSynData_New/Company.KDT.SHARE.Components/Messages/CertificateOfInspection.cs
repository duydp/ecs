﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages
{
    public class CertificateOfInspection
    {
        /// <summary>
        /// dia diem giam dinh
        /// </summary>
        [XmlElement("Examination")]
        public Examination Examination { get; set; }
        

        /// <summary>
        /// Thong tin khac
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        ///// <summary>
        ///// Thong tin hang
        ///// </summary>
        //[XmlElement("GoodsItem")]
        //public List<GoodsItem> GoodsItem { get; set; }

    }
}
