﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class ImportDeclarationDocument : DeclarationBase
    {
        [XmlElement("DeclarationDocument")]
        public List<DeclarationDocument> DeclarationDocuments { get; set; }
    }
}
