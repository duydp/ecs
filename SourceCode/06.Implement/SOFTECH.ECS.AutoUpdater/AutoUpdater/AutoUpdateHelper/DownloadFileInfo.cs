﻿/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ECS.AutoUpdate
{
    public class DownloadFileInfo
    {
        #region The private fields
        string downloadUrl = string.Empty;
        string fileName = string.Empty;
        string lastver = string.Empty;
        int size = 0;
        bool isDatabase = false;
        bool needRestart = false;
        #endregion

        #region The public property
        public string DownloadUrl { get { return downloadUrl; } set { downloadUrl = value; } }
        public string FileFullName { get { return fileName; } set { fileName = value; } }
        public string FileName { get { return Path.GetFileName(FileFullName); } }
        public string LastVer { get { return lastver; } set { lastver = value; } }
        public int Size { get { return size; } set { size = value; } }
        public bool IsDatabase { get { return isDatabase; } set { isDatabase = value; } }
        public bool NeedRestart { get { return needRestart; } set { needRestart = value; } }
        #endregion

        #region The constructor of DownloadFileInfo
        public DownloadFileInfo()
        {

        }
        public DownloadFileInfo(string url, string name, string ver, int size, bool isDatabase, bool needRestart)
        {
            this.downloadUrl = url;
            this.fileName = name;
            this.lastver = ver;
            this.size = size;
            this.isDatabase = isDatabase;
            this.needRestart = needRestart;
        }
        #endregion
    }
}
