﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;
using System.Data;
using System.IO;
using System.Xml;

namespace Company.KDT.SHARE.Components
{
    public class SQL
    {
        //public static Microsoft.SqlServer.Management.Smo.Server server;
        static SqlConnection sqlConnection = new SqlConnection();
        /// <summary>
        /// Initializes the field 'server'
        /// </summary>
        public static void InitializeServer()
        {
            // To Connect to our SQL Server - we Can use the Connection from the System.Data.SqlClient Namespace.
            SqlConnection sqlConnection = new SqlConnection(@"Integrated Security=SSPI; Data Source=(local)\ECSEXPRESS");

            //build a "serverConnection" with the information of the "sqlConnection"
            //Microsoft.SqlServer.Management.Common.ServerConnection serverConnection = new Microsoft.SqlServer.Management.Common.ServerConnection(sqlConnection);

            //The "serverConnection is used in the ctor of the Server.
            //server = new Server(serverConnection);
        }

        /// <summary>
        /// Initializes the field 'server'
        /// </summary>
        public static void InitializeServer(string sqlConnectionString)
        {
            // To Connect to our SQL Server - we Can use the Connection from the System.Data.SqlClient Namespace.
            sqlConnection = new SqlConnection(sqlConnectionString);

            //build a "serverConnection" with the information of the "sqlConnection"
            //Microsoft.SqlServer.Management.Common.ServerConnection serverConnection = new Microsoft.SqlServer.Management.Common.ServerConnection(sqlConnection);

            //The "serverConnection is used in the ctor of the Server.
            //server = new Server(serverConnection);
        }

        public static List<string> ListDatabases()
        {
            List<string> listData = new List<string>();
            DataTable dtSource = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = "SELECT name FROM sys.sysdatabases";
            adapter.SelectCommand = new SqlCommand(query, sqlConnection);
            adapter.Fill(dtSource);
            foreach (DataRow dr in dtSource.Rows)
            {
                listData.Add(dr[0].ToString());
            }
            return listData;
        }

        public static List<string> TableLists(string connectionString)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            List<string> listData = new List<string>();
            DataTable dtSource = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();

            //string query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME <> 'sysdiagrams'";

            //Shows all user tables and row counts for the current database 
            //Remove is_ms_shipped = 0 check to include system objects 
            //i.index_id < 2 indicates clustered index (1) or hash table (0) 
            string query = "SELECT o.name, ddps.row_count " +
            "FROM sys.indexes AS i " +
             "INNER JOIN sys.objects AS o ON i.OBJECT_ID = o.OBJECT_ID " +
             "INNER JOIN sys.dm_db_partition_stats AS ddps ON i.OBJECT_ID = ddps.OBJECT_ID " +
             "AND i.index_id = ddps.index_id " +
            "WHERE i.index_id < 2 AND o.is_ms_shipped = 0 AND o.name <> 'sysdiagrams' " +
            "ORDER BY o.NAME ";

            adapter.SelectCommand = new SqlCommand(query, sqlConnection);
            adapter.Fill(dtSource);
            foreach (DataRow dr in dtSource.Rows)
            {
                listData.Add(string.Format("{0} ({1})", dr["name"].ToString(), dr["row_count"].ToString()));
            }
            return listData;
        }

        public static DataTable DataTables(string connectionString)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            DataTable dtSource = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();

            //string query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME <> 'sysdiagrams'";

            //Shows all user tables and row counts for the current database 
            //Remove is_ms_shipped = 0 check to include system objects 
            //i.index_id < 2 indicates clustered index (1) or hash table (0) 
            string query = "SELECT o.name, ddps.row_count " +
            "FROM sys.indexes AS i " +
             "INNER JOIN sys.objects AS o ON i.OBJECT_ID = o.OBJECT_ID " +
             "INNER JOIN sys.dm_db_partition_stats AS ddps ON i.OBJECT_ID = ddps.OBJECT_ID " +
             "AND i.index_id = ddps.index_id " +
            "WHERE i.index_id < 2 AND o.is_ms_shipped = 0 AND o.name <> 'sysdiagrams' " +
            "ORDER BY o.NAME ";

            adapter.SelectCommand = new SqlCommand(query, sqlConnection);
            adapter.Fill(dtSource);

            return dtSource;
        }

        /// <summary>
        /// Columns: column_name, data_type, character_maximum_length, table_name,ordinal_position, is_nullable
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static DataTable DataColumns(string connectionString, string tableName)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            List<string> listData = new List<string>();
            DataTable dtSource = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();

            string query =
                "SELECT column_name, data_type, character_maximum_length, table_name,ordinal_position, is_nullable " +
                "FROM information_schema.COLUMNS WHERE table_name LIKE '" + tableName + "' " +
                "ORDER BY ordinal_position";

            adapter.SelectCommand = new SqlCommand(query, sqlConnection);
            adapter.Fill(dtSource);

            return dtSource;
        }

        //private BackupDevice backupDevice;

        //public BackupDevice BackupDevice
        //{
        //    get { return backupDevice; }
        //    set { backupDevice = value; }
        //}

        private static void SearchFile(string[] paras, string[] files)
        {
            foreach (string item in files)
            {
                if (Path.GetExtension(item).ToLower() == ".mdf")
                {
                    paras[0] = item;
                }
                else if (Path.GetExtension(item).ToLower() == ".ldf")
                {
                    paras[1] = item;
                }
                if (paras[0] != string.Empty && paras[1] != string.Empty)
                    break;
            }
        }

        /// <summary>
        /// Tao CSDL tu file du lieu da co - Attach database file (.mdf)
        /// </summary>
        /// <param name="serverName">Server (Mac dinh: .\ECSExpress)</param>
        /// <param name="dbName">Ten CSDL (Mac dinh theo mau: ECS_TQDT_KD_V4)</param>
        /// <param name="dbUser">Ten dang nhap (Mac dinh: sa)</param>
        /// <param name="dbPassWord">Mat khau (Mac dinh: 123456)</param>
        /// <param name="loaiHinh">Loai hinh: KD, GC, SXXK</param>
        /// <param name="version">Phien ban du lieu: 3, 4</param>
        /// <returns></returns>
        public static bool AttachDatabase(string serverName, string dbName, string dbUser, string dbPassWord, string loaiHinh, int version)
        {
            try
            {
                string[] paras = new string[3] { string.Empty, string.Empty, string.Empty };
                string[] files = System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.mdf", System.IO.SearchOption.AllDirectories);
                SearchFile(paras, files);

                files = System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.ldf", System.IO.SearchOption.AllDirectories);
                SearchFile(paras, files);

                if (paras[0] == string.Empty || paras[1] == string.Empty)
                {
                    Helper.Controls.MessageBoxControlV.ShowMessage("Không tìm thấy file Cơ sở dữ liệu.", false);
                    return false;
                }

                try
                {
                    using (SqlConnection cnn = new SqlConnection())
                    {
                        cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};Connect Timeout={4}", new object[] { serverName, dbName, dbUser, dbPassWord, Company.KDT.SHARE.Components.Globals.TimeoutBackup * 60 }); //TimeoutBackup = phút * 60 giây.
                        cnn.Open();
                        cnn.Close();
                    }

                    Helper.Controls.MessageBoxControlV.ShowMessage("Phát hiện dữ liệu mặc định đã được thiết lập.", false);
                }
                catch (SqlException)
                {
                    if (Helper.Controls.MessageBoxControlV.ShowMessage("Phát hiện dữ liệu mặc định chưa thiết lập\nBạn có muốn thiết lập tự động không?", true) == System.Windows.Forms.DialogResult.No)
                        return false;

                    try
                    {
                        /******************************************************************
                        //1. Tạo thư mục lưu data (Nếu chưa có): [Ổ đĩa]/SOFTECH/ECS/DATABASE 
                        //2. Kiểm tra tên file data, tên file log vật lý tại thư mục lưu dữ liệu có trùng tên trước khi copy.
                        //   Nếu đã có thì cảnh báo và không thực hiện copy file.
                        //3. Copy file du lieu từ [Thư mục chương trình ECS]\DATA den vi tri thư mục mới: [Ổ đĩa]/SOFTECH/ECS/DATABASE
                        //4. Lấy thông tin đường dẫn mới của file data.
                        *******************************************************************/

                        //1.
                        string dataFilePath = paras[0];
                        string dataLogPath = paras[1];
                        System.IO.FileInfo fiData = new FileInfo(dataFilePath);
                        System.IO.FileInfo fiDataLog = new FileInfo(dataLogPath);

                        string driverName = System.IO.Directory.GetDirectoryRoot(dataFilePath); //fiData.Directory.Root.Name;                                                
                        if (driverName.Trim() == "C:\\")
                        {
                            string[] drivers = System.IO.Directory.GetLogicalDrives();

                            if (drivers.Length == 1)
                            {
                                driverName = "C:\\";
                            }
                            else
                            {
                                for (int i = 0; i < drivers.Length; i++)
                                {
                                    if (drivers[i] == "C:\\" || drivers[i] == "A:\\" || drivers[i] == "B:\\")
                                        continue;
                                    if (drivers[i] == "D:\\")
                                    {
                                        driverName = drivers[i];
                                        break;
                                    }
                                    else
                                    {
                                        driverName = drivers[i];
                                        break;
                                    }
                                }
                            }
                        }

                        string folderData = (driverName + "SOFTECH\\ECS\\TQDT\\DATABASE");
                        if (!System.IO.Directory.Exists(folderData))
                        {
                            //System.IO.Directory.CreateDirectory(folderData);
                            //Dung dong code duoi de phan quyen tren folder
                            Company.KDT.SHARE.Components.CommonApplicationData direc = new Company.KDT.SHARE.Components.CommonApplicationData(driverName + "SOFTECH\\ECS\\TQDT", "DATABASE", true);
                        }

                        //2.
                        if (System.IO.File.Exists(folderData + "\\" + fiData.Name))
                        {
                            Helper.Controls.MessageBoxControlV.ShowMessage("Đã tồn tại 1 file Cơ sở dữ liệu tên '" + fiData.Name + "', không thể tạo mới dữ liệu.\r\n\nBạn vui lòng kiểm tra lại.", false);
                            return false;
                        }
                        else if (System.IO.File.Exists(folderData + "\\" + fiDataLog.Name))
                        {
                            Helper.Controls.MessageBoxControlV.ShowMessage("Đã tồn tại 1 file 'LOG' Cơ sở dữ liệu tên '" + fiDataLog.Name + "', không thể tạo mới dữ liệu.\r\n\nBạn vui lòng kiểm tra lại.", false);
                            return false;
                        }
                        else
                        {
                            //Phan quyen thu muc data

                            //Copy file data
                            System.IO.File.Copy(dataFilePath, folderData + "\\" + fiData.Name);
                            System.IO.File.Copy(dataLogPath, folderData + "\\" + fiDataLog.Name);

                            paras[0] = folderData + "\\" + fiData.Name;
                            paras[1] = folderData + "\\" + fiDataLog.Name;
                        }

                        //-----------------------------------------------------------------

                        paras[2] = dbName;

                        using (SqlConnection cnn = new SqlConnection())
                        {
                            //Thiet lap thong tin mac dinh ket noi
                            serverName = ".\\ECSExpress";
                            dbName = string.Format("ECS_TQDT_{0}_V{1}", loaiHinh, version);
                            dbUser = "sa";
                            dbPassWord = "123456";

                            cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};Connect Timeout={4}", new object[] { serverName, "master", dbUser, dbPassWord, Company.KDT.SHARE.Components.Globals.TimeoutBackup * 60 }); //TimeoutBackup = phút * 60 giây.
                            cnn.Open();
                            string sfmtCreateData = string.Format(@"
                                                                    CREATE DATABASE [{0}] ON 
                                                                    ( FILENAME = N'{1}' ),
                                                                    ( FILENAME = N'{2}' )
                                                                     FOR ATTACH
                                                                    ", new object[]{paras[2],paras[0],paras[1]                                                                     
                                                                 });
                            SqlCommand sqlCmd = new SqlCommand(sfmtCreateData);
                            sqlCmd.CommandType = CommandType.Text;
                            sqlCmd.Connection = cnn;
                            sqlCmd.ExecuteNonQuery();
                        }

                        System.Xml.XmlDocument docApp = new System.Xml.XmlDocument();

                        string cfgName = string.Empty;

                        //if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "//" + applicationName + ".exe.Config"))
                        //    cfgName = AppDomain.CurrentDomain.BaseDirectory + "//" + applicationName + ".exe.Config";
                        if (File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile))
                            cfgName = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;

                        if (cfgName != string.Empty)
                        {
                            docApp.Load(cfgName);
                            XmlNode nodeCnn = docApp.SelectSingleNode("configuration/connectionStrings/add[@name=\"MSSQL\"]");
                            if (nodeCnn != null/* && !string.IsNullOrEmpty(nodeCnn.Attributes["name"].Value) && nodeCnn.Attributes["name"].Value.ToUpper() == "MSSQL"*/)
                            {
                                nodeCnn.Attributes["connectionString"].Value = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { serverName, dbName, dbUser, dbPassWord });
                            }
                            docApp.Save(cfgName);
                        }

                        //Sau khi luu cau hinh ket noi, kiem tra lai ket noi den CSDL moi tao
                        try
                        {
                            using (SqlConnection cnn = new SqlConnection())
                            {
                                cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { serverName, dbName, dbUser, dbPassWord });
                                cnn.Open();
                                cnn.Close();
                            }

                            Helper.Controls.MessageBoxControlV.ShowMessage("Đã tạo Cơ sở dữ liệu thành công.", false);
                        }
                        catch (SqlException sqlex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(sqlex);
                            Helper.Controls.MessageBoxControlV.ShowMessage("Đã tạo Cơ sở dữ liệu, nhưng kết nối không thành công. Xin vui lòng kiểm tra lại thông tin.\r\n\nBạn có thể liên hệ nhà cung cấp phần mềm để được hướng dẫn.", false);
                            return false;
                        }

                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("Access is denied"))
                        {
                            Helper.Controls.MessageBoxControlH.ShowMessage("Không có quyền truy cập/ ghi file dữ liệu.\r\n\nXin vui lòng kiểm tra lại phân quyền cho phép ghi file trên thư mục của chương trình.", false);
                        }
                        else
                            Helper.Controls.MessageBoxControlH.ShowMessage(ex.Message, false);
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return false; }
        }

        public static bool Execute_SQLScript(string serverName, string dbName, string dbUser, string dbPassWord, string fileName, out string error)
        {
            try
            {
                if (!System.IO.File.Exists("osql.exe"))
                {
                    Logger.LocalLogger.Instance().WriteMessage(new Exception("Thiếu tệp tin OSQL.exe"));
                    throw new Exception("Thiếu tệp tin OSQL.exe");
                    //return false;
                }

                #region Update theo Osql

                if (!System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Data\\Update\\LogUpdate"))
                    System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Data\\Update\\LogUpdate");

                string sfmtArgs = string.Format(@"-S {0} -U {1} -P {2} -d {3} -i ""{4}"" -o ""Data\Update\LogUpdate\{5}.log""", new object[] { serverName, dbUser, dbPassWord, dbName, fileName, Path.GetFileNameWithoutExtension(fileName) });
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("Osql.exe", sfmtArgs);
                psi.CreateNoWindow = true;
                psi.ErrorDialog = true;
                psi.UseShellExecute = false;
                System.Diagnostics.Process p = System.Diagnostics.Process.Start(psi);
                p.WaitForExit();

                error = "";

                return true;

                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                error = ex.Message;
                return false;
            }
        }

        public static string GetFolderDatabaseRemote(string serverName, string dbName, string dbUser, string dbPassWord)
        {
            if (string.IsNullOrEmpty(serverName) || string.IsNullOrEmpty(dbName) || string.IsNullOrEmpty(dbUser))
                return "";
            else
            {
                string folderRemote = "";
                bool connection = false;
                try
                {
                    using (SqlConnection cnn = new SqlConnection())
                    {
                        cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};Connect Timeout={4}", new object[] { serverName, dbName, dbUser, dbPassWord, Company.KDT.SHARE.Components.Globals.TimeoutBackup * 60 }); //TimeoutBackup = phút * 60 giây.
                        cnn.Open();
                        connection = true;
                        string cmdText = @"
			                                --Default backup database
                                            --Created 09-2012
                                            --Modified 09-2012
                                            --Backup database at folder data *.mdf with format dbName_yyyy_MM_dd.bak
                                            BEGIN

                                            DECLARE @filedata  AS NVARCHAR(max)
                                            DECLARE @filename  AS NVARCHAR(max)
                                            DECLARE @dir  AS NVARCHAR(max)
                                            DECLARE @dbName AS NVARCHAR(255)

                                            SET @filedata=(
                                            SELECT TOP(1) 	filename
                                                from sysfiles
                                                order by fileid)
                                            SET @dbName = (select TOP(1) name from sys.sysdatabases  WHERE filename=@filedata)
                                            SET @filename =
                                            LTRIM(
                                              RTRIM(
                                               REVERSE(
                                                SUBSTRING(
                                                 REVERSE(@filedata),
                                                 0,
                                                 CHARINDEX('\', REVERSE(@filedata),0)
                                                )
                                               )
                                              )
                                             )
                                            SET @dir = REPLACE(@filedata,@filename,'')
                                            
                                            Select @dir

                                            END
                                         ";
                        SqlCommand sqlCmd = new SqlCommand(cmdText);
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Connection = cnn;
                        folderRemote = (string)sqlCmd.ExecuteScalar();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw ex;
                }

                return folderRemote;
            }
        }

        public static bool BackupDatabase_SQLScript(string serverName, string dbName, string dbUser, string dbPassWord)
        {
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Data\\Update\\LogUpdate"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Data\\Update\\LogUpdate");

            if (string.IsNullOrEmpty(serverName) || string.IsNullOrEmpty(dbName) || string.IsNullOrEmpty(dbUser))
                return false;
            else
            {
                bool connection = false;
                try
                {
                    using (SqlConnection cnn = new SqlConnection())
                    {
                        cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};Connect Timeout={4}", new object[] { serverName, dbName, dbUser, dbPassWord, Company.KDT.SHARE.Components.Globals.TimeoutBackup * 60 }); //TimeoutBackup = phút * 60 giây.
                        cnn.Open();
                        connection = true;
                        string cmdText = @"
			                                --Default backup database
                                            --Created 09-2012
                                            --Modified 09-2012
                                            --Backup database at folder data *.mdf with format dbName_yyyy_MM_dd.bak
                                            BEGIN

                                            DECLARE @filedata  AS NVARCHAR(max)
                                            DECLARE @filename  AS NVARCHAR(max)
                                            DECLARE @dir  AS NVARCHAR(max)
                                            DECLARE @dbName AS NVARCHAR(255)

                                            SET @filedata=(
                                            SELECT TOP(1) 	filename
                                                from sysfiles
                                                order by fileid)
                                            SET @dbName = (select TOP(1) name from sys.sysdatabases  WHERE filename=@filedata)
                                            SET @filename =
                                            LTRIM(
                                              RTRIM(
                                               REVERSE(
                                                SUBSTRING(
                                                 REVERSE(@filedata),
                                                 0,
                                                 CHARINDEX('\', REVERSE(@filedata),0)
                                                )
                                               )
                                              )
                                             )
                                            SET @dir = REPLACE(@filedata,@filename,'')
                                            SET @filename = @dbName+ '_' +
                                                             REPLACE(CONVERT(VARCHAR(255), GETDATE(), 102),'.','_') +'.bak'

                                            SET @filename = @dir + @filename			                                
                                            BACKUP DATABASE @dbName
                                            TO DISK =@filename
                                            WITH FORMAT,INIT,MEDIANAME = 'Z_SQLServerBackups',
                                            NAME = 'Full Backup of QLDT',
                                            SKIP,NOREWIND,NOUNLOAD,STATS = 10;
                                            END
                                         ";
                        SqlCommand sqlCmd = new SqlCommand(cmdText);
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Connection = cnn;
                        sqlCmd.CommandTimeout = Company.KDT.SHARE.Components.Globals.TimeoutBackup * 60;
                        sqlCmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw ex;
                    //return false;
                }

                return connection;
            }
        }

        public static bool BackupDatabase_SQLScript(string serverName, string dbName, string dbUser, string dbPassWord, string folderPath, string fileName)
        {
            if (string.IsNullOrEmpty(serverName) || string.IsNullOrEmpty(dbName) || string.IsNullOrEmpty(dbUser))
                return false;
            else
            {
                bool connection = false;
                try
                {
                    using (SqlConnection cnn = new SqlConnection())
                    {
                        cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};Connect Timeout={4}", new object[] { serverName, dbName, dbUser, dbPassWord, Company.KDT.SHARE.Components.Globals.TimeoutBackup * 60 }); //TimeoutBackup = phút * 60 giây.

                        cnn.Open();
                        connection = true;
                        string cmdText = @"
											--Default backup database LanNT                                            
											--Create 10-05-2012
											BEGIN

											DECLARE @filedata  AS NVARCHAR(255)
											DECLARE @filename  AS NVARCHAR(255)
											DECLARE @dbName AS NVARCHAR(255)
										   
											SET @dbName = '" + dbName + @"'
											SET @filename ='" + Path.Combine(folderPath, fileName) + @"'		                                
											BACKUP DATABASE @dbName
											TO DISK =@filename
											WITH FORMAT,INIT,MEDIANAME = 'Z_SQLServerBackups',
											NAME = 'Full Backup of AdventureWorks',
											SKIP,NOREWIND,NOUNLOAD,STATS = 10;
											END
										 ";
                        SqlCommand sqlCmd = new SqlCommand(cmdText);
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Connection = cnn;
                        sqlCmd.CommandTimeout = Company.KDT.SHARE.Components.Globals.TimeoutBackup * 60;
                        sqlCmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw ex;
                    //return false;
                }

                return connection;
            }
        }

        public static string GetDatabaseVersion_QLDT(string serverName, string dbName, string dbUser, string dbPassWord)
        {
            try
            {
                return GetDatabaseVersion(serverName, dbName, dbUser, dbPassWord, "t_PhienBan", "BanSo");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return string.Empty;
            }
        }

        public static string GetDatabaseVersion_ECS(string serverName, string dbName, string dbUser, string dbPassWord)
        {
            try
            {
                return GetDatabaseVersion(serverName, dbName, dbUser, dbPassWord, "t_Haiquan_Version", "Version");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return string.Empty;
            }
        }

        public static string GetDatabaseVersion(string serverName, string dbName, string dbUser, string dbPassWord, string tableName, string versionColumnName)
        {
            string lasVersion = "";

            try
            {
                using (SqlConnection cnn = new SqlConnection())
                {
                    cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { serverName, dbName, dbUser, dbPassWord });
                    cnn.Open();

                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.CommandType = CommandType.Text;
                    sqlCmd.Connection = cnn;

                    string cmdText = @"SELECT COUNT(*)
                                           FROM   INFORMATION_SCHEMA.TABLES 
                                           WHERE  TABLE_NAME = N'" + tableName + @"' 
                                           AND TABLE_TYPE = 'BASE TABLE'";

                    sqlCmd.CommandText = cmdText;
                    object objRet = sqlCmd.ExecuteScalar();
                    if (objRet != null && int.Parse(objRet.ToString()) != 0)
                    {
                        cmdText = "select max(cast([" + versionColumnName + "] as decimal(5,2))) from " + tableName + ""; //TODO: Hungtq, 11/11/2013
                        sqlCmd.CommandText = cmdText;
                        objRet = sqlCmd.ExecuteScalar();
                        if (objRet != null)
                            lasVersion = System.Convert.ToDecimal(objRet).ToString("##0.0#", new System.Globalization.CultureInfo("en-US")); //TODO: Hungtq, 11/11/2013
                        else
                        {
                            cmdText = @"INSERT INTO [dbo].[" + tableName + @"]
                                               ([" + versionColumnName + @"]
                                               ,[Date]
                                               ,[Notes])
                                         VALUES
                                               ('1.0'
                                               ,GETDATE()
                                               ,'AUTOUPDATE-DEFAULT V1.0')";
                            lasVersion = "1.0";
                            sqlCmd.CommandText = cmdText;
                            sqlCmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        cmdText = @"CREATE TABLE [dbo].[" + tableName + @"](
	                                [ID] [int] IDENTITY(1,1) NOT NULL,
	                                [" + versionColumnName + @"] [nvarchar](5) NULL,
	                                [Date] [datetime] NOT NULL,
	                                [Notes] [nvarchar](255) NULL,
                                     CONSTRAINT [PK_t_Version] PRIMARY KEY CLUSTERED 
                                    (
	                                    [ID] ASC
                                    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                                    ) ON [PRIMARY]";
                        sqlCmd.CommandText = cmdText;
                        sqlCmd.ExecuteNonQuery();

                        cmdText = @"INSERT INTO [dbo].[" + tableName + @"]
                                               ([" + versionColumnName + @"]
                                               ,[Date]
                                               ,[Notes])
                                         VALUES
                                               ('1.0'
                                               ,GETDATE()
                                               ,'AUTOUPDATE-DEFAULT V1.0')";
                        lasVersion = "1.0";
                        sqlCmd.CommandText = cmdText;
                        sqlCmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                lasVersion = string.Empty;
            }

            return lasVersion;
        }

        public static string GetSQLFileVersion(string sqlFile)
        {
            System.IO.FileInfo fi = new FileInfo(sqlFile);

            string sqlFileName = fi != null ? fi.Name : "";

            int posVersion = sqlFileName.IndexOf("_V");
            if (posVersion == -1)
                return "";

            string version = sqlFileName.Replace(".sql", string.Empty);

            version = version.Remove(0, posVersion + 2);

            return version;
        }

        public static string GetDatabaseNewVersion(string serverName, string dbName, string dbUser, string dbPassWord)
        {
            List<string> sqlFiles = new List<string>();
            string oldversion = "";
            string newversion = "";

            string[] files = GetSQLUpdateFiles();
            //Logger.LocalLogger.Instance().WriteMessage(System.AppDomain.CurrentDomain.BaseDirectory + "Data\\Update", new Exception("Ghi log"));

            DataTable dtVersion = new DataTable();
            dtVersion.Columns.Add("Version", typeof(decimal));

            if (files.Length > 0)
            {
                foreach (string strV in files)
                {
                    DataRow drV = dtVersion.NewRow();
                    drV["Version"] = Convert.ToDecimal(SQL.GetSQLFileVersion(strV), new System.Globalization.CultureInfo("en-US"));
                    dtVersion.Rows.Add(drV);
                }

                DataView dvV = dtVersion.DefaultView;
                dvV.Sort = "Version asc";

                foreach (DataRowView item in dvV)
                {
                    newversion = System.Convert.ToString(item["Version"], new System.Globalization.CultureInfo("en-US"));

                    if (string.Compare(newversion, oldversion) > 0)
                    {
                        oldversion = newversion;
                    }
                }
            }

            return newversion;
        }

        public static string[] GetSQLUpdateFiles()
        {
            string[] sqlFiles = System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "Data\\Update", "*.sql");
            Array.Sort<string>(sqlFiles);

            return sqlFiles;
        }

        /// <summary>
        /// Tạo chuỗi query sql lọc thông tin từ mảng mã cột, giá trị cột tương ứng.
        /// </summary>
        /// <param name="columnNames"></param>
        /// <param name="columnValues"></param>
        /// <returns></returns>
        public static string CreateQueryFilter(string[] columnNames, object[] columnValues)
        {
            string query = "";
            for (int i = 0; i < columnNames.Length; i++)
            {
                if (columnValues[i].GetType() == typeof(System.String) || columnValues[i].GetType() == typeof(System.DateTime))
                    query += columnNames[i] + " = '" + columnValues[i] + "'";
                else
                    query += columnNames[i] + " = " + columnValues[i];

                if (i < columnNames.Length - 1)
                    query += " AND ";
            }
            return query;
        }

        public static bool TestConnection(string connectionString)
        {
            try
            {
                sqlConnection = new SqlConnection(connectionString);

                sqlConnection.Open();

                sqlConnection.Close();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return false;
        }

        /// <summary>
        /// TODO: VNACCS---------------------------------------------------------------------------------------------
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="dbName"></param>
        /// <param name="dbUser"></param>
        /// <param name="dbPassWord"></param>
        public static void CheckUserTableIsHaveVNACCS(string serverName, string dbName, string dbUser, string dbPassWord)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection())
                {
                    cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { serverName, dbName, dbUser, dbPassWord });
                    cnn.Open();

                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.CommandType = CommandType.Text;
                    sqlCmd.Connection = cnn;

                    string cmdText = "";
                    //Chua co field -> Bo sung them cac field VNACC vao bang USER
                    #region Bo sung them cac field
                    cmdText = @"IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_UserCode') = 0
                                    BEGIN
                                        ALTER TABLE dbo.[User]
                                        ADD VNACC_UserCode [varchar](50) NULL
                                    END	


                                    IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_UserID') = 0
                                    BEGIN
                                        ALTER TABLE dbo.[User]
                                        ADD VNACC_UserID [varchar](3) NULL
                                    END	


                                    IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_UserPass') = 0
                                    BEGIN
                                        ALTER TABLE dbo.[User]
                                        ADD VNACC_UserPass [nvarchar](200) NULL
                                    END	


                                    IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_TerminalID') = 0
                                    BEGIN
                                        ALTER TABLE dbo.[User]
                                        ADD VNACC_TerminalID [varchar](50) NULL
                                    END	


                                    IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_TerminalPass') = 0
                                    BEGIN
                                        ALTER TABLE dbo.[User]
                                        ADD VNACC_TerminalPass [nvarchar](200) NULL
                                    END	


                                    IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_CA') = 0
                                    BEGIN
                                        ALTER TABLE dbo.[User]
                                        ADD VNACC_CA [nvarchar](250) NULL
                                    END	


                                    IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_CAPublicKey') = 0
                                    BEGIN
                                        ALTER TABLE dbo.[User]
                                        ADD VNACC_CAPublicKey [nvarchar](250) NULL
                                    END	


                                    IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'isOnline') = 0
                                    BEGIN
                                        ALTER TABLE dbo.[User]
                                        ADD isOnline BIT
                                    END	


                                    IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'HostName') = 0
                                    BEGIN
                                        ALTER TABLE dbo.[User]
                                        ADD HostName VARCHAR(100) NULL
                                    END	


                                    IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'IP') = 0
                                    BEGIN
                                        ALTER TABLE dbo.[User]
                                        ADD IP VARCHAR(20) NULL
                                    END	

                                    IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'DataBase') = 0
                                    BEGIN
                                        ALTER TABLE dbo.[User]
                                        ADD [DataBase] VARCHAR(200) NULL
                                    END	
                                    ";
                    #endregion
                    sqlCmd.CommandText = cmdText;
                    sqlCmd.ExecuteNonQuery();

                    #region Bo sung them store
                    cmdText = @"DECLARE @sql NVARCHAR(max)
                                SET @sql = 
                                'IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[p_User_SelectDynamic_VNACC]'') AND type in (N''P'', N''PC''))
                                DROP PROCEDURE [dbo].[p_User_SelectDynamic_VNACC]

                                IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[p_User_Load_VNACC]'') AND type in (N''P'', N''PC''))
                                DROP PROCEDURE [dbo].[p_User_Load_VNACC]

                                IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[p_User_Update_VNACC]'') AND type in (N''P'', N''PC''))
                                DROP PROCEDURE [dbo].[p_User_Update_VNACC]
                                '
                                EXEC sp_executesql @sql

                                SET @sql = 
                                '
                                CREATE PROCEDURE [dbo].[p_User_SelectDynamic_VNACC]
                                    @WhereCondition NVARCHAR(500),
                                    @OrderByExpression NVARCHAR(250) = NULL
                                AS

                                DECLARE @SQL NVARCHAR(MAX)

                                SET @SQL = 
                                ''SELECT 
                                    [ID],
                                    [USER_NAME],
                                    [PASSWORD],
                                    [HO_TEN],
                                    [MO_TA],
                                    [isAdmin],
                                    [VNACC_UserCode],
                                    [VNACC_UserID],
                                    [VNACC_UserPass],
                                    [VNACC_TerminalID],
                                    [VNACC_TerminalPass],
                                    [VNACC_CA],
                                    [VNACC_CAPublicKey],
                                    [isOnline],
                                    [HostName],
                                    [IP],
                                    [DataBase]
                                FROM [dbo].[User] 
                                WHERE '' + @WhereCondition

                                IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
                                BEGIN
                                    SET @SQL = @SQL + '' ORDER BY '' + @OrderByExpression
                                END

                                EXEC sp_executesql @SQL
                                '
                                EXEC sp_executesql @sql

                                SET @sql = 
                                '
                                CREATE PROCEDURE [dbo].[p_User_Load_VNACC]
                                    @ID bigint
                                AS

                                SELECT
                                    [ID],
                                    [USER_NAME],
                                    [PASSWORD],
                                    [HO_TEN],
                                    [MO_TA],
                                    [isAdmin],
                                    [VNACC_UserCode],
                                    [VNACC_UserID],
                                    [VNACC_UserPass],
                                    [VNACC_TerminalID],
                                    [VNACC_TerminalPass],
                                    [VNACC_CA],
                                    [VNACC_CAPublicKey],
                                    [isOnline],
                                    [HostName],
                                    [IP],
                                    [DataBase]
                                FROM
                                    [dbo].[User]
                                WHERE
                                    [ID] = @ID
                                '
                                EXEC sp_executesql @sql

                                SET @sql = 
                                '
                                CREATE PROCEDURE [dbo].[p_User_Update_VNACC]
                                    @ID bigint,
                                    @USER_NAME varchar(50),
                                    @PASSWORD varchar(200),
                                    @HO_TEN nvarchar(1000),
                                    @MO_TA nvarchar(1000),
                                    @isAdmin bit,
                                    @VNACC_UserCode varchar(50),
                                    @VNACC_UserID varchar(3),
                                    @VNACC_UserPass nvarchar(200),
                                    @VNACC_TerminalID varchar(50),
                                    @VNACC_TerminalPass nvarchar(200),
                                    @VNACC_CA nvarchar(250),
                                    @VNACC_CAPublicKey nvarchar(250),
                                    @isOnline bit,
                                    @HostName varchar(100),
                                    @IP varchar(20),
                                    @DataBase varchar(200)
                                AS

                                UPDATE
                                    [dbo].[User]
                                SET
                                    [USER_NAME] = @USER_NAME,
                                    [PASSWORD] = @PASSWORD,
                                    [HO_TEN] = @HO_TEN,
                                    [MO_TA] = @MO_TA,
                                    [isAdmin] = @isAdmin,
                                    [VNACC_UserCode] = @VNACC_UserCode,
                                    [VNACC_UserID] = @VNACC_UserID,
                                    [VNACC_UserPass] = @VNACC_UserPass,
                                    [VNACC_TerminalID] = @VNACC_TerminalID,
                                    [VNACC_TerminalPass] = @VNACC_TerminalPass,
                                    [VNACC_CA] = @VNACC_CA,
                                    [VNACC_CAPublicKey] = @VNACC_CAPublicKey,
                                    [isOnline] = @isOnline,
                                    [HostName] = @HostName,
                                    [IP] = @IP,
                                    [DataBase] = @DataBase
                                WHERE
                                    [ID] = @ID
                                '
                                EXEC sp_executesql @sql";
                    #endregion
                    sqlCmd.CommandText = cmdText;
                    sqlCmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        public static void CheckAndCreateProcedureCheckTerminalOnline(string serverName, string dbName, string dbUser, string dbPassWord)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection())
                {
                    cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { serverName, dbName, dbUser, dbPassWord });
                    cnn.Open();

                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.CommandType = CommandType.Text;
                    sqlCmd.Connection = cnn;

                    string cmdText = "";                    

                    #region Bo sung them store Kiem tra trang thai Terminal
                    cmdText = @"DECLARE @sql NVARCHAR(max)
                                SET @sql = 
                                'IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[CheckAndUpdateTerminalStatus]'') AND type in (N''P'', N''PC''))
                                DROP PROCEDURE [dbo].[CheckAndUpdateTerminalStatus]
                                '
                                EXEC sp_executesql @sql

                                SET @sql = 
                                '
                                CREATE PROC [dbo].[CheckAndUpdateTerminalStatus]
                                AS
                                	
                                DECLARE @user VARCHAR(100), @database VARCHAR(200)

                                DECLARE cur CURSOR FAST_FORWARD READ_ONLY FOR
                                SELECT [USER_NAME], [DataBase] FROM [User]

                                OPEN cur

                                FETCH NEXT FROM cur INTO @user, @database

                                WHILE @@FETCH_STATUS = 0
                                BEGIN

                                if(SELECT COUNT(*) from sys.dm_exec_connections c INNER JOIN sys.dm_exec_sessions s ON c.session_id = s.session_id
                                LEFT JOIN sys.sysprocesses pr ON pr.spid = c.session_id
                                where client_net_address is not null 
                                and (client_net_address = (select IP from [User] where [user_name]  = @user) OR client_net_address = ''<local machine>'') 
                                AND ISNULL(DB_NAME(pr.dbid), N'''') = ( SELECT [DataBase] FROM [User] WHERE [DataBase] = @database and [DataBase] is not null and [user_name]  = @user )
                                and s.program_name = ''.Net SqlClient Data Provider'') > 0
                                    begin
                                        update [User] set isOnline = 1 where [user_name]  = @user
                                    end
                                else
                                    begin
                                        update [User] set isOnline = 0 where [user_name]  = @user
                                    END
                                	
                                FETCH NEXT FROM cur INTO @user, @database

                                END

                                CLOSE cur
                                DEALLOCATE cur
                                '
                                EXEC sp_executesql @sql";
                    #endregion
                    sqlCmd.CommandText = cmdText;
                    sqlCmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

    }
}
