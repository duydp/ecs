﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
	public partial class KDT_GC_DinhMucThucTeDangKy : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long HopDong_ID { set; get; }
		public decimal SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public int TrangThaiXuLy { set; get; }
		public string GuidString { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string TenDoanhNghiep { set; get; }
		public string MaHaiQuan { set; get; }
		public long LenhSanXuat_ID { set; get; }
		public string GhiChu { set; get; }
        public List<KDT_GC_DinhMucThucTe_SP> SPCollection = new List<KDT_GC_DinhMucThucTe_SP>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_DinhMucThucTeDangKy> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_DinhMucThucTeDangKy> collection = new List<KDT_GC_DinhMucThucTeDangKy>();
			while (reader.Read())
			{
				KDT_GC_DinhMucThucTeDangKy entity = new KDT_GC_DinhMucThucTeDangKy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetDecimal(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidString"))) entity.GuidString = reader.GetString(reader.GetOrdinal("GuidString"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("LenhSanXuat_ID"))) entity.LenhSanXuat_ID = reader.GetInt64(reader.GetOrdinal("LenhSanXuat_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_DinhMucThucTeDangKy> collection, long id)
        {
            foreach (KDT_GC_DinhMucThucTeDangKy item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_DinhMucThucTeDangKy VALUES(@HopDong_ID, @SoTiepNhan, @NgayTiepNhan, @TrangThaiXuLy, @GuidString, @MaDoanhNghiep, @TenDoanhNghiep, @MaHaiQuan, @LenhSanXuat_ID, @GhiChu)";
            string update = "UPDATE t_KDT_GC_DinhMucThucTeDangKy SET HopDong_ID = @HopDong_ID, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, GuidString = @GuidString, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, MaHaiQuan = @MaHaiQuan, LenhSanXuat_ID = @LenhSanXuat_ID, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_DinhMucThucTeDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidString", SqlDbType.NVarChar, "GuidString", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidString", SqlDbType.NVarChar, "GuidString", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_DinhMucThucTeDangKy VALUES(@HopDong_ID, @SoTiepNhan, @NgayTiepNhan, @TrangThaiXuLy, @GuidString, @MaDoanhNghiep, @TenDoanhNghiep, @MaHaiQuan, @LenhSanXuat_ID, @GhiChu)";
            string update = "UPDATE t_KDT_GC_DinhMucThucTeDangKy SET HopDong_ID = @HopDong_ID, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, GuidString = @GuidString, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, MaHaiQuan = @MaHaiQuan, LenhSanXuat_ID = @LenhSanXuat_ID, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_DinhMucThucTeDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidString", SqlDbType.NVarChar, "GuidString", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidString", SqlDbType.NVarChar, "GuidString", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_DinhMucThucTeDangKy Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_DinhMucThucTeDangKy_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_DinhMucThucTeDangKy> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_DinhMucThucTeDangKy> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_DinhMucThucTeDangKy> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_GC_DinhMucThucTeDangKy> SelectCollectionBy_LenhSanXuat_ID(long lenhSanXuat_ID)
		{
            IDataReader reader = SelectReaderBy_LenhSanXuat_ID(lenhSanXuat_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_LenhSanXuat_ID(long lenhSanXuat_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, lenhSanXuat_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_LenhSanXuat_ID(long lenhSanXuat_ID)
		{
			const string spName = "p_KDT_GC_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, lenhSanXuat_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_DinhMucThucTeDangKy(long hopDong_ID, decimal soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string guidString, string maDoanhNghiep, string tenDoanhNghiep, string maHaiQuan, long lenhSanXuat_ID, string ghiChu)
		{
			KDT_GC_DinhMucThucTeDangKy entity = new KDT_GC_DinhMucThucTeDangKy();	
			entity.HopDong_ID = hopDong_ID;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GuidString = guidString;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.MaHaiQuan = maHaiQuan;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_DinhMucThucTeDangKy_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.NVarChar, GuidString);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_DinhMucThucTeDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_DinhMucThucTeDangKy item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_DinhMucThucTeDangKy(long id, long hopDong_ID, decimal soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string guidString, string maDoanhNghiep, string tenDoanhNghiep, string maHaiQuan, long lenhSanXuat_ID, string ghiChu)
		{
			KDT_GC_DinhMucThucTeDangKy entity = new KDT_GC_DinhMucThucTeDangKy();			
			entity.ID = id;
			entity.HopDong_ID = hopDong_ID;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GuidString = guidString;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.MaHaiQuan = maHaiQuan;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_DinhMucThucTeDangKy_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.NVarChar, GuidString);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_DinhMucThucTeDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_DinhMucThucTeDangKy item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_DinhMucThucTeDangKy(long id, long hopDong_ID, decimal soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string guidString, string maDoanhNghiep, string tenDoanhNghiep, string maHaiQuan, long lenhSanXuat_ID, string ghiChu)
		{
			KDT_GC_DinhMucThucTeDangKy entity = new KDT_GC_DinhMucThucTeDangKy();			
			entity.ID = id;
			entity.HopDong_ID = hopDong_ID;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GuidString = guidString;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.MaHaiQuan = maHaiQuan;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_DinhMucThucTeDangKy_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.NVarChar, GuidString);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_DinhMucThucTeDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_DinhMucThucTeDangKy item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_DinhMucThucTeDangKy(long id)
		{
			KDT_GC_DinhMucThucTeDangKy entity = new KDT_GC_DinhMucThucTeDangKy();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_DinhMucThucTeDangKy_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_LenhSanXuat_ID(long lenhSanXuat_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, lenhSanXuat_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_DinhMucThucTeDangKy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_DinhMucThucTeDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_DinhMucThucTeDangKy item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.TrangThaiXuLy = -1;
                        this.ID = this.Insert(transaction);
                    }
                    else
                    {
                        this.Update(transaction);
                    }

                    foreach (KDT_GC_DinhMucThucTe_SP item in SPCollection)
                    {
                        item.DinhMucThucTe_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }

                        foreach (KDT_GC_DinhMucThucTe_DinhMuc items in item.DMCollection)
                        {
                            if (items.ID == 0)
                            {
                                items.DinhMucThucTeSP_ID = item.ID;
                                items.Insert(transaction);
                            }
                            else
                            {
                                items.Update(transaction);
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public void TransferGC()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.Update(transaction);
                    foreach (KDT_GC_DinhMucThucTe_SP sp in SPCollection)
                    {
                        foreach (KDT_GC_DinhMucThucTe_DinhMuc item in sp.DMCollection)
                        {
                            GC_DinhMuc dmDuyet = new GC_DinhMuc();
                            dmDuyet.SoTiepNhan = this.SoTiepNhan;
                            dmDuyet.NgayTiepNhan = this.NgayTiepNhan;
                            dmDuyet.LenhSanXuat_ID = this.LenhSanXuat_ID;
                            dmDuyet.GuidString = this.GuidString;
                            dmDuyet.TrangThaiXuLy = this.TrangThaiXuLy;
                            dmDuyet.MaSanPham = item.MaSanPham;
                            dmDuyet.TenSanPham = item.TenSanPham;
                            dmDuyet.MaNguyenPhuLieu = item.MaNPL;
                            dmDuyet.TenNPL = item.TenNPL;
                            dmDuyet.DinhMucSuDung = item.DinhMucSuDung;
                            dmDuyet.TyLeHaoHut = item.TyLeHaoHut;
                            dmDuyet.NPL_TuCungUng = item.NPL_TuCungUng;
                            dmDuyet.HopDong_ID = this.HopDong_ID;
                            dmDuyet.InsertUpdate(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }

            }
        }

        public void DeleteFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {

                    foreach (KDT_GC_DinhMucThucTe_SP lenhsanxuat in this.SPCollection)
                    {
                        foreach (KDT_GC_DinhMucThucTe_DinhMuc item in lenhsanxuat.DMCollection)
                        {
                            Company.GC.BLL.GC.DinhMuc dmGC = new Company.GC.BLL.GC.DinhMuc();
                            dmGC.MaNguyenPhuLieu = item.MaNPL;
                            dmGC.MaSanPham = item.MaSanPham;
                            dmGC.HopDong_ID = this.HopDong_ID;
                            dmGC.DeleteTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void DeleteDinhMucGC()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.Update(transaction);
                    foreach (KDT_GC_DinhMucThucTe_SP lenhsanxuat in this.SPCollection)
                    {
                        Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();                        
                        dmDuyet.MaSanPham = lenhsanxuat.MaSanPham;
                        dmDuyet.HopDong_ID = this.HopDong_ID;
                        dmDuyet.LenhSanXuat_ID = this.LenhSanXuat_ID;
                        dmDuyet.DeleletDinhMuc(this.HopDong_ID, dmDuyet.MaSanPham,dmDuyet.LenhSanXuat_ID, transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
	}	
}