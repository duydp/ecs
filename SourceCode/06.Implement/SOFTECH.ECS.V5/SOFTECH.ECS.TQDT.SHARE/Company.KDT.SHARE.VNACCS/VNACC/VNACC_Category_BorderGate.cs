using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class VNACC_Category_BorderGate
    {
        protected static List<VNACC_Category_BorderGate> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_BorderGate> collection = new List<VNACC_Category_BorderGate>();
            while (reader.Read())
            {
                VNACC_Category_BorderGate entity = new VNACC_Category_BorderGate();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("BorderGateCode"))) entity.BorderGateCode = reader.GetString(reader.GetOrdinal("BorderGateCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("BorderGateName"))) entity.BorderGateName = reader.GetString(reader.GetOrdinal("BorderGateName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_BorderGate> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [BorderGateCode], [BorderGateName] FROM [dbo].[t_VNACC_Category_BorderGate]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_BorderGate> SelectCollectionBy(List<VNACC_Category_BorderGate> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_BorderGate o)
            {
                return o.TableID.Contains(tableID);
            });
        }

        public static List<VNACC_Category_BorderGate> SelectCollectionByCountryCode(List<VNACC_Category_BorderGate> collections, string countryCode)
        {
            return collections.FindAll(delegate(VNACC_Category_BorderGate o)
            {
                return o.BorderGateCode.Contains(countryCode);
            });
        }
    }
}