using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu.CTTT
{
    public partial class CTTT_HopDongXuatKhau : ICloneable
    {
        #region Properties.

        public int Id { set; get; }
        public string SoHopDong { set; get; }
        public DateTime NgayKy { set; get; }
        public decimal TriGia { set; get; }
        public string GhiChu { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static List<CTTT_HopDongXuatKhau> ConvertToCollection(IDataReader reader)
        {
            List<CTTT_HopDongXuatKhau> collection = new List<CTTT_HopDongXuatKhau>();
            while (reader.Read())
            {
                CTTT_HopDongXuatKhau entity = new CTTT_HopDongXuatKhau();
                if (!reader.IsDBNull(reader.GetOrdinal("Id"))) entity.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) entity.NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static bool Find(List<CTTT_HopDongXuatKhau> collection, int id)
        {
            foreach (CTTT_HopDongXuatKhau item in collection)
            {
                if (item.Id == id)
                {
                    return true;
                }
            }

            return false;
        }

        public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_CTTT_HopDongXuatKhau VALUES(@SoHopDong, @NgayKy, @TriGia, @GhiChu)";
            string update = "UPDATE t_CTTT_HopDongXuatKhau SET SoHopDong = @SoHopDong, NgayKy = @NgayKy, TriGia = @TriGia, GhiChu = @GhiChu WHERE Id = @Id";
            string delete = "DELETE FROM t_CTTT_HopDongXuatKhau WHERE Id = @Id";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.Int, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@NgayKy", SqlDbType.DateTime, "NgayKy", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.Int, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@NgayKy", SqlDbType.DateTime, "NgayKy", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.Int, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

        public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_CTTT_HopDongXuatKhau VALUES(@SoHopDong, @NgayKy, @TriGia, @GhiChu)";
            string update = "UPDATE t_CTTT_HopDongXuatKhau SET SoHopDong = @SoHopDong, NgayKy = @NgayKy, TriGia = @TriGia, GhiChu = @GhiChu WHERE Id = @Id";
            string delete = "DELETE FROM t_CTTT_HopDongXuatKhau WHERE Id = @Id";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.Int, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@NgayKy", SqlDbType.DateTime, "NgayKy", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.Int, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@NgayKy", SqlDbType.DateTime, "NgayKy", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.Int, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static CTTT_HopDongXuatKhau Load(int id)
        {
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Id", SqlDbType.Int, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            List<CTTT_HopDongXuatKhau> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static List<CTTT_HopDongXuatKhau> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static List<CTTT_HopDongXuatKhau> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		


        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static int InsertCTTT_HopDongXuatKhau(string soHopDong, DateTime ngayKy, decimal triGia, string ghiChu)
        {
            CTTT_HopDongXuatKhau entity = new CTTT_HopDongXuatKhau();
            entity.SoHopDong = soHopDong;
            entity.NgayKy = ngayKy;
            entity.TriGia = triGia;
            entity.GhiChu = ghiChu;
            return entity.Insert();
        }

        public int Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@Id", SqlDbType.Int, 4);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy.Year <= 1753 ? DBNull.Value : (object)NgayKy);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                Id = (int)db.GetParameterValue(dbCommand, "@Id");
                return Id;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                Id = (int)db.GetParameterValue(dbCommand, "@Id");
                return Id;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(List<CTTT_HopDongXuatKhau> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (CTTT_HopDongXuatKhau item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateCTTT_HopDongXuatKhau(int id, string soHopDong, DateTime ngayKy, decimal triGia, string ghiChu)
        {
            CTTT_HopDongXuatKhau entity = new CTTT_HopDongXuatKhau();
            entity.Id = id;
            entity.SoHopDong = soHopDong;
            entity.NgayKy = ngayKy;
            entity.TriGia = triGia;
            entity.GhiChu = ghiChu;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_CTTT_HopDongXuatKhau_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Id", SqlDbType.Int, Id);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy.Year <= 1753 ? DBNull.Value : (object)NgayKy);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(List<CTTT_HopDongXuatKhau> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (CTTT_HopDongXuatKhau item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateCTTT_HopDongXuatKhau(int id, string soHopDong, DateTime ngayKy, decimal triGia, string ghiChu)
        {
            CTTT_HopDongXuatKhau entity = new CTTT_HopDongXuatKhau();
            entity.Id = id;
            entity.SoHopDong = soHopDong;
            entity.NgayKy = ngayKy;
            entity.TriGia = triGia;
            entity.GhiChu = ghiChu;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Id", SqlDbType.Int, Id);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy.Year <= 1753 ? DBNull.Value : (object)NgayKy);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(List<CTTT_HopDongXuatKhau> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (CTTT_HopDongXuatKhau item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteCTTT_HopDongXuatKhau(int id)
        {
            CTTT_HopDongXuatKhau entity = new CTTT_HopDongXuatKhau();
            entity.Id = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Id", SqlDbType.Int, Id);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(List<CTTT_HopDongXuatKhau> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (CTTT_HopDongXuatKhau item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion


        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
    }
}