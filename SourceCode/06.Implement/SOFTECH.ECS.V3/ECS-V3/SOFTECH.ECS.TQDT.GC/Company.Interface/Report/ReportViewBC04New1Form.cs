﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;
using Company.GC.BLL.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC04New1Form : BaseForm
    {
        public DataSet ds;
        public HopDong HD = new HopDong();
        private BangKe04New1 BK04;
        public XRLabel Label;

        public ReportViewBC04New1Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            ds = new DataSet();
            BK04 = new BangKe04New1();
            Label = new XRLabel();
            this.BK04.report = this;
            ds = this.CreatSchemaDataSetRP04();
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                cbPage.Items.Add("Trang " + (i + 1), i);
            }
            cbPage.SelectedIndex = 0;
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BK04.HD = this.HD;
            this.BK04.First = (cbPage.SelectedIndex == 0);
            if (cbPage.SelectedIndex == cbPage.Items.Count - 1)
            {
                BK04.Last = true;
            }
            else
            {
                BK04.Last = false;
            }
            this.BK04.BindReport(this.ds.Tables[cbPage.SelectedIndex]);
            printControl1.PrintingSystem = this.BK04.PrintingSystem;
            this.BK04.CreateDocument();
        }

        public DataSet CreatSchemaDataSetRP04()
        {

            DataSet dsSTK = new DataSet();
            ToKhaiMauDichCollection tkmdColl = HD.GetTKXKPhanBo();//Lấy danh sách tờ khai xuất cung ứng
            List<ToKhaiChuyenTiep> tkctColl = HD.GetTKCTNhapNPLPhanBo();// Lấy danh sách tờ khai chuyển tiếp xuất cung ứng
            Company.GC.BLL.GC.NguyenPhuLieuCollection nplColl = this.HD.GetNPLCungUngPhanBo();// Lấy danh sách npl cung ứng

            DataTable dt = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoCungUngTK(HD.ID).Tables[0];
            DataTable dt1 = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoCungUngTKCT(HD.ID).Tables[0];
            int soTable = (nplColl.Count - 1) / 7 + 1;//Tổng số table  được lưu vào biến soTable

            DataTable dtTemp;
            for (int i = 0; i < soTable; i++)
            {
                //Tạo cấu trúc bảng
                dtTemp = new DataTable("tbl_" + i.ToString());
                dtTemp.Columns.Add("MaTKX", Type.GetType("System.Int64"));
                dtTemp.Columns.Add("SoTKX", Type.GetType("System.String"));
                for (int j = i * 7; j < (i + 1) * 7; j++)
                {
                    DataColumn col = new DataColumn();
                    if (j < nplColl.Count)
                    {
                        col.ColumnName = nplColl[j].Ma;
                        col.DataType = typeof(decimal);
                        col.Caption = "Tên: " + nplColl[j].Ten + "\nMã: " + nplColl[j].Ma + "\nĐV tính: " + DonViTinh_GetName(nplColl[j].DVT_ID);
                    }
                    else
                    {
                        col.ColumnName = "col_" + j.ToString();
                        col.Caption = "Tên: \nMã \nĐV tính: ";
                    }
                    dtTemp.Columns.Add(col);
                }
                dtTemp.Columns.Add("HTCU", Type.GetType("System.String"));
                //string strID= "";
                //foreach (ToKhaiMauDich tkmd in tkmdColl)
                //{
                //    strID += tkmd.ID + ",";
                //}
                //if (strID.Length > 0) strID = strID.Remove(strID.Length - 1);
                //if (strID != "")
                {

                    
                    foreach (ToKhaiMauDich tkmd in tkmdColl)
                    {
                        //Đưa từng dòng dữ liệu cung ứng của tờ khai mậu dịch vào bảng dtTemp
                        DataRow dr = dtTemp.NewRow();
                        dr[0] = tkmd.ID;
                        dr[1] = tkmd.SoToKhai.ToString() + "-" + tkmd.MaLoaiHinh + "-" + tkmd.NgayDangKy.Year.ToString();
                        DataRow[] dtRow = dt.Select(" ID_TKMD = " + tkmd.ID);

                        for (int k = 2; k < dtTemp.Columns.Count; k++)
                        {

                            foreach (DataRow row in dtRow)
                            {
                                if (row["MaNPL"].ToString().Trim().ToUpper() == dtTemp.Columns[k].ColumnName.Trim().ToUpper())
                                {
                                    dr[k] = Convert.ToDecimal(row["LuongPhanBo"]);
                                    continue;
                                }
                            }

                        }
                        dtTemp.Rows.Add(dr);
                    }
                }
                

                //strID = "";
                //foreach (ToKhaiChuyenTiep tkct in tkctColl)
                //{
                //    strID += tkct.ID + ",";
                //}
                //if (strID.Length > 0) strID = strID.Remove(strID.Length - 1);
                //if (strID != "")
                {
                    
                    foreach (ToKhaiChuyenTiep tkct in tkctColl)
                    {
                        //Đưa từng dòng dữ liệu cung ứng của tờ khai chuyển tiếp vào bảng dtTemp
                        DataRow dr = dtTemp.NewRow();
                        dr[0] = tkct.ID;
                        dr[1] = tkct.SoToKhai.ToString() + "-" + tkct.MaLoaiHinh + "-" + tkct.NgayDangKy.Year.ToString();
                        DataRow[] dtRow = dt1.Select(" ID_TKMD = " + tkct.ID);

                        for (int k = 2; k < dtTemp.Columns.Count; k++)
                        {

                            foreach (DataRow row in dtRow)
                            {
                                if (row["MaNPL"].ToString().Trim().ToUpper() == dtTemp.Columns[k].ColumnName.Trim().ToUpper())
                                {
                                    dr[k] = Convert.ToDecimal(row["LuongPhanBo"]);
                                    continue;
                                }
                            }

                        }
                        dtTemp.Rows.Add(dr);
                    }
                }
                dsSTK.Tables.Add(dtTemp);//Add bảng dtTemp vào Dataset dsSTK
            }

            return dsSTK;
        }


        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
            }   
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.BK04.setText(this.Label, txtName.Text);
            this.BK04.CreateDocument();
        }
   
    }
}