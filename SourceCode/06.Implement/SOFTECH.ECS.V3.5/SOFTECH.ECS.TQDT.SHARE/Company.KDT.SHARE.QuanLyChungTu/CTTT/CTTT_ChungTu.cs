﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu.CTTT
{
    public partial class CTTT_ChungTu
    {
        public List<CTTT_ChungTuChiTiet> ChungTuChiTietCollections = new List<CTTT_ChungTuChiTiet>();
        public List<CTTT_ChiPhiKhac> ChiPhiKhacCollections = new List<CTTT_ChiPhiKhac>();
        public void LoadChungTuChiTiet()
        {
            ChungTuChiTietCollections = (List<CTTT_ChungTuChiTiet>)CTTT_ChungTuChiTiet.SelectCollectionBy_IDCT(ID);
        }
        public void LoadChiPhiKhac()
        {
            ChiPhiKhacCollections = (List<CTTT_ChiPhiKhac>)CTTT_ChiPhiKhac.SelectCollectionBy_IDCT(this.ID);
        }

        public DataTable LoadChungTuChiTietTable()
        {

            string spSql = @"SELECT CASE WHEN a.MaLoaiHinh like '%V%' then (Select top 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai  where SoTK = a.SoToKhai) Else a.SoToKhai End as SoToKhaiVNACCS,
        a.SoToKhai,a.MaLoaiHinh,a.NgayDangKy,a.MaHaiQuan,
		CASE
		WHEN a.TriGia IS NULL THEN 0
		else b.TongTriGia - 
			Case 
				when c.TriGiaDaPhanBo IS NULL THEN 0
				ELSE c.TriGiaDaPhanBo 
				END 
				 - a.TriGia 
		END AS GiaTriConLai,
		a.TriGia
		 FROM 
(SELECT 
      [SoToKhai]
      ,[MaLoaiHinh]
      ,[NgayDangKy]
      ,[TriGia]
      ,[GhiChu]
      ,[MaHaiQuan]
  FROM [t_CTTT_ChungTuChiTiet] 
WHERE IDCT = @IDCT) a
INNER JOIN 
(SELECT SUM(hmd.TriGiaKB) AS TongTriGia,tkmd.SoToKhai,tkmd.MaLoaiHinh,tkmd.NgayDangKy,tkmd.MaHaiQuan
   from t_SXXK_ToKhaiMauDich tkmd INNER JOIN t_SXXK_HangMauDich hmd 
ON hmd.SoToKhai = tkmd.SoToKhai and hmd.MaHaiQuan = tkmd.MaHaiQuan AND hmd.MaLoaiHinh = tkmd.MaLoaiHinh AND hmd.NamDangKy = tkmd.NamDangKy 
 GROUP BY tkmd.SoToKhai,tkmd.MaLoaiHinh,tkmd.NamDangKy,tkmd.MaHaiQuan,tkmd.NgayDangKy ) b
ON a.SotoKhai = b.soToKhai AND b.MaLoaiHinh = a.MaLoaiHinh AND year(b.NgayDangKy) = year(a.NgayDangKy)
LEFT JOIN 
(SELECT Sum(TriGia) AS TriGiaDaPhanBo,SotoKhai,MaLoaiHinh, MAX(t_CTTT_ChungTuChiTiet.NgayDangKy) AS NgayDangKy,MAX(t_CTTT_ChungTuChiTiet.IDChiTiet) AS idctct
FROM t_CTTT_ChungTuChiTiet WHERE IDCT NOT IN (@IDCT) GROUP BY SoToKhai,MaLoaiHinh,year(NgayDangKy)) as c
ON a.SoToKhai = c.SoToKhai AND a.MaLoaiHinh = c.MaLoaiHinh AND year(a.NgayDangKy) =  year(c.NgayDangKy)
 ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbcommand = (SqlCommand)db.GetSqlStringCommand(spSql);
            db.AddInParameter(dbcommand, "@IDCT", SqlDbType.BigInt, this.ID);

            return db.ExecuteDataSet(dbcommand).Tables[0];

        }

    }
    public class enumLoaiChungTuTT
    {
        public static readonly string Nhap = "N";
        public static readonly string Xuat = "X";
    }
}
