﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using System.Xml.Serialization;
using System.IO;
using Company.Interface.VNACCS;
using Company.Interface.SXXK;
#if GC_V4
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.Interface.Report;


#endif
#if SXXK_V4
using Company.BLL.KDT;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.Interface.SXXK;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.Interface.Report;

#endif
#if KD_V4
using Company.KD.BLL.KDT;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.BLL.VNACCS;
using Company.Interface.Report;
#endif
namespace Company.Interface
{
    public partial class VNACC_ToKhaiManager : BaseForm
    {
        public List<KDT_VNACC_ToKhaiMauDich> ListTKMD = new List<KDT_VNACC_ToKhaiMauDich>();
        DataSet ds = new DataSet();
        DataSet dsAMA = new DataSet();
        DataSet ds_Done = new DataSet();
        public bool IsShowChonToKhai = false;
        public string whereCondition;
        public string LoaiChungTu;
        public string LoaiHang;
        public KDT_VNACC_ToKhaiMauDich TKMDDuocChon = null;
#if GC_V4
        public HopDong hd = new HopDong();
        public bool isPhieuXuat = false;
#endif

        public VNACC_ToKhaiManager()
        {
            InitializeComponent();

            this.Text = "Theo dõi tờ khai";
        }
        private void VNACC_ToKhaiManager_Load(object sender, EventArgs e)
        {
            this.AcceptButton = this.btnTimKiem;
            cbbPhanLuong.SelectedValue = 0;
            cbbTrangThai.SelectedValue = 0;
            ctrMaHaiQuan.CategoryType = ECategory.A038;
            ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            txtNamDangKy.Text = DateTime.Today.Year.ToString();
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;

            btnCapNhatThanhKhoan.Visible = false;
#if SXXK_V4
            btnCapNhatThanhKhoan.Visible = true;
            grList.RootTable.Columns["HopDong_So"].Visible = false;
#elif GC_V4
            btnCapNhatThanhKhoan.Visible = true;
            grList.RootTable.Columns["HopDong_So"].Visible = true;
            if (hd.ID != 0) 
            {
                txtNamDangKy.Text = hd.NamTN.ToString();
                cbbTrangThai.SelectedValue = 3;
            }
#elif KD_V4
            grList.RootTable.Columns["HopDong_So"].Visible = false;
#endif
        }
        private void LoadData(string where)
        {
#if GC_V4
            if (hd.ID != 0)
            {
                where += " AND HopDong_ID = " + hd.ID;
                if(!String.IsNullOrEmpty(whereCondition))
                {
                    if (!String.IsNullOrEmpty(LoaiHang) && !String.IsNullOrEmpty(LoaiChungTu))
                    {
                        where += whereCondition;
                        where += " AND SoToKhai NOT IN (SELECT SOTK  FROM dbo.T_KHOKETOAN_PHIEUXNKHO WHERE HOPDONG_ID =" + hd.ID + " AND LOAICHUNGTU='" + LoaiChungTu + "' AND LOAIHANGHOA='" + LoaiHang + "') ";
                    }
                }
                //if (isPhieuXuat)
                //    where += " AND MaLoaiHinh IN ('E52','E54','E56')";
                //else
                //{
                //    where += " AND SoToKhai NOT IN (SELECT SOTK  FROM dbo.T_KHOKETOAN_PHIEUXNKHO WHERE HOPDONG_ID =" + hd.ID + ") ";
                //}
            }
            else
            {
                if (!String.IsNullOrEmpty(whereCondition))
                {
                    if (!String.IsNullOrEmpty(LoaiHang) && String.IsNullOrEmpty(LoaiChungTu))
                    {
                        where += whereCondition;
                        where += " AND SoToKhai NOT IN (SELECT SOTK  FROM dbo.T_KHOKETOAN_PHIEUXNKHO WHERE LOAICHUNGTU='" + LoaiChungTu + "' AND LOAIHANGHOA='" + LoaiHang + "') ";
                    }
                }
            }
                
#endif
            ds = VNACC_Category_Common.SelectDynamic("ReferenceDB in ('E001','E002')", null);
            ListTKMD = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic(where, null);
            grList.DropDowns["drdMaLoaiHinh"].DataSource = ds.Tables[0];
            grList.DataSource = ListTKMD;


        }
        private KDT_VNACC_ToKhaiMauDich getTKMDID(long id)
        {
            ListTKMD = KDT_VNACC_ToKhaiMauDich.SelectCollectionAll();
            foreach (KDT_VNACC_ToKhaiMauDich TKMD in ListTKMD)
            {
                if (TKMD.ID == id)
                {
                    string where = "TKMD_ID=" + TKMD.ID;
                    //Hang mau dich Collection
                    List<KDT_VNACC_HangMauDich> listHang = new List<KDT_VNACC_HangMauDich>();
                    listHang = KDT_VNACC_HangMauDich.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_HangMauDich hang in listHang)
                    {
                        List<KDT_VNACC_HangMauDich_ThueThuKhac> listThueThuKhac = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
                        listThueThuKhac = KDT_VNACC_HangMauDich_ThueThuKhac.SelectCollectionDynamic("Master_id=" + hang.ID, "ID");
                        foreach (KDT_VNACC_HangMauDich_ThueThuKhac thue in listThueThuKhac)
                        {
                            hang.ThueThuKhacCollection.Add(thue);
                        }
                        TKMD.HangCollection.Add(hang);
                    }
                    // giay phep Collection
                    List<KDT_VNACC_TK_GiayPhep> listGiayPhep = new List<KDT_VNACC_TK_GiayPhep>();
                    listGiayPhep = KDT_VNACC_TK_GiayPhep.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_GiayPhep item in listGiayPhep)
                    {
                        TKMD.GiayPhepCollection.Add(item);
                    }
                    // so dien tu dinh kem
                    List<KDT_VNACC_TK_DinhKemDienTu> listDinhKiem = new List<KDT_VNACC_TK_DinhKemDienTu>();
                    listDinhKiem = KDT_VNACC_TK_DinhKemDienTu.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_DinhKemDienTu item in listDinhKiem)
                    {
                        TKMD.DinhKemCollection.Add(item);
                    }
                    // trung chuyển collection KDT_VNACC_TK_TrungChuyen
                    List<KDT_VNACC_TK_TrungChuyen> listTC = new List<KDT_VNACC_TK_TrungChuyen>();
                    listTC = KDT_VNACC_TK_TrungChuyen.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_TrungChuyen item in listTC)
                    {
                        TKMD.TrungChuyenCollection.Add(item);
                    }
                    //Chi thi Hai quan collection
                    List<KDT_VNACC_ChiThiHaiQuan> listChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
                    string whereChiThiHQ = string.Format("Master_ID = {0} and LoaiThongTin = 'TK'", TKMD.ID);
                    listChiThi = KDT_VNACC_ChiThiHaiQuan.SelectCollectionDynamic(whereChiThiHQ, "");
                    foreach (KDT_VNACC_ChiThiHaiQuan item in listChiThi)
                    {
                        TKMD.ChiThiHQCollection.Add(item);
                    }
                    // Van don Collection
                    List<KDT_VNACC_TK_SoVanDon> listVanDon = new List<KDT_VNACC_TK_SoVanDon>();
                    listVanDon = KDT_VNACC_TK_SoVanDon.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_SoVanDon item in listVanDon)
                    {
                        TKMD.VanDonCollection.Add(item);
                    }
                    //Tri gia Khoan Dieu Chinh
                    List<KDT_VNACC_TK_KhoanDieuChinh> listDieuChinh = new List<KDT_VNACC_TK_KhoanDieuChinh>();
                    listDieuChinh = KDT_VNACC_TK_KhoanDieuChinh.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_KhoanDieuChinh item in listDieuChinh)
                    {
                        TKMD.KhoanDCCollection.Add(item);
                    }

                    List<KDT_VNACC_TK_Container> listContainer = new List<KDT_VNACC_TK_Container>();
                    listContainer = KDT_VNACC_TK_Container.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_Container item in listContainer)
                    {
                        List<KDT_VNACC_TK_SoContainer> listSoCont = new List<KDT_VNACC_TK_SoContainer>();
                        listSoCont = KDT_VNACC_TK_SoContainer.SelectCollectionDynamic("Master_id=" + item.ID, "ID");
                        foreach (KDT_VNACC_TK_SoContainer soCont in listSoCont)
                        {
                            item.SoContainerCollection.Add(soCont);
                        }
                        TKMD.ContainerTKMD = item;
                    }
                    List<KDT_VNACC_TK_SoContainer> listSoContainer = new List<KDT_VNACC_TK_SoContainer>();
                    listSoContainer = KDT_VNACC_TK_SoContainer.SelectCollectionDynamic("Master_ID =" + TKMD.ID, "ID");

                    List<KDT_VNACC_TK_PhanHoi_SacThue> listSacThue = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
                    listSacThue = KDT_VNACC_TK_PhanHoi_SacThue.SelectCollectionDynamic("Master_ID =" + TKMD.ID, "ID");
                    foreach (KDT_VNACC_TK_PhanHoi_SacThue item in listSacThue)
                    {
                        TKMD.SacThueCollection.Add(item);
                    }
                    List<KDT_VNACC_TK_PhanHoi_TyGia> listTyGia = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                    listTyGia = KDT_VNACC_TK_PhanHoi_TyGia.SelectCollectionDynamic("Master_ID =" + TKMD.ID, "ID");
                    foreach (KDT_VNACC_TK_PhanHoi_TyGia item in listTyGia)
                    {
                        TKMD.TyGiaCollection.Add(item);
                    }
                    return TKMD;
                }
            }
            return null;
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                try
                {
                    Cursor = Cursors.WaitCursor;

                    int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                    KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(id);
                    //TKMD = TKMD.LoadToKhai(id);
                    TKMD.LoadFull();
                    if (IsShowChonToKhai)
                    {
                        TKMDDuocChon = TKMD;
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                        return;
                    }
                    string loaiHinh = "";
                    DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + TKMD.MaLoaiHinh + "'", "ID");
                    DataRow dr = ds.Tables[0].Rows[0];
                    loaiHinh = dr["ReferenceDB"].ToString();

                    if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E002.ToString())
                    {
                        VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
                        f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                        f.TKMD = TKMD;
                        f.ShowDialog();
                        btnTimKiem_Click(null, null);
                    }
                    if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E001.ToString())
                    {
                        VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
                        f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                        f.TKMD = TKMD;
                        f.ShowDialog();
                        btnTimKiem_Click(null, null);
                    }
                    btnTimKiem_Click(null, null);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
                }
                finally { Cursor = Cursors.Default; }
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {

                ListTKMD.Clear();

                LoadData(GetSeachWhere());

                grList.DataSource = ListTKMD;
                grList.Refresh();
                grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.True;
                grList.RootTable.Columns["SoToKhai"].EditType = EditType.TextBox;


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                this.Cursor = Cursors.Default;
            }

        }

        private void cbbTrangThai_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
            if (!GlobalSettings.IsDelete)
            {
                btnXoa.Enabled = false;
                btnXoaTKThuNghiem.Enabled = false;
            }
            else
            {
                btnXoa.Enabled = true;
                btnXoaTKThuNghiem.Enabled = true;
            }
            if (cbbTrangThai.SelectedValue == null)
                btnHuyToKhai.Visible = false;
            else
            {
                if (cbbTrangThai.SelectedValue.ToString() == EnumTrangThaiXuLy.KhaiBaoChinhThuc || cbbTrangThai.SelectedValue.ToString() == EnumTrangThaiXuLy.ThongQuan || cbbTrangThai.SelectedValue.ToString() == EnumTrangThaiXuLy.KhaiBaoSua || cbbTrangThai.SelectedValue.ToString() == EnumTrangThaiXuLy.Huy)
                {
                    btnHuyToKhai.Visible = true;
                    btnXoa.Enabled = false;
                    btnXoaTKThuNghiem.Enabled = false;
                }
                else
                {
                    btnHuyToKhai.Visible = false;
                }

            }


        }
        private void CopyToKhai(KDT_VNACC_ToKhaiMauDich tkmd, bool isCopyHangHoa)
        {
            KDT_VNACC_ToKhaiMauDich tkmdcopy = new KDT_VNACC_ToKhaiMauDich();
            HelperVNACCS.UpdateObject<KDT_VNACC_ToKhaiMauDich>(tkmdcopy, tkmd, false);
            tkmdcopy.NgayDangKy = new DateTime(1900, 1, 1);
            tkmdcopy.SoToKhai = 0;
            tkmdcopy.SoNhanhToKhai = 0;
            tkmdcopy.SoToKhaiDauTien = "";
            tkmdcopy.MaPhanLoaiKiemTra = string.Empty;
            tkmdcopy.AnHangCollection = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
            tkmdcopy.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
            tkmdcopy.SacThueCollection = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
            tkmdcopy.TrangThaiXuLy = "0";
            tkmdcopy.TyGiaCollection = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
            tkmdcopy.InputMessageID = string.Empty;
            tkmdcopy.MessageTag = string.Empty;
            tkmdcopy.IndexTag = string.Empty;

            if (!isCopyHangHoa)
            {
                tkmdcopy.HangCollection = new List<KDT_VNACC_HangMauDich>();
            }
            else
            {
                tkmdcopy.HangCollection = tkmd.HangCollection;
            }

            DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + tkmdcopy.MaLoaiHinh + "'", "ID");
            DataRow dr = ds.Tables[0].Rows[0];
            string loaiHinh = dr["ReferenceDB"].ToString();
            if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E002.ToString())
            {
                VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
                //f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                f.isCopy = true;
                f.TKMD = tkmdcopy;
                f.ShowDialog();
            }
            if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E001.ToString())
            {
                VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
                //f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                f.IsCopy = true;
                f.TKMD = tkmdcopy;
                f.ShowDialog();
            }
            btnTimKiem_Click(null, null);
        }

        private void copyNộiDungTờKhaiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (grList.SelectedItems[0].RowType == RowType.Record)
            {
                try
                {
                    int id = Convert.ToInt32(grList.SelectedItems[0].GetRow().Cells["ID"].Value);
                    KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(id);
                    TKMD.LoadFull();
                    //TKMD = TKMD.LoadToKhai(id);
                    CopyToKhai(TKMD, false);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage(ex.Message, false);
                }
            }
        }

        private void copyNộiDungTờKhaiHàngHóaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (grList.SelectedItems[0].RowType == RowType.Record)
            {
                try
                {
                    int id = Convert.ToInt32(grList.SelectedItems[0].GetRow().Cells["ID"].Value);
                    KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(id);
                    /*TKMD = TKMD.LoadToKhai(id);*/
                    TKMD.LoadFull();
                    CopyToKhai(TKMD, true);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage(ex.Message, false);
                }
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            if (grList.CurrentRow.RowType == RowType.Record)
            {
                int id = System.Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                int TrangThai = System.Convert.ToInt32(grList.CurrentRow.Cells["TrangThaiXuLy"].Value.ToString());
                if ((TrangThai == 0) || (TrangThai == 1))
                {
                    if (ShowMessage("Bạn có muốn xóa tờ khai này không?", true) == "Yes")
                    {

                        KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                        TKMD.DeleteFull(id);
                        ShowMessage("Xóa tờ khai thành công.", false);
                    }
                }
                else
                    ShowMessage("Không thể xóa tờ khai đã được xác nhận với Hải quan. ", false);

            }
            btnTimKiem_Click(null, null);
        }

        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }

        private void grList_LoadingRow(object sender, RowLoadEventArgs e)
        {

            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["MaPhanLoaiKiemTra"].Value != null)
                {
                    string pl = e.Row.Cells["MaPhanLoaiKiemTra"].Value.ToString();
                    if (pl == "1")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Xanh";
                    else if (pl == "2")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Vàng";
                    else if (pl == "3")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Đỏ";
                    else
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Chưa phân luồng";
                }
                if (e.Row.Cells["NgayPhatHanhHD"].Value != null)
                {
                    DateTime ngayHD = System.Convert.ToDateTime(e.Row.Cells["NgayPhatHanhHD"].Value.ToString());
                    if (ngayHD.Year == 1900)
                        e.Row.Cells["NgayPhatHanhHD"].Text = "";
                }
                if (e.Row.Cells["NgayDangKy"].Value != null)
                {
                    DateTime ngaydk = System.Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value.ToString());
                    if (ngaydk.Year == 1900)
                        e.Row.Cells["NgayDangKy"].Text = "";
                }
                if (grList.RootTable.Columns.Contains("MaDiaDiemDoHang"))
                {
                    if (!string.IsNullOrEmpty(e.Row.Cells["MaDiaDiemDoHang"].Text))
                    {
                        if (e.Row.Cells["MaDiaDiemDoHang"].Text.Trim().Length == 5)
                            e.Row.Cells["MaDiaDiemDoHang"].Text = e.Row.Cells["MaDiaDiemDoHang"].Text.Substring(0, 2);
                        else
                            e.Row.Cells["MaDiaDiemDoHang"].Text = "VN";
                    }
                    else
                        e.Row.Cells["MaDiaDiemDoHang"].Text = "VN";
                }
                if (grList.RootTable.Columns.Contains("MaDiaDiemXepHang"))
                {
                    if (!string.IsNullOrEmpty(e.Row.Cells["MaDiaDiemXepHang"].Text))
                    {
                        if (e.Row.Cells["MaDiaDiemXepHang"].Text.Trim().Length == 5)
                            e.Row.Cells["MaDiaDiemXepHang"].Text = e.Row.Cells["MaDiaDiemXepHang"].Text.Substring(0, 2);
                        else
                            e.Row.Cells["MaDiaDiemXepHang"].Text = "VN";
                    }
                    else
                        e.Row.Cells["MaDiaDiemXepHang"].Text = "VN";
                }
#if GC_V4
                if (e.Row.Cells["HopDong_So"].Value != null)
                {
                    //HopDong hd = HopDong.Load(TKMD.HopDong_ID);
                    try
                    {
                    int HD = 0;
                    HD = Convert.ToInt32(e.Row.Cells["HopDong_So"].Value.ToString());
                    HopDong hd = HopDong.Load(HD);
                        e.Row.Cells["HopDong_So"].Text = hd.SoHopDong;
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    
                }
#endif
            }



        }

        private void btnXoaTKThuNghiem_Click(object sender, EventArgs e)
        {
            VNACC_ToKhaiDelete frm = new VNACC_ToKhaiDelete();
            frm.Show();
        }

        private void btnCapNhatThanhKhoan_Click(object sender, EventArgs e)
        {
#if SXXK_V4 || GC_V4
            Company.Interface.VNACCS.VNACCS_ChuyenThanhKhoan frm = new Company.Interface.VNACCS.VNACCS_ChuyenThanhKhoan();
            frm.NamDangKy = txtNamDangKy.Text;
            frm.ShowDialog(this);
#endif
        }

        private void hủyTờKhaiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (grList.SelectedItems[0].RowType == RowType.Record)
            {
                try
                {
                    if (ShowMessage("Bạn có chắc chắn muốn hủy tờ khai này ", true) == "Yes")
                    {
                        int id = Convert.ToInt32(grList.SelectedItems[0].GetRow().Cells["ID"].Value);
                        KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(id);
                        /*TKMD = TKMD.LoadToKhai(id);*/
                        TKMD.LoadFull();
                        string result = string.Empty;
#if SXXK_V4
                        result = Company.BLL.VNACCS.ConvertFromVNACCS.HuyToKhai(TKMD.SoToKhai, GlobalSettings.MA_HAI_QUAN);
#elif GC_V4
                        result = Company.GC.BLL.VNACC.ConvertFromVNACCS.HuyToKhai(TKMD.SoToKhai, TKMD.MaLoaiHinh);
#endif
                        if (string.IsNullOrEmpty(result))
                        {
                            TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.Huy;
                            TKMD.InsertUpdateFull();
                            ShowMessage("Huỷ thành công \r\n", false);
                        }
                        else
                            ShowMessage("Không hủy được tờ khai \r\n" + result, false);
                    }

                    btnTimKiem_Click(null, null);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage(ex.Message, false);
                }
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Bảng kê tờ khai_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                grList.Tables[0].Columns.Add(new GridEXColumn("MaDiaDiemDoHang", ColumnType.Text, EditType.NoEdit) { Caption = "Nước nhập" });
                grList.Tables[0].Columns.Add(new GridEXColumn("MaDiaDiemXepHang", ColumnType.Text, EditType.NoEdit) { Caption = "Nước xuất" });
                if (ShowMessage("Bạn có muốn xuất kèm theo thông tin hàng hóa của tờ khai không ? ", true) == "No")
                {
                    if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
                    {
                        grList.Tables[0].Columns["SoLuong"].Visible = true;
                        grList.Tables[0].Columns["TrongLuong"].Visible = true;
                        grList.Tables[0].Columns["TongTriGiaHD"].Visible = true;
                        grList.Tables[0].Columns["NgayPhatHanhHD"].Visible = true;
                        grList.Tables[0].Columns["GhiChu"].Visible = true;
                        grList.Tables[0].Columns["TenPTVC"].Visible = true;
                        grList.Tables[0].Columns["NgayHangDen"].Visible = true;
                        grList.Tables[0].Columns["TenDiaDiemDoHang"].Visible = true;
                        grList.Tables[0].Columns["TenDiaDiemXepHang"].Visible = true;

                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = grList;
                        try
                        {
                            System.IO.Stream str = sfNPL.OpenFile();
                            gridEXExporter1.Export(str);
                            str.Close();
                            if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                            {
                                System.Diagnostics.Process.Start(sfNPL.FileName);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi mở file " + ex.Message, false);

                        }
                        grList.Tables[0].Columns["SoLuong"].Visible = false;
                        grList.Tables[0].Columns["TrongLuong"].Visible = false;
                        grList.Tables[0].Columns["TongTriGiaHD"].Visible = false;
                        grList.Tables[0].Columns["NgayPhatHanhHD"].Visible = false;
                        grList.Tables[0].Columns["GhiChu"].Visible = false;
                        grList.Tables[0].Columns["TenPTVC"].Visible = false;
                        grList.Tables[0].Columns["NgayHangDen"].Visible = false;
                        grList.Tables[0].Columns["TenDiaDiemDoHang"].Visible = false;
                        grList.Tables[0].Columns["TenDiaDiemXepHang"].Visible = false;
                    }
                }
                else
                {
                    if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
                    {
                        grList.Tables[0].Columns["SoLuong"].Visible = true;
                        grList.Tables[0].Columns["TrongLuong"].Visible = true;
                        grList.Tables[0].Columns["TongTriGiaHD"].Visible = true;
                        grList.Tables[0].Columns["NgayPhatHanhHD"].Visible = true;
                        grList.Tables[0].Columns["GhiChu"].Visible = true;
                        grList.Tables[0].Columns["TenPTVC"].Visible = true;
                        grList.Tables[0].Columns["NgayHangDen"].Visible = true;
                        grList.Tables[0].Columns["TenDiaDiemDoHang"].Visible = true;
                        grList.Tables[0].Columns["TenDiaDiemXepHang"].Visible = true;
                        grList.Tables[0].Columns.Add(new GridEXColumn("TriGiaTinhThue", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá tính thuế" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaPhanLoaiThueSuatThue", ColumnType.Text, EditType.NoEdit) { Caption = "Mã phân loại thuế suất thuế" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("ThueSuatThue", ColumnType.Text, EditType.NoEdit) { Caption = "Thuế suất thuế" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("PhanLoaiThueSuatThue", ColumnType.Text, EditType.NoEdit) { Caption = "Phân loại thuế suất thuế" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TongTienThuePhaiNop", ColumnType.Text, EditType.NoEdit) { Caption = "Tổng tiền thuế phải nộp" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaHangHoa", ColumnType.Text, EditType.NoEdit) { Caption = "Mã hàng" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TenHang", ColumnType.Text, EditType.NoEdit) { Caption = "Tên hàng" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaSoHang", ColumnType.Text, EditType.NoEdit) { Caption = "Mã HS" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("SoLuong1", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng hàng" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("DVTLuong1", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT hàng" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("DonGiaHoaDon", ColumnType.Text, EditType.NoEdit) { Caption = "Đơn giá hàng" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaTTDonGia", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT đơn giá" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TriGiaHoaDon", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá hàng" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("NuocXuatXu", ColumnType.Text, EditType.NoEdit) { Caption = "Xuất xứ" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaBieuThueNK", ColumnType.Text, EditType.NoEdit) { Caption = "Mã biểu thuế NK" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaMienGiamThue", ColumnType.Text, EditType.NoEdit) { Caption = "Mã miễn giảm thuế" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("DieuKhoanMienGiam", ColumnType.Text, EditType.NoEdit) { Caption = "Điều khoản miễn giảm" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("DonGiaTinhThue", ColumnType.Text, EditType.NoEdit) { Caption = "Đơn giá tính thuế" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TriGiaTinhThueS", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá tính thuế" });
                        if (GlobalSettings.MA_DON_VI != "4000395355")
                        {
                            grList.Tables[0].Columns.Add(new GridEXColumn("CoQuanHaiQuan", ColumnType.Text, EditType.NoEdit) { Caption = "Mã HQ" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("TenCoQuanHaiQuan", ColumnType.Text, EditType.NoEdit) { Caption = "Tên cơ quan HQ" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("MaTSThueThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Mã thuế suất thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("MaMGThueThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Mã miễn giảm thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("SoTienGiamThueThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Số tiền miễn giảm thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("TenKhoanMucThueVaThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Tên khoản mục thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("TriGiaTinhThueVaThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá tính thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("ThueSuatThueVaThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Thuế suất thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("SoTienThueVaThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Số tiền thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("DieuKhoanMienGiamThueVaThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Điều khoản miễn giảm thuế và thu khác" });
                        }
                        else
                        {
                            grList.Tables[0].Columns["CoQuanHaiQuan"].Visible = false;
                            //grList.Tables[0].Columns["Check"].Visible = false;
                            //grList.Tables[0].Columns["SoHopDong"].Visible = false;
#if KD_V4
                            grList.Tables[0].Columns.Add(new GridEXColumn("MaTSThueThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Mã thuế suất thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("MaMGThueThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Mã miễn giảm thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("SoTienGiamThueThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Số tiền miễn giảm thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("TenKhoanMucThueVaThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Tên khoản mục thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("TriGiaTinhThueVaThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá tính thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("ThueSuatThueVaThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Thuế suất thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("SoTienThueVaThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Số tiền thuế và thu khác" });
                            grList.Tables[0].Columns.Add(new GridEXColumn("DieuKhoanMienGiamThueVaThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Điều khoản miễn giảm thuế và thu khác" });
#endif
                        }
                        grList.Tables[0].Columns.Add(new GridEXColumn("SoTienThue", ColumnType.Text, EditType.NoEdit) { Caption = "Số tiền thuế" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaTTTyGiaTinhThue", ColumnType.Text, EditType.NoEdit) { Caption = "Mã tỷ giá tính thuế" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TyGiaTinhThue", ColumnType.Text, EditType.NoEdit) { Caption = "Tỷ giá tính thuế" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("SoHieuKyHieu", ColumnType.Text, EditType.NoEdit) { Caption = "Số hiệu và ký hiệu" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaDieuKienGiaHD", ColumnType.Text, EditType.NoEdit) { Caption = "Điều kiện giá" });

                        grList.Tables[0].Columns.Add(new GridEXColumn("MaPhanLoaiPhiVC", ColumnType.Text, EditType.NoEdit) { Caption = "Mã phân loại phí vận chuyển" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("PhiVanChuyen", ColumnType.Text, EditType.NoEdit) { Caption = "Phí vận chuyển" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaTTPhiVC", ColumnType.Text, EditType.NoEdit) { Caption = "Mã tiền tệ phí VC" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaPhanLoaiPhiBH", ColumnType.Text, EditType.NoEdit) { Caption = "Mã phân loại phí bảo hiểm" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("PhiBaoHiem", ColumnType.Text, EditType.NoEdit) { Caption = "Phí bảo hiểm" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaTTPhiBH", ColumnType.Text, EditType.NoEdit) { Caption = "Mã tiền tệ phí BH" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("ChiTietKhaiTriGia", ColumnType.Text, EditType.NoEdit) { Caption = "Chi tiết khai trị giá" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TongHeSoPhanBoTG", ColumnType.Text, EditType.NoEdit) { Caption = "Tổng hệ số phân bổ trị giá" });

                        grList.Tables[0].Columns.Add(new GridEXColumn("MaTenDieuChinh", ColumnType.Text, EditType.NoEdit) { Caption = "Mã tên khoản điều chỉnh" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaPhanLoaiDieuChinh", ColumnType.Text, EditType.NoEdit) { Caption = "Mã phân loại điều chỉnh" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaTTDieuChinhTriGia", ColumnType.Text, EditType.NoEdit) { Caption = "Mã tiền tệ điều chỉnh" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TriGiaKhoanDieuChinh", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá khoản điều chỉnh" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TongHeSoPhanBo", ColumnType.Text, EditType.NoEdit) { Caption = "Tổng hệ số phân bổ" });

                        grList.Tables[0].Columns.Add(new GridEXColumn("MaTTTyGiaTinhThue", ColumnType.Text, EditType.NoEdit) { Caption = "Mã tiền tệ TGTT" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TyGiaTinhThue", ColumnType.Text, EditType.NoEdit) { Caption = "Tỷ giá tính thuế" });
                        grList.DataSource = KDT_VNACC_ToKhaiMauDich.SelectCollectionAndHMD_ExportExcel(GetSeachWhere(), "");
                        grList.Refetch();

                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = grList;
                        try
                        {
                            System.IO.Stream str = sfNPL.OpenFile();
                            gridEXExporter1.Export(str);
                            str.Close();
                            if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                            {
                                System.Diagnostics.Process.Start(sfNPL.FileName);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi mở file " + ex.Message, false);

                        }

                        grList.Tables[0].Columns.Remove("MaHangHoa");
                        grList.Tables[0].Columns.Remove("TenHang");
                        grList.Tables[0].Columns.Remove("MaSoHang");
                        grList.Tables[0].Columns.Remove("SoLuong1");
                        grList.Tables[0].Columns.Remove("DVTLuong1");
                        grList.Tables[0].Columns.Remove("DonGiaHoaDon");
                        grList.Tables[0].Columns.Remove("TriGiaHoaDon");
                        grList.Tables[0].Columns.Remove("NuocXuatXu");
                        grList.Tables[0].Columns.Remove("MaPhanLoaiThueSuatThue");
                        grList.Tables[0].Columns.Remove("ThueSuatThue");
                        grList.Tables[0].Columns.Remove("PhanLoaiThueSuatThue");
                        grList.Tables[0].Columns.Remove("TriGiaTinhThue");
                        grList.Tables[0].Columns.Remove("TongTienThuePhaiNop");

                        grList.Tables[0].Columns.Remove("MaTTDonGia");
                        grList.Tables[0].Columns.Remove("MaBieuThueNK");
                        grList.Tables[0].Columns.Remove("MaMienGiamThue");
                        grList.Tables[0].Columns.Remove("DieuKhoanMienGiam");
                        grList.Tables[0].Columns.Remove("TriGiaTinhThueS");
                        grList.Tables[0].Columns.Remove("DonGiaTinhThue");
                        if (GlobalSettings.MA_DON_VI != "4000395355")
                        {
                            grList.Tables[0].Columns.Remove("CoQuanHaiQuan");
                            grList.Tables[0].Columns.Remove("TenCoQuanHaiQuan");
                            grList.Tables[0].Columns.Remove("MaTSThueThuKhac");
                            grList.Tables[0].Columns.Remove("MaMGThueThuKhac");
                            grList.Tables[0].Columns.Remove("SoTienGiamThueThuKhac");
                            grList.Tables[0].Columns.Remove("TenKhoanMucThueVaThuKhac");
                            grList.Tables[0].Columns.Remove("TriGiaTinhThueVaThuKhac");
                            grList.Tables[0].Columns.Remove("ThueSuatThueVaThuKhac");
                            grList.Tables[0].Columns.Remove("SoTienThueVaThuKhac");
                            grList.Tables[0].Columns.Remove("DieuKhoanMienGiamThueVaThuKhac");
                        }
                        else
                        {
                            grList.Tables[0].Columns["CoQuanHaiQuan"].Visible = true;
                            //grList.Tables[0].Columns["SoHopDong"].Visible = true;
                            //grList.Tables[0].Columns["Check"].Visible = true;
#if KD_V4
                            grList.Tables[0].Columns.Remove("MaTSThueThuKhac");
                            grList.Tables[0].Columns.Remove("MaMGThueThuKhac");
                            grList.Tables[0].Columns.Remove("SoTienGiamThueThuKhac");
                            grList.Tables[0].Columns.Remove("TenKhoanMucThueVaThuKhac");
                            grList.Tables[0].Columns.Remove("TriGiaTinhThueVaThuKhac");
                            grList.Tables[0].Columns.Remove("ThueSuatThueVaThuKhac");
                            grList.Tables[0].Columns.Remove("SoTienThueVaThuKhac");
                            grList.Tables[0].Columns.Remove("DieuKhoanMienGiamThueVaThuKhac");
#endif
                        }
                        grList.Tables[0].Columns.Remove("MaTTDonGia");
                        grList.Tables[0].Columns.Remove("SoTienThue");
                        grList.Tables[0].Columns.Remove("MaTTTyGiaTinhThue");
                        grList.Tables[0].Columns.Remove("TyGiaTinhThue");
                        grList.Tables[0].Columns.Remove("SoHieuKyHieu");
                        grList.Tables[0].Columns.Remove("MaDieuKienGiaHD");
                        grList.Tables[0].Columns["SoLuong"].Visible = false;
                        grList.Tables[0].Columns["TrongLuong"].Visible = false;
                        grList.Tables[0].Columns["TongTriGiaHD"].Visible = false;
                        grList.Tables[0].Columns["NgayPhatHanhHD"].Visible = false;
                        grList.Tables[0].Columns["GhiChu"].Visible = false;
                        grList.Tables[0].Columns["TenPTVC"].Visible = false;
                        grList.Tables[0].Columns["NgayHangDen"].Visible = false;
                        grList.Tables[0].Columns["TenDiaDiemDoHang"].Visible = false;
                        grList.Tables[0].Columns["TenDiaDiemXepHang"].Visible = false;

                        grList.Tables[0].Columns.Remove("MaPhanLoaiPhiVC");
                        grList.Tables[0].Columns.Remove("PhiVanChuyen");
                        grList.Tables[0].Columns.Remove("MaTTPhiVC");
                        grList.Tables[0].Columns.Remove("MaPhanLoaiPhiBH");
                        grList.Tables[0].Columns.Remove("PhiBaoHiem");
                        grList.Tables[0].Columns.Remove("MaTTPhiBH");
                        grList.Tables[0].Columns.Remove("ChiTietKhaiTriGia");
                        grList.Tables[0].Columns.Remove("TongHeSoPhanBoTG");

                        grList.Tables[0].Columns.Remove("MaTenDieuChinh");
                        grList.Tables[0].Columns.Remove("MaPhanLoaiDieuChinh");
                        grList.Tables[0].Columns.Remove("MaTTDieuChinhTriGia");
                        grList.Tables[0].Columns.Remove("TriGiaKhoanDieuChinh");
                        grList.Tables[0].Columns.Remove("TongHeSoPhanBo");

                        grList.Tables[0].Columns.Remove("MaTTTyGiaTinhThue");
                        grList.Tables[0].Columns.Remove("TyGiaTinhThue");
                    }
                }
                grList.Tables[0].Columns.Remove("MaDiaDiemDoHang");
                grList.Tables[0].Columns.Remove("MaDiaDiemXepHang");
                btnTimKiem.PerformClick();
                //grList.Refetch();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private string GetSeachWhere()
        {
            try
            {

                this.Cursor = Cursors.WaitCursor;

                string soToKhai = txtSoToKhai.Text.Trim();
                string phanluong = (cbbPhanLuong.SelectedValue.ToString() == "0") ? "" : cbbPhanLuong.SelectedValue.ToString().Trim();
                string trangthai = (cbbTrangThai.SelectedValue == null) ? "1" : cbbTrangThai.SelectedValue.ToString().Trim();
                DateTime ngayDangKy = clcTuNgay.Value;
                string haiquan = ctrMaHaiQuan.Code.Trim();

                string where = String.Empty;
                where = "MaDonVi='" + GlobalSettings.MA_DON_VI + "' ";
                if (trangthai != "")
                    where = where + " and TrangThaiXuLy='" + trangthai + "'";
                if (soToKhai != "")
                    where = where + " and SoToKhai like '%" + soToKhai + "%' ";
                if (phanluong != "")
                    where = where + " and MaPhanLoaiKiemTra='" + phanluong + "' ";
                if (haiquan != "")
                    where = where + " and CoQuanHaiQuan like '" + haiquan + "%' ";
                if (txtNamDangKy.Text != "" && Convert.ToInt64(txtNamDangKy.Text) > 1900)
                    if (trangthai != "0" && trangthai != "1")
                        where = where + " and year(NgayDangKy)=" + txtNamDangKy.Text + " ";
                if (!string.IsNullOrEmpty(txtMaLoaiHinh.Text))
                {
                    if (txtMaLoaiHinh.Text.Contains(";"))
                    {
                        string[] listMlh = txtMaLoaiHinh.Text.Split(';');
                        string seachMlh = string.Empty;
                        foreach (string item in listMlh)
                        {
                            seachMlh += "'" + item.Trim() + "'" + ",";
                        }
                        seachMlh = seachMlh.Remove(seachMlh.Length - 1);
                        where = where + " AND MaLoaiHinh in (" + seachMlh + ") ";
                    }
                    else
                    {
                        where = where + " AND MaLoaiHinh = '" + txtMaLoaiHinh.Text.Trim() + "' ";
                    }
                }
                if (!string.IsNullOrEmpty(txtSoHoaDon.Text))
                    where = where + " AND SoHoaDon like '%" + txtSoHoaDon.Text.Trim() + "%' ";
                if (!string.IsNullOrEmpty(txtSoTKDauTien.Text))
                    where = where + " AND SoToKhaiDauTien like '%" + txtSoTKDauTien.Text.Trim() + "%' ";

                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayDangKy Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                return where;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1 ";
            }

        }
        private string GetSeachWhereOld()
        {
            try
            {

                this.Cursor = Cursors.WaitCursor;

                string soToKhai = txtSoToKhai.Text.Trim();
                string phanluong = (cbbPhanLuong.SelectedValue.ToString() == "0") ? "" : cbbPhanLuong.SelectedValue.ToString().Trim();
                string trangthai = (cbbTrangThai.SelectedValue == null) ? "1" : cbbTrangThai.SelectedValue.ToString().Trim();
                DateTime ngayDangKy = clcTuNgay.Value;
                string haiquan = ctrMaHaiQuan.Code.Trim();

                string where = String.Empty;
                where = "MaDonVi='" + GlobalSettings.MA_DON_VI + "' ";
                if (trangthai != "")
                    where = where + " and TrangThaiXuLy='" + trangthai + "'";
                if (soToKhai != "")
                    where = where + " and SoToKhai like '%" + soToKhai + "%' ";
                if (phanluong != "")
                    where = where + " and MaPhanLoaiKiemTra='" + phanluong + "' ";
                if (haiquan != "")
                    where = where + " and CoQuanHaiQuan like '" + haiquan + "%' ";
                if (txtNamDangKy.Text != "" && Convert.ToInt64(txtNamDangKy.Text) > 1900)
                    if (trangthai != "0" && trangthai != "1")
                        where = where + " and year(NgayDangKy)=" + txtNamDangKy.Text + " ";
                if (!string.IsNullOrEmpty(txtMaLoaiHinh.Text))
                {
                    if (txtMaLoaiHinh.Text.Contains(";"))
                    {
                        string[] listMlh = txtMaLoaiHinh.Text.Split(';');
                        string seachMlh = string.Empty;
                        foreach (string item in listMlh)
                        {
                            seachMlh += "'" + item.Trim() + "'" + ",";
                        }
                        seachMlh = seachMlh.Remove(seachMlh.Length - 1);
                        where = where + " AND MaLoaiHinh in (" + seachMlh + ") ";
                    }
                    else
                    {
                        where = where + " AND MaLoaiHinh = '" + txtMaLoaiHinh.Text.Trim() + "' ";
                    }
                }
                if (!string.IsNullOrEmpty(txtSoHoaDon.Text))
                    where = where + " AND SoHoaDon like '%" + txtSoHoaDon.Text.Trim() + "%' ";
                if (!string.IsNullOrEmpty(txtSoTKDauTien.Text))
                    where = where + " AND SoToKhaiDauTien like '%" + txtSoTKDauTien.Text.Trim() + "%' ";

                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayDangKy Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                return where;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1 ";
            }

        }
        //30/10/2014 minhnd
        private void ChuyenTrangThai()
        {
            if (grList.SelectedItems.Count == 1)
            {

                ToKhaiMauDichCollection tkmdColl = new ToKhaiMauDichCollection();
                foreach (GridEXSelectedItem grItem in grList.SelectedItems)
                {
                    if (grItem.RowType == RowType.Record)
                    {
                        try
                        {

                            //ChuyenTrangThaiTK obj = new ChuyenTrangThaiTK((ToKhaiMauDich)grItem.GetRow().DataRow);
                            VNACC_ChuyenTrangThaiTK obj = new VNACC_ChuyenTrangThaiTK((KDT_VNACC_ToKhaiMauDich)grItem.GetRow().DataRow);
                            obj.ShowDialog(this);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }

                    }
                }
                btnTimKiem_Click(null, null);
            }
            else
            {
                // ShowMessage("Không có dữ liệu được chọn", false);
                MLMessages("Chưa có dữ liệu được chọn", "MSG_REC02", "", false);

            }
        }

        private void toolStripMenuChuyenTrangThai_Click(object sender, EventArgs e)
        {
            WSForm2 login = new WSForm2();
            login.ShowDialog(this);
            if (WSForm2.IsSuccess == true)
            {
                this.ChuyenTrangThai();
            }

        }
        public DataSet GetDSTK_AMA_ChuaCapNhat()
        {

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet ds1 = new DataSet();
            //Lấy danh sách TK chưa có Ton
            try
            {
                string sp = "SELECT SoToKhaiBoSung,CASE WHEN SoToKhai<>0 THEN (SELECT TOP 1 t.ID  FROM dbo.t_KDT_VNACC_ToKhaiMauDich t WHERE t.SoToKhai = SoToKhai AND t.NgayDangKy = NgayKhaiBao AND t.MaLoaiHinh = MaLoaiHinh) ELSE 0 END AS TKMD_ID," +
                            "SoToKhai,NgayKhaiBao,MaLoaiHinh,TrangThaiXuLy,SoThuTuDongHangTrenToKhaiGoc,MoTaHangHoaTruocKhiKhaiBoSung,SoLuongTinhThueTruocKhiKhaiBoSung,TriGiaTinhThueTruocKhiKhaiBoSung,MoTaHangHoaSauKhiKhaiBoSung,SoLuongTinhThueSauKhiKhaiBoSung,TriGiaTinhThueSauKhiKhaiBoSung ,MaSoHangHoaTruocKhiKhaiBoSung,MaSoHangHoaSauKhiKhaiBoSung, MaNuocXuatXuTruocKhiKhaiBoSung,MaNuocXuatXuSauKhiKhaiBoSung,MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung, ThueSuatTruocKhiKhaiBoSung,ThueSuatSauKhiKhaiBoSung FROM dbo.t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung join dbo.t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue ON t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue.TKMDBoSung_ID = t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung.ID WHERE SoToKhaiBoSung <>0 AND TrangThaiXuLy IN (2,3) AND YEAR(NgayKhaiBao) = {0} AND  (MoTaHangHoaTruocKhiKhaiBoSung<>MoTaHangHoaSauKhiKhaiBoSung OR SoLuongTinhThueTruocKhiKhaiBoSung<>SoLuongTinhThueSauKhiKhaiBoSung) ORDER BY SoToKhaiBoSung,SoToKhai";
                DbCommand cmd = db.GetSqlStringCommand(string.Format(sp, txtNamDangKy.Text));
                db.LoadDataSet(cmd, ds1, "t_AMA");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return ds1;

        }
        private void btnCapNhatAMA_Click(object sender, EventArgs e)
        {
            AMA_To_VNACC_Form f = new AMA_To_VNACC_Form();
            dsAMA = GetDSTK_AMA_ChuaCapNhat();
            ds_Done = dsAMA.Clone();
            if (dsAMA.Tables[0].Rows.Count > 0)
            {
                #region KT vs hàng tờ khai gốc
                //KT vs hàng tờ khai gốc
                string SoToKhai = "";
                int TKMD_ID = 0;
                int stt = 0;
                string maHangTruoc = "";
                string maHangSau = "";
                string tenHangTruoc = "";
                string tenHangSau = "";
                decimal soLuongTruoc = 0;
                decimal soLuongSau = 0;
                decimal triGiaTruoc = 0;
                decimal triGiaSau = 0;
                string motaTruoc = "";
                string motaSau = "";
                string maDVTTruoc = "";
                string maDVTSau = "";
                string maHSTruoc = "";
                string maHSSau = "";
                string maNuocXXTruoc = "";
                string maNuocXXSau = "";
                string thueSuatTruoc = "";
                string thueSuatSau = "";
                String sql = "";
                try
                {
                    foreach (DataRow dr in dsAMA.Tables[0].Rows)
                    {
                        try
                        {
                            //SoToKhai = Convert.ToDecimal(dr["SoToKhai"].ToString());
                            SoToKhai = dr["SoToKhai"].ToString();
                            TKMD_ID = Convert.ToInt32(dr["TKMD_ID"].ToString());
                            stt = Convert.ToInt32(dr["SoThuTuDongHangTrenToKhaiGoc"].ToString());
                            motaTruoc = dr["MoTaHangHoaTruocKhiKhaiBoSung"].ToString();
                            motaSau = dr["MoTaHangHoaSauKhiKhaiBoSung"].ToString();
                            maHSTruoc = dr["MaSoHangHoaTruocKhiKhaiBoSung"].ToString();
                            maHSSau = dr["MaSoHangHoaSauKhiKhaiBoSung"].ToString();
                            maNuocXXTruoc = dr["MaNuocXuatXuTruocKhiKhaiBoSung"].ToString();
                            maNuocXXSau = dr["MaNuocXuatXuSauKhiKhaiBoSung"].ToString();
                            soLuongTruoc = Convert.ToDecimal(dr["SoLuongTinhThueTruocKhiKhaiBoSung"].ToString());
                            soLuongSau = Convert.ToDecimal(dr["SoLuongTinhThueSauKhiKhaiBoSung"].ToString());
                            maDVTTruoc = dr["MaDonViTinhSoLuongTinhThueTruocKhaiBoSung"].ToString();
                            maDVTSau = dr["MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung"].ToString();
                            triGiaTruoc = Convert.ToDecimal(dr["TriGiaTinhThueTruocKhiKhaiBoSung"].ToString());
                            triGiaSau = Convert.ToDecimal(dr["TriGiaTinhThueSauKhiKhaiBoSung"].ToString());
                            thueSuatTruoc = dr["ThueSuatTruocKhiKhaiBoSung"].ToString();
                            thueSuatSau = dr["ThueSuatSauKhiKhaiBoSung"].ToString();
                            if (string.IsNullOrEmpty(motaSau) || soLuongSau == 0)
                            {

                            }
                            if (thueSuatSau == "KCT")
                            {
                                thueSuatSau = "0";
                            }
                            if (thueSuatTruoc == "KCT")
                            {
                                thueSuatTruoc = "0";
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }

                        if (motaTruoc.Contains("#&"))
                        {
                            string[] temp = motaTruoc.Split(new string[] { "#&" }, StringSplitOptions.None);
                            if (temp.Length > 0)
                            {
                                maHangTruoc = temp[0];
                                tenHangTruoc = temp[1];
                            }

                        }
                        else
                        {
                            maHangTruoc = "";
                            tenHangTruoc = motaTruoc;
                        }

                        if (motaSau.Contains("#&"))
                        {
                            string[] temp = motaSau.Split(new string[] { "#&" }, StringSplitOptions.None);
                            if (temp.Length > 0)
                            {
                                maHangSau = temp[0];
                                tenHangSau = temp[1];
                            }
                        }
                        else
                        {
                            maHangSau = "";
                            tenHangSau = motaSau;
                        }
                        //Duy
                        //if (tenHangTruoc != tenHangSau)
                        //{
                        //    ds_Done.Tables[0].NewRow();
                        //    ds_Done.Tables[0].ImportRow(dr);
                        //}
                        //else if (maHangTruoc != maHangSau)
                        //{
                        //    ds_Done.Tables[0].NewRow();
                        //    ds_Done.Tables[0].ImportRow(dr);
                        //}
                        //else if (soLuongTruoc != soLuongSau)
                        //{
                        //    ds_Done.Tables[0].NewRow();
                        //    ds_Done.Tables[0].ImportRow(dr);
                        //}
                        //else if (maDVTTruoc != maDVTSau)
                        //{
                        //    ds_Done.Tables[0].NewRow();
                        //    ds_Done.Tables[0].ImportRow(dr);
                        //}
                        //else if (maHSTruoc != maHSSau)
                        //{
                        //    ds_Done.Tables[0].NewRow();
                        //    ds_Done.Tables[0].ImportRow(dr);
                        //}
                        //else if (triGiaTruoc != triGiaSau)
                        //{
                        //    ds_Done.Tables[0].NewRow();
                        //    ds_Done.Tables[0].ImportRow(dr);
                        //}
                        //else if (thueSuatTruoc != thueSuatSau)
                        //{
                        //    ds_Done.Tables[0].NewRow();
                        //    ds_Done.Tables[0].ImportRow(dr);
                        //}
                        //else
                        //{
                        //}
                        //Duy
                        decimal _test = Convert.ToDecimal(dr["SoLuongTinhThueSauKhiKhaiBoSung"].ToString());
                        if (!string.IsNullOrEmpty(maHangSau) && !string.IsNullOrEmpty(tenHangSau) && soLuongSau != 0)
                        {
                            KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                            sql = String.Format(" MaHangHoa = '{0}' AND TenHang = N'{1}' AND TKMD_ID = {2} AND SoLuong1 = {3} AND SoDong = '{4}'", maHangSau, tenHangSau, TKMD_ID, soLuongSau, stt);
                            //sql = @" MaHangHoa = '" + maHangSau + "' AND TenHang = N'" + tenHangSau + "' AND TKMD_ID = " + TKMD_ID + " AND SoLuong1 = " + _test.ToString() + " AND SoDong = '" + stt + "'";

                            sql = sql.Replace(',', '.');
                            try
                            {
                                hmd = KDT_VNACC_HangMauDich.SelectCollectionDynamic(sql, "")[0];
                                if (hmd.SoLuong1 == Convert.ToDecimal(soLuongSau) && hmd.TenHang == tenHangSau && hmd.MaHangHoa == maHangSau && hmd.DVTLuong1 == maDVTSau)
                                {

                                }
                                else
                                {
                                    if (soLuongSau != 0)
                                    {
                                        ds_Done.Tables[0].NewRow();
                                        ds_Done.Tables[0].ImportRow(dr);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                if (soLuongSau != 0)
                                {
                                    ds_Done.Tables[0].NewRow();
                                    ds_Done.Tables[0].ImportRow(dr);
                                }
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }


                        }
                        else
                        {
                            KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                            sql = string.Format(" TKMD_ID = {0} AND SoLuong1 = {1} AND SoDong = '{2}'", TKMD_ID, soLuongSau, stt);
                            sql = sql.Replace(',', '.');
                            try
                            {
                                hmd = KDT_VNACC_HangMauDich.SelectCollectionDynamic(sql, "")[0];
                                if (hmd.SoLuong1 == soLuongSau)
                                {
                                    //dsAMA.Tables[0].Rows.Remove(dr);
                                    //dsAMA.Tables[0].AcceptChanges();
                                }
                                else
                                {
                                    if (soLuongSau != 0)
                                    {
                                        ds_Done.Tables[0].NewRow();
                                        ds_Done.Tables[0].ImportRow(dr);
                                    }

                                }
                            }
                            catch (Exception ex)
                            {
                                if (soLuongSau != 0)
                                {
                                    ds_Done.Tables[0].NewRow();
                                    ds_Done.Tables[0].ImportRow(dr);
                                }
                                else
                                {
                                    ds_Done.Tables[0].NewRow();
                                    ds_Done.Tables[0].ImportRow(dr);
                                }
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                #endregion KT vs tờ khai gốc
                if (ds_Done.Tables[0].Rows.Count > 0)
                {
                    //dsAMA.AcceptChanges();
                    f.ds = ds_Done;
                    f.ShowDialog();
                }
                else
                    ShowMessage("Không có tờ khai AMA nào chưa được cập nhật", false);
                // MessageBox.Show("Không có tờ khai AMA nào chưa được cập nhật", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                ShowMessage("Không có tờ khai AMA nào chưa được cập nhật", false);
                // MessageBox.Show("Không có tờ khai AMA nào chưa được cập nhật","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void toolStripMenuChuyenThongQuan_Click(object sender, EventArgs e)
        {
            WSForm2 login = new WSForm2();
            login.ShowDialog(this);
            if (WSForm2.IsSuccess == true)
            {
                if (grList.SelectedItems.Count == 1)
                {

                    ToKhaiMauDichCollection tkmdColl = new ToKhaiMauDichCollection();
                    foreach (GridEXSelectedItem grItem in grList.SelectedItems)
                    {
                        if (grItem.RowType == RowType.Record)
                        {
                            try
                            {
                                if (ShowMessage("Chức năng này chỉ dùng cho các tờ khai đã xác nhận khai báo nhưng có lỗi trong quá trình nhận phản hồi thông quan và Đồng bộ tờ khai chưa thông quan", true) == "Yes")
                                {
                                    KDT_VNACC_ToKhaiMauDich tkmd = new KDT_VNACC_ToKhaiMauDich();
                                    tkmd = (KDT_VNACC_ToKhaiMauDich)grItem.GetRow().DataRow;
                                    tkmd.TrangThaiXuLy = EnumTrangThaiXuLy.ThongQuan;
                                    tkmd.Update();
#if SXXK_V4
                                    Company.BLL.VNACCS.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(tkmd);
#elif GC_V4
                                Company.GC.BLL.VNACC.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(tkmd, GlobalSettings.MA_DON_VI != "4000395355");
#endif
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }

                        }
                    }
                    btnTimKiem_Click(null, null);
                }
                else
                {
                    MLMessages("Chưa có dữ liệu được chọn", "MSG_REC02", "", false);
                }
            }
        }

        private void toolStripMenuUpdateStatus_Click(object sender, EventArgs e)
        {
            //WSForm2 login = new WSForm2();
            //login.ShowDialog(this);
            //if (WSForm2.IsSuccess == true)
            //{
            if (grList.SelectedItems.Count == 1)
            {

                ToKhaiMauDichCollection tkmdColl = new ToKhaiMauDichCollection();
                foreach (GridEXSelectedItem grItem in grList.SelectedItems)
                {
                    if (grItem.RowType == RowType.Record)
                    {
                        try
                        {
                            KDT_VNACC_ToKhaiMauDich TKMD = (KDT_VNACC_ToKhaiMauDich)grItem.GetRow().DataRow;
                            UpdateStatusForm f = new UpdateStatusForm();
                            f.TKMD = TKMD;
                            f.ShowDialog(this);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }

                    }
                }
                btnTimKiem_Click(null, null);
            }
            else
            {
                // ShowMessage("Không có dữ liệu được chọn", false);
                MLMessages("Chưa có dữ liệu được chọn", "MSG_REC02", "", false);

            }
            //}
        }
        #region Cập nhật thông tin
        private void CapNhatThongTin(ReturnMessages msgResult, KDT_VNACC_ToKhaiMauDich TKMD)
        {
            CapNhatThongTin(msgResult, string.Empty, TKMD);
        }
        private void CapNhatThongTin(ReturnMessages msgResult, string MaNV, KDT_VNACC_ToKhaiMauDich TKMD)
        {
            try
            {
                ProcessMessages.GetDataResult_TKMD(msgResult, MaNV, TKMD);
                TKMD.InsertUpdateFull();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void TuDongCapNhatThongTin(KDT_VNACC_ToKhaiMauDich TKMD)
        {
            if (TKMD != null && TKMD.SoToKhai > 0 && TKMD.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan like '{0}%' and TrangThai in ('C','E')", TKMD.SoToKhai.ToString().Substring(0, 11)), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        if (msgPb.TrangThai == EnumTrangThaiXuLyMessage.ChuaXuLy || msgPb.TrangThai == EnumTrangThaiXuLyMessage.XuLyLoi)
                        {
                            ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                            CapNhatThongTin(msgReturn, TKMD);
                            //msgPb.Delete();
                            msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                            msgPb.InsertUpdate();
                        }
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
        }
        #endregion
        private void toolStripUpdateResult_Click(object sender, EventArgs e)
        {
            WSForm2 login = new WSForm2();
            login.ShowDialog(this);
            if (WSForm2.IsSuccess == true)
            {
                if (grList.SelectedItems.Count >= 1)
                {
                    GridEXSelectedItemCollection items = grList.SelectedItems;
                    List<KDT_VNACC_ToKhaiMauDich> TKMDCollection = new List<KDT_VNACC_ToKhaiMauDich>();
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            TKMDCollection.Add((KDT_VNACC_ToKhaiMauDich)i.GetRow().DataRow);
                        }
                        //if (grItem.RowType == RowType.Record)
                        //{
                        //    try
                        //    {
                        //            KDT_VNACC_ToKhaiMauDich tkmd = new KDT_VNACC_ToKhaiMauDich();
                        //            tkmd = (KDT_VNACC_ToKhaiMauDich)grItem.GetRow().DataRow;
                        //            tkmd.TrangThaiXuLy = EnumTrangThaiXuLy.ThongQuan;
                        //            tkmd.Update();
                        //            MsgPhanBo.UpdateMsgPhanBo(tkmd.SoToKhai.ToString());
                        //            TuDongCapNhatThongTin(tkmd);
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        Logger.LocalLogger.Instance().WriteMessage(ex);
                        //    }

                        //}
                    }

                    foreach (KDT_VNACC_ToKhaiMauDich TKMD in TKMDCollection)
                    {
                        TKMD.LoadFull();
                        MsgPhanBo.UpdateMsgPhanBo(TKMD.SoToKhai.ToString());
                        TuDongCapNhatThongTin(TKMD);
                    }
                    ShowMessage("Cập nhật thành công.", false);
                    btnTimKiem_Click(null, null);
                }
                else
                {
                    MLMessages("Chưa có dữ liệu được chọn", "MSG_REC02", "", false);
                }
            }
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value;}));
            }
            else
            {
                uiProgressBar1.Value = value;
            }
        }
        String InitialDirectory = String.Empty;
        private void btnExportPdf_Click(object sender, EventArgs e)
        {
            btnExportPdf.Enabled = false;
            FolderBrowserDialog targetDirectory = new FolderBrowserDialog();
            if (targetDirectory.ShowDialog() != DialogResult.OK)
            {
                btnExportPdf.Enabled = true;
                return;
            }
            else
            {
                InitialDirectory = targetDirectory.SelectedPath;
            }

            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }
        private void DoWork(object obj)
        {
            try
            {
                //ReportViewVNACCSToKhaiNhap f = new ReportViewVNACCSToKhaiNhap(true);
                //ReportViewVNACCSToKhaiXuat form = new ReportViewVNACCSToKhaiXuat(true);
                List<KDT_VNACC_ToKhaiMauDich> TKMDSelect = new List<KDT_VNACC_ToKhaiMauDich>();
                Janus.Windows.GridEX.GridEXRow[] listChecked = grList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    KDT_VNACC_ToKhaiMauDich TKMD = (KDT_VNACC_ToKhaiMauDich)item.DataRow;
                    //TKMD.LoadFull();
                    TKMDSelect.Add(TKMD);
                }
                int k = 0;
                foreach (KDT_VNACC_ToKhaiMauDich item in TKMDSelect)
                {
                    List<MsgPhanBo> listPhanBo = new List<MsgPhanBo>();
                    if (string.IsNullOrEmpty(item.InputMessageID))
                        listPhanBo = MsgPhanBo.GetMsgPhanBoChuaXuLy(GlobalVNACC.TerminalID);
                    else
                        listPhanBo = MsgPhanBo.GetMsgPhanBoChuaXuLyInput(item.InputMessageID, item.SoToKhai.ToString());

                    if (listPhanBo != null && listPhanBo.Count > 0)
                    {
                        foreach (MsgPhanBo ite in listPhanBo)
                        {
                            if (ProcessMessages.CheckInGroup(ite.MaNghiepVu, EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiNhap_BanChapNhanThongQuan))
                            {
                                MsgLog Log = MsgLog.Load(ite.Master_ID);

                                ReturnMessages rMsg = new ReturnMessages(Log.Log_Messages);

                                if (rMsg.Body == null)
                                {
                                    string[] lines = Log.Log_Messages.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                                    rMsg = new ReturnMessages(lines);
                                }
                                if (rMsg != null)
                                {
                                    VAD1AG0 vad1ag = new VAD1AG0();
                                    vad1ag.LoadVAD1AG0(rMsg.Body.ToString());
                                    ReportViewVNACCSToKhaiNhap f = new ReportViewVNACCSToKhaiNhap(true);
                                    f.IsThongQuan = true;
                                    f.maNV = ite.MaNghiepVu;
                                    f.Vad1ag = vad1ag;
                                    f.GetAllReport(f.IsThongQuan);
                                    f.InitialDirectory = InitialDirectory;
                                    f.ExportAll_Pdf_ItemClick(null, null);
                                }
                            }
                            else if (ProcessMessages.CheckInGroup(ite.MaNghiepVu, EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiXuat_BanChapNhanThongQuan))
                            {
                                MsgLog Log = MsgLog.Load(ite.Master_ID);

                                ReturnMessages rMsg = new ReturnMessages(Log.Log_Messages);

                                if (rMsg.Body == null)
                                {
                                    string[] lines = Log.Log_Messages.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                                    rMsg = new ReturnMessages(lines);
                                }
                                if (rMsg != null)
                                {
                                    VAE1LF0 vae1lf = new VAE1LF0();
                                    vae1lf.LoadVAE1LF0(rMsg.Body.ToString());
                                    ReportViewVNACCSToKhaiXuat form = new ReportViewVNACCSToKhaiXuat(true);
                                    form.IsThongQuan = true;
                                    form.maNV = ite.MaNghiepVu;
                                    form.VAE1LF = vae1lf;
                                    form.GetAllReport(form.IsThongQuan);
                                    form.InitialDirectory = InitialDirectory;
                                    form.ExportAll_Pdf_ItemClick(null, null);
                                }
                            }
                        }
                    }
                    SetProcessBar((k * 100 / TKMDSelect.Count));
                    k++;

                }
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        btnExportPdf.Enabled = false;

                    }));
                }
                else
                {
                    btnExportPdf.Enabled = true;
                }
                SetProcessBar(0);
                ShowMessageTQDT("Xuất tờ khai thành công !", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
