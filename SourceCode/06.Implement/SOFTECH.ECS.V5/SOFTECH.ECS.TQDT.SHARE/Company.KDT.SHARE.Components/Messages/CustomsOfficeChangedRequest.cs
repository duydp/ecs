﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Đơn xin chuyển cửa khẩu
    /// </summary>
    public class CustomsOfficeChangedRequest
    {
        /// <summary>
        /// .....
        /// </summary>
        [XmlElement("AdditionalDocument")]
        public AdditionalDocument AdditionalDocument { get; set; }
        /// <summary>
        /// ......
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        /// <summary>
        /// Ap dung cho de nghi chuyen cua khau - phuong tien van tai chuyen cua khau
        /// </summary>
        [XmlElement("BorderTransportMeans")]
        public BorderTransportMeans BorderTransportMeans { get; set; }
    }
}
