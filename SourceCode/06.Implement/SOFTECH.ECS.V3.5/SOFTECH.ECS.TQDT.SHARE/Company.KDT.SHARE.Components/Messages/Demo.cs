﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public class Demo
    {

        #region GC thanh khoản
        public static GC_ThanhkhoanHopDong GC_ThanhKhoan()
        {
            GC_ThanhkhoanHopDong thanhkhoan = new GC_ThanhkhoanHopDong()
            {
                Issuer = "605",
                Function = "8",
                Reference = "reference",
                Issue = DateTime.Now.ToString(),
                IssueLocation = "issueLocation",
                DeclarationOffice = "C34C",
                Status = "1",
                CustomsReference = "customsReference",
                Acceptance = "acceptance",
                Agents = new List<Agent>(),
                Importer = new NameBase() { Name = "name", Identity = "Identity" },
                ContractReference = new ContractDocument()
                {
                    Reference = "So hop dong",
                    Issue = DateTime.Now.ToString(),
                    Expire = DateTime.Now.ToString(),
                    DeclarationOffice = "C34C",
                    CustomsReference = "CustomsReference"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "content" },
                    Statement = "1"
                },
                AdditionalDocuments = new List<AdditionalDocument>()
            };
            //add agent
            thanhkhoan.Agents.Add(new Agent() { Name = "name", Identity = "identity", Status = "status" });
            thanhkhoan.Agents.Add(new Agent() { Name = "name", Identity = "identity", Status = "status" });
            thanhkhoan.Agents.Add(new Agent() { Name = "name", Identity = "identity", Status = "status" });
            ///add AdditionalDocument
            AdditionalDocument addDoc1 = new AdditionalDocument()
            {
                DocumentType = "6051",
                DocumentReference = "NKD01/2008/10",
                Description = "Ghi chu",
                GoodItems = new List<GoodsItem>()
            };
            addDoc1.GoodItems.Add(new GoodsItem()
            {
                Commodity = new Commodity() { Description = "Ghi chu", Identification = "NG1", ComodityType = "1" },
                GoodsMeasure = new GoodsMeasure() { Quantity = "324", MeasureUnit = "343" }
            });
            addDoc1.GoodItems.Add(new GoodsItem()
            {
                Commodity = new Commodity() { Description = "Ghi chu", Identification = "NG1", ComodityType = "1" },
                GoodsMeasure = new GoodsMeasure() { Quantity = "324", MeasureUnit = "343" }
            });
            thanhkhoan.AdditionalDocuments.Add(addDoc1);
            string msg = Helpers.Serializer(thanhkhoan);
            return thanhkhoan;
        }
        #endregion
        #region GC_TokhaiChuyenTiep_Nhap
        public static GC_TokhaiChuyenTiep_Nhap GC_TokhaiChuyenTiep_Nhap()
        {
            GC_TokhaiChuyenTiep_Nhap TK_CTNhap = new GC_TokhaiChuyenTiep_Nhap()
            {
                Issuer = "968",
                Reference = "D1B13ADF631146E5A3D2229340D49358",
                Issue = DateTime.Now.ToString(),
                Function = "8",
                IssueLocation = "issue location",
                Status = "1",
                CustomsReference = "customs reference",
                Acceptance = "acceptance",
                DeclarationOffice = "C34C",
                GoodsItem = "3",
                LoadingList = "0",
                TotalGrossMass = "32",
                TotalNetGrossMass = "32",
                NatureOfTransaction = "NGC18",
                PaymentMethod = "CASH",
                Agents = new List<Agent>(),
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "USD", Rate = "20000" },
                DeclarationPackaging = new Packaging() { Quantity = "34" },
                AdditionalDocuments = new List<AdditionalDocument>(),
                Importer = new NameBase() { Name = "name", Identity = "identity" },
                RepresentativePerson = new RepresentativePerson() { ContactFunction = ".", Name = "name" },
                AdditionalInformation = new AdditionalInformation() { Statement = "001", Content = new Content() { Text = "noi dung" } },
                GoodsShipment = new GoodsShipment()
                {
                    ExportationCountry = "VN",
                    Consignee = new NameBase() { Name = "name", Identity = "identity" },
                    Consignor = new NameBase() { Name = "name", Identity = "identity" },
                    CustomsGoodsItems = new List<CustomsGoodsItem>(),
                    DeliveryDestination = new DeliveryDestination() { Line = "Dia diem giao hang", Time = "Thời điểm giao hàng" },
                    EntryCustomsOffice = new LocationNameBase() { Name = "name", Code = "code" },
                    Exporter = new NameBase() { Name = "name", Identity = "identity" },
                    NotifyParty = new NameBase() { Name = "name", Identity = "identity" },
                    TradeTerm = new TradeTerm() { Condition = "điều kiện giao hàng" }
                },
                Licenses = new List<License>(),
                ContractDocuments = new List<ContractDocument>(),
                CommercialInvoices = new List<CommercialInvoice>(),
                CertificateOfOrigins = new List<CertificateOfOrigin>(),
                BillOfLadings = new List<BillOfLading>(),
                CustomsOfficeChangedRequest = new CustomsOfficeChangedRequest()
                {
                    AdditionalDocument = new AdditionalDocument()
                    {
                        Reference = "So van don",
                        Issue = DateTime.Now.ToString("YYYY-MM-dd"),
                    },
                    AdditionalInformation = new AdditionalInformation()
                    {
                        Content = new Content() { Text = "Noi dung xin chuyen cua khau" },
                        ExaminationPlace = "dia diem kiem tra",
                        Time = DateTime.Now.ToString("YYYY-MM-dd"),
                        Route = "Tuyen duong"
                    }
                },
                AttachDocuments = new List<AttachDocumentItem>(),
                AdditionalDocumentArr = new List<AdditionalDocument>()
            };
            //add agent
            TK_CTNhap.Agents.Add(new Agent() { Name = "name", Identity = "identity", Status = "status" });
            TK_CTNhap.Agents.Add(new Agent() { Name = "name", Identity = "identity", Status = "status" });
            TK_CTNhap.Agents.Add(new Agent() { Name = "name", Identity = "identity", Status = "status" });
            //add additionDocumment List
            AdditionalDocument addDoc1 = new AdditionalDocument()
            {
                Issue = DateTime.Now.ToString("YYYY-MM-dd"),
                Reference = "So hop dong",
                Type = "316",
                Name = "Hop dong",
                Expire = DateTime.Now.ToString("YYYY-MM-dd")

            };
            AdditionalDocument addDoc2 = new AdditionalDocument()
              {
                  Issue = DateTime.Now.ToString("YYYY-MM-dd"),
                  Reference = "So hop dong",
                  Type = "316",
                  Name = "Hop dong",
                  Expire = DateTime.Now.ToString("YYYY-MM-dd"),

              };

            TK_CTNhap.AdditionalDocuments.Add(addDoc1);
            TK_CTNhap.AdditionalDocuments.Add(addDoc2);
            //add GoodsShipment.CustomGoodsItems
            //item1
            CustomsGoodsItem item1 = new CustomsGoodsItem()
            {
                CustomsValue = "882000",
                Sequence = "1",
                StatisticalValue = "34343445",
                UnitPrice = "21313",
                StatisticalUnitPrice = "432432432",
                ConversionRate = "1",
                AdditionalDocument = new AdditionalDocument()
                {
                    Issue = DateTime.Now.ToString(),
                    Issuer = "issuer",
                    IssueLocation = "issue location",
                    Reference = "reference",
                    Type = "008",
                    Name = "CO",
                    Expire = DateTime.Now.ToString(),
                },
                Commodity = new Commodity()
                {
                    Type = "1",
                    Brand = "nhan hieu",
                    Description = "ten nguyen phu lieu",
                    Grade = "quy cach",
                    Ingredients = "thanh phan",
                    ModelNumber = "Model",
                    Identification = "MaNPL",
                    TariffClassification = "435435435",
                    TariffClassificationExtension = "",
                    DutyTaxFee = new List<DutyTaxFee>(),
                    InvoiceLine = new InvoiceLine() { ItemCharge = "", Line = "" }
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "42",
                    MeasureUnit = "2"
                },
                Manufacturer = new NameBase() { Name = "name", Identity = "identity" },
                Origin = new Origin() { OriginCountry = "EH" },
                CustomsValuation = new CustomsValuation()
                {
                    ExitToEntryCharge = "0",
                    FreightCharge = "0",
                    Method = "",
                    OtherChargeDeduction = "0"
                }


            };

            item1.Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "324234",
                Tax = "7",
                Type = "1"
            });
            item1.Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "324234",
                Tax = "7",
                Type = "1"
            });
            item1.Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "324234",
                Tax = "7",
                Type = "1"
            });
            item1.Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "324234",
                Tax = "7",
                Type = "1"
            });
            //item2
            CustomsGoodsItem item2 = new CustomsGoodsItem()
            {
                CustomsValue = "882000",
                Sequence = "1",
                StatisticalValue = "34343445",
                UnitPrice = "21313",
                StatisticalUnitPrice = "432432432",
                ConversionRate = "1",
                AdditionalDocument = new AdditionalDocument()
                {
                    Issue = DateTime.Now.ToString(),
                    Issuer = "issuer",
                    IssueLocation = "issue location",
                    Reference = "reference",
                    Type = "008",
                    Name = "CO",
                    Expire = DateTime.Now.ToString(),
                },
                Commodity = new Commodity()
                {
                    Type = "1",
                    Brand = "nhan hieu",
                    Description = "ten nguyen phu lieu",
                    Grade = "quy cach",
                    Ingredients = "thanh phan",
                    ModelNumber = "Model",
                    Identification = "MaNPL",
                    TariffClassification = "435435435",
                    TariffClassificationExtension = "",
                    DutyTaxFee = new List<DutyTaxFee>(),
                    InvoiceLine = new InvoiceLine() { ItemCharge = "", Line = "" }
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "42",
                    MeasureUnit = "2"
                },
                Manufacturer = new NameBase() { Name = "name", Identity = "identity" },
                Origin = new Origin() { OriginCountry = "EH" },
                CustomsValuation = new CustomsValuation()
                {
                    ExitToEntryCharge = "0",
                    FreightCharge = "0",
                    Method = "",
                    OtherChargeDeduction = "0"
                }


            };
            item2.Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "324234",
                Tax = "7",
                Type = "1"
            });
            item2.Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "324234",
                Tax = "7",
                Type = "1"
            });
            item2.Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "324234",
                Tax = "7",
                Type = "1"
            });
            item2.Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "324234",
                Tax = "7",
                Type = "1"
            });
            TK_CTNhap.GoodsShipment.CustomsGoodsItems.Add(item1);
            TK_CTNhap.GoodsShipment.CustomsGoodsItems.Add(item2);
            ////Add Licenses
            //lic1
            License lic1 = new License()
            {
                Issuer = "Nguoi cap",
                Reference = "So GP",
                Issue = DateTime.Now.ToString("YYYY-MM-dd"),
                IssueLocation = "noi cap",
                Type = "type",
                Expire = DateTime.Now.ToString("YYYY-MM-dd"),
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "noi dung" } },
                GoodItems = new List<GoodsItem>()
            };
            lic1.GoodItems.Add(new GoodsItem()
            {
                Sequence = "1",
                StatisticalValue = "435",
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "BEF" },
                Commodity = new Commodity() { Description = "decription", Identification = "iden", TariffClassification = "tarification" },
                GoodsMeasure = new GoodsMeasure() { Quantity = "232", MeasureUnit = "232" },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "noi dung" }
                }
            });
            //lic1
            License lic2 = new License()
            {
                Issuer = "Nguoi cap",
                Reference = "So GP",
                Issue = DateTime.Now.ToString("YYYY-MM-dd"),
                IssueLocation = "noi cap",
                Type = "type",
                Expire = DateTime.Now.ToString("YYYY-MM-dd"),
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "noi dung" } },
                GoodItems = new List<GoodsItem>()
            };
            lic2.GoodItems.Add(new GoodsItem()
            {
                Sequence = "1",
                StatisticalValue = "435",
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "BEF" },
                Commodity = new Commodity() { Description = "decription", Identification = "iden", TariffClassification = "tarification" },
                GoodsMeasure = new GoodsMeasure() { Quantity = "232", MeasureUnit = "232" },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "noi dung" }
                }
            });
            TK_CTNhap.Licenses.Add(lic1);
            TK_CTNhap.Licenses.Add(lic2);
            /// Add ContractDocuments
            //contract1
            ContractDocument contract1 = new ContractDocument()
            {
                Reference = "So HD",
                Issue = DateTime.Now.ToString("YYYY-MM-dd"),
                Expire = DateTime.Now.ToString("YYYY-MM-dd"),
                Payment = new Payment() { Method = "CASH" },
                TradeTerm = new TradeTerm() { Condition = "condition" },
                DeliveryDestination = new DeliveryDestination() { Line = "line" },
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "USD" },
                TotalValue = "34324",
                Buyer = new NameBase() { Name = "name", Identity = "iden" },
                Seller = new NameBase() { Name = "name", Identity = "iden" },
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "thong tin khac" } },
                ContractItems = new List<ContractItem>()
            };
            contract1.ContractItems.Add(new ContractItem()
            {
                unitPrice = "42",
                statisticalValue = "2342",
                Commodity = new Commodity() { Description = "tenhang1", Identification = "mahang1", TariffClassification = "23423324" },
                Origin = new Origin() { OriginCountry = "AS" },
                GoodsMeasure = new GoodsMeasure() { Quantity = "42", MeasureUnit = "32" },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "Ghi chu khac" }
                }
            });
            contract1.ContractItems.Add(new ContractItem()
            {
                unitPrice = "42",
                statisticalValue = "2342",
                Commodity = new Commodity() { Description = "tenhang1", Identification = "mahang1", TariffClassification = "23423324" },
                Origin = new Origin() { OriginCountry = "AS" },
                GoodsMeasure = new GoodsMeasure() { Quantity = "42", MeasureUnit = "32" },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "Ghi chu khac" }
                }
            });
            TK_CTNhap.ContractDocuments.Add(contract1);
            ///Add CommercialInvoices
            ///commercial_1
            CommercialInvoice commercial_1 = new CommercialInvoice()
            {
                Reference = "so hoa don",
                Issue = DateTime.Now.ToString("YYYY-MM-dd"),
                Seller = new NameBase() { Name = "name", Identity = "iden" },
                Buyer = new NameBase() { Name = "name", Identity = "iden" },
                AdditionalDocument = new AdditionalDocument() { Reference = "reference", Issue = DateTime.Now.ToString() },
                Payment = new Payment() { Method = "CASH" },
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "HKD" },
                TradeTerm = new TradeTerm() { Condition = "conditon" },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>()
            };
            commercial_1.CommercialInvoiceItems.Add(new CommercialInvoiceItem()
            {
                Sequence = "1",
                UnitPrice = "63",
                StatisticalValue = "23434",
                Origin = new Origin() { OriginCountry = "DZ" },
                Commodity = new Commodity() { Description = "tenhang1", Identification = "mahang1", TariffClassification = "23423324" },
                GoodsMeasure = new GoodsMeasure() { Quantity = "42", MeasureUnit = "32" },
                ValuationAdjustment = new ValuationAdjustment() { Addition = "223", Deduction = "423" },
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "Ghi chu khac" } },
            });

            TK_CTNhap.CommercialInvoices.Add(commercial_1);
            ///Add CertificateOfOrigins

            CertificateOfOrigin CO1 = new CertificateOfOrigin();
            TK_CTNhap.CertificateOfOrigins.Add(CO1);
            ///Add BillOfLadings
            BillOfLading bill1 = new BillOfLading();
            TK_CTNhap.BillOfLadings.Add(bill1);
            ///Add AttachDocuments
            TK_CTNhap.AttachDocuments.Add(new AttachDocumentItem()
            {
                Sequence = "1",
                Issuer = "999",
                Issue = DateTime.Now.ToString("YYYY-MM-dd"),
                Reference = "So chung tu",
                Description = "Thong tin khac",
                AttachedFiles = new List<AttachedFile>()
            });
            TK_CTNhap.AttachDocuments[0].AttachedFiles.Add(new AttachedFile()
            {
                FileName = "abc.jpg",
                Content = new Content() { Base64 = "bin.base64", Text = "23423fsdfds" }
            });
            AdditionalDocument addDoc = new AdditionalDocument()
            {
                Type = "999",
                Reference = "So chung tu",
                Issue = DateTime.Now.ToString("YYYY-MM-dd"),
                IssueLocation = "noi cap",
                Issuer = "to chuc cap",
                Expire = DateTime.Now.ToString("YYYY-MM-dd"),
                IsDebt = "1",
                Submit = DateTime.Now.ToString("YYYY-MM-dd"),
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "Thong tin khac" } }
            };
            TK_CTNhap.AdditionalDocumentArr.Add(addDoc);

            string msg = Helpers.Serializer(TK_CTNhap);
            return TK_CTNhap;
        }
        #endregion
        #region GC_TokhaiChuyentiep_Xuat
        public static GC_TokhaiChuyenTiep_Xuat GC_TokhaiChuyentiep_Xuat()
        {
            GC_TokhaiChuyenTiep_Xuat TK_CTXUat = new GC_TokhaiChuyenTiep_Xuat()
            {
                Issuer = "986",
                Reference = "D1B13ADF631146E5A3D2229340D49358",
                Issue = DateTime.Now.ToString(),
                Function = "8",
                IssueLocation = "issue location",
                Status = "1",
                CustomsReference = "customs reference",
                Acceptance = "acceptance",
                DeclarationOffice = "C34C",
                GoodsItem = "3",
                LoadingList = "0",
                TotalGrossMass = "32",
                NatureOfTransaction = "NGC18",
                Payment = new Payment() { Method = "LC" },
                Agents = new List<Agent>(),
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "USD", Rate = "32432" },
                DeclarationPackaging = new Packaging() { Quantity = "0" },
                AdditionalDocuments = new List<AdditionalDocument>(),
                Exporter = new NameBase() { Name = "name", Identity = "Iden" },
                RepresentativePerson = new RepresentativePerson()
                {
                    ContactFunction = ".",
                    Name = "name"
                },
                GoodsShipment = new GoodsShipment()
                {
                    ImportationCountry = "USA",
                    Consignor = new NameBase() { Name = "Name", Identity = "Iden" },
                    Consignee = new NameBase() { Name = "name", Identity = "Iden" },
                    NotifyParty = new NameBase() { Name = "Name", Identity = "Iden" },
                    DeliveryDestination = new DeliveryDestination() { Line = "HCM", Time = DateTime.Now.ToString("YYYY-MM-dd") },
                    EntryCustomsOffice = new LocationNameBase() { Code = "Code", Name = "Name" },
                    ExitCustomsOffice = new LocationNameBase() { Code = "code", Name = "name" },
                    Importer = new NameBase() { Name = "name", Identity = "Iden" },
                    TradeTerm = new TradeTerm() { Condition = "condition" },
                    CustomsGoodsItems = new List<CustomsGoodsItem>()

                }
            };
            //add agent
            TK_CTXUat.Agents.Add(new Agent() { Name = "name", Identity = "identity", Status = "status" });
            TK_CTXUat.Agents.Add(new Agent() { Name = "name", Identity = "identity", Status = "status" });
            TK_CTXUat.Agents.Add(new Agent() { Name = "name", Identity = "identity", Status = "status" });
            //add additionDocumment List
            AdditionalDocument addDoc1 = new AdditionalDocument()
            {
                Issue = DateTime.Now.ToString("YYYY-MM-dd"),
                Reference = "So hop dong",
                Type = "316",
                Name = "Hop dong",
                Expire = DateTime.Now.ToString("YYYY-MM-dd")

            };
            AdditionalDocument addDoc2 = new AdditionalDocument()
            {
                Issue = DateTime.Now.ToString("YYYY-MM-dd"),
                Reference = "So hop dong",
                Type = "316",
                Name = "Hop dong",
                Expire = DateTime.Now.ToString("YYYY-MM-dd"),

            };

            TK_CTXUat.AdditionalDocuments.Add(addDoc1);
            TK_CTXUat.AdditionalDocuments.Add(addDoc2);
            //add GoodsShipment.CustomGoodsItems
            //item1
            CustomsGoodsItem item1 = new CustomsGoodsItem()
            {
                CustomsValue = "882000",
                Sequence = "1",
                StatisticalValue = "34343445",
                UnitPrice = "21313",


                Commodity = new Commodity()
                {
                    Type = "1",
                    Brand = "nhan hieu",
                    Description = "ten nguyen phu lieu",
                    Grade = "quy cach",
                    Ingredients = "thanh phan",
                    ModelNumber = "Model",
                    Identification = "MaNPL",
                    TariffClassification = "435435435",
                    TariffClassificationExtension = "",
                    DutyTaxFee = new List<DutyTaxFee>(),
                    InvoiceLine = new InvoiceLine() { ItemCharge = "", Line = "" }
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "42",
                    MeasureUnit = "2"
                },
                Manufacturer = new NameBase() { Name = "name", Identity = "identity" },
                Origin = new Origin() { OriginCountry = "EH" },
                CustomsValuation = new CustomsValuation()
                {
                    ExitToEntryCharge = "0",
                    FreightCharge = "0",
                    Method = "",
                    OtherChargeDeduction = "0"
                }


            };

            item1.Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "324234",
                Tax = "7",
                Type = "1"
            });
            item1.Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "324234",
                Tax = "7",
                Type = "1"
            });
            item1.Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "324234",
                Tax = "7",
                Type = "1"
            });
            item1.Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "324234",
                Tax = "7",
                Type = "1"
            });
            TK_CTXUat.GoodsShipment.CustomsGoodsItems.Add(item1);
            string msg = Helpers.Serializer(TK_CTXUat);
            return TK_CTXUat;
        }
        #endregion
        #region SXXK_HosoThanhKhoan
        public static SXXK_HosoThanhKhoan SXXK_HosoThanhKhoan()
        {
            SXXK_HosoThanhKhoan hosoTK = new SXXK_HosoThanhKhoan()
            {
                Issuer = "103",
                Reference = "D1B13ADF631146E5A3D2229340D49358",
                Issue = DateTime.Now.ToString(),
                Function = "8",
                IssueLocation = "issue location",
                Status = "1",
                CustomsReference = "customs reference",
                Acceptance = "acceptance",
                DeclarationOffice = "C34C",
                LoadingList = "0",
                Agents = new List<Agent>(),
                Importer = new NameBase() { Name = "name", Identity = "Iden" },
                ImportDeclarationList = new List<DeclarationDocument>(),
                ExportDeclarationList = new List<DeclarationDocument>(),
                PaymentDocumentList = new PaymentDocumentList() { ContractReferences = new List<ContractReference>() },
                MaterialList = new List<ContractDocument>(),
                MaterialLeft = new List<ContractDocument>(),
                MaterialExport = new List<ContractDocument>(),
                MaterialNotExport = new List<ContractDocument>()
            };
            //add agent
            hosoTK.Agents.Add(new Agent() { Name = "name", Identity = "identity", Status = "status" });
            hosoTK.Agents.Add(new Agent() { Name = "name", Identity = "identity", Status = "status" });
            hosoTK.Agents.Add(new Agent() { Name = "name", Identity = "identity", Status = "status" });
            //add ImportDeclarationList
            hosoTK.ImportDeclarationList.Add(new DeclarationDocument()
            {
                CustomsReference = "1",
                NatureOfTransaction = "NKD01",
                Acceptance = "2012",
                DeclarationOffice = "C34C",
                Clearance = DateTime.Now.ToString("YYYY-MM-dd")
            });
            hosoTK.ImportDeclarationList.Add(new DeclarationDocument()
            {
                CustomsReference = "2",
                NatureOfTransaction = "NKD01",
                Acceptance = "2012",
                DeclarationOffice = "C34C",
                Clearance = DateTime.Now.ToString("YYYY-MM-dd")
            });
            hosoTK.ImportDeclarationList.Add(new DeclarationDocument()
            {
                CustomsReference = "3",
                NatureOfTransaction = "NKD01",
                Acceptance = "2012",
                DeclarationOffice = "C34C",
                Clearance = DateTime.Now.ToString("YYYY-MM-dd")
            });
            hosoTK.ImportDeclarationList.Add(new DeclarationDocument()
            {
                CustomsReference = "4",
                NatureOfTransaction = "NKD01",
                Acceptance = "2012",
                DeclarationOffice = "C34C",
                Clearance = DateTime.Now.ToString("YYYY-MM-dd")
            });
            //add ExportDeclarationList
            hosoTK.ExportDeclarationList.Add(new DeclarationDocument()
            {
                CustomsReference = "2",
                NatureOfTransaction = "NKD01",
                Acceptance = "2012",
                DeclarationOffice = "C34C",
                Clearance = DateTime.Now.ToString("YYYY-MM-dd")
            });
            //add PaymentDocumentList.ContractReferences
            hosoTK.PaymentDocumentList.ContractReferences.Add(new ContractReference()
            {
                Reference = "reference",
                Issue = DateTime.Now.ToString(),
                CustomsValue = "custom value",
                Commodity = new Commodity() { Identification = "Iden" },
                StatisticalValue = "statisticalValue",
                PaymentDocument = new PaymentDocument()
                {
                    Reference = "reference",
                    Issue = "Issue",
                    IssueLocation = "IssueLocation",
                    Payment = new Payment() { Method = "CASH" },
                    CustomsValue = "custom value",
                    AdditionalInformation = new AdditionalInformation()
                    {
                        Content = new Content() { Text = "noi dung" }
                    }
                }
            });
            //add materialList
            hosoTK.MaterialList.Add(new ContractDocument()
            {
                CustomsReference = "customReference",
                NatureOfTransaction = "natureOfTranSaction",
                Acceptance = "acceptance",
                DeclarationOffice = "C34C",
                Comodity = new Commodity()
                {
                    Description = "description",
                    Identification = "Iden",

                },
                ExportDeclarationDocument = new List<DeclarationDocument>()

            });
            hosoTK.MaterialList[0].ExportDeclarationDocument.Add(new DeclarationDocument()
            {
                CustomsReference = "customreference",
                NatureOfTransaction = "natureOfTranSacTion",
                Acceptance = "acceptance",
                DeclarationOffice = "declarationOffice",
                GoodsMeasure = new GoodsMeasure()
                {
                    Tariff = "tariff",
                    MeasureUnit = "324"
                }
            });
            //add materialLeft
            hosoTK.MaterialLeft.Add(new ContractDocument()
            {
                CustomsReference = "customReference",
                NatureOfTransaction = "natureOfTransaction",
                Acceptance = "acceptance",
                DeclarationOffice = "declarationOffice",
                Material = new Material(),
                Comodity = new Commodity()
                {
                    Description = "decription",
                    Identification = "Iden",
                },
                GoodsMeasure = new GoodsMeasure() 
                { 
                    Tariff= "tariff",
                    MeasureUnit="34",
                    ConversionRate="23"
                }

            });
            hosoTK.MaterialLeft[0].Material = new Material();
            hosoTK.MaterialExport.Add(new ContractDocument() { });
            hosoTK.MaterialNotExport.Add(new ContractDocument() { });
            string msg = Helpers.Serializer(hosoTK);
            return hosoTK;
        }
        #endregion

    }

}

