﻿using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.GC.BLL.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;
using Company.Interface.Report.GC;
using Company.Interface.Report;

namespace Company.Interface.GC
{
    public partial class BK09Form : BaseForm
    {

        public BK09Form()
        {
            InitializeComponent();            
        }
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        private BKToKhaiCollection tokhaiColl = new BKToKhaiCollection();
        private BKToKhai bktokhai = new BKToKhai();
        public DataSet dsBK = new DataSet();
        private void LoadData()
        {

            //tokhaiColl = new BKToKhai().SelectCollectionDynamic(" MaLoaiHinh like 'X%'", "");
            //dgList.DataSource = tokhaiColl;
            dsBK = new BKToKhai().SelectDynamic(" MaLoaiHinh like 'X%' AND IDHopDong = " + this.HD.ID, "STT");
            dgList.DataSource = dsBK.Tables[0];
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
 
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void lblViewRP_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            ReportViewBC09Form f = new ReportViewBC09Form();
            f.HD = this.HD;
            f.dsBK = this.dsBK;
            f.Show();
            this.Cursor = Cursors.Default ;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (bktokhai.UpdateDataSetFromGrid(dsBK ))
               {
                   showMsg("MSG_2702001");
                   //Message("MSG_SAV02", "", false);
                   //ShowMessage("Cập nhật thành công", false);
               }
            else
               {
                   showMsg("MSG_2702002");
                   //Message("MSG_SAV01", "", false);
                   //ShowMessage("Cập nhật không thành công", false);
               }

             LoadData();

        }

        private void KB06Form_Load(object sender, EventArgs e)
        {
            
            LoadData();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

      
        
       
     

         
 
    }
}