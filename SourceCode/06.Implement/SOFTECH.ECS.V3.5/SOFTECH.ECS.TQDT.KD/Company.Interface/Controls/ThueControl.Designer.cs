﻿namespace Company.Interface.Controls
{
    partial class ThueControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.Common.JanusColorScheme janusColorScheme3 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.Common.JanusColorScheme janusColorScheme4 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.GridEX.GridEXLayout gridEX1_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThueControl));
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.txtTriGiaThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.vsmain = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.label29 = new System.Windows.Forms.Label();
            this.txtTyLeThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtTSVatGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTSTTDBGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTSXNKGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTongSoTienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTienThue_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienThue_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTien_CLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienThue_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtCLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTL_CLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.txtTienThueCBPG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienThueBVMT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaTTCBPG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaTinhThueBVMT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTSCBPG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTSBVMT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.gridEX1 = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX1)).BeginInit();
            this.SuspendLayout();
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(703, 196);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2,
            this.uiTabPage3});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.txtTriGiaThuKhac);
            this.uiTabPage1.Controls.Add(this.label29);
            this.uiTabPage1.Controls.Add(this.txtTyLeThuKhac);
            this.uiTabPage1.Controls.Add(this.label30);
            this.uiTabPage1.Controls.Add(this.txtTSVatGiam);
            this.uiTabPage1.Controls.Add(this.txtTSTTDBGiam);
            this.uiTabPage1.Controls.Add(this.txtTSXNKGiam);
            this.uiTabPage1.Controls.Add(this.label21);
            this.uiTabPage1.Controls.Add(this.label24);
            this.uiTabPage1.Controls.Add(this.label25);
            this.uiTabPage1.Controls.Add(this.txtTongSoTienThue);
            this.uiTabPage1.Controls.Add(this.label20);
            this.uiTabPage1.Controls.Add(this.txtTienThue_TTDB);
            this.uiTabPage1.Controls.Add(this.txtTienThue_GTGT);
            this.uiTabPage1.Controls.Add(this.txtTien_CLG);
            this.uiTabPage1.Controls.Add(this.txtTienThue_NK);
            this.uiTabPage1.Controls.Add(this.txtTGTT_TTDB);
            this.uiTabPage1.Controls.Add(this.txtTGTT_GTGT);
            this.uiTabPage1.Controls.Add(this.txtCLG);
            this.uiTabPage1.Controls.Add(this.txtTGTT_NK);
            this.uiTabPage1.Controls.Add(this.txtTL_CLG);
            this.uiTabPage1.Controls.Add(this.txtTS_TTDB);
            this.uiTabPage1.Controls.Add(this.txtTS_GTGT);
            this.uiTabPage1.Controls.Add(this.txtTS_NK);
            this.uiTabPage1.Controls.Add(this.label16);
            this.uiTabPage1.Controls.Add(this.label17);
            this.uiTabPage1.Controls.Add(this.label19);
            this.uiTabPage1.Controls.Add(this.label1);
            this.uiTabPage1.Controls.Add(this.label5);
            this.uiTabPage1.Controls.Add(this.label9);
            this.uiTabPage1.Controls.Add(this.label10);
            this.uiTabPage1.Controls.Add(this.label11);
            this.uiTabPage1.Controls.Add(this.label12);
            this.uiTabPage1.Controls.Add(this.label13);
            this.uiTabPage1.Controls.Add(this.label14);
            this.uiTabPage1.Controls.Add(this.label15);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(701, 174);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thuế XNK ";
            // 
            // txtTriGiaThuKhac
            // 
            this.txtTriGiaThuKhac.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTriGiaThuKhac.DecimalDigits = 3;
            this.txtTriGiaThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaThuKhac.Location = new System.Drawing.Point(176, 126);
            this.txtTriGiaThuKhac.Name = "txtTriGiaThuKhac";
            this.txtTriGiaThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaThuKhac.Size = new System.Drawing.Size(166, 21);
            this.txtTriGiaThuKhac.TabIndex = 60;
            this.txtTriGiaThuKhac.TabStop = false;
            this.txtTriGiaThuKhac.Text = "0.000";
            this.txtTriGiaThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTriGiaThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTriGiaThuKhac.VisualStyleManager = this.vsmain;
            // 
            // vsmain
            // 
            janusColorScheme3.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme3.Name = "Office2003";
            janusColorScheme3.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme4.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme4.Name = "Office2007";
            janusColorScheme4.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme4.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.vsmain.ColorSchemes.Add(janusColorScheme3);
            this.vsmain.ColorSchemes.Add(janusColorScheme4);
            this.vsmain.DefaultColorScheme = "Office2007";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(175, 110);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 13);
            this.label29.TabIndex = 70;
            this.label29.Text = "Tiền thu khác";
            // 
            // txtTyLeThuKhac
            // 
            this.txtTyLeThuKhac.DecimalDigits = 8;
            this.txtTyLeThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyLeThuKhac.Location = new System.Drawing.Point(11, 126);
            this.txtTyLeThuKhac.Name = "txtTyLeThuKhac";
            this.txtTyLeThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyLeThuKhac.Size = new System.Drawing.Size(158, 21);
            this.txtTyLeThuKhac.TabIndex = 58;
            this.txtTyLeThuKhac.TabStop = false;
            this.txtTyLeThuKhac.Text = "0.00000000";
            this.txtTyLeThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyLeThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            524288});
            this.txtTyLeThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTyLeThuKhac.VisualStyleManager = this.vsmain;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(8, 110);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(74, 13);
            this.label30.TabIndex = 69;
            this.label30.Text = "Tỷ lệ thu khác";
            // 
            // txtTSVatGiam
            // 
            this.txtTSVatGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSVatGiam.Location = new System.Drawing.Point(176, 81);
            this.txtTSVatGiam.MaxLength = 80;
            this.txtTSVatGiam.Name = "txtTSVatGiam";
            this.txtTSVatGiam.Size = new System.Drawing.Size(49, 21);
            this.txtTSVatGiam.TabIndex = 50;
            this.txtTSVatGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSVatGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSVatGiam.VisualStyleManager = this.vsmain;
            // 
            // txtTSTTDBGiam
            // 
            this.txtTSTTDBGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSTTDBGiam.Location = new System.Drawing.Point(513, 38);
            this.txtTSTTDBGiam.MaxLength = 80;
            this.txtTSTTDBGiam.Name = "txtTSTTDBGiam";
            this.txtTSTTDBGiam.Size = new System.Drawing.Size(49, 21);
            this.txtTSTTDBGiam.TabIndex = 44;
            this.txtTSTTDBGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSTTDBGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSTTDBGiam.VisualStyleManager = this.vsmain;
            // 
            // txtTSXNKGiam
            // 
            this.txtTSXNKGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSXNKGiam.Location = new System.Drawing.Point(175, 38);
            this.txtTSXNKGiam.MaxLength = 80;
            this.txtTSXNKGiam.Name = "txtTSXNKGiam";
            this.txtTSXNKGiam.Size = new System.Drawing.Size(49, 21);
            this.txtTSXNKGiam.TabIndex = 39;
            this.txtTSXNKGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSXNKGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSXNKGiam.VisualStyleManager = this.vsmain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(175, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 13);
            this.label21.TabIndex = 42;
            this.label21.Text = "TS Giảm";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(512, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 13);
            this.label24.TabIndex = 54;
            this.label24.Text = "TS Giảm";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(175, 65);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(45, 13);
            this.label25.TabIndex = 63;
            this.label25.Text = "TS Giảm";
            // 
            // txtTongSoTienThue
            // 
            this.txtTongSoTienThue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTongSoTienThue.DecimalDigits = 0;
            this.txtTongSoTienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTienThue.FormatString = "###,###,###.########";
            this.txtTongSoTienThue.Location = new System.Drawing.Point(468, 125);
            this.txtTongSoTienThue.Name = "txtTongSoTienThue";
            this.txtTongSoTienThue.ReadOnly = true;
            this.txtTongSoTienThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoTienThue.Size = new System.Drawing.Size(211, 21);
            this.txtTongSoTienThue.TabIndex = 61;
            this.txtTongSoTienThue.TabStop = false;
            this.txtTongSoTienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoTienThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoTienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongSoTienThue.VisualStyleManager = this.vsmain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(365, 130);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 13);
            this.label20.TabIndex = 68;
            this.label20.Text = "Tổng số tiền thuế";
            // 
            // txtTienThue_TTDB
            // 
            this.txtTienThue_TTDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_TTDB.DecimalDigits = 0;
            this.txtTienThue_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_TTDB.Location = new System.Drawing.Point(569, 38);
            this.txtTienThue_TTDB.Name = "txtTienThue_TTDB";
            this.txtTienThue_TTDB.ReadOnly = true;
            this.txtTienThue_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_TTDB.Size = new System.Drawing.Size(110, 21);
            this.txtTienThue_TTDB.TabIndex = 46;
            this.txtTienThue_TTDB.TabStop = false;
            this.txtTienThue_TTDB.Text = "0";
            this.txtTienThue_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTienThue_TTDB.VisualStyleManager = this.vsmain;
            // 
            // txtTienThue_GTGT
            // 
            this.txtTienThue_GTGT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_GTGT.DecimalDigits = 0;
            this.txtTienThue_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_GTGT.Location = new System.Drawing.Point(232, 81);
            this.txtTienThue_GTGT.Name = "txtTienThue_GTGT";
            this.txtTienThue_GTGT.ReadOnly = true;
            this.txtTienThue_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_GTGT.Size = new System.Drawing.Size(110, 21);
            this.txtTienThue_GTGT.TabIndex = 52;
            this.txtTienThue_GTGT.TabStop = false;
            this.txtTienThue_GTGT.Text = "0";
            this.txtTienThue_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTienThue_GTGT.VisualStyleManager = this.vsmain;
            // 
            // txtTien_CLG
            // 
            this.txtTien_CLG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTien_CLG.DecimalDigits = 0;
            this.txtTien_CLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTien_CLG.Location = new System.Drawing.Point(569, 81);
            this.txtTien_CLG.Name = "txtTien_CLG";
            this.txtTien_CLG.ReadOnly = true;
            this.txtTien_CLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTien_CLG.Size = new System.Drawing.Size(110, 21);
            this.txtTien_CLG.TabIndex = 56;
            this.txtTien_CLG.TabStop = false;
            this.txtTien_CLG.Text = "0";
            this.txtTien_CLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTien_CLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTien_CLG.Visible = false;
            this.txtTien_CLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTien_CLG.VisualStyleManager = this.vsmain;
            // 
            // txtTienThue_NK
            // 
            this.txtTienThue_NK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_NK.DecimalDigits = 0;
            this.txtTienThue_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_NK.Location = new System.Drawing.Point(232, 38);
            this.txtTienThue_NK.Name = "txtTienThue_NK";
            this.txtTienThue_NK.ReadOnly = true;
            this.txtTienThue_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_NK.Size = new System.Drawing.Size(107, 21);
            this.txtTienThue_NK.TabIndex = 40;
            this.txtTienThue_NK.TabStop = false;
            this.txtTienThue_NK.Text = "0";
            this.txtTienThue_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTienThue_NK.VisualStyleManager = this.vsmain;
            // 
            // txtTGTT_TTDB
            // 
            this.txtTGTT_TTDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTGTT_TTDB.DecimalDigits = 0;
            this.txtTGTT_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_TTDB.Location = new System.Drawing.Point(348, 38);
            this.txtTGTT_TTDB.Name = "txtTGTT_TTDB";
            this.txtTGTT_TTDB.ReadOnly = true;
            this.txtTGTT_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_TTDB.Size = new System.Drawing.Size(108, 21);
            this.txtTGTT_TTDB.TabIndex = 41;
            this.txtTGTT_TTDB.TabStop = false;
            this.txtTGTT_TTDB.Text = "0";
            this.txtTGTT_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTGTT_TTDB.VisualStyleManager = this.vsmain;
            // 
            // txtTGTT_GTGT
            // 
            this.txtTGTT_GTGT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTGTT_GTGT.DecimalDigits = 0;
            this.txtTGTT_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_GTGT.Location = new System.Drawing.Point(11, 81);
            this.txtTGTT_GTGT.Name = "txtTGTT_GTGT";
            this.txtTGTT_GTGT.ReadOnly = true;
            this.txtTGTT_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_GTGT.Size = new System.Drawing.Size(108, 21);
            this.txtTGTT_GTGT.TabIndex = 47;
            this.txtTGTT_GTGT.TabStop = false;
            this.txtTGTT_GTGT.Text = "0";
            this.txtTGTT_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTGTT_GTGT.VisualStyleManager = this.vsmain;
            // 
            // txtCLG
            // 
            this.txtCLG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCLG.DecimalDigits = 0;
            this.txtCLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCLG.Location = new System.Drawing.Point(348, 81);
            this.txtCLG.Name = "txtCLG";
            this.txtCLG.ReadOnly = true;
            this.txtCLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCLG.Size = new System.Drawing.Size(108, 21);
            this.txtCLG.TabIndex = 53;
            this.txtCLG.TabStop = false;
            this.txtCLG.Text = "0";
            this.txtCLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtCLG.VisualStyleManager = this.vsmain;
            // 
            // txtTGTT_NK
            // 
            this.txtTGTT_NK.BackColor = System.Drawing.Color.White;
            this.txtTGTT_NK.DecimalDigits = 0;
            this.txtTGTT_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_NK.Location = new System.Drawing.Point(11, 38);
            this.txtTGTT_NK.Name = "txtTGTT_NK";
            this.txtTGTT_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_NK.Size = new System.Drawing.Size(108, 21);
            this.txtTGTT_NK.TabIndex = 35;
            this.txtTGTT_NK.TabStop = false;
            this.txtTGTT_NK.Text = "0";
            this.txtTGTT_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTGTT_NK.VisualStyleManager = this.vsmain;
            // 
            // txtTL_CLG
            // 
            this.txtTL_CLG.DecimalDigits = 3;
            this.txtTL_CLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTL_CLG.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTL_CLG.Location = new System.Drawing.Point(465, 81);
            this.txtTL_CLG.MaxLength = 10;
            this.txtTL_CLG.Name = "txtTL_CLG";
            this.txtTL_CLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTL_CLG.Size = new System.Drawing.Size(41, 21);
            this.txtTL_CLG.TabIndex = 55;
            this.txtTL_CLG.Text = "0";
            this.txtTL_CLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTL_CLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTL_CLG.Visible = false;
            this.txtTL_CLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTL_CLG.VisualStyleManager = this.vsmain;
            // 
            // txtTS_TTDB
            // 
            this.txtTS_TTDB.DecimalDigits = 3;
            this.txtTS_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_TTDB.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTS_TTDB.Location = new System.Drawing.Point(465, 38);
            this.txtTS_TTDB.MaxLength = 10;
            this.txtTS_TTDB.Name = "txtTS_TTDB";
            this.txtTS_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_TTDB.Size = new System.Drawing.Size(41, 21);
            this.txtTS_TTDB.TabIndex = 43;
            this.txtTS_TTDB.Text = "0";
            this.txtTS_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTS_TTDB.VisualStyleManager = this.vsmain;
            // 
            // txtTS_GTGT
            // 
            this.txtTS_GTGT.DecimalDigits = 3;
            this.txtTS_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_GTGT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTS_GTGT.Location = new System.Drawing.Point(128, 81);
            this.txtTS_GTGT.MaxLength = 10;
            this.txtTS_GTGT.Name = "txtTS_GTGT";
            this.txtTS_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_GTGT.Size = new System.Drawing.Size(41, 21);
            this.txtTS_GTGT.TabIndex = 49;
            this.txtTS_GTGT.Text = "0";
            this.txtTS_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTS_GTGT.VisualStyleManager = this.vsmain;
            // 
            // txtTS_NK
            // 
            this.txtTS_NK.DecimalDigits = 3;
            this.txtTS_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_NK.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTS_NK.Location = new System.Drawing.Point(128, 38);
            this.txtTS_NK.MaxLength = 10;
            this.txtTS_NK.Name = "txtTS_NK";
            this.txtTS_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_NK.Size = new System.Drawing.Size(41, 21);
            this.txtTS_NK.TabIndex = 37;
            this.txtTS_NK.Text = "0";
            this.txtTS_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTS_NK.VisualStyleManager = this.vsmain;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 36;
            this.label16.Text = "Trị giá tính thuế NK";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(229, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 13);
            this.label17.TabIndex = 45;
            this.label17.Text = "Tiền thuế NK";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(128, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 13);
            this.label19.TabIndex = 38;
            this.label19.Text = "TS (%)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(345, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "Trị giá tính thuế TTĐB";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 13);
            this.label5.TabIndex = 59;
            this.label5.Text = "Trị giá tính thuế GTGT";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(345, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 65;
            this.label9.Text = "Chênh lệch giá";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(465, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 51;
            this.label10.Text = "TS (%)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(128, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 62;
            this.label11.Text = "TS (%)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(465, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 66;
            this.label12.Text = "Tỷ lệ (%)";
            this.label12.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(566, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 57;
            this.label13.Text = "Tiền thuế TTĐB";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(229, 65);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 13);
            this.label14.TabIndex = 64;
            this.label14.Text = "Tiền thuế GTGT";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(566, 65);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(98, 13);
            this.label15.TabIndex = 67;
            this.label15.Text = "Tiền chênh lệch giá";
            this.label15.Visible = false;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.txtTienThueCBPG);
            this.uiTabPage2.Controls.Add(this.txtTienThueBVMT);
            this.uiTabPage2.Controls.Add(this.txtTriGiaTTCBPG);
            this.uiTabPage2.Controls.Add(this.txtTriGiaTinhThueBVMT);
            this.uiTabPage2.Controls.Add(this.txtTSCBPG);
            this.uiTabPage2.Controls.Add(this.txtTSBVMT);
            this.uiTabPage2.Controls.Add(this.label4);
            this.uiTabPage2.Controls.Add(this.label6);
            this.uiTabPage2.Controls.Add(this.label7);
            this.uiTabPage2.Controls.Add(this.label2);
            this.uiTabPage2.Controls.Add(this.label8);
            this.uiTabPage2.Controls.Add(this.label18);
            this.uiTabPage2.Controls.Add(this.label22);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(701, 174);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thuế mở rộng";
            // 
            // txtTienThueCBPG
            // 
            this.txtTienThueCBPG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThueCBPG.DecimalDigits = 0;
            this.txtTienThueCBPG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThueCBPG.Location = new System.Drawing.Point(340, 89);
            this.txtTienThueCBPG.Name = "txtTienThueCBPG";
            this.txtTienThueCBPG.ReadOnly = true;
            this.txtTienThueCBPG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThueCBPG.Size = new System.Drawing.Size(158, 21);
            this.txtTienThueCBPG.TabIndex = 69;
            this.txtTienThueCBPG.TabStop = false;
            this.txtTienThueCBPG.Text = "0";
            this.txtTienThueCBPG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThueCBPG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThueCBPG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTienThueCBPG.VisualStyleManager = this.vsmain;
            // 
            // txtTienThueBVMT
            // 
            this.txtTienThueBVMT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThueBVMT.DecimalDigits = 0;
            this.txtTienThueBVMT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThueBVMT.Location = new System.Drawing.Point(340, 36);
            this.txtTienThueBVMT.Name = "txtTienThueBVMT";
            this.txtTienThueBVMT.ReadOnly = true;
            this.txtTienThueBVMT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThueBVMT.Size = new System.Drawing.Size(158, 21);
            this.txtTienThueBVMT.TabIndex = 63;
            this.txtTienThueBVMT.TabStop = false;
            this.txtTienThueBVMT.Text = "0";
            this.txtTienThueBVMT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThueBVMT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThueBVMT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTienThueBVMT.VisualStyleManager = this.vsmain;
            // 
            // txtTriGiaTTCBPG
            // 
            this.txtTriGiaTTCBPG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTriGiaTTCBPG.DecimalDigits = 0;
            this.txtTriGiaTTCBPG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTTCBPG.Location = new System.Drawing.Point(6, 89);
            this.txtTriGiaTTCBPG.Name = "txtTriGiaTTCBPG";
            this.txtTriGiaTTCBPG.ReadOnly = true;
            this.txtTriGiaTTCBPG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTTCBPG.Size = new System.Drawing.Size(158, 21);
            this.txtTriGiaTTCBPG.TabIndex = 64;
            this.txtTriGiaTTCBPG.TabStop = false;
            this.txtTriGiaTTCBPG.Text = "0";
            this.txtTriGiaTTCBPG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTTCBPG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTTCBPG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTriGiaTTCBPG.VisualStyleManager = this.vsmain;
            // 
            // txtTriGiaTinhThueBVMT
            // 
            this.txtTriGiaTinhThueBVMT.BackColor = System.Drawing.Color.White;
            this.txtTriGiaTinhThueBVMT.DecimalDigits = 0;
            this.txtTriGiaTinhThueBVMT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThueBVMT.Location = new System.Drawing.Point(6, 36);
            this.txtTriGiaTinhThueBVMT.Name = "txtTriGiaTinhThueBVMT";
            this.txtTriGiaTinhThueBVMT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThueBVMT.Size = new System.Drawing.Size(158, 21);
            this.txtTriGiaTinhThueBVMT.TabIndex = 58;
            this.txtTriGiaTinhThueBVMT.TabStop = false;
            this.txtTriGiaTinhThueBVMT.Text = "0";
            this.txtTriGiaTinhThueBVMT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThueBVMT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTinhThueBVMT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTriGiaTinhThueBVMT.VisualStyleManager = this.vsmain;
            // 
            // txtTSCBPG
            // 
            this.txtTSCBPG.DecimalDigits = 3;
            this.txtTSCBPG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSCBPG.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTSCBPG.Location = new System.Drawing.Point(189, 89);
            this.txtTSCBPG.MaxLength = 10;
            this.txtTSCBPG.Name = "txtTSCBPG";
            this.txtTSCBPG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTSCBPG.Size = new System.Drawing.Size(41, 21);
            this.txtTSCBPG.TabIndex = 66;
            this.txtTSCBPG.Text = "0";
            this.txtTSCBPG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSCBPG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTSCBPG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSCBPG.VisualStyleManager = this.vsmain;
            // 
            // txtTSBVMT
            // 
            this.txtTSBVMT.DecimalDigits = 3;
            this.txtTSBVMT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSBVMT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTSBVMT.Location = new System.Drawing.Point(189, 36);
            this.txtTSBVMT.MaxLength = 10;
            this.txtTSBVMT.Name = "txtTSBVMT";
            this.txtTSBVMT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTSBVMT.Size = new System.Drawing.Size(41, 21);
            this.txtTSBVMT.TabIndex = 60;
            this.txtTSBVMT.Text = "0";
            this.txtTSBVMT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSBVMT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTSBVMT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSBVMT.VisualStyleManager = this.vsmain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 59;
            this.label4.Text = "Trị giá tính thuế BVMT";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(337, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 68;
            this.label6.Text = "Tiền Thuế BVMT";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(186, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 13);
            this.label7.TabIndex = 61;
            this.label7.Text = "Thuế suất BVMT (%)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(4, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(265, 13);
            this.label2.TabIndex = 70;
            this.label2.Text = "Nếu không có thuế suất, vui lòng để thuế suất bằng 0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(4, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(173, 13);
            this.label8.TabIndex = 70;
            this.label8.Text = "Trị giá tính thuế chống bán phá giá";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(186, 73);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(107, 13);
            this.label18.TabIndex = 71;
            this.label18.Text = "Thuế Suất CBPG (%)";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(337, 73);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(143, 13);
            this.label22.TabIndex = 73;
            this.label22.Text = "Tiền thuế chống bán phá giá";
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.gridEX1);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(701, 174);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Lệ phí HQ";
            // 
            // gridEX1
            // 
            gridEX1_DesignTimeLayout.LayoutString = resources.GetString("gridEX1_DesignTimeLayout.LayoutString");
            this.gridEX1.DesignTimeLayout = gridEX1_DesignTimeLayout;
            this.gridEX1.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridEX1.Location = new System.Drawing.Point(0, 0);
            this.gridEX1.Name = "gridEX1";
            this.gridEX1.Size = new System.Drawing.Size(701, 151);
            this.gridEX1.TabIndex = 0;
            this.gridEX1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridEX1.VisualStyleManager = this.vsmain;
            // 
            // ThueControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.uiTab1);
            this.Name = "ThueControl";
            this.Size = new System.Drawing.Size(703, 196);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.uiTabPage1.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            this.uiTabPage2.PerformLayout();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEX1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.Tab.UITab uiTab1;
        public Janus.Windows.Common.VisualStyleManager vsmain;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.GridEX gridEX1;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaThuKhac;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTyLeThuKhac;
        public Janus.Windows.GridEX.EditControls.EditBox txtTSVatGiam;
        public Janus.Windows.GridEX.EditControls.EditBox txtTSTTDBGiam;
        public Janus.Windows.GridEX.EditControls.EditBox txtTSXNKGiam;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoTienThue;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTienThue_TTDB;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTienThue_GTGT;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTien_CLG;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTienThue_NK;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTGTT_TTDB;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTGTT_GTGT;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtCLG;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTGTT_NK;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTL_CLG;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTS_TTDB;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTS_GTGT;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTS_NK;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTienThueCBPG;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTienThueBVMT;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTTCBPG;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThueBVMT;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTSCBPG;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTSBVMT;
    }
}
