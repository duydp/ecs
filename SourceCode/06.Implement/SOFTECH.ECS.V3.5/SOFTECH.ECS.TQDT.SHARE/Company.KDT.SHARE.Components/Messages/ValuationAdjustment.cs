﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class ValuationAdjustment
    {
        /// <summary>
        /// Mã loại giá trị n..4
        /// </summary>
        [XmlElement("addition")]
        public string Addition { get; set; }
        /// <summary>
        /// ...
        /// </summary>
        [XmlElement("deduction")]
        public string Deduction { get; set; }
        /// <summary>
        /// Phần trăm giá trị n..10,2
        /// </summary>
        [XmlElement("percentage")]
        public string Percentage { get; set; }
        /// <summary>
        ///  n..16,2
        /// </summary>
        [XmlElement("amount")]
        public string Amount { get; set; }


    }
}
