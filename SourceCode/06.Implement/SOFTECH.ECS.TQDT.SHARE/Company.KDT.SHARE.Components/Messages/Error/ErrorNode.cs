﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{

    public class ErrorNode
    {
        [XmlText()]
        public string Text { get; set; }

        [XmlElement("Content")]
        public Content Content { get; set; }
    }
}
