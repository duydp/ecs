﻿namespace Company.Interface.VNACCS.Vouchers
{
    partial class VNACC_VouchersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grListCO_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_VouchersForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout grListVanDon_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem13 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem14 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout gridEX1_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListHopDong_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridEX2_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListHoaDon_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridEX3_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListGiayPhep_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridEX4_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListContainer_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridEX5_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListCTKhac_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridEX6_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListDKLNG_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridEX7_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdEdit1 = new Janus.Windows.UI.CommandBars.UICommand("cmdEdit");
            this.cmdCancel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdEdit = new Janus.Windows.UI.CommandBars.UICommand("cmdEdit");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.tbCtrVouchers = new System.Windows.Forms.TabControl();
            this.tpCO = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.grListCO = new Janus.Windows.GridEX.GridEX();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnViewCO = new System.Windows.Forms.Button();
            this.btnDeleteCO = new System.Windows.Forms.Button();
            this.btnAddCO = new System.Windows.Forms.Button();
            this.lblFileNameCO = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateNgayCapCO = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbbLoaiCO = new Janus.Windows.EditControls.UIComboBox();
            this.txtNuocCapCO = new Company.Interface.Controls.NuocHControl();
            this.txtGhiChuCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNguoiCapCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtToChucCapCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tpVanDon = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.grListVanDon = new Janus.Windows.GridEX.GridEX();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblFileNameVanDon = new System.Windows.Forms.Label();
            this.btnViewVanDon = new System.Windows.Forms.Button();
            this.btnDeleteVanDon = new System.Windows.Forms.Button();
            this.btnAddVanDon = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dtpNgayVanDon = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbbLoaiVanDon = new Janus.Windows.EditControls.UIComboBox();
            this.ctrNuocPHVanDon = new Company.Interface.Controls.NuocHControl();
            this.txtGhiChuVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaDiemCTQC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.gridEX1 = new Janus.Windows.GridEX.GridEX();
            this.tpHopDong = new System.Windows.Forms.TabPage();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.grListHopDong = new Janus.Windows.GridEX.GridEX();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnViewHopDong = new System.Windows.Forms.Button();
            this.btnDeleteHopDong = new System.Windows.Forms.Button();
            this.btnAddHopDong = new System.Windows.Forms.Button();
            this.lblFileNameHopDong = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dtpNgayHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtpThoiHanThanhToan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtGhiChuHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTongTriGiaHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.gridEX2 = new Janus.Windows.GridEX.GridEX();
            this.tpHoaDon = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.grListHoaDon = new Janus.Windows.GridEX.GridEX();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btnViewHoaDon = new System.Windows.Forms.Button();
            this.btnDeleteHoaDon = new System.Windows.Forms.Button();
            this.btnAddHoaDon = new System.Windows.Forms.Button();
            this.lblFileNameHoaDon = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.dtpNgayPhatHanhHoaDonTM = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtGhiChuHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHoaDonTM = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.gridEX3 = new Janus.Windows.GridEX.GridEX();
            this.tpGiayPhep = new System.Windows.Forms.TabPage();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.grListGiayPhep = new Janus.Windows.GridEX.GridEX();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.btnViewGiayPhep = new System.Windows.Forms.Button();
            this.btnDeleteGiayPhep = new System.Windows.Forms.Button();
            this.btnAddGiayPhep = new System.Windows.Forms.Button();
            this.lblFileNameGiayPhep = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.dtpNgayHetHanGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtpNgayCapGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtLoaiGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNguoiCapGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChuGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNoiCapGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.gridEX4 = new Janus.Windows.GridEX.GridEX();
            this.tpContainer = new System.Windows.Forms.TabPage();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.grListContainer = new Janus.Windows.GridEX.GridEX();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.btnViewContainer = new System.Windows.Forms.Button();
            this.btnDeleteContainer = new System.Windows.Forms.Button();
            this.btnAddContainer = new System.Windows.Forms.Button();
            this.lblFileNamContainer = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.dtpNgayDangKyToKhai = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtGhiChuContainer = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.gridEX5 = new Janus.Windows.GridEX.GridEX();
            this.tpCTKhac = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.grListCTKhac = new Janus.Windows.GridEX.GridEX();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.btnViewCTKhac = new System.Windows.Forms.Button();
            this.btnDeleteCTKhac = new System.Windows.Forms.Button();
            this.btnAddCTKhac = new System.Windows.Forms.Button();
            this.lblFileNameCTKhac = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.dtpNgayChungTuKhac = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtTenChungTuKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChuCTKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNoiPhatHanhCTKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoChungTuKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.gridEX6 = new Janus.Windows.GridEX.GridEX();
            this.tpLamNgoaiGio = new System.Windows.Forms.TabPage();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.grListDKLNG = new Janus.Windows.GridEX.GridEX();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.dtpGioLamTT = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtpNgayDangKyThuTuc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.btnDeleteOverTime = new System.Windows.Forms.Button();
            this.txtNoiDung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.gridEX7 = new Janus.Windows.GridEX.GridEX();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.ctrMaLoaiHinh = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrCoQuanHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.tbCtrVouchers.SuspendLayout();
            this.tpCO.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListCO)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tpVanDon.SuspendLayout();
            this.groupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListVanDon)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX1)).BeginInit();
            this.tpHopDong.SuspendLayout();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListHopDong)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX2)).BeginInit();
            this.tpHoaDon.SuspendLayout();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListHoaDon)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX3)).BeginInit();
            this.tpGiayPhep.SuspendLayout();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListGiayPhep)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX4)).BeginInit();
            this.tpContainer.SuspendLayout();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListContainer)).BeginInit();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX5)).BeginInit();
            this.tpCTKhac.SuspendLayout();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListCTKhac)).BeginInit();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX6)).BeginInit();
            this.tpLamNgoaiGio.SuspendLayout();
            this.groupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListDKLNG)).BeginInit();
            this.groupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX7)).BeginInit();
            this.groupBox16.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.groupBox16);
            this.grbMain.Controls.Add(this.tbCtrVouchers);
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(1042, 510);
            this.grbMain.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbMain.VisualStyleManager = this.vsmMain;
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSave,
            this.cmdFeedback,
            this.cmdResult,
            this.cmdEdit,
            this.cmdCancel});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend1,
            this.cmdSave1,
            this.cmdFeedback1,
            this.cmdResult1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(401, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend1.Icon")));
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSave1.Icon")));
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdFeedback1.Icon")));
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdResult1.Icon")));
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdSend
            // 
            this.cmdSend.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdEdit1,
            this.cmdCancel1});
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "KHAI BÁO";
            // 
            // cmdEdit1
            // 
            this.cmdEdit1.Key = "cmdEdit";
            this.cmdEdit1.Name = "cmdEdit1";
            // 
            // cmdCancel1
            // 
            this.cmdCancel1.Key = "cmdCancel";
            this.cmdCancel1.Name = "cmdCancel1";
            // 
            // cmdSave
            // 
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "LƯU LẠI";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "LẤY PHẢN HỒI";
            // 
            // cmdResult
            // 
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "KẾT QUẢ XỬ LÝ";
            // 
            // cmdEdit
            // 
            this.cmdEdit.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdEdit.Icon")));
            this.cmdEdit.Key = "cmdEdit";
            this.cmdEdit.Name = "cmdEdit";
            this.cmdEdit.Text = "KHAI BÁO SỬA ĐĂNG KÝ LÀM NGOÀI GIỜ";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCancel.Icon")));
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Text = "KHAI BÁO HỦY ĐĂNG KÝ LÀM NGOÀI GIỜ";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1042, 28);
            // 
            // tbCtrVouchers
            // 
            this.tbCtrVouchers.Controls.Add(this.tpCO);
            this.tbCtrVouchers.Controls.Add(this.tpVanDon);
            this.tbCtrVouchers.Controls.Add(this.tpHopDong);
            this.tbCtrVouchers.Controls.Add(this.tpHoaDon);
            this.tbCtrVouchers.Controls.Add(this.tpGiayPhep);
            this.tbCtrVouchers.Controls.Add(this.tpContainer);
            this.tbCtrVouchers.Controls.Add(this.tpCTKhac);
            this.tbCtrVouchers.Controls.Add(this.tpLamNgoaiGio);
            this.tbCtrVouchers.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbCtrVouchers.Location = new System.Drawing.Point(0, 0);
            this.tbCtrVouchers.Name = "tbCtrVouchers";
            this.tbCtrVouchers.Padding = new System.Drawing.Point(0, 0);
            this.tbCtrVouchers.SelectedIndex = 0;
            this.tbCtrVouchers.Size = new System.Drawing.Size(1042, 482);
            this.tbCtrVouchers.TabIndex = 0;
            this.tbCtrVouchers.Selected += new System.Windows.Forms.TabControlEventHandler(this.tbCtrVouchers_Selected);
            // 
            // tpCO
            // 
            this.tpCO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tpCO.Controls.Add(this.groupBox3);
            this.tpCO.Controls.Add(this.groupBox2);
            this.tpCO.Controls.Add(this.groupBox1);
            this.tpCO.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.tpCO.Location = new System.Drawing.Point(4, 22);
            this.tpCO.Margin = new System.Windows.Forms.Padding(0);
            this.tpCO.Name = "tpCO";
            this.tpCO.Size = new System.Drawing.Size(1034, 456);
            this.tpCO.TabIndex = 0;
            this.tpCO.Text = "KHAI BÁO C/O";
            this.tpCO.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.grListCO);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox3.Location = new System.Drawing.Point(0, 223);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1034, 233);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "DANH SÁCH";
            // 
            // grListCO
            // 
            this.grListCO.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListCO.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListCO.ColumnAutoResize = true;
            grListCO_DesignTimeLayout.LayoutString = resources.GetString("grListCO_DesignTimeLayout.LayoutString");
            this.grListCO.DesignTimeLayout = grListCO_DesignTimeLayout;
            this.grListCO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListCO.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListCO.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListCO.FrozenColumns = 5;
            this.grListCO.GroupByBoxVisible = false;
            this.grListCO.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListCO.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListCO.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListCO.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListCO.Hierarchical = true;
            this.grListCO.Location = new System.Drawing.Point(3, 17);
            this.grListCO.Name = "grListCO";
            this.grListCO.RecordNavigator = true;
            this.grListCO.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListCO.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListCO.Size = new System.Drawing.Size(1028, 213);
            this.grListCO.TabIndex = 81;
            this.grListCO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListCO.VisualStyleManager = this.vsmMain;
            this.grListCO.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListCO_RowDoubleClick);
            this.grListCO.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListCO_LoadingRow);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.btnViewCO);
            this.groupBox2.Controls.Add(this.btnDeleteCO);
            this.groupBox2.Controls.Add(this.btnAddCO);
            this.groupBox2.Controls.Add(this.lblFileNameCO);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox2.Location = new System.Drawing.Point(0, 181);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1034, 42);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "FILE ĐÍNH KÈM";
            // 
            // btnViewCO
            // 
            this.btnViewCO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnViewCO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewCO.ForeColor = System.Drawing.Color.White;
            this.btnViewCO.Location = new System.Drawing.Point(825, 11);
            this.btnViewCO.Name = "btnViewCO";
            this.btnViewCO.Size = new System.Drawing.Size(100, 25);
            this.btnViewCO.TabIndex = 83;
            this.btnViewCO.Text = "XEM FILE";
            this.btnViewCO.UseVisualStyleBackColor = false;
            this.btnViewCO.Click += new System.EventHandler(this.btnViewCO_Click);
            // 
            // btnDeleteCO
            // 
            this.btnDeleteCO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnDeleteCO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteCO.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnDeleteCO.ForeColor = System.Drawing.Color.White;
            this.btnDeleteCO.Location = new System.Drawing.Point(927, 11);
            this.btnDeleteCO.Name = "btnDeleteCO";
            this.btnDeleteCO.Size = new System.Drawing.Size(100, 25);
            this.btnDeleteCO.TabIndex = 82;
            this.btnDeleteCO.Text = "XÓA";
            this.btnDeleteCO.UseVisualStyleBackColor = false;
            this.btnDeleteCO.Click += new System.EventHandler(this.btnDeleteCO_Click);
            // 
            // btnAddCO
            // 
            this.btnAddCO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnAddCO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCO.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAddCO.ForeColor = System.Drawing.Color.White;
            this.btnAddCO.Location = new System.Drawing.Point(720, 11);
            this.btnAddCO.Name = "btnAddCO";
            this.btnAddCO.Size = new System.Drawing.Size(100, 25);
            this.btnAddCO.TabIndex = 81;
            this.btnAddCO.Text = "THÊM FILE";
            this.btnAddCO.UseVisualStyleBackColor = false;
            this.btnAddCO.Click += new System.EventHandler(this.btnAddCO_Click);
            // 
            // lblFileNameCO
            // 
            this.lblFileNameCO.AutoSize = true;
            this.lblFileNameCO.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNameCO.Location = new System.Drawing.Point(122, 18);
            this.lblFileNameCO.Name = "lblFileNameCO";
            this.lblFileNameCO.Size = new System.Drawing.Size(0, 13);
            this.lblFileNameCO.TabIndex = 43;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(19, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 43;
            this.label1.Text = "FILE NAME :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.dateNgayCapCO);
            this.groupBox1.Controls.Add(this.cbbLoaiCO);
            this.groupBox1.Controls.Add(this.txtNuocCapCO);
            this.groupBox1.Controls.Add(this.txtGhiChuCO);
            this.groupBox1.Controls.Add(this.txtNguoiCapCO);
            this.groupBox1.Controls.Add(this.txtToChucCapCO);
            this.groupBox1.Controls.Add(this.txtSoCO);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1034, 181);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CHI TIẾT C/O";
            // 
            // dateNgayCapCO
            // 
            this.dateNgayCapCO.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dateNgayCapCO.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dateNgayCapCO.DropDownCalendar.Name = "";
            this.dateNgayCapCO.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dateNgayCapCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateNgayCapCO.Location = new System.Drawing.Point(125, 62);
            this.dateNgayCapCO.Name = "dateNgayCapCO";
            this.dateNgayCapCO.Nullable = true;
            this.dateNgayCapCO.NullButtonText = "Xóa";
            this.dateNgayCapCO.ShowNullButton = true;
            this.dateNgayCapCO.Size = new System.Drawing.Size(159, 21);
            this.dateNgayCapCO.TabIndex = 55;
            this.dateNgayCapCO.TodayButtonText = "Hôm nay";
            this.dateNgayCapCO.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dateNgayCapCO.VisualStyleManager = this.vsmMain;
            // 
            // cbbLoaiCO
            // 
            this.cbbLoaiCO.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Form D";
            uiComboBoxItem1.Value = "D";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Form E";
            uiComboBoxItem2.Value = "E";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Form AK";
            uiComboBoxItem3.Value = "AK";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Form S";
            uiComboBoxItem4.Value = "S";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Form AJ";
            uiComboBoxItem5.Value = "AJ";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Form JV";
            uiComboBoxItem6.Value = "JV";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Form AANZ";
            uiComboBoxItem7.Value = "AANZ";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Form AI";
            uiComboBoxItem8.Value = "AI";
            this.cbbLoaiCO.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8});
            this.cbbLoaiCO.Location = new System.Drawing.Point(720, 23);
            this.cbbLoaiCO.Name = "cbbLoaiCO";
            this.cbbLoaiCO.Size = new System.Drawing.Size(254, 21);
            this.cbbLoaiCO.TabIndex = 54;
            this.cbbLoaiCO.VisualStyleManager = this.vsmMain;
            // 
            // txtNuocCapCO
            // 
            this.txtNuocCapCO.BackColor = System.Drawing.Color.Transparent;
            this.txtNuocCapCO.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.txtNuocCapCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNuocCapCO.Location = new System.Drawing.Point(720, 62);
            this.txtNuocCapCO.Ma = "";
            this.txtNuocCapCO.Name = "txtNuocCapCO";
            this.txtNuocCapCO.ReadOnly = false;
            this.txtNuocCapCO.Size = new System.Drawing.Size(267, 22);
            this.txtNuocCapCO.TabIndex = 53;
            this.txtNuocCapCO.VisualStyleManager = null;
            // 
            // txtGhiChuCO
            // 
            this.txtGhiChuCO.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtGhiChuCO.Location = new System.Drawing.Point(721, 104);
            this.txtGhiChuCO.Multiline = true;
            this.txtGhiChuCO.Name = "txtGhiChuCO";
            this.txtGhiChuCO.Size = new System.Drawing.Size(254, 61);
            this.txtGhiChuCO.TabIndex = 51;
            this.txtGhiChuCO.VisualStyleManager = this.vsmMain;
            // 
            // txtNguoiCapCO
            // 
            this.txtNguoiCapCO.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtNguoiCapCO.Location = new System.Drawing.Point(125, 144);
            this.txtNguoiCapCO.Name = "txtNguoiCapCO";
            this.txtNguoiCapCO.Size = new System.Drawing.Size(470, 21);
            this.txtNguoiCapCO.TabIndex = 50;
            this.txtNguoiCapCO.VisualStyleManager = this.vsmMain;
            // 
            // txtToChucCapCO
            // 
            this.txtToChucCapCO.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtToChucCapCO.Location = new System.Drawing.Point(125, 104);
            this.txtToChucCapCO.Name = "txtToChucCapCO";
            this.txtToChucCapCO.Size = new System.Drawing.Size(470, 21);
            this.txtToChucCapCO.TabIndex = 49;
            this.txtToChucCapCO.VisualStyleManager = this.vsmMain;
            // 
            // txtSoCO
            // 
            this.txtSoCO.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtSoCO.Location = new System.Drawing.Point(125, 23);
            this.txtSoCO.Name = "txtSoCO";
            this.txtSoCO.Size = new System.Drawing.Size(470, 21);
            this.txtSoCO.TabIndex = 48;
            this.txtSoCO.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(21, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 47;
            this.label5.Text = "Người cấp C/O :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(20, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 42;
            this.label4.Text = "Tổ chức cấp C/O :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(19, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 41;
            this.label3.Text = "Ngày cấp C/O :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(631, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 13);
            this.label11.TabIndex = 46;
            this.label11.Text = "Ghi chú khác :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(631, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 45;
            this.label10.Text = "Nước cấp C/O :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(631, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 13);
            this.label9.TabIndex = 44;
            this.label9.Text = "Loại C/O :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.label2.Location = new System.Drawing.Point(19, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "Số C/O :";
            // 
            // tpVanDon
            // 
            this.tpVanDon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tpVanDon.Controls.Add(this.groupBox18);
            this.tpVanDon.Controls.Add(this.groupBox4);
            this.tpVanDon.Controls.Add(this.groupBox5);
            this.tpVanDon.Controls.Add(this.gridEX1);
            this.tpVanDon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.tpVanDon.Location = new System.Drawing.Point(4, 22);
            this.tpVanDon.Margin = new System.Windows.Forms.Padding(0);
            this.tpVanDon.Name = "tpVanDon";
            this.tpVanDon.Size = new System.Drawing.Size(1034, 456);
            this.tpVanDon.TabIndex = 1;
            this.tpVanDon.Text = "KHAI BÁO VẬN ĐƠN";
            this.tpVanDon.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.grListVanDon);
            this.groupBox18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox18.Location = new System.Drawing.Point(0, 223);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(1034, 233);
            this.groupBox18.TabIndex = 85;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "DANH SÁCH";
            // 
            // grListVanDon
            // 
            this.grListVanDon.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListVanDon.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListVanDon.ColumnAutoResize = true;
            grListVanDon_DesignTimeLayout.LayoutString = resources.GetString("grListVanDon_DesignTimeLayout.LayoutString");
            this.grListVanDon.DesignTimeLayout = grListVanDon_DesignTimeLayout;
            this.grListVanDon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListVanDon.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListVanDon.FrozenColumns = 5;
            this.grListVanDon.GroupByBoxVisible = false;
            this.grListVanDon.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListVanDon.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListVanDon.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListVanDon.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListVanDon.Hierarchical = true;
            this.grListVanDon.Location = new System.Drawing.Point(3, 17);
            this.grListVanDon.Name = "grListVanDon";
            this.grListVanDon.RecordNavigator = true;
            this.grListVanDon.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListVanDon.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListVanDon.Size = new System.Drawing.Size(1028, 213);
            this.grListVanDon.TabIndex = 86;
            this.grListVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListVanDon.VisualStyleManager = this.vsmMain;
            this.grListVanDon.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListVanDon_RowDoubleClick);
            this.grListVanDon.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListVanDon_LoadingRow);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.lblFileNameVanDon);
            this.groupBox4.Controls.Add(this.btnViewVanDon);
            this.groupBox4.Controls.Add(this.btnDeleteVanDon);
            this.groupBox4.Controls.Add(this.btnAddVanDon);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox4.Location = new System.Drawing.Point(0, 181);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1034, 42);
            this.groupBox4.TabIndex = 83;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "FILE ĐÍNH KÈM";
            // 
            // lblFileNameVanDon
            // 
            this.lblFileNameVanDon.AutoSize = true;
            this.lblFileNameVanDon.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNameVanDon.Location = new System.Drawing.Point(122, 18);
            this.lblFileNameVanDon.Name = "lblFileNameVanDon";
            this.lblFileNameVanDon.Size = new System.Drawing.Size(0, 13);
            this.lblFileNameVanDon.TabIndex = 84;
            // 
            // btnViewVanDon
            // 
            this.btnViewVanDon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnViewVanDon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewVanDon.ForeColor = System.Drawing.Color.White;
            this.btnViewVanDon.Location = new System.Drawing.Point(825, 11);
            this.btnViewVanDon.Name = "btnViewVanDon";
            this.btnViewVanDon.Size = new System.Drawing.Size(100, 25);
            this.btnViewVanDon.TabIndex = 83;
            this.btnViewVanDon.Text = "XEM FILE";
            this.btnViewVanDon.UseVisualStyleBackColor = false;
            this.btnViewVanDon.Click += new System.EventHandler(this.btnViewVanDon_Click);
            // 
            // btnDeleteVanDon
            // 
            this.btnDeleteVanDon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnDeleteVanDon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteVanDon.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteVanDon.ForeColor = System.Drawing.Color.White;
            this.btnDeleteVanDon.Location = new System.Drawing.Point(927, 11);
            this.btnDeleteVanDon.Name = "btnDeleteVanDon";
            this.btnDeleteVanDon.Size = new System.Drawing.Size(100, 25);
            this.btnDeleteVanDon.TabIndex = 82;
            this.btnDeleteVanDon.Text = "XÓA";
            this.btnDeleteVanDon.UseVisualStyleBackColor = false;
            this.btnDeleteVanDon.Click += new System.EventHandler(this.btnDeleteVanDon_Click);
            // 
            // btnAddVanDon
            // 
            this.btnAddVanDon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnAddVanDon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddVanDon.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddVanDon.ForeColor = System.Drawing.Color.White;
            this.btnAddVanDon.Location = new System.Drawing.Point(720, 11);
            this.btnAddVanDon.Name = "btnAddVanDon";
            this.btnAddVanDon.Size = new System.Drawing.Size(100, 25);
            this.btnAddVanDon.TabIndex = 81;
            this.btnAddVanDon.Text = "THÊM FILE";
            this.btnAddVanDon.UseVisualStyleBackColor = false;
            this.btnAddVanDon.Click += new System.EventHandler(this.btnAddVanDon_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(19, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 43;
            this.label6.Text = "FILE NAME :";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.dtpNgayVanDon);
            this.groupBox5.Controls.Add(this.cbbLoaiVanDon);
            this.groupBox5.Controls.Add(this.ctrNuocPHVanDon);
            this.groupBox5.Controls.Add(this.txtGhiChuVanDon);
            this.groupBox5.Controls.Add(this.txtDiaDiemCTQC);
            this.groupBox5.Controls.Add(this.txtSoVanDon);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1034, 181);
            this.groupBox5.TabIndex = 82;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "CHI TIẾT VẬN ĐƠN";
            // 
            // dtpNgayVanDon
            // 
            this.dtpNgayVanDon.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayVanDon.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayVanDon.DropDownCalendar.Name = "";
            this.dtpNgayVanDon.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayVanDon.Location = new System.Drawing.Point(714, 23);
            this.dtpNgayVanDon.Name = "dtpNgayVanDon";
            this.dtpNgayVanDon.Nullable = true;
            this.dtpNgayVanDon.NullButtonText = "Xóa";
            this.dtpNgayVanDon.ShowNullButton = true;
            this.dtpNgayVanDon.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayVanDon.TabIndex = 55;
            this.dtpNgayVanDon.TodayButtonText = "Hôm nay";
            this.dtpNgayVanDon.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayVanDon.VisualStyleManager = this.vsmMain;
            // 
            // cbbLoaiVanDon
            // 
            this.cbbLoaiVanDon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Đường biển";
            uiComboBoxItem9.Value = 1;
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Đường không";
            uiComboBoxItem10.Value = 2;
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Đường bộ";
            uiComboBoxItem11.Value = 3;
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.IsSeparator = true;
            uiComboBoxItem12.Text = "Đường sắt";
            uiComboBoxItem12.Value = 4;
            uiComboBoxItem13.FormatStyle.Alpha = 0;
            uiComboBoxItem13.IsSeparator = false;
            uiComboBoxItem13.Text = "Đường sông";
            uiComboBoxItem13.Value = 5;
            uiComboBoxItem14.FormatStyle.Alpha = 0;
            uiComboBoxItem14.IsSeparator = false;
            uiComboBoxItem14.Text = "Loại khác";
            uiComboBoxItem14.Value = 9;
            this.cbbLoaiVanDon.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem9,
            uiComboBoxItem10,
            uiComboBoxItem11,
            uiComboBoxItem12,
            uiComboBoxItem13,
            uiComboBoxItem14});
            this.cbbLoaiVanDon.Location = new System.Drawing.Point(714, 58);
            this.cbbLoaiVanDon.Name = "cbbLoaiVanDon";
            this.cbbLoaiVanDon.Size = new System.Drawing.Size(159, 21);
            this.cbbLoaiVanDon.TabIndex = 54;
            this.cbbLoaiVanDon.VisualStyleManager = this.vsmMain;
            // 
            // ctrNuocPHVanDon
            // 
            this.ctrNuocPHVanDon.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocPHVanDon.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrNuocPHVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocPHVanDon.Location = new System.Drawing.Point(125, 63);
            this.ctrNuocPHVanDon.Ma = "";
            this.ctrNuocPHVanDon.Name = "ctrNuocPHVanDon";
            this.ctrNuocPHVanDon.ReadOnly = false;
            this.ctrNuocPHVanDon.Size = new System.Drawing.Size(267, 22);
            this.ctrNuocPHVanDon.TabIndex = 53;
            this.ctrNuocPHVanDon.VisualStyleManager = null;
            // 
            // txtGhiChuVanDon
            // 
            this.txtGhiChuVanDon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtGhiChuVanDon.Location = new System.Drawing.Point(125, 143);
            this.txtGhiChuVanDon.Multiline = true;
            this.txtGhiChuVanDon.Name = "txtGhiChuVanDon";
            this.txtGhiChuVanDon.Size = new System.Drawing.Size(843, 21);
            this.txtGhiChuVanDon.TabIndex = 51;
            this.txtGhiChuVanDon.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaDiemCTQC
            // 
            this.txtDiaDiemCTQC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtDiaDiemCTQC.Location = new System.Drawing.Point(211, 104);
            this.txtDiaDiemCTQC.Name = "txtDiaDiemCTQC";
            this.txtDiaDiemCTQC.Size = new System.Drawing.Size(757, 21);
            this.txtDiaDiemCTQC.TabIndex = 49;
            this.txtDiaDiemCTQC.VisualStyleManager = this.vsmMain;
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtSoVanDon.Location = new System.Drawing.Point(125, 23);
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(443, 21);
            this.txtSoVanDon.TabIndex = 48;
            this.txtSoVanDon.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(20, 109);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(185, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "Địa điểm chuyển tải /quá cảnh :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(610, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 13);
            this.label12.TabIndex = 41;
            this.label12.Text = "Ngày vận đơn :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(24, 143);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 13);
            this.label13.TabIndex = 46;
            this.label13.Text = "Ghi chú khác :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(19, 72);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 13);
            this.label14.TabIndex = 45;
            this.label14.Text = "Nước phát hành :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(610, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 13);
            this.label15.TabIndex = 44;
            this.label15.Text = "Loại vận đơn :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(19, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 43;
            this.label16.Text = "Số vận đơn :";
            // 
            // gridEX1
            // 
            this.gridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridEX1.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.gridEX1.ColumnAutoResize = true;
            gridEX1_DesignTimeLayout.LayoutString = resources.GetString("gridEX1_DesignTimeLayout.LayoutString");
            this.gridEX1.DesignTimeLayout = gridEX1_DesignTimeLayout;
            this.gridEX1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEX1.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridEX1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX1.FrozenColumns = 5;
            this.gridEX1.GroupByBoxVisible = false;
            this.gridEX1.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX1.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.gridEX1.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX1.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX1.Hierarchical = true;
            this.gridEX1.Location = new System.Drawing.Point(0, 0);
            this.gridEX1.Name = "gridEX1";
            this.gridEX1.RecordNavigator = true;
            this.gridEX1.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridEX1.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX1.Size = new System.Drawing.Size(1034, 456);
            this.gridEX1.TabIndex = 84;
            this.gridEX1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridEX1.VisualStyleManager = this.vsmMain;
            // 
            // tpHopDong
            // 
            this.tpHopDong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tpHopDong.Controls.Add(this.groupBox19);
            this.tpHopDong.Controls.Add(this.groupBox6);
            this.tpHopDong.Controls.Add(this.groupBox7);
            this.tpHopDong.Controls.Add(this.gridEX2);
            this.tpHopDong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.tpHopDong.Location = new System.Drawing.Point(4, 22);
            this.tpHopDong.Name = "tpHopDong";
            this.tpHopDong.Size = new System.Drawing.Size(1034, 456);
            this.tpHopDong.TabIndex = 2;
            this.tpHopDong.Text = "KHAI BÁO HỢP ĐỒNG";
            this.tpHopDong.UseVisualStyleBackColor = true;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.grListHopDong);
            this.groupBox19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox19.Location = new System.Drawing.Point(0, 184);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(1034, 272);
            this.groupBox19.TabIndex = 85;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "DANH SÁCH";
            // 
            // grListHopDong
            // 
            this.grListHopDong.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListHopDong.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListHopDong.ColumnAutoResize = true;
            grListHopDong_DesignTimeLayout.LayoutString = resources.GetString("grListHopDong_DesignTimeLayout.LayoutString");
            this.grListHopDong.DesignTimeLayout = grListHopDong_DesignTimeLayout;
            this.grListHopDong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListHopDong.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListHopDong.FrozenColumns = 5;
            this.grListHopDong.GroupByBoxVisible = false;
            this.grListHopDong.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHopDong.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListHopDong.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHopDong.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListHopDong.Hierarchical = true;
            this.grListHopDong.Location = new System.Drawing.Point(3, 17);
            this.grListHopDong.Name = "grListHopDong";
            this.grListHopDong.RecordNavigator = true;
            this.grListHopDong.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListHopDong.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHopDong.Size = new System.Drawing.Size(1028, 252);
            this.grListHopDong.TabIndex = 87;
            this.grListHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListHopDong.VisualStyleManager = this.vsmMain;
            this.grListHopDong.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListHopDong_RowDoubleClick);
            this.grListHopDong.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListHopDong_LoadingRow);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.btnViewHopDong);
            this.groupBox6.Controls.Add(this.btnDeleteHopDong);
            this.groupBox6.Controls.Add(this.btnAddHopDong);
            this.groupBox6.Controls.Add(this.lblFileNameHopDong);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox6.Location = new System.Drawing.Point(0, 142);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1034, 42);
            this.groupBox6.TabIndex = 83;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "FILE ĐÍNH KÈM";
            // 
            // btnViewHopDong
            // 
            this.btnViewHopDong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnViewHopDong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewHopDong.ForeColor = System.Drawing.Color.White;
            this.btnViewHopDong.Location = new System.Drawing.Point(825, 11);
            this.btnViewHopDong.Name = "btnViewHopDong";
            this.btnViewHopDong.Size = new System.Drawing.Size(100, 25);
            this.btnViewHopDong.TabIndex = 83;
            this.btnViewHopDong.Text = "XEM FILE";
            this.btnViewHopDong.UseVisualStyleBackColor = false;
            this.btnViewHopDong.Click += new System.EventHandler(this.btnViewHopDong_Click);
            // 
            // btnDeleteHopDong
            // 
            this.btnDeleteHopDong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnDeleteHopDong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteHopDong.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnDeleteHopDong.ForeColor = System.Drawing.Color.White;
            this.btnDeleteHopDong.Location = new System.Drawing.Point(927, 11);
            this.btnDeleteHopDong.Name = "btnDeleteHopDong";
            this.btnDeleteHopDong.Size = new System.Drawing.Size(100, 25);
            this.btnDeleteHopDong.TabIndex = 82;
            this.btnDeleteHopDong.Text = "XÓA";
            this.btnDeleteHopDong.UseVisualStyleBackColor = false;
            this.btnDeleteHopDong.Click += new System.EventHandler(this.btnDeleteHopDong_Click);
            // 
            // btnAddHopDong
            // 
            this.btnAddHopDong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnAddHopDong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddHopDong.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAddHopDong.ForeColor = System.Drawing.Color.White;
            this.btnAddHopDong.Location = new System.Drawing.Point(720, 11);
            this.btnAddHopDong.Name = "btnAddHopDong";
            this.btnAddHopDong.Size = new System.Drawing.Size(100, 25);
            this.btnAddHopDong.TabIndex = 81;
            this.btnAddHopDong.Text = "THÊM FILE";
            this.btnAddHopDong.UseVisualStyleBackColor = false;
            this.btnAddHopDong.Click += new System.EventHandler(this.btnAddHopDong_Click);
            // 
            // lblFileNameHopDong
            // 
            this.lblFileNameHopDong.AutoSize = true;
            this.lblFileNameHopDong.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNameHopDong.Location = new System.Drawing.Point(122, 17);
            this.lblFileNameHopDong.Name = "lblFileNameHopDong";
            this.lblFileNameHopDong.Size = new System.Drawing.Size(0, 13);
            this.lblFileNameHopDong.TabIndex = 43;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(19, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 43;
            this.label17.Text = "FILE NAME :";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Transparent;
            this.groupBox7.Controls.Add(this.dtpNgayHopDong);
            this.groupBox7.Controls.Add(this.dtpThoiHanThanhToan);
            this.groupBox7.Controls.Add(this.txtGhiChuHopDong);
            this.groupBox7.Controls.Add(this.txtTongTriGiaHopDong);
            this.groupBox7.Controls.Add(this.txtSoHopDong);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Controls.Add(this.label24);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox7.Location = new System.Drawing.Point(0, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1034, 142);
            this.groupBox7.TabIndex = 82;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "CHI TIẾT HỢP ĐỒNG";
            // 
            // dtpNgayHopDong
            // 
            this.dtpNgayHopDong.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayHopDong.DropDownCalendar.Name = "";
            this.dtpNgayHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHopDong.Location = new System.Drawing.Point(586, 20);
            this.dtpNgayHopDong.Name = "dtpNgayHopDong";
            this.dtpNgayHopDong.Nullable = true;
            this.dtpNgayHopDong.NullButtonText = "Xóa";
            this.dtpNgayHopDong.ShowNullButton = true;
            this.dtpNgayHopDong.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayHopDong.TabIndex = 53;
            this.dtpNgayHopDong.TodayButtonText = "Hôm nay";
            this.dtpNgayHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayHopDong.VisualStyleManager = this.vsmMain;
            // 
            // dtpThoiHanThanhToan
            // 
            this.dtpThoiHanThanhToan.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpThoiHanThanhToan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpThoiHanThanhToan.DropDownCalendar.Name = "";
            this.dtpThoiHanThanhToan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpThoiHanThanhToan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpThoiHanThanhToan.Location = new System.Drawing.Point(151, 59);
            this.dtpThoiHanThanhToan.Name = "dtpThoiHanThanhToan";
            this.dtpThoiHanThanhToan.Nullable = true;
            this.dtpThoiHanThanhToan.NullButtonText = "Xóa";
            this.dtpThoiHanThanhToan.ShowNullButton = true;
            this.dtpThoiHanThanhToan.Size = new System.Drawing.Size(159, 21);
            this.dtpThoiHanThanhToan.TabIndex = 52;
            this.dtpThoiHanThanhToan.TodayButtonText = "Hôm nay";
            this.dtpThoiHanThanhToan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpThoiHanThanhToan.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChuHopDong
            // 
            this.txtGhiChuHopDong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtGhiChuHopDong.Location = new System.Drawing.Point(125, 104);
            this.txtGhiChuHopDong.Multiline = true;
            this.txtGhiChuHopDong.Name = "txtGhiChuHopDong";
            this.txtGhiChuHopDong.Size = new System.Drawing.Size(804, 21);
            this.txtGhiChuHopDong.TabIndex = 51;
            this.txtGhiChuHopDong.VisualStyleManager = this.vsmMain;
            // 
            // txtTongTriGiaHopDong
            // 
            this.txtTongTriGiaHopDong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtTongTriGiaHopDong.Location = new System.Drawing.Point(586, 62);
            this.txtTongTriGiaHopDong.Name = "txtTongTriGiaHopDong";
            this.txtTongTriGiaHopDong.Size = new System.Drawing.Size(343, 21);
            this.txtTongTriGiaHopDong.TabIndex = 49;
            this.txtTongTriGiaHopDong.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtSoHopDong.Location = new System.Drawing.Point(125, 23);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(343, 21);
            this.txtSoHopDong.TabIndex = 48;
            this.txtSoHopDong.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(481, 67);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 13);
            this.label19.TabIndex = 42;
            this.label19.Text = "Tổng trị giá :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(19, 70);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(126, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "Thời hạn thanh toán :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(19, 104);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 13);
            this.label21.TabIndex = 46;
            this.label21.Text = "Ghi chú khác :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Location = new System.Drawing.Point(484, 28);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(96, 13);
            this.label23.TabIndex = 44;
            this.label23.Text = "Ngày hợp đồng :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(19, 28);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 13);
            this.label24.TabIndex = 43;
            this.label24.Text = "Số hợp đồng :";
            // 
            // gridEX2
            // 
            this.gridEX2.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridEX2.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.gridEX2.ColumnAutoResize = true;
            gridEX2_DesignTimeLayout.LayoutString = resources.GetString("gridEX2_DesignTimeLayout.LayoutString");
            this.gridEX2.DesignTimeLayout = gridEX2_DesignTimeLayout;
            this.gridEX2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEX2.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridEX2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX2.FrozenColumns = 5;
            this.gridEX2.GroupByBoxVisible = false;
            this.gridEX2.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX2.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.gridEX2.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX2.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX2.Hierarchical = true;
            this.gridEX2.Location = new System.Drawing.Point(0, 0);
            this.gridEX2.Name = "gridEX2";
            this.gridEX2.RecordNavigator = true;
            this.gridEX2.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridEX2.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX2.Size = new System.Drawing.Size(1034, 456);
            this.gridEX2.TabIndex = 84;
            this.gridEX2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridEX2.VisualStyleManager = this.vsmMain;
            // 
            // tpHoaDon
            // 
            this.tpHoaDon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tpHoaDon.Controls.Add(this.groupBox20);
            this.tpHoaDon.Controls.Add(this.groupBox8);
            this.tpHoaDon.Controls.Add(this.groupBox9);
            this.tpHoaDon.Controls.Add(this.gridEX3);
            this.tpHoaDon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.tpHoaDon.Location = new System.Drawing.Point(4, 22);
            this.tpHoaDon.Name = "tpHoaDon";
            this.tpHoaDon.Size = new System.Drawing.Size(1034, 456);
            this.tpHoaDon.TabIndex = 3;
            this.tpHoaDon.Text = "KHAI BÁO HÓA ĐƠN";
            this.tpHoaDon.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.grListHoaDon);
            this.groupBox20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox20.Location = new System.Drawing.Point(0, 137);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(1034, 319);
            this.groupBox20.TabIndex = 85;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "DANH SÁCH";
            // 
            // grListHoaDon
            // 
            this.grListHoaDon.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListHoaDon.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListHoaDon.ColumnAutoResize = true;
            grListHoaDon_DesignTimeLayout.LayoutString = resources.GetString("grListHoaDon_DesignTimeLayout.LayoutString");
            this.grListHoaDon.DesignTimeLayout = grListHoaDon_DesignTimeLayout;
            this.grListHoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListHoaDon.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListHoaDon.FrozenColumns = 5;
            this.grListHoaDon.GroupByBoxVisible = false;
            this.grListHoaDon.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHoaDon.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListHoaDon.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHoaDon.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListHoaDon.Hierarchical = true;
            this.grListHoaDon.Location = new System.Drawing.Point(3, 17);
            this.grListHoaDon.Name = "grListHoaDon";
            this.grListHoaDon.RecordNavigator = true;
            this.grListHoaDon.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListHoaDon.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHoaDon.Size = new System.Drawing.Size(1028, 299);
            this.grListHoaDon.TabIndex = 89;
            this.grListHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListHoaDon.VisualStyleManager = this.vsmMain;
            this.grListHoaDon.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListHoaDon_RowDoubleClick);
            this.grListHoaDon.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListHoaDon_LoadingRow);
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Transparent;
            this.groupBox8.Controls.Add(this.btnViewHoaDon);
            this.groupBox8.Controls.Add(this.btnDeleteHoaDon);
            this.groupBox8.Controls.Add(this.btnAddHoaDon);
            this.groupBox8.Controls.Add(this.lblFileNameHoaDon);
            this.groupBox8.Controls.Add(this.label25);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox8.Location = new System.Drawing.Point(0, 95);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(1034, 42);
            this.groupBox8.TabIndex = 83;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "FILE ĐÍNH KÈM";
            // 
            // btnViewHoaDon
            // 
            this.btnViewHoaDon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnViewHoaDon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewHoaDon.ForeColor = System.Drawing.Color.White;
            this.btnViewHoaDon.Location = new System.Drawing.Point(825, 11);
            this.btnViewHoaDon.Name = "btnViewHoaDon";
            this.btnViewHoaDon.Size = new System.Drawing.Size(100, 25);
            this.btnViewHoaDon.TabIndex = 83;
            this.btnViewHoaDon.Text = "XEM FILE";
            this.btnViewHoaDon.UseVisualStyleBackColor = false;
            this.btnViewHoaDon.Click += new System.EventHandler(this.btnViewHoaDon_Click);
            // 
            // btnDeleteHoaDon
            // 
            this.btnDeleteHoaDon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnDeleteHoaDon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteHoaDon.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnDeleteHoaDon.ForeColor = System.Drawing.Color.White;
            this.btnDeleteHoaDon.Location = new System.Drawing.Point(927, 11);
            this.btnDeleteHoaDon.Name = "btnDeleteHoaDon";
            this.btnDeleteHoaDon.Size = new System.Drawing.Size(100, 25);
            this.btnDeleteHoaDon.TabIndex = 82;
            this.btnDeleteHoaDon.Text = "XÓA";
            this.btnDeleteHoaDon.UseVisualStyleBackColor = false;
            this.btnDeleteHoaDon.Click += new System.EventHandler(this.btnDeleteHoaDon_Click);
            // 
            // btnAddHoaDon
            // 
            this.btnAddHoaDon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnAddHoaDon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddHoaDon.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAddHoaDon.ForeColor = System.Drawing.Color.White;
            this.btnAddHoaDon.Location = new System.Drawing.Point(720, 11);
            this.btnAddHoaDon.Name = "btnAddHoaDon";
            this.btnAddHoaDon.Size = new System.Drawing.Size(100, 25);
            this.btnAddHoaDon.TabIndex = 81;
            this.btnAddHoaDon.Text = "THÊM FILE";
            this.btnAddHoaDon.UseVisualStyleBackColor = false;
            this.btnAddHoaDon.Click += new System.EventHandler(this.btnAddHoaDon_Click);
            // 
            // lblFileNameHoaDon
            // 
            this.lblFileNameHoaDon.AutoSize = true;
            this.lblFileNameHoaDon.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNameHoaDon.Location = new System.Drawing.Point(168, 18);
            this.lblFileNameHoaDon.Name = "lblFileNameHoaDon";
            this.lblFileNameHoaDon.Size = new System.Drawing.Size(0, 13);
            this.lblFileNameHoaDon.TabIndex = 43;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(19, 18);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(70, 13);
            this.label25.TabIndex = 43;
            this.label25.Text = "FILE NAME :";
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.Transparent;
            this.groupBox9.Controls.Add(this.dtpNgayPhatHanhHoaDonTM);
            this.groupBox9.Controls.Add(this.txtGhiChuHoaDon);
            this.groupBox9.Controls.Add(this.txtSoHoaDonTM);
            this.groupBox9.Controls.Add(this.label29);
            this.groupBox9.Controls.Add(this.label31);
            this.groupBox9.Controls.Add(this.label32);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox9.Location = new System.Drawing.Point(0, 0);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(1034, 95);
            this.groupBox9.TabIndex = 82;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "CHI TIẾT HÓA ĐƠN";
            // 
            // dtpNgayPhatHanhHoaDonTM
            // 
            this.dtpNgayPhatHanhHoaDonTM.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayPhatHanhHoaDonTM.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayPhatHanhHoaDonTM.DropDownCalendar.Name = "";
            this.dtpNgayPhatHanhHoaDonTM.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayPhatHanhHoaDonTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayPhatHanhHoaDonTM.Location = new System.Drawing.Point(833, 18);
            this.dtpNgayPhatHanhHoaDonTM.Name = "dtpNgayPhatHanhHoaDonTM";
            this.dtpNgayPhatHanhHoaDonTM.Nullable = true;
            this.dtpNgayPhatHanhHoaDonTM.NullButtonText = "Xóa";
            this.dtpNgayPhatHanhHoaDonTM.ShowNullButton = true;
            this.dtpNgayPhatHanhHoaDonTM.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayPhatHanhHoaDonTM.TabIndex = 54;
            this.dtpNgayPhatHanhHoaDonTM.TodayButtonText = "Hôm nay";
            this.dtpNgayPhatHanhHoaDonTM.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayPhatHanhHoaDonTM.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChuHoaDon
            // 
            this.txtGhiChuHoaDon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtGhiChuHoaDon.Location = new System.Drawing.Point(171, 53);
            this.txtGhiChuHoaDon.Name = "txtGhiChuHoaDon";
            this.txtGhiChuHoaDon.Size = new System.Drawing.Size(420, 21);
            this.txtGhiChuHoaDon.TabIndex = 53;
            this.txtGhiChuHoaDon.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHoaDonTM
            // 
            this.txtSoHoaDonTM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtSoHoaDonTM.Location = new System.Drawing.Point(171, 18);
            this.txtSoHoaDonTM.Name = "txtSoHoaDonTM";
            this.txtSoHoaDonTM.Size = new System.Drawing.Size(420, 21);
            this.txtSoHoaDonTM.TabIndex = 48;
            this.txtSoHoaDonTM.VisualStyleManager = this.vsmMain;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Location = new System.Drawing.Point(21, 61);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(84, 13);
            this.label29.TabIndex = 46;
            this.label29.Text = "Ghi chú khác :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Location = new System.Drawing.Point(610, 23);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(217, 13);
            this.label31.TabIndex = 44;
            this.label31.Text = "Ngày phát hành hóa đơn thương mại :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Location = new System.Drawing.Point(19, 28);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(146, 13);
            this.label32.TabIndex = 43;
            this.label32.Text = "Số hóa đơn thương mại  :";
            // 
            // gridEX3
            // 
            this.gridEX3.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridEX3.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.gridEX3.ColumnAutoResize = true;
            gridEX3_DesignTimeLayout.LayoutString = resources.GetString("gridEX3_DesignTimeLayout.LayoutString");
            this.gridEX3.DesignTimeLayout = gridEX3_DesignTimeLayout;
            this.gridEX3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEX3.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridEX3.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX3.FrozenColumns = 5;
            this.gridEX3.GroupByBoxVisible = false;
            this.gridEX3.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX3.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.gridEX3.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX3.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX3.Hierarchical = true;
            this.gridEX3.Location = new System.Drawing.Point(0, 0);
            this.gridEX3.Name = "gridEX3";
            this.gridEX3.RecordNavigator = true;
            this.gridEX3.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridEX3.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX3.Size = new System.Drawing.Size(1034, 456);
            this.gridEX3.TabIndex = 84;
            this.gridEX3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridEX3.VisualStyleManager = this.vsmMain;
            // 
            // tpGiayPhep
            // 
            this.tpGiayPhep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tpGiayPhep.Controls.Add(this.groupBox22);
            this.tpGiayPhep.Controls.Add(this.groupBox10);
            this.tpGiayPhep.Controls.Add(this.groupBox11);
            this.tpGiayPhep.Controls.Add(this.gridEX4);
            this.tpGiayPhep.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.tpGiayPhep.Location = new System.Drawing.Point(4, 22);
            this.tpGiayPhep.Name = "tpGiayPhep";
            this.tpGiayPhep.Size = new System.Drawing.Size(1034, 456);
            this.tpGiayPhep.TabIndex = 4;
            this.tpGiayPhep.Text = "KHAI BÁO GIẤY PHÉP";
            this.tpGiayPhep.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.grListGiayPhep);
            this.groupBox22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox22.Location = new System.Drawing.Point(0, 223);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(1034, 233);
            this.groupBox22.TabIndex = 85;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "DANH SÁCH";
            // 
            // grListGiayPhep
            // 
            this.grListGiayPhep.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListGiayPhep.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListGiayPhep.ColumnAutoResize = true;
            grListGiayPhep_DesignTimeLayout.LayoutString = resources.GetString("grListGiayPhep_DesignTimeLayout.LayoutString");
            this.grListGiayPhep.DesignTimeLayout = grListGiayPhep_DesignTimeLayout;
            this.grListGiayPhep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListGiayPhep.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListGiayPhep.FrozenColumns = 5;
            this.grListGiayPhep.GroupByBoxVisible = false;
            this.grListGiayPhep.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListGiayPhep.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListGiayPhep.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListGiayPhep.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListGiayPhep.Hierarchical = true;
            this.grListGiayPhep.Location = new System.Drawing.Point(3, 17);
            this.grListGiayPhep.Name = "grListGiayPhep";
            this.grListGiayPhep.RecordNavigator = true;
            this.grListGiayPhep.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListGiayPhep.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListGiayPhep.Size = new System.Drawing.Size(1028, 213);
            this.grListGiayPhep.TabIndex = 89;
            this.grListGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListGiayPhep.VisualStyleManager = this.vsmMain;
            this.grListGiayPhep.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListGiayPhep_RowDoubleClick);
            this.grListGiayPhep.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListGiayPhep_LoadingRow);
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.Transparent;
            this.groupBox10.Controls.Add(this.btnViewGiayPhep);
            this.groupBox10.Controls.Add(this.btnDeleteGiayPhep);
            this.groupBox10.Controls.Add(this.btnAddGiayPhep);
            this.groupBox10.Controls.Add(this.lblFileNameGiayPhep);
            this.groupBox10.Controls.Add(this.label33);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox10.Location = new System.Drawing.Point(0, 181);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(1034, 42);
            this.groupBox10.TabIndex = 83;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "FILE ĐÍNH KÈM";
            // 
            // btnViewGiayPhep
            // 
            this.btnViewGiayPhep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnViewGiayPhep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewGiayPhep.ForeColor = System.Drawing.Color.White;
            this.btnViewGiayPhep.Location = new System.Drawing.Point(825, 11);
            this.btnViewGiayPhep.Name = "btnViewGiayPhep";
            this.btnViewGiayPhep.Size = new System.Drawing.Size(100, 25);
            this.btnViewGiayPhep.TabIndex = 83;
            this.btnViewGiayPhep.Text = "XEM FILE";
            this.btnViewGiayPhep.UseVisualStyleBackColor = false;
            this.btnViewGiayPhep.Click += new System.EventHandler(this.btnViewGiayPhep_Click);
            // 
            // btnDeleteGiayPhep
            // 
            this.btnDeleteGiayPhep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnDeleteGiayPhep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteGiayPhep.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnDeleteGiayPhep.ForeColor = System.Drawing.Color.White;
            this.btnDeleteGiayPhep.Location = new System.Drawing.Point(927, 11);
            this.btnDeleteGiayPhep.Name = "btnDeleteGiayPhep";
            this.btnDeleteGiayPhep.Size = new System.Drawing.Size(100, 25);
            this.btnDeleteGiayPhep.TabIndex = 82;
            this.btnDeleteGiayPhep.Text = "XÓA";
            this.btnDeleteGiayPhep.UseVisualStyleBackColor = false;
            this.btnDeleteGiayPhep.Click += new System.EventHandler(this.btnDeleteGiayPhep_Click);
            // 
            // btnAddGiayPhep
            // 
            this.btnAddGiayPhep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnAddGiayPhep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddGiayPhep.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAddGiayPhep.ForeColor = System.Drawing.Color.White;
            this.btnAddGiayPhep.Location = new System.Drawing.Point(720, 11);
            this.btnAddGiayPhep.Name = "btnAddGiayPhep";
            this.btnAddGiayPhep.Size = new System.Drawing.Size(100, 25);
            this.btnAddGiayPhep.TabIndex = 81;
            this.btnAddGiayPhep.Text = "THÊM FILE";
            this.btnAddGiayPhep.UseVisualStyleBackColor = false;
            this.btnAddGiayPhep.Click += new System.EventHandler(this.btnAddGiayPhep_Click);
            // 
            // lblFileNameGiayPhep
            // 
            this.lblFileNameGiayPhep.AutoSize = true;
            this.lblFileNameGiayPhep.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNameGiayPhep.Location = new System.Drawing.Point(150, 17);
            this.lblFileNameGiayPhep.Name = "lblFileNameGiayPhep";
            this.lblFileNameGiayPhep.Size = new System.Drawing.Size(0, 13);
            this.lblFileNameGiayPhep.TabIndex = 43;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Location = new System.Drawing.Point(19, 18);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(70, 13);
            this.label33.TabIndex = 43;
            this.label33.Text = "FILE NAME :";
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.Transparent;
            this.groupBox11.Controls.Add(this.dtpNgayHetHanGiayPhep);
            this.groupBox11.Controls.Add(this.dtpNgayCapGiayPhep);
            this.groupBox11.Controls.Add(this.txtLoaiGiayPhep);
            this.groupBox11.Controls.Add(this.txtNguoiCapGiayPhep);
            this.groupBox11.Controls.Add(this.txtGhiChuGiayPhep);
            this.groupBox11.Controls.Add(this.txtNoiCapGiayPhep);
            this.groupBox11.Controls.Add(this.txtSoGiayPhep);
            this.groupBox11.Controls.Add(this.label34);
            this.groupBox11.Controls.Add(this.label35);
            this.groupBox11.Controls.Add(this.label36);
            this.groupBox11.Controls.Add(this.label37);
            this.groupBox11.Controls.Add(this.label38);
            this.groupBox11.Controls.Add(this.label39);
            this.groupBox11.Controls.Add(this.label40);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox11.Location = new System.Drawing.Point(0, 0);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(1034, 181);
            this.groupBox11.TabIndex = 82;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "CHI TIẾT GIẤY PHÉP";
            // 
            // dtpNgayHetHanGiayPhep
            // 
            this.dtpNgayHetHanGiayPhep.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayHetHanGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayHetHanGiayPhep.DropDownCalendar.Name = "";
            this.dtpNgayHetHanGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayHetHanGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHetHanGiayPhep.Location = new System.Drawing.Point(825, 104);
            this.dtpNgayHetHanGiayPhep.Name = "dtpNgayHetHanGiayPhep";
            this.dtpNgayHetHanGiayPhep.Nullable = true;
            this.dtpNgayHetHanGiayPhep.NullButtonText = "Xóa";
            this.dtpNgayHetHanGiayPhep.ShowNullButton = true;
            this.dtpNgayHetHanGiayPhep.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayHetHanGiayPhep.TabIndex = 60;
            this.dtpNgayHetHanGiayPhep.TodayButtonText = "Hôm nay";
            this.dtpNgayHetHanGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayHetHanGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // dtpNgayCapGiayPhep
            // 
            this.dtpNgayCapGiayPhep.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayCapGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayCapGiayPhep.DropDownCalendar.Name = "";
            this.dtpNgayCapGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayCapGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayCapGiayPhep.Location = new System.Drawing.Point(825, 18);
            this.dtpNgayCapGiayPhep.Name = "dtpNgayCapGiayPhep";
            this.dtpNgayCapGiayPhep.Nullable = true;
            this.dtpNgayCapGiayPhep.NullButtonText = "Xóa";
            this.dtpNgayCapGiayPhep.ShowNullButton = true;
            this.dtpNgayCapGiayPhep.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayCapGiayPhep.TabIndex = 59;
            this.dtpNgayCapGiayPhep.TodayButtonText = "Hôm nay";
            this.dtpNgayCapGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayCapGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // txtLoaiGiayPhep
            // 
            this.txtLoaiGiayPhep.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtLoaiGiayPhep.Location = new System.Drawing.Point(825, 65);
            this.txtLoaiGiayPhep.Name = "txtLoaiGiayPhep";
            this.txtLoaiGiayPhep.Size = new System.Drawing.Size(159, 21);
            this.txtLoaiGiayPhep.TabIndex = 58;
            this.txtLoaiGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // txtNguoiCapGiayPhep
            // 
            this.txtNguoiCapGiayPhep.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtNguoiCapGiayPhep.Location = new System.Drawing.Point(153, 109);
            this.txtNguoiCapGiayPhep.Name = "txtNguoiCapGiayPhep";
            this.txtNguoiCapGiayPhep.Size = new System.Drawing.Size(488, 21);
            this.txtNguoiCapGiayPhep.TabIndex = 55;
            this.txtNguoiCapGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChuGiayPhep
            // 
            this.txtGhiChuGiayPhep.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtGhiChuGiayPhep.Location = new System.Drawing.Point(153, 144);
            this.txtGhiChuGiayPhep.Name = "txtGhiChuGiayPhep";
            this.txtGhiChuGiayPhep.Size = new System.Drawing.Size(778, 21);
            this.txtGhiChuGiayPhep.TabIndex = 50;
            this.txtGhiChuGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // txtNoiCapGiayPhep
            // 
            this.txtNoiCapGiayPhep.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtNoiCapGiayPhep.Location = new System.Drawing.Point(153, 65);
            this.txtNoiCapGiayPhep.Name = "txtNoiCapGiayPhep";
            this.txtNoiCapGiayPhep.Size = new System.Drawing.Size(488, 21);
            this.txtNoiCapGiayPhep.TabIndex = 49;
            this.txtNoiCapGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // txtSoGiayPhep
            // 
            this.txtSoGiayPhep.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtSoGiayPhep.Location = new System.Drawing.Point(153, 23);
            this.txtSoGiayPhep.Name = "txtSoGiayPhep";
            this.txtSoGiayPhep.Size = new System.Drawing.Size(488, 21);
            this.txtSoGiayPhep.TabIndex = 48;
            this.txtSoGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Location = new System.Drawing.Point(673, 104);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(145, 13);
            this.label34.TabIndex = 47;
            this.label34.Text = "Ngày hết hạn giấy phép :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Location = new System.Drawing.Point(20, 109);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(126, 13);
            this.label35.TabIndex = 42;
            this.label35.Text = "Người cấp giấy phép :";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Location = new System.Drawing.Point(19, 70);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(111, 13);
            this.label36.TabIndex = 41;
            this.label36.Text = "Nơi cấp giấy phép :";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Location = new System.Drawing.Point(20, 149);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(84, 13);
            this.label37.TabIndex = 46;
            this.label37.Text = "Ghi chú khác :";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Location = new System.Drawing.Point(673, 65);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(94, 13);
            this.label38.TabIndex = 45;
            this.label38.Text = "Loại giấy phép :";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Location = new System.Drawing.Point(673, 23);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(122, 13);
            this.label39.TabIndex = 44;
            this.label39.Text = "Ngày cấp giấy phép :";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Location = new System.Drawing.Point(19, 28);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(85, 13);
            this.label40.TabIndex = 43;
            this.label40.Text = "Số giấy phép :";
            // 
            // gridEX4
            // 
            this.gridEX4.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridEX4.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.gridEX4.ColumnAutoResize = true;
            gridEX4_DesignTimeLayout.LayoutString = resources.GetString("gridEX4_DesignTimeLayout.LayoutString");
            this.gridEX4.DesignTimeLayout = gridEX4_DesignTimeLayout;
            this.gridEX4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEX4.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridEX4.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX4.FrozenColumns = 5;
            this.gridEX4.GroupByBoxVisible = false;
            this.gridEX4.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX4.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.gridEX4.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX4.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX4.Hierarchical = true;
            this.gridEX4.Location = new System.Drawing.Point(0, 0);
            this.gridEX4.Name = "gridEX4";
            this.gridEX4.RecordNavigator = true;
            this.gridEX4.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridEX4.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX4.Size = new System.Drawing.Size(1034, 456);
            this.gridEX4.TabIndex = 84;
            this.gridEX4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridEX4.VisualStyleManager = this.vsmMain;
            // 
            // tpContainer
            // 
            this.tpContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tpContainer.Controls.Add(this.groupBox23);
            this.tpContainer.Controls.Add(this.groupBox12);
            this.tpContainer.Controls.Add(this.groupBox13);
            this.tpContainer.Controls.Add(this.gridEX5);
            this.tpContainer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.tpContainer.Location = new System.Drawing.Point(4, 22);
            this.tpContainer.Name = "tpContainer";
            this.tpContainer.Size = new System.Drawing.Size(1034, 456);
            this.tpContainer.TabIndex = 5;
            this.tpContainer.Text = "KHAI BÁO CONTAINER";
            this.tpContainer.UseVisualStyleBackColor = true;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.grListContainer);
            this.groupBox23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox23.Location = new System.Drawing.Point(0, 188);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(1034, 268);
            this.groupBox23.TabIndex = 85;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "DANH SÁCH";
            // 
            // grListContainer
            // 
            this.grListContainer.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListContainer.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListContainer.ColumnAutoResize = true;
            grListContainer_DesignTimeLayout.LayoutString = resources.GetString("grListContainer_DesignTimeLayout.LayoutString");
            this.grListContainer.DesignTimeLayout = grListContainer_DesignTimeLayout;
            this.grListContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListContainer.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListContainer.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListContainer.FrozenColumns = 5;
            this.grListContainer.GroupByBoxVisible = false;
            this.grListContainer.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListContainer.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListContainer.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListContainer.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListContainer.Hierarchical = true;
            this.grListContainer.Location = new System.Drawing.Point(3, 17);
            this.grListContainer.Name = "grListContainer";
            this.grListContainer.RecordNavigator = true;
            this.grListContainer.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListContainer.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListContainer.Size = new System.Drawing.Size(1028, 248);
            this.grListContainer.TabIndex = 90;
            this.grListContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListContainer.VisualStyleManager = this.vsmMain;
            this.grListContainer.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListContainer_RowDoubleClick);
            this.grListContainer.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListContainer_LoadingRow);
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.Transparent;
            this.groupBox12.Controls.Add(this.btnViewContainer);
            this.groupBox12.Controls.Add(this.btnDeleteContainer);
            this.groupBox12.Controls.Add(this.btnAddContainer);
            this.groupBox12.Controls.Add(this.lblFileNamContainer);
            this.groupBox12.Controls.Add(this.label41);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox12.Location = new System.Drawing.Point(0, 146);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(1034, 42);
            this.groupBox12.TabIndex = 83;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "FILE ĐÍNH KÈM";
            // 
            // btnViewContainer
            // 
            this.btnViewContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnViewContainer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewContainer.ForeColor = System.Drawing.Color.White;
            this.btnViewContainer.Location = new System.Drawing.Point(825, 11);
            this.btnViewContainer.Name = "btnViewContainer";
            this.btnViewContainer.Size = new System.Drawing.Size(100, 25);
            this.btnViewContainer.TabIndex = 83;
            this.btnViewContainer.Text = "XEM FILE";
            this.btnViewContainer.UseVisualStyleBackColor = false;
            this.btnViewContainer.Click += new System.EventHandler(this.btnViewContainer_Click);
            // 
            // btnDeleteContainer
            // 
            this.btnDeleteContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnDeleteContainer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteContainer.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnDeleteContainer.ForeColor = System.Drawing.Color.White;
            this.btnDeleteContainer.Location = new System.Drawing.Point(927, 11);
            this.btnDeleteContainer.Name = "btnDeleteContainer";
            this.btnDeleteContainer.Size = new System.Drawing.Size(100, 25);
            this.btnDeleteContainer.TabIndex = 82;
            this.btnDeleteContainer.Text = "XÓA";
            this.btnDeleteContainer.UseVisualStyleBackColor = false;
            this.btnDeleteContainer.Click += new System.EventHandler(this.btnDeleteContainer_Click);
            // 
            // btnAddContainer
            // 
            this.btnAddContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnAddContainer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddContainer.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAddContainer.ForeColor = System.Drawing.Color.White;
            this.btnAddContainer.Location = new System.Drawing.Point(720, 11);
            this.btnAddContainer.Name = "btnAddContainer";
            this.btnAddContainer.Size = new System.Drawing.Size(100, 25);
            this.btnAddContainer.TabIndex = 81;
            this.btnAddContainer.Text = "THÊM FILE";
            this.btnAddContainer.UseVisualStyleBackColor = false;
            this.btnAddContainer.Click += new System.EventHandler(this.btnAddContainer_Click);
            // 
            // lblFileNamContainer
            // 
            this.lblFileNamContainer.AutoSize = true;
            this.lblFileNamContainer.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNamContainer.Location = new System.Drawing.Point(122, 17);
            this.lblFileNamContainer.Name = "lblFileNamContainer";
            this.lblFileNamContainer.Size = new System.Drawing.Size(0, 13);
            this.lblFileNamContainer.TabIndex = 43;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Location = new System.Drawing.Point(19, 18);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(70, 13);
            this.label41.TabIndex = 43;
            this.label41.Text = "FILE NAME :";
            // 
            // groupBox13
            // 
            this.groupBox13.BackColor = System.Drawing.Color.Transparent;
            this.groupBox13.Controls.Add(this.ctrCoQuanHaiQuan);
            this.groupBox13.Controls.Add(this.ctrMaLoaiHinh);
            this.groupBox13.Controls.Add(this.dtpNgayDangKyToKhai);
            this.groupBox13.Controls.Add(this.txtGhiChuContainer);
            this.groupBox13.Controls.Add(this.txtSoToKhai);
            this.groupBox13.Controls.Add(this.label43);
            this.groupBox13.Controls.Add(this.label44);
            this.groupBox13.Controls.Add(this.label46);
            this.groupBox13.Controls.Add(this.label47);
            this.groupBox13.Controls.Add(this.label48);
            this.groupBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox13.Location = new System.Drawing.Point(0, 0);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(1034, 146);
            this.groupBox13.TabIndex = 82;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "CHI TIẾT CONTAINER";
            // 
            // dtpNgayDangKyToKhai
            // 
            this.dtpNgayDangKyToKhai.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayDangKyToKhai.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayDangKyToKhai.DropDownCalendar.Name = "";
            this.dtpNgayDangKyToKhai.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayDangKyToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDangKyToKhai.Location = new System.Drawing.Point(578, 20);
            this.dtpNgayDangKyToKhai.Name = "dtpNgayDangKyToKhai";
            this.dtpNgayDangKyToKhai.Nullable = true;
            this.dtpNgayDangKyToKhai.NullButtonText = "Xóa";
            this.dtpNgayDangKyToKhai.ShowNullButton = true;
            this.dtpNgayDangKyToKhai.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayDangKyToKhai.TabIndex = 50;
            this.dtpNgayDangKyToKhai.TodayButtonText = "Hôm nay";
            this.dtpNgayDangKyToKhai.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayDangKyToKhai.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChuContainer
            // 
            this.txtGhiChuContainer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtGhiChuContainer.Location = new System.Drawing.Point(125, 104);
            this.txtGhiChuContainer.Name = "txtGhiChuContainer";
            this.txtGhiChuContainer.Size = new System.Drawing.Size(343, 21);
            this.txtGhiChuContainer.TabIndex = 49;
            this.txtGhiChuContainer.VisualStyleManager = this.vsmMain;
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtSoToKhai.Location = new System.Drawing.Point(125, 23);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(150, 21);
            this.txtSoToKhai.TabIndex = 48;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Location = new System.Drawing.Point(20, 109);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(84, 13);
            this.label43.TabIndex = 42;
            this.label43.Text = "Ghi chú khác :";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Location = new System.Drawing.Point(19, 70);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(80, 13);
            this.label44.TabIndex = 41;
            this.label44.Text = "Mã loại hình :";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Location = new System.Drawing.Point(473, 70);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(81, 13);
            this.label46.TabIndex = 45;
            this.label46.Text = "Mã hải quan :";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Location = new System.Drawing.Point(473, 28);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(89, 13);
            this.label47.TabIndex = 44;
            this.label47.Text = "Ngày đăng ký :";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Location = new System.Drawing.Point(19, 28);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(69, 13);
            this.label48.TabIndex = 43;
            this.label48.Text = "Số tờ khai :";
            // 
            // gridEX5
            // 
            this.gridEX5.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridEX5.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.gridEX5.ColumnAutoResize = true;
            gridEX5_DesignTimeLayout.LayoutString = resources.GetString("gridEX5_DesignTimeLayout.LayoutString");
            this.gridEX5.DesignTimeLayout = gridEX5_DesignTimeLayout;
            this.gridEX5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEX5.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridEX5.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX5.FrozenColumns = 5;
            this.gridEX5.GroupByBoxVisible = false;
            this.gridEX5.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX5.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.gridEX5.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX5.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX5.Hierarchical = true;
            this.gridEX5.Location = new System.Drawing.Point(0, 0);
            this.gridEX5.Name = "gridEX5";
            this.gridEX5.RecordNavigator = true;
            this.gridEX5.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridEX5.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX5.Size = new System.Drawing.Size(1034, 456);
            this.gridEX5.TabIndex = 84;
            this.gridEX5.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridEX5.VisualStyleManager = this.vsmMain;
            // 
            // tpCTKhac
            // 
            this.tpCTKhac.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tpCTKhac.Controls.Add(this.groupBox24);
            this.tpCTKhac.Controls.Add(this.groupBox14);
            this.tpCTKhac.Controls.Add(this.groupBox15);
            this.tpCTKhac.Controls.Add(this.gridEX6);
            this.tpCTKhac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.tpCTKhac.Location = new System.Drawing.Point(4, 22);
            this.tpCTKhac.Name = "tpCTKhac";
            this.tpCTKhac.Size = new System.Drawing.Size(1034, 456);
            this.tpCTKhac.TabIndex = 6;
            this.tpCTKhac.Text = "KHAI BÁO CT KHÁC";
            this.tpCTKhac.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.grListCTKhac);
            this.groupBox24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox24.Location = new System.Drawing.Point(0, 223);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(1034, 233);
            this.groupBox24.TabIndex = 85;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "DANH SÁCH";
            // 
            // grListCTKhac
            // 
            this.grListCTKhac.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListCTKhac.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListCTKhac.ColumnAutoResize = true;
            grListCTKhac_DesignTimeLayout.LayoutString = resources.GetString("grListCTKhac_DesignTimeLayout.LayoutString");
            this.grListCTKhac.DesignTimeLayout = grListCTKhac_DesignTimeLayout;
            this.grListCTKhac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListCTKhac.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListCTKhac.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListCTKhac.FrozenColumns = 5;
            this.grListCTKhac.GroupByBoxVisible = false;
            this.grListCTKhac.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListCTKhac.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListCTKhac.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListCTKhac.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListCTKhac.Hierarchical = true;
            this.grListCTKhac.Location = new System.Drawing.Point(3, 17);
            this.grListCTKhac.Name = "grListCTKhac";
            this.grListCTKhac.RecordNavigator = true;
            this.grListCTKhac.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListCTKhac.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListCTKhac.Size = new System.Drawing.Size(1028, 213);
            this.grListCTKhac.TabIndex = 91;
            this.grListCTKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListCTKhac.VisualStyleManager = this.vsmMain;
            this.grListCTKhac.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListCTKhac_RowDoubleClick);
            this.grListCTKhac.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListCTKhac_LoadingRow);
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.Color.Transparent;
            this.groupBox14.Controls.Add(this.btnViewCTKhac);
            this.groupBox14.Controls.Add(this.btnDeleteCTKhac);
            this.groupBox14.Controls.Add(this.btnAddCTKhac);
            this.groupBox14.Controls.Add(this.lblFileNameCTKhac);
            this.groupBox14.Controls.Add(this.label49);
            this.groupBox14.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox14.Location = new System.Drawing.Point(0, 181);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(1034, 42);
            this.groupBox14.TabIndex = 83;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "FILE ĐÍNH KÈM";
            // 
            // btnViewCTKhac
            // 
            this.btnViewCTKhac.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnViewCTKhac.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewCTKhac.ForeColor = System.Drawing.Color.White;
            this.btnViewCTKhac.Location = new System.Drawing.Point(825, 11);
            this.btnViewCTKhac.Name = "btnViewCTKhac";
            this.btnViewCTKhac.Size = new System.Drawing.Size(100, 25);
            this.btnViewCTKhac.TabIndex = 83;
            this.btnViewCTKhac.Text = "XEM FILE";
            this.btnViewCTKhac.UseVisualStyleBackColor = false;
            this.btnViewCTKhac.Click += new System.EventHandler(this.btnViewCTKhac_Click);
            // 
            // btnDeleteCTKhac
            // 
            this.btnDeleteCTKhac.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnDeleteCTKhac.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteCTKhac.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnDeleteCTKhac.ForeColor = System.Drawing.Color.White;
            this.btnDeleteCTKhac.Location = new System.Drawing.Point(927, 11);
            this.btnDeleteCTKhac.Name = "btnDeleteCTKhac";
            this.btnDeleteCTKhac.Size = new System.Drawing.Size(100, 25);
            this.btnDeleteCTKhac.TabIndex = 82;
            this.btnDeleteCTKhac.Text = "XÓA";
            this.btnDeleteCTKhac.UseVisualStyleBackColor = false;
            this.btnDeleteCTKhac.Click += new System.EventHandler(this.btnDeleteCTKhac_Click);
            // 
            // btnAddCTKhac
            // 
            this.btnAddCTKhac.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnAddCTKhac.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCTKhac.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAddCTKhac.ForeColor = System.Drawing.Color.White;
            this.btnAddCTKhac.Location = new System.Drawing.Point(720, 11);
            this.btnAddCTKhac.Name = "btnAddCTKhac";
            this.btnAddCTKhac.Size = new System.Drawing.Size(100, 25);
            this.btnAddCTKhac.TabIndex = 81;
            this.btnAddCTKhac.Text = "THÊM FILE";
            this.btnAddCTKhac.UseVisualStyleBackColor = false;
            this.btnAddCTKhac.Click += new System.EventHandler(this.btnAddCTKhac_Click);
            // 
            // lblFileNameCTKhac
            // 
            this.lblFileNameCTKhac.AutoSize = true;
            this.lblFileNameCTKhac.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNameCTKhac.Location = new System.Drawing.Point(122, 17);
            this.lblFileNameCTKhac.Name = "lblFileNameCTKhac";
            this.lblFileNameCTKhac.Size = new System.Drawing.Size(0, 13);
            this.lblFileNameCTKhac.TabIndex = 43;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Location = new System.Drawing.Point(19, 18);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(70, 13);
            this.label49.TabIndex = 43;
            this.label49.Text = "FILE NAME :";
            // 
            // groupBox15
            // 
            this.groupBox15.BackColor = System.Drawing.Color.Transparent;
            this.groupBox15.Controls.Add(this.dtpNgayChungTuKhac);
            this.groupBox15.Controls.Add(this.txtTenChungTuKhac);
            this.groupBox15.Controls.Add(this.txtGhiChuCTKhac);
            this.groupBox15.Controls.Add(this.txtNoiPhatHanhCTKhac);
            this.groupBox15.Controls.Add(this.txtSoChungTuKhac);
            this.groupBox15.Controls.Add(this.label50);
            this.groupBox15.Controls.Add(this.label51);
            this.groupBox15.Controls.Add(this.label52);
            this.groupBox15.Controls.Add(this.label55);
            this.groupBox15.Controls.Add(this.label56);
            this.groupBox15.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox15.Location = new System.Drawing.Point(0, 0);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(1034, 181);
            this.groupBox15.TabIndex = 82;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "CHỨNG TỪ KHÁC";
            // 
            // dtpNgayChungTuKhac
            // 
            this.dtpNgayChungTuKhac.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayChungTuKhac.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayChungTuKhac.DropDownCalendar.Name = "";
            this.dtpNgayChungTuKhac.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayChungTuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayChungTuKhac.Location = new System.Drawing.Point(720, 20);
            this.dtpNgayChungTuKhac.Name = "dtpNgayChungTuKhac";
            this.dtpNgayChungTuKhac.Nullable = true;
            this.dtpNgayChungTuKhac.NullButtonText = "Xóa";
            this.dtpNgayChungTuKhac.ShowNullButton = true;
            this.dtpNgayChungTuKhac.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayChungTuKhac.TabIndex = 55;
            this.dtpNgayChungTuKhac.TodayButtonText = "Hôm nay";
            this.dtpNgayChungTuKhac.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayChungTuKhac.VisualStyleManager = this.vsmMain;
            // 
            // txtTenChungTuKhac
            // 
            this.txtTenChungTuKhac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtTenChungTuKhac.Location = new System.Drawing.Point(125, 65);
            this.txtTenChungTuKhac.Name = "txtTenChungTuKhac";
            this.txtTenChungTuKhac.Size = new System.Drawing.Size(488, 21);
            this.txtTenChungTuKhac.TabIndex = 54;
            this.txtTenChungTuKhac.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChuCTKhac
            // 
            this.txtGhiChuCTKhac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtGhiChuCTKhac.Location = new System.Drawing.Point(125, 144);
            this.txtGhiChuCTKhac.Name = "txtGhiChuCTKhac";
            this.txtGhiChuCTKhac.Size = new System.Drawing.Size(488, 21);
            this.txtGhiChuCTKhac.TabIndex = 50;
            this.txtGhiChuCTKhac.VisualStyleManager = this.vsmMain;
            // 
            // txtNoiPhatHanhCTKhac
            // 
            this.txtNoiPhatHanhCTKhac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtNoiPhatHanhCTKhac.Location = new System.Drawing.Point(125, 104);
            this.txtNoiPhatHanhCTKhac.Name = "txtNoiPhatHanhCTKhac";
            this.txtNoiPhatHanhCTKhac.Size = new System.Drawing.Size(488, 21);
            this.txtNoiPhatHanhCTKhac.TabIndex = 49;
            this.txtNoiPhatHanhCTKhac.VisualStyleManager = this.vsmMain;
            // 
            // txtSoChungTuKhac
            // 
            this.txtSoChungTuKhac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtSoChungTuKhac.Location = new System.Drawing.Point(125, 23);
            this.txtSoChungTuKhac.Name = "txtSoChungTuKhac";
            this.txtSoChungTuKhac.Size = new System.Drawing.Size(488, 21);
            this.txtSoChungTuKhac.TabIndex = 48;
            this.txtSoChungTuKhac.VisualStyleManager = this.vsmMain;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Location = new System.Drawing.Point(21, 152);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(84, 13);
            this.label50.TabIndex = 47;
            this.label50.Text = "Ghi chú khác :";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Location = new System.Drawing.Point(20, 109);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(90, 13);
            this.label51.TabIndex = 42;
            this.label51.Text = "Nơi phát hành :";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Location = new System.Drawing.Point(19, 70);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(88, 13);
            this.label52.TabIndex = 41;
            this.label52.Text = "Tên chứng từ :";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Location = new System.Drawing.Point(619, 25);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(95, 13);
            this.label55.TabIndex = 44;
            this.label55.Text = "Ngày chứng từ :";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Location = new System.Drawing.Point(19, 28);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(81, 13);
            this.label56.TabIndex = 43;
            this.label56.Text = "Số chứng từ :";
            // 
            // gridEX6
            // 
            this.gridEX6.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridEX6.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.gridEX6.ColumnAutoResize = true;
            gridEX6_DesignTimeLayout.LayoutString = resources.GetString("gridEX6_DesignTimeLayout.LayoutString");
            this.gridEX6.DesignTimeLayout = gridEX6_DesignTimeLayout;
            this.gridEX6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEX6.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridEX6.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX6.FrozenColumns = 5;
            this.gridEX6.GroupByBoxVisible = false;
            this.gridEX6.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX6.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.gridEX6.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX6.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX6.Hierarchical = true;
            this.gridEX6.Location = new System.Drawing.Point(0, 0);
            this.gridEX6.Name = "gridEX6";
            this.gridEX6.RecordNavigator = true;
            this.gridEX6.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridEX6.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX6.Size = new System.Drawing.Size(1034, 456);
            this.gridEX6.TabIndex = 84;
            this.gridEX6.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridEX6.VisualStyleManager = this.vsmMain;
            // 
            // tpLamNgoaiGio
            // 
            this.tpLamNgoaiGio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tpLamNgoaiGio.Controls.Add(this.groupBox25);
            this.tpLamNgoaiGio.Controls.Add(this.groupBox17);
            this.tpLamNgoaiGio.Controls.Add(this.gridEX7);
            this.tpLamNgoaiGio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.tpLamNgoaiGio.Location = new System.Drawing.Point(4, 22);
            this.tpLamNgoaiGio.Name = "tpLamNgoaiGio";
            this.tpLamNgoaiGio.Size = new System.Drawing.Size(1034, 456);
            this.tpLamNgoaiGio.TabIndex = 7;
            this.tpLamNgoaiGio.Text = "KHAI BÁO ĐĂNG KÝ LÀM NGOÀI GIỜ";
            this.tpLamNgoaiGio.UseVisualStyleBackColor = true;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.grListDKLNG);
            this.groupBox25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox25.Location = new System.Drawing.Point(0, 140);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(1034, 316);
            this.groupBox25.TabIndex = 85;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "DANH SÁCH";
            // 
            // grListDKLNG
            // 
            this.grListDKLNG.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListDKLNG.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListDKLNG.ColumnAutoResize = true;
            grListDKLNG_DesignTimeLayout.LayoutString = resources.GetString("grListDKLNG_DesignTimeLayout.LayoutString");
            this.grListDKLNG.DesignTimeLayout = grListDKLNG_DesignTimeLayout;
            this.grListDKLNG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListDKLNG.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListDKLNG.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListDKLNG.FrozenColumns = 5;
            this.grListDKLNG.GroupByBoxVisible = false;
            this.grListDKLNG.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListDKLNG.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListDKLNG.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListDKLNG.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListDKLNG.Hierarchical = true;
            this.grListDKLNG.Location = new System.Drawing.Point(3, 17);
            this.grListDKLNG.Name = "grListDKLNG";
            this.grListDKLNG.RecordNavigator = true;
            this.grListDKLNG.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListDKLNG.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListDKLNG.Size = new System.Drawing.Size(1028, 296);
            this.grListDKLNG.TabIndex = 92;
            this.grListDKLNG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListDKLNG.VisualStyleManager = this.vsmMain;
            this.grListDKLNG.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListDKLNG_RowDoubleClick);
            this.grListDKLNG.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListDKLNG_LoadingRow);
            // 
            // groupBox17
            // 
            this.groupBox17.BackColor = System.Drawing.Color.Transparent;
            this.groupBox17.Controls.Add(this.dtpGioLamTT);
            this.groupBox17.Controls.Add(this.dtpNgayDangKyThuTuc);
            this.groupBox17.Controls.Add(this.btnDeleteOverTime);
            this.groupBox17.Controls.Add(this.txtNoiDung);
            this.groupBox17.Controls.Add(this.label59);
            this.groupBox17.Controls.Add(this.label60);
            this.groupBox17.Controls.Add(this.label64);
            this.groupBox17.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.groupBox17.Location = new System.Drawing.Point(0, 0);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(1034, 140);
            this.groupBox17.TabIndex = 82;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "CHI TIẾT LÀM NGOÀI GIỜ";
            // 
            // dtpGioLamTT
            // 
            this.dtpGioLamTT.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Time;
            // 
            // 
            // 
            this.dtpGioLamTT.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpGioLamTT.DropDownCalendar.Name = "";
            this.dtpGioLamTT.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpGioLamTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGioLamTT.Location = new System.Drawing.Point(125, 62);
            this.dtpGioLamTT.Name = "dtpGioLamTT";
            this.dtpGioLamTT.Nullable = true;
            this.dtpGioLamTT.NullButtonText = "Xóa";
            this.dtpGioLamTT.ShowNullButton = true;
            this.dtpGioLamTT.Size = new System.Drawing.Size(159, 21);
            this.dtpGioLamTT.TabIndex = 84;
            this.dtpGioLamTT.TodayButtonText = "Hôm nay";
            this.dtpGioLamTT.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpGioLamTT.VisualStyleManager = this.vsmMain;
            // 
            // dtpNgayDangKyThuTuc
            // 
            this.dtpNgayDangKyThuTuc.CustomFormat = "HH : MM";
            this.dtpNgayDangKyThuTuc.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayDangKyThuTuc.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayDangKyThuTuc.DropDownCalendar.Name = "";
            this.dtpNgayDangKyThuTuc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayDangKyThuTuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDangKyThuTuc.Location = new System.Drawing.Point(125, 20);
            this.dtpNgayDangKyThuTuc.Name = "dtpNgayDangKyThuTuc";
            this.dtpNgayDangKyThuTuc.Nullable = true;
            this.dtpNgayDangKyThuTuc.NullButtonText = "Xóa";
            this.dtpNgayDangKyThuTuc.ShowNullButton = true;
            this.dtpNgayDangKyThuTuc.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayDangKyThuTuc.TabIndex = 83;
            this.dtpNgayDangKyThuTuc.TodayButtonText = "Hôm nay";
            this.dtpNgayDangKyThuTuc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayDangKyThuTuc.VisualStyleManager = this.vsmMain;
            // 
            // btnDeleteOverTime
            // 
            this.btnDeleteOverTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnDeleteOverTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteOverTime.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnDeleteOverTime.ForeColor = System.Drawing.Color.White;
            this.btnDeleteOverTime.Location = new System.Drawing.Point(928, 109);
            this.btnDeleteOverTime.Name = "btnDeleteOverTime";
            this.btnDeleteOverTime.Size = new System.Drawing.Size(100, 25);
            this.btnDeleteOverTime.TabIndex = 82;
            this.btnDeleteOverTime.Text = "XÓA";
            this.btnDeleteOverTime.UseVisualStyleBackColor = false;
            this.btnDeleteOverTime.Click += new System.EventHandler(this.btnDeleteOverTime_Click);
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(68)))));
            this.txtNoiDung.Location = new System.Drawing.Point(125, 104);
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.Size = new System.Drawing.Size(593, 21);
            this.txtNoiDung.TabIndex = 49;
            this.txtNoiDung.VisualStyleManager = this.vsmMain;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Location = new System.Drawing.Point(20, 109);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(61, 13);
            this.label59.TabIndex = 42;
            this.label59.Text = "Nội dung :";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Location = new System.Drawing.Point(19, 70);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(98, 13);
            this.label60.TabIndex = 41;
            this.label60.Text = "Giờ làm thủ tục :";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.BackColor = System.Drawing.Color.Transparent;
            this.label64.Location = new System.Drawing.Point(19, 28);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(89, 13);
            this.label64.TabIndex = 43;
            this.label64.Text = "Ngày đăng ký :";
            // 
            // gridEX7
            // 
            this.gridEX7.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridEX7.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.gridEX7.ColumnAutoResize = true;
            gridEX7_DesignTimeLayout.LayoutString = resources.GetString("gridEX7_DesignTimeLayout.LayoutString");
            this.gridEX7.DesignTimeLayout = gridEX7_DesignTimeLayout;
            this.gridEX7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEX7.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridEX7.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX7.FrozenColumns = 5;
            this.gridEX7.GroupByBoxVisible = false;
            this.gridEX7.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX7.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.gridEX7.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX7.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX7.Hierarchical = true;
            this.gridEX7.Location = new System.Drawing.Point(0, 0);
            this.gridEX7.Name = "gridEX7";
            this.gridEX7.RecordNavigator = true;
            this.gridEX7.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridEX7.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX7.Size = new System.Drawing.Size(1034, 456);
            this.gridEX7.TabIndex = 84;
            this.gridEX7.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridEX7.VisualStyleManager = this.vsmMain;
            // 
            // groupBox16
            // 
            this.groupBox16.BackColor = System.Drawing.Color.Transparent;
            this.groupBox16.Controls.Add(this.btnClose);
            this.groupBox16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox16.Location = new System.Drawing.Point(0, 481);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(1042, 29);
            this.groupBox16.TabIndex = 1;
            this.groupBox16.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(930, 1);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 25);
            this.btnClose.TabIndex = 82;
            this.btnClose.Text = "ĐÓNG";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // ctrMaLoaiHinh
            // 
            this.ctrMaLoaiHinh.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaLoaiHinh.Appearance.Options.UseBackColor = true;
            this.ctrMaLoaiHinh.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E001;
            this.ctrMaLoaiHinh.Code = "";
            this.ctrMaLoaiHinh.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaLoaiHinh.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaLoaiHinh.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaLoaiHinh.IsOnlyWarning = false;
            this.ctrMaLoaiHinh.IsValidate = true;
            this.ctrMaLoaiHinh.Location = new System.Drawing.Point(125, 70);
            this.ctrMaLoaiHinh.Name = "ctrMaLoaiHinh";
            this.ctrMaLoaiHinh.Name_VN = "";
            this.ctrMaLoaiHinh.SetOnlyWarning = false;
            this.ctrMaLoaiHinh.SetValidate = false;
            this.ctrMaLoaiHinh.ShowColumnCode = true;
            this.ctrMaLoaiHinh.ShowColumnName = true;
            this.ctrMaLoaiHinh.Size = new System.Drawing.Size(343, 21);
            this.ctrMaLoaiHinh.TabIndex = 51;
            this.ctrMaLoaiHinh.TagName = "";
            this.ctrMaLoaiHinh.Where = null;
            this.ctrMaLoaiHinh.WhereCondition = "";
            // 
            // ctrCoQuanHaiQuan
            // 
            this.ctrCoQuanHaiQuan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrCoQuanHaiQuan.Appearance.Options.UseBackColor = true;
            this.ctrCoQuanHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrCoQuanHaiQuan.Code = "";
            this.ctrCoQuanHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrCoQuanHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrCoQuanHaiQuan.IsOnlyWarning = false;
            this.ctrCoQuanHaiQuan.IsValidate = true;
            this.ctrCoQuanHaiQuan.Location = new System.Drawing.Point(578, 70);
            this.ctrCoQuanHaiQuan.Name = "ctrCoQuanHaiQuan";
            this.ctrCoQuanHaiQuan.Name_VN = "";
            this.ctrCoQuanHaiQuan.SetOnlyWarning = false;
            this.ctrCoQuanHaiQuan.SetValidate = false;
            this.ctrCoQuanHaiQuan.ShowColumnCode = true;
            this.ctrCoQuanHaiQuan.ShowColumnName = true;
            this.ctrCoQuanHaiQuan.Size = new System.Drawing.Size(220, 21);
            this.ctrCoQuanHaiQuan.TabIndex = 52;
            this.ctrCoQuanHaiQuan.TagCode = "";
            this.ctrCoQuanHaiQuan.TagName = "";
            this.ctrCoQuanHaiQuan.Where = null;
            this.ctrCoQuanHaiQuan.WhereCondition = "";
            // 
            // VNACC_VouchersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1042, 538);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximumSize = new System.Drawing.Size(1058, 577);
            this.MinimumSize = new System.Drawing.Size(1058, 577);
            this.Name = "VNACC_VouchersForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "KHAI BÁO CHỨNG TỪ ";
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.tbCtrVouchers.ResumeLayout(false);
            this.tpCO.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListCO)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpVanDon.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListVanDon)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX1)).EndInit();
            this.tpHopDong.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListHopDong)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX2)).EndInit();
            this.tpHoaDon.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListHoaDon)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX3)).EndInit();
            this.tpGiayPhep.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListGiayPhep)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX4)).EndInit();
            this.tpContainer.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListContainer)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX5)).EndInit();
            this.tpCTKhac.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListCTKhac)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX6)).EndInit();
            this.tpLamNgoaiGio.ResumeLayout(false);
            this.groupBox25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListDKLNG)).EndInit();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX7)).EndInit();
            this.groupBox16.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private System.Windows.Forms.TabControl tbCtrVouchers;
        private System.Windows.Forms.TabPage tpCO;
        private System.Windows.Forms.GroupBox groupBox3;
        private Janus.Windows.GridEX.GridEX grListCO;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tpVanDon;
        private System.Windows.Forms.TabPage tpHopDong;
        private System.Windows.Forms.TabPage tpHoaDon;
        private System.Windows.Forms.TabPage tpGiayPhep;
        private System.Windows.Forms.TabPage tpContainer;
        private System.Windows.Forms.TabPage tpCTKhac;
        private System.Windows.Forms.TabPage tpLamNgoaiGio;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiCO;
        private Company.Interface.Controls.NuocHControl txtNuocCapCO;

        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiCapCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtToChucCapCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCO;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnViewCO;
        private System.Windows.Forms.Button btnDeleteCO;
        private System.Windows.Forms.Button btnAddCO;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnViewVanDon;
        private System.Windows.Forms.Button btnDeleteVanDon;
        private System.Windows.Forms.Button btnAddVanDon;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox5;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiVanDon;
        private Company.Interface.Controls.NuocHControl ctrNuocPHVanDon;

        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuVanDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemCTQC;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.GridEX gridEX1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnViewHopDong;
        private System.Windows.Forms.Button btnDeleteHopDong;
        private System.Windows.Forms.Button btnAddHopDong;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox7;

        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuHopDong;
        private Janus.Windows.GridEX.EditControls.EditBox txtTongTriGiaHopDong;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.GridEX.GridEX gridEX2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btnViewHoaDon;
        private System.Windows.Forms.Button btnDeleteHoaDon;
        private System.Windows.Forms.Button btnAddHoaDon;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox9;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHoaDonTM;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private Janus.Windows.GridEX.GridEX gridEX3;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button btnViewGiayPhep;
        private System.Windows.Forms.Button btnDeleteGiayPhep;
        private System.Windows.Forms.Button btnAddGiayPhep;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox11;

        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuGiayPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiCapGiayPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGiayPhep;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private Janus.Windows.GridEX.GridEX gridEX4;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button btnViewContainer;
        private System.Windows.Forms.Button btnDeleteContainer;
        private System.Windows.Forms.Button btnAddContainer;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.GroupBox groupBox13;

        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuContainer;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private Janus.Windows.GridEX.GridEX gridEX5;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Button btnViewCTKhac;
        private System.Windows.Forms.Button btnDeleteCTKhac;
        private System.Windows.Forms.Button btnAddCTKhac;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.GroupBox groupBox15;

        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuCTKhac;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiPhatHanhCTKhac;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoChungTuKhac;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private Janus.Windows.GridEX.GridEX gridEX6;
        private System.Windows.Forms.GroupBox groupBox17;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiDung;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label64;
        private Janus.Windows.GridEX.GridEX gridEX7;


        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuHoaDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiCapGiayPhep;

        private Janus.Windows.GridEX.EditControls.EditBox txtTenChungTuKhac;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar ucCalendar10;
        private System.Windows.Forms.GroupBox groupBox18;
        private Janus.Windows.GridEX.GridEX grListVanDon;
        private System.Windows.Forms.GroupBox groupBox19;
        private Janus.Windows.GridEX.GridEX grListHopDong;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.GroupBox groupBox22;
        private Janus.Windows.GridEX.GridEX grListGiayPhep;
        private System.Windows.Forms.GroupBox groupBox23;
        private Janus.Windows.GridEX.GridEX grListContainer;
        private System.Windows.Forms.GroupBox groupBox24;
        private Janus.Windows.GridEX.GridEX grListCTKhac;
        private System.Windows.Forms.GroupBox groupBox25;
        private Janus.Windows.GridEX.GridEX grListDKLNG;
        private Janus.Windows.GridEX.GridEX grListHoaDon;

        private System.Windows.Forms.Label lblFileNameCO;
        private System.Windows.Forms.Label lblFileNameVanDon;
        private System.Windows.Forms.Label lblFileNameHopDong;
        private System.Windows.Forms.Label lblFileNameHoaDon;
        private System.Windows.Forms.Label lblFileNameGiayPhep;
        private System.Windows.Forms.Label lblFileNamContainer;
        private System.Windows.Forms.Label lblFileNameCTKhac;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDeleteOverTime;
        private Janus.Windows.UI.CommandBars.UICommand cmdEdit1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdEdit;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiGiayPhep;
        private Janus.Windows.CalendarCombo.CalendarCombo dateNgayCapCO;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayVanDon;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayHopDong;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpThoiHanThanhToan;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayPhatHanhHoaDonTM;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayCapGiayPhep;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayHetHanGiayPhep;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayDangKyToKhai;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayChungTuKhac;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayDangKyThuTuc;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpGioLamTT;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHaiQuan;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaLoaiHinh;
    }
}