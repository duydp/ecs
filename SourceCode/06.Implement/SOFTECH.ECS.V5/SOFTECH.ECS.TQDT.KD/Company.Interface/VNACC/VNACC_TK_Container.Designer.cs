﻿namespace Company.Interface
{
    partial class VNACC_TK_Container
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TK_Container));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaDiaDiem5 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDiaDiem4 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDiaDiem3 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDiaDiem2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDiaDiem1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDiaChiDiaDiem = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDiaDiem = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.grList = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grList);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(492, 322);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ctrMaDiaDiem5);
            this.uiGroupBox1.Controls.Add(this.ctrMaDiaDiem4);
            this.uiGroupBox1.Controls.Add(this.ctrMaDiaDiem3);
            this.uiGroupBox1.Controls.Add(this.ctrMaDiaDiem2);
            this.uiGroupBox1.Controls.Add(this.ctrMaDiaDiem1);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtDiaChiDiaDiem);
            this.uiGroupBox1.Controls.Add(this.txtTenDiaDiem);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(492, 131);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Địa điểm xếp hàng lên xe chở hàng";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaDiaDiem5
            // 
            this.ctrMaDiaDiem5.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDiaDiem5.Code = "";
            this.ctrMaDiaDiem5.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiem5.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiem5.IsValidate = false;
            this.ctrMaDiaDiem5.Location = new System.Drawing.Point(402, 21);
            this.ctrMaDiaDiem5.Name = "ctrMaDiaDiem5";
            this.ctrMaDiaDiem5.Name_VN = "";
            this.ctrMaDiaDiem5.ShowColumnCode = true;
            this.ctrMaDiaDiem5.ShowColumnName = false;
            this.ctrMaDiaDiem5.Size = new System.Drawing.Size(80, 24);
            this.ctrMaDiaDiem5.TabIndex = 56;
            this.ctrMaDiaDiem5.TagName = "";
            this.ctrMaDiaDiem5.WhereCondition = "";
            // 
            // ctrMaDiaDiem4
            // 
            this.ctrMaDiaDiem4.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDiaDiem4.Code = "";
            this.ctrMaDiaDiem4.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiem4.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiem4.IsValidate = false;
            this.ctrMaDiaDiem4.Location = new System.Drawing.Point(314, 21);
            this.ctrMaDiaDiem4.Name = "ctrMaDiaDiem4";
            this.ctrMaDiaDiem4.Name_VN = "";
            this.ctrMaDiaDiem4.ShowColumnCode = true;
            this.ctrMaDiaDiem4.ShowColumnName = false;
            this.ctrMaDiaDiem4.Size = new System.Drawing.Size(80, 24);
            this.ctrMaDiaDiem4.TabIndex = 56;
            this.ctrMaDiaDiem4.TagName = "";
            this.ctrMaDiaDiem4.WhereCondition = "";
            // 
            // ctrMaDiaDiem3
            // 
            this.ctrMaDiaDiem3.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDiaDiem3.Code = "";
            this.ctrMaDiaDiem3.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiem3.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiem3.IsValidate = false;
            this.ctrMaDiaDiem3.Location = new System.Drawing.Point(226, 21);
            this.ctrMaDiaDiem3.Name = "ctrMaDiaDiem3";
            this.ctrMaDiaDiem3.Name_VN = "";
            this.ctrMaDiaDiem3.ShowColumnCode = true;
            this.ctrMaDiaDiem3.ShowColumnName = false;
            this.ctrMaDiaDiem3.Size = new System.Drawing.Size(80, 24);
            this.ctrMaDiaDiem3.TabIndex = 56;
            this.ctrMaDiaDiem3.TagName = "";
            this.ctrMaDiaDiem3.WhereCondition = "";
            // 
            // ctrMaDiaDiem2
            // 
            this.ctrMaDiaDiem2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDiaDiem2.Code = "";
            this.ctrMaDiaDiem2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiem2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiem2.IsValidate = false;
            this.ctrMaDiaDiem2.Location = new System.Drawing.Point(138, 21);
            this.ctrMaDiaDiem2.Name = "ctrMaDiaDiem2";
            this.ctrMaDiaDiem2.Name_VN = "";
            this.ctrMaDiaDiem2.ShowColumnCode = true;
            this.ctrMaDiaDiem2.ShowColumnName = false;
            this.ctrMaDiaDiem2.Size = new System.Drawing.Size(80, 24);
            this.ctrMaDiaDiem2.TabIndex = 56;
            this.ctrMaDiaDiem2.TagName = "";
            this.ctrMaDiaDiem2.WhereCondition = "";
            // 
            // ctrMaDiaDiem1
            // 
            this.ctrMaDiaDiem1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDiaDiem1.Code = "";
            this.ctrMaDiaDiem1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiem1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiem1.IsValidate = false;
            this.ctrMaDiaDiem1.Location = new System.Drawing.Point(50, 21);
            this.ctrMaDiaDiem1.Name = "ctrMaDiaDiem1";
            this.ctrMaDiaDiem1.Name_VN = "";
            this.ctrMaDiaDiem1.ShowColumnCode = true;
            this.ctrMaDiaDiem1.ShowColumnName = false;
            this.ctrMaDiaDiem1.Size = new System.Drawing.Size(80, 24);
            this.ctrMaDiaDiem1.TabIndex = 56;
            this.ctrMaDiaDiem1.TagName = "";
            this.ctrMaDiaDiem1.WhereCondition = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Địa chỉ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mã";
            // 
            // txtDiaChiDiaDiem
            // 
            this.txtDiaChiDiaDiem.Location = new System.Drawing.Point(50, 76);
            this.txtDiaChiDiaDiem.Multiline = true;
            this.txtDiaChiDiaDiem.Name = "txtDiaChiDiaDiem";
            this.txtDiaChiDiaDiem.Size = new System.Drawing.Size(430, 46);
            this.txtDiaChiDiaDiem.TabIndex = 0;
            this.txtDiaChiDiaDiem.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDiaDiem
            // 
            this.txtTenDiaDiem.Location = new System.Drawing.Point(50, 47);
            this.txtTenDiaDiem.Name = "txtTenDiaDiem";
            this.txtTenDiaDiem.Size = new System.Drawing.Size(430, 21);
            this.txtTenDiaDiem.TabIndex = 0;
            this.txtTenDiaDiem.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.btnGhi);
            this.uiGroupBox2.Controls.Add(this.btnXoa);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 284);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(492, 38);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(373, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.Location = new System.Drawing.Point(210, 9);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 6;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(292, 9);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 7;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // grList
            // 
            this.grList.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.ColumnAutoResize = true;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.GroupByBoxVisible = false;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Location = new System.Drawing.Point(0, 131);
            this.grList.Name = "grList";
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(492, 153);
            this.grList.TabIndex = 1;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.grList_DeletingRecord);
            // 
            // VNACC_TK_Container
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 322);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_TK_Container";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin Container";
            this.Load += new System.EventHandler(this.VNACC_TK_Container_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX grList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDiaDiem;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiem;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDiaDiem1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDiaDiem4;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDiaDiem3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDiaDiem2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDiaDiem5;
    }
}