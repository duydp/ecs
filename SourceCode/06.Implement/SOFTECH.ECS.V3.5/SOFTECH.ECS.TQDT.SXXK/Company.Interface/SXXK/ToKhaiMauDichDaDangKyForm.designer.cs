﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK
{
    partial class ToKhaiMauDichDaDangKyForm
    {
        private UIGroupBox uiGroupBox1;
        private ImageList ImageList1;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiMauDichDaDangKyForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkAll = new System.Windows.Forms.CheckBox();
            this.ccTuNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.khaibaoCTMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.HuyCTMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.NhanDuLieuCTMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.xacnhanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.SaoChepCha = new System.Windows.Forms.ToolStripMenuItem();
            this.saoChepTK = new System.Windows.Forms.ToolStripMenuItem();
            this.saoChepHH = new System.Windows.Forms.ToolStripMenuItem();
            this.print = new System.Windows.Forms.ToolStripMenuItem();
            this.xóaTờKhaiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ccNgayDen = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.btnXuat = new Janus.Windows.EditControls.UIButton();
            this.donViHaiQuanNewControl1 = new Company.Interface.Controls.DonViHaiQuanNewControl();
            this.label5 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.uiRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(785, 405);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.chkAll);
            this.uiGroupBox1.Controls.Add(this.ccTuNgay);
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.ccNgayDen);
            this.uiGroupBox1.Controls.Add(this.btnXuat);
            this.uiGroupBox1.Controls.Add(this.donViHaiQuanNewControl1);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnSearch);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(785, 405);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // chkAll
            // 
            this.chkAll.AutoSize = true;
            this.chkAll.BackColor = System.Drawing.Color.Transparent;
            this.chkAll.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAll.Location = new System.Drawing.Point(462, 41);
            this.chkAll.Name = "chkAll";
            this.chkAll.Size = new System.Drawing.Size(103, 17);
            this.chkAll.TabIndex = 286;
            this.chkAll.Text = "Tất cả tờ khai";
            this.chkAll.UseVisualStyleBackColor = false;
            // 
            // ccTuNgay
            // 
            // 
            // 
            // 
            this.ccTuNgay.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccTuNgay.DropDownCalendar.Name = "";
            this.ccTuNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccTuNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccTuNgay.IsNullDate = true;
            this.ccTuNgay.Location = new System.Drawing.Point(129, 38);
            this.ccTuNgay.Name = "ccTuNgay";
            this.ccTuNgay.Nullable = true;
            this.ccTuNgay.NullButtonText = "Xóa";
            this.ccTuNgay.Size = new System.Drawing.Size(108, 21);
            this.ccTuNgay.TabIndex = 285;
            this.ccTuNgay.TodayButtonText = "Hôm nay";
            this.ccTuNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccTuNgay.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ContextMenuStrip = this.contextMenuStrip1;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(3, 65);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(782, 292);
            this.dgList.TabIndex = 284;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.khaibaoCTMenu,
            this.HuyCTMenu,
            this.NhanDuLieuCTMenu,
            this.xacnhanToolStripMenuItem,
            this.toolStripSeparator1,
            this.SaoChepCha,
            this.print,
            this.xóaTờKhaiToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(148, 164);
            // 
            // khaibaoCTMenu
            // 
            this.khaibaoCTMenu.Image = global::Company.Interface.Properties.Resources.Send16;
            this.khaibaoCTMenu.Name = "khaibaoCTMenu";
            this.khaibaoCTMenu.Size = new System.Drawing.Size(147, 22);
            this.khaibaoCTMenu.Text = "Khai báo";
            // 
            // HuyCTMenu
            // 
            this.HuyCTMenu.Image = global::Company.Interface.Properties.Resources.cmdCancel_Icon.ToBitmap();
            this.HuyCTMenu.Name = "HuyCTMenu";
            this.HuyCTMenu.Size = new System.Drawing.Size(147, 22);
            this.HuyCTMenu.Text = "Hủy khai báo";
            // 
            // NhanDuLieuCTMenu
            // 
            this.NhanDuLieuCTMenu.Image = global::Company.Interface.Properties.Resources.Reload02_24.ToBitmap();
            this.NhanDuLieuCTMenu.Name = "NhanDuLieuCTMenu";
            this.NhanDuLieuCTMenu.Size = new System.Drawing.Size(147, 22);
            this.NhanDuLieuCTMenu.Text = "Nhận dữ liệu";
            // 
            // xacnhanToolStripMenuItem
            // 
            this.xacnhanToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("xacnhanToolStripMenuItem.Image")));
            this.xacnhanToolStripMenuItem.Name = "xacnhanToolStripMenuItem";
            this.xacnhanToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.xacnhanToolStripMenuItem.Text = "Xác nhận";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(144, 6);
            // 
            // SaoChepCha
            // 
            this.SaoChepCha.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saoChepTK,
            this.saoChepHH});
            this.SaoChepCha.Image = ((System.Drawing.Image)(resources.GetObject("SaoChepCha.Image")));
            this.SaoChepCha.Name = "SaoChepCha";
            this.SaoChepCha.Size = new System.Drawing.Size(147, 22);
            this.SaoChepCha.Text = "Sao chép";
            // 
            // saoChepTK
            // 
            this.saoChepTK.Image = ((System.Drawing.Image)(resources.GetObject("saoChepTK.Image")));
            this.saoChepTK.Name = "saoChepTK";
            this.saoChepTK.Size = new System.Drawing.Size(191, 22);
            this.saoChepTK.Text = "Sao chép tờ khai";
            // 
            // saoChepHH
            // 
            this.saoChepHH.Image = ((System.Drawing.Image)(resources.GetObject("saoChepHH.Image")));
            this.saoChepHH.Name = "saoChepHH";
            this.saoChepHH.Size = new System.Drawing.Size(191, 22);
            this.saoChepHH.Text = "Sao chép cả hàng hóa";
            // 
            // print
            // 
            this.print.Image = global::Company.Interface.Properties.Resources.kdeprint_printer;
            this.print.Name = "print";
            this.print.Size = new System.Drawing.Size(147, 22);
            this.print.Text = "In tờ khai";
            // 
            // xóaTờKhaiToolStripMenuItem
            // 
            this.xóaTờKhaiToolStripMenuItem.Image = global::Company.Interface.Properties.Resources.mail_delete;
            this.xóaTờKhaiToolStripMenuItem.Name = "xóaTờKhaiToolStripMenuItem";
            this.xóaTờKhaiToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.xóaTờKhaiToolStripMenuItem.Text = "Xóa tờ khai";
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(252, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 283;
            this.label6.Text = "Đến ngày";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 282;
            this.label2.Text = "Từ ngày";
            // 
            // ccNgayDen
            // 
            // 
            // 
            // 
            this.ccNgayDen.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayDen.DropDownCalendar.Name = "";
            this.ccNgayDen.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayDen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDen.IsNullDate = true;
            this.ccNgayDen.Location = new System.Drawing.Point(319, 38);
            this.ccNgayDen.Name = "ccNgayDen";
            this.ccNgayDen.Nullable = true;
            this.ccNgayDen.NullButtonText = "Xóa";
            this.ccNgayDen.Size = new System.Drawing.Size(108, 21);
            this.ccNgayDen.TabIndex = 281;
            this.ccNgayDen.TodayButtonText = "Hôm nay";
            this.ccNgayDen.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayDen.VisualStyleManager = this.vsmMain;
            // 
            // btnXuat
            // 
            this.btnXuat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuat.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXuat.Icon")));
            this.btnXuat.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnXuat.Location = new System.Drawing.Point(591, 367);
            this.btnXuat.Name = "btnXuat";
            this.btnXuat.Size = new System.Drawing.Size(99, 23);
            this.btnXuat.TabIndex = 279;
            this.btnXuat.Text = "Xuất Dữ Liệu";
            this.btnXuat.VisualStyleManager = this.vsmMain;
            this.btnXuat.WordWrap = false;
            this.btnXuat.Click += new System.EventHandler(this.btnXuat_Click);
            // 
            // donViHaiQuanNewControl1
            // 
            this.donViHaiQuanNewControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.donViHaiQuanNewControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanNewControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanNewControl1.Location = new System.Drawing.Point(129, 10);
            this.donViHaiQuanNewControl1.Ma = "";
            this.donViHaiQuanNewControl1.MaCuc = "";
            this.donViHaiQuanNewControl1.Name = "donViHaiQuanNewControl1";
            this.donViHaiQuanNewControl1.ReadOnly = true;
            this.donViHaiQuanNewControl1.Size = new System.Drawing.Size(541, 22);
            this.donViHaiQuanNewControl1.TabIndex = 277;
            this.donViHaiQuanNewControl1.Ten = "";
            this.donViHaiQuanNewControl1.VisualStyleManager = null;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(9, 382);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(179, 13);
            this.label5.TabIndex = 276;
            this.label5.Text = "Hướng dẫn: Chọn dòng để xuất";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(696, 367);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 275;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSearch.Icon")));
            this.btnSearch.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnSearch.Location = new System.Drawing.Point(571, 36);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(99, 23);
            this.btnSearch.TabIndex = 271;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyleManager = this.vsmMain;
            this.btnSearch.WordWrap = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 266;
            this.label1.Text = "Hải quan tiếp nhận";
            // 
            // uiRebar1
            // 
            this.uiRebar1.CommandManager = null;
            this.uiRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiRebar1.Location = new System.Drawing.Point(0, 0);
            this.uiRebar1.Name = "uiRebar1";
            this.uiRebar1.Size = new System.Drawing.Size(810, 0);
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // ToKhaiMauDichDaDangKyForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(785, 405);
            this.Controls.Add(this.uiGroupBox1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiMauDichDaDangKyForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Tag = "ToKhaiMauDichManageForm";
            this.Text = "Theo dõi tờ khai mậu dịch đã khai báo";
            this.Load += new System.EventHandler(this.ToKhaiMauDichManageForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UIButton btnSearch;
        private Label label1;
        private Janus.Windows.UI.CommandBars.UIRebar uiRebar1;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem khaibaoCTMenu;
        private ToolStripMenuItem NhanDuLieuCTMenu;
        private ToolStripMenuItem HuyCTMenu;
        private ToolStripMenuItem SaoChepCha;
        private ToolStripMenuItem saoChepTK;
        private ToolStripMenuItem saoChepHH;
        private ToolStripMenuItem print;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private ToolStripMenuItem xacnhanToolStripMenuItem;
        private UIButton btnClose;
        private Label label5;
        private ToolStripSeparator toolStripSeparator1;
        private Company.Interface.Controls.DonViHaiQuanNewControl donViHaiQuanNewControl1;
        private ToolStripMenuItem xóaTờKhaiToolStripMenuItem;
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private UIButton btnXuat;
        private Label label6;
        private Label label2;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayDen;
        private GridEX dgList;
        private Janus.Windows.CalendarCombo.CalendarCombo ccTuNgay;
        private CheckBox chkAll;
    }
}
