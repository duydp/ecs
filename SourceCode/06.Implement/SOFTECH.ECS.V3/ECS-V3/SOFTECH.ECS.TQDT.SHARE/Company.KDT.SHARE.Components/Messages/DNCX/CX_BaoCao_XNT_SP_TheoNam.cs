﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.DNCX
{
    [XmlRoot("Declaration")]
    public class CX_BaoCao_XNT_SP_TheoNam
    {
        /// <summary>
        /// Thông tin hàng hóa
        /// </summary>
        [XmlElement("CustomsGoodsItem")]
        public List<CustomsGoodsItem_BC> CustomsGoodsItem { get; set; }
    }
}
