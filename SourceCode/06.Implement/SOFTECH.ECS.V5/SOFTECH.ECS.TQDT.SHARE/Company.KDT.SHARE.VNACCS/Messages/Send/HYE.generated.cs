using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class HYE : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute ICN { get; set; }
public PropertiesAttribute STL { get; set; }
public PropertiesAttribute REF { get; set; }
public PropertiesAttribute KIJ { get; set; }

public HYE()
        {
ICN = new PropertiesAttribute(12, typeof(int));
STL = new PropertiesAttribute(20, typeof(string));
REF = new PropertiesAttribute(20, typeof(string));
KIJ = new PropertiesAttribute(996, typeof(string));
TongSoByte = 1056;
}

}public partial class EnumGroupID
    {
}

}
