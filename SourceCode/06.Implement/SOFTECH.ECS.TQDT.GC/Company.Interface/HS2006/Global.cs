using System;

namespace HaiQuan.HS
{
	public class Global
	{
#pragma warning disable 612,618
		public static string ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["HSConnectionString"];
#pragma warning restore 612,618
	}
	
	public enum HSSearchWhat
	{
		AllWord = 0,
		AnyWord = 1,
		Exact = 2
	}
}
