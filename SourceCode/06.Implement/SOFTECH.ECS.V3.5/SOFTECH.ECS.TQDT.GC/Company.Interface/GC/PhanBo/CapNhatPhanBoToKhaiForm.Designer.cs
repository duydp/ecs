﻿namespace SOFTECH.ECS.TQDT.GC.GC.PhanBo
{
    partial class CapNhatPhanBoToKhaiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CapNhatPhanBoToKhaiForm));
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNamDangKy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnCapNhatPB = new Janus.Windows.EditControls.UIButton();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox12 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchDate = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox14 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchTK = new Janus.Windows.EditControls.UIButton();
            this.txtSoTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.clcTuNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcDenNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).BeginInit();
            this.uiGroupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).BeginInit();
            this.uiGroupBox14.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(919, 499);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(838, 11);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiButton1
            // 
            this.uiButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Image = ((System.Drawing.Image)(resources.GetObject("uiButton1.Image")));
            this.uiButton1.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton1.Location = new System.Drawing.Point(216, 13);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(103, 23);
            this.uiButton1.TabIndex = 4;
            this.uiButton1.Text = "Tìm kiếm";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Năm đăng ký";
            // 
            // txtNamDangKy
            // 
            this.txtNamDangKy.BackColor = System.Drawing.Color.White;
            this.txtNamDangKy.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtNamDangKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamDangKy.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtNamDangKy.Location = new System.Drawing.Point(101, 14);
            this.txtNamDangKy.MaxLength = 4;
            this.txtNamDangKy.Name = "txtNamDangKy";
            this.txtNamDangKy.Size = new System.Drawing.Size(100, 21);
            this.txtNamDangKy.TabIndex = 7;
            this.txtNamDangKy.Text = "0";
            this.txtNamDangKy.Value = ((short)(0));
            this.txtNamDangKy.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16;
            this.txtNamDangKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNamDangKy.TextChanged += new System.EventHandler(this.uiButton1_Click);
            // 
            // btnCapNhatPB
            // 
            this.btnCapNhatPB.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCapNhatPB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhatPB.Image = ((System.Drawing.Image)(resources.GetObject("btnCapNhatPB.Image")));
            this.btnCapNhatPB.Location = new System.Drawing.Point(8, 12);
            this.btnCapNhatPB.Name = "btnCapNhatPB";
            this.btnCapNhatPB.Size = new System.Drawing.Size(133, 23);
            this.btnCapNhatPB.TabIndex = 4;
            this.btnCapNhatPB.Text = "Cập nhật phân bổ";
            this.btnCapNhatPB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCapNhatPB.Click += new System.EventHandler(this.btnCapNhatPB_Click);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageIndex = 4;
            this.btnExportExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcel.Location = new System.Drawing.Point(325, 13);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(90, 23);
            this.btnExportExcel.TabIndex = 304;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox12);
            this.uiGroupBox1.Controls.Add(this.btnExportExcel);
            this.uiGroupBox1.Controls.Add(this.uiButton1);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtNamDangKy);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(919, 94);
            this.uiGroupBox1.TabIndex = 306;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnSave);
            this.uiGroupBox2.Controls.Add(this.btnCapNhatPB);
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 458);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(919, 41);
            this.uiGroupBox2.TabIndex = 307;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(147, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(190, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Cập nhật ngày hàng về kho";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dgList
            // 
            this.dgList.AlternatingColors = true;
            this.dgList.AutoEdit = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(0, 94);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.Size = new System.Drawing.Size(919, 364);
            this.dgList.TabIndex = 308;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.CellEdited += new Janus.Windows.GridEX.ColumnActionEventHandler(this.dgList_CellEdited);
            this.dgList.CellValueChanged += new Janus.Windows.GridEX.ColumnActionEventHandler(this.dgList_CellValueChanged);
            // 
            // uiGroupBox12
            // 
            this.uiGroupBox12.AutoScroll = true;
            this.uiGroupBox12.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox12.Controls.Add(this.uiGroupBox13);
            this.uiGroupBox12.Controls.Add(this.uiGroupBox14);
            this.uiGroupBox12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox12.Location = new System.Drawing.Point(3, 37);
            this.uiGroupBox12.Name = "uiGroupBox12";
            this.uiGroupBox12.Size = new System.Drawing.Size(913, 54);
            this.uiGroupBox12.TabIndex = 314;
            this.uiGroupBox12.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.AutoScroll = true;
            this.uiGroupBox13.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox13.Controls.Add(this.clcTuNgay);
            this.uiGroupBox13.Controls.Add(this.clcDenNgay);
            this.uiGroupBox13.Controls.Add(this.label12);
            this.uiGroupBox13.Controls.Add(this.label16);
            this.uiGroupBox13.Controls.Add(this.btnSearchDate);
            this.uiGroupBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox13.Location = new System.Drawing.Point(400, 8);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(510, 43);
            this.uiGroupBox13.TabIndex = 316;
            this.uiGroupBox13.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchDate
            // 
            this.btnSearchDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchDate.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchDate.Image")));
            this.btnSearchDate.Location = new System.Drawing.Point(402, 14);
            this.btnSearchDate.Name = "btnSearchDate";
            this.btnSearchDate.Size = new System.Drawing.Size(89, 23);
            this.btnSearchDate.TabIndex = 88;
            this.btnSearchDate.Text = "Tìm kiếm";
            this.btnSearchDate.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchDate.Click += new System.EventHandler(this.btnSearchDate_Click);
            // 
            // uiGroupBox14
            // 
            this.uiGroupBox14.AutoScroll = true;
            this.uiGroupBox14.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox14.Controls.Add(this.btnSearchTK);
            this.uiGroupBox14.Controls.Add(this.txtSoTK);
            this.uiGroupBox14.Controls.Add(this.label11);
            this.uiGroupBox14.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox14.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox14.Name = "uiGroupBox14";
            this.uiGroupBox14.Size = new System.Drawing.Size(397, 43);
            this.uiGroupBox14.TabIndex = 315;
            this.uiGroupBox14.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchTK
            // 
            this.btnSearchTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchTK.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTK.Image")));
            this.btnSearchTK.Location = new System.Drawing.Point(282, 14);
            this.btnSearchTK.Name = "btnSearchTK";
            this.btnSearchTK.Size = new System.Drawing.Size(89, 23);
            this.btnSearchTK.TabIndex = 87;
            this.btnSearchTK.Text = "Tìm kiếm";
            this.btnSearchTK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchTK.Click += new System.EventHandler(this.btnSearchTK_Click);
            // 
            // txtSoTK
            // 
            this.txtSoTK.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoTK.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoTK.BackColor = System.Drawing.Color.White;
            this.txtSoTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTK.Location = new System.Drawing.Point(82, 15);
            this.txtSoTK.Name = "txtSoTK";
            this.txtSoTK.Size = new System.Drawing.Size(194, 21);
            this.txtSoTK.TabIndex = 85;
            this.txtSoTK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTK.TextChanged += new System.EventHandler(this.btnSearchTK_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(9, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 86;
            this.label11.Text = "Số tờ khai";
            // 
            // clcTuNgay
            // 
            this.clcTuNgay.CustomFormat = "dd/MM/yyyy";
            this.clcTuNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcTuNgay.DropDownCalendar.Name = "";
            this.clcTuNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcTuNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcTuNgay.Location = new System.Drawing.Point(70, 15);
            this.clcTuNgay.Name = "clcTuNgay";
            this.clcTuNgay.Size = new System.Drawing.Size(120, 21);
            this.clcTuNgay.TabIndex = 97;
            this.clcTuNgay.Value = new System.DateTime(2020, 5, 23, 0, 0, 0, 0);
            this.clcTuNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcTuNgay.TextChanged += new System.EventHandler(this.btnSearchDate_Click);
            // 
            // clcDenNgay
            // 
            this.clcDenNgay.CustomFormat = "dd/MM/yyyy";
            this.clcDenNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcDenNgay.DropDownCalendar.Name = "";
            this.clcDenNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDenNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcDenNgay.Location = new System.Drawing.Point(290, 15);
            this.clcDenNgay.Name = "clcDenNgay";
            this.clcDenNgay.Size = new System.Drawing.Size(97, 21);
            this.clcDenNgay.TabIndex = 96;
            this.clcDenNgay.Value = new System.DateTime(2020, 5, 23, 0, 0, 0, 0);
            this.clcDenNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDenNgay.TextChanged += new System.EventHandler(this.btnSearchDate_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(206, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 14);
            this.label12.TabIndex = 94;
            this.label12.Text = "Đến ngày :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(7, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 14);
            this.label16.TabIndex = 95;
            this.label16.Text = "Từ ngày :";
            // 
            // CapNhatPhanBoToKhaiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 499);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "CapNhatPhanBoToKhaiForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Danh sách các tờ khai chưa đưa vào phân bổ";
            this.Load += new System.EventHandler(this.CapNhatPhanBoToKhaiForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).EndInit();
            this.uiGroupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            this.uiGroupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).EndInit();
            this.uiGroupBox14.ResumeLayout(false);
            this.uiGroupBox14.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.FilterEditor.FilterEditor filterEditor1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamDangKy;
        private Janus.Windows.EditControls.UIButton btnCapNhatPB;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox12;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox13;
        private Janus.Windows.EditControls.UIButton btnSearchDate;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox14;
        private Janus.Windows.EditControls.UIButton btnSearchTK;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTK;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.CalendarCombo.CalendarCombo clcTuNgay;
        private Janus.Windows.CalendarCombo.CalendarCombo clcDenNgay;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
    }
}