﻿using System;
using System.Drawing;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL;
using Company.KD.BLL.Utils;
using System.Data;

namespace Company.Interface
{
    public partial class SelectHangMauDichForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------

        public ToKhaiMauDich TKMD;
        public HangMauDichCollection HMDTMPCollection = new HangMauDichCollection();
        //-----------------------------------------------------------------------------------------

        public SelectHangMauDichForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool checkHangExit(long HMD_ID)
        {
            foreach (HangMauDich hmd in HMDTMPCollection)
            {
                if (hmd.ID == HMD_ID) return true;
            }
            return false;
        }

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {            
            dgList.DataSource = TKMD.HMDCollection;
            if (TKMD.TrangThaiXuLy != -1)
            {
                //this.btnChonAll.Enabled = false;
                //this.btnChonNhieuHang.Enabled = false;
            }
        }

        private void btnChonAll_Click(object sender, EventArgs e)
        {           
            GridEXRow[] items = dgList.GetRows();
            if (dgList.GetRows().Length < 0) return;
            if (items.Length <= 0) return;
            {
                foreach (GridEXRow i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangMauDich HMD = (HangMauDich)i.DataRow;
                        if(!checkHangExit(HMD.ID))
                            HMDTMPCollection.Add(HMD);
                    }
                }                
            }
            this.Close();
        }

        private void btnChonNhieuHang_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetCheckedRows();
            if (dgList.GetRows().Length < 0) return;
            if (items.Length <= 0)
            {
                ShowMessage("Bạn chưa chọn hàng", false);
                return;
            }
            foreach (GridEXRow i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangMauDich HMD = (HangMauDich)i.DataRow;
                    if (!checkHangExit(HMD.ID))
                        HMDTMPCollection.Add(HMD);
                }
            }
            this.Close();
        }

     


    }
}
