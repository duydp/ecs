﻿using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.GC.BLL.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;
using Company.Interface.Report.GC;
using Company.Interface.Report;

namespace Company.Interface.GC
{
    public partial class BK04Form : BaseForm
    {

        public BK04Form()
        {
            InitializeComponent();            
        }
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        //private BKNguyenPhuLieuCollection nplColl = new BKNguyenPhuLieuCollection();
        private BKNguyenPhuLieu npl = new BKNguyenPhuLieu();
        public DataSet dsBK = new DataSet();
        private void LoadData()
        {

            //nplColl = new BKNguyenPhuLieu().SelectCollectionAll();
            //dgList.DataSource = nplColl;
            dsBK = new BKNguyenPhuLieu().SelectDynamic("OldHD_ID =" + this.HD.ID, "STT");
            dgList.DataSource = dsBK.Tables[0]; 
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
 
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void lblViewRP_Click(object sender, EventArgs e)
        {
            //BangKe04_HQGC BK04 = new BangKe04_HQGC();
            //BK04.HD = this.HD;
            //BK04.BindReport();
            //BK04.ShowPreview();
            this.Cursor = Cursors.WaitCursor;
            ReportViewBC04Form f = new ReportViewBC04Form();           
            f.HD = this.HD;
            f.dsBK = this.dsBK;
            f.Show();
            this.Cursor = Cursors.Default;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (npl.UpdateDataSetFromGrid(dsBK ))
               {
                   //ShowMessage("Cập nhật thành công", false);
                   //Message("MSG_SAV02", "", false);
                   showMsg("MSG_2702001");
               }
            else
               {
                   //Message("MSG_SAV01", "", false);
                   showMsg("MSG_2702002");
                   //ShowMessage("Cập nhật không thành công", false);
               }

             LoadData();

        }

        private void KB06Form_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["TongNPLCU"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            LoadData();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

      
        
       
     

         
 
    }
}