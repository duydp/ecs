﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.Common;

namespace Company.KDT.SHARE.Components.Controls
{
    public partial class TKMDVersionControlV : UserControl
    {
        public TKMDVersionControlV()
        {
            InitializeComponent();
        }

        private int _version;
        private bool _isCKS;

        public bool IsCKS
        {
            get { return _isCKS; }
            set { _isCKS = value; }
        }
        public int Version
        {
            get { return _version; }
            set { _version = value; }
        }

        delegate void SetVersionCallback(int value);
        public void SetVersion(int value)
        {
            if (lblVersion.InvokeRequired)
            {
                SetVersionCallback d = new SetVersionCallback(SetVersion);
                this.Invoke(d, new object[] { string.Format("Phiên bản: {0}", (value != 0 ? "V" + value : "V?")) });
            }
            else
            {
                lblVersion.Text = string.Format("Phiên bản: {0}", (value != 0 ? "V" + value : "V?"));
            }
        }

        delegate void SetCKSCallback(bool isCKS);
        public void SetCKS(bool isCKS)
        {
            if (lblCKS.InvokeRequired)
            {
                SetCKSCallback d = new SetCKSCallback(SetCKS);
                this.Invoke(d, new object[] { string.Format("CKS: {0}", (isCKS ? "Có" : "Không")) });
            }
            else
            {
                lblCKS.Text = string.Format("CKS: {0}", (isCKS ? "Có" : "Không"));
            }
        }

        public void LoadInfo(long tkmdId)
        {
            try
            {
                List<ToKhaiMauDichBoSung> collection = (List<ToKhaiMauDichBoSung>)ToKhaiMauDichBoSung.SelectCollectionBy_TKMD_ID(tkmdId);

                if (collection != null && collection.Count > 0)
                {
                    SetVersion(collection[0].PhienBan);
                    SetCKS(collection[0].ChuKySo);
                }
                else
                {
                    SetVersion(Version);
                    SetCKS(IsCKS);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public void LoadInfoTKCT(long tkctId)
        {
            try
            {
                List<ToKhaiChuyenTiepBoSung> collection = (List<ToKhaiChuyenTiepBoSung>)ToKhaiChuyenTiepBoSung.SelectCollectionBy_TKCT_ID(tkctId);

                if (collection != null && collection.Count > 0)
                {
                    SetVersion(collection[0].PhienBan);
                    SetCKS(collection[0].ChuKySo);
                }
                else
                {
                    SetVersion(Version);
                    SetCKS(IsCKS);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
