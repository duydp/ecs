﻿using System;
using System.Drawing;
using Company.BLL.DuLieuChuan;
using Company.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.BLL;
using Company.BLL.Utils;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class ListChungTuKemForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------

        public ListChungTuKemForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            if (TKMD.ID > 0)
            {
                dgList.DataSource = ChungTuKem.SelectCollectionBy_TKMDID(TKMD.ID);
            }
            else
            {
                //Load lai du lieu co san khi copy sang (Neu co)
                if (TKMD.ChungTuKemCollection.Count > 0)
                {
                    dgList.DataSource = TKMD.ChungTuKemCollection;
                }
            }

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

            //if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
            //    btnTaoMoi.Enabled = false;
            //    btnXoa.Enabled = false;
            //}

            //Fill ValueList
            this.dgList.RootTable.Columns["MA_LOAI_CT"].HasValueList = true;
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = this.dgList.RootTable.Columns["MA_LOAI_CT"].ValueList;

            System.Data.DataView view = BLL.DuLieuChuan.LoaiChungTu.SelectAll().Tables[0].DefaultView;
            view.Sort = "ID ASC";

            for (int i = 0; i < view.Count; i++)
            {
                System.Data.DataRowView row = view[i];
                valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"].ToString(), row["Ten"].ToString()));
            }

            //HUNGTQ, Uopdate 07/06/2010
            //SetButtonStateCHUNGTUKEM(TKMD, isKhaiBoSung);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<ChungTuKem> listChungTu = new List<ChungTuKem>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuKem gp = (ChungTuKem)i.GetRow().DataRow;
                        listChungTu.Add(gp);
                    }
                }
            }

            foreach (ChungTuKem gp in listChungTu)
            {
                if (gp.ID > 0)
                {
                    //Load chung tu kem chi tiet (Neu co)
                    gp.LoadListCTChiTiet();

                    //Xoa chung tu dinh kem chi tiet
                    for (int k = 0; k < gp.listCTChiTiet.Count; k++)
                    {
                        gp.listCTChiTiet[k].Delete();
                    }

                    //Xoa chung tu dinh kem trong DB
                    gp.Delete();
                }

                //Xoa chung tu dinh kem trong collection
                TKMD.ChungTuKemCollection.Remove(gp);
            }

            dgList.DataSource = TKMD.ChungTuKemCollection;

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            ChungTuKemForm ctForm = new ChungTuKemForm();
            ctForm.isKhaiBoSung = this.isKhaiBoSung;
            ctForm.TKMD = TKMD;
            ctForm.ShowDialog();

            dgList.DataSource = TKMD.ChungTuKemCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ChungTuKemForm f = new ChungTuKemForm();
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.isAddNew = false;
            f.TKMD = TKMD;
            f.CTK = (ChungTuKem)e.Row.DataRow;
            // if (f.chungTuKem.ID > 0 && f.chungTuKem..Count == 0)
            //if (f.chungTuKem.ID > 0)
            //    f.chungTuKem.LoadListCTChiTiet();

            f.ShowDialog();
            dgList.DataSource = TKMD.ChungTuKemCollection;

            //Cap nhat lai thong tin chung chu kem chi tiet
            for (int i = 0; i < TKMD.ChungTuKemCollection.Count; i++)
            {
                TKMD.ChungTuKemCollection[i].LoadListCTChiTiet();
            }

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateCHUNGTUKEM(BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung)
        {
            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.SoTiepNhan == 0 || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET);

                btnXoa.Enabled = status;
                btnTaoMoi.Enabled = status;
            }
            //Khai bao bo sung
            else
            {
                //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                if (tkmd.SoToKhai == 0)
                {
                    //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                    //Globals.ShowMessageTQDT(msg, false);

                    //return false;
                }
                else
                {
                    status = true;

                    //btnXoa.Enabled = status;
                    btnTaoMoi.Enabled = status;
                }
            }

            return true;
        }

        #endregion

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            else
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuKem gp = (ChungTuKem)i.GetRow().DataRow;
                        if (gp.SOTN > 0)
                            btnXoa.Enabled = false;
                        else
                            btnXoa.Enabled = true;
                    }
                }
            }
        }
    }
}
