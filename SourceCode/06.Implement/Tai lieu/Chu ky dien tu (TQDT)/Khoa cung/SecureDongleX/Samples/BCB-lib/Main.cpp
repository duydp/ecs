//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "..\\..\\Include\\SDX.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
#pragma comment(lib, "SDX.lib")

TfrmMain *frmMain;

int retcode, handle, select, block_index,block_len;
int bufferLength,startByte;
DWORD uid, hid;
char buffer[513]={0};
char bufferEncrypt[1000]={0};
char plainText[1000]={0};
char key[512]={0};
AnsiString tmpStr;

//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
   : TForm(Owner)
{
}

//---------------------------------------------------------------------------

// Write to SecureDongle X with specified UID
void __fastcall TfrmMain::btnWriteClick(TObject *Sender)
{
   retcode = SDX_Find();
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error Finding SecureDongle X: " + (AnsiString)retcode);
      return;
   }
   if (retcode == 0)
   {
      lstOutput->Items->Add("No SecureDongle X plugged");
      return;
   }
   lstOutput->Items->Add("Found SecureDongle X: " + (AnsiString)retcode);

   tmpStr = InputBox("Input", "Please input UID (i.e. 715400947)", "715400947");
   uid = tmpStr.ToInt();
   hid = 0;
   retcode = SDX_Open(1, uid, &hid);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   handle = retcode;

   tmpStr = InputBox("Input", "Please input write block index (0-4)", "0");
   block_index = tmpStr.ToInt();
   tmpStr = InputBox("Input", "Please input data to be written (i.e. helloworld)", "SDX - Sample Data");
   strcpy(buffer, tmpStr.c_str());
   retcode = SDX_Write(handle, block_index, buffer);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   lstOutput->Items->Add("Write OK");

   SDX_Close(handle);
}
//---------------------------------------------------------------------------

// Read SecureDongle X with specified UID
void __fastcall TfrmMain::btnReadClick(TObject *Sender)
{
   retcode = SDX_Find();
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error Finding SecureDongle X: " + (AnsiString)retcode);
      return;
   }
   if (retcode == 0)
   {
      lstOutput->Items->Add("No SecureDongle X plugged");
      return;
   }
   lstOutput->Items->Add("Found SecureDongle X: " + (AnsiString)retcode);

   tmpStr = InputBox("Input", "Please input UID (i.e. 715400947)", "715400947");
   uid = tmpStr.ToInt();
   hid = 0;
   retcode = SDX_Open(1, uid, &hid);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   handle = retcode;

   tmpStr = InputBox("Input", "Please input Read block index (0-4)", "0");
   block_index = tmpStr.ToInt();
   retcode = SDX_Read(handle, block_index, buffer);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   lstOutput->Items->Add("Read: " + (AnsiString)buffer);

   SDX_Close(handle);
}
//---------------------------------------------------------------------------

// Transform data using specified SecureDongle X
void __fastcall TfrmMain::btnTransformClick(TObject *Sender)
{
   retcode = SDX_Find();
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error Finding SecureDongle X: " + (AnsiString)retcode);
      return;
   }
   if (retcode == 0)
   {
      lstOutput->Items->Add("No SecureDongle X plugged");
      return;
   }
   lstOutput->Items->Add("Found SecureDongle X: " + (AnsiString)retcode);

   tmpStr = InputBox("Input", "Please input UID (i.e. 715400947)", "715400947");
   uid = tmpStr.ToInt();
   hid = 0;
   retcode = SDX_Open(1, uid, &hid);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   handle = retcode;

   ZeroMemory(buffer,512);
   tmpStr = InputBox("Input", "Please input data (i.e. helloworld)", "SDXTransform Test");
   strcpy(buffer, tmpStr.c_str());
   block_len=strlen(buffer);
   retcode=SDX_Transform(handle, block_len, buffer);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   lstOutput->Items->Add("Transform result: " + (AnsiString)buffer);

   SDX_Close(handle);
}
//---------------------------------------------------------------------------

// Encrypt using RSA and store to SecureDongle X with specified UID
void __fastcall TfrmMain::btnEncryptClick(TObject *Sender)
{
   retcode = SDX_Find();
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error Finding SecureDongle X: " + (AnsiString)retcode);
      return;
   }
   if (retcode == 0)
   {
      lstOutput->Items->Add("No SecureDongle X plugged");
      return;
   }
   lstOutput->Items->Add("Found SecureDongle X: " + (AnsiString)retcode);

   tmpStr = InputBox("Input", "Please input UID (i.e. 715400947)", "715400947");
   uid = tmpStr.ToInt();
   hid = 0;
   retcode = SDX_Open(1, uid, &hid);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   handle = retcode;

   tmpStr = InputBox("Input", "Please enter data to encrypt (i.e. helloworld)", "SDX Test RSA Encrypt");
	strcpy(bufferEncrypt, tmpStr.c_str());
   tmpStr = InputBox("Input", "Please enter start index (0-2559)", "0");
   startByte = tmpStr.ToInt();
   bufferLength = strlen(bufferEncrypt);

   retcode = SDX_RSAEncrypt(handle, startByte, bufferEncrypt, &bufferLength, key);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   lstOutput->Items->Add("Write success, size: " + (AnsiString)bufferLength + ". Key to decrypt is stored inside key512 variable.");
   lstOutput->Items->Add("You can try reading the contents of the SDX");

   SDX_Close(handle);
}
//---------------------------------------------------------------------------

// Read from SecureDongle X with specified UID and decrypt using RSA
void __fastcall TfrmMain::btnDecryptClick(TObject *Sender)
{
   retcode = SDX_Find();
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error Finding SecureDongle X: " + (AnsiString)retcode);
      return;
   }
   if (retcode == 0)
   {
      lstOutput->Items->Add("No SecureDongle X plugged");
      return;
   }
   lstOutput->Items->Add("Found SecureDongle X: " + (AnsiString)retcode);

   tmpStr = InputBox("Input", "Please input UID (i.e. 715400947)", "715400947");
   uid = tmpStr.ToInt();
   hid = 0;
   retcode = SDX_Open(1, uid, &hid);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   handle = retcode;

   tmpStr = InputBox("Input", "Please enter start index (0-2559)", "0");
   startByte = tmpStr.ToInt();
   tmpStr = InputBox("Input", "Please enter length of data to decrypt", "256");
   bufferLength = tmpStr.ToInt();

   retcode = SDX_RSADecrypt(handle, startByte, plainText, &bufferLength, key);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   lstOutput->Items->Add("Successfully read data with size: " + (AnsiString)bufferLength + " bytes. Decrypted data:");
   lstOutput->Items->Add((AnsiString)plainText);

   SDX_Close(handle);
}
//---------------------------------------------------------------------------

// Write to SecureDongle X with specified UID
// This is a sample of simple Data Mapping to put plain text data not in order
// For example, in 5-byte data, byte 1 will be put in SDX memory location 10, byte 2 will be put in SDX memory location 1
void __fastcall TfrmMain::btnWriteMapClick(TObject *Sender)
{
   retcode = SDX_Find();
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error Finding SecureDongle X: " + (AnsiString)retcode);
      return;
   }
   if (retcode == 0)
   {
      lstOutput->Items->Add("No SecureDongle X plugged");
      return;
   }
   lstOutput->Items->Add("Found SecureDongle X: " + (AnsiString)retcode);

   tmpStr = InputBox("Input", "Please input UID (i.e. 715400947)", "715400947");
   uid = tmpStr.ToInt();
   hid = 0;
   retcode = SDX_Open(1, uid, &hid);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   handle = retcode;

   tmpStr = InputBox("Input", "Please input write block index (0-4)", "0");
   block_index = tmpStr.ToInt();
   tmpStr = InputBox("Input", "Please input data to be written (i.e. helloworld)", "SDX - Sample Data");
	strcpy(buffer, tmpStr.c_str());

   //****************** ENCRYPTION SAMPLE *******************
   int i, index, len;
   long size;
   char temp[512] = {0};

   len = strlen(buffer);
   lstOutput->Items->Add("Data Length = " + (AnsiString)len);
   if (strlen(buffer) > 512)
   {
      lstOutput->Items->Add("Error: Data size max is 512\n");
      return;
   }

   for (i=0; i<512; i++)
   {
      if(rand() % 2 == 0)
      {
         temp[i] = (rand() % 26) + 65;
      } else {
         temp[i] = (rand() % 26) + 97;
      }
   }
   for (i=0; i<len; i++)
   {
      size = (long)i * (len-1);
      index = size % 512;
      temp[index] = buffer[i];
   }
   //********************************************************

   retcode = SDX_Write(handle, block_index, temp);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   lstOutput->Items->Add("Write OK. You can try to read the content of the block");

   SDX_Close(handle);
}
//---------------------------------------------------------------------------

// Read from SecureDongle X with specified UID
// This is the sample to read the Mapped data
void __fastcall TfrmMain::btnReadMapClick(TObject *Sender)
{
   retcode = SDX_Find();
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error Finding SecureDongle X: " + (AnsiString)retcode);
      return;
   }
   if (retcode == 0)
   {
      lstOutput->Items->Add("No SecureDongle X plugged");
      return;
   }
   lstOutput->Items->Add("Found SecureDongle X: " + (AnsiString)retcode);

   tmpStr = InputBox("Input", "Please input UID (i.e. 715400947)", "715400947");
   uid = tmpStr.ToInt();
   hid = 0;
   retcode = SDX_Open(1, uid, &hid);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }
   handle = retcode;

   tmpStr = InputBox("Input", "Please input Read block index (0-4)", "0");
   block_index = tmpStr.ToInt();
   retcode = SDX_Read(handle, block_index, buffer);
   if (retcode < 0)
   {
      lstOutput->Items->Add("Error: " + (AnsiString)retcode);
      return;
   }

   //******************* DECRYPTION **********************
   int i, index, len;
   long size;
   char temp[512];

   tmpStr = InputBox("Input", "Please enter size of the data to be read (1-512)", "");
   len = tmpStr.ToInt();
   if (len < 1 || len > 512) {
      lstOutput->Items->Add("Data size wrong.\n");
      return;
   }
   for (i=0;i<len;i++)
   {
      size = (long)i * (len-1);
      index = size % 512;
      temp[i]  = buffer[index];
   }
   temp[i]='\0';
   //*****************************************************

   lstOutput->Items->Add("Read: " + (AnsiString)temp);

   SDX_Close(handle);
}
//---------------------------------------------------------------------------

