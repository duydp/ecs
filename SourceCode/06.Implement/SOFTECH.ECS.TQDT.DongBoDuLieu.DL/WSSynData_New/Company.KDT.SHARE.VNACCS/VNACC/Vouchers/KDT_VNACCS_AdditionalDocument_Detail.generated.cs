﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_AdditionalDocument_Detail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long AdditionalDocument_ID { set; get; }
		public string SoChungTu { set; get; }
		public string TenChungTu { set; get; }
		public DateTime NgayPhatHanh { set; get; }
		public string NoiPhatHanh { set; get; }
        public List<KDT_VNACCS_AdditionalDocument_Detail> ListAdditionalDocument = new List<KDT_VNACCS_AdditionalDocument_Detail>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_AdditionalDocument_Detail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_AdditionalDocument_Detail> collection = new List<KDT_VNACCS_AdditionalDocument_Detail>();
			while (reader.Read())
			{
				KDT_VNACCS_AdditionalDocument_Detail entity = new KDT_VNACCS_AdditionalDocument_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("AdditionalDocument_ID"))) entity.AdditionalDocument_ID = reader.GetInt64(reader.GetOrdinal("AdditionalDocument_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTu"))) entity.SoChungTu = reader.GetString(reader.GetOrdinal("SoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChungTu"))) entity.TenChungTu = reader.GetString(reader.GetOrdinal("TenChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPhatHanh"))) entity.NgayPhatHanh = reader.GetDateTime(reader.GetOrdinal("NgayPhatHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiPhatHanh"))) entity.NoiPhatHanh = reader.GetString(reader.GetOrdinal("NoiPhatHanh"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_AdditionalDocument_Detail> collection, long id)
        {
            foreach (KDT_VNACCS_AdditionalDocument_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_AdditionalDocument_Details VALUES(@AdditionalDocument_ID, @SoChungTu, @TenChungTu, @NgayPhatHanh, @NoiPhatHanh)";
            string update = "UPDATE t_KDT_VNACCS_AdditionalDocument_Details SET AdditionalDocument_ID = @AdditionalDocument_ID, SoChungTu = @SoChungTu, TenChungTu = @TenChungTu, NgayPhatHanh = @NgayPhatHanh, NoiPhatHanh = @NoiPhatHanh WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_AdditionalDocument_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@AdditionalDocument_ID", SqlDbType.BigInt, "AdditionalDocument_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenChungTu", SqlDbType.NVarChar, "TenChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhatHanh", SqlDbType.DateTime, "NgayPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiPhatHanh", SqlDbType.NVarChar, "NoiPhatHanh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@AdditionalDocument_ID", SqlDbType.BigInt, "AdditionalDocument_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenChungTu", SqlDbType.NVarChar, "TenChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhatHanh", SqlDbType.DateTime, "NgayPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiPhatHanh", SqlDbType.NVarChar, "NoiPhatHanh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_AdditionalDocument_Details VALUES(@AdditionalDocument_ID, @SoChungTu, @TenChungTu, @NgayPhatHanh, @NoiPhatHanh)";
            string update = "UPDATE t_KDT_VNACCS_AdditionalDocument_Details SET AdditionalDocument_ID = @AdditionalDocument_ID, SoChungTu = @SoChungTu, TenChungTu = @TenChungTu, NgayPhatHanh = @NgayPhatHanh, NoiPhatHanh = @NoiPhatHanh WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_AdditionalDocument_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@AdditionalDocument_ID", SqlDbType.BigInt, "AdditionalDocument_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenChungTu", SqlDbType.NVarChar, "TenChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhatHanh", SqlDbType.DateTime, "NgayPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiPhatHanh", SqlDbType.NVarChar, "NoiPhatHanh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@AdditionalDocument_ID", SqlDbType.BigInt, "AdditionalDocument_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenChungTu", SqlDbType.NVarChar, "TenChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhatHanh", SqlDbType.DateTime, "NgayPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiPhatHanh", SqlDbType.NVarChar, "NoiPhatHanh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_AdditionalDocument_Detail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_AdditionalDocument_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_AdditionalDocument_Detail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_AdditionalDocument_Detail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_AdditionalDocument_Detail> SelectCollectionBy_AdditionalDocument_ID(long additionalDocument_ID)
		{
            IDataReader reader = SelectReaderBy_AdditionalDocument_ID(additionalDocument_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_AdditionalDocument_ID(long additionalDocument_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectBy_AdditionalDocument_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@AdditionalDocument_ID", SqlDbType.BigInt, additionalDocument_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_AdditionalDocument_ID(long additionalDocument_ID)
		{
			const string spName = "p_KDT_VNACCS_AdditionalDocument_Detail_SelectBy_AdditionalDocument_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@AdditionalDocument_ID", SqlDbType.BigInt, additionalDocument_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_AdditionalDocument_Detail(long additionalDocument_ID, string soChungTu, string tenChungTu, DateTime ngayPhatHanh, string noiPhatHanh)
		{
			KDT_VNACCS_AdditionalDocument_Detail entity = new KDT_VNACCS_AdditionalDocument_Detail();	
			entity.AdditionalDocument_ID = additionalDocument_ID;
			entity.SoChungTu = soChungTu;
			entity.TenChungTu = tenChungTu;
			entity.NgayPhatHanh = ngayPhatHanh;
			entity.NoiPhatHanh = noiPhatHanh;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@AdditionalDocument_ID", SqlDbType.BigInt, AdditionalDocument_ID);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@TenChungTu", SqlDbType.NVarChar, TenChungTu);
			db.AddInParameter(dbCommand, "@NgayPhatHanh", SqlDbType.DateTime, NgayPhatHanh.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanh);
			db.AddInParameter(dbCommand, "@NoiPhatHanh", SqlDbType.NVarChar, NoiPhatHanh);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_AdditionalDocument_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_AdditionalDocument_Detail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_AdditionalDocument_Detail(long id, long additionalDocument_ID, string soChungTu, string tenChungTu, DateTime ngayPhatHanh, string noiPhatHanh)
		{
			KDT_VNACCS_AdditionalDocument_Detail entity = new KDT_VNACCS_AdditionalDocument_Detail();			
			entity.ID = id;
			entity.AdditionalDocument_ID = additionalDocument_ID;
			entity.SoChungTu = soChungTu;
			entity.TenChungTu = tenChungTu;
			entity.NgayPhatHanh = ngayPhatHanh;
			entity.NoiPhatHanh = noiPhatHanh;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_AdditionalDocument_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@AdditionalDocument_ID", SqlDbType.BigInt, AdditionalDocument_ID);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@TenChungTu", SqlDbType.NVarChar, TenChungTu);
			db.AddInParameter(dbCommand, "@NgayPhatHanh", SqlDbType.DateTime, NgayPhatHanh.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanh);
			db.AddInParameter(dbCommand, "@NoiPhatHanh", SqlDbType.NVarChar, NoiPhatHanh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_AdditionalDocument_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_AdditionalDocument_Detail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_AdditionalDocument_Detail(long id, long additionalDocument_ID, string soChungTu, string tenChungTu, DateTime ngayPhatHanh, string noiPhatHanh)
		{
			KDT_VNACCS_AdditionalDocument_Detail entity = new KDT_VNACCS_AdditionalDocument_Detail();			
			entity.ID = id;
			entity.AdditionalDocument_ID = additionalDocument_ID;
			entity.SoChungTu = soChungTu;
			entity.TenChungTu = tenChungTu;
			entity.NgayPhatHanh = ngayPhatHanh;
			entity.NoiPhatHanh = noiPhatHanh;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@AdditionalDocument_ID", SqlDbType.BigInt, AdditionalDocument_ID);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@TenChungTu", SqlDbType.NVarChar, TenChungTu);
			db.AddInParameter(dbCommand, "@NgayPhatHanh", SqlDbType.DateTime, NgayPhatHanh.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanh);
			db.AddInParameter(dbCommand, "@NoiPhatHanh", SqlDbType.NVarChar, NoiPhatHanh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_AdditionalDocument_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_AdditionalDocument_Detail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_AdditionalDocument_Detail(long id)
		{
			KDT_VNACCS_AdditionalDocument_Detail entity = new KDT_VNACCS_AdditionalDocument_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public  int DeleteBy_AdditionalDocument_ID(long additionalDocument_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_DeleteBy_AdditionalDocument_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@AdditionalDocument_ID", SqlDbType.BigInt, additionalDocument_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_AdditionalDocument_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_AdditionalDocument_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_AdditionalDocument_Detail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}