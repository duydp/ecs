﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.KDT.SHARE.Components
{
    public partial class ProcessMessage:IMessage
    {
        public string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        public string sfmtDate = "yyyy-MM-dd";
        public string sfmtVnDateTime = "dd-MM-yyyy HH:mm:ss";

        public virtual MessageInfo Send(string pass)
        {
            MessageInfo ret = new MessageInfo();
            
            return ret;
        }
        public virtual MessageInfo Request(string pass)
        {
            MessageInfo ret = new MessageInfo();

            return ret;
        }
        public virtual void Process(MessageInfo messageInfo)
        {

        }
        public KDT_ContainerDangKy ProcessMessageBSContainer(KDT_ContainerDangKy containerDangKy)
        {
            try
            {
                FeedBackContent feedbackContent = null;
                Container_VNACCS fback = null;

                string whereCondition = String.Format("ItemID = {0} and MessageType= {1} ", containerDangKy.ID, DeclarationIssuer.Container);
                string orderByExpression = "CreatedTime ASC";
                List<KDT_Message> msgSendCollection = KDT_Message.SelectCollectionDynamic(whereCondition, orderByExpression);
                if (msgSendCollection.Count >= 1)
                {
                    foreach (KDT_Message item in msgSendCollection)
                    {
                        if (item.MessageFunction == (Int32)MessageFunctions.CapSoTiepNhan || item.MessageFunction == (Int32)MessageFunctions.DuyetSoTiepNhan)
                        {
                            feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(item.MessageContent);
                            containerDangKy.SoTiepNhan = long.Parse(feedbackContent.CustomsReference.Trim());
                            if (feedbackContent.Acceptance.Length == 10)
                                containerDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDate, null);
                            else if (feedbackContent.Acceptance.Length == 19)
                                containerDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, sfmtDateTime, null);
                            else containerDangKy.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, sfmtDateTime, null);
                            foreach (KDT_Message ite in msgSendCollection)
                            {
                                if (ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBao || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoSua || ite.ReferenceID == item.ReferenceID && ite.MessageFunction == (Int32)MessageFunctions.KhaiBaoHuy)
                                {
                                    MessageSend<Container_VNACCS> msg = Helpers.Deserialize<MessageSend<Container_VNACCS>>(ite.MessageContent);
                                    if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
                                    {
                                        string content = Helpers.ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                                        try
                                        {
                                            fback = Helpers.Deserialize<Container_VNACCS>(content);
                                            if (fback.TransportEquipments.TransportEquipment.Count >= 1)
                                            {
                                                foreach (KDT_ContainerBS containerBS in containerDangKy.ListCont)
                                                {
                                                    containerBS.Delete();
                                                }
                                                containerDangKy.ListCont.Clear();
                                                foreach (TransportEquipment items in fback.TransportEquipments.TransportEquipment)
                                                {
                                                    KDT_ContainerBS cont = new KDT_ContainerBS();
                                                    cont.Master_id = containerDangKy.ID;
                                                    cont.SoVanDon = items.BillOfLading.ToString();
                                                    cont.SoContainer = items.Container;
                                                    cont.SoSeal = items.Seal;
                                                    cont.GhiChu = items.Content;
                                                    containerDangKy.ListCont.Add(cont);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return containerDangKy;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
