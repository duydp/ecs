﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components.Utils;

namespace Company.KDT.SHARE.QuanLyChungTu.GCCT
{
    /// <summary>
    /// XML root cho việc Serializer ra XML đồng bộ dữ liệu
    /// </summary>
    [System.Xml.Serialization.XmlRoot(Namespace = "Company.KDT.SHARE.QuanLyChungTu.GCCT")]
    public partial class ChungTuKemAnh
    {
        public List<ChungTuKemAnhChiTiet> ListChungTuKemAnhChiTiet = new List<ChungTuKemAnhChiTiet>();

        public void LoadListChungTuKemAnhChiTiet()
        {
            ListChungTuKemAnhChiTiet = (List<ChungTuKemAnhChiTiet>)ChungTuKemAnhChiTiet.SelectCollectionBy_ChungTuKemAnhID(this.ID);
        }

        public void InsertUpdateFull(List<ChungTuKemAnhChiTiet> listCTDK)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.Insert(transaction);
                        else
                            this.Update(transaction);


                        foreach (ChungTuKemAnhChiTiet ctct in listCTDK)
                        {
                            ctct.ChungTuKemAnhID = this.ID;
                            if (id == 0)
                                ctct.ID = 0;

                            if (ctct.ID == 0)
                                ctct.Insert(transaction);
                            else
                                ctct.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.Insert(transaction);
                        else
                            this.Update(transaction);
                        foreach (ChungTuKemAnhChiTiet item in ListChungTuKemAnhChiTiet)
                        {
                            item.ChungTuKemAnhID = this.ID;
                            if (id == 0)
                                item.ID = 0;

                            if (item.ID == 0)
                                item.Insert(transaction);
                            else
                                item.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        this.ID = id;
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
    
    }
}
