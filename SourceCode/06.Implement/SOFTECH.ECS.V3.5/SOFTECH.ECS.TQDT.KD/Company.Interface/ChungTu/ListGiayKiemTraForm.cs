﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;

/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif


namespace Company.Interface
{
    public partial class ListGiayKiemTraForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------

        public ListGiayKiemTraForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            /*e.Row.Cells["NguyenTe_ID"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value.ToString());*/
            e.Row.Cells["LoaiGiay"].Text = e.Row.Cells["LoaiGiay"].Text == "DK" ? "Đăng ký kiểm tra" : "Kết quả kiểm tra";
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            if (TKMD.ID > 0)
            {
                string query;

                if (isKhaiBoSung)
                    query = string.Format("TKMD_ID = {0} AND LOAIKB = '1'", TKMD.ID);
                else
                    query = string.Format("TKMD_ID = {0} AND LOAIKB = '0'", TKMD.ID);

                dgList.DataSource = GiayKiemTra.SelectCollectionDynamic(query, null);
            }
            else
            {
                dgList.DataSource = TKMD.GiayKiemTraCollection;
            }

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                //btnTaoMoi.Enabled = false;
                //btnXoa.Enabled = false;
            }
        }




        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<GiayKiemTra> listGiayKiemTra = new List<GiayKiemTra>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        GiayKiemTra giayKT = (GiayKiemTra)i.GetRow().DataRow;
                        listGiayKiemTra.Add(giayKT);
                    }
                }
                dgList.DataSource = TKMD.GiayKiemTraCollection;
                try
                {
                    dgList.Refresh();
                }
                catch
                {
                    dgList.Refresh();
                }

            }

            foreach (GiayKiemTra giayKT in listGiayKiemTra)
            {
                //if (giayKT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO || giayKT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                if (Company.KDT.SHARE.QuanLyChungTu.GiayKiemTra.KiemTraChungTuCoBoSung(listGiayKiemTra) == false) //Update by Hungtq 06/04/2013
                {
                    giayKT.DeleteAll();
                    TKMD.GiayKiemTraCollection.Remove(giayKT);
                }
                else
                {
                    this.ShowMessage(string.Format("Giấy đăng ký kiểm tra bổ sung số : {0} đã được gửi đến hải quan. Không thể xóa", giayKT.SoGiayKiemTra), false);
                }

            }

            dgList.DataSource = TKMD.GiayKiemTraCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }


        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            GiayKiemTraForm f = new GiayKiemTraForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.ShowDialog(this);
            dgList.DataSource = TKMD.GiayKiemTraCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GiayKiemTraForm f = new GiayKiemTraForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.GiayKiemTra = (GiayKiemTra)e.Row.DataRow;
            f.ShowDialog(this);
            dgList.DataSource = TKMD.GiayKiemTraCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }




    }
}
