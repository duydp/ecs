﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
namespace DongBoDuLieu.BLL.SXXK
{
   public class DataSXXK
    {
       public DataTable NguyenPhuLieu { get; set; }
       public DataTable DinhMuc { get; set; }
       public DataTable SanPham { get; set; }
       public DataTable ToKhai { get; set; }

    }
}
