IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_NPLXuatTon]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_NPLXuatTon]
GO
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_SXXK_NPLXuatTon]  
-- Database: ECS_TQDT_GC_V5    
-- Author: Ngo Thanh Tung    
-- Time created: Thursday, January 23, 2014    
------------------------------------------------------------------------------------------------------------------------    
  
CREATE PROCEDURE [dbo].[p_KDT_SXXK_NPLXuatTon]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT   
TKMD.SoToKhai,  
TKMD.MaLoaiHinh,  
TKMD.CoQuanHaiQuan,  
TKMD.NgayDangKy,  
TKMD.TenDonVi,  
TKMD.TenDoiTac,  
HMD.MaHangHoa AS MaSP,  
HMD.TenHang AS TenSP,  
HMD.MaSoHang AS MaHS,  
HMD.DVTLuong1 AS TenDVT_SP,  
HMD.SoLuong1 AS LuongSP,  
DM.MaNguyenPhuLieu AS MaNPL,  
CASE WHEN DM.MaNguyenPhuLieu IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_SXXK_NguyenPhuLieu WHERE Ma=DM.MaNguyenPhuLieu) ELSE NULL END AS TenNPL,  
DM.DinhMucSuDung AS DinhMuc,  
DM.TyLeHaoHut,  
HMD.SoLuong1 * (DM.DinhMucSuDung + (DM.DinhMucSuDung * TyLeHaoHut)/100) AS LuongNPL,  
HMD.SoLuong1 * (DM.DinhMucSuDung + (DM.DinhMucSuDung * TyLeHaoHut)/100) AS TonNPL,  
TKMD.NgayHoanThanhKiemTra AS NgayThucXuat,  
TKMD.NgayHoanThanhKiemTra AS NgayHoanThanhXuat  
FROM dbo.t_KDT_VNACC_ToKhaiMauDich TKMD INNER JOIN dbo.t_KDT_VNACC_HangMauDich HMD ON HMD.TKMD_ID = TKMD.ID  
INNER JOIN dbo.t_SXXK_DinhMuc DM
ON DM.MaSanPham = HMD.MaHangHoa  
 WHERE  TKMD.LoaiHang=''S'' AND TKMD.TrangThaiXuLy IN (''2'',''3'') AND ' +  @WhereCondition    
    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY TKMD.NgayDangKy,TKMD.SoToKhai '  
END    
    
EXEC sp_executesql @SQL    


GO
 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.4',GETDATE(), N'CẬP NHẬT KẾT XUẤT DỮ LIỆU')
END
