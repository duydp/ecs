using System;
using System.Xml;
namespace WSDongBoDuLieu
{
    public class DoanhNghiep
    {
        private string _MaDoanhNghiep = "";
        private string _TenDoanhNghiep = "";
        private string _MatKhau = "";

        public string MaDoanhNghiep
        {
            get { return _MaDoanhNghiep; }
            set { _MaDoanhNghiep = value; }
        }
        public string TenDoanhNghiep
        {
            get { return _TenDoanhNghiep; }
            set { _TenDoanhNghiep = value; }
        }
        public string MatKhau
        {
            get { return _MatKhau; }
            set { _MatKhau = value; }
        }

        public static bool InsertDoanhNghiep(DoanhNghiep dn, string path)
        {
            if (isExitDoanhNghiep(dn.MaDoanhNghiep, path))
                throw new Exception("Doanh nghiệp này đã tồn tại");
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path + "\\ListUser.xml");
                XmlNode nodeRoot = doc.SelectSingleNode("Root");

                XmlElement nodeDoanhNghiep = doc.CreateElement("DoanhNghiep");
                XmlAttribute attMaDoanhNghiep = doc.CreateAttribute("MaDoanhNghiep");
                attMaDoanhNghiep.Value = dn.MaDoanhNghiep.Trim();
                nodeDoanhNghiep.Attributes.Append(attMaDoanhNghiep);

                XmlElement nodeTenDoanhNghiep = doc.CreateElement("TenDoanhNghiep");
                nodeTenDoanhNghiep.InnerText = dn.TenDoanhNghiep;
                nodeDoanhNghiep.AppendChild(nodeTenDoanhNghiep);
                XmlElement nodeMatKhau = doc.CreateElement("MatKhau");
                nodeMatKhau.InnerText = dn.MatKhau;
                nodeDoanhNghiep.AppendChild(nodeMatKhau);

                nodeRoot.AppendChild(nodeDoanhNghiep);
                doc.Save(path + "\\ListUser.xml");
                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
        }

        public static DoanhNghiep getDoanhNghiep(string maDoanhNghiep, string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path + "\\ListUser.xml");
            XmlNode nodeRoot = doc.SelectSingleNode("Root");
            XmlNodeList node = nodeRoot.SelectNodes("DoanhNghiep[@MaDoanhNghiep=\"" + maDoanhNghiep.Trim() + "\"]");

            if (node == null || node.Count == 0)
                return null;
            DoanhNghiep dn = new DoanhNghiep();
            XmlNode nodeDN = node[0];
            dn.MaDoanhNghiep = nodeDN.Attributes["MaDoanhNghiep"].Value;
            dn.MatKhau = nodeDN.ChildNodes[1].InnerText;
            return dn;

        }

        public static bool isExitDoanhNghiep(string MaDoanhNghiep, string path)
        {
            return getDoanhNghiep(MaDoanhNghiep, path) != null;
        }

        public static bool UpdateMatKhauDoanhNghiep(string maDoanhNghiep, string matKhau, string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path + "\\ListUser.xml");
            XmlNode nodeRoot = doc.SelectSingleNode("Root");
            XmlNodeList node = nodeRoot.SelectNodes("DoanhNghiep[@MaDoanhNghiep=\"" + maDoanhNghiep.Trim() + "\"]");

            if (node == null || node.Count == 0)
                return false;
            XmlNode nodeDN = node[0];
            nodeDN.ChildNodes[1].InnerText = matKhau;
            doc.Save(path + "\\ListUser.xml");
            return true;
        }
    }
}