﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace Company.BLL.VNACCS
{
    public partial class MsgPhanBo
    {
        public static MsgPhanBo LoadBy( string sotokhai, DateTime TuNgay, DateTime DenNgay)
        {
            const string spName = "[dbo].[p_KDT_VNACCS_MsgPhanBo_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.VarChar, sotokhai);
            db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, TuNgay);
            db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, DenNgay);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<MsgPhanBo> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }	
        public static List<MsgPhanBo> GetMsgPhanBoChuaXuLy(string terminalid)
        {
            return (List<MsgPhanBo>)SelectCollectionDynamic("TerminalID = '" + terminalid + "' And (TrangThai = 'C' OR CreatedTime > '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ) ", "CreatedTime");

        }
        public static List<MsgPhanBo> GetMsgPhanBoChuaXuLyInput(string inputMSGID, string SoToKhai)
        {
            if (!string.IsNullOrEmpty(SoToKhai))
                return (List<MsgPhanBo>)SelectCollectionDynamic("SoTiepNhan like '%" + SoToKhai + "%' ", "CreatedTime");
            else
                return (List<MsgPhanBo>)SelectCollectionDynamic("MessagesInputID = '" + inputMSGID + "'", "CreatedTime");

        }
        public static List<MsgPhanBo> GetMsgPhanBoChuaXuLy(string terminalid, bool All, DateTime FromDate, DateTime ToDate)
        {
            if (!All)
                return (List<MsgPhanBo>)SelectCollectionDynamic("TerminalID = '" + terminalid + "' And ( CreatedTime between '" + DateTime.Now.ToString("yyyy-MM-dd") + "'  and  '" + DateTime.Now.ToString("yyyy-MM-dd") + "' )", "CreatedTime");
            else
                return (List<MsgPhanBo>)SelectCollectionDynamic("CreatedTime between '" + DateTime.Now.ToString("yyyy-MM-dd") + "'  and  '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ", "CreatedTime");

        }

        public static string GetRTPTagPhanBoA2(string TerminalID)
        {
            string spName = "Select top 1 * from t_kdt_vnaccs_MsgPhanBo where TerminalID = '" + TerminalID + "' AND GhiChu is null Order by  Master_id desc";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("RTPTag"))) return reader.GetString(reader.GetOrdinal("RTPTag"));
            }
            return string.Empty;
            //throw new Exception("Lỗi không tìm thấy RTPTag");
        }
        public static int UpdateMsgPhanBo(string SoToKhai)
        {
            string spName = "Update [t_KDT_VNACCS_MsgPhanBo] set TrangThai = 'C' where SoTiepNhan = '" + SoToKhai + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteNonQuery(dbCommand);
        }
        public static List<string> SelectListSoToKhai(string where)
        {
            List<string> myData = new List<string>();
            string spName = "Select Sotiepnhan from t_kdt_vnaccs_MsgPhanBo where " + where;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
          
            while (reader.Read())
            {
                string a ;
                a = reader.GetString(0);
                myData.Add(a);
            }
            return myData;
        }
        public static MsgPhanBo GetMessThongQuan(string SoToKhai)
        {
            string where = "MaNghiepVu in (";
            foreach (string item in Company.KDT.SHARE.VNACCS.EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiNhap_BanChapNhanThongQuan)
            {
                where += "'" + item + "'" + ",";
            }
            foreach (string item in Company.KDT.SHARE.VNACCS.EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiXuat_BanChapNhanThongQuan)
            {
                where += "'" + item + "'" + ",";
            }
            where = where.Remove(where.Length - 1);
            where += ")" + " AND SoTiepNhan like '" + SoToKhai + "%'";
            List<MsgPhanBo> listmsg = (List<MsgPhanBo>)MsgPhanBo.SelectCollectionDynamic(where, "CreatedTime desc");
            if (listmsg != null && listmsg.Count > 0)
                return listmsg[0];
            else
                return null;

        }
        public static MsgPhanBo GetMessChuaThongQuan(string SoToKhai)
        {
            string where = "MaNghiepVu in (";
            foreach (string item in Company.KDT.SHARE.VNACCS.EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiNhap_BanXacNhan)
            {
                where += "'" + item + "'" + ",";
            }
            foreach (string item in Company.KDT.SHARE.VNACCS.EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiXuat_BanXacNhan)
            {
                where += "'" + item + "'" + ",";
            }
            where = where.Remove(where.Length - 1);
            where += ")" + " AND SoTiepNhan like '" + SoToKhai + "%'";
            List<MsgPhanBo> listmsg = (List<MsgPhanBo>)MsgPhanBo.SelectCollectionDynamic(where, "CreatedTime desc");
            if (listmsg != null && listmsg.Count > 0)
                return listmsg[0];
            else
                return null;

        }
    }
}
