﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.KDT.SHARE.Components
{
   public class ItemSign
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
