﻿-- Drop Existing Procedures
IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucDangKy_SelectDynamic_SP_NPL]') IS NOT NULL
    DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_SelectDynamic_SP_NPL];
GO
IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPhamDangKy_SelectDynamic_SanPham]') IS NOT NULL
    DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPhamDangKy_SelectDynamic_SanPham];
GO
IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieuDangKy_SelectDynamic_NPL]') IS NOT NULL
    DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuDangKy_SelectDynamic_NPL];
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucDangKy_SelectDynamic_SP_NPL]
-- Database: ECS_TQ_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 22, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_SelectDynamic_SP_NPL]
    @WhereCondition NVARCHAR(500),
    @OrderByExpression NVARCHAR(250) = NULL
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

DECLARE @SQL NVARCHAR(3250);

SET @SQL
    = N'SELECT DMDK.[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaDaiLy],
	[TrangThaiXuLy],
	[SoTiepNhanChungTu],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL]
 FROM [dbo].[t_KDT_SXXK_DinhMucDangKy] DMDK 
 inner join  t_kdt_SXXK_DinhMuc DM 
 on DMDK.ID = DM.master_id 
' + @WhereCondition;

IF @OrderByExpression IS NOT NULL
   AND LEN(@OrderByExpression) > 0
BEGIN
    SET @SQL = @SQL + N' ORDER BY ' + @OrderByExpression;
END;

EXEC sp_executesql @SQL;


GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPhamDangKy_SelectDynamic_SanPham]
-- Database: ECS_TQ_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 22, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPhamDangKy_SelectDynamic_SanPham]
    @WhereCondition NVARCHAR(500),
    @OrderByExpression NVARCHAR(250) = NULL
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

DECLARE @SQL NVARCHAR(3250);

SET @SQL
    = N'SELECT 	SP_DK.[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaDaiLy],
	[TrangThaiXuLy],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL]
 FROM [dbo].[t_KDT_SXXK_SanPhamDangKy] SP_DK
 inner join t_KDT_SXXK_SanPham SP 
 on SP_DK.id= SP.master_id 
 ' + @WhereCondition;

IF @OrderByExpression IS NOT NULL
   AND LEN(@OrderByExpression) > 0
BEGIN
    SET @SQL = @SQL + N' ORDER BY ' + @OrderByExpression;
END;

EXEC sp_executesql @SQL;


GO







------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieuDangKy_SelectDynamic_NPL]
-- Database: ECS_TQ_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 22, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuDangKy_SelectDynamic_NPL]
    @WhereCondition NVARCHAR(500),
    @OrderByExpression NVARCHAR(250) = NULL
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

DECLARE @SQL NVARCHAR(3250);

SET @SQL
    = N'SELECT 	NPL_DK.[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaDaiLy],
	[TrangThaiXuLy],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL]
 FROM [dbo].[t_KDT_SXXK_NguyenPhuLieuDangKy] NPL_DK
 inner join t_KDT_SXXK_NguyenPhuLieu NPL
 on NPL_DK.ID=NPL.master_id ' + @WhereCondition;

IF @OrderByExpression IS NOT NULL
   AND LEN(@OrderByExpression) > 0
BEGIN
    SET @SQL = @SQL + N' ORDER BY ' + @OrderByExpression;
END;

EXEC sp_executesql @SQL;
GO

IF
(
    SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '9.0'
) = 0
BEGIN
    INSERT INTO dbo.t_HaiQuan_Version
    VALUES
    ('9.0', GETDATE(), N'Cap nhat store tim kiem');
END;
ELSE
    UPDATE t_HaiQuan_Version
    SET Notes = N'Cap nhat store tim kiem'
    WHERE [Version] = '9.0';

