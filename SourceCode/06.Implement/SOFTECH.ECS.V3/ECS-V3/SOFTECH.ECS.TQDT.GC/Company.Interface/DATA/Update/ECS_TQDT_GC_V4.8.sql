﻿/*
Run this script on:

        192.168.72.100.ECS_TQDT_GC_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_GC_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 09/17/2012 8:22:59 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[p_GC_SelectHang]'
GO
ALTER PROC [dbo].[p_GC_SelectHang]      
(      
 @MaHaiQuan VARCHAR(10),      
 @MaDoanhNghiep VARCHAR(20),      
 @MaLoaiHinh VARCHAR(5)      
)      
AS      
      
/*Updateed by Hungtq, 08/09/2012*/      
      
DECLARE @sql VARCHAR(max), @TableName VARCHAR(50)     

/*TO KHAI GIA CONG*/
      
SET @sql =      
  ' SELECT distinct t.MaHaiQuan, t.MaDoanhNghiep, h.MaPhu AS Ma, h.TenHang AS Ten, h.MaHS, h.DVT_ID '      
+ ' FROM dbo.t_KDT_ToKhaiMauDich t INNER JOIN dbo.t_KDT_HangMauDich h '      
+ ' ON t.ID = h.TKMD_ID '      
+ ' WHERE t.MaHaiQuan = ''' + @MaHaiQuan + ''' AND t.MaDoanhNghiep = ''' + @MaDoanhNghiep + ''''      
+ ' AND t.TrangThaiXuLy = 1 AND t.MaLoaiHinh LIKE ''' + @MaLoaiHinh + '%'''      
    
SET @sql = @sql + ' UNION '      
    
IF(@MaLoaiHinh = 'N')        
 SET @TableName = 't_GC_NguyenPhuLieu'    
IF(@MaLoaiHinh = 'X')        
 SET @TableName = 't_GC_SanPham'    
IF(@MaLoaiHinh = 'T')        
 SET @TableName = 't_GC_ThietBi'    
    
SET @sql = @sql     
+ ' SELECT DISTINCT MaHaiQuan, MaDoanhNghiep, Ma, Ten, MaHS, DVT_ID '    
+ ' FROM dbo.' + @TableName + ' d INNER JOIN dbo.t_KDT_GC_HopDong h ON d.HopDong_ID = h.ID '
+ ' WHERE MaHaiQuan = ''' + @MaHaiQuan + ''' AND MaDoanhNghiep = ''' + @MaDoanhNghiep + ''''    

/*TO KHAI GIA CONG CHUYEN TIEP*/

SET @sql = @sql + ' UNION '      

SET @sql = @sql     
+ ' SELECT t.MaHaiQuanTiepNhan AS MaHaiQuan, t.MaDoanhNghiep, h.MaHang AS Ma, h.TenHang AS Ten, h.MaHS, h.ID_DVT AS DVT_ID '      
+ ' FROM dbo.t_KDT_GC_ToKhaiChuyenTiep t INNER JOIN dbo.t_KDT_GC_HangChuyenTiep h '      
+ ' ON t.ID = h.Master_ID '      
+ ' WHERE t.MaHaiQuanTiepNhan = ''' + @MaHaiQuan + ''' AND t.MaDoanhNghiep = ''' + @MaDoanhNghiep + ''''      
+ ' AND t.TrangThaiXuLy = 1 AND t.MaLoaiHinh LIKE ''' + @MaLoaiHinh + '%'''      
    
SET @sql = 'SELECT distinct MaHaiQuan, MaDoanhNghiep, Ma, Ten, MaHS, DVT_ID from (' + @sql + ') as v'    

--PRINT @sql      
EXEC (@sql)      
      
--exec p_GC_SelectHang 'C34C', '0100101308', 'N'      
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION

if( (select count(*) from dbo.t_HaiQuan_Version WHERE [Version] ='4.8') = 0)
	begin
		insert into dbo.t_HaiQuan_Version values('4.8', getdate(), N'Cập nhật cấu trúc dữ liệu.')
	end 
	
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
