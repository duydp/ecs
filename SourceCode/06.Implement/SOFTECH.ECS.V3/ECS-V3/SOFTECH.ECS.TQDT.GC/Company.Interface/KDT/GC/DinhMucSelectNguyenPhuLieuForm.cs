﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;

namespace Company.Interface.KDT.GC
{
    public partial class DinhMucSelectNguyenPhuLieuForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.GC.NguyenPhuLieu npl = new  Company.GC.BLL.GC.NguyenPhuLieu();
        public bool isBrower = false;
        public DinhMucSelectNguyenPhuLieuForm()
        {
            InitializeComponent();
        }   
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void NguyenPhuLieuGCEditForm_Load(object sender, EventArgs e)
        {
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;            
            Company.GC.BLL.GC.NguyenPhuLieu nplTMP = new  Company.GC.BLL.GC.NguyenPhuLieu();
            nplTMP.HopDong_ID = HD.ID;
            dgList.DataSource = npl.SelectCollectionBy_HopDong_ID();            
        }


   

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            npl = (Company.GC.BLL.GC.NguyenPhuLieu)e.Row.DataRow;
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
        }

        private void filterEditor1_Click(object sender, EventArgs e)
        {

        }
   
    }
}