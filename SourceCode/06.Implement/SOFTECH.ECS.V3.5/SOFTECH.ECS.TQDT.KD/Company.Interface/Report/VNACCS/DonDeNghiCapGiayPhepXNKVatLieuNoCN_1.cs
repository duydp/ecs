using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class DonDeNghiCapGiayPhepXNKVatLieuNoCN_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public DonDeNghiCapGiayPhepXNKVatLieuNoCN_1()
        {
            InitializeComponent();
        }
        public void BindingReport(VAHP010 vahp)
        {
            int HangHoa = vahp.HangHoa.Count;
            if (HangHoa <= 10)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 10, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblMaNguoiKhai.Text = vahp.A01.GetValue().ToString().ToUpper();
            lblTenNguoikhai.Text = vahp.A02.GetValue().ToString().ToUpper();
            lblSoDonXinCapPhep.Text = vahp.A08.GetValue().ToString().ToUpper();
            lblChucNangChungTu.Text = vahp.A06.GetValue().ToString().ToUpper();
            lblLoaiGiayPhep.Text = vahp.A07.GetValue().ToString().ToUpper();
            lblPhanLoaiXNK.Text = vahp.A09.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vahp.A03.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBao.Text = Convert.ToDateTime(vahp.A03.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBao.Text = "";
            }
            //lblNgayKhaiBao.Text = vahp.A03.GetValue().ToString().ToUpper();
            lblMaDonViCapPhep.Text = vahp.A04.GetValue().ToString().ToUpper();
            lblTenDonViCapPhep.Text = vahp.A05.GetValue().ToString().ToUpper();
            lblMaDoanhNghiepXNK.Text = vahp.A10.GetValue().ToString().ToUpper();
            lblTenDoanhNghiepXNK.Text = vahp.A11.GetValue().ToString().ToUpper();
            lblSoQDThanhLap.Text = vahp.A13.GetValue().ToString().ToUpper();
            lblSoGiayCNDKKD.Text = vahp.A14.GetValue().ToString().ToUpper();
            lblDonViCapCNDKKD.Text = vahp.A15.GetValue().ToString().ToUpper();
            //lblNgayCapCNDKKD.Text = vahp.A16.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vahp.A16.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayCapCNDKKD.Text = Convert.ToDateTime(vahp.A16.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayCapCNDKKD.Text = "";
            }
            lblMaBuuChinhDN.Text = vahp.A18.GetValue().ToString().ToUpper();
            lblSoNhaDN.Text = vahp.A12.GetValue().ToString().ToUpper();
            lblMaQuocGiaDN.Text = vahp.A17.GetValue().ToString().ToUpper();
            lblSoDienThoaiDN.Text = vahp.A19.GetValue().ToString().ToUpper();
            lblSoFaxDN.Text = vahp.A20.GetValue().ToString().ToUpper();
            lblEmailDN.Text = vahp.A21.GetValue().ToString().ToUpper();
            lblTenCongTy.Text = vahp.A11.GetValue().ToString().ToUpper();
            lblTenCongTyDeNghi.Text = vahp.A11.GetValue().ToString().ToUpper();
            lblSoHopDongMua.Text = vahp.A22.GetValue().ToString().ToUpper();
            //lblNgayHopDongMua.Text = vahp.A23.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vahp.A23.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHopDongMua.Text = Convert.ToDateTime(vahp.A23.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHopDongMua.Text = "";
            }
            lblSoHopDongBan.Text = vahp.A24.GetValue().ToString().ToUpper();
            //lblNgayHopDongBan.Text = vahp.A25.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vahp.A25.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHopDongBan.Text = Convert.ToDateTime(vahp.A25.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHopDongBan.Text = "";
            }
            lblMaPhuongTienVC.Text = vahp.A26.GetValue().ToString().ToUpper();
            lblTenPhuongTienVC.Text = vahp.MTN.GetValue().ToString().ToUpper();
            lblMaCuaKhauXNHang.Text = vahp.B05.GetValue().ToString().ToUpper();
            lblTenCuaKhauXNHang.Text = vahp.B06.GetValue().ToString().ToUpper();
            //lblNgayBatDauXNK.Text = vahp.A27.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vahp.A27.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayBatDauXNK.Text = Convert.ToDateTime(vahp.A27.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayBatDauXNK.Text = "";
            }
            if (Convert.ToDateTime(vahp.A28.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKetThucXNK.Text = Convert.ToDateTime(vahp.A28.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKetThucXNK.Text = "";
            }
            //lblNgayKetThucXNK.Text = vahp.A28.GetValue().ToString().ToUpper();
            lblHoSoLienQuan.Text = vahp.AD.GetValue().ToString().ToUpper();
            lblGhiChu.Text = vahp.A30.GetValue().ToString().ToUpper();
            lblTenGiamDoc.Text = vahp.A29.GetValue().ToString().ToUpper();

        }



    }
}
