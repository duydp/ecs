/*
Run this script on:

        192.168.72.151.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.151.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 01/06/2012 8:07:35 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[p_KDT_HopDongThuongMaiDetail_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_InsertUpdate]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_InsertUpdate]
	@ID bigint,
	@HMD_ID bigint,
	@HopDongTM_ID bigint,
	@GhiChu nvarchar(255),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(35,18),
	@DonGiaKB float,
	@TriGiaKB float
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HopDongThuongMaiDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HopDongThuongMaiDetail] 
		SET
			[HMD_ID] = @HMD_ID,
			[HopDongTM_ID] = @HopDongTM_ID,
			[GhiChu] = @GhiChu,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[DonGiaKB] = @DonGiaKB,
			[TriGiaKB] = @TriGiaKB
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HopDongThuongMaiDetail]
		(
			[HMD_ID],
			[HopDongTM_ID],
			[GhiChu],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[DonGiaKB],
			[TriGiaKB]
		)
		VALUES 
		(
			@HMD_ID,
			@HopDongTM_ID,
			@GhiChu,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@DonGiaKB,
			@TriGiaKB
		)		
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HopDongThuongMaiDetail_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_Update]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_Update]
	@ID bigint,
	@HMD_ID bigint,
	@HopDongTM_ID bigint,
	@GhiChu nvarchar(255),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(35,18),
	@DonGiaKB float,
	@TriGiaKB float
AS

UPDATE
	[dbo].[t_KDT_HopDongThuongMaiDetail]
SET
	[HMD_ID] = @HMD_ID,
	[HopDongTM_ID] = @HopDongTM_ID,
	[GhiChu] = @GhiChu,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[DonGiaKB] = @DonGiaKB,
	[TriGiaKB] = @TriGiaKB
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HopDongThuongMaiDetail_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_Insert]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_Insert]
	@HMD_ID bigint,
	@HopDongTM_ID bigint,
	@GhiChu nvarchar(255),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(35,18),
	@DonGiaKB float,
	@TriGiaKB float,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HopDongThuongMaiDetail]
(
	[HMD_ID],
	[HopDongTM_ID],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
)
VALUES 
(
	@HMD_ID,
	@HopDongTM_ID,
	@GhiChu,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@DonGiaKB,
	@TriGiaKB
)

SET @ID = SCOPE_IDENTITY()


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HoaDonThuongMaiDetail_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_InsertUpdate]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_InsertUpdate]
	@ID bigint,
	@HMD_ID bigint,
	@HoaDonTM_ID bigint,
	@GiaTriDieuChinhTang float,
	@GiaiTriDieuChinhGiam float,
	@GhiChu nvarchar(100),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38, 15),
	@DonGiaKB float,
	@TriGiaKB float
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HoaDonThuongMaiDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HoaDonThuongMaiDetail] 
		SET
			[HMD_ID] = @HMD_ID,
			[HoaDonTM_ID] = @HoaDonTM_ID,
			[GiaTriDieuChinhTang] = @GiaTriDieuChinhTang,
			[GiaiTriDieuChinhGiam] = @GiaiTriDieuChinhGiam,
			[GhiChu] = @GhiChu,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[DonGiaKB] = @DonGiaKB,
			[TriGiaKB] = @TriGiaKB
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HoaDonThuongMaiDetail]
		(
			[HMD_ID],
			[HoaDonTM_ID],
			[GiaTriDieuChinhTang],
			[GiaiTriDieuChinhGiam],
			[GhiChu],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[DonGiaKB],
			[TriGiaKB]
		)
		VALUES 
		(
			@HMD_ID,
			@HoaDonTM_ID,
			@GiaTriDieuChinhTang,
			@GiaiTriDieuChinhGiam,
			@GhiChu,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@DonGiaKB,
			@TriGiaKB
		)		
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HoaDonThuongMaiDetail_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_Update]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_Update]
	@ID bigint,
	@HMD_ID bigint,
	@HoaDonTM_ID bigint,
	@GiaTriDieuChinhTang float,
	@GiaiTriDieuChinhGiam float,
	@GhiChu nvarchar(100),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38,15),
	@DonGiaKB float,
	@TriGiaKB float
AS

UPDATE
	[dbo].[t_KDT_HoaDonThuongMaiDetail]
SET
	[HMD_ID] = @HMD_ID,
	[HoaDonTM_ID] = @HoaDonTM_ID,
	[GiaTriDieuChinhTang] = @GiaTriDieuChinhTang,
	[GiaiTriDieuChinhGiam] = @GiaiTriDieuChinhGiam,
	[GhiChu] = @GhiChu,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[DonGiaKB] = @DonGiaKB,
	[TriGiaKB] = @TriGiaKB
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HoaDonThuongMaiDetail_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_Insert]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_Insert]
	@HMD_ID bigint,
	@HoaDonTM_ID bigint,
	@GiaTriDieuChinhTang float,
	@GiaiTriDieuChinhGiam float,
	@GhiChu nvarchar(100),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38, 15),
	@DonGiaKB float,
	@TriGiaKB float,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HoaDonThuongMaiDetail]
(
	[HMD_ID],
	[HoaDonTM_ID],
	[GiaTriDieuChinhTang],
	[GiaiTriDieuChinhGiam],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
)
VALUES 
(
	@HMD_ID,
	@HoaDonTM_ID,
	@GiaTriDieuChinhTang,
	@GiaiTriDieuChinhGiam,
	@GhiChu,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@DonGiaKB,
	@TriGiaKB
)

SET @ID = SCOPE_IDENTITY()


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangGiayPhepDetail_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_InsertUpdate]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_InsertUpdate]
	@ID bigint,
	@GiayPhep_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38, 15),
	@DonGiaKB float,
	@TriGiaKB float
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangGiayPhepDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangGiayPhepDetail] 
		SET
			[GiayPhep_ID] = @GiayPhep_ID,
			[MaNguyenTe] = @MaNguyenTe,
			[MaChuyenNganh] = @MaChuyenNganh,
			[GhiChu] = @GhiChu,
			[HMD_ID] = @HMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[DonGiaKB] = @DonGiaKB,
			[TriGiaKB] = @TriGiaKB
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangGiayPhepDetail]
		(
			[GiayPhep_ID],
			[MaNguyenTe],
			[MaChuyenNganh],
			[GhiChu],
			[HMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[DonGiaKB],
			[TriGiaKB]
		)
		VALUES 
		(
			@GiayPhep_ID,
			@MaNguyenTe,
			@MaChuyenNganh,
			@GhiChu,
			@HMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@DonGiaKB,
			@TriGiaKB
		)		
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NhomCuaKhauGetCuaKhau]'
GO
ALTER PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhauGetCuaKhau]    
 -- Add the parameters for the stored procedure here    
 @Cuc_ID nvarchar(10)    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
  SELECT     t_HaiQuan_CuaKhau.ID, t_HaiQuan_CuaKhau.Ten    
  FROM         t_HaiQuan_NhomCuaKhau INNER JOIN    
         t_HaiQuan_CuaKhau ON t_HaiQuan_NhomCuaKhau.CuaKhau_ID = t_HaiQuan_CuaKhau.ID    
 WHERE     (t_HaiQuan_NhomCuaKhau.Cuc_ID like '%' +@Cuc_ID +'%')   
    
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangGiayPhepDetail_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_Update]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_Update]
	@ID bigint,
	@GiayPhep_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38, 15),
	@DonGiaKB float,
	@TriGiaKB float
AS

UPDATE
	[dbo].[t_KDT_HangGiayPhepDetail]
SET
	[GiayPhep_ID] = @GiayPhep_ID,
	[MaNguyenTe] = @MaNguyenTe,
	[MaChuyenNganh] = @MaChuyenNganh,
	[GhiChu] = @GhiChu,
	[HMD_ID] = @HMD_ID,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[DonGiaKB] = @DonGiaKB,
	[TriGiaKB] = @TriGiaKB
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangGiayPhepDetail_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_Insert]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_Insert]
	@GiayPhep_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38, 15),
	@DonGiaKB float,
	@TriGiaKB float,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HangGiayPhepDetail]
(
	[GiayPhep_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
)
VALUES 
(
	@GiayPhep_ID,
	@MaNguyenTe,
	@MaChuyenNganh,
	@GhiChu,
	@HMD_ID,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@DonGiaKB,
	@TriGiaKB
)

SET @ID = SCOPE_IDENTITY()


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[TCVN2Unicode]'
GO
CREATE FUNCTION [dbo].[TCVN2Unicode] (@strInput VARCHAR(4000))
RETURNS NVARCHAR(4000)
AS
BEGIN
    DECLARE @TCVN CHAR(671)
    DECLARE @UNICODE CHAR(671)
    SET @TCVN = ',184 ,181 ,182 ,183 ,185 ,168 ,190 ,187 ,188 ,189 ,198 ,169 ,202 ,199 ,200 ,201 ,203 ,208 ,204 ,206 ,207 ,209 ,170 ,213 ,210 ,211 ,212 ,214 ,221 ,215 ,216 ,220 ,222 ,227 ,223 ,225 ,226 ,228 ,171 ,232 ,229 ,230 ,231 ,233 ,172 ,237 ,234 ,235 ,236 ,238 ,243 ,239 ,241 ,242 ,244 ,173 ,248 ,245 ,246 ,247 ,249 ,253 ,250 ,251 ,252 ,254 ,174 ,184 ,181 ,182 ,183 ,185 ,161 ,190 ,187 ,188 ,189 ,198 ,162 ,202 ,199 ,200 ,201 ,203 ,208 ,204 ,206 ,207 ,209 ,163 ,213 ,210 ,211 ,212 ,214 ,221 ,215 ,216 ,220 ,222 ,227 ,223 ,225 ,226 ,228 ,164 ,232 ,229 ,230 ,231 ,233 ,165 ,237 ,234 ,235 ,236 ,238 ,243 ,239 ,241 ,242 ,244 ,166 ,248 ,245 ,246 ,247 ,249 ,253 ,250 ,251 ,252 ,254 ,167 ,'
    SET @UNICODE = ',225 ,224 ,7843,227 ,7841,259 ,7855,7857,7859,7861,7863,226 ,7845,7847,7849,7851,7853,233 ,232 ,7867,7869,7865,234 ,7871,7873,7875,7877,7879,237 ,236 ,7881,297 ,7883,243 ,242 ,7887,245 ,7885,244 ,7889,7891,7893,7895,7897,417 ,7899,7901,7903,7905,7907,250 ,249 ,7911,361 ,7909,432 ,7913,7915,7917,7919,7921,253 ,7923,7927,7929,7925,273 ,193 ,192 ,7842,195 ,7840,258 ,7854,7856,7858,7860,7862,194 ,7844,7846,7848,7850,7852,201 ,200 ,7866,7868,7864,202 ,7870,7872,7874,7876,7878,205 ,204 ,7880,296 ,7882,211 ,210 ,7886,213 ,7884,212 ,7888,7890,7892,7894,7896,416 ,7898,7900,7902,7904,7906,218 ,217 ,7910,360 ,7908,431 ,7912,7914,7916,7918,7920,221 ,7922,7926,7928,7924,272 ,'
    IF @strInput IS NULL RETURN NULL
    IF @strInput = '' RETURN NULL
    DECLARE @strOutput NVARCHAR(4000)
    DECLARE @COUNTER INT
    DECLARE @POSITION INT
    SET @COUNTER = 1
    SET @strOutput = ''
    WHILE (@COUNTER <= LEN(@strInput))
    BEGIN
        SET @POSITION = CHARINDEX(','+CONVERT(CHAR(4),ASCII(SUBSTRING(@strInput, @COUNTER, 1)))+',', @TCVN, 1)
        IF @POSITION > 0
            SET @strOutput = @strOutput + NCHAR(CONVERT(INT,SUBSTRING(@UNICODE, @POSITION+1, 4)))
        ELSE
            SET @strOutput = @strOutput + SUBSTRING(@strInput, @COUNTER, 1)
        SET @COUNTER = @COUNTER + 1
    END
    RETURN @strOutput
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Unicode2TCVN]'
GO

CREATE FUNCTION [dbo].[Unicode2TCVN] (@strInput NVARCHAR(4000))
RETURNS VARCHAR(4000)
AS
BEGIN
    DECLARE @TCVN CHAR(671)
    DECLARE @UNICODE CHAR(671)
    SET @TCVN = ',184 ,181 ,182 ,183 ,185 ,168 ,190 ,187 ,188 ,189 ,198 ,169 ,202 ,199 ,200 ,201 ,203 ,208 ,204 ,206 ,207 ,209 ,170 ,213 ,210 ,211 ,212 ,214 ,221 ,215 ,216 ,220 ,222 ,227 ,223 ,225 ,226 ,228 ,171 ,232 ,229 ,230 ,231 ,233 ,172 ,237 ,234 ,235 ,236 ,238 ,243 ,239 ,241 ,242 ,244 ,173 ,248 ,245 ,246 ,247 ,249 ,253 ,250 ,251 ,252 ,254 ,174 ,184 ,181 ,182 ,183 ,185 ,161 ,190 ,187 ,188 ,189 ,198 ,162 ,202 ,199 ,200 ,201 ,203 ,208 ,204 ,206 ,207 ,209 ,163 ,213 ,210 ,211 ,212 ,214 ,221 ,215 ,216 ,220 ,222 ,227 ,223 ,225 ,226 ,228 ,164 ,232 ,229 ,230 ,231 ,233 ,165 ,237 ,234 ,235 ,236 ,238 ,243 ,239 ,241 ,242 ,244 ,166 ,248 ,245 ,246 ,247 ,249 ,253 ,250 ,251 ,252 ,254 ,167 ,'
    SET @UNICODE = ',225 ,224 ,7843,227 ,7841,259 ,7855,7857,7859,7861,7863,226 ,7845,7847,7849,7851,7853,233 ,232 ,7867,7869,7865,234 ,7871,7873,7875,7877,7879,237 ,236 ,7881,297 ,7883,243 ,242 ,7887,245 ,7885,244 ,7889,7891,7893,7895,7897,417 ,7899,7901,7903,7905,7907,250 ,249 ,7911,361 ,7909,432 ,7913,7915,7917,7919,7921,253 ,7923,7927,7929,7925,273 ,193 ,192 ,7842,195 ,7840,258 ,7854,7856,7858,7860,7862,194 ,7844,7846,7848,7850,7852,201 ,200 ,7866,7868,7864,202 ,7870,7872,7874,7876,7878,205 ,204 ,7880,296 ,7882,211 ,210 ,7886,213 ,7884,212 ,7888,7890,7892,7894,7896,416 ,7898,7900,7902,7904,7906,218 ,217 ,7910,360 ,7908,431 ,7912,7914,7916,7918,7920,221 ,7922,7926,7928,7924,272 ,'
    IF @strInput IS NULL RETURN NULL
    IF @strInput = '' RETURN NULL
    DECLARE @strOutput VARCHAR(4000)
    DECLARE @COUNTER INT
    DECLARE @POSITION INT
    SET @COUNTER = 1
    SET @strOutput = ''
    WHILE (@COUNTER <= LEN(@strInput))
    BEGIN
        SET @POSITION = CHARINDEX(','+CONVERT(CHAR(4),UNICODE(SUBSTRING(@strInput, @COUNTER, 1)))+',', @UNICODE, 1)
        IF @POSITION > 0
            SET @strOutput = @strOutput + CHAR(CONVERT(INT,SUBSTRING(@TCVN, @POSITION+1, 4)))
        ELSE
            SET @strOutput = @strOutput + SUBSTRING(@strInput, @COUNTER, 1)
        SET @COUNTER = @COUNTER + 1
    END
    RETURN @strOutput
END

GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.5', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('1.5', getdate(), null)
	END
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'

COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
