﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TK_Container : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = null;
        DataTable dt = new DataTable();
        private KDT_VNACC_TK_Container Cont = null;

        public VNACC_TK_Container()
        {
            InitializeComponent();
        }
        private void LoadData()
        {
            Cont = TKMD.ContainerTKMD;
            SetContainer();
            dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(Cont.SoContainerCollection);
            grList.DataSource = dt;

        }
        private void GetContainer()
        {
            Cont.MaDiaDiem1 = ctrMaDiaDiem1.Code;
            Cont.MaDiaDiem2 = ctrMaDiaDiem2.Code;
            Cont.MaDiaDiem3 = ctrMaDiaDiem3.Code;
            Cont.MaDiaDiem4 = ctrMaDiaDiem4.Code;
            Cont.MaDiaDiem5 = ctrMaDiaDiem5.Code;
            Cont.TenDiaDiem = txtTenDiaDiem.Text;
            Cont.DiaChiDiaDiem = txtDiaChiDiaDiem.Text;
            GetSoContainer();
        }
        private void SetContainer()
        {
            ctrMaDiaDiem1.Code = Cont.MaDiaDiem1;
            ctrMaDiaDiem2.Code = Cont.MaDiaDiem2;
            ctrMaDiaDiem3.Code = Cont.MaDiaDiem3;
            ctrMaDiaDiem4.Code = Cont.MaDiaDiem4;
            ctrMaDiaDiem5.Code = Cont.MaDiaDiem5;
            txtTenDiaDiem.Text = Cont.TenDiaDiem;
            txtDiaChiDiaDiem.Text = Cont.DiaChiDiaDiem;
        }

        private void VNACC_TK_Container_Load(object sender, EventArgs e)
        {
            try
            {
                LoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {

            try
            {
                bool isAddNew = false;
                if (Cont == null)
                {
                    Cont = new KDT_VNACC_TK_Container();
                    isAddNew = true;
                }
                GetContainer();
                if (isAddNew)
                    TKMD.ContainerTKMD = Cont;
                Cont = new KDT_VNACC_TK_Container();
                Cont = null;

                //So container

                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
        private void GetSoContainer()
        {
            int sott = 1;
            Cont.SoContainerCollection.Clear();
            if (dt.Rows.Count == 0) return;
            foreach (DataRow i in dt.Rows)
            {
                KDT_VNACC_TK_SoContainer SoCont = new KDT_VNACC_TK_SoContainer();
                if (i["ID"].ToString().Length != 0)
                {
                    SoCont.ID = Convert.ToInt32(i["ID"].ToString());
                    SoCont.Master_id = Convert.ToInt32(i["Master_ID"].ToString());
                }
                SoCont.SoTT = sott;
                SoCont.SoContainer = i["SoContainer"].ToString();
                Cont.SoContainerCollection.Add(SoCont);
                sott++;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                if (ID > 0)
                {
                    if (ShowMessage("Bạn có muốn xóa số Container này không?", true) == "Yes")
                    {
                        KDT_VNACC_TK_SoContainer.DeleteDynamic("ID=" + ID);
                        grList.CurrentRow.Delete();
                    }
                }
                else
                    grList.CurrentRow.Delete();
                grList.Refetch();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void grList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

    }
}
