using System.Data;
using System.Data.Common;

namespace Company.BLL.DuLieuChuan
{
    public class LoaiPhieuChuyenTiep : BaseClass
    {
        public static DataTable SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_LoaiPhieuChuyenTiep ORDER BY TenPhieuChuyenTiep";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public static DataTable SelectBy_ID(string ID)
        {
            string query = string.Format("SELECT * FROM t_HaiQuan_LoaiPhieuChuyenTiep WHERE ID LIKE '{0}'", ID);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
    }
}