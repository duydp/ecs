using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class KDT_SXXK_BKHangTonKhoDangKy : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public string MaHaiQuan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public int LanThanhLy { set; get; }
		public int TrangThaiXuLy { set; get; }
		public string GUIDSTR { set; get; }
		public int QuyQuyetToan { set; get; }
		public int NamQuyetToan { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_SXXK_BKHangTonKhoDangKy> ConvertToCollection(IDataReader reader)
		{
			List<KDT_SXXK_BKHangTonKhoDangKy> collection = new List<KDT_SXXK_BKHangTonKhoDangKy>();
			while (reader.Read())
			{
				KDT_SXXK_BKHangTonKhoDangKy entity = new KDT_SXXK_BKHangTonKhoDangKy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuyQuyetToan"))) entity.QuyQuyetToan = reader.GetInt32(reader.GetOrdinal("QuyQuyetToan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamQuyetToan"))) entity.NamQuyetToan = reader.GetInt32(reader.GetOrdinal("NamQuyetToan"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_SXXK_BKHangTonKhoDangKy> collection, long id)
        {
            foreach (KDT_SXXK_BKHangTonKhoDangKy item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_SXXK_BKHangTonKhoDangKy VALUES(@SoTiepNhan, @NgayTiepNhan, @MaHaiQuan, @MaDoanhNghiep, @LanThanhLy, @TrangThaiXuLy, @GUIDSTR, @QuyQuyetToan, @NamQuyetToan)";
            string update = "UPDATE t_KDT_SXXK_BKHangTonKhoDangKy SET SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, LanThanhLy = @LanThanhLy, TrangThaiXuLy = @TrangThaiXuLy, GUIDSTR = @GUIDSTR, QuyQuyetToan = @QuyQuyetToan, NamQuyetToan = @NamQuyetToan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_SXXK_BKHangTonKhoDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuyQuyetToan", SqlDbType.Int, "QuyQuyetToan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamQuyetToan", SqlDbType.Int, "NamQuyetToan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuyQuyetToan", SqlDbType.Int, "QuyQuyetToan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamQuyetToan", SqlDbType.Int, "NamQuyetToan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_SXXK_BKHangTonKhoDangKy VALUES(@SoTiepNhan, @NgayTiepNhan, @MaHaiQuan, @MaDoanhNghiep, @LanThanhLy, @TrangThaiXuLy, @GUIDSTR, @QuyQuyetToan, @NamQuyetToan)";
            string update = "UPDATE t_KDT_SXXK_BKHangTonKhoDangKy SET SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, LanThanhLy = @LanThanhLy, TrangThaiXuLy = @TrangThaiXuLy, GUIDSTR = @GUIDSTR, QuyQuyetToan = @QuyQuyetToan, NamQuyetToan = @NamQuyetToan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_SXXK_BKHangTonKhoDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuyQuyetToan", SqlDbType.Int, "QuyQuyetToan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamQuyetToan", SqlDbType.Int, "NamQuyetToan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuyQuyetToan", SqlDbType.Int, "QuyQuyetToan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamQuyetToan", SqlDbType.Int, "NamQuyetToan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_SXXK_BKHangTonKhoDangKy Load(long id)
		{
			const string spName = "[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_SXXK_BKHangTonKhoDangKy> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_SXXK_BKHangTonKhoDangKy> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_SXXK_BKHangTonKhoDangKy> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_SXXK_BKHangTonKhoDangKy(long soTiepNhan, DateTime ngayTiepNhan, string maHaiQuan, string maDoanhNghiep, int lanThanhLy, int trangThaiXuLy, string gUIDSTR, int quyQuyetToan, int namQuyetToan)
		{
			KDT_SXXK_BKHangTonKhoDangKy entity = new KDT_SXXK_BKHangTonKhoDangKy();	
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.LanThanhLy = lanThanhLy;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GUIDSTR = gUIDSTR;
			entity.QuyQuyetToan = quyQuyetToan;
			entity.NamQuyetToan = namQuyetToan;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@QuyQuyetToan", SqlDbType.Int, QuyQuyetToan);
			db.AddInParameter(dbCommand, "@NamQuyetToan", SqlDbType.Int, NamQuyetToan);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_SXXK_BKHangTonKhoDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_BKHangTonKhoDangKy item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_SXXK_BKHangTonKhoDangKy(long id, long soTiepNhan, DateTime ngayTiepNhan, string maHaiQuan, string maDoanhNghiep, int lanThanhLy, int trangThaiXuLy, string gUIDSTR, int quyQuyetToan, int namQuyetToan)
		{
			KDT_SXXK_BKHangTonKhoDangKy entity = new KDT_SXXK_BKHangTonKhoDangKy();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.LanThanhLy = lanThanhLy;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GUIDSTR = gUIDSTR;
			entity.QuyQuyetToan = quyQuyetToan;
			entity.NamQuyetToan = namQuyetToan;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_SXXK_BKHangTonKhoDangKy_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@QuyQuyetToan", SqlDbType.Int, QuyQuyetToan);
			db.AddInParameter(dbCommand, "@NamQuyetToan", SqlDbType.Int, NamQuyetToan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_SXXK_BKHangTonKhoDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_BKHangTonKhoDangKy item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_SXXK_BKHangTonKhoDangKy(long id, long soTiepNhan, DateTime ngayTiepNhan, string maHaiQuan, string maDoanhNghiep, int lanThanhLy, int trangThaiXuLy, string gUIDSTR, int quyQuyetToan, int namQuyetToan)
		{
			KDT_SXXK_BKHangTonKhoDangKy entity = new KDT_SXXK_BKHangTonKhoDangKy();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.LanThanhLy = lanThanhLy;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GUIDSTR = gUIDSTR;
			entity.QuyQuyetToan = quyQuyetToan;
			entity.NamQuyetToan = namQuyetToan;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@QuyQuyetToan", SqlDbType.Int, QuyQuyetToan);
			db.AddInParameter(dbCommand, "@NamQuyetToan", SqlDbType.Int, NamQuyetToan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_SXXK_BKHangTonKhoDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_BKHangTonKhoDangKy item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_SXXK_BKHangTonKhoDangKy(long id)
		{
			KDT_SXXK_BKHangTonKhoDangKy entity = new KDT_SXXK_BKHangTonKhoDangKy();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_SXXK_BKHangTonKhoDangKy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_SXXK_BKHangTonKhoDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_BKHangTonKhoDangKy item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}