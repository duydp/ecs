﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components
{
    public partial class KDT_SXXK_LoaiPhuKien
    {
        List<KDT_SXXK_PhuKienDetail> _ListPhuKien = new List<KDT_SXXK_PhuKienDetail>();

        public List<KDT_SXXK_PhuKienDetail> ListPhuKienDeTail
        {
            set { this._ListPhuKien = value; }
            get { return this._ListPhuKien; }
        }

        public void DeleteFull(long id)
        {
            this.ID = id;
            KDT_SXXK_PhuKienDetail.DeleteDynamic("Master_ID = " + id);
            this.Delete();
        }
        public  static KDT_SXXK_LoaiPhuKien LoaiPhuKien_Load(long id)
        {
            KDT_SXXK_LoaiPhuKien LoaiPK = new KDT_SXXK_LoaiPhuKien();
            LoaiPK = KDT_SXXK_LoaiPhuKien.Load(id);
            if (LoaiPK != null && LoaiPK.ID > 0)
            {
                LoaiPK.ListPhuKienDeTail = KDT_SXXK_PhuKienDetail.SelectCollectionDynamic("Master_ID = " + LoaiPK.ID, "");
                return LoaiPK;
            }
            else
                return null;
        }
        public static KDT_SXXK_LoaiPhuKien LoaiPhuKien_Load(long idPKDK, string MaPK)
        {
            List<KDT_SXXK_LoaiPhuKien> listLoaiPK = KDT_SXXK_LoaiPhuKien.SelectCollectionDynamic("Master_ID = " + idPKDK + " AND MaPhuKien = '" + MaPK + "'", null);
            if (listLoaiPK != null && listLoaiPK.Count > 0)
            {
                KDT_SXXK_LoaiPhuKien LoaiPK = listLoaiPK[0];
                if (LoaiPK != null && LoaiPK.ID > 0)
                {
                    LoaiPK.ListPhuKienDeTail = KDT_SXXK_PhuKienDetail.SelectCollectionDynamic("Master_ID = " + LoaiPK.ID, "");
                    return LoaiPK;
                }
                else
                    return null;
            }
            else
                return null;
        }
        //public enum MaLoaiPhuKien
        //{
        //    NgayDuaVaoThanhKhoan = "501",
        //    HopDong = "502",
        //    MaHangHoa = "503",
        //    SoLuong = "504",
        //    DVT = "505"
        //}

    }
}
