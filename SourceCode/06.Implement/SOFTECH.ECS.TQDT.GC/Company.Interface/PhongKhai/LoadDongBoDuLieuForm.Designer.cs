﻿namespace Company.Interface.PhongKhai
{
    partial class LoadDongBoDuLieuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadDongBoDuLieuForm));
            this.txtMaHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMaDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDatabase = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamDangKy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnCapNhat = new Janus.Windows.EditControls.UIButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTrangthai = new System.Windows.Forms.Label();
            this.btnThoat = new Janus.Windows.EditControls.UIButton();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.uiProgressBar1 = new Janus.Windows.EditControls.UIProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiProgressBar1);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.txtTenDN);
            this.grbMain.Controls.Add(this.btnThoat);
            this.grbMain.Controls.Add(this.lblTrangthai);
            this.grbMain.Controls.Add(this.label4);
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.btnCapNhat);
            this.grbMain.Controls.Add(this.txtNamDangKy);
            this.grbMain.Controls.Add(this.txtDatabase);
            this.grbMain.Controls.Add(this.txtMaDN);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Controls.Add(this.txtMaHQ);
            this.grbMain.Size = new System.Drawing.Size(301, 91);
            this.grbMain.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.VS2005;
            this.grbMain.VisualStyleManager = null;
            // 
            // txtMaHQ
            // 
            this.txtMaHQ.Location = new System.Drawing.Point(104, 190);
            this.txtMaHQ.Name = "txtMaHQ";
            this.txtMaHQ.ReadOnly = true;
            this.txtMaHQ.Size = new System.Drawing.Size(215, 21);
            this.txtMaHQ.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 225);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mã Hải Quan";
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(113, 217);
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.ReadOnly = true;
            this.txtMaDN.Size = new System.Drawing.Size(215, 21);
            this.txtMaDN.TabIndex = 2;
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(109, 313);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(215, 21);
            this.txtDatabase.TabIndex = 3;
            this.txtDatabase.Text = "SXXK";
            this.txtDatabase.Visible = false;
            // 
            // txtNamDangKy
            // 
            this.txtNamDangKy.Location = new System.Drawing.Point(109, 340);
            this.txtNamDangKy.Name = "txtNamDangKy";
            this.txtNamDangKy.Size = new System.Drawing.Size(215, 21);
            this.txtNamDangKy.TabIndex = 4;
            this.txtNamDangKy.Visible = false;
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCapNhat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhat.Icon = ((System.Drawing.Icon)(resources.GetObject("btnCapNhat.Icon")));
            this.btnCapNhat.Location = new System.Drawing.Point(79, 284);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(83, 23);
            this.btnCapNhat.TabIndex = 214;
            this.btnCapNhat.Text = "Đồng bộ";
            this.btnCapNhat.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnCapNhat.VisualStyleManager = this.vsmMain;
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-5, 249);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 215;
            this.label2.Text = "Mã Doanh Nghiệp";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 313);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 216;
            this.label3.Text = "Database";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 344);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 217;
            this.label4.Text = "Năm Đăng ký";
            this.label4.Visible = false;
            // 
            // lblTrangthai
            // 
            this.lblTrangthai.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangthai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangthai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangthai.Location = new System.Drawing.Point(9, 48);
            this.lblTrangthai.Name = "lblTrangthai";
            this.lblTrangthai.Size = new System.Drawing.Size(285, 43);
            this.lblTrangthai.TabIndex = 218;
            this.lblTrangthai.Text = "TT";
            // 
            // btnThoat
            // 
            this.btnThoat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThoat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoat.Icon = ((System.Drawing.Icon)(resources.GetObject("btnThoat.Icon")));
            this.btnThoat.Location = new System.Drawing.Point(192, 284);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(82, 23);
            this.btnThoat.TabIndex = 219;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnThoat.VisualStyleManager = this.vsmMain;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(-5, 274);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 13);
            this.label5.TabIndex = 221;
            this.label5.Text = "Tên Doanh Nghiệp";
            // 
            // txtTenDN
            // 
            this.txtTenDN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDN.Location = new System.Drawing.Point(116, 245);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.ReadOnly = true;
            this.txtTenDN.Size = new System.Drawing.Size(215, 21);
            this.txtTenDN.TabIndex = 220;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // timer1
            // 
            this.timer1.Interval = 70;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // uiProgressBar1
            // 
            this.uiProgressBar1.Location = new System.Drawing.Point(12, 22);
            this.uiProgressBar1.Maximum = 300;
            this.uiProgressBar1.Name = "uiProgressBar1";
            this.uiProgressBar1.Size = new System.Drawing.Size(285, 23);
            this.uiProgressBar1.TabIndex = 222;
            this.uiProgressBar1.ValueChanged += new System.EventHandler(this.uiProgressBar1_ValueChanged);
            // 
            // LoadDongBoDuLieuForm
            // 
            this.ClientSize = new System.Drawing.Size(301, 91);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoadDongBoDuLieuForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Đồng Bộ Dữ Liệu";
            this.Load += new System.EventHandler(this.DongBoDuLieuByDNForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoadDongBoDuLieuForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQ;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamDangKy;
        private Janus.Windows.GridEX.EditControls.EditBox txtDatabase;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDN;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnCapNhat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTrangthai;
        private Janus.Windows.EditControls.UIButton btnThoat;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDN;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Timer timer1;
        private Janus.Windows.EditControls.UIProgressBar uiProgressBar1;
    }
}
