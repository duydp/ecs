﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_ChuyenCuaKhau_ManagerForm : BaseForm
    {

        List<KDT_VNACC_ToKhaiVanChuyen> ListTKVC = new List<KDT_VNACC_ToKhaiVanChuyen>();
        public KDT_VNACC_ToKhaiVanChuyen TKVC;
        public KDT_VNACC_ToKhaiVanChuyen TKVCSelect;
        public bool IsSelect = false;
        public string where = "";
        public VNACC_ChuyenCuaKhau_ManagerForm()
        {
            InitializeComponent();
        }

        private void VNACC_ChuyenCuaKhau_ManagerForm_Load(object sender, EventArgs e)
        {
            cbbPhanLoaiXuatNhap.SelectedValue = 0;
            cbbTrangThaiXuLy.SelectedValue = 0;
            //string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //ctrMaHaiQuan.Code = MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
            ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS; //Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(GlobalSettings.MA_HAI_QUAN)
            btnTimKiem_Click(null, null);
        }
        private void LoadData(string where)
        {
            ListTKVC = KDT_VNACC_ToKhaiVanChuyen.SelectCollectionDynamic(where, "");
            grList.DataSource = ListTKVC;
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                if (e.Row.RowType == RowType.Record)
                {
                    try
                    {
                        Cursor = Cursors.WaitCursor;

                        int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                        TKVC = new KDT_VNACC_ToKhaiVanChuyen();
                        TKVC = TKVC.LoadToKhai(id);
                        if (IsSelect)
                        {
                            TKVC = TKVCSelect;
                            this.DialogResult = DialogResult.OK;
                            this.Close();
                            return;
                        }
                        else
                        {
                            VNACC_ChuyenCuaKhau frm = new VNACC_ChuyenCuaKhau();
                            frm.TKVC = TKVC;
                            frm.ShowDialog();
                            btnTimKiem_Click(null, null);
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
                    }
                    finally { Cursor = Cursors.Default; }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string soToKhai = txtSoToKhai.Text.Trim();
                string LoaiHinh = (cbbPhanLoaiXuatNhap.SelectedValue.ToString() == "0") ? "" : cbbPhanLoaiXuatNhap.SelectedValue.ToString().Trim();
                string trangthai = (cbbTrangThaiXuLy.SelectedValue == null) ? "1" : cbbTrangThaiXuLy.SelectedValue.ToString().Trim();
                string haiquan = ctrMaHaiQuan.Code.Trim();
                where = String.Empty;
                if (trangthai != "")
                    where = where + "TrangThaiXuLy='" + trangthai + "'";
                if (soToKhai != "")
                    where = where + " and SoToKhai like '%" + soToKhai + "%' ";
                if (LoaiHinh != "")
                    where = where + " and CoBaoXuatNhapKhau='" + LoaiHinh + "' ";
                if (haiquan != "")
                    where = where + " and CoQuanHaiQuan='" + haiquan + "' ";
                if (txtNamDangKy.Text != "" && Convert.ToInt64(txtNamDangKy.Text) > 1900)
                    if (trangthai != "0" && trangthai != "1")
                        where = where + " and year(NgayDangKy)=" + txtNamDangKy.Text + " ";

                //if (ngayDangKy.ToString("dd/MM/yyyy") != "01/01/1900")
                //    where = where + " and NgayDangKy='" + ngayDangKy.ToString("yyyy/MM/dd") + "'";
                ListTKVC.Clear();

                LoadData(where);
                grList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
                this.Cursor = Cursors.Default;
            }
        }
        private void tsmiCopy_Click(object sender, EventArgs e)
        {
            if (grList.SelectedItems[0].RowType == RowType.Record)
            {
                try
                {
                    int id = Convert.ToInt32(grList.SelectedItems[0].GetRow().Cells["ID"].Value);
                    KDT_VNACC_ToKhaiVanChuyen TKMD = new KDT_VNACC_ToKhaiVanChuyen();
                    TKMD = TKMD.LoadToKhai(id);
                    CopyToKhai(TKMD, false);

                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage(ex.Message, false);
                }
            }

        }
        private void CopyToKhai(KDT_VNACC_ToKhaiVanChuyen tkmd, bool isCopyHangHoa)
        {
            KDT_VNACC_ToKhaiVanChuyen tkmdcopy = new KDT_VNACC_ToKhaiVanChuyen();
            HelperVNACCS.UpdateObject<KDT_VNACC_ToKhaiVanChuyen>(tkmdcopy, tkmd, false);
            tkmdcopy.NgayDangKy = new DateTime(1900, 1, 1);
            tkmdcopy.SoToKhaiVC = 0;
            tkmdcopy.NgayPheDuyetVC = new DateTime(1900, 1, 1);
            tkmdcopy.TrangThaiXuLy = "0";
            tkmdcopy.InputMessageID = string.Empty;
            tkmdcopy.MessageTag = string.Empty;
            tkmdcopy.IndexTag = string.Empty;
            if (tkmdcopy != null)
            {
                VNACC_ChuyenCuaKhau f = new VNACC_ChuyenCuaKhau();
                f.TKVC = tkmdcopy;
                f.ShowDialog(this);
            }

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                if (grList.CurrentRow.RowType == RowType.Record)
                {
                    int id = System.Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                    int TrangThai = System.Convert.ToInt32(grList.CurrentRow.Cells["TrangThaiXuLy"].Value.ToString());
                    if ((TrangThai == 0) || (TrangThai == 1))
                    {
                        if (ShowMessage("Bạn có muốn xóa tờ khai này không?", true) == "Yes")
                        {

                            KDT_VNACC_ToKhaiVanChuyen TKMD = new KDT_VNACC_ToKhaiVanChuyen();
                            TKMD.DeleteFull(id);
                            ShowMessage("Xóa tờ khai thành công.", false);
                        }
                    }
                    else
                        ShowMessage("Không thể xóa tờ khai đã được xác nhận với Hải quan. ", false);

                }
                btnTimKiem_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }

        private void grList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

        private void cbbTrangThaiXuLy_SelectedValueChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
        }

        private void cbbPhanLoaiXuatNhap_SelectedValueChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
        }

        private void grList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (e.Row.RowType == RowType.Record)
                {
                    string maLH = e.Row.Cells["CoBaoXuatNhapKhau"].Value.ToString().Trim();
                    if (maLH == "I")
                        e.Row.Cells["CoBaoXuatNhapKhau"].Text = maLH + " - Nhập khẩu";
                    else if (maLH == "E")
                        e.Row.Cells["CoBaoXuatNhapKhau"].Text = maLH + " - Xuất khẩu";
                    else
                        e.Row.Cells["CoBaoXuatNhapKhau"].Text = maLH + " - Loại khác";

                    string maPTVC = e.Row.Cells["MaPhuongTienVC"].Value.ToString().Trim();
         

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách tờ khai vận chuyển_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (ShowMessage("Bạn có muốn xuất kèm theo danh sách hàng hóa không ?", true) == "No")
                {
                    if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = grList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
                else
                {
                    if (sfNPL.ShowDialog(this) == DialogResult.Yes || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        grList.Tables[0].Columns.Add(new GridEXColumn("SoTTVanDon", ColumnType.Text, EditType.NoEdit) { Caption = "STT" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("SoVanDon", ColumnType.Text, EditType.NoEdit) { Caption = "Số vận đơn" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("NgayPhatHanhVD", ColumnType.Text, EditType.NoEdit) { Caption = "Ngày phát hành" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaHS", ColumnType.Text, EditType.NoEdit) { Caption = "Mã HS" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("KyHieuVaSoHieu", ColumnType.Text, EditType.NoEdit) { Caption = "Ký hiệu và số hiệu" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("NgayNhapKhoHQLanDau", ColumnType.Text, EditType.NoEdit) { Caption = "Ngày nhập kho HQ " });
                        grList.Tables[0].Columns.Add(new GridEXColumn("PhanLoaiSanPhan", ColumnType.Text, EditType.NoEdit) { Caption = "Phân loại sản phẩm" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaNuocSanXuat", ColumnType.Text, EditType.NoEdit) { Caption = "Mã nước sản xuất" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TenNuocSanXuat", ColumnType.Text, EditType.NoEdit) { Caption = "Tên nước sản xuất" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaDiaDiemXuatPhatVC", ColumnType.Text, EditType.NoEdit) { Caption = "Mã địa điểm xuất phát VC" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TenDiaDiemXuatPhatVC", ColumnType.Text, EditType.NoEdit) { Caption = "Tên địa điểm xuất phát VC" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TenDiaDiemDichVC", ColumnType.Text, EditType.NoEdit) { Caption = "Tên địa điểm đích VC" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaPhuongTienVC", ColumnType.Text, EditType.NoEdit) { Caption = "Mã PTVC" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TenPhuongTienVC", ColumnType.Text, EditType.NoEdit) { Caption = "Tên PTVC" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("NgayHangDuKienDenDi", ColumnType.Text, EditType.NoEdit) { Caption = "Ngày hàng dự kiến đi" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaNguoiNhapKhau", ColumnType.Text, EditType.NoEdit) { Caption = "Mã người nhập khẩu" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TenNguoiNhapKhau", ColumnType.Text, EditType.NoEdit) { Caption = "Tên người nhập khẩu" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("DiaChiNguoiNhapKhau", ColumnType.Text, EditType.NoEdit) { Caption = "Địa chỉ người nhập khẩu" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaNguoiXuatKhua", ColumnType.Text, EditType.NoEdit) { Caption = "Mã người xuất khẩu" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TenNguoiXuatKhau", ColumnType.Text, EditType.NoEdit) { Caption = "Tên người xuất khẩu" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("DiaChiNguoiXuatKhau", ColumnType.Text, EditType.NoEdit) { Caption = "Địa chỉ người xuất khẩu" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaNguoiUyThac", ColumnType.Text, EditType.NoEdit) { Caption = "Mã người ủy thác" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TenNguoiUyThac", ColumnType.Text, EditType.NoEdit) { Caption = "Tên người ủy thác" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("DiaChiNguoiUyThac", ColumnType.Text, EditType.NoEdit) { Caption = "Địa chỉ người ủy thác" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaDVTTriGia", ColumnType.Text, EditType.NoEdit) { Caption = "Mã ĐVT trị giá" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TriGia", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("SoLuong", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaDVTSoLuong", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TongTrongLuong", ColumnType.Text, EditType.NoEdit) { Caption = "Tổng trọng lượng" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaDVTTrongLuong", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TheTich", ColumnType.Text, EditType.NoEdit) { Caption = "Thể tích" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaDVTTheTich", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("SoGiayPhep", ColumnType.Text, EditType.NoEdit) { Caption = "Số giấy phép" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("NgayCapPhep", ColumnType.Text, EditType.NoEdit) { Caption = "Ngày cấp giấy phép" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("NgayHetHanCapPhep", ColumnType.Text, EditType.NoEdit) { Caption = "Ngày hết hạn giấy phép" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaDanhDauDDKH1", ColumnType.Text, EditType.NoEdit) { Caption = "Mã đánh dấu điểm điểm kho hàng 1 "});
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaDanhDauDDKH2", ColumnType.Text, EditType.NoEdit) { Caption = "Mã đánh dấu điểm điểm kho hàng 2 " });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaDanhDauDDKH3", ColumnType.Text, EditType.NoEdit) { Caption = "Mã đánh dấu điểm điểm kho hàng 3 " });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaDanhDauDDKH4", ColumnType.Text, EditType.NoEdit) { Caption = "Mã đánh dấu điểm điểm kho hàng 4 " });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaDanhDauDDKH5", ColumnType.Text, EditType.NoEdit) { Caption = "Mã đánh dấu điểm điểm kho hàng 5 " });


                        grList.DataSource = KDT_VNACC_ToKhaiVanChuyen.SelectDynamicFull(where, "").Tables[0];
                        gridEXExporter1.GridEX = grList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        grList.Tables[0].Columns.Remove("SoTTVanDon");
                        grList.Tables[0].Columns.Remove("SoVanDon");
                        grList.Tables[0].Columns.Remove("NgayPhatHanhVD");
                        grList.Tables[0].Columns.Remove("MaHS");
                        grList.Tables[0].Columns.Remove("KyHieuVaSoHieu");
                        grList.Tables[0].Columns.Remove("NgayNhapKhoHQLanDau");
                        grList.Tables[0].Columns.Remove("PhanLoaiSanPhan");
                        grList.Tables[0].Columns.Remove("MaNuocSanXuat");
                        grList.Tables[0].Columns.Remove("TenNuocSanXuat");
                        grList.Tables[0].Columns.Remove("MaDiaDiemXuatPhatVC");
                        grList.Tables[0].Columns.Remove("TenDiaDiemXuatPhatVC");
                        grList.Tables[0].Columns.Remove("LoaiHangHoa");
                        grList.Tables[0].Columns.Remove("TenDiaDiemDichVC");
                        grList.Tables[0].Columns.Remove("MaPhuongTienVC");
                        grList.Tables[0].Columns.Remove("TenPhuongTienVC");
                        grList.Tables[0].Columns.Remove("NgayHangDuKienDenDi");
                        grList.Tables[0].Columns.Remove("MaNguoiNhapKhau");
                        grList.Tables[0].Columns.Remove("TenNguoiNhapKhau");
                        grList.Tables[0].Columns.Remove("DiaChiNguoiNhapKhau");
                        grList.Tables[0].Columns.Remove("MaNguoiXuatKhua");
                        grList.Tables[0].Columns.Remove("TenNguoiXuatKhau");
                        grList.Tables[0].Columns.Remove("DiaChiNguoiXuatKhau");
                        grList.Tables[0].Columns.Remove("MaNguoiUyThac");
                        grList.Tables[0].Columns.Remove("TenNguoiUyThac");
                        grList.Tables[0].Columns.Remove("DiaChiNguoiUyThac");
                        grList.Tables[0].Columns.Remove("TriGia");
                        grList.Tables[0].Columns.Remove("MaDVTTriGia");
                        grList.Tables[0].Columns.Remove("SoLuong");
                        grList.Tables[0].Columns.Remove("MaDVTSoLuong");
                        grList.Tables[0].Columns.Remove("TongTrongLuong");
                        grList.Tables[0].Columns.Remove("MaDVTTrongLuong");
                        grList.Tables[0].Columns.Remove("TheTich");
                        grList.Tables[0].Columns.Remove("MaDVTTheTich");
                        grList.Tables[0].Columns.Remove("MaDanhDauDDKH1");
                        grList.Tables[0].Columns.Remove("MaDanhDauDDKH2");
                        grList.Tables[0].Columns.Remove("MaDanhDauDDKH3");
                        grList.Tables[0].Columns.Remove("MaDanhDauDDKH4");
                        grList.Tables[0].Columns.Remove("MaDanhDauDDKH5");
                        grList.Tables[0].Columns.Remove("SoGiayPhep");
                        grList.Tables[0].Columns.Remove("NgayCapPhep");
                        grList.Tables[0].Columns.Remove("NgayHetHanCapPhep");
                        grList.Refresh();
                        btnTimKiem_Click(null, null);
                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xử lý :\r\n " + ex.Message, false);
            }
        }

        private void cbbPhanLoaiXuatNhap_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null,null);
        }
    }
}
