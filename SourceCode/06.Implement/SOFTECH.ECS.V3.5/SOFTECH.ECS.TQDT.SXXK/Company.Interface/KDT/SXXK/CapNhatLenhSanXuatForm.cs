﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.KDT.GC
{
    public partial class CapNhatLenhSanXuatGCForm : Company.Interface.BaseForm
    {
        public DinhMuc DM = new DinhMuc();
        public DinhMucDangKy DMDK = new DinhMucDangKy();
        public KDT_SXXK_DinhMucThucTeDangKy dinhMucThucTeDangKy = new KDT_SXXK_DinhMucThucTeDangKy();
        public KDT_SXXK_DinhMucThucTe_SP DinhMucThucTeSP = new KDT_SXXK_DinhMucThucTe_SP();
        public KDT_SXXK_DinhMucThucTe_DinhMuc DinhMucThucTeDinhMuc = new KDT_SXXK_DinhMucThucTe_DinhMuc();
        public KDT_LenhSanXuat lenhSanXuat = new KDT_LenhSanXuat();
        public KDT_LenhSanXuat_SP lenhSanXuatSP = new KDT_LenhSanXuat_SP();
        public List<SXXK_DinhMuc> DinhMucCollection = new List<SXXK_DinhMuc>();
        public bool isExits = false;
        public CapNhatLenhSanXuatGCForm()
        {
            InitializeComponent();
        }

        private void CapNhatLenhSanXuatGCForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void BindDataSP()
        {
            try
            {
                grListSP.DataSource = null;
                grListSP.Refetch();
                grListSP.DataSource = DinhMuc.SelectDistinctCollectionDynamicSP(" Master_ID IN (SELECT ID FROM dbo.t_KDT_SXXK_DinhMucDangKy WHERE MaDinhDanhLenhSX = '" + (cbbLenhSanXuat.Value == null ? "" : cbbLenhSanXuat.Value.ToString()) + "')", "");
                grListSP.Refresh(); 
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LoadData()
        {
            try
            {
                cbbLenhSanXuat.DataSource = DinhMuc.SelectDistinctCollectionDynamicLSX(" Master_ID IN (SELECT ID FROM dbo.t_KDT_SXXK_DinhMucDangKy WHERE MaSanPham NOT IN (SELECT SP.MaSanPham FROM dbo.t_KDT_LenhSanXuat LSX INNER JOIN  t_KDT_LenhSanXuat_SP SP ON LSX.ID = SP.LenhSanXuat_ID )) AND MaDinhDanhLenhSX IS NOT NULL", "");
                cbbLenhSanXuat.DisplayMember = "MaDinhDanhLenhSX";
                cbbLenhSanXuat.ValueMember = "MaDinhDanhLenhSX";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataNPL()
        {
            try
            {
                DinhMucCollection DMCollection = new DinhMucCollection();
                DMCollection = DinhMuc.SelectCollectionDynamic("MaDinhDanhLenhSX = '" + cbbLenhSanXuat.Value.ToString() + "' AND MaSanPham ='" + DM.MaSanPham + "'", "");
                dgListNPL.Refetch();
                dgListNPL.DataSource = DMCollection;
                dgListNPL.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void grListSP_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DM = new DinhMuc();
                        DM = (DinhMuc)i.GetRow().DataRow;
                    }
                }
                BindDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(grListSP.RootTable.Columns["MaSanPham"], ConditionOperator.Contains, txtMaSP.Text);
                grListSP.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListNPL_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                    DM = new DinhMuc();
                    DM = (DinhMuc)e.Row.DataRow;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool Validate(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbbLenhSanXuat, errorProvider, "LỆNH SẢN XUẤT", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcTuNgay, errorProvider, "TỪ NGÀY", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcDenNgay, errorProvider, "ĐẾN NGÀY", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbTinhTrang, errorProvider, "TÌNH TRẠNG", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void GetValue()
        {
            try
            {
                if(lenhSanXuat==null)
                    lenhSanXuat = new KDT_LenhSanXuat();
                lenhSanXuat.SoLenhSanXuat = cbbLenhSanXuat.Value.ToString();
                lenhSanXuat.TuNgay = new DateTime(clcTuNgay.Value.Year, clcTuNgay.Value.Month, clcTuNgay.Value.Day, 00, 00, 00);
                lenhSanXuat.DenNgay = new DateTime(clcDenNgay.Value.Year, clcDenNgay.Value.Month, clcDenNgay.Value.Day, 23, 59, 59);
                lenhSanXuat.SoDonHang = txtPO.Text;
                lenhSanXuat.TinhTrang = Convert.ToInt32(cbbTinhTrang.SelectedValue);
                lenhSanXuat.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                lenhSanXuat.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                lenhSanXuat.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                lenhSanXuat.GhiChu = txtGhiChu.Text.ToString();             
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Get()
        {
            try
            {
                if (DMDK.ID > 0)
                {
                    dinhMucThucTeDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    dinhMucThucTeDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    dinhMucThucTeDangKy.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                    dinhMucThucTeDangKy.LenhSanXuat_ID = lenhSanXuat.ID;
                    dinhMucThucTeDangKy.SoTiepNhan = DMDK.SoTiepNhan;
                    dinhMucThucTeDangKy.NgayTiepNhan = DMDK.NgayTiepNhan;
                    dinhMucThucTeDangKy.TrangThaiXuLy = DMDK.TrangThaiXuLy;
                    dinhMucThucTeDangKy.GuidString = String.IsNullOrEmpty(DMDK.GUIDSTR) ? Guid.NewGuid().ToString().ToUpper() : DMDK.GUIDSTR;
                }
                else
                {
                    dinhMucThucTeDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    dinhMucThucTeDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    dinhMucThucTeDangKy.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                    dinhMucThucTeDangKy.LenhSanXuat_ID = lenhSanXuat.ID;
                    dinhMucThucTeDangKy.NgayTiepNhan = DateTime.Now;
                    dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Validate(false))
                    return;
                SanPhamCollection AllSPCollection = SanPham.SelectCollectionAll();
                NguyenPhuLieuCollection AllNPLCollection = NguyenPhuLieu.SelectCollectionAll();
                DinhMucCollection SPCollection = new DinhMucCollection();
                SPCollection = DinhMuc.SelectDistinctCollectionDynamicSP(" Master_ID IN (SELECT ID FROM dbo.t_KDT_GC_DinhMucDangKy ) AND MaDinhDanhLenhSX = '" + cbbLenhSanXuat.Value.ToString() + "'", "");
                if (SPCollection.Count == 0)
                {
                    ShowMessageTQDT("LỆNH SẢN XUẤT NÀY CHƯA CÓ DANH SÁCH SẢN PHẨM ĐỂ CẬP NHẬT . DOANH NGHIỆP HÃY KIỂM TRA LẠI", false);
                    return;
                }
                foreach (DinhMuc item in SPCollection)
                {
                    //Cập nhật lệnh sản xuất 
                    lenhSanXuatSP = new KDT_LenhSanXuat_SP();
                    lenhSanXuatSP.MaSanPham = item.MaSanPham;
                    lenhSanXuatSP.TenSanPham = item.TenSP;
                    foreach (SanPham sp in AllSPCollection)
                    {
                        isExits = false;
                        if (lenhSanXuatSP.MaSanPham == sp.Ma)
                        {
                            lenhSanXuatSP.TenSanPham = sp.Ten;
                            lenhSanXuatSP.DVT_ID = sp.DVT_ID;
                            lenhSanXuatSP.MaHS = sp.MaHS;
                            isExits = true;
                            break;
                        }
                    }
                    if (!isExits)
                    {
                        foreach (Company.BLL.KDT.SXXK.SXXK_SanPham sp in Company.BLL.KDT.SXXK.SXXK_SanPham.SelectCollectionDynamic(" MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "'", ""))
                        {
                            isExits = false;
                            if (lenhSanXuatSP.MaSanPham == sp.Ma)
                            {
                                lenhSanXuatSP.TenSanPham = sp.Ten;
                                lenhSanXuatSP.DVT_ID = sp.DVT_ID;
                                lenhSanXuatSP.MaHS = sp.MaHS;
                                isExits = true;
                                break;
                            }
                        }
                    }
                    lenhSanXuat.SPCollection.Add(lenhSanXuatSP);
                    //Cập nhật vào định mức thực tế theo từng giai đoạn
                    if (DMDK.ID == 0)
                    {
                        DMDK = new DinhMucDangKy();
                        DMDK = DinhMucDangKy.Load(item.Master_ID);
                    }

                    DinhMucThucTeSP = new KDT_SXXK_DinhMucThucTe_SP();
                    DinhMucThucTeSP.MaSanPham = lenhSanXuatSP.MaSanPham;
                    DinhMucThucTeSP.TenSanPham = lenhSanXuatSP.TenSanPham;
                    DinhMucThucTeSP.DVT_ID = lenhSanXuatSP.DVT_ID;
                    DinhMucThucTeSP.MaHS = lenhSanXuatSP.MaHS;

                    DinhMucCollection DMCollection = new DinhMucCollection();
                    DMCollection = DinhMuc.SelectCollectionDynamic("MaDinhDanhLenhSX = '" + cbbLenhSanXuat.Value.ToString() + "' AND MaSanPham ='" + item.MaSanPham + "'", "");
                    foreach (DinhMuc dinhmuc in DMCollection)
                    {
                        DinhMucThucTeDinhMuc = new KDT_SXXK_DinhMucThucTe_DinhMuc();
                        DinhMucThucTeDinhMuc.MaSanPham = dinhmuc.MaSanPham;
                        DinhMucThucTeDinhMuc.TenSanPham = dinhmuc.TenSP;
                        DinhMucThucTeDinhMuc.DVT_SP = lenhSanXuatSP.DVT_ID;
                        DinhMucThucTeDinhMuc.MaNPL = dinhmuc.MaNguyenPhuLieu;
                        DinhMucThucTeDinhMuc.TenNPL = dinhmuc.TenNPL;
                        foreach (NguyenPhuLieu npl in AllNPLCollection)
                        {
                            isExits = false;
                            if (DinhMucThucTeDinhMuc.MaNPL == npl.Ma)
                            {
                                DinhMucThucTeDinhMuc.TenNPL = npl.Ten;
                                DinhMucThucTeDinhMuc.MaHS = npl.MaHS;
                                DinhMucThucTeDinhMuc.DVT_NPL = npl.DVT_ID;
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu npl in Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionDynamic(" MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "'", ""))
                            {
                                isExits = false;
                                if (DinhMucThucTeDinhMuc.MaNPL == npl.Ma)
                                {
                                    DinhMucThucTeDinhMuc.TenNPL = npl.Ten;
                                    DinhMucThucTeDinhMuc.MaHS = npl.MaHS;
                                    DinhMucThucTeDinhMuc.DVT_NPL = npl.DVT_ID;
                                    isExits = true;
                                    break;
                                }
                            }
                        }
                        DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                        if (dinhmuc.TyLeHaoHut > 0)
                        {
                            DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung + dinhmuc.DinhMucSuDung * dinhmuc.TyLeHaoHut / 100;
                            DinhMucThucTeDinhMuc.TyLeHaoHut = 0;
                        }
                        DinhMucThucTeSP.DMCollection.Add(DinhMucThucTeDinhMuc);
                    }
                    dinhMucThucTeDangKy.SPCollection.Add(DinhMucThucTeSP);
                }
                GetValue();
                lenhSanXuat.InsertUpdateFull();
                Get();
                dinhMucThucTeDangKy.InsertUpdateFull();
                DinhMucCollection = SXXK_DinhMuc.SelectCollectionDynamic(" MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "'", "");
                foreach (SXXK_DinhMuc item in DinhMucCollection)
                {
                    foreach (KDT_LenhSanXuat_SP sp in lenhSanXuat.SPCollection)
                    {
                        if (sp.MaSanPham == item.MaSanPham)
                        {
                            if (item.LenhSanXuat_ID == 0)
                            {
                                item.LenhSanXuat_ID = lenhSanXuat.ID;
                                item.Update();
                            }
                        }
                    }
                }

                ShowMessageTQDT("Thông báo từ Hệ thống", "Cập nhật thành công", false);

                cbbLenhSanXuat.Text = String.Empty;
                txtPO.Text = String.Empty;
                txtGhiChu.Text = String.Empty;
                DMDK = new DinhMucDangKy();
                dinhMucThucTeDangKy = new KDT_SXXK_DinhMucThucTeDangKy();
                lenhSanXuat = new KDT_LenhSanXuat();
                
                LoadData();
                
                cbbLenhSanXuat_ValueChanged(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbbLenhSanXuat_ValueChanged(object sender, EventArgs e)
        {
            BindDataSP();
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            AutoUpdateLenhSanXuatForm f = new AutoUpdateLenhSanXuatForm();
            f.Show(this);
        }
    }
}
