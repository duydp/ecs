﻿using System;

using System.Data;
using System.Threading;
using System.Globalization;
using System.Resources;
using Company.Controls.CustomValidation;
using Company.KDT.SHARE.QuanLyChungTu;
/* LanNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.Utils;
using Company.KD.BLL.KDT.SXXK;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif
namespace Company.Interface
{
    public partial class AddChungTuGiayKTForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public ChungTuGiayKiemTra ChungTu;
        public GiayKiemTra GiayKiemTra;
        public AddChungTuGiayKTForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            if (save())
            {
                txtSoChungTu.Clear();
                txtTenChungTu.Clear();
                ChungTu = null;
            }
        }

        private void AddContainerForm_Load(object sender, EventArgs e)
        {
            cbLoaiChungTu.DataSource = LoaiChungTu.SelectAll().Tables[0];
            cbLoaiChungTu.DisplayMember = "Ten";
            cbLoaiChungTu.ValueMember = "ID";
            if (ChungTu != null)
            {
                txtSoChungTu.Text = ChungTu.SoChungTu;
                txtTenChungTu.Text = ChungTu.TenChungTu;
                cbLoaiChungTu.SelectedValue = ChungTu.LoaiChungTu;
                ccNgayTiepNhan.Value = ChungTu.NgayPhatHanh;
                ccNgayTiepNhan.Text = ccNgayTiepNhan.Value.ToString("dd/MM/yyyy");
            }

            bool isEnable = TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO
                            || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                            || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                            || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;

            btnSaveNew.Enabled = btnSave.Enabled = isEnable;
        }
        private bool checkSoHieu(string SoChungTu)
        {
            foreach (ChungTuGiayKiemTra c in GiayKiemTra.ChungTuCollection)
                if (c.SoChungTu.Trim().ToUpper() == SoChungTu.ToUpper().Trim())
                {
                    return true;
                }
            return false;
        }
        private bool save()
        {
            cvError.Validate();
            if (!cvError.IsValid)
            {
                return false;
            }
            if (TKMD.VanTaiDon == null)
            {
                TKMD.VanTaiDon = new VanDon();
            }

            GiayKiemTra.ChungTuCollection.Remove(ChungTu);

            if (checkSoHieu(txtSoChungTu.Text))
            {
//                 if (ChungTu != null)
//                     TKMD.VanTaiDon.ContainerCollection.Add(ChungTu);
//                 TKMD.VanTaiDon.ContainerCollection.Remove(ChungTu);
                ShowMessage("Đã có số chứng từ này.", false);
                return false;
            }
            if (ChungTu == null)
                ChungTu = new ChungTuGiayKiemTra();
            ChungTu.TenChungTu = txtTenChungTu.Text.Trim();
            ChungTu.LoaiChungTu = cbLoaiChungTu.SelectedValue.ToString();
            ChungTu.SoChungTu = txtSoChungTu.Text.Trim();
            ChungTu.NgayPhatHanh = ccNgayTiepNhan.Value;
            GiayKiemTra.ChungTuCollection.Add(ChungTu);

//             if (cbLoaiChungTu.SelectedValue.ToString() == "2")
//                 TKMD.SoContainer20++;
//             else
//                 TKMD.SoContainer40++;
            return true;

        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (save())
                this.Close();
        }






    }
}