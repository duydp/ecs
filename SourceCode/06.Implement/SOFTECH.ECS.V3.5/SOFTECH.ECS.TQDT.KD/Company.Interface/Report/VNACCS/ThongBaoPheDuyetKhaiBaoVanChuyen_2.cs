using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class ThongBaoPheDuyetKhaiBaoVanChuyen_2 : DevExpress.XtraReports.UI.XtraReport
    {
        public ThongBaoPheDuyetKhaiBaoVanChuyen_2()
        {
            InitializeComponent();
        }
        public void BindingReport(VAS5050 vas504)
        {
            lblTenThongTinXuat.Text = vas504.AB.GetValue().ToString();
            lblMaCoBaoYeuCauXN.Text = vas504.KA.GetValue().ToString().ToUpper();
            lblTenCoBaoYeuCauXN.Text = vas504.KB.GetValue().ToString().ToUpper();
            lblCoQuanHQ.Text = vas504.AD.GetValue().ToString().ToUpper();
            lblSoToKhaiVC.Text = vas504.AE.GetValue().ToString().ToUpper();
            lblCoBaoXNK.Text = vas504.ED.GetValue().ToString().ToUpper();
            int SoCont = vas504.HangHoa.Count;
            if (SoCont <= 20)
                TongSoTrang.Text = System.Convert.ToDecimal(5).ToString();
            else
            {
                TongSoTrang.Text = System.Convert.ToDecimal(5 + Math.Round((decimal)SoCont / 20, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            if (Convert.ToDateTime(vas504.AF.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayLapTK.Text = Convert.ToDateTime(vas504.AF.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayLapTK.Text = "";
            }
            //for (int i = 0; i < vas504.DG.listAttribute[0].ListValue.Count; i++)
            //{

            //            lblSoTTDongHang.Text = vas504.DG.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
            //            lblSoHangHoa.Text = vas504.DG.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
            //            if (Convert.ToDateTime(vas504.DG.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
            //            {
            //                lblNgayPhatHanhVanDon.Text = Convert.ToDateTime(vas504.DG.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
            //            }
            //            else
            //            {
            //                lblNgayPhatHanhVanDon.Text = "";
            //            }
            //            lblMoTaHangHoa.Text = vas504.DG.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
            //            lblMaHS.Text = vas504.DG.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
            //            lblKyHieuSoHieu.Text = vas504.DG.listAttribute[5].GetValueCollection(i).ToString().ToUpper();
            //            if (Convert.ToDateTime(vas504.DG.listAttribute[6].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
            //            {
            //                lblNgayNhapKhoHQ.Text = Convert.ToDateTime(vas504.DG.listAttribute[6].GetValueCollection(i)).ToString("dd/MM/yyyy");
            //            }
            //            else
            //            {
            //                lblNgayNhapKhoHQ.Text = "";
            //            }
            //            lblPhanLoaiSP.Text = vas504.DG.listAttribute[7].GetValueCollection(i).ToString().ToUpper();
            //            lblMaNuocSX.Text = vas504.DG.listAttribute[8].GetValueCollection(i).ToString().ToUpper();
            //            lblTenNuocSX.Text = vas504.DG.listAttribute[9].GetValueCollection(i).ToString().ToUpper();
            //            lblMaDiaDiemXuatPhat.Text = vas504.DG.listAttribute[10].GetValueCollection(i).ToString().ToUpper();
            //            lblTenDiaDiemXuatPhat.Text = vas504.DG.listAttribute[11].GetValueCollection(i).ToString().ToUpper();
            //            lblMaDiaDiemDich.Text = vas504.DG.listAttribute[12].GetValueCollection(i).ToString().ToUpper();
            //            lblTenDiaDiemDich.Text = vas504.DG.listAttribute[13].GetValueCollection(i).ToString().ToUpper();
            //            lblLoaiManifest.Text = vas504.DG.listAttribute[14].GetValueCollection(i).ToString().ToUpper();
            //            lblMaPhuongTienVC2.Text = vas504.DG.listAttribute[15].GetValueCollection(i).ToString().ToUpper();
            //            lblTenPhuongTienVC2.Text = vas504.DG.listAttribute[16].GetValueCollection(i).ToString().ToUpper();
            //            if (Convert.ToDateTime(vas504.DG.listAttribute[17].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
            //            {
            //                lblNgayDuKienDenDi.Text = Convert.ToDateTime(vas504.DG.listAttribute[17].GetValueCollection(i)).ToString("dd/MM/yyyy");
            //            }
            //            else
            //            {
            //                lblNgayDuKienDenDi.Text = "";
            //            }
            //            lblMaNguoiNK.Text = vas504.DG.listAttribute[18].GetValueCollection(i).ToString().ToUpper();
            //            lblTenNguoiNK.Text = vas504.DG.listAttribute[19].GetValueCollection(i).ToString().ToUpper();
            //            lblDiaChiNguoiNK.Text = vas504.DG.listAttribute[20].GetValueCollection(i).ToString().ToUpper();
            //            lblMaNguoiXK.Text = vas504.DG.listAttribute[21].GetValueCollection(i).ToString().ToUpper();
            //            lblTenNguoiXK.Text = vas504.DG.listAttribute[22].GetValueCollection(i).ToString().ToUpper();
            //            lblDiaChiNguoiXK.Text = vas504.DG.listAttribute[23].GetValueCollection(i).ToString().ToUpper();
            //            lblMaNguoiUyThac.Text = vas504.DG.listAttribute[24].GetValueCollection(i).ToString().ToUpper();
            //            lblTenNguoiUyThac.Text = vas504.DG.listAttribute[25].GetValueCollection(i).ToString().ToUpper();
            //            lblDiaChiNguoiUyThac.Text = vas504.DG.listAttribute[26].GetValueCollection(i).ToString().ToUpper();
            //            lblMaVBPL1.Text = vas504.DG.listAttribute[27].GetValueCollection(i).ToString().ToUpper();
            //            lblMaVBPL2.Text = vas504.DG.listAttribute[28].GetValueCollection(i).ToString().ToUpper();
            //            lblMaVBPL3.Text = vas504.DG.listAttribute[29].GetValueCollection(i).ToString().ToUpper();
            //            lblMaVBPL4.Text = vas504.DG.listAttribute[30].GetValueCollection(i).ToString().ToUpper();
            //            lblMaVBPL5.Text = vas504.DG.listAttribute[31].GetValueCollection(i).ToString().ToUpper();
            //            lblMaDVTTriGia.Text = vas504.DG.listAttribute[32].GetValueCollection(i).ToString().ToUpper();
            //            lblTriGia.Text = vas504.DG.listAttribute[33].GetValueCollection(i).ToString().ToUpper();
            //            lblSoLuong.Text = vas504.DG.listAttribute[34].GetValueCollection(i).ToString().ToUpper();
            //            lblMaDVTSoLuong.Text = vas504.DG.listAttribute[35].GetValueCollection(i).ToString().ToUpper();
            //            lblTongTrongLuong.Text = vas504.DG.listAttribute[36].GetValueCollection(i).ToString().ToUpper();
            //            lblMaDVTTrongLuong.Text = vas504.DG.listAttribute[37].GetValueCollection(i).ToString().ToUpper();
            //            lblTheTich.Text = vas504.DG.listAttribute[38].GetValueCollection(i).ToString().ToUpper();
            //            lblMaDVTTheTich.Text = vas504.DG.listAttribute[39].GetValueCollection(i).ToString().ToUpper();
            //            lblMaDanhDauDDKhoiHanh1.Text = vas504.DG.listAttribute[40].GetValueCollection(i).ToString().ToUpper();
            //            lblMaDanhDauDDKhoiHanh2.Text = vas504.DG.listAttribute[41].GetValueCollection(i).ToString().ToUpper();
            //            lblMaDanhDauDDKhoiHanh3.Text = vas504.DG.listAttribute[42].GetValueCollection(i).ToString().ToUpper();
            //            lblMaDanhDauDDKhoiHanh4.Text = vas504.DG.listAttribute[43].GetValueCollection(i).ToString().ToUpper();
            //            lblMaDanhDauDDKhoiHanh5.Text = vas504.DG.listAttribute[44].GetValueCollection(i).ToString().ToUpper();
            //            lblSoGiayPhep.Text = vas504.DG.listAttribute[45].GetValueCollection(i).ToString().ToUpper().ToUpper();
            //            if (Convert.ToDateTime(vas504.DG.listAttribute[46].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
            //            {
            //                lblNgayCapPhep.Text = Convert.ToDateTime(vas504.DG.listAttribute[46].GetValueCollection(i)).ToString("dd/MM/yyyy");
            //            }
            //            else
            //            {
            //                lblNgayCapPhep.Text = "";
            //            }
            //            if (Convert.ToDateTime(vas504.DG.listAttribute[47].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
            //            {
            //                lblNgayHetHanCapPhep.Text = Convert.ToDateTime(vas504.DG.listAttribute[47].GetValueCollection(i)).ToString("dd/MM/yyyy");
            //            }
            //            else
            //            {
            //                lblNgayHetHanCapPhep.Text = "";
            //            }
            //            lblGhiChu2.Text = vas504.DG.listAttribute[48].GetValueCollection(i).ToString().ToUpper();
                      
            //}

            DataTable dt = ConvertListToTable(vas504.DG, 4);
            BindingReportHang(dt);
        }

        private DataTable ConvertListToTable( GroupAttribute group, int loop)
        {
            int STT = 0;
            DataTable gr = new DataTable();
            foreach (PropertiesAttribute attribute in group.listAttribute)
            {
                gr.Columns.Add(attribute.GroupID, attribute.OfType == typeof(int) ? typeof(decimal) : attribute.OfType );
            }
            gr.Columns.Add("STT", typeof(string));
            for (int i = 0; i < loop; i++)
            {
                STT++;
                DataRow dr = gr.NewRow();                       
                foreach (PropertiesAttribute attribute in group.listAttribute)
                {
                    
                    if (attribute.OfType == typeof(DateTime))
                    {
                        if (System.Convert.ToDateTime(attribute.GetValueCollection(i)).Year > 1900)
                            dr[attribute.GroupID] = attribute.GetValueCollection(i);
                    }
                    else if (attribute.OfType == typeof(decimal) || attribute.OfType == typeof(int))
                    {
                        if (System.Convert.ToDecimal(attribute.GetValueCollection(i)) != 0)
                            dr[attribute.GroupID] = attribute.GetValueCollection(i);
                    }
                    else
                        dr[attribute.GroupID] = attribute.GetValueCollection(i);
                   
                }
                dr["STT"] = STT.ToString().ToUpper();
                gr.Rows.Add(dr);
            }
            return gr;
        }
        public void BindingReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSoTTDongHang.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblSoHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BP1");
            lblNgayPhatHanhVanDon.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BQ1","{0 : dd/MM/yyyy}");
            lblMoTaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BT1");
            lblMaHS.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BV1");
            lblKyHieuSoHieu.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BU1");
            lblNgayNhapKhoHQ.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BR1", "{0 : dd/MM/yyyy}");
            lblPhanLoaiSP.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BS1");
            lblMaNuocSX.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BW1");
            lblTenNuocSX.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BX1");
            lblMaDiaDiemXuatPhat.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BY1");
            lblTenDiaDiemXuatPhat.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BZ1");
            lblMaDiaDiemDich.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CA1");
            lblTenDiaDiemDich.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CB1");
            lblLoaiManifest.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CC1");
            lblMaPhuongTienVC2.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CD1");
            lblTenPhuongTienVC2.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CE1");
            lblNgayDuKienDenDi.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CF1", "{0 : dd/MM/yyyy}");
            lblMaNguoiNK.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_YA1");
            lblTenNguoiNK.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_YB1");
            lblDiaChiNguoiNK.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_YC1");
            lblMaNguoiXK.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_WA1");
            lblTenNguoiXK.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_WB1");
            lblDiaChiNguoiXK.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_WC1");
            lblMaNguoiUyThac.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_WD1");
            lblTenNguoiUyThac.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_WE1");
            lblDiaChiNguoiUyThac.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_WF1");
            lblMaVBPL1.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_OA1");
            lblMaVBPL2.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_OA2");
            lblMaVBPL3.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_OA3");
            lblMaVBPL4.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_OA4");
            lblMaVBPL5.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_OA5");
            lblMaDVTTriGia.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CM1");
            lblTriGia.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CN1");
            lblSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CO1");
            lblMaDVTSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CP1");
            lblTongTrongLuong.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CQ1");
            lblMaDVTTrongLuong.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CR1");
            lblTheTich.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CS1");
            lblMaDVTTheTich.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CT1");
            lblMaDanhDauDDKhoiHanh1.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_RA1");
            lblMaDanhDauDDKhoiHanh2.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_RA2");
            lblMaDanhDauDDKhoiHanh3.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_RA3");
            lblMaDanhDauDDKhoiHanh4.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_RA4");
            lblMaDanhDauDDKhoiHanh5.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_RA5");
            lblSoGiayPhep.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BM1");
            lblNgayCapPhep.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BN1", "{0 : dd/MM/yyyy}");
            lblNgayHetHanCapPhep.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_BO1", "{0 : dd/MM/yyyy}");
            lblGhiChu2.DataBindings.Add("Text", DetailReport.DataSource, "VAS5050_CV1");


        }

        private void lblSoTTDongHang_AfterPrint(object sender, EventArgs e)
        {
            XRControl cell = (XRControl)sender;
            cell.Text = "3";
            lblSoTrang.Text = "3";
        }

    }
}
