namespace Company.Interface.SXXK.BangKe
{
    partial class BKNPLCungUngDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgList2_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BKNPLCungUngDetailForm));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.grpThongTinNPL = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbNhomLyDo = new Janus.Windows.EditControls.UIComboBox();
            this.clcNgayChungTu = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.txtDonGiaTT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtThueXNK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtLuongNPL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtGiaiTrinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnLuu = new Janus.Windows.EditControls.UIButton();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSoChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTenSanPham = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaSanPham = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList2 = new Janus.Windows.GridEX.GridEX();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtLuongSanPham = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnSave_BK = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinNPL)).BeginInit();
            this.grpThongTinNPL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnXoa);
            this.grbMain.Controls.Add(this.btnSave_BK);
            this.grbMain.Controls.Add(this.grpThongTinNPL);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Size = new System.Drawing.Size(813, 554);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(9, 282);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(249, 14);
            this.label5.TabIndex = 1;
            this.label5.Text = "Danh sách NPL cung ứng cho sản phẩm";
            // 
            // grpThongTinNPL
            // 
            this.grpThongTinNPL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpThongTinNPL.BackColor = System.Drawing.Color.Transparent;
            this.grpThongTinNPL.Controls.Add(this.cbbNhomLyDo);
            this.grpThongTinNPL.Controls.Add(this.clcNgayChungTu);
            this.grpThongTinNPL.Controls.Add(this.txtDonGiaTT);
            this.grpThongTinNPL.Controls.Add(this.txtThueXNK);
            this.grpThongTinNPL.Controls.Add(this.txtLuongNPL);
            this.grpThongTinNPL.Controls.Add(this.txtGiaiTrinh);
            this.grpThongTinNPL.Controls.Add(this.txtTenNPL);
            this.grpThongTinNPL.Controls.Add(this.btnLuu);
            this.grpThongTinNPL.Controls.Add(this.label12);
            this.grpThongTinNPL.Controls.Add(this.txtSoChungTu);
            this.grpThongTinNPL.Controls.Add(this.label11);
            this.grpThongTinNPL.Controls.Add(this.txtMaNPL);
            this.grpThongTinNPL.Controls.Add(this.label8);
            this.grpThongTinNPL.Controls.Add(this.label14);
            this.grpThongTinNPL.Controls.Add(this.label13);
            this.grpThongTinNPL.Controls.Add(this.label7);
            this.grpThongTinNPL.Controls.Add(this.label3);
            this.grpThongTinNPL.Controls.Add(this.label6);
            this.grpThongTinNPL.Controls.Add(this.label2);
            this.grpThongTinNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpThongTinNPL.Location = new System.Drawing.Point(14, 105);
            this.grpThongTinNPL.Name = "grpThongTinNPL";
            this.grpThongTinNPL.Size = new System.Drawing.Size(789, 174);
            this.grpThongTinNPL.TabIndex = 0;
            this.grpThongTinNPL.Text = "Thông tin NPL cung ứng";
            this.grpThongTinNPL.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grpThongTinNPL.VisualStyleManager = this.vsmMain;
            // 
            // cbbNhomLyDo
            // 
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Tự cung ứng";
            uiComboBoxItem1.Value = ((short)(1));
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Loai khác";
            uiComboBoxItem2.Value = 9;
            this.cbbNhomLyDo.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbbNhomLyDo.Location = new System.Drawing.Point(373, 82);
            this.cbbNhomLyDo.Name = "cbbNhomLyDo";
            this.cbbNhomLyDo.Size = new System.Drawing.Size(330, 21);
            this.cbbNhomLyDo.TabIndex = 7;
            this.cbbNhomLyDo.Text = "Chọn nhóm lý do giải trình";
            this.cbbNhomLyDo.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayChungTu
            // 
            this.clcNgayChungTu.Location = new System.Drawing.Point(373, 52);
            this.clcNgayChungTu.Name = "clcNgayChungTu";
            this.clcNgayChungTu.ReadOnly = false;
            this.clcNgayChungTu.Size = new System.Drawing.Size(108, 21);
            this.clcNgayChungTu.TabIndex = 6;
            this.clcNgayChungTu.TagName = "";
            this.clcNgayChungTu.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayChungTu.WhereCondition = "";
            // 
            // txtDonGiaTT
            // 
            this.txtDonGiaTT.DecimalDigits = 6;
            this.txtDonGiaTT.Location = new System.Drawing.Point(104, 111);
            this.txtDonGiaTT.Name = "txtDonGiaTT";
            this.txtDonGiaTT.Size = new System.Drawing.Size(163, 21);
            this.txtDonGiaTT.TabIndex = 5;
            this.txtDonGiaTT.Text = "0.000000";
            this.txtDonGiaTT.Value = new decimal(new int[] {
            0,
            0,
            0,
            393216});
            this.txtDonGiaTT.VisualStyleManager = this.vsmMain;
            // 
            // txtThueXNK
            // 
            this.txtThueXNK.DecimalDigits = 4;
            this.txtThueXNK.Location = new System.Drawing.Point(104, 141);
            this.txtThueXNK.Name = "txtThueXNK";
            this.txtThueXNK.Size = new System.Drawing.Size(163, 21);
            this.txtThueXNK.TabIndex = 5;
            this.txtThueXNK.Text = "0.0000";
            this.txtThueXNK.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtThueXNK.VisualStyleManager = this.vsmMain;
            // 
            // txtLuongNPL
            // 
            this.txtLuongNPL.DecimalDigits = 4;
            this.txtLuongNPL.Location = new System.Drawing.Point(104, 82);
            this.txtLuongNPL.Name = "txtLuongNPL";
            this.txtLuongNPL.Size = new System.Drawing.Size(163, 21);
            this.txtLuongNPL.TabIndex = 5;
            this.txtLuongNPL.Text = "0.0000";
            this.txtLuongNPL.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtLuongNPL.VisualStyleManager = this.vsmMain;
            // 
            // txtGiaiTrinh
            // 
            this.txtGiaiTrinh.Location = new System.Drawing.Point(373, 111);
            this.txtGiaiTrinh.Multiline = true;
            this.txtGiaiTrinh.Name = "txtGiaiTrinh";
            this.txtGiaiTrinh.Size = new System.Drawing.Size(330, 51);
            this.txtGiaiTrinh.TabIndex = 3;
            this.txtGiaiTrinh.VisualStyleManager = this.vsmMain;
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.Location = new System.Drawing.Point(373, 21);
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.Size = new System.Drawing.Size(406, 21);
            this.txtTenNPL.TabIndex = 3;
            this.txtTenNPL.VisualStyleManager = this.vsmMain;
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLuu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLuu.Icon")));
            this.btnLuu.Location = new System.Drawing.Point(709, 145);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(70, 23);
            this.btnLuu.TabIndex = 3;
            this.btnLuu.Text = "Ghi";
            this.btnLuu.VisualStyleManager = this.vsmMain;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(9, 115);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Đơn giá TT";
            // 
            // txtSoChungTu
            // 
            this.txtSoChungTu.Location = new System.Drawing.Point(104, 52);
            this.txtSoChungTu.Name = "txtSoChungTu";
            this.txtSoChungTu.Size = new System.Drawing.Size(163, 21);
            this.txtSoChungTu.TabIndex = 3;
            this.txtSoChungTu.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(9, 145);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Thuế suất";
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaNPL.Location = new System.Drawing.Point(104, 21);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(163, 21);
            this.txtMaNPL.TabIndex = 3;
            this.txtMaNPL.VisualStyleManager = this.vsmMain;
            this.txtMaNPL.ButtonClick += new System.EventHandler(this.txtMaNPL_ButtonClick);
            this.txtMaNPL.Leave += new System.EventHandler(this.txtMaNPL_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Lượng cung ứng";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(279, 110);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Giải trình";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(279, 86);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Nhóm giải trình";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(279, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Ngày chứng từ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(279, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tên NPL";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Số chứng từ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã NPL";
            // 
            // txtTenSanPham
            // 
            this.txtTenSanPham.Location = new System.Drawing.Point(373, 56);
            this.txtTenSanPham.Multiline = true;
            this.txtTenSanPham.Name = "txtTenSanPham";
            this.txtTenSanPham.ReadOnly = true;
            this.txtTenSanPham.Size = new System.Drawing.Size(406, 21);
            this.txtTenSanPham.TabIndex = 3;
            this.txtTenSanPham.VisualStyleManager = this.vsmMain;
            // 
            // txtMaSanPham
            // 
            this.txtMaSanPham.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaSanPham.Location = new System.Drawing.Point(94, 57);
            this.txtMaSanPham.Name = "txtMaSanPham";
            this.txtMaSanPham.Size = new System.Drawing.Size(163, 21);
            this.txtMaSanPham.TabIndex = 3;
            this.txtMaSanPham.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(263, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên sản phẩm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mã sản phẩm";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(721, 531);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgList2);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(12, 299);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(789, 228);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgList2
            // 
            this.dgList2.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList2.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList2.AlternatingColors = true;
            this.dgList2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList2.AutomaticSort = false;
            this.dgList2.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList2.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList2_DesignTimeLayout.LayoutString = resources.GetString("dgList2_DesignTimeLayout.LayoutString");
            this.dgList2.DesignTimeLayout = dgList2_DesignTimeLayout;
            this.dgList2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList2.GroupByBoxVisible = false;
            this.dgList2.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList2.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList2.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList2.ImageList = this.ImageList1;
            this.dgList2.Location = new System.Drawing.Point(2, 11);
            this.dgList2.Name = "dgList2";
            this.dgList2.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList2.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList2.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList2.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList2.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList2.Size = new System.Drawing.Size(786, 215);
            this.dgList2.TabIndex = 0;
            this.dgList2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList2.VisualStyleManager = this.vsmMain;
            this.dgList2.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList2_RowDoubleClick);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Số tờ khai";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Location = new System.Drawing.Point(94, 20);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.ReadOnly = true;
            this.txtSoToKhai.Size = new System.Drawing.Size(163, 21);
            this.txtSoToKhai.TabIndex = 3;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtMaSanPham);
            this.uiGroupBox1.Controls.Add(this.txtLuongSanPham);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.txtTenSanPham);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(14, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(789, 90);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông tin sản phẩm";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtLuongSanPham
            // 
            this.txtLuongSanPham.DecimalDigits = 4;
            this.txtLuongSanPham.Location = new System.Drawing.Point(373, 20);
            this.txtLuongSanPham.Name = "txtLuongSanPham";
            this.txtLuongSanPham.Size = new System.Drawing.Size(140, 21);
            this.txtLuongSanPham.TabIndex = 5;
            this.txtLuongSanPham.Text = "0.0000";
            this.txtLuongSanPham.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtLuongSanPham.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(259, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Lượng sản phẩm";
            // 
            // btnSave_BK
            // 
            this.btnSave_BK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave_BK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave_BK.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave_BK.Icon")));
            this.btnSave_BK.Location = new System.Drawing.Point(503, 530);
            this.btnSave_BK.Name = "btnSave_BK";
            this.btnSave_BK.Size = new System.Drawing.Size(114, 23);
            this.btnSave_BK.TabIndex = 3;
            this.btnSave_BK.Text = "Lưu bảng kê ";
            this.btnSave_BK.VisualStyleManager = this.vsmMain;
            this.btnSave_BK.Click += new System.EventHandler(this.btnSave_BK_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(623, 531);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(92, 23);
            this.btnXoa.TabIndex = 3;
            this.btnXoa.Text = "Xóa NPL";
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // BKNPLCungUngDetailForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(813, 554);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BKNPLCungUngDetailForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Bảng kê nguyên phụ liệu tự cung ứng";
            this.Load += new System.EventHandler(this.BKNPLCungUngDetailForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinNPL)).EndInit();
            this.grpThongTinNPL.ResumeLayout(false);
            this.grpThongTinNPL.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox grpThongTinNPL;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX dgList2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSanPham;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenSanPham;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
      
        private Janus.Windows.GridEX.EditControls.EditBox txtSoChungTu;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongNPL;
        private Janus.Windows.EditControls.UIButton btnLuu;
        private System.Windows.Forms.Label label8;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar ucCalendar1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
      
        private System.Windows.Forms.Label label10;
        private Janus.Windows.EditControls.UIButton btnSave_BK;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayChungTu;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongSanPham;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaTT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThueXNK;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIComboBox cbbNhomLyDo;
        private Janus.Windows.GridEX.EditControls.EditBox txtGiaiTrinh;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.EditControls.UIButton btnXoa;
    }
}