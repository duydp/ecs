﻿IF(SELECT COUNT(*) FROM [dbo].[t_HaiQuan_LoaiHinhMauDich]) = 0
BEGIN
	INSERT  INTO [dbo].[t_HaiQuan_LoaiHinhMauDich]
			( [ID] ,
			  [Ten] ,
			  [Ten_VT]
			)
	VALUES  ( 'NGC99' ,
			  N'Nhập GC Tái nhập - Tái chế' ,
			  'NGC-TNTC'
			)
END        

GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='3.9', Date = getdate(), Notes = N'Cập nhật loại hình mậu dịch mới'
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('3.9', getdate(), N'Cập nhật loại hình mậu dịch mới')
	end 