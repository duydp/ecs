﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CreateUpdateFile
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmCreateUpdateFile());

            //try
            //{
            //    Company.KDT.SHARE.Components.DownloadUpdate dl = new Company.KDT.SHARE.Components.DownloadUpdate("");
            //    dl.DoDownload();
            //    //dl.ProcDownload(null);
            //}
            //catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
