using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.VNACCS
{
	public partial class DongBoDuLieu_TrackVNACCS : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string SoToKhai { set; get; }
		public DateTime NgayDangKy { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string ID_NPL { set; get; }
		public string ID_SanPham { set; get; }
		public string ID_DinhMuc { set; get; }
		public int IDUserDaiLy { set; get; }
		public DateTime NgayDongBo { set; get; }
		public int TrangThaiDongBoLen { set; get; }
		public int TrangThaiDongBoXuong { set; get; }
		public string UserName { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<DongBoDuLieu_TrackVNACCS> ConvertToCollection(IDataReader reader)
		{
			List<DongBoDuLieu_TrackVNACCS> collection = new List<DongBoDuLieu_TrackVNACCS>();
			while (reader.Read())
			{
				DongBoDuLieu_TrackVNACCS entity = new DongBoDuLieu_TrackVNACCS();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetString(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_NPL"))) entity.ID_NPL = reader.GetString(reader.GetOrdinal("ID_NPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_SanPham"))) entity.ID_SanPham = reader.GetString(reader.GetOrdinal("ID_SanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_DinhMuc"))) entity.ID_DinhMuc = reader.GetString(reader.GetOrdinal("ID_DinhMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDUserDaiLy"))) entity.IDUserDaiLy = reader.GetInt32(reader.GetOrdinal("IDUserDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDongBo"))) entity.NgayDongBo = reader.GetDateTime(reader.GetOrdinal("NgayDongBo"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiDongBoLen"))) entity.TrangThaiDongBoLen = reader.GetInt32(reader.GetOrdinal("TrangThaiDongBoLen"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiDongBoXuong"))) entity.TrangThaiDongBoXuong = reader.GetInt32(reader.GetOrdinal("TrangThaiDongBoXuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) entity.UserName = reader.GetString(reader.GetOrdinal("UserName"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<DongBoDuLieu_TrackVNACCS> collection, long id)
        {
            foreach (DongBoDuLieu_TrackVNACCS item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_DongBoDuLieu_TrackVNACCS VALUES(@SoToKhai, @NgayDangKy, @MaDoanhNghiep, @ID_NPL, @ID_SanPham, @ID_DinhMuc, @IDUserDaiLy, @NgayDongBo, @TrangThaiDongBoLen, @TrangThaiDongBoXuong, @UserName)";
            string update = "UPDATE t_DongBoDuLieu_TrackVNACCS SET SoToKhai = @SoToKhai, NgayDangKy = @NgayDangKy, MaDoanhNghiep = @MaDoanhNghiep, ID_NPL = @ID_NPL, ID_SanPham = @ID_SanPham, ID_DinhMuc = @ID_DinhMuc, IDUserDaiLy = @IDUserDaiLy, NgayDongBo = @NgayDongBo, TrangThaiDongBoLen = @TrangThaiDongBoLen, TrangThaiDongBoXuong = @TrangThaiDongBoXuong, UserName = @UserName WHERE ID = @ID";
            string delete = "DELETE FROM t_DongBoDuLieu_TrackVNACCS WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.VarChar, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_NPL", SqlDbType.VarChar, "ID_NPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_SanPham", SqlDbType.VarChar, "ID_SanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_DinhMuc", SqlDbType.VarChar, "ID_DinhMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IDUserDaiLy", SqlDbType.Int, "IDUserDaiLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDongBo", SqlDbType.DateTime, "NgayDongBo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiDongBoLen", SqlDbType.Int, "TrangThaiDongBoLen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiDongBoXuong", SqlDbType.Int, "TrangThaiDongBoXuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserName", SqlDbType.NVarChar, "UserName", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.VarChar, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_NPL", SqlDbType.VarChar, "ID_NPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_SanPham", SqlDbType.VarChar, "ID_SanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_DinhMuc", SqlDbType.VarChar, "ID_DinhMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IDUserDaiLy", SqlDbType.Int, "IDUserDaiLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDongBo", SqlDbType.DateTime, "NgayDongBo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiDongBoLen", SqlDbType.Int, "TrangThaiDongBoLen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiDongBoXuong", SqlDbType.Int, "TrangThaiDongBoXuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserName", SqlDbType.NVarChar, "UserName", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_DongBoDuLieu_TrackVNACCS VALUES(@SoToKhai, @NgayDangKy, @MaDoanhNghiep, @ID_NPL, @ID_SanPham, @ID_DinhMuc, @IDUserDaiLy, @NgayDongBo, @TrangThaiDongBoLen, @TrangThaiDongBoXuong, @UserName)";
            string update = "UPDATE t_DongBoDuLieu_TrackVNACCS SET SoToKhai = @SoToKhai, NgayDangKy = @NgayDangKy, MaDoanhNghiep = @MaDoanhNghiep, ID_NPL = @ID_NPL, ID_SanPham = @ID_SanPham, ID_DinhMuc = @ID_DinhMuc, IDUserDaiLy = @IDUserDaiLy, NgayDongBo = @NgayDongBo, TrangThaiDongBoLen = @TrangThaiDongBoLen, TrangThaiDongBoXuong = @TrangThaiDongBoXuong, UserName = @UserName WHERE ID = @ID";
            string delete = "DELETE FROM t_DongBoDuLieu_TrackVNACCS WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.VarChar, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_NPL", SqlDbType.VarChar, "ID_NPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_SanPham", SqlDbType.VarChar, "ID_SanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_DinhMuc", SqlDbType.VarChar, "ID_DinhMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IDUserDaiLy", SqlDbType.Int, "IDUserDaiLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDongBo", SqlDbType.DateTime, "NgayDongBo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiDongBoLen", SqlDbType.Int, "TrangThaiDongBoLen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiDongBoXuong", SqlDbType.Int, "TrangThaiDongBoXuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserName", SqlDbType.NVarChar, "UserName", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.VarChar, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_NPL", SqlDbType.VarChar, "ID_NPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_SanPham", SqlDbType.VarChar, "ID_SanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_DinhMuc", SqlDbType.VarChar, "ID_DinhMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IDUserDaiLy", SqlDbType.Int, "IDUserDaiLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDongBo", SqlDbType.DateTime, "NgayDongBo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiDongBoLen", SqlDbType.Int, "TrangThaiDongBoLen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiDongBoXuong", SqlDbType.Int, "TrangThaiDongBoXuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserName", SqlDbType.NVarChar, "UserName", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static DongBoDuLieu_TrackVNACCS Load(long id)
		{
			const string spName = "[dbo].[p_DongBoDuLieu_TrackVNACCS_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<DongBoDuLieu_TrackVNACCS> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<DongBoDuLieu_TrackVNACCS> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<DongBoDuLieu_TrackVNACCS> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_DongBoDuLieu_TrackVNACCS_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_DongBoDuLieu_TrackVNACCS_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_DongBoDuLieu_TrackVNACCS_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_DongBoDuLieu_TrackVNACCS_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertDongBoDuLieu_TrackVNACCS(string soToKhai, DateTime ngayDangKy, string maDoanhNghiep, string iD_NPL, string iD_SanPham, string iD_DinhMuc, int iDUserDaiLy, DateTime ngayDongBo, int trangThaiDongBoLen, int trangThaiDongBoXuong, string userName)
		{
			DongBoDuLieu_TrackVNACCS entity = new DongBoDuLieu_TrackVNACCS();	
			entity.SoToKhai = soToKhai;
			entity.NgayDangKy = ngayDangKy;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.ID_NPL = iD_NPL;
			entity.ID_SanPham = iD_SanPham;
			entity.ID_DinhMuc = iD_DinhMuc;
			entity.IDUserDaiLy = iDUserDaiLy;
			entity.NgayDongBo = ngayDongBo;
			entity.TrangThaiDongBoLen = trangThaiDongBoLen;
			entity.TrangThaiDongBoXuong = trangThaiDongBoXuong;
			entity.UserName = userName;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_DongBoDuLieu_TrackVNACCS_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.VarChar, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@ID_NPL", SqlDbType.VarChar, ID_NPL);
			db.AddInParameter(dbCommand, "@ID_SanPham", SqlDbType.VarChar, ID_SanPham);
			db.AddInParameter(dbCommand, "@ID_DinhMuc", SqlDbType.VarChar, ID_DinhMuc);
			db.AddInParameter(dbCommand, "@IDUserDaiLy", SqlDbType.Int, IDUserDaiLy);
			db.AddInParameter(dbCommand, "@NgayDongBo", SqlDbType.DateTime, NgayDongBo.Year <= 1753 ? DBNull.Value : (object) NgayDongBo);
			db.AddInParameter(dbCommand, "@TrangThaiDongBoLen", SqlDbType.Int, TrangThaiDongBoLen);
			db.AddInParameter(dbCommand, "@TrangThaiDongBoXuong", SqlDbType.Int, TrangThaiDongBoXuong);
			db.AddInParameter(dbCommand, "@UserName", SqlDbType.NVarChar, UserName);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<DongBoDuLieu_TrackVNACCS> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DongBoDuLieu_TrackVNACCS item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateDongBoDuLieu_TrackVNACCS(long id, string soToKhai, DateTime ngayDangKy, string maDoanhNghiep, string iD_NPL, string iD_SanPham, string iD_DinhMuc, int iDUserDaiLy, DateTime ngayDongBo, int trangThaiDongBoLen, int trangThaiDongBoXuong, string userName)
		{
			DongBoDuLieu_TrackVNACCS entity = new DongBoDuLieu_TrackVNACCS();			
			entity.ID = id;
			entity.SoToKhai = soToKhai;
			entity.NgayDangKy = ngayDangKy;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.ID_NPL = iD_NPL;
			entity.ID_SanPham = iD_SanPham;
			entity.ID_DinhMuc = iD_DinhMuc;
			entity.IDUserDaiLy = iDUserDaiLy;
			entity.NgayDongBo = ngayDongBo;
			entity.TrangThaiDongBoLen = trangThaiDongBoLen;
			entity.TrangThaiDongBoXuong = trangThaiDongBoXuong;
			entity.UserName = userName;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_DongBoDuLieu_TrackVNACCS_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.VarChar, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@ID_NPL", SqlDbType.VarChar, ID_NPL);
			db.AddInParameter(dbCommand, "@ID_SanPham", SqlDbType.VarChar, ID_SanPham);
			db.AddInParameter(dbCommand, "@ID_DinhMuc", SqlDbType.VarChar, ID_DinhMuc);
			db.AddInParameter(dbCommand, "@IDUserDaiLy", SqlDbType.Int, IDUserDaiLy);
			db.AddInParameter(dbCommand, "@NgayDongBo", SqlDbType.DateTime, NgayDongBo.Year <= 1753 ? DBNull.Value : (object) NgayDongBo);
			db.AddInParameter(dbCommand, "@TrangThaiDongBoLen", SqlDbType.Int, TrangThaiDongBoLen);
			db.AddInParameter(dbCommand, "@TrangThaiDongBoXuong", SqlDbType.Int, TrangThaiDongBoXuong);
			db.AddInParameter(dbCommand, "@UserName", SqlDbType.NVarChar, UserName);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<DongBoDuLieu_TrackVNACCS> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DongBoDuLieu_TrackVNACCS item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateDongBoDuLieu_TrackVNACCS(long id, string soToKhai, DateTime ngayDangKy, string maDoanhNghiep, string iD_NPL, string iD_SanPham, string iD_DinhMuc, int iDUserDaiLy, DateTime ngayDongBo, int trangThaiDongBoLen, int trangThaiDongBoXuong, string userName)
		{
			DongBoDuLieu_TrackVNACCS entity = new DongBoDuLieu_TrackVNACCS();			
			entity.ID = id;
			entity.SoToKhai = soToKhai;
			entity.NgayDangKy = ngayDangKy;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.ID_NPL = iD_NPL;
			entity.ID_SanPham = iD_SanPham;
			entity.ID_DinhMuc = iD_DinhMuc;
			entity.IDUserDaiLy = iDUserDaiLy;
			entity.NgayDongBo = ngayDongBo;
			entity.TrangThaiDongBoLen = trangThaiDongBoLen;
			entity.TrangThaiDongBoXuong = trangThaiDongBoXuong;
			entity.UserName = userName;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_DongBoDuLieu_TrackVNACCS_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.VarChar, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@ID_NPL", SqlDbType.VarChar, ID_NPL);
			db.AddInParameter(dbCommand, "@ID_SanPham", SqlDbType.VarChar, ID_SanPham);
			db.AddInParameter(dbCommand, "@ID_DinhMuc", SqlDbType.VarChar, ID_DinhMuc);
			db.AddInParameter(dbCommand, "@IDUserDaiLy", SqlDbType.Int, IDUserDaiLy);
			db.AddInParameter(dbCommand, "@NgayDongBo", SqlDbType.DateTime, NgayDongBo.Year <= 1753 ? DBNull.Value : (object) NgayDongBo);
			db.AddInParameter(dbCommand, "@TrangThaiDongBoLen", SqlDbType.Int, TrangThaiDongBoLen);
			db.AddInParameter(dbCommand, "@TrangThaiDongBoXuong", SqlDbType.Int, TrangThaiDongBoXuong);
			db.AddInParameter(dbCommand, "@UserName", SqlDbType.NVarChar, UserName);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<DongBoDuLieu_TrackVNACCS> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DongBoDuLieu_TrackVNACCS item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteDongBoDuLieu_TrackVNACCS(long id)
		{
			DongBoDuLieu_TrackVNACCS entity = new DongBoDuLieu_TrackVNACCS();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_DongBoDuLieu_TrackVNACCS_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_DongBoDuLieu_TrackVNACCS_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<DongBoDuLieu_TrackVNACCS> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DongBoDuLieu_TrackVNACCS item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}