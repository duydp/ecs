﻿namespace Company.Interface.KDT.DNCX
{
    partial class ThanhLyTSCD_ThemHangFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaHQTK = new Company.Interface.Controls.DonViHaiQuanControl();
            this.txtNgayDangKyTK = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtMaLoaiHinhTK = new Company.Interface.Controls.LoaiHinhMauDichHControl();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.cmbDVT = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoLuongThanhLy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiButton1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(686, 235);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtMaHQTK);
            this.uiGroupBox1.Controls.Add(this.txtNgayDangKyTK);
            this.uiGroupBox1.Controls.Add(this.txtMaLoaiHinhTK);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(686, 106);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Tờ khai nhập khẩu";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHQTK
            // 
            this.txtMaHQTK.BackColor = System.Drawing.Color.Transparent;
            this.txtMaHQTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHQTK.Location = new System.Drawing.Point(319, 59);
            this.txtMaHQTK.Ma = "";
            this.txtMaHQTK.MaCuc = "";
            this.txtMaHQTK.Name = "txtMaHQTK";
            this.txtMaHQTK.ReadOnly = false;
            this.txtMaHQTK.Size = new System.Drawing.Size(273, 22);
            this.txtMaHQTK.TabIndex = 4;
            this.txtMaHQTK.VisualStyleManager = null;
            // 
            // txtNgayDangKyTK
            // 
            // 
            // 
            // 
            this.txtNgayDangKyTK.DropDownCalendar.Name = "";
            this.txtNgayDangKyTK.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.txtNgayDangKyTK.Location = new System.Drawing.Point(84, 60);
            this.txtNgayDangKyTK.Name = "txtNgayDangKyTK";
            this.txtNgayDangKyTK.Nullable = true;
            this.txtNgayDangKyTK.Size = new System.Drawing.Size(130, 21);
            this.txtNgayDangKyTK.TabIndex = 3;
            this.txtNgayDangKyTK.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.txtNgayDangKyTK.VisualStyleManager = this.vsmMain;
            // 
            // txtMaLoaiHinhTK
            // 
            this.txtMaLoaiHinhTK.BackColor = System.Drawing.Color.Transparent;
            this.txtMaLoaiHinhTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaLoaiHinhTK.Location = new System.Drawing.Point(319, 29);
            this.txtMaLoaiHinhTK.Ma = "";
            this.txtMaLoaiHinhTK.Name = "txtMaLoaiHinhTK";
            this.txtMaLoaiHinhTK.Nhom = "";
            this.txtMaLoaiHinhTK.ReadOnly = false;
            this.txtMaLoaiHinhTK.Size = new System.Drawing.Size(273, 22);
            this.txtMaLoaiHinhTK.TabIndex = 2;
            this.txtMaLoaiHinhTK.VisualStyleManager = null;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Số tờ khai";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(233, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Mã hải quan TK";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(235, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Mã loại hình TK";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Số tờ khai";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoToKhai.Location = new System.Drawing.Point(84, 30);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(130, 21);
            this.txtSoToKhai.TabIndex = 0;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cmbDVT);
            this.uiGroupBox2.Controls.Add(this.txtSoLuongThanhLy);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtTenHangHoa);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.txtMaHangHoa);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 112);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(686, 90);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Hàng hóa thanh lý";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            this.uiGroupBox2.Click += new System.EventHandler(this.uiGroupBox2_Click);
            // 
            // cmbDVT
            // 
            this.cmbDVT.Location = new System.Drawing.Point(319, 53);
            this.cmbDVT.Name = "cmbDVT";
            this.cmbDVT.Size = new System.Drawing.Size(103, 21);
            this.cmbDVT.TabIndex = 5;
            this.cmbDVT.VisualStyleManager = this.vsmMain;
            // 
            // txtSoLuongThanhLy
            // 
            this.txtSoLuongThanhLy.DecimalDigits = 4;
            this.txtSoLuongThanhLy.Location = new System.Drawing.Point(84, 54);
            this.txtSoLuongThanhLy.Name = "txtSoLuongThanhLy";
            this.txtSoLuongThanhLy.Size = new System.Drawing.Size(130, 21);
            this.txtSoLuongThanhLy.TabIndex = 4;
            this.txtSoLuongThanhLy.Text = "0.0000";
            this.txtSoLuongThanhLy.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtSoLuongThanhLy.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Số lượng  TL";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(235, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "ĐVT";
            // 
            // txtTenHangHoa
            // 
            this.txtTenHangHoa.Location = new System.Drawing.Point(319, 26);
            this.txtTenHangHoa.Name = "txtTenHangHoa";
            this.txtTenHangHoa.Size = new System.Drawing.Size(329, 21);
            this.txtTenHangHoa.TabIndex = 2;
            this.txtTenHangHoa.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(233, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Tên hàng hóa";
            // 
            // txtMaHangHoa
            // 
            this.txtMaHangHoa.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaHangHoa.Location = new System.Drawing.Point(84, 26);
            this.txtMaHangHoa.Name = "txtMaHangHoa";
            this.txtMaHangHoa.Size = new System.Drawing.Size(130, 21);
            this.txtMaHangHoa.TabIndex = 2;
            this.txtMaHangHoa.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Mã hàng hóa";
            // 
            // uiButton1
            // 
            this.uiButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton1.Location = new System.Drawing.Point(599, 208);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(75, 23);
            this.uiButton1.TabIndex = 5;
            this.uiButton1.Text = "Ghi";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // ThanhLyTSCD_ThemHangFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 235);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ThanhLyTSCD_ThemHangFrm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thêm hàng cần thanh lý";
            this.Load += new System.EventHandler(this.ThanhLyTSCD_ThemHangFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private Company.Interface.Controls.LoaiHinhMauDichHControl txtMaLoaiHinhTK;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.CalendarCombo.CalendarCombo txtNgayDangKyTK;
        private Company.Interface.Controls.DonViHaiQuanControl txtMaHQTK;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangHoa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangHoa;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongThanhLy;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIComboBox cmbDVT;
    }
}