using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using DevExpress.XtraCharts.Native;
using System.IO;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using System.Collections.Generic;

namespace Company.Interface.Report.GC.ThongTu38
{
    public partial class BaoCaoQuyetToan_Mau15_TT38_Total : DevExpress.XtraReports.UI.XtraReport
    {
       public int NAMQUYETTOAN;
       public String MaNPL;
       public String MaSP;
       public String TenSP;
       public decimal LUONGTONDK = 0;
       public decimal TRIGIATONDK = 0;
       public decimal LUONGNHAPTK = 0;
       public decimal TRIGIANHAPTK = 0;
       public decimal LUONGXUATTK = 0;
       public decimal TRIGIAXUATTK = 0;
       public decimal LUONGTONCK = 0;
       public decimal TRIGIATONCK = 0;
       public bool isNumber = true;
       public DataTable dtNPLTotal = new DataTable();
       public DataTable dtSPTotal = new DataTable();
       public KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodItem;
        public BaoCaoQuyetToan_Mau15_TT38_Total()
        {
            InitializeComponent();
        }

        public void BinReportBCQTKho(bool isNumber ,List<T_KHOKETOAN_BCQT> NPLBCQT ,List<T_KHOKETOAN_BCQT> SPBCQT)
        {
            lbNam.Text = NAMQUYETTOAN.ToString();
            lblTonDauKy_T.Text = "Tồn đầu kỳ (01/01/" + NAMQUYETTOAN + ")";
            lbTenToChuc.Text = GlobalSettings.TEN_DON_VI;
            lbMaSo.Text = GlobalSettings.MA_DON_VI;
            lbDiaChi.Text = GlobalSettings.DIA_CHI;
            DetailReport.DataSource = NPLBCQT;
            DetailReport1.DataSource = SPBCQT;
            if (isNumber)
            {
                lblT_SoTien.Text = "Số lượng";
                txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
                txtTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TENHANGHOA");
                txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MAHANGHOA");
                txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LUONGTONDAU", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LUONGNHAP", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LUONGXUAT", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "LUONGTONCUOI", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource, "GHICHU");

                txtST.DataBindings.Add("Text", DetailReport1.DataSource, "STT");
                txtTenSP.DataBindings.Add("Text", DetailReport1.DataSource, "TENHANGHOA");
                txtMaSP.DataBindings.Add("Text", DetailReport1.DataSource, "MAHANGHOA");
                txtTonDK.DataBindings.Add("Text", DetailReport1.DataSource, "LUONGTONDAU", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtNhapTK.DataBindings.Add("Text", DetailReport1.DataSource, "LUONGNHAP", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtXuatTK.DataBindings.Add("Text", DetailReport1.DataSource, "LUONGXUAT", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtTonCK.DataBindings.Add("Text", DetailReport1.DataSource, "LUONGTONCUOI", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            }
            else
            {
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    lblT_SoTien.Text = "Số tiền (VNĐ)";
                }
                else
                {
                    lblT_SoTien.Text = "Số tiền (USD)";
                }
                txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
                txtTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TENHANGHOA");
                txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MAHANGHOA");
                txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIATONDAU", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIANHAP", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIAXUAT", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIATONCUOI", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));

                txtST.DataBindings.Add("Text", DetailReport1.DataSource, "STT");
                txtTenSP.DataBindings.Add("Text", DetailReport1.DataSource, "TENHANGHOA");
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    txtMaSP.DataBindings.Add("Text", DetailReport1.DataSource, "MAHANGHOA");
                }
                txtTonDK.DataBindings.Add("Text", DetailReport1.DataSource, "TRIGIATONDAU", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtNhapTK.DataBindings.Add("Text", DetailReport1.DataSource, "TRIGIANHAP", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtXuatTK.DataBindings.Add("Text", DetailReport1.DataSource, "TRIGIAXUAT", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtTonCK.DataBindings.Add("Text", DetailReport1.DataSource, "TRIGIATONCUOI", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            }
        }
    }
}
