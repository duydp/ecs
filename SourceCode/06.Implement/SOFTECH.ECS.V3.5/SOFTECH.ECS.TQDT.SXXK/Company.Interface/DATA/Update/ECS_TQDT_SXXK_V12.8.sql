/*
Run this script on:

192.168.72.100\ecsexpress.ECS_TQDT_KD_VNACCS_Backup    -  This database will be modified

to synchronize it with:

192.168.72.100\ecsexpress.ECS_TQDT_KD_VNACCS

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 8.1.0 from Red Gate Software Ltd at 11/22/2013 8:32:23 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16),
		@IDToKhaiMD BIGINT,
		@IDHangMauDich BIGINT
IF(NOT EXISTS(SELECT * FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich] WHERE [MaDonVi] = '0400392263'))
BEGIN
	
-- Add 1 row to [dbo].[t_KDT_VNACC_ToKhaiMauDich]
--SET IDENTITY_INSERT [dbo].[t_KDT_VNACC_ToKhaiMauDich] ON
INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich] ([SoToKhai], [SoToKhaiDauTien], [SoNhanhToKhai], [TongSoTKChiaNho], [SoToKhaiTNTX], [MaLoaiHinh], [MaPhanLoaiHH], [MaPhuongThucVT], [PhanLoaiToChuc], [CoQuanHaiQuan], [NhomXuLyHS], [ThoiHanTaiNhapTaiXuat], [NgayDangKy], [MaDonVi], [TenDonVi], [MaBuuChinhDonVi], [DiaChiDonVi], [SoDienThoaiDonVi], [MaUyThac], [TenUyThac], [MaDoiTac], [TenDoiTac], [MaBuuChinhDoiTac], [DiaChiDoiTac1], [DiaChiDoiTac2], [DiaChiDoiTac3], [DiaChiDoiTac4], [MaNuocDoiTac], [NguoiUyThacXK], [MaDaiLyHQ], [SoLuong], [MaDVTSoLuong], [TrongLuong], [MaDVTTrongLuong], [MaDDLuuKho], [SoHieuKyHieu], [MaPTVC], [TenPTVC], [NgayHangDen], [MaDiaDiemDoHang], [TenDiaDiemDohang], [MaDiaDiemXepHang], [TenDiaDiemXepHang], [SoLuongCont], [MaKetQuaKiemTra], [PhanLoaiHD], [SoTiepNhanHD], [SoHoaDon], [NgayPhatHanhHD], [PhuongThucTT], [PhanLoaiGiaHD], [MaDieuKienGiaHD], [MaTTHoaDon], [TongTriGiaHD], [MaPhanLoaiTriGia], [SoTiepNhanTKTriGia], [MaTTHieuChinhTriGia], [GiaHieuChinhTriGia], [MaPhanLoaiPhiVC], [MaTTPhiVC], [PhiVanChuyen], [MaPhanLoaiPhiBH], [MaTTPhiBH], [PhiBaoHiem], [SoDangKyBH], [ChiTietKhaiTriGia], [TriGiaTinhThue], [PhanLoaiKhongQDVND], [MaTTTriGiaTinhThue], [TongHeSoPhanBoTG], [MaLyDoDeNghiBP], [NguoiNopThue], [MaNHTraThueThay], [NamPhatHanhHM], [KyHieuCTHanMuc], [SoCTHanMuc], [MaXDThoiHanNopThue], [MaNHBaoLanh], [NamPhatHanhBL], [KyHieuCTBaoLanh], [SoCTBaoLanh], [NgayNhapKhoDau], [NgayKhoiHanhVC], [DiaDiemDichVC], [NgayDen], [GhiChu], [SoQuanLyNoiBoDN], [TrangThaiXuLy], [InputMessageID], [MessageTag], [IndexTag], [PhanLoaiBaoCaoSuaDoi], [MaPhanLoaiKiemTra], [MaSoThueDaiDien], [TenCoQuanHaiQuan], [NgayThayDoiDangKy], [BieuThiTruongHopHetHan], [TenDaiLyHaiQuan], [MaNhanVienHaiQuan], [TenDDLuuKho], [MaPhanLoaiTongGiaCoBan], [PhanLoaiCongThucChuan], [MaPhanLoaiDieuChinhTriGia], [PhuongPhapDieuChinhTriGia], [TongTienThuePhaiNop], [SoTienBaoLanh], [TenTruongDonViHaiQuan], [NgayCapPhep], [PhanLoaiThamTraSauThongQuan], [NgayPheDuyetBP], [NgayHoanThanhKiemTraBP], [SoNgayDoiCapPhepNhapKhau], [TieuDe], [MaSacThueAnHan_VAT], [TenSacThueAnHan_VAT], [HanNopThueSauKhiAnHan_VAT], [PhanLoaiNopThue], [TongSoTienThueXuatKhau], [MaTTTongTienThueXuatKhau], [TongSoTienLePhi], [MaTTCuaSoTienBaoLanh], [SoQuanLyNguoiSuDung], [NgayHoanThanhKiemTra], [TongSoTrangCuaToKhai], [TongSoDongHangCuaToKhai], [NgayKhaiBaoNopThue], [MaVanbanPhapQuy1], [MaVanbanPhapQuy2], [MaVanbanPhapQuy3], [MaVanbanPhapQuy4], [MaVanbanPhapQuy5]) VALUES (0, 0, 0, 0, 0, 'A11', 'A', '2', '1', '33PD', '02', '1900-01-01 00:00:00.000', '1900-01-01 00:00:00.000', '0400392263', N'Công Ty Cổ Phần Softech', '(+84)43', N'Dia chi - SOFTECH', '0437545666', '', N'', '', N'CONG TY TNHH FOJI MOLD VIET NAM', '0313', N'LO F - 8A', N'KCN NOMURA', N'AN DUONG', N'HAI PHONG', 'JP', N'', '', 2, 'PA', 3.0000, 'KGM', '01NVW02', 'A1509', '9999', N'AAA', '2013-09-10 00:00:00.000', 'BNG', N'CANG BEN NGHE (HCM)', 'BRSNZ', N'SANTA CRUZ', 2, 'A', 'A', 0, '56585', '2013-09-08 00:00:00.000', 'TTR', 'A', 'CIF', 'USD', 25000.0000, '', 0, '', 0.0000, '', '', 0.0000, '', '', 0.0000, '', N'', 63108000.0000, NULL, NULL, 0.0000, '', '1', '', 0, '', '', 'D', '', 0, '', '', '2013-09-09 00:00:00.000', '1900-01-01 00:00:00.000', '', '1900-01-01 00:00:00.000', N'HANG HOA NHAP KHAU KINH DOANH ', '000009092013', '0', '', '', '', '', '', '9101', 'THUYANTTH', '1900-01-01 00:00:00.000', '', '', '', 'NIPPON EXPRESS VNHN1', '', '', '', '', 18932400.0000, 0.0000, N'Chi c?c tru?ng', '2013-10-21 18:28:21.000', '', '1900-01-01 00:00:00.000', '1900-01-01 00:00:00.000', 0, '', NULL, NULL, NULL, 'A', 0.0000, NULL, 0.0000, NULL, '', '2013-10-21 18:28:21.000', 3, 1, '1900-01-01 00:00:00.000', '', '', '', '', '')
SELECT @IDToKhaiMD = MAX(ID) FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich]
--SET IDENTITY_INSERT [dbo].[t_KDT_VNACC_ToKhaiMauDich] OFF

-- Add 1 row to [dbo].[t_KDT_VNACC_HangMauDich]
--SET IDENTITY_INSERT [dbo].[t_KDT_VNACC_HangMauDich] ON
INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich] ([TKMD_ID], [MaSoHang], [MaQuanLy], [TenHang], [ThueSuat], [ThueSuatTuyetDoi], [MaDVTTuyetDoi], [MaTTTuyetDoi], [NuocXuatXu], [SoLuong1], [DVTLuong1], [SoLuong2], [DVTLuong2], [TriGiaHoaDon], [DonGiaHoaDon], [MaTTDonGia], [DVTDonGia], [MaBieuThueNK], [MaHanNgach], [MaThueNKTheoLuong], [MaMienGiamThue], [SoTienGiamThue], [TriGiaTinhThue], [MaTTTriGiaTinhThue], [SoMucKhaiKhoanDC], [SoTTDongHangTKTNTX], [SoDMMienThue], [SoDongDMMienThue], [MaMienGiam], [SoTienMienGiam], [MaTTSoTienMienGiam], [MaVanBanPhapQuyKhac1], [MaVanBanPhapQuyKhac2], [MaVanBanPhapQuyKhac3], [MaVanBanPhapQuyKhac4], [MaVanBanPhapQuyKhac5], [SoDong], [MaPhanLoaiTaiXacNhanGia], [TenNoiXuatXu], [SoLuongTinhThue], [MaDVTDanhThue], [DonGiaTinhThue], [DV_SL_TrongDonGiaTinhThue], [MaTTDonGiaTinhThue], [TriGiaTinhThueS], [MaTTTriGiaTinhThueS], [MaTTSoTienMienGiam1], [MaPhanLoaiThueSuatThue], [ThueSuatThue], [PhanLoaiThueSuatThue], [SoTienThue], [MaTTSoTienThueXuatKhau], [TienLePhi_DonGia], [TienBaoHiem_DonGia], [TienLePhi_SoLuong], [TienLePhi_MaDVSoLuong], [TienBaoHiem_SoLuong], [TienBaoHiem_MaDVSoLuong], [TienLePhi_KhoanTien], [TienBaoHiem_KhoanTien], [DieuKhoanMienGiam]) VALUES (@IDToKhaiMD, '91012900', 'NPL2', N'TEN Hang', 30, 0.0000, '', '', 'VN', 1500, 'PCE', 12, 'PCE', 3000.0000, 2.0000, 'USD', 'PCE', 'B03', '', '', '', 0.0000, 63108000.0000, 'VND', '00000', '', 0, '', '', 0.0000, NULL, NULL, NULL, NULL, NULL, NULL, '01', '', 'VIETNAM', 0.0000, '', 42072.0000, 'PCE', NULL, 63108000.0000, NULL, NULL, 'G', '30%', 'M', 18932400.0000, NULL, NULL, NULL, 0.0000, NULL, 0.0000, NULL, 0.0000, 0.0000, '')
SELECT @IDHangMauDich = MAX(ID) FROM [dbo].[t_KDT_VNACC_HangMauDich]
--SET IDENTITY_INSERT [dbo].[t_KDT_VNACC_HangMauDich] OFF

-- Add 1 row to [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]
--SET IDENTITY_INSERT [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac] ON
INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac] ([Master_id], [MaTSThueThuKhac], [MaMGThueThuKhac], [SoTienGiamThueThuKhac], [TenKhoanMucThueVaThuKhac], [TriGiaTinhThueVaThuKhac], [SoLuongTinhThueVaThuKhac], [MaDVTDanhThueVaThuKhac], [ThueSuatThueVaThuKhac], [SoTienThueVaThuKhac], [DieuKhoanMienGiamThueVaThuKhac]) VALUES (@IDHangMauDich, 'VB015', 'VK120', 0.0000, N'', 82040400.0000, 0, '', '', 0.0000, 'NGUYEN LIEU NK DE SAN XUAT, GIA CONG HANG XK')

--SET IDENTITY_INSERT [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac] OFF

-- Add 1 row to [dbo].[t_KDT_VNACC_TK_SoVanDon]
--SET IDENTITY_INSERT [dbo].[t_KDT_VNACC_TK_SoVanDon] ON
INSERT INTO [dbo].[t_KDT_VNACC_TK_SoVanDon] ([TKMD_ID], [SoTT], [SoVanDon], [InputMessageID], [MessageTag], [IndexTag]) VALUES (@IDToKhaiMD, 1, '0001BL', NULL, NULL, NULL)
--SET IDENTITY_INSERT [dbo].[t_KDT_VNACC_TK_SoVanDon] OFF
END

COMMIT TRANSACTION
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '12.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('12.8', GETDATE(), N'cap nhat to khai mau')
END	
