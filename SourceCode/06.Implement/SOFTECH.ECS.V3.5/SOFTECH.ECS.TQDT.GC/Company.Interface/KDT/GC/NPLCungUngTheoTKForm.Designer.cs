﻿namespace Company.Interface.KDT.GC
{
    partial class NPLCungUngTheoTKForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NPLCungUngTheoTKForm));
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cmMainNPLCungUng = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar2 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdAddSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddSP");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdXacNhap = new Janus.Windows.UI.CommandBars.UICommand("cmdXacNhap");
            this.cmdAddSP = new Janus.Windows.UI.CommandBars.UICommand("cmdAddSP");
            this.cmdNhanDuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanDuLieu");
            this.cmdHuy = new Janus.Windows.UI.CommandBars.UICommand("cmdHuy");
            this.InPhieuTN = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdPhanBo = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanBo");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMainNPLCungUng)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.label8);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(776, 349);
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(3, 17);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(770, 329);
            this.dgList.TabIndex = 3;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.dgList_DeletingRecord);
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "printer.png");
            // 
            // cmMainNPLCungUng
            // 
            this.cmMainNPLCungUng.BottomRebar = this.BottomRebar1;
            this.cmMainNPLCungUng.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar2});
            this.cmMainNPLCungUng.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave,
            this.cmdKhaiBao,
            this.cmdXacNhap,
            this.cmdAddSP,
            this.cmdNhanDuLieu,
            this.cmdHuy,
            this.InPhieuTN,
            this.cmdPhanBo});
            this.cmMainNPLCungUng.ContainerControl = this;
            this.cmMainNPLCungUng.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMainNPLCungUng.ImageList = this.ImageList1;
            this.cmMainNPLCungUng.LeftRebar = this.LeftRebar1;
            this.cmMainNPLCungUng.RightRebar = this.RightRebar1;
            this.cmMainNPLCungUng.ShowShortcutInToolTips = true;
            this.cmMainNPLCungUng.Tag = null;
            this.cmMainNPLCungUng.TopRebar = this.TopRebar1;
            this.cmMainNPLCungUng.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMainNPLCungUng.VisualStyleManager = this.vsmMain;
            this.cmMainNPLCungUng.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMainNPLCungUng_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMainNPLCungUng;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar2
            // 
            this.uiCommandBar2.CommandManager = this.cmMainNPLCungUng;
            this.uiCommandBar2.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAddSP1,
            this.cmdSave1});
            this.uiCommandBar2.FullRow = true;
            this.uiCommandBar2.Key = "CommandBar1";
            this.uiCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar2.Name = "uiCommandBar2";
            this.uiCommandBar2.RowIndex = 0;
            this.uiCommandBar2.Size = new System.Drawing.Size(776, 32);
            this.uiCommandBar2.Text = "CommandBar1";
            // 
            // cmdAddSP1
            // 
            this.cmdAddSP1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAddSP1.Icon")));
            this.cmdAddSP1.Key = "cmdAddSP";
            this.cmdAddSP1.Name = "cmdAddSP1";
            this.cmdAddSP1.Shortcut = System.Windows.Forms.Shortcut.CtrlM;
            this.cmdAddSP1.Text = "&Thêm mới SP";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSave1.Icon")));
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            this.cmdSave1.Shortcut = System.Windows.Forms.Shortcut.CtrlL;
            this.cmdSave1.Text = "&Lưu thông tin và xử lý phân bổ";
            // 
            // cmdSave
            // 
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdXacNhap
            // 
            this.cmdXacNhap.Key = "cmdXacNhap";
            this.cmdXacNhap.Name = "cmdXacNhap";
            this.cmdXacNhap.Text = "Xác nhận";
            // 
            // cmdAddSP
            // 
            this.cmdAddSP.Key = "cmdAddSP";
            this.cmdAddSP.Name = "cmdAddSP";
            this.cmdAddSP.Text = "Thêm mới SP";
            // 
            // cmdNhanDuLieu
            // 
            this.cmdNhanDuLieu.Key = "cmdNhanDuLieu";
            this.cmdNhanDuLieu.Name = "cmdNhanDuLieu";
            this.cmdNhanDuLieu.Text = "Nhận dữ liệu";
            // 
            // cmdHuy
            // 
            this.cmdHuy.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdHuy.Icon")));
            this.cmdHuy.Key = "cmdHuy";
            this.cmdHuy.Name = "cmdHuy";
            this.cmdHuy.Text = "Hủy";
            // 
            // InPhieuTN
            // 
            this.InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            this.InPhieuTN.Key = "InPhieuTN";
            this.InPhieuTN.Name = "InPhieuTN";
            this.InPhieuTN.Text = "In phiếu tiếp nhận";
            // 
            // cmdPhanBo
            // 
            this.cmdPhanBo.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            this.cmdPhanBo.Key = "cmdPhanBo";
            this.cmdPhanBo.Name = "cmdPhanBo";
            this.cmdPhanBo.Text = "Xử lý phân bổ";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMainNPLCungUng;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMainNPLCungUng;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar2});
            this.TopRebar1.CommandManager = this.cmMainNPLCungUng;
            this.TopRebar1.Controls.Add(this.uiCommandBar2);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(776, 32);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = null;
            this.uiCommandBar1.Key = "";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.Size = new System.Drawing.Size(28, 26);
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(215, 14);
            this.label8.TabIndex = 2;
            this.label8.Text = "Danh sách sản phẩm tự cung ứng";
            // 
            // NPLCungUngTheoTKForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(776, 381);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "NPLCungUngTheoTKForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Phân bổ nguyên phụ liệu cung ứng";
            this.Load += new System.EventHandler(this.NPLCungUngTheoTKSendForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMainNPLCungUng)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX dgList;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMainNPLCungUng;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdXacNhap;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar2;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddSP;
        private Janus.Windows.UI.CommandBars.UICommand cmdNhanDuLieu;
        private Janus.Windows.UI.CommandBars.UICommand cmdHuy;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN;
        private Janus.Windows.UI.CommandBars.UICommand cmdPhanBo;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddSP1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
    }
}