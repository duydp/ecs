
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_t_KDT_ToKhaiMauDichBoSung_NgayKhaiBao]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[t_KDT_ToKhaiMauDichBoSung] DROP CONSTRAINT [DF_t_KDT_ToKhaiMauDichBoSung_NgayKhaiBao]
END

GO

/****** Object:  Table [dbo].[t_KDT_ToKhaiMauDichBoSung]    Script Date: 04/09/2013 16:59:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_ToKhaiMauDichBoSung]') AND type in (N'U'))
DROP TABLE [dbo].[t_KDT_ToKhaiMauDichBoSung]
GO


/****** Object:  Table [dbo].[t_KDT_ToKhaiMauDichBoSung]    Script Date: 04/09/2013 16:38:42 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_ToKhaiMauDichBoSung]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[t_KDT_ToKhaiMauDichBoSung](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[PhienBan] [int] NOT NULL,
	[ChuKySo] [bit] NOT NULL,
	[GhiChu] [nvarchar](250) NULL,
	[MaNguoiNhanHang] [varchar](50) NULL,
	[TenNguoiNhanHang] [nvarchar](255) NULL,
	[MaNguoiGiaoHang] [varchar](50) NULL,
	[TenNguoiGiaoHang] [nvarchar](255) NULL,
	[MaNguoiChiDinhGiaoHang] [varchar](50) NULL,
	[TenNguoiChiDinhGiaoHang] [nvarchar](255) NULL,
	[MaNguoiKhaiHQ] [varchar](50) NULL,
	[TenNguoiKhaiHQ] [nvarchar](255) NULL,
	[NoiDungUyQuyen] [nvarchar](255) NULL,
	[TongCacKhoanPhaiCong] [decimal](35, 18) NULL,
	[TongCacKhoanPhaiTru] [decimal](35, 18) NULL,
	[NgayKhaiBao] [datetime] NULL,
 CONSTRAINT [PK_t_KDT_ToKhaiMauDich_BoSung] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END

GO

ALTER TABLE [dbo].[t_KDT_ToKhaiMauDichBoSung] ADD  CONSTRAINT [DF_t_KDT_ToKhaiMauDichBoSung_NgayKhaiBao]  DEFAULT (((1900)-(1))-(1)) FOR [NgayKhaiBao]
GO



/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]    Script Date: 04/09/2013 16:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]    Script Date: 04/09/2013 16:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]    Script Date: 04/09/2013 16:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]    Script Date: 04/09/2013 16:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]    Script Date: 04/09/2013 16:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate_ByTKMD]    Script Date: 04/09/2013 16:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate_ByTKMD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate_ByTKMD]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]    Script Date: 04/09/2013 16:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]    Script Date: 04/09/2013 16:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]    Script Date: 04/09/2013 16:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]    Script Date: 04/09/2013 16:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]    Script Date: 04/09/2013 16:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]    Script Date: 04/09/2013 16:49:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]    Script Date: 04/09/2013 16:49:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[TKMD_ID] = @TKMD_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]    Script Date: 04/09/2013 16:49:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]    Script Date: 04/09/2013 16:49:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250),
	@MaNguoiNhanHang varchar(50),
	@TenNguoiNhanHang nvarchar(255),
	@MaNguoiGiaoHang varchar(50),
	@TenNguoiGiaoHang nvarchar(255),
	@MaNguoiChiDinhGiaoHang varchar(50),
	@TenNguoiChiDinhGiaoHang nvarchar(255),
	@MaNguoiKhaiHQ varchar(50),
	@TenNguoiKhaiHQ nvarchar(255),
	@NoiDungUyQuyen nvarchar(255),
	@TongCacKhoanPhaiCong decimal(35, 18),
	@TongCacKhoanPhaiTru decimal(35, 18),
	@NgayKhaiBao datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ToKhaiMauDichBoSung]
(
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[MaNguoiChiDinhGiaoHang],
	[TenNguoiChiDinhGiaoHang],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[NoiDungUyQuyen],
	[TongCacKhoanPhaiCong],
	[TongCacKhoanPhaiTru],
	[NgayKhaiBao]
)
VALUES 
(
	@TKMD_ID,
	@PhienBan,
	@ChuKySo,
	@GhiChu,
	@MaNguoiNhanHang,
	@TenNguoiNhanHang,
	@MaNguoiGiaoHang,
	@TenNguoiGiaoHang,
	@MaNguoiChiDinhGiaoHang,
	@TenNguoiChiDinhGiaoHang,
	@MaNguoiKhaiHQ,
	@TenNguoiKhaiHQ,
	@NoiDungUyQuyen,
	@TongCacKhoanPhaiCong,
	@TongCacKhoanPhaiTru,
	@NgayKhaiBao
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]    Script Date: 04/09/2013 16:49:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250),
	@MaNguoiNhanHang varchar(50),
	@TenNguoiNhanHang nvarchar(255),
	@MaNguoiGiaoHang varchar(50),
	@TenNguoiGiaoHang nvarchar(255),
	@MaNguoiChiDinhGiaoHang varchar(50),
	@TenNguoiChiDinhGiaoHang nvarchar(255),
	@MaNguoiKhaiHQ varchar(50),
	@TenNguoiKhaiHQ nvarchar(255),
	@NoiDungUyQuyen nvarchar(255),
	@TongCacKhoanPhaiCong decimal(35, 18),
	@TongCacKhoanPhaiTru decimal(35, 18),
	@NgayKhaiBao datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiMauDichBoSung] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[PhienBan] = @PhienBan,
			[ChuKySo] = @ChuKySo,
			[GhiChu] = @GhiChu,
			[MaNguoiNhanHang] = @MaNguoiNhanHang,
			[TenNguoiNhanHang] = @TenNguoiNhanHang,
			[MaNguoiGiaoHang] = @MaNguoiGiaoHang,
			[TenNguoiGiaoHang] = @TenNguoiGiaoHang,
			[MaNguoiChiDinhGiaoHang] = @MaNguoiChiDinhGiaoHang,
			[TenNguoiChiDinhGiaoHang] = @TenNguoiChiDinhGiaoHang,
			[MaNguoiKhaiHQ] = @MaNguoiKhaiHQ,
			[TenNguoiKhaiHQ] = @TenNguoiKhaiHQ,
			[NoiDungUyQuyen] = @NoiDungUyQuyen,
			[TongCacKhoanPhaiCong] = @TongCacKhoanPhaiCong,
			[TongCacKhoanPhaiTru] = @TongCacKhoanPhaiTru,
			[NgayKhaiBao] = @NgayKhaiBao
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiMauDichBoSung]
		(
			[TKMD_ID],
			[PhienBan],
			[ChuKySo],
			[GhiChu],
			[MaNguoiNhanHang],
			[TenNguoiNhanHang],
			[MaNguoiGiaoHang],
			[TenNguoiGiaoHang],
			[MaNguoiChiDinhGiaoHang],
			[TenNguoiChiDinhGiaoHang],
			[MaNguoiKhaiHQ],
			[TenNguoiKhaiHQ],
			[NoiDungUyQuyen],
			[TongCacKhoanPhaiCong],
			[TongCacKhoanPhaiTru],
			[NgayKhaiBao]
		)
		VALUES 
		(
			@TKMD_ID,
			@PhienBan,
			@ChuKySo,
			@GhiChu,
			@MaNguoiNhanHang,
			@TenNguoiNhanHang,
			@MaNguoiGiaoHang,
			@TenNguoiGiaoHang,
			@MaNguoiChiDinhGiaoHang,
			@TenNguoiChiDinhGiaoHang,
			@MaNguoiKhaiHQ,
			@TenNguoiKhaiHQ,
			@NoiDungUyQuyen,
			@TongCacKhoanPhaiCong,
			@TongCacKhoanPhaiTru,
			@NgayKhaiBao
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate_ByTKMD]    Script Date: 04/09/2013 16:49:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--Bo sung them
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate_ByTKMD]
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] WHERE [TKMD_ID] = @TKMD_ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiMauDichBoSung] 
		SET
			[PhienBan] = @PhienBan,
			[ChuKySo] = @ChuKySo,
			[GhiChu] = @GhiChu
		WHERE
			[TKMD_ID] = @TKMD_ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiMauDichBoSung]
		(
			[TKMD_ID],
			[PhienBan],
			[ChuKySo],
			[GhiChu]
		)
		VALUES 
		(
			@TKMD_ID,
			@PhienBan,
			@ChuKySo,
			@GhiChu
		)		
	END



GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]    Script Date: 04/09/2013 16:49:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[MaNguoiChiDinhGiaoHang],
	[TenNguoiChiDinhGiaoHang],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[NoiDungUyQuyen],
	[TongCacKhoanPhaiCong],
	[TongCacKhoanPhaiTru],
	[NgayKhaiBao]
FROM
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]    Script Date: 04/09/2013 16:49:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[MaNguoiChiDinhGiaoHang],
	[TenNguoiChiDinhGiaoHang],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[NoiDungUyQuyen],
	[TongCacKhoanPhaiCong],
	[TongCacKhoanPhaiTru],
	[NgayKhaiBao]
FROM
	[dbo].[t_KDT_ToKhaiMauDichBoSung]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]    Script Date: 04/09/2013 16:49:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[MaNguoiChiDinhGiaoHang],
	[TenNguoiChiDinhGiaoHang],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[NoiDungUyQuyen],
	[TongCacKhoanPhaiCong],
	[TongCacKhoanPhaiTru],
	[NgayKhaiBao]
FROM
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[TKMD_ID] = @TKMD_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]    Script Date: 04/09/2013 16:49:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[MaNguoiChiDinhGiaoHang],
	[TenNguoiChiDinhGiaoHang],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[NoiDungUyQuyen],
	[TongCacKhoanPhaiCong],
	[TongCacKhoanPhaiTru],
	[NgayKhaiBao]
FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]    Script Date: 04/09/2013 16:49:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 09, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250),
	@MaNguoiNhanHang varchar(50),
	@TenNguoiNhanHang nvarchar(255),
	@MaNguoiGiaoHang varchar(50),
	@TenNguoiGiaoHang nvarchar(255),
	@MaNguoiChiDinhGiaoHang varchar(50),
	@TenNguoiChiDinhGiaoHang nvarchar(255),
	@MaNguoiKhaiHQ varchar(50),
	@TenNguoiKhaiHQ nvarchar(255),
	@NoiDungUyQuyen nvarchar(255),
	@TongCacKhoanPhaiCong decimal(35, 18),
	@TongCacKhoanPhaiTru decimal(35, 18),
	@NgayKhaiBao datetime
AS

UPDATE
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
SET
	[TKMD_ID] = @TKMD_ID,
	[PhienBan] = @PhienBan,
	[ChuKySo] = @ChuKySo,
	[GhiChu] = @GhiChu,
	[MaNguoiNhanHang] = @MaNguoiNhanHang,
	[TenNguoiNhanHang] = @TenNguoiNhanHang,
	[MaNguoiGiaoHang] = @MaNguoiGiaoHang,
	[TenNguoiGiaoHang] = @TenNguoiGiaoHang,
	[MaNguoiChiDinhGiaoHang] = @MaNguoiChiDinhGiaoHang,
	[TenNguoiChiDinhGiaoHang] = @TenNguoiChiDinhGiaoHang,
	[MaNguoiKhaiHQ] = @MaNguoiKhaiHQ,
	[TenNguoiKhaiHQ] = @TenNguoiKhaiHQ,
	[NoiDungUyQuyen] = @NoiDungUyQuyen,
	[TongCacKhoanPhaiCong] = @TongCacKhoanPhaiCong,
	[TongCacKhoanPhaiTru] = @TongCacKhoanPhaiTru,
	[NgayKhaiBao] = @NgayKhaiBao
WHERE
	[ID] = @ID


GO




-------------------------------------------------
--	GIAY PHEP
-------------------------------------------------
/*
Run this script on:

        192.168.72.100.ECS_TQDT_KD_V4_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_KD_V4

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 03/18/2013 10:54:25 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO

/****** Object:  Table [dbo].[t_KDT_HangVanDonDetail]    Script Date: 04/09/2013 10:52:08 ******/
IF (SELECT COUNT(*) --column_name 'Column Name', data_type 'Data Type', CHARacter_maximum_length 'Maximum Length' 
	FROM information_schema.columns
	WHERE table_name = 't_KDT_GiayPhep' AND COLUMN_NAME = 'LoaiGiayPhep') = 0
BEGIN

ALTER TABLE [dbo].[t_KDT_GiayPhep] ADD
[LoaiGiayPhep] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HinhThucTruLui] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL

END

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_Insert]
	@SoGiayPhep varchar(100),
	@NgayGiayPhep datetime,
	@NgayHetHan datetime,
	@NguoiCap nvarchar(100),
	@NoiCap nvarchar(100),
	@MaDonViDuocCap varchar(50),
	@TenDonViDuocCap nvarchar(100),
	@MaCoQuanCap varchar(50),
	@TenQuanCap nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@LoaiGiayPhep varchar(10),
	@HinhThucTruLui nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GiayPhep]
(
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
)
VALUES 
(
	@SoGiayPhep,
	@NgayGiayPhep,
	@NgayHetHan,
	@NguoiCap,
	@NoiCap,
	@MaDonViDuocCap,
	@TenDonViDuocCap,
	@MaCoQuanCap,
	@TenQuanCap,
	@ThongTinKhac,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GuidStr,
	@LoaiKB,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThai,
	@NamTiepNhan,
	@LoaiGiayPhep,
	@HinhThucTruLui
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_Update]
	@ID bigint,
	@SoGiayPhep varchar(100),
	@NgayGiayPhep datetime,
	@NgayHetHan datetime,
	@NguoiCap nvarchar(100),
	@NoiCap nvarchar(100),
	@MaDonViDuocCap varchar(50),
	@TenDonViDuocCap nvarchar(100),
	@MaCoQuanCap varchar(50),
	@TenQuanCap nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@LoaiGiayPhep varchar(10),
	@HinhThucTruLui nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_GiayPhep]
SET
	[SoGiayPhep] = @SoGiayPhep,
	[NgayGiayPhep] = @NgayGiayPhep,
	[NgayHetHan] = @NgayHetHan,
	[NguoiCap] = @NguoiCap,
	[NoiCap] = @NoiCap,
	[MaDonViDuocCap] = @MaDonViDuocCap,
	[TenDonViDuocCap] = @TenDonViDuocCap,
	[MaCoQuanCap] = @MaCoQuanCap,
	[TenQuanCap] = @TenQuanCap,
	[ThongTinKhac] = @ThongTinKhac,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GuidStr] = @GuidStr,
	[LoaiKB] = @LoaiKB,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThai] = @TrangThai,
	[NamTiepNhan] = @NamTiepNhan,
	[LoaiGiayPhep] = @LoaiGiayPhep,
	[HinhThucTruLui] = @HinhThucTruLui
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_InsertUpdate]
	@ID bigint,
	@SoGiayPhep varchar(100),
	@NgayGiayPhep datetime,
	@NgayHetHan datetime,
	@NguoiCap nvarchar(100),
	@NoiCap nvarchar(100),
	@MaDonViDuocCap varchar(50),
	@TenDonViDuocCap nvarchar(100),
	@MaCoQuanCap varchar(50),
	@TenQuanCap nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@LoaiGiayPhep varchar(10),
	@HinhThucTruLui nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GiayPhep] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GiayPhep] 
		SET
			[SoGiayPhep] = @SoGiayPhep,
			[NgayGiayPhep] = @NgayGiayPhep,
			[NgayHetHan] = @NgayHetHan,
			[NguoiCap] = @NguoiCap,
			[NoiCap] = @NoiCap,
			[MaDonViDuocCap] = @MaDonViDuocCap,
			[TenDonViDuocCap] = @TenDonViDuocCap,
			[MaCoQuanCap] = @MaCoQuanCap,
			[TenQuanCap] = @TenQuanCap,
			[ThongTinKhac] = @ThongTinKhac,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GuidStr] = @GuidStr,
			[LoaiKB] = @LoaiKB,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThai] = @TrangThai,
			[NamTiepNhan] = @NamTiepNhan,
			[LoaiGiayPhep] = @LoaiGiayPhep,
			[HinhThucTruLui] = @HinhThucTruLui
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GiayPhep]
		(
			[SoGiayPhep],
			[NgayGiayPhep],
			[NgayHetHan],
			[NguoiCap],
			[NoiCap],
			[MaDonViDuocCap],
			[TenDonViDuocCap],
			[MaCoQuanCap],
			[TenQuanCap],
			[ThongTinKhac],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GuidStr],
			[LoaiKB],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThai],
			[NamTiepNhan],
			[LoaiGiayPhep],
			[HinhThucTruLui]
		)
		VALUES 
		(
			@SoGiayPhep,
			@NgayGiayPhep,
			@NgayHetHan,
			@NguoiCap,
			@NoiCap,
			@MaDonViDuocCap,
			@TenDonViDuocCap,
			@MaCoQuanCap,
			@TenQuanCap,
			@ThongTinKhac,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GuidStr,
			@LoaiKB,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThai,
			@NamTiepNhan,
			@LoaiGiayPhep,
			@HinhThucTruLui
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
FROM
	[dbo].[t_KDT_GiayPhep]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
FROM
	[dbo].[t_KDT_GiayPhep]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
FROM
	[dbo].[t_KDT_GiayPhep]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GiayPhep] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
FROM [dbo].[t_KDT_GiayPhep] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

UPDATE dbo.t_HaiQuan_Version SET [Version] = '8.0', [Date] = GETDATE(), Notes = N'Cập nhật tờ khai bổ sung'