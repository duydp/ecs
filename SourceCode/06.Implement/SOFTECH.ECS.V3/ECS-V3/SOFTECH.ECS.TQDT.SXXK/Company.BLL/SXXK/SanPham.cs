﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using System;
namespace Company.BLL.SXXK
{
    public partial class SanPham
    {
        public void Saved(SanPhamCollection collection)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (SanPham item in collection)
                    {
                        InsertUpdateTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public DataSet SearchBy_MaHaiQuan(string maHaiQuan)
        {
            string where = string.Format("MaHaiQuan = '{0}'", maHaiQuan.PadRight(6));
            return this.SelectDynamic(where, "Ma");
        }
        public SanPhamCollection getSanPhamCoDinhMuc(string maHaiQuan, string maDoanhNghiep)
        {
            string sql = "SELECT * from t_SXXK_SanPham " +
                        " where ma in (select masanpham from t_SXXK_ThongTinDinhMuc) and MaHaiQuan='" + maHaiQuan + "' AND MaDoanhNghiep = '" + maDoanhNghiep + "'";
            DbCommand dbc = db.GetSqlStringCommand(sql);
            SanPhamCollection collection = new SanPhamCollection();

            IDataReader reader = db.ExecuteReader(dbc);
            while (reader.Read())
            {
                SanPham entity = new SanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                collection.Add(entity);
            }
            return collection;
        }

        public static SanPhamCollection GetSanPhamXML(string maHaiQuan, string maDoanhNghiep)
        {
            string sql = "SELECT * from t_SXXK_SanPham " +
                        " where  MaHaiQuan='" + maHaiQuan + "' AND MaDoanhNghiep = '" + maDoanhNghiep + "'";
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand dbc = db.GetSqlStringCommand(sql);
            SanPhamCollection collection = new SanPhamCollection();

            IDataReader reader = db.ExecuteReader(dbc);
            while (reader.Read())
            {
                SanPham entity = new SanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                collection.Add(entity);
            }
            return collection;
        }
        public static void NhapSPXML(SanPhamCollection SPCollection)
        {
            SanPham sp = new SanPham();
            try { sp.InsertUpdate(SPCollection); }
            catch { }

        }
        public bool UpdateRegistedToDatabase(string maHaiQuan, string maDoanhNghiep, DataSet ds)
        {
            //SanPhamCollection collection = new SanPhamCollection();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                SanPham sp = new SanPham();
                sp.MaHaiQuan = maHaiQuan;
                sp.MaDoanhNghiep = maDoanhNghiep;
                sp.Ma = row["Ma"].ToString().Trim();
                sp.Ten = row["Ten"].ToString().Trim();
                sp.MaHS = row["MaHS"].ToString().Trim();
                int k = sp.MaHS.Length;
                if (sp.MaHS.Length < 10)
                {
                    for (int i = 1; i <= 10 - k; ++i)
                        sp.MaHS += "0";
                }
                sp.DVT_ID = row["DVT_ID"].ToString();
                try
                {
                    sp.Insert();
                }
                catch { }

            }
            return true;
            //return this.InsertUpdate(collection);
        }

        public static SanPham getSanPham(string maHaiQuan, string maDoanhNghiep, string maSP)
        {
            string sql = "SELECT * from t_SXXK_SanPham " +
                        " where  MaHaiQuan='" + maHaiQuan + "' AND MaDoanhNghiep = '" + maDoanhNghiep + "' AND Ma = '" + maSP + "'";
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand dbc = db.GetSqlStringCommand(sql);


            IDataReader reader = db.ExecuteReader(dbc);
            SanPham entity = null;
            while (reader.Read())
            {
                entity = new SanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));

            }
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet GetSPFromUserName(string userName)
        {
            string query = "SELECT sp.Ma, sp.Ten, sp.MaHS, sp.DVT_ID FROM dbo.t_KDT_SXXK_SanPham sp INNER JOIN dbo.t_KDT_SXXK_SanPhamDangKy spdk " +
                           "ON sp.Master_ID = spdk.ID WHERE spdk.ID IN (SELECT ID_DK FROM dbo.t_KDT_SXXK_LogKhaiBao " +
                           "WHERE LoaiKhaiBao = 'SP' AND UserNameKhaiBao = '" + userName + "')";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(query);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_SanPham_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, this._DVT_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public bool InsertUpdate(SanPhamCollection collection, SqlTransaction transaction)
        {
            bool ret = true;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();

                try
                {
                    //bool ret01 = true;
                    foreach (SanPham item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            //ret01 = false;
                            break;
                        }
                    }
                    //if (ret01)
                    //{
                    //    transaction.Commit();
                    //    ret = true;
                    //}
                    //else
                    //{
                    //    transaction.Rollback();
                    //    ret = false;
                    //}
                }
                catch(Exception ex)
                {
                    ret = false;
                    //transaction.Rollback();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw;
                }
                //finally
                //{
                //    connection.Close();

                //}
            }
            return ret;
        }
    }
}