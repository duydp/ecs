IF NOT EXISTS (SELECT * FROM sys.columns WHERE Name IN (N'ListTKNK_Hoan',N'ListTKNK_KhongThu') AND Object_ID = Object_ID(N't_KDT_SXXK_HoSoThanhLyDangKy'))
BEGIN
	ALTER TABLE [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy] ADD
	ListTKNK_Hoan nvarchar(4000),  
	ListTKNK_KhongThu nvarchar(4000)  
END
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Insert]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Insert]
GO

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Insert]  
 @SoTiepNhan bigint,  
 @MaHaiQuanTiepNhan char(6),  
 @NamTiepNhan smallint,  
 @NgayTiepNhan datetime,  
 @MaDoanhNghiep varchar(14),  
 @TrangThaiXuLy int,  
 @TrangThaiThanhKhoan int,  
 @SoHoSo int,  
 @NgayBatDau datetime,  
 @NgayKetThuc datetime,  
 @LanThanhLy int,  
 @SoQuyetDinh nvarchar(100),  
 @NgayQuyetDinh datetime,  
 @UserName varchar(50),  
 @GuidStr nvarchar(36),  
 @ListTKNK_Hoan nvarchar(4000),  
 @ListTKNK_KhongThu nvarchar(4000),  
 @ID bigint OUTPUT  
AS  
INSERT INTO [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]  
(  
 [SoTiepNhan],  
 [MaHaiQuanTiepNhan],  
 [NamTiepNhan],  
 [NgayTiepNhan],  
 [MaDoanhNghiep],  
 [TrangThaiXuLy],  
 [TrangThaiThanhKhoan],  
 [SoHoSo],  
 [NgayBatDau],  
 [NgayKetThuc],  
 [LanThanhLy],  
 [SoQuyetDinh],  
 [NgayQuyetDinh],  
 [UserName],  
 [GuidStr],  
 [ListTKNK_Hoan],  
 [ListTKNK_KhongThu]  
)  
VALUES   
(  
 @SoTiepNhan,  
 @MaHaiQuanTiepNhan,  
 @NamTiepNhan,  
 @NgayTiepNhan,  
 @MaDoanhNghiep,  
 @TrangThaiXuLy,  
 @TrangThaiThanhKhoan,  
 @SoHoSo,  
 @NgayBatDau,  
 @NgayKetThuc,  
 @LanThanhLy,  
 @SoQuyetDinh,  
 @NgayQuyetDinh,  
 @UserName,  
 @GuidStr,  
 @ListTKNK_Hoan,  
 @ListTKNK_KhongThu  
)  
  
SET @ID = SCOPE_IDENTITY()  
GO

IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Update]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Update]
GO

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Update]  
 @ID bigint,  
 @SoTiepNhan bigint,  
 @MaHaiQuanTiepNhan char(6),  
 @NamTiepNhan smallint,  
 @NgayTiepNhan datetime,  
 @MaDoanhNghiep varchar(14),  
 @TrangThaiXuLy int,  
 @TrangThaiThanhKhoan int,  
 @SoHoSo int,  
 @NgayBatDau datetime,  
 @NgayKetThuc datetime,  
 @LanThanhLy int,  
 @SoQuyetDinh nvarchar(100),  
 @NgayQuyetDinh datetime,  
 @UserName varchar(50),  
 @GuidStr nvarchar(36),  
 @ListTKNK_Hoan nvarchar(4000),  
 @ListTKNK_KhongThu nvarchar(4000)  
AS  
  
UPDATE  
 [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]  
SET  
 [SoTiepNhan] = @SoTiepNhan,  
 [MaHaiQuanTiepNhan] = @MaHaiQuanTiepNhan,  
 [NamTiepNhan] = @NamTiepNhan,  
 [NgayTiepNhan] = @NgayTiepNhan,  
 [MaDoanhNghiep] = @MaDoanhNghiep,  
 [TrangThaiXuLy] = @TrangThaiXuLy,  
 [TrangThaiThanhKhoan] = @TrangThaiThanhKhoan,  
 [SoHoSo] = @SoHoSo,  
 [NgayBatDau] = @NgayBatDau,  
 [NgayKetThuc] = @NgayKetThuc,  
 [LanThanhLy] = @LanThanhLy,  
 [SoQuyetDinh] = @SoQuyetDinh,  
 [NgayQuyetDinh] = @NgayQuyetDinh,  
 [UserName] = @UserName,  
 [GuidStr] = @GuidStr,  
 [ListTKNK_Hoan] = @ListTKNK_Hoan,  
 [ListTKNK_KhongThu] = @ListTKNK_KhongThu  
WHERE  
 [ID] = @ID  
 
 GO
 IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_InsertUpdate]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_InsertUpdate]
GO
 CREATE PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@MaHaiQuanTiepNhan char(6),
	@NamTiepNhan smallint,
	@NgayTiepNhan datetime,
	@MaDoanhNghiep varchar(14),
	@TrangThaiXuLy int,
	@TrangThaiThanhKhoan int,
	@SoHoSo int,
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@LanThanhLy int,
	@SoQuyetDinh nvarchar(100),
	@NgayQuyetDinh datetime,
	@UserName varchar(50),
	@GuidStr nvarchar(36),
	@ListTKNK_Hoan nvarchar(4000),
	@ListTKNK_KhongThu nvarchar(4000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_HoSoThanhLyDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[MaHaiQuanTiepNhan] = @MaHaiQuanTiepNhan,
			[NamTiepNhan] = @NamTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[TrangThaiThanhKhoan] = @TrangThaiThanhKhoan,
			[SoHoSo] = @SoHoSo,
			[NgayBatDau] = @NgayBatDau,
			[NgayKetThuc] = @NgayKetThuc,
			[LanThanhLy] = @LanThanhLy,
			[SoQuyetDinh] = @SoQuyetDinh,
			[NgayQuyetDinh] = @NgayQuyetDinh,
			[UserName] = @UserName,
			[GuidStr] = @GuidStr,
			[ListTKNK_Hoan] = @ListTKNK_Hoan,
			[ListTKNK_KhongThu] = @ListTKNK_KhongThu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]
		(
			[SoTiepNhan],
			[MaHaiQuanTiepNhan],
			[NamTiepNhan],
			[NgayTiepNhan],
			[MaDoanhNghiep],
			[TrangThaiXuLy],
			[TrangThaiThanhKhoan],
			[SoHoSo],
			[NgayBatDau],
			[NgayKetThuc],
			[LanThanhLy],
			[SoQuyetDinh],
			[NgayQuyetDinh],
			[UserName],
			[GuidStr],
			[ListTKNK_Hoan],
			[ListTKNK_KhongThu]
		)
		VALUES 
		(
			@SoTiepNhan,
			@MaHaiQuanTiepNhan,
			@NamTiepNhan,
			@NgayTiepNhan,
			@MaDoanhNghiep,
			@TrangThaiXuLy,
			@TrangThaiThanhKhoan,
			@SoHoSo,
			@NgayBatDau,
			@NgayKetThuc,
			@LanThanhLy,
			@SoQuyetDinh,
			@NgayQuyetDinh,
			@UserName,
			@GuidStr,
			@ListTKNK_Hoan,
			@ListTKNK_KhongThu
		)		
	END
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE Name IN (N'SoTKGiay') AND Object_ID = Object_ID(N't_VNACCS_CapSoToKhai'))
BEGIN
	ALTER TABLE [dbo].[t_VNACCS_CapSoToKhai] ADD
	SoTKGiay VARCHAR(12) 
END
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]  
GO

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]  
 @SoTK INT,  
 @MaLoaiHinh VARCHAR(50),  
 @NamDangKy INT,  
 @SoTKVNACCS DECIMAL(18, 0),  
 @SoTKVNACCSFull DECIMAL(12, 0),  
 @SoTKDauTien DECIMAL(15, 0),  
 @SoNhanhTK INT,  
 @TongSoTKChiaNho INT,  
 @SoTKTNTX DECIMAL(15, 0),  
 @Temp1 NVARCHAR(255),  
 @SoTKGiay VARCHAR(12)  
AS  
IF EXISTS(SELECT [SoTK], [MaLoaiHinh] FROM [dbo].[t_VNACCS_CapSoToKhai] WHERE [SoTK] = @SoTK AND [MaLoaiHinh] = @MaLoaiHinh)  
 BEGIN  
  UPDATE  
   [dbo].[t_VNACCS_CapSoToKhai]   
  SET  
   [NamDangKy] = @NamDangKy,  
   [SoTKVNACCS] = @SoTKVNACCS,  
   [SoTKVNACCSFull] = @SoTKVNACCSFull,  
   [SoTKDauTien] = @SoTKDauTien,  
   [SoNhanhTK] = @SoNhanhTK,  
   [TongSoTKChiaNho] = @TongSoTKChiaNho,  
   [SoTKTNTX] = @SoTKTNTX,  
   [Temp1] = @Temp1,  
   [SoTKGiay] = @SoTKGiay  
  WHERE  
   [SoTK] = @SoTK  
   AND [MaLoaiHinh] = @MaLoaiHinh  
 END  
ELSE  
 BEGIN  
 INSERT INTO [dbo].[t_VNACCS_CapSoToKhai]  
 (  
   [SoTK],  
   [MaLoaiHinh],  
   [NamDangKy],  
   [SoTKVNACCS],  
   [SoTKVNACCSFull],  
   [SoTKDauTien],  
   [SoNhanhTK],  
   [TongSoTKChiaNho],  
   [SoTKTNTX],  
   [Temp1],  
   [SoTKGiay]  
 )  
 VALUES  
 (  
   @SoTK,  
   @MaLoaiHinh,  
   @NamDangKy,  
   @SoTKVNACCS,  
   @SoTKVNACCSFull,  
   @SoTKDauTien,  
   @SoNhanhTK,  
   @TongSoTKChiaNho,  
   @SoTKTNTX,  
   @Temp1,  
   @SoTKGiay  
 )   
 END  

 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '33.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('33.8',GETDATE(), N'CẬP NHẬT SỬA LỖI')
END