/*
Alter Column StationCode data Type varchar(5) -> varchar(6)

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [dbo].[t_VNACC_Category_Stations]'
GO
ALTER TABLE [dbo].[t_VNACC_Category_Stations] DROP CONSTRAINT [PK_t_KDT_VNACC_Stations]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_VNACC_Category_Stations]'
GO
ALTER TABLE [dbo].[t_VNACC_Category_Stations] ALTER COLUMN [StationCode] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_VNACC_Stations] on [dbo].[t_VNACC_Category_Stations]'
GO
ALTER TABLE [dbo].[t_VNACC_Category_Stations] ADD CONSTRAINT [PK_t_KDT_VNACC_Stations] PRIMARY KEY CLUSTERED  ([StationCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

-- cập nhật Danh mục cửa khẩu đường sắt (A601)
Delete t_VNACC_Category_Stations where CountryCode='CN'
Update t_VNACC_Category_Stations set StationCode='VNDGLS', StationName='DONG DANG (LANG SON)' where StationCode='DG'
Update t_VNACC_Category_Stations set StationCode='VNLCLC', StationName='LAO CAI' where StationCode='LC'
Update t_VNACC_Category_Stations set StationCode='VNYVHN', StationName='YEN VIEN (HA NOI)' where StationCode='HN'
--- Cập nhật Danh mục mã xác định thời hạn nộp thuế (E019)
update t_VNACC_Category_Common  set Name_VN=N'Sử dụng bảo lãnh riêng' WHERE ReferenceDB='E019' and Code= 'A'
update t_VNACC_Category_Common  set Name_VN=N'Sử dụng bảo lãnh chung' WHERE ReferenceDB='E019' and Code= 'B'
update t_VNACC_Category_Common  set Name_VN=N'Không sử dụng bảo lãnh' WHERE ReferenceDB='E019' and Code= 'C'
update t_VNACC_Category_Common  set Name_VN=N'Nộp thuế ngay' WHERE ReferenceDB='E019' and Code= 'D'

           --Cập nhật version
     IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '13.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('13.6', GETDATE(), N'Cap nhat Danh muc  (A601,E019)')
END	
