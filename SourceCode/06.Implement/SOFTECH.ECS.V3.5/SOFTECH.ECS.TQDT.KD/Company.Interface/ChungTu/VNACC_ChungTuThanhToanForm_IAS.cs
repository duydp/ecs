﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.Interface.VNACCS;

namespace Company.Interface
{
    public partial class VNACC_ChungTuThanhToanForm_IAS : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ChungTuThanhToan CTTT = new KDT_VNACC_ChungTuThanhToan();
        public KDT_VNACC_ChungTuThanhToan_ChiTiet CTTT_ChiTiet = new KDT_VNACC_ChungTuThanhToan_ChiTiet();
        private ELoaiThongTin LoaiChungTuThanhToan = ELoaiThongTin.IAS;

        public VNACC_ChungTuThanhToanForm_IAS()
        {
            InitializeComponent();

            this.Load += new EventHandler(VNACC_ChungTuThanhToanForm_IAS_Load);
            cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmbMain_CommandClick);

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EPayment.IAS.ToString());
        }

        public VNACC_ChungTuThanhToanForm_IAS(long id)
        {
            InitializeComponent();

            this.Load += new EventHandler(VNACC_ChungTuThanhToanForm_IAS_Load);
            cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmbMain_CommandClick);

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EPayment.IAS.ToString());

            CTTT = KDT_VNACC_ChungTuThanhToan.Load(id);
            CTTT.LoadChungTuThanhToan_ChiTiet();
            SetCTTT();
        }

        void VNACC_ChungTuThanhToanForm_IAS_Load(object sender, EventArgs e)
        {
            cmdLuu.Visible = cmdLuu1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            cmdLayPhanHoi.Visible = cmdLayPhanHoi.Visible = Janus.Windows.UI.InheritableBoolean.False;
            try
            {
                Cursor = Cursors.WaitCursor;

                SetIDControl();

                SetMaxLengthControl();

                ValidateForm(true);

                SetAutoRemoveUnicodeAndUpperCaseControl();

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdLuu":
                    this.SaveCTTT();
                    break;

                case "cmdKhaiBao":
                    GetCTTT();
                    break;
                case "cmdLayPhanHoi":
                    break;
            }
        }

        private void SaveCTTT()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                //if (!ValidateForm(false))
                //    return;

                GetCTTT();

                CTTT.InsertUpdateFull();

                Helper.Controls.MessageBoxControlV.ShowMessage(Company.KDT.SHARE.Components.ThongBao.APPLICATION_SAVE_DATA_SUCCESS_0Param, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetCTTT()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                CTTT.LoaiChungTu = LoaiChungTuThanhToan.ToString();
                CTTT.MaNganHangCungCapBaoLanh = txtMaNganHangCungCapBaoLanh.Text;
                CTTT.TenNganHangCungCapBaoLanh = txtTenNganHangCungCapBaoLanh.Text;
                CTTT.NamPhatHanh = Convert.ToInt32(txtNamPhatHanh.Value);
                CTTT.KiHieuChungTuPhatHanhBaoLanh = txtKiHieuChungTuPhatHanhBaoLanh.Text;
                CTTT.SoChungTuPhatHanhBaoLanh = txtSoChungTuPhatHanhBaoLanh.Text;
                CTTT.SoToKhai = Convert.ToInt32(txtSoToKhai.Value);
                CTTT.NgayDuDinhKhaiBao = dtNgayDuDinhKhaiBao.Value;
                CTTT.MaLoaiHinh = txtMaLoaiHinh.Text;
                CTTT.CoQuanHaiQuan = txtCoQuanHaiQuan.Text;
                CTTT.TenCucHaiQuan = txtTenCucHaiQuan.Text;
                CTTT.SoVanDon_1 = txtSoVanDon_1.Text;
                CTTT.SoVanDon_2 = txtSoVanDon_2.Text;
                CTTT.SoVanDon_3 = txtSoVanDon_3.Text;
                CTTT.SoVanDon_4 = txtSoVanDon_4.Text;
                CTTT.SoVanDon_5 = txtSoVanDon_5.Text;
                CTTT.SoHoaDon = txtSoHoaDon.Text;
                CTTT.MaDonViSuDungBaoLanh = txtMaDonViSuDungBaoLanh.Text;
                CTTT.TenDonViSuDungBaoLanh = txtTenDonViSuDungBaoLanh.Text;
                CTTT.MaDonViDaiDienSuDungBaoLanh = txtMaDonViDaiDienSuDungBaoLanh.Text;
                CTTT.TenDonViDaiDienSuDungBaoLanh = txtTenDonViDaiDienSuDungBaoLanh.Text;
                CTTT.NgayBatDauHieuLuc = dtNgayBatDauHieuLuc.Value;
                CTTT.NgayHetHieuLuc = dtNgayHetHieuLuc.Value;
                CTTT.SoTienDangKyBaoLanh = Convert.ToDecimal(txtSoTienDangKyBaoLanh.Value);
                CTTT.MaTienTe = ucMaTienTe.Code;
                CTTT.CoBaoVoHieu = txtCoBaoVohieu.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void SetCTTT()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNganHangCungCapBaoLanh.Text = CTTT.MaNganHangCungCapBaoLanh;
                txtTenNganHangCungCapBaoLanh.Text = CTTT.TenNganHangCungCapBaoLanh;
                txtNamPhatHanh.Value = CTTT.NamPhatHanh;
                txtKiHieuChungTuPhatHanhBaoLanh.Text = CTTT.KiHieuChungTuPhatHanhBaoLanh;
                txtSoChungTuPhatHanhBaoLanh.Text = CTTT.SoChungTuPhatHanhBaoLanh;
                txtSoToKhai.Value = CTTT.SoToKhai;
                dtNgayDuDinhKhaiBao.Value = CTTT.NgayDuDinhKhaiBao;
                txtMaLoaiHinh.Text = CTTT.MaLoaiHinh;
                txtCoQuanHaiQuan.Text = CTTT.CoQuanHaiQuan;
                txtTenCucHaiQuan.Text = CTTT.TenCucHaiQuan;
                txtSoVanDon_1.Text = CTTT.SoVanDon_1;
                txtSoVanDon_2.Text = CTTT.SoVanDon_2;
                txtSoVanDon_3.Text = CTTT.SoVanDon_3;
                txtSoVanDon_4.Text = CTTT.SoVanDon_4;
                txtSoVanDon_5.Text = CTTT.SoVanDon_5;
                txtSoHoaDon.Text = CTTT.SoHoaDon;
                txtMaDonViSuDungBaoLanh.Text = CTTT.MaDonViSuDungBaoLanh;
                txtTenDonViSuDungBaoLanh.Text = CTTT.TenDonViSuDungBaoLanh;
                txtMaDonViDaiDienSuDungBaoLanh.Text = CTTT.MaDonViDaiDienSuDungBaoLanh;
                txtTenDonViDaiDienSuDungBaoLanh.Text = CTTT.TenDonViDaiDienSuDungBaoLanh;
                dtNgayBatDauHieuLuc.Value = CTTT.NgayBatDauHieuLuc;
                dtNgayHetHieuLuc.Value = CTTT.NgayHetHieuLuc;
                txtSoTienDangKyBaoLanh.Value = CTTT.SoTienDangKyBaoLanh;
                ucMaTienTe.Code = CTTT.MaTienTe;
                txtCoBaoVohieu.Text = CTTT.CoBaoVoHieu;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void grdHang_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                CTTT_ChiTiet = (KDT_VNACC_ChungTuThanhToan_ChiTiet)items[0].GetRow().DataRow;
                SetHang(CTTT_ChiTiet);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetHang(KDT_VNACC_ChungTuThanhToan_ChiTiet hang)
        {
            hang.SoThuTuGiaoDich = Convert.ToInt32(txtSoThuTuGiaoDich.Value);
            hang.NgayTaoDuLieu = dtNgayTaoDuLieu.Value;
            hang.ThoiGianTaoDuLieu = dtThoiGianTaoDuLieu.Value;
            hang.SoTienTruLui = Convert.ToDecimal(txtSoTienTruLui.Value);
            hang.SoTienTangLen = Convert.ToDecimal(txtSoTienTangLen.Value);
            hang.SoToKhai = Convert.ToInt32(txtSoToKhaiChiTiet.Value);
            hang.NgayDangKyToKhai = dtNgayDangKyToKhai.Value;
            hang.MaLoaiHinh = txtMaLoaiHinhChiTiet.Text;
            hang.CoQuanHaiQuan = txtCoQuanhaiQuanChiTiet.Text;
            hang.TenCucHaiQuan = txtTenCucHaiQuanChiTiet.Text;
        }

        private void SetHang(KDT_VNACC_ChungTuThanhToan_ChiTiet hang)
        {
            txtSoThuTuGiaoDich.Value = hang.SoThuTuGiaoDich;
            dtNgayTaoDuLieu.Value = hang.NgayTaoDuLieu;
            dtThoiGianTaoDuLieu.Value = hang.ThoiGianTaoDuLieu;
            txtSoTienTruLui.Value = hang.SoTienTruLui;
            txtSoTienTangLen.Value = hang.SoTienTangLen;
            txtSoToKhaiChiTiet.Value = hang.SoToKhai;
            dtNgayDangKyToKhai.Value = hang.NgayDangKyToKhai;
            txtMaLoaiHinhChiTiet.Text = hang.MaLoaiHinh;
            txtCoQuanhaiQuanChiTiet.Text = hang.CoQuanHaiQuan;
            txtTenCucHaiQuanChiTiet.Text = hang.TenCucHaiQuan;

        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                ucLoaiHinhBaoLanh.TagName = "TYS"; //Loại hình bảo lãnh
                txtMaNganHangCungCapBaoLanh.Tag = "SBC"; //Mã ngân hàng cung cấp bảo lãnh
                txtNamPhatHanh.Tag = "RYA"; //Năm phát hành
                txtKiHieuChungTuPhatHanhBaoLanh.Tag = "SCM"; //Kí hiệu chứng từ phát hành bảo lãnh
                txtSoChungTuPhatHanhBaoLanh.Tag = "SCN"; //Số chứng từ  phát hành bảo lãnh
                ucMaTienTe.TagName = "CCC"; //Mã tiền tệ

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                ucLoaiHinhBaoLanh.IsOnlyWarning = isOnlyWarning;
                ucLoaiHinhBaoLanh.SetValidate = !isOnlyWarning;
                isValid &= ucLoaiHinhBaoLanh.IsValidate; //"Loại hình bảo lãnh");
                isValid &= ValidateControl.ValidateNull(txtMaNganHangCungCapBaoLanh, errorProvider, "Mã ngân hàng cung cấp bảo lãnh", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNamPhatHanh, errorProvider, "Năm phát hành", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtKiHieuChungTuPhatHanhBaoLanh, errorProvider, "Kí hiệu chứng từ phát hành bảo lãnh", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoChungTuPhatHanhBaoLanh, errorProvider, "Số chứng từ  phát hành bảo lãnh", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                //ucLoaiHinhBaoLanh.MaxLength = 1;
                txtMaNganHangCungCapBaoLanh.MaxLength = 11;
                txtNamPhatHanh.MaxLength = 4;
                txtKiHieuChungTuPhatHanhBaoLanh.MaxLength = 10;
                txtSoChungTuPhatHanhBaoLanh.MaxLength = 10;
                //ucMaTienTe.MaxLength = 3;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            ucLoaiHinhBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Số tờ khai đầu tiên
            txtMaNganHangCungCapBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã loại hình
            txtNamPhatHanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại hàng hóa
            txtKiHieuChungTuPhatHanhBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã hiệu phương thức vận chuyển
            txtSoChungTuPhatHanhBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Phân loại cá nhân/tổ chức
            ucMaTienTe.TextChanged += new EventHandler(SetTextChanged_Handler); //Cơ quan Hải quan

            ucLoaiHinhBaoLanh.IsUpperCase = true; //Số tờ khai đầu tiên
            txtMaNganHangCungCapBaoLanh.CharacterCasing = CharacterCasing.Upper; //Mã loại hình
            txtNamPhatHanh.CharacterCasing = CharacterCasing.Upper; //Mã phân loại hàng hóa
            txtKiHieuChungTuPhatHanhBaoLanh.CharacterCasing = CharacterCasing.Upper; //Mã hiệu phương thức vận chuyển
            txtSoChungTuPhatHanhBaoLanh.CharacterCasing = CharacterCasing.Upper; //Phân loại cá nhân/tổ chức
            ucMaTienTe.IsUpperCase = true; //Cơ quan Hải quan
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Send VNACCS

        private void GetIAS()
        {
            try
            {
                CTTT.InputMessageID = HelperVNACCS.NewInputMSGID();
                MessagesSend msg;
                IAS IAS;
                IAS = VNACCMaperFromObject.IASMapper(CTTT);
                if (IAS == null)
                {
                    this.ShowMessage("Lỗi khi tạo messages !", false);
                    return;
                }
                msg = MessagesSend.Load<IAS>(IAS, CTTT.InputMessageID);
                MsgLog.SaveMessages(msg, CTTT.ID, EnumThongBao.SendMess, "");
                SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                f.isSend = true;
                f.isRep = false;
                f.inputMSGID = CTTT.InputMessageID;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.Cancel)
                {
                    this.ShowMessage(f.msgFeedBack, false);
                }
                else if (f.DialogResult == DialogResult.No)
                {
                }
                else
                {
                    CTTT.InputMessageID = msg.Header.InputMessagesID.GetValue().ToString();
                    if (f.feedback.Error == null || f.feedback.Error.Count == 0)
                    {
                        if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
                        {
                            CapNhatThongTin(f.feedback.ResponseData);
                            f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
                        }
                    }
                    this.ShowMessageTQDT(f.msgFeedBack, false);
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
            }
        

        #endregion

        #region Cập nhật thông tin

        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            ProcessMessages.GetDataResult_ChungTuThanhToan_IAS(msgResult, "", CTTT);
            CTTT.InsertUpdateFull();
            SetCTTT();
            //setCommandStatus();
        }

        private void TuDongCapNhatThongTin()
        {
            if (CTTT != null && CTTT.SoChungTuPhatHanhBaoLanh != "" && CTTT.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}' And MessagesInputID = '{1}'", CTTT.SoChungTuPhatHanhBaoLanh.ToString(), CTTT.InputMessageID), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }

        #endregion

    }
}
