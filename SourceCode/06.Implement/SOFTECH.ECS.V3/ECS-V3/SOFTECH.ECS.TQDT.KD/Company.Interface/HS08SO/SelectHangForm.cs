﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3
using Company.KD.BLL.KDT;
using Company.KD.BLL;
using Company.KD.BLL.Utils;
#elif SXXK_V3
using Company.BLL.KDT;
using Company.BLL;
using Company.BLL.Utils;
#elif GC_V3
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
#endif
using System.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.HS08SO
{
    public partial class SelectHangForm : BaseForm
    {
        public string LoaiHangHoa;

        private List<HangMauDich> HangCollection = new List<HangMauDich>();
        public List<HangMauDich> HangCollectionSelected = new List<HangMauDich>();

        public SelectHangForm()
        {
            InitializeComponent();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool checkHangExitByMa(string maHang, string dvtId, string maHS)
        {
            foreach (HangMauDich hmd in HangCollectionSelected)
            {
                if (hmd.Ma.Trim() == maHang.Trim() && hmd.MaHS.Trim() == maHS.Trim() && hmd.DVT_ID.Trim() == dvtId.Trim())
                    return true;
            }
            return false;
        }

        private bool checkHangExitByTen(string tenHang, string dvtId, string maHS)
        {
            foreach (HangMauDich hmd in HangCollectionSelected)
            {
                if (hmd.Ten == tenHang && hmd.MaHS.Trim() == maHS.Trim() && hmd.DVT_ID.Trim() == dvtId.Trim())
                    return true;
            }
            return false;
        }

        private void SelectHangForm_Load(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnChonNhieuHang_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetCheckedRows();
            if (items.Length < 0) return;
            foreach (GridEXRow i in items)
            {
                if (i.IsChecked && i.RowType == RowType.Record)
                {
                    HangMauDich HMD = (HangMauDich)i.DataRow;
#if KD_V3
                    if (HMD.Ma != "")
                        if (!checkHangExitByMa(HMD.Ma, HMD.DVT_ID, HMD.MaHS))
                            HangCollectionSelected.Add(HMD);
                    if (HMD.Ma == "")
                        if (!checkHangExitByTen(HMD.Ten, HMD.DVT_ID, HMD.MaHS))
                            HangCollectionSelected.Add(HMD);
#elif SXXK_V3 || GC_V3
                    if (HMD.Ma != "")
                        if (!checkHangExitByMa(HMD.Ma, HMD.DVT_ID, HMD.MaHS))
                            HangCollectionSelected.Add(HMD);
#endif
                }
            }
            this.Close();
        }

        private void btnChonTatCa_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetRows();
            if (items.Length < 0) return;
            foreach (GridEXRow i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangMauDich HMD = (HangMauDich)i.DataRow;
#if KD_V3
                    if (HMD.Ma != "")
                        if (!checkHangExitByMa(HMD.Ma, HMD.DVT_ID, HMD.MaHS))
                            HangCollectionSelected.Add(HMD);
                    if (HMD.Ma == "")
                        if (!checkHangExitByTen(HMD.Ten, HMD.DVT_ID, HMD.MaHS))
                            HangCollectionSelected.Add(HMD);
#elif SXXK_V3 || GC_V3
                    if (HMD.Ma != "")
                        if (!checkHangExitByMa(HMD.Ma, HMD.DVT_ID, HMD.MaHS))
                            HangCollectionSelected.Add(HMD);
#endif
                }
            }
            this.Close();
        }

        public void BindData()
        {
            try
            {
                string title = (LoaiHangHoa == "NPL" ? "Nguyên phụ liệu" : (LoaiHangHoa == "SP" ? "Sản phẩm" : (LoaiHangHoa == "TB" ? "Thiết bị" : ""))) + " đã đăng ký";
                this.Text = this.Text + " - " + title;

#if KD_V3
                if (LoaiHangHoa == "NPL")
                {
                    HangCollection = Company.KD.BLL.KDT.HangMauDich.SelectHang(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, "N");
                }
                else if (LoaiHangHoa == "SP")
                {
                    HangCollection = Company.KD.BLL.KDT.HangMauDich.SelectHang(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, "X");
                }
#elif SXXK_V3
                if (LoaiHangHoa == "NPL")
                {
                    HangCollection = Company.BLL.KDT.HangMauDich.SelectHang(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, "N");
                }
                else if (LoaiHangHoa == "SP")
                {
                    HangCollection = Company.BLL.KDT.HangMauDich.SelectHang(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, "X");
                }
#elif GC_V3
                if (LoaiHangHoa == "NPL")
                {
                    HangCollection = Company.GC.BLL.KDT.HangMauDich.SelectHang(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, "N");
                }
                else if (LoaiHangHoa == "SP")
                {
                    HangCollection = Company.GC.BLL.KDT.HangMauDich.SelectHang(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, "X");
                }
                else if (LoaiHangHoa == "TB")
                {
                    HangCollection = Company.GC.BLL.KDT.HangMauDich.SelectHang(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, "T");
                }
#endif

                dgList.DataSource = HangCollection;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
