<%------------------------------------------------------------------------------------------
* Author: Eric J. Smith 
* Description: This template will generate standard CRUD stored procedures for a given 
*   database table.
------------------------------------------------------------------------------------------%>
<%@ CodeTemplate Debug="True" Language="C#" Inherits="CodeSmith.BaseTemplates.SqlCodeTemplate" TargetLanguage="T-SQL"
	Description="Generates standard CRUD procedures based on a database table schema." %>
<%-- Context --%>
<%@ Property Name="SourceTable" Type="SchemaExplorer.TableSchema" Category="1. Context"
	Description="Table that the stored procedures should be based on." %>

<%-- Options --%>
<%@ Property Name="IncludeDropStatements" Type="System.Boolean" Default="True" Category="2. Options"
	Description="If true drop statements will be generated to drop existing stored procedures." %>
<%@ Property Name="ProcedurePrefix" Type="System.String" Default="p_" Category="2. Options"
	Description="Prefix to use for all generated procedure names." %>
<%@ Property Name="TablePrefix" Type="System.String" Default="t_" Category="2. Options"
	Description="If this prefix is found at the start of a table name, it will be stripped off." %>
<%@ Property Name="AutoExecuteScript" Type="System.Boolean" Default="False" Category="2. Options"
	Description="Whether or not to immediately execute the script on the target database." %>
<%@ Property Name="OrderByExpression" Type="System.String" Default="" Optional="True" Category="2. Options"
	Description="If supplied, this expression will be used to apply an order to the results on SELECT statements." %>
<%@ Property Name="Fields" Type="ColumnSchemaCollection" %>

<%-- Assembly References --%>
<%@ Assembly Name="SchemaExplorer" %>
<%@ Assembly Name="CodeSmith.BaseTemplates" %>
<%@ Assembly Name="CodeSmith.CustomProperties" %>
<%@ Assembly Name="System.Data" %>
<%-- Namespace Imports --%>
<%@ Import Namespace="SchemaExplorer" %>
<%@ Import Namespace="CodeSmith.CustomProperties" %>
<%@ Import Namespace="CodeSmith.BaseTemplates" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Text.RegularExpressions" %>
<%
this.Progress.MaximumValue = 13;
this.Progress.Step = 1;

// this template requires a primary key on the source table
if (!SourceTable.HasPrimaryKey) throw new ApplicationException("SourceTable does not contain a primary key.");

if (IncludeDropStatements)
{
	Response.WriteLine("-- Drop Existing Procedures");
	Response.WriteLine("");
	
	GenerateDropStatement(GetSelectByProcedureName(this.Fields));
    
	Response.WriteLine("");
	Response.WriteLine("GO");
	Response.WriteLine("");
	
	this.Progress.PerformStep();
}
%>
<%
		string procedureName = GetSelectByProcedureName(this.Fields);
		if (true)
		{
			_generatedProcedureNames.Add(procedureName);
			GenerateProcedureHeader(procedureName);
%>

CREATE PROCEDURE <%= procedureName %>
<% GenerateParameters(this.Fields, 1); %>
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	*
FROM
	dbo.<%= this.Fields[0].Table.Name %>
WHERE
	<% GenerateConditions(this.Fields, 1); %>
<% GenerateOrderByClause(); %>

<%
			GenerateProcedureFooter(procedureName);
		}
	this.Progress.PerformStep();
%>

<script runat="template">
#region Member Variables
private StringCollection _droppedProcedureNames = new StringCollection();
private StringCollection _generatedProcedureNames = new StringCollection();
#endregion

#region Isolation Level
public enum TransactionIsolationLevelEnum
{
	ReadCommitted,
	ReadUncommitted,
	RepeatableRead,
	Serializable
}

public void GenerateSetTransactionIsolationLevelStatement(TransactionIsolationLevelEnum isolationLevel)
{
	Response.Write("SET TRANSACTION ISOLATION LEVEL ");
	
	switch (isolationLevel)
	{
		case TransactionIsolationLevelEnum.ReadUncommitted:
		{
			Response.WriteLine("READ UNCOMMITTED");
			break;
		}
		case TransactionIsolationLevelEnum.RepeatableRead:
		{
			Response.WriteLine("REPEATABLE READ");
			break;
		}
		case TransactionIsolationLevelEnum.Serializable:
		{
			Response.WriteLine("SERIALIZABLE");
			break;
		}
		default:
		{
			Response.WriteLine("READ COMMITTED");
			break;
		}
	}
}
#endregion

#region Code Generation Helpers
public string GetTableOwner()
{
	return GetTableOwner(true);
}

public string GetTableOwner(bool includeDot)
{
	if (SourceTable.Owner.Length > 0)
	{
		if (includeDot)
		{
			return "[" + SourceTable.Owner + "].";
		}
		else
		{
			return "[" + SourceTable.Owner + "]";
		}
	}
	else
	{
		return "";
	}
}

public void GenerateDropStatement(string procedureName)
{
	Response.WriteLine("IF OBJECT_ID(N'{0}') IS NOT NULL", procedureName);
	GenerateIndent(1);
	Response.WriteLine("DROP PROCEDURE {0}", procedureName);
	Response.WriteLine("");
	
	// add this procedure to the list of dropped procedures
	_droppedProcedureNames.Add(procedureName);	
}

public void GenerateProcedureHeader(string procedureName)
{	
	Response.WriteLine("");
	Response.WriteLine("------------------------------------------------------------------------------------------------------------------------");
	Response.WriteLine("-- Stored procedure name: {0}", procedureName);
	Response.WriteLine("-- Database: {0}", SourceTable.Database.Name);				
	Response.WriteLine("-- Time created: {0}", DateTime.Now.ToLongDateString());		
	Response.WriteLine("------------------------------------------------------------------------------------------------------------------------");
}

public void GenerateProcedureFooter(string procedureName)
{	
	Response.WriteLine("GO");
}

public void GenerateIndent(int indentLevel)
{
    for (int i = 0; i < indentLevel; i++)
	{
		Response.Write('\t');
	}
}

public void GenerateParameter(ColumnSchema column, int indentLevel, bool isFirst, bool isLast)
{
	GenerateParameter(column, indentLevel, isFirst, isLast, false);
}

public void GenerateParameter(ColumnSchema column, int indentLevel, bool isFirst, bool isLast, bool isOutput)
{
	GenerateIndent(indentLevel);
	Response.Write(GetSqlParameterStatement(column, isOutput));
	if (!isLast) Response.Write(",");
	if (indentLevel >= 0)
	{
		Response.WriteLine("");
	}
	else if (!isLast)
	{
		Response.Write(" ");
	}
}

public void GenerateParameters(ColumnSchemaCollection columns, int indentLevel)
{
	GenerateParameters(columns, indentLevel, false);
}

public void GenerateParameters(ColumnSchemaCollection columns, int indentLevel, bool includeTrailingComma)
{
	for (int i = 0; i < columns.Count; i++)
	{
		GenerateParameter(columns[i], indentLevel, i == 0, i == columns.Count - 1 && !includeTrailingComma);
	}
}

public void GenerateColumn(ColumnSchema column, int indentLevel, bool isFirst, bool isLast)
{
	GenerateIndent(indentLevel);
	Response.Write("[");
	Response.Write(column.Name);
	Response.Write("]");
	if (!isLast) Response.Write(",");
	if (indentLevel >= 0)
	{
		Response.WriteLine("");
	}
	else if (!isLast)
	{
		Response.Write(" ");
	}
}

public void GenerateAllColumns(ColumnSchemaCollection columns, int indentLevel)
{
	/*
	ColumnSchemaCollection filteredColumns = FilterExcludedColumns(columns);
	for (int i = 0; i < filteredColumns.Count; i++)
	{
		GenerateColumn(filteredColumns[i], indentLevel, i == 0, i == filteredColumns.Count - 1);
	}
	
	// Pass exclude columns.	
	GenerateIndent(indentLevel);
	
	*/
	for (int i = 0; i < columns.Count; i++)
	{		
		GenerateColumn(columns[i], indentLevel, i == 0, i == columns.Count - 1);
	}
	
}

public void GenerateColumns(ColumnSchemaCollection columns, int indentLevel)
{
	ColumnSchemaCollection filteredColumns = columns;
	for (int i = 0; i < filteredColumns.Count; i++)
	{
		GenerateColumn(filteredColumns[i], indentLevel, i == 0, i == filteredColumns.Count - 1);
	}
	
	// Pass exclude columns.	
	//GenerateIndent(indentLevel);
	
	/*
	for (int i = 0; i < columns.Count; i++)
	{		
		GenerateColumn(columns[i], indentLevel, i == 0, i == columns.Count - 1);
	}
	*/
}

public void GenerateUpdate(ColumnSchema column, int indentLevel, bool isFirst, bool isLast)
{
	GenerateIndent(indentLevel);
	Response.Write("[");
	Response.Write(column.Name);
	Response.Write("] = @");
	Response.Write(column.Name);
	if (!isLast) Response.Write(",");
	if (indentLevel >= 0)
	{
		Response.WriteLine("");
	}
	else if (!isLast)
	{
		Response.Write(" ");
	}
}


public void GenerateCondition(ColumnSchema column, int indentLevel, bool isFirst, bool isLast)
{
	GenerateIndent(indentLevel);
	if (!isFirst) Response.Write("AND ");
	Response.Write("[");
	Response.Write(column.Name);
	Response.Write("] = @");
	Response.Write(column.Name);
	if (indentLevel >= 0)
	{
		Response.WriteLine("");
	}
	else if (!isLast)
	{
		Response.Write(" ");
	}
}

public void GenerateConditions(ColumnSchemaCollection columns, int indentLevel)
{
	ColumnSchemaCollection filteredColumns = columns;
	for (int i = 0; i < filteredColumns.Count; i++)
	{
		GenerateCondition(filteredColumns[i], indentLevel, i == 0, i == filteredColumns.Count - 1);
	}
}

public void GenerateVariable(ColumnSchema column, int indentLevel, bool isFirst, bool isLast)
{
	GenerateIndent(indentLevel);
	Response.Write("@");
	Response.Write(column.Name);
	if (!isLast) Response.Write(",");
	if (indentLevel >= 0)
	{
		Response.WriteLine("");
	}
	else if (!isLast)
	{
		Response.Write(" ");
	}
}

public void GenerateOrderByClause()
{
	if (OrderByExpression != null && OrderByExpression.Trim().Length > 0)
	{
		Response.WriteLine("ORDER BY");
		GenerateIndent(1);
		Response.WriteLine(OrderByExpression);
	}
}
#endregion

#region Procedure Naming
public string GetInsertProcedureName()
{
	return String.Format("{0}[{1}{2}_Insert]", GetTableOwner(), ProcedurePrefix, GetEntityName(false));
}

public string GetUpdateProcedureName()
{
	return String.Format("{0}[{1}{2}_Update]", GetTableOwner(), ProcedurePrefix, GetEntityName(false));
}

public string GetInsertUpdateProcedureName()
{
	return String.Format("{0}[{1}{2}_InsertUpdate]", GetTableOwner(), ProcedurePrefix, GetEntityName(false));
}

public string GetDeleteProcedureName()
{
	return String.Format("{0}[{1}{2}_Delete]", GetTableOwner(), ProcedurePrefix, GetEntityName(false));
}

public string GetSelectProcedureName()
{
	return String.Format("{0}[{1}{2}_Load]", GetTableOwner(), ProcedurePrefix, GetEntityName(false));
}

public string GetSelectAllProcedureName()
{
	return String.Format("{0}[{1}{2}_SelectAll]", GetTableOwner(), ProcedurePrefix, GetEntityName(false));
}

public string GetSelectPagedProcedureName()
{
	return String.Format("{0}[{1}{2}_SelectPaged]", GetTableOwner(), ProcedurePrefix, GetEntityName(false));
}

public string GetSelectByProcedureName(ColumnSchemaCollection targetColumns)
{
	return String.Format("{0}[{1}{2}_SelectBy_{3}]", GetTableOwner(), ProcedurePrefix, GetEntityName(false), GetBySuffix(targetColumns));
}

public string GetSelectDynamicProcedureName()
{
	return String.Format("{0}[{1}{2}_SelectDynamic]", GetTableOwner(), ProcedurePrefix, GetEntityName(false));
}

public string GetDeleteByProcedureName(ColumnSchemaCollection targetColumns)
{
	return String.Format("{0}[{1}{2}_DeleteBy_{3}]", GetTableOwner(), ProcedurePrefix, GetEntityName(false), GetBySuffix(targetColumns));
}

public string GetDeleteDynamicProcedureName()
{
	return String.Format("{0}[{1}{2}_DeleteDynamic]", GetTableOwner(), ProcedurePrefix, GetEntityName(false));
}

public string GetEntityName(bool plural)
{
	string entityName = SourceTable.Name;
	
	if (entityName.StartsWith(TablePrefix))
	{
		entityName = entityName.Substring(TablePrefix.Length);
	}
	
	if (plural)
	{
		entityName = StringUtil.ToPlural(entityName);
	}
	else
	{
		entityName = StringUtil.ToSingular(entityName);		
	}
	
	return entityName;
}

public string GetBySuffix(ColumnSchemaCollection columns)
{
    System.Text.StringBuilder bySuffix = new System.Text.StringBuilder();
	for (int i = 0; i < columns.Count; i++)
	{
	    if (i > 0) bySuffix.Append("_");
	    bySuffix.Append(columns[i].Name);
	}
	
	return bySuffix.ToString();
}
#endregion

#region Template Overrides
// Assign an appropriate file name to the output.
public override string GetFileName()
{
	if (this.SourceTable != null)
	{
		return this.SourceTable.Name + "_Procedures.sql";
	}
	else
	{
		return base.GetFileName();
	}
}

// Override the OutputFile property and assign our specific settings to it.
[Category("2. Options")]
[FileDialog(FileDialogType.Save, Title="Select Output File", Filter="Query Files (*.sql)|*.sql|All Files (*.*)|*.*", DefaultExtension=".sql")]
public override string OutputFile
{
	get {return base.OutputFile;}
	set {base.OutputFile = value;}
}

protected override void OnPostRender(string result) 
{
	if (this.AutoExecuteScript)
	{
		// execute the output on the same database as the source table.
		CodeSmith.BaseTemplates.ScriptResult scriptResult = CodeSmith.BaseTemplates.ScriptUtility.ExecuteScript(this.SourceTable.Database.ConnectionString, result, new System.Data.SqlClient.SqlInfoMessageEventHandler(cn_InfoMessage)); 
		Trace.Write(scriptResult.ToString());
	}
	
	base.OnPostRender(result);
}

private void cn_InfoMessage(object sender, System.Data.SqlClient.SqlInfoMessageEventArgs e)
{
	Trace.WriteLine(e.Message);
}
#endregion

</script>