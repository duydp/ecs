﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.GC;
using Company.GC.BLL.KDT.SXXK;
using DevExpress.XtraPrinting;
using Company.GC.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using Infragistics.Excel;
using System.Threading;
using System.Diagnostics;
using SOFTECH.ECS.TQDT.GC.GC.PhanBo;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.Interface.Report
{
    public enum EWorkType
    {
        TaoBangPhanBo,
        XuatExcel
    }

    public partial class ViewPhanBoForm : BaseForm
    {
        PhanBoReportBySeven ReportSeven = new PhanBoReportBySeven();
        PhanBoReport Report = new PhanBoReport();
        public Company.GC.BLL.KDT.ToKhaiMauDich TKMD;
        DataSet dsBangPhanBo = new DataSet();
        DataSet dsForExcel = new DataSet();
        public HopDong HD;
        int soLuongSP;
        int soTable = 0;
        int tableCuoi = 0;
        int TongBang5;
        int TongBang3;
        public int SoTKNhap;
        public ViewPhanBoForm()
        {
            InitializeComponent();

            InitialBackgrounWorker();
        }

        private void PreviewForm_Load(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = Application.StartupPath;

            objDoWork = EWorkType.TaoBangPhanBo.ToString();
            btnBeginProcess_Click(null, EventArgs.Empty);
        }

        public void ViewPhanBo(BackgroundWorker bgw, DoWorkEventArgs e)
        {
            try
            {
                soLuongSP = this.TKMD.HMDCollection.Count;//Lấy danh sách số lượng SP
                this.TKMD.LoadHMDCollection();
                //Tính toán lấy số table 5 SP và số table 3 SP lưu vào 2 biến TongBang5 va TongBang3 

                soTable = (soLuongSP - 1) / 5 + 1;// Tổng số table
                tableCuoi = soLuongSP % 5;//Số lượng SP của table cuối cùng
                if (tableCuoi == 0 || tableCuoi > 3)
                {
                    //Nếu bằng 0 hoặc lớn hơn 3 thì phải chia làm 2 bảng 3
                    soTable += 1;//Tổng số table tăng lên 1
                    TongBang5 = soTable - 2;//Tổng số table 5 = tổng số table - 2
                    TongBang3 = 2;// Tổng số table 3 = 2
                }
                else
                {

                    TongBang5 = soTable - 1;//Tổng bảng 5 = Tổng số table - 1
                    TongBang3 = 1;// Tổng số bang 3 = 1
                }
                //Tạo bảng phân bổ
                //             ProcessPhanBo f = new ProcessPhanBo();
                //             f.soLuongSP = this.soLuongSP;
                //             f.soTable = this.soTable;
                //             f.tableCuoi = this.tableCuoi;
                //             f.TKMD = this.TKMD;
                //             f.TongBang3 = this.TongBang3;
                //             f.TongBang5 = this.TongBang5;
                //             f.HDID = this.HD.ID;
                //             f.ShowDialog(this);
                //             f.DoProcess(null);
                //             dsBangPhanBo = f.BangPhanBo;
                //btnExportExcel_Click(null, null);
                dsBangPhanBo = TaoBangPhanBo(bgw, e);

                this.Invoke((MethodInvoker)delegate
                {
                    for (int i = 0; i < soTable; i++)
                    {
                        cbPage.Items.Add("Trang " + (i + 1), i);
                    }

                    cbPage.SelectedIndex = 0;
                });
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); throw ex; }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private List<string> GetPhanBoToKhaiXuat(string maNPL, decimal tongNhuCau, SqlTransaction transaction)
        {
            List<string> list = new List<string>();
            string temp = "";
            string TriGiaTheoTK = "";
            int MuaVN = 1;
            //DataSet ds = PhanBoToKhaiXuat.SelectViewPhanBoToKhaiXuatOfTKMDAndMaNPL(this.TKMD.ID, maNPL);
            //DataSet ds = PhanBoToKhaiXuat.SelectViewPhanBoToKhaiXuatOfTKMDAndMaNPL(this.TKMD.ID, maNPL, this.TKMD.MaLoaiHinh, transaction);
            //minhnd
            DataSet ds = PhanBoToKhaiXuat.SelectViewPhanBoToKhaiXuatOfTKMDAndMaNPL_New(this.TKMD.ID, maNPL, this.TKMD.MaLoaiHinh, transaction);
            //DataSet ds = PhanBoToKhaiXuat.SelectViewPhanBoToKhaiXuatOfTKMDAndMaNPL(this.TKMD.ID, maNPL, this.TKMD.MaLoaiHinh, this.HD.ID, transaction);
            decimal tongPhanBo = 0;
            decimal tongTriGia = 0;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr["SoToKhaiNhap"].ToString() != "0")
                {
                    string SoTK = dr["SoToKhaiNhap"].ToString();
                    string MaLoaiHinh = dr["MaLoaiHinhNhap"].ToString();
                    if (MaLoaiHinh.Contains("V"))
                    {
                        SoTK = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(SoTK)).ToString();
                        MaLoaiHinh = MaLoaiHinh.Substring(2);
                    }
                    SoTKNhap = Convert.ToInt32(dr["SoToKhaiNhap"].ToString());
                    temp += "TK " + SoTK + "/" + MaLoaiHinh + "/" + dr["NamDangKyNhap"].ToString() + ": " + Convert.ToDecimal(dr["LuongPhanBo"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL) + "; ";
                    TriGiaTheoTK += "TK " + SoTK + "/" + MaLoaiHinh + "/" + dr["NamDangKyNhap"].ToString() + " Trị giá : " + Convert.ToDecimal(dr["TriGia"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL)+ "; ";
                    //temp += "TK " + SoTK + "/" + MaLoaiHinh + "/" + dr["NamDangKyNhap"].ToString() + " Số lượng : " + Convert.ToDecimal(dr["LuongPhanBo"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL) + " Trị giá : " + Convert.ToDecimal(dr["TriGia"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL)+ "; ";
                    tongPhanBo += Convert.ToDecimal(dr["LuongPhanBo"]);
                    tongTriGia += Convert.ToDecimal(dr["TriGia"]);
                }
            }
            TriGiaTheoTK += " Tổng trị giá : " + Convert.ToDecimal(tongTriGia.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL));
            decimal muaVN = Math.Round(tongNhuCau - tongPhanBo, GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
            if (muaVN > 0)
            {
                temp += "Mua VN: " + muaVN.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                //Cập nhật tờ khai là mua tại VN
                PhanBoToKhaiNhap.Update_MuaVN(SoTKNhap,MuaVN);
            }
            list.Add(temp);
            list.Add(tongTriGia.ToString());
            list.Add(TriGiaTheoTK);
            return list;

        }
        private void cbPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPage.SelectedIndex < TongBang5)
            {
                this.ReportSeven.ToSo = cbPage.SelectedIndex + 1;
                this.ReportSeven.First = (cbPage.SelectedIndex == 0);
                this.ReportSeven.Last = (cbPage.SelectedIndex == (dsBangPhanBo.Tables.Count - 1));
                this.ReportSeven.dt = dsBangPhanBo.Tables[cbPage.SelectedIndex];
                this.ReportSeven.BindReport(this.TKMD);
                printControl1.PrintingSystem = this.ReportSeven.PrintingSystem;
                this.ReportSeven.CreateDocument();
                //this.ReportSeven.ShowPreview();
            }
            else
            {
                this.Report.ToSo = cbPage.SelectedIndex + 1;
                this.Report.First = (cbPage.SelectedIndex == 0);
                this.Report.Last = (cbPage.SelectedIndex == (dsBangPhanBo.Tables.Count - 1));
                this.Report.dt = dsBangPhanBo.Tables[cbPage.SelectedIndex];
                this.Report.BindReport(this.TKMD);
                printControl1.PrintingSystem = this.Report.PrintingSystem;
                this.Report.CreateDocument();
                //this.Report.ShowPreview();
            }
        }

        private string GetTenHang(string maHang)
        {
            foreach (HangMauDich HMD in this.TKMD.HMDCollection)
            {
                if (HMD.MaPhu.Trim() == maHang.Trim()) return HMD.TenHang;
            }
            return "";
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            objDoWork = EWorkType.XuatExcel.ToString();
            btnBeginProcess_Click(sender, e);
        }

        private void XuatExcelTatCaTrang(BackgroundWorker bgw, DoWorkEventArgs e)
        {
            try
            {
                dsForExcel = CreatBangPhanBo(bgw, e);

                //TODO: Background - TotalProgressValue. Hungtq, 04/11/2014
                totalProgressValue += 2;

                this.TKMD.LoadHMDCollection();
                //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
                newProgressValue += 1;
                percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                SetProgressBar(percentComplete);

                Workbook wb = new Workbook();
                Worksheet ws = wb.Worksheets.Add("BangPhanBoNPL");

                WorksheetRowCollection wsrc = ws.Rows;
                WorksheetRow wsr0 = ws.Rows[0];
                WorksheetRow wsr1 = ws.Rows[1];
                WorksheetRow wsr2 = ws.Rows[2];
                if (this.TKMD.MaLoaiHinh.Contains("E"))
                    wsr0.Cells[1].Value = "BẢNG PHÂN BỔ NGUYÊN PHỤ LIỆU HÀNG XUẤT KHẨU TỜ KHAI XUẤT KHẨU SỐ " + this.TKMD.LoaiVanDon + " NGÀY " + this.TKMD.NgayDangKy.ToShortDateString();
                else
                    wsr0.Cells[1].Value = "BẢNG PHÂN BỔ NGUYÊN PHỤ LIỆU HÀNG XUẤT KHẨU TỜ KHAI XUẤT KHẨU SỐ " + this.TKMD.SoToKhai + " NGÀY " + this.TKMD.NgayDangKy.ToShortDateString();
                wsr0.Height = 1000;
                wsr0.Cells[1].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                ws.Columns[0].Width = 1200;
                ws.Columns[1].Width = 8000;
                ws.Columns[2].Width = 2500;
                ws.Columns[3].Width = 1500;
                wsr1.Cells[0].Value = "STT";
                wsr1.Cells[1].Value = "Tên NPL";
                wsr1.Cells[2].Value = "Mã NPL";
                wsr1.Cells[3].Value = "ĐVT";
                ws.MergedCellsRegions.Add(1, 0, 2, 0);
                ws.MergedCellsRegions.Add(1, 1, 2, 1);
                ws.MergedCellsRegions.Add(1, 2, 2, 2);
                ws.MergedCellsRegions.Add(1, 3, 2, 3);
                int temp = 4;
                int temp1 = 4;
                //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
                newProgressValue += 1;
                percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                SetProgressBar(percentComplete);

                //TODO: Background - TotalProgressValue. Hungtq, 04/11/2014
                totalProgressValue += this.dsForExcel.Tables.Count;

                foreach (DataTable dt in this.dsForExcel.Tables)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        if (dt.Columns["DinhMuc" + i].Caption != " ")
                        {

                            wsr1.Cells[temp].Value = GetTenHang(dt.Columns["DinhMuc" + i].Caption) + " / " + dt.Columns["DinhMuc" + i].Caption + "\nSố lượng: " + dt.Columns["LuongSD" + i].Caption;
                            ws.MergedCellsRegions.Add(1, temp, 1, temp + 2);
                            temp = temp + 3;
                            wsr2.Cells[temp1].Value = "ĐM";
                            temp1++;
                            wsr2.Cells[temp1].Value = "TLHH";
                            temp1++;
                            wsr2.Cells[temp1].Value = "NC";
                            temp1++;
                        }
                        else
                            break;
                    }

                    //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
                    newProgressValue += 1;
                    percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                    percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                    SetProgressBar(percentComplete);
                }

                wsr1.Cells[temp].Value = "Tổng NPL gồm TLHH";
                ws.MergedCellsRegions.Add(1, temp, 2, temp);
                temp++;
                wsr1.Cells[temp].Value = "Tổng trị giá theo tờ khai";
                ws.MergedCellsRegions.Add(1, temp, 2, temp);
                temp++;
                wsr1.Cells[temp].Value = "Tờ khai nhập khẩu sử dụng";
                ws.MergedCellsRegions.Add(1, temp, 2, temp);
                wsr0.CellFormat.BottomBorderStyle = CellBorderLineStyle.Medium;
                wsr1.CellFormat.WrapText = ExcelDefaultableBoolean.True;
                wsr1.CellFormat.Alignment = HorizontalCellAlignment.Center;
                wsr1.CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                wsr1.CellFormat.BottomBorderStyle = wsr1.CellFormat.LeftBorderStyle = wsr1.CellFormat.RightBorderStyle = wsr2.CellFormat.TopBorderStyle = CellBorderLineStyle.Medium;
                wsr1.Height = 900;
                wsr2.CellFormat.WrapText = ExcelDefaultableBoolean.True;
                wsr2.CellFormat.Alignment = HorizontalCellAlignment.Center;
                wsr2.CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                wsr2.CellFormat.BottomBorderStyle = wsr2.CellFormat.LeftBorderStyle = wsr2.CellFormat.RightBorderStyle = wsr2.CellFormat.TopBorderStyle = CellBorderLineStyle.Medium;
                wsr2.Height = 400;
                ws.DisplayOptions.PanesAreFrozen = true;
                ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 4;

                DataTable dtLast = this.dsForExcel.Tables[this.dsForExcel.Tables.Count - 1];

                //TODO: Background - TotalProgressValue. Hungtq, 04/11/2014
                totalProgressValue += dtLast.Rows.Count;

                for (int i = 0; i < dtLast.Rows.Count; i++)
                {
                    temp = 4;
                    WorksheetRow wsr = ws.Rows[i + 3];
                    wsr.Cells[0].Value = dtLast.Rows[i]["STT"];
                    wsr.Cells[1].Value = dtLast.Rows[i]["TenNPL"].ToString();
                    wsr.Cells[2].Value = dtLast.Rows[i]["MaNPL"].ToString();
                    wsr.Cells[3].Value = dtLast.Rows[i]["DVT"].ToString();
                    foreach (DataTable dt in this.dsForExcel.Tables)
                    {
                        for (int j = 0; j < 4; j++)
                        {
                            if (dt.Columns["DinhMuc" + j].Caption != " ")
                            {
                                wsr.Cells[temp].Value = Convert.ToDecimal(dt.Rows[i]["DinhMuc" + j]).ToString("N" + GlobalSettings.SoThapPhan.DinhMuc);
                                wsr.Cells[temp].CellFormat.Alignment = HorizontalCellAlignment.Right;
                                temp++;
                                wsr.Cells[temp].Value = Convert.ToDecimal(dt.Rows[i]["TLHH" + j]).ToString("N0");
                                wsr.Cells[temp].CellFormat.Alignment = HorizontalCellAlignment.Right;
                                temp++;
                                wsr.Cells[temp].Value = Convert.ToDecimal(dt.Rows[i]["LuongSD" + j]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                                wsr.Cells[temp].CellFormat.Alignment = HorizontalCellAlignment.Right;
                                temp++;
                            }
                            else break;

                        }
                    }

                    wsr.Cells[temp].Value = Convert.ToDecimal(dtLast.Rows[i]["TongNPLTLHH"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                    wsr.Cells[temp].CellFormat.Alignment = HorizontalCellAlignment.Right;
                    //temp++;
                    //wsr.Cells[temp].Value = dtLast.Rows[i]["TriGiaTheoTK"].ToString();
                    temp++;
                    wsr.Cells[temp].Value = Convert.ToDecimal(dtLast.Rows[i]["TongTriGia"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                    wsr.Cells[temp].CellFormat.Alignment = HorizontalCellAlignment.Right;
                    temp++;
                    wsr.Cells[temp].Value = dtLast.Rows[i]["PhanBo"].ToString();

                    //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
                    newProgressValue += 1;
                    percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                    percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                    SetProgressBar(percentComplete);
                }

                this.Invoke((MethodInvoker)delegate
                {
                    string fileName = "";
                    DialogResult rs = saveFileDialog1.ShowDialog();
                    if (rs == DialogResult.OK)
                    {
                        fileName = saveFileDialog1.FileName;
                        wb.Save(fileName);
                    }

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        Process.Start(fileName);
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Có lỗi khi xuất Excel: ", ex);
                throw ex;
            }
        }

        public DataSet CreatBangPhanBo(BackgroundWorker bgw, DoWorkEventArgs e)
        {
            if (bgw != null && bgw.CancellationPending)
                return null;

            //TODO: Background - TotalProgressValue. Hungtq, 04/11/2014
            totalProgressValue = 3;

            this.TKMD.LoadHMDCollection();
            //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
            newProgressValue += 1;
            percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
            percentComplete = (percentComplete > 100 ? 100 : percentComplete);
            SetProgressBar(percentComplete);

            int soLuongSP = this.TKMD.HMDCollection.Count;
            int soTable = 0;
            soTable = (soLuongSP - 1) / 4 + 1;
            //TODO: Background - TotalProgressValue. Hungtq, 04/11/2014
            totalProgressValue += soTable;

            DataSet dsBangPhanBoForExcel = new DataSet();
            DataTable dttemp = new DataTable();
            Company.GC.BLL.GC.DinhMuc dm1 = new Company.GC.BLL.GC.DinhMuc();

            decimal TLHH = dm1.GetTLHHOfHopDong(this.HD.ID);
            //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
            newProgressValue += 1;
            percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
            percentComplete = (percentComplete > 100 ? 100 : percentComplete);
            SetProgressBar(percentComplete);

            DataSet ds = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoToKhai(this.TKMD.ID, this.HD.ID);
            //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
            newProgressValue += 1;
            percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
            percentComplete = (percentComplete > 100 ? 100 : percentComplete);
            SetProgressBar(percentComplete);

            for (int z = 0; z < soTable; z++)
            {
                if (bgw != null && bgw.CancellationPending)
                    return null;

                dttemp = new DataTable("dtBangPhanBo" + z.ToString());
                DataColumn[] dcCol = new DataColumn[21];
                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                int t = 0;
                for (int k = z * 4; k < (z + 1) * 4; k++)
                {
                    if (bgw != null && bgw.CancellationPending)
                        return null;

                    if (k < soLuongSP)
                    {
                        HangMauDich hmd = this.TKMD.HMDCollection[k];
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "DinhMuc" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = hmd.MaPhu;
                        t++;

                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "TLHH" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = "";
                        t++;

                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "LuongSD" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = hmd.SoLuong.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
                        t++;
                    }
                    else
                    {
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "DinhMuc" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = " ";
                        t++;
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "TLHH" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = " ";
                        t++;
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "LuongSD" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = " ";
                        t++;
                    }
                }

                dcCol[4 + t] = new DataColumn();
                dcCol[4 + t].ColumnName = "TongNPLTLHH";
                dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                dcCol[4 + t].Caption = "TongNPLTLHH";
                //t++;
                //dcCol[4 + t] = new DataColumn();
                //dcCol[4 + t].ColumnName = "TriGiaTheoTK";
                //dcCol[4 + t].DataType = Type.GetType("System.String");
                //dcCol[4 + t].Caption = "TriGiaTheoTK";

                t++;
                dcCol[4 + t] = new DataColumn();
                dcCol[4 + t].ColumnName = "TongTriGia";
                dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                dcCol[4 + t].Caption = "TongTriGia";

                t++;
                dcCol[4 + t] = new DataColumn();
                dcCol[4 + t].ColumnName = "PhanBo";
                dcCol[4 + t].DataType = Type.GetType("System.String");
                dcCol[4 + t].Caption = "PhanBo";
                dttemp.Columns.AddRange(dcCol);

                int stt = 0;

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (bgw != null && bgw.CancellationPending)
                        return null;

                    try
                    {
                        DataRow drData = dttemp.NewRow();
                        drData[0] = ++stt;
                        drData[1] = dr["MaNPL"].ToString();
                        drData[2] = dr["TenNPL"].ToString();
                        //drData[3] = DonViTinh.GetName(dr["DVT_ID"].ToString());
                        drData[3] = dr["DVT"].ToString();
                        for (int n = 0; n < 4; n++)
                        {
                            if (dttemp.Columns["DinhMuc" + n].Caption != " ")
                            {
                                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                                //dm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                //dm.MaHaiQuan = GlobalSe1ttings.MA_HAI_QUAN;
                                dm.MaSanPham = dttemp.Columns["DinhMuc" + n].Caption;
                                dm.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                                dm.HopDong_ID = this.HD.ID;
                                dm.Load();
                                decimal dinhMucSD = Math.Round(dm.DinhMucSuDung, GlobalSettings.SoThapPhan.DinhMuc, MidpointRounding.AwayFromZero);
                                drData["DinhMuc" + n] = dinhMucSD;
                                drData["TLHH" + n] = dm.TyLeHaoHut;
                                drData["LuongSD" + n] = Math.Round(Convert.ToDecimal(dttemp.Columns["LuongSD" + n].Caption) * (dinhMucSD * (1 + dm.TyLeHaoHut / 100)), GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
                            }
                        }

                        dttemp.Rows.Add(drData);
                    }
                    catch { }
                }

                dsBangPhanBoForExcel.Tables.Add(dttemp);

                //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
                newProgressValue += 1;
                percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                SetProgressBar(percentComplete);
            }

            //TODO: Background - TotalProgressValue. Hungtq, 04/11/2014
            totalProgressValue += 1;
            //decimal tongNPLTLHH = 0;
            if (dsBangPhanBoForExcel.Tables.Count > 0)
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        for (int t = 0; t < dsBangPhanBoForExcel.Tables[0].Rows.Count; t++)
                        {
                            decimal tongTungNPL = 0;
                            for (int i = 0; i < dsBangPhanBoForExcel.Tables.Count; i++)
                            {
                                try
                                {
                                    DataRow dr = dsBangPhanBoForExcel.Tables[i].Rows[t];
                                    for (int n = 0; n < 4; n++)
                                    {
                                        if (dr["LuongSD" + n] != DBNull.Value)
                                            tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                    }
                                }
                                catch { }
                            }
                            try
                            {
                                dsBangPhanBoForExcel.Tables[dsBangPhanBoForExcel.Tables.Count - 1].Rows[t]["TongNPLTLHH"] = tongTungNPL;
                                List<string> list = new List<string>();
                                list = GetPhanBoToKhaiXuat(dsBangPhanBoForExcel.Tables[dsBangPhanBoForExcel.Tables.Count - 1].Rows[t]["MaNPL"].ToString(), tongTungNPL, transaction);
                                dsBangPhanBoForExcel.Tables[dsBangPhanBoForExcel.Tables.Count - 1].Rows[t]["PhanBo"] = list[0].ToString();
                                dsBangPhanBoForExcel.Tables[dsBangPhanBoForExcel.Tables.Count - 1].Rows[t]["TongTriGia"] = Convert.ToDecimal(list[1].ToString());
                                //dsBangPhanBoForExcel.Tables[dsBangPhanBoForExcel.Tables.Count - 1].Rows[t]["TriGiaTheoTK"] = list[2].ToString();
                            }
                            catch { }
                        }
                    }
                }
            }
            //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
            newProgressValue += 1;
            percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
            percentComplete = (percentComplete > 100 ? 100 : percentComplete);
            SetProgressBar(percentComplete);

            return dsBangPhanBoForExcel;
        }

        public DataSet TaoBangPhanBo(BackgroundWorker bgw, DoWorkEventArgs e)
        {
            if (bgw != null && bgw.CancellationPending)
                return null;

            //TODO: Background - TotalProgressValue. Hungtq, 04/11/2014
            totalProgressValue = 3 + TongBang5 + TongBang3;

            DataSet dsTemp = new DataSet();
            DataTable dttempByFive = new DataTable();
            DataTable dttempByThree = new DataTable();

            DataSet ds = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoToKhai(this.TKMD.ID, this.HD.ID);
            //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
            newProgressValue += 1;
            percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
            percentComplete = (percentComplete > 100 ? 100 : percentComplete);
            SetProgressBar(percentComplete);

            #region TaoBang5

            for (int BangSo = 0; BangSo < TongBang5; BangSo++)
            {
                if (bgw != null && bgw.CancellationPending)
                    return null;

                //Tạo cấu trúc bảng 5
                dttempByFive = new DataTable("dtBangPhanBoSeven" + BangSo.ToString());
                DataColumn[] dcCol = new DataColumn[19];

                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                int t = 4;
                for (int k = BangSo * 5; k < (BangSo + 1) * 5; k++)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[k];

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "DinhMuc" + k % 5;
                    dcCol[t].DataType = Type.GetType("System.Decimal");
                    dcCol[t].Caption = hmd.MaPhu;
                    t++;

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "TLHH" + k % 5;
                    dcCol[t].DataType = Type.GetType("System.Decimal");
                    dcCol[t].Caption = "";
                    t++;

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "LuongSD" + k % 5;
                    dcCol[t].DataType = Type.GetType("System.Decimal");
                    dcCol[t].Caption = hmd.SoLuong.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
                    t++;
                }
                dttempByFive.Columns.AddRange(dcCol);

                int stt = 0;
                //Lấy danh sách npl phân bổ cửa tờ khai xuất TKMD đưa vào dataset ds
                //  DataSet ds = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoToKhai(this.TKMD.ID, this.HD.ID);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    try
                    {
                        //Add từng dòng dữ liệu theo cấu trúc như trên
                        DataRow drData = dttempByFive.NewRow();
                        drData[0] = ++stt;
                        drData[1] = dr["MaNPL"].ToString();
                        drData[2] = dr["TenNPL"].ToString();
                        //drData[3] = DonViTinh.GetName(dr["DVT_ID"].ToString());
                        drData[3] = dr["DVT"].ToString();
                        for (int n = 0; n < 5; n++)
                        {
                            if (dttempByFive.Columns["DinhMuc" + n].Caption != " ")
                            {
                                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                                dm.MaSanPham = dttempByFive.Columns["DinhMuc" + n].Caption;
                                dm.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                                dm.HopDong_ID = this.HD.ID;
                                dm.Load();
                                decimal dinhMucSD = Math.Round(dm.DinhMucSuDung, GlobalSettings.SoThapPhan.DinhMuc, MidpointRounding.AwayFromZero);
                                drData["DinhMuc" + n] = dinhMucSD;
                                drData["TLHH" + n] = dm.TyLeHaoHut;
                                drData["LuongSD" + n] = Math.Round(Convert.ToDecimal(dttempByFive.Columns["LuongSD" + n].Caption) * (dinhMucSD * (1 + dm.TyLeHaoHut / 100)), GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
                            }

                        }
                        dttempByFive.Rows.Add(drData);
                    }
                    catch { }
                }

                dsTemp.Tables.Add(dttempByFive);

                //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
                newProgressValue += 1;
                percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                SetProgressBar(percentComplete);
            }

            #endregion

            #region TaoBang3
            //Tương tự như bảng 5
            Company.GC.BLL.GC.DinhMuc DMtemp = new Company.GC.BLL.GC.DinhMuc();
            decimal TLHH = DMtemp.GetTLHHOfHopDong(this.HD.ID);
            //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
            newProgressValue += 1;
            percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
            percentComplete = (percentComplete > 100 ? 100 : percentComplete);
            SetProgressBar(percentComplete);

            for (int BangSo = 0; BangSo < TongBang3; BangSo++)
            {
                if (bgw != null && bgw.CancellationPending)
                    return null;

                dttempByThree = new DataTable("dtBangPhanBoFour" + BangSo.ToString());
                DataColumn[] dcCol = new DataColumn[18];
                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                int t = 4;
                for (int k = (BangSo * 3) + (5 * TongBang5); k < (BangSo + 1) * 3 + (5 * TongBang5); k++)
                {

                    int phanchia = k - (5 * TongBang5);
                    if (k < soLuongSP)
                    {
                        HangMauDich hmd = this.TKMD.HMDCollection[k];

                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "DinhMuc" + phanchia % 3;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = hmd.MaPhu;
                        t++;

                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "TLHH" + phanchia % 3;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = "";
                        t++;

                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "LuongSD" + phanchia % 3;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = hmd.SoLuong.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
                        t++;
                    }
                    else
                    {
                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "DinhMuc" + phanchia % 3;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = " ";
                        t++;

                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "TLHH" + phanchia % 3;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = " ";
                        t++;

                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "LuongSD" + phanchia % 3;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = " ";
                        t++;

                    }
                }


                dcCol[t] = new DataColumn();
                dcCol[t].ColumnName = "TongNPLTLHH";
                dcCol[t].DataType = Type.GetType("System.Decimal");
                dcCol[t].Caption = "TongNPLTLHH";

                t++;
                dcCol[t] = new DataColumn();
                dcCol[t].ColumnName = "TongTriGia";
                dcCol[t].DataType = Type.GetType("System.Decimal");
                dcCol[t].Caption = "TongTriGia";

                //t++;
                //dcCol[t] = new DataColumn();
                //dcCol[t].ColumnName = "TriGiaTheoTK";
                //dcCol[t].DataType = Type.GetType("System.String");
                //dcCol[t].Caption = "TriGiaTheoTK";

                t++;
                dcCol[t] = new DataColumn();
                dcCol[t].ColumnName = "PhanBo";
                dcCol[t].DataType = Type.GetType("System.String");
                dcCol[t].Caption = "PhanBo";


                dttempByThree.Columns.AddRange(dcCol);

                int stt = 0;
                //    DataSet ds = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoToKhai(this.TKMD.ID, this.HD.ID);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    try
                    {
                        DataRow drData = dttempByThree.NewRow();
                        drData[0] = ++stt;
                        drData[1] = dr["MaNPL"].ToString();
                        drData[2] = dr["TenNPL"].ToString();
                        //drData[3] = DonViTinh.GetName(dr["DVT_ID"].ToString());
                        drData[3] = dr["DVT"].ToString();
                        for (int n = 0; n < 3; n++)
                        {
                            if (dttempByThree.Columns["DinhMuc" + n].Caption != " ")
                            {
                                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                                dm.MaSanPham = dttempByThree.Columns["DinhMuc" + n].Caption;
                                dm.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                                dm.HopDong_ID = this.HD.ID;
                                dm.Load();

                                decimal dinhMucSD = Math.Round(dm.DinhMucSuDung, GlobalSettings.SoThapPhan.DinhMuc, MidpointRounding.AwayFromZero);
                                drData["DinhMuc" + n] = dinhMucSD;
                                drData["TLHH" + n] = dm.TyLeHaoHut;
                                drData["LuongSD" + n] = Math.Round(Convert.ToDecimal(dttempByThree.Columns["LuongSD" + n].Caption) * (dinhMucSD * (1 + dm.TyLeHaoHut / 100)), GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
                            }
                        }

                        dttempByThree.Rows.Add(drData);
                    }
                    catch { }
                }

                dsTemp.Tables.Add(dttempByThree);

                //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
                newProgressValue += 1;
                percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                SetProgressBar(percentComplete);
            }

            //decimal tongNPLTLHH = 0;

            //Add thêm dữ liệu 2 cột TongNPLTLHH va PhanBo vào table cuối cùng
            if (soTable > 0)
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        for (int t = 0; t < dsTemp.Tables[0].Rows.Count; t++)
                        
                       {
                            decimal tongTungNPL = 0;

                            if (tableCuoi == 0 || tableCuoi > 3)
                            {
                                for (int i = 0; i < soTable - 2; i++)
                                {
                                    try
                                    {
                                        DataRow dr = dsTemp.Tables[i].Rows[t];
                                        for (int n = 0; n < 5; n++)
                                        {
                                            if (dr["LuongSD" + n] != DBNull.Value)
                                                tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                        }
                                    }
                                    catch { }
                                }

                                for (int i = soTable - 2; i < soTable; i++)
                                {
                                    try
                                    {
                                        DataRow dr = dsTemp.Tables[i].Rows[t];
                                        for (int n = 0; n < 3; n++)
                                        {
                                            if (dr["LuongSD" + n] != DBNull.Value)
                                                tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                        }
                                    }
                                    catch { }
                                }

                            }
                            else
                            {
                                for (int i = 0; i < soTable - 1; i++)
                                {
                                    try
                                    {
                                        DataRow dr = dsTemp.Tables[i].Rows[t];
                                        for (int n = 0; n < 5; n++)
                                        {
                                            if (dr["LuongSD" + n] != DBNull.Value)
                                                tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                        }
                                    }
                                    catch { }
                                }

                                for (int i = soTable - 1; i < soTable; i++)
                                {
                                    try
                                    {
                                        DataRow dr = dsTemp.Tables[i].Rows[t];
                                        for (int n = 0; n < 3; n++)
                                        {
                                            if (dr["LuongSD" + n] != DBNull.Value)
                                                tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                        }
                                    }
                                    catch { }
                                }

                            }

                            try
                            {

                                dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["TongNPLTLHH"] = tongTungNPL;
                                List<string> list = new List<string>();
                                list = GetPhanBoToKhaiXuat(dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["MaNPL"].ToString(), tongTungNPL, transaction);
                                dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["PhanBo"] = list[0].ToString();
                                dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["TongTriGia"] = Convert.ToDecimal(list[1].ToString());
                                //dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["TriGiaTheoTK"] = list[2].ToString();
                                //Thread.Sleep(5000);
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
                }

                //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 04/11/2014
                newProgressValue += 1;
                percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                SetProgressBar(percentComplete);
            }

            #endregion

            return dsTemp;
        }

        private void btnXuatTrangHienTai_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        #region Background Worker

        private BackgroundWorker bgwProgress = new BackgroundWorker();
        private int totalProgressValue = 0, newProgressValue = 0, percentComplete = 0;
        private System.Windows.Forms.Timer timeElapsed = new System.Windows.Forms.Timer();
        private double secondElapsed = 0;
        private string secondElapsedString = string.Empty;
        private string objDoWork = string.Empty;
        private System.Windows.Forms.ToolTip toolTipProgressBar = new ToolTip();

        //ProgressBar
        public void ResetProgressBar(int minPercent, int maxPercent)
        {
            if (progressBar1 == null)
                return;

            MethodInvoker mi = new MethodInvoker(() => progressBar1.Minimum = minPercent);
            if (progressBar1.InvokeRequired)
            {
                progressBar1.Invoke(mi);
            }
            else
            {
                mi.Invoke();
            }

            mi = new MethodInvoker(() => progressBar1.Maximum = maxPercent);
            if (progressBar1.InvokeRequired)
            {
                progressBar1.Invoke(mi);
            }
            else
            {
                mi.Invoke();
            }

            SetProgressBar(0);
        }
        public int GetProcessBarMaxValue()
        {
            return progressBar1.Maximum;
        }
        public void SetProgressBar(int percent)
        {
            if (progressBar1 == null)
                return;

            MethodInvoker mi = new MethodInvoker(() => progressBar1.Value = percent);
            if (progressBar1.InvokeRequired)
            {
                progressBar1.Invoke(mi);
            }
            else
            {
                mi.Invoke();
            }

        }
        private void progressBar_MouseMove(object sender, MouseEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                toolTipProgressBar.SetToolTip(progressBar1, string.Format("{0} %", percentComplete.ToString("#0")));
            });
        }

        //Text
        delegate void SetLogTextCallback(string text);
        private void SetLogText(string text)
        {
            //if (txtLog.InvokeRequired)
            //{
            //    SetLogTextCallback d = new SetLogTextCallback(SetLogText);
            //    Invoke(d, new object[] { text });
            //}
            //else
            //{
            //    txtLog.AppendText("\r\n" + System.DateTime.Now + ":\r\n");
            //    txtLog.AppendText(text + "\r\n");
            //}
        }

        private void InitialBackgrounWorker()
        {
            newProgressValue = percentComplete = totalProgressValue = 0;
            ResetProgressBar(0, 100);

            bgwProgress = new BackgroundWorker();
            bgwProgress.WorkerReportsProgress = true;
            bgwProgress.WorkerSupportsCancellation = true;
            bgwProgress.DoWork += new DoWorkEventHandler(bgwProgress_DoWork);
            bgwProgress.ProgressChanged += new ProgressChangedEventHandler(bgwProgress_ProgressChanged);
            bgwProgress.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwProgress_RunWorkerCompleted);

            //btnCancel.Click += btnCancel_Click;
            progressBar1.MouseMove += progressBar_MouseMove;
        }

        void timeElapsed_Tick(object sender, EventArgs e)
        {
            try
            {
                secondElapsed += 1;

                secondElapsedString = string.Format("{0}:{1}:{2}", TimeSpan.FromSeconds(secondElapsed).Hours.ToString("00"), TimeSpan.FromSeconds(secondElapsed).Minutes.ToString("00"), TimeSpan.FromSeconds(secondElapsed).Seconds.ToString("00"));
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex); SetLogText(ex.Message);
            }
        }

        private void btnBeginProcess_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.bgwProgress.IsBusy)
                {
                    btnExportExcel.Enabled = false;
                    btnXuatTrangHienTai.Enabled = false;
                    btnPrint.Enabled = false;

                    //btnCancel.Enabled = true;

                    bgwProgress.CancelAsync();
                }
                else
                {
                    newProgressValue = percentComplete = totalProgressValue = 0;
                    ResetProgressBar(0, 100);

                    secondElapsed = 0;
                    timeElapsed.Tick += new EventHandler(timeElapsed_Tick);
                    timeElapsed.Interval = 1000;
                    timeElapsed.Enabled = true;

                    btnExportExcel.Enabled = false;
                    btnXuatTrangHienTai.Enabled = false;
                    btnPrint.Enabled = false;

                    //btnCancel.Enabled = true;
                    bgwProgress.RunWorkerAsync(objDoWork);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex); SetLogText(ex.Message);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                bgwProgress.CancelAsync();

                timeElapsed.Enabled = false;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); SetLogText(ex.Message); }
        }

        private void bgwProgress_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bgw = sender as BackgroundWorker;
            if (bgw != null && bgw.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            string val = e.Argument.ToString();

            if (val != "" && val == EWorkType.XuatExcel.ToString())
                XuatExcelTatCaTrang(bgw, e);
            else if (val != "" && val == EWorkType.TaoBangPhanBo.ToString())
                ViewPhanBo(bgw, e);
        }

        private void bgwProgress_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage <= GetProcessBarMaxValue())
            {
                SetProgressBar(e.ProgressPercentage);
            }
        }

        /// <summary>
        /// Thực hiện sau khi load dữ liệu làm thêm theo quyền chấm công
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bgwProgress_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Logger.LocalLogger.Instance().WriteMessage(e.Error);
                SetLogText(e.Error.Message);
                ShowMessage(e.Error.Message, false);
            }

            // Check to see if the background process was cancelled.
            if (e.Cancelled)
            {

            }
            else
            {
                //if (objDoWork == EWorkType.XoaPhanBo.ToString()) { }
            }

            btnExportExcel.Enabled = true;
            btnXuatTrangHienTai.Enabled = true;
            btnPrint.Enabled = true;

            timeElapsed.Enabled = false;

            this.Cursor = Cursors.Default;
        }

        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (bgwProgress.IsBusy) bgwProgress.CancelAsync();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); SetLogText(ex.Message); }
            timeElapsed.Enabled = false;
        }

        #endregion

    }
}