﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using iTextSharp.text;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.X509;
using System.IO.Packaging;
using System.Xml;
using System.Security;
using System.Diagnostics;
using Company.KDT.SHARE.QuanLyChungTu.Properties;
using iTextSharp.text.pdf;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public class SoftechSignLibrary
    {
        public static IntPtr parentHandler;
        public static string Tittle;

        public static int trang;
        private static CmsSigner signer;
        public static Image signImage;
        public static int signRender;
        public static int signWidth;
        public static int signHeight;
        public static int LeDoc;
        public static int LeNgang;
        private static readonly string SignatureID = "idPackageSignature";
        private static readonly string ManifestHashAlgorithm = "http://www.w3.org/2000/09/xmldsig#sha1";
        private static readonly string OfficeObjectID = "idOfficeObject";
        private static readonly string RT_OfficeDocument = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument";

        public static bool CheckExitsSignature(string source)
        {
            try
            {
                PdfReader reader = new PdfReader(source);
                List<string> blanks = reader.AcroFields.GetSignatureNames();
                if (blanks.Count == 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }
        public static string GetFileProcessName(string filePath)
        {
            Process[] procs = Process.GetProcesses();
            string fileName = Path.GetFileName(filePath);

            foreach (Process proc in procs)
            {
                if (proc.MainWindowHandle != new IntPtr(0) && !proc.HasExited)
                {
                    ProcessModule[] arr = new ProcessModule[proc.Modules.Count];

                    foreach (ProcessModule pm in proc.Modules)
                    {
                        if (pm.ModuleName == fileName)
                            return proc.ProcessName;
                    }
                }
            }
            return null;
        }
        private void KillProcess(string processName)
        {
            foreach (var process in Process.GetProcessesByName(processName))
            {
                process.Kill();
            }
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
 public static bool SoftechSign(X509Certificate2 card, byte[] InputFile, ref byte[] Output)
{
    try
    {
        if (card == null)
        {
            return false;
        }
        PdfSignatureAppearance signatureAppearance = null;
        PdfDictionary update = null;
        Dictionary<PdfName, int> exclusionSizes = new Dictionary<PdfName, int>();
        PdfStamper stamper = null;
        string path = "temp.pdf";
        try
        {
            X509CertificateParser parser = new X509CertificateParser();
            Org.BouncyCastle.X509.X509Certificate[] certChain = new Org.BouncyCastle.X509.X509Certificate[] { parser.ReadCertificate(card.RawData) };
            PdfReader reader = new PdfReader(InputFile);
            signatureAppearance = PdfStamper.CreateSignature(reader, new FileStream(path, FileMode.Create), '\0', null, true).SignatureAppearance;
            signatureAppearance.SignDate = DateTime.Now;
            Font font = new Font(BaseFont.CreateFont(Application.StartupPath + @"\times.ttf", "Identity-H", false));
            font.SetColor(0xff, 0, 0);
            signatureAppearance.Layer2Font = font;
            signatureAppearance.Layer2Text = "Ký bởi: " + PdfPKCS7.GetSubjectFields(certChain[0]).GetField("CN") + "\nNgày ký: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            float width = reader.GetPageSize(1).Width;
            float height = reader.GetPageSize(1).Height;
            float yPos = reader.GetPageSize(1).Height - 50;
            float xPos = 70.0f;
            signatureAppearance.SetVisibleSignature(new iTextSharp.text.Rectangle(xPos, yPos,width - 50, yPos + 50), 1, null);
            //signatureAppearance.SetVisibleSignature(new iTextSharp.text.Rectangle(220, 150, 446, 224), 1, null);
            //signatureAppearance.SetVisibleSignature(new Rectangle(400f, 20f, 600f, 0f), 1, null);
            signatureAppearance.SetCrypto(null, certChain, null, PdfSignatureAppearance.WINCER_SIGNED);
            PdfSignature signature = new PdfSignature(PdfName.ADOBE_PPKMS, PdfName.ADBE_PKCS7_SHA1)
            {
                Date = new PdfDate(signatureAppearance.SignDate),
                Name = ""
            };
            if (signatureAppearance.Reason != null)
            {
                signature.Reason = signatureAppearance.Reason;
            }
            if (signatureAppearance.Location != null)
            {
                signature.Location = signatureAppearance.Location;
            }
            signatureAppearance.CryptoDictionary = signature;
            signature = null;
            int num = 0xfa0;
            exclusionSizes[PdfName.CONTENTS] = (num * 2) + 2;
            signatureAppearance.PreClose(exclusionSizes);
            HashAlgorithm algorithm = new SHA1CryptoServiceProvider();
            Stream rangeStream = signatureAppearance.RangeStream;
            int inputCount = 0;
            byte[] buffer2 = new byte[0x2000];
            while ((inputCount = rangeStream.Read(buffer2, 0, 0x2000)) > 0)
            {
                algorithm.TransformBlock(buffer2, 0, inputCount, buffer2, 0);
            }
            rangeStream.Close();
            algorithm.TransformFinalBlock(buffer2, 0, 0);
            buffer2 = null;
            byte[] sourceArray = SignPass(algorithm.Hash, card, false);
            if (sourceArray != null)
            {
                byte[] destinationArray = new byte[num];
                update = new PdfDictionary();
                Array.Copy(sourceArray, 0, destinationArray, 0, sourceArray.Length);
                update.Put(PdfName.CONTENTS, new PdfString(destinationArray).SetHexWriting(true));
                signatureAppearance.Close(update);
                FileStream stream2 = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Read);
                byte[] buffer4 = new byte[stream2.Length];
                stream2.Read(buffer4, 0, (int)stream2.Length);
                stream2.Close();
                Output = buffer4;
                return true;
            }
            stamper = null;
            signatureAppearance = null;
            algorithm = null;
            return false;
        }
        catch (Exception exception)
        {
            Logger.LocalLogger.Instance().WriteMessage(exception);
            string str2 = exception.ToString();
            stamper = null;
            signatureAppearance = null;
            return false;
        }
    }
    catch (Exception ex)
    {
        Logger.LocalLogger.Instance().WriteMessage(ex);
        return false;
    }    
}
        public static bool SoftechSign(X509Certificate2 card, string InputFile, string OutputFile)
        {
            try
            {
                if (card == null)
                {
                    return false;
                }
                PdfSignatureAppearance signatureAppearance = null;
                PdfDictionary update = null;
                Dictionary<PdfName, int> exclusionSizes = new Dictionary<PdfName, int>();
                PdfStamper stamper = null;
                string FileName = Path.GetFileName(InputFile);
                string path = OutputFile + "\\" + FileName.Replace(".pdf", "_Signed.pdf");
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
                try
                {
                    X509CertificateParser parser = new X509CertificateParser();
                    Org.BouncyCastle.X509.X509Certificate[] certChain = new Org.BouncyCastle.X509.X509Certificate[] { parser.ReadCertificate(card.RawData) };
                    PdfReader reader = new PdfReader(InputFile);
                    signatureAppearance = PdfStamper.CreateSignature(reader, new FileStream(path, FileMode.Create), '\0', null, true).SignatureAppearance;
                    signatureAppearance.SignDate = DateTime.Now;
                    Font font = new Font(BaseFont.CreateFont(Application.StartupPath + @"\times.ttf", "Identity-H", false));
                    font.SetColor(0xff, 0, 0);
                    signatureAppearance.Layer2Font = font;
                    signatureAppearance.Layer2Text = "Ký bởi: " + PdfPKCS7.GetSubjectFields(certChain[0]).GetField("CN") + "\nNgày ký: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    float width = reader.GetPageSize(1).Width;
                    float height = reader.GetPageSize(1).Height;
                    float yPos = reader.GetPageSize(1).Height - 50;
                    float xPos = 70.0f;
                    signatureAppearance.SetVisibleSignature(new iTextSharp.text.Rectangle(xPos, yPos, width - 50, yPos + 50), 1, null);

                    //signatureAppearance.SetVisibleSignature(new iTextSharp.text.Rectangle(220, 150, 446, 224), 1, null);
                    //signatureAppearance.SetVisibleSignature(new Rectangle(400f, 20f, 600f, 0f), 1, null);
                    signatureAppearance.SetCrypto(null, certChain, null, PdfSignatureAppearance.WINCER_SIGNED);
                    PdfSignature signature = new PdfSignature(PdfName.ADOBE_PPKMS, PdfName.ADBE_PKCS7_SHA1)
                    {
                        Date = new PdfDate(signatureAppearance.SignDate),
                        Name = ""
                    };
                    if (signatureAppearance.Reason != null)
                    {
                        signature.Reason = signatureAppearance.Reason;
                    }
                    if (signatureAppearance.Location != null)
                    {
                        signature.Location = signatureAppearance.Location;
                    }
                    signatureAppearance.CryptoDictionary = signature;
                    signature = null;
                    int num = 0xfa0;
                    exclusionSizes[PdfName.CONTENTS] = (num * 2) + 2;
                    signatureAppearance.PreClose(exclusionSizes);
                    HashAlgorithm algorithm = new SHA1CryptoServiceProvider();
                    Stream rangeStream = signatureAppearance.RangeStream;
                    int inputCount = 0;
                    byte[] buffer2 = new byte[0x2000];
                    while ((inputCount = rangeStream.Read(buffer2, 0, 0x2000)) > 0)
                    {
                        algorithm.TransformBlock(buffer2, 0, inputCount, buffer2, 0);
                    }
                    rangeStream.Close();
                    algorithm.TransformFinalBlock(buffer2, 0, 0);
                    buffer2 = null;
                    byte[] sourceArray = SignPass(algorithm.Hash, card, false);
                    if (sourceArray != null)
                    {
                        byte[] destinationArray = new byte[num];
                        update = new PdfDictionary();
                        Array.Copy(sourceArray, 0, destinationArray, 0, sourceArray.Length);
                        update.Put(PdfName.CONTENTS, new PdfString(destinationArray).SetHexWriting(true));
                        signatureAppearance.Close(update);
                        //File.Copy(Application.StartupPath + @"\\Temp", InputFile, true);
                        return true;
                    }
                    stamper = null;
                    signatureAppearance = null;
                    algorithm = null;
                    signature.Clear();
                    return false;
                }
                catch (Exception exception)
                {
                    Logger.LocalLogger.Instance().WriteMessage(exception);
                    string str2 = exception.ToString();
                    stamper = null;
                    signatureAppearance = null;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }
        public static byte[] SignPass(byte[] msg, X509Certificate2 X509Cert2, bool detached)
        {
            try
            {
                byte[] buffer2;
                ContentInfo contentInfo = new ContentInfo(msg);
                SignedCms cms = new SignedCms(contentInfo, detached);
                try
                {
                    bool flag = true;
                    signer = new CmsSigner(X509Cert2);
                    signer.IncludeOption = X509IncludeOption.EndCertOnly;
                    flag = true;
                    cms.ComputeSignature(signer, false);
                    buffer2 = cms.Encode();
                }
                catch (ArgumentNullException)
                {
                    buffer2 = null;
                }
                catch (CryptographicException)
                {
                    throw;
                }
                catch (InvalidOperationException)
                {
                    buffer2 = null;
                }
                catch (Exception)
                {
                    contentInfo = null;
                    cms = null;
                    buffer2 = null;
                }
                finally
                {
                    contentInfo = null;
                    cms = null;
                }
                return buffer2;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private static void AddSignableItems(PackageRelationship relationship, List<Uri> partsToSign, List<PackageRelationshipSelector> relationshipsToSign)
        {
            try
            {
                PackageRelationshipSelector item = new PackageRelationshipSelector(relationship.SourceUri, PackageRelationshipSelectorType.Id, relationship.Id);
                relationshipsToSign.Add(item);
                if (relationship.TargetMode == TargetMode.Internal)
                {
                    PackagePart part = relationship.Package.GetPart(PackUriHelper.ResolvePartUri(relationship.SourceUri, relationship.TargetUri));
                    if (!partsToSign.Contains(part.Uri))
                    {
                        partsToSign.Add(part.Uri);
                        foreach (PackageRelationship relationship2 in part.GetRelationships())
                        {
                            AddSignableItems(relationship2, partsToSign, relationshipsToSign);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private static System.Security.Cryptography.Xml.DataObject CreateOfficeObject(string signatureID, string manifestHashAlgorithm)
        {
            try
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(string.Format(Resources.OfficeObject, signatureID, manifestHashAlgorithm));
                System.Security.Cryptography.Xml.DataObject obj2 = new System.Security.Cryptography.Xml.DataObject();
                obj2.LoadXml(document.DocumentElement);
                obj2.Id = OfficeObjectID;
                return obj2;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private static void SignPackage(Package package, X509Certificate2 certificate)
        {
            try
            {
                List<Uri> partsToSign = new List<Uri>();
                List<PackageRelationshipSelector> relationshipsToSign = new List<PackageRelationshipSelector>();
                List<Uri> list3 = new List<Uri>();
                foreach (PackageRelationship relationship in package.GetRelationshipsByType(RT_OfficeDocument))
                {
                    AddSignableItems(relationship, partsToSign, relationshipsToSign);
                }
                PackageDigitalSignatureManager manager = new PackageDigitalSignatureManager(package)
                {
                    CertificateOption = CertificateEmbeddingOption.InSignaturePart
                };
                string signatureID = SignatureID;
                string manifestHashAlgorithm = ManifestHashAlgorithm;
                System.Security.Cryptography.Xml.DataObject obj2 = CreateOfficeObject(signatureID, manifestHashAlgorithm);
                System.Security.Cryptography.Xml.Reference reference = new System.Security.Cryptography.Xml.Reference("#" + OfficeObjectID);
                manager.Sign(partsToSign, certificate, relationshipsToSign, signatureID, new System.Security.Cryptography.Xml.DataObject[] { obj2 }, new System.Security.Cryptography.Xml.Reference[] { reference });
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        public static bool SignOfficeDocument(string path, X509Certificate2 certificate)
        {
            try
            {
                using (Package package = Package.Open(path))
                {
                    SignPackage(package, certificate);
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }
        public static bool SignOfficeDocument(byte[] InputFile, X509Certificate2 certificate, ref byte[] Output)
        {
            try
            {
                File.WriteAllBytes(Application.StartupPath + @"\temp.xlsx", InputFile);
                SignOfficeDocument(Application.StartupPath + @"\temp.xlsx", certificate);
                Output = File.ReadAllBytes(Application.StartupPath + @"\temp.xlsx");
                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }

        public static bool isSignedExcel(string filePath)
        {
            bool isSigned;
            try
            {
                using (Package package = Package.Open(filePath))
                {
                    PackageDigitalSignatureManager manager = new PackageDigitalSignatureManager(package);
                    isSigned = manager.IsSigned;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                isSigned = false;
            }
            return isSigned;
        }
        public static bool isVerifySignedExcel(string filePath)
        {
            bool flag;
            try
            {
                using (Package package = Package.Open(filePath))
                {
                    PackageDigitalSignatureManager manager = new PackageDigitalSignatureManager(package);
                    if (manager.IsSigned)
                    {
                        return (manager.Signatures[0].Verify() == VerifyResult.Success);
                    }
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                flag = false;
            }
            return flag;
        }
        public static X509Certificate2 FindCertificate(StoreLocation location, StoreName name, X509FindType findType, string findValue)
        {
            X509Certificate2 certificate;
            X509Store store = new X509Store(name, location);
            try
            {
                store.Open(OpenFlags.ReadOnly);
                X509Certificate2Collection certificates = store.Certificates.Find(findType, findValue, true);
                if (certificates.Count > 0)
                {
                    return certificates[0];
                }
                certificate = null;
            }
            finally
            {
                store.Close();
            }
            return certificate;
        }
        public static X509Certificate2 GetCertificate(IntPtr parentFormHandler)
        {
            parentHandler = parentFormHandler;
            return GetCertificate(parentFormHandler, Tittle, " Hãy chọn một chữ ký số ");
        }
        public static X509Certificate2 GetCertificate(IntPtr parentFormHandler, string tittle, string message)
        {
            parentHandler = parentFormHandler;
            Tittle = tittle;
            StringBuilder builder = new StringBuilder(0x400);
            X509Store store = new X509Store(StoreName.TrustedPeople, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection certificates = store.Certificates;
            X509Store store2 = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store2.Open(OpenFlags.ReadOnly);
            certificates.AddRange(store2.Certificates);
            X509Certificate2 current = null;
            X509Certificate2Collection certificates2 = X509Certificate2UI.SelectFromCollection(certificates, tittle, " Hãy chọn một chữ ký số ", X509SelectionFlag.SingleSelection, parentFormHandler);
            if (certificates2.Count > 0)
            {
                X509Certificate2Enumerator enumerator = certificates2.GetEnumerator();
                enumerator.MoveNext();
                current = enumerator.Current;
            }
            store.Close();
            store2.Close();
            return current;
        }
    }
}
