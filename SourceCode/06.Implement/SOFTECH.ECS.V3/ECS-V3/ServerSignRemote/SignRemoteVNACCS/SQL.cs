﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace SignRemoteVNACCS
{
   public class SQL
    {
        static SqlConnection sqlConnection = new SqlConnection();
        public static void InitializeServer(string sqlConnectionString)
        {
            // To Connect to our SQL Server - we Can use the Connection from the System.Data.SqlClient Namespace.
            sqlConnection = new SqlConnection(sqlConnectionString);

            //build a "serverConnection" with the information of the "sqlConnection"
            //Microsoft.SqlServer.Management.Common.ServerConnection serverConnection = new Microsoft.SqlServer.Management.Common.ServerConnection(sqlConnection);

            //The "serverConnection is used in the ctor of the Server.
            //server = new Server(serverConnection);
        }
        public static List<string> ListDatabases()
        {
            List<string> listData = new List<string>();
            DataTable dtSource = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = "SELECT name FROM sys.sysdatabases";
            adapter.SelectCommand = new SqlCommand(query, sqlConnection);
            adapter.Fill(dtSource);
            foreach (DataRow dr in dtSource.Rows)
            {
                listData.Add(dr[0].ToString());
            }
            return listData;
        }
    }
}
