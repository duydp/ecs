using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.IO;

namespace Company.Interface.Controls
{
    class ScreenShoot
    {
        public static void CaptureImage(string BugInformation)
        {
            WSBugtracker.WSBugTracker objWS = new Company.Interface.WSBugtracker.WSBugTracker();
            MemoryStream st = new MemoryStream();
            Rectangle bounds = Screen.GetBounds(Screen.GetBounds(Point.Empty));
            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
                }
                
                bitmap.Save(st, ImageFormat.Jpeg);
                objWS.SendCompleted += new Company.Interface.WSBugtracker.SendCompletedEventHandler(objWS_SendCompleted);
                objWS.SendAsync("test", "test", st.ToArray(), BugInformation, DateTime.Now);
            }
        }

        static void objWS_SendCompleted(object sender, Company.Interface.WSBugtracker.SendCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                throw e.Error;
            }
        }
    }
}
