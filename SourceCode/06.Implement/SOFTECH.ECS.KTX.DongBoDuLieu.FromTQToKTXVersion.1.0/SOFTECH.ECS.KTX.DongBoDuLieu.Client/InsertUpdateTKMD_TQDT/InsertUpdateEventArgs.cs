﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace DongBo.InsertUpdateTKMD_TQDT
{
    public class InsertUpdateEventArgs : EventArgs
    {
         public Exception Error { get; private set; }
        public string Message { get; private set; }
        public int Percent { get; private set; }
        public int SoToKhaiDuocCapNhat { get; private set; }
        public InsertUpdateEventArgs(string message, int percent, Exception ex, int sotokhaiduoccapnhat)
        {
            Message = message;
            Error = ex;
            Percent = percent;
            SoToKhaiDuocCapNhat = sotokhaiduoccapnhat;
        }
        public InsertUpdateEventArgs(string message, int percent,int sotokhai)
            : this(message, percent, null, sotokhai)
        {
        }
        public InsertUpdateEventArgs(Exception ex,int sotokhai)
            : this(string.Empty, 0, ex, sotokhai)
        {

        }
    }
}
