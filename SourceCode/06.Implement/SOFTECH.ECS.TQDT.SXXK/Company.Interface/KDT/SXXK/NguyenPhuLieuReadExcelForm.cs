using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.KDT.SXXK;
using GemBox.Spreadsheet;
namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuReadExcelForm : BaseForm
    {
        public NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
        public NguyenPhuLieuReadExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch) 
        {
            return ch - 'A';
        }
        private int checkNPLExit(string maNPL)
        {
            for (int i = 0; i < this.NPLCollection.Count; i++)
            {
                if (this.NPLCollection[i].Ma.ToUpper() == maNPL.ToUpper()) return i;
            }
            return -1;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            
            cvError.Validate();
            if(!cvError.IsValid)return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                //error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                }
                else
                {
                    error.SetError(txtRow, "Started row must be greater than 0 ");
                }

                error.SetIconPadding(txtRow, 8);
                return;

            }
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text,true);
            }
            catch 
            {
                //ShowMessage("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", false);
                MLMessages("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                return;
            }
            try
            {
               ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
               // ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"", "MSG_EXC01", txtSheet.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
            int maHangCol = ConvertCharToInt(maHangColumn);
            char tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
            int tenHangCol = ConvertCharToInt(tenHangColumn);
            char maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
            int maHSCol = ConvertCharToInt(maHSColumn);
            char dvtColumn = Convert.ToChar(txtDVTColumn.Text);
            int dvtCol = ConvertCharToInt(dvtColumn);
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        NguyenPhuLieu npl = new NguyenPhuLieu();
                        npl.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                        npl.Ten = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                        npl.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                        //while (npl.MaHS.Trim().Length < 10)
                        //    npl.MaHS += "0";
                        if (npl.Ma.Trim() == "")
                        {
                            string rel = MLMessages("Mã của nguyên phụ liệu tại dòng : " + (wsr.Index+1) + "rỗng nên được bỏ qua. Bạn có muốn tiếp tục không ?", "MSG_EXC06", "" + (wsr.Index + 1), true);
                            if (rel == "Yes")
                            {
                                continue;
                            }
                            else
                                break;
                        }
                        try
                        {
                            npl.DVT_ID = DonViTinh_GetID(Convert.ToString(wsr.Cells[dvtCol].Value).ToUpper());
                        }
                        catch
                        {
                            string rel = MLMessages("Đơn vị tính : " + Convert.ToString(wsr.Cells[dvtCol].Value).ToUpper() + "của hàng thứ : " + (wsr.Index + 1) + " không có trong hệ thống nên sẽ được bỏ qua. Bạn có muốn tiếp tục không ?", "MSG_EXC06", "" + (wsr.Index + 1), true);
                            if (rel == "Yes")
                            {
                                continue;
                            }
                            else
                                break;
                        }
                        Company.BLL.SXXK.NguyenPhuLieu NPLDaDuyet = new Company.BLL.SXXK.NguyenPhuLieu();
                        NPLDaDuyet.Ma = npl.Ma;
                        NPLDaDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        NPLDaDuyet.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        if (NPLDaDuyet.Load())
                        {
                            string rel = MLMessages("Nguyên phụ liệu có mã  : " + NPLDaDuyet.Ma + " đã có trong hệ thống nên sẽ được bỏ qua. Bạn có muốn tiếp tục không ?", "MSG_EXC08","",true);
                            if (rel == "Yes")
                            {
                                continue;
                            }
                            else
                                break;
                        }
                        if (checkNPLExit(npl.Ma) >= 0)
                        {
                          //  if (ShowMessage("Nguyên phụ liệu có mã \"" + npl.Ma + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế nguyên phụ liệu trên lưới bằng sản phẩm này?", true) == "Yes")
                            if (MLMessages("Nguyên phụ liệu có mã \"" + npl.Ma + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế nguyên phụ liệu trên lưới bằng nguyên phụ liệu này?", "MSG_EXC08", "", true) != "Yes")
                                this.NPLCollection[checkNPLExit(npl.Ma)] = npl;

                        }
                        else this.NPLCollection.Add(npl);
                    }
                    catch
                    {
                       // if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes") 
                        if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\n Bạn có muốn tiếp tục không?", "MSG_EXC06", Convert.ToString(wsr.Index + 1), true) != "Yes")
                        {
                            this.NPLCollection.Clear();
                            return;
                        }
                    }
                }
            }
            this.Close();
        }

        private void NguyenPhuLieuReadExcelForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
            txtSheet.Focus();
        }
    }
}