﻿
using System;
using System.Drawing;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
//Kiểm tra interface của project LanNT
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.KDT;
using Company.KD.BLL;
using Company.KD.BLL.Utils;

using System.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using System.Threading;

namespace Company.Interface
{
    public partial class HangMauDichForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        private double clg;
        private double dgnt;
        //public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public string LoaiHangHoa;
        public ToKhaiMauDich TKMD;
        private decimal luong;

        public string MaNguyenTe;
        public string MaNuocXX;
        public string NhomLoaiHinh = string.Empty;
        private double st_clg;
        private HangMauDich hangMauDich = null;
        //-----------------------------------------------------------------------------------------				
        private double tgnt;
        private double tgtt_gtgt;

        private double tgtt_nk;
        private double tgtt_ttdb;
        private double tl_clg;
        private double tongTGKB;
        private double ts_gtgt;

        private double ts_nk;
        private double ts_ttdb;
        private double tt_gtgt;

        private double tt_nk;
        private double tt_ttdb;
        public double TyGiaTT;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
        //-----------------------------------------------------------------------------------------

        public HangMauDichForm()
        {
            InitializeComponent();
        }
        public HangMauDichForm(HangMauDich hangmaudich)
            : this()
        {
            hangMauDich = hangmaudich;
        }
        private void khoitao_GiaoDien()
        {

        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            txtTGNT.DecimalDigits = Company.KDT.SHARE.Components.Globals.TriGiaNT;
        }

        private double tinhthue()
        {
            this.tgtt_nk = this.tgnt * this.TyGiaTT;
            if (!chkThueTuyetDoi.Checked)
                this.tt_nk = this.tgtt_nk * this.ts_nk;
            else
            {
                double dgntTuyetDoi = Convert.ToDouble(txtDonGiaTuyetDoi.Value);
                this.tt_nk = Math.Round(dgntTuyetDoi * TyGiaTT * Convert.ToDouble(luong));
            }
            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;

            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb + this.st_clg;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;

            //txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private double tinhthue2()
        {
            
            if (!chkThueTuyetDoi.Checked)
            {
                this.ts_nk = Convert.ToDouble(txtTS_NK.Value) / 100;
            }

            this.ts_ttdb = Convert.ToDouble(txtTS_TTDB.Value) / 100;
            this.ts_gtgt = Convert.ToDouble(txtTS_GTGT.Value) / 100;
            this.tl_clg = Convert.ToDouble(txtTL_CLG.Value) / 100;

            //this.tgnt = Convert.ToDouble(Convert.ToDecimal(this.dgnt) * (this.luong));

            this.tgtt_nk = Convert.ToDouble(txtTGTT_NK.Value);
            if (tgtt_nk == 0)
            {
                tgtt_nk = tgnt * TyGiaTT;
            }
            if (!chkThueTuyetDoi.Checked)
                this.tt_nk = this.tgtt_nk * this.ts_nk;
            else
            {
                double dgntTuyetDoi = Convert.ToDouble(txtDonGiaTuyetDoi.Value);
                this.tt_nk = Math.Round(dgntTuyetDoi * TyGiaTT * Convert.ToDouble(luong));
            }
            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb + this.st_clg;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            //txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);

            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private double tinhthue3()
        {
           
            decimal tgntkophi = (decimal)tgnt;
            if (TKMD.DKGH_ID.Trim() == "CNF" || TKMD.DKGH_ID.Trim() == "CFR") tgntkophi = (decimal)this.tgnt - TKMD.PhiVanChuyen;

            this.tgtt_nk = (double)tgntkophi * this.TyGiaTT;

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb + this.st_clg;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            txtTriGiaKB.Value = tgnt * this.TyGiaTT;
            //txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangMauDich hmd in TKMD.HMDCollection)
            {
                if (hmd.MaPhu.Trim() == maHang) return true;
            }
            return false;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
            cvError.Validate();
            if (!cvError.IsValid) return;

            epError.Clear();
            if (string.IsNullOrEmpty(ctrNuocXX.Ma))
            {
                epError.SetError(ctrNuocXX, "Không được để trống nước xuất xứ");
                return;
            }
            if (string.IsNullOrEmpty(cbDonViTinh.Text))
            {
                epError.SetError(cbDonViTinh, "Không được để trống đơn vị tính");
                return;
            }

            txtMaHang.Focus();

            try
            {
                bool isAddNew = false;
                if (hangMauDich == null)
                {
                    hangMauDich = new HangMauDich();
                    hangMauDich.SoThuTuHang = 0;
                    isAddNew = true;
                }
                GetHang(hangMauDich);
                // Tính thuế.
                //hmd.TinhThue(this.TyGiaTT);
                if (isAddNew)
                    TKMD.HMDCollection.Add(hangMauDich);
                TKMD.SoLuongPLTK = (short)Helpers.GetLoading(TKMD.HMDCollection.Count);

                this.refresh_STTHang();
                dgList.DataSource = TKMD.HMDCollection;
                dgList.Refetch();
                #region Comment by LanNT thay đoạn này bằng đoạn dưới
                //txtMaHang.Text = "";
                //txtTenHang.Text = "";
                //txtMaHS.Text = "";
                //txtLuong.Value = 0;
                //txtDGNT.Value = 0;
                //txtTGTT_NK.Value = 0;

                //txtTGNT.Value = 0;
                //txtTriGiaKB.Value = 0;
                //txtTGTT_TTDB.Value = 0;
                //txtTGTT_NK.Value = 0;
                //txtTGTT_GTGT.Value = 0;
                //txtCLG.Value = 0;
                //txtTienThue_NK.Value = 0;
                //txtTienThue_TTDB.Value = 0;
                //txtTienThue_GTGT.Value = 0;
                //txtTongSoTienThue.Value = 0;
                //grbThue.Enabled = true;
                //chkMienThue.Checked = false;
                //chkThueTuyetDoi.Checked = false;
                //chkHangFOC.Checked = false;
                //this.edit = false;
                //epError.Clear();
                //LuongCon = 0;
                //txtTSVatGiam.Text = "";
                //txtTSXNKGiam.Text = "";
                //txtTSTTDBGiam.Text = "";
                #endregion Comment by LanNT thay đoạn này bằng đoạn dưới
                SetHang(hangMauDich);
                hangMauDich = null;


            }
            catch (Exception ex)
            {
                ShowMessage(setText("Dữ liệu của bạn không hợp lệ ", "Invalid input value") + ex.Message, false);
            }
        }

        private void GetHang(HangMauDich hmd)
        {

            hmd.MaHS = txtMaHS.Text;
            hmd.MaPhu = txtMaHang.Text.Trim();
            hmd.TenHang = txtTenHang.Text.Trim();
            hmd.NuocXX_ID = ctrNuocXX.Ma;
            hmd.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            hmd.SoLuong = Convert.ToDecimal(txtLuong.Value);
            hmd.DonGiaKB = Convert.ToDouble(txtDGNT.Value);
            hmd.DonGiaTuyetDoi = Convert.ToDouble(txtDonGiaTuyetDoi.Value);
            hmd.TriGiaKB = Convert.ToDouble(txtTGNT.Value);
            hmd.TriGiaKB_VND = Convert.ToDouble(txtTriGiaKB.Value);
            hmd.TriGiaTT = Convert.ToDouble(txtTGTT_NK.Value);
            hmd.ThueSuatXNK = Convert.ToDouble(txtTS_NK.Value);
            hmd.ThueSuatTTDB = Convert.ToDouble(txtTS_TTDB.Value);
            hmd.ThueSuatGTGT = Convert.ToDouble(txtTS_GTGT.Value);
            hmd.TyLeThuKhac = Convert.ToDouble(txtTL_CLG.Value);
            hmd.ThueXNK = Math.Round(Convert.ToDouble(txtTienThue_NK.Value), 0);
            hmd.ThueTTDB = Math.Round(Convert.ToDouble(txtTienThue_TTDB.Value), 0);
            hmd.ThueGTGT = Math.Round(Convert.ToDouble(txtTienThue_GTGT.Value), 0);
            hmd.DonGiaTT = hmd.TriGiaTT / Convert.ToDouble(hmd.SoLuong);
            hmd.PhuThu = Convert.ToDouble(txtTien_CLG.Value);
            hmd.FOC = chkHangFOC.Checked;
            hmd.ThueTuyetDoi = chkThueTuyetDoi.Checked;
            hmd.ThueSuatXNKGiam = txtTSXNKGiam.Text.Trim();
            hmd.ThueSuatTTDBGiam = txtTSTTDBGiam.Text.Trim();
            hmd.ThueSuatVATGiam = txtTSVatGiam.Text.Trim();
            hmd.TyLeThuKhac = Convert.ToDouble(txtTyLeThuKhac.Value);
            hmd.TriGiaThuKhac = (txtTriGiaThuKhac.Text != "" ? Convert.ToDouble(txtTriGiaThuKhac.Text) : 0);
            if (chkMienThue.Checked)
                hmd.MienThue = 1;
            else
                hmd.MienThue = 0;
            #region Thêm Mới V3
            hmd.MaHSMoRong = txtMaHSMoRong.Text;
            hmd.NhanHieu = txtNhanHieu.Text;
            hmd.QuyCachPhamChat = txtQuyCach.Text;
            hmd.ThanhPhan = txtThanhPhan.Text;
            hmd.Model = txtModel.Text;
            hmd.TenHangSX = txtTenHangSX.Text;
            hmd.MaHangSX = txtMaHangSX.Text;
            #endregion
        }
        private void SetHang(HangMauDich hmd)
        {

            txtMaHang.Text = hmd.MaPhu;
            txtTenHang.Text = hmd.TenHang;
            txtMaHS.Text = hmd.MaHS;
            cbDonViTinh.SelectedValue = hmd.DVT_ID;

            #region Thêm Mới V3
            txtMaHSMoRong.Text = hmd.MaHSMoRong;
            txtMaHangSX.Text = hmd.MaHangSX;
            txtTenHangSX.Text = hmd.TenHangSX;
            txtNhanHieu.Text = hmd.NhanHieu;
            txtQuyCach.Text = hmd.QuyCachPhamChat;
            txtThanhPhan.Text = hmd.ThanhPhan;
            txtModel.Text = hmd.Model;
            #endregion

            chkHangFOC.Checked = hmd.FOC;
            chkThueTuyetDoi.Checked = hmd.ThueTuyetDoi;
            txtLuong.Value = hmd.SoLuong;
            txtDGNT.Value = hmd.DonGiaKB;
            txtDonGiaTuyetDoi.Value = hmd.DonGiaTuyetDoi;
            txtTGNT.Value = hmd.TriGiaKB;
            txtTriGiaKB.Value = hmd.TriGiaKB_VND;
            ctrNuocXX.Ma = hmd.NuocXX_ID;

            txtTGTT_NK.Value = hmd.TriGiaTT;
            txtTS_NK.Value = hmd.ThueSuatXNK;
            txtTS_TTDB.Value = hmd.ThueSuatTTDB;
            txtTS_GTGT.Value = hmd.ThueSuatGTGT;
            txtTL_CLG.Value = hmd.TyLeThuKhac;
            txtTienThue_NK.Value = hmd.ThueXNK;
            txtTienThue_TTDB.Value = hmd.ThueTTDB;
            txtTienThue_GTGT.Value = hmd.ThueGTGT;
            txtTien_CLG.Value = hmd.PhuThu;
            txtTSXNKGiam.Text = hmd.ThueSuatXNKGiam;
            txtTSTTDBGiam.Text = hmd.ThueSuatTTDBGiam;
            txtTSVatGiam.Text = hmd.ThueSuatVATGiam;
            txtTyLeThuKhac.Value = hmd.TyLeThuKhac;
            txtTriGiaThuKhac.Value = hmd.TriGiaThuKhac;
            chkMienThue.Checked = hmd.MienThue == 1 ? true : false;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();

        }
        private void TinhTriGiaTinhThue()
        {
            double Phi = Convert.ToDouble(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
            double TongTriGiaHang = 0;
            foreach (HangMauDich hmd in TKMD.HMDCollection)
            {
                if (hmd.FOC) continue;
                TongTriGiaHang += hmd.TriGiaKB;
            }
            if (TongTriGiaHang == 0) return;
            double TriGiaTTMotDong = Phi / TongTriGiaHang;
            foreach (HangMauDich hmd in TKMD.HMDCollection)
            {
                if (hmd.FOC) continue;
                hmd.TriGiaTT = (TriGiaTTMotDong * hmd.TriGiaKB + hmd.TriGiaKB) * TyGiaTT;
                hmd.DonGiaTT = hmd.TriGiaTT / Convert.ToDouble(hmd.SoLuong);
                if (!hmd.ThueTuyetDoi)
                    hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100, MidpointRounding.AwayFromZero);
                hmd.ThueTTDB = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * hmd.ThueSuatTTDB / 100, MidpointRounding.AwayFromZero);
                hmd.ThueGTGT = Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB) * hmd.ThueSuatGTGT / 100, MidpointRounding.AwayFromZero);
                hmd.TriGiaThuKhac = hmd.TyLeThuKhac > 0 ? Math.Round(hmd.TriGiaTT * hmd.TyLeThuKhac / 100, MidpointRounding.AwayFromZero) : hmd.TriGiaThuKhac;
                //hmd.PhuThu = (hmd.TyLeThuKhac * hmd.ThueXNK) / 10;
            }

        }
        //-----------------------------------------------------------------------------------------
        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            if (NhomLoaiHinh.StartsWith("N"))
            {
                Company.KD.BLL.SXXK.HangHoaNhap npl = new Company.KD.BLL.SXXK.HangHoaNhap();
                npl.Ma = txtMaHang.Text.Trim();
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = "";
                if (npl.Load())
                {
                    txtMaHang.Text = npl.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = npl.MaHS;
                    txtTenHang.Text = npl.Ten.Trim();
                    cbDonViTinh.SelectedValue = npl.DVT_ID;
                    epError.SetError(txtMaHang, null);
                }

            }
            else
            {
                Company.KD.BLL.SXXK.HangHoaXuat sp = new Company.KD.BLL.SXXK.HangHoaXuat();
                sp.Ma = txtMaHang.Text.Trim();
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = "";
                if (sp.Load())
                {
                    txtMaHang.Text = sp.Ma.Trim();
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    cbDonViTinh.SelectedValue = sp.DVT_ID;
                    epError.SetError(txtMaHang, null);
                }
            }

        }

        private void HangMauDichForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();

            bool isActive = TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET ||
                TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;

            btnXoa.Enabled = btnGhi.Enabled = isActive;

            if (hangMauDich != null)
                SetHang(hangMauDich);

            if (btnXoa.Enabled)
                this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            else this.dgList.AllowDelete = InheritableBoolean.False;

            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.MaNguyenTe + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.MaNguyenTe);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.MaNguyenTe);

            if (this.NhomLoaiHinh[0].ToString().Equals("X"))
                label16.Text = setText("Trị giá tính thuế XK", "Export Tax assessment");


            dgList.DataSource = TKMD.HMDCollection;

            lblTyGiaTT.Text = "Tỷ giá tính thuế: " + this.TyGiaTT.ToString("N");
            refresh_STTHang();
            if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.View)
                btnGhi.Visible = false;
            txtMaHang.Focus();
            txtLuong.FormatString = "N" + GlobalSettings.SoThapPhan.SoLuongHMD;
            txtDGNT.FormatString = "N" + GlobalSettings.SoThapPhan.DonGiaNT;
            txtTriGiaKB.FormatString = "N" + GlobalSettings.SoThapPhan.TriGiaNT;
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue();
            else
                tinhthue3();
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            if (GlobalSettings.TinhThueTGNT == "0")
            {
                this.luong = Convert.ToDecimal(txtLuong.Value);
                if (luong != 0)
                {
                    this.dgnt = Convert.ToDouble(txtDGNT.Value);// Convert.ToDouble(Convert.ToDecimal(this.dgnt) * (this.luong));
                    txtTGNT.Value = Helpers.FormatNumeric(this.dgnt * Convert.ToDouble(luong), GlobalSettings.SoThapPhan.TriGiaNT, Thread.CurrentThread.CurrentCulture);
                    this.tgnt = Convert.ToDouble(Convert.ToDecimal(txtTGNT.Text));
                    if (this.NhomLoaiHinh.Contains("N"))
                        tinhthue();
                    else
                        tinhthue3();
                }

            }
            
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDouble(txtTS_NK.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTS_TTDB_Leave(object sender, EventArgs e)
        {
            this.ts_ttdb = Convert.ToDouble(txtTS_TTDB.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTS_GTGT_Leave(object sender, EventArgs e)
        {
            this.ts_gtgt = Convert.ToDouble(txtTS_GTGT.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTS_CLG_Leave(object sender, EventArgs e)
        {
            this.tl_clg = Convert.ToDouble(txtTL_CLG.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        //-----------------------------------------------------------------------------------------------
        private void refresh_STTHang()
        {
            int i = 1;
            TKMD.TongTriGiaKhaiBao = 0;
            decimal tongThue = 0;
            foreach (HangMauDich hmd in TKMD.HMDCollection)
            {
                hmd.SoThuTuHang = i++;
                TKMD.TongTriGiaKhaiBao += (decimal)hmd.TriGiaKB;
                TKMD.TongTriGiaTinhThue += (decimal)hmd.TriGiaTT;
                tongThue += (decimal)hmd.ThueTTDB + (decimal)hmd.ThueXNK + (decimal)hmd.ThueGTGT + (decimal)hmd.TriGiaThuKhac;
            }
            lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ :  {0} ({1})", TKMD.TongTriGiaKhaiBao, this.MaNguyenTe);
            lblTongTienThue.Text = string.Format("Tổng tiền thuế :  {0} (VND)", tongThue.ToString("N"));
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["NuocXX_ID"].Value != null)
                e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            if (e.Row.Cells["DVT_ID"].Value != null)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void chkMienThue_CheckedChanged(object sender, EventArgs e)
        {
            foreach (System.Windows.Forms.Control item in grbThue.Controls)
            {
                if (item.Name != "chkMienThue" && item.GetType() != typeof(System.Windows.Forms.Label))
                    item.Enabled = !chkMienThue.Checked;
            }
            chkMienThue.Enabled = true;

            txtTS_NK.Value = 0;
            txtTS_TTDB.Value = 0;
            txtTS_GTGT.Value = 0;
            txtTL_CLG.Value = 0;
            if (TKMD.MaLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            switch (this.NhomLoaiHinh[0].ToString())
            {
                case "N":
                    //if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.isBrower = true;
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                    this.NPLRegistedForm.ShowDialog(this);
                    if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "")
                    {
                        txtMaHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                    }
                    break;
                case "X":
                    //if (this.SPRegistedForm == null)
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.isBowrer = true;
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.ShowDialog(this);
                    if (this.SPRegistedForm.SanPhamSelected.Ma != "")
                    {
                        txtMaHang.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3);
                    }
                    break;
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            epError.Clear();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0) return;
            #region LanNT Comment thay doan nay bang doan duoi

            //foreach (GridEXSelectedItem i in items)
            //{
            //    if (i.RowType == RowType.Record)
            //    {
            //        HangMauDichEditForm f = new HangMauDichEditForm();
            //        f.HMD = this.HMDCollection[i.Position];

            //        f.NhomLoaiHinh = this.NhomLoaiHinh;
            //        f.LoaiHangHoa = this.LoaiHangHoa;
            //        f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        f.MaNguyenTe = this.MaNguyenTe;
            //        f.TyGiaTT = this.TyGiaTT;
            //        f.collection = this.HMDCollection;
            //        f.TKMD = this.TKMD;
            //        if (this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            //            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            //        else
            //            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            //        if (f.HMD.MienThue == 1)
            //        {
            //            f.chkMienThue.Checked = true;
            //            f.chkMienThue_CheckedChanged(null, null);
            //        }

            //        f.ShowDialog(this);
            //        if (f.IsEdited)
            //        {
            //            if (f.HMD.TKMD_ID > 0)
            //                f.HMD.Update();
            //            else
            //                this.HMDCollection[i.Position] = f.HMD;
            //        }
            //        else if (f.IsDeleted)
            //        {
            //            if (f.HMD.TKMD_ID > 0)
            //                f.HMD.Delete();
            //            this.HMDCollection.RemoveAt(i.Position);
            //        }
            //        dgList.DataSource = this.HMDCollection;
            //        try
            //        {
            //            dgList.Refetch();
            //        }
            //        catch { dgList.DataSource = this.HMDCollection; }
            //        refresh_STTHang();
            //    }
            //    break;
            //}
            #endregion LanNT Comment thay doan nay bang doan duoi
            hangMauDich = (HangMauDich)items[0].GetRow().DataRow;
            SetHang(hangMauDich);
        }


        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    HangMauDich hmd = (HangMauDich)e.Row.DataRow;
                    if (hmd.ID > 0)
                    {
                        hmd.Delete();
                    }
                    this.tongTGKB -= hmd.TriGiaKB;
                    lblTongTGKB.Text = string.Format(lblTongTGKB.Text + ": {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangMauDich hmd = (HangMauDich)i.GetRow().DataRow;
                        if (hmd.ID > 0)
                        {
                            if (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID).Count > 0)
                            {
                                new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hóa đơn thương mại, không thể xóa.", false);
                                e.Cancel = true;
                                return;
                            }
                            else if (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID).Count > 0)
                            {
                                new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hợp đồng thương mại, không thể xóa.", false);
                                e.Cancel = true;
                                return;
                            }

                            hmd.Delete();
                        }
                    }
                }
                refresh_STTHang();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void SaoChepCha_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            HangMauDich hmd = (HangMauDich)dgList.GetRow().DataRow;
            txtMaHang.Text = hmd.MaPhu;
            txtTenHang.Text = hmd.TenHang;
            txtMaHS.Text = hmd.MaHS;
            cbDonViTinh.SelectedValue = hmd.DVT_ID;
            ctrNuocXX.Ma = hmd.NuocXX_ID;
            txtDGNT.Value = this.dgnt = hmd.DonGiaKB;
            txtLuong.Value = this.luong = hmd.SoLuong;
            txtTGNT.Value = hmd.TriGiaKB;
            txtTriGiaKB.Value = hmd.TriGiaKB_VND;
            txtTGTT_NK.Value = hmd.TriGiaTT;
            txtTS_NK.Value = this.ts_nk = hmd.ThueSuatXNK;
            txtTS_TTDB.Value = this.ts_ttdb = hmd.ThueSuatTTDB;
            txtTS_GTGT.Value = this.ts_gtgt = hmd.ThueSuatGTGT;
            //txtTL_CLG.Value = this.tl_clg = hmd.PhuThu;
            txtTriGiaThuKhac.Value = hmd.TriGiaThuKhac;
            txtTyLeThuKhac.Value = hmd.TyLeThuKhac;
            chkHangFOC.Checked = hmd.FOC;
            chkMienThue.Checked = hmd.ThueTuyetDoi;
            txtLuong.Focus();
        }

        private void txtTGTT_NK_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
            txtTriGiaThuKhac.Value = (decimal.Parse(txtTGTT_NK.Text) / 100) * decimal.Parse(txtTyLeThuKhac.Text);

        }
        private void txtTyLeThuKhac_Leave(object sender, EventArgs e)
        {
            txtTriGiaThuKhac.Value = (Convert.ToDecimal(txtTGTT_NK.Value) / 100) * Convert.ToDecimal(txtTyLeThuKhac.Value);
        }
        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            //if (!MaHS.Validate(txtMaHS.Text, 10))
            //{
            //     txtMaHS.Focus();
            //    epError.SetIconPadding(txtMaHS, -8);
            //    epError.SetError(txtMaHS,setText("Mã HS không hợp lệ.","Invalid input value"));
            //}
            //else
            //{
            //    epError.SetError(txtMaHS, string.Empty);
            //}
            //string MoTa = MaHS.CheckExist(txtMaHS.Text);
            //if (MoTa == "")
            //{
            //    txtMaHS.Focus();
            //    epError.SetIconPadding(txtMaHS, -8);
            //    epError.SetError(txtMaHS, setText("Mã HS không có trong danh mục mã HS.","This HS is not exist"));
            //}
            //else
            //{
            //    epError.SetError(txtMaHS, string.Empty);
            //}
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            List<HangMauDich> hmdColl = new List<HangMauDich>();
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        hmdColl.Add((HangMauDich)i.GetRow().DataRow);
                    }

                }
                foreach (HangMauDich hmd in hmdColl)
                {
                    try
                    {
                        if (hmd.ID > 0)
                        {
                            if (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID).Count > 0)
                            {
                                new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hóa đơn thương mại, không thể xóa.", false);
                                return;
                            }
                            else if (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID).Count > 0)
                            {
                                new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hợp đồng thương mại, không thể xóa.", false);
                                return;
                            }

                            hmd.Delete();
                        }
                        try
                        {
                            TKMD.HMDCollection.Remove(hmd);
                        }
                        catch { }
                    }
                    catch { }
                }
            }
            dgList.DataSource = TKMD.HMDCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            refresh_STTHang();
        }

        private void txtMaHS_TextChanged(object sender, EventArgs e)
        {

        }

        private void uiCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (chkThueTuyetDoi.Checked)
            {
                txtTS_NK.Enabled = false;
                txtTienThue_NK.ReadOnly = false;
                txtDonGiaTuyetDoi.Enabled = true;

                this.txtTienThue_NK.BackColor = Color.White;
            }
            else
            {
                txtTS_NK.Enabled = true;
                txtTienThue_NK.ReadOnly = true;
                txtDonGiaTuyetDoi.Enabled = false;
                this.txtTienThue_NK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            }
        }
        private void txtTienThue_NK_Leave(object sender, EventArgs e)
        {
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtDonGiaTuyetDoi_Leave(object sender, EventArgs e)
        {
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue();
            else
                tinhthue3();
        }

        private void txtTGNT_Leave(object sender, EventArgs e)
        {
            if (GlobalSettings.TinhThueTGNT == "1")
            {
                this.luong = Convert.ToDecimal(txtLuong.Value);
                if (luong != 0)
                {
                    this.tgnt = Convert.ToDouble(txtTGNT.Value);// Convert.ToDouble(Convert.ToDecimal(this.dgnt) * (this.luong));
                    txtDGNT.Value = Helpers.FormatNumeric(tgnt / Convert.ToDouble(luong), GlobalSettings.SoThapPhan.DonGiaNT, Thread.CurrentThread.CurrentCulture);
                    this.dgnt = Convert.ToDouble(Convert.ToDecimal(txtDGNT.Text));
                    if (this.NhomLoaiHinh.Contains("N"))
                        tinhthue();
                    else
                        tinhthue3();
                }
            }
        }

        private void lblTyGiaTT_Click(object sender, EventArgs e)
        {

        }

        private void txtPhuThu_Leave(object sender, EventArgs e)
        {
            //if (Convert.ToDecimal(txtTGTT_NK.Value) == 0) return;
            //txtTyLeThuKhac.Value = Convert.ToDecimal(txtPhuThu.Value) / (Convert.ToDecimal(txtTGTT_NK.Value) / 100);
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            hangMauDich = new HangMauDich();
            hangMauDich.SoThuTuHang = 0;
            SetHang(hangMauDich);
            hangMauDich = null;
        }

        private void linkLabel1_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            Company.Interface.Report.CauHinhInForm f = new Company.Interface.Report.CauHinhInForm();
            f.Listlable.Add(f.lblDonGiaNT);
            f.Listlable.Add(f.lblTriGiaNT);
            f.Listlable.Add(f.lblLuongHMD);
            f.ShowDialog();
            this.txtDGNT.Focus();
            txtLuong.FormatString = "N" + GlobalSettings.SoThapPhan.SoLuongHMD;
            txtDGNT.FormatString = "N" + GlobalSettings.SoThapPhan.DonGiaNT;
            txtTriGiaKB.FormatString = "N" + GlobalSettings.SoThapPhan.TriGiaNT;
        }

        private void linkLabel2_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            Company.Interface.CauHinhToKhaiForm f = new Company.Interface.CauHinhToKhaiForm();
            f.Listlable.Add(f.lblPhuongThucTT);
            f.ShowDialog();
            this.txtDGNT.Focus();
        }
    }
}
