﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_BillOfLadings_Detail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long BillOfLadings_ID { set; get; }
		public string SoVanDonGoc { set; get; }
		public DateTime NgayVanDonGoc { set; get; }
		public string MaNguoiPhatHanh { set; get; }
		public decimal SoLuongVDN { set; get; }
		public decimal PhanLoaiTachVD { set; get; }
		public decimal LoaiHang { set; get; }

        public List<KDT_VNACCS_BranchDetail> Collection = new List<KDT_VNACCS_BranchDetail>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_BillOfLadings_Detail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_BillOfLadings_Detail> collection = new List<KDT_VNACCS_BillOfLadings_Detail>();
			while (reader.Read())
			{
				KDT_VNACCS_BillOfLadings_Detail entity = new KDT_VNACCS_BillOfLadings_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BillOfLadings_ID"))) entity.BillOfLadings_ID = reader.GetInt64(reader.GetOrdinal("BillOfLadings_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDonGoc"))) entity.SoVanDonGoc = reader.GetString(reader.GetOrdinal("SoVanDonGoc"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDonGoc"))) entity.NgayVanDonGoc = reader.GetDateTime(reader.GetOrdinal("NgayVanDonGoc"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiPhatHanh"))) entity.MaNguoiPhatHanh = reader.GetString(reader.GetOrdinal("MaNguoiPhatHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongVDN"))) entity.SoLuongVDN = reader.GetDecimal(reader.GetOrdinal("SoLuongVDN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiTachVD"))) entity.PhanLoaiTachVD = reader.GetDecimal(reader.GetOrdinal("PhanLoaiTachVD"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHang"))) entity.LoaiHang = reader.GetDecimal(reader.GetOrdinal("LoaiHang"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_BillOfLadings_Detail> collection, long id)
        {
            foreach (KDT_VNACCS_BillOfLadings_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BillOfLadings_Details VALUES(@BillOfLadings_ID, @SoVanDonGoc, @NgayVanDonGoc, @MaNguoiPhatHanh, @SoLuongVDN, @PhanLoaiTachVD, @LoaiHang)";
            string update = "UPDATE t_KDT_VNACCS_BillOfLadings_Details SET BillOfLadings_ID = @BillOfLadings_ID, SoVanDonGoc = @SoVanDonGoc, NgayVanDonGoc = @NgayVanDonGoc, MaNguoiPhatHanh = @MaNguoiPhatHanh, SoLuongVDN = @SoLuongVDN, PhanLoaiTachVD = @PhanLoaiTachVD, LoaiHang = @LoaiHang WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BillOfLadings_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BillOfLadings_ID", SqlDbType.BigInt, "BillOfLadings_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDonGoc", SqlDbType.NVarChar, "SoVanDonGoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayVanDonGoc", SqlDbType.DateTime, "NgayVanDonGoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, "MaNguoiPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongVDN", SqlDbType.Decimal, "SoLuongVDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiTachVD", SqlDbType.Decimal, "PhanLoaiTachVD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHang", SqlDbType.Decimal, "LoaiHang", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BillOfLadings_ID", SqlDbType.BigInt, "BillOfLadings_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDonGoc", SqlDbType.NVarChar, "SoVanDonGoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayVanDonGoc", SqlDbType.DateTime, "NgayVanDonGoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, "MaNguoiPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongVDN", SqlDbType.Decimal, "SoLuongVDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiTachVD", SqlDbType.Decimal, "PhanLoaiTachVD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHang", SqlDbType.Decimal, "LoaiHang", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BillOfLadings_Details VALUES(@BillOfLadings_ID, @SoVanDonGoc, @NgayVanDonGoc, @MaNguoiPhatHanh, @SoLuongVDN, @PhanLoaiTachVD, @LoaiHang)";
            string update = "UPDATE t_KDT_VNACCS_BillOfLadings_Details SET BillOfLadings_ID = @BillOfLadings_ID, SoVanDonGoc = @SoVanDonGoc, NgayVanDonGoc = @NgayVanDonGoc, MaNguoiPhatHanh = @MaNguoiPhatHanh, SoLuongVDN = @SoLuongVDN, PhanLoaiTachVD = @PhanLoaiTachVD, LoaiHang = @LoaiHang WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BillOfLadings_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BillOfLadings_ID", SqlDbType.BigInt, "BillOfLadings_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDonGoc", SqlDbType.NVarChar, "SoVanDonGoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayVanDonGoc", SqlDbType.DateTime, "NgayVanDonGoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, "MaNguoiPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongVDN", SqlDbType.Decimal, "SoLuongVDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiTachVD", SqlDbType.Decimal, "PhanLoaiTachVD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHang", SqlDbType.Decimal, "LoaiHang", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BillOfLadings_ID", SqlDbType.BigInt, "BillOfLadings_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDonGoc", SqlDbType.NVarChar, "SoVanDonGoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayVanDonGoc", SqlDbType.DateTime, "NgayVanDonGoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, "MaNguoiPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongVDN", SqlDbType.Decimal, "SoLuongVDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiTachVD", SqlDbType.Decimal, "PhanLoaiTachVD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHang", SqlDbType.Decimal, "LoaiHang", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_BillOfLadings_Detail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_BillOfLadings_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_BillOfLadings_Detail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_BillOfLadings_Detail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_BillOfLadings_Detail> SelectCollectionBy_BillOfLadings_ID(long billOfLadings_ID)
		{
            IDataReader reader = SelectReaderBy_BillOfLadings_ID(billOfLadings_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_BillOfLadings_ID(long billOfLadings_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectBy_BillOfLadings_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BillOfLadings_ID", SqlDbType.BigInt, billOfLadings_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_BillOfLadings_ID(long billOfLadings_ID)
		{
			const string spName = "p_KDT_VNACCS_BillOfLadings_Detail_SelectBy_BillOfLadings_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BillOfLadings_ID", SqlDbType.BigInt, billOfLadings_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_BillOfLadings_Detail(long billOfLadings_ID, string soVanDonGoc, DateTime ngayVanDonGoc, string maNguoiPhatHanh, decimal soLuongVDN, decimal phanLoaiTachVD, decimal loaiHang)
		{
			KDT_VNACCS_BillOfLadings_Detail entity = new KDT_VNACCS_BillOfLadings_Detail();	
			entity.BillOfLadings_ID = billOfLadings_ID;
			entity.SoVanDonGoc = soVanDonGoc;
			entity.NgayVanDonGoc = ngayVanDonGoc;
			entity.MaNguoiPhatHanh = maNguoiPhatHanh;
			entity.SoLuongVDN = soLuongVDN;
			entity.PhanLoaiTachVD = phanLoaiTachVD;
			entity.LoaiHang = loaiHang;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@BillOfLadings_ID", SqlDbType.BigInt, BillOfLadings_ID);
			db.AddInParameter(dbCommand, "@SoVanDonGoc", SqlDbType.NVarChar, SoVanDonGoc);
			db.AddInParameter(dbCommand, "@NgayVanDonGoc", SqlDbType.DateTime, NgayVanDonGoc.Year <= 1753 ? DBNull.Value : (object) NgayVanDonGoc);
			db.AddInParameter(dbCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, MaNguoiPhatHanh);
			db.AddInParameter(dbCommand, "@SoLuongVDN", SqlDbType.Decimal, SoLuongVDN);
			db.AddInParameter(dbCommand, "@PhanLoaiTachVD", SqlDbType.Decimal, PhanLoaiTachVD);
			db.AddInParameter(dbCommand, "@LoaiHang", SqlDbType.Decimal, LoaiHang);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_BillOfLadings_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BillOfLadings_Detail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_BillOfLadings_Detail(long id, long billOfLadings_ID, string soVanDonGoc, DateTime ngayVanDonGoc, string maNguoiPhatHanh, decimal soLuongVDN, decimal phanLoaiTachVD, decimal loaiHang)
		{
			KDT_VNACCS_BillOfLadings_Detail entity = new KDT_VNACCS_BillOfLadings_Detail();			
			entity.ID = id;
			entity.BillOfLadings_ID = billOfLadings_ID;
			entity.SoVanDonGoc = soVanDonGoc;
			entity.NgayVanDonGoc = ngayVanDonGoc;
			entity.MaNguoiPhatHanh = maNguoiPhatHanh;
			entity.SoLuongVDN = soLuongVDN;
			entity.PhanLoaiTachVD = phanLoaiTachVD;
			entity.LoaiHang = loaiHang;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_BillOfLadings_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@BillOfLadings_ID", SqlDbType.BigInt, BillOfLadings_ID);
			db.AddInParameter(dbCommand, "@SoVanDonGoc", SqlDbType.NVarChar, SoVanDonGoc);
			db.AddInParameter(dbCommand, "@NgayVanDonGoc", SqlDbType.DateTime, NgayVanDonGoc.Year <= 1753 ? DBNull.Value : (object) NgayVanDonGoc);
			db.AddInParameter(dbCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, MaNguoiPhatHanh);
			db.AddInParameter(dbCommand, "@SoLuongVDN", SqlDbType.Decimal, SoLuongVDN);
			db.AddInParameter(dbCommand, "@PhanLoaiTachVD", SqlDbType.Decimal, PhanLoaiTachVD);
			db.AddInParameter(dbCommand, "@LoaiHang", SqlDbType.Decimal, LoaiHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_BillOfLadings_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BillOfLadings_Detail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_BillOfLadings_Detail(long id, long billOfLadings_ID, string soVanDonGoc, DateTime ngayVanDonGoc, string maNguoiPhatHanh, decimal soLuongVDN, decimal phanLoaiTachVD, decimal loaiHang)
		{
			KDT_VNACCS_BillOfLadings_Detail entity = new KDT_VNACCS_BillOfLadings_Detail();			
			entity.ID = id;
			entity.BillOfLadings_ID = billOfLadings_ID;
			entity.SoVanDonGoc = soVanDonGoc;
			entity.NgayVanDonGoc = ngayVanDonGoc;
			entity.MaNguoiPhatHanh = maNguoiPhatHanh;
			entity.SoLuongVDN = soLuongVDN;
			entity.PhanLoaiTachVD = phanLoaiTachVD;
			entity.LoaiHang = loaiHang;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@BillOfLadings_ID", SqlDbType.BigInt, BillOfLadings_ID);
			db.AddInParameter(dbCommand, "@SoVanDonGoc", SqlDbType.NVarChar, SoVanDonGoc);
			db.AddInParameter(dbCommand, "@NgayVanDonGoc", SqlDbType.DateTime, NgayVanDonGoc.Year <= 1753 ? DBNull.Value : (object) NgayVanDonGoc);
			db.AddInParameter(dbCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, MaNguoiPhatHanh);
			db.AddInParameter(dbCommand, "@SoLuongVDN", SqlDbType.Decimal, SoLuongVDN);
			db.AddInParameter(dbCommand, "@PhanLoaiTachVD", SqlDbType.Decimal, PhanLoaiTachVD);
			db.AddInParameter(dbCommand, "@LoaiHang", SqlDbType.Decimal, LoaiHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_BillOfLadings_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BillOfLadings_Detail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_BillOfLadings_Detail(long id)
		{
			KDT_VNACCS_BillOfLadings_Detail entity = new KDT_VNACCS_BillOfLadings_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public  int DeleteBy_BillOfLadings_ID(long billOfLadings_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_DeleteBy_BillOfLadings_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BillOfLadings_ID", SqlDbType.BigInt, billOfLadings_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_BillOfLadings_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BillOfLadings_Detail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert();
                    }
                    else
                    {
                        this.Update();
                    }

                    foreach (KDT_VNACCS_BranchDetail item in Collection)
                    {
                        item.BillOfLadings_Details_ID = this.ID;
                        item.InsertUpdateFull();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                }

        }
        public void DeleteFull()
        {

            try
            {
                foreach (KDT_VNACCS_BranchDetail branchDetail in Collection)
                {
                    branchDetail.BillOfLadings_Details_ID = this.ID;
                    branchDetail.Collection = KDT_VNACCS_BranchDetail_TransportEquipment.SelectCollectionBy_BranchDetail_ID(branchDetail.ID);
                    foreach (KDT_VNACCS_BranchDetail_TransportEquipment transportEquipment in branchDetail.Collection)
                    {
                        transportEquipment.BranchDetail_ID = branchDetail.ID;
                        transportEquipment.DeleteBy_BranchDetail_ID(branchDetail.ID);
                    }
                    branchDetail.DeleteBy_BillOfLadings_Details_ID(this.ID);
                }
                this.DeleteDynamic("ID = " + this.ID);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
            }
        }
	}	
}