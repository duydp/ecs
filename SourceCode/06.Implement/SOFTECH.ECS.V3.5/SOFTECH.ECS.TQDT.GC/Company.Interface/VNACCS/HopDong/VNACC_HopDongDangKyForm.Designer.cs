﻿namespace Company.Interface.VNACCS.HopDong
{
    partial class VNACC_HopDongDangKyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout gridFile_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_HopDongDangKyForm));
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar2 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnAddTK = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.ctrThoiHanTT = new DevExpress.XtraEditors.DateEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.ctrNgayHopDong = new DevExpress.XtraEditors.DateEdit();
            this.txtTongTriGia = new DevExpress.XtraEditors.TextEdit();
            this.txtSoHopDong = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDN = new DevExpress.XtraEditors.TextEdit();
            this.txtMaDN = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtTenNguoiKhai = new DevExpress.XtraEditors.TextEdit();
            this.txtMaNguoiKhai = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNgayTiepNhan = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtSoTiepNhan = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtHaiQuanTiepNhan = new DevExpress.XtraEditors.TextEdit();
            this.gridFile = new Janus.Windows.GridEX.GridEX();
            this.TenTepTin = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.btnView = new Janus.Windows.EditControls.UIButton();
            this.lblTongDungLuong = new DevExpress.XtraEditors.LabelControl();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.gridList = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.cmdMain = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar3 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSend2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSave2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cmdFeedback2 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdFeedback3 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctrThoiHanTT.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrThoiHanTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrNgayHopDong.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrNgayHopDong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTriGia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHopDong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaDN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNguoiKhai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaNguoiKhai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctrNgayTiepNhan.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrNgayTiepNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTiepNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHaiQuanTiepNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).BeginInit();
            this.uiTabPage2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            this.cmdMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar3)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(731, 529);
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = null;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar2
            // 
            this.uiCommandBar2.CommandManager = null;
            this.uiCommandBar2.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave1,
            this.cmdSend1,
            this.cmdFeedback1,
            this.cmdKetQuaXuLy1});
            this.uiCommandBar2.Key = "CommandBar1";
            this.uiCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar2.Name = "uiCommandBar2";
            this.uiCommandBar2.RowIndex = 1;
            this.uiCommandBar2.Size = new System.Drawing.Size(397, 28);
            // 
            // cmdSave1
            // 
            this.cmdSave1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave1.Image")));
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend1.Image")));
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback1.Image")));
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaXuLy1.Image")));
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = null;
            this.uiCommandBar1.Key = "";
            this.uiCommandBar1.Location = new System.Drawing.Point(397, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(49, 28);
            // 
            // cmdSave
            // 
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu thông tin";
            // 
            // cmdSend
            // 
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "Nhận phản hồi";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "Kết quả xử lý";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = null;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = null;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.btnAddTK);
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 298);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(726, 47);
            this.uiGroupBox5.TabIndex = 24;
            // 
            // btnAddTK
            // 
            this.btnAddTK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAddTK.Enabled = false;
            this.btnAddTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddTK.Image = global::Company.Interface.Properties.Resources._83;
            this.btnAddTK.Location = new System.Drawing.Point(46, 11);
            this.btnAddTK.Name = "btnAddTK";
            this.btnAddTK.Size = new System.Drawing.Size(116, 30);
            this.btnAddTK.TabIndex = 12;
            this.btnAddTK.Text = "Thêm tờ khai";
            this.btnAddTK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnAddTK.VisualStyleManager = this.vsmMain;
            this.btnAddTK.Click += new System.EventHandler(this.btnAddTK_Click);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.labelControl11);
            this.uiGroupBox4.Controls.Add(this.ctrThoiHanTT);
            this.uiGroupBox4.Controls.Add(this.labelControl10);
            this.uiGroupBox4.Controls.Add(this.ctrNgayHopDong);
            this.uiGroupBox4.Controls.Add(this.txtTongTriGia);
            this.uiGroupBox4.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox4.Controls.Add(this.labelControl12);
            this.uiGroupBox4.Controls.Add(this.labelControl9);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(-1, 203);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(727, 95);
            this.uiGroupBox4.TabIndex = 23;
            this.uiGroupBox4.Text = "Thông tin hợp đồng";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(0, 70);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(119, 13);
            this.labelControl11.TabIndex = 13;
            this.labelControl11.Text = "Thời hạn thanh toán :";
            // 
            // ctrThoiHanTT
            // 
            this.ctrThoiHanTT.EditValue = new System.DateTime(2015, 8, 6, 10, 25, 47, 628);
            this.ctrThoiHanTT.Location = new System.Drawing.Point(128, 63);
            this.ctrThoiHanTT.Name = "ctrThoiHanTT";
            this.ctrThoiHanTT.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.ctrThoiHanTT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ctrThoiHanTT.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ctrThoiHanTT.Size = new System.Drawing.Size(116, 20);
            this.ctrThoiHanTT.TabIndex = 10;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(371, 30);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(89, 13);
            this.labelControl10.TabIndex = 10;
            this.labelControl10.Text = "Ngày hợp đồng :";
            // 
            // ctrNgayHopDong
            // 
            this.ctrNgayHopDong.EditValue = new System.DateTime(2015, 8, 6, 10, 25, 57, 981);
            this.ctrNgayHopDong.Location = new System.Drawing.Point(463, 23);
            this.ctrNgayHopDong.Name = "ctrNgayHopDong";
            this.ctrNgayHopDong.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.ctrNgayHopDong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ctrNgayHopDong.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ctrNgayHopDong.Size = new System.Drawing.Size(116, 20);
            this.ctrNgayHopDong.TabIndex = 9;
            // 
            // txtTongTriGia
            // 
            this.txtTongTriGia.Location = new System.Drawing.Point(463, 63);
            this.txtTongTriGia.Name = "txtTongTriGia";
            this.txtTongTriGia.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtTongTriGia.Size = new System.Drawing.Size(208, 20);
            this.txtTongTriGia.TabIndex = 11;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Location = new System.Drawing.Point(128, 27);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtSoHopDong.Size = new System.Drawing.Size(234, 20);
            this.txtSoHopDong.TabIndex = 8;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(371, 66);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(70, 13);
            this.labelControl12.TabIndex = 12;
            this.labelControl12.Text = "Tổng trị giá :";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(3, 34);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(78, 13);
            this.labelControl9.TabIndex = 11;
            this.labelControl9.Text = "Số hợp đồng  :";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtTenDN);
            this.uiGroupBox2.Controls.Add(this.txtMaDN);
            this.uiGroupBox2.Controls.Add(this.labelControl5);
            this.uiGroupBox2.Controls.Add(this.labelControl8);
            this.uiGroupBox2.Controls.Add(this.labelControl7);
            this.uiGroupBox2.Controls.Add(this.txtTenNguoiKhai);
            this.uiGroupBox2.Controls.Add(this.txtMaNguoiKhai);
            this.uiGroupBox2.Controls.Add(this.labelControl6);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(-4, 103);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(730, 100);
            this.uiGroupBox2.TabIndex = 22;
            this.uiGroupBox2.Text = "Thông tin doanh nghiệp";
            // 
            // txtTenDN
            // 
            this.txtTenDN.Location = new System.Drawing.Point(128, 20);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtTenDN.Size = new System.Drawing.Size(234, 20);
            this.txtTenDN.TabIndex = 4;
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(128, 56);
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtMaDN.Size = new System.Drawing.Size(234, 20);
            this.txtMaDN.TabIndex = 6;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(3, 23);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(106, 13);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "Tên doanh nghiệp :";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(371, 59);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(85, 13);
            this.labelControl8.TabIndex = 12;
            this.labelControl8.Text = "Mã người khai :";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(371, 23);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(89, 13);
            this.labelControl7.TabIndex = 10;
            this.labelControl7.Text = "Tên người khai :";
            // 
            // txtTenNguoiKhai
            // 
            this.txtTenNguoiKhai.Location = new System.Drawing.Point(463, 20);
            this.txtTenNguoiKhai.Name = "txtTenNguoiKhai";
            this.txtTenNguoiKhai.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtTenNguoiKhai.Size = new System.Drawing.Size(208, 20);
            this.txtTenNguoiKhai.TabIndex = 5;
            // 
            // txtMaNguoiKhai
            // 
            this.txtMaNguoiKhai.Location = new System.Drawing.Point(463, 56);
            this.txtMaNguoiKhai.Name = "txtMaNguoiKhai";
            this.txtMaNguoiKhai.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtMaNguoiKhai.Size = new System.Drawing.Size(208, 20);
            this.txtMaNguoiKhai.TabIndex = 7;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(3, 59);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(102, 13);
            this.labelControl6.TabIndex = 13;
            this.labelControl6.Text = "Mã doanh nghiệp :";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ctrNgayTiepNhan);
            this.uiGroupBox1.Controls.Add(this.labelControl1);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.labelControl4);
            this.uiGroupBox1.Controls.Add(this.labelControl2);
            this.uiGroupBox1.Controls.Add(this.txtHaiQuanTiepNhan);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(729, 104);
            this.uiGroupBox1.TabIndex = 21;
            this.uiGroupBox1.Text = "Thông tin Hải quan";
            // 
            // ctrNgayTiepNhan
            // 
            this.ctrNgayTiepNhan.EditValue = new System.DateTime(2015, 8, 6, 10, 26, 6, 900);
            this.ctrNgayTiepNhan.Location = new System.Drawing.Point(393, 26);
            this.ctrNgayTiepNhan.Name = "ctrNgayTiepNhan";
            this.ctrNgayTiepNhan.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.ctrNgayTiepNhan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ctrNgayTiepNhan.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ctrNgayTiepNhan.Size = new System.Drawing.Size(122, 20);
            this.ctrNgayTiepNhan.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(6, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(76, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Số tiếp nhận :";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Location = new System.Drawing.Point(128, 26);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtSoTiepNhan.Properties.Appearance.Options.UseBackColor = true;
            this.txtSoTiepNhan.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(118, 20);
            this.txtSoTiepNhan.TabIndex = 1;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(6, 75);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(55, 13);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Hải quan :";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(271, 29);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Ngày tiếp nhận :";
            // 
            // txtHaiQuanTiepNhan
            // 
            this.txtHaiQuanTiepNhan.Location = new System.Drawing.Point(128, 72);
            this.txtHaiQuanTiepNhan.Name = "txtHaiQuanTiepNhan";
            this.txtHaiQuanTiepNhan.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtHaiQuanTiepNhan.Size = new System.Drawing.Size(234, 20);
            this.txtHaiQuanTiepNhan.TabIndex = 3;
            // 
            // gridFile
            // 
            this.gridFile.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridFile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridFile.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.gridFile.ColumnAutoResize = true;
            gridFile_DesignTimeLayout.LayoutString = resources.GetString("gridFile_DesignTimeLayout.LayoutString");
            this.gridFile.DesignTimeLayout = gridFile_DesignTimeLayout;
            this.gridFile.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridFile.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridFile.FrozenColumns = 5;
            this.gridFile.GroupByBoxVisible = false;
            this.gridFile.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridFile.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.gridFile.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridFile.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridFile.Hierarchical = true;
            this.gridFile.Location = new System.Drawing.Point(-1, -3);
            this.gridFile.Name = "gridFile";
            this.gridFile.RecordNavigator = true;
            this.gridFile.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridFile.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridFile.Size = new System.Drawing.Size(733, 446);
            this.gridFile.TabIndex = 80;
            this.gridFile.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.gridFile.VisualStyleManager = this.vsmMain;
            // 
            // TenTepTin
            // 
            this.TenTepTin.Caption = "Tên tệp tin";
            this.TenTepTin.Name = "TenTepTin";
            this.TenTepTin.Visible = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.labelControl13);
            this.uiGroupBox3.Controls.Add(this.btnDelete);
            this.uiGroupBox3.Controls.Add(this.labelControl14);
            this.uiGroupBox3.Controls.Add(this.btnView);
            this.uiGroupBox3.Controls.Add(this.lblTongDungLuong);
            this.uiGroupBox3.Controls.Add(this.btnAdd);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 449);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(730, 55);
            this.uiGroupBox3.TabIndex = 81;
            this.uiGroupBox3.Text = "Thông tin tệp đính kèm";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl13.Location = new System.Drawing.Point(127, 20);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(17, 13);
            this.labelControl13.TabIndex = 0;
            this.labelControl13.Text = "0 B";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = global::Company.Interface.Properties.Resources.delete;
            this.btnDelete.Location = new System.Drawing.Point(497, 14);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(104, 30);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Xóa File";
            this.btnDelete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl14.Location = new System.Drawing.Point(5, 39);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(350, 13);
            this.labelControl14.TabIndex = 0;
            this.labelControl14.Text = "Lưu ý : Tổng dung lượng các file đính kèm không vượt quá 2MB";
            // 
            // btnView
            // 
            this.btnView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnView.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnView.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.ImageIndex = 5;
            this.btnView.Location = new System.Drawing.Point(614, 14);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(104, 30);
            this.btnView.TabIndex = 3;
            this.btnView.Text = "Xem File";
            this.btnView.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // lblTongDungLuong
            // 
            this.lblTongDungLuong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongDungLuong.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblTongDungLuong.Location = new System.Drawing.Point(5, 20);
            this.lblTongDungLuong.Name = "lblTongDungLuong";
            this.lblTongDungLuong.Size = new System.Drawing.Size(100, 13);
            this.lblTongDungLuong.TabIndex = 0;
            this.lblTongDungLuong.Text = "Tổng dung lượng :";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = global::Company.Interface.Properties.Resources._83;
            this.btnAdd.Location = new System.Drawing.Point(379, 14);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(104, 30);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "Thêm File";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnAdd.VisualStyleManager = this.vsmMain;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(731, 529);
            this.uiTab1.TabIndex = 4;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.gridList);
            this.uiTabPage1.Controls.Add(this.uiGroupBox5);
            this.uiTabPage1.Controls.Add(this.uiGroupBox1);
            this.uiTabPage1.Controls.Add(this.uiGroupBox2);
            this.uiTabPage1.Controls.Add(this.uiGroupBox4);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(729, 507);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thông tin hợp đồng";
            // 
            // gridList
            // 
            this.gridList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.gridList.ColumnAutoResize = true;
            gridList_DesignTimeLayout.LayoutString = resources.GetString("gridList_DesignTimeLayout.LayoutString");
            this.gridList.DesignTimeLayout = gridList_DesignTimeLayout;
            this.gridList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridList.FrozenColumns = 5;
            this.gridList.GroupByBoxVisible = false;
            this.gridList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.gridList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridList.Hierarchical = true;
            this.gridList.Location = new System.Drawing.Point(-1, 345);
            this.gridList.Name = "gridList";
            this.gridList.RecordNavigator = true;
            this.gridList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridList.Size = new System.Drawing.Size(727, 162);
            this.gridList.TabIndex = 81;
            this.gridList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.gridList.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.uiGroupBox3);
            this.uiTabPage2.Controls.Add(this.gridFile);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(729, 507);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thông tin File đính kèm";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(95, 26);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(94, 22);
            this.toolStripMenuItem1.Text = "Xóa";
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar3});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave,
            this.cmdSend,
            this.cmdFeedback2,
            this.cmdResult});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.ImageList1;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.cmdMain;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // cmdMain
            // 
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar3});
            this.cmdMain.CommandManager = this.cmMain;
            this.cmdMain.Controls.Add(this.uiCommandBar3);
            this.cmdMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdMain.Location = new System.Drawing.Point(0, 0);
            this.cmdMain.Name = "cmdMain";
            this.cmdMain.Size = new System.Drawing.Size(731, 28);
            this.cmdMain.Text = "cmdMain";
            // 
            // uiCommandBar3
            // 
            this.uiCommandBar3.CommandManager = this.cmMain;
            this.uiCommandBar3.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend2,
            this.cmdSave2,
            this.cmdFeedback3,
            this.cmdResult1});
            this.uiCommandBar3.Key = "cmdMain";
            this.uiCommandBar3.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar3.Name = "uiCommandBar3";
            this.uiCommandBar3.RowIndex = 0;
            this.uiCommandBar3.Size = new System.Drawing.Size(388, 28);
            // 
            // cmdSend2
            // 
            this.cmdSend2.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend2.Icon")));
            this.cmdSend2.Key = "cmdSend";
            this.cmdSend2.Name = "cmdSend2";
            // 
            // cmdSave2
            // 
            this.cmdSave2.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave2.Image")));
            this.cmdSave2.Key = "cmdSave";
            this.cmdSave2.Name = "cmdSave2";
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "attached.ico");
            this.ImageList1.Images.SetKeyName(5, "internet.ico");
            this.ImageList1.Images.SetKeyName(6, "explorerBarItem25.Icon.ico");
            this.ImageList1.Images.SetKeyName(7, "page_edit.png");
            this.ImageList1.Images.SetKeyName(8, "page_red.png");
            this.ImageList1.Images.SetKeyName(9, "printer.png");
            this.ImageList1.Images.SetKeyName(10, "export.png");
            this.ImageList1.Images.SetKeyName(11, "Yes.png");
            // 
            // cmdFeedback2
            // 
            this.cmdFeedback2.Key = "cmdFeedback";
            this.cmdFeedback2.Name = "cmdFeedback2";
            this.cmdFeedback2.Text = "Nhận dữ liệu";
            // 
            // cmdFeedback3
            // 
            this.cmdFeedback3.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdFeedback3.Icon")));
            this.cmdFeedback3.Key = "cmdFeedback";
            this.cmdFeedback3.Name = "cmdFeedback3";
            // 
            // cmdResult
            // 
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "Kết quả xử lý";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult1.Image")));
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // VNACC_HopDongDangKyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 557);
            this.Controls.Add(this.cmdMain);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_HopDongDangKyForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Đăng ký hợp đồng";
            this.Load += new System.EventHandler(this.VNACC_HopDongDangKyForm_Load);
            this.Controls.SetChildIndex(this.cmdMain, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctrThoiHanTT.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrThoiHanTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrNgayHopDong.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrNgayHopDong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTriGia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHopDong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaDN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNguoiKhai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaNguoiKhai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctrNgayTiepNhan.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrNgayTiepNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoTiepNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHaiQuanTiepNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).EndInit();
            this.uiTabPage2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            this.cmdMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar2;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtMaNguoiKhai;
        private DevExpress.XtraEditors.TextEdit txtTenNguoiKhai;
        private DevExpress.XtraEditors.TextEdit txtMaDN;
        private DevExpress.XtraEditors.TextEdit txtTenDN;
        private DevExpress.XtraEditors.TextEdit txtSoTiepNhan;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtHaiQuanTiepNhan;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit ctrNgayTiepNhan;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TenTepTin;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Janus.Windows.GridEX.GridEX gridFile;
        private Janus.Windows.EditControls.UIButton btnAddTK;
        private DevExpress.XtraEditors.DateEdit ctrThoiHanTT;
        private DevExpress.XtraEditors.DateEdit ctrNgayHopDong;
        private DevExpress.XtraEditors.TextEdit txtSoHopDong;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtTongTriGia;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private Janus.Windows.EditControls.UIButton btnView;
        private DevExpress.XtraEditors.LabelControl lblTongDungLuong;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.GridEX.GridEX gridList;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar cmdMain;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar3;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend2;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave2;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback3;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback2;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;

    }
}