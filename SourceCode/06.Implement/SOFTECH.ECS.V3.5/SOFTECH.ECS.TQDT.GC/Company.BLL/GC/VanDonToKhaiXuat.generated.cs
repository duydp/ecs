using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.GC.BLL.GC
{
	public partial class VanDonToKhaiXuat : ICloneable
	{
		#region Properties.
		
		public long TKMD_ID { set; get; }
		public string SoVanDon { set; get; }
		public DateTime NgayVanDon { set; get; }
		public long IDHopDong { set; get; }
		public string Temp2 { set; get; }
		public string Temp3 { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<VanDonToKhaiXuat> ConvertToCollection(IDataReader reader)
		{
			IList<VanDonToKhaiXuat> collection = new List<VanDonToKhaiXuat>();
			while (reader.Read())
			{
				VanDonToKhaiXuat entity = new VanDonToKhaiXuat();
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Temp2"))) entity.Temp2 = reader.GetString(reader.GetOrdinal("Temp2"));
				if (!reader.IsDBNull(reader.GetOrdinal("Temp3"))) entity.Temp3 = reader.GetString(reader.GetOrdinal("Temp3"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<VanDonToKhaiXuat> collection, long tKMD_ID)
        {
            foreach (VanDonToKhaiXuat item in collection)
            {
                if (item.TKMD_ID == tKMD_ID)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_GC_BC08TT74_VanDonToKhaiXuat VALUES(@SoVanDon, @NgayVanDon, @IDHopDong, @Temp2, @Temp3)";
            string update = "UPDATE t_GC_BC08TT74_VanDonToKhaiXuat SET SoVanDon = @SoVanDon, NgayVanDon = @NgayVanDon, IDHopDong = @IDHopDong, Temp2 = @Temp2, Temp3 = @Temp3 WHERE TKMD_ID = @TKMD_ID";
            string delete = "DELETE FROM t_GC_BC08TT74_VanDonToKhaiXuat WHERE TKMD_ID = @TKMD_ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IDHopDong", SqlDbType.BigInt, "IDHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Temp2", SqlDbType.NVarChar, "Temp2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Temp3", SqlDbType.NVarChar, "Temp3", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IDHopDong", SqlDbType.BigInt, "IDHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Temp2", SqlDbType.NVarChar, "Temp2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Temp3", SqlDbType.NVarChar, "Temp3", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_GC_BC08TT74_VanDonToKhaiXuat VALUES(@SoVanDon, @NgayVanDon, @IDHopDong, @Temp2, @Temp3)";
            string update = "UPDATE t_GC_BC08TT74_VanDonToKhaiXuat SET SoVanDon = @SoVanDon, NgayVanDon = @NgayVanDon, IDHopDong = @IDHopDong, Temp2 = @Temp2, Temp3 = @Temp3 WHERE TKMD_ID = @TKMD_ID";
            string delete = "DELETE FROM t_GC_BC08TT74_VanDonToKhaiXuat WHERE TKMD_ID = @TKMD_ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IDHopDong", SqlDbType.BigInt, "IDHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Temp2", SqlDbType.NVarChar, "Temp2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Temp3", SqlDbType.NVarChar, "Temp3", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IDHopDong", SqlDbType.BigInt, "IDHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Temp2", SqlDbType.NVarChar, "Temp2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Temp3", SqlDbType.NVarChar, "Temp3", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VanDonToKhaiXuat Load(long tKMD_ID)
		{
			const string spName = "[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<VanDonToKhaiXuat> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<VanDonToKhaiXuat> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<VanDonToKhaiXuat> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<VanDonToKhaiXuat> SelectCollectionBy_TKMD_ID(long tKMD_ID)
		{
            IDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "p_GC_BC08TT74_VanDonToKhaiXuat_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertVanDonToKhaiXuat(string soVanDon, DateTime ngayVanDon, long iDHopDong, string temp2, string temp3)
		{
			VanDonToKhaiXuat entity = new VanDonToKhaiXuat();	
			entity.SoVanDon = soVanDon;
			entity.NgayVanDon = ngayVanDon;
			entity.IDHopDong = iDHopDong;
			entity.Temp2 = temp2;
			entity.Temp3 = temp3;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
			db.AddInParameter(dbCommand, "@Temp2", SqlDbType.NVarChar, Temp2);
			db.AddInParameter(dbCommand, "@Temp3", SqlDbType.NVarChar, Temp3);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				TKMD_ID = (long) db.GetParameterValue(dbCommand, "@TKMD_ID");
				return TKMD_ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				TKMD_ID = (long) db.GetParameterValue(dbCommand, "@TKMD_ID");
				return TKMD_ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<VanDonToKhaiXuat> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VanDonToKhaiXuat item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVanDonToKhaiXuat(long tKMD_ID, string soVanDon, DateTime ngayVanDon, long iDHopDong, string temp2, string temp3)
		{
			VanDonToKhaiXuat entity = new VanDonToKhaiXuat();			
			entity.TKMD_ID = tKMD_ID;
			entity.SoVanDon = soVanDon;
			entity.NgayVanDon = ngayVanDon;
			entity.IDHopDong = iDHopDong;
			entity.Temp2 = temp2;
			entity.Temp3 = temp3;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_GC_BC08TT74_VanDonToKhaiXuat_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
			db.AddInParameter(dbCommand, "@Temp2", SqlDbType.NVarChar, Temp2);
			db.AddInParameter(dbCommand, "@Temp3", SqlDbType.NVarChar, Temp3);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<VanDonToKhaiXuat> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VanDonToKhaiXuat item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVanDonToKhaiXuat(long tKMD_ID, string soVanDon, DateTime ngayVanDon, long iDHopDong, string temp2, string temp3)
		{
			VanDonToKhaiXuat entity = new VanDonToKhaiXuat();			
			entity.TKMD_ID = tKMD_ID;
			entity.SoVanDon = soVanDon;
			entity.NgayVanDon = ngayVanDon;
			entity.IDHopDong = iDHopDong;
			entity.Temp2 = temp2;
			entity.Temp3 = temp3;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
			db.AddInParameter(dbCommand, "@Temp2", SqlDbType.NVarChar, Temp2);
			db.AddInParameter(dbCommand, "@Temp3", SqlDbType.NVarChar, Temp3);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<VanDonToKhaiXuat> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VanDonToKhaiXuat item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVanDonToKhaiXuat(long tKMD_ID)
		{
			VanDonToKhaiXuat entity = new VanDonToKhaiXuat();
			entity.TKMD_ID = tKMD_ID;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<VanDonToKhaiXuat> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VanDonToKhaiXuat item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}