using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_TEADieuChinh : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public int LanDieuChinhGP_GCN { set; get; }
		public string ChungNhanDieuChinhSo { set; get; }
		public DateTime NgayChungNhanDieuChinh { set; get; }
		public string DieuChinhBoi { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_TEADieuChinh> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_TEADieuChinh> collection = new List<KDT_VNACCS_TEADieuChinh>();
			while (reader.Read())
			{
				KDT_VNACCS_TEADieuChinh entity = new KDT_VNACCS_TEADieuChinh();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinhGP_GCN"))) entity.LanDieuChinhGP_GCN = reader.GetInt32(reader.GetOrdinal("LanDieuChinhGP_GCN"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChungNhanDieuChinhSo"))) entity.ChungNhanDieuChinhSo = reader.GetString(reader.GetOrdinal("ChungNhanDieuChinhSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayChungNhanDieuChinh"))) entity.NgayChungNhanDieuChinh = reader.GetDateTime(reader.GetOrdinal("NgayChungNhanDieuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhBoi"))) entity.DieuChinhBoi = reader.GetString(reader.GetOrdinal("DieuChinhBoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_TEADieuChinh> collection, long id)
        {
            foreach (KDT_VNACCS_TEADieuChinh item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TEADieuChinh VALUES(@Master_ID, @LanDieuChinhGP_GCN, @ChungNhanDieuChinhSo, @NgayChungNhanDieuChinh, @DieuChinhBoi, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACCS_TEADieuChinh SET Master_ID = @Master_ID, LanDieuChinhGP_GCN = @LanDieuChinhGP_GCN, ChungNhanDieuChinhSo = @ChungNhanDieuChinhSo, NgayChungNhanDieuChinh = @NgayChungNhanDieuChinh, DieuChinhBoi = @DieuChinhBoi, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TEADieuChinh WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LanDieuChinhGP_GCN", SqlDbType.Int, "LanDieuChinhGP_GCN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChungNhanDieuChinhSo", SqlDbType.VarChar, "ChungNhanDieuChinhSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungNhanDieuChinh", SqlDbType.DateTime, "NgayChungNhanDieuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DieuChinhBoi", SqlDbType.NVarChar, "DieuChinhBoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LanDieuChinhGP_GCN", SqlDbType.Int, "LanDieuChinhGP_GCN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChungNhanDieuChinhSo", SqlDbType.VarChar, "ChungNhanDieuChinhSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungNhanDieuChinh", SqlDbType.DateTime, "NgayChungNhanDieuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DieuChinhBoi", SqlDbType.NVarChar, "DieuChinhBoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TEADieuChinh VALUES(@Master_ID, @LanDieuChinhGP_GCN, @ChungNhanDieuChinhSo, @NgayChungNhanDieuChinh, @DieuChinhBoi, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACCS_TEADieuChinh SET Master_ID = @Master_ID, LanDieuChinhGP_GCN = @LanDieuChinhGP_GCN, ChungNhanDieuChinhSo = @ChungNhanDieuChinhSo, NgayChungNhanDieuChinh = @NgayChungNhanDieuChinh, DieuChinhBoi = @DieuChinhBoi, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TEADieuChinh WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LanDieuChinhGP_GCN", SqlDbType.Int, "LanDieuChinhGP_GCN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChungNhanDieuChinhSo", SqlDbType.VarChar, "ChungNhanDieuChinhSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungNhanDieuChinh", SqlDbType.DateTime, "NgayChungNhanDieuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DieuChinhBoi", SqlDbType.NVarChar, "DieuChinhBoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LanDieuChinhGP_GCN", SqlDbType.Int, "LanDieuChinhGP_GCN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChungNhanDieuChinhSo", SqlDbType.VarChar, "ChungNhanDieuChinhSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungNhanDieuChinh", SqlDbType.DateTime, "NgayChungNhanDieuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DieuChinhBoi", SqlDbType.NVarChar, "DieuChinhBoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_TEADieuChinh Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TEADieuChinh_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_TEADieuChinh> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_TEADieuChinh> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_TEADieuChinh> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TEADieuChinh_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TEADieuChinh_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TEADieuChinh_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TEADieuChinh_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_TEADieuChinh(long master_ID, int lanDieuChinhGP_GCN, string chungNhanDieuChinhSo, DateTime ngayChungNhanDieuChinh, string dieuChinhBoi, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TEADieuChinh entity = new KDT_VNACCS_TEADieuChinh();	
			entity.Master_ID = master_ID;
			entity.LanDieuChinhGP_GCN = lanDieuChinhGP_GCN;
			entity.ChungNhanDieuChinhSo = chungNhanDieuChinhSo;
			entity.NgayChungNhanDieuChinh = ngayChungNhanDieuChinh;
			entity.DieuChinhBoi = dieuChinhBoi;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_TEADieuChinh_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@LanDieuChinhGP_GCN", SqlDbType.Int, LanDieuChinhGP_GCN);
			db.AddInParameter(dbCommand, "@ChungNhanDieuChinhSo", SqlDbType.VarChar, ChungNhanDieuChinhSo);
			db.AddInParameter(dbCommand, "@NgayChungNhanDieuChinh", SqlDbType.DateTime, NgayChungNhanDieuChinh.Year <= 1753 ? DBNull.Value : (object) NgayChungNhanDieuChinh);
			db.AddInParameter(dbCommand, "@DieuChinhBoi", SqlDbType.NVarChar, DieuChinhBoi);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_TEADieuChinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TEADieuChinh item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_TEADieuChinh(long id, long master_ID, int lanDieuChinhGP_GCN, string chungNhanDieuChinhSo, DateTime ngayChungNhanDieuChinh, string dieuChinhBoi, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TEADieuChinh entity = new KDT_VNACCS_TEADieuChinh();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.LanDieuChinhGP_GCN = lanDieuChinhGP_GCN;
			entity.ChungNhanDieuChinhSo = chungNhanDieuChinhSo;
			entity.NgayChungNhanDieuChinh = ngayChungNhanDieuChinh;
			entity.DieuChinhBoi = dieuChinhBoi;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_TEADieuChinh_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@LanDieuChinhGP_GCN", SqlDbType.Int, LanDieuChinhGP_GCN);
			db.AddInParameter(dbCommand, "@ChungNhanDieuChinhSo", SqlDbType.VarChar, ChungNhanDieuChinhSo);
			db.AddInParameter(dbCommand, "@NgayChungNhanDieuChinh", SqlDbType.DateTime, NgayChungNhanDieuChinh.Year <= 1753 ? DBNull.Value : (object) NgayChungNhanDieuChinh);
			db.AddInParameter(dbCommand, "@DieuChinhBoi", SqlDbType.NVarChar, DieuChinhBoi);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_TEADieuChinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TEADieuChinh item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_TEADieuChinh(long id, long master_ID, int lanDieuChinhGP_GCN, string chungNhanDieuChinhSo, DateTime ngayChungNhanDieuChinh, string dieuChinhBoi, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TEADieuChinh entity = new KDT_VNACCS_TEADieuChinh();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.LanDieuChinhGP_GCN = lanDieuChinhGP_GCN;
			entity.ChungNhanDieuChinhSo = chungNhanDieuChinhSo;
			entity.NgayChungNhanDieuChinh = ngayChungNhanDieuChinh;
			entity.DieuChinhBoi = dieuChinhBoi;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TEADieuChinh_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@LanDieuChinhGP_GCN", SqlDbType.Int, LanDieuChinhGP_GCN);
			db.AddInParameter(dbCommand, "@ChungNhanDieuChinhSo", SqlDbType.VarChar, ChungNhanDieuChinhSo);
			db.AddInParameter(dbCommand, "@NgayChungNhanDieuChinh", SqlDbType.DateTime, NgayChungNhanDieuChinh.Year <= 1753 ? DBNull.Value : (object) NgayChungNhanDieuChinh);
			db.AddInParameter(dbCommand, "@DieuChinhBoi", SqlDbType.NVarChar, DieuChinhBoi);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_TEADieuChinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TEADieuChinh item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_TEADieuChinh(long id)
		{
			KDT_VNACCS_TEADieuChinh entity = new KDT_VNACCS_TEADieuChinh();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TEADieuChinh_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TEADieuChinh_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_TEADieuChinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TEADieuChinh item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}