﻿namespace Company.Interface
{
    partial class VNACC_GiayPhep_SFA_HangForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grdHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_GiayPhep_SFA_HangForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.ucXuatXu = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucDonViTinhSoLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucDonViTinhKhoiLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.txtMaSoHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtKhoiLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grdHang = new Janus.Windows.GridEX.GridEX();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMaSoHangHoa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaSoHangHoa)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 329), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 329);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 305);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 305);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grdHang);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(580, 329);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Controls.Add(this.ucXuatXu);
            this.uiGroupBox1.Controls.Add(this.ucDonViTinhSoLuong);
            this.uiGroupBox1.Controls.Add(this.ucDonViTinhKhoiLuong);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.txtMaSoHangHoa);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.txtSoLuong);
            this.uiGroupBox1.Controls.Add(this.txtKhoiLuong);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.txtTenHang);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(580, 126);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(266, 95);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 14;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = global::Company.Interface.Properties.Resources.disk;
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.Location = new System.Drawing.Point(99, 95);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 12;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // ucXuatXu
            // 
            this.ucXuatXu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucXuatXu.Code = "";
            this.ucXuatXu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucXuatXu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucXuatXu.IsValidate = true;
            this.ucXuatXu.Location = new System.Drawing.Point(392, 38);
            this.ucXuatXu.Name = "ucXuatXu";
            this.ucXuatXu.Name_VN = "";
            this.ucXuatXu.ShowColumnCode = true;
            this.ucXuatXu.ShowColumnName = false;
            this.ucXuatXu.Size = new System.Drawing.Size(53, 26);
            this.ucXuatXu.TabIndex = 5;
            this.ucXuatXu.TagName = "";
            this.ucXuatXu.WhereCondition = "";
            this.ucXuatXu.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucDonViTinhSoLuong
            // 
            this.ucDonViTinhSoLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ucDonViTinhSoLuong.Code = "";
            this.ucDonViTinhSoLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhSoLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhSoLuong.IsValidate = true;
            this.ucDonViTinhSoLuong.Location = new System.Drawing.Point(256, 65);
            this.ucDonViTinhSoLuong.Name = "ucDonViTinhSoLuong";
            this.ucDonViTinhSoLuong.Name_VN = "";
            this.ucDonViTinhSoLuong.ShowColumnCode = true;
            this.ucDonViTinhSoLuong.ShowColumnName = false;
            this.ucDonViTinhSoLuong.Size = new System.Drawing.Size(53, 26);
            this.ucDonViTinhSoLuong.TabIndex = 8;
            this.ucDonViTinhSoLuong.TagName = "";
            this.ucDonViTinhSoLuong.WhereCondition = "";
            this.ucDonViTinhSoLuong.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucDonViTinhKhoiLuong
            // 
            this.ucDonViTinhKhoiLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucDonViTinhKhoiLuong.Code = "";
            this.ucDonViTinhKhoiLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhKhoiLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhKhoiLuong.IsValidate = true;
            this.ucDonViTinhKhoiLuong.Location = new System.Drawing.Point(519, 65);
            this.ucDonViTinhKhoiLuong.Name = "ucDonViTinhKhoiLuong";
            this.ucDonViTinhKhoiLuong.Name_VN = "";
            this.ucDonViTinhKhoiLuong.ShowColumnCode = true;
            this.ucDonViTinhKhoiLuong.ShowColumnName = false;
            this.ucDonViTinhKhoiLuong.Size = new System.Drawing.Size(53, 26);
            this.ucDonViTinhKhoiLuong.TabIndex = 11;
            this.ucDonViTinhKhoiLuong.TagName = "";
            this.ucDonViTinhKhoiLuong.WhereCondition = "";
            this.ucDonViTinhKhoiLuong.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // btnXoa
            // 
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(183, 95);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 13;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // txtMaSoHangHoa
            // 
            this.txtMaSoHangHoa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSoHangHoa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSoHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSoHangHoa.Location = new System.Drawing.Point(99, 41);
            this.txtMaSoHangHoa.MaxLength = 12;
            this.txtMaSoHangHoa.Name = "txtMaSoHangHoa";
            this.txtMaSoHangHoa.Size = new System.Drawing.Size(151, 21);
            this.txtMaSoHangHoa.TabIndex = 3;
            this.txtMaSoHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSoHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSoHangHoa.Leave += new System.EventHandler(this.txtMaSoHangHoa_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Mã số hàng hóa";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.DecimalDigits = 0;
            this.txtSoLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.Location = new System.Drawing.Point(99, 68);
            this.txtSoLuong.MaxLength = 15;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong.Size = new System.Drawing.Size(151, 21);
            this.txtSoLuong.TabIndex = 7;
            this.txtSoLuong.Text = "0";
            this.txtSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtKhoiLuong
            // 
            this.txtKhoiLuong.DecimalDigits = 3;
            this.txtKhoiLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtKhoiLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKhoiLuong.Location = new System.Drawing.Point(392, 68);
            this.txtKhoiLuong.MaxLength = 15;
            this.txtKhoiLuong.Name = "txtKhoiLuong";
            this.txtKhoiLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtKhoiLuong.Size = new System.Drawing.Size(121, 21);
            this.txtKhoiLuong.TabIndex = 10;
            this.txtKhoiLuong.Text = "0.000";
            this.txtKhoiLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKhoiLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtKhoiLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(44, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Số lượng";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(329, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Xuất xứ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(329, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Khối lượng";
            // 
            // txtTenHang
            // 
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(99, 14);
            this.txtTenHang.MaxLength = 255;
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(471, 21);
            this.txtTenHang.TabIndex = 1;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHang.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên hàng";
            // 
            // grdHang
            // 
            this.grdHang.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdHang.CardColumnHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.ColumnAutoResize = true;
            grdHang_DesignTimeLayout.LayoutString = resources.GetString("grdHang_DesignTimeLayout.LayoutString");
            this.grdHang.DesignTimeLayout = grdHang_DesignTimeLayout;
            this.grdHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grdHang.GroupByBoxVisible = false;
            this.grdHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.Location = new System.Drawing.Point(0, 126);
            this.grdHang.Name = "grdHang";
            this.grdHang.RecordNavigator = true;
            this.grdHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdHang.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdHang.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdHang.Size = new System.Drawing.Size(580, 203);
            this.grdHang.TabIndex = 1;
            this.grdHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdHang.VisualStyleManager = this.vsmMain;
            this.grdHang.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grdHang_RowDoubleClick);
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // rfvMaSoHangHoa
            // 
            this.rfvMaSoHangHoa.ControlToValidate = this.txtMaSoHangHoa;
            this.rfvMaSoHangHoa.ErrorMessage = "\"Mã số hàng hóa\" không được để trống.";
            this.rfvMaSoHangHoa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaSoHangHoa.Icon")));
            this.rfvMaSoHangHoa.Tag = "rfvMaSoHangHoa";
            // 
            // VNACC_GiayPhep_SFA_HangForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 335);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_GiayPhep_SFA_HangForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin hàng hóa";
            this.Load += new System.EventHandler(this.VNACC_GiayPhep_SFA_HangForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaSoHangHoa)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX grdHang;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtKhoiLuong;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHang;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSoHangHoa;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaSoHangHoa;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhKhoiLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucXuatXu;
        private System.Windows.Forms.Label label1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhSoLuong;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private System.Windows.Forms.Label label3;
    }
}