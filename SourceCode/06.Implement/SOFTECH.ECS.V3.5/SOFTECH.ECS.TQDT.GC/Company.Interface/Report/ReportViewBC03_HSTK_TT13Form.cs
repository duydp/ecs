﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC03_HSTK_TT13Form : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public DataSet dsSanPham;
        public DataSet dsSanPhamTemp;
        public DataTable dtBangSanPham;
        private BangKe03_HSTK_TT13 BC03;
        public HopDong HD = new HopDong();
        Company.GC.BLL.GC.NguyenPhuLieu NPL;
        public XRLabel label;

        public ReportViewBC03_HSTK_TT13Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            cbSort.Items.Add("Mã NPL", "MaHang asc");
            cbSort.Items.Add("Tên NPL", "TenHang asc");
            cbSort.Items.Add("Số tờ khai", "SoToKhai asc");

            loadReport();
        }

        private void loadReport()
        {

            BC03 = new BangKe03_HSTK_TT13();
            BC03.HD = HD;
            NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
            label = new XRLabel();

            DataSet dataSource = NPL.BaoCaoBC03HSTK_GC_TT117(this.HD.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

            if (dataSource != null && dataSource.Tables[0].Rows.Count == 0)
            {
                dataSource.Tables.Clear();
            }

            //this.BC03.report = this;
            for (int i = 0; i < dataSource.Tables.Count; i++)
            {
                cbPage.Items.Add(dataSource.Tables[i].TableName, dataSource.Tables[i]);
            }
            if (cbPage.Items.Count > 0)
                cbPage.SelectedIndex = 0;


        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {

            DataTable dt = (DataTable)cbPage.SelectedItem.Value;
            if (dt.Rows.Count == 0) return;
            if (!string.IsNullOrEmpty(cbSort.Text))
            {
                DataView dv = dt.DefaultView;
                dv.Sort = (string)cbSort.SelectedItem.Value;
                dt = dv.ToTable();
            }
            this.BC03.BindReport(dt);
            printControl1.PrintingSystem = this.BC03.PrintingSystem;
            this.BC03.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }
        }

        #region Old
        //public DataTable CreatSchemaDataSetNewRP02(int BangSo)
        //{
        //    DataTable dttemp = new DataTable("dtBangSanPham" + BangSo.ToString());
        //    DataColumn[] dcCol = new DataColumn[8];

        //    dcCol[0] = new DataColumn("TKXK", typeof(string));
        //    dcCol[0].Caption = "TKXK";

        //    int t = 1;
        //    for (int k = BangSo * 6; k < (BangSo + 1) * 6; k++)
        //    {

        //        if (k < soLuongSP)
        //        {
        //            string Ma = dsSanPhamTemp.Tables[0].Rows[k].ItemArray[0].ToString();
        //            string DVT_ID = SP.GetDVTByMa(Ma,this.HD.ID);

        //            /* DataSet tạm để lấy tên sản phẩm */
        //            DataSet dsSP = SP.GetSP(this.HD.ID, Ma);
        //            string Ten = "";
        //            if(dsSP.Tables[0].Rows.Count > 0)
        //                 Ten = dsSP.Tables[0].Rows[0].ItemArray[0].ToString();

        //            dcCol[t] = new DataColumn();
        //            dcCol[t].ColumnName = "Mã " + Ma;
        //            dcCol[t].DataType = typeof(decimal);
        //            dcCol[t].Caption = " Tên : " + Ten + "\r\n Mã : " + Ma + "\r\n ĐV tính : " + DonViTinh_GetName(DVT_ID);
        //            t++;
        //        }
        //        else
        //        {
        //            dcCol[t] = new DataColumn();
        //            dcCol[t].ColumnName = "";
        //            dcCol[t].DataType = typeof(decimal);
        //            dcCol[t].Caption = "";
        //            t++;
        //        }
        //    }

        //    dcCol[t] = new DataColumn();
        //    dcCol[t].ColumnName = "GhiChu";
        //    dcCol[t].DataType = typeof(string);
        //    dcCol[t].Caption = "GhiChu";
        //    dttemp.Columns.AddRange(dcCol);


        //    ToKhaiMauDichCollection TKMDCol = this.HD.GetTKXKBCSanPhamNew();

        //    foreach (ToKhaiMauDich TKMD in TKMDCol)
        //    {
        //        TKMD.LoadHMDCollection();
        //        DataRow drData = dttemp.NewRow();
        //        foreach (HangMauDich HMD in TKMD.HMDCollection)
        //        {
        //            drData[0] = TKMD.SoToKhai.ToString().Trim() + "/" + LoaiHinhMauDich_GetTenVT(TKMD.MaLoaiHinh).Trim() + "/" + TKMD.MaHaiQuan + "\r\n" + TKMD.NgayDangKy.ToShortDateString();
        //            for (int n = 1; n <= 6; n++)
        //            {
        //                string ColName = "Mã " + HMD.MaPhu;
        //                if (dttemp.Columns[n].ColumnName == ColName)
        //                    drData[n] = HMD.SoLuong;
        //            }
        //        }
        //        for (int n = 1; n <= 6; n++)
        //        {
        //            if (drData[n].ToString() != "")
        //            {
        //                dttemp.Rows.Add(drData);
        //                break;
        //            }
        //        }

        //    }

        //    ToKhaiChuyenTiepCollection TKCTCol = this.HD.GetTKCTXuatSP();

        //    foreach (ToKhaiChuyenTiep TKCT in TKCTCol)
        //    {
        //        TKCT.LoadHCTCollection();
        //        DataRow drData = dttemp.NewRow();
        //        foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
        //        {
        //            drData[0] = TKCT.SoToKhai.ToString().Trim() +"-" + TKCT.MaLoaiHinh.Trim() + "-" + DateTime.Parse(TKCT.NgayDangKy.ToString().Trim()).Year;
        //            for (int n = 1; n <= 6; n++)
        //            {
        //                string ColName = "Mã " + HCT.MaHang;
        //                if (dttemp.Columns[n].ColumnName == ColName)
        //                    drData[n] = HCT.SoLuong;
        //            }
        //        }
        //        for (int n = 1; n <= 6; n++)
        //        {
        //            if (drData[n].ToString() != "")
        //            {
        //                dttemp.Rows.Add(drData);
        //                break;
        //            }
        //        }

        //    }

        //    return dttemp;
        //}

        //public DataTable CreatSchemaDataSetNewRP02TT74(int BangSo)
        //{
        //    DataTable dttemp = new DataTable("dtBangSanPham" + BangSo.ToString());
        //    DataColumn[] dcCol = new DataColumn[9];

        //    dcCol[0] = new DataColumn("MaHang", typeof(string));
        //    dcCol[0].Caption = "MaHang";

        //    dcCol[1] = new DataColumn();
        //    dcCol[1].ColumnName = "DVTinh";
        //    dcCol[1].DataType = typeof(string);
        //    dcCol[1].Caption = "Đv tính";

        //    int t = 2;
        //    for (int k = BangSo * 5; k < (BangSo + 1) * 5; k++)
        //    {

        //        if (k < soLuongTK)
        //        {
        //            DataRow dr = dsToKhai.Tables[0].Rows[k];
        //            string soTK = dr.ItemArray[0].ToString();
        //            string ngayTK = Convert.ToDateTime(dr.ItemArray[2].ToString()).ToShortDateString();
        //            string loaiHinh = dr.ItemArray[1].ToString();
        //            string namDK = Convert.ToDateTime(ngayTK).Year.ToString();
        //            string maHQ = dr.ItemArray[4].ToString();
        //            dcCol[t] = new DataColumn();
        //            dcCol[t].ColumnName = "Số TK " + soTK + loaiHinh.Trim() + namDK;
        //            dcCol[t].DataType = typeof(decimal);
        //            if (loaiHinh.StartsWith("XGC"))
        //                dcCol[t].Caption = "Tờ khai \r\n Số: " + soTK + "/" + LoaiHinhMauDich_GetTenVT(loaiHinh).Trim() + "/" + maHQ + "  \r\n Ngày: " + ngayTK + "\r\n ";
        //            else
        //                dcCol[t].Caption = "Tờ khai \r\n Số: " + soTK + "-" + loaiHinh.Trim() + "  \r\n Ngày: " + ngayTK + "\r\n ";
        //            t++;
        //        }
        //        else
        //        {
        //            dcCol[t] = new DataColumn();
        //            dcCol[t].ColumnName = "";
        //            dcCol[t].DataType = typeof(decimal);
        //            dcCol[t].Caption = "";
        //            t++;
        //        }
        //    }

        //    dcCol[t] = new DataColumn();
        //    dcCol[t].ColumnName = "TongCong";
        //    dcCol[t].DataType = typeof(decimal);
        //    dcCol[t].Caption = "Tổng cộng";


        //    dcCol[t + 1] = new DataColumn();
        //    dcCol[t + 1].ColumnName = "GhiChu";
        //    dcCol[t + 1].DataType = typeof(string);
        //    dcCol[t + 1].Caption = "GhiChu";

        //    dttemp.Columns.AddRange(dcCol);

        //    //Company.GC.BLL.GC.SanPhamCollection spCol = this.HD.GetSP();
        //    //foreach (Company.GC.BLL.GC.SanPham sp in spCol)
        //    foreach (DataRow drSP in dsSanPhamTemp.Tables[0].Rows)
        //    {
        //        string maSP = drSP[0].ToString();
        //        DataSet dsSP = SP.GetSP(this.HD.ID, maSP);
        //        DataRow drData = dttemp.NewRow();
        //        drData[0] = maSP;
        //        drData[1] = DonViTinh_GetName(dsSP.Tables[0].Rows[0].ItemArray[1].ToString());
        //        for (int i = 2; i < 7; i++)
        //        {

        //            foreach (ToKhaiMauDich TKMD in TKMDCol)
        //            {
        //                string ColName = "Số TK " + TKMD.SoToKhai + TKMD.MaLoaiHinh.Trim() + TKMD.NgayDangKy.Year;
        //                if (dttemp.Columns[i].ColumnName == ColName)
        //                {
        //                    TKMD.LoadHMDCollection();
        //                    foreach (HangMauDich HMD in TKMD.HMDCollection)
        //                    {
        //                        if (HMD.MaPhu == maSP)
        //                        {
        //                            drData[i] = HMD.SoLuong;
        //                            break;
        //                        }
        //                    }
        //                    break;
        //                }
        //            }
        //            foreach (ToKhaiChuyenTiep TKCT in TKCTCol)
        //            {
        //                string ColName = "Số TK " + TKCT.SoToKhai + TKCT.MaLoaiHinh.Trim() + TKCT.NgayDangKy.Year;
        //                if (dttemp.Columns[i].ColumnName == ColName)
        //                {
        //                    TKCT.LoadHCTCollection();
        //                    foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
        //                    {
        //                        if (HCT.MaHang == maSP)
        //                        {
        //                            drData[i] = HCT.SoLuong;
        //                            break;
        //                        }
        //                    }
        //                    break;
        //                }
        //            }
        //        }
        //        dttemp.Rows.Add(drData);// addRow tất cả các SP
        //        /* 
        //         * chỉ addRow những dòng có dữ liệu
        //        for (int i = 2; i <= 7; i++)
        //        {
        //            if (drData[i].ToString() != "")
        //            {
        //                //drData[7] = Tong;
        //                dttemp.Rows.Add(drData);
        //                break;
        //            }
        //        }*/

        //    }
        //    if (this.BC02.Last)
        //    {
        //        foreach (DataRow dr in dttemp.Rows)
        //        {
        //            Decimal tongCong = 0;
        //            foreach (ToKhaiMauDich TKMD in TKMDCol)
        //            {
        //                TKMD.LoadHMDCollection();
        //                foreach (HangMauDich HMD in TKMD.HMDCollection)
        //                {
        //                    if (HMD.MaPhu.Trim() == dr[0].ToString().Trim())
        //                    {
        //                        tongCong += Decimal.Parse(HMD.SoLuong.ToString());
        //                        break;
        //                    }
        //                }
        //            }
        //            foreach (ToKhaiChuyenTiep TKCT in TKCTCol)
        //            {
        //                TKCT.LoadHCTCollection();
        //                foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
        //                {
        //                    if (HCT.MaHang.Trim() == dr[0].ToString().Trim())
        //                    {
        //                        tongCong += Decimal.Parse(HCT.SoLuong.ToString());
        //                        break;
        //                    }
        //                }
        //            }
        //            dr[7] = tongCong;
        //        }
        //    }

        //    return dttemp;
        //}
        #endregion

        private void btnPrivious_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex--;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex++;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.BC03.setText(this.label, txtName.Text);
            this.BC03.CreateDocument();
        }
        private void cbSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPage.Items.Count == 0) return;
            DataTable data = (DataTable)cbPage.SelectedItem.Value;
            if (data.Rows.Count == 0) return;

            DataView dv = data.DefaultView;
            dv.Sort = (string)cbSort.SelectedItem.Value;
            this.BC03.BindReport(dv.ToTable());
            printControl1.PrintingSystem = this.BC03.PrintingSystem;
            this.BC03.CreateDocument();
        }
    }
}