using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HangMauDich_KhaiBoSung_Thue : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMDBoSung_ID { set; get; }
		public string SoDong { set; get; }
		public string SoThuTuDongHangTrenToKhaiGoc { set; get; }
		public string MoTaHangHoaTruocKhiKhaiBoSung { set; get; }
		public string MoTaHangHoaSauKhiKhaiBoSung { set; get; }
		public string MaSoHangHoaTruocKhiKhaiBoSung { set; get; }
		public string MaSoHangHoaSauKhiKhaiBoSung { set; get; }
		public string MaNuocXuatXuTruocKhiKhaiBoSung { set; get; }
		public string MaNuocXuatXuSauKhiKhaiBoSung { set; get; }
		public decimal TriGiaTinhThueTruocKhiKhaiBoSung { set; get; }
		public decimal TriGiaTinhThueSauKhiKhaiBoSung { set; get; }
		public decimal SoLuongTinhThueTruocKhiKhaiBoSung { set; get; }
		public decimal SoLuongTinhThueSauKhiKhaiBoSung { set; get; }
		public string MaDonViTinhSoLuongTinhThueTruocKhaiBoSung { set; get; }
		public string MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung { set; get; }
		public string ThueSuatTruocKhiKhaiBoSung { set; get; }
		public string ThueSuatSauKhiKhaiBoSung { set; get; }
		public string SoTienThueTruocKhiKhaiBoSung { set; get; }
		public string SoTienThueSauKhiKhaiBoSung { set; get; }
		public string HienThiMienThueTruocKhiKhaiBoSung { set; get; }
		public string HienThiMienThueSauKhiKhaiBoSung { set; get; }
		public string HienThiSoTienTangGiamThueXuatNhapKhau { set; get; }
		public decimal SoTienTangGiamThue { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> collection = new List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue>();
			while (reader.Read())
			{
				KDT_VNACC_HangMauDich_KhaiBoSung_Thue entity = new KDT_VNACC_HangMauDich_KhaiBoSung_Thue();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMDBoSung_ID"))) entity.TKMDBoSung_ID = reader.GetInt64(reader.GetOrdinal("TKMDBoSung_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDong"))) entity.SoDong = reader.GetString(reader.GetOrdinal("SoDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuDongHangTrenToKhaiGoc"))) entity.SoThuTuDongHangTrenToKhaiGoc = reader.GetString(reader.GetOrdinal("SoThuTuDongHangTrenToKhaiGoc"));
				if (!reader.IsDBNull(reader.GetOrdinal("MoTaHangHoaTruocKhiKhaiBoSung"))) entity.MoTaHangHoaTruocKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("MoTaHangHoaTruocKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("MoTaHangHoaSauKhiKhaiBoSung"))) entity.MoTaHangHoaSauKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("MoTaHangHoaSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSoHangHoaTruocKhiKhaiBoSung"))) entity.MaSoHangHoaTruocKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("MaSoHangHoaTruocKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSoHangHoaSauKhiKhaiBoSung"))) entity.MaSoHangHoaSauKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("MaSoHangHoaSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXuatXuTruocKhiKhaiBoSung"))) entity.MaNuocXuatXuTruocKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("MaNuocXuatXuTruocKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXuatXuSauKhiKhaiBoSung"))) entity.MaNuocXuatXuSauKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("MaNuocXuatXuSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThueTruocKhiKhaiBoSung"))) entity.TriGiaTinhThueTruocKhiKhaiBoSung = reader.GetDecimal(reader.GetOrdinal("TriGiaTinhThueTruocKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThueSauKhiKhaiBoSung"))) entity.TriGiaTinhThueSauKhiKhaiBoSung = reader.GetDecimal(reader.GetOrdinal("TriGiaTinhThueSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTinhThueTruocKhiKhaiBoSung"))) entity.SoLuongTinhThueTruocKhiKhaiBoSung = reader.GetDecimal(reader.GetOrdinal("SoLuongTinhThueTruocKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTinhThueSauKhiKhaiBoSung"))) entity.SoLuongTinhThueSauKhiKhaiBoSung = reader.GetDecimal(reader.GetOrdinal("SoLuongTinhThueSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViTinhSoLuongTinhThueTruocKhaiBoSung"))) entity.MaDonViTinhSoLuongTinhThueTruocKhaiBoSung = reader.GetString(reader.GetOrdinal("MaDonViTinhSoLuongTinhThueTruocKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung"))) entity.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTruocKhiKhaiBoSung"))) entity.ThueSuatTruocKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("ThueSuatTruocKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatSauKhiKhaiBoSung"))) entity.ThueSuatSauKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("ThueSuatSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienThueTruocKhiKhaiBoSung"))) entity.SoTienThueTruocKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("SoTienThueTruocKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienThueSauKhiKhaiBoSung"))) entity.SoTienThueSauKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("SoTienThueSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThiMienThueTruocKhiKhaiBoSung"))) entity.HienThiMienThueTruocKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("HienThiMienThueTruocKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThiMienThueSauKhiKhaiBoSung"))) entity.HienThiMienThueSauKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("HienThiMienThueSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThiSoTienTangGiamThueXuatNhapKhau"))) entity.HienThiSoTienTangGiamThueXuatNhapKhau = reader.GetString(reader.GetOrdinal("HienThiSoTienTangGiamThueXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienTangGiamThue"))) entity.SoTienTangGiamThue = reader.GetDecimal(reader.GetOrdinal("SoTienTangGiamThue"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> collection, long id)
        {
            foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue VALUES(@TKMDBoSung_ID, @SoDong, @SoThuTuDongHangTrenToKhaiGoc, @MoTaHangHoaTruocKhiKhaiBoSung, @MoTaHangHoaSauKhiKhaiBoSung, @MaSoHangHoaTruocKhiKhaiBoSung, @MaSoHangHoaSauKhiKhaiBoSung, @MaNuocXuatXuTruocKhiKhaiBoSung, @MaNuocXuatXuSauKhiKhaiBoSung, @TriGiaTinhThueTruocKhiKhaiBoSung, @TriGiaTinhThueSauKhiKhaiBoSung, @SoLuongTinhThueTruocKhiKhaiBoSung, @SoLuongTinhThueSauKhiKhaiBoSung, @MaDonViTinhSoLuongTinhThueTruocKhaiBoSung, @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung, @ThueSuatTruocKhiKhaiBoSung, @ThueSuatSauKhiKhaiBoSung, @SoTienThueTruocKhiKhaiBoSung, @SoTienThueSauKhiKhaiBoSung, @HienThiMienThueTruocKhiKhaiBoSung, @HienThiMienThueSauKhiKhaiBoSung, @HienThiSoTienTangGiamThueXuatNhapKhau, @SoTienTangGiamThue)";
            string update = "UPDATE t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue SET TKMDBoSung_ID = @TKMDBoSung_ID, SoDong = @SoDong, SoThuTuDongHangTrenToKhaiGoc = @SoThuTuDongHangTrenToKhaiGoc, MoTaHangHoaTruocKhiKhaiBoSung = @MoTaHangHoaTruocKhiKhaiBoSung, MoTaHangHoaSauKhiKhaiBoSung = @MoTaHangHoaSauKhiKhaiBoSung, MaSoHangHoaTruocKhiKhaiBoSung = @MaSoHangHoaTruocKhiKhaiBoSung, MaSoHangHoaSauKhiKhaiBoSung = @MaSoHangHoaSauKhiKhaiBoSung, MaNuocXuatXuTruocKhiKhaiBoSung = @MaNuocXuatXuTruocKhiKhaiBoSung, MaNuocXuatXuSauKhiKhaiBoSung = @MaNuocXuatXuSauKhiKhaiBoSung, TriGiaTinhThueTruocKhiKhaiBoSung = @TriGiaTinhThueTruocKhiKhaiBoSung, TriGiaTinhThueSauKhiKhaiBoSung = @TriGiaTinhThueSauKhiKhaiBoSung, SoLuongTinhThueTruocKhiKhaiBoSung = @SoLuongTinhThueTruocKhiKhaiBoSung, SoLuongTinhThueSauKhiKhaiBoSung = @SoLuongTinhThueSauKhiKhaiBoSung, MaDonViTinhSoLuongTinhThueTruocKhaiBoSung = @MaDonViTinhSoLuongTinhThueTruocKhaiBoSung, MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung, ThueSuatTruocKhiKhaiBoSung = @ThueSuatTruocKhiKhaiBoSung, ThueSuatSauKhiKhaiBoSung = @ThueSuatSauKhiKhaiBoSung, SoTienThueTruocKhiKhaiBoSung = @SoTienThueTruocKhiKhaiBoSung, SoTienThueSauKhiKhaiBoSung = @SoTienThueSauKhiKhaiBoSung, HienThiMienThueTruocKhiKhaiBoSung = @HienThiMienThueTruocKhiKhaiBoSung, HienThiMienThueSauKhiKhaiBoSung = @HienThiMienThueSauKhiKhaiBoSung, HienThiSoTienTangGiamThueXuatNhapKhau = @HienThiSoTienTangGiamThueXuatNhapKhau, SoTienTangGiamThue = @SoTienTangGiamThue WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, "TKMDBoSung_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuTuDongHangTrenToKhaiGoc", SqlDbType.VarChar, "SoThuTuDongHangTrenToKhaiGoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTaHangHoaTruocKhiKhaiBoSung", SqlDbType.NVarChar, "MoTaHangHoaTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTaHangHoaSauKhiKhaiBoSung", SqlDbType.NVarChar, "MoTaHangHoaSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHangHoaTruocKhiKhaiBoSung", SqlDbType.VarChar, "MaSoHangHoaTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHangHoaSauKhiKhaiBoSung", SqlDbType.VarChar, "MaSoHangHoaSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocXuatXuTruocKhiKhaiBoSung", SqlDbType.VarChar, "MaNuocXuatXuTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocXuatXuSauKhiKhaiBoSung", SqlDbType.VarChar, "MaNuocXuatXuSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, "TriGiaTinhThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, "TriGiaTinhThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, "SoLuongTinhThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, "SoLuongTinhThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueTruocKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTruocKhiKhaiBoSung", SqlDbType.VarChar, "ThueSuatTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatSauKhiKhaiBoSung", SqlDbType.VarChar, "ThueSuatSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, "SoTienThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThueSauKhiKhaiBoSung", SqlDbType.VarChar, "SoTienThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiMienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiMienThueSauKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, "HienThiSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienTangGiamThue", SqlDbType.Decimal, "SoTienTangGiamThue", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, "TKMDBoSung_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuTuDongHangTrenToKhaiGoc", SqlDbType.VarChar, "SoThuTuDongHangTrenToKhaiGoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTaHangHoaTruocKhiKhaiBoSung", SqlDbType.NVarChar, "MoTaHangHoaTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTaHangHoaSauKhiKhaiBoSung", SqlDbType.NVarChar, "MoTaHangHoaSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHangHoaTruocKhiKhaiBoSung", SqlDbType.VarChar, "MaSoHangHoaTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHangHoaSauKhiKhaiBoSung", SqlDbType.VarChar, "MaSoHangHoaSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocXuatXuTruocKhiKhaiBoSung", SqlDbType.VarChar, "MaNuocXuatXuTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocXuatXuSauKhiKhaiBoSung", SqlDbType.VarChar, "MaNuocXuatXuSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, "TriGiaTinhThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, "TriGiaTinhThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, "SoLuongTinhThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, "SoLuongTinhThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueTruocKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTruocKhiKhaiBoSung", SqlDbType.VarChar, "ThueSuatTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatSauKhiKhaiBoSung", SqlDbType.VarChar, "ThueSuatSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, "SoTienThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThueSauKhiKhaiBoSung", SqlDbType.VarChar, "SoTienThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiMienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiMienThueSauKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, "HienThiSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienTangGiamThue", SqlDbType.Decimal, "SoTienTangGiamThue", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue VALUES(@TKMDBoSung_ID, @SoDong, @SoThuTuDongHangTrenToKhaiGoc, @MoTaHangHoaTruocKhiKhaiBoSung, @MoTaHangHoaSauKhiKhaiBoSung, @MaSoHangHoaTruocKhiKhaiBoSung, @MaSoHangHoaSauKhiKhaiBoSung, @MaNuocXuatXuTruocKhiKhaiBoSung, @MaNuocXuatXuSauKhiKhaiBoSung, @TriGiaTinhThueTruocKhiKhaiBoSung, @TriGiaTinhThueSauKhiKhaiBoSung, @SoLuongTinhThueTruocKhiKhaiBoSung, @SoLuongTinhThueSauKhiKhaiBoSung, @MaDonViTinhSoLuongTinhThueTruocKhaiBoSung, @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung, @ThueSuatTruocKhiKhaiBoSung, @ThueSuatSauKhiKhaiBoSung, @SoTienThueTruocKhiKhaiBoSung, @SoTienThueSauKhiKhaiBoSung, @HienThiMienThueTruocKhiKhaiBoSung, @HienThiMienThueSauKhiKhaiBoSung, @HienThiSoTienTangGiamThueXuatNhapKhau, @SoTienTangGiamThue)";
            string update = "UPDATE t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue SET TKMDBoSung_ID = @TKMDBoSung_ID, SoDong = @SoDong, SoThuTuDongHangTrenToKhaiGoc = @SoThuTuDongHangTrenToKhaiGoc, MoTaHangHoaTruocKhiKhaiBoSung = @MoTaHangHoaTruocKhiKhaiBoSung, MoTaHangHoaSauKhiKhaiBoSung = @MoTaHangHoaSauKhiKhaiBoSung, MaSoHangHoaTruocKhiKhaiBoSung = @MaSoHangHoaTruocKhiKhaiBoSung, MaSoHangHoaSauKhiKhaiBoSung = @MaSoHangHoaSauKhiKhaiBoSung, MaNuocXuatXuTruocKhiKhaiBoSung = @MaNuocXuatXuTruocKhiKhaiBoSung, MaNuocXuatXuSauKhiKhaiBoSung = @MaNuocXuatXuSauKhiKhaiBoSung, TriGiaTinhThueTruocKhiKhaiBoSung = @TriGiaTinhThueTruocKhiKhaiBoSung, TriGiaTinhThueSauKhiKhaiBoSung = @TriGiaTinhThueSauKhiKhaiBoSung, SoLuongTinhThueTruocKhiKhaiBoSung = @SoLuongTinhThueTruocKhiKhaiBoSung, SoLuongTinhThueSauKhiKhaiBoSung = @SoLuongTinhThueSauKhiKhaiBoSung, MaDonViTinhSoLuongTinhThueTruocKhaiBoSung = @MaDonViTinhSoLuongTinhThueTruocKhaiBoSung, MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung, ThueSuatTruocKhiKhaiBoSung = @ThueSuatTruocKhiKhaiBoSung, ThueSuatSauKhiKhaiBoSung = @ThueSuatSauKhiKhaiBoSung, SoTienThueTruocKhiKhaiBoSung = @SoTienThueTruocKhiKhaiBoSung, SoTienThueSauKhiKhaiBoSung = @SoTienThueSauKhiKhaiBoSung, HienThiMienThueTruocKhiKhaiBoSung = @HienThiMienThueTruocKhiKhaiBoSung, HienThiMienThueSauKhiKhaiBoSung = @HienThiMienThueSauKhiKhaiBoSung, HienThiSoTienTangGiamThueXuatNhapKhau = @HienThiSoTienTangGiamThueXuatNhapKhau, SoTienTangGiamThue = @SoTienTangGiamThue WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, "TKMDBoSung_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuTuDongHangTrenToKhaiGoc", SqlDbType.VarChar, "SoThuTuDongHangTrenToKhaiGoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTaHangHoaTruocKhiKhaiBoSung", SqlDbType.NVarChar, "MoTaHangHoaTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTaHangHoaSauKhiKhaiBoSung", SqlDbType.NVarChar, "MoTaHangHoaSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHangHoaTruocKhiKhaiBoSung", SqlDbType.VarChar, "MaSoHangHoaTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHangHoaSauKhiKhaiBoSung", SqlDbType.VarChar, "MaSoHangHoaSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocXuatXuTruocKhiKhaiBoSung", SqlDbType.VarChar, "MaNuocXuatXuTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocXuatXuSauKhiKhaiBoSung", SqlDbType.VarChar, "MaNuocXuatXuSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, "TriGiaTinhThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, "TriGiaTinhThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, "SoLuongTinhThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, "SoLuongTinhThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueTruocKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTruocKhiKhaiBoSung", SqlDbType.VarChar, "ThueSuatTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatSauKhiKhaiBoSung", SqlDbType.VarChar, "ThueSuatSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, "SoTienThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThueSauKhiKhaiBoSung", SqlDbType.VarChar, "SoTienThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiMienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiMienThueSauKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, "HienThiSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienTangGiamThue", SqlDbType.Decimal, "SoTienTangGiamThue", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, "TKMDBoSung_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuTuDongHangTrenToKhaiGoc", SqlDbType.VarChar, "SoThuTuDongHangTrenToKhaiGoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTaHangHoaTruocKhiKhaiBoSung", SqlDbType.NVarChar, "MoTaHangHoaTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTaHangHoaSauKhiKhaiBoSung", SqlDbType.NVarChar, "MoTaHangHoaSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHangHoaTruocKhiKhaiBoSung", SqlDbType.VarChar, "MaSoHangHoaTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHangHoaSauKhiKhaiBoSung", SqlDbType.VarChar, "MaSoHangHoaSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocXuatXuTruocKhiKhaiBoSung", SqlDbType.VarChar, "MaNuocXuatXuTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocXuatXuSauKhiKhaiBoSung", SqlDbType.VarChar, "MaNuocXuatXuSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, "TriGiaTinhThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, "TriGiaTinhThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, "SoLuongTinhThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, "SoLuongTinhThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueTruocKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTruocKhiKhaiBoSung", SqlDbType.VarChar, "ThueSuatTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatSauKhiKhaiBoSung", SqlDbType.VarChar, "ThueSuatSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, "SoTienThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThueSauKhiKhaiBoSung", SqlDbType.VarChar, "SoTienThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiMienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiMienThueSauKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, "HienThiSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienTangGiamThue", SqlDbType.Decimal, "SoTienTangGiamThue", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_HangMauDich_KhaiBoSung_Thue Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> SelectCollectionBy_TKMDBoSung_ID(long tKMDBoSung_ID)
		{
            IDataReader reader = SelectReaderBy_TKMDBoSung_ID(tKMDBoSung_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TKMDBoSung_ID(long tKMDBoSung_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, tKMDBoSung_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TKMDBoSung_ID(long tKMDBoSung_ID)
		{
			const string spName = "p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, tKMDBoSung_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_HangMauDich_KhaiBoSung_Thue(long tKMDBoSung_ID, string soDong, string soThuTuDongHangTrenToKhaiGoc, string moTaHangHoaTruocKhiKhaiBoSung, string moTaHangHoaSauKhiKhaiBoSung, string maSoHangHoaTruocKhiKhaiBoSung, string maSoHangHoaSauKhiKhaiBoSung, string maNuocXuatXuTruocKhiKhaiBoSung, string maNuocXuatXuSauKhiKhaiBoSung, decimal triGiaTinhThueTruocKhiKhaiBoSung, decimal triGiaTinhThueSauKhiKhaiBoSung, decimal soLuongTinhThueTruocKhiKhaiBoSung, decimal soLuongTinhThueSauKhiKhaiBoSung, string maDonViTinhSoLuongTinhThueTruocKhaiBoSung, string maDonViTinhSoLuongTinhThueSauKhiKhaiBoSung, string thueSuatTruocKhiKhaiBoSung, string thueSuatSauKhiKhaiBoSung, string soTienThueTruocKhiKhaiBoSung, string soTienThueSauKhiKhaiBoSung, string hienThiMienThueTruocKhiKhaiBoSung, string hienThiMienThueSauKhiKhaiBoSung, string hienThiSoTienTangGiamThueXuatNhapKhau, decimal soTienTangGiamThue)
		{
			KDT_VNACC_HangMauDich_KhaiBoSung_Thue entity = new KDT_VNACC_HangMauDich_KhaiBoSung_Thue();	
			entity.TKMDBoSung_ID = tKMDBoSung_ID;
			entity.SoDong = soDong;
			entity.SoThuTuDongHangTrenToKhaiGoc = soThuTuDongHangTrenToKhaiGoc;
			entity.MoTaHangHoaTruocKhiKhaiBoSung = moTaHangHoaTruocKhiKhaiBoSung;
			entity.MoTaHangHoaSauKhiKhaiBoSung = moTaHangHoaSauKhiKhaiBoSung;
			entity.MaSoHangHoaTruocKhiKhaiBoSung = maSoHangHoaTruocKhiKhaiBoSung;
			entity.MaSoHangHoaSauKhiKhaiBoSung = maSoHangHoaSauKhiKhaiBoSung;
			entity.MaNuocXuatXuTruocKhiKhaiBoSung = maNuocXuatXuTruocKhiKhaiBoSung;
			entity.MaNuocXuatXuSauKhiKhaiBoSung = maNuocXuatXuSauKhiKhaiBoSung;
			entity.TriGiaTinhThueTruocKhiKhaiBoSung = triGiaTinhThueTruocKhiKhaiBoSung;
			entity.TriGiaTinhThueSauKhiKhaiBoSung = triGiaTinhThueSauKhiKhaiBoSung;
			entity.SoLuongTinhThueTruocKhiKhaiBoSung = soLuongTinhThueTruocKhiKhaiBoSung;
			entity.SoLuongTinhThueSauKhiKhaiBoSung = soLuongTinhThueSauKhiKhaiBoSung;
			entity.MaDonViTinhSoLuongTinhThueTruocKhaiBoSung = maDonViTinhSoLuongTinhThueTruocKhaiBoSung;
			entity.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung = maDonViTinhSoLuongTinhThueSauKhiKhaiBoSung;
			entity.ThueSuatTruocKhiKhaiBoSung = thueSuatTruocKhiKhaiBoSung;
			entity.ThueSuatSauKhiKhaiBoSung = thueSuatSauKhiKhaiBoSung;
			entity.SoTienThueTruocKhiKhaiBoSung = soTienThueTruocKhiKhaiBoSung;
			entity.SoTienThueSauKhiKhaiBoSung = soTienThueSauKhiKhaiBoSung;
			entity.HienThiMienThueTruocKhiKhaiBoSung = hienThiMienThueTruocKhiKhaiBoSung;
			entity.HienThiMienThueSauKhiKhaiBoSung = hienThiMienThueSauKhiKhaiBoSung;
			entity.HienThiSoTienTangGiamThueXuatNhapKhau = hienThiSoTienTangGiamThueXuatNhapKhau;
			entity.SoTienTangGiamThue = soTienTangGiamThue;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, TKMDBoSung_ID);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@SoThuTuDongHangTrenToKhaiGoc", SqlDbType.VarChar, SoThuTuDongHangTrenToKhaiGoc);
			db.AddInParameter(dbCommand, "@MoTaHangHoaTruocKhiKhaiBoSung", SqlDbType.NVarChar, MoTaHangHoaTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MoTaHangHoaSauKhiKhaiBoSung", SqlDbType.NVarChar, MoTaHangHoaSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaSoHangHoaTruocKhiKhaiBoSung", SqlDbType.VarChar, MaSoHangHoaTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaSoHangHoaSauKhiKhaiBoSung", SqlDbType.VarChar, MaSoHangHoaSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaNuocXuatXuTruocKhiKhaiBoSung", SqlDbType.VarChar, MaNuocXuatXuTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaNuocXuatXuSauKhiKhaiBoSung", SqlDbType.VarChar, MaNuocXuatXuSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, TriGiaTinhThueTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, TriGiaTinhThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, SoLuongTinhThueTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, SoLuongTinhThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung", SqlDbType.VarChar, MaDonViTinhSoLuongTinhThueTruocKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.VarChar, MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@ThueSuatTruocKhiKhaiBoSung", SqlDbType.VarChar, ThueSuatTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@ThueSuatSauKhiKhaiBoSung", SqlDbType.VarChar, ThueSuatSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoTienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, SoTienThueTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoTienThueSauKhiKhaiBoSung", SqlDbType.VarChar, SoTienThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiMienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, HienThiMienThueTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiMienThueSauKhiKhaiBoSung", SqlDbType.VarChar, HienThiMienThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, HienThiSoTienTangGiamThueXuatNhapKhau);
			db.AddInParameter(dbCommand, "@SoTienTangGiamThue", SqlDbType.Decimal, SoTienTangGiamThue);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_HangMauDich_KhaiBoSung_Thue(long id, long tKMDBoSung_ID, string soDong, string soThuTuDongHangTrenToKhaiGoc, string moTaHangHoaTruocKhiKhaiBoSung, string moTaHangHoaSauKhiKhaiBoSung, string maSoHangHoaTruocKhiKhaiBoSung, string maSoHangHoaSauKhiKhaiBoSung, string maNuocXuatXuTruocKhiKhaiBoSung, string maNuocXuatXuSauKhiKhaiBoSung, decimal triGiaTinhThueTruocKhiKhaiBoSung, decimal triGiaTinhThueSauKhiKhaiBoSung, decimal soLuongTinhThueTruocKhiKhaiBoSung, decimal soLuongTinhThueSauKhiKhaiBoSung, string maDonViTinhSoLuongTinhThueTruocKhaiBoSung, string maDonViTinhSoLuongTinhThueSauKhiKhaiBoSung, string thueSuatTruocKhiKhaiBoSung, string thueSuatSauKhiKhaiBoSung, string soTienThueTruocKhiKhaiBoSung, string soTienThueSauKhiKhaiBoSung, string hienThiMienThueTruocKhiKhaiBoSung, string hienThiMienThueSauKhiKhaiBoSung, string hienThiSoTienTangGiamThueXuatNhapKhau, decimal soTienTangGiamThue)
		{
			KDT_VNACC_HangMauDich_KhaiBoSung_Thue entity = new KDT_VNACC_HangMauDich_KhaiBoSung_Thue();			
			entity.ID = id;
			entity.TKMDBoSung_ID = tKMDBoSung_ID;
			entity.SoDong = soDong;
			entity.SoThuTuDongHangTrenToKhaiGoc = soThuTuDongHangTrenToKhaiGoc;
			entity.MoTaHangHoaTruocKhiKhaiBoSung = moTaHangHoaTruocKhiKhaiBoSung;
			entity.MoTaHangHoaSauKhiKhaiBoSung = moTaHangHoaSauKhiKhaiBoSung;
			entity.MaSoHangHoaTruocKhiKhaiBoSung = maSoHangHoaTruocKhiKhaiBoSung;
			entity.MaSoHangHoaSauKhiKhaiBoSung = maSoHangHoaSauKhiKhaiBoSung;
			entity.MaNuocXuatXuTruocKhiKhaiBoSung = maNuocXuatXuTruocKhiKhaiBoSung;
			entity.MaNuocXuatXuSauKhiKhaiBoSung = maNuocXuatXuSauKhiKhaiBoSung;
			entity.TriGiaTinhThueTruocKhiKhaiBoSung = triGiaTinhThueTruocKhiKhaiBoSung;
			entity.TriGiaTinhThueSauKhiKhaiBoSung = triGiaTinhThueSauKhiKhaiBoSung;
			entity.SoLuongTinhThueTruocKhiKhaiBoSung = soLuongTinhThueTruocKhiKhaiBoSung;
			entity.SoLuongTinhThueSauKhiKhaiBoSung = soLuongTinhThueSauKhiKhaiBoSung;
			entity.MaDonViTinhSoLuongTinhThueTruocKhaiBoSung = maDonViTinhSoLuongTinhThueTruocKhaiBoSung;
			entity.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung = maDonViTinhSoLuongTinhThueSauKhiKhaiBoSung;
			entity.ThueSuatTruocKhiKhaiBoSung = thueSuatTruocKhiKhaiBoSung;
			entity.ThueSuatSauKhiKhaiBoSung = thueSuatSauKhiKhaiBoSung;
			entity.SoTienThueTruocKhiKhaiBoSung = soTienThueTruocKhiKhaiBoSung;
			entity.SoTienThueSauKhiKhaiBoSung = soTienThueSauKhiKhaiBoSung;
			entity.HienThiMienThueTruocKhiKhaiBoSung = hienThiMienThueTruocKhiKhaiBoSung;
			entity.HienThiMienThueSauKhiKhaiBoSung = hienThiMienThueSauKhiKhaiBoSung;
			entity.HienThiSoTienTangGiamThueXuatNhapKhau = hienThiSoTienTangGiamThueXuatNhapKhau;
			entity.SoTienTangGiamThue = soTienTangGiamThue;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, TKMDBoSung_ID);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@SoThuTuDongHangTrenToKhaiGoc", SqlDbType.VarChar, SoThuTuDongHangTrenToKhaiGoc);
			db.AddInParameter(dbCommand, "@MoTaHangHoaTruocKhiKhaiBoSung", SqlDbType.NVarChar, MoTaHangHoaTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MoTaHangHoaSauKhiKhaiBoSung", SqlDbType.NVarChar, MoTaHangHoaSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaSoHangHoaTruocKhiKhaiBoSung", SqlDbType.VarChar, MaSoHangHoaTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaSoHangHoaSauKhiKhaiBoSung", SqlDbType.VarChar, MaSoHangHoaSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaNuocXuatXuTruocKhiKhaiBoSung", SqlDbType.VarChar, MaNuocXuatXuTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaNuocXuatXuSauKhiKhaiBoSung", SqlDbType.VarChar, MaNuocXuatXuSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, TriGiaTinhThueTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, TriGiaTinhThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, SoLuongTinhThueTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, SoLuongTinhThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung", SqlDbType.VarChar, MaDonViTinhSoLuongTinhThueTruocKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.VarChar, MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@ThueSuatTruocKhiKhaiBoSung", SqlDbType.VarChar, ThueSuatTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@ThueSuatSauKhiKhaiBoSung", SqlDbType.VarChar, ThueSuatSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoTienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, SoTienThueTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoTienThueSauKhiKhaiBoSung", SqlDbType.VarChar, SoTienThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiMienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, HienThiMienThueTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiMienThueSauKhiKhaiBoSung", SqlDbType.VarChar, HienThiMienThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, HienThiSoTienTangGiamThueXuatNhapKhau);
			db.AddInParameter(dbCommand, "@SoTienTangGiamThue", SqlDbType.Decimal, SoTienTangGiamThue);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_HangMauDich_KhaiBoSung_Thue(long id, long tKMDBoSung_ID, string soDong, string soThuTuDongHangTrenToKhaiGoc, string moTaHangHoaTruocKhiKhaiBoSung, string moTaHangHoaSauKhiKhaiBoSung, string maSoHangHoaTruocKhiKhaiBoSung, string maSoHangHoaSauKhiKhaiBoSung, string maNuocXuatXuTruocKhiKhaiBoSung, string maNuocXuatXuSauKhiKhaiBoSung, decimal triGiaTinhThueTruocKhiKhaiBoSung, decimal triGiaTinhThueSauKhiKhaiBoSung, decimal soLuongTinhThueTruocKhiKhaiBoSung, decimal soLuongTinhThueSauKhiKhaiBoSung, string maDonViTinhSoLuongTinhThueTruocKhaiBoSung, string maDonViTinhSoLuongTinhThueSauKhiKhaiBoSung, string thueSuatTruocKhiKhaiBoSung, string thueSuatSauKhiKhaiBoSung, string soTienThueTruocKhiKhaiBoSung, string soTienThueSauKhiKhaiBoSung, string hienThiMienThueTruocKhiKhaiBoSung, string hienThiMienThueSauKhiKhaiBoSung, string hienThiSoTienTangGiamThueXuatNhapKhau, decimal soTienTangGiamThue)
		{
			KDT_VNACC_HangMauDich_KhaiBoSung_Thue entity = new KDT_VNACC_HangMauDich_KhaiBoSung_Thue();			
			entity.ID = id;
			entity.TKMDBoSung_ID = tKMDBoSung_ID;
			entity.SoDong = soDong;
			entity.SoThuTuDongHangTrenToKhaiGoc = soThuTuDongHangTrenToKhaiGoc;
			entity.MoTaHangHoaTruocKhiKhaiBoSung = moTaHangHoaTruocKhiKhaiBoSung;
			entity.MoTaHangHoaSauKhiKhaiBoSung = moTaHangHoaSauKhiKhaiBoSung;
			entity.MaSoHangHoaTruocKhiKhaiBoSung = maSoHangHoaTruocKhiKhaiBoSung;
			entity.MaSoHangHoaSauKhiKhaiBoSung = maSoHangHoaSauKhiKhaiBoSung;
			entity.MaNuocXuatXuTruocKhiKhaiBoSung = maNuocXuatXuTruocKhiKhaiBoSung;
			entity.MaNuocXuatXuSauKhiKhaiBoSung = maNuocXuatXuSauKhiKhaiBoSung;
			entity.TriGiaTinhThueTruocKhiKhaiBoSung = triGiaTinhThueTruocKhiKhaiBoSung;
			entity.TriGiaTinhThueSauKhiKhaiBoSung = triGiaTinhThueSauKhiKhaiBoSung;
			entity.SoLuongTinhThueTruocKhiKhaiBoSung = soLuongTinhThueTruocKhiKhaiBoSung;
			entity.SoLuongTinhThueSauKhiKhaiBoSung = soLuongTinhThueSauKhiKhaiBoSung;
			entity.MaDonViTinhSoLuongTinhThueTruocKhaiBoSung = maDonViTinhSoLuongTinhThueTruocKhaiBoSung;
			entity.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung = maDonViTinhSoLuongTinhThueSauKhiKhaiBoSung;
			entity.ThueSuatTruocKhiKhaiBoSung = thueSuatTruocKhiKhaiBoSung;
			entity.ThueSuatSauKhiKhaiBoSung = thueSuatSauKhiKhaiBoSung;
			entity.SoTienThueTruocKhiKhaiBoSung = soTienThueTruocKhiKhaiBoSung;
			entity.SoTienThueSauKhiKhaiBoSung = soTienThueSauKhiKhaiBoSung;
			entity.HienThiMienThueTruocKhiKhaiBoSung = hienThiMienThueTruocKhiKhaiBoSung;
			entity.HienThiMienThueSauKhiKhaiBoSung = hienThiMienThueSauKhiKhaiBoSung;
			entity.HienThiSoTienTangGiamThueXuatNhapKhau = hienThiSoTienTangGiamThueXuatNhapKhau;
			entity.SoTienTangGiamThue = soTienTangGiamThue;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, TKMDBoSung_ID);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@SoThuTuDongHangTrenToKhaiGoc", SqlDbType.VarChar, SoThuTuDongHangTrenToKhaiGoc);
			db.AddInParameter(dbCommand, "@MoTaHangHoaTruocKhiKhaiBoSung", SqlDbType.NVarChar, MoTaHangHoaTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MoTaHangHoaSauKhiKhaiBoSung", SqlDbType.NVarChar, MoTaHangHoaSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaSoHangHoaTruocKhiKhaiBoSung", SqlDbType.VarChar, MaSoHangHoaTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaSoHangHoaSauKhiKhaiBoSung", SqlDbType.VarChar, MaSoHangHoaSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaNuocXuatXuTruocKhiKhaiBoSung", SqlDbType.VarChar, MaNuocXuatXuTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaNuocXuatXuSauKhiKhaiBoSung", SqlDbType.VarChar, MaNuocXuatXuSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, TriGiaTinhThueTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, TriGiaTinhThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueTruocKhiKhaiBoSung", SqlDbType.Decimal, SoLuongTinhThueTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.Decimal, SoLuongTinhThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung", SqlDbType.VarChar, MaDonViTinhSoLuongTinhThueTruocKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung", SqlDbType.VarChar, MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@ThueSuatTruocKhiKhaiBoSung", SqlDbType.VarChar, ThueSuatTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@ThueSuatSauKhiKhaiBoSung", SqlDbType.VarChar, ThueSuatSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoTienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, SoTienThueTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoTienThueSauKhiKhaiBoSung", SqlDbType.VarChar, SoTienThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiMienThueTruocKhiKhaiBoSung", SqlDbType.VarChar, HienThiMienThueTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiMienThueSauKhiKhaiBoSung", SqlDbType.VarChar, HienThiMienThueSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, HienThiSoTienTangGiamThueXuatNhapKhau);
			db.AddInParameter(dbCommand, "@SoTienTangGiamThue", SqlDbType.Decimal, SoTienTangGiamThue);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_HangMauDich_KhaiBoSung_Thue(long id)
		{
			KDT_VNACC_HangMauDich_KhaiBoSung_Thue entity = new KDT_VNACC_HangMauDich_KhaiBoSung_Thue();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TKMDBoSung_ID(long tKMDBoSung_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, tKMDBoSung_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}