﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
   public class TTTK_TransportEquipments
    {
        /// <summary>
        /// Danh sách container trên tờ khai nộp phí
        /// </summary>
        [XmlElement("TransportEquipment")]
       public List<TTTK_TransportEquipment> TransportEquipment { get; set; }
    }
}
