﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using Company.GC.BLL.SXXK.ToKhai;
using Company.GC.BLL.DuLieuChuan;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{
    public partial class PhieuTNForm : BaseForm
    {
        PhieuTN Report = new PhieuTN();
        PhieuTN ReportAll = new PhieuTN();
        public string TenPhieu;
        public string[,] phieuTN;
        public string maHaiQuan = "";
        public PhieuTNForm()
        {
            InitializeComponent();
        }

        private void PreviewForm_Load(object sender, EventArgs e)
        {
            this.ViewPhanBo();
        }
        private void ViewPhanBo()
        {           
            this.Report.phieu = this.TenPhieu;
            this.Report.soTN = this.phieuTN[0,0];
            this.Report.ngayTN = this.phieuTN[0,1];
            this.Report.maHaiQuan = maHaiQuan;
            printControl1.PrintingSystem = this.Report.PrintingSystem;
            this.Report.CreateDocument();
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            } 
        }
        private void cbPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if(this.cbPage.SelectedIndex < this.cbPage.Items.Count - 1)
            //{
            //    Report = new PhieuTN();
            //    this.Report.phieu = this.TenPhieu;
            //    this.Report.soTN = this.phieuTN[this.cbPage.SelectedIndex,0];
            //    this.Report.ngayTN = this.phieuTN[this.cbPage.SelectedIndex,1];
            //    this.Report.maHaiQuan = maHaiQuan;
            //    this.Report.BindReport();
            //    printControl1.PrintingSystem = this.Report.PrintingSystem;
            //    this.Report.CreateDocument();                  
            //}
            //else
            //    {
            //    this.ReportAll.phieu = this.TenPhieu;
            //    this.ReportAll.soTN = this.phieuTN[0,0];
            //    this.ReportAll.ngayTN = this.phieuTN[0,1];
            //    this.ReportAll.BindReport();
            //    this.ReportAll.CreateDocument();
            //    int  pages= this.ReportAll.Pages.Count;
            //    for(int i = 1; i< this.cbPage.Items.Count - 1; i++)
            //    {
            //        Report = new PhieuTN();
            //        this.Report.phieu = this.TenPhieu;
            //        this.Report.soTN = this.phieuTN[i,0];
            //        this.Report.ngayTN = this.phieuTN[i,1];
            //        this.Report.BindReport(); 
            //        this.Report.CreateDocument();
            //        this.ReportAll.Pages.AddRange(this.Report.Pages);
            //    }
            //     pages= this.ReportAll.Pages.Count;
            //     printControl1.PrintingSystem = this.ReportAll.PrintingSystem;
            //}    
        }

    }
}