GO
IF OBJECT_ID('trg_t_GC_DinhMuc_Log') IS NOT NULL
DROP TRIGGER trg_t_GC_DinhMuc_Log
GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '41.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('41.2',GETDATE(), N'CẬP NHẬT XOÁ PROCDEDURE LOG ĐỊNH MỨC ')
END