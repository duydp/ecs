﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.VNACCS
{
    public enum ECategory
    {
        /// <summary>
        /// Mã đại lý Hải quan
        /// </summary> 
        //A001,
        /// <summary>
        /// Nhóm xử lý hồ sơ (Customs-Sub Section)
        /// </summary> 
        A014,
        /// <summary>
        /// Mã nước(Country, coded)
        /// Mã nước xuất xứ
        /// </summary> 
        A015,
        /// <summary>
        /// Mã địa điểm xếp hàng
        /// Mã địa điểm dỡ hàng
        /// </summary> 
        A016,
        /// <summary>
        /// Mã người nhập khẩu
        /// Mã người ủy thác nhập khẩu
        /// </summary> 
        //A017,
        /// <summary>
        /// Mã người xuất khẩu
        /// </summary> 
        //A021,
        /// <summary>
        /// Cơ quan Hải quan
        /// </summary> 
        A038,
        /// <summary>
        /// Mã địa điểm lưu kho hàng chờ thông quan dự kiến
        /// Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
        /// Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
        /// </summary> 
        A202,
        /// <summary>
        /// Mã hãng hàng không
        /// </summary> 
        A214,
        /// <summary>
        /// Container size
        /// </summary> 
        A301,
        /// <summary>
        /// Danh muc cac diem neo dau
        /// </summary> 
        A305,
        /// <summary>
        /// Danh muc ma danh dau hang hoa
        /// </summary> 
        A308,
        /// <summary>
        /// Mã đơn vị tính (Packages unit code)
        /// </summary> 
        A316,
         /// <summary>
        ///  Mã mục đích vận chuyển
        /// </summary> 
        A312,
        /// <summary>
        ///  Mã Laoij hình vận tải
        /// </summary> 
        A317,
        /// <summary>
        /// Mã tên khoản điều chỉnh
        /// </summary> 
        A401,
       
        /// <summary>
        /// Mã xác định mức thuế nhập khẩu theo lượng(ma ap dung muc thue tuyet doi)
        /// </summary> 
        A402,
        /// <summary>
        /// Mã biểu thuế nhập khẩu 
        /// </summary> 
        A404,
        /// <summary>
        /// Mã đơn vị tính thuế tuyệt đối
        /// Mã đơn vị tính
        /// Đơn vị của đơn giá hóa đơn và số lượng
        /// </summary> 
        A501,
        /// <summary>
        /// Mã số hàng hóa (HS)
        /// </summary>
        A506,
        /// <summary>
        /// Mã HS 4 so
        /// </summary>
        A232,
        ///// <summary>
        ///// Số đăng ký bảo hiểm tổng hợp
        ///// </summary> 
        //A515,
        ///// <summary>
        ///// Số tiếp nhận tờ khai trị giá tổng hợp
        ///// </summary> 
        //A517,
        /// <summary>
        /// Mã văn bản pháp quy khác
        /// </summary> 
        A519,
        /// <summary>
        /// Mã miễn / Giảm / Không chịu thuế nhập khẩu
        /// </summary> 
        A520,
        /// <summary>
        /// Mã miễn / Giảm / Không chịu thuế và thu khác
        /// </summary> 
        A521,
        /// <summary>
        /// Mã áp dụng thuế suất / Mức thuế và thu khác
        /// </summary> 
        A522,
        /// <summary>
        /// Mã đồng tiền của hóa đơn
        /// Mã tiền tệ của giá cơ sở hiệu chỉnh trị giá
        /// Mã tiền tệ phí vận chuyển
        /// Mã tiền tệ của tiền bảo hiểm
        /// Mã đồng tiền của khoản điều chỉnh trị giá
        /// Mã đồng tiền của mức thuế tuyệt đối
        /// Mã đồng tiền của đơn giá hóa đơn
        /// Mã đồng tiền trị giá tính thuế
        /// </summary> 
        A527,
        /// <summary>
        /// Phân loại giấy phép nhập khẩu
        /// </summary> 
        A528,
        /// <summary>
        /// Loại chứng từ đính kèm
        /// </summary> 
        A546,
        /// <summary>
        /// Mã phân loại phí vận chuyển
        /// </summary> 
        A557,
        /// <summary>
        /// Mã địa điểm dỡ hàng (Stations)
        /// </summary> 
        A601,
        /// <summary>
        /// Mã địa điểm dỡ hàng (Border gate)
        /// </summary> 
        A620,
        /// <summary>
        /// Transport means (Vehicle)
        /// </summary> 
        A621,
        /// <summary>
        /// OGA User
        /// </summary> 
        A700,
        ///// <summary> 
        ///// Mã phương tiện vận chuyển
        ///// </summary> 
        //B314,
        ///// <summary>
        ///// Số tờ khai
        ///// </summary> 
        //B501,
        ///// <summary>
        ///// Số tiếp nhận hóa đơn điện tử
        ///// </summary> 
        //B530,
        /// <summary>
        /// Trạng thái khai báo VNACC (Application status)
        /// </summary>
        B524,
        ///// <summary>
        ///// Số đăng ký danh mục miễn thuế nhập khẩu
        ///// Số dòng tương ứng trong danh mục miễn thuế nhập khẩu
        ///// </summary> 
        //B700,
        ///// <summary>
        ///// Số tờ khai tạm nhập tái xuất tương ứng
        ///// Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng
        ///// </summary> 
        //B701,
        /// <summary>
        /// Mã điều kiện giá hóa đơn
        /// </summary>
        A553,
        /// <summary>
        /// Mã loại hình (IDA)
        /// </summary> 
        E001,
        /// <summary>
        /// Mã loại hình (EDA)
        /// </summary> 
        E002,
        /// <summary>
        /// Mã phân loại hàng hóa
        /// </summary> 
        E003,
        /// <summary>
        /// Phân loại cá nhân/tổ chức
        /// </summary> 
        E004,
        /// <summary>
        /// Mã hiệu phương thức vận chuyển
        /// </summary> 
        E005,
        /// <summary>
        /// Mã đơn vị tính trọng lượng (Gross)
        /// </summary> 
        E006,
        /// <summary>
        /// Mã kết quả kiểm tra nội dung
        /// </summary> 
        E007,
        /// <summary>
        /// Phân loại hình thức hóa đơn
        /// </summary> 
        E008,
        /// <summary>
        /// Phương thức thanh toán
        /// </summary> 
        E009,
        /// <summary>
        /// Mã phân loại giá hóa đơn
        /// </summary> 
        E010,
        /// <summary>
        /// Mã điều kiện giá hóa đơn
        /// </summary> 
        E011,
        /// <summary>
        /// Mã phân loại khai trị giá
        /// </summary> 
        E012,
        /// <summary>
        /// Mã phân loại bảo hiểm
        /// </summary> 
        E013,
        /// <summary>
        /// Mã phân loại điều chỉnh trị giá
        /// </summary> 
        E014,
        /// <summary>
        /// Người nộp thuế TK nhap
        /// </summary> 
        E015,


        /// <summary>
        /// Mã lý do đề nghị BP (release before permit) (Phát hành trước giấy phép)
        /// </summary> 
        E016,
        /// <summary>
        /// Phân loại xuất nhập cho tờ khai xuất nhập khẩu tại chỗ
        /// </summary>
        A016T,

        /// <summary>
        /// Phân loại chỉ thị của Hải quan
        /// </summary> 
        /// 
        E017,
        /// <summary>
        /// Số lượng (kiện)
        /// </summary> 
        E018,
        /// <summary>
        /// Mã xác định thời hạn nộp thuế
        /// </summary> 
        E019,
        /// <summary>
        /// Phân loại đính kèm khai báo điện tử
        /// </summary> 
        E020,
        /// <summary>
        /// Phân loại nộp thuế
        /// </summary> 
        E021,
        /// <summary>
        /// Mã phân loại tái xác nhận giá
        /// </summary> 
        E022,
        /// <summary>
        /// Mã phân loại thuế suất thuế nhập khẩu
        /// </summary> 
        E023,
        /// <summary>
        /// Loại manifest (hàng hóa)
        /// </summary> 
        E024,
        /// <summary>
        /// Chức năng của chứng từ (Function code)
        /// </summary>
        E025,
        /// <summary>
        /// Phân loại tra cứu (Reference type)
        /// </summary>
        E026,
        /// <summary>
        /// Phương tiện vận chuyển (Means of transport type)
        /// </summary>
        E027,
        /// <summary>
        /// Phân loại vận chuyển (Transportation classification_IVA)
        /// </summary>
        E028,
        /// <summary>
        /// Mã số phân loại (Number classification_IVA)
        /// </summary>
        E029,
        /// <summary>
        /// Thông tin tình trạng thẩm tra/kiểm tra (Status information on examination/inspection)
        /// </summary>
        E030,
        /// <summary>
        /// Phương thức xử lý cuối cùng (Final method of processing)
        /// </summary>
        E031,
        /// <summary>
        /// Loại hình bảo lãnh (Type of security)
        /// </summary>
        E032,
        /// <summary>
        /// Mã lý do khai bổ sung
        /// </summary>
        E033,

        /// <summary>
        /// Người nộp thuế tờ khai xuất
        /// </summary>
        E034,
         /// <summary>
        /// Mã ngân hàng
        /// </summary>
        E035,
        /// <summary>
        /// Mã loai Phuong tiện vận chuyển
        /// </summary>
        E036,
        /// <summary>
        /// Mã loai Manifect(hàng hóa)
        /// </summary>
        E037,
        /// <summary>
        /// Mã đơn vị tính thể tích
        /// </summary>
        E038
       

    }

    public enum EImportExport
    {
        /// <summary>
        /// Import
        /// </summary>
        I,
        /// <summary>
        /// Export
        /// </summary>
        E,
        /// <summary>
        /// Unknown
        /// </summary>
        H
    }

    public enum ELoaiThongTin
    {
        /// <summary>
        /// 
        /// </summary>
        GP_SEA,
        GP_SFA,
        GP_SAA,
        GP_SMA,
        /// <summary>
        /// 
        /// </summary>
        TK,
        TK_KhaiBoSung,
        /// <summary>
        /// 
        /// </summary>
        HDon,
        /// <summary>
        /// 
        /// </summary>
        HDong,
        /// <summary>
        /// Chung tu thanh toan IAS
        /// </summary>
        IAS,
        /// <summary>
        /// Chung tu thanh toan IBA
        /// </summary>
        IBA,
        /// <summary>
        /// Chung tu dinh kem MSB
        /// </summary>
        MSB,
        /// <summary>
        /// Chung tu dinh kem HYS
        /// </summary>
        HYS
    }

    public enum EGiayPhep
    {
        SEA,
        SFA,
        SAA,
        SMA
    }

    public enum EDeclaration_CustomsClearance
    {
        IDA = 1001, //IDA Pre-registration of import declaration
        IDB = 1002, //IDB Pre-registration of import declaration (Call up)
        IDC = 1003, //IDC Import declaration
        IDA01 = 1004, //IDA01 Correction of Pre-registration of import declaration 
        IDD = 1005, //IDD Correction of Pre-registration of import declaration (Call up)
        IDE = 1006, //IDE Correction of import declaration
        MIC = 1007, //MIC Import declaration(Tax exempted small amount clearance)
        MID = 1008, //MID Correction of import declaration(Tax exempted small amount clearance) (Call up)
        MIE = 1009, //MIE Correction of import declaration(Tax exempted small amount clearance)
        IID = 1012, //IID Reference of import declaration
        EDA = 1013, //EDA Pre-registration of export declaration 
        EDB = 1014, //EDB Pre-registration of export declaration (Call up)
        EDC = 1015, //EDC Export declaration
        EDA01 = 1016, //EDA01 Correction of Pre-registration of export declaration 
        EDD = 1017, //EDD Correction of Pre-registration of export declaration (Call up)
        EDE = 1018, //EDE Correction of export declaration
        MEC = 1019, //MEC Export declaration (Tax exempted small amount clearance) 
        MED = 1020, //MED Correction of export declaration (Tax exempted small amount clearance)  (Call up)
        MEE = 1021, //MEE Correction of export declaration (Tax exempted small amount clearance) 
        IEX = 1024, //IEX Reference of export declaration
        AMA = 1028, //AMA Pre-registration of amended tax information
        AMB = 1029, //AMB Amended tax information (Call up)
        AMC = 1030, //AMC Declaration of Amended tax
        IAD = 1032, //IAD Reference of amended tax information
        TEA = 1036, //TEA Registration of the list of tax exemption
        TEB = 1037, //TEB Registration of the list of tax exemption (Call up)
        ITE = 1039, //ITE Reference of the list of tax exemption
        TIA = 1040, //TIA Correction of the information of  tentative export and import
        TIB = 1041, //TIB Correction of the information of  tentative export and import (Call up)
        ITI = 1043 //ITI Reference of declaration status information on tentative export and import
    }

    public enum EInvoice
    {
        IVA = 1044,	//IVA Registration of invoice/packing list information	
        IVA01 = 1045, //IVA01 Alteration of invoice/packing list information (Call up)	
        IIV = 1046 //IIV Reference of invoice/packing list information	
    }

    public enum EPayment
    {
        IAS = 1035, //IVA Registration of invoice/packing list information
        IBA = 1049 //IBA Reference of Bank Payment information
    }

    public enum EDeclaration_OGAProcedure
    {
        SAA = 1070, //SAA Application for Quarantine certificate of import animals/ animal products
        SAB = 1071, //SAB Application for Quarantine certificate of import animals/ animal products (Call up)
        SAZ = 1073, //SAZ Registration of examination/inspection (Application for Quarantine certificate of import animals/ animal products)
        ISA = 1074, //ISA Reference of Application / Permit for Quarantine certificate of import animals/ animal products
        SAS = 1072, //SAS Assignment of examiner/inspector (Application for Quarantine certificate of import animals/ animal products)
        SEA = 1060, //SEA Application for Permission to export/import industrial explosives.
        SEB = 1061, //SEB Application for Permission to export/import industrial explosives (Call up)
        ISE = 1064, //ISE Reference of Application / Permit for Permission to export/import industrial explosives
        SFA = 1080, //SFA Application for Certificate on satisfaction of food's quality for importation
        SFB = 1081, //SFB Application for Certificate on satisfaction of food's quality for importation (Call up)
        ISF = 1084, //ISF Reference of Application / Permit for Certificate on satisfaction of food's quality for importation
        SMA = 1090, //SMA Application for Permission for importation of medicine product
        SMB = 1091, //SMB Application for Permission for importation of medicine product  (Call up)
        ISM = 1094 //ISM Reference of Application/Permit for importation of medicine product
    }

    public enum EManifest_Sea
    {
        MFR, //MFR Manifest registration A
        CMF11, //CMF11 Manifest correction A (call up)
        DMF, //DMF Manifest submission A
        IMI, //IMI Reference of manifest information A
        NVC, //NVC House manifest registration A
        NVC11, //NVC11 House manifest registration A (call up)
        INV //INV Reference of house manifest information A
    }
    public enum EManifest_Air
    {
        ACH, //ACH Manifest registration B
        CAW11, //CAW11 Manifest correction B (call up)
        EAW, //EAW Manifest submission B
        IMF01, //IMF01 Reference of manifest information B
        HCH, //HCH House manifest registration B
        HCH11, //HCH11 House manifest registration B (call up)
        IMF02 //IMF02 Reference of house manifest information B
    }
    public enum EManifest_Railway
    {
        RMR, //RMR Manifest registration C
        CRM11, //CRM11 Manifest correction C (call up)
        DRM, //DRM Manifest submission C
        IRM, //IRM Reference of manifest information C
        HRM, //HRM House manifest registration C
        HRM11, //HRM11 House manifest registration C (call up)
        IHR //IHR Reference of house manifest information C
    }
    public enum EManifest_Common
    {
        CLR11, //CLR11 Registration of cargo loaded confirmation (call up)
        ICG, //ICG Reference of cargo information
        DCL, //DCL Submission of unloaded containers list
        DCL11 //DCL11 Submission of unloaded containers list (call up)
    }

    /// <summary>
    /// Seaports and Airports related Single Window
    /// </summary>
    public enum ESeaportsAndAirports_Sea
    {
        VBX, //VBX Registration of vessel basic information
        VBX11, //VBX11 Correction of vessel basic information (Call up)
        VIT, //VIT Registration of arrival notification A
        VIT11, //VIT11 Correction of arrival notification A (Call up)
        VOT, //VOT Registration of departure notification A
        VOT11, //VOT11 Correction of departure notification A (Call up)
        IVS //IVS Reference of arrival/departure notification A
    }
    /// <summary>
    /// Seaports and Airports related Single Window
    /// </summary>
    public enum ESeaportsAndAirports_Air
    {
        GIR, //GIR Registration of arrival notification B
        GIR11, //GIR11 Correction of arrival notification B (Call up)
        GOR, //GOR Registration of departure notification B
        GOR11, //GOR11 Correction of departure notification B (Call up)
        IGD //IGD Reference of arrival/ departure notification B
    }
    /// <summary>
    /// Seaports and Airports related Single Window
    /// </summary>
    public enum ESeaportsAndAirports_Rail
    {
        RIR, //RIR Registration of arrival notification C
        RIR11, //RIR11 Correction of arrival notification C (Call up)
        ROR, //ROR Registration of departure notification C
        ROR11, //ROR11 Correction of departure notification C (Call up)
        IRD //IRD Reference of arrival/departure notification C
    }
    /// <summary>
    /// Seaports and Airports related Single Window
    /// </summary>
    public enum ESeaportsAndAirports_Vehicle
    {
        TIR, //TIR Registration of arrival declaration D
        TIR11, //TIR11 Correction of arrival declaration D (Call up)
        TOR, //TOR Registration of departure declaration D
        TOR11, //TOR11 Correction of departure declaration D (Call up)
        ILD //ILD Reference of Arrival/ Departure declaration D
    }
    /// <summary>
    /// Seaports and Airports related Single Window
    /// </summary>
    public enum ESeaportsAndAirports_Common
    {
        PLR01, //PLR01 Report of passenger list
        PLR02, //PLR02 Report of passenger list
        PLR11, //PLR11 Report of passenger list (Call up)
        NLR, //NLR Report of crew list
        NLR11, //NLR11 Report of crew list (Call up)
        INP //INP Reference of passengers/crew information
    }

    /// <summary>
    /// Declaration on transportation
    /// </summary>
    public enum EDeclarationOnTransportation
    {
        OLA = 3001, //OLA Pre-registration of declaration on transportation
        OLB = 3002, //OLB Pre-registration of declaration on transportation (Call up)
        OLC = 3003, //OLC Declaration on transportation
        COT11 = 3006, //COT11 Correction of declared information on transportation (Call up)
        BOA = 3007, //BOA Registration of notification on departure regarding transportation
        BIA11 = 3009, //BIA11 Registration of notification on arrival regarding transportation (Call up)
        ITF = 3010 //ITF Reference of declaration on transportation
    }

    public enum EOthers
    {
        TCC = 6008, //TCC Establishment of communication channel check
        MSB = 1033, //MSB Attached document files registration
        HYS = 6001, //HYS Registration of application attached by electronic file
        HYE = 6002, //HYE Modification of application attached by electronic file
        IHY = 6004, //IHY Reference of application attached by electronic file
        ROT = 6007 //ROT Re-output
    }
}
