﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
  public  class Notice
    {
        /// <summary>
        /// Số tờ khai nộp phí
        /// </summary>
      [XmlElement("noticeNo")]
      public string NoticeNo { get; set; }

        /// <summary>
        /// Ngày tờ khai nộp phí YYYY-MM-DD
        /// </summary>
      [XmlElement("noticeDate")]
      public string NoticeDate { get; set; }

        /// <summary>
        /// Hình thức thanh toán
        /// </summary>
      [XmlElement("paymentMethod")]
      public string PaymentMethod { get; set; }

    }
}
