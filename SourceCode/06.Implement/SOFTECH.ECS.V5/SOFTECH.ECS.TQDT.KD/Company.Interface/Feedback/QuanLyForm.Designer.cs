﻿namespace Company.Interface.Feedback
{
    partial class QuanLyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuanLyForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabMain = new Janus.Windows.UI.Tab.UITab();
            this.tabPageQuestion = new Janus.Windows.UI.Tab.UITabPage();
            this.linhVucCauHoi1 = new Company.Interface.Feedback.LinhVucCauHoi();
            this.cauHoiUserControl1 = new Company.Interface.Feedback.CauHoiUserControl();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tabPageSearch = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTraLoi = new System.Windows.Forms.RichTextBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtNoiDung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnReset = new Janus.Windows.EditControls.UIButton();
            this.btnLinhVuc = new Janus.Windows.EditControls.UIButton();
            this.btnCauHoi = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabMain)).BeginInit();
            this.tabMain.SuspendLayout();
            this.tabPageQuestion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnCauHoi);
            this.grbMain.Controls.Add(this.btnLinhVuc);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnDelete);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.btnReset);
            this.grbMain.Controls.Add(this.splitContainer1);
            this.grbMain.Size = new System.Drawing.Size(981, 635);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabMain);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.uiGroupBox2);
            this.splitContainer1.Panel2.Controls.Add(this.splitter2);
            this.splitContainer1.Panel2.Controls.Add(this.uiGroupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(975, 598);
            this.splitContainer1.SplitterDistance = 434;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // tabMain
            // 
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.Size = new System.Drawing.Size(434, 598);
            this.tabMain.TabIndex = 0;
            this.tabMain.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.tabPageQuestion,
            this.tabPageSearch});
            this.tabMain.VisualStyleManager = this.vsmMain;
            // 
            // tabPageQuestion
            // 
            this.tabPageQuestion.Controls.Add(this.linhVucCauHoi1);
            this.tabPageQuestion.Controls.Add(this.cauHoiUserControl1);
            this.tabPageQuestion.Controls.Add(this.splitter1);
            this.tabPageQuestion.Location = new System.Drawing.Point(1, 21);
            this.tabPageQuestion.Name = "tabPageQuestion";
            this.tabPageQuestion.Size = new System.Drawing.Size(432, 576);
            this.tabPageQuestion.TabStop = true;
            this.tabPageQuestion.Text = "Câu hỏi";
            // 
            // linhVucCauHoi1
            // 
            this.linhVucCauHoi1.Dock = System.Windows.Forms.DockStyle.Top;
            this.linhVucCauHoi1.Location = new System.Drawing.Point(0, 5);
            this.linhVucCauHoi1.LookAndFeel.SkinName = "Office 2007 Blue";
            this.linhVucCauHoi1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.linhVucCauHoi1.Name = "linhVucCauHoi1";
            this.linhVucCauHoi1.Size = new System.Drawing.Size(432, 372);
            this.linhVucCauHoi1.TabIndex = 3;
            // 
            // cauHoiUserControl1
            // 
            this.cauHoiUserControl1.BackColor = System.Drawing.Color.Transparent;
            this.cauHoiUserControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cauHoiUserControl1.Location = new System.Drawing.Point(0, 384);
            this.cauHoiUserControl1.Name = "cauHoiUserControl1";
            this.cauHoiUserControl1.Size = new System.Drawing.Size(432, 192);
            this.cauHoiUserControl1.TabIndex = 2;
            this.cauHoiUserControl1.CauHoiSelectionChanged += new Company.Interface.Feedback.CauHoiUserControl.CauHoiSelectionChangedEventHandler(this.cauHoiUserControl1_CauHoiSelectionChanged);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(432, 5);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // tabPageSearch
            // 
            this.tabPageSearch.Location = new System.Drawing.Point(1, 21);
            this.tabPageSearch.Name = "tabPageSearch";
            this.tabPageSearch.Size = new System.Drawing.Size(429, 575);
            this.tabPageSearch.TabStop = true;
            this.tabPageSearch.Text = "Tìm kiếm";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox2.Controls.Add(this.txtTraLoi);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 90);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(536, 508);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.Text = "Trả lời";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtTraLoi
            // 
            this.txtTraLoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTraLoi.Location = new System.Drawing.Point(3, 17);
            this.txtTraLoi.Name = "txtTraLoi";
            this.txtTraLoi.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txtTraLoi.Size = new System.Drawing.Size(530, 488);
            this.txtTraLoi.TabIndex = 0;
            this.txtTraLoi.Text = "";
            this.txtTraLoi.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(0, 85);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(536, 5);
            this.splitter2.TabIndex = 2;
            this.splitter2.TabStop = false;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.txtNoiDung);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(536, 85);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Câu hỏi";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoiDung.Location = new System.Drawing.Point(3, 17);
            this.txtNoiDung.Multiline = true;
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNoiDung.Size = new System.Drawing.Size(530, 65);
            this.txtNoiDung.TabIndex = 0;
            this.txtNoiDung.VisualStyleManager = this.vsmMain;
            this.txtNoiDung.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(900, 607);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDelete.Icon")));
            this.btnDelete.Location = new System.Drawing.Point(738, 607);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyleManager = this.vsmMain;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(819, 607);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Icon = ((System.Drawing.Icon)(resources.GetObject("btnReset.Icon")));
            this.btnReset.Location = new System.Drawing.Point(657, 607);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 3;
            this.btnReset.Text = "Làm mới";
            this.btnReset.VisualStyleManager = this.vsmMain;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnLinhVuc
            // 
            this.btnLinhVuc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLinhVuc.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLinhVuc.Icon")));
            this.btnLinhVuc.Location = new System.Drawing.Point(4, 607);
            this.btnLinhVuc.Name = "btnLinhVuc";
            this.btnLinhVuc.Size = new System.Drawing.Size(90, 23);
            this.btnLinhVuc.TabIndex = 1;
            this.btnLinhVuc.Text = "Lĩnh vực...";
            this.btnLinhVuc.VisualStyleManager = this.vsmMain;
            this.btnLinhVuc.Click += new System.EventHandler(this.btnLinhVuc_Click);
            // 
            // btnCauHoi
            // 
            this.btnCauHoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCauHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnCauHoi.Icon")));
            this.btnCauHoi.Location = new System.Drawing.Point(100, 606);
            this.btnCauHoi.Name = "btnCauHoi";
            this.btnCauHoi.Size = new System.Drawing.Size(90, 23);
            this.btnCauHoi.TabIndex = 2;
            this.btnCauHoi.Text = "Câu hỏi...";
            this.btnCauHoi.VisualStyleManager = this.vsmMain;
            this.btnCauHoi.Click += new System.EventHandler(this.btnCauHoi_Click);
            // 
            // QuanLyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(981, 635);
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "QuanLyForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Quản lý thông tin góp ý & giải đáp";
            this.Load += new System.EventHandler(this.QuanLyForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabMain)).EndInit();
            this.tabMain.ResumeLayout(false);
            this.tabPageQuestion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private Janus.Windows.UI.Tab.UITab tabMain;
        private Janus.Windows.UI.Tab.UITabPage tabPageQuestion;
        private CauHoiUserControl cauHoiUserControl1;
        private System.Windows.Forms.Splitter splitter1;
        private Janus.Windows.UI.Tab.UITabPage tabPageSearch;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.RichTextBox txtTraLoi;
        private System.Windows.Forms.Splitter splitter2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiDung;
        public Janus.Windows.EditControls.UIButton btnClose;
        public Janus.Windows.EditControls.UIButton btnDelete;
        public Janus.Windows.EditControls.UIButton btnSave;
        public Janus.Windows.EditControls.UIButton btnReset;
        public Janus.Windows.EditControls.UIButton btnLinhVuc;
        public Janus.Windows.EditControls.UIButton btnCauHoi;
        private LinhVucCauHoi linhVucCauHoi1;
    }
}