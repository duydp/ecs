using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class KDT_VNACC_ChungTuDinhKem
    {
        private List<KDT_VNACC_ChungTuDinhKem_ChiTiet> _listChungTuDinhKemChiTiet = new List<KDT_VNACC_ChungTuDinhKem_ChiTiet>();

        public List<KDT_VNACC_ChungTuDinhKem_ChiTiet> ChungTuDinhKemChiTietCollection
        {
            set { this._listChungTuDinhKemChiTiet = value; }
            get { return this._listChungTuDinhKemChiTiet; }
        }

        public void LoadListChungTuDinhKem_ChiTiet()
        {
            _listChungTuDinhKemChiTiet = KDT_VNACC_ChungTuDinhKem_ChiTiet.SelectCollectionBy_ChungTuKemID(this.ID);
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert(transaction);
                    else
                        this.Update();

                    foreach (KDT_VNACC_ChungTuDinhKem_ChiTiet item in this.ChungTuDinhKemChiTietCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.ChungTuKemID = this.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    ret = true;
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                   // this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

    }
}