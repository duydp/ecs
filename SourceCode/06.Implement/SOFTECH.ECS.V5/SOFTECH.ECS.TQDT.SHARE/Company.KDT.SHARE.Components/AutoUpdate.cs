﻿/*
 * Nguon lay tu www.CodeProject.com by Eduardo Oliveira.
 * Application Auto Update Revisited
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Company.KDT.SHARE.Components
{
    public class AutoUpdate
    {
        private string m_RemotePath = "";
        private string m_UpdateFileName = "Update.txt";
        private string m_ErrorMessage = "There was a problem runing the Auto Update.";

        public string RemotePath
        {
            get { return m_RemotePath; }
            set { m_RemotePath = value; }
        }

        public string UpdateFileName
        {
            get { return m_UpdateFileName; }
            set { m_UpdateFileName = value; }
        }

        public string ErrorMessage
        {
            get { return m_ErrorMessage; }
            set { m_ErrorMessage = value; }
        }

        private string GetVersion(string Version)
        {
            string[] x = Version.Split(new char[] { '.' });
            return String.Format("{0:00000}{1:00000}{2:00000}{3:00000}", int.Parse(x[0]), int.Parse(x[1]), int.Parse(x[2]), int.Parse(x[3]));
        }

        /*
            <File Name>;<Version>   [' comments    ]
            <File Name>[;<Version>] [' comments    ]  
            <File Name>[;?]         [' comments    ]
            <File Name>[;delete]    [' comments    ]
            ...
            Blank lines and comments are ignored
            The first line should be the current program/version
            from the second line to the end the second parameter is optional
            if the second parameter is not specified the file is updated.
            if the version is specified the update checks the version
            if the second parameter is an interrogation mark (?) the update checks if the 
            file alredy exists and "don't" upgrade if exists.
            if the second parameter is "delete" the system try to delete the file
            "'" (chr(39)) start a line comment (like vb)

            Function Return Value
            True means that the program needs to exit because: the autoupdate did the update
            or there was an error during the update
            False - nothing was done
         */

        public bool UpdateFiles(String RemotePath)
        {
            if (RemotePath == "") RemotePath = m_RemotePath; else m_RemotePath = RemotePath;

            bool Ret = false;
            string AssemblyName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
            string ToDeleteExtension = ".ToDelete";
            string RemoteUri = RemotePath + AssemblyName + "/";
            System.Net.WebClient MyWebClient = new System.Net.WebClient();

            try
            {
                // try to delete old files if ( exist
                string s = System.IO.Directory.GetDirectoryRoot(System.Windows.Forms.Application.StartupPath + "\\*" + ToDeleteExtension);
                while (s != "")
                {
                    try
                    {
                        System.IO.File.Delete(System.Windows.Forms.Application.StartupPath + "\\" + s);
                    }
                    catch (Exception ex) {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                    s = System.IO.Directory.GetDirectoryRoot(System.Windows.Forms.Application.StartupPath + "\\*" + ToDeleteExtension);
                }

                // get the update file content
                string Contents = MyWebClient.DownloadString(RemoteUri + UpdateFileName);

                // Process the autoupdate
                // get rid of the line feeds if ( exists
                Contents = Contents.Replace((char)System.Windows.Forms.Keys.LineFeed, ' ');
                string[] FileList = Contents.Split(new char[] { (char)System.Windows.Forms.Keys.Return });
                Contents = "";

                // Remove all comments and blank lines
                string f = "";
                foreach (string F in FileList)
                {
                    if (F.IndexOf("'") != 0) f = F.Substring(F.IndexOf("'") - 1);

                    if (f.Trim() != "")
                    {
                        if (Contents != "") Contents += "\r";
                        Contents += f.Trim();
                    }
                }

                // rebuild the file list
                FileList = Contents.Split(new char[] { (char)System.Windows.Forms.Keys.Return });

                string[] Info = FileList[0].Split(new char[] { ';' });
                // if ( the name is correct and it is a new version
                if (System.Windows.Forms.Application.StartupPath.ToLower() + "\\" + Info[0].ToLower() == System.Windows.Forms.Application.ExecutablePath.ToLower()
                 && Convert.ToDecimal(GetVersion(Info[1])) > Convert.ToDecimal(GetVersion(System.Windows.Forms.Application.ProductVersion)))
                {
                    // process files in the list
                    foreach (String F in FileList)
                    {
                        Info = F.Split(new char[] { ';' });
                        bool isToDelete = false;
                        bool isToUpgrade = false;
                        string TempFileName = System.Windows.Forms.Application.StartupPath + "\\" + DateTime.Now.TimeOfDay.TotalMilliseconds;
                        string FileName = System.Windows.Forms.Application.StartupPath + "\\" + Info[0].Trim();
                        bool FileExists = System.IO.File.Exists(FileName);

                        if (Info.Length == 1)
                        {
                            // Just the file as parameter always upgrade
                            isToUpgrade = true;
                            isToDelete = FileExists;
                        }
                        else if (Info[1].Trim() == "delete")
                            // second parameter is "delete"
                            isToDelete = FileExists;
                        else if (Info[1].Trim() == "?")
                            // second parameter is "?" (check if ( file exists and don't upgrade if ( exists)
                            isToUpgrade = !FileExists;
                        else if (FileExists)
                        {
                            // verify the file version
                            FileVersionInfo fv = FileVersionInfo.GetVersionInfo(FileName);
                            isToUpgrade = Convert.ToDecimal(GetVersion(Info[1].Trim())) > Convert.ToInt64(GetVersion(fv.FileMajorPart + "." + fv.FileMinorPart + "." + fv.FileBuildPart + "." + fv.FilePrivatePart));
                            isToDelete = isToUpgrade;
                        }
                        else
                            // the second parameter exists as version number and the file doesn't exists
                            isToUpgrade = true;

                        Debug.Print(TempFileName);
                        // download the new file
                        if (isToUpgrade) MyWebClient.DownloadFile(RemoteUri + Info[0], TempFileName);
                        // rename the existent file to be deleted in the future
                        if (isToDelete) System.IO.File.Move(FileName, TempFileName + ToDeleteExtension);
                        // rename the downloaded file to the real name
                        if (isToUpgrade) System.IO.File.Move(TempFileName, FileName);
                        // try to delete the file
                    }

                    Ret = true;
                }
            }
            catch (Exception ex)
            {
                Ret = true;
                m_ErrorMessage += "\r\n" + ex.Message + "\r\n" + "Assembly: " + AssemblyName;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return Ret;
        }
    }
}
