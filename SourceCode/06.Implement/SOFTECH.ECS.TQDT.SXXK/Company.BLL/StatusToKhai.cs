﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public class StatusToKhai
    {
        /// <summary>
        /// Chua khai bao
        /// </summary>
        public static bool CHUA_KHAI_BAO = false;
        /// <summary>
        /// Cho duyet
        /// </summary>
        public static bool CHO_DUYET = false;
        /// <summary>
        /// Da duyet
        /// </summary>
        public static bool DA_DUYET = false;
        /// <summary>
        /// Khong phe duyet
        /// </summary>
        public static bool KHONG_PHE_DUYET = false;
        /// <summary>
        /// Cho sua to khai
        /// </summary>
        public static bool CHO_SUA = false;
        /// <summary>
        /// Cho huy to khai
        /// </summary>
        public static bool CHO_HUY = false;
        /// <summary>
        /// Da huy to khai
        /// </summary>
        public static bool DA_HUY = false;
        /// <summary>
        /// Da phan luong to khai
        /// </summary>
        public static bool DA_PHAN_LUONG = false;
        /// <summary>
        /// Duoc thong quan
        /// </summary>
        public static bool DUOC_THONG_QUAN = false;
        /*Chua khai bao: Them moi hang, Luu to khai, Chung tu kem, Khai bao, Tinh lai thue, Chung tu bo sung*/
        /// <summary>
        /// Duoc phep them hang
        /// </summary>
        public static bool IsThemHang = false;
        /// <summary>
        /// Duoc phep luu to khai
        /// </summary>
        public static bool IsLuu = false;
        /// <summary>
        /// Duoc phep them chung tu kem
        /// </summary>
        public static bool IsChungTu_Kem = false;
        /// <summary>
        /// Duoc phep them chung tu bo sung
        /// </summary>
        public static bool IsChungTu_BoSung = false;
        /// <summary>
        /// Duoc phep tinh lai thue
        /// </summary>
        public static bool IsTinh_Lai_Thue = false;
        /// <summary>
        /// Duoc phep khai bao
        /// </summary>
        public static bool IsKhaiBao = false;
        /// <summary>
        /// Duoc phep nhan du lieu
        /// </summary>
        public static bool IsNhanDuLieu = false;
        /// <summary>
        /// Duoc phep huy khai bao
        /// </summary>
        public static bool IsHuyKhaiBao = false;
        /// <summary>
        /// Duoc phep sua to khai
        /// </summary>
        public static bool IsSuaToKhai = false;
        /// <summary>
        /// Duoc phep huy to khai
        /// </summary>
        public static bool IsHuyToKhai = false;

        public void CheckStatus(Company.BLL.KDT.ToKhaiMauDich TKMD)
        {
            try
            {
                CHUA_KHAI_BAO = (TKMD.TrangThaiXuLy == -1);

                CHO_DUYET = (TKMD.TrangThaiXuLy == 0);

                DA_DUYET = (TKMD.TrangThaiXuLy == 1);

                CHO_SUA = (TKMD.TrangThaiXuLy == 5);

                CHO_HUY = (TKMD.TrangThaiXuLy == 10);

                DA_HUY = (TKMD.TrangThaiXuLy == 11);

                DA_PHAN_LUONG = (TKMD.PhanLuong != "");

                DUOC_THONG_QUAN = (TKMD.PhanLuong == "1");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
