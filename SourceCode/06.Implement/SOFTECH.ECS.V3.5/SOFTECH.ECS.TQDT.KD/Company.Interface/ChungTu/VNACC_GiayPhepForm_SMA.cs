﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.Maper;

namespace Company.Interface
{
    public partial class VNACC_GiayPhepForm_SMA : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_GiayPhep_SMA GiayPhep = new KDT_VNACC_GiayPhep_SMA();

        public VNACC_GiayPhepForm_SMA()
        {
            InitializeComponent();

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_OGAProcedure.SMA.ToString());
        }

        private void VNACC_GiayPhepForm_Load(object sender, EventArgs e)
        {
            SetIDControl();

            SetMaxLengthControl();

            ucCuaKhauNhapDuKien.Code = "";
            ucCuaKhauNhapDuKien.ReLoadData();

            if (GiayPhep.ID != 0)
            {
                SetGiayPhep();
            }

            ValidateForm(true);

            SetAutoRemoveUnicodeAndUpperCaseControl();
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    this.ShowThemHang();
                    break;
                case "cmdLuu":
                    this.SaveGiayPhep();
                    break;
                case "cmdChiThiHaiQuan":
                    ShowChiThiHaiQuan();
                    break;

                case "cmdKhaiBao":
                    SendVnaccsIDC(false);
                    break;
                case "cmdKetQuaHQ":
                    ShowKetQuaTraVe();
                    break;
            }
        }
        private void ShowKetQuaTraVe()
        {
            SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
            f.isSend = false;
            f.isRep = true;
            f.inputMSGID = GiayPhep.InputMessageID;
            f.ShowDialog(this);
        }

        private void ShowChiThiHaiQuan()
        {
            VNACC_ChiThiHaiQuanForm f = new VNACC_ChiThiHaiQuanForm();
            f.Master_ID = GiayPhep.ID;
            f.LoaiThongTin = ELoaiThongTin.GP_SMA;
            f.ShowDialog();
        }

        private void ShowThemHang()
        {
            VNACC_GiayPhep_SMA_HangForm f = new VNACC_GiayPhep_SMA_HangForm();
            f.GiayPhep_SMA = GiayPhep;
            f.ShowDialog();
        }

        private void SaveGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!ValidateForm(false))
                    return;

                GetGiayPhep();

                GiayPhep.InsertUpdateFull();

                Helper.Controls.MessageBoxControlV.ShowMessage(Company.KDT.SHARE.Components.ThongBao.APPLICATION_SAVE_DATA_SUCCESS_0Param, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                errorProvider.Clear();

                GiayPhep.MaNguoiKhai = txtMaNguoiKhai.Text;
                GiayPhep.SoDonXinCapPhep = txtSoDonXinCapPhep.Text != "" ? Convert.ToInt32(txtSoDonXinCapPhep.Text) : 0;
                GiayPhep.ChucNangChungTu = Convert.ToInt32(ucChucNangChungTu.Code);
                GiayPhep.LoaiGiayPhep = ucLoaiGiayPhep.Code;
                GiayPhep.MaDonViCapPhep = ucMaDonViCapPhep.Code;
                GiayPhep.MaThuongNhanNhapKhau = txtMaThuongNhanNhapKhau.Text;
                GiayPhep.TenThuongNhanNhapKhau = txtTenThuongNhanNhapKhau.Text;
                GiayPhep.MaBuuChinhNhapKhau = txtMaBuuChinhNhapKhau.Text;
                GiayPhep.DiaChiThuongNhanNhapKhau = txtSoNhaTenDuongNhapKhau.Text;
                GiayPhep.MaQuocGiaNhapKhau = ucMaQuocGiaNhapKhau.Code;
                GiayPhep.SoDienThoaiNhapKhau = txtSoDienThoaiNhapKhau.Text;
                GiayPhep.SoFaxNhapKhau = txtSoFaxNhapKhau.Text;
                GiayPhep.EmailNhapKhau = txtEmailNhapKhau.Text;
                GiayPhep.SoCuaGiayPhepLuuHanh = txtSoCuaGiayPhepLuuHanh.Text;
                GiayPhep.SoGiayPhepThucHanh = txtSoGiayPhepThucHanh.Text;
                GiayPhep.MaCuaKhauNhapDuKien = ucCuaKhauNhapDuKien.Code;
                GiayPhep.TenCuaKhauNhapDuKien = ucCuaKhauNhapDuKien.Name_VN;
                GiayPhep.HoSoLienQuan = txtHoSoLienQuan.Text;
                GiayPhep.GhiChu = txtGhiChu.Text;
                GiayPhep.TonKhoDenNgay = dtTonKhoDenNgay.Value;
                GiayPhep.TenGiamDocDoanhNghiep = txtTenGiamDocDoanhNghiep.Text;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void SetGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                errorProvider.Clear();

                txtMaNguoiKhai.Text = GiayPhep.MaNguoiKhai;
                txtSoDonXinCapPhep.Text = GiayPhep.SoDonXinCapPhep.ToString();
                ucChucNangChungTu.Code = GiayPhep.ChucNangChungTu.ToString();
                ucLoaiGiayPhep.Code = GiayPhep.LoaiGiayPhep;
                ucMaDonViCapPhep.Code = GiayPhep.MaDonViCapPhep;
                txtMaThuongNhanNhapKhau.Text = GiayPhep.MaThuongNhanNhapKhau;
                txtTenThuongNhanNhapKhau.Text = GiayPhep.TenThuongNhanNhapKhau;
                txtMaBuuChinhNhapKhau.Text = GiayPhep.MaBuuChinhNhapKhau;
                txtSoNhaTenDuongNhapKhau.Text = GiayPhep.DiaChiThuongNhanNhapKhau;
                ucMaQuocGiaNhapKhau.Code = GiayPhep.MaQuocGiaNhapKhau;
                txtSoDienThoaiNhapKhau.Text = GiayPhep.SoDienThoaiNhapKhau;
                txtSoFaxNhapKhau.Text = GiayPhep.SoFaxNhapKhau;
                txtEmailNhapKhau.Text = GiayPhep.EmailNhapKhau;
                txtSoCuaGiayPhepLuuHanh.Text = GiayPhep.SoCuaGiayPhepLuuHanh;
                txtSoGiayPhepThucHanh.Text = GiayPhep.SoGiayPhepThucHanh;
                ucCuaKhauNhapDuKien.Code = GiayPhep.MaCuaKhauNhapDuKien;
                txtHoSoLienQuan.Text = GiayPhep.HoSoLienQuan;
                txtGhiChu.Text = GiayPhep.GhiChu;
                dtTonKhoDenNgay.Value = GiayPhep.TonKhoDenNgay;
                txtTenGiamDocDoanhNghiep.Text = GiayPhep.TenGiamDocDoanhNghiep;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNguoiKhai.Tag = "SMC"; //Mã người khai
                txtSoDonXinCapPhep.Tag = "APN"; //Số đơn xin cấp phép
                ucChucNangChungTu.TagName = "FNC"; //Chức năng của chứng từ
                ucLoaiGiayPhep.TagName = "APT"; //Loại giấy phép
                ucMaDonViCapPhep.TagName = "APP"; //Mã đơn vị cấp phép
                txtMaThuongNhanNhapKhau.Tag = "IMC"; //Mã doanh nghiệp nhập khẩu
                txtTenThuongNhanNhapKhau.Tag = "IMN"; //Tên doanh nghiệp nhập khẩu
                txtMaBuuChinhNhapKhau.Tag = "IPC"; //Địa chỉ của doanh nghiệp nhập khẩu (Mã bưu chính)
                txtSoNhaTenDuongNhapKhau.Tag = "IMA"; //Địa chỉ của doanh nghiệp nhập khẩu (Số nhà, tên đường)
                ucMaQuocGiaNhapKhau.TagName = "ICC"; //Địa chỉ của doanh nghiệp nhập khẩu (Mã quốc gia)  xuất khẩu
                txtSoDienThoaiNhapKhau.Tag = "IPN"; //Địa chỉ của doanh nghiệp nhập khẩu (Số điện thoại)
                txtSoFaxNhapKhau.Tag = "IMF"; //Địa chỉ của doanh nghiệp nhập khẩu (Số fax)
                txtEmailNhapKhau.Tag = "IME"; //Địa chỉ của doanh nghiệp nhập khẩu (Email)
                txtSoCuaGiayPhepLuuHanh.Tag = "FSC"; //Số của Giấy phép lưu hành nước sở tại
                txtSoCuaGiayPhepLuuHanh.Tag = "GMP"; //Số của Giấy phép thực hành tốt sản xuất thuốc 
                ucCuaKhauNhapDuKien.TagCode = "EPC"; //Mã cửa khẩu nhập dự kiến 
                //txtTenCuaKhauNhapDuKien.Tag = "EPN"; //Tên cửa khẩu nhập dự kiến 
                txtHoSoLienQuan.Tag = "AD"; //Hồ sơ liên quan
                txtGhiChu.Tag = "RMK"; //Ghi chú
                dtTonKhoDenNgay.Tag = "SUD"; //Tồn kho đến ngày
                txtTenGiamDocDoanhNghiep.Tag = "PIC"; //Giám đốc doanh nghiệp
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            bool warning = false;

            try
            {
                Cursor = Cursors.WaitCursor;

                isValid &= ValidateControl.ValidateNull(txtMaNguoiKhai, errorProvider, "Mã người khai", isOnlyWarning);

                ucMaDonViCapPhep.SetValidate = !isOnlyWarning; ucMaDonViCapPhep.SetOnlyWarning = isOnlyWarning;
                isValid &= ucLoaiGiayPhep.IsValidate; //"Loại giấy phép"

                ucMaDonViCapPhep.ShowColumnCode = true; ucMaDonViCapPhep.ShowColumnName = false;
                ucMaDonViCapPhep.SetValidate = !isOnlyWarning; ucMaDonViCapPhep.SetOnlyWarning = isOnlyWarning;
                warning = ucMaDonViCapPhep.IsOnlyWarning;
                isValid &= ucMaDonViCapPhep.IsValidate; //"Mã đơn vị cấp phép"

                isValid &= ValidateControl.ValidateNull(txtMaThuongNhanNhapKhau, errorProvider, "Mã doanh nghiệp nhập khẩu", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNguoiKhai.MaxLength = 13;
                txtSoDonXinCapPhep.MaxLength = 12;
                //ucChucNangChungTu.MaxLength = 1;
                //ucLoaiGiayPhep.MaxLength = 4;
                //ucMaDonViCapPhep.MaxLength = 6;
                txtMaThuongNhanNhapKhau.MaxLength = 13;
                txtTenThuongNhanNhapKhau.MaxLength = 300;
                txtMaBuuChinhNhapKhau.MaxLength = 7;
                txtSoNhaTenDuongNhapKhau.MaxLength = 300;
                //ucMaQuocGiaNhapKhau.MaxLength = 2;
                txtSoDienThoaiNhapKhau.MaxLength = 20;
                txtSoFaxNhapKhau.MaxLength = 20;
                txtEmailNhapKhau.MaxLength = 210;
                txtSoCuaGiayPhepLuuHanh.MaxLength = 17;
                txtSoCuaGiayPhepLuuHanh.MaxLength = 17;
                //ucCuaKhauNhapDuKien.MaxLength = 6;
                //txtTenCuaKhauNhapDuKien.MaxLength = 35;
                txtHoSoLienQuan.MaxLength = 750;
                txtGhiChu.MaxLength = 996;
                //dtTonKhoDenNgay.MaxLength = 8;
                txtTenGiamDocDoanhNghiep.MaxLength = 300;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            txtMaNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            ucLoaiGiayPhep.TextChanged += new EventHandler(SetTextChanged_Handler);
            ucMaDonViCapPhep.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaThuongNhanNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaBuuChinhNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaQuocGiaNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoDienThoaiNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoFaxNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoCuaGiayPhepLuuHanh.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoGiayPhepThucHanh.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucCuaKhauNhapDuKien.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtTenCuaKhauNhapDuKien.TextChanged += new EventHandler(SetTextChanged_Handler);

            txtMaNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            ucLoaiGiayPhep.IsUpperCase = true;
            ucMaDonViCapPhep.IsUpperCase = true;
            txtMaThuongNhanNhapKhau.CharacterCasing = CharacterCasing.Upper;
            txtMaBuuChinhNhapKhau.CharacterCasing = CharacterCasing.Upper;
            ucMaQuocGiaNhapKhau.IsUpperCase = true;
            txtSoDienThoaiNhapKhau.CharacterCasing = CharacterCasing.Upper;
            txtSoFaxNhapKhau.CharacterCasing = CharacterCasing.Upper;
            txtSoCuaGiayPhepLuuHanh.CharacterCasing = CharacterCasing.Upper;
            txtSoGiayPhepThucHanh.CharacterCasing = CharacterCasing.Upper;
            ucCuaKhauNhapDuKien.IsUpperCase = true;
            //txtTenCuaKhauNhapDuKien.CharacterCasing = CharacterCasing.Upper;
        }

        #region Send VNACCS

        /// <summary>
        /// Khai báo thông tin
        /// </summary>
        /// <param name="KhaiBaoSua"></param>
        private void SendVnaccsIDC(bool KhaiBaoSua)
        {
            try
            {
                if (GiayPhep.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }

                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến Hải quan? ", true) == "Yes")
                {
                    GiayPhep.InputMessageID = HelperVNACCS.NewInputMSGID(); //Tạo mới GUID ID
                    GiayPhep.InsertUpdateFull(); //Lưu thông tin GUID ID vừa tạo

                    MessagesSend msg = null; //Form khai báo
                    SMA seaObj = VNACCMaperFromObject.SMAMapper(GiayPhep); //Set Mapper
                    if (seaObj == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    msg = MessagesSend.Load<SMA>(seaObj, GiayPhep.InputMessageID);

                    MsgLog.SaveMessages(msg, GiayPhep.ID, EnumThongBao.SendMess, ""); //Lưu thông tin trước khai báo
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true; //Có khai báo
                    f.isRep = true; //Có nhận phản hồi
                    f.inputMSGID = GiayPhep.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result) //Có kêt quả trả về
                    {
                        string ketqua = "Khai báo thông tin thành công";

                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                decimal soTiepNhan = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                ketqua = ketqua + Environment.NewLine;
                                ketqua += "Số đơn xin cấp phép: " + soTiepNhan;

                                GiayPhep.SoDonXinCapPhep = soTiepNhan;
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật số giấp phép: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }

                        TuDongCapNhatThongTin();
                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        #endregion

        #region Cập nhật thông tin

        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            ProcessMessages.GetDataResult_GiayPhep(msgResult, "", GiayPhep);
            GiayPhep.InsertUpdateFull();
            SetGiayPhep();
            //setCommandStatus();
        }

        private void TuDongCapNhatThongTin()
        {
            if (GiayPhep != null && GiayPhep.SoDonXinCapPhep > 0 && GiayPhep.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}' And MessagesInputID = '{1}'", GiayPhep.SoDonXinCapPhep.ToString(), GiayPhep.InputMessageID), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }

        #endregion

    }
}
