using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_TransportMean
	{
        protected static List<VNACC_Category_TransportMean> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_TransportMean> collection = new List<VNACC_Category_TransportMean>();
            while (reader.Read())
            {
                VNACC_Category_TransportMean entity = new VNACC_Category_TransportMean();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TransportMeansCode"))) entity.TransportMeansCode = reader.GetString(reader.GetOrdinal("TransportMeansCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("TransportMeansName"))) entity.TransportMeansName = reader.GetString(reader.GetOrdinal("TransportMeansName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_TransportMean> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [TransportMeansCode], TransportMeansName FROM [dbo].[t_VNACC_Category_TransportMeans]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_TransportMean> SelectCollectionBy(List<VNACC_Category_TransportMean> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_TransportMean o)
            {
                return o.TableID.Contains(tableID);
            });
        }	
	}	
}