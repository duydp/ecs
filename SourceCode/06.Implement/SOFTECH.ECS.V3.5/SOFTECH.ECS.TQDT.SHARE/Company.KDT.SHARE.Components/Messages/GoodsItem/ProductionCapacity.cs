﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GoodsItem
{
   public class ProductionCapacity
    {
        ///// <summary>
        ///// Thời gian sản xuất
        ///// </summary>
        //[XmlElement("time")]
        //public string Time { get; set; }

        ///// <summary>
        ///// Số lượng sản phẩm
        ///// </summary>
        //[XmlElement("quantity")]
        //public string Quantity { get; set; }

        /// <summary>
        /// Thời gian sản xuất
        /// </summary>
        [XmlElement("Product")]
        public List<Product> Product { get; set; }

    }
}
