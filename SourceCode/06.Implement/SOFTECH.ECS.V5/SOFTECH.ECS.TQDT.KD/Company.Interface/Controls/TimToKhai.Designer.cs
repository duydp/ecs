﻿namespace Company.Interface.Controls
{
    partial class TimToKhai
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimToKhai));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem13 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem14 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.buttonDropDown = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cbStatus = new Janus.Windows.EditControls.UIComboBox();
            this.cboPhanLuong = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.borderLabel = new System.Windows.Forms.Label();
            this.txtSoToKhai = new System.Windows.Forms.TextBox();
            this.buttonGo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonDropDown
            // 
            this.buttonDropDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDropDown.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonDropDown.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDropDown.ImageIndex = 1;
            this.buttonDropDown.ImageList = this.imageList1;
            this.buttonDropDown.Location = new System.Drawing.Point(683, 0);
            this.buttonDropDown.Name = "buttonDropDown";
            this.buttonDropDown.Size = new System.Drawing.Size(76, 23);
            this.buttonDropDown.TabIndex = 6;
            this.buttonDropDown.Text = "Nâng cao";
            this.buttonDropDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonDropDown.UseVisualStyleBackColor = true;
            this.buttonDropDown.Click += new System.EventHandler(this.buttonDropDown_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "search_16.png");
            this.imageList1.Images.SetKeyName(1, "down.png");
            // 
            // cbStatus
            // 
            this.cbStatus.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.FormatStyle.ForeColor = System.Drawing.Color.Red;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Chưa khai báo";
            uiComboBoxItem8.Value = -1;
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.FormatStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Chờ duyệt";
            uiComboBoxItem9.Value = 0;
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Đã duyệt";
            uiComboBoxItem10.Value = 1;
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Không phê duyệt";
            uiComboBoxItem11.Value = "2";
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.IsSeparator = false;
            uiComboBoxItem12.Text = "Sửa tờ khai";
            uiComboBoxItem12.Value = "5";
            uiComboBoxItem13.FormatStyle.Alpha = 0;
            uiComboBoxItem13.IsSeparator = false;
            uiComboBoxItem13.Text = "Đã hủy";
            uiComboBoxItem13.Value = "10";
            uiComboBoxItem14.FormatStyle.Alpha = 0;
            uiComboBoxItem14.IsSeparator = false;
            uiComboBoxItem14.Text = "Chờ hủy";
            uiComboBoxItem14.Value = "11";
            this.cbStatus.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem8,
            uiComboBoxItem9,
            uiComboBoxItem10,
            uiComboBoxItem11,
            uiComboBoxItem12,
            uiComboBoxItem13,
            uiComboBoxItem14});
            this.cbStatus.Location = new System.Drawing.Point(219, 1);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(105, 21);
            this.cbStatus.TabIndex = 2;
            this.cbStatus.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbStatus.SelectedIndexChanged += new System.EventHandler(this.ToKhaiTimKiem);
            // 
            // cboPhanLuong
            // 
            this.cboPhanLuong.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboPhanLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPhanLuong.Location = new System.Drawing.Point(398, 1);
            this.cboPhanLuong.Name = "cboPhanLuong";
            this.cboPhanLuong.Size = new System.Drawing.Size(105, 21);
            this.cboPhanLuong.TabIndex = 3;
            this.cboPhanLuong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cboPhanLuong.SelectedIndexChanged += new System.EventHandler(this.ToKhaiTimKiem);
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtSoTiepNhan.DecimalDigits = 0;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.FormatString = "#####";
            this.txtSoTiepNhan.Location = new System.Drawing.Point(80, 1);
            this.txtSoTiepNhan.MaxLength = 5;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(68, 21);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoTiepNhan.Value = ((ulong)(0ul));
            this.txtSoTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Số tiếp nhận";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(152, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Trạng thái";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(327, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Phân luồng";
            // 
            // borderLabel
            // 
            this.borderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.borderLabel.BackColor = System.Drawing.SystemColors.Window;
            this.borderLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.borderLabel.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.borderLabel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.borderLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.borderLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.borderLabel.Location = new System.Drawing.Point(504, 0);
            this.borderLabel.Name = "borderLabel";
            this.borderLabel.Size = new System.Drawing.Size(103, 23);
            this.borderLabel.TabIndex = 19;
            this.borderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.borderLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.borderLabel_MouseDown);
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.txtSoToKhai.BackColor = System.Drawing.SystemColors.Window;
            this.txtSoToKhai.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSoToKhai.Location = new System.Drawing.Point(512, 6);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(87, 13);
            this.txtSoToKhai.TabIndex = 4;
            this.txtSoToKhai.Visible = false;
            this.txtSoToKhai.Leave += new System.EventHandler(this.textBox_Leave);
            this.txtSoToKhai.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            this.txtSoToKhai.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // buttonGo
            // 
            this.buttonGo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonGo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonGo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGo.ImageIndex = 0;
            this.buttonGo.ImageList = this.imageList1;
            this.buttonGo.Location = new System.Drawing.Point(609, 0);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(72, 23);
            this.buttonGo.TabIndex = 5;
            this.buttonGo.Text = "Tìm kiếm";
            this.buttonGo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.ToKhaiTimKiem);
            // 
            // TimToKhai
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.txtSoToKhai);
            this.Controls.Add(this.borderLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtSoTiepNhan);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboPhanLuong);
            this.Controls.Add(this.cbStatus);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.buttonDropDown);
            this.Name = "TimToKhai";
            this.Size = new System.Drawing.Size(760, 24);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonDropDown;
        private Janus.Windows.EditControls.UIComboBox cbStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label borderLabel;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button buttonGo;
        public Janus.Windows.EditControls.UIComboBox cboPhanLuong;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhan;
        public System.Windows.Forms.TextBox txtSoToKhai;

    }
}
