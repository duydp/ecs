﻿namespace Company.Interface
{
    partial class VNACC_HaoDon_HangHoaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_HaoDon_HangHoaForm));
            Janus.Windows.GridEX.GridEXLayout grdList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaSoHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrNuocXX = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT_KhauTru = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT_TriGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTT_DonGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDVTLuong2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDVTLuong1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnAddNew = new Janus.Windows.EditControls.UIButton();
            this.txtSoTienKhauTru = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.grdList = new Janus.Windows.GridEX.GridEX();
            this.label28 = new System.Windows.Forms.Label();
            this.txtTriGia_HoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDonGia_HoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtMaHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoKienHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoLuong_2 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtLoaiKhauTru = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuong_1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Size = new System.Drawing.Size(691, 427);
            this.grbMain.Click += new System.EventHandler(this.grbMain_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.ctrMaSoHang);
            this.uiGroupBox3.Controls.Add(this.ctrNuocXX);
            this.uiGroupBox3.Controls.Add(this.ctrMaTT_KhauTru);
            this.uiGroupBox3.Controls.Add(this.ctrMaTT_TriGia);
            this.uiGroupBox3.Controls.Add(this.ctrMaTT_DonGia);
            this.uiGroupBox3.Controls.Add(this.ctrDVTLuong2);
            this.uiGroupBox3.Controls.Add(this.ctrDVTLuong1);
            this.uiGroupBox3.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox3.Controls.Add(this.txtSoTienKhauTru);
            this.uiGroupBox3.Controls.Add(this.grdList);
            this.uiGroupBox3.Controls.Add(this.label28);
            this.uiGroupBox3.Controls.Add(this.txtTriGia_HoaDon);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Controls.Add(this.txtDonGia_HoaDon);
            this.uiGroupBox3.Controls.Add(this.txtMaHang);
            this.uiGroupBox3.Controls.Add(this.txtSoKienHang);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.txtSoLuong_2);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.txtLoaiKhauTru);
            this.uiGroupBox3.Controls.Add(this.txtSoLuong_1);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.label9);
            this.uiGroupBox3.Controls.Add(this.txtTenHang);
            this.uiGroupBox3.Controls.Add(this.label27);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.label11);
            this.uiGroupBox3.Controls.Add(this.label18);
            this.uiGroupBox3.Controls.Add(this.label10);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(691, 427);
            this.uiGroupBox3.TabIndex = 12;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaSoHang
            // 
            this.ctrMaSoHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ctrMaSoHang.Code = "";
            this.ctrMaSoHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaSoHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaSoHang.IsValidate = false;
            this.ctrMaSoHang.Location = new System.Drawing.Point(518, 9);
            this.ctrMaSoHang.Name = "ctrMaSoHang";
            this.ctrMaSoHang.Name_VN = "";
            this.ctrMaSoHang.ShowColumnCode = true;
            this.ctrMaSoHang.ShowColumnName = false;
            this.ctrMaSoHang.Size = new System.Drawing.Size(159, 26);
            this.ctrMaSoHang.TabIndex = 1;
            this.ctrMaSoHang.TagName = "";
            this.ctrMaSoHang.WhereCondition = "";
            // 
            // ctrNuocXX
            // 
            this.ctrNuocXX.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ctrNuocXX.Code = "";
            this.ctrNuocXX.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNuocXX.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrNuocXX.IsValidate = false;
            this.ctrNuocXX.Location = new System.Drawing.Point(110, 64);
            this.ctrNuocXX.Name = "ctrNuocXX";
            this.ctrNuocXX.Name_VN = "";
            this.ctrNuocXX.ShowColumnCode = true;
            this.ctrNuocXX.ShowColumnName = true;
            this.ctrNuocXX.Size = new System.Drawing.Size(264, 26);
            this.ctrNuocXX.TabIndex = 3;
            this.ctrNuocXX.TagName = "";
            this.ctrNuocXX.WhereCondition = "";
            // 
            // ctrMaTT_KhauTru
            // 
            this.ctrMaTT_KhauTru.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_KhauTru.Code = "";
            this.ctrMaTT_KhauTru.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_KhauTru.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_KhauTru.IsValidate = false;
            this.ctrMaTT_KhauTru.Location = new System.Drawing.Point(618, 145);
            this.ctrMaTT_KhauTru.Name = "ctrMaTT_KhauTru";
            this.ctrMaTT_KhauTru.Name_VN = "";
            this.ctrMaTT_KhauTru.ShowColumnCode = true;
            this.ctrMaTT_KhauTru.ShowColumnName = false;
            this.ctrMaTT_KhauTru.Size = new System.Drawing.Size(60, 26);
            this.ctrMaTT_KhauTru.TabIndex = 15;
            this.ctrMaTT_KhauTru.TagName = "";
            this.ctrMaTT_KhauTru.WhereCondition = "";
            // 
            // ctrMaTT_TriGia
            // 
            this.ctrMaTT_TriGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_TriGia.Code = "";
            this.ctrMaTT_TriGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_TriGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_TriGia.IsValidate = false;
            this.ctrMaTT_TriGia.Location = new System.Drawing.Point(618, 118);
            this.ctrMaTT_TriGia.Name = "ctrMaTT_TriGia";
            this.ctrMaTT_TriGia.Name_VN = "";
            this.ctrMaTT_TriGia.ShowColumnCode = true;
            this.ctrMaTT_TriGia.ShowColumnName = false;
            this.ctrMaTT_TriGia.Size = new System.Drawing.Size(60, 26);
            this.ctrMaTT_TriGia.TabIndex = 13;
            this.ctrMaTT_TriGia.TagName = "";
            this.ctrMaTT_TriGia.WhereCondition = "";
            // 
            // ctrMaTT_DonGia
            // 
            this.ctrMaTT_DonGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTT_DonGia.Code = "";
            this.ctrMaTT_DonGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTT_DonGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTT_DonGia.IsValidate = false;
            this.ctrMaTT_DonGia.Location = new System.Drawing.Point(618, 91);
            this.ctrMaTT_DonGia.Name = "ctrMaTT_DonGia";
            this.ctrMaTT_DonGia.Name_VN = "";
            this.ctrMaTT_DonGia.ShowColumnCode = true;
            this.ctrMaTT_DonGia.ShowColumnName = false;
            this.ctrMaTT_DonGia.Size = new System.Drawing.Size(60, 26);
            this.ctrMaTT_DonGia.TabIndex = 11;
            this.ctrMaTT_DonGia.TagName = "";
            this.ctrMaTT_DonGia.WhereCondition = "";
            // 
            // ctrDVTLuong2
            // 
            this.ctrDVTLuong2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong2.Code = "";
            this.ctrDVTLuong2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong2.IsValidate = false;
            this.ctrDVTLuong2.Location = new System.Drawing.Point(295, 120);
            this.ctrDVTLuong2.Name = "ctrDVTLuong2";
            this.ctrDVTLuong2.Name_VN = "";
            this.ctrDVTLuong2.ShowColumnCode = true;
            this.ctrDVTLuong2.ShowColumnName = false;
            this.ctrDVTLuong2.Size = new System.Drawing.Size(79, 26);
            this.ctrDVTLuong2.TabIndex = 8;
            this.ctrDVTLuong2.TagName = "";
            this.ctrDVTLuong2.WhereCondition = "";
            // 
            // ctrDVTLuong1
            // 
            this.ctrDVTLuong1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong1.Code = "";
            this.ctrDVTLuong1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong1.IsValidate = false;
            this.ctrDVTLuong1.Location = new System.Drawing.Point(295, 93);
            this.ctrDVTLuong1.Name = "ctrDVTLuong1";
            this.ctrDVTLuong1.Name_VN = "";
            this.ctrDVTLuong1.ShowColumnCode = true;
            this.ctrDVTLuong1.ShowColumnName = true;
            this.ctrDVTLuong1.Size = new System.Drawing.Size(79, 26);
            this.ctrDVTLuong1.TabIndex = 6;
            this.ctrDVTLuong1.TagName = "";
            this.ctrDVTLuong1.WhereCondition = "";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label26);
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.btnGhi);
            this.uiGroupBox2.Controls.Add(this.btnXoa);
            this.uiGroupBox2.Controls.Add(this.btnAddNew);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(5, 185);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(677, 39);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(610, 255);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "(USD)";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(597, 11);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.Location = new System.Drawing.Point(430, 11);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 0;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(514, 11);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 1;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnAddNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAddNew.Icon")));
            this.btnAddNew.Location = new System.Drawing.Point(345, 11);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(75, 23);
            this.btnAddNew.TabIndex = 0;
            this.btnAddNew.Text = "Tạo mới";
            this.btnAddNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddNew.VisualStyleManager = this.vsmMain;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // txtSoTienKhauTru
            // 
            this.txtSoTienKhauTru.DecimalDigits = 20;
            this.txtSoTienKhauTru.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienKhauTru.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienKhauTru.Location = new System.Drawing.Point(473, 148);
            this.txtSoTienKhauTru.Name = "txtSoTienKhauTru";
            this.txtSoTienKhauTru.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienKhauTru.Size = new System.Drawing.Size(144, 21);
            this.txtSoTienKhauTru.TabIndex = 14;
            this.txtSoTienKhauTru.Text = "0";
            this.txtSoTienKhauTru.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienKhauTru.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienKhauTru.VisualStyleManager = this.vsmMain;
            // 
            // grdList
            // 
            this.grdList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdList.AlternatingColors = true;
            this.grdList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            grdList_DesignTimeLayout.LayoutString = resources.GetString("grdList_DesignTimeLayout.LayoutString");
            this.grdList.DesignTimeLayout = grdList_DesignTimeLayout;
            this.grdList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grdList.FrozenColumns = 4;
            this.grdList.GroupByBoxVisible = false;
            this.grdList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdList.Location = new System.Drawing.Point(12, 228);
            this.grdList.Name = "grdList";
            this.grdList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.grdList.Size = new System.Drawing.Size(670, 184);
            this.grdList.TabIndex = 16;
            this.grdList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdList.TotalRowPosition = Janus.Windows.GridEX.TotalRowPosition.BottomFixed;
            this.grdList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdList.VisualStyleManager = this.vsmMain;
            this.grdList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.grdList_DeletingRecord);
            this.grdList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grdList_RowDoubleClick);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(384, 152);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(84, 13);
            this.label28.TabIndex = 4;
            this.label28.Text = "Số tiền khấu trừ";
            // 
            // txtTriGia_HoaDon
            // 
            this.txtTriGia_HoaDon.BackColor = System.Drawing.Color.White;
            this.txtTriGia_HoaDon.DecimalDigits = 20;
            this.txtTriGia_HoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGia_HoaDon.FormatString = "G20";
            this.txtTriGia_HoaDon.Location = new System.Drawing.Point(473, 121);
            this.txtTriGia_HoaDon.Name = "txtTriGia_HoaDon";
            this.txtTriGia_HoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGia_HoaDon.Size = new System.Drawing.Size(144, 21);
            this.txtTriGia_HoaDon.TabIndex = 12;
            this.txtTriGia_HoaDon.Text = "0";
            this.txtTriGia_HoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGia_HoaDon.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGia_HoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTriGia_HoaDon.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(384, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Trị giá hóa đơn";
            // 
            // txtDonGia_HoaDon
            // 
            this.txtDonGia_HoaDon.DecimalDigits = 20;
            this.txtDonGia_HoaDon.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDonGia_HoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGia_HoaDon.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGia_HoaDon.Location = new System.Drawing.Point(473, 94);
            this.txtDonGia_HoaDon.MaxLength = 15;
            this.txtDonGia_HoaDon.Name = "txtDonGia_HoaDon";
            this.txtDonGia_HoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGia_HoaDon.Size = new System.Drawing.Size(144, 21);
            this.txtDonGia_HoaDon.TabIndex = 10;
            this.txtDonGia_HoaDon.Text = "0";
            this.txtDonGia_HoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonGia_HoaDon.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGia_HoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDonGia_HoaDon.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHang
            // 
            this.txtMaHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHang.Location = new System.Drawing.Point(110, 12);
            this.txtMaHang.MaxLength = 30;
            this.txtMaHang.Name = "txtMaHang";
            this.txtMaHang.Size = new System.Drawing.Size(310, 21);
            this.txtMaHang.TabIndex = 0;
            this.txtMaHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHang.VisualStyleManager = this.vsmMain;
            // 
            // txtSoKienHang
            // 
            this.txtSoKienHang.DecimalDigits = 20;
            this.txtSoKienHang.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoKienHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoKienHang.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoKienHang.Location = new System.Drawing.Point(518, 67);
            this.txtSoKienHang.MaxLength = 15;
            this.txtSoKienHang.Name = "txtSoKienHang";
            this.txtSoKienHang.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoKienHang.Size = new System.Drawing.Size(159, 21);
            this.txtSoKienHang.TabIndex = 4;
            this.txtSoKienHang.Text = "0";
            this.txtSoKienHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoKienHang.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoKienHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(230, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Đơn vị tính";
            // 
            // txtSoLuong_2
            // 
            this.txtSoLuong_2.DecimalDigits = 20;
            this.txtSoLuong_2.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong_2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong_2.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong_2.Location = new System.Drawing.Point(110, 121);
            this.txtSoLuong_2.MaxLength = 15;
            this.txtSoLuong_2.Name = "txtSoLuong_2";
            this.txtSoLuong_2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong_2.Size = new System.Drawing.Size(119, 21);
            this.txtSoLuong_2.TabIndex = 7;
            this.txtSoLuong_2.Text = "0";
            this.txtSoLuong_2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong_2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong_2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(384, 98);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Đơn giá hóa đơn";
            // 
            // txtLoaiKhauTru
            // 
            this.txtLoaiKhauTru.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtLoaiKhauTru.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtLoaiKhauTru.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoaiKhauTru.Location = new System.Drawing.Point(110, 148);
            this.txtLoaiKhauTru.MaxLength = 12;
            this.txtLoaiKhauTru.Name = "txtLoaiKhauTru";
            this.txtLoaiKhauTru.Size = new System.Drawing.Size(264, 21);
            this.txtLoaiKhauTru.TabIndex = 9;
            this.txtLoaiKhauTru.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLoaiKhauTru.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuong_1
            // 
            this.txtSoLuong_1.DecimalDigits = 20;
            this.txtSoLuong_1.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong_1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong_1.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong_1.Location = new System.Drawing.Point(110, 94);
            this.txtSoLuong_1.MaxLength = 15;
            this.txtSoLuong_1.Name = "txtSoLuong_1";
            this.txtSoLuong_1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong_1.Size = new System.Drawing.Size(119, 21);
            this.txtSoLuong_1.TabIndex = 5;
            this.txtSoLuong_1.Text = "0";
            this.txtSoLuong_1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong_1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong_1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuong_1.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(230, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Đơn vị tính";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(432, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Số kiện hàng";
            // 
            // txtTenHang
            // 
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(110, 40);
            this.txtTenHang.MaxLength = 255;
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(567, 21);
            this.txtTenHang.TabIndex = 2;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHang.VisualStyleManager = this.vsmMain;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(17, 16);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(48, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Mã hàng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Số lượng (2)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên hàng";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(17, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Số lượng (1)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(432, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Mã số hàng hóa";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(17, 71);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "Nước xuất xứ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(17, 152);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Loại khấu trừ";
            // 
            // VNACC_HaoDon_HangHoaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 427);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_HaoDon_HangHoaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin hàng trong hóa đơn";
            this.Load += new System.EventHandler(this.VNACC_HaoDon_HangHoaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienKhauTru;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGia_HoaDon;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGia_HoaDon;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoKienHang;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong_2;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong_1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHang;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHang;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiKhauTru;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIButton btnAddNew;
        private Janus.Windows.GridEX.GridEX grdList;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private System.Windows.Forms.Label label11;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_KhauTru;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_TriGia;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTT_DonGia;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrNuocXX;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaSoHang;
    }
}