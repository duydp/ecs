﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{   
    [XmlRoot("Declaration")]
    public class GC_ThanhKhoan : DeclarationBase
    { 
      
        /// <summary>
        /// Thông tin thanh khoản
        /// </summary>
        [XmlElement("ContractReference")]
        public ContractDocument ContractReference { get; set; }
        /// <summary>
        /// Hàng thanh khoản
        /// </summary>
        [XmlElement("GoodsItems")]
        public List<Product> GoodsItems { set; get; }

        


    }
}
