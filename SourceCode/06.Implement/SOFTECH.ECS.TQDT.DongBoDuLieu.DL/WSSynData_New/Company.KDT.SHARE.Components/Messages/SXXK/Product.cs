﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class Product
    {
        /// <summary>
        /// Thông tin hàng
        /// </summary>
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }
        /// <summary>
        /// Số lượng và đơn vị tính
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }

        /// <summary>
        /// Mã sản phẩm khai báo sửa (NPL)
        /// </summary>
        [XmlElement("preIdentification")]
        public string PreIdentification { get; set; }

        /// <summary>
        /// Thời gian sản xuất (thời gian)
        /// </summary>
        [XmlElement("time")]
        public string Time { get; set; }

        /// <summary>
        /// Thời gian sản xuất (ĐVT)
        /// </summary>
        [XmlElement("measureUnitTime")]
        public string MeasureUnitTime { get; set; }

        /// <summary>
        /// Mã sản phẩm khai báo sửa (NPL)
        /// </summary>
        [XmlElement("Identification")]
        public string Identification { get; set; }

        /// <summary>
        /// Mã HS
        /// </summary>
        [XmlElement("tariffClassification")]
        public string TariffClassification { get; set; }

        /// <summary>
        /// Chu kỳ sản xuất (thời gian)
        /// </summary>
        [XmlElement("period")]
        public string Period { get; set; }

        /// <summary>
        /// Chu kỳ sản xuất (ĐVT)
        /// </summary>
        [XmlElement("measureUnit")]
        public string MeasureUnit { get; set; }

        /// <summary>
        /// Số lượng sản phẩm
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }

    }
}
