using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAL8000_HANG : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute A26 { get; set; }
public PropertiesAttribute A28 { get; set; }
public PropertiesAttribute A31 { get; set; }
public PropertiesAttribute A33 { get; set; }
public PropertiesAttribute A32 { get; set; }
public PropertiesAttribute A34 { get; set; }
public PropertiesAttribute A39 { get; set; }
public PropertiesAttribute A40 { get; set; }
public PropertiesAttribute A41 { get; set; }
public PropertiesAttribute A42 { get; set; }
public PropertiesAttribute A43 { get; set; }
public PropertiesAttribute A44 { get; set; }
public PropertiesAttribute A45 { get; set; }
public PropertiesAttribute A46 { get; set; }
public PropertiesAttribute A47 { get; set; }
public PropertiesAttribute A48 { get; set; }
public PropertiesAttribute A49 { get; set; }
public PropertiesAttribute A50 { get; set; }
public PropertiesAttribute A51 { get; set; }
public PropertiesAttribute A52 { get; set; }
public PropertiesAttribute ADC { get; set; }
public PropertiesAttribute A53 { get; set; }
public GroupAttribute KD1 { get; set; }

public VAL8000_HANG()
        {
A26 = new PropertiesAttribute(2, typeof(string));
A28 = new PropertiesAttribute(2, typeof(string));
A31 = new PropertiesAttribute(600, typeof(string));
A33 = new PropertiesAttribute(2, typeof(string));
A32 = new PropertiesAttribute(600, typeof(string));
A34 = new PropertiesAttribute(2, typeof(string));
A39 = new PropertiesAttribute(17, typeof(int));
A40 = new PropertiesAttribute(12, typeof(int));
A41 = new PropertiesAttribute(4, typeof(string));
A42 = new PropertiesAttribute(12, typeof(string));
A43 = new PropertiesAttribute(30, typeof(string));
A44 = new PropertiesAttribute(16, typeof(int));
A45 = new PropertiesAttribute(1, typeof(string));
A46 = new PropertiesAttribute(17, typeof(int));
A47 = new PropertiesAttribute(12, typeof(int));
A48 = new PropertiesAttribute(4, typeof(string));
A49 = new PropertiesAttribute(12, typeof(string));
A50 = new PropertiesAttribute(30, typeof(string));
A51 = new PropertiesAttribute(16, typeof(int));
A52 = new PropertiesAttribute(1, typeof(string));
ADC = new PropertiesAttribute(1, typeof(string));
A53 = new PropertiesAttribute(16, typeof(int));
#region KD1
List<PropertiesAttribute> listKD1 = new List<PropertiesAttribute>();
listKD1.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAL8000_HANG_KD1, typeof(string)));
listKD1.Add(new PropertiesAttribute(27, 5, EnumGroupID.VAL8000_HANG_KE1, typeof(string)));
listKD1.Add(new PropertiesAttribute(17, 5, EnumGroupID.VAL8000_HANG_KF1, typeof(int)));
listKD1.Add(new PropertiesAttribute(12, 5, EnumGroupID.VAL8000_HANG_KG1, typeof(int)));
listKD1.Add(new PropertiesAttribute(4, 5, EnumGroupID.VAL8000_HANG_KH1, typeof(string)));
listKD1.Add(new PropertiesAttribute(10, 5, EnumGroupID.VAL8000_HANG_KI1, typeof(string)));
listKD1.Add(new PropertiesAttribute(25, 5, EnumGroupID.VAL8000_HANG_KJ1, typeof(string)));
listKD1.Add(new PropertiesAttribute(16, 5, EnumGroupID.VAL8000_HANG_KK1, typeof(int)));
listKD1.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAL8000_HANG_KL1, typeof(string)));
listKD1.Add(new PropertiesAttribute(17, 5, EnumGroupID.VAL8000_HANG_KM1, typeof(int)));
listKD1.Add(new PropertiesAttribute(12, 5, EnumGroupID.VAL8000_HANG_KN1, typeof(int)));
listKD1.Add(new PropertiesAttribute(4, 5, EnumGroupID.VAL8000_HANG_KO1, typeof(string)));
listKD1.Add(new PropertiesAttribute(10, 5, EnumGroupID.VAL8000_HANG_KP1, typeof(string)));
listKD1.Add(new PropertiesAttribute(25, 5, EnumGroupID.VAL8000_HANG_KQ1, typeof(string)));
listKD1.Add(new PropertiesAttribute(16, 5, EnumGroupID.VAL8000_HANG_KR1, typeof(int)));
listKD1.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAL8000_HANG_KS1, typeof(string)));
listKD1.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAL8000_HANG_KV1, typeof(string)));
listKD1.Add(new PropertiesAttribute(16, 5, EnumGroupID.VAL8000_HANG_KT1, typeof(int)));
KD1 = new GroupAttribute("KD1", 5, listKD1);
#endregion KD1
TongSoByte = 2708;
}

}public partial class EnumGroupID
    {
public static readonly string VAL8000_HANG_KD1 = "VAL8000_HANG_KD1";
public static readonly string VAL8000_HANG_KE1 = "VAL8000_HANG_KE1";
public static readonly string VAL8000_HANG_KF1 = "VAL8000_HANG_KF1";
public static readonly string VAL8000_HANG_KG1 = "VAL8000_HANG_KG1";
public static readonly string VAL8000_HANG_KH1 = "VAL8000_HANG_KH1";
public static readonly string VAL8000_HANG_KI1 = "VAL8000_HANG_KI1";
public static readonly string VAL8000_HANG_KJ1 = "VAL8000_HANG_KJ1";
public static readonly string VAL8000_HANG_KK1 = "VAL8000_HANG_KK1";
public static readonly string VAL8000_HANG_KL1 = "VAL8000_HANG_KL1";
public static readonly string VAL8000_HANG_KM1 = "VAL8000_HANG_KM1";
public static readonly string VAL8000_HANG_KN1 = "VAL8000_HANG_KN1";
public static readonly string VAL8000_HANG_KO1 = "VAL8000_HANG_KO1";
public static readonly string VAL8000_HANG_KP1 = "VAL8000_HANG_KP1";
public static readonly string VAL8000_HANG_KQ1 = "VAL8000_HANG_KQ1";
public static readonly string VAL8000_HANG_KR1 = "VAL8000_HANG_KR1";
public static readonly string VAL8000_HANG_KS1 = "VAL8000_HANG_KS1";
public static readonly string VAL8000_HANG_KV1 = "VAL8000_HANG_KV1";
public static readonly string VAL8000_HANG_KT1 = "VAL8000_HANG_KT1";
}

}
