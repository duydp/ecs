﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class IssueBase
    {
        /// <summary>
        /// Loại chứng từ  (Nhập khẩu=929, Xuất khẩu=930)
        /// </summary>
        [XmlElement("issuer")]
        public string Issuer { get; set; }

        /// <summary>
        /// Số vận đơn
        /// 
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Loại
        /// 
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }

        /// <summary>
        /// Ngày vận đơn
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// la DNCX
        /// Năm quyêt toán
        /// </summary>
        [XmlElement("finalYear")]
        public string FinalYear { get; set; }

        /// <summary>
        /// la DNCX
        /// Quý quyết toán
        /// </summary>
        [XmlElement("finalQuarter")]
        public string FinalQuarter { get; set; }

        /// <summary>
        /// Loại chức năng
        /// 1. Hủy
        /// 5. Sửa
        /// 8. Khai báo
        /// 12. Chưa xử lý
        /// 13. Hỏi trạng thái
        /// 16. Đề nghị 
        /// 27. Không chấp nhận
        /// 29. Cấp số tiếp nhận
        /// 30. Cấp số tờ khai
        /// 31. Duyệt luồng chứng từ
        /// 32. Thông báo thông quan
        /// 33. Thông báo thực xuất
        /// 313. Thông báo bổ sung chứng từ
        /// </summary>
        [XmlElement("function")]
        public string Function { get; set; }

        /// <summary>
        /// Nước phát hành
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        /// <summary>
        /// Đơn vị HQ khai báo
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }

        /// <summary>
        /// Đơn vị HQ khai báo dưa vao thanh khoan
        /// </summary>
        [XmlElement("declarationOfficeLiquidity")]
        public string DeclarationOfficeLiquidity { get; set; }

        /// <summary>
        /// Cờ báo nhập khẩu/xuất khẩu 
        /// </summary>
        [XmlElement("isExportImport")]
        public string IsExportImport { get; set; }

        /// <summary>
        /// Ngày cấp phép
        /// </summary>
        [XmlElement("clearance")]
        public string Clearance { get; set; }


        /// <summary>
        /// Mã LH vận chuyển
        /// </summary>
        [XmlElement("natureOfTransaction")]
        public string NatureOfTransaction { get; set; }

        /// <summary>
        /// Hải quan giám sát
        /// </summary>
        [XmlElement("declarationOfficeControl")]
        public string DeclarationOfficeControl { get; set; }


        /// <summary>
        /// Mã phương tiện vận chuyển
        /// </summary>
        [XmlElement("transportation")]
        public string Transportation { get; set; }

        /// <summary>
        /// Mã mục đích vận chuyển
        /// </summary>
        [XmlElement("purposesTransport")]
        public string PurposesTransport { get; set; }

        /// <summary>
        /// Ngày, giờ dự kiến bắt đầu vận chuyển
        /// </summary>
        [XmlElement("startDateOfTransport")]
        public string StartDateOfTransport { get; set; }

        /// <summary>
        /// Ngày, giờ dự kiến kết thúc vận chuyển
        /// </summary>
        [XmlElement("arrivalDateOfTransport")]
        public string ArrivalDateOfTransport { get; set; }

        /// <summary>
        /// Ngày bắt đầu báo cáo
        /// </summary>
        [XmlElement("startDate")]
        public string StartDate { get; set; }

        /// <summary>
        /// Ngày kết thúc báo cáo
        /// </summary>
        [XmlElement("finishDate")]
        public string FinishDate { get; set; }

        /// <summary>
        /// Loại định mức
        /// </summary>
        [XmlElement("productionNormType")]
        public string ProductionNormType { get; set; }

        /// <summary>
        /// nước thuê gia công
        /// </summary>
        [XmlElement("exportationCountry")]
        public string ExportationCountry { get; set; }
        /// nước gia công
        /// </summary>
        [XmlElement("importationCountry")]
        public string ImportationCountry { get; set; }
    }
}
