﻿namespace ServerRemote
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup1 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem1 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem2 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup2 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem3 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem4 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem5 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup3 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem6 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem7 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup4 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem8 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem9 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem10 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem11 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel3 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel3Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarConfig = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel4 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel4Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerSign = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel7 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel7Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSign = new Janus.Windows.EditControls.UIButton();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uiPanel2 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel2Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.uiPanel5 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel5Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.uiPanel6 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel6Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdAutoUpdate1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.cmdUpdateDatabase1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateDatabase");
            this.cmdVersion1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVersion");
            this.cmdConfig1 = new Janus.Windows.UI.CommandBars.UICommand("cmdConfig");
            this.cmdConfigUSB1 = new Janus.Windows.UI.CommandBars.UICommand("cmdConfigUSB");
            this.cmdLog1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.cmdExit1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExit");
            this.cmdAutoUpdate = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.cmdUpdateDatabase = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateDatabase");
            this.cmdVersion = new Janus.Windows.UI.CommandBars.UICommand("cmdVersion");
            this.cmdConfig = new Janus.Windows.UI.CommandBars.UICommand("cmdConfig");
            this.cmdConfigUSB = new Janus.Windows.UI.CommandBars.UICommand("cmdConfigUSB");
            this.cmdExit = new Janus.Windows.UI.CommandBars.UICommand("cmdExit");
            this.cmdLog = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.cmdCloseMe = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseMe");
            this.cmdCloseAll = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.cmdCloseAllButMe = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButMe");
            this.mnuRightClick = new Janus.Windows.UI.CommandBars.UIContextMenu();
            this.cmdCloseMe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseMe");
            this.cmdCloseAllButMe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButMe");
            this.cmdCloseAll1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.popupNotifier1 = new NotificationWindow.PopupNotifier();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel3)).BeginInit();
            this.uiPanel3.SuspendLayout();
            this.uiPanel3Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel4)).BeginInit();
            this.uiPanel4.SuspendLayout();
            this.uiPanel4Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerSign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel7)).BeginInit();
            this.uiPanel7.SuspendLayout();
            this.uiPanel7Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).BeginInit();
            this.uiPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel5)).BeginInit();
            this.uiPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel6)).BeginInit();
            this.uiPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mnuRightClick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(229, 152);
            this.grbMain.Size = new System.Drawing.Size(908, 364);
            this.grbMain.Visible = false;
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.TabbedMdi = true;
            this.uiPanelManager1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiPanelManager1.MdiTabMouseDown += new Janus.Windows.UI.Dock.MdiTabMouseEventHandler(this.uiPanelManager1_MdiTabMouseDown);
            this.uiPanel0.Id = new System.Guid("98f7d2f3-6465-4de4-8027-4c5ff71bc833");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel3.Id = new System.Guid("bba94330-4e00-4889-b3cf-72736cb4f638");
            this.uiPanel0.Panels.Add(this.uiPanel3);
            this.uiPanel4.Id = new System.Guid("3cfe4e0c-93d9-45b5-b0b9-6dd58c648cc7");
            this.uiPanel0.Panels.Add(this.uiPanel4);
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            this.uiPanel7.Id = new System.Guid("7e73309c-ea7a-44a5-8c23-a9204fe858c3");
            this.uiPanelManager1.Panels.Add(this.uiPanel7);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("98f7d2f3-6465-4de4-8027-4c5ff71bc833"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, Janus.Windows.UI.Dock.PanelDockStyle.Left, true, new System.Drawing.Size(226, 481), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("bba94330-4e00-4889-b3cf-72736cb4f638"), new System.Guid("98f7d2f3-6465-4de4-8027-4c5ff71bc833"), -1, true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("3cfe4e0c-93d9-45b5-b0b9-6dd58c648cc7"), new System.Guid("98f7d2f3-6465-4de4-8027-4c5ff71bc833"), -1, true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("7e73309c-ea7a-44a5-8c23-a9204fe858c3"), Janus.Windows.UI.Dock.PanelDockStyle.Top, new System.Drawing.Size(908, 117), true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("98f7d2f3-6465-4de4-8027-4c5ff71bc833"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("cf3f0df6-936a-4907-b1e5-935844cf0de0"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("45a62a62-69e3-460d-9844-e84dbea08c1e"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("22c1e50e-67ef-4d12-a06e-6787dd5dfa40"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("bba94330-4e00-4889-b3cf-72736cb4f638"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("3cfe4e0c-93d9-45b5-b0b9-6dd58c648cc7"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("7e73309c-ea7a-44a5-8c23-a9204fe858c3"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiPanel0.GroupStyle = Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator;
            this.uiPanel0.Location = new System.Drawing.Point(3, 35);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.SelectedPanel = this.uiPanel3;
            this.uiPanel0.Size = new System.Drawing.Size(226, 481);
            this.uiPanel0.TabIndex = 4;
            // 
            // uiPanel3
            // 
            this.uiPanel3.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel3.Image")));
            this.uiPanel3.InnerContainer = this.uiPanel3Container;
            this.uiPanel3.Location = new System.Drawing.Point(0, 0);
            this.uiPanel3.Name = "uiPanel3";
            this.uiPanel3.Size = new System.Drawing.Size(222, 377);
            this.uiPanel3.TabIndex = 4;
            this.uiPanel3.Text = "Quản lý Message và File ký";
            // 
            // uiPanel3Container
            // 
            this.uiPanel3Container.Controls.Add(this.explorerBarConfig);
            this.uiPanel3Container.Location = new System.Drawing.Point(1, 24);
            this.uiPanel3Container.Name = "uiPanel3Container";
            this.uiPanel3Container.Size = new System.Drawing.Size(220, 353);
            this.uiPanel3Container.TabIndex = 0;
            // 
            // explorerBarConfig
            // 
            this.explorerBarConfig.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarConfig.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarConfig.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarConfig.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBarConfig.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBarConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem1.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem1.Image")));
            explorerBarItem1.Key = "cmdMessageSend";
            explorerBarItem1.Text = "Danh sách Message gửi ký";
            explorerBarItem2.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem2.Image")));
            explorerBarItem2.Key = "cmdMessageSigned";
            explorerBarItem2.Text = "Danh sách Message đã ký";
            explorerBarGroup1.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem1,
            explorerBarItem2});
            explorerBarGroup1.Key = "Group1";
            explorerBarGroup1.Text = "Quản lý Message ký số";
            explorerBarItem3.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem3.Image")));
            explorerBarItem3.Key = "cmdFileSend";
            explorerBarItem3.Text = "Danh sách File gửi ký";
            explorerBarItem4.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem4.Image")));
            explorerBarItem4.Key = "cmdFileSigned";
            explorerBarItem4.Text = "Danh sách File đã ký";
            explorerBarItem5.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem5.Image")));
            explorerBarItem5.Key = "cmdSign";
            explorerBarItem5.Text = "Ký số cho File đính kèm";
            explorerBarGroup2.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem3,
            explorerBarItem4,
            explorerBarItem5});
            explorerBarGroup2.Key = "Group2";
            explorerBarGroup2.Text = "Quản lý File ký số";
            this.explorerBarConfig.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup1,
            explorerBarGroup2});
            this.explorerBarConfig.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarConfig.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBarConfig.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBarConfig.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBarConfig.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBarConfig.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBarConfig.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBarConfig.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarConfig.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBarConfig.Location = new System.Drawing.Point(0, 0);
            this.explorerBarConfig.Name = "explorerBarConfig";
            this.explorerBarConfig.Size = new System.Drawing.Size(220, 353);
            this.explorerBarConfig.TabIndex = 7;
            this.explorerBarConfig.Text = "explorerBar3";
            this.explorerBarConfig.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBarConfig.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerSign_ItemClick);
            // 
            // uiPanel4
            // 
            this.uiPanel4.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel4.Image")));
            this.uiPanel4.InnerContainer = this.uiPanel4Container;
            this.uiPanel4.Location = new System.Drawing.Point(0, 0);
            this.uiPanel4.Name = "uiPanel4";
            this.uiPanel4.Size = new System.Drawing.Size(222, 377);
            this.uiPanel4.TabIndex = 4;
            this.uiPanel4.Text = "Cấu hình hệ thống";
            // 
            // uiPanel4Container
            // 
            this.uiPanel4Container.Controls.Add(this.explorerSign);
            this.uiPanel4Container.Location = new System.Drawing.Point(1, 24);
            this.uiPanel4Container.Name = "uiPanel4Container";
            this.uiPanel4Container.Size = new System.Drawing.Size(220, 353);
            this.uiPanel4Container.TabIndex = 0;
            // 
            // explorerSign
            // 
            this.explorerSign.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerSign.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerSign.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerSign.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerSign.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerSign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.explorerSign.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            explorerBarItem6.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem6.Image")));
            explorerBarItem6.Key = "cmdConfig";
            explorerBarItem6.Text = "Cấu hình Thông số kết nối";
            explorerBarItem7.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem7.Image")));
            explorerBarItem7.Key = "cmdUSB";
            explorerBarItem7.Text = "Cấu hình chữ ký số";
            explorerBarGroup3.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem6,
            explorerBarItem7});
            explorerBarGroup3.Key = "Group1";
            explorerBarGroup3.Text = "Cấu hình hệ thống";
            explorerBarItem8.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem8.Image")));
            explorerBarItem8.Key = "cmdChangePassword";
            explorerBarItem8.Text = "Đổi mật khẩu";
            explorerBarItem9.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem9.Image")));
            explorerBarItem9.Key = "cmdList";
            explorerBarItem9.Text = "Danh sách Đại lý";
            explorerBarItem10.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem10.Image")));
            explorerBarItem10.Key = "cmdExit";
            explorerBarItem10.Text = "Thoát";
            explorerBarItem11.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem11.Image")));
            explorerBarItem11.Key = "cmdSwitchUser";
            explorerBarItem11.Text = "Đăng nhập người dùng khác";
            explorerBarGroup4.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem8,
            explorerBarItem9,
            explorerBarItem10,
            explorerBarItem11});
            explorerBarGroup4.Key = "Group2";
            explorerBarGroup4.Text = "Tài khoản";
            this.explorerSign.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup3,
            explorerBarGroup4});
            this.explorerSign.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerSign.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerSign.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerSign.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerSign.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerSign.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerSign.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerSign.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerSign.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerSign.Location = new System.Drawing.Point(0, 0);
            this.explorerSign.Name = "explorerSign";
            this.explorerSign.Size = new System.Drawing.Size(220, 353);
            this.explorerSign.TabIndex = 7;
            this.explorerSign.Text = "explorerBar3";
            this.explorerSign.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerSign.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarConfig_ItemClick);
            // 
            // uiPanel7
            // 
            this.uiPanel7.InnerContainer = this.uiPanel7Container;
            this.uiPanel7.Location = new System.Drawing.Point(229, 35);
            this.uiPanel7.Name = "uiPanel7";
            this.uiPanel7.Size = new System.Drawing.Size(908, 117);
            this.uiPanel7.TabIndex = 4;
            this.uiPanel7.Text = "TRẠNG THÁI";
            // 
            // uiPanel7Container
            // 
            this.uiPanel7Container.Controls.Add(this.uiGroupBox1);
            this.uiPanel7Container.Location = new System.Drawing.Point(1, 23);
            this.uiPanel7Container.Name = "uiPanel7Container";
            this.uiPanel7Container.Size = new System.Drawing.Size(906, 89);
            this.uiPanel7Container.TabIndex = 0;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Rebar;
            this.uiGroupBox1.Controls.Add(this.btnSign);
            this.uiGroupBox1.Controls.Add(this.lblStatus);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(906, 89);
            this.uiGroupBox1.TabIndex = 2;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSign
            // 
            this.btnSign.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSign.Image = ((System.Drawing.Image)(resources.GetObject("btnSign.Image")));
            this.btnSign.Location = new System.Drawing.Point(328, 43);
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(259, 36);
            this.btnSign.TabIndex = 3;
            this.btnSign.Text = "BẮT ĐẦU KÝ TỰ ĐỘNG";
            this.btnSign.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Blue;
            this.lblStatus.Location = new System.Drawing.Point(429, 17);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(74, 16);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.Text = "TẠM DỪNG";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(330, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "TRẠNG THÁI";
            // 
            // uiPanel2
            // 
            this.uiPanel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiPanel2.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel2.Image")));
            this.uiPanel2.InnerContainer = this.uiPanel2Container;
            this.uiPanel2.Location = new System.Drawing.Point(0, 0);
            this.uiPanel2.Name = "uiPanel2";
            this.uiPanel2.Size = new System.Drawing.Size(222, 453);
            this.uiPanel2.TabIndex = 4;
            this.uiPanel2.Text = "Ký chữ ký số";
            // 
            // uiPanel2Container
            // 
            this.uiPanel2Container.Location = new System.Drawing.Point(0, 0);
            this.uiPanel2Container.Name = "uiPanel2Container";
            this.uiPanel2Container.Size = new System.Drawing.Size(222, 453);
            this.uiPanel2Container.TabIndex = 0;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "PHẦN MỀM ECS-SIGNREMOTE";
            this.notifyIcon1.BalloonTipTitle = "CÔNG TY CỔ PHẦN SOFTECH";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "PHẦN MỀM ECS-SIGNREMOTE";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // uiPanel1
            // 
            this.uiPanel1.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel1.Image")));
            this.uiPanel1.ImageAlignment = Janus.Windows.UI.Dock.PanelImageAlignment.Near;
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(222, 453);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Cấu hình hệ thống";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(222, 453);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // uiPanel5
            // 
            this.uiPanel5.InnerContainer = this.uiPanel5Container;
            this.uiPanel5.Location = new System.Drawing.Point(0, 0);
            this.uiPanel5.Name = "uiPanel5";
            this.uiPanel5.Size = new System.Drawing.Size(222, 381);
            this.uiPanel5.TabIndex = 4;
            this.uiPanel5.Text = "Panel 5";
            // 
            // uiPanel5Container
            // 
            this.uiPanel5Container.Location = new System.Drawing.Point(0, 0);
            this.uiPanel5Container.Name = "uiPanel5Container";
            this.uiPanel5Container.Size = new System.Drawing.Size(222, 381);
            this.uiPanel5Container.TabIndex = 0;
            // 
            // uiPanel6
            // 
            this.uiPanel6.InnerContainer = this.uiPanel6Container;
            this.uiPanel6.Location = new System.Drawing.Point(0, 0);
            this.uiPanel6.Name = "uiPanel6";
            this.uiPanel6.Size = new System.Drawing.Size(222, 413);
            this.uiPanel6.TabIndex = 4;
            this.uiPanel6.Text = "Panel 6";
            // 
            // uiPanel6Container
            // 
            this.uiPanel6Container.Location = new System.Drawing.Point(0, 0);
            this.uiPanel6Container.Name = "uiPanel6Container";
            this.uiPanel6Container.Size = new System.Drawing.Size(222, 413);
            this.uiPanel6Container.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAutoUpdate,
            this.cmdUpdateDatabase,
            this.cmdVersion,
            this.cmdConfig,
            this.cmdConfigUSB,
            this.cmdExit,
            this.cmdLog,
            this.cmdCloseMe,
            this.cmdCloseAll,
            this.cmdCloseAllButMe});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.ContextMenus.AddRange(new Janus.Windows.UI.CommandBars.UIContextMenu[] {
            this.mnuRightClick});
            this.cmdMain.Id = new System.Guid("5da4a94d-ea80-4790-8f9f-cff90e8ba397");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmdMain.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick_1);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 605);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(1145, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAutoUpdate1,
            this.cmdUpdateDatabase1,
            this.cmdVersion1,
            this.cmdConfig1,
            this.cmdConfigUSB1,
            this.cmdLog1,
            this.cmdExit1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(947, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdAutoUpdate1
            // 
            this.cmdAutoUpdate1.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate1.Name = "cmdAutoUpdate1";
            // 
            // cmdUpdateDatabase1
            // 
            this.cmdUpdateDatabase1.Key = "cmdUpdateDatabase";
            this.cmdUpdateDatabase1.Name = "cmdUpdateDatabase1";
            // 
            // cmdVersion1
            // 
            this.cmdVersion1.Key = "cmdVersion";
            this.cmdVersion1.Name = "cmdVersion1";
            // 
            // cmdConfig1
            // 
            this.cmdConfig1.Key = "cmdConfig";
            this.cmdConfig1.Name = "cmdConfig1";
            // 
            // cmdConfigUSB1
            // 
            this.cmdConfigUSB1.Key = "cmdConfigUSB";
            this.cmdConfigUSB1.Name = "cmdConfigUSB1";
            // 
            // cmdLog1
            // 
            this.cmdLog1.Key = "cmdLog";
            this.cmdLog1.Name = "cmdLog1";
            // 
            // cmdExit1
            // 
            this.cmdExit1.Key = "cmdExit";
            this.cmdExit1.Name = "cmdExit1";
            // 
            // cmdAutoUpdate
            // 
            this.cmdAutoUpdate.Image = ((System.Drawing.Image)(resources.GetObject("cmdAutoUpdate.Image")));
            this.cmdAutoUpdate.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate.Name = "cmdAutoUpdate";
            this.cmdAutoUpdate.Text = "Cập nhật Chương trình";
            // 
            // cmdUpdateDatabase
            // 
            this.cmdUpdateDatabase.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateDatabase.Image")));
            this.cmdUpdateDatabase.Key = "cmdUpdateDatabase";
            this.cmdUpdateDatabase.Name = "cmdUpdateDatabase";
            this.cmdUpdateDatabase.Text = "Cập nhật cơ sở dữ liệu";
            // 
            // cmdVersion
            // 
            this.cmdVersion.Image = ((System.Drawing.Image)(resources.GetObject("cmdVersion.Image")));
            this.cmdVersion.Key = "cmdVersion";
            this.cmdVersion.Name = "cmdVersion";
            this.cmdVersion.Text = "Phiên bản CSDL";
            // 
            // cmdConfig
            // 
            this.cmdConfig.Image = ((System.Drawing.Image)(resources.GetObject("cmdConfig.Image")));
            this.cmdConfig.Key = "cmdConfig";
            this.cmdConfig.Name = "cmdConfig";
            this.cmdConfig.Text = "Cấu hình Thông số kết nối";
            // 
            // cmdConfigUSB
            // 
            this.cmdConfigUSB.Image = ((System.Drawing.Image)(resources.GetObject("cmdConfigUSB.Image")));
            this.cmdConfigUSB.Key = "cmdConfigUSB";
            this.cmdConfigUSB.Name = "cmdConfigUSB";
            this.cmdConfigUSB.Text = "Cấu hình Chữ ký số";
            // 
            // cmdExit
            // 
            this.cmdExit.Image = ((System.Drawing.Image)(resources.GetObject("cmdExit.Image")));
            this.cmdExit.Key = "cmdExit";
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Text = "Thoát";
            // 
            // cmdLog
            // 
            this.cmdLog.Image = ((System.Drawing.Image)(resources.GetObject("cmdLog.Image")));
            this.cmdLog.Key = "cmdLog";
            this.cmdLog.Name = "cmdLog";
            this.cmdLog.Text = "Nhật ký chương trình";
            // 
            // cmdCloseMe
            // 
            this.cmdCloseMe.Image = ((System.Drawing.Image)(resources.GetObject("cmdCloseMe.Image")));
            this.cmdCloseMe.Key = "cmdCloseMe";
            this.cmdCloseMe.Name = "cmdCloseMe";
            this.cmdCloseMe.Text = "Đóng cửa sổ này";
            // 
            // cmdCloseAll
            // 
            this.cmdCloseAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdCloseAll.Image")));
            this.cmdCloseAll.Key = "cmdCloseAll";
            this.cmdCloseAll.Name = "cmdCloseAll";
            this.cmdCloseAll.Text = "Đóng hết các cửa sổ";
            // 
            // cmdCloseAllButMe
            // 
            this.cmdCloseAllButMe.Image = ((System.Drawing.Image)(resources.GetObject("cmdCloseAllButMe.Image")));
            this.cmdCloseAllButMe.Key = "cmdCloseAllButMe";
            this.cmdCloseAllButMe.Name = "cmdCloseAllButMe";
            this.cmdCloseAllButMe.Text = "Đóng các cửa sổ khác";
            // 
            // mnuRightClick
            // 
            this.mnuRightClick.CommandManager = this.cmdMain;
            this.mnuRightClick.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCloseMe1,
            this.cmdCloseAllButMe1,
            this.cmdCloseAll1});
            this.mnuRightClick.Key = "mnuRightClick";
            // 
            // cmdCloseMe1
            // 
            this.cmdCloseMe1.Key = "cmdCloseMe";
            this.cmdCloseMe1.Name = "cmdCloseMe1";
            // 
            // cmdCloseAllButMe1
            // 
            this.cmdCloseAllButMe1.Key = "cmdCloseAllButMe";
            this.cmdCloseAllButMe1.Name = "cmdCloseAllButMe1";
            // 
            // cmdCloseAll1
            // 
            this.cmdCloseAll1.Key = "cmdCloseAll";
            this.cmdCloseAll1.Name = "cmdCloseAll1";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 32);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 573);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(1145, 32);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 573);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1140, 32);
            // 
            // popupNotifier1
            // 
            this.popupNotifier1.BodyColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(120)))), ((int)(((byte)(100)))));
            this.popupNotifier1.BorderColor = System.Drawing.Color.Transparent;
            this.popupNotifier1.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(120)))), ((int)(((byte)(100)))));
            this.popupNotifier1.ButtonHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(120)))), ((int)(((byte)(100)))));
            this.popupNotifier1.ContentColor = System.Drawing.Color.White;
            this.popupNotifier1.ContentFont = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.popupNotifier1.ContentHoverColor = System.Drawing.Color.White;
            this.popupNotifier1.ContentText = null;
            this.popupNotifier1.HeaderColor = System.Drawing.Color.White;
            this.popupNotifier1.HeaderHeight = 1;
            this.popupNotifier1.Image = ((System.Drawing.Image)(resources.GetObject("popupNotifier1.Image")));
            this.popupNotifier1.OptionsMenu = null;
            this.popupNotifier1.Size = new System.Drawing.Size(400, 100);
            this.popupNotifier1.TitleColor = System.Drawing.Color.White;
            this.popupNotifier1.TitleFont = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.popupNotifier1.TitlePadding = new System.Windows.Forms.Padding(60, 5, 5, 7);
            this.popupNotifier1.TitleText = null;
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(1140, 519);
            this.cmdMain.SetContextMenu(this, this.mnuRightClick);
            this.Controls.Add(this.uiPanel7);
            this.Controls.Add(this.uiPanel0);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Phần mềm Quản lý ký chữ ký số";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanel0, 0);
            this.Controls.SetChildIndex(this.uiPanel7, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel3)).EndInit();
            this.uiPanel3.ResumeLayout(false);
            this.uiPanel3Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel4)).EndInit();
            this.uiPanel4.ResumeLayout(false);
            this.uiPanel4Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerSign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel7)).EndInit();
            this.uiPanel7.ResumeLayout(false);
            this.uiPanel7Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).EndInit();
            this.uiPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel5)).EndInit();
            this.uiPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel6)).EndInit();
            this.uiPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mnuRightClick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.Dock.UIPanelManager uiPanelManager1;
        private Janus.Windows.UI.Dock.UIPanelGroup uiPanel0;
        private Janus.Windows.UI.Dock.UIPanel uiPanel1;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel1Container;
        private Janus.Windows.UI.Dock.UIPanel uiPanel2;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel2Container;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private Janus.Windows.UI.Dock.UIPanel uiPanel3;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel3Container;
        private Janus.Windows.UI.Dock.UIPanel uiPanel5;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel5Container;
        private Janus.Windows.UI.Dock.UIPanel uiPanel6;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel6Container;
        private Janus.Windows.UI.Dock.UIPanel uiPanel4;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel4Container;
        private Janus.Windows.ExplorerBar.ExplorerBar explorerSign;
        private Janus.Windows.ExplorerBar.ExplorerBar explorerBarConfig;
        private System.Windows.Forms.Timer timer1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAutoUpdate;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateDatabase;
        private Janus.Windows.UI.CommandBars.UICommand cmdVersion;
        private Janus.Windows.UI.CommandBars.UICommand cmdConfig;
        private Janus.Windows.UI.CommandBars.UICommand cmdConfigUSB;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAutoUpdate1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateDatabase1;
        private Janus.Windows.UI.CommandBars.UICommand cmdVersion1;
        private Janus.Windows.UI.CommandBars.UICommand cmdConfig1;
        private Janus.Windows.UI.CommandBars.UICommand cmdConfigUSB1;
        private Janus.Windows.UI.CommandBars.UICommand cmdExit1;
        private Janus.Windows.UI.CommandBars.UICommand cmdExit;
        private Janus.Windows.UI.CommandBars.UICommand cmdLog;
        private Janus.Windows.UI.CommandBars.UICommand cmdLog1;
        private NotificationWindow.PopupNotifier popupNotifier1;
        private Janus.Windows.UI.Dock.UIPanel uiPanel7;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel7Container;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnSign;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblStatus;
        private Janus.Windows.UI.CommandBars.UICommand cmdCloseMe;
        private Janus.Windows.UI.CommandBars.UICommand cmdCloseAll;
        private Janus.Windows.UI.CommandBars.UICommand cmdCloseAllButMe;
        private Janus.Windows.UI.CommandBars.UIContextMenu mnuRightClick;
        private Janus.Windows.UI.CommandBars.UICommand cmdCloseMe1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCloseAllButMe1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCloseAll1;
    }
}
