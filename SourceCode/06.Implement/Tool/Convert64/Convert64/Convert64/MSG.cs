﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace Convert64
{
    public partial class cMSG
    {
        public static DataTable SelectIDs()
        {
            string spName = "SELECT [ID] FROM dbo.[MSG] order by ID asc";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public static DataTable SelectID(string ngayKB)
        {
            string spName = "SELECT [ID] FROM dbo.[MSG] where LEFT(GhiCHu, 10) = '" + ngayKB + "' order by ID asc";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public static DataTable SelectDates()
        {
            const string spName = "SELECT LEFT(GhiCHu, 10) as NgayKB FROM dbo.[MSG] GROUP BY LEFT(GhiCHu, 10) ORDER BY CONVERT(DATETIME, LEFT(GhiCHu, 10), 103) desc";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
    }
}
