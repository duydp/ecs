﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public partial class HeThongPhongKhai
    {
        /// <summary>
        /// lấy thông tin các doanh nghiệp mà đại lý khai báo
        /// </summary>
        /// <returns> Danh sách các doanh nghiệp (bao gồm Mã doanh nghiệp và Tên doanh nghiệp) dưới dạng XML </returns>
        public static string GetDoanhNghiep()
        {
            string result = string.Empty;
            List<HeThongPhongKhai> listhtpk = new List<HeThongPhongKhai>();
            listhtpk = SelectCollectionDynamic("Key_Config = 'CauHinh' AND Value_Config = 1 AND Role = 0", null);
            if (listhtpk != null && listhtpk.Count > 0)
            {
                result = Helpers.Serializer(listhtpk);
            }
            return result;
        }
        /// <summary>
        /// Lấy thông tin đại lý
        /// </summary>
        /// <returns></returns>
        public static ThongTinDaiLy GetThongTinDL()
        {
            ThongTinDaiLy dl = new ThongTinDaiLy();
            List<HeThongPhongKhai> listhtpk = new List<HeThongPhongKhai>();
            listhtpk = SelectCollectionDynamic("Role = 1", null);

            if (listhtpk != null && listhtpk.Count > 0)
            {
                dl.TenDL = listhtpk[0].TenDoanhNghiep.Trim();
                dl.MaDL = listhtpk[0].MaDoanhNghiep.Trim();
                foreach (HeThongPhongKhai item in listhtpk)
                {
                    if(dl.TenDL == item.TenDoanhNghiep.Trim() && dl.MaDL == item.MaDoanhNghiep.Trim())
                    switch (item.Key_Config)
                    {
                        case "DIA_CHI":
                            dl.DiaChiDL = item.Value_Config;
                            break;
                        case "MailDoanhNghiep":
                            dl.EmailDL = item.Value_Config;
                            break;
                        case "ÐIEN_THOAI":
                            dl.SoDienThoaiDL = item.Value_Config;
                            break;
                        case "NGUOI_LIEN_HE":
                            dl.NguoiLienHeDL = item.Value_Config;
                            break;
                        case "SO_FAX":
                            dl.FaxDL = item.Value_Config;
                            break;
                    }
                }
                return dl;
            }
            else
                return null;
        }
        public static bool InsertThongTinDL(ThongTinDaiLy DLinfo)
        {
            List<HeThongPhongKhai> listhtpk = new List<HeThongPhongKhai>();
            //HeThongPhongKhai.DeleteHeThongPhongKhai(DLinfo.MaDL, "1");
            for (int i = 0; i < 5; i++)
            {
                HeThongPhongKhai htpk = new HeThongPhongKhai();
                htpk.TenDoanhNghiep = DLinfo.TenDL;
                htpk.MaDoanhNghiep = DLinfo.MaDL;
                switch (i)
                {
                    case 0:
                        htpk.Key_Config = "DIA_CHI";
                        htpk.Value_Config = DLinfo.DiaChiDL;
                        break;
                    case 1:
                        htpk.Key_Config = "MailDoanhNghiep";
                        htpk.Value_Config = DLinfo.EmailDL;
                        break;
                    case 2:
                        htpk.Key_Config = "ÐIEN_THOAI";
                        htpk.Value_Config = DLinfo.SoDienThoaiDL;
                        break;
                    case 3:
                        htpk.Key_Config = "NGUOI_LIEN_HE";
                        htpk.Value_Config = DLinfo.NguoiLienHeDL;
                        break;
                    case 4:
                        htpk.Key_Config = "SO_FAX";
                        htpk.Value_Config = DLinfo.FaxDL;
                        break;
                }
                htpk.PassWord = "0";
                htpk.Role = 1;
                listhtpk.Add(htpk);
            }
            if (listhtpk.Count > 0)
            {
                return HeThongPhongKhai.InsertUpdateCollection(listhtpk);
            }
            else
                return false;
        }
        #region updatemaDl
        public static bool UpdateInfoDaiLy(ThongTinDaiLy DLinfo)
        {
            try
            {
                List<HeThongPhongKhai> listhtpk = new List<HeThongPhongKhai>();
                for (int i = 0; i < 5; i++)
                {

                    string where = " where [role]=1 and Key_config=";
                    HeThongPhongKhai htpk = new HeThongPhongKhai();
                    htpk.TenDoanhNghiep = DLinfo.TenDL;
                    htpk.MaDoanhNghiep = DLinfo.MaDL;
                    string query = " update t_hethongphongkhai set madoanhnghiep ='" + DLinfo.MaDL + "', tendoanhnghiep='" + DLinfo.TenDL + "', value_config=";
                    switch (i)
                    {
                        case 0:
                            query = query + "'" + DLinfo.DiaChiDL + "'" + where + "'DIA_CHI'";
                            HeThongPhongKhai.UpdateDaiLy(query);
                            break;
                        case 1:
                            query = query + "'" + DLinfo.EmailDL + "'" + where + "'MailDoanhNghiep'";
                            HeThongPhongKhai.UpdateDaiLy(query);
                            break;
                        case 2:
                            query = query + "'" + DLinfo.SoDienThoaiDL + "'" + where + "'ÐIEN_THOAI'";
                            HeThongPhongKhai.UpdateDaiLy(query);
                            break;
                        case 3:
                            query = query + "'" + DLinfo.NguoiLienHeDL + "'" + where + "'NGUOI_LIEN_HE'";
                            HeThongPhongKhai.UpdateDaiLy(query);
                            break;
                        case 4:
                            query = query + "'" + DLinfo.FaxDL + "'" + where + "'SO_FAX'";
                            HeThongPhongKhai.UpdateDaiLy(query);
                            break;
                    }
                    htpk.PassWord = "0";
                    htpk.Role = 1;
                    listhtpk.Add(htpk);

                }
                return true;
            }
            catch
            {
                return false;
            }
        #endregion

        }



       
    }
}
