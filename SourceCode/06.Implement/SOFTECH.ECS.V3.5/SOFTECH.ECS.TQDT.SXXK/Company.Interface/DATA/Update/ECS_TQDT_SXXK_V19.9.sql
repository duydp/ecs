
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_SXXK_HangTonDangKy]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_SXXK_HangTonDangKy](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoTiepNhan] [bigint] NULL,
	[NgayTiepNhan] [datetime] NULL,
	[MaHaiQuan] [varchar](6) NULL,
	[MaDoanhNghiep] [varchar](20) NULL,
	[NamQuyetToan] [int] NULL,
	[QuyQuyetToan] [int] NULL,
	[TrangThaiXuLy] [int] NULL,
	[GuidStr] [varchar](100) NULL,
	[DeXuatKhac] [nvarchar](250) NULL,
 CONSTRAINT [PK_t_KDT_SXXK_HangTonDangKy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_SXXK_HangTon]') AND type in (N'U'))
Begin
CREATE TABLE [dbo].[t_KDT_SXXK_HangTon](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_id] [bigint] NOT NULL,
	[MaHang] [nvarchar](250) NULL,
	[MaHS] [varchar](50) NULL,
	[TenHang] [nvarchar](255) NULL,
	[LoaiHang] [int] NULL,
	[DVT_ID] [varchar](10) NULL,
	[LuongTonSoSach] [numeric](18, 4) NULL,
	[LuongTonThucTe] [numeric](18, 4) NULL,
	[GhiChu] [nvarchar](255) NULL,
 CONSTRAINT [PK_t_KDT_SXXK_HangTon] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
End
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTonDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTonDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTonDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTonDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTonDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTonDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTonDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTonDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTonDangKy_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@NamQuyetToan int,
	@QuyQuyetToan int,
	@TrangThaiXuLy int,
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_HangTonDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[NamQuyetToan],
	[QuyQuyetToan],
	[TrangThaiXuLy],
	[GuidStr],
	[DeXuatKhac]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@MaDoanhNghiep,
	@NamQuyetToan,
	@QuyQuyetToan,
	@TrangThaiXuLy,
	@GuidStr,
	@DeXuatKhac
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTonDangKy_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@NamQuyetToan int,
	@QuyQuyetToan int,
	@TrangThaiXuLy int,
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250)
AS

UPDATE
	[dbo].[t_KDT_SXXK_HangTonDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[NamQuyetToan] = @NamQuyetToan,
	[QuyQuyetToan] = @QuyQuyetToan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GuidStr] = @GuidStr,
	[DeXuatKhac] = @DeXuatKhac
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTonDangKy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@NamQuyetToan int,
	@QuyQuyetToan int,
	@TrangThaiXuLy int,
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_HangTonDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_HangTonDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[NamQuyetToan] = @NamQuyetToan,
			[QuyQuyetToan] = @QuyQuyetToan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GuidStr] = @GuidStr,
			[DeXuatKhac] = @DeXuatKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_HangTonDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[MaDoanhNghiep],
			[NamQuyetToan],
			[QuyQuyetToan],
			[TrangThaiXuLy],
			[GuidStr],
			[DeXuatKhac]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@MaDoanhNghiep,
			@NamQuyetToan,
			@QuyQuyetToan,
			@TrangThaiXuLy,
			@GuidStr,
			@DeXuatKhac
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTonDangKy_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_HangTonDangKy]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTonDangKy_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_HangTonDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTonDangKy_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[NamQuyetToan],
	[QuyQuyetToan],
	[TrangThaiXuLy],
	[GuidStr],
	[DeXuatKhac]
FROM
	[dbo].[t_KDT_SXXK_HangTonDangKy]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTonDangKy_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[NamQuyetToan],
	[QuyQuyetToan],
	[TrangThaiXuLy],
	[GuidStr],
	[DeXuatKhac]
FROM [dbo].[t_KDT_SXXK_HangTonDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTonDangKy_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTonDangKy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[NamQuyetToan],
	[QuyQuyetToan],
	[TrangThaiXuLy],
	[GuidStr],
	[DeXuatKhac]
FROM
	[dbo].[t_KDT_SXXK_HangTonDangKy]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTon_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTon_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTon_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTon_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTon_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTon_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTon_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTon_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTon_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTon_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTon_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTon_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTon_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTon_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_HangTon_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_HangTon_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTon_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTon_Insert]
	@Master_id bigint,
	@MaHang nvarchar(250),
	@MaHS varchar(50),
	@TenHang nvarchar(255),
	@LoaiHang int,
	@DVT_ID varchar(10),
	@LuongTonSoSach numeric(18, 4),
	@LuongTonThucTe numeric(18, 4),
	@GhiChu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_HangTon]
(
	[Master_id],
	[MaHang],
	[MaHS],
	[TenHang],
	[LoaiHang],
	[DVT_ID],
	[LuongTonSoSach],
	[LuongTonThucTe],
	[GhiChu]
)
VALUES 
(
	@Master_id,
	@MaHang,
	@MaHS,
	@TenHang,
	@LoaiHang,
	@DVT_ID,
	@LuongTonSoSach,
	@LuongTonThucTe,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTon_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTon_Update]
	@ID bigint,
	@Master_id bigint,
	@MaHang nvarchar(250),
	@MaHS varchar(50),
	@TenHang nvarchar(255),
	@LoaiHang int,
	@DVT_ID varchar(10),
	@LuongTonSoSach numeric(18, 4),
	@LuongTonThucTe numeric(18, 4),
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_SXXK_HangTon]
SET
	[Master_id] = @Master_id,
	[MaHang] = @MaHang,
	[MaHS] = @MaHS,
	[TenHang] = @TenHang,
	[LoaiHang] = @LoaiHang,
	[DVT_ID] = @DVT_ID,
	[LuongTonSoSach] = @LuongTonSoSach,
	[LuongTonThucTe] = @LuongTonThucTe,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTon_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTon_InsertUpdate]
	@ID bigint,
	@Master_id bigint,
	@MaHang nvarchar(250),
	@MaHS varchar(50),
	@TenHang nvarchar(255),
	@LoaiHang int,
	@DVT_ID varchar(10),
	@LuongTonSoSach numeric(18, 4),
	@LuongTonThucTe numeric(18, 4),
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_HangTon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_HangTon] 
		SET
			[Master_id] = @Master_id,
			[MaHang] = @MaHang,
			[MaHS] = @MaHS,
			[TenHang] = @TenHang,
			[LoaiHang] = @LoaiHang,
			[DVT_ID] = @DVT_ID,
			[LuongTonSoSach] = @LuongTonSoSach,
			[LuongTonThucTe] = @LuongTonThucTe,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_HangTon]
		(
			[Master_id],
			[MaHang],
			[MaHS],
			[TenHang],
			[LoaiHang],
			[DVT_ID],
			[LuongTonSoSach],
			[LuongTonThucTe],
			[GhiChu]
		)
		VALUES 
		(
			@Master_id,
			@MaHang,
			@MaHS,
			@TenHang,
			@LoaiHang,
			@DVT_ID,
			@LuongTonSoSach,
			@LuongTonThucTe,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTon_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_HangTon]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTon_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_HangTon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTon_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[MaHang],
	[MaHS],
	[TenHang],
	[LoaiHang],
	[DVT_ID],
	[LuongTonSoSach],
	[LuongTonThucTe],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_HangTon]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTon_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_id],
	[MaHang],
	[MaHS],
	[TenHang],
	[LoaiHang],
	[DVT_ID],
	[LuongTonSoSach],
	[LuongTonThucTe],
	[GhiChu]
FROM [dbo].[t_KDT_SXXK_HangTon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTon_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 24, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTon_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[MaHang],
	[MaHS],
	[TenHang],
	[LoaiHang],
	[DVT_ID],
	[LuongTonSoSach],
	[LuongTonThucTe],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_HangTon]	

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BCXuatNhapTon_GetBCXNTByLanThanhLy]    Script Date: 10/01/2014 11:57:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

        
ALTER PROCEDURE [dbo].[p_KDT_SXXK_BCXuatNhapTon_GetBCXNTByLanThanhLy]        
 @MaDoanhNghiep NVarchar(14),        
 @LanThanhLy int        
AS        
        
SET NOCOUNT ON        
SET TRANSACTION ISOLATION LEVEL READ COMMITTED        
    
Select      
MAX(SoToKhaiNhap) AS SoToKhaiNhap,       
MAX(A.MaLoaiHinhNhap) AS MaLoaiHinh,  
YEAR(MAX(NgayDangKyNhap)) AS NamDK,  
 MaNPL,        
 MAX(TenNPL) as TenNPL,         
 SUM(LuongTonDau) as LuongTonDau,        
 Sum(LuongNhap) as LuongNhap,        
 Sum(LuongNPLSuDung) as LuongNPLSuDung,        
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,        
 Sum(LuongNopThue) as LuongNopThue,        
 Sum(LuongXuatTheoHD) as LuongXuatTheoHD,        
 Sum(LuongTonCuoi) as LuongTonCuoi,        
 B.DVT_ID, --MAX(ID_DVT_NPL) as ID_DVT_NPL,        
 Convert(DECIMAL(25,6),DonGiaTT) AS DonGiaTT,  
 ThueSuat as ThueSuat       
 From         
(         
SELECT        
 SoToKhaiNhap,   
 MaLoaiHinhNhap,       
 NgayDangKyNhap,        
 MaNPL,        
 TenNPL,         
 LuongTonDau,        
 CASE when  LuongNhap = LuongTonDau then LuongNhap else 0 end as LuongNhap,         
 Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung,         
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,         
 0 as LuongNopThue,        
 0 as LuongXuatTheoHD,        
 --case         
 -- when (LuongTonDau - Sum(LuongNPLSuDung)) >= 0         
 -- then  LuongTonDau - Sum(LuongNPLSuDung)        
 -- else 0        
 -- end         
 -- as LuongTonCuoi,        
 Case When Min(LuongTonCuoi) < 0 then 0 else Min(LuongTonCuoi) end as LuongTonCuoi,        
 (select ID from t_HaiQuan_DonViTinh where ten=TenDVT_NPL)as ID_DVT_NPL,        
 DonGiaTT,        
 ThueSuat         
FROM        
 [dbo].[t_KDT_SXXK_BCXuatNhapTon]        
WHERE MaDoanhNghiep =@MaDoanhNghiep AND LanThanhLy =@LanThanhLy        
GROUP BY NgayDangKyNhap, SoToKhaiNhap, MaLoaiHinhNhap, MaNPL,TenNPL, TenDVT_NPL,DonGiaTT, LuongTonDau,LuongNhap,ThueSuat        
) as A inner join t_SXXK_NguyenPhuLieu B        
on A.MaNPL = B.Ma  AND B.MaDoanhNghiep = @MaDoanhNghiep      
        
Group by MaNPL,Convert(DECIMAL(25,6),DonGiaTT) ,B.DVT_ID, ThueSuat        
order by MaNPL  
Go


/****** Object:  UserDefinedFunction [dbo].[f_KDT_ToKhaimauDich_TheoDoiThanhKhoan_DongHoSo]    Script Date: 09/22/2014 14:01:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[f_KDT_ToKhaimauDich_TheoDoiThanhKhoan_DongHoSo]
    (
      @MaDoanhNghiep VARCHAR(50) ,
      @SoToKhai BIGINT ,
      @NamDangKy INT,
      @MaLoaiHinh VARCHAR(6)
    )
RETURNS INT
AS 
    BEGIN
/*
HungTQ, Created 05/04/2011.
Theo doi to khai nhap co tham gia thanh khoan va da dong ho so khong?
*/
        DECLARE @Return INT ,
            @TrangThaiThanhKhoan INT --400: Da chay thanh khoan, 401:Da dong ho so.
        SET @TrangThaiThanhKhoan = 401

        
        SELECT  @Return = COUNT(*)
        FROM    [dbo].[t_KDT_SXXK_NPLNhapTon]
        WHERE   TonCuoi < TonDau
                AND LanThanhLy IN ( SELECT  LanThanhLy
                                    FROM    t_KDT_SXXK_HoSoThanhLyDangKy
                                    WHERE   TrangThaiThanhKhoan = @TrangThaiThanhKhoan --Chua dong ho so = !=401
                           )
                AND MaDoanhNghiep = @MaDoanhNghiep
                AND MaLoaiHinh LIKE 'N%'
                AND SoToKhai = @SoToKhai
                AND NamDangKy = @NamDangKy
                AND MaLoaiHinh = @MaLoaiHinh
                
        RETURN @Return
    END        

Go

/****** Object:  UserDefinedFunction [dbo].[f_KDT_ToKhaimauDich_TheoDoiThanhKhoan_ThamGiaThanhKhoan]    Script Date: 09/22/2014 14:04:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[f_KDT_ToKhaimauDich_TheoDoiThanhKhoan_ThamGiaThanhKhoan]
    (
      @MaDoanhNghiep VARCHAR(50) ,
      @SoToKhai BIGINT ,
      @NamDangKy INT,
      @MaLoaiHinh VARCHAR(6)
    )
RETURNS INT
AS 
    BEGIN
/*
HungTQ, Created 05/04/2011.
Theo doi to khai nhap co tham gia thanh khoan va da dong ho so khong?
*/
        DECLARE @Return INT ,
            @TrangThaiThanhKhoan INT --400: Da chay thanh khoan, 401:Da dong ho so.
        SET @TrangThaiThanhKhoan = 400

        
        SELECT  @Return = COUNT(*)
        FROM    [dbo].[t_KDT_SXXK_NPLNhapTon]
        WHERE   TonCuoi < TonDau
                AND LanThanhLy IN ( SELECT  LanThanhLy
                                    FROM    t_KDT_SXXK_HoSoThanhLyDangKy
                                    WHERE   TrangThaiThanhKhoan = @TrangThaiThanhKhoan --Chua dong ho so = !=401
                           )
                AND MaDoanhNghiep = @MaDoanhNghiep
                AND MaLoaiHinh LIKE 'N%'
                AND SoToKhai = @SoToKhai
                AND NamDangKy = @NamDangKy
                AND MaLoaiHinh = @MaLoaiHinh
                
        RETURN @Return
    END        

Go

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.9',GETDATE(), N' create Table HangTon')
END