﻿namespace Company.Interface
{
    partial class HangTon_OLD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HangTon_OLD));
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgListNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgListNPL_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgListSP_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgListSP_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgListToTal_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgListToTal_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgListDetail_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgListDetail_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgListTotalExport_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgListTotalExport_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout gridListDetail_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference gridListDetail_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            this.mnuGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripThanhkhoan = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripChuyenTieuThuNoiDia = new System.Windows.Forms.ToolStripMenuItem();
            this.gridEXPrintDocument1 = new Janus.Windows.GridEX.GridEXPrintDocument();
            this.gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.chkTuDongTinhThue = new Janus.Windows.EditControls.UICheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkHienThiTKKhongDuyet = new Janus.Windows.EditControls.UICheckBox();
            this.chkHienThiLech = new Janus.Windows.EditControls.UICheckBox();
            this.cbbLanThanhLy = new Janus.Windows.EditControls.UICheckBox();
            this.chkTimChinhXac = new Janus.Windows.EditControls.UICheckBox();
            this.cbTrangThai = new Janus.Windows.EditControls.UIComboBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenChuHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLanThanhLy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtNamTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListNPL = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnExportExcelNPL = new Janus.Windows.EditControls.UIButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtTenChuHang2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTimKiemNPL = new Janus.Windows.EditControls.UIButton();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListSP = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnExportExcelSP = new Janus.Windows.EditControls.UIButton();
            this.cbbStatus = new Janus.Windows.EditControls.UIComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtChuHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTenSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnSearchSP = new Janus.Windows.EditControls.UIButton();
            this.txtSoTK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtLanTL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNamDK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListToTal = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnTotalExport = new Janus.Windows.EditControls.UIButton();
            this.btnProcess = new Janus.Windows.EditControls.UIButton();
            this.uiTabPage5 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListDetail = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.uiTabPage6 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListTotalExport = new Janus.Windows.GridEX.GridEX();
            this.mnuNPLXuat = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuTKXuatSP = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTKNhapNPL = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.uiTabPage7 = new Janus.Windows.UI.Tab.UITabPage();
            this.gridListDetail = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.dateToDetail = new System.Windows.Forms.DateTimePicker();
            this.dateFromDetail = new System.Windows.Forms.DateTimePicker();
            this.btnExportExcelDetail = new Janus.Windows.EditControls.UIButton();
            this.btnSearchDetail = new Janus.Windows.EditControls.UIButton();
            this.backgroundHangTon = new System.ComponentModel.BackgroundWorker();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdDieuChinhThue1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDieuChinhThue");
            this.cmdDieuChinhTon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDieuChinhTon");
            this.cmdKiemTraNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKiemTraNPL");
            this.cmdCapNhatTKSai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatTKSai");
            this.cmdCapNhatTK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatTK");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdExportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdDieuChinhThue = new Janus.Windows.UI.CommandBars.UICommand("cmdDieuChinhThue");
            this.cmdDieuChinhTon = new Janus.Windows.UI.CommandBars.UICommand("cmdDieuChinhTon");
            this.cmdKiemTraNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdKiemTraNPL");
            this.cmdCapNhatTKSai = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatTKSai");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdExportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdClose = new Janus.Windows.UI.CommandBars.UICommand("cmdClose");
            this.cmdCapNhatTK = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatTK");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.mnuGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListToTal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.uiTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDetail)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.uiTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotalExport)).BeginInit();
            this.mnuNPLXuat.SuspendLayout();
            this.uiTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridListDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(1122, 610);
            // 
            // vsmMain
            // 
            this.vsmMain.DefaultColorScheme = "Office2003";
            // 
            // mnuGrid
            // 
            this.mnuGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripThanhkhoan,
            this.toolStripChuyenTieuThuNoiDia});
            this.mnuGrid.Name = "mnuGrid";
            this.mnuGrid.Size = new System.Drawing.Size(260, 48);
            this.mnuGrid.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuGrid_ItemClicked);
            // 
            // toolStripThanhkhoan
            // 
            this.toolStripThanhkhoan.Image = ((System.Drawing.Image)(resources.GetObject("toolStripThanhkhoan.Image")));
            this.toolStripThanhkhoan.Name = "toolStripThanhkhoan";
            this.toolStripThanhkhoan.Size = new System.Drawing.Size(259, 22);
            this.toolStripThanhkhoan.Text = "Theo dõi thanh khoản tờ khai nhập";
            // 
            // toolStripChuyenTieuThuNoiDia
            // 
            this.toolStripChuyenTieuThuNoiDia.Image = ((System.Drawing.Image)(resources.GetObject("toolStripChuyenTieuThuNoiDia.Image")));
            this.toolStripChuyenTieuThuNoiDia.Name = "toolStripChuyenTieuThuNoiDia";
            this.toolStripChuyenTieuThuNoiDia.Size = new System.Drawing.Size(259, 22);
            this.toolStripChuyenTieuThuNoiDia.Text = "Chuyển NPL tiêu thụ nội địa";
            this.toolStripChuyenTieuThuNoiDia.Click += new System.EventHandler(this.toolStripChuyenTieuThuNoiDia_Click);
            // 
            // gridEXPrintDocument1
            // 
            this.gridEXPrintDocument1.CardColumnsPerPage = 1;
            this.gridEXPrintDocument1.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintDocument1.PageHeaderCenter = "DANH SÁCH NGUYÊN PHỤ LIỆU TỒN";
            this.gridEXPrintDocument1.PageHeaderFormatStyle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // gridEXExporter1
            // 
            this.gridEXExporter1.SheetName = "NPLTon";
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(1122, 610);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2,
            this.uiTabPage3,
            this.uiTabPage4,
            this.uiTabPage5,
            this.uiTabPage6,
            this.uiTabPage7});
            this.uiTab1.VisualStyleManager = this.vsmMain;
            this.uiTab1.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.uiTab1_SelectedTabChanged);
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.dgList);
            this.uiTabPage1.Controls.Add(this.uiGroupBox4);
            this.uiTabPage1.Controls.Add(this.uiGroupBox2);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1120, 588);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Quản lý NPL theo tờ khai";
            // 
            // dgList
            // 
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            this.dgList.ContextMenuStrip = this.mnuGrid;
            dgList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList_DesignTimeLayout_Reference_0.Instance")));
            dgList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList_DesignTimeLayout_Reference_0});
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(0, 94);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(1120, 453);
            this.dgList.TabIndex = 12;
            this.dgList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.EditingCell += new Janus.Windows.GridEX.EditingCellEventHandler(this.dgList_EditingCell);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            this.dgList.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_FormattingRow);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.panel2);
            this.uiGroupBox4.Controls.Add(this.panel1);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.chkTuDongTinhThue);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 547);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1120, 41);
            this.uiGroupBox4.TabIndex = 11;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.Color.Yellow;
            this.panel2.Location = new System.Drawing.Point(318, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(29, 23);
            this.panel2.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Location = new System.Drawing.Point(7, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(29, 23);
            this.panel1.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(353, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(443, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Các tờ khai không nằm trong trạng thái đã duyệt ( đang chờ HQ duyệt hoặc đang sửa" +
                ",hủy)";
            // 
            // chkTuDongTinhThue
            // 
            this.chkTuDongTinhThue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkTuDongTinhThue.BackColor = System.Drawing.Color.Transparent;
            this.chkTuDongTinhThue.Checked = true;
            this.chkTuDongTinhThue.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTuDongTinhThue.Location = new System.Drawing.Point(994, 12);
            this.chkTuDongTinhThue.Name = "chkTuDongTinhThue";
            this.chkTuDongTinhThue.Size = new System.Drawing.Size(115, 23);
            this.chkTuDongTinhThue.TabIndex = 4;
            this.chkTuDongTinhThue.Text = "Tự động tính thuế";
            this.chkTuDongTinhThue.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(42, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(226, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Các dòng nguyên phụ liệu bị sai lệch thông tin";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.chkHienThiTKKhongDuyet);
            this.uiGroupBox2.Controls.Add(this.chkHienThiLech);
            this.uiGroupBox2.Controls.Add(this.cbbLanThanhLy);
            this.uiGroupBox2.Controls.Add(this.chkTimChinhXac);
            this.uiGroupBox2.Controls.Add(this.cbTrangThai);
            this.uiGroupBox2.Controls.Add(this.btnSearch);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtTenChuHang);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.txtMaNPL);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.txtLanThanhLy);
            this.uiGroupBox2.Controls.Add(this.txtNamTiepNhan);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1120, 94);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // chkHienThiTKKhongDuyet
            // 
            this.chkHienThiTKKhongDuyet.BackColor = System.Drawing.Color.Transparent;
            this.chkHienThiTKKhongDuyet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHienThiTKKhongDuyet.Location = new System.Drawing.Point(495, 71);
            this.chkHienThiTKKhongDuyet.Name = "chkHienThiTKKhongDuyet";
            this.chkHienThiTKKhongDuyet.Size = new System.Drawing.Size(271, 18);
            this.chkHienThiTKKhongDuyet.TabIndex = 7;
            this.chkHienThiTKKhongDuyet.Text = "Chỉ hiển thị thông tin của tờ khai không duyệt";
            this.chkHienThiTKKhongDuyet.VisualStyleManager = this.vsmMain;
            this.chkHienThiTKKhongDuyet.CheckedChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // chkHienThiLech
            // 
            this.chkHienThiLech.BackColor = System.Drawing.Color.Transparent;
            this.chkHienThiLech.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHienThiLech.Location = new System.Drawing.Point(305, 71);
            this.chkHienThiLech.Name = "chkHienThiLech";
            this.chkHienThiLech.Size = new System.Drawing.Size(184, 18);
            this.chkHienThiLech.TabIndex = 6;
            this.chkHienThiLech.Text = "Chỉ hiển thị thông tin NPL sai lệch";
            this.chkHienThiLech.VisualStyleManager = this.vsmMain;
            this.chkHienThiLech.CheckedChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // cbbLanThanhLy
            // 
            this.cbbLanThanhLy.BackColor = System.Drawing.Color.Transparent;
            this.cbbLanThanhLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLanThanhLy.Location = new System.Drawing.Point(655, 20);
            this.cbbLanThanhLy.Name = "cbbLanThanhLy";
            this.cbbLanThanhLy.Size = new System.Drawing.Size(79, 18);
            this.cbbLanThanhLy.TabIndex = 5;
            this.cbbLanThanhLy.Text = "Lần thanh lý";
            this.cbbLanThanhLy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            // 
            // chkTimChinhXac
            // 
            this.chkTimChinhXac.BackColor = System.Drawing.Color.Transparent;
            this.chkTimChinhXac.Checked = true;
            this.chkTimChinhXac.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTimChinhXac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTimChinhXac.Location = new System.Drawing.Point(495, 47);
            this.chkTimChinhXac.Name = "chkTimChinhXac";
            this.chkTimChinhXac.Size = new System.Drawing.Size(119, 18);
            this.chkTimChinhXac.TabIndex = 5;
            this.chkTimChinhXac.Text = "Tìm chính xác mã NPL";
            this.chkTimChinhXac.VisualStyleManager = this.vsmMain;
            this.chkTimChinhXac.CheckedChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // cbTrangThai
            // 
            this.cbTrangThai.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Tất cả";
            uiComboBoxItem1.Value = "A";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Chưa thanh khoản";
            uiComboBoxItem2.Value = "";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Thanh Khoản 1 phần";
            uiComboBoxItem3.Value = "L";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Thanh khoản hết";
            uiComboBoxItem4.Value = "H";
            this.cbTrangThai.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbTrangThai.Location = new System.Drawing.Point(305, 44);
            this.cbTrangThai.Name = "cbTrangThai";
            this.cbTrangThai.Size = new System.Drawing.Size(120, 21);
            this.cbTrangThai.TabIndex = 4;
            this.cbTrangThai.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbTrangThai.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnSearch.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearch.Location = new System.Drawing.Point(740, 44);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(84, 23);
            this.btnSearch.TabIndex = 8;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.WordWrap = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(233, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Tình trạng";
            // 
            // txtTenChuHang
            // 
            this.txtTenChuHang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTenChuHang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenChuHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenChuHang.Location = new System.Drawing.Point(98, 44);
            this.txtTenChuHang.Name = "txtTenChuHang";
            this.txtTenChuHang.Size = new System.Drawing.Size(114, 21);
            this.txtTenChuHang.TabIndex = 3;
            this.txtTenChuHang.Text = "*";
            this.txtTenChuHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenChuHang.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Tên chủ hàng";
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtMaNPL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNPL.Location = new System.Drawing.Point(495, 17);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(154, 21);
            this.txtMaNPL.TabIndex = 2;
            this.txtMaNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaNPL.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(442, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Mã NPL";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.DecimalDigits = 0;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.FormatString = "############";
            this.txtSoTiepNhan.Location = new System.Drawing.Point(98, 17);
            this.txtSoTiepNhan.MaxLength = 12;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(114, 21);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoTiepNhan.Value = ((ulong)(0ul));
            this.txtSoTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(218, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Năm đăng ký";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Số tờ khai";
            // 
            // txtLanThanhLy
            // 
            this.txtLanThanhLy.DecimalDigits = 0;
            this.txtLanThanhLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLanThanhLy.FormatString = "####";
            this.txtLanThanhLy.Location = new System.Drawing.Point(740, 18);
            this.txtLanThanhLy.MaxLength = 4;
            this.txtLanThanhLy.Name = "txtLanThanhLy";
            this.txtLanThanhLy.Size = new System.Drawing.Size(84, 21);
            this.txtLanThanhLy.TabIndex = 1;
            this.txtLanThanhLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtLanThanhLy.Value = ((short)(0));
            this.txtLanThanhLy.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16;
            this.txtLanThanhLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtLanThanhLy.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtNamTiepNhan
            // 
            this.txtNamTiepNhan.DecimalDigits = 0;
            this.txtNamTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamTiepNhan.FormatString = "####";
            this.txtNamTiepNhan.Location = new System.Drawing.Point(305, 17);
            this.txtNamTiepNhan.MaxLength = 4;
            this.txtNamTiepNhan.Name = "txtNamTiepNhan";
            this.txtNamTiepNhan.Size = new System.Drawing.Size(120, 21);
            this.txtNamTiepNhan.TabIndex = 1;
            this.txtNamTiepNhan.Text = "2008";
            this.txtNamTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtNamTiepNhan.Value = ((short)(2008));
            this.txtNamTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16;
            this.txtNamTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNamTiepNhan.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.dgListNPL);
            this.uiTabPage2.Controls.Add(this.uiGroupBox5);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1120, 588);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Quản lý theo nguyên phụ liệu";
            // 
            // dgListNPL
            // 
            this.dgListNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPL.AlternatingColors = true;
            this.dgListNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPL.ColumnAutoResize = true;
            dgListNPL_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgListNPL_DesignTimeLayout_Reference_0.Instance")));
            dgListNPL_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgListNPL_DesignTimeLayout_Reference_0});
            dgListNPL_DesignTimeLayout.LayoutString = resources.GetString("dgListNPL_DesignTimeLayout.LayoutString");
            this.dgListNPL.DesignTimeLayout = dgListNPL_DesignTimeLayout;
            this.dgListNPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPL.GroupByBoxVisible = false;
            this.dgListNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPL.Location = new System.Drawing.Point(0, 51);
            this.dgListNPL.Name = "dgListNPL";
            this.dgListNPL.RecordNavigator = true;
            this.dgListNPL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPL.Size = new System.Drawing.Size(1120, 537);
            this.dgListNPL.TabIndex = 194;
            this.dgListNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListNPL.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.btnExportExcelNPL);
            this.uiGroupBox5.Controls.Add(this.txtTenChuHang2);
            this.uiGroupBox5.Controls.Add(this.label1);
            this.uiGroupBox5.Controls.Add(this.btnTimKiemNPL);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1120, 51);
            this.uiGroupBox5.TabIndex = 193;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // btnExportExcelNPL
            // 
            this.btnExportExcelNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcelNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcelNPL.Image")));
            this.btnExportExcelNPL.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnExportExcelNPL.ImageList = this.imageList1;
            this.btnExportExcelNPL.Location = new System.Drawing.Point(424, 17);
            this.btnExportExcelNPL.Name = "btnExportExcelNPL";
            this.btnExportExcelNPL.Size = new System.Drawing.Size(91, 23);
            this.btnExportExcelNPL.TabIndex = 19;
            this.btnExportExcelNPL.Text = "Xuất Excel";
            this.btnExportExcelNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcelNPL.Click += new System.EventHandler(this.btnExportExcelNPL_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "page_excel.png");
            this.imageList1.Images.SetKeyName(1, "cog_go.png");
            // 
            // txtTenChuHang2
            // 
            this.txtTenChuHang2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTenChuHang2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenChuHang2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenChuHang2.Location = new System.Drawing.Point(99, 18);
            this.txtTenChuHang2.Name = "txtTenChuHang2";
            this.txtTenChuHang2.Size = new System.Drawing.Size(220, 21);
            this.txtTenChuHang2.TabIndex = 17;
            this.txtTenChuHang2.Text = "*";
            this.txtTenChuHang2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Tên chủ hàng";
            // 
            // btnTimKiemNPL
            // 
            this.btnTimKiemNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiemNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnTimKiemNPL.Image")));
            this.btnTimKiemNPL.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnTimKiemNPL.Location = new System.Drawing.Point(325, 17);
            this.btnTimKiemNPL.Name = "btnTimKiemNPL";
            this.btnTimKiemNPL.Size = new System.Drawing.Size(84, 23);
            this.btnTimKiemNPL.TabIndex = 18;
            this.btnTimKiemNPL.Text = "Tìm kiếm";
            this.btnTimKiemNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTimKiemNPL.WordWrap = false;
            this.btnTimKiemNPL.Click += new System.EventHandler(this.btnTimKiemNPL_Click);
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.dgListSP);
            this.uiTabPage3.Controls.Add(this.uiGroupBox1);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1120, 588);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Quản lý sản phẩm";
            // 
            // dgListSP
            // 
            this.dgListSP.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListSP_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgListSP_DesignTimeLayout_Reference_0.Instance")));
            dgListSP_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgListSP_DesignTimeLayout_Reference_0});
            dgListSP_DesignTimeLayout.LayoutString = resources.GetString("dgListSP_DesignTimeLayout.LayoutString");
            this.dgListSP.DesignTimeLayout = dgListSP_DesignTimeLayout;
            this.dgListSP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListSP.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListSP.FrozenColumns = 6;
            this.dgListSP.GroupByBoxVisible = false;
            this.dgListSP.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSP.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSP.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListSP.Location = new System.Drawing.Point(0, 82);
            this.dgListSP.Name = "dgListSP";
            this.dgListSP.RecordNavigator = true;
            this.dgListSP.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListSP.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListSP.ScrollBars = Janus.Windows.GridEX.ScrollBars.Both;
            this.dgListSP.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListSP.Size = new System.Drawing.Size(1120, 506);
            this.dgListSP.TabIndex = 4;
            this.dgListSP.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListSP.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnExportExcelSP);
            this.uiGroupBox1.Controls.Add(this.cbbStatus);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.txtChuHang);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.txtTenSP);
            this.uiGroupBox1.Controls.Add(this.txtMaSP);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.btnSearchSP);
            this.uiGroupBox1.Controls.Add(this.txtSoTK);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.txtLanTL);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.txtNamDK);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1120, 82);
            this.uiGroupBox1.TabIndex = 3;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnExportExcelSP
            // 
            this.btnExportExcelSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcelSP.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcelSP.Image")));
            this.btnExportExcelSP.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnExportExcelSP.ImageList = this.imageList1;
            this.btnExportExcelSP.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcelSP.Location = new System.Drawing.Point(784, 49);
            this.btnExportExcelSP.Name = "btnExportExcelSP";
            this.btnExportExcelSP.Size = new System.Drawing.Size(91, 23);
            this.btnExportExcelSP.TabIndex = 11;
            this.btnExportExcelSP.Text = "Xuất Excel";
            this.btnExportExcelSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcelSP.Click += new System.EventHandler(this.btnExportExcelSP_Click);
            // 
            // cbbStatus
            // 
            this.cbbStatus.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Tất cả";
            uiComboBoxItem5.Value = "A";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Chưa thanh khoản";
            uiComboBoxItem6.Value = "";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Thanh Khoản 1 phần";
            uiComboBoxItem7.Value = "L";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Thanh khoản hết";
            uiComboBoxItem8.Value = "H";
            this.cbbStatus.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8});
            this.cbbStatus.Location = new System.Drawing.Point(305, 44);
            this.cbbStatus.Name = "cbbStatus";
            this.cbbStatus.Size = new System.Drawing.Size(120, 21);
            this.cbbStatus.TabIndex = 4;
            this.cbbStatus.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbStatus.TextChanged += new System.EventHandler(this.btnSearchSP_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(233, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Tình trạng";
            // 
            // txtChuHang
            // 
            this.txtChuHang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtChuHang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtChuHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChuHang.Location = new System.Drawing.Point(98, 44);
            this.txtChuHang.Name = "txtChuHang";
            this.txtChuHang.Size = new System.Drawing.Size(114, 21);
            this.txtChuHang.TabIndex = 3;
            this.txtChuHang.Text = "*";
            this.txtChuHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtChuHang.TextChanged += new System.EventHandler(this.btnSearchSP_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 49);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Tên chủ hàng";
            // 
            // txtTenSP
            // 
            this.txtTenSP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTenSP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenSP.Location = new System.Drawing.Point(531, 49);
            this.txtTenSP.Name = "txtTenSP";
            this.txtTenSP.Size = new System.Drawing.Size(154, 21);
            this.txtTenSP.TabIndex = 2;
            this.txtTenSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenSP.TextChanged += new System.EventHandler(this.btnSearchSP_Click);
            // 
            // txtMaSP
            // 
            this.txtMaSP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtMaSP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSP.Location = new System.Drawing.Point(531, 17);
            this.txtMaSP.Name = "txtMaSP";
            this.txtMaSP.Size = new System.Drawing.Size(154, 21);
            this.txtMaSP.TabIndex = 2;
            this.txtMaSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSP.TextChanged += new System.EventHandler(this.btnSearchSP_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(442, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Tên sản phẩm";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(691, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Lần thanh lý";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(442, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Mã sản phẩm";
            // 
            // btnSearchSP
            // 
            this.btnSearchSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchSP.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchSP.Image")));
            this.btnSearchSP.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnSearchSP.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearchSP.Location = new System.Drawing.Point(694, 49);
            this.btnSearchSP.Name = "btnSearchSP";
            this.btnSearchSP.Size = new System.Drawing.Size(84, 23);
            this.btnSearchSP.TabIndex = 8;
            this.btnSearchSP.Text = "Tìm kiếm";
            this.btnSearchSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchSP.WordWrap = false;
            this.btnSearchSP.Click += new System.EventHandler(this.btnSearchSP_Click);
            // 
            // txtSoTK
            // 
            this.txtSoTK.DecimalDigits = 0;
            this.txtSoTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTK.FormatString = "############";
            this.txtSoTK.Location = new System.Drawing.Point(98, 17);
            this.txtSoTK.MaxLength = 12;
            this.txtSoTK.Name = "txtSoTK";
            this.txtSoTK.Size = new System.Drawing.Size(114, 21);
            this.txtSoTK.TabIndex = 0;
            this.txtSoTK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoTK.Value = ((ulong)(0ul));
            this.txtSoTK.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTK.TextChanged += new System.EventHandler(this.btnSearchSP_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(218, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Năm đăng ký";
            // 
            // txtLanTL
            // 
            this.txtLanTL.DecimalDigits = 0;
            this.txtLanTL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLanTL.FormatString = "####";
            this.txtLanTL.Location = new System.Drawing.Point(784, 17);
            this.txtLanTL.MaxLength = 4;
            this.txtLanTL.Name = "txtLanTL";
            this.txtLanTL.Size = new System.Drawing.Size(91, 21);
            this.txtLanTL.TabIndex = 1;
            this.txtLanTL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtLanTL.Value = ((short)(0));
            this.txtLanTL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16;
            this.txtLanTL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtLanTL.TextChanged += new System.EventHandler(this.btnSearchSP_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(29, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Số tờ khai";
            // 
            // txtNamDK
            // 
            this.txtNamDK.DecimalDigits = 0;
            this.txtNamDK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamDK.FormatString = "####";
            this.txtNamDK.Location = new System.Drawing.Point(305, 17);
            this.txtNamDK.MaxLength = 4;
            this.txtNamDK.Name = "txtNamDK";
            this.txtNamDK.Size = new System.Drawing.Size(48, 21);
            this.txtNamDK.TabIndex = 1;
            this.txtNamDK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtNamDK.Value = ((short)(0));
            this.txtNamDK.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16;
            this.txtNamDK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNamDK.TextChanged += new System.EventHandler(this.btnSearchSP_Click);
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.dgListToTal);
            this.uiTabPage4.Controls.Add(this.uiGroupBox3);
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(1120, 588);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "Thống kê lượng tồn";
            // 
            // dgListToTal
            // 
            this.dgListToTal.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListToTal.AlternatingColors = true;
            this.dgListToTal.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListToTal.ColumnAutoResize = true;
            dgListToTal_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgListToTal_DesignTimeLayout_Reference_0.Instance")));
            dgListToTal_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgListToTal_DesignTimeLayout_Reference_0});
            dgListToTal_DesignTimeLayout.LayoutString = resources.GetString("dgListToTal_DesignTimeLayout.LayoutString");
            this.dgListToTal.DesignTimeLayout = dgListToTal_DesignTimeLayout;
            this.dgListToTal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListToTal.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListToTal.GroupByBoxVisible = false;
            this.dgListToTal.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListToTal.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListToTal.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListToTal.Location = new System.Drawing.Point(0, 86);
            this.dgListToTal.Name = "dgListToTal";
            this.dgListToTal.RecordNavigator = true;
            this.dgListToTal.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListToTal.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListToTal.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListToTal.Size = new System.Drawing.Size(1120, 502);
            this.dgListToTal.TabIndex = 195;
            this.dgListToTal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListToTal.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dtpTo);
            this.uiGroupBox3.Controls.Add(this.dtpFrom);
            this.uiGroupBox3.Controls.Add(this.btnExportExcel);
            this.uiGroupBox3.Controls.Add(this.label17);
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Controls.Add(this.btnTotalExport);
            this.uiGroupBox3.Controls.Add(this.btnProcess);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1120, 86);
            this.uiGroupBox3.TabIndex = 194;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dtpTo
            // 
            this.dtpTo.CustomFormat = "dd/MM/yyyy";
            this.dtpTo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(96, 49);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(200, 21);
            this.dtpTo.TabIndex = 20;
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpFrom.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(96, 20);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(200, 21);
            this.dtpFrom.TabIndex = 20;
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnExportExcel.ImageList = this.imageList1;
            this.btnExportExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcel.Location = new System.Drawing.Point(311, 50);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(91, 23);
            this.btnExportExcel.TabIndex = 19;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(11, 55);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 16;
            this.label17.Text = "Đến ngày : ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(11, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "Từ ngày :";
            // 
            // btnTotalExport
            // 
            this.btnTotalExport.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotalExport.Image = ((System.Drawing.Image)(resources.GetObject("btnTotalExport.Image")));
            this.btnTotalExport.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnTotalExport.ImageSize = new System.Drawing.Size(20, 20);
            this.btnTotalExport.Location = new System.Drawing.Point(408, 21);
            this.btnTotalExport.Name = "btnTotalExport";
            this.btnTotalExport.Size = new System.Drawing.Size(145, 23);
            this.btnTotalExport.TabIndex = 18;
            this.btnTotalExport.Text = "Thống kê lượng xuất";
            this.btnTotalExport.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTotalExport.WordWrap = false;
            this.btnTotalExport.Click += new System.EventHandler(this.btnTotalExport_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.Image = ((System.Drawing.Image)(resources.GetObject("btnProcess.Image")));
            this.btnProcess.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnProcess.ImageSize = new System.Drawing.Size(20, 20);
            this.btnProcess.Location = new System.Drawing.Point(311, 20);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(91, 23);
            this.btnProcess.TabIndex = 18;
            this.btnProcess.Text = "Thống kê";
            this.btnProcess.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnProcess.WordWrap = false;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // uiTabPage5
            // 
            this.uiTabPage5.Controls.Add(this.dgListDetail);
            this.uiTabPage5.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage5.Name = "uiTabPage5";
            this.uiTabPage5.Size = new System.Drawing.Size(1120, 588);
            this.uiTabPage5.TabStop = true;
            this.uiTabPage5.Text = "Chi tiết các tờ khai";
            // 
            // dgListDetail
            // 
            this.dgListDetail.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDetail.AlternatingColors = true;
            this.dgListDetail.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDetail.ColumnAutoResize = true;
            this.dgListDetail.ContextMenuStrip = this.contextMenuStrip1;
            dgListDetail_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgListDetail_DesignTimeLayout_Reference_0.Instance")));
            dgListDetail_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgListDetail_DesignTimeLayout_Reference_0});
            dgListDetail_DesignTimeLayout.LayoutString = resources.GetString("dgListDetail_DesignTimeLayout.LayoutString");
            this.dgListDetail.DesignTimeLayout = dgListDetail_DesignTimeLayout;
            this.dgListDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDetail.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDetail.GroupByBoxVisible = false;
            this.dgListDetail.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDetail.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDetail.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDetail.Location = new System.Drawing.Point(0, 0);
            this.dgListDetail.Name = "dgListDetail";
            this.dgListDetail.RecordNavigator = true;
            this.dgListDetail.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDetail.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDetail.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDetail.Size = new System.Drawing.Size(1120, 588);
            this.dgListDetail.TabIndex = 196;
            this.dgListDetail.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListDetail.VisualStyleManager = this.vsmMain;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(129, 26);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.toolStripMenuItem1.Text = "Xuất Excel";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // uiTabPage6
            // 
            this.uiTabPage6.Controls.Add(this.dgListTotalExport);
            this.uiTabPage6.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage6.Name = "uiTabPage6";
            this.uiTabPage6.Size = new System.Drawing.Size(1120, 588);
            this.uiTabPage6.TabStop = true;
            this.uiTabPage6.Text = "Thống kê tổng lượng xuất";
            // 
            // dgListTotalExport
            // 
            this.dgListTotalExport.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTotalExport.AlternatingColors = true;
            this.dgListTotalExport.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTotalExport.ColumnAutoResize = true;
            this.dgListTotalExport.ContextMenuStrip = this.mnuNPLXuat;
            dgListTotalExport_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgListTotalExport_DesignTimeLayout_Reference_0.Instance")));
            dgListTotalExport_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgListTotalExport_DesignTimeLayout_Reference_0});
            dgListTotalExport_DesignTimeLayout.LayoutString = resources.GetString("dgListTotalExport_DesignTimeLayout.LayoutString");
            this.dgListTotalExport.DesignTimeLayout = dgListTotalExport_DesignTimeLayout;
            this.dgListTotalExport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTotalExport.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTotalExport.GroupByBoxVisible = false;
            this.dgListTotalExport.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotalExport.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotalExport.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTotalExport.Location = new System.Drawing.Point(0, 0);
            this.dgListTotalExport.Name = "dgListTotalExport";
            this.dgListTotalExport.RecordNavigator = true;
            this.dgListTotalExport.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTotalExport.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTotalExport.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListTotalExport.Size = new System.Drawing.Size(1120, 588);
            this.dgListTotalExport.TabIndex = 196;
            this.dgListTotalExport.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListTotalExport.VisualStyleManager = this.vsmMain;
            // 
            // mnuNPLXuat
            // 
            this.mnuNPLXuat.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuTKXuatSP,
            this.mnuTKNhapNPL,
            this.mnuExportExcel});
            this.mnuNPLXuat.Name = "mnuNPLXuat";
            this.mnuNPLXuat.Size = new System.Drawing.Size(193, 70);
            // 
            // mnuTKXuatSP
            // 
            this.mnuTKXuatSP.Image = ((System.Drawing.Image)(resources.GetObject("mnuTKXuatSP.Image")));
            this.mnuTKXuatSP.Name = "mnuTKXuatSP";
            this.mnuTKXuatSP.Size = new System.Drawing.Size(192, 22);
            this.mnuTKXuatSP.Text = "Xem tờ khai xuất SP";
            this.mnuTKXuatSP.Click += new System.EventHandler(this.mnuTKXuatSP_Click);
            // 
            // mnuTKNhapNPL
            // 
            this.mnuTKNhapNPL.Image = ((System.Drawing.Image)(resources.GetObject("mnuTKNhapNPL.Image")));
            this.mnuTKNhapNPL.Name = "mnuTKNhapNPL";
            this.mnuTKNhapNPL.Size = new System.Drawing.Size(192, 22);
            this.mnuTKNhapNPL.Text = "Xem tờ khai nhập NPL";
            this.mnuTKNhapNPL.Click += new System.EventHandler(this.mnuTKNhapNPL_Click);
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("mnuExportExcel.Image")));
            this.mnuExportExcel.Name = "mnuExportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(192, 22);
            this.mnuExportExcel.Text = "Xuất Excel";
            this.mnuExportExcel.Click += new System.EventHandler(this.mnuExportExcel_Click);
            // 
            // uiTabPage7
            // 
            this.uiTabPage7.Controls.Add(this.gridListDetail);
            this.uiTabPage7.Controls.Add(this.uiGroupBox6);
            this.uiTabPage7.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage7.Name = "uiTabPage7";
            this.uiTabPage7.Size = new System.Drawing.Size(1120, 588);
            this.uiTabPage7.TabStop = true;
            this.uiTabPage7.Text = "Theo dõi chi tiết thanh khoản";
            // 
            // gridListDetail
            // 
            this.gridListDetail.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            gridListDetail_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("gridListDetail_DesignTimeLayout_Reference_0.Instance")));
            gridListDetail_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            gridListDetail_DesignTimeLayout_Reference_0});
            gridListDetail_DesignTimeLayout.LayoutString = resources.GetString("gridListDetail_DesignTimeLayout.LayoutString");
            this.gridListDetail.DesignTimeLayout = gridListDetail_DesignTimeLayout;
            this.gridListDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridListDetail.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridListDetail.FrozenColumns = 6;
            this.gridListDetail.GroupByBoxVisible = false;
            this.gridListDetail.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridListDetail.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridListDetail.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridListDetail.Location = new System.Drawing.Point(0, 49);
            this.gridListDetail.Name = "gridListDetail";
            this.gridListDetail.RecordNavigator = true;
            this.gridListDetail.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridListDetail.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridListDetail.ScrollBars = Janus.Windows.GridEX.ScrollBars.Both;
            this.gridListDetail.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.gridListDetail.Size = new System.Drawing.Size(1120, 539);
            this.gridListDetail.TabIndex = 6;
            this.gridListDetail.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridListDetail.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.gridListDetail.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.label18);
            this.uiGroupBox6.Controls.Add(this.label19);
            this.uiGroupBox6.Controls.Add(this.dateToDetail);
            this.uiGroupBox6.Controls.Add(this.dateFromDetail);
            this.uiGroupBox6.Controls.Add(this.btnExportExcelDetail);
            this.uiGroupBox6.Controls.Add(this.btnSearchDetail);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1120, 49);
            this.uiGroupBox6.TabIndex = 5;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(318, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(64, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "Đến ngày : ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(28, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 23;
            this.label19.Text = "Từ ngày :";
            // 
            // dateToDetail
            // 
            this.dateToDetail.CustomFormat = "dd/MM/yyyy";
            this.dateToDetail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateToDetail.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateToDetail.Location = new System.Drawing.Point(388, 18);
            this.dateToDetail.Name = "dateToDetail";
            this.dateToDetail.Size = new System.Drawing.Size(200, 21);
            this.dateToDetail.TabIndex = 22;
            // 
            // dateFromDetail
            // 
            this.dateFromDetail.CustomFormat = "dd/MM/yyyy";
            this.dateFromDetail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateFromDetail.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateFromDetail.Location = new System.Drawing.Point(88, 18);
            this.dateFromDetail.Name = "dateFromDetail";
            this.dateFromDetail.Size = new System.Drawing.Size(200, 21);
            this.dateFromDetail.TabIndex = 21;
            // 
            // btnExportExcelDetail
            // 
            this.btnExportExcelDetail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcelDetail.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcelDetail.Image")));
            this.btnExportExcelDetail.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnExportExcelDetail.ImageList = this.imageList1;
            this.btnExportExcelDetail.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcelDetail.Location = new System.Drawing.Point(695, 16);
            this.btnExportExcelDetail.Name = "btnExportExcelDetail";
            this.btnExportExcelDetail.Size = new System.Drawing.Size(91, 23);
            this.btnExportExcelDetail.TabIndex = 11;
            this.btnExportExcelDetail.Text = "Xuất Excel";
            this.btnExportExcelDetail.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcelDetail.Click += new System.EventHandler(this.btnExportExcelDetail_Click);
            // 
            // btnSearchDetail
            // 
            this.btnSearchDetail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchDetail.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchDetail.Image")));
            this.btnSearchDetail.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnSearchDetail.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearchDetail.Location = new System.Drawing.Point(605, 16);
            this.btnSearchDetail.Name = "btnSearchDetail";
            this.btnSearchDetail.Size = new System.Drawing.Size(84, 23);
            this.btnSearchDetail.TabIndex = 8;
            this.btnSearchDetail.Text = "Tìm kiếm";
            this.btnSearchDetail.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchDetail.WordWrap = false;
            this.btnSearchDetail.Click += new System.EventHandler(this.btnSearchDetail_Click);
            // 
            // backgroundHangTon
            // 
            this.backgroundHangTon.WorkerReportsProgress = true;
            this.backgroundHangTon.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundHangTon_DoWork);
            this.backgroundHangTon.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundHangTon_RunWorkerCompleted);
            this.backgroundHangTon.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundHangTon_ProgressChanged);
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdDieuChinhThue,
            this.cmdDieuChinhTon,
            this.cmdKiemTraNPL,
            this.cmdCapNhatTKSai,
            this.cmdSave,
            this.cmdExportExcel,
            this.cmdPrint,
            this.cmdClose,
            this.cmdCapNhatTK});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.imageList1;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.ShowShortcutInToolTips = true;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdDieuChinhThue1,
            this.cmdDieuChinhTon1,
            this.cmdKiemTraNPL1,
            this.cmdCapNhatTKSai1,
            this.cmdCapNhatTK1,
            this.cmdSave1,
            this.cmdExportExcel1,
            this.cmdPrint1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(946, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdDieuChinhThue1
            // 
            this.cmdDieuChinhThue1.Key = "cmdDieuChinhThue";
            this.cmdDieuChinhThue1.Name = "cmdDieuChinhThue1";
            // 
            // cmdDieuChinhTon1
            // 
            this.cmdDieuChinhTon1.Key = "cmdDieuChinhTon";
            this.cmdDieuChinhTon1.Name = "cmdDieuChinhTon1";
            // 
            // cmdKiemTraNPL1
            // 
            this.cmdKiemTraNPL1.Key = "cmdKiemTraNPL";
            this.cmdKiemTraNPL1.Name = "cmdKiemTraNPL1";
            // 
            // cmdCapNhatTKSai1
            // 
            this.cmdCapNhatTKSai1.Key = "cmdCapNhatTKSai";
            this.cmdCapNhatTKSai1.Name = "cmdCapNhatTKSai1";
            // 
            // cmdCapNhatTK1
            // 
            this.cmdCapNhatTK1.Key = "cmdCapNhatTK";
            this.cmdCapNhatTK1.Name = "cmdCapNhatTK1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdExportExcel1
            // 
            this.cmdExportExcel1.Key = "cmdExportExcel";
            this.cmdExportExcel1.Name = "cmdExportExcel1";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // cmdDieuChinhThue
            // 
            this.cmdDieuChinhThue.Image = ((System.Drawing.Image)(resources.GetObject("cmdDieuChinhThue.Image")));
            this.cmdDieuChinhThue.Key = "cmdDieuChinhThue";
            this.cmdDieuChinhThue.Name = "cmdDieuChinhThue";
            this.cmdDieuChinhThue.Text = "Điều chỉnh thuế";
            // 
            // cmdDieuChinhTon
            // 
            this.cmdDieuChinhTon.Image = ((System.Drawing.Image)(resources.GetObject("cmdDieuChinhTon.Image")));
            this.cmdDieuChinhTon.Key = "cmdDieuChinhTon";
            this.cmdDieuChinhTon.Name = "cmdDieuChinhTon";
            this.cmdDieuChinhTon.Text = "Điều chỉnh lượng tồn";
            // 
            // cmdKiemTraNPL
            // 
            this.cmdKiemTraNPL.Image = ((System.Drawing.Image)(resources.GetObject("cmdKiemTraNPL.Image")));
            this.cmdKiemTraNPL.Key = "cmdKiemTraNPL";
            this.cmdKiemTraNPL.Name = "cmdKiemTraNPL";
            this.cmdKiemTraNPL.Text = "Kiểm tra NPL của tờ khai";
            // 
            // cmdCapNhatTKSai
            // 
            this.cmdCapNhatTKSai.Image = ((System.Drawing.Image)(resources.GetObject("cmdCapNhatTKSai.Image")));
            this.cmdCapNhatTKSai.Key = "cmdCapNhatTKSai";
            this.cmdCapNhatTKSai.Name = "cmdCapNhatTKSai";
            this.cmdCapNhatTKSai.Text = "Cập nhật tờ khai sai số liệu";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu lại";
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportExcel.Image")));
            this.cmdExportExcel.Key = "cmdExportExcel";
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Text = "Xuất Excel";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint.Image")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Text = "In";
            // 
            // cmdClose
            // 
            this.cmdClose.Image = ((System.Drawing.Image)(resources.GetObject("cmdClose.Image")));
            this.cmdClose.Key = "cmdClose";
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Text = "Đóng";
            // 
            // cmdCapNhatTK
            // 
            this.cmdCapNhatTK.Image = ((System.Drawing.Image)(resources.GetObject("cmdCapNhatTK.Image")));
            this.cmdCapNhatTK.Key = "cmdCapNhatTK";
            this.cmdCapNhatTK.Name = "cmdCapNhatTK";
            this.cmdCapNhatTK.Text = "Cập nhật tờ khai";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1122, 32);
            // 
            // HangTon_OLD
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1122, 642);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HangTon_OLD";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Theo dõi nguyên phụ liệu tồn - sản phẩm";
            this.Load += new System.EventHandler(this.HangTon_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.mnuGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListToTal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.uiTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDetail)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.uiTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotalExport)).EndInit();
            this.mnuNPLXuat.ResumeLayout(false);
            this.uiTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridListDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintDocument1;
        private System.Windows.Forms.ContextMenuStrip mnuGrid;
        private System.Windows.Forms.ToolStripMenuItem toolStripThanhkhoan;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.EditControls.UICheckBox chkTuDongTinhThue;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UICheckBox chkTimChinhXac;
        private Janus.Windows.EditControls.UIComboBox cbTrangThai;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenChuHang;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamTiepNhan;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenChuHang2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnTimKiemNPL;
        private Janus.Windows.EditControls.UICheckBox chkHienThiLech;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStripMenuItem toolStripChuyenTieuThuNoiDia;
        private System.ComponentModel.BackgroundWorker backgroundHangTon;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIComboBox cbbStatus;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtChuHang;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSP;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIButton btnSearchSP;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTK;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamDK;
        private Janus.Windows.EditControls.UICheckBox chkHienThiTKKhongDuyet;
        private Janus.Windows.EditControls.UICheckBox cbbLanThanhLy;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLanThanhLy;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenSP;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLanTL;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.EditControls.UIButton btnExportExcelNPL;
        private Janus.Windows.EditControls.UIButton btnExportExcelSP;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.EditControls.UIButton btnProcess;
        private Janus.Windows.GridEX.GridEX dgListToTal;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage5;
        private Janus.Windows.GridEX.GridEX dgListDetail;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private Janus.Windows.GridEX.GridEX dgListNPL;
        private Janus.Windows.GridEX.GridEX dgListSP;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDieuChinhThue1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDieuChinhThue;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDieuChinhTon1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKiemTraNPL1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCapNhatTKSai1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDieuChinhTon;
        private Janus.Windows.UI.CommandBars.UICommand cmdKiemTraNPL;
        private Janus.Windows.UI.CommandBars.UICommand cmdCapNhatTKSai;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private Janus.Windows.UI.CommandBars.UICommand cmdClose;
        private Janus.Windows.UI.CommandBars.UICommand cmdCapNhatTK1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCapNhatTK;
        private Janus.Windows.EditControls.UIButton btnTotalExport;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage6;
        private Janus.Windows.GridEX.GridEX dgListTotalExport;
        private System.Windows.Forms.ContextMenuStrip mnuNPLXuat;
        private System.Windows.Forms.ToolStripMenuItem mnuTKXuatSP;
        private System.Windows.Forms.ToolStripMenuItem mnuTKNhapNPL;
        private System.Windows.Forms.ToolStripMenuItem mnuExportExcel;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage7;
        private Janus.Windows.GridEX.GridEX gridListDetail;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnExportExcelDetail;
        private Janus.Windows.EditControls.UIButton btnSearchDetail;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DateTimePicker dateToDetail;
        private System.Windows.Forms.DateTimePicker dateFromDetail;

    }
}