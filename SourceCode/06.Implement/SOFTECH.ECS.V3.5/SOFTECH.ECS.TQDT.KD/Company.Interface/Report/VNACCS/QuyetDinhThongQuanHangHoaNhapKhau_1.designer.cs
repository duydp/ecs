namespace Company.Interface.Report.VNACCS
{
    partial class QuyetDinhThongQuanHangHoaNhapKhau_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuyetDinhThongQuanHangHoaNhapKhau_1));
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lblMaVanBanPhapQuyKhac5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNguoiUyThacNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaLyDoDeNghi = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongSoDongHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel211 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel210 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.lblPhanLoaiNopThue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel208 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongSoTrangToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiNopThue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel205 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaXacDinhThoiHanNopThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTyGiaTinhThue3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel202 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDongTienTyGiaTinhThue3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTyGiaTinhThue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel199 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDongTienTyGiaTinhThue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTyGiaTinhThue1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel196 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDongTienTyGiaTinhThue1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel194 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTienBaoLanh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel192 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTienThuePhaiNop = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel190 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel189 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel188 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel187 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel186 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDongTong6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel184 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTienThue6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThue6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThue6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel180 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDongTong5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel178 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTienThue5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThue5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThue5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel174 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDongTong4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel172 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTienThue4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThue4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThue4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel168 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDongTong3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel166 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTienThue3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThue3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThue3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel162 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDongTong2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel160 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTienThue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel156 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDongTong1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel154 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTienThue1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenSacThue1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSacThue1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel153 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDongTienDieuChinhTriGia5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDongTienDieuChinhTriGia4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel150 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel149 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDongTienDieuChinhTriGia3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel147 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDongTienDieuChinhTriGia2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiDieuChinh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel146 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel145 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel144 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel143 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiTietKhaiTriGia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel141 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaKhoanDieuChinh5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel138 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel137 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel136 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaTenKhoanDieuChinh5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiDieuChinh5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaKhoanDieuChinh4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel131 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiDieuChinh4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel129 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaTenKhoanDieuChinh4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel127 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaKhoanDieuChinh3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel124 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiDieuChinh3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel122 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaTenKhoanDieuChinh3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaKhoanDieuChinh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel117 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiDieuChinh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaTenKhoanDieuChinh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaKhoanDieuChinh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel104 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDongTienDieuChinhTriGia1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel101 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaTenKhoanDieuChinh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel98 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel97 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaTienTe = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel116 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel115 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel114 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhiBaoHiem = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel112 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhiVanChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaTienTeCuaTienBaoHiem = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel109 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiBaoHiem = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaTienTePhiVanChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel106 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiPhiVanChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGiaCoSo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDangKyBaoHiemTongHop = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhuongPhapDieuChinhTriGia = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiDieuChinhTriGia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel99 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiCongThucChuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTiepNhanKhaiTriGiaTongHop = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiKhaiTriGia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoGiayPhep5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel90 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiGiayPhepNhapKhau5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel88 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoGiayPhep3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiGiayPhepNhapKhau3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoGiayPhep4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiGiayPhepNhapKhau4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoGiayPhep2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiGiayPhepNhapKhau2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoGiayPhep1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiGiayPhepNhapKhau1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.lblMaKetQuaKiemTraNoiDung = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiNhapLieu = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongHeSoPhanBoTriGia = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTriGiaTinhThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTriGiaHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDongTienHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDieuKienGiaHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiGiaHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhuongThucThanhToan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayPhatHanh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTiepNhanHoaDonDienTu = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiHinhThucHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.lblMaVanBanPhapQuyKhac4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaVanBanPhapQuyKhac3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaVanBanPhapQuyKhac2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaVanBanPhapQuyKhac1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenPhuongTienVanChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhuongTienVanChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDuocPhepNhapKho = new DevExpress.XtraReports.UI.XRLabel();
            this.lblKyHieuSoHieu = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHangDen = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDiaDiemXepHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDiaDiemDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDiaDiemXepHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDiaDiemDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDiaDiemLuuKhoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDiaDiemLuuKhoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoLuongContainer = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinhTrongLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTrongLuongHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoVanDon5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoVanDon4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoVanDon3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoVanDon2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoVanDon1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.lblMaNhanVienHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDaiLyHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDaiLyHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.lblNguoiUyThacXuatKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaChiNguoiXuatKhau4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaChiNguoiXuatKhau2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaChiNguoiXuatKhau3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaChiNguoiXuatKhau1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNguoiXuatKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaBuuChinhXuatKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNguoiXuatKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNuoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lblMaNguoiUyThacNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDienThoaiNguoiNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaChiNguoiNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaBuuChinhNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNguoiNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNguoiNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.txtBarCode = new DevExpress.XtraReports.UI.XRBarCode();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBieuThiTHHetHan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiCaNhanToChuc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaHieuPhuongThucVanChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiHangHoa = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThoiHanTaiNhapTaiXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaBoPhanXuLyToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSoHangHoaDaiDienToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongSoToKhaiChiaNho = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoNhanhToKhaiChiaNho = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioThayDoiDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayThayDoiDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl222 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaLoaiHinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenCoQuanHaiQuanTiepNhanToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiKiemTra = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhaiDauTien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhaiTamNhapTaiXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMaVanBanPhapQuyKhac5,
            this.lblTenNguoiUyThacNhapKhau,
            this.lblMaLyDoDeNghi,
            this.lblTongSoDongHang,
            this.xrLabel211,
            this.xrLabel210,
            this.xrLine10,
            this.lblPhanLoaiNopThue,
            this.xrLabel208,
            this.lblTongSoTrangToKhai,
            this.lblNguoiNopThue,
            this.xrLabel205,
            this.lblMaXacDinhThoiHanNopThue,
            this.lblTyGiaTinhThue3,
            this.xrLabel202,
            this.lblMaDongTienTyGiaTinhThue3,
            this.lblTyGiaTinhThue2,
            this.xrLabel199,
            this.lblMaDongTienTyGiaTinhThue2,
            this.lblTyGiaTinhThue1,
            this.xrLabel196,
            this.lblMaDongTienTyGiaTinhThue1,
            this.xrLabel194,
            this.lblSoTienBaoLanh,
            this.xrLabel192,
            this.lblTongTienThuePhaiNop,
            this.xrLabel190,
            this.xrLabel189,
            this.xrLabel188,
            this.xrLabel187,
            this.xrLabel186,
            this.lblSoDongTong6,
            this.xrLabel184,
            this.lblTongTienThue6,
            this.lblTenSacThue6,
            this.lblMaSacThue6,
            this.xrLabel180,
            this.lblSoDongTong5,
            this.xrLabel178,
            this.lblTongTienThue5,
            this.lblTenSacThue5,
            this.lblMaSacThue5,
            this.xrLabel174,
            this.lblSoDongTong4,
            this.xrLabel172,
            this.lblTongTienThue4,
            this.lblTenSacThue4,
            this.lblMaSacThue4,
            this.xrLabel168,
            this.lblSoDongTong3,
            this.xrLabel166,
            this.lblTongTienThue3,
            this.lblTenSacThue3,
            this.lblMaSacThue3,
            this.xrLabel162,
            this.lblSoDongTong2,
            this.xrLabel160,
            this.lblTongTienThue2,
            this.lblTenSacThue2,
            this.lblMaSacThue2,
            this.xrLabel156,
            this.lblSoDongTong1,
            this.xrLabel154,
            this.lblTongTienThue1,
            this.lblTenSacThue1,
            this.lblMaSacThue1,
            this.xrLabel153,
            this.lblMaDongTienDieuChinhTriGia5,
            this.lblMaDongTienDieuChinhTriGia4,
            this.xrLabel150,
            this.xrLabel149,
            this.lblMaDongTienDieuChinhTriGia3,
            this.xrLabel147,
            this.lblMaDongTienDieuChinhTriGia2,
            this.xrLabel102,
            this.lblMaPhanLoaiDieuChinh1,
            this.xrLabel146,
            this.xrLabel145,
            this.xrLabel144,
            this.xrLabel143,
            this.lblChiTietKhaiTriGia,
            this.xrLabel141,
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5,
            this.lblTriGiaKhoanDieuChinh5,
            this.xrLabel138,
            this.xrLabel137,
            this.xrLabel136,
            this.lblMaTenKhoanDieuChinh5,
            this.lblMaPhanLoaiDieuChinh5,
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4,
            this.lblTriGiaKhoanDieuChinh4,
            this.xrLabel131,
            this.lblMaPhanLoaiDieuChinh4,
            this.xrLabel129,
            this.lblMaTenKhoanDieuChinh4,
            this.xrLabel127,
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3,
            this.lblTriGiaKhoanDieuChinh3,
            this.xrLabel124,
            this.lblMaPhanLoaiDieuChinh3,
            this.xrLabel122,
            this.lblMaTenKhoanDieuChinh3,
            this.xrLabel120,
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2,
            this.lblTriGiaKhoanDieuChinh2,
            this.xrLabel117,
            this.lblMaPhanLoaiDieuChinh2,
            this.xrLabel111,
            this.lblMaTenKhoanDieuChinh2,
            this.xrLabel108,
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1,
            this.lblTriGiaKhoanDieuChinh1,
            this.xrLabel104,
            this.lblMaDongTienDieuChinhTriGia1,
            this.xrLabel101,
            this.lblMaTenKhoanDieuChinh1,
            this.xrLabel98,
            this.xrLabel97,
            this.xrLabel96,
            this.xrLabel74,
            this.xrLabel70,
            this.lblMaTienTe,
            this.xrLabel116,
            this.xrLabel115,
            this.xrLabel114,
            this.lblPhiBaoHiem,
            this.xrLabel112,
            this.lblPhiVanChuyen,
            this.lblMaTienTeCuaTienBaoHiem,
            this.xrLabel109,
            this.lblMaPhanLoaiBaoHiem,
            this.lblMaTienTePhiVanChuyen,
            this.xrLabel106,
            this.lblMaPhanLoaiPhiVanChuyen,
            this.lblGiaCoSo,
            this.xrLabel103,
            this.lblSoDangKyBaoHiemTongHop,
            this.lblPhuongPhapDieuChinhTriGia,
            this.lblMaPhanLoaiDieuChinhTriGia,
            this.xrLabel99,
            this.lblPhanLoaiCongThucChuan,
            this.lblSoTiepNhanKhaiTriGiaTongHop,
            this.lblMaPhanLoaiKhaiTriGia,
            this.xrLabel95,
            this.xrLabel94,
            this.xrLabel93,
            this.xrLabel92,
            this.lblSoGiayPhep5,
            this.xrLabel90,
            this.lblPhanLoaiGiayPhepNhapKhau5,
            this.xrLabel88,
            this.lblSoGiayPhep3,
            this.xrLabel86,
            this.lblPhanLoaiGiayPhepNhapKhau3,
            this.xrLabel84,
            this.lblSoGiayPhep4,
            this.xrLabel82,
            this.lblPhanLoaiGiayPhepNhapKhau4,
            this.xrLabel80,
            this.lblSoGiayPhep2,
            this.xrLabel78,
            this.lblPhanLoaiGiayPhepNhapKhau2,
            this.xrLabel75,
            this.lblSoGiayPhep1,
            this.xrLabel73,
            this.lblPhanLoaiGiayPhepNhapKhau1,
            this.xrLabel69,
            this.xrLabel62,
            this.xrLine9,
            this.xrLine8,
            this.xrLine7,
            this.lblMaKetQuaKiemTraNoiDung,
            this.xrLabel77,
            this.lblMaPhanLoaiNhapLieu,
            this.lblTongHeSoPhanBoTriGia,
            this.lblTongTriGiaTinhThue,
            this.lblTongTriGiaHoaDon,
            this.xrLabel72,
            this.xrLabel71,
            this.lblMaDongTienHoaDon,
            this.lblMaDieuKienGiaHoaDon,
            this.xrLabel68,
            this.lblMaPhanLoaiGiaHoaDon,
            this.lblPhuongThucThanhToan,
            this.lblNgayPhatHanh,
            this.lblSoTiepNhanHoaDonDienTu,
            this.lblSoHoaDon,
            this.xrLabel67,
            this.lblPhanLoaiHinhThucHoaDon,
            this.xrLabel61,
            this.xrLabel60,
            this.xrLabel59,
            this.xrLabel58,
            this.xrLabel57,
            this.xrLabel52,
            this.xrLabel51,
            this.xrLabel40,
            this.xrLine6,
            this.lblMaVanBanPhapQuyKhac4,
            this.lblMaVanBanPhapQuyKhac3,
            this.lblMaVanBanPhapQuyKhac2,
            this.lblMaVanBanPhapQuyKhac1,
            this.lblTenPhuongTienVanChuyen,
            this.lblMaPhuongTienVanChuyen,
            this.lblNgayDuocPhepNhapKho,
            this.lblKyHieuSoHieu,
            this.lblNgayHangDen,
            this.xrLabel66,
            this.xrLabel65,
            this.xrLabel64,
            this.xrLabel63,
            this.lblTenDiaDiemXepHang,
            this.lblTenDiaDiemDoHang,
            this.lblMaDiaDiemXepHang,
            this.lblMaDiaDiemDoHang,
            this.lblTenDiaDiemLuuKhoHang,
            this.lblMaDiaDiemLuuKhoHang,
            this.xrLabel56,
            this.xrLabel55,
            this.xrLabel54,
            this.xrLabel53,
            this.xrLine5,
            this.lblSoLuongContainer,
            this.lblMaDonViTinhTrongLuong,
            this.lblTongTrongLuongHang,
            this.lblMaDonViTinh,
            this.lblSoLuong,
            this.xrLabel47,
            this.xrLabel46,
            this.xrLabel45,
            this.lblSoVanDon5,
            this.lblSoVanDon4,
            this.lblSoVanDon3,
            this.lblSoVanDon2,
            this.lblSoVanDon1,
            this.xrLabel39,
            this.xrLabel38,
            this.xrLabel37,
            this.xrLabel36,
            this.xrLabel34,
            this.xrLabel33,
            this.xrLine4,
            this.lblMaNhanVienHaiQuan,
            this.xrLabel35,
            this.lblTenDaiLyHaiQuan,
            this.lblMaDaiLyHaiQuan,
            this.xrLabel28,
            this.xrLabel30,
            this.xrLine3,
            this.lblNguoiUyThacXuatKhau,
            this.lblDiaChiNguoiXuatKhau4,
            this.lblDiaChiNguoiXuatKhau2,
            this.lblDiaChiNguoiXuatKhau3,
            this.lblDiaChiNguoiXuatKhau1,
            this.lblTenNguoiXuatKhau,
            this.lblMaBuuChinhXuatKhau,
            this.lblMaNguoiXuatKhau,
            this.lblMaNuoc,
            this.xrLabel25,
            this.xrLabel26,
            this.xrLabel22,
            this.xrLabel23,
            this.xrLabel29,
            this.xrLabel27,
            this.xrLine2,
            this.lblMaNguoiUyThacNhapKhau,
            this.lblSoDienThoaiNguoiNhapKhau,
            this.lblDiaChiNguoiNhapKhau,
            this.lblMaBuuChinhNhapKhau,
            this.lblTenNguoiNhapKhau,
            this.lblMaNguoiNhapKhau,
            this.xrLabel19,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel15,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel8,
            this.xrLabel2});
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.Detail.HeightF = 667.4903F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblMaVanBanPhapQuyKhac5
            // 
            this.lblMaVanBanPhapQuyKhac5.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaVanBanPhapQuyKhac5.LocationFloat = new DevExpress.Utils.PointFloat(642.4976F, 314.5F);
            this.lblMaVanBanPhapQuyKhac5.Name = "lblMaVanBanPhapQuyKhac5";
            this.lblMaVanBanPhapQuyKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaVanBanPhapQuyKhac5.SizeF = new System.Drawing.SizeF(30.6F, 10F);
            this.lblMaVanBanPhapQuyKhac5.StylePriority.UseFont = false;
            this.lblMaVanBanPhapQuyKhac5.StylePriority.UseTextAlignment = false;
            this.lblMaVanBanPhapQuyKhac5.Text = "XE";
            this.lblMaVanBanPhapQuyKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenNguoiUyThacNhapKhau
            // 
            this.lblTenNguoiUyThacNhapKhau.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenNguoiUyThacNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(101.0888F, 102.6216F);
            this.lblTenNguoiUyThacNhapKhau.Multiline = true;
            this.lblTenNguoiUyThacNhapKhau.Name = "lblTenNguoiUyThacNhapKhau";
            this.lblTenNguoiUyThacNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNguoiUyThacNhapKhau.SizeF = new System.Drawing.SizeF(624.8703F, 23F);
            this.lblTenNguoiUyThacNhapKhau.StylePriority.UseFont = false;
            this.lblTenNguoiUyThacNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblTenNguoiUyThacNhapKhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenNguoiUyThacNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaLyDoDeNghi
            // 
            this.lblMaLyDoDeNghi.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaLyDoDeNghi.LocationFloat = new DevExpress.Utils.PointFloat(541.8064F, 646.4066F);
            this.lblMaLyDoDeNghi.Name = "lblMaLyDoDeNghi";
            this.lblMaLyDoDeNghi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaLyDoDeNghi.SizeF = new System.Drawing.SizeF(28.15F, 10F);
            this.lblMaLyDoDeNghi.StylePriority.UseFont = false;
            this.lblMaLyDoDeNghi.StylePriority.UseTextAlignment = false;
            this.lblMaLyDoDeNghi.Text = "X";
            this.lblMaLyDoDeNghi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongSoDongHang
            // 
            this.lblTongSoDongHang.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTongSoDongHang.LocationFloat = new DevExpress.Utils.PointFloat(686.0313F, 657.4903F);
            this.lblTongSoDongHang.Name = "lblTongSoDongHang";
            this.lblTongSoDongHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoDongHang.SizeF = new System.Drawing.SizeF(28.15F, 10F);
            this.lblTongSoDongHang.StylePriority.UseFont = false;
            this.lblTongSoDongHang.StylePriority.UseTextAlignment = false;
            this.lblTongSoDongHang.Text = "NE";
            this.lblTongSoDongHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTongSoDongHang.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel211
            // 
            this.xrLabel211.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel211.LocationFloat = new DevExpress.Utils.PointFloat(512.5892F, 657.4903F);
            this.xrLabel211.Name = "xrLabel211";
            this.xrLabel211.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel211.SizeF = new System.Drawing.SizeF(173.441F, 10F);
            this.xrLabel211.StylePriority.UseFont = false;
            this.xrLabel211.StylePriority.UseTextAlignment = false;
            this.xrLabel211.Text = "Tổng số dòng hàng của tờ khai";
            this.xrLabel211.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel210
            // 
            this.xrLabel210.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel210.LocationFloat = new DevExpress.Utils.PointFloat(342.5193F, 657.4903F);
            this.xrLabel210.Name = "xrLabel210";
            this.xrLabel210.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel210.SizeF = new System.Drawing.SizeF(137.26F, 10F);
            this.xrLabel210.StylePriority.UseFont = false;
            this.xrLabel210.StylePriority.UseTextAlignment = false;
            this.xrLabel210.Text = "Tổng số trang của tờ khai";
            this.xrLabel210.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine10
            // 
            this.xrLine10.LocationFloat = new DevExpress.Utils.PointFloat(0.8400043F, 656.4066F);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.SizeF = new System.Drawing.SizeF(767F, 2F);
            // 
            // lblPhanLoaiNopThue
            // 
            this.lblPhanLoaiNopThue.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblPhanLoaiNopThue.LocationFloat = new DevExpress.Utils.PointFloat(684.0787F, 646.4071F);
            this.lblPhanLoaiNopThue.Name = "lblPhanLoaiNopThue";
            this.lblPhanLoaiNopThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiNopThue.SizeF = new System.Drawing.SizeF(28.15F, 10F);
            this.lblPhanLoaiNopThue.StylePriority.UseFont = false;
            this.lblPhanLoaiNopThue.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiNopThue.Text = "X";
            this.lblPhanLoaiNopThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel208
            // 
            this.xrLabel208.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel208.LocationFloat = new DevExpress.Utils.PointFloat(578.7087F, 646.4071F);
            this.xrLabel208.Name = "xrLabel208";
            this.xrLabel208.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel208.SizeF = new System.Drawing.SizeF(105.37F, 10F);
            this.xrLabel208.StylePriority.UseFont = false;
            this.xrLabel208.StylePriority.UseTextAlignment = false;
            this.xrLabel208.Text = "Phân loại nộp thuế";
            this.xrLabel208.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongSoTrangToKhai
            // 
            this.lblTongSoTrangToKhai.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTongSoTrangToKhai.LocationFloat = new DevExpress.Utils.PointFloat(481.906F, 657.4903F);
            this.lblTongSoTrangToKhai.Name = "lblTongSoTrangToKhai";
            this.lblTongSoTrangToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoTrangToKhai.SizeF = new System.Drawing.SizeF(25.14731F, 10F);
            this.lblTongSoTrangToKhai.StylePriority.UseFont = false;
            this.lblTongSoTrangToKhai.StylePriority.UseTextAlignment = false;
            this.lblTongSoTrangToKhai.Text = "NE";
            this.lblTongSoTrangToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTongSoTrangToKhai.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblNguoiNopThue
            // 
            this.lblNguoiNopThue.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblNguoiNopThue.LocationFloat = new DevExpress.Utils.PointFloat(684.0787F, 636.4071F);
            this.lblNguoiNopThue.Name = "lblNguoiNopThue";
            this.lblNguoiNopThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoiNopThue.SizeF = new System.Drawing.SizeF(28.15F, 10F);
            this.lblNguoiNopThue.StylePriority.UseFont = false;
            this.lblNguoiNopThue.StylePriority.UseTextAlignment = false;
            this.lblNguoiNopThue.Text = "X";
            this.lblNguoiNopThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel205
            // 
            this.xrLabel205.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel205.LocationFloat = new DevExpress.Utils.PointFloat(578.7087F, 636.4071F);
            this.xrLabel205.Name = "xrLabel205";
            this.xrLabel205.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel205.SizeF = new System.Drawing.SizeF(105.37F, 10F);
            this.xrLabel205.StylePriority.UseFont = false;
            this.xrLabel205.StylePriority.UseTextAlignment = false;
            this.xrLabel205.Text = "Người nộp thuế";
            this.xrLabel205.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaXacDinhThoiHanNopThue
            // 
            this.lblMaXacDinhThoiHanNopThue.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaXacDinhThoiHanNopThue.LocationFloat = new DevExpress.Utils.PointFloat(542.4652F, 636.4071F);
            this.lblMaXacDinhThoiHanNopThue.Name = "lblMaXacDinhThoiHanNopThue";
            this.lblMaXacDinhThoiHanNopThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaXacDinhThoiHanNopThue.SizeF = new System.Drawing.SizeF(28.14825F, 10F);
            this.lblMaXacDinhThoiHanNopThue.StylePriority.UseFont = false;
            this.lblMaXacDinhThoiHanNopThue.StylePriority.UseTextAlignment = false;
            this.lblMaXacDinhThoiHanNopThue.Text = "X";
            this.lblMaXacDinhThoiHanNopThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTyGiaTinhThue3
            // 
            this.lblTyGiaTinhThue3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTyGiaTinhThue3.LocationFloat = new DevExpress.Utils.PointFloat(593.3661F, 626.4087F);
            this.lblTyGiaTinhThue3.Name = "lblTyGiaTinhThue3";
            this.lblTyGiaTinhThue3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTyGiaTinhThue3.SizeF = new System.Drawing.SizeF(88.22F, 10F);
            this.lblTyGiaTinhThue3.StylePriority.UseFont = false;
            this.lblTyGiaTinhThue3.StylePriority.UseTextAlignment = false;
            this.lblTyGiaTinhThue3.Text = "123.456.789";
            this.lblTyGiaTinhThue3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTyGiaTinhThue3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel202
            // 
            this.xrLabel202.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel202.LocationFloat = new DevExpress.Utils.PointFloat(577.2062F, 626.4082F);
            this.xrLabel202.Name = "xrLabel202";
            this.xrLabel202.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel202.SizeF = new System.Drawing.SizeF(15.79F, 10F);
            this.xrLabel202.StylePriority.UseFont = false;
            this.xrLabel202.StylePriority.UseTextAlignment = false;
            this.xrLabel202.Text = "-";
            this.xrLabel202.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaDongTienTyGiaTinhThue3
            // 
            this.lblMaDongTienTyGiaTinhThue3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDongTienTyGiaTinhThue3.LocationFloat = new DevExpress.Utils.PointFloat(512.5892F, 626.4078F);
            this.lblMaDongTienTyGiaTinhThue3.Name = "lblMaDongTienTyGiaTinhThue3";
            this.lblMaDongTienTyGiaTinhThue3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDongTienTyGiaTinhThue3.SizeF = new System.Drawing.SizeF(64.25F, 10F);
            this.lblMaDongTienTyGiaTinhThue3.StylePriority.UseFont = false;
            this.lblMaDongTienTyGiaTinhThue3.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienTyGiaTinhThue3.Text = "XXE";
            this.lblMaDongTienTyGiaTinhThue3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTyGiaTinhThue2
            // 
            this.lblTyGiaTinhThue2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTyGiaTinhThue2.LocationFloat = new DevExpress.Utils.PointFloat(593.3648F, 616.4081F);
            this.lblTyGiaTinhThue2.Name = "lblTyGiaTinhThue2";
            this.lblTyGiaTinhThue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTyGiaTinhThue2.SizeF = new System.Drawing.SizeF(87.85F, 10F);
            this.lblTyGiaTinhThue2.StylePriority.UseFont = false;
            this.lblTyGiaTinhThue2.StylePriority.UseTextAlignment = false;
            this.lblTyGiaTinhThue2.Text = "123.456.789";
            this.lblTyGiaTinhThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTyGiaTinhThue2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel199
            // 
            this.xrLabel199.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel199.LocationFloat = new DevExpress.Utils.PointFloat(577.2062F, 616.4079F);
            this.xrLabel199.Name = "xrLabel199";
            this.xrLabel199.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel199.SizeF = new System.Drawing.SizeF(15.79F, 10F);
            this.xrLabel199.StylePriority.UseFont = false;
            this.xrLabel199.StylePriority.UseTextAlignment = false;
            this.xrLabel199.Text = "-";
            this.xrLabel199.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaDongTienTyGiaTinhThue2
            // 
            this.lblMaDongTienTyGiaTinhThue2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDongTienTyGiaTinhThue2.LocationFloat = new DevExpress.Utils.PointFloat(512.5892F, 616.4077F);
            this.lblMaDongTienTyGiaTinhThue2.Name = "lblMaDongTienTyGiaTinhThue2";
            this.lblMaDongTienTyGiaTinhThue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDongTienTyGiaTinhThue2.SizeF = new System.Drawing.SizeF(64.25F, 10F);
            this.lblMaDongTienTyGiaTinhThue2.StylePriority.UseFont = false;
            this.lblMaDongTienTyGiaTinhThue2.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienTyGiaTinhThue2.Text = "XXE";
            this.lblMaDongTienTyGiaTinhThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTyGiaTinhThue1
            // 
            this.lblTyGiaTinhThue1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTyGiaTinhThue1.LocationFloat = new DevExpress.Utils.PointFloat(593.3648F, 606.4078F);
            this.lblTyGiaTinhThue1.Name = "lblTyGiaTinhThue1";
            this.lblTyGiaTinhThue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTyGiaTinhThue1.SizeF = new System.Drawing.SizeF(87.85F, 10F);
            this.lblTyGiaTinhThue1.StylePriority.UseFont = false;
            this.lblTyGiaTinhThue1.StylePriority.UseTextAlignment = false;
            this.lblTyGiaTinhThue1.Text = "123.456.789";
            this.lblTyGiaTinhThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTyGiaTinhThue1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel196
            // 
            this.xrLabel196.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel196.LocationFloat = new DevExpress.Utils.PointFloat(577.2062F, 606.4077F);
            this.xrLabel196.Name = "xrLabel196";
            this.xrLabel196.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel196.SizeF = new System.Drawing.SizeF(15.79F, 10F);
            this.xrLabel196.StylePriority.UseFont = false;
            this.xrLabel196.StylePriority.UseTextAlignment = false;
            this.xrLabel196.Text = "-";
            this.xrLabel196.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaDongTienTyGiaTinhThue1
            // 
            this.lblMaDongTienTyGiaTinhThue1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDongTienTyGiaTinhThue1.LocationFloat = new DevExpress.Utils.PointFloat(512.5892F, 606.4073F);
            this.lblMaDongTienTyGiaTinhThue1.Name = "lblMaDongTienTyGiaTinhThue1";
            this.lblMaDongTienTyGiaTinhThue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDongTienTyGiaTinhThue1.SizeF = new System.Drawing.SizeF(64.25F, 10F);
            this.lblMaDongTienTyGiaTinhThue1.StylePriority.UseFont = false;
            this.lblMaDongTienTyGiaTinhThue1.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienTyGiaTinhThue1.Text = "XXE";
            this.lblMaDongTienTyGiaTinhThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel194
            // 
            this.xrLabel194.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel194.LocationFloat = new DevExpress.Utils.PointFloat(601.1586F, 596.407F);
            this.xrLabel194.Name = "xrLabel194";
            this.xrLabel194.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel194.SizeF = new System.Drawing.SizeF(35F, 10F);
            this.xrLabel194.StylePriority.UseFont = false;
            this.xrLabel194.StylePriority.UseTextAlignment = false;
            this.xrLabel194.Text = "VND";
            this.xrLabel194.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoTienBaoLanh
            // 
            this.lblSoTienBaoLanh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienBaoLanh.LocationFloat = new DevExpress.Utils.PointFloat(512.5892F, 596.4067F);
            this.lblSoTienBaoLanh.Name = "lblSoTienBaoLanh";
            this.lblSoTienBaoLanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienBaoLanh.SizeF = new System.Drawing.SizeF(88.22F, 10F);
            this.lblSoTienBaoLanh.StylePriority.UseFont = false;
            this.lblSoTienBaoLanh.StylePriority.UseTextAlignment = false;
            this.lblSoTienBaoLanh.Text = "12.345.678.901";
            this.lblSoTienBaoLanh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoTienBaoLanh.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel192
            // 
            this.xrLabel192.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel192.LocationFloat = new DevExpress.Utils.PointFloat(601.1586F, 586.4069F);
            this.xrLabel192.Name = "xrLabel192";
            this.xrLabel192.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel192.SizeF = new System.Drawing.SizeF(35F, 10F);
            this.xrLabel192.StylePriority.UseFont = false;
            this.xrLabel192.StylePriority.UseTextAlignment = false;
            this.xrLabel192.Text = "VND";
            this.xrLabel192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTongTienThuePhaiNop
            // 
            this.lblTongTienThuePhaiNop.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTienThuePhaiNop.LocationFloat = new DevExpress.Utils.PointFloat(512.5892F, 586.4069F);
            this.lblTongTienThuePhaiNop.Name = "lblTongTienThuePhaiNop";
            this.lblTongTienThuePhaiNop.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThuePhaiNop.SizeF = new System.Drawing.SizeF(88.22F, 10F);
            this.lblTongTienThuePhaiNop.StylePriority.UseFont = false;
            this.lblTongTienThuePhaiNop.StylePriority.UseTextAlignment = false;
            this.lblTongTienThuePhaiNop.Text = "12.345.678.901";
            this.lblTongTienThuePhaiNop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThuePhaiNop.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel190
            // 
            this.xrLabel190.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel190.LocationFloat = new DevExpress.Utils.PointFloat(381.0734F, 646.4071F);
            this.xrLabel190.Name = "xrLabel190";
            this.xrLabel190.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel190.SizeF = new System.Drawing.SizeF(160.7332F, 10F);
            this.xrLabel190.StylePriority.UseFont = false;
            this.xrLabel190.StylePriority.UseTextAlignment = false;
            this.xrLabel190.Text = "Mã lý do đề nghị BP";
            this.xrLabel190.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel189
            // 
            this.xrLabel189.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel189.LocationFloat = new DevExpress.Utils.PointFloat(380.7253F, 636.4071F);
            this.xrLabel189.Name = "xrLabel189";
            this.xrLabel189.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel189.SizeF = new System.Drawing.SizeF(161.74F, 10F);
            this.xrLabel189.StylePriority.UseFont = false;
            this.xrLabel189.StylePriority.UseTextAlignment = false;
            this.xrLabel189.Text = "Mã xác định thời hạn nộp thuế";
            this.xrLabel189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel188
            // 
            this.xrLabel188.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel188.LocationFloat = new DevExpress.Utils.PointFloat(380.228F, 606.4073F);
            this.xrLabel188.Name = "xrLabel188";
            this.xrLabel188.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel188.SizeF = new System.Drawing.SizeF(132.36F, 10F);
            this.xrLabel188.StylePriority.UseFont = false;
            this.xrLabel188.StylePriority.UseTextAlignment = false;
            this.xrLabel188.Text = "Tỷ giá tính thuế";
            this.xrLabel188.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel187
            // 
            this.xrLabel187.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel187.LocationFloat = new DevExpress.Utils.PointFloat(380.228F, 596.4073F);
            this.xrLabel187.Name = "xrLabel187";
            this.xrLabel187.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel187.SizeF = new System.Drawing.SizeF(132.36F, 10F);
            this.xrLabel187.StylePriority.UseFont = false;
            this.xrLabel187.StylePriority.UseTextAlignment = false;
            this.xrLabel187.Text = "Số tiền bảo lãnh";
            this.xrLabel187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel186
            // 
            this.xrLabel186.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel186.LocationFloat = new DevExpress.Utils.PointFloat(380.228F, 586.4071F);
            this.xrLabel186.Name = "xrLabel186";
            this.xrLabel186.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel186.SizeF = new System.Drawing.SizeF(132.36F, 10F);
            this.xrLabel186.StylePriority.UseFont = false;
            this.xrLabel186.StylePriority.UseTextAlignment = false;
            this.xrLabel186.Text = "Tổng tiền thuế phải nộp";
            this.xrLabel186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoDongTong6
            // 
            this.lblSoDongTong6.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoDongTong6.LocationFloat = new DevExpress.Utils.PointFloat(273.3716F, 637.4076F);
            this.lblSoDongTong6.Name = "lblSoDongTong6";
            this.lblSoDongTong6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDongTong6.SizeF = new System.Drawing.SizeF(77.52F, 10F);
            this.lblSoDongTong6.StylePriority.UseFont = false;
            this.lblSoDongTong6.StylePriority.UseTextAlignment = false;
            this.lblSoDongTong6.Text = "NE";
            this.lblSoDongTong6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDongTong6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel184
            // 
            this.xrLabel184.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel184.LocationFloat = new DevExpress.Utils.PointFloat(237.8941F, 637.4076F);
            this.xrLabel184.Name = "xrLabel184";
            this.xrLabel184.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel184.SizeF = new System.Drawing.SizeF(35F, 10F);
            this.xrLabel184.StylePriority.UseFont = false;
            this.xrLabel184.StylePriority.UseTextAlignment = false;
            this.xrLabel184.Text = "VND";
            this.xrLabel184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTongTienThue6
            // 
            this.lblTongTienThue6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTienThue6.LocationFloat = new DevExpress.Utils.PointFloat(149.325F, 637.4074F);
            this.lblTongTienThue6.Name = "lblTongTienThue6";
            this.lblTongTienThue6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThue6.SizeF = new System.Drawing.SizeF(88.22F, 10F);
            this.lblTongTienThue6.StylePriority.UseFont = false;
            this.lblTongTienThue6.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue6.Text = "12.345.678.901";
            this.lblTongTienThue6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThue6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTenSacThue6
            // 
            this.lblTenSacThue6.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenSacThue6.LocationFloat = new DevExpress.Utils.PointFloat(50.21556F, 637.408F);
            this.lblTenSacThue6.Name = "lblTenSacThue6";
            this.lblTenSacThue6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThue6.SizeF = new System.Drawing.SizeF(99.1096F, 10F);
            this.lblTenSacThue6.StylePriority.UseFont = false;
            this.lblTenSacThue6.StylePriority.UseTextAlignment = false;
            this.lblTenSacThue6.Text = "WWWWWWWWE";
            this.lblTenSacThue6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaSacThue6
            // 
            this.lblMaSacThue6.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaSacThue6.LocationFloat = new DevExpress.Utils.PointFloat(15.47604F, 637.4076F);
            this.lblMaSacThue6.Name = "lblMaSacThue6";
            this.lblMaSacThue6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThue6.SizeF = new System.Drawing.SizeF(34.73951F, 10F);
            this.lblMaSacThue6.StylePriority.UseFont = false;
            this.lblMaSacThue6.StylePriority.UseTextAlignment = false;
            this.lblMaSacThue6.Text = "X";
            this.lblMaSacThue6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel180
            // 
            this.xrLabel180.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel180.LocationFloat = new DevExpress.Utils.PointFloat(0.4761855F, 637.4074F);
            this.xrLabel180.Name = "xrLabel180";
            this.xrLabel180.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel180.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel180.StylePriority.UseFont = false;
            this.xrLabel180.StylePriority.UseTextAlignment = false;
            this.xrLabel180.Text = "6";
            this.xrLabel180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoDongTong5
            // 
            this.lblSoDongTong5.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoDongTong5.LocationFloat = new DevExpress.Utils.PointFloat(273.3716F, 627.4075F);
            this.lblSoDongTong5.Name = "lblSoDongTong5";
            this.lblSoDongTong5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDongTong5.SizeF = new System.Drawing.SizeF(77.52F, 10F);
            this.lblSoDongTong5.StylePriority.UseFont = false;
            this.lblSoDongTong5.StylePriority.UseTextAlignment = false;
            this.lblSoDongTong5.Text = "NE";
            this.lblSoDongTong5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDongTong5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel178
            // 
            this.xrLabel178.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel178.LocationFloat = new DevExpress.Utils.PointFloat(237.894F, 627.4075F);
            this.xrLabel178.Name = "xrLabel178";
            this.xrLabel178.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel178.SizeF = new System.Drawing.SizeF(35F, 10F);
            this.xrLabel178.StylePriority.UseFont = false;
            this.xrLabel178.StylePriority.UseTextAlignment = false;
            this.xrLabel178.Text = "VND";
            this.xrLabel178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTongTienThue5
            // 
            this.lblTongTienThue5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTienThue5.LocationFloat = new DevExpress.Utils.PointFloat(149.325F, 627.4075F);
            this.lblTongTienThue5.Name = "lblTongTienThue5";
            this.lblTongTienThue5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThue5.SizeF = new System.Drawing.SizeF(88.22F, 10F);
            this.lblTongTienThue5.StylePriority.UseFont = false;
            this.lblTongTienThue5.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue5.Text = "12.345.678.901";
            this.lblTongTienThue5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThue5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTenSacThue5
            // 
            this.lblTenSacThue5.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenSacThue5.LocationFloat = new DevExpress.Utils.PointFloat(50.21556F, 627.4075F);
            this.lblTenSacThue5.Name = "lblTenSacThue5";
            this.lblTenSacThue5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThue5.SizeF = new System.Drawing.SizeF(99.10953F, 10F);
            this.lblTenSacThue5.StylePriority.UseFont = false;
            this.lblTenSacThue5.StylePriority.UseTextAlignment = false;
            this.lblTenSacThue5.Text = "WWWWWWWWE";
            this.lblTenSacThue5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaSacThue5
            // 
            this.lblMaSacThue5.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaSacThue5.LocationFloat = new DevExpress.Utils.PointFloat(15.47604F, 627.4075F);
            this.lblMaSacThue5.Name = "lblMaSacThue5";
            this.lblMaSacThue5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThue5.SizeF = new System.Drawing.SizeF(34.73951F, 10F);
            this.lblMaSacThue5.StylePriority.UseFont = false;
            this.lblMaSacThue5.StylePriority.UseTextAlignment = false;
            this.lblMaSacThue5.Text = "X";
            this.lblMaSacThue5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel174
            // 
            this.xrLabel174.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel174.LocationFloat = new DevExpress.Utils.PointFloat(0.4760742F, 627.4075F);
            this.xrLabel174.Name = "xrLabel174";
            this.xrLabel174.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel174.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel174.StylePriority.UseFont = false;
            this.xrLabel174.StylePriority.UseTextAlignment = false;
            this.xrLabel174.Text = "5";
            this.xrLabel174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoDongTong4
            // 
            this.lblSoDongTong4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoDongTong4.LocationFloat = new DevExpress.Utils.PointFloat(273.3716F, 617.4076F);
            this.lblSoDongTong4.Name = "lblSoDongTong4";
            this.lblSoDongTong4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDongTong4.SizeF = new System.Drawing.SizeF(77.52F, 10F);
            this.lblSoDongTong4.StylePriority.UseFont = false;
            this.lblSoDongTong4.StylePriority.UseTextAlignment = false;
            this.lblSoDongTong4.Text = "NE";
            this.lblSoDongTong4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDongTong4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel172
            // 
            this.xrLabel172.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel172.LocationFloat = new DevExpress.Utils.PointFloat(237.894F, 617.4076F);
            this.xrLabel172.Name = "xrLabel172";
            this.xrLabel172.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel172.SizeF = new System.Drawing.SizeF(35F, 10F);
            this.xrLabel172.StylePriority.UseFont = false;
            this.xrLabel172.StylePriority.UseTextAlignment = false;
            this.xrLabel172.Text = "VND";
            this.xrLabel172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTongTienThue4
            // 
            this.lblTongTienThue4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTienThue4.LocationFloat = new DevExpress.Utils.PointFloat(149.325F, 617.4072F);
            this.lblTongTienThue4.Name = "lblTongTienThue4";
            this.lblTongTienThue4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThue4.SizeF = new System.Drawing.SizeF(88.22F, 10F);
            this.lblTongTienThue4.StylePriority.UseFont = false;
            this.lblTongTienThue4.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue4.Text = "12.345.678.901";
            this.lblTongTienThue4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThue4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTenSacThue4
            // 
            this.lblTenSacThue4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenSacThue4.LocationFloat = new DevExpress.Utils.PointFloat(50.21556F, 617.4072F);
            this.lblTenSacThue4.Name = "lblTenSacThue4";
            this.lblTenSacThue4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThue4.SizeF = new System.Drawing.SizeF(99.10954F, 10F);
            this.lblTenSacThue4.StylePriority.UseFont = false;
            this.lblTenSacThue4.StylePriority.UseTextAlignment = false;
            this.lblTenSacThue4.Text = "WWWWWWWWE";
            this.lblTenSacThue4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaSacThue4
            // 
            this.lblMaSacThue4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaSacThue4.LocationFloat = new DevExpress.Utils.PointFloat(15.47604F, 617.4072F);
            this.lblMaSacThue4.Name = "lblMaSacThue4";
            this.lblMaSacThue4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThue4.SizeF = new System.Drawing.SizeF(34.73952F, 10F);
            this.lblMaSacThue4.StylePriority.UseFont = false;
            this.lblMaSacThue4.StylePriority.UseTextAlignment = false;
            this.lblMaSacThue4.Text = "X";
            this.lblMaSacThue4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel168
            // 
            this.xrLabel168.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel168.LocationFloat = new DevExpress.Utils.PointFloat(0.4760477F, 617.4072F);
            this.xrLabel168.Name = "xrLabel168";
            this.xrLabel168.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel168.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel168.StylePriority.UseFont = false;
            this.xrLabel168.StylePriority.UseTextAlignment = false;
            this.xrLabel168.Text = "4";
            this.xrLabel168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoDongTong3
            // 
            this.lblSoDongTong3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoDongTong3.LocationFloat = new DevExpress.Utils.PointFloat(273.3716F, 607.4077F);
            this.lblSoDongTong3.Name = "lblSoDongTong3";
            this.lblSoDongTong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDongTong3.SizeF = new System.Drawing.SizeF(77.52F, 10F);
            this.lblSoDongTong3.StylePriority.UseFont = false;
            this.lblSoDongTong3.StylePriority.UseTextAlignment = false;
            this.lblSoDongTong3.Text = "NE";
            this.lblSoDongTong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDongTong3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel166
            // 
            this.xrLabel166.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel166.LocationFloat = new DevExpress.Utils.PointFloat(237.8941F, 607.4077F);
            this.xrLabel166.Name = "xrLabel166";
            this.xrLabel166.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel166.SizeF = new System.Drawing.SizeF(35F, 10F);
            this.xrLabel166.StylePriority.UseFont = false;
            this.xrLabel166.StylePriority.UseTextAlignment = false;
            this.xrLabel166.Text = "VND";
            this.xrLabel166.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTongTienThue3
            // 
            this.lblTongTienThue3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTienThue3.LocationFloat = new DevExpress.Utils.PointFloat(149.325F, 607.4075F);
            this.lblTongTienThue3.Name = "lblTongTienThue3";
            this.lblTongTienThue3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThue3.SizeF = new System.Drawing.SizeF(88.22F, 10F);
            this.lblTongTienThue3.StylePriority.UseFont = false;
            this.lblTongTienThue3.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue3.Text = "12.345.678.901";
            this.lblTongTienThue3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThue3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTenSacThue3
            // 
            this.lblTenSacThue3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenSacThue3.LocationFloat = new DevExpress.Utils.PointFloat(50.21556F, 607.4075F);
            this.lblTenSacThue3.Name = "lblTenSacThue3";
            this.lblTenSacThue3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThue3.SizeF = new System.Drawing.SizeF(99.1096F, 10F);
            this.lblTenSacThue3.StylePriority.UseFont = false;
            this.lblTenSacThue3.StylePriority.UseTextAlignment = false;
            this.lblTenSacThue3.Text = "WWWWWWWWE";
            this.lblTenSacThue3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaSacThue3
            // 
            this.lblMaSacThue3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaSacThue3.LocationFloat = new DevExpress.Utils.PointFloat(15.47615F, 607.4075F);
            this.lblMaSacThue3.Name = "lblMaSacThue3";
            this.lblMaSacThue3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThue3.SizeF = new System.Drawing.SizeF(34.73941F, 10F);
            this.lblMaSacThue3.StylePriority.UseFont = false;
            this.lblMaSacThue3.StylePriority.UseTextAlignment = false;
            this.lblMaSacThue3.Text = "X";
            this.lblMaSacThue3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel162
            // 
            this.xrLabel162.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel162.LocationFloat = new DevExpress.Utils.PointFloat(0.4761855F, 607.4075F);
            this.xrLabel162.Name = "xrLabel162";
            this.xrLabel162.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel162.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel162.StylePriority.UseFont = false;
            this.xrLabel162.StylePriority.UseTextAlignment = false;
            this.xrLabel162.Text = "3";
            this.xrLabel162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoDongTong2
            // 
            this.lblSoDongTong2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoDongTong2.LocationFloat = new DevExpress.Utils.PointFloat(273.3719F, 597.4075F);
            this.lblSoDongTong2.Name = "lblSoDongTong2";
            this.lblSoDongTong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDongTong2.SizeF = new System.Drawing.SizeF(77.52F, 10F);
            this.lblSoDongTong2.StylePriority.UseFont = false;
            this.lblSoDongTong2.StylePriority.UseTextAlignment = false;
            this.lblSoDongTong2.Text = "NE";
            this.lblSoDongTong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDongTong2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel160
            // 
            this.xrLabel160.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel160.LocationFloat = new DevExpress.Utils.PointFloat(237.8941F, 597.4075F);
            this.xrLabel160.Name = "xrLabel160";
            this.xrLabel160.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel160.SizeF = new System.Drawing.SizeF(35F, 10F);
            this.xrLabel160.StylePriority.UseFont = false;
            this.xrLabel160.StylePriority.UseTextAlignment = false;
            this.xrLabel160.Text = "VND";
            this.xrLabel160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTongTienThue2
            // 
            this.lblTongTienThue2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTienThue2.LocationFloat = new DevExpress.Utils.PointFloat(149.3252F, 597.4073F);
            this.lblTongTienThue2.Name = "lblTongTienThue2";
            this.lblTongTienThue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThue2.SizeF = new System.Drawing.SizeF(88.22F, 10F);
            this.lblTongTienThue2.StylePriority.UseFont = false;
            this.lblTongTienThue2.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue2.Text = "12.345.678.901";
            this.lblTongTienThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThue2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTenSacThue2
            // 
            this.lblTenSacThue2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenSacThue2.LocationFloat = new DevExpress.Utils.PointFloat(50.21556F, 597.4073F);
            this.lblTenSacThue2.Name = "lblTenSacThue2";
            this.lblTenSacThue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThue2.SizeF = new System.Drawing.SizeF(99.10968F, 10F);
            this.lblTenSacThue2.StylePriority.UseFont = false;
            this.lblTenSacThue2.StylePriority.UseTextAlignment = false;
            this.lblTenSacThue2.Text = "WWWWWWWWE";
            this.lblTenSacThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaSacThue2
            // 
            this.lblMaSacThue2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaSacThue2.LocationFloat = new DevExpress.Utils.PointFloat(15.47619F, 597.4073F);
            this.lblMaSacThue2.Name = "lblMaSacThue2";
            this.lblMaSacThue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThue2.SizeF = new System.Drawing.SizeF(34.73936F, 10F);
            this.lblMaSacThue2.StylePriority.UseFont = false;
            this.lblMaSacThue2.StylePriority.UseTextAlignment = false;
            this.lblMaSacThue2.Text = "X";
            this.lblMaSacThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel156
            // 
            this.xrLabel156.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel156.LocationFloat = new DevExpress.Utils.PointFloat(0.4761855F, 597.4073F);
            this.xrLabel156.Name = "xrLabel156";
            this.xrLabel156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel156.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel156.StylePriority.UseFont = false;
            this.xrLabel156.StylePriority.UseTextAlignment = false;
            this.xrLabel156.Text = "2";
            this.xrLabel156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoDongTong1
            // 
            this.lblSoDongTong1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoDongTong1.LocationFloat = new DevExpress.Utils.PointFloat(273.3716F, 587.4076F);
            this.lblSoDongTong1.Name = "lblSoDongTong1";
            this.lblSoDongTong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDongTong1.SizeF = new System.Drawing.SizeF(77.52F, 10F);
            this.lblSoDongTong1.StylePriority.UseFont = false;
            this.lblSoDongTong1.StylePriority.UseTextAlignment = false;
            this.lblSoDongTong1.Text = "NE";
            this.lblSoDongTong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDongTong1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel154
            // 
            this.xrLabel154.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel154.LocationFloat = new DevExpress.Utils.PointFloat(237.8941F, 587.4076F);
            this.xrLabel154.Name = "xrLabel154";
            this.xrLabel154.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel154.SizeF = new System.Drawing.SizeF(35F, 10F);
            this.xrLabel154.StylePriority.UseFont = false;
            this.xrLabel154.StylePriority.UseTextAlignment = false;
            this.xrLabel154.Text = "VND";
            this.xrLabel154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTongTienThue1
            // 
            this.lblTongTienThue1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTienThue1.LocationFloat = new DevExpress.Utils.PointFloat(149.325F, 587.4073F);
            this.lblTongTienThue1.Name = "lblTongTienThue1";
            this.lblTongTienThue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThue1.SizeF = new System.Drawing.SizeF(88.22F, 10F);
            this.lblTongTienThue1.StylePriority.UseFont = false;
            this.lblTongTienThue1.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue1.Text = "12.345.678.901";
            this.lblTongTienThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThue1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTenSacThue1
            // 
            this.lblTenSacThue1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenSacThue1.LocationFloat = new DevExpress.Utils.PointFloat(50.21556F, 587.4073F);
            this.lblTenSacThue1.Name = "lblTenSacThue1";
            this.lblTenSacThue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenSacThue1.SizeF = new System.Drawing.SizeF(99.10962F, 10F);
            this.lblTenSacThue1.StylePriority.UseFont = false;
            this.lblTenSacThue1.StylePriority.UseTextAlignment = false;
            this.lblTenSacThue1.Text = "WWWWWWWWE";
            this.lblTenSacThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaSacThue1
            // 
            this.lblMaSacThue1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaSacThue1.LocationFloat = new DevExpress.Utils.PointFloat(15.47619F, 587.4073F);
            this.lblMaSacThue1.Name = "lblMaSacThue1";
            this.lblMaSacThue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSacThue1.SizeF = new System.Drawing.SizeF(34.73936F, 10F);
            this.lblMaSacThue1.StylePriority.UseFont = false;
            this.lblMaSacThue1.StylePriority.UseTextAlignment = false;
            this.lblMaSacThue1.Text = "X";
            this.lblMaSacThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel153
            // 
            this.xrLabel153.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel153.LocationFloat = new DevExpress.Utils.PointFloat(248.3884F, 526.7349F);
            this.xrLabel153.Name = "xrLabel153";
            this.xrLabel153.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel153.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel153.StylePriority.UseFont = false;
            this.xrLabel153.StylePriority.UseTextAlignment = false;
            this.xrLabel153.Text = "-";
            this.xrLabel153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDongTienDieuChinhTriGia5
            // 
            this.lblMaDongTienDieuChinhTriGia5.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDongTienDieuChinhTriGia5.LocationFloat = new DevExpress.Utils.PointFloat(182.9669F, 526.7349F);
            this.lblMaDongTienDieuChinhTriGia5.Name = "lblMaDongTienDieuChinhTriGia5";
            this.lblMaDongTienDieuChinhTriGia5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDongTienDieuChinhTriGia5.SizeF = new System.Drawing.SizeF(64F, 9F);
            this.lblMaDongTienDieuChinhTriGia5.StylePriority.UseFont = false;
            this.lblMaDongTienDieuChinhTriGia5.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienDieuChinhTriGia5.Text = "XXE";
            this.lblMaDongTienDieuChinhTriGia5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDongTienDieuChinhTriGia4
            // 
            this.lblMaDongTienDieuChinhTriGia4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDongTienDieuChinhTriGia4.LocationFloat = new DevExpress.Utils.PointFloat(182.9669F, 517.7349F);
            this.lblMaDongTienDieuChinhTriGia4.Name = "lblMaDongTienDieuChinhTriGia4";
            this.lblMaDongTienDieuChinhTriGia4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDongTienDieuChinhTriGia4.SizeF = new System.Drawing.SizeF(64F, 9F);
            this.lblMaDongTienDieuChinhTriGia4.StylePriority.UseFont = false;
            this.lblMaDongTienDieuChinhTriGia4.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienDieuChinhTriGia4.Text = "XXE";
            this.lblMaDongTienDieuChinhTriGia4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel150
            // 
            this.xrLabel150.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel150.LocationFloat = new DevExpress.Utils.PointFloat(248.3884F, 517.7349F);
            this.xrLabel150.Name = "xrLabel150";
            this.xrLabel150.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel150.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel150.StylePriority.UseFont = false;
            this.xrLabel150.StylePriority.UseTextAlignment = false;
            this.xrLabel150.Text = "-";
            this.xrLabel150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel149
            // 
            this.xrLabel149.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel149.LocationFloat = new DevExpress.Utils.PointFloat(248.3884F, 508.7348F);
            this.xrLabel149.Name = "xrLabel149";
            this.xrLabel149.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel149.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel149.StylePriority.UseFont = false;
            this.xrLabel149.StylePriority.UseTextAlignment = false;
            this.xrLabel149.Text = "-";
            this.xrLabel149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDongTienDieuChinhTriGia3
            // 
            this.lblMaDongTienDieuChinhTriGia3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDongTienDieuChinhTriGia3.LocationFloat = new DevExpress.Utils.PointFloat(182.9669F, 508.7348F);
            this.lblMaDongTienDieuChinhTriGia3.Name = "lblMaDongTienDieuChinhTriGia3";
            this.lblMaDongTienDieuChinhTriGia3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDongTienDieuChinhTriGia3.SizeF = new System.Drawing.SizeF(64F, 9F);
            this.lblMaDongTienDieuChinhTriGia3.StylePriority.UseFont = false;
            this.lblMaDongTienDieuChinhTriGia3.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienDieuChinhTriGia3.Text = "XXE";
            this.lblMaDongTienDieuChinhTriGia3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel147
            // 
            this.xrLabel147.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel147.LocationFloat = new DevExpress.Utils.PointFloat(248.3884F, 499.7348F);
            this.xrLabel147.Name = "xrLabel147";
            this.xrLabel147.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel147.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel147.StylePriority.UseFont = false;
            this.xrLabel147.StylePriority.UseTextAlignment = false;
            this.xrLabel147.Text = "-";
            this.xrLabel147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDongTienDieuChinhTriGia2
            // 
            this.lblMaDongTienDieuChinhTriGia2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDongTienDieuChinhTriGia2.LocationFloat = new DevExpress.Utils.PointFloat(182.9669F, 499.7348F);
            this.lblMaDongTienDieuChinhTriGia2.Name = "lblMaDongTienDieuChinhTriGia2";
            this.lblMaDongTienDieuChinhTriGia2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDongTienDieuChinhTriGia2.SizeF = new System.Drawing.SizeF(64F, 9F);
            this.lblMaDongTienDieuChinhTriGia2.StylePriority.UseFont = false;
            this.lblMaDongTienDieuChinhTriGia2.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienDieuChinhTriGia2.Text = "XXE";
            this.lblMaDongTienDieuChinhTriGia2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel102
            // 
            this.xrLabel102.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(167.9669F, 490.7348F);
            this.xrLabel102.Name = "xrLabel102";
            this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel102.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel102.StylePriority.UseFont = false;
            this.xrLabel102.StylePriority.UseTextAlignment = false;
            this.xrLabel102.Text = "-";
            this.xrLabel102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaPhanLoaiDieuChinh1
            // 
            this.lblMaPhanLoaiDieuChinh1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiDieuChinh1.LocationFloat = new DevExpress.Utils.PointFloat(101.3868F, 490.7348F);
            this.lblMaPhanLoaiDieuChinh1.Name = "lblMaPhanLoaiDieuChinh1";
            this.lblMaPhanLoaiDieuChinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiDieuChinh1.SizeF = new System.Drawing.SizeF(66F, 9F);
            this.lblMaPhanLoaiDieuChinh1.StylePriority.UseFont = false;
            this.lblMaPhanLoaiDieuChinh1.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiDieuChinh1.Text = "XXE";
            this.lblMaPhanLoaiDieuChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel146
            // 
            this.xrLabel146.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel146.LocationFloat = new DevExpress.Utils.PointFloat(0.4761855F, 587.4073F);
            this.xrLabel146.Name = "xrLabel146";
            this.xrLabel146.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel146.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel146.StylePriority.UseFont = false;
            this.xrLabel146.StylePriority.UseTextAlignment = false;
            this.xrLabel146.Text = "1";
            this.xrLabel146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel145
            // 
            this.xrLabel145.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel145.LocationFloat = new DevExpress.Utils.PointFloat(272.7635F, 577.4073F);
            this.xrLabel145.Name = "xrLabel145";
            this.xrLabel145.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel145.SizeF = new System.Drawing.SizeF(77.52F, 10F);
            this.xrLabel145.StylePriority.UseFont = false;
            this.xrLabel145.StylePriority.UseTextAlignment = false;
            this.xrLabel145.Text = "Số dòng tổng";
            this.xrLabel145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel144
            // 
            this.xrLabel144.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel144.LocationFloat = new DevExpress.Utils.PointFloat(149.0227F, 577.4073F);
            this.xrLabel144.Name = "xrLabel144";
            this.xrLabel144.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel144.SizeF = new System.Drawing.SizeF(87.91F, 10F);
            this.xrLabel144.StylePriority.UseFont = false;
            this.xrLabel144.StylePriority.UseTextAlignment = false;
            this.xrLabel144.Text = "Tổng tiền thuế";
            this.xrLabel144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel143
            // 
            this.xrLabel143.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel143.LocationFloat = new DevExpress.Utils.PointFloat(14.86802F, 577.4073F);
            this.xrLabel143.Name = "xrLabel143";
            this.xrLabel143.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel143.SizeF = new System.Drawing.SizeF(134.1547F, 10F);
            this.xrLabel143.StylePriority.UseFont = false;
            this.xrLabel143.StylePriority.UseTextAlignment = false;
            this.xrLabel143.Text = "Tên sắc thuế";
            this.xrLabel143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblChiTietKhaiTriGia
            // 
            this.lblChiTietKhaiTriGia.Font = new System.Drawing.Font("Times New Roman", 6.5F);
            this.lblChiTietKhaiTriGia.LocationFloat = new DevExpress.Utils.PointFloat(0.8400016F, 545.4072F);
            this.lblChiTietKhaiTriGia.Multiline = true;
            this.lblChiTietKhaiTriGia.Name = "lblChiTietKhaiTriGia";
            this.lblChiTietKhaiTriGia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiTietKhaiTriGia.SizeF = new System.Drawing.SizeF(771.47F, 32F);
            this.lblChiTietKhaiTriGia.StylePriority.UseFont = false;
            this.lblChiTietKhaiTriGia.StylePriority.UseTextAlignment = false;
            this.lblChiTietKhaiTriGia.Text = resources.GetString("lblChiTietKhaiTriGia.Text");
            this.lblChiTietKhaiTriGia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel141
            // 
            this.xrLabel141.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel141.LocationFloat = new DevExpress.Utils.PointFloat(9.291246F, 536.4072F);
            this.xrLabel141.Name = "xrLabel141";
            this.xrLabel141.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel141.SizeF = new System.Drawing.SizeF(130.62F, 9F);
            this.xrLabel141.StylePriority.UseFont = false;
            this.xrLabel141.StylePriority.UseTextAlignment = false;
            this.xrLabel141.Text = "Chi tiết khai trị giá";
            this.xrLabel141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTongHeSoPhanBoTriGiaKhoanDieuChinh5
            // 
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.LocationFloat = new DevExpress.Utils.PointFloat(461.8822F, 526.7349F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.Name = "lblTongHeSoPhanBoTriGiaKhoanDieuChinh5";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.SizeF = new System.Drawing.SizeF(197F, 9F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.StylePriority.UseFont = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.StylePriority.UseTextAlignment = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.Text = "12.345.678.901.234.567.890";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaKhoanDieuChinh5
            // 
            this.lblTriGiaKhoanDieuChinh5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaKhoanDieuChinh5.LocationFloat = new DevExpress.Utils.PointFloat(264.8822F, 526.7349F);
            this.lblTriGiaKhoanDieuChinh5.Name = "lblTriGiaKhoanDieuChinh5";
            this.lblTriGiaKhoanDieuChinh5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaKhoanDieuChinh5.SizeF = new System.Drawing.SizeF(197F, 9F);
            this.lblTriGiaKhoanDieuChinh5.StylePriority.UseFont = false;
            this.lblTriGiaKhoanDieuChinh5.StylePriority.UseTextAlignment = false;
            this.lblTriGiaKhoanDieuChinh5.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaKhoanDieuChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTriGiaKhoanDieuChinh5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel138
            // 
            this.xrLabel138.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel138.LocationFloat = new DevExpress.Utils.PointFloat(167.9668F, 526.7349F);
            this.xrLabel138.Name = "xrLabel138";
            this.xrLabel138.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel138.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel138.StylePriority.UseFont = false;
            this.xrLabel138.StylePriority.UseTextAlignment = false;
            this.xrLabel138.Text = "-";
            this.xrLabel138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel137
            // 
            this.xrLabel137.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel137.LocationFloat = new DevExpress.Utils.PointFloat(34.41707F, 526.7344F);
            this.xrLabel137.Name = "xrLabel137";
            this.xrLabel137.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel137.SizeF = new System.Drawing.SizeF(13.9F, 9F);
            this.xrLabel137.StylePriority.UseFont = false;
            this.xrLabel137.StylePriority.UseTextAlignment = false;
            this.xrLabel137.Text = "5";
            this.xrLabel137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel136
            // 
            this.xrLabel136.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel136.LocationFloat = new DevExpress.Utils.PointFloat(86.38686F, 526.7349F);
            this.xrLabel136.Name = "xrLabel136";
            this.xrLabel136.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel136.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel136.StylePriority.UseFont = false;
            this.xrLabel136.StylePriority.UseTextAlignment = false;
            this.xrLabel136.Text = "-";
            this.xrLabel136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaTenKhoanDieuChinh5
            // 
            this.lblMaTenKhoanDieuChinh5.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaTenKhoanDieuChinh5.LocationFloat = new DevExpress.Utils.PointFloat(49.11798F, 526.7349F);
            this.lblMaTenKhoanDieuChinh5.Name = "lblMaTenKhoanDieuChinh5";
            this.lblMaTenKhoanDieuChinh5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTenKhoanDieuChinh5.SizeF = new System.Drawing.SizeF(37F, 9F);
            this.lblMaTenKhoanDieuChinh5.StylePriority.UseFont = false;
            this.lblMaTenKhoanDieuChinh5.StylePriority.UseTextAlignment = false;
            this.lblMaTenKhoanDieuChinh5.Text = "X";
            this.lblMaTenKhoanDieuChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaPhanLoaiDieuChinh5
            // 
            this.lblMaPhanLoaiDieuChinh5.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiDieuChinh5.LocationFloat = new DevExpress.Utils.PointFloat(101.3868F, 526.7349F);
            this.lblMaPhanLoaiDieuChinh5.Name = "lblMaPhanLoaiDieuChinh5";
            this.lblMaPhanLoaiDieuChinh5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiDieuChinh5.SizeF = new System.Drawing.SizeF(66F, 9F);
            this.lblMaPhanLoaiDieuChinh5.StylePriority.UseFont = false;
            this.lblMaPhanLoaiDieuChinh5.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiDieuChinh5.Text = "XXE";
            this.lblMaPhanLoaiDieuChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTongHeSoPhanBoTriGiaKhoanDieuChinh4
            // 
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.LocationFloat = new DevExpress.Utils.PointFloat(461.8822F, 517.7349F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.Name = "lblTongHeSoPhanBoTriGiaKhoanDieuChinh4";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.SizeF = new System.Drawing.SizeF(197F, 9F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.StylePriority.UseFont = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.StylePriority.UseTextAlignment = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.Text = "12.345.678.901.234.567.890";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaKhoanDieuChinh4
            // 
            this.lblTriGiaKhoanDieuChinh4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaKhoanDieuChinh4.LocationFloat = new DevExpress.Utils.PointFloat(264.8822F, 517.7349F);
            this.lblTriGiaKhoanDieuChinh4.Name = "lblTriGiaKhoanDieuChinh4";
            this.lblTriGiaKhoanDieuChinh4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaKhoanDieuChinh4.SizeF = new System.Drawing.SizeF(197F, 9F);
            this.lblTriGiaKhoanDieuChinh4.StylePriority.UseFont = false;
            this.lblTriGiaKhoanDieuChinh4.StylePriority.UseTextAlignment = false;
            this.lblTriGiaKhoanDieuChinh4.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaKhoanDieuChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTriGiaKhoanDieuChinh4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel131
            // 
            this.xrLabel131.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel131.LocationFloat = new DevExpress.Utils.PointFloat(167.9668F, 517.7349F);
            this.xrLabel131.Name = "xrLabel131";
            this.xrLabel131.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel131.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel131.StylePriority.UseFont = false;
            this.xrLabel131.StylePriority.UseTextAlignment = false;
            this.xrLabel131.Text = "-";
            this.xrLabel131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaPhanLoaiDieuChinh4
            // 
            this.lblMaPhanLoaiDieuChinh4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiDieuChinh4.LocationFloat = new DevExpress.Utils.PointFloat(101.3868F, 517.7349F);
            this.lblMaPhanLoaiDieuChinh4.Name = "lblMaPhanLoaiDieuChinh4";
            this.lblMaPhanLoaiDieuChinh4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiDieuChinh4.SizeF = new System.Drawing.SizeF(66F, 9F);
            this.lblMaPhanLoaiDieuChinh4.StylePriority.UseFont = false;
            this.lblMaPhanLoaiDieuChinh4.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiDieuChinh4.Text = "XXE";
            this.lblMaPhanLoaiDieuChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel129
            // 
            this.xrLabel129.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel129.LocationFloat = new DevExpress.Utils.PointFloat(86.38686F, 517.7349F);
            this.xrLabel129.Name = "xrLabel129";
            this.xrLabel129.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel129.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel129.StylePriority.UseFont = false;
            this.xrLabel129.StylePriority.UseTextAlignment = false;
            this.xrLabel129.Text = "-";
            this.xrLabel129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaTenKhoanDieuChinh4
            // 
            this.lblMaTenKhoanDieuChinh4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaTenKhoanDieuChinh4.LocationFloat = new DevExpress.Utils.PointFloat(49.11798F, 517.7349F);
            this.lblMaTenKhoanDieuChinh4.Name = "lblMaTenKhoanDieuChinh4";
            this.lblMaTenKhoanDieuChinh4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTenKhoanDieuChinh4.SizeF = new System.Drawing.SizeF(37F, 9F);
            this.lblMaTenKhoanDieuChinh4.StylePriority.UseFont = false;
            this.lblMaTenKhoanDieuChinh4.StylePriority.UseTextAlignment = false;
            this.lblMaTenKhoanDieuChinh4.Text = "X";
            this.lblMaTenKhoanDieuChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel127
            // 
            this.xrLabel127.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel127.LocationFloat = new DevExpress.Utils.PointFloat(34.41689F, 517.7344F);
            this.xrLabel127.Name = "xrLabel127";
            this.xrLabel127.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel127.SizeF = new System.Drawing.SizeF(13.9F, 9F);
            this.xrLabel127.StylePriority.UseFont = false;
            this.xrLabel127.StylePriority.UseTextAlignment = false;
            this.xrLabel127.Text = "4";
            this.xrLabel127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTongHeSoPhanBoTriGiaKhoanDieuChinh3
            // 
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.LocationFloat = new DevExpress.Utils.PointFloat(461.8822F, 508.7348F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.Name = "lblTongHeSoPhanBoTriGiaKhoanDieuChinh3";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.SizeF = new System.Drawing.SizeF(197F, 9F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.StylePriority.UseFont = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.StylePriority.UseTextAlignment = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.Text = "12.345.678.901.234.567.890";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaKhoanDieuChinh3
            // 
            this.lblTriGiaKhoanDieuChinh3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaKhoanDieuChinh3.LocationFloat = new DevExpress.Utils.PointFloat(264.8822F, 508.7348F);
            this.lblTriGiaKhoanDieuChinh3.Name = "lblTriGiaKhoanDieuChinh3";
            this.lblTriGiaKhoanDieuChinh3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaKhoanDieuChinh3.SizeF = new System.Drawing.SizeF(197F, 9F);
            this.lblTriGiaKhoanDieuChinh3.StylePriority.UseFont = false;
            this.lblTriGiaKhoanDieuChinh3.StylePriority.UseTextAlignment = false;
            this.lblTriGiaKhoanDieuChinh3.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaKhoanDieuChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTriGiaKhoanDieuChinh3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel124
            // 
            this.xrLabel124.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel124.LocationFloat = new DevExpress.Utils.PointFloat(167.9673F, 508.7348F);
            this.xrLabel124.Name = "xrLabel124";
            this.xrLabel124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel124.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel124.StylePriority.UseFont = false;
            this.xrLabel124.StylePriority.UseTextAlignment = false;
            this.xrLabel124.Text = "-";
            this.xrLabel124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaPhanLoaiDieuChinh3
            // 
            this.lblMaPhanLoaiDieuChinh3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiDieuChinh3.LocationFloat = new DevExpress.Utils.PointFloat(101.3868F, 508.7348F);
            this.lblMaPhanLoaiDieuChinh3.Name = "lblMaPhanLoaiDieuChinh3";
            this.lblMaPhanLoaiDieuChinh3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiDieuChinh3.SizeF = new System.Drawing.SizeF(66F, 9F);
            this.lblMaPhanLoaiDieuChinh3.StylePriority.UseFont = false;
            this.lblMaPhanLoaiDieuChinh3.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiDieuChinh3.Text = "XXE";
            this.lblMaPhanLoaiDieuChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel122
            // 
            this.xrLabel122.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel122.LocationFloat = new DevExpress.Utils.PointFloat(86.38686F, 508.7348F);
            this.xrLabel122.Name = "xrLabel122";
            this.xrLabel122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel122.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel122.StylePriority.UseFont = false;
            this.xrLabel122.StylePriority.UseTextAlignment = false;
            this.xrLabel122.Text = "-";
            this.xrLabel122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaTenKhoanDieuChinh3
            // 
            this.lblMaTenKhoanDieuChinh3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaTenKhoanDieuChinh3.LocationFloat = new DevExpress.Utils.PointFloat(49.11834F, 508.7348F);
            this.lblMaTenKhoanDieuChinh3.Name = "lblMaTenKhoanDieuChinh3";
            this.lblMaTenKhoanDieuChinh3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTenKhoanDieuChinh3.SizeF = new System.Drawing.SizeF(37F, 9F);
            this.lblMaTenKhoanDieuChinh3.StylePriority.UseFont = false;
            this.lblMaTenKhoanDieuChinh3.StylePriority.UseTextAlignment = false;
            this.lblMaTenKhoanDieuChinh3.Text = "X";
            this.lblMaTenKhoanDieuChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel120
            // 
            this.xrLabel120.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(34.11834F, 508.7344F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(13.9F, 9F);
            this.xrLabel120.StylePriority.UseFont = false;
            this.xrLabel120.StylePriority.UseTextAlignment = false;
            this.xrLabel120.Text = "3";
            this.xrLabel120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTongHeSoPhanBoTriGiaKhoanDieuChinh2
            // 
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.LocationFloat = new DevExpress.Utils.PointFloat(461.8822F, 499.7348F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.Name = "lblTongHeSoPhanBoTriGiaKhoanDieuChinh2";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.SizeF = new System.Drawing.SizeF(197F, 9F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.StylePriority.UseFont = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.StylePriority.UseTextAlignment = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.Text = "12.345.678.901.234.567.890";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaKhoanDieuChinh2
            // 
            this.lblTriGiaKhoanDieuChinh2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaKhoanDieuChinh2.LocationFloat = new DevExpress.Utils.PointFloat(264.8822F, 499.7348F);
            this.lblTriGiaKhoanDieuChinh2.Name = "lblTriGiaKhoanDieuChinh2";
            this.lblTriGiaKhoanDieuChinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaKhoanDieuChinh2.SizeF = new System.Drawing.SizeF(197F, 9F);
            this.lblTriGiaKhoanDieuChinh2.StylePriority.UseFont = false;
            this.lblTriGiaKhoanDieuChinh2.StylePriority.UseTextAlignment = false;
            this.lblTriGiaKhoanDieuChinh2.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaKhoanDieuChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTriGiaKhoanDieuChinh2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel117
            // 
            this.xrLabel117.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel117.LocationFloat = new DevExpress.Utils.PointFloat(167.967F, 499.7348F);
            this.xrLabel117.Name = "xrLabel117";
            this.xrLabel117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel117.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel117.StylePriority.UseFont = false;
            this.xrLabel117.StylePriority.UseTextAlignment = false;
            this.xrLabel117.Text = "-";
            this.xrLabel117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaPhanLoaiDieuChinh2
            // 
            this.lblMaPhanLoaiDieuChinh2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiDieuChinh2.LocationFloat = new DevExpress.Utils.PointFloat(101.3868F, 499.7348F);
            this.lblMaPhanLoaiDieuChinh2.Name = "lblMaPhanLoaiDieuChinh2";
            this.lblMaPhanLoaiDieuChinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiDieuChinh2.SizeF = new System.Drawing.SizeF(66F, 9F);
            this.lblMaPhanLoaiDieuChinh2.StylePriority.UseFont = false;
            this.lblMaPhanLoaiDieuChinh2.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiDieuChinh2.Text = "XXE";
            this.lblMaPhanLoaiDieuChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel111
            // 
            this.xrLabel111.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(86.38686F, 499.7348F);
            this.xrLabel111.Name = "xrLabel111";
            this.xrLabel111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel111.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel111.StylePriority.UseFont = false;
            this.xrLabel111.StylePriority.UseTextAlignment = false;
            this.xrLabel111.Text = "-";
            this.xrLabel111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaTenKhoanDieuChinh2
            // 
            this.lblMaTenKhoanDieuChinh2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaTenKhoanDieuChinh2.LocationFloat = new DevExpress.Utils.PointFloat(49.11798F, 499.7348F);
            this.lblMaTenKhoanDieuChinh2.Name = "lblMaTenKhoanDieuChinh2";
            this.lblMaTenKhoanDieuChinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTenKhoanDieuChinh2.SizeF = new System.Drawing.SizeF(37F, 9F);
            this.lblMaTenKhoanDieuChinh2.StylePriority.UseFont = false;
            this.lblMaTenKhoanDieuChinh2.StylePriority.UseTextAlignment = false;
            this.lblMaTenKhoanDieuChinh2.Text = "X";
            this.lblMaTenKhoanDieuChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel108
            // 
            this.xrLabel108.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(34.11798F, 499.7344F);
            this.xrLabel108.Name = "xrLabel108";
            this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel108.SizeF = new System.Drawing.SizeF(13.9F, 9F);
            this.xrLabel108.StylePriority.UseFont = false;
            this.xrLabel108.StylePriority.UseTextAlignment = false;
            this.xrLabel108.Text = "2";
            this.xrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTongHeSoPhanBoTriGiaKhoanDieuChinh1
            // 
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.LocationFloat = new DevExpress.Utils.PointFloat(461.8822F, 490.7348F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.Name = "lblTongHeSoPhanBoTriGiaKhoanDieuChinh1";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.SizeF = new System.Drawing.SizeF(197F, 9F);
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.StylePriority.UseFont = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.StylePriority.UseTextAlignment = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.Text = "12.345.678.901.234.567.890";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaKhoanDieuChinh1
            // 
            this.lblTriGiaKhoanDieuChinh1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaKhoanDieuChinh1.LocationFloat = new DevExpress.Utils.PointFloat(264.8822F, 490.7348F);
            this.lblTriGiaKhoanDieuChinh1.Name = "lblTriGiaKhoanDieuChinh1";
            this.lblTriGiaKhoanDieuChinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaKhoanDieuChinh1.SizeF = new System.Drawing.SizeF(197F, 9F);
            this.lblTriGiaKhoanDieuChinh1.StylePriority.UseFont = false;
            this.lblTriGiaKhoanDieuChinh1.StylePriority.UseTextAlignment = false;
            this.lblTriGiaKhoanDieuChinh1.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaKhoanDieuChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTriGiaKhoanDieuChinh1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel104
            // 
            this.xrLabel104.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel104.LocationFloat = new DevExpress.Utils.PointFloat(248.3884F, 490.7348F);
            this.xrLabel104.Name = "xrLabel104";
            this.xrLabel104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel104.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel104.StylePriority.UseFont = false;
            this.xrLabel104.StylePriority.UseTextAlignment = false;
            this.xrLabel104.Text = "-";
            this.xrLabel104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDongTienDieuChinhTriGia1
            // 
            this.lblMaDongTienDieuChinhTriGia1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDongTienDieuChinhTriGia1.LocationFloat = new DevExpress.Utils.PointFloat(182.9669F, 490.7348F);
            this.lblMaDongTienDieuChinhTriGia1.Name = "lblMaDongTienDieuChinhTriGia1";
            this.lblMaDongTienDieuChinhTriGia1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDongTienDieuChinhTriGia1.SizeF = new System.Drawing.SizeF(64F, 9F);
            this.lblMaDongTienDieuChinhTriGia1.StylePriority.UseFont = false;
            this.lblMaDongTienDieuChinhTriGia1.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienDieuChinhTriGia1.Text = "XXE";
            this.lblMaDongTienDieuChinhTriGia1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel101
            // 
            this.xrLabel101.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel101.LocationFloat = new DevExpress.Utils.PointFloat(86.38686F, 490.7348F);
            this.xrLabel101.Name = "xrLabel101";
            this.xrLabel101.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel101.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel101.StylePriority.UseFont = false;
            this.xrLabel101.StylePriority.UseTextAlignment = false;
            this.xrLabel101.Text = "-";
            this.xrLabel101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaTenKhoanDieuChinh1
            // 
            this.lblMaTenKhoanDieuChinh1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaTenKhoanDieuChinh1.LocationFloat = new DevExpress.Utils.PointFloat(49.11798F, 490.7348F);
            this.lblMaTenKhoanDieuChinh1.Name = "lblMaTenKhoanDieuChinh1";
            this.lblMaTenKhoanDieuChinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTenKhoanDieuChinh1.SizeF = new System.Drawing.SizeF(37F, 9F);
            this.lblMaTenKhoanDieuChinh1.StylePriority.UseFont = false;
            this.lblMaTenKhoanDieuChinh1.StylePriority.UseTextAlignment = false;
            this.lblMaTenKhoanDieuChinh1.Text = "X";
            this.lblMaTenKhoanDieuChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel98
            // 
            this.xrLabel98.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel98.LocationFloat = new DevExpress.Utils.PointFloat(34.11798F, 490.7344F);
            this.xrLabel98.Name = "xrLabel98";
            this.xrLabel98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel98.SizeF = new System.Drawing.SizeF(13.9F, 9F);
            this.xrLabel98.StylePriority.UseFont = false;
            this.xrLabel98.StylePriority.UseTextAlignment = false;
            this.xrLabel98.Text = "1";
            this.xrLabel98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel97
            // 
            this.xrLabel97.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel97.LocationFloat = new DevExpress.Utils.PointFloat(461.8621F, 481.5003F);
            this.xrLabel97.Name = "xrLabel97";
            this.xrLabel97.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel97.SizeF = new System.Drawing.SizeF(197F, 9F);
            this.xrLabel97.StylePriority.UseFont = false;
            this.xrLabel97.StylePriority.UseTextAlignment = false;
            this.xrLabel97.Text = "Tổng hệ số phân bổ";
            this.xrLabel97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel96
            // 
            this.xrLabel96.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(263.9821F, 481.5005F);
            this.xrLabel96.Name = "xrLabel96";
            this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel96.SizeF = new System.Drawing.SizeF(197.88F, 10F);
            this.xrLabel96.StylePriority.UseFont = false;
            this.xrLabel96.StylePriority.UseTextAlignment = false;
            this.xrLabel96.Text = "Trị giá khoản điều chỉnh";
            this.xrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel74
            // 
            this.xrLabel74.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(99.09554F, 481.5005F);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(71.76F, 10F);
            this.xrLabel74.StylePriority.UseFont = false;
            this.xrLabel74.StylePriority.UseTextAlignment = false;
            this.xrLabel74.Text = "Mã phân loại";
            this.xrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel70
            // 
            this.xrLabel70.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(50.21556F, 481.5003F);
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel70.SizeF = new System.Drawing.SizeF(43.64F, 10F);
            this.xrLabel70.StylePriority.UseFont = false;
            this.xrLabel70.StylePriority.UseTextAlignment = false;
            this.xrLabel70.Text = "Mã tên";
            this.xrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaTienTe
            // 
            this.lblMaTienTe.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaTienTe.LocationFloat = new DevExpress.Utils.PointFloat(501.9916F, 442.5001F);
            this.lblMaTienTe.Name = "lblMaTienTe";
            this.lblMaTienTe.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTienTe.SizeF = new System.Drawing.SizeF(29.67F, 10F);
            this.lblMaTienTe.StylePriority.UseFont = false;
            this.lblMaTienTe.StylePriority.UseTextAlignment = false;
            this.lblMaTienTe.Text = "XXE";
            this.lblMaTienTe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel116
            // 
            this.xrLabel116.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel116.LocationFloat = new DevExpress.Utils.PointFloat(8.310805F, 400.5F);
            this.xrLabel116.Name = "xrLabel116";
            this.xrLabel116.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel116.SizeF = new System.Drawing.SizeF(143.65F, 9F);
            this.xrLabel116.StylePriority.UseFont = false;
            this.xrLabel116.StylePriority.UseTextAlignment = false;
            this.xrLabel116.Text = "Giấy phép nhập khẩu";
            this.xrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel115
            // 
            this.xrLabel115.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel115.LocationFloat = new DevExpress.Utils.PointFloat(375.4593F, 472.5004F);
            this.xrLabel115.Name = "xrLabel115";
            this.xrLabel115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel115.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel115.StylePriority.UseFont = false;
            this.xrLabel115.StylePriority.UseTextAlignment = false;
            this.xrLabel115.Text = "-";
            this.xrLabel115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel114
            // 
            this.xrLabel114.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel114.LocationFloat = new DevExpress.Utils.PointFloat(213.7403F, 472.5004F);
            this.xrLabel114.Name = "xrLabel114";
            this.xrLabel114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel114.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel114.StylePriority.UseFont = false;
            this.xrLabel114.StylePriority.UseTextAlignment = false;
            this.xrLabel114.Text = "-";
            this.xrLabel114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhiBaoHiem
            // 
            this.lblPhiBaoHiem.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhiBaoHiem.LocationFloat = new DevExpress.Utils.PointFloat(228.7403F, 472.5004F);
            this.lblPhiBaoHiem.Name = "lblPhiBaoHiem";
            this.lblPhiBaoHiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhiBaoHiem.SizeF = new System.Drawing.SizeF(145.1691F, 10.00003F);
            this.lblPhiBaoHiem.StylePriority.UseFont = false;
            this.lblPhiBaoHiem.StylePriority.UseTextAlignment = false;
            this.lblPhiBaoHiem.Text = "1.234.567.890.123.456";
            this.lblPhiBaoHiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblPhiBaoHiem.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel112
            // 
            this.xrLabel112.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel112.LocationFloat = new DevExpress.Utils.PointFloat(213.7403F, 462.5003F);
            this.xrLabel112.Name = "xrLabel112";
            this.xrLabel112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel112.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel112.StylePriority.UseFont = false;
            this.xrLabel112.StylePriority.UseTextAlignment = false;
            this.xrLabel112.Text = "-";
            this.xrLabel112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhiVanChuyen
            // 
            this.lblPhiVanChuyen.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhiVanChuyen.LocationFloat = new DevExpress.Utils.PointFloat(228.7404F, 462.5003F);
            this.lblPhiVanChuyen.Name = "lblPhiVanChuyen";
            this.lblPhiVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhiVanChuyen.SizeF = new System.Drawing.SizeF(144.24F, 10F);
            this.lblPhiVanChuyen.StylePriority.UseFont = false;
            this.lblPhiVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblPhiVanChuyen.Text = "123.456.789.012.345.678";
            this.lblPhiVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblPhiVanChuyen.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaTienTeCuaTienBaoHiem
            // 
            this.lblMaTienTeCuaTienBaoHiem.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaTienTeCuaTienBaoHiem.LocationFloat = new DevExpress.Utils.PointFloat(184.0703F, 472.5005F);
            this.lblMaTienTeCuaTienBaoHiem.Name = "lblMaTienTeCuaTienBaoHiem";
            this.lblMaTienTeCuaTienBaoHiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTienTeCuaTienBaoHiem.SizeF = new System.Drawing.SizeF(29.67F, 10F);
            this.lblMaTienTeCuaTienBaoHiem.StylePriority.UseFont = false;
            this.lblMaTienTeCuaTienBaoHiem.StylePriority.UseTextAlignment = false;
            this.lblMaTienTeCuaTienBaoHiem.Text = "XXE";
            this.lblMaTienTeCuaTienBaoHiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel109
            // 
            this.xrLabel109.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel109.LocationFloat = new DevExpress.Utils.PointFloat(169.9996F, 472.5004F);
            this.xrLabel109.Name = "xrLabel109";
            this.xrLabel109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel109.SizeF = new System.Drawing.SizeF(14.07082F, 10.00003F);
            this.xrLabel109.StylePriority.UseFont = false;
            this.xrLabel109.StylePriority.UseTextAlignment = false;
            this.xrLabel109.Text = "-";
            this.xrLabel109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiBaoHiem
            // 
            this.lblMaPhanLoaiBaoHiem.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiBaoHiem.LocationFloat = new DevExpress.Utils.PointFloat(151.0303F, 472.5004F);
            this.lblMaPhanLoaiBaoHiem.Name = "lblMaPhanLoaiBaoHiem";
            this.lblMaPhanLoaiBaoHiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiBaoHiem.SizeF = new System.Drawing.SizeF(18.97F, 10F);
            this.lblMaPhanLoaiBaoHiem.StylePriority.UseFont = false;
            this.lblMaPhanLoaiBaoHiem.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiBaoHiem.Text = "X";
            this.lblMaPhanLoaiBaoHiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaTienTePhiVanChuyen
            // 
            this.lblMaTienTePhiVanChuyen.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaTienTePhiVanChuyen.LocationFloat = new DevExpress.Utils.PointFloat(184.0703F, 462.5003F);
            this.lblMaTienTePhiVanChuyen.Name = "lblMaTienTePhiVanChuyen";
            this.lblMaTienTePhiVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTienTePhiVanChuyen.SizeF = new System.Drawing.SizeF(29.67F, 10F);
            this.lblMaTienTePhiVanChuyen.StylePriority.UseFont = false;
            this.lblMaTienTePhiVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblMaTienTePhiVanChuyen.Text = "XXE";
            this.lblMaTienTePhiVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel106
            // 
            this.xrLabel106.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel106.LocationFloat = new DevExpress.Utils.PointFloat(169.9188F, 462.5003F);
            this.xrLabel106.Name = "xrLabel106";
            this.xrLabel106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel106.SizeF = new System.Drawing.SizeF(14.15158F, 10F);
            this.xrLabel106.StylePriority.UseFont = false;
            this.xrLabel106.StylePriority.UseTextAlignment = false;
            this.xrLabel106.Text = "-";
            this.xrLabel106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiPhiVanChuyen
            // 
            this.lblMaPhanLoaiPhiVanChuyen.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiPhiVanChuyen.LocationFloat = new DevExpress.Utils.PointFloat(150.5832F, 462.5003F);
            this.lblMaPhanLoaiPhiVanChuyen.Name = "lblMaPhanLoaiPhiVanChuyen";
            this.lblMaPhanLoaiPhiVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiPhiVanChuyen.SizeF = new System.Drawing.SizeF(18.97F, 10F);
            this.lblMaPhanLoaiPhiVanChuyen.StylePriority.UseFont = false;
            this.lblMaPhanLoaiPhiVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiPhiVanChuyen.Text = "X";
            this.lblMaPhanLoaiPhiVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGiaCoSo
            // 
            this.lblGiaCoSo.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblGiaCoSo.LocationFloat = new DevExpress.Utils.PointFloat(547.015F, 442.5001F);
            this.lblGiaCoSo.Name = "lblGiaCoSo";
            this.lblGiaCoSo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiaCoSo.SizeF = new System.Drawing.SizeF(154.35F, 10F);
            this.lblGiaCoSo.StylePriority.UseFont = false;
            this.lblGiaCoSo.StylePriority.UseTextAlignment = false;
            this.lblGiaCoSo.Text = "12.345.678.901.234.567.890";
            this.lblGiaCoSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblGiaCoSo.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel103
            // 
            this.xrLabel103.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(531.806F, 442.5001F);
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel103.StylePriority.UseFont = false;
            this.xrLabel103.StylePriority.UseTextAlignment = false;
            this.xrLabel103.Text = "-";
            this.xrLabel103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoDangKyBaoHiemTongHop
            // 
            this.lblSoDangKyBaoHiemTongHop.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoDangKyBaoHiemTongHop.LocationFloat = new DevExpress.Utils.PointFloat(390.4593F, 472.5004F);
            this.lblSoDangKyBaoHiemTongHop.Name = "lblSoDangKyBaoHiemTongHop";
            this.lblSoDangKyBaoHiemTongHop.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDangKyBaoHiemTongHop.SizeF = new System.Drawing.SizeF(89.32F, 10F);
            this.lblSoDangKyBaoHiemTongHop.StylePriority.UseFont = false;
            this.lblSoDangKyBaoHiemTongHop.StylePriority.UseTextAlignment = false;
            this.lblSoDangKyBaoHiemTongHop.Text = "XXXXXE";
            this.lblSoDangKyBaoHiemTongHop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhuongPhapDieuChinhTriGia
            // 
            this.lblPhuongPhapDieuChinhTriGia.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblPhuongPhapDieuChinhTriGia.LocationFloat = new DevExpress.Utils.PointFloat(300.3975F, 442.5002F);
            this.lblPhuongPhapDieuChinhTriGia.Name = "lblPhuongPhapDieuChinhTriGia";
            this.lblPhuongPhapDieuChinhTriGia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhuongPhapDieuChinhTriGia.SizeF = new System.Drawing.SizeF(186.24F, 10F);
            this.lblPhuongPhapDieuChinhTriGia.StylePriority.UseFont = false;
            this.lblPhuongPhapDieuChinhTriGia.StylePriority.UseTextAlignment = false;
            this.lblPhuongPhapDieuChinhTriGia.Text = "XXXXXXXXX1XXXXXXXXX2XE";
            this.lblPhuongPhapDieuChinhTriGia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiDieuChinhTriGia
            // 
            this.lblMaPhanLoaiDieuChinhTriGia.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiDieuChinhTriGia.LocationFloat = new DevExpress.Utils.PointFloat(270.726F, 442.5001F);
            this.lblMaPhanLoaiDieuChinhTriGia.Name = "lblMaPhanLoaiDieuChinhTriGia";
            this.lblMaPhanLoaiDieuChinhTriGia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiDieuChinhTriGia.SizeF = new System.Drawing.SizeF(29.67F, 10F);
            this.lblMaPhanLoaiDieuChinhTriGia.StylePriority.UseFont = false;
            this.lblMaPhanLoaiDieuChinhTriGia.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiDieuChinhTriGia.Text = "XE";
            this.lblMaPhanLoaiDieuChinhTriGia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel99
            // 
            this.xrLabel99.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel99.LocationFloat = new DevExpress.Utils.PointFloat(255.726F, 442.5001F);
            this.xrLabel99.Name = "xrLabel99";
            this.xrLabel99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel99.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel99.StylePriority.UseFont = false;
            this.xrLabel99.StylePriority.UseTextAlignment = false;
            this.xrLabel99.Text = "-";
            this.xrLabel99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiCongThucChuan
            // 
            this.lblPhanLoaiCongThucChuan.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblPhanLoaiCongThucChuan.LocationFloat = new DevExpress.Utils.PointFloat(235.9542F, 442.5001F);
            this.lblPhanLoaiCongThucChuan.Name = "lblPhanLoaiCongThucChuan";
            this.lblPhanLoaiCongThucChuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiCongThucChuan.SizeF = new System.Drawing.SizeF(19.77F, 10F);
            this.lblPhanLoaiCongThucChuan.StylePriority.UseFont = false;
            this.lblPhanLoaiCongThucChuan.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiCongThucChuan.Text = "X";
            this.lblPhanLoaiCongThucChuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTiepNhanKhaiTriGiaTongHop
            // 
            this.lblSoTiepNhanKhaiTriGiaTongHop.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoTiepNhanKhaiTriGiaTongHop.LocationFloat = new DevExpress.Utils.PointFloat(151.0308F, 442.5002F);
            this.lblSoTiepNhanKhaiTriGiaTongHop.Name = "lblSoTiepNhanKhaiTriGiaTongHop";
            this.lblSoTiepNhanKhaiTriGiaTongHop.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTiepNhanKhaiTriGiaTongHop.SizeF = new System.Drawing.SizeF(84.26F, 10F);
            this.lblSoTiepNhanKhaiTriGiaTongHop.StylePriority.UseFont = false;
            this.lblSoTiepNhanKhaiTriGiaTongHop.StylePriority.UseTextAlignment = false;
            this.lblSoTiepNhanKhaiTriGiaTongHop.Text = "XXXXXXXXE";
            this.lblSoTiepNhanKhaiTriGiaTongHop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoTiepNhanKhaiTriGiaTongHop.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaPhanLoaiKhaiTriGia
            // 
            this.lblMaPhanLoaiKhaiTriGia.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiKhaiTriGia.LocationFloat = new DevExpress.Utils.PointFloat(151.0308F, 432.5002F);
            this.lblMaPhanLoaiKhaiTriGia.Name = "lblMaPhanLoaiKhaiTriGia";
            this.lblMaPhanLoaiKhaiTriGia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiKhaiTriGia.SizeF = new System.Drawing.SizeF(19.77F, 10F);
            this.lblMaPhanLoaiKhaiTriGia.StylePriority.UseFont = false;
            this.lblMaPhanLoaiKhaiTriGia.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiKhaiTriGia.Text = "X";
            this.lblMaPhanLoaiKhaiTriGia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel95
            // 
            this.xrLabel95.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(44.04369F, 471.5003F);
            this.xrLabel95.Name = "xrLabel95";
            this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel95.SizeF = new System.Drawing.SizeF(106.53F, 10F);
            this.xrLabel95.StylePriority.UseFont = false;
            this.xrLabel95.StylePriority.UseTextAlignment = false;
            this.xrLabel95.Text = "Phí bảo hiểm";
            this.xrLabel95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel94
            // 
            this.xrLabel94.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(44.05369F, 461.5003F);
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(106.52F, 10F);
            this.xrLabel94.StylePriority.UseFont = false;
            this.xrLabel94.StylePriority.UseTextAlignment = false;
            this.xrLabel94.Text = "Phí vận chuyển";
            this.xrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel93
            // 
            this.xrLabel93.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(20.4008F, 451.5003F);
            this.xrLabel93.Name = "xrLabel93";
            this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel93.SizeF = new System.Drawing.SizeF(130.62F, 10F);
            this.xrLabel93.StylePriority.UseFont = false;
            this.xrLabel93.StylePriority.UseTextAlignment = false;
            this.xrLabel93.Text = "Các khoản điều chỉnh";
            this.xrLabel93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel92
            // 
            this.xrLabel92.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(20.40079F, 441.5002F);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(130.62F, 10F);
            this.xrLabel92.StylePriority.UseFont = false;
            this.xrLabel92.StylePriority.UseTextAlignment = false;
            this.xrLabel92.Text = "Khai trị giá tổng hợp";
            this.xrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoGiayPhep5
            // 
            this.lblSoGiayPhep5.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoGiayPhep5.LocationFloat = new DevExpress.Utils.PointFloat(350.646F, 418.5F);
            this.lblSoGiayPhep5.Name = "lblSoGiayPhep5";
            this.lblSoGiayPhep5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoGiayPhep5.SizeF = new System.Drawing.SizeF(175F, 9F);
            this.lblSoGiayPhep5.StylePriority.UseFont = false;
            this.lblSoGiayPhep5.StylePriority.UseTextAlignment = false;
            this.lblSoGiayPhep5.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoGiayPhep5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel90
            // 
            this.xrLabel90.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel90.LocationFloat = new DevExpress.Utils.PointFloat(337.646F, 418.5F);
            this.xrLabel90.Name = "xrLabel90";
            this.xrLabel90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel90.SizeF = new System.Drawing.SizeF(13F, 9F);
            this.xrLabel90.StylePriority.UseFont = false;
            this.xrLabel90.StylePriority.UseTextAlignment = false;
            this.xrLabel90.Text = "-";
            this.xrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiGiayPhepNhapKhau5
            // 
            this.lblPhanLoaiGiayPhepNhapKhau5.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblPhanLoaiGiayPhepNhapKhau5.LocationFloat = new DevExpress.Utils.PointFloat(288.0722F, 418.5F);
            this.lblPhanLoaiGiayPhepNhapKhau5.Name = "lblPhanLoaiGiayPhepNhapKhau5";
            this.lblPhanLoaiGiayPhepNhapKhau5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiGiayPhepNhapKhau5.SizeF = new System.Drawing.SizeF(49.57F, 9F);
            this.lblPhanLoaiGiayPhepNhapKhau5.StylePriority.UseFont = false;
            this.lblPhanLoaiGiayPhepNhapKhau5.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiGiayPhepNhapKhau5.Text = "XXXXE";
            this.lblPhanLoaiGiayPhepNhapKhau5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel88
            // 
            this.xrLabel88.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel88.LocationFloat = new DevExpress.Utils.PointFloat(273.0722F, 418.5002F);
            this.xrLabel88.Name = "xrLabel88";
            this.xrLabel88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel88.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel88.StylePriority.UseFont = false;
            this.xrLabel88.StylePriority.UseTextAlignment = false;
            this.xrLabel88.Text = "5";
            this.xrLabel88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoGiayPhep3
            // 
            this.lblSoGiayPhep3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoGiayPhep3.LocationFloat = new DevExpress.Utils.PointFloat(604.3806F, 409.5F);
            this.lblSoGiayPhep3.Name = "lblSoGiayPhep3";
            this.lblSoGiayPhep3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoGiayPhep3.SizeF = new System.Drawing.SizeF(170.59F, 10F);
            this.lblSoGiayPhep3.StylePriority.UseFont = false;
            this.lblSoGiayPhep3.StylePriority.UseTextAlignment = false;
            this.lblSoGiayPhep3.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoGiayPhep3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel86
            // 
            this.xrLabel86.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(591.3805F, 409.5F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(13F, 10F);
            this.xrLabel86.StylePriority.UseFont = false;
            this.xrLabel86.StylePriority.UseTextAlignment = false;
            this.xrLabel86.Text = "-";
            this.xrLabel86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiGiayPhepNhapKhau3
            // 
            this.lblPhanLoaiGiayPhepNhapKhau3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblPhanLoaiGiayPhepNhapKhau3.LocationFloat = new DevExpress.Utils.PointFloat(541.8068F, 409.5F);
            this.lblPhanLoaiGiayPhepNhapKhau3.Name = "lblPhanLoaiGiayPhepNhapKhau3";
            this.lblPhanLoaiGiayPhepNhapKhau3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiGiayPhepNhapKhau3.SizeF = new System.Drawing.SizeF(49.57F, 10F);
            this.lblPhanLoaiGiayPhepNhapKhau3.StylePriority.UseFont = false;
            this.lblPhanLoaiGiayPhepNhapKhau3.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiGiayPhepNhapKhau3.Text = "XXXXE";
            this.lblPhanLoaiGiayPhepNhapKhau3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel84
            // 
            this.xrLabel84.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(526.8066F, 409.5F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel84.StylePriority.UseFont = false;
            this.xrLabel84.StylePriority.UseTextAlignment = false;
            this.xrLabel84.Text = "3";
            this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoGiayPhep4
            // 
            this.lblSoGiayPhep4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoGiayPhep4.LocationFloat = new DevExpress.Utils.PointFloat(96.07214F, 418.5002F);
            this.lblSoGiayPhep4.Name = "lblSoGiayPhep4";
            this.lblSoGiayPhep4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoGiayPhep4.SizeF = new System.Drawing.SizeF(175F, 9F);
            this.lblSoGiayPhep4.StylePriority.UseFont = false;
            this.lblSoGiayPhep4.StylePriority.UseTextAlignment = false;
            this.lblSoGiayPhep4.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoGiayPhep4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel82
            // 
            this.xrLabel82.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel82.LocationFloat = new DevExpress.Utils.PointFloat(83.0722F, 419.5F);
            this.xrLabel82.Name = "xrLabel82";
            this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel82.SizeF = new System.Drawing.SizeF(13F, 9F);
            this.xrLabel82.StylePriority.UseFont = false;
            this.xrLabel82.StylePriority.UseTextAlignment = false;
            this.xrLabel82.Text = "-";
            this.xrLabel82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiGiayPhepNhapKhau4
            // 
            this.lblPhanLoaiGiayPhepNhapKhau4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblPhanLoaiGiayPhepNhapKhau4.LocationFloat = new DevExpress.Utils.PointFloat(33.49849F, 418.5F);
            this.lblPhanLoaiGiayPhepNhapKhau4.Name = "lblPhanLoaiGiayPhepNhapKhau4";
            this.lblPhanLoaiGiayPhepNhapKhau4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiGiayPhepNhapKhau4.SizeF = new System.Drawing.SizeF(49.57F, 9F);
            this.lblPhanLoaiGiayPhepNhapKhau4.StylePriority.UseFont = false;
            this.lblPhanLoaiGiayPhepNhapKhau4.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiGiayPhepNhapKhau4.Text = "XXXXE";
            this.lblPhanLoaiGiayPhepNhapKhau4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel80
            // 
            this.xrLabel80.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(18.49844F, 418.5F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel80.StylePriority.UseFont = false;
            this.xrLabel80.StylePriority.UseTextAlignment = false;
            this.xrLabel80.Text = "4";
            this.xrLabel80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoGiayPhep2
            // 
            this.lblSoGiayPhep2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoGiayPhep2.LocationFloat = new DevExpress.Utils.PointFloat(351.1235F, 409.5F);
            this.lblSoGiayPhep2.Name = "lblSoGiayPhep2";
            this.lblSoGiayPhep2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoGiayPhep2.SizeF = new System.Drawing.SizeF(175F, 9F);
            this.lblSoGiayPhep2.StylePriority.UseFont = false;
            this.lblSoGiayPhep2.StylePriority.UseTextAlignment = false;
            this.lblSoGiayPhep2.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoGiayPhep2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel78
            // 
            this.xrLabel78.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(338.1232F, 409.5F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(13F, 9F);
            this.xrLabel78.StylePriority.UseFont = false;
            this.xrLabel78.StylePriority.UseTextAlignment = false;
            this.xrLabel78.Text = "-";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiGiayPhepNhapKhau2
            // 
            this.lblPhanLoaiGiayPhepNhapKhau2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblPhanLoaiGiayPhepNhapKhau2.LocationFloat = new DevExpress.Utils.PointFloat(288.5496F, 409.5F);
            this.lblPhanLoaiGiayPhepNhapKhau2.Name = "lblPhanLoaiGiayPhepNhapKhau2";
            this.lblPhanLoaiGiayPhepNhapKhau2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiGiayPhepNhapKhau2.SizeF = new System.Drawing.SizeF(49.57F, 9F);
            this.lblPhanLoaiGiayPhepNhapKhau2.StylePriority.UseFont = false;
            this.lblPhanLoaiGiayPhepNhapKhau2.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiGiayPhepNhapKhau2.Text = "XXXXE";
            this.lblPhanLoaiGiayPhepNhapKhau2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel75
            // 
            this.xrLabel75.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(273.5496F, 409.5002F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(14.52F, 9F);
            this.xrLabel75.StylePriority.UseFont = false;
            this.xrLabel75.StylePriority.UseTextAlignment = false;
            this.xrLabel75.Text = "2";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoGiayPhep1
            // 
            this.lblSoGiayPhep1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoGiayPhep1.LocationFloat = new DevExpress.Utils.PointFloat(96.07214F, 409.5002F);
            this.lblSoGiayPhep1.Name = "lblSoGiayPhep1";
            this.lblSoGiayPhep1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoGiayPhep1.SizeF = new System.Drawing.SizeF(175F, 9F);
            this.lblSoGiayPhep1.StylePriority.UseFont = false;
            this.lblSoGiayPhep1.StylePriority.UseTextAlignment = false;
            this.lblSoGiayPhep1.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoGiayPhep1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel73
            // 
            this.xrLabel73.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(83.07217F, 409.5F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(13F, 9F);
            this.xrLabel73.StylePriority.UseFont = false;
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.Text = "-";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiGiayPhepNhapKhau1
            // 
            this.lblPhanLoaiGiayPhepNhapKhau1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblPhanLoaiGiayPhepNhapKhau1.LocationFloat = new DevExpress.Utils.PointFloat(33.49846F, 409.5F);
            this.lblPhanLoaiGiayPhepNhapKhau1.Name = "lblPhanLoaiGiayPhepNhapKhau1";
            this.lblPhanLoaiGiayPhepNhapKhau1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiGiayPhepNhapKhau1.SizeF = new System.Drawing.SizeF(49.57F, 9F);
            this.lblPhanLoaiGiayPhepNhapKhau1.StylePriority.UseFont = false;
            this.lblPhanLoaiGiayPhepNhapKhau1.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiGiayPhepNhapKhau1.Text = "XXXXE";
            this.lblPhanLoaiGiayPhepNhapKhau1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel69
            // 
            this.xrLabel69.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(18.49844F, 409.5F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel69.StylePriority.UseFont = false;
            this.xrLabel69.StylePriority.UseTextAlignment = false;
            this.xrLabel69.Text = "1";
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel62
            // 
            this.xrLabel62.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(7.380814F, 431.5002F);
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(143.65F, 10F);
            this.xrLabel62.StylePriority.UseFont = false;
            this.xrLabel62.StylePriority.UseTextAlignment = false;
            this.xrLabel62.Text = "Mã phân loại khai trị giá";
            this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine9
            // 
            this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 576.4072F);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.SizeF = new System.Drawing.SizeF(767F, 2F);
            // 
            // xrLine8
            // 
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 429.5002F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.SizeF = new System.Drawing.SizeF(767F, 2F);
            // 
            // xrLine7
            // 
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 398.5F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(767F, 2F);
            // 
            // lblMaKetQuaKiemTraNoiDung
            // 
            this.lblMaKetQuaKiemTraNoiDung.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaKetQuaKiemTraNoiDung.LocationFloat = new DevExpress.Utils.PointFloat(171.8115F, 388.6111F);
            this.lblMaKetQuaKiemTraNoiDung.Name = "lblMaKetQuaKiemTraNoiDung";
            this.lblMaKetQuaKiemTraNoiDung.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaKetQuaKiemTraNoiDung.SizeF = new System.Drawing.SizeF(19.77F, 10F);
            this.lblMaKetQuaKiemTraNoiDung.StylePriority.UseFont = false;
            this.lblMaKetQuaKiemTraNoiDung.StylePriority.UseTextAlignment = false;
            this.lblMaKetQuaKiemTraNoiDung.Text = "X";
            this.lblMaKetQuaKiemTraNoiDung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel77
            // 
            this.xrLabel77.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(419.63F, 380.5F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel77.StylePriority.UseFont = false;
            this.xrLabel77.StylePriority.UseTextAlignment = false;
            this.xrLabel77.Text = "-";
            this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiNhapLieu
            // 
            this.lblMaPhanLoaiNhapLieu.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiNhapLieu.LocationFloat = new DevExpress.Utils.PointFloat(434.63F, 380.5F);
            this.lblMaPhanLoaiNhapLieu.Name = "lblMaPhanLoaiNhapLieu";
            this.lblMaPhanLoaiNhapLieu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiNhapLieu.SizeF = new System.Drawing.SizeF(14.24182F, 10.00003F);
            this.lblMaPhanLoaiNhapLieu.StylePriority.UseFont = false;
            this.lblMaPhanLoaiNhapLieu.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiNhapLieu.Text = "X";
            this.lblMaPhanLoaiNhapLieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongHeSoPhanBoTriGia
            // 
            this.lblTongHeSoPhanBoTriGia.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongHeSoPhanBoTriGia.LocationFloat = new DevExpress.Utils.PointFloat(266.2898F, 380.5F);
            this.lblTongHeSoPhanBoTriGia.Name = "lblTongHeSoPhanBoTriGia";
            this.lblTongHeSoPhanBoTriGia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongHeSoPhanBoTriGia.SizeF = new System.Drawing.SizeF(153F, 10F);
            this.lblTongHeSoPhanBoTriGia.StylePriority.UseFont = false;
            this.lblTongHeSoPhanBoTriGia.StylePriority.UseTextAlignment = false;
            this.lblTongHeSoPhanBoTriGia.Text = "12.345.678.901.234.567.890";
            this.lblTongHeSoPhanBoTriGia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongHeSoPhanBoTriGia.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTongTriGiaTinhThue
            // 
            this.lblTongTriGiaTinhThue.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTriGiaTinhThue.LocationFloat = new DevExpress.Utils.PointFloat(295.2162F, 370.6111F);
            this.lblTongTriGiaTinhThue.Name = "lblTongTriGiaTinhThue";
            this.lblTongTriGiaTinhThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaTinhThue.SizeF = new System.Drawing.SizeF(154.35F, 9F);
            this.lblTongTriGiaTinhThue.StylePriority.UseFont = false;
            this.lblTongTriGiaTinhThue.StylePriority.UseTextAlignment = false;
            this.lblTongTriGiaTinhThue.Text = "1.234.567.890.123.456.789";
            this.lblTongTriGiaTinhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongTriGiaTinhThue.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTongTriGiaHoaDon
            // 
            this.lblTongTriGiaHoaDon.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTriGiaHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(295.2162F, 361.6111F);
            this.lblTongTriGiaHoaDon.Name = "lblTongTriGiaHoaDon";
            this.lblTongTriGiaHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaHoaDon.SizeF = new System.Drawing.SizeF(154.35F, 9F);
            this.lblTongTriGiaHoaDon.StylePriority.UseFont = false;
            this.lblTongTriGiaHoaDon.StylePriority.UseTextAlignment = false;
            this.lblTongTriGiaHoaDon.Text = "12.345.678.901.234.567.890";
            this.lblTongTriGiaHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongTriGiaHoaDon.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel72
            // 
            this.xrLabel72.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(280.2162F, 361.6111F);
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel72.StylePriority.UseFont = false;
            this.xrLabel72.StylePriority.UseTextAlignment = false;
            this.xrLabel72.Text = "-";
            this.xrLabel72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel71
            // 
            this.xrLabel71.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(234.6129F, 361.6111F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(14.99998F, 9F);
            this.xrLabel71.StylePriority.UseFont = false;
            this.xrLabel71.StylePriority.UseTextAlignment = false;
            this.xrLabel71.Text = "-";
            this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDongTienHoaDon
            // 
            this.lblMaDongTienHoaDon.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDongTienHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(250.5461F, 361.6111F);
            this.lblMaDongTienHoaDon.Name = "lblMaDongTienHoaDon";
            this.lblMaDongTienHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDongTienHoaDon.SizeF = new System.Drawing.SizeF(29.67001F, 9F);
            this.lblMaDongTienHoaDon.StylePriority.UseFont = false;
            this.lblMaDongTienHoaDon.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienHoaDon.Text = "NNE";
            this.lblMaDongTienHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblMaDongTienHoaDon.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDieuKienGiaHoaDon
            // 
            this.lblMaDieuKienGiaHoaDon.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDieuKienGiaHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(204.9414F, 361.6111F);
            this.lblMaDieuKienGiaHoaDon.Name = "lblMaDieuKienGiaHoaDon";
            this.lblMaDieuKienGiaHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDieuKienGiaHoaDon.SizeF = new System.Drawing.SizeF(29.66998F, 9F);
            this.lblMaDieuKienGiaHoaDon.StylePriority.UseFont = false;
            this.lblMaDieuKienGiaHoaDon.StylePriority.UseTextAlignment = false;
            this.lblMaDieuKienGiaHoaDon.Text = "NNE";
            this.lblMaDieuKienGiaHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblMaDieuKienGiaHoaDon.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel68
            // 
            this.xrLabel68.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(189.9414F, 361.6111F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel68.StylePriority.UseFont = false;
            this.xrLabel68.StylePriority.UseTextAlignment = false;
            this.xrLabel68.Text = "-";
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiGiaHoaDon
            // 
            this.lblMaPhanLoaiGiaHoaDon.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiGiaHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(170.4781F, 361.6111F);
            this.lblMaPhanLoaiGiaHoaDon.Name = "lblMaPhanLoaiGiaHoaDon";
            this.lblMaPhanLoaiGiaHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiGiaHoaDon.SizeF = new System.Drawing.SizeF(18.99998F, 9F);
            this.lblMaPhanLoaiGiaHoaDon.StylePriority.UseFont = false;
            this.lblMaPhanLoaiGiaHoaDon.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiGiaHoaDon.Text = "X";
            this.lblMaPhanLoaiGiaHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhuongThucThanhToan
            // 
            this.lblPhuongThucThanhToan.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblPhuongThucThanhToan.LocationFloat = new DevExpress.Utils.PointFloat(170.4781F, 352.6111F);
            this.lblPhuongThucThanhToan.Name = "lblPhuongThucThanhToan";
            this.lblPhuongThucThanhToan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhuongThucThanhToan.SizeF = new System.Drawing.SizeF(99.66F, 9F);
            this.lblPhuongThucThanhToan.StylePriority.UseFont = false;
            this.lblPhuongThucThanhToan.StylePriority.UseTextAlignment = false;
            this.lblPhuongThucThanhToan.Text = "XXXXXXE";
            this.lblPhuongThucThanhToan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayPhatHanh
            // 
            this.lblNgayPhatHanh.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblNgayPhatHanh.LocationFloat = new DevExpress.Utils.PointFloat(170.4781F, 343.6111F);
            this.lblNgayPhatHanh.Name = "lblNgayPhatHanh";
            this.lblNgayPhatHanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayPhatHanh.SizeF = new System.Drawing.SizeF(99.66F, 9F);
            this.lblNgayPhatHanh.StylePriority.UseFont = false;
            this.lblNgayPhatHanh.StylePriority.UseTextAlignment = false;
            this.lblNgayPhatHanh.Text = "dd/MM/yyyy";
            this.lblNgayPhatHanh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTiepNhanHoaDonDienTu
            // 
            this.lblSoTiepNhanHoaDonDienTu.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoTiepNhanHoaDonDienTu.LocationFloat = new DevExpress.Utils.PointFloat(170.4781F, 335.6111F);
            this.lblSoTiepNhanHoaDonDienTu.Name = "lblSoTiepNhanHoaDonDienTu";
            this.lblSoTiepNhanHoaDonDienTu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTiepNhanHoaDonDienTu.SizeF = new System.Drawing.SizeF(99.66F, 9F);
            this.lblSoTiepNhanHoaDonDienTu.StylePriority.UseFont = false;
            this.lblSoTiepNhanHoaDonDienTu.StylePriority.UseTextAlignment = false;
            this.lblSoTiepNhanHoaDonDienTu.Text = "NNNNNNNNN1NE";
            this.lblSoTiepNhanHoaDonDienTu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoTiepNhanHoaDonDienTu.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblSoHoaDon
            // 
            this.lblSoHoaDon.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(206.0982F, 325.6111F);
            this.lblSoHoaDon.Name = "lblSoHoaDon";
            this.lblSoHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoHoaDon.SizeF = new System.Drawing.SizeF(294F, 9F);
            this.lblSoHoaDon.StylePriority.UseFont = false;
            this.lblSoHoaDon.StylePriority.UseTextAlignment = false;
            this.lblSoHoaDon.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel67
            // 
            this.xrLabel67.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(191.0982F, 325.6111F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(15F, 9F);
            this.xrLabel67.StylePriority.UseFont = false;
            this.xrLabel67.StylePriority.UseTextAlignment = false;
            this.xrLabel67.Text = "-";
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiHinhThucHoaDon
            // 
            this.lblPhanLoaiHinhThucHoaDon.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblPhanLoaiHinhThucHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(171.3264F, 325.6111F);
            this.lblPhanLoaiHinhThucHoaDon.Name = "lblPhanLoaiHinhThucHoaDon";
            this.lblPhanLoaiHinhThucHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiHinhThucHoaDon.SizeF = new System.Drawing.SizeF(19.77F, 9F);
            this.lblPhanLoaiHinhThucHoaDon.StylePriority.UseFont = false;
            this.lblPhanLoaiHinhThucHoaDon.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiHinhThucHoaDon.Text = "X";
            this.lblPhanLoaiHinhThucHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel61
            // 
            this.xrLabel61.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(8.793195F, 388.6111F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(160.88F, 9F);
            this.xrLabel61.StylePriority.UseFont = false;
            this.xrLabel61.StylePriority.UseTextAlignment = false;
            this.xrLabel61.Text = "Mã kết quả kiểm tra nội dung";
            this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel60
            // 
            this.xrLabel60.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(8.793195F, 379.6111F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(160.88F, 9F);
            this.xrLabel60.StylePriority.UseFont = false;
            this.xrLabel60.StylePriority.UseTextAlignment = false;
            this.xrLabel60.Text = "Tổng hệ số phân bổ trị giá";
            this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel59
            // 
            this.xrLabel59.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(8.793195F, 370.6111F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(160.88F, 9F);
            this.xrLabel59.StylePriority.UseFont = false;
            this.xrLabel59.StylePriority.UseTextAlignment = false;
            this.xrLabel59.Text = "Tổng giá trị tính thuế";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel58
            // 
            this.xrLabel58.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(8.793195F, 361.6111F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(160.88F, 9F);
            this.xrLabel58.StylePriority.UseFont = false;
            this.xrLabel58.StylePriority.UseTextAlignment = false;
            this.xrLabel58.Text = "Tổng giá trị hóa đơn";
            this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(8.793195F, 352.6111F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(160.88F, 9F);
            this.xrLabel57.StylePriority.UseFont = false;
            this.xrLabel57.StylePriority.UseTextAlignment = false;
            this.xrLabel57.Text = "Phương thức thanh toán";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(8.793195F, 343.6111F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(160.88F, 9F);
            this.xrLabel52.StylePriority.UseFont = false;
            this.xrLabel52.StylePriority.UsePadding = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "Ngày phát hành";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(8.793195F, 334.6111F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(160.88F, 9F);
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.Text = "Số tiếp nhân chứng từ điện tử";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(8.793195F, 325.6111F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(86.47F, 9F);
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "Số hóa đơn";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine6
            // 
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(0.31F, 325F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(767F, 2F);
            // 
            // lblMaVanBanPhapQuyKhac4
            // 
            this.lblMaVanBanPhapQuyKhac4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaVanBanPhapQuyKhac4.LocationFloat = new DevExpress.Utils.PointFloat(611.8975F, 314.5F);
            this.lblMaVanBanPhapQuyKhac4.Name = "lblMaVanBanPhapQuyKhac4";
            this.lblMaVanBanPhapQuyKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaVanBanPhapQuyKhac4.SizeF = new System.Drawing.SizeF(30.6F, 10F);
            this.lblMaVanBanPhapQuyKhac4.StylePriority.UseFont = false;
            this.lblMaVanBanPhapQuyKhac4.StylePriority.UseTextAlignment = false;
            this.lblMaVanBanPhapQuyKhac4.Text = "XE";
            this.lblMaVanBanPhapQuyKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaVanBanPhapQuyKhac3
            // 
            this.lblMaVanBanPhapQuyKhac3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaVanBanPhapQuyKhac3.LocationFloat = new DevExpress.Utils.PointFloat(581.2976F, 314.5F);
            this.lblMaVanBanPhapQuyKhac3.Name = "lblMaVanBanPhapQuyKhac3";
            this.lblMaVanBanPhapQuyKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaVanBanPhapQuyKhac3.SizeF = new System.Drawing.SizeF(30.6F, 10F);
            this.lblMaVanBanPhapQuyKhac3.StylePriority.UseFont = false;
            this.lblMaVanBanPhapQuyKhac3.StylePriority.UseTextAlignment = false;
            this.lblMaVanBanPhapQuyKhac3.Text = "XE";
            this.lblMaVanBanPhapQuyKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaVanBanPhapQuyKhac2
            // 
            this.lblMaVanBanPhapQuyKhac2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaVanBanPhapQuyKhac2.LocationFloat = new DevExpress.Utils.PointFloat(550.6976F, 314.5F);
            this.lblMaVanBanPhapQuyKhac2.Name = "lblMaVanBanPhapQuyKhac2";
            this.lblMaVanBanPhapQuyKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaVanBanPhapQuyKhac2.SizeF = new System.Drawing.SizeF(30.6F, 10F);
            this.lblMaVanBanPhapQuyKhac2.StylePriority.UseFont = false;
            this.lblMaVanBanPhapQuyKhac2.StylePriority.UseTextAlignment = false;
            this.lblMaVanBanPhapQuyKhac2.Text = "XE";
            this.lblMaVanBanPhapQuyKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaVanBanPhapQuyKhac1
            // 
            this.lblMaVanBanPhapQuyKhac1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaVanBanPhapQuyKhac1.LocationFloat = new DevExpress.Utils.PointFloat(520.0976F, 314.5F);
            this.lblMaVanBanPhapQuyKhac1.Name = "lblMaVanBanPhapQuyKhac1";
            this.lblMaVanBanPhapQuyKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaVanBanPhapQuyKhac1.SizeF = new System.Drawing.SizeF(30.6F, 10F);
            this.lblMaVanBanPhapQuyKhac1.StylePriority.UseFont = false;
            this.lblMaVanBanPhapQuyKhac1.StylePriority.UseTextAlignment = false;
            this.lblMaVanBanPhapQuyKhac1.Text = "XE";
            this.lblMaVanBanPhapQuyKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenPhuongTienVanChuyen
            // 
            this.lblTenPhuongTienVanChuyen.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenPhuongTienVanChuyen.LocationFloat = new DevExpress.Utils.PointFloat(478.5027F, 262.8001F);
            this.lblTenPhuongTienVanChuyen.Name = "lblTenPhuongTienVanChuyen";
            this.lblTenPhuongTienVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenPhuongTienVanChuyen.SizeF = new System.Drawing.SizeF(283.4973F, 10.00003F);
            this.lblTenPhuongTienVanChuyen.StylePriority.UseFont = false;
            this.lblTenPhuongTienVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblTenPhuongTienVanChuyen.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTenPhuongTienVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhuongTienVanChuyen
            // 
            this.lblMaPhuongTienVanChuyen.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhuongTienVanChuyen.LocationFloat = new DevExpress.Utils.PointFloat(408.6127F, 262.8001F);
            this.lblMaPhuongTienVanChuyen.Name = "lblMaPhuongTienVanChuyen";
            this.lblMaPhuongTienVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhuongTienVanChuyen.SizeF = new System.Drawing.SizeF(69.89F, 10F);
            this.lblMaPhuongTienVanChuyen.StylePriority.UseFont = false;
            this.lblMaPhuongTienVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblMaPhuongTienVanChuyen.Text = "XXXXXXXXE";
            this.lblMaPhuongTienVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayDuocPhepNhapKho
            // 
            this.lblNgayDuocPhepNhapKho.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblNgayDuocPhepNhapKho.LocationFloat = new DevExpress.Utils.PointFloat(518.7067F, 304.5F);
            this.lblNgayDuocPhepNhapKho.Name = "lblNgayDuocPhepNhapKho";
            this.lblNgayDuocPhepNhapKho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDuocPhepNhapKho.SizeF = new System.Drawing.SizeF(77F, 10F);
            this.lblNgayDuocPhepNhapKho.StylePriority.UseFont = false;
            this.lblNgayDuocPhepNhapKho.StylePriority.UseTextAlignment = false;
            this.lblNgayDuocPhepNhapKho.Text = "dd/MM/yyyy";
            this.lblNgayDuocPhepNhapKho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblKyHieuSoHieu
            // 
            this.lblKyHieuSoHieu.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblKyHieuSoHieu.LocationFloat = new DevExpress.Utils.PointFloat(412.5028F, 282.8F);
            this.lblKyHieuSoHieu.Multiline = true;
            this.lblKyHieuSoHieu.Name = "lblKyHieuSoHieu";
            this.lblKyHieuSoHieu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblKyHieuSoHieu.SizeF = new System.Drawing.SizeF(350.5F, 33F);
            this.lblKyHieuSoHieu.StylePriority.UseFont = false;
            this.lblKyHieuSoHieu.StylePriority.UseTextAlignment = false;
            this.lblKyHieuSoHieu.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXXE";
            this.lblKyHieuSoHieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayHangDen
            // 
            this.lblNgayHangDen.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblNgayHangDen.LocationFloat = new DevExpress.Utils.PointFloat(460.8367F, 272.8F);
            this.lblNgayHangDen.Name = "lblNgayHangDen";
            this.lblNgayHangDen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHangDen.SizeF = new System.Drawing.SizeF(77F, 10F);
            this.lblNgayHangDen.StylePriority.UseFont = false;
            this.lblNgayHangDen.StylePriority.UseTextAlignment = false;
            this.lblNgayHangDen.Text = "dd/MM/yyyy";
            this.lblNgayHangDen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel66
            // 
            this.xrLabel66.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(312.69F, 314.5F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(206.02F, 10F);
            this.xrLabel66.StylePriority.UseFont = false;
            this.xrLabel66.StylePriority.UseTextAlignment = false;
            this.xrLabel66.Text = "Mã văn bản pháp quy khác";
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel65
            // 
            this.xrLabel65.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(312.69F, 304.5F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(206.02F, 10F);
            this.xrLabel65.StylePriority.UseFont = false;
            this.xrLabel65.StylePriority.UseTextAlignment = false;
            this.xrLabel65.Text = "Ngày được phép nhập kho đầu tiên";
            this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(312.55F, 283.8001F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(99F, 10F);
            this.xrLabel64.StylePriority.UseFont = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "Ký hiệu và số hiệu";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel63
            // 
            this.xrLabel63.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(312.6867F, 272.8F);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(148.15F, 10F);
            this.xrLabel63.StylePriority.UseFont = false;
            this.xrLabel63.StylePriority.UseTextAlignment = false;
            this.xrLabel63.Text = "Ngày hàng đến";
            this.xrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenDiaDiemXepHang
            // 
            this.lblTenDiaDiemXepHang.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenDiaDiemXepHang.LocationFloat = new DevExpress.Utils.PointFloat(478.5028F, 242.7997F);
            this.lblTenDiaDiemXepHang.Name = "lblTenDiaDiemXepHang";
            this.lblTenDiaDiemXepHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDiaDiemXepHang.SizeF = new System.Drawing.SizeF(284.5F, 10F);
            this.lblTenDiaDiemXepHang.StylePriority.UseFont = false;
            this.lblTenDiaDiemXepHang.StylePriority.UseTextAlignment = false;
            this.lblTenDiaDiemXepHang.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTenDiaDiemXepHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenDiaDiemDoHang
            // 
            this.lblTenDiaDiemDoHang.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenDiaDiemDoHang.LocationFloat = new DevExpress.Utils.PointFloat(478.5027F, 231.7997F);
            this.lblTenDiaDiemDoHang.Name = "lblTenDiaDiemDoHang";
            this.lblTenDiaDiemDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDiaDiemDoHang.SizeF = new System.Drawing.SizeF(283.4973F, 10F);
            this.lblTenDiaDiemDoHang.StylePriority.UseFont = false;
            this.lblTenDiaDiemDoHang.StylePriority.UseTextAlignment = false;
            this.lblTenDiaDiemDoHang.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTenDiaDiemDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDiaDiemXepHang
            // 
            this.lblMaDiaDiemXepHang.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDiaDiemXepHang.LocationFloat = new DevExpress.Utils.PointFloat(416.5028F, 242.7997F);
            this.lblMaDiaDiemXepHang.Name = "lblMaDiaDiemXepHang";
            this.lblMaDiaDiemXepHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDiaDiemXepHang.SizeF = new System.Drawing.SizeF(62F, 10F);
            this.lblMaDiaDiemXepHang.StylePriority.UseFont = false;
            this.lblMaDiaDiemXepHang.StylePriority.UseTextAlignment = false;
            this.lblMaDiaDiemXepHang.Text = "XXXXE";
            this.lblMaDiaDiemXepHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDiaDiemDoHang
            // 
            this.lblMaDiaDiemDoHang.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDiaDiemDoHang.LocationFloat = new DevExpress.Utils.PointFloat(412.5028F, 231.8F);
            this.lblMaDiaDiemDoHang.Name = "lblMaDiaDiemDoHang";
            this.lblMaDiaDiemDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDiaDiemDoHang.SizeF = new System.Drawing.SizeF(65.99994F, 10F);
            this.lblMaDiaDiemDoHang.StylePriority.UseFont = false;
            this.lblMaDiaDiemDoHang.StylePriority.UseTextAlignment = false;
            this.lblMaDiaDiemDoHang.Text = "XXXXXE";
            this.lblMaDiaDiemDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenDiaDiemLuuKhoHang
            // 
            this.lblTenDiaDiemLuuKhoHang.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenDiaDiemLuuKhoHang.LocationFloat = new DevExpress.Utils.PointFloat(478.5027F, 221.7998F);
            this.lblTenDiaDiemLuuKhoHang.Name = "lblTenDiaDiemLuuKhoHang";
            this.lblTenDiaDiemLuuKhoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDiaDiemLuuKhoHang.SizeF = new System.Drawing.SizeF(185.36F, 10F);
            this.lblTenDiaDiemLuuKhoHang.StylePriority.UseFont = false;
            this.lblTenDiaDiemLuuKhoHang.StylePriority.UseTextAlignment = false;
            this.lblTenDiaDiemLuuKhoHang.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblTenDiaDiemLuuKhoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDiaDiemLuuKhoHang
            // 
            this.lblMaDiaDiemLuuKhoHang.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDiaDiemLuuKhoHang.LocationFloat = new DevExpress.Utils.PointFloat(412.5028F, 221.8F);
            this.lblMaDiaDiemLuuKhoHang.Name = "lblMaDiaDiemLuuKhoHang";
            this.lblMaDiaDiemLuuKhoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDiaDiemLuuKhoHang.SizeF = new System.Drawing.SizeF(66F, 10F);
            this.lblMaDiaDiemLuuKhoHang.StylePriority.UseFont = false;
            this.lblMaDiaDiemLuuKhoHang.StylePriority.UseTextAlignment = false;
            this.lblMaDiaDiemLuuKhoHang.Text = "XXXXXXE";
            this.lblMaDiaDiemLuuKhoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(312.55F, 252.8001F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(141.85F, 10F);
            this.xrLabel56.StylePriority.UseFont = false;
            this.xrLabel56.StylePriority.UseTextAlignment = false;
            this.xrLabel56.Text = "Phương tiện vận chuyển";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(312.55F, 242.8001F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(103F, 10F);
            this.xrLabel55.StylePriority.UseFont = false;
            this.xrLabel55.StylePriority.UseTextAlignment = false;
            this.xrLabel55.Text = "Địa điểm xếp hàng";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(312.69F, 231.7997F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(95F, 10F);
            this.xrLabel54.StylePriority.UseFont = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = "Địa điểm dỡ hàng";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(312.55F, 221.7998F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(95F, 10F);
            this.xrLabel53.StylePriority.UseFont = false;
            this.xrLabel53.StylePriority.UseTextAlignment = false;
            this.xrLabel53.Text = "Địa điểm lưu kho";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine5
            // 
            this.xrLine5.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(310.5499F, 221.7998F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(2F, 153.2898F);
            // 
            // lblSoLuongContainer
            // 
            this.lblSoLuongContainer.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoLuongContainer.LocationFloat = new DevExpress.Utils.PointFloat(253.18F, 303.8F);
            this.lblSoLuongContainer.Name = "lblSoLuongContainer";
            this.lblSoLuongContainer.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuongContainer.SizeF = new System.Drawing.SizeF(27.38F, 10F);
            this.lblSoLuongContainer.StylePriority.UseFont = false;
            this.lblSoLuongContainer.StylePriority.UseTextAlignment = false;
            this.lblSoLuongContainer.Text = "NNE";
            this.lblSoLuongContainer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoLuongContainer.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDonViTinhTrongLuong
            // 
            this.lblMaDonViTinhTrongLuong.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDonViTinhTrongLuong.LocationFloat = new DevExpress.Utils.PointFloat(280.55F, 293.8F);
            this.lblMaDonViTinhTrongLuong.Name = "lblMaDonViTinhTrongLuong";
            this.lblMaDonViTinhTrongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinhTrongLuong.SizeF = new System.Drawing.SizeF(30F, 10F);
            this.lblMaDonViTinhTrongLuong.StylePriority.UseFont = false;
            this.lblMaDonViTinhTrongLuong.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinhTrongLuong.Text = "XXE";
            this.lblMaDonViTinhTrongLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongTrongLuongHang
            // 
            this.lblTongTrongLuongHang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTrongLuongHang.LocationFloat = new DevExpress.Utils.PointFloat(176.57F, 293.8F);
            this.lblTongTrongLuongHang.Name = "lblTongTrongLuongHang";
            this.lblTongTrongLuongHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTrongLuongHang.SizeF = new System.Drawing.SizeF(103.5049F, 10.00003F);
            this.lblTongTrongLuongHang.StylePriority.UseFont = false;
            this.lblTongTrongLuongHang.StylePriority.UseTextAlignment = false;
            this.lblTongTrongLuongHang.Text = "1.234.567.890";
            this.lblTongTrongLuongHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongTrongLuongHang.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDonViTinh
            // 
            this.lblMaDonViTinh.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDonViTinh.LocationFloat = new DevExpress.Utils.PointFloat(280.5529F, 283.7998F);
            this.lblMaDonViTinh.Name = "lblMaDonViTinh";
            this.lblMaDonViTinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinh.SizeF = new System.Drawing.SizeF(29.99704F, 10.00003F);
            this.lblMaDonViTinh.StylePriority.UseFont = false;
            this.lblMaDonViTinh.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinh.Text = "XXE";
            this.lblMaDonViTinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoLuong
            // 
            this.lblSoLuong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoLuong.LocationFloat = new DevExpress.Utils.PointFloat(176.565F, 283.8001F);
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuong.SizeF = new System.Drawing.SizeF(103.51F, 10F);
            this.lblSoLuong.StylePriority.UseFont = false;
            this.lblSoLuong.StylePriority.UseTextAlignment = false;
            this.lblSoLuong.Text = "12.345.678";
            this.lblSoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuong.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel47
            // 
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(9.287783F, 303.8001F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(160.834F, 10.00003F);
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.StylePriority.UseTextAlignment = false;
            this.xrLabel47.Text = "Số lượng container";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(9.287783F, 293.8002F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(161.8291F, 9.999969F);
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "Tổng trọng lượng hàng (Gross)";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(9.070062F, 283.8001F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(86.47F, 10F);
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.Text = "Số lượng";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoVanDon5
            // 
            this.lblSoVanDon5.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoVanDon5.LocationFloat = new DevExpress.Utils.PointFloat(39.03133F, 273.8001F);
            this.lblSoVanDon5.Name = "lblSoVanDon5";
            this.lblSoVanDon5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoVanDon5.SizeF = new System.Drawing.SizeF(261.48F, 10F);
            this.lblSoVanDon5.StylePriority.UseFont = false;
            this.lblSoVanDon5.StylePriority.UseTextAlignment = false;
            this.lblSoVanDon5.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoVanDon5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoVanDon4
            // 
            this.lblSoVanDon4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoVanDon4.LocationFloat = new DevExpress.Utils.PointFloat(39.03131F, 263.8F);
            this.lblSoVanDon4.Name = "lblSoVanDon4";
            this.lblSoVanDon4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoVanDon4.SizeF = new System.Drawing.SizeF(261.48F, 10F);
            this.lblSoVanDon4.StylePriority.UseFont = false;
            this.lblSoVanDon4.StylePriority.UseTextAlignment = false;
            this.lblSoVanDon4.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoVanDon4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoVanDon3
            // 
            this.lblSoVanDon3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoVanDon3.LocationFloat = new DevExpress.Utils.PointFloat(39.03131F, 253.8001F);
            this.lblSoVanDon3.Name = "lblSoVanDon3";
            this.lblSoVanDon3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoVanDon3.SizeF = new System.Drawing.SizeF(261.48F, 10F);
            this.lblSoVanDon3.StylePriority.UseFont = false;
            this.lblSoVanDon3.StylePriority.UseTextAlignment = false;
            this.lblSoVanDon3.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoVanDon3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoVanDon2
            // 
            this.lblSoVanDon2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoVanDon2.LocationFloat = new DevExpress.Utils.PointFloat(39.03132F, 243.8F);
            this.lblSoVanDon2.Name = "lblSoVanDon2";
            this.lblSoVanDon2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoVanDon2.SizeF = new System.Drawing.SizeF(261.48F, 10F);
            this.lblSoVanDon2.StylePriority.UseFont = false;
            this.lblSoVanDon2.StylePriority.UseTextAlignment = false;
            this.lblSoVanDon2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoVanDon2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoVanDon1
            // 
            this.lblSoVanDon1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoVanDon1.LocationFloat = new DevExpress.Utils.PointFloat(39.03131F, 231.8F);
            this.lblSoVanDon1.Name = "lblSoVanDon1";
            this.lblSoVanDon1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoVanDon1.SizeF = new System.Drawing.SizeF(261.48F, 12.00006F);
            this.lblSoVanDon1.StylePriority.UseFont = false;
            this.lblSoVanDon1.StylePriority.UseTextAlignment = false;
            this.lblSoVanDon1.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoVanDon1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(23.0302F, 273.8001F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "5";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(23.03021F, 263.8F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "4";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(23.03021F, 253.8F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "3";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(23.03021F, 243.8F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "2";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(23.03022F, 231.8F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(15.00001F, 12.00006F);
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "1";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(9.999867F, 221.8F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(86.47F, 10F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "Số vận đơn";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine4
            // 
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 219.8F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(767F, 2F);
            // 
            // lblMaNhanVienHaiQuan
            // 
            this.lblMaNhanVienHaiQuan.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaNhanVienHaiQuan.LocationFloat = new DevExpress.Utils.PointFloat(703.8687F, 209.8F);
            this.lblMaNhanVienHaiQuan.Name = "lblMaNhanVienHaiQuan";
            this.lblMaNhanVienHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNhanVienHaiQuan.SizeF = new System.Drawing.SizeF(51.59F, 10F);
            this.lblMaNhanVienHaiQuan.StylePriority.UseFont = false;
            this.lblMaNhanVienHaiQuan.StylePriority.UseTextAlignment = false;
            this.lblMaNhanVienHaiQuan.Text = "XXXXE";
            this.lblMaNhanVienHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(581.0186F, 209.8F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(122F, 10F);
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "Mã nhân viên Hải quan";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenDaiLyHaiQuan
            // 
            this.lblTenDaiLyHaiQuan.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenDaiLyHaiQuan.LocationFloat = new DevExpress.Utils.PointFloat(156.8117F, 209.8F);
            this.lblTenDaiLyHaiQuan.Name = "lblTenDaiLyHaiQuan";
            this.lblTenDaiLyHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDaiLyHaiQuan.SizeF = new System.Drawing.SizeF(422.3367F, 10F);
            this.lblTenDaiLyHaiQuan.StylePriority.UseFont = false;
            this.lblTenDaiLyHaiQuan.StylePriority.UseTextAlignment = false;
            this.lblTenDaiLyHaiQuan.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXXE";
            this.lblTenDaiLyHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDaiLyHaiQuan
            // 
            this.lblMaDaiLyHaiQuan.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaDaiLyHaiQuan.LocationFloat = new DevExpress.Utils.PointFloat(102.657F, 209.8F);
            this.lblMaDaiLyHaiQuan.Name = "lblMaDaiLyHaiQuan";
            this.lblMaDaiLyHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDaiLyHaiQuan.SizeF = new System.Drawing.SizeF(52.04629F, 10F);
            this.lblMaDaiLyHaiQuan.StylePriority.UseFont = false;
            this.lblMaDaiLyHaiQuan.StylePriority.UseTextAlignment = false;
            this.lblMaDaiLyHaiQuan.Text = "XXXXE";
            this.lblMaDaiLyHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(21.9334F, 187.7F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(80F, 10F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "Mã nước";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(9.999867F, 209.8F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(92.06844F, 10F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "Đại lý Hải quan";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 207.8F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(767F, 2F);
            // 
            // lblNguoiUyThacXuatKhau
            // 
            this.lblNguoiUyThacXuatKhau.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblNguoiUyThacXuatKhau.LocationFloat = new DevExpress.Utils.PointFloat(140.0499F, 197.7F);
            this.lblNguoiUyThacXuatKhau.Multiline = true;
            this.lblNguoiUyThacXuatKhau.Name = "lblNguoiUyThacXuatKhau";
            this.lblNguoiUyThacXuatKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoiUyThacXuatKhau.SizeF = new System.Drawing.SizeF(602.95F, 10F);
            this.lblNguoiUyThacXuatKhau.StylePriority.UseFont = false;
            this.lblNguoiUyThacXuatKhau.StylePriority.UseTextAlignment = false;
            this.lblNguoiUyThacXuatKhau.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblNguoiUyThacXuatKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaChiNguoiXuatKhau4
            // 
            this.lblDiaChiNguoiXuatKhau4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblDiaChiNguoiXuatKhau4.LocationFloat = new DevExpress.Utils.PointFloat(426.4936F, 177.7F);
            this.lblDiaChiNguoiXuatKhau4.Name = "lblDiaChiNguoiXuatKhau4";
            this.lblDiaChiNguoiXuatKhau4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaChiNguoiXuatKhau4.SizeF = new System.Drawing.SizeF(301.1364F, 10F);
            this.lblDiaChiNguoiXuatKhau4.StylePriority.UseFont = false;
            this.lblDiaChiNguoiXuatKhau4.StylePriority.UseTextAlignment = false;
            this.lblDiaChiNguoiXuatKhau4.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblDiaChiNguoiXuatKhau4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaChiNguoiXuatKhau2
            // 
            this.lblDiaChiNguoiXuatKhau2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblDiaChiNguoiXuatKhau2.LocationFloat = new DevExpress.Utils.PointFloat(426.4937F, 167.7F);
            this.lblDiaChiNguoiXuatKhau2.Name = "lblDiaChiNguoiXuatKhau2";
            this.lblDiaChiNguoiXuatKhau2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaChiNguoiXuatKhau2.SizeF = new System.Drawing.SizeF(301.1348F, 10F);
            this.lblDiaChiNguoiXuatKhau2.StylePriority.UseFont = false;
            this.lblDiaChiNguoiXuatKhau2.StylePriority.UseTextAlignment = false;
            this.lblDiaChiNguoiXuatKhau2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblDiaChiNguoiXuatKhau2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaChiNguoiXuatKhau3
            // 
            this.lblDiaChiNguoiXuatKhau3.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblDiaChiNguoiXuatKhau3.LocationFloat = new DevExpress.Utils.PointFloat(101.6724F, 177.7F);
            this.lblDiaChiNguoiXuatKhau3.Name = "lblDiaChiNguoiXuatKhau3";
            this.lblDiaChiNguoiXuatKhau3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaChiNguoiXuatKhau3.SizeF = new System.Drawing.SizeF(320F, 10F);
            this.lblDiaChiNguoiXuatKhau3.StylePriority.UseFont = false;
            this.lblDiaChiNguoiXuatKhau3.StylePriority.UseTextAlignment = false;
            this.lblDiaChiNguoiXuatKhau3.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblDiaChiNguoiXuatKhau3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaChiNguoiXuatKhau1
            // 
            this.lblDiaChiNguoiXuatKhau1.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblDiaChiNguoiXuatKhau1.LocationFloat = new DevExpress.Utils.PointFloat(102.0683F, 167.7F);
            this.lblDiaChiNguoiXuatKhau1.Name = "lblDiaChiNguoiXuatKhau1";
            this.lblDiaChiNguoiXuatKhau1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaChiNguoiXuatKhau1.SizeF = new System.Drawing.SizeF(320F, 10F);
            this.lblDiaChiNguoiXuatKhau1.StylePriority.UseFont = false;
            this.lblDiaChiNguoiXuatKhau1.StylePriority.UseTextAlignment = false;
            this.lblDiaChiNguoiXuatKhau1.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblDiaChiNguoiXuatKhau1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenNguoiXuatKhau
            // 
            this.lblTenNguoiXuatKhau.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenNguoiXuatKhau.LocationFloat = new DevExpress.Utils.PointFloat(102.0784F, 147.7F);
            this.lblTenNguoiXuatKhau.Multiline = true;
            this.lblTenNguoiXuatKhau.Name = "lblTenNguoiXuatKhau";
            this.lblTenNguoiXuatKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNguoiXuatKhau.SizeF = new System.Drawing.SizeF(625.55F, 10F);
            this.lblTenNguoiXuatKhau.StylePriority.UseFont = false;
            this.lblTenNguoiXuatKhau.StylePriority.UseTextAlignment = false;
            this.lblTenNguoiXuatKhau.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblTenNguoiXuatKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaBuuChinhXuatKhau
            // 
            this.lblMaBuuChinhXuatKhau.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaBuuChinhXuatKhau.LocationFloat = new DevExpress.Utils.PointFloat(102.0683F, 157.7F);
            this.lblMaBuuChinhXuatKhau.Name = "lblMaBuuChinhXuatKhau";
            this.lblMaBuuChinhXuatKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaBuuChinhXuatKhau.SizeF = new System.Drawing.SizeF(70.43F, 10F);
            this.lblMaBuuChinhXuatKhau.StylePriority.UseFont = false;
            this.lblMaBuuChinhXuatKhau.StylePriority.UseTextAlignment = false;
            this.lblMaBuuChinhXuatKhau.Text = "XXXXXXE";
            this.lblMaBuuChinhXuatKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaNguoiXuatKhau
            // 
            this.lblMaNguoiXuatKhau.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaNguoiXuatKhau.LocationFloat = new DevExpress.Utils.PointFloat(102.657F, 137.7F);
            this.lblMaNguoiXuatKhau.Name = "lblMaNguoiXuatKhau";
            this.lblMaNguoiXuatKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNguoiXuatKhau.SizeF = new System.Drawing.SizeF(128.16F, 10F);
            this.lblMaNguoiXuatKhau.StylePriority.UseFont = false;
            this.lblMaNguoiXuatKhau.StylePriority.UseTextAlignment = false;
            this.lblMaNguoiXuatKhau.Text = "XXXXXXXXX1-XXE";
            this.lblMaNguoiXuatKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaNuoc
            // 
            this.lblMaNuoc.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaNuoc.LocationFloat = new DevExpress.Utils.PointFloat(102.7779F, 187.7F);
            this.lblMaNuoc.Name = "lblMaNuoc";
            this.lblMaNuoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNuoc.SizeF = new System.Drawing.SizeF(86.70018F, 9.999985F);
            this.lblMaNuoc.StylePriority.UseFont = false;
            this.lblMaNuoc.StylePriority.UseTextAlignment = false;
            this.lblMaNuoc.Text = "XXXXXXE";
            this.lblMaNuoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(21.24375F, 147.7F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(80F, 10F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "Tên";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(21.24375F, 157.7F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(80F, 10F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "Mã bưu chính";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(10F, 127.7F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(92.77802F, 10.00002F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "Người xuất khẩu";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(21.647F, 137.7F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(80.01F, 10F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Mã";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(10.0599F, 197.7F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(129.99F, 10F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "Người ủy thác xuất khẩu";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(21.24375F, 167.7F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(80F, 10F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Địa chỉ";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0.84F, 125.7F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(767F, 2F);
            // 
            // lblMaNguoiUyThacNhapKhau
            // 
            this.lblMaNguoiUyThacNhapKhau.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaNguoiUyThacNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(100.9678F, 93.6216F);
            this.lblMaNguoiUyThacNhapKhau.Name = "lblMaNguoiUyThacNhapKhau";
            this.lblMaNguoiUyThacNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNguoiUyThacNhapKhau.SizeF = new System.Drawing.SizeF(131.26F, 10F);
            this.lblMaNguoiUyThacNhapKhau.StylePriority.UseFont = false;
            this.lblMaNguoiUyThacNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblMaNguoiUyThacNhapKhau.Text = "XXXXXXXXX1-XXE";
            this.lblMaNguoiUyThacNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoDienThoaiNguoiNhapKhau
            // 
            this.lblSoDienThoaiNguoiNhapKhau.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoDienThoaiNguoiNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(104.3462F, 72.77702F);
            this.lblSoDienThoaiNguoiNhapKhau.Name = "lblSoDienThoaiNguoiNhapKhau";
            this.lblSoDienThoaiNguoiNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDienThoaiNguoiNhapKhau.SizeF = new System.Drawing.SizeF(175.7287F, 10.00001F);
            this.lblSoDienThoaiNguoiNhapKhau.StylePriority.UseFont = false;
            this.lblSoDienThoaiNguoiNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblSoDienThoaiNguoiNhapKhau.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoDienThoaiNguoiNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaChiNguoiNhapKhau
            // 
            this.lblDiaChiNguoiNhapKhau.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblDiaChiNguoiNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(103.7876F, 52.77701F);
            this.lblDiaChiNguoiNhapKhau.Multiline = true;
            this.lblDiaChiNguoiNhapKhau.Name = "lblDiaChiNguoiNhapKhau";
            this.lblDiaChiNguoiNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaChiNguoiNhapKhau.SizeF = new System.Drawing.SizeF(625.55F, 23F);
            this.lblDiaChiNguoiNhapKhau.StylePriority.UseFont = false;
            this.lblDiaChiNguoiNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblDiaChiNguoiNhapKhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaChiNguoiNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaBuuChinhNhapKhau
            // 
            this.lblMaBuuChinhNhapKhau.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaBuuChinhNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(103.7892F, 42.77702F);
            this.lblMaBuuChinhNhapKhau.Name = "lblMaBuuChinhNhapKhau";
            this.lblMaBuuChinhNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaBuuChinhNhapKhau.SizeF = new System.Drawing.SizeF(70.65282F, 9.999996F);
            this.lblMaBuuChinhNhapKhau.StylePriority.UseFont = false;
            this.lblMaBuuChinhNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblMaBuuChinhNhapKhau.Text = "XXXXXXE";
            this.lblMaBuuChinhNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenNguoiNhapKhau
            // 
            this.lblTenNguoiNhapKhau.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenNguoiNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(103.7892F, 19.99999F);
            this.lblTenNguoiNhapKhau.Multiline = true;
            this.lblTenNguoiNhapKhau.Name = "lblTenNguoiNhapKhau";
            this.lblTenNguoiNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNguoiNhapKhau.SizeF = new System.Drawing.SizeF(625.55F, 23F);
            this.lblTenNguoiNhapKhau.StylePriority.UseFont = false;
            this.lblTenNguoiNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblTenNguoiNhapKhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenNguoiNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaNguoiNhapKhau
            // 
            this.lblMaNguoiNhapKhau.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaNguoiNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(103.7876F, 9.999997F);
            this.lblMaNguoiNhapKhau.Name = "lblMaNguoiNhapKhau";
            this.lblMaNguoiNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNguoiNhapKhau.SizeF = new System.Drawing.SizeF(129.2702F, 9.999997F);
            this.lblMaNguoiNhapKhau.StylePriority.UseFont = false;
            this.lblMaNguoiNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblMaNguoiNhapKhau.Text = "XXXXXXXXX1-XXE";
            this.lblMaNguoiNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(20.39916F, 102.6216F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(80.68964F, 10F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Tên";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(20.39917F, 92.6216F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(80.56865F, 9.999992F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Mã";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(10.05991F, 82.6216F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(140.9709F, 10.00001F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Người ủy thác nhập khẩu";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(20.96783F, 72.6216F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(80F, 10F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "Số điện thoại";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(22.65689F, 51.99998F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(79.44147F, 9.999996F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "Địa chỉ";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(22.09836F, 41.99999F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(80F, 10F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Mã bưu chính";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(22.09999F, 19.99999F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(80F, 10F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "Tên";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(22.09836F, 9.999997F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(80F, 10F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Mã";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(9.999996F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(92.1F, 10F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Người nhập khẩu";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txtBarCode,
            this.lblSoTrang,
            this.xrLine1,
            this.xrLabel3,
            this.xrLabel32,
            this.xrLabel31,
            this.xrLabel10,
            this.lblBieuThiTHHetHan,
            this.lblPhanLoaiCaNhanToChuc,
            this.lblMaHieuPhuongThucVanChuyen,
            this.lblMaPhanLoaiHangHoa,
            this.lblThoiHanTaiNhapTaiXuat,
            this.lblMaBoPhanXuLyToKhai,
            this.lblMaSoHangHoaDaiDienToKhai,
            this.lblTongSoToKhaiChiaNho,
            this.lblSoNhanhToKhaiChiaNho,
            this.lblGioThayDoiDangKy,
            this.lblNgayThayDoiDangKy,
            this.xrLabel21,
            this.xrLabel20,
            this.lbl222,
            this.lblGioDangKy,
            this.xrLabel16,
            this.lblMaLoaiHinh,
            this.lblTenCoQuanHaiQuanTiepNhanToKhai,
            this.xrLabel14,
            this.lblNgayDangKy,
            this.lblMaPhanLoaiKiemTra,
            this.lblSoToKhaiDauTien,
            this.xrLabel9,
            this.lblSoToKhaiTamNhapTaiXuat,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.lblSoToKhai,
            this.xrLabel1});
            this.TopMargin.HeightF = 111.75F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TopMargin.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TopMargin_BeforePrint);
            // 
            // txtBarCode
            // 
            this.txtBarCode.LocationFloat = new DevExpress.Utils.PointFloat(604.3806F, 23.17154F);
            this.txtBarCode.Name = "txtBarCode";
            this.txtBarCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100F);
            this.txtBarCode.SizeF = new System.Drawing.SizeF(162.6194F, 23F);
            this.txtBarCode.Symbology = code128Generator1;
            this.txtBarCode.Visible = false;
            this.txtBarCode.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtBarCode_BeforePrint);
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(700F, 10F);
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTrang.SizeF = new System.Drawing.SizeF(30F, 11F);
            this.lblSoTrang.StylePriority.UseFont = false;
            this.lblSoTrang.StylePriority.UseTextAlignment = false;
            this.lblSoTrang.Text = "X/X";
            this.lblSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 90.17152F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(767F, 2F);
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(9.292793F, 35.17154F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(96.88F, 11F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Số tờ khai";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(718.0407F, 79.17153F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(13F, 11F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "-";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(511.9435F, 35.17154F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(10.21F, 11F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "/";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(470.9644F, 35.17154F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(14.79F, 11F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "-";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblBieuThiTHHetHan
            // 
            this.lblBieuThiTHHetHan.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblBieuThiTHHetHan.LocationFloat = new DevExpress.Utils.PointFloat(731.0406F, 79.17156F);
            this.lblBieuThiTHHetHan.Name = "lblBieuThiTHHetHan";
            this.lblBieuThiTHHetHan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBieuThiTHHetHan.SizeF = new System.Drawing.SizeF(16F, 11F);
            this.lblBieuThiTHHetHan.StylePriority.UseFont = false;
            this.lblBieuThiTHHetHan.StylePriority.UseTextAlignment = false;
            this.lblBieuThiTHHetHan.Text = "X";
            this.lblBieuThiTHHetHan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiCaNhanToChuc
            // 
            this.lblPhanLoaiCaNhanToChuc.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblPhanLoaiCaNhanToChuc.LocationFloat = new DevExpress.Utils.PointFloat(391.1726F, 57.17154F);
            this.lblPhanLoaiCaNhanToChuc.Name = "lblPhanLoaiCaNhanToChuc";
            this.lblPhanLoaiCaNhanToChuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiCaNhanToChuc.SizeF = new System.Drawing.SizeF(25.10007F, 11F);
            this.lblPhanLoaiCaNhanToChuc.StylePriority.UseFont = false;
            this.lblPhanLoaiCaNhanToChuc.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiCaNhanToChuc.Text = "[ X ]";
            this.lblPhanLoaiCaNhanToChuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaHieuPhuongThucVanChuyen
            // 
            this.lblMaHieuPhuongThucVanChuyen.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaHieuPhuongThucVanChuyen.LocationFloat = new DevExpress.Utils.PointFloat(374.6226F, 57.17154F);
            this.lblMaHieuPhuongThucVanChuyen.Name = "lblMaHieuPhuongThucVanChuyen";
            this.lblMaHieuPhuongThucVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHieuPhuongThucVanChuyen.SizeF = new System.Drawing.SizeF(16.55F, 11F);
            this.lblMaHieuPhuongThucVanChuyen.StylePriority.UseFont = false;
            this.lblMaHieuPhuongThucVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblMaHieuPhuongThucVanChuyen.Text = "X";
            this.lblMaHieuPhuongThucVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiHangHoa
            // 
            this.lblMaPhanLoaiHangHoa.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiHangHoa.LocationFloat = new DevExpress.Utils.PointFloat(358.0726F, 57.17154F);
            this.lblMaPhanLoaiHangHoa.Name = "lblMaPhanLoaiHangHoa";
            this.lblMaPhanLoaiHangHoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiHangHoa.SizeF = new System.Drawing.SizeF(16.55F, 11F);
            this.lblMaPhanLoaiHangHoa.StylePriority.UseFont = false;
            this.lblMaPhanLoaiHangHoa.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiHangHoa.Text = "X";
            this.lblMaPhanLoaiHangHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblThoiHanTaiNhapTaiXuat
            // 
            this.lblThoiHanTaiNhapTaiXuat.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblThoiHanTaiNhapTaiXuat.LocationFloat = new DevExpress.Utils.PointFloat(641.0407F, 79.17153F);
            this.lblThoiHanTaiNhapTaiXuat.Name = "lblThoiHanTaiNhapTaiXuat";
            this.lblThoiHanTaiNhapTaiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThoiHanTaiNhapTaiXuat.SizeF = new System.Drawing.SizeF(77F, 11F);
            this.lblThoiHanTaiNhapTaiXuat.StylePriority.UseFont = false;
            this.lblThoiHanTaiNhapTaiXuat.StylePriority.UseTextAlignment = false;
            this.lblThoiHanTaiNhapTaiXuat.Text = "dd/MM/yyyy";
            this.lblThoiHanTaiNhapTaiXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaBoPhanXuLyToKhai
            // 
            this.lblMaBoPhanXuLyToKhai.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaBoPhanXuLyToKhai.LocationFloat = new DevExpress.Utils.PointFloat(640.7144F, 68.17153F);
            this.lblMaBoPhanXuLyToKhai.Name = "lblMaBoPhanXuLyToKhai";
            this.lblMaBoPhanXuLyToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaBoPhanXuLyToKhai.SizeF = new System.Drawing.SizeF(23.96F, 11F);
            this.lblMaBoPhanXuLyToKhai.StylePriority.UseFont = false;
            this.lblMaBoPhanXuLyToKhai.StylePriority.UseTextAlignment = false;
            this.lblMaBoPhanXuLyToKhai.Text = "XE";
            this.lblMaBoPhanXuLyToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaSoHangHoaDaiDienToKhai
            // 
            this.lblMaSoHangHoaDaiDienToKhai.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaSoHangHoaDaiDienToKhai.LocationFloat = new DevExpress.Utils.PointFloat(640.7145F, 57.17154F);
            this.lblMaSoHangHoaDaiDienToKhai.Name = "lblMaSoHangHoaDaiDienToKhai";
            this.lblMaSoHangHoaDaiDienToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSoHangHoaDaiDienToKhai.SizeF = new System.Drawing.SizeF(41.67F, 11F);
            this.lblMaSoHangHoaDaiDienToKhai.StylePriority.UseFont = false;
            this.lblMaSoHangHoaDaiDienToKhai.StylePriority.UseTextAlignment = false;
            this.lblMaSoHangHoaDaiDienToKhai.Text = "XXXE";
            this.lblMaSoHangHoaDaiDienToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongSoToKhaiChiaNho
            // 
            this.lblTongSoToKhaiChiaNho.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTongSoToKhaiChiaNho.LocationFloat = new DevExpress.Utils.PointFloat(522.8367F, 35.17154F);
            this.lblTongSoToKhaiChiaNho.Name = "lblTongSoToKhaiChiaNho";
            this.lblTongSoToKhaiChiaNho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoToKhaiChiaNho.SizeF = new System.Drawing.SizeF(22.08F, 11F);
            this.lblTongSoToKhaiChiaNho.StylePriority.UseFont = false;
            this.lblTongSoToKhaiChiaNho.StylePriority.UseTextAlignment = false;
            this.lblTongSoToKhaiChiaNho.Text = "NE";
            this.lblTongSoToKhaiChiaNho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTongSoToKhaiChiaNho.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblSoNhanhToKhaiChiaNho
            // 
            this.lblSoNhanhToKhaiChiaNho.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoNhanhToKhaiChiaNho.LocationFloat = new DevExpress.Utils.PointFloat(485.7546F, 35.17154F);
            this.lblSoNhanhToKhaiChiaNho.Name = "lblSoNhanhToKhaiChiaNho";
            this.lblSoNhanhToKhaiChiaNho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoNhanhToKhaiChiaNho.SizeF = new System.Drawing.SizeF(24.16F, 11F);
            this.lblSoNhanhToKhaiChiaNho.StylePriority.UseFont = false;
            this.lblSoNhanhToKhaiChiaNho.StylePriority.UseTextAlignment = false;
            this.lblSoNhanhToKhaiChiaNho.Text = "NE";
            this.lblSoNhanhToKhaiChiaNho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoNhanhToKhaiChiaNho.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblGioThayDoiDangKy
            // 
            this.lblGioThayDoiDangKy.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGioThayDoiDangKy.LocationFloat = new DevExpress.Utils.PointFloat(431.7528F, 79.17153F);
            this.lblGioThayDoiDangKy.Name = "lblGioThayDoiDangKy";
            this.lblGioThayDoiDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioThayDoiDangKy.SizeF = new System.Drawing.SizeF(60F, 11F);
            this.lblGioThayDoiDangKy.StylePriority.UseFont = false;
            this.lblGioThayDoiDangKy.StylePriority.UseTextAlignment = false;
            this.lblGioThayDoiDangKy.Text = "hh:mm:ss";
            this.lblGioThayDoiDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayThayDoiDangKy
            // 
            this.lblNgayThayDoiDangKy.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblNgayThayDoiDangKy.LocationFloat = new DevExpress.Utils.PointFloat(354.7528F, 79.17153F);
            this.lblNgayThayDoiDangKy.Name = "lblNgayThayDoiDangKy";
            this.lblNgayThayDoiDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayThayDoiDangKy.SizeF = new System.Drawing.SizeF(77F, 11F);
            this.lblNgayThayDoiDangKy.StylePriority.UseFont = false;
            this.lblNgayThayDoiDangKy.StylePriority.UseTextAlignment = false;
            this.lblNgayThayDoiDangKy.Text = "dd/MM/yyyy";
            this.lblNgayThayDoiDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(491.7528F, 79.17153F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(148.96F, 11F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(491.7545F, 68.17153F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(148.96F, 11F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Mã bộ phận xử lý tờ khai";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl222
            // 
            this.lbl222.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl222.LocationFloat = new DevExpress.Utils.PointFloat(455.2944F, 57.17154F);
            this.lbl222.Name = "lbl222";
            this.lbl222.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl222.SizeF = new System.Drawing.SizeF(185.42F, 11F);
            this.lbl222.StylePriority.UseFont = false;
            this.lbl222.StylePriority.UseTextAlignment = false;
            this.lbl222.Text = "Mã số hàng hóa đại diện của tờ khai";
            this.lbl222.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGioDangKy
            // 
            this.lblGioDangKy.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGioDangKy.LocationFloat = new DevExpress.Utils.PointFloat(172.7528F, 79.17153F);
            this.lblGioDangKy.Name = "lblGioDangKy";
            this.lblGioDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioDangKy.SizeF = new System.Drawing.SizeF(53F, 11F);
            this.lblGioDangKy.StylePriority.UseFont = false;
            this.lblGioDangKy.StylePriority.UseTextAlignment = false;
            this.lblGioDangKy.Text = "hh:mm:ss";
            this.lblGioDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(225.7528F, 79.17153F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(129F, 11F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Ngày thay đổi đăng ký";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaLoaiHinh.LocationFloat = new DevExpress.Utils.PointFloat(332.0028F, 57.17154F);
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaLoaiHinh.SizeF = new System.Drawing.SizeF(26.06979F, 11F);
            this.lblMaLoaiHinh.StylePriority.UseFont = false;
            this.lblMaLoaiHinh.StylePriority.UseTextAlignment = false;
            this.lblMaLoaiHinh.Text = "XXE";
            this.lblMaLoaiHinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenCoQuanHaiQuanTiepNhanToKhai
            // 
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.LocationFloat = new DevExpress.Utils.PointFloat(225.7528F, 68.17153F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Name = "lblTenCoQuanHaiQuanTiepNhanToKhai";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.SizeF = new System.Drawing.SizeF(102F, 11F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.StylePriority.UseFont = false;
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.StylePriority.UseTextAlignment = false;
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Text = "XXXXXXXXXE";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(225.7528F, 57.17154F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(106.25F, 11F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Mã loại hình";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblNgayDangKy.LocationFloat = new DevExpress.Utils.PointFloat(95.75281F, 79.17153F);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.SizeF = new System.Drawing.SizeF(77F, 11F);
            this.lblNgayDangKy.StylePriority.UseFont = false;
            this.lblNgayDangKy.StylePriority.UseTextAlignment = false;
            this.lblNgayDangKy.Text = "dd/MM/yyyy";
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiKiemTra
            // 
            this.lblMaPhanLoaiKiemTra.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblMaPhanLoaiKiemTra.LocationFloat = new DevExpress.Utils.PointFloat(152.0028F, 57.17154F);
            this.lblMaPhanLoaiKiemTra.Name = "lblMaPhanLoaiKiemTra";
            this.lblMaPhanLoaiKiemTra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiKiemTra.SizeF = new System.Drawing.SizeF(39.59F, 11F);
            this.lblMaPhanLoaiKiemTra.StylePriority.UseFont = false;
            this.lblMaPhanLoaiKiemTra.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiKiemTra.Text = "XX E";
            this.lblMaPhanLoaiKiemTra.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhaiDauTien
            // 
            this.lblSoToKhaiDauTien.AutoWidth = true;
            this.lblSoToKhaiDauTien.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoToKhaiDauTien.LocationFloat = new DevExpress.Utils.PointFloat(357.0028F, 35.17154F);
            this.lblSoToKhaiDauTien.Name = "lblSoToKhaiDauTien";
            this.lblSoToKhaiDauTien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiDauTien.SizeF = new System.Drawing.SizeF(113.96F, 11F);
            this.lblSoToKhaiDauTien.StylePriority.UseFont = false;
            this.lblSoToKhaiDauTien.StylePriority.UseTextAlignment = false;
            this.lblSoToKhaiDauTien.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiDauTien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoToKhaiDauTien.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel9
            // 
            this.xrLabel9.AutoWidth = true;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(225.7528F, 35.17154F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(131.25F, 11F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Số tờ khai đầu tiên";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhaiTamNhapTaiXuat
            // 
            this.lblSoToKhaiTamNhapTaiXuat.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoToKhaiTamNhapTaiXuat.LocationFloat = new DevExpress.Utils.PointFloat(225.7528F, 46.17154F);
            this.lblSoToKhaiTamNhapTaiXuat.Name = "lblSoToKhaiTamNhapTaiXuat";
            this.lblSoToKhaiTamNhapTaiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiTamNhapTaiXuat.SizeF = new System.Drawing.SizeF(117.71F, 11F);
            this.lblSoToKhaiTamNhapTaiXuat.StylePriority.UseFont = false;
            this.lblSoToKhaiTamNhapTaiXuat.StylePriority.UseTextAlignment = false;
            this.lblSoToKhaiTamNhapTaiXuat.Text = "NNNNNNNNN1NE";
            this.lblSoToKhaiTamNhapTaiXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoToKhaiTamNhapTaiXuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(8.355824F, 79.17156F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(86.46F, 11F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Ngày đăng ký";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(9.292762F, 68.17153F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(211.88F, 11F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(9.292793F, 57.17154F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(142.71F, 11F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Mã phân loại kiểm tra";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(9.287786F, 46.17154F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(211.88F, 11F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.lblSoToKhai.LocationFloat = new DevExpress.Utils.PointFloat(106.1728F, 35.17154F);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.SizeF = new System.Drawing.SizeF(115F, 11F);
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoToKhai.Text = "NNNNNNNNN1NE";
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(175.3184F, 10.00001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(403.83F, 25.17154F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Tờ khai hàng hóa nhập khẩu (thông quan)";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 12.5F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // QuyetDinhThongQuanHangHoaNhapKhau_1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(42, 26, 112, 12);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiKiemTra;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhaiDauTien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhaiTamNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lblMaLoaiHinh;
        private DevExpress.XtraReports.UI.XRLabel lblTenCoQuanHaiQuanTiepNhanToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel lbl222;
        private DevExpress.XtraReports.UI.XRLabel lblGioDangKy;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel lblSoNhanhToKhaiChiaNho;
        private DevExpress.XtraReports.UI.XRLabel lblGioThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblNgayThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoToKhaiChiaNho;
        private DevExpress.XtraReports.UI.XRLabel lblBieuThiTHHetHan;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiCaNhanToChuc;
        private DevExpress.XtraReports.UI.XRLabel lblMaHieuPhuongThucVanChuyen;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiHangHoa;
        private DevExpress.XtraReports.UI.XRLabel lblThoiHanTaiNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRLabel lblMaBoPhanXuLyToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblMaSoHangHoaDaiDienToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblMaBuuChinhNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel lblTenNguoiNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel lblSoDienThoaiNguoiNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel lblDiaChiNguoiNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiUyThacNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel lblTenNguoiXuatKhau;
        private DevExpress.XtraReports.UI.XRLabel lblMaBuuChinhXuatKhau;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiXuatKhau;
        private DevExpress.XtraReports.UI.XRLabel lblMaNuoc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel lblDiaChiNguoiXuatKhau2;
        private DevExpress.XtraReports.UI.XRLabel lblDiaChiNguoiXuatKhau3;
        private DevExpress.XtraReports.UI.XRLabel lblDiaChiNguoiXuatKhau1;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiUyThacXuatKhau;
        private DevExpress.XtraReports.UI.XRLabel lblDiaChiNguoiXuatKhau4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel lblMaNhanVienHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel lblTenDaiLyHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel lblMaDaiLyHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel lblSoVanDon1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuongContainer;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinhTrongLuong;
        private DevExpress.XtraReports.UI.XRLabel lblTongTrongLuongHang;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinh;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel lblSoVanDon5;
        private DevExpress.XtraReports.UI.XRLabel lblSoVanDon4;
        private DevExpress.XtraReports.UI.XRLabel lblSoVanDon3;
        private DevExpress.XtraReports.UI.XRLabel lblSoVanDon2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRLabel lblTenDiaDiemXepHang;
        private DevExpress.XtraReports.UI.XRLabel lblTenDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRLabel lblMaDiaDiemXepHang;
        private DevExpress.XtraReports.UI.XRLabel lblMaDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRLabel lblTenDiaDiemLuuKhoHang;
        private DevExpress.XtraReports.UI.XRLabel lblMaDiaDiemLuuKhoHang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel lblKyHieuSoHieu;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHangDen;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDuocPhepNhapKho;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhuongTienVanChuyen;
        private DevExpress.XtraReports.UI.XRLabel lblTenPhuongTienVanChuyen;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLabel lblMaVanBanPhapQuyKhac4;
        private DevExpress.XtraReports.UI.XRLabel lblMaVanBanPhapQuyKhac3;
        private DevExpress.XtraReports.UI.XRLabel lblMaVanBanPhapQuyKhac2;
        private DevExpress.XtraReports.UI.XRLabel lblMaVanBanPhapQuyKhac1;
        private DevExpress.XtraReports.UI.XRLabel lblPhuongThucThanhToan;
        private DevExpress.XtraReports.UI.XRLabel lblNgayPhatHanh;
        private DevExpress.XtraReports.UI.XRLabel lblSoTiepNhanHoaDonDienTu;
        private DevExpress.XtraReports.UI.XRLabel lblSoHoaDon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiHinhThucHoaDon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiNhapLieu;
        private DevExpress.XtraReports.UI.XRLabel lblTongHeSoPhanBoTriGia;
        private DevExpress.XtraReports.UI.XRLabel lblTongTriGiaTinhThue;
        private DevExpress.XtraReports.UI.XRLabel lblTongTriGiaHoaDon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel72;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLabel lblMaDongTienHoaDon;
        private DevExpress.XtraReports.UI.XRLabel lblMaDieuKienGiaHoaDon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiGiaHoaDon;
        private DevExpress.XtraReports.UI.XRLabel lblMaKetQuaKiemTraNoiDung;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel62;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLabel lblSoGiayPhep5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel90;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiGiayPhepNhapKhau5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel88;
        private DevExpress.XtraReports.UI.XRLabel lblSoGiayPhep3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiGiayPhepNhapKhau3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel lblSoGiayPhep4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel82;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiGiayPhepNhapKhau4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRLabel lblSoGiayPhep2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiGiayPhepNhapKhau2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel lblSoGiayPhep1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiGiayPhepNhapKhau1;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiDieuChinhTriGia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel99;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiCongThucChuan;
        private DevExpress.XtraReports.UI.XRLabel lblSoTiepNhanKhaiTriGiaTongHop;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiKhaiTriGia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel95;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRLabel xrLabel93;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLabel xrLabel115;
        private DevExpress.XtraReports.UI.XRLabel xrLabel114;
        private DevExpress.XtraReports.UI.XRLabel lblPhiBaoHiem;
        private DevExpress.XtraReports.UI.XRLabel xrLabel112;
        private DevExpress.XtraReports.UI.XRLabel lblPhiVanChuyen;
        private DevExpress.XtraReports.UI.XRLabel lblMaTienTeCuaTienBaoHiem;
        private DevExpress.XtraReports.UI.XRLabel xrLabel109;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiBaoHiem;
        private DevExpress.XtraReports.UI.XRLabel lblMaTienTePhiVanChuyen;
        private DevExpress.XtraReports.UI.XRLabel xrLabel106;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiPhiVanChuyen;
        private DevExpress.XtraReports.UI.XRLabel lblGiaCoSo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel103;
        private DevExpress.XtraReports.UI.XRLabel lblSoDangKyBaoHiemTongHop;
        private DevExpress.XtraReports.UI.XRLabel lblPhuongPhapDieuChinhTriGia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel116;
        private DevExpress.XtraReports.UI.XRLabel lblMaTienTe;
        private DevExpress.XtraReports.UI.XRLabel lblTongHeSoPhanBoTriGiaKhoanDieuChinh2;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaKhoanDieuChinh2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel117;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiDieuChinh2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel111;
        private DevExpress.XtraReports.UI.XRLabel lblMaTenKhoanDieuChinh2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel108;
        private DevExpress.XtraReports.UI.XRLabel lblTongHeSoPhanBoTriGiaKhoanDieuChinh1;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaKhoanDieuChinh1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel104;
        private DevExpress.XtraReports.UI.XRLabel lblMaDongTienDieuChinhTriGia1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel101;
        private DevExpress.XtraReports.UI.XRLabel lblMaTenKhoanDieuChinh1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel98;
        private DevExpress.XtraReports.UI.XRLabel xrLabel97;
        private DevExpress.XtraReports.UI.XRLabel xrLabel96;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel xrLabel70;
        private DevExpress.XtraReports.UI.XRLabel lblChiTietKhaiTriGia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel141;
        private DevExpress.XtraReports.UI.XRLabel lblTongHeSoPhanBoTriGiaKhoanDieuChinh5;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaKhoanDieuChinh5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel138;
        private DevExpress.XtraReports.UI.XRLabel xrLabel137;
        private DevExpress.XtraReports.UI.XRLabel xrLabel136;
        private DevExpress.XtraReports.UI.XRLabel lblMaTenKhoanDieuChinh5;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiDieuChinh5;
        private DevExpress.XtraReports.UI.XRLabel lblTongHeSoPhanBoTriGiaKhoanDieuChinh4;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaKhoanDieuChinh4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel131;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiDieuChinh4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel129;
        private DevExpress.XtraReports.UI.XRLabel lblMaTenKhoanDieuChinh4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel127;
        private DevExpress.XtraReports.UI.XRLabel lblTongHeSoPhanBoTriGiaKhoanDieuChinh3;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaKhoanDieuChinh3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel124;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiDieuChinh3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel122;
        private DevExpress.XtraReports.UI.XRLabel lblMaTenKhoanDieuChinh3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel120;
        private DevExpress.XtraReports.UI.XRLabel xrLabel146;
        private DevExpress.XtraReports.UI.XRLabel xrLabel145;
        private DevExpress.XtraReports.UI.XRLabel xrLabel144;
        private DevExpress.XtraReports.UI.XRLabel xrLabel143;
        private DevExpress.XtraReports.UI.XRLabel xrLabel153;
        private DevExpress.XtraReports.UI.XRLabel lblMaDongTienDieuChinhTriGia5;
        private DevExpress.XtraReports.UI.XRLabel lblMaDongTienDieuChinhTriGia4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel150;
        private DevExpress.XtraReports.UI.XRLabel xrLabel149;
        private DevExpress.XtraReports.UI.XRLabel lblMaDongTienDieuChinhTriGia3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel147;
        private DevExpress.XtraReports.UI.XRLabel lblMaDongTienDieuChinhTriGia2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel102;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiDieuChinh1;
        private DevExpress.XtraReports.UI.XRLabel lblSoDongTong1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel154;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThue1;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThue1;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThue1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel186;
        private DevExpress.XtraReports.UI.XRLabel lblSoDongTong6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel184;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThue6;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThue6;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThue6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel180;
        private DevExpress.XtraReports.UI.XRLabel lblSoDongTong5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel178;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThue5;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThue5;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThue5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel174;
        private DevExpress.XtraReports.UI.XRLabel lblSoDongTong4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel172;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThue4;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThue4;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThue4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel168;
        private DevExpress.XtraReports.UI.XRLabel lblSoDongTong3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel166;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThue3;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThue3;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThue3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel162;
        private DevExpress.XtraReports.UI.XRLabel lblSoDongTong2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel160;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThue2;
        private DevExpress.XtraReports.UI.XRLabel lblTenSacThue2;
        private DevExpress.XtraReports.UI.XRLabel lblMaSacThue2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel156;
        private DevExpress.XtraReports.UI.XRLabel xrLabel210;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiNopThue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel208;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoTrangToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiNopThue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel205;
        private DevExpress.XtraReports.UI.XRLabel lblMaXacDinhThoiHanNopThue;
        private DevExpress.XtraReports.UI.XRLabel lblTyGiaTinhThue3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel202;
        private DevExpress.XtraReports.UI.XRLabel lblMaDongTienTyGiaTinhThue3;
        private DevExpress.XtraReports.UI.XRLabel lblTyGiaTinhThue2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel199;
        private DevExpress.XtraReports.UI.XRLabel lblMaDongTienTyGiaTinhThue2;
        private DevExpress.XtraReports.UI.XRLabel lblTyGiaTinhThue1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel196;
        private DevExpress.XtraReports.UI.XRLabel lblMaDongTienTyGiaTinhThue1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel194;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienBaoLanh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel192;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThuePhaiNop;
        private DevExpress.XtraReports.UI.XRLabel xrLabel190;
        private DevExpress.XtraReports.UI.XRLabel xrLabel189;
        private DevExpress.XtraReports.UI.XRLabel xrLabel188;
        private DevExpress.XtraReports.UI.XRLabel xrLabel187;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoDongHang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel211;
        private DevExpress.XtraReports.UI.XRLabel lblMaLyDoDeNghi;
        private DevExpress.XtraReports.UI.XRLabel lblTenNguoiUyThacNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel lblMaVanBanPhapQuyKhac5;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRLabel lblSoTrang;
        private DevExpress.XtraReports.UI.XRBarCode txtBarCode;
    }
}
