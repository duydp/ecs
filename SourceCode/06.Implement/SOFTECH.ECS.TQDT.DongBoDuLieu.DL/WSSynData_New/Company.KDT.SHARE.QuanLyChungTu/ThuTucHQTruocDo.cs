﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Logger;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class ThuTucHQTruocDo 
    {
        public List<ChungTuHQTruocDo> ListChungTu { get; set; }

        public static ThuTucHQTruocDo LoadFullChungTu(long master_id, string type)
        {
            IList<ThuTucHQTruocDo> listTemp;
            listTemp = (List<ThuTucHQTruocDo>)ThuTucHQTruocDo.SelectCollectionDynamic(string.Format("Master_ID = {0} and Type = '{1}'", master_id, type), null);
            if (listTemp.Count > 0)
            {
                listTemp[0].ListChungTu = (List<ChungTuHQTruocDo>)ChungTuHQTruocDo.LoadByMasterID(listTemp[0].ID, listTemp[0].Type);
                return listTemp[0];
            }
            return null;
        }
        public int InsertUpdateFull(SqlTransaction transaction)
        {
            if (this.ID == 0)
            {
                long id = this.Insert(transaction);
                foreach (ChungTuHQTruocDo item in ListChungTu)
                {
                    item.Master_ID = id;
                    item.Type = this.Type;
                    item.Insert(transaction);

                }

            }
            else if (this.ID > 0)
            {
                this.Update(transaction);
                foreach (ChungTuHQTruocDo item in this.ListChungTu)
                {
                    item.Master_ID = this.ID;
                    item.Type = this.Type;
                    item.InsertUpdate(transaction);
                }
            }
            return 1;
        }
        public int InsertUpdateFull()
        {
            int result = 0;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection con = (SqlConnection)db.CreateConnection())
            {
                con.Open();
                SqlTransaction transaction = con.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        long id = this.Insert(transaction);
                        foreach (ChungTuHQTruocDo item in ListChungTu)
                        {
                            item.Master_ID = id;
                            item.Type = this.Type;
                            item.Insert(transaction);

                        }

                    }
                    else if(this.ID > 0)
                    {
                        this.Update(transaction);
                        foreach (ChungTuHQTruocDo item in this.ListChungTu)
                        {
                            item.Master_ID = this.ID;
                            item.Type = this.Type;
                            item.InsertUpdate(transaction);
                        }
                    }
                    transaction.Commit();
                    result = 1;

                }
                catch (System.Exception ex)
                {
                    transaction.Rollback();
                    LocalLogger.Instance().WriteMessage(ex);
                    result = 0;
                    
                }
                finally
                {
                    con.Close();
                }
            } 
            return result;

        }


    }
}
