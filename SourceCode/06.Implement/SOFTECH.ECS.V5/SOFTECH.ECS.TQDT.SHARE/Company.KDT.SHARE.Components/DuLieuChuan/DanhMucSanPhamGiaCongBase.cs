using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public partial class DanhMucSanPhamGiaCong: BaseClass
    {
        #region Private members.

        protected string _MaSanPham = String.Empty;
        protected string _MaDuKien = String.Empty;
        protected string _TenSanPham = String.Empty;
        protected string _DVT_ID = String.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public string MaSanPham
        {
            set { this._MaSanPham = value; }
            get { return this._MaSanPham; }
        }
        public string MaDuKien
        {
            set { this._MaDuKien = value; }
            get { return this._MaDuKien; }
        }
        public string TenSanPham
        {
            set { this._TenSanPham = value; }
            get { return this._TenSanPham; }
        }
        public string DVT_ID
        {
            set { this._DVT_ID = value; }
            get { return this._DVT_ID; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_HaiQuan_DanhMucSanPhamGiaCong_Load";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);

            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) this._MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDuKien"))) this._MaDuKien = reader.GetString(reader.GetOrdinal("MaDuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) this._TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_HaiQuan_DanhMucSanPhamGiaCong_SelectAll";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_HaiQuan_DanhMucSanPhamGiaCong_SelectAll";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_HaiQuan_DanhMucSanPhamGiaCong_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_HaiQuan_DanhMucSanPhamGiaCong_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DanhMucSanPhamGiaCongCollection SelectCollectionAll()
        {
            DanhMucSanPhamGiaCongCollection collection = new DanhMucSanPhamGiaCongCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                DanhMucSanPhamGiaCong entity = new DanhMucSanPhamGiaCong();

                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDuKien"))) entity.MaDuKien = reader.GetString(reader.GetOrdinal("MaDuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DanhMucSanPhamGiaCongCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            DanhMucSanPhamGiaCongCollection collection = new DanhMucSanPhamGiaCongCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                DanhMucSanPhamGiaCong entity = new DanhMucSanPhamGiaCong();

                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDuKien"))) entity.MaDuKien = reader.GetString(reader.GetOrdinal("MaDuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_HaiQuan_DanhMucSanPhamGiaCong_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            db.AddInParameter(dbCommand, "@MaDuKien", SqlDbType.VarChar, this._MaDuKien);
            db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);

            if (transaction != null)
            {
                return db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(DanhMucSanPhamGiaCongCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DanhMucSanPhamGiaCong item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, DanhMucSanPhamGiaCongCollection collection)
        {
            foreach (DanhMucSanPhamGiaCong item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_HaiQuan_DanhMucSanPhamGiaCong_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            db.AddInParameter(dbCommand, "@MaDuKien", SqlDbType.VarChar, this._MaDuKien);
            db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(DanhMucSanPhamGiaCongCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DanhMucSanPhamGiaCong item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_HaiQuan_DanhMucSanPhamGiaCong_Update";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            db.AddInParameter(dbCommand, "@MaDuKien", SqlDbType.VarChar, this._MaDuKien);
            db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(DanhMucSanPhamGiaCongCollection collection, SqlTransaction transaction)
        {
            foreach (DanhMucSanPhamGiaCong item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        public void InsertUpdate1( DanhMucSanPhamGiaCongCollection  collection)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (DanhMucSanPhamGiaCong item in collection)
                    {
                        DanhMucSanPhamGiaCong dm = new DanhMucSanPhamGiaCong();
                        dm.MaSanPham = item.MaSanPham;
                        dm.MaDuKien = item.MaDuKien;
                        dm.TenSanPham = item.TenSanPham;
                        dm.DVT_ID = item.DVT_ID;
                        dm.Insert();                            
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_HaiQuan_DanhMucSanPhamGiaCong_Delete";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(DanhMucSanPhamGiaCongCollection collection, SqlTransaction transaction)
        {
            foreach (DanhMucSanPhamGiaCong item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(DanhMucSanPhamGiaCongCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DanhMucSanPhamGiaCong item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

    }
}