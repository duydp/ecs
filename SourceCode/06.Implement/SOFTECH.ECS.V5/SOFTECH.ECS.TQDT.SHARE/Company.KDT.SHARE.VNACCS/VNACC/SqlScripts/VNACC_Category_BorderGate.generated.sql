-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BorderGateCode varchar(10),
	@BorderGateName nvarchar(250),
	@CustomsOfficeCode varchar(5),
	@ImmigrationBureauUserCode varchar(5),
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@QuarantineOfficeUserCode varchar(5),
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_BorderGate]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BorderGateCode],
	[BorderGateName],
	[CustomsOfficeCode],
	[ImmigrationBureauUserCode],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[QuarantineOfficeUserCode],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@BorderGateCode,
	@BorderGateName,
	@CustomsOfficeCode,
	@ImmigrationBureauUserCode,
	@ArrivalDepartureForImmigration,
	@PassengerForImmigration,
	@QuarantineOfficeUserCode,
	@ArrivalDepartureForQuarantine,
	@PassengerForQuarantine,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BorderGateCode varchar(10),
	@BorderGateName nvarchar(250),
	@CustomsOfficeCode varchar(5),
	@ImmigrationBureauUserCode varchar(5),
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@QuarantineOfficeUserCode varchar(5),
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_BorderGate]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[BorderGateName] = @BorderGateName,
	[CustomsOfficeCode] = @CustomsOfficeCode,
	[ImmigrationBureauUserCode] = @ImmigrationBureauUserCode,
	[ArrivalDepartureForImmigration] = @ArrivalDepartureForImmigration,
	[PassengerForImmigration] = @PassengerForImmigration,
	[QuarantineOfficeUserCode] = @QuarantineOfficeUserCode,
	[ArrivalDepartureForQuarantine] = @ArrivalDepartureForQuarantine,
	[PassengerForQuarantine] = @PassengerForQuarantine,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[BorderGateCode] = @BorderGateCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BorderGateCode varchar(10),
	@BorderGateName nvarchar(250),
	@CustomsOfficeCode varchar(5),
	@ImmigrationBureauUserCode varchar(5),
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@QuarantineOfficeUserCode varchar(5),
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [BorderGateCode] FROM [dbo].[t_VNACC_Category_BorderGate] WHERE [BorderGateCode] = @BorderGateCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_BorderGate] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[BorderGateName] = @BorderGateName,
			[CustomsOfficeCode] = @CustomsOfficeCode,
			[ImmigrationBureauUserCode] = @ImmigrationBureauUserCode,
			[ArrivalDepartureForImmigration] = @ArrivalDepartureForImmigration,
			[PassengerForImmigration] = @PassengerForImmigration,
			[QuarantineOfficeUserCode] = @QuarantineOfficeUserCode,
			[ArrivalDepartureForQuarantine] = @ArrivalDepartureForQuarantine,
			[PassengerForQuarantine] = @PassengerForQuarantine,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[BorderGateCode] = @BorderGateCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_BorderGate]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[BorderGateCode],
			[BorderGateName],
			[CustomsOfficeCode],
			[ImmigrationBureauUserCode],
			[ArrivalDepartureForImmigration],
			[PassengerForImmigration],
			[QuarantineOfficeUserCode],
			[ArrivalDepartureForQuarantine],
			[PassengerForQuarantine],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@BorderGateCode,
			@BorderGateName,
			@CustomsOfficeCode,
			@ImmigrationBureauUserCode,
			@ArrivalDepartureForImmigration,
			@PassengerForImmigration,
			@QuarantineOfficeUserCode,
			@ArrivalDepartureForQuarantine,
			@PassengerForQuarantine,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Delete]
	@BorderGateCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_BorderGate]
WHERE
	[BorderGateCode] = @BorderGateCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_BorderGate] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Load]
	@BorderGateCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BorderGateCode],
	[BorderGateName],
	[CustomsOfficeCode],
	[ImmigrationBureauUserCode],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[QuarantineOfficeUserCode],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_BorderGate]
WHERE
	[BorderGateCode] = @BorderGateCode
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BorderGateCode],
	[BorderGateName],
	[CustomsOfficeCode],
	[ImmigrationBureauUserCode],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[QuarantineOfficeUserCode],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_BorderGate] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BorderGateCode],
	[BorderGateName],
	[CustomsOfficeCode],
	[ImmigrationBureauUserCode],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[QuarantineOfficeUserCode],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_BorderGate]	

GO

