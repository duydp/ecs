﻿namespace Company.Interface
{
    partial class VNACC_TKBoSung_ThueHangHoaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grdTienTangGiamThuKhac_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TKBoSung_ThueHangHoaForm));
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdChiThiHaiQuan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHaiQuan");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdlayPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdlayPhanHoi");
            this.cmdChiThiHaiQuan = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHaiQuan");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdToolBar = new Janus.Windows.UI.CommandBars.UIRebar();
            this.txtMaNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaBuuChinhNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.txtDiaChiNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label60 = new System.Windows.Forms.Label();
            this.txtSoDienThoaiNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTenNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoToKhaiBoSung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtNgayDangKyKhaiBoSung = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtGioDangKyKhaiBoSung = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoThongBao = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtNgayHoanThanhKiemTra = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtGioHoanThanhKiemTra = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.ucCoQuanHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label6 = new System.Windows.Forms.Label();
            this.ucNhomXuLyHoSo = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhanLoaiXuatNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ucMaLoaiHinh = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label8 = new System.Windows.Forms.Label();
            this.dtNgayKhaiBao = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtDauHieuBaoQua60Ngay = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHetThoiHan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dtNgayCapPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label10 = new System.Windows.Forms.Label();
            this.dtThoiHanTaiNhapTaiXuat = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTenDaiLyHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDaiLyHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMaNhanVienHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.ucMaLyDoKhaiBoSung = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucPhanLoaiNopThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoQuanLyTrongNoiBoDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.ucMaTienTeTienThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label22 = new System.Windows.Forms.Label();
            this.txtTenNganHangTraThueThay = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtMaNganHangTraThueThay = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamPhatHanhHanMuc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtKiHieuChungTuPhatHanhHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtSoChungPhatHanhHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtMaXacDinhThoiHanNopThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.dtNgayHieuLucChungTu = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label30 = new System.Windows.Forms.Label();
            this.txtSoNgayNopThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtSoNgayNopThueDanhChoVATHangHoaDacBiet = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTenNganHangBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtMaNganHangBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtKyHieuChungTuBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoChungTuBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamPhatHanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.ucMaTienTeTruocKhiKhaiBoSung = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTyGiaHoiDoaiTruocKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ucMaTienTeSauKhiKhaiBoSung = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTyGiaHoiDoaiSauKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtTongSoTienTangGiamThueXuatNhapKhau = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtHienThiTongSoTienTangGiamThueXuatNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaTienTeTongSoTienTangGiamThueXuatNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grdTienTangGiamThuKhac = new Janus.Windows.GridEX.GridEX();
            this.label37 = new System.Windows.Forms.Label();
            this.txtTongSoTrangToKhaiBoSung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtTongSoDongHangToKhaiBoSung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLyDo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.txtTenNguoiPhuTrach = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtTenTruongDonViHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label43 = new System.Windows.Forms.Label();
            this.dtNgayDangKyDuLieu = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtGioDangKyDuLieu = new Janus.Windows.CalendarCombo.CalendarCombo();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdToolBar)).BeginInit();
            this.cmdToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTienTangGiamThuKhac)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 527), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 35);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 527);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 503);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 503);
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.grdTienTangGiamThuKhac);
            this.grbMain.Controls.Add(this.ucNhomXuLyHoSo);
            this.grbMain.Controls.Add(this.ucMaLoaiHinh);
            this.grbMain.Controls.Add(this.ucCoQuanHaiQuan);
            this.grbMain.Controls.Add(this.txtSoNgayNopThueDanhChoVATHangHoaDacBiet);
            this.grbMain.Controls.Add(this.txtSoNgayNopThue);
            this.grbMain.Controls.Add(this.txtTongSoTienTangGiamThueXuatNhapKhau);
            this.grbMain.Controls.Add(this.txtTyGiaHoiDoaiSauKhiKhaiBoSung);
            this.grbMain.Controls.Add(this.txtTyGiaHoiDoaiTruocKhiKhaiBoSung);
            this.grbMain.Controls.Add(this.txtNamPhatHanh);
            this.grbMain.Controls.Add(this.txtNamPhatHanhHanMuc);
            this.grbMain.Controls.Add(this.txtSoToKhai);
            this.grbMain.Controls.Add(this.txtSoThongBao);
            this.grbMain.Controls.Add(this.txtSoToKhaiBoSung);
            this.grbMain.Controls.Add(this.txtMaNhanVienHaiQuan);
            this.grbMain.Controls.Add(this.txtSoQuanLyTrongNoiBoDoanhNghiep);
            this.grbMain.Controls.Add(this.txtSoChungTuBaoLanh);
            this.grbMain.Controls.Add(this.txtSoChungPhatHanhHanMuc);
            this.grbMain.Controls.Add(this.txtMaXacDinhThoiHanNopThue);
            this.grbMain.Controls.Add(this.txtMaTienTeTongSoTienTangGiamThueXuatNhapKhau);
            this.grbMain.Controls.Add(this.txtTongSoDongHangToKhaiBoSung);
            this.grbMain.Controls.Add(this.txtLyDo);
            this.grbMain.Controls.Add(this.txtTongSoTrangToKhaiBoSung);
            this.grbMain.Controls.Add(this.txtHienThiTongSoTienTangGiamThueXuatNhapKhau);
            this.grbMain.Controls.Add(this.txtKyHieuChungTuBaoLanh);
            this.grbMain.Controls.Add(this.txtKiHieuChungTuPhatHanhHanMuc);
            this.grbMain.Controls.Add(this.txtTenTruongDonViHaiQuan);
            this.grbMain.Controls.Add(this.txtTenNguoiPhuTrach);
            this.grbMain.Controls.Add(this.txtMaNganHangBaoLanh);
            this.grbMain.Controls.Add(this.txtMaNganHangTraThueThay);
            this.grbMain.Controls.Add(this.txtMaDaiLyHaiQuan);
            this.grbMain.Controls.Add(this.txtMaNguoiKhai);
            this.grbMain.Controls.Add(this.txtMaHetThoiHan);
            this.grbMain.Controls.Add(this.txtDauHieuBaoQua60Ngay);
            this.grbMain.Controls.Add(this.txtPhanLoaiXuatNhapKhau);
            this.grbMain.Controls.Add(this.txtMaBuuChinhNguoiKhai);
            this.grbMain.Controls.Add(this.label50);
            this.grbMain.Controls.Add(this.label59);
            this.grbMain.Controls.Add(this.txtDiaChiNguoiKhai);
            this.grbMain.Controls.Add(this.label60);
            this.grbMain.Controls.Add(this.dtNgayHieuLucChungTu);
            this.grbMain.Controls.Add(this.dtGioHoanThanhKiemTra);
            this.grbMain.Controls.Add(this.dtGioDangKyDuLieu);
            this.grbMain.Controls.Add(this.dtGioDangKyKhaiBoSung);
            this.grbMain.Controls.Add(this.dtNgayHoanThanhKiemTra);
            this.grbMain.Controls.Add(this.dtNgayKhaiBao);
            this.grbMain.Controls.Add(this.dtThoiHanTaiNhapTaiXuat);
            this.grbMain.Controls.Add(this.dtNgayCapPhep);
            this.grbMain.Controls.Add(this.label20);
            this.grbMain.Controls.Add(this.dtNgayDangKyDuLieu);
            this.grbMain.Controls.Add(this.dtNgayDangKyKhaiBoSung);
            this.grbMain.Controls.Add(this.label39);
            this.grbMain.Controls.Add(this.label21);
            this.grbMain.Controls.Add(this.label40);
            this.grbMain.Controls.Add(this.label38);
            this.grbMain.Controls.Add(this.label34);
            this.grbMain.Controls.Add(this.label33);
            this.grbMain.Controls.Add(this.label35);
            this.grbMain.Controls.Add(this.label36);
            this.grbMain.Controls.Add(this.label37);
            this.grbMain.Controls.Add(this.label14);
            this.grbMain.Controls.Add(this.label32);
            this.grbMain.Controls.Add(this.label23);
            this.grbMain.Controls.Add(this.label22);
            this.grbMain.Controls.Add(this.label25);
            this.grbMain.Controls.Add(this.label31);
            this.grbMain.Controls.Add(this.label18);
            this.grbMain.Controls.Add(this.label19);
            this.grbMain.Controls.Add(this.label30);
            this.grbMain.Controls.Add(this.label27);
            this.grbMain.Controls.Add(this.label28);
            this.grbMain.Controls.Add(this.label42);
            this.grbMain.Controls.Add(this.label17);
            this.grbMain.Controls.Add(this.label41);
            this.grbMain.Controls.Add(this.label26);
            this.grbMain.Controls.Add(this.label15);
            this.grbMain.Controls.Add(this.label16);
            this.grbMain.Controls.Add(this.label24);
            this.grbMain.Controls.Add(this.txtSoDienThoaiNguoiKhai);
            this.grbMain.Controls.Add(this.label12);
            this.grbMain.Controls.Add(this.ucPhanLoaiNopThue);
            this.grbMain.Controls.Add(this.ucMaTienTeSauKhiKhaiBoSung);
            this.grbMain.Controls.Add(this.ucMaTienTeTienThue);
            this.grbMain.Controls.Add(this.ucMaTienTeTruocKhiKhaiBoSung);
            this.grbMain.Controls.Add(this.ucMaLyDoKhaiBoSung);
            this.grbMain.Controls.Add(this.label7);
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.label4);
            this.grbMain.Controls.Add(this.label6);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.label10);
            this.grbMain.Controls.Add(this.label8);
            this.grbMain.Controls.Add(this.label9);
            this.grbMain.Controls.Add(this.label43);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung);
            this.grbMain.Controls.Add(this.txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung);
            this.grbMain.Controls.Add(this.txtTenNganHangBaoLanh);
            this.grbMain.Controls.Add(this.label11);
            this.grbMain.Controls.Add(this.txtTenNganHangTraThueThay);
            this.grbMain.Controls.Add(this.label13);
            this.grbMain.Controls.Add(this.txtTenDaiLyHaiQuan);
            this.grbMain.Controls.Add(this.txtTenNguoiKhai);
            this.grbMain.Controls.Add(this.label29);
            this.grbMain.Location = new System.Drawing.Point(203, 35);
            this.grbMain.Size = new System.Drawing.Size(760, 527);
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang,
            this.cmdLuu,
            this.cmdKhaiBao,
            this.cmdlayPhanHoi,
            this.cmdChiThiHaiQuan});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("7efa1b81-a632-4adb-89e9-9280c46f7b4f");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.Tag = null;
            this.cmbMain.TopRebar = this.cmdToolBar;
            this.cmbMain.VisualStyleManager = this.vsmMain;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdLuu1,
            this.Separator1,
            this.cmdKhaiBao1,
            this.Separator2,
            this.cmdChiThiHaiQuan1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(966, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdChiThiHaiQuan1
            // 
            this.cmdChiThiHaiQuan1.Key = "cmdChiThiHaiQuan";
            this.cmdChiThiHaiQuan1.Name = "cmdChiThiHaiQuan1";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThemHang.Icon")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLuu.Icon")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdKhaiBao.Icon")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdlayPhanHoi
            // 
            this.cmdlayPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdlayPhanHoi.Icon")));
            this.cmdlayPhanHoi.Key = "cmdlayPhanHoi";
            this.cmdlayPhanHoi.Name = "cmdlayPhanHoi";
            this.cmdlayPhanHoi.Text = "Lấy phản hồi";
            // 
            // cmdChiThiHaiQuan
            // 
            this.cmdChiThiHaiQuan.Image = ((System.Drawing.Image)(resources.GetObject("cmdChiThiHaiQuan.Image")));
            this.cmdChiThiHaiQuan.Key = "cmdChiThiHaiQuan";
            this.cmdChiThiHaiQuan.Name = "cmdChiThiHaiQuan";
            this.cmdChiThiHaiQuan.Text = "Chỉ thị Hải quan";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmdToolBar
            // 
            this.cmdToolBar.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdToolBar.CommandManager = this.cmbMain;
            this.cmdToolBar.Controls.Add(this.uiCommandBar1);
            this.cmdToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdToolBar.Location = new System.Drawing.Point(0, 0);
            this.cmdToolBar.Name = "cmdToolBar";
            this.cmdToolBar.Size = new System.Drawing.Size(966, 32);
            // 
            // txtMaNguoiKhai
            // 
            this.txtMaNguoiKhai.Location = new System.Drawing.Point(150, 214);
            this.txtMaNguoiKhai.Name = "txtMaNguoiKhai";
            this.txtMaNguoiKhai.Size = new System.Drawing.Size(114, 21);
            this.txtMaNguoiKhai.TabIndex = 16;
            this.txtMaNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // txtMaBuuChinhNguoiKhai
            // 
            this.txtMaBuuChinhNguoiKhai.Location = new System.Drawing.Point(150, 241);
            this.txtMaBuuChinhNguoiKhai.Name = "txtMaBuuChinhNguoiKhai";
            this.txtMaBuuChinhNguoiKhai.Size = new System.Drawing.Size(114, 21);
            this.txtMaBuuChinhNguoiKhai.TabIndex = 18;
            this.txtMaBuuChinhNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Location = new System.Drawing.Point(52, 272);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(39, 13);
            this.label50.TabIndex = 65;
            this.label50.Text = "Địa chỉ";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Location = new System.Drawing.Point(52, 246);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(71, 13);
            this.label59.TabIndex = 64;
            this.label59.Text = "Mã bưu chính";
            // 
            // txtDiaChiNguoiKhai
            // 
            this.txtDiaChiNguoiKhai.Location = new System.Drawing.Point(150, 268);
            this.txtDiaChiNguoiKhai.Name = "txtDiaChiNguoiKhai";
            this.txtDiaChiNguoiKhai.Size = new System.Drawing.Size(585, 21);
            this.txtDiaChiNguoiKhai.TabIndex = 19;
            this.txtDiaChiNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Location = new System.Drawing.Point(52, 300);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(56, 13);
            this.label60.TabIndex = 67;
            this.label60.Text = "Điện thoại";
            // 
            // txtSoDienThoaiNguoiKhai
            // 
            this.txtSoDienThoaiNguoiKhai.Location = new System.Drawing.Point(150, 295);
            this.txtSoDienThoaiNguoiKhai.Name = "txtSoDienThoaiNguoiKhai";
            this.txtSoDienThoaiNguoiKhai.Size = new System.Drawing.Size(114, 21);
            this.txtSoDienThoaiNguoiKhai.TabIndex = 20;
            this.txtSoDienThoaiNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(52, 218);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "Mã - tên";
            // 
            // txtTenNguoiKhai
            // 
            this.txtTenNguoiKhai.Location = new System.Drawing.Point(270, 214);
            this.txtTenNguoiKhai.Name = "txtTenNguoiKhai";
            this.txtTenNguoiKhai.Size = new System.Drawing.Size(465, 21);
            this.txtTenNguoiKhai.TabIndex = 17;
            this.txtTenNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // txtSoToKhaiBoSung
            // 
            this.txtSoToKhaiBoSung.DecimalDigits = 0;
            this.txtSoToKhaiBoSung.Location = new System.Drawing.Point(150, 38);
            this.txtSoToKhaiBoSung.MaxLength = 12;
            this.txtSoToKhaiBoSung.Name = "txtSoToKhaiBoSung";
            this.txtSoToKhaiBoSung.Size = new System.Drawing.Size(114, 21);
            this.txtSoToKhaiBoSung.TabIndex = 3;
            this.txtSoToKhaiBoSung.Text = "0";
            this.txtSoToKhaiBoSung.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhaiBoSung.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(19, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Số tờ khai bổ sung";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(357, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 63;
            this.label2.Text = "Ngày đăng ký";
            // 
            // dtNgayDangKyKhaiBoSung
            // 
            // 
            // 
            // 
            this.dtNgayDangKyKhaiBoSung.DropDownCalendar.Name = "";
            this.dtNgayDangKyKhaiBoSung.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayDangKyKhaiBoSung.Enabled = false;
            this.dtNgayDangKyKhaiBoSung.IsNullDate = true;
            this.dtNgayDangKyKhaiBoSung.Location = new System.Drawing.Point(439, 38);
            this.dtNgayDangKyKhaiBoSung.Name = "dtNgayDangKyKhaiBoSung";
            this.dtNgayDangKyKhaiBoSung.Size = new System.Drawing.Size(100, 21);
            this.dtNgayDangKyKhaiBoSung.TabIndex = 4;
            this.dtNgayDangKyKhaiBoSung.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtGioDangKyKhaiBoSung
            // 
            this.dtGioDangKyKhaiBoSung.CustomFormat = "hh:mm:ss";
            this.dtGioDangKyKhaiBoSung.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtGioDangKyKhaiBoSung.DropDownCalendar.Name = "";
            this.dtGioDangKyKhaiBoSung.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtGioDangKyKhaiBoSung.Enabled = false;
            this.dtGioDangKyKhaiBoSung.IsNullDate = true;
            this.dtGioDangKyKhaiBoSung.Location = new System.Drawing.Point(545, 38);
            this.dtGioDangKyKhaiBoSung.Name = "dtGioDangKyKhaiBoSung";
            this.dtGioDangKyKhaiBoSung.Size = new System.Drawing.Size(79, 21);
            this.dtGioDangKyKhaiBoSung.TabIndex = 5;
            this.dtGioDangKyKhaiBoSung.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(19, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Số thông báo";
            // 
            // txtSoThongBao
            // 
            this.txtSoThongBao.DecimalDigits = 0;
            this.txtSoThongBao.Enabled = false;
            this.txtSoThongBao.Location = new System.Drawing.Point(150, 11);
            this.txtSoThongBao.MaxLength = 12;
            this.txtSoThongBao.Name = "txtSoThongBao";
            this.txtSoThongBao.Size = new System.Drawing.Size(114, 21);
            this.txtSoThongBao.TabIndex = 0;
            this.txtSoThongBao.Text = "0";
            this.txtSoThongBao.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoThongBao.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(299, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 13);
            this.label4.TabIndex = 63;
            this.label4.Text = "Ngày hoàn thành kiểm tra";
            // 
            // dtNgayHoanThanhKiemTra
            // 
            // 
            // 
            // 
            this.dtNgayHoanThanhKiemTra.DropDownCalendar.Name = "";
            this.dtNgayHoanThanhKiemTra.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayHoanThanhKiemTra.Enabled = false;
            this.dtNgayHoanThanhKiemTra.IsNullDate = true;
            this.dtNgayHoanThanhKiemTra.Location = new System.Drawing.Point(439, 11);
            this.dtNgayHoanThanhKiemTra.Name = "dtNgayHoanThanhKiemTra";
            this.dtNgayHoanThanhKiemTra.Size = new System.Drawing.Size(100, 21);
            this.dtNgayHoanThanhKiemTra.TabIndex = 1;
            this.dtNgayHoanThanhKiemTra.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtGioHoanThanhKiemTra
            // 
            this.dtGioHoanThanhKiemTra.CustomFormat = "hh:mm:ss";
            this.dtGioHoanThanhKiemTra.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtGioHoanThanhKiemTra.DropDownCalendar.Name = "";
            this.dtGioHoanThanhKiemTra.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtGioHoanThanhKiemTra.Enabled = false;
            this.dtGioHoanThanhKiemTra.IsNullDate = true;
            this.dtGioHoanThanhKiemTra.Location = new System.Drawing.Point(545, 11);
            this.dtGioHoanThanhKiemTra.Name = "dtGioHoanThanhKiemTra";
            this.dtGioHoanThanhKiemTra.Size = new System.Drawing.Size(79, 21);
            this.dtGioHoanThanhKiemTra.TabIndex = 2;
            this.dtGioHoanThanhKiemTra.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(19, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Cơ quan Hải quan";
            // 
            // ucCoQuanHaiQuan
            // 
            this.ucCoQuanHaiQuan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucCoQuanHaiQuan.Appearance.Options.UseBackColor = true;
            this.ucCoQuanHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ucCoQuanHaiQuan.Code = "";
            this.ucCoQuanHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCoQuanHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCoQuanHaiQuan.IsValidate = true;
            this.ucCoQuanHaiQuan.Location = new System.Drawing.Point(150, 65);
            this.ucCoQuanHaiQuan.Name = "ucCoQuanHaiQuan";
            this.ucCoQuanHaiQuan.Name_VN = "";
            this.ucCoQuanHaiQuan.SetValidate = false;
            this.ucCoQuanHaiQuan.ShowColumnCode = true;
            this.ucCoQuanHaiQuan.ShowColumnName = true;
            this.ucCoQuanHaiQuan.Size = new System.Drawing.Size(114, 26);
            this.ucCoQuanHaiQuan.TabIndex = 6;
            this.ucCoQuanHaiQuan.TagName = "";
            this.ucCoQuanHaiQuan.WhereCondition = "";
            this.ucCoQuanHaiQuan.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            this.ucCoQuanHaiQuan.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(this.ucCoQuanHaiQuan_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(338, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "Nhóm xử lý hồ sơ";
            // 
            // ucNhomXuLyHoSo
            // 
            this.ucNhomXuLyHoSo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucNhomXuLyHoSo.Appearance.Options.UseBackColor = true;
            this.ucNhomXuLyHoSo.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A014;
            this.ucNhomXuLyHoSo.Code = "";
            this.ucNhomXuLyHoSo.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucNhomXuLyHoSo.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucNhomXuLyHoSo.IsValidate = true;
            this.ucNhomXuLyHoSo.Location = new System.Drawing.Point(439, 65);
            this.ucNhomXuLyHoSo.Name = "ucNhomXuLyHoSo";
            this.ucNhomXuLyHoSo.Name_VN = "";
            this.ucNhomXuLyHoSo.SetValidate = false;
            this.ucNhomXuLyHoSo.ShowColumnCode = true;
            this.ucNhomXuLyHoSo.ShowColumnName = true;
            this.ucNhomXuLyHoSo.Size = new System.Drawing.Size(296, 26);
            this.ucNhomXuLyHoSo.TabIndex = 7;
            this.ucNhomXuLyHoSo.TagName = "";
            this.ucNhomXuLyHoSo.WhereCondition = "";
            this.ucNhomXuLyHoSo.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(this.ucCoQuanHaiQuan_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(19, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(251, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "Phân loại xuất nhập khẩu - Số tờ khai - Mã loại hình";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.DecimalDigits = 0;
            this.txtSoToKhai.Location = new System.Drawing.Point(191, 117);
            this.txtSoToKhai.MaxLength = 12;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(114, 21);
            this.txtSoToKhai.TabIndex = 9;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // txtPhanLoaiXuatNhapKhau
            // 
            this.txtPhanLoaiXuatNhapKhau.Location = new System.Drawing.Point(150, 117);
            this.txtPhanLoaiXuatNhapKhau.Name = "txtPhanLoaiXuatNhapKhau";
            this.txtPhanLoaiXuatNhapKhau.Size = new System.Drawing.Size(35, 21);
            this.txtPhanLoaiXuatNhapKhau.TabIndex = 8;
            this.txtPhanLoaiXuatNhapKhau.VisualStyleManager = this.vsmMain;
            this.txtPhanLoaiXuatNhapKhau.TextChanged += new System.EventHandler(this.txtPhanLoaiXuatNhapKhau_TextChanged);
            // 
            // ucMaLoaiHinh
            // 
            this.ucMaLoaiHinh.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaLoaiHinh.Appearance.Options.UseBackColor = true;
            this.ucMaLoaiHinh.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E001;
            this.ucMaLoaiHinh.Code = "";
            this.ucMaLoaiHinh.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaLoaiHinh.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaLoaiHinh.IsValidate = true;
            this.ucMaLoaiHinh.Location = new System.Drawing.Point(311, 114);
            this.ucMaLoaiHinh.Name = "ucMaLoaiHinh";
            this.ucMaLoaiHinh.Name_VN = "";
            this.ucMaLoaiHinh.SetValidate = false;
            this.ucMaLoaiHinh.ShowColumnCode = true;
            this.ucMaLoaiHinh.ShowColumnName = true;
            this.ucMaLoaiHinh.Size = new System.Drawing.Size(73, 26);
            this.ucMaLoaiHinh.TabIndex = 10;
            this.ucMaLoaiHinh.TagName = "";
            this.ucMaLoaiHinh.WhereCondition = "";
            this.ucMaLoaiHinh.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            this.ucMaLoaiHinh.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(this.ucCoQuanHaiQuan_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(19, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(275, 13);
            this.label8.TabIndex = 63;
            this.label8.Text = "Ngày khai báo - Dấu hiệu báo quá 60 ngày - Mã hết hạn";
            // 
            // dtNgayKhaiBao
            // 
            // 
            // 
            // 
            this.dtNgayKhaiBao.DropDownCalendar.Name = "";
            this.dtNgayKhaiBao.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayKhaiBao.Enabled = false;
            this.dtNgayKhaiBao.IsNullDate = true;
            this.dtNgayKhaiBao.Location = new System.Drawing.Point(150, 167);
            this.dtNgayKhaiBao.Name = "dtNgayKhaiBao";
            this.dtNgayKhaiBao.Size = new System.Drawing.Size(114, 21);
            this.dtNgayKhaiBao.TabIndex = 11;
            this.dtNgayKhaiBao.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtDauHieuBaoQua60Ngay
            // 
            this.txtDauHieuBaoQua60Ngay.Enabled = false;
            this.txtDauHieuBaoQua60Ngay.Location = new System.Drawing.Point(270, 167);
            this.txtDauHieuBaoQua60Ngay.Name = "txtDauHieuBaoQua60Ngay";
            this.txtDauHieuBaoQua60Ngay.Size = new System.Drawing.Size(35, 21);
            this.txtDauHieuBaoQua60Ngay.TabIndex = 12;
            this.txtDauHieuBaoQua60Ngay.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHetThoiHan
            // 
            this.txtMaHetThoiHan.Enabled = false;
            this.txtMaHetThoiHan.Location = new System.Drawing.Point(311, 167);
            this.txtMaHetThoiHan.Name = "txtMaHetThoiHan";
            this.txtMaHetThoiHan.Size = new System.Drawing.Size(35, 21);
            this.txtMaHetThoiHan.TabIndex = 13;
            this.txtMaHetThoiHan.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(436, 151);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 63;
            this.label9.Text = "Ngày cấp phép";
            // 
            // dtNgayCapPhep
            // 
            // 
            // 
            // 
            this.dtNgayCapPhep.DropDownCalendar.Name = "";
            this.dtNgayCapPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayCapPhep.IsNullDate = true;
            this.dtNgayCapPhep.Location = new System.Drawing.Point(439, 167);
            this.dtNgayCapPhep.Name = "dtNgayCapPhep";
            this.dtNgayCapPhep.Size = new System.Drawing.Size(100, 21);
            this.dtNgayCapPhep.TabIndex = 14;
            this.dtNgayCapPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(601, 151);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 13);
            this.label10.TabIndex = 63;
            this.label10.Text = "Thời hạn tái nhập/ tái xuất";
            // 
            // dtThoiHanTaiNhapTaiXuat
            // 
            // 
            // 
            // 
            this.dtThoiHanTaiNhapTaiXuat.DropDownCalendar.Name = "";
            this.dtThoiHanTaiNhapTaiXuat.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtThoiHanTaiNhapTaiXuat.IsNullDate = true;
            this.dtThoiHanTaiNhapTaiXuat.Location = new System.Drawing.Point(635, 167);
            this.dtThoiHanTaiNhapTaiXuat.Name = "dtThoiHanTaiNhapTaiXuat";
            this.dtThoiHanTaiNhapTaiXuat.Size = new System.Drawing.Size(100, 21);
            this.dtThoiHanTaiNhapTaiXuat.TabIndex = 15;
            this.dtThoiHanTaiNhapTaiXuat.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(19, 196);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 34;
            this.label11.Text = "Người khai";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(19, 332);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 13);
            this.label12.TabIndex = 37;
            this.label12.Text = "Đại lý Hải quan";
            // 
            // txtTenDaiLyHaiQuan
            // 
            this.txtTenDaiLyHaiQuan.Enabled = false;
            this.txtTenDaiLyHaiQuan.Location = new System.Drawing.Point(270, 327);
            this.txtTenDaiLyHaiQuan.Name = "txtTenDaiLyHaiQuan";
            this.txtTenDaiLyHaiQuan.Size = new System.Drawing.Size(465, 21);
            this.txtTenDaiLyHaiQuan.TabIndex = 22;
            this.txtTenDaiLyHaiQuan.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDaiLyHaiQuan
            // 
            this.txtMaDaiLyHaiQuan.Enabled = false;
            this.txtMaDaiLyHaiQuan.Location = new System.Drawing.Point(150, 327);
            this.txtMaDaiLyHaiQuan.Name = "txtMaDaiLyHaiQuan";
            this.txtMaDaiLyHaiQuan.Size = new System.Drawing.Size(114, 21);
            this.txtMaDaiLyHaiQuan.TabIndex = 21;
            this.txtMaDaiLyHaiQuan.VisualStyleManager = this.vsmMain;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(19, 359);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(116, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "Mã nhân viên Hải quan";
            // 
            // txtMaNhanVienHaiQuan
            // 
            this.txtMaNhanVienHaiQuan.Enabled = false;
            this.txtMaNhanVienHaiQuan.Location = new System.Drawing.Point(150, 354);
            this.txtMaNhanVienHaiQuan.Name = "txtMaNhanVienHaiQuan";
            this.txtMaNhanVienHaiQuan.Size = new System.Drawing.Size(114, 21);
            this.txtMaNhanVienHaiQuan.TabIndex = 23;
            this.txtMaNhanVienHaiQuan.VisualStyleManager = this.vsmMain;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(19, 386);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(110, 13);
            this.label18.TabIndex = 37;
            this.label18.Text = "Mã lý do khai bổ sung";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(334, 386);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 13);
            this.label20.TabIndex = 37;
            this.label20.Text = "Phân loại nộp thuế";
            // 
            // ucMaLyDoKhaiBoSung
            // 
            this.ucMaLyDoKhaiBoSung.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaLyDoKhaiBoSung.Appearance.Options.UseBackColor = true;
            this.ucMaLyDoKhaiBoSung.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E033;
            this.ucMaLyDoKhaiBoSung.Code = "";
            this.ucMaLyDoKhaiBoSung.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaLyDoKhaiBoSung.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaLyDoKhaiBoSung.IsValidate = true;
            this.ucMaLyDoKhaiBoSung.Location = new System.Drawing.Point(150, 381);
            this.ucMaLyDoKhaiBoSung.Name = "ucMaLyDoKhaiBoSung";
            this.ucMaLyDoKhaiBoSung.Name_VN = "";
            this.ucMaLyDoKhaiBoSung.SetValidate = false;
            this.ucMaLyDoKhaiBoSung.ShowColumnCode = true;
            this.ucMaLyDoKhaiBoSung.ShowColumnName = true;
            this.ucMaLyDoKhaiBoSung.Size = new System.Drawing.Size(114, 26);
            this.ucMaLyDoKhaiBoSung.TabIndex = 25;
            this.ucMaLyDoKhaiBoSung.TagName = "";
            this.ucMaLyDoKhaiBoSung.WhereCondition = "";
            this.ucMaLyDoKhaiBoSung.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucPhanLoaiNopThue
            // 
            this.ucPhanLoaiNopThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucPhanLoaiNopThue.Appearance.Options.UseBackColor = true;
            this.ucPhanLoaiNopThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E021;
            this.ucPhanLoaiNopThue.Code = "";
            this.ucPhanLoaiNopThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucPhanLoaiNopThue.Enabled = false;
            this.ucPhanLoaiNopThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucPhanLoaiNopThue.IsValidate = true;
            this.ucPhanLoaiNopThue.Location = new System.Drawing.Point(439, 381);
            this.ucPhanLoaiNopThue.Name = "ucPhanLoaiNopThue";
            this.ucPhanLoaiNopThue.Name_VN = "";
            this.ucPhanLoaiNopThue.SetValidate = false;
            this.ucPhanLoaiNopThue.ShowColumnCode = true;
            this.ucPhanLoaiNopThue.ShowColumnName = true;
            this.ucPhanLoaiNopThue.Size = new System.Drawing.Size(137, 26);
            this.ucPhanLoaiNopThue.TabIndex = 26;
            this.ucPhanLoaiNopThue.TagName = "";
            this.ucPhanLoaiNopThue.WhereCondition = "";
            this.ucPhanLoaiNopThue.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtSoQuanLyTrongNoiBoDoanhNghiep
            // 
            this.txtSoQuanLyTrongNoiBoDoanhNghiep.Location = new System.Drawing.Point(270, 413);
            this.txtSoQuanLyTrongNoiBoDoanhNghiep.Name = "txtSoQuanLyTrongNoiBoDoanhNghiep";
            this.txtSoQuanLyTrongNoiBoDoanhNghiep.Size = new System.Drawing.Size(306, 21);
            this.txtSoQuanLyTrongNoiBoDoanhNghiep.TabIndex = 27;
            this.txtSoQuanLyTrongNoiBoDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(19, 417);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(186, 13);
            this.label21.TabIndex = 37;
            this.label21.Text = "Số quản lý trong nội bộ doanh nghiệp";
            // 
            // ucMaTienTeTienThue
            // 
            this.ucMaTienTeTienThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaTienTeTienThue.Appearance.Options.UseBackColor = true;
            this.ucMaTienTeTienThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ucMaTienTeTienThue.Code = "";
            this.ucMaTienTeTienThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaTienTeTienThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaTienTeTienThue.IsValidate = true;
            this.ucMaTienTeTienThue.Location = new System.Drawing.Point(439, 354);
            this.ucMaTienTeTienThue.Name = "ucMaTienTeTienThue";
            this.ucMaTienTeTienThue.Name_VN = "";
            this.ucMaTienTeTienThue.SetValidate = false;
            this.ucMaTienTeTienThue.ShowColumnCode = true;
            this.ucMaTienTeTienThue.ShowColumnName = false;
            this.ucMaTienTeTienThue.Size = new System.Drawing.Size(137, 26);
            this.ucMaTienTeTienThue.TabIndex = 24;
            this.ucMaTienTeTienThue.TagName = "";
            this.ucMaTienTeTienThue.WhereCondition = "";
            this.ucMaTienTeTienThue.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(308, 359);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(121, 13);
            this.label22.TabIndex = 37;
            this.label22.Text = "Mã tiền tệ của tiền thuế";
            // 
            // txtTenNganHangTraThueThay
            // 
            this.txtTenNganHangTraThueThay.Location = new System.Drawing.Point(270, 440);
            this.txtTenNganHangTraThueThay.Name = "txtTenNganHangTraThueThay";
            this.txtTenNganHangTraThueThay.Size = new System.Drawing.Size(465, 21);
            this.txtTenNganHangTraThueThay.TabIndex = 29;
            this.txtTenNganHangTraThueThay.VisualStyleManager = this.vsmMain;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(12, 444);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(142, 13);
            this.label24.TabIndex = 37;
            this.label24.Text = "Mã ngân hàng trả thuế thay";
            // 
            // txtMaNganHangTraThueThay
            // 
            this.txtMaNganHangTraThueThay.Location = new System.Drawing.Point(150, 440);
            this.txtMaNganHangTraThueThay.Name = "txtMaNganHangTraThueThay";
            this.txtMaNganHangTraThueThay.Size = new System.Drawing.Size(114, 21);
            this.txtMaNganHangTraThueThay.TabIndex = 28;
            this.txtMaNganHangTraThueThay.VisualStyleManager = this.vsmMain;
            // 
            // txtNamPhatHanhHanMuc
            // 
            this.txtNamPhatHanhHanMuc.DecimalDigits = 0;
            this.txtNamPhatHanhHanMuc.Location = new System.Drawing.Point(150, 467);
            this.txtNamPhatHanhHanMuc.MaxLength = 4;
            this.txtNamPhatHanhHanMuc.Name = "txtNamPhatHanhHanMuc";
            this.txtNamPhatHanhHanMuc.Size = new System.Drawing.Size(114, 21);
            this.txtNamPhatHanhHanMuc.TabIndex = 30;
            this.txtNamPhatHanhHanMuc.Text = "0";
            this.txtNamPhatHanhHanMuc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtNamPhatHanhHanMuc.VisualStyleManager = this.vsmMain;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(19, 471);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(124, 13);
            this.label25.TabIndex = 37;
            this.label25.Text = "Năm phát hành hạn mức";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Location = new System.Drawing.Point(301, 471);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(129, 13);
            this.label26.TabIndex = 37;
            this.label26.Text = "Kí hiệu chứng từ hạn mức";
            // 
            // txtKiHieuChungTuPhatHanhHanMuc
            // 
            this.txtKiHieuChungTuPhatHanhHanMuc.Location = new System.Drawing.Point(439, 467);
            this.txtKiHieuChungTuPhatHanhHanMuc.Name = "txtKiHieuChungTuPhatHanhHanMuc";
            this.txtKiHieuChungTuPhatHanhHanMuc.Size = new System.Drawing.Size(100, 21);
            this.txtKiHieuChungTuPhatHanhHanMuc.TabIndex = 31;
            this.txtKiHieuChungTuPhatHanhHanMuc.VisualStyleManager = this.vsmMain;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Location = new System.Drawing.Point(542, 471);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(110, 13);
            this.label27.TabIndex = 37;
            this.label27.Text = "Số chứng từ hạn mức";
            // 
            // txtSoChungPhatHanhHanMuc
            // 
            this.txtSoChungPhatHanhHanMuc.Location = new System.Drawing.Point(651, 467);
            this.txtSoChungPhatHanhHanMuc.Name = "txtSoChungPhatHanhHanMuc";
            this.txtSoChungPhatHanhHanMuc.Size = new System.Drawing.Size(84, 21);
            this.txtSoChungPhatHanhHanMuc.TabIndex = 32;
            this.txtSoChungPhatHanhHanMuc.VisualStyleManager = this.vsmMain;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Location = new System.Drawing.Point(12, 498);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(152, 13);
            this.label28.TabIndex = 37;
            this.label28.Text = "Mã xác định thời hạn nộp thuế";
            // 
            // txtMaXacDinhThoiHanNopThue
            // 
            this.txtMaXacDinhThoiHanNopThue.Location = new System.Drawing.Point(170, 494);
            this.txtMaXacDinhThoiHanNopThue.Name = "txtMaXacDinhThoiHanNopThue";
            this.txtMaXacDinhThoiHanNopThue.Size = new System.Drawing.Size(94, 21);
            this.txtMaXacDinhThoiHanNopThue.TabIndex = 33;
            this.txtMaXacDinhThoiHanNopThue.VisualStyleManager = this.vsmMain;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Location = new System.Drawing.Point(290, 498);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(139, 13);
            this.label29.TabIndex = 59;
            this.label29.Text = "Ngày hiệu lực của chứng từ";
            // 
            // dtNgayHieuLucChungTu
            // 
            // 
            // 
            // 
            this.dtNgayHieuLucChungTu.DropDownCalendar.Name = "";
            this.dtNgayHieuLucChungTu.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayHieuLucChungTu.Enabled = false;
            this.dtNgayHieuLucChungTu.IsNullDate = true;
            this.dtNgayHieuLucChungTu.Location = new System.Drawing.Point(439, 494);
            this.dtNgayHieuLucChungTu.Name = "dtNgayHieuLucChungTu";
            this.dtNgayHieuLucChungTu.Size = new System.Drawing.Size(100, 21);
            this.dtNgayHieuLucChungTu.TabIndex = 34;
            this.dtNgayHieuLucChungTu.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Location = new System.Drawing.Point(19, 525);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(94, 13);
            this.label30.TabIndex = 37;
            this.label30.Text = "Thời hạn nộp thuế";
            // 
            // txtSoNgayNopThue
            // 
            this.txtSoNgayNopThue.DecimalDigits = 0;
            this.txtSoNgayNopThue.Enabled = false;
            this.txtSoNgayNopThue.Location = new System.Drawing.Point(150, 521);
            this.txtSoNgayNopThue.MaxLength = 3;
            this.txtSoNgayNopThue.Name = "txtSoNgayNopThue";
            this.txtSoNgayNopThue.Size = new System.Drawing.Size(114, 21);
            this.txtSoNgayNopThue.TabIndex = 35;
            this.txtSoNgayNopThue.Text = "0";
            this.txtSoNgayNopThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoNgayNopThue.VisualStyleManager = this.vsmMain;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Location = new System.Drawing.Point(308, 525);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(123, 13);
            this.label31.TabIndex = 37;
            this.label31.Text = "Thời hạn nộp thuế GTGT";
            // 
            // txtSoNgayNopThueDanhChoVATHangHoaDacBiet
            // 
            this.txtSoNgayNopThueDanhChoVATHangHoaDacBiet.DecimalDigits = 0;
            this.txtSoNgayNopThueDanhChoVATHangHoaDacBiet.Enabled = false;
            this.txtSoNgayNopThueDanhChoVATHangHoaDacBiet.Location = new System.Drawing.Point(439, 521);
            this.txtSoNgayNopThueDanhChoVATHangHoaDacBiet.MaxLength = 3;
            this.txtSoNgayNopThueDanhChoVATHangHoaDacBiet.Name = "txtSoNgayNopThueDanhChoVATHangHoaDacBiet";
            this.txtSoNgayNopThueDanhChoVATHangHoaDacBiet.Size = new System.Drawing.Size(100, 21);
            this.txtSoNgayNopThueDanhChoVATHangHoaDacBiet.TabIndex = 36;
            this.txtSoNgayNopThueDanhChoVATHangHoaDacBiet.Text = "0";
            this.txtSoNgayNopThueDanhChoVATHangHoaDacBiet.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // txtTenNganHangBaoLanh
            // 
            this.txtTenNganHangBaoLanh.Location = new System.Drawing.Point(270, 548);
            this.txtTenNganHangBaoLanh.Name = "txtTenNganHangBaoLanh";
            this.txtTenNganHangBaoLanh.Size = new System.Drawing.Size(465, 21);
            this.txtTenNganHangBaoLanh.TabIndex = 38;
            this.txtTenNganHangBaoLanh.VisualStyleManager = this.vsmMain;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(12, 552);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Mã ngân hàng bảo lãnh";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(301, 579);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(129, 13);
            this.label17.TabIndex = 37;
            this.label17.Text = "Kí hiệu chứng từ bảo lãnh";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(542, 579);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 13);
            this.label19.TabIndex = 37;
            this.label19.Text = "Số chứng từ bảo lãnh";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Location = new System.Drawing.Point(19, 579);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(124, 13);
            this.label23.TabIndex = 37;
            this.label23.Text = "Năm phát hành bảo lãnh";
            // 
            // txtMaNganHangBaoLanh
            // 
            this.txtMaNganHangBaoLanh.Location = new System.Drawing.Point(150, 548);
            this.txtMaNganHangBaoLanh.Name = "txtMaNganHangBaoLanh";
            this.txtMaNganHangBaoLanh.Size = new System.Drawing.Size(114, 21);
            this.txtMaNganHangBaoLanh.TabIndex = 37;
            this.txtMaNganHangBaoLanh.VisualStyleManager = this.vsmMain;
            // 
            // txtKyHieuChungTuBaoLanh
            // 
            this.txtKyHieuChungTuBaoLanh.Location = new System.Drawing.Point(439, 575);
            this.txtKyHieuChungTuBaoLanh.Name = "txtKyHieuChungTuBaoLanh";
            this.txtKyHieuChungTuBaoLanh.Size = new System.Drawing.Size(100, 21);
            this.txtKyHieuChungTuBaoLanh.TabIndex = 40;
            this.txtKyHieuChungTuBaoLanh.VisualStyleManager = this.vsmMain;
            // 
            // txtSoChungTuBaoLanh
            // 
            this.txtSoChungTuBaoLanh.Location = new System.Drawing.Point(651, 575);
            this.txtSoChungTuBaoLanh.Name = "txtSoChungTuBaoLanh";
            this.txtSoChungTuBaoLanh.Size = new System.Drawing.Size(84, 21);
            this.txtSoChungTuBaoLanh.TabIndex = 41;
            this.txtSoChungTuBaoLanh.VisualStyleManager = this.vsmMain;
            // 
            // txtNamPhatHanh
            // 
            this.txtNamPhatHanh.DecimalDigits = 0;
            this.txtNamPhatHanh.Location = new System.Drawing.Point(150, 575);
            this.txtNamPhatHanh.MaxLength = 4;
            this.txtNamPhatHanh.Name = "txtNamPhatHanh";
            this.txtNamPhatHanh.Size = new System.Drawing.Size(114, 21);
            this.txtNamPhatHanh.TabIndex = 39;
            this.txtNamPhatHanh.Text = "0";
            this.txtNamPhatHanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtNamPhatHanh.VisualStyleManager = this.vsmMain;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Location = new System.Drawing.Point(133, 616);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(213, 13);
            this.label32.TabIndex = 37;
            this.label32.Text = "Mã tiền tệ trị giá khai báo - Tỷ giá tính thuế";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Location = new System.Drawing.Point(19, 639);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(102, 13);
            this.label33.TabIndex = 37;
            this.label33.Text = "(Trước khi khai báo)";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Location = new System.Drawing.Point(19, 671);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(92, 13);
            this.label34.TabIndex = 37;
            this.label34.Text = "(Sau khi khai báo)";
            // 
            // ucMaTienTeTruocKhiKhaiBoSung
            // 
            this.ucMaTienTeTruocKhiKhaiBoSung.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaTienTeTruocKhiKhaiBoSung.Appearance.Options.UseBackColor = true;
            this.ucMaTienTeTruocKhiKhaiBoSung.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ucMaTienTeTruocKhiKhaiBoSung.Code = "";
            this.ucMaTienTeTruocKhiKhaiBoSung.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaTienTeTruocKhiKhaiBoSung.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaTienTeTruocKhiKhaiBoSung.IsValidate = true;
            this.ucMaTienTeTruocKhiKhaiBoSung.Location = new System.Drawing.Point(150, 632);
            this.ucMaTienTeTruocKhiKhaiBoSung.Name = "ucMaTienTeTruocKhiKhaiBoSung";
            this.ucMaTienTeTruocKhiKhaiBoSung.Name_VN = "";
            this.ucMaTienTeTruocKhiKhaiBoSung.SetValidate = false;
            this.ucMaTienTeTruocKhiKhaiBoSung.ShowColumnCode = true;
            this.ucMaTienTeTruocKhiKhaiBoSung.ShowColumnName = false;
            this.ucMaTienTeTruocKhiKhaiBoSung.Size = new System.Drawing.Size(55, 26);
            this.ucMaTienTeTruocKhiKhaiBoSung.TabIndex = 42;
            this.ucMaTienTeTruocKhiKhaiBoSung.TagName = "";
            this.ucMaTienTeTruocKhiKhaiBoSung.WhereCondition = "";
            this.ucMaTienTeTruocKhiKhaiBoSung.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtTyGiaHoiDoaiTruocKhiKhaiBoSung
            // 
            this.txtTyGiaHoiDoaiTruocKhiKhaiBoSung.DecimalDigits = 0;
            this.txtTyGiaHoiDoaiTruocKhiKhaiBoSung.Location = new System.Drawing.Point(211, 635);
            this.txtTyGiaHoiDoaiTruocKhiKhaiBoSung.MaxLength = 9;
            this.txtTyGiaHoiDoaiTruocKhiKhaiBoSung.Name = "txtTyGiaHoiDoaiTruocKhiKhaiBoSung";
            this.txtTyGiaHoiDoaiTruocKhiKhaiBoSung.Size = new System.Drawing.Size(114, 21);
            this.txtTyGiaHoiDoaiTruocKhiKhaiBoSung.TabIndex = 43;
            this.txtTyGiaHoiDoaiTruocKhiKhaiBoSung.Text = "0";
            this.txtTyGiaHoiDoaiTruocKhiKhaiBoSung.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTyGiaHoiDoaiTruocKhiKhaiBoSung.VisualStyleManager = this.vsmMain;
            // 
            // ucMaTienTeSauKhiKhaiBoSung
            // 
            this.ucMaTienTeSauKhiKhaiBoSung.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaTienTeSauKhiKhaiBoSung.Appearance.Options.UseBackColor = true;
            this.ucMaTienTeSauKhiKhaiBoSung.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ucMaTienTeSauKhiKhaiBoSung.Code = "";
            this.ucMaTienTeSauKhiKhaiBoSung.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaTienTeSauKhiKhaiBoSung.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaTienTeSauKhiKhaiBoSung.IsValidate = true;
            this.ucMaTienTeSauKhiKhaiBoSung.Location = new System.Drawing.Point(150, 664);
            this.ucMaTienTeSauKhiKhaiBoSung.Name = "ucMaTienTeSauKhiKhaiBoSung";
            this.ucMaTienTeSauKhiKhaiBoSung.Name_VN = "";
            this.ucMaTienTeSauKhiKhaiBoSung.SetValidate = false;
            this.ucMaTienTeSauKhiKhaiBoSung.ShowColumnCode = true;
            this.ucMaTienTeSauKhiKhaiBoSung.ShowColumnName = false;
            this.ucMaTienTeSauKhiKhaiBoSung.Size = new System.Drawing.Size(55, 26);
            this.ucMaTienTeSauKhiKhaiBoSung.TabIndex = 45;
            this.ucMaTienTeSauKhiKhaiBoSung.TagName = "";
            this.ucMaTienTeSauKhiKhaiBoSung.WhereCondition = "";
            this.ucMaTienTeSauKhiKhaiBoSung.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtTyGiaHoiDoaiSauKhiKhaiBoSung
            // 
            this.txtTyGiaHoiDoaiSauKhiKhaiBoSung.DecimalDigits = 0;
            this.txtTyGiaHoiDoaiSauKhiKhaiBoSung.Location = new System.Drawing.Point(211, 667);
            this.txtTyGiaHoiDoaiSauKhiKhaiBoSung.MaxLength = 9;
            this.txtTyGiaHoiDoaiSauKhiKhaiBoSung.Name = "txtTyGiaHoiDoaiSauKhiKhaiBoSung";
            this.txtTyGiaHoiDoaiSauKhiKhaiBoSung.Size = new System.Drawing.Size(114, 21);
            this.txtTyGiaHoiDoaiSauKhiKhaiBoSung.TabIndex = 46;
            this.txtTyGiaHoiDoaiSauKhiKhaiBoSung.Text = "0";
            this.txtTyGiaHoiDoaiSauKhiKhaiBoSung.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTyGiaHoiDoaiSauKhiKhaiBoSung.VisualStyleManager = this.vsmMain;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Location = new System.Drawing.Point(523, 616);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(68, 13);
            this.label35.TabIndex = 37;
            this.label35.Text = "Phần ghi chú";
            // 
            // txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung
            // 
            this.txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.Location = new System.Drawing.Point(331, 635);
            this.txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.Name = "txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung";
            this.txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.Size = new System.Drawing.Size(404, 21);
            this.txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.TabIndex = 44;
            this.txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung
            // 
            this.txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.Location = new System.Drawing.Point(331, 667);
            this.txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.Name = "txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung";
            this.txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.Size = new System.Drawing.Size(404, 21);
            this.txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.TabIndex = 47;
            this.txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(12, 702);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(409, 13);
            this.label14.TabIndex = 37;
            this.label14.Text = "Tổng số tiền tăng/ giảm thuế xuất nhập khẩu: Mã hiển thị - Tổng số tiền - Mã tiền" +
                " tệ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.SystemColors.Control;
            this.label36.Location = new System.Drawing.Point(713, 1040);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(22, 13);
            this.label36.TabIndex = 37;
            this.label36.Text = "     ";
            // 
            // txtTongSoTienTangGiamThueXuatNhapKhau
            // 
            this.txtTongSoTienTangGiamThueXuatNhapKhau.DecimalDigits = 0;
            this.txtTongSoTienTangGiamThueXuatNhapKhau.Enabled = false;
            this.txtTongSoTienTangGiamThueXuatNhapKhau.Location = new System.Drawing.Point(211, 718);
            this.txtTongSoTienTangGiamThueXuatNhapKhau.MaxLength = 9;
            this.txtTongSoTienTangGiamThueXuatNhapKhau.Name = "txtTongSoTienTangGiamThueXuatNhapKhau";
            this.txtTongSoTienTangGiamThueXuatNhapKhau.Size = new System.Drawing.Size(114, 21);
            this.txtTongSoTienTangGiamThueXuatNhapKhau.TabIndex = 49;
            this.txtTongSoTienTangGiamThueXuatNhapKhau.Text = "0";
            this.txtTongSoTienTangGiamThueXuatNhapKhau.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoTienTangGiamThueXuatNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtHienThiTongSoTienTangGiamThueXuatNhapKhau
            // 
            this.txtHienThiTongSoTienTangGiamThueXuatNhapKhau.Enabled = false;
            this.txtHienThiTongSoTienTangGiamThueXuatNhapKhau.Location = new System.Drawing.Point(150, 718);
            this.txtHienThiTongSoTienTangGiamThueXuatNhapKhau.Name = "txtHienThiTongSoTienTangGiamThueXuatNhapKhau";
            this.txtHienThiTongSoTienTangGiamThueXuatNhapKhau.Size = new System.Drawing.Size(55, 21);
            this.txtHienThiTongSoTienTangGiamThueXuatNhapKhau.TabIndex = 48;
            this.txtHienThiTongSoTienTangGiamThueXuatNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtMaTienTeTongSoTienTangGiamThueXuatNhapKhau
            // 
            this.txtMaTienTeTongSoTienTangGiamThueXuatNhapKhau.Enabled = false;
            this.txtMaTienTeTongSoTienTangGiamThueXuatNhapKhau.Location = new System.Drawing.Point(331, 718);
            this.txtMaTienTeTongSoTienTangGiamThueXuatNhapKhau.Name = "txtMaTienTeTongSoTienTangGiamThueXuatNhapKhau";
            this.txtMaTienTeTongSoTienTangGiamThueXuatNhapKhau.Size = new System.Drawing.Size(55, 21);
            this.txtMaTienTeTongSoTienTangGiamThueXuatNhapKhau.TabIndex = 50;
            this.txtMaTienTeTongSoTienTangGiamThueXuatNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // grdTienTangGiamThuKhac
            // 
            this.grdTienTangGiamThuKhac.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdTienTangGiamThuKhac.CardColumnHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdTienTangGiamThuKhac.ColumnAutoResize = true;
            grdTienTangGiamThuKhac_DesignTimeLayout.LayoutString = resources.GetString("grdTienTangGiamThuKhac_DesignTimeLayout.LayoutString");
            this.grdTienTangGiamThuKhac.DesignTimeLayout = grdTienTangGiamThuKhac_DesignTimeLayout;
            this.grdTienTangGiamThuKhac.Enabled = false;
            this.grdTienTangGiamThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grdTienTangGiamThuKhac.GroupByBoxVisible = false;
            this.grdTienTangGiamThuKhac.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdTienTangGiamThuKhac.Location = new System.Drawing.Point(12, 765);
            this.grdTienTangGiamThuKhac.Name = "grdTienTangGiamThuKhac";
            this.grdTienTangGiamThuKhac.RecordNavigator = true;
            this.grdTienTangGiamThuKhac.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdTienTangGiamThuKhac.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdTienTangGiamThuKhac.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdTienTangGiamThuKhac.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdTienTangGiamThuKhac.Size = new System.Drawing.Size(723, 132);
            this.grdTienTangGiamThuKhac.TabIndex = 51;
            this.grdTienTangGiamThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdTienTangGiamThuKhac.VisualStyleManager = this.vsmMain;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Location = new System.Drawing.Point(9, 749);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(208, 13);
            this.label37.TabIndex = 37;
            this.label37.Text = "Tổng số tiền tăng/ giảm thuế và thu khác:";
            // 
            // txtTongSoTrangToKhaiBoSung
            // 
            this.txtTongSoTrangToKhaiBoSung.Enabled = false;
            this.txtTongSoTrangToKhaiBoSung.Location = new System.Drawing.Point(150, 912);
            this.txtTongSoTrangToKhaiBoSung.Name = "txtTongSoTrangToKhaiBoSung";
            this.txtTongSoTrangToKhaiBoSung.Size = new System.Drawing.Size(55, 21);
            this.txtTongSoTrangToKhaiBoSung.TabIndex = 52;
            this.txtTongSoTrangToKhaiBoSung.VisualStyleManager = this.vsmMain;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Location = new System.Drawing.Point(19, 916);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(129, 13);
            this.label38.TabIndex = 37;
            this.label38.Text = "Tổng số trang của tờ khai";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Location = new System.Drawing.Point(275, 916);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(154, 13);
            this.label39.TabIndex = 37;
            this.label39.Text = "Tổng số dòng hàng của tờ khai";
            // 
            // txtTongSoDongHangToKhaiBoSung
            // 
            this.txtTongSoDongHangToKhaiBoSung.Enabled = false;
            this.txtTongSoDongHangToKhaiBoSung.Location = new System.Drawing.Point(439, 912);
            this.txtTongSoDongHangToKhaiBoSung.Name = "txtTongSoDongHangToKhaiBoSung";
            this.txtTongSoDongHangToKhaiBoSung.Size = new System.Drawing.Size(55, 21);
            this.txtTongSoDongHangToKhaiBoSung.TabIndex = 53;
            this.txtTongSoDongHangToKhaiBoSung.VisualStyleManager = this.vsmMain;
            // 
            // txtLyDo
            // 
            this.txtLyDo.Enabled = false;
            this.txtLyDo.Location = new System.Drawing.Point(150, 939);
            this.txtLyDo.Multiline = true;
            this.txtLyDo.Name = "txtLyDo";
            this.txtLyDo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLyDo.Size = new System.Drawing.Size(585, 45);
            this.txtLyDo.TabIndex = 54;
            this.txtLyDo.VisualStyleManager = this.vsmMain;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Location = new System.Drawing.Point(19, 955);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(33, 13);
            this.label40.TabIndex = 37;
            this.label40.Text = "Lý do";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Location = new System.Drawing.Point(12, 994);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(124, 13);
            this.label41.TabIndex = 37;
            this.label41.Text = "Tên của người phụ trách";
            // 
            // txtTenNguoiPhuTrach
            // 
            this.txtTenNguoiPhuTrach.Enabled = false;
            this.txtTenNguoiPhuTrach.Location = new System.Drawing.Point(150, 990);
            this.txtTenNguoiPhuTrach.Name = "txtTenNguoiPhuTrach";
            this.txtTenNguoiPhuTrach.Size = new System.Drawing.Size(281, 21);
            this.txtTenNguoiPhuTrach.TabIndex = 55;
            this.txtTenNguoiPhuTrach.VisualStyleManager = this.vsmMain;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Location = new System.Drawing.Point(12, 1021);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(138, 13);
            this.label42.TabIndex = 37;
            this.label42.Text = "Tên trưởng đơn vị Hải quan";
            // 
            // txtTenTruongDonViHaiQuan
            // 
            this.txtTenTruongDonViHaiQuan.Enabled = false;
            this.txtTenTruongDonViHaiQuan.Location = new System.Drawing.Point(150, 1017);
            this.txtTenTruongDonViHaiQuan.Name = "txtTenTruongDonViHaiQuan";
            this.txtTenTruongDonViHaiQuan.Size = new System.Drawing.Size(281, 21);
            this.txtTenTruongDonViHaiQuan.TabIndex = 56;
            this.txtTenTruongDonViHaiQuan.VisualStyleManager = this.vsmMain;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Location = new System.Drawing.Point(437, 1021);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(108, 13);
            this.label43.TabIndex = 63;
            this.label43.Text = "Ngày đăng ký dữ liệu";
            // 
            // dtNgayDangKyDuLieu
            // 
            // 
            // 
            // 
            this.dtNgayDangKyDuLieu.DropDownCalendar.Name = "";
            this.dtNgayDangKyDuLieu.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayDangKyDuLieu.Enabled = false;
            this.dtNgayDangKyDuLieu.IsNullDate = true;
            this.dtNgayDangKyDuLieu.Location = new System.Drawing.Point(550, 1017);
            this.dtNgayDangKyDuLieu.Name = "dtNgayDangKyDuLieu";
            this.dtNgayDangKyDuLieu.Size = new System.Drawing.Size(100, 21);
            this.dtNgayDangKyDuLieu.TabIndex = 57;
            this.dtNgayDangKyDuLieu.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtGioDangKyDuLieu
            // 
            this.dtGioDangKyDuLieu.CustomFormat = "hh:mm:ss";
            this.dtGioDangKyDuLieu.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtGioDangKyDuLieu.DropDownCalendar.Name = "";
            this.dtGioDangKyDuLieu.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtGioDangKyDuLieu.Enabled = false;
            this.dtGioDangKyDuLieu.IsNullDate = true;
            this.dtGioDangKyDuLieu.Location = new System.Drawing.Point(656, 1017);
            this.dtGioDangKyDuLieu.Name = "dtGioDangKyDuLieu";
            this.dtGioDangKyDuLieu.Size = new System.Drawing.Size(79, 21);
            this.dtGioDangKyDuLieu.TabIndex = 58;
            this.dtGioDangKyDuLieu.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // VNACC_TKBoSung_ThueHangHoaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(966, 565);
            this.Controls.Add(this.cmdToolBar);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_TKBoSung_ThueHangHoaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin tờ khai (AMA: Pre-registration of amended tax infomation)";
            this.Load += new System.EventHandler(this.VNACC_GiayPhepForm_Load);
            this.Controls.SetChildIndex(this.cmdToolBar, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdToolBar)).EndInit();
            this.cmdToolBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdTienTangGiamThuKhac)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar cmdToolBar;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdlayPhanHoi;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHaiQuan;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand Separator2;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHaiQuan1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiKhai;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhNguoiKhai;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label59;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiKhai;
        private System.Windows.Forms.Label label60;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiNguoiKhai;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiKhai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhaiBoSung;
        private Janus.Windows.CalendarCombo.CalendarCombo dtGioDangKyKhaiBoSung;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayDangKyKhaiBoSung;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoThongBao;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.CalendarCombo.CalendarCombo dtGioHoanThanhKiemTra;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayHoanThanhKiemTra;
        private System.Windows.Forms.Label label4;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCoQuanHaiQuan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucNhomXuLyHoSo;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaLoaiHinh;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhanLoaiXuatNhapKhau;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHetThoiHan;
        private Janus.Windows.GridEX.EditControls.EditBox txtDauHieuBaoQua60Ngay;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayKhaiBao;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayCapPhep;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.CalendarCombo.CalendarCombo dtThoiHanTaiNhapTaiXuat;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDaiLyHaiQuan;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDaiLyHaiQuan;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNhanVienHaiQuan;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaLyDoKhaiBoSung;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucPhanLoaiNopThue;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoQuanLyTrongNoiBoDoanhNghiep;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaTienTeTienThue;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNganHangTraThueThay;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNganHangTraThueThay;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamPhatHanhHanMuc;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.GridEX.EditControls.EditBox txtKiHieuChungTuPhatHanhHanMuc;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoChungPhatHanhHanMuc;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaXacDinhThoiHanNopThue;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayHieuLucChungTu;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoNgayNopThueDanhChoVATHangHoaDacBiet;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoNgayNopThue;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamPhatHanh;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoChungTuBaoLanh;
        private Janus.Windows.GridEX.EditControls.EditBox txtKyHieuChungTuBaoLanh;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNganHangBaoLanh;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNganHangBaoLanh;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTyGiaHoiDoaiSauKhiKhaiBoSung;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTyGiaHoiDoaiTruocKhiKhaiBoSung;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaTienTeSauKhiKhaiBoSung;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaTienTeTruocKhiKhaiBoSung;
        private System.Windows.Forms.Label label35;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoTienTangGiamThueXuatNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaTienTeTongSoTienTangGiamThueXuatNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtHienThiTongSoTienTangGiamThueXuatNhapKhau;
        private System.Windows.Forms.Label label36;
        private Janus.Windows.GridEX.GridEX grdTienTangGiamThuKhac;
        private System.Windows.Forms.Label label37;
        private Janus.Windows.GridEX.EditControls.EditBox txtTongSoDongHangToKhaiBoSung;
        private Janus.Windows.GridEX.EditControls.EditBox txtTongSoTrangToKhaiBoSung;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private Janus.Windows.GridEX.EditControls.EditBox txtLyDo;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiPhuTrach;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenTruongDonViHaiQuan;
        private Janus.Windows.CalendarCombo.CalendarCombo dtGioDangKyDuLieu;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayDangKyDuLieu;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
    }
}