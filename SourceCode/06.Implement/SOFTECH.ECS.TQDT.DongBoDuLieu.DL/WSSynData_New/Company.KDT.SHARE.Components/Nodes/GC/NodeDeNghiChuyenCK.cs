﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Nodes.GC
{
    public class NodeDeNghiChuyenCK
    {	        
        /// <summary>
        /// Số tiếp nhận chứng từ
        /// </summary>
        public const string customsReference = "customsReference";        
        /// <summary>
        /// Loại chứng từ (= 601) 
        /// </summary>        
        public const string issuer = "issuer";
        /// <summary>
        /// Đơn vị khai báo
        /// </summary>
        public const string Agent = "Agent";
        /// <summary>
        /// Đơn vị HQ khai báo
        /// </summary>
        public const string declarationOffice = "declarationOffice";
        /// <summary>
        /// Ngày tiếp nhận chứng từ
        /// </summary>
        public const string acceptance = "acceptance";
        /// <summary>
        /// Chức năng (khai báo = 8, sửa =5)
        /// </summary>
        public const string function = "function";
        /// <summary>
        /// Nơi khai báo
        /// </summary>
        public const string issueLocation = "issueLocation";	
        /// <summary>
        /// Số hợp đồng || Số tham chiếu tờ khai
        /// </summary>
        public const string reference = "reference";
        /// <summary>
        /// Ngày hợp đồng || Ngày khai báo
        /// </summary>
        public const string issue = "issue";
        /// <summary>
        /// Trạng thái (mới = 0; cũ =1) || Trạng thái chứng từ || Loại (xem AgentStauts)
        /// </summary>
        public const string status = "status";
        /// <summary>
        /// Tên người nhận gia công || Tên người thuê gia công || Tên nhóm sản phẩm || Tên đơn vi khai báo || Tên đơn vị XNK
        /// </summary>
        public const string name = "name";
        /// <summary>
        /// Mã người nhận gia công || Mã người thuê gia công || Mã nhóm sản phẩm || Mã đơn vị khai báo || Mã đơn vị XNK
        /// </summary>
        public const string identity = "identity";
        /// <summary>
        /// Ghi chú khác về hợp đồng
        /// </summary>
        public const string AdditionalInformation = "AdditionalInformation";
        /// <summary>
        /// Ghi chú khác về hợp đồng
        /// </summary>
        public const string content = "content";
        ///
        /// <summary>
        /// Thông tin vận đơn
        /// </summary>
        public const string AdditionalDocument = "AdditionalDocument";
        /// <summary>
        /// Địa điểm kiểm tra hàng hóa ngoài cửa khẩu
        /// </summary>
        public const string examinationPlace = "examinationPlace"; 
        /// <summary>
        /// Thời gian dự kiến đến địa điểm kiểm tra
        /// </summary>
        public const string time = "time";
        /// <summary>
        /// Tuyến đường vận chuyển
        /// </summary>
        public const string route = "route";
        /// <summary>
        /// Đề nghị chuyển CK
        /// </summary>
        public const string CustomsOfficeChangedRequests = "CustomsOfficeChangedRequests";
        public const string CustomsOfficeChangedRequest = "CustomsOfficeChangedRequest"; 

    }
}
