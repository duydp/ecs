﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.KD.BLL.Utils;
using System.Threading;
using Company.KD.BLL.KDT.SXXK;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;
using System.IO;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KD.BLL.SXXK;
namespace Company.KD.BLL.KDT
{
    public partial class ToKhaiMauDich
    {
        // public bool isSuaTKDaDuyet = false;
        private ChungTuCollection _chungtuCollection = new ChungTuCollection();
        List<HangMauDich> _HMDCollection = new List<HangMauDich>();
        //HangMauDichCollection _HMDCollection = new HangMauDichCollection();
        private ToKhaiTriGiaCollection _TKTGCollection = new ToKhaiTriGiaCollection();
        private ToKhaiTriGiaPP23Collection _TKTGPP23Collection = new ToKhaiTriGiaPP23Collection();

        //Thong Bao Thue By DATLMQ 24/01/2011
        public bool IsThongBaoThue = false;
        public string SoQD = "";
        public DateTime NgayQD = new DateTime();
        public DateTime NgayHetHan = new DateTime();
        public string TaiKhoanKhoBac = "";
        public string TenKhoBac = "";
        //Thue XNK
        public string ChuongThueXNK = "";
        public string LoaiThueXNK = "";
        public string KhoanThueXNK = "";
        public string MucThueXNK = "";
        public string TieuMucThueXNK = "";
        public double SoTienThueXNK = 0;
        //Thue VAT
        public string ChuongThueVAT = "";
        public string LoaiThueVAT = "";
        public string KhoanThueVAT = "";
        public string MucThueVAT = "";
        public string TieuMucThueVAT = "";
        public double SoTienThueVAT = 0;
        //Thue TTDB
        public string ChuongThueTTDB = "";
        public string LoaiThueTTDB = "";
        public string KhoanThueTTDB = "";
        public string MucThueTTDB = "";
        public string TieuMucThueTTDB = "";
        public double SoTienThueTTDB = 0;
        //Thue TVCBPG
        public string ChuongThueTVCBPG = "";
        public string LoaiThueTVCBPG = "";
        public string KhoanThueTVCBPG = "";
        public string MucThueTVCBPG = "";
        public string TieuMucThueTVCBPG = "";
        public double SoTienThueTVCBPG = 0;

        public bool isEdit = false;
        //edit :
        //  public List<CO> ListCO = new List<CO>();
        private List<CO> _ListCO = new List<CO>();
        public List<ChungTuKem> listCTDK = new List<ChungTuKem>();
         List<GiayNopTien> _giayNopTiens = new List<GiayNopTien>();
        public List<GiayNopTien> GiayNopTiens { get { return _giayNopTiens; } set { _giayNopTiens = value; } }
        public void LoadListGiayNopTiens()
        {
            _giayNopTiens = GiayNopTien.SelectCollectionDynamic("TKMD_ID=" + ID, "");
        }
        public ToKhaiTriGiaCollection TKTGCollection
        {
            set { this._TKTGCollection = value; }
            get { return this._TKTGCollection; }
        }
        public void LoadToKhaiTriGiaPP23()
        {
            ToKhaiTriGiaPP23 TKTGPP23 = new ToKhaiTriGiaPP23();
            TKTGPP23.TKMD_ID = this.ID;
            this.TKTGPP23Collection = TKTGPP23.SelectCollectionBy_TKMD_ID();
        }
        public void LoadChungTuKem()
        {
            this.listCTDK = ChungTuKem.SelectCollectionBy_TKMDID(this.ID);
        }
        public ToKhaiTriGiaPP23Collection TKTGPP23Collection
        {
            set { this._TKTGPP23Collection = value; }
            get { return this._TKTGPP23Collection; }
        }
        //
        public List<CO> ListCO
        {
            set { this._ListCO = value; }
            get { return this._ListCO; }
        }
        public List<HangMauDich> HMDCollection
        {
            set { this._HMDCollection = value; }
            get { return this._HMDCollection; }
        }
        public ChungTuCollection ChungTuTKCollection
        {
            set { this._chungtuCollection = value; }
            get { return this._chungtuCollection; }
        }
        public IDataReader SelectReaderDynamic(string sql, DateTime from, DateTime to)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@From", SqlDbType.DateTime, from);
            db.AddInParameter(dbCommand, "@To", SqlDbType.DateTime, to);

            return db.ExecuteReader(dbCommand);
        }
        public static long SelectCountSoTK(string madoanhnghiep)
        {
            string sql = "select count(*) from t_KDT_ToKhaiMauDich where MaDoanhNghiep='" + madoanhnghiep + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return 0;
            else
                return Convert.ToInt64(o);
        }
        public  long SelectCountSoTKThongQuan(string madoanhnghiep)
        {
            string where = "MaDoanhNghiep='" + madoanhnghiep + "' AND TrangThaiXuLy = 1 AND HUONGDAN<>'' AND MSG_STATUS IS NULL";
            ToKhaiMauDichCollection tkmdco = new ToKhaiMauDichCollection();
            tkmdco = SelectCollectionDynamicDongBoDuLieu(where,null);
            if (tkmdco == null || tkmdco.Count == 0)
            {
                return 0;
            }
            else
                return tkmdco.Count;
        }
        public DataTable GetContainerInfo(long tkmd)
        {
            string sql = " SELECT     dbo.t_KDT_Container.SoHieu, dbo.t_KDT_Container.LoaiContainer, dbo.t_KDT_Container.Seal_No, dbo.t_KDT_ToKhaiMauDich.NgayDangKy, "
                     + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, dbo.t_KDT_ToKhaiMauDich.MaDoanhNghiep, dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
                     + " dbo.t_KDT_VanDon.NgayVanDon, dbo.t_KDT_VanDon.SoHieuChuyenDi "
                     + " FROM         dbo.t_KDT_Container INNER JOIN "
                     + " dbo.t_KDT_VanDon ON dbo.t_KDT_Container.VanDon_ID = dbo.t_KDT_VanDon.ID inner join dbo.t_KDT_ToKhaiMauDich ON "
                     + " dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_VanDon.TKMD_ID "
                     + " where dbo.t_KDT_VanDon.TKMD_ID = " + tkmd;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        //Lypt created date 22/01/20110
        public DataTable TKMD_GetMaxID()
        {
            string sql = "select max(id) ID from t_KDT_ToKhaiMauDich";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        //het Lypt created
        //-----------------------------------------------------------------------------------------
        public ToKhaiMauDichCollection SelectCollectionDynamic(string sql, DateTime from, DateTime to)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDynamic(sql, from, to);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        private void InsertUpdateFull(SqlTransaction transaction)
        {
            if (this.ID == 0)
                this.ID = this.InsertTransaction(transaction);
            else
                this.UpdateTransaction(transaction);

            foreach (HangMauDich hmdDetail in this.HMDCollection)
            {
                if (hmdDetail.ID == 0)
                {
                    hmdDetail.TKMD_ID = this.ID;
                    hmdDetail.ID = hmdDetail.Insert(transaction);
                }
                else
                {
                    hmdDetail.Update(transaction);
                }
            }
            foreach (ChungTu ct in this.ChungTuTKCollection)
            {
                ct.InsertUpdateTransaction(transaction);
            }
        }
        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    int i = 1;
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.SoThuTuHang = i++;
                        if (hmd.ID == 0)
                        {
                            hmd.TKMD_ID = this.ID;
                            hmd.ID = hmd.Insert(transaction);
                        }
                        else
                        {
                            hmd.Update(transaction);
                        }
                    }
                    //Add extra :

                    i = 1;
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.STTHang = i++;
                        if (ct.ID == 0)
                        {
                            ct.Master_ID = this.ID;
                            ct.ID = ct.InsertTransaction(transaction);
                        }
                        else
                        {
                            ct.UpdateTransaction(transaction);
                        }
                    }

                    #region Van Tai Don
                    if (VanTaiDon != null)
                    {
                        if (VanTaiDon.ID == 0)
                        {
                            VanTaiDon.TKMD_ID = this.ID;
                            VanTaiDon.ID = VanTaiDon.Insert(transaction);
                        }
                        else
                        {
                            VanTaiDon.Update(transaction);
                        }
                        foreach (Container container in VanTaiDon.ContainerCollection)
                        {
                            container.VanDon_ID = VanTaiDon.ID;
                            if (container.ID == 0)
                                container.Insert(transaction);
                            else
                                container.Update(transaction);
                        }
                        foreach (HangVanDonDetail item in VanTaiDon.ListHangOfVanDon)
                        {
                            item.VanDon_ID = VanTaiDon.ID;
                            if (item.ID == 0)
                                item.Insert(transaction);
                            else item.Update(transaction);
                        }
                    }
                    #endregion Van Tai Don
                    #region Chứng từ nợ
                    if (ChungTuNoCollection != null)
                    {
                        foreach (ChungTuNo item in ChungTuNoCollection)
                        {
                            item.TKMDID = this.ID;
                            if (item.ID == 0)
                                item.Insert(transaction);
                            else item.Update(transaction);
                        }
                    }
                    #endregion
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        //-----------------------------------------------------------------------------------------

        public void LoadHMDCollection()
        {
            //HangMauDich hmd = new HangMauDich();
            //hmd.TKMD_ID = this.ID;
            //this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
            this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
        }
        public void LoadCO()
        {
            // CO co = new CO();
            //co.TKMD_ID = this.ID;
            ListCO = (List<CO>)CO.SelectCollectionBy_TKMD_ID(this.ID);
            //this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
        }
        public void LoadChungTuTKCollection()
        {
            ChungTu ct = new ChungTu();
            ct.Master_ID = this.ID;
            this.ChungTuTKCollection = ct.SelectCollectionBy_Master_ID();
        }
        public void LoadTKTGCollection()
        {
            ToKhaiTriGia tktg = new ToKhaiTriGia();
            tktg.TKMD_ID = this.ID;
            _TKTGCollection = tktg.SelectCollectionBy_TKMD_ID();
            foreach (ToKhaiTriGia item in _TKTGCollection)
            {
                item.LoadHTGCollection();
            }
        }
        //-----------------------------------------------------------------------------------------

        public long InsertFull()
        {
            long ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    long ret01 = this.InsertTransaction(transaction);
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.TKMD_ID = ret01;
                        hmd.Insert(transaction);
                    }
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.Master_ID = ret01;
                        ct.InsertTransaction(transaction);
                    }
                    if (ret01 > 0)
                    {
                        transaction.Commit();
                        ret = ret01;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = 0;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public long UpdateFull()
        {
            long ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    long ret01 = this.UpdateTransaction(transaction);
                    //HangMauDich hmd1 = new HangMauDich();
                    //ToKhaiMauDich tk = new ToKhaiMauDich();
                    //tk.ID = this.ID;
                    //tk.LoadHMDCollection();
                    //hmd1.DeleteCollection(tk.HMDCollection, transaction);
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        //if (hmd.Load()) hmd.Update(transaction);
                        //else
                        //{
                        //    hmd.TKMD_ID = this.ID;
                        //    hmd.Insert(transaction);
                        //}
                        hmd.InsertUpdate(transaction);
                    }
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.Master_ID = ret01;
                        ct.InsertTransaction(transaction);
                    }
                    if (ret01 > 0)
                    {
                        transaction.Commit();
                        ret = ret01;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = 0;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public void DeleteHangCollection(SqlTransaction transaction)
        {
            string sql = "delete from t_KDT_HangMauDich where TKMD_ID=@ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                db.ExecuteNonQuery(dbCommand, transaction);
            else
                db.ExecuteNonQuery(dbCommand);
        }

        public static void DongBoDuLieuPhongKhai(ToKhaiMauDichCollection tkmdCollection)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (ToKhaiMauDich tkmd in tkmdCollection)
                    {
                        foreach (HangMauDich hmd in tkmd.HMDCollection)
                        {
                            hmd.ID = 0;
                        }
                        MsgSend msg = new MsgSend();
                        msg.master_id = tkmd.ID;
                        msg.LoaiHS = "TK";
                        msg.DeleteTransaction(transaction);
                        tkmd.DeleteHangCollection(transaction);
                        tkmd.InsertUpdateFull(transaction);
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }


        }

        public void TransgferDataToSXXK()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction);
                    BLL.SXXK.ToKhai.ToKhaiMauDich tkDuyet = new Company.KD.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkDuyet.MaDoanhNghiep = this.MaDoanhNghiep;
                    tkDuyet.PhanLuong = "";
                    tkDuyet.MaHaiQuan = Globals.trimMaHaiQuan(this.MaHaiQuan);
                    tkDuyet.MaLoaiHinh = this.MaLoaiHinh;
                    tkDuyet.NamDangKy = (short)this.NgayDangKy.Year;
                    tkDuyet.NgayDangKy = this.NgayDangKy;
                    tkDuyet.SoToKhai = this.SoToKhai;
                    tkDuyet.TenDonViDoiTac = this.TenDonViDoiTac;
                    tkDuyet.NgayGiayPhep = this.NgayGiayPhep;
                    tkDuyet.NgayHetHanGiayPhep = this.NgayHetHanGiayPhep;
                    tkDuyet.SoHopDong = this.SoHopDong;
                    tkDuyet.NgayHopDong = this.NgayHopDong;
                    tkDuyet.NgayHetHanHopDong = this.NgayHetHanHopDong;
                    tkDuyet.SoHoaDonThuongMai = this.SoHoaDonThuongMai;
                    tkDuyet.NgayHoaDonThuongMai = this.NgayHoaDonThuongMai;
                    tkDuyet.PTVT_ID = this.PTVT_ID;
                    tkDuyet.SoHieuPTVT = this.SoHieuPTVT;
                    tkDuyet.NgayDenPTVT = this.NgayDenPTVT;
                    tkDuyet.LoaiVanDon = this.LoaiVanDon;
                    tkDuyet.NgayVanDon = this.NgayVanDon;
                    tkDuyet.NuocXK_ID = this.NuocXK_ID;
                    tkDuyet.NuocNK_ID = this.NuocNK_ID;
                    tkDuyet.DiaDiemXepHang = this.DiaDiemXepHang;
                    tkDuyet.DKGH_ID = this.DKGH_ID;
                    tkDuyet.CuaKhau_ID = this.CuaKhau_ID;
                    tkDuyet.NguyenTe_ID = this.NguyenTe_ID;
                    tkDuyet.TyGiaTinhThue = this.TyGiaTinhThue;
                    tkDuyet.TyGiaUSD = this.TyGiaUSD;
                    tkDuyet.PTTT_ID = this.PTTT_ID;
                    tkDuyet.SoHang = this.SoHang;
                    tkDuyet.SoLuongPLTK = this.SoLuongPLTK;
                    tkDuyet.TenChuHang = this.TenChuHang;
                    tkDuyet.SoContainer20 = this.SoContainer20;
                    tkDuyet.SoContainer40 = this.SoContainer40;
                    tkDuyet.SoKien = this.SoKien;
                    tkDuyet.TrongLuong = this.TrongLuong;
                    tkDuyet.TongTriGiaKhaiBao = this.TongTriGiaKhaiBao;
                    tkDuyet.TongTriGiaTinhThue = this.TongTriGiaTinhThue;
                    tkDuyet.LoaiToKhaiGiaCong = this.LoaiToKhaiGiaCong;
                    tkDuyet.LePhiHaiQuan = this.LePhiHaiQuan;
                    tkDuyet.PhiBaoHiem = this.PhiBaoHiem;
                    tkDuyet.PhiVanChuyen = this.PhiVanChuyen;
                    tkDuyet.ThanhLy = "";
                    tkDuyet.Xuat_NPL_SP = this.LoaiHangHoa;
                    tkDuyet.ChungTu = this.GiayTo;
                    tkDuyet.TrangThaiThanhKhoan = "D";
                    tkDuyet.PhiKhac = this.PhiKhac;
                    tkDuyet.NgayHoanThanh = tkDuyet.NgayDangKy;
                    tkDuyet.InsertUpdateTransaction(transaction);
                    if (this.HMDCollection.Count == 0)
                        this.LoadHMDCollection();
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        BLL.SXXK.ToKhai.HangMauDich hmdDuyet = new Company.KD.BLL.SXXK.ToKhai.HangMauDich();
                        hmdDuyet.DonGiaKB = hmd.DonGiaKB;
                        hmdDuyet.DonGiaTT = hmd.DonGiaTT;
                        hmdDuyet.DVT_ID = hmd.DVT_ID;
                        hmdDuyet.MaHaiQuan = Globals.trimMaHaiQuan(this.MaHaiQuan);
                        hmdDuyet.MaHS = hmd.MaHS;
                        hmdDuyet.MaLoaiHinh = this.MaLoaiHinh;
                        hmdDuyet.MaPhu = hmd.MaPhu;
                        hmdDuyet.MienThue = hmd.MienThue;
                        hmdDuyet.NamDangKy = (short)this.NgayDangKy.Year;
                        hmdDuyet.NuocXX_ID = hmd.NuocXX_ID;
                        hmdDuyet.PhuThu = hmd.PhuThu;
                        hmdDuyet.SoLuong = hmd.SoLuong;
                        hmdDuyet.SoThuTuHang = (short)hmd.SoThuTuHang;
                        hmdDuyet.SoToKhai = this.SoToKhai;
                        hmdDuyet.TenHang = hmd.TenHang;
                        hmdDuyet.ThueGTGT = hmd.ThueGTGT;
                        hmdDuyet.ThueSuatGTGT = hmd.ThueSuatGTGT;
                        hmdDuyet.ThueSuatTTDB = hmd.ThueSuatTTDB;
                        hmdDuyet.ThueSuatXNK = hmd.ThueSuatXNK;
                        hmdDuyet.ThueTTDB = hmd.ThueTTDB;
                        hmdDuyet.ThueXNK = hmd.ThueXNK;
                        hmdDuyet.TriGiaKB = hmd.TriGiaKB;
                        hmdDuyet.TriGiaKB_VND = hmd.TriGiaKB_VND;
                        hmdDuyet.TriGiaThuKhac = hmd.TriGiaThuKhac;
                        hmdDuyet.TriGiaTT = hmd.TriGiaTT;
                        hmdDuyet.TyLeThuKhac = hmd.TyLeThuKhac;

                        hmdDuyet.InsertUpdateTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        private HangMauDich GetHangMauDichByIDHMD(long HMD_ID)
        {
            foreach (HangMauDich HMD in this.HMDCollection)
            {
                if (HMD.ID == HMD_ID)
                    return HMD;
            }
            return null;
        }

        #region ChungTuHaiQuan

        public List<ChungTuKem> ChungTuKemCollection = new List<ChungTuKem>();
        public List<ChungTuNo> ChungTuNoCollection = new List<ChungTuNo>();

        public List<Company.KDT.SHARE.QuanLyChungTu.GiayPhep> GiayPhepCollection = new List<GiayPhep>();
        public List<HoaDonThuongMai> HoaDonThuongMaiCollection = new List<HoaDonThuongMai>();
        public List<HopDongThuongMai> HopDongThuongMaiCollection = new List<HopDongThuongMai>();
        public VanDon VanTaiDon;
        public List<CO> COCollection = new List<CO>();
        public List<DeNghiChuyenCuaKhau> listChuyenCuaKhau = new List<DeNghiChuyenCuaKhau>();
        public List<NoiDungDieuChinhTK> NoiDungDieuChinhTKCollection = new List<NoiDungDieuChinhTK>();

        public void LoadChungTuHaiQuan()
        {
            /*
            COCollection = CO.SelectCollectionBy_TKMD_ID(this.ID);
            HoaDonThuongMaiCollection = HoaDonThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
            HopDongThuongMaiCollection = HopDongThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
            List<VanDon> VanDonCollection = VanDon.SelectCollectionBy_TKMD_ID(this.ID);
            if (VanDonCollection != null && VanDonCollection.Count > 0)
            {
                VanTaiDon = VanDonCollection[0];
                VanTaiDon.LoadContainerCollection();
            }
            GiayPhepCollection = GiayPhep.SelectCollectionBy_TKMD_ID(this.ID);
            listChuyenCuaKhau = DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(this.ID);
            ChungTuKemCollection = ChungTuKem.SelectCollectionBy_TKMDID(this.ID);
            */

            //Xoa du lieu truoc khi load lai tranh truong hop trung du lieu.
            COCollection.Clear();
            HoaDonThuongMaiCollection.Clear();
            HopDongThuongMaiCollection.Clear();
            GiayPhepCollection.Clear();
            listChuyenCuaKhau.Clear();
            ChungTuKemCollection.Clear();

            COCollection = (List<CO>)CO.SelectCollectionBy_TKMD_ID(this.ID);
            foreach (CO co in COCollection)
            {
                co.LoadListHMDofCo();
            }
            HoaDonThuongMaiCollection = (List<HoaDonThuongMai>)HoaDonThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
            for (int j = 0; j < HoaDonThuongMaiCollection.Count; j++)
            {
                HoaDonThuongMaiCollection[j].LoadListHangDMOfHoaDon();
            }

            HopDongThuongMaiCollection = (List<HopDongThuongMai>)HopDongThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
            for (int i = 0; i < HopDongThuongMaiCollection.Count; i++)
            {
                HopDongThuongMaiCollection[i].LoadListHangDMOfHopDong();
            }

            IList<VanDon> VanDonCollection = VanDon.SelectCollectionBy_TKMD_ID(this.ID);
            if (VanDonCollection != null && VanDonCollection.Count > 0)
            {
                VanTaiDon = VanDonCollection[0];
                VanTaiDon.LoadContainerCollection();
                VanTaiDon.LoadListHangOfVanDon();
            }

            GiayPhepCollection = GiayPhep.SelectCollectionBy_TKMD_ID(this.ID);
            for (int m = 0; m < GiayPhepCollection.Count; m++)
            {
                GiayPhepCollection[m].LoadListHMDofGiayPhep();
            }

            listChuyenCuaKhau = DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(this.ID);

            ChungTuKemCollection = ChungTuKem.SelectCollectionBy_TKMDID(this.ID);
            for (int k = 0; k < ChungTuKemCollection.Count; k++)
            {
                ChungTuKemCollection[k].LoadListCTChiTiet();
            }

            //datlmq bo sung them noi dung dieu chinh to khai
            NoiDungDieuChinhTKCollection = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(this.ID);
            for (int k = 0; k < NoiDungDieuChinhTKCollection.Count; k++)
            {
                NoiDungDieuChinhTKCollection[k].LoadListNDDCTK();
            }
            ChungTuNoCollection = ChungTuNo.SelectCollectionDynamic("TKMDID=" + this.ID, "");
        }


        #endregion ChungTuHaiQuan

        #region Webservice của hải quan

        //-----------------------------------------------------------------------------------------

        public string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\XMLKinhDoanh\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();


            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            this.GUIDSTR = (System.Guid.NewGuid().ToString());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = this._TenDoanhNghiep;
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            this.Update();
            return doc.InnerXml;
        }


        /// <summary>
        /// Xuất dữ liệu ra Xml cho phong bì để khai báo hoặc lấy phản hồi
        /// </summary>
        /// <param name="messageType">Loại chứng từ</param>
        /// <param name="messageFunction">Chức năng</param>
        /// <param name="referenceId">Số tham chiếu. Nếu khai báo thì tạo mới, nếu lấy phản hồi thì truyền vào số tham chiếu đã có khi khai báo</param>
        /// <returns>Định dạng dữ liệu Xml cho phong bì theo tiêu chuẩn của Hải quan</returns>
        public string GenerateToXmlEnvelope(MessageTypes messageType, MessageFunctions messageFunction, Guid referenceId)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + @"\XMLKinhDoanh\PhongBi.xml");

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(Globals.trimMaHaiQuan(this.MaHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = Globals.trimMaHaiQuan(this.MaHaiQuan);

            // Subject.
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)messageType).ToString();

            // Function.
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference ID.
            XmlNode nodeReference = nodeSubject.ChildNodes[2];

            nodeReference.InnerText = this.GUIDSTR;
            //nodeReference.InnerText = referenceId.ToString();

            // Message ID.
            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = System.Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            nodeFrom.ChildNodes[0].InnerText = this.TenDoanhNghiep;
            //Không set lại giá trị LanNT
            // this.GUIDSTR = referenceId.ToString();
            this.Update();
            return doc.InnerXml;

        }

        //// Cau hinh phong bi lay phan hoi :
        //public string ConfigPhongBiLayPhanHoi(int type, int function)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    string path = EntityBase.GetPathProgram();
        //    doc.Load(path + @"\XMLKinhDoanh\PhongBi.xml");

        //    // Thiết lập thông tin Hải quan tiếp nhận khai báo.
        //    XmlNode nodeTo = doc.GetElementsByTagName("to")[0];
        //    XmlNode nodeName = nodeTo.ChildNodes[0];
        //    nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(Globals.trimMaHaiQuan(this._MaHaiQuan));
        //    XmlNode nodeIdentity = nodeTo.ChildNodes[1];
        //    nodeIdentity.InnerText = Globals.trimMaHaiQuan(this.MaHaiQuan);

        //    // Subject.
        //    XmlNode nodeSubject = doc.GetElementsByTagName("subject")[0];
        //    XmlNode nodeType = nodeSubject.ChildNodes[0];
        //    nodeType.InnerText = type.ToString();

        //    // Function.
        //    XmlNode nodeFunction = nodeSubject.ChildNodes[1];
        //    nodeFunction.InnerText = function.ToString();

        //    // Reference ID.
        //    XmlNode nodeReference = nodeSubject.ChildNodes[2];
        //    nodeReference.InnerText = this.GUIDSTR;

        //    // Message ID.
        //    XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
        //    nodeMessageID.InnerText = System.Guid.NewGuid().ToString();

        //    XmlNode nodeFrom = doc.GetElementsByTagName("from")[0];
        //    nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
        //    // this.GuidReference = this.GUIDSTR;

        //    this.Update();
        //    return doc.InnerXml;

        //}

        // Cau hinh phong bi lay phan hoi :
        public string ConfigPhongBiLayPhanHoi(MessageTypes type, MessageFunctions function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + @"\XMLKinhDoanh\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(Globals.trimMaHaiQuan(this._MaHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = Globals.trimMaHaiQuan(this.MaHaiQuan);

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)type).ToString();//type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)function).ToString(); //function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;

            return doc.InnerXml;

        }

        #region KD
        public List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> ConvertHMDKDToHangMauDich()
        {
            List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            foreach (Company.KD.BLL.KDT.HangMauDich hmdKD in this.HMDCollection)
            {
                Company.KDT.SHARE.QuanLyChungTu.HangMauDich hang = new Company.KDT.SHARE.QuanLyChungTu.HangMauDich();
                hang.ID = hmdKD.ID;
                hang.DonGiaKB = hmdKD.DonGiaKB;
                hang.DVT_ID = hmdKD.DVT_ID;
                hang.MaHS = hmdKD.MaHS;
                hang.MaPhu = hmdKD.MaPhu;
                hang.NuocXX_ID = hmdKD.NuocXX_ID;
                hang.SoLuong = hmdKD.SoLuong;
                hang.SoThuTuHang = hmdKD.SoThuTuHang;
                hang.TenHang = hmdKD.TenHang;
                hang.TriGiaKB = hmdKD.TriGiaKB;
                hang.TriGiaKB_VND = hmdKD.TriGiaKB_VND;
                listHang.Add(hang);
            }
            return listHang;
        }

        #region Khai báo tờ khai nhập qua WebService


        #region SUA THONG TIN TO KHAI ĐA DUYET :
        // Sua to khai da duyet :
        private string ConvertCollectionToXMLSuaToKhai()
        {
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();

            XmlDocument xmlDocument = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            if (this.MaLoaiHinh.StartsWith("N"))
            {
                xmlDocument.Load(path + "\\XMLKinhDoanh\\Sua_TKN.xml");
            }
            else
            {
                xmlDocument.Load(path + "\\XMLKinhDoanh\\Sua_TKX.xml");
            }

            // Thông tin Hải quan tiếp nhận khai báo.
            XmlNode nodeHQNhan = xmlDocument.SelectSingleNode("Root/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(Globals.trimMaHaiQuan(this.MaHaiQuan));

            // Thông tin Doanh nghiệp gửi khai báo.
            XmlNode nodeDN = xmlDocument.SelectSingleNode("Root/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            // Thông tin tờ khai.
            XmlNode nodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];
            nodeToKhai.Attributes["MA_LH"].Value = this.MaLoaiHinh;
            nodeToKhai.Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);

            if (this.MaLoaiHinh.StartsWith("N"))
            {
                nodeToKhai.Attributes["MA_DV_NK"].Value = this.MaDoanhNghiep;
                nodeToKhai.Attributes["MA_DV_XK"].Value = "";
            }
            else
            {
                nodeToKhai.Attributes["MA_DV_XK"].Value = "";
                nodeToKhai.Attributes["MA_DV_XK"].Value = this.MaDoanhNghiep;
            }

            nodeToKhai.Attributes["MA_DV_NK"].Value = this.MaDoanhNghiep;
            nodeToKhai.Attributes["MA_DV_UT"].Value = this.MaDonViUT;
            nodeToKhai.Attributes["MA_DV_XK"].Value = "";
            nodeToKhai.Attributes["MA_DVKT"].Value = this.MaDaiLyTTHQ;

            if (this.TenDonViDoiTac.Length > 30)
                nodeToKhai.Attributes["TEN_DV_XK"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac.Substring(0, 30));
            else
                nodeToKhai.Attributes["TEN_DV_XK"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac);

            if (this.SoGiayPhep.Trim().Length > 35)
                nodeToKhai.Attributes["SO_GP"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep.Trim().Substring(0, 35));
            else
                nodeToKhai.Attributes["SO_GP"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep.Trim());

            if (this.NgayGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_GP"].Value = this.NgayGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_GP"].Value = null;

            if (this.NgayHetHanGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHGP"].Value = this.NgayHetHanGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_HHGP"].Value = null;

            if (this.SoHopDong.Length <= 50)
                nodeToKhai.Attributes["SO_HD"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHopDong);
            else
                nodeToKhai.Attributes["SO_HD"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHopDong.Substring(0, 50));

            if (this.NgayHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HD"].Value = this.NgayHopDong.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_HD"].Value = null;

            if (this.NgayHetHanHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHHD"].Value = this.NgayHetHanHopDong.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_HHHD"].Value = null;

            if (this.SoHoaDonThuongMai.Length <= 50)
                nodeToKhai.Attributes["SO_HDTM"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai);
            else
                nodeToKhai.Attributes["SO_HDTM"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai.Substring(0, 50));

            if (this.NgayHoaDonThuongMai.Year > 1900)
                nodeToKhai.Attributes["NGAY_HDTM"].Value = this.NgayHoaDonThuongMai.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_HDTM"].Value = null;

            nodeToKhai.Attributes["MA_PTVT"].Value = this.PTVT_ID;

            if (this.SoHieuPTVT.Length <= 20)
                nodeToKhai.Attributes["TEN_PTVT"].Value = this.SoHieuPTVT;
            else
                nodeToKhai.Attributes["TEN_PTVT"].Value = this.SoHieuPTVT.Substring(0, 20);

            if (this.NgayDenPTVT.Year > 1900)
                nodeToKhai.Attributes["NGAY_DEN"].Value = this.NgayDenPTVT.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_DEN"].Value = null;

            if (SoVanDon.Length <= 20)
                nodeToKhai.Attributes["SO_VANDON"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon);
            else
                nodeToKhai.Attributes["SO_VANDON"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon.Substring(0, 20));

            if (this.NgayVanDon.Year > 1900)
                nodeToKhai.Attributes["NGAY_VANDON"].Value = this.NgayVanDon.ToString("MM/dd/yyyy");
            else
                //nodeToKhai.Attributes["NGAY_VANDON"].Value = new DateTime(1900,01,01).ToString("MM/dd/yyyy");
                nodeToKhai.Attributes["NGAY_VANDON"].Value = null;

            if (this.MaLoaiHinh.Substring(0, 1).Equals("N"))
            {
                nodeToKhai.Attributes["NUOC_XK"].Value = this.NuocXK_ID;
                nodeToKhai.Attributes["NUOC_NK"].Value = "VN";
            }
            else
            {
                nodeToKhai.Attributes["NUOC_XK"].Value = "VN";
                nodeToKhai.Attributes["NUOC_NK"].Value = this.NuocNK_ID;
            }

            if (this.DiaDiemXepHang.Length <= 40)
                nodeToKhai.Attributes["CANG_XUAT"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
            else
                nodeToKhai.Attributes["CANG_XUAT"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));

            nodeToKhai.Attributes["MA_CK_NHAP"].Value = this.CuaKhau_ID;
            nodeToKhai.Attributes["MA_DKGH"].Value = this.DKGH_ID;
            nodeToKhai.Attributes["MA_NGTE"].Value = this.NguyenTe_ID;

            // BEGIN: Xử lý tỷ giá tính thuế VNĐ.
            try
            {
                //string tygiavnd = this.TyGiaTinhThue, 9);
                string tygiavnd = BaseClass.Round(this.TyGiaTinhThue, 9);
                if (tygiavnd.Substring(tygiavnd.Length - 2, 2) == "00")
                    tygiavnd = tygiavnd.Substring(0, tygiavnd.Length - 3);

                nodeToKhai.Attributes["TY_GIA_VND"].Value = tygiavnd;
            }
            catch
            {
                nodeToKhai.Attributes["TY_GIA_VND"].Value = BaseClass.Round(this.TyGiaTinhThue, 9);
            }
            // END: Xử lý tỷ giá tính thuế VNĐ.

            nodeToKhai.Attributes["MA_PTTT"].Value = this.PTTT_ID;

            if (this.GiayTo.Length > 40)
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo.Substring(0, 40));
            else
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo);

            nodeToKhai.Attributes["SO_CONT20"].Value = Convert.ToUInt32(this.SoContainer20).ToString();
            nodeToKhai.Attributes["SO_CONT40"].Value = Convert.ToUInt32(this.SoContainer40).ToString();
            nodeToKhai.Attributes["SO_PLTK"].Value = Convert.ToUInt32(this.SoLuongPLTK).ToString();
            nodeToKhai.Attributes["SO_KIEN"].Value = Convert.ToUInt32(this.SoKien).ToString();
            nodeToKhai.Attributes["TRONG_LUONG"].Value = BaseClass.Round(this.TrongLuong, 9);
            nodeToKhai.Attributes["TRONG_LUONG_NET"].Value = BaseClass.Round(this.TrongLuongNet, 9);
            nodeToKhai.Attributes["PHI_BH"].Value = BaseClass.Round(this.PhiBaoHiem, 9);
            nodeToKhai.Attributes["PHI_VC"].Value = BaseClass.Round(this.PhiVanChuyen, 9);
            nodeToKhai.Attributes["LE_PHI_HQ"].Value = BaseClass.Round(this.LePhiHaiQuan, 9);

            if (this.TenChuHang.Length > 30)
                nodeToKhai.Attributes["CHU_HANG"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang.Substring(0, 30));
            else
                nodeToKhai.Attributes["CHU_HANG"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang);

            //CHUA CO PHAI BO SUNG

            //nodeToKhai.Attributes["DE_XUAT_KHAC"].Value = "KHONG CO DE XUAT KHAC CUA DOANH NGHIEP";
            nodeToKhai.Attributes["DE_XUAT_KHAC"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DeXuatKhac);
            nodeToKhai.Attributes["LY_DO_SUA"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.LyDoSua);
            nodeToKhai.Attributes["SOTK"].Value = this.SoToKhai + "";
            nodeToKhai.Attributes["NGAY_DK"].Value = this.NgayDangKy.ToString("MM/dd/yyyy");

            //Kiem tra : neu khai sua to khai da duyet thi moi gan :

            // Thông tin hàng hóa.
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                //HangMauDich hmd = new HangMauDich();
                //hmd.TKMD_ID = this.ID;
                //this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
            }

            XmlNode nodeHangHoa = xmlDocument.GetElementsByTagName("HANG")[0];
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = xmlDocument.CreateElement("HANG.ITEM");
                XmlAttribute sttAtt = xmlDocument.CreateAttribute("STT_HANG");
                sttAtt.Value = hmd.SoThuTuHang.ToString();
                node.Attributes.Append(sttAtt);

                XmlAttribute maAtt = xmlDocument.CreateAttribute("MA_HANG");
                maAtt.Value = hmd.MaPhu;
                node.Attributes.Append(maAtt);

                XmlAttribute maHsAtt = xmlDocument.CreateAttribute("MA_HS");
                maHsAtt.Value = hmd.MaHS;
                node.Attributes.Append(maHsAtt);

                XmlAttribute tenAtt = xmlDocument.CreateAttribute("TEN_HANG");
                if (hmd.TenHang.Length <= 256)
                    tenAtt.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang);
                else
                    tenAtt.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang.Substring(0, 256));

                node.Attributes.Append(tenAtt);

                XmlAttribute nuocAtt = xmlDocument.CreateAttribute("NUOC_XX");
                nuocAtt.Value = hmd.NuocXX_ID;
                node.Attributes.Append(nuocAtt);

                XmlAttribute soLuongAtt = xmlDocument.CreateAttribute("LUONG");
                soLuongAtt.Value = BaseClass.Round(hmd.SoLuong, 9);
                node.Attributes.Append(soLuongAtt);

                XmlAttribute maDvtAtt = xmlDocument.CreateAttribute("MA_DVT");
                maDvtAtt.Value = hmd.DVT_ID;
                node.Attributes.Append(maDvtAtt);

                XmlAttribute donGiaAtt = xmlDocument.CreateAttribute("DGIA_NT");
                donGiaAtt.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                node.Attributes.Append(donGiaAtt);

                XmlAttribute triGiaAtt = xmlDocument.CreateAttribute("TGIA_NT");
                triGiaAtt.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                node.Attributes.Append(triGiaAtt);

                XmlAttribute donGiaTinhThueAtt = xmlDocument.CreateAttribute("DGIA_TT_VND");
                donGiaTinhThueAtt.Value = BaseClass.Round(hmd.DonGiaTT, 9); ;
                node.Attributes.Append(donGiaTinhThueAtt);

                XmlAttribute triGiaTinhThueAtt = xmlDocument.CreateAttribute("TGIA_TT_VND");
                triGiaTinhThueAtt.Value = BaseClass.Round(hmd.TriGiaTT, 9); ;
                node.Attributes.Append(triGiaTinhThueAtt);

                XmlAttribute tsXnkAtt = xmlDocument.CreateAttribute("TS_XNK");
                tsXnkAtt.Value = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.Attributes.Append(tsXnkAtt);

                XmlAttribute tsTtdbAtt = xmlDocument.CreateAttribute("TS_TTDB");
                tsTtdbAtt.Value = BaseClass.Round(hmd.ThueSuatTTDB, 9);
                node.Attributes.Append(tsTtdbAtt);

                XmlAttribute tsVatAtt = xmlDocument.CreateAttribute("TS_VAT");
                tsVatAtt.Value = BaseClass.Round(hmd.ThueSuatGTGT, 9);
                node.Attributes.Append(tsVatAtt);

                XmlAttribute tlPhuThuAtt = xmlDocument.CreateAttribute("TL_PHU_THU");
                tlPhuThuAtt.Value = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.Attributes.Append(tlPhuThuAtt);

                XmlAttribute thueXnkAtt = xmlDocument.CreateAttribute("THUE_XNK");
                thueXnkAtt.Value = BaseClass.Round(hmd.ThueXNK, 9);
                node.Attributes.Append(thueXnkAtt);

                XmlAttribute thueTtdbAtt = xmlDocument.CreateAttribute("THUE_TTDB");
                thueTtdbAtt.Value = BaseClass.Round(hmd.ThueTTDB, 9);
                node.Attributes.Append(thueTtdbAtt);

                XmlAttribute thueVatAtt = xmlDocument.CreateAttribute("THUE_VAT");
                thueVatAtt.Value = BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(thueVatAtt);

                XmlAttribute phuThuAtt = xmlDocument.CreateAttribute("PHU_THU");
                phuThuAtt.Value = BaseClass.Round(hmd.TriGiaThuKhac, 9);
                node.Attributes.Append(phuThuAtt);

                nodeHangHoa.AppendChild(node);
            }

            return xmlDocument.InnerXml;
        }
        private string ConvertCollectionToXMLSuaToKhaiNhapKD()
        {
            //Linhhtn update 20100616
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\XMLKinhDoanh\\msg_Sua_TKN_KD.xml");
            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(Globals.trimMaHaiQuan(this._MaHaiQuan));


            XmlNode nodeDN = docNPL.SelectSingleNode("Root/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";
            //thong tin to khai

            XmlNode nodeToKhai = docNPL.GetElementsByTagName("TO_KHAI")[0];
            nodeToKhai.Attributes["MA_LH"].Value = this.MaLoaiHinh;
            nodeToKhai.Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);
            nodeToKhai.Attributes["MA_DV_NK"].Value = this.MaDoanhNghiep;
            nodeToKhai.Attributes["MA_DV_UT"].Value = this.MaDonViUT;
            nodeToKhai.Attributes["MA_DV_XK"].Value = "";
            nodeToKhai.Attributes["MA_DVKT"].Value = this.MaDaiLyTTHQ;
            if (this.TenDonViDoiTac.Length > 30)
                nodeToKhai.Attributes["TEN_DV_XK"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac.Substring(0, 30));
            else
                nodeToKhai.Attributes["TEN_DV_XK"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac);
            if (this.SoGiayPhep.Trim().Length > 35)
                nodeToKhai.Attributes["SO_GP"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep.Trim().Substring(0, 35));
            else
                nodeToKhai.Attributes["SO_GP"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep.Trim());
            if (this.NgayGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_GP"].Value = this.NgayGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_GP"].Value = null;
            if (this.NgayHetHanGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHGP"].Value = this.NgayHetHanGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_HHGP"].Value = null;

            if (this.SoHopDong.Length <= 50)
                nodeToKhai.Attributes["SO_HD"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHopDong);
            else
                nodeToKhai.Attributes["SO_HD"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHopDong.Substring(0, 50));

            if (this.NgayHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HD"].Value = this.NgayHopDong.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_HD"].Value = null;

            if (this.NgayHetHanHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHHD"].Value = this.NgayHetHanHopDong.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_HHHD"].Value = null;

            if (this.SoHoaDonThuongMai.Length <= 50)
                nodeToKhai.Attributes["SO_HDTM"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai);
            else
                nodeToKhai.Attributes["SO_HDTM"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai.Substring(0, 50));

            if (this.NgayHoaDonThuongMai.Year > 1900)
                nodeToKhai.Attributes["NGAY_HDTM"].Value = this.NgayHoaDonThuongMai.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_HDTM"].Value = null;

            nodeToKhai.Attributes["MA_PTVT"].Value = this.PTVT_ID;

            if (this.SoHieuPTVT.Length <= 20)
                nodeToKhai.Attributes["TEN_PTVT"].Value = this.SoHieuPTVT;
            else
                nodeToKhai.Attributes["TEN_PTVT"].Value = this.SoHieuPTVT.Substring(0, 20);

            if (this.NgayDenPTVT.Year > 1900)
                nodeToKhai.Attributes["NGAY_DEN"].Value = this.NgayDenPTVT.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_DEN"].Value = null;

            if (SoVanDon.Length <= 20)
                nodeToKhai.Attributes["SO_VANDON"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon);
            else
                nodeToKhai.Attributes["SO_VANDON"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon.Substring(0, 20));

            if (this.NgayVanDon.Year > 1900)
                nodeToKhai.Attributes["NGAY_VANDON"].Value = this.NgayVanDon.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_VANDON"].Value = null;

            nodeToKhai.Attributes["NUOC_XK"].Value = this.NuocXK_ID;
            nodeToKhai.Attributes["NUOC_NK"].Value = "VN";

            if (this.DiaDiemXepHang.Length <= 40)
                nodeToKhai.Attributes["CANG_XUAT"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
            else
                nodeToKhai.Attributes["CANG_XUAT"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));

            nodeToKhai.Attributes["MA_CK_NHAP"].Value = this.CuaKhau_ID;
            nodeToKhai.Attributes["MA_DKGH"].Value = this.DKGH_ID;
            nodeToKhai.Attributes["MA_NGTE"].Value = this.NguyenTe_ID;
            nodeToKhai.Attributes["TY_GIA_VND"].Value = BaseClass.Round(this.TyGiaTinhThue, 9);
            nodeToKhai.Attributes["MA_PTTT"].Value = this.PTTT_ID;

            if (this.GiayTo.Length > 40)
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo.Substring(0, 40));
            else
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo);

            nodeToKhai.Attributes["SO_CONT20"].Value = Convert.ToUInt32(this.SoContainer20).ToString();
            nodeToKhai.Attributes["SO_CONT40"].Value = Convert.ToUInt32(this.SoContainer40).ToString();
            nodeToKhai.Attributes["SO_PLTK"].Value = Convert.ToUInt32(this.SoLuongPLTK).ToString();
            nodeToKhai.Attributes["SO_KIEN"].Value = Convert.ToUInt32(this.SoKien).ToString();
            nodeToKhai.Attributes["TRONG_LUONG"].Value = BaseClass.Round(this.TrongLuong, 9);
            nodeToKhai.Attributes["TRONG_LUONG_NET"].Value = BaseClass.Round(this.TrongLuongNet, 9);
            nodeToKhai.Attributes["PHI_BH"].Value = BaseClass.Round(this.PhiBaoHiem, 9);
            nodeToKhai.Attributes["PHI_VC"].Value = BaseClass.Round(this.PhiVanChuyen, 9);
            nodeToKhai.Attributes["LE_PHI_HQ"].Value = BaseClass.Round(this.LePhiHaiQuan, 9);

            if (this.TenChuHang.Length > 30)
                nodeToKhai.Attributes["CHU_HANG"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang.Substring(0, 30));
            else
                nodeToKhai.Attributes["CHU_HANG"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang);

            //CHUA CO PHAI BO SUNG
            if (this.DeXuatKhac.Length > 30)
                nodeToKhai.Attributes["DE_XUAT_KHAC"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DeXuatKhac.Substring(0, 30));
            else
                nodeToKhai.Attributes["DE_XUAT_KHAC"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DeXuatKhac);
            if (this.LyDoSua.Length > 30)
                nodeToKhai.Attributes["LY_DO_SUA"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.LyDoSua.Substring(0, 30));
            else
                nodeToKhai.Attributes["LY_DO_SUA"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.LyDoSua);
            nodeToKhai.Attributes["SOTK"].Value = this.SoToKhai + "";
            nodeToKhai.Attributes["NGAY_DK"].Value = this.NgayDangKy.ToString("MM/dd/yyyy");


            //Kiem tra : neu khai sua to khai da duyet thi moi gan :


            //hang hoa
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                //HangMauDich hmd = new HangMauDich();
                //hmd.TKMD_ID = this.ID;
                //this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlNode nodeHang = docNPL.GetElementsByTagName("HANG")[0];
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("HANG.ITEM");
                XmlAttribute sttAtt = docNPL.CreateAttribute("STT_HANG");
                sttAtt.Value = hmd.SoThuTuHang.ToString();
                node.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docNPL.CreateAttribute("MA_HANG");
                maAtt.Value = hmd.MaPhu;
                node.Attributes.Append(maAtt);

                XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
                MaHSAtt.Value = hmd.MaHS;
                node.Attributes.Append(MaHSAtt);

                XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_HANG");
                if (hmd.TenHang.Length <= 256)
                    TenAtt.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang);
                else
                    TenAtt.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang.Substring(0, 256));
                node.Attributes.Append(TenAtt);

                XmlAttribute NuocAtt = docNPL.CreateAttribute("NUOC_XX");
                NuocAtt.Value = hmd.NuocXX_ID;
                node.Attributes.Append(NuocAtt);

                XmlAttribute SoLuongAtt = docNPL.CreateAttribute("LUONG");
                SoLuongAtt.Value = BaseClass.Round(hmd.SoLuong, 9);
                node.Attributes.Append(SoLuongAtt);

                XmlAttribute Ma_DVTAtt = docNPL.CreateAttribute("MA_DVT");
                Ma_DVTAtt.Value = hmd.DVT_ID;
                node.Attributes.Append(Ma_DVTAtt);

                XmlAttribute DonGiaAtt = docNPL.CreateAttribute("DGIA_NT");
                DonGiaAtt.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                node.Attributes.Append(DonGiaAtt);

                XmlAttribute TriGiaAtt = docNPL.CreateAttribute("TGIA_NT");
                TriGiaAtt.Value = BaseClass.Round(hmd.TriGiaKB, 9); ;
                node.Attributes.Append(TriGiaAtt);

                XmlAttribute DonGiaVNDAtt = docNPL.CreateAttribute("DGIA_TT_VND");
                DonGiaVNDAtt.Value = BaseClass.Round(hmd.DonGiaTT, 9); ;
                node.Attributes.Append(DonGiaVNDAtt);

                XmlAttribute TriGiaVNDAtt = docNPL.CreateAttribute("TGIA_TT_VND");
                TriGiaVNDAtt.Value = BaseClass.Round(hmd.TriGiaTT, 9); ;
                node.Attributes.Append(TriGiaVNDAtt);

                XmlAttribute TS_XNKAtt = docNPL.CreateAttribute("TS_XNK");
                TS_XNKAtt.Value = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.Attributes.Append(TS_XNKAtt);

                XmlAttribute TS_TTDBAtt = docNPL.CreateAttribute("TS_TTDB");
                TS_TTDBAtt.Value = BaseClass.Round(hmd.ThueSuatTTDB, 9);
                node.Attributes.Append(TS_TTDBAtt);

                XmlAttribute TS_VSTAtt = docNPL.CreateAttribute("TS_VAT");
                TS_VSTAtt.Value = BaseClass.Round(hmd.ThueSuatGTGT, 9);
                node.Attributes.Append(TS_VSTAtt);

                XmlAttribute TL_PhuThuAtt = docNPL.CreateAttribute("TL_PHU_THU");
                TL_PhuThuAtt.Value = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.Attributes.Append(TL_PhuThuAtt);

                XmlAttribute ThueXNKAtt = docNPL.CreateAttribute("THUE_XNK");
                ThueXNKAtt.Value = BaseClass.Round(hmd.ThueXNK, 9);
                node.Attributes.Append(ThueXNKAtt);

                XmlAttribute ThueTTDBAtt = docNPL.CreateAttribute("THUE_TTDB");
                ThueTTDBAtt.Value = BaseClass.Round(hmd.ThueTTDB, 9);
                node.Attributes.Append(ThueTTDBAtt);

                XmlAttribute ThueVATAtt = docNPL.CreateAttribute("THUE_VAT");
                ThueVATAtt.Value = BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(ThueVATAtt);

                XmlAttribute TPhuThuAtt = docNPL.CreateAttribute("PHU_THU");
                TPhuThuAtt.Value = BaseClass.Round(hmd.TriGiaThuKhac, 9);
                node.Attributes.Append(TPhuThuAtt);

                nodeHang.AppendChild(node);
            }
            //chung tu*/
            //Linhhtn update 20100616
            return docNPL.InnerXml;
        }

        private string ConvertCollectionToXMLToKhaiXuatKD()
        {
            //load du lieu
            //NumberFormatInfo f = new NumberFormatInfo();
            //f.NumberDecimalSeparator = ".";
            //f.NumberGroupSeparator = ",";

            //Linhhtn update 20100616
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();


            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\XMLKinhDoanh\\SuaToKhaiXuatDaDuyet.xml");

            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(Globals.trimMaHaiQuan(this._MaHaiQuan));

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            //thong tin to khai
            XmlNode nodeToKhai = docNPL.GetElementsByTagName("TO_KHAI")[0];
            nodeToKhai.Attributes["MA_LH"].Value = this.MaLoaiHinh;
            nodeToKhai.Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);
            nodeToKhai.Attributes["MA_DV_XK"].Value = "";
            nodeToKhai.Attributes["MA_DV_UT"].Value = this.MaDonViUT;
            nodeToKhai.Attributes["MA_DV_NK"].Value = this.MaDoanhNghiep;
            nodeToKhai.Attributes["MA_DVKT"].Value = this.MaDaiLyTTHQ;
            if (this.TenDonViDoiTac.Length > 30)
                nodeToKhai.Attributes["TEN_DV_XK"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac.Substring(0, 30));
            else
                nodeToKhai.Attributes["TEN_DV_XK"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenDonViDoiTac);
            if (this.SoGiayPhep.Length <= 35)
                nodeToKhai.Attributes["SO_GP"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep);
            else
                nodeToKhai.Attributes["SO_GP"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoGiayPhep.Substring(0, 35));
            if (this.NgayGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_GP"].Value = this.NgayGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_GP"].Value = null;
            if (this.SoHopDong.Length <= 50)
                nodeToKhai.Attributes["SO_HD"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHopDong);
            else
                nodeToKhai.Attributes["SO_HD"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHopDong.Substring(0, 50));
            if (this.NgayHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HD"].Value = this.NgayHopDong.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_HD"].Value = null;
            if (this.NgayHetHanHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHHD"].Value = this.NgayHetHanHopDong.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_HHHD"].Value = null;
            if (this.SoHoaDonThuongMai.Length <= 50)
                nodeToKhai.Attributes["SO_HDTM"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai);
            else
                nodeToKhai.Attributes["SO_HDTM"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai.Substring(0, 50));
            if (this.NgayHoaDonThuongMai.Year > 1900)
                nodeToKhai.Attributes["NGAY_HDTM"].Value = this.NgayHoaDonThuongMai.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_HDTM"].Value = null;
            nodeToKhai.Attributes["MA_PTVT"].Value = this.PTVT_ID;
            if (this.SoHieuPTVT.Length <= 20)
                nodeToKhai.Attributes["TEN_PTVT"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT);
            else
                nodeToKhai.Attributes["TEN_PTVT"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoHieuPTVT.Substring(0, 20));
            if (this.NgayDenPTVT.Year > 1900)
                nodeToKhai.Attributes["NGAY_DEN"].Value = this.NgayDenPTVT.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_DEN"].Value = null;
            if (SoVanDon.Length <= 25)
                nodeToKhai.Attributes["SO_VANDON"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon);
            else
                nodeToKhai.Attributes["SO_VANDON"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.SoVanDon.Substring(0, 25));
            if (this.NgayVanDon.Year > 1900)
                nodeToKhai.Attributes["NGAY_VANDON"].Value = this.NgayVanDon.ToString("MM/dd/yyyy");
            else
                nodeToKhai.Attributes["NGAY_VANDON"].Value = null;
            nodeToKhai.Attributes["NUOC_XK"].Value = "VN";
            nodeToKhai.Attributes["NUOC_NK"].Value = this.NuocNK_ID;
            if (DiaDiemXepHang.Length <= 40)
                nodeToKhai.Attributes["CANG_XUAT"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
            else
                nodeToKhai.Attributes["CANG_XUAT"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));
            nodeToKhai.Attributes["MA_CK_NHAP"].Value = this.CuaKhau_ID;
            nodeToKhai.Attributes["MA_DKGH"].Value = this.DKGH_ID;
            nodeToKhai.Attributes["MA_NGTE"].Value = this.NguyenTe_ID;

            //Linhhtn update 20100616 - update ToString(f) to BaseClass.Round();
            nodeToKhai.Attributes["TY_GIA_VND"].Value = BaseClass.Round(this.TyGiaTinhThue, 9);

            string strFront = GetFrontTGVND(BaseClass.Round(this.TyGiaTinhThue, 9));
            string strEnd = GetEndTGVND(BaseClass.Round(this.TyGiaTinhThue, 9));

            //Linhhtn comment
            //if (strEnd != "")
            //    nodeToKhai.Attributes["TY_GIA_VND"].Value = strFront.Trim() + '.' + strEnd.Trim();
            //else
            //    nodeToKhai.Attributes["TY_GIA_VND"].Value = strFront.Trim();

            nodeToKhai.Attributes["MA_PTTT"].Value = this.PTTT_ID;
            if (this.GiayTo.Length > 40)
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo.Substring(0, 40));
            else
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.GiayTo);
            nodeToKhai.Attributes["SO_CONT20"].Value = Convert.ToUInt32(this.SoContainer20).ToString();
            nodeToKhai.Attributes["SO_CONT40"].Value = Convert.ToUInt32(this.SoContainer40).ToString();
            nodeToKhai.Attributes["SO_PLTK"].Value = Convert.ToUInt32(this.SoLuongPLTK).ToString();
            nodeToKhai.Attributes["SO_KIEN"].Value = Convert.ToUInt32(this.SoKien).ToString();
            nodeToKhai.Attributes["TRONG_LUONG"].Value = BaseClass.Round(this.TrongLuong, 9);
            nodeToKhai.Attributes["TRONG_LUONG_NET"].Value = BaseClass.Round(this.TrongLuongNet, 9);
            nodeToKhai.Attributes["PHI_BH"].Value = BaseClass.Round(this.PhiBaoHiem, 9);
            nodeToKhai.Attributes["PHI_VC"].Value = BaseClass.Round(this.PhiVanChuyen, 9);
            nodeToKhai.Attributes["LE_PHI_HQ"].Value = BaseClass.Round(this.LePhiHaiQuan, 9);

            if (this.TenChuHang.Length > 30)
                nodeToKhai.Attributes["CHU_HANG"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang.Substring(0, 30));
            else
                nodeToKhai.Attributes["CHU_HANG"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.TenChuHang);

            //Check thong tin sua to khai :
            //CHUA CO PHAI BO SUNG
            if (this.DeXuatKhac.Length > 30)
                nodeToKhai.Attributes["DE_XUAT_KHAC"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DeXuatKhac.Substring(0, 30));
            else
                nodeToKhai.Attributes["DE_XUAT_KHAC"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.DeXuatKhac);
            if (this.LyDoSua.Length > 30)
                nodeToKhai.Attributes["LY_DO_SUA"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.LyDoSua.Substring(0, 30));
            else
                nodeToKhai.Attributes["LY_DO_SUA"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(this.LyDoSua);
            nodeToKhai.Attributes["SOTK"].Value = this.SoToKhai + "";
            nodeToKhai.Attributes["NGAY_DK"].Value = this.NgayDangKy.ToString();

            //hang hoa
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                //HangMauDich hmd = new HangMauDich();
                //hmd.TKMD_ID = this.ID;
                //this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlNode nodeHang = docNPL.GetElementsByTagName("HANG")[0];
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("HANG.ITEM");
                XmlAttribute sttAtt = docNPL.CreateAttribute("STT_HANG");
                sttAtt.Value = hmd.SoThuTuHang.ToString();
                node.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docNPL.CreateAttribute("MA_HANG");
                maAtt.Value = hmd.MaPhu;
                node.Attributes.Append(maAtt);

                XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
                MaHSAtt.Value = hmd.MaHS;
                node.Attributes.Append(MaHSAtt);

                XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_HANG");
                if (hmd.TenHang.Length <= 256)
                    TenAtt.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang);
                else
                    TenAtt.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(hmd.TenHang.Substring(0, 256));
                node.Attributes.Append(TenAtt);

                XmlAttribute NuocAtt = docNPL.CreateAttribute("NUOC_XX");
                NuocAtt.Value = hmd.NuocXX_ID;
                node.Attributes.Append(NuocAtt);

                XmlAttribute SoLuongAtt = docNPL.CreateAttribute("LUONG");
                SoLuongAtt.Value = BaseClass.Round(hmd.SoLuong, 9);
                node.Attributes.Append(SoLuongAtt);

                XmlAttribute Ma_DVTAtt = docNPL.CreateAttribute("MA_DVT");
                Ma_DVTAtt.Value = hmd.DVT_ID;
                node.Attributes.Append(Ma_DVTAtt);

                XmlAttribute DonGiaAtt = docNPL.CreateAttribute("DGIA_NT");
                DonGiaAtt.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                node.Attributes.Append(DonGiaAtt);

                XmlAttribute TriGiaAtt = docNPL.CreateAttribute("TGIA_NT");
                TriGiaAtt.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                node.Attributes.Append(TriGiaAtt);

                XmlAttribute DonGiaVNDAtt = docNPL.CreateAttribute("DGIA_TT_VND");
                DonGiaVNDAtt.Value = BaseClass.Round(hmd.DonGiaTT, 9);
                node.Attributes.Append(DonGiaVNDAtt);

                XmlAttribute TriGiaVNDAtt = docNPL.CreateAttribute("TGIA_TT_VND");
                TriGiaVNDAtt.Value = BaseClass.Round(hmd.TriGiaTT, 9);
                node.Attributes.Append(TriGiaVNDAtt);

                XmlAttribute TS_XNKAtt = docNPL.CreateAttribute("TS_XNK");
                TS_XNKAtt.Value = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.Attributes.Append(TS_XNKAtt);

                XmlAttribute TS_TTDBAtt = docNPL.CreateAttribute("TS_TTDB");
                TS_TTDBAtt.Value = BaseClass.Round(hmd.ThueSuatTTDB, 9);
                node.Attributes.Append(TS_TTDBAtt);

                XmlAttribute TS_VSTAtt = docNPL.CreateAttribute("TS_VAT");
                TS_VSTAtt.Value = BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(TS_VSTAtt);

                XmlAttribute TL_PhuThuAtt = docNPL.CreateAttribute("TL_PHU_THU");
                TL_PhuThuAtt.Value = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.Attributes.Append(TL_PhuThuAtt);

                XmlAttribute ThueXNKAtt = docNPL.CreateAttribute("THUE_XNK");
                ThueXNKAtt.Value = BaseClass.Round(hmd.ThueXNK, 9);
                node.Attributes.Append(ThueXNKAtt);

                XmlAttribute ThueTTDBAtt = docNPL.CreateAttribute("THUE_TTDB");
                ThueTTDBAtt.Value = BaseClass.Round(hmd.ThueTTDB, 9);
                node.Attributes.Append(ThueTTDBAtt);

                XmlAttribute ThueVATAtt = docNPL.CreateAttribute("THUE_VAT");
                ThueVATAtt.Value = BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(ThueVATAtt);

                XmlAttribute TPhuThuAtt = docNPL.CreateAttribute("PHU_THU");
                TPhuThuAtt.Value = BaseClass.Round(hmd.TriGiaThuKhac, 9);
                node.Attributes.Append(TPhuThuAtt);

                nodeHang.AppendChild(node);
            }

            return docNPL.InnerXml;
        }

        public string WSRequestSuaToKhai(string pass)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GUIDSTR)));

            XmlDocument xmlDocumentTemplate = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");

            XmlNode root = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            //root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.TenDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(Globals.trimMaHaiQuan(this._MaHaiQuan)));

            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode Content = xmlDocument.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();


            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    if (i > 1)
                        System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(xmlDocument.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không thể kết nối tới hệ thống hải quan!" + "|" + "DOTNET_LEVEL");
                }
                xmlDocumentTemplate = new XmlDocument();
                xmlDocumentTemplate.LoadXml(kq);
                Result = xmlDocumentTemplate.SelectSingleNode("Envelope/Body/Content/Root");

                if (Result.Attributes["Err"].Value == "no")
                {
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        this.ActionStatus = 1;
                        break;
                    }
                    else
                    {
                        this.ActionStatus = -1;
                    }
                }
                else
                {
                    if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
                    {
                        throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlDocumentTemplate.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                    }
                    else
                        throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlDocumentTemplate.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
                }
            }

            if (i > 1)
                return xmlDocument.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlDocumentTemplate.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }

            //XmlElement dd  =Result.InnerText;

            if (xmlDocumentTemplate.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
            {
                try
                {
                    //kiem tra thong tin tra ve tu XML de xu ly :
                    if (xmlDocumentTemplate.SelectSingleNode("Envelope/Body/Content/Root").Attributes["SOTN"].Value != "")
                    {

                        this.SoTiepNhan = Convert.ToInt64(xmlDocumentTemplate.SelectSingleNode("Envelope/Body/Content/Root").Attributes["SOTN"].Value);
                        this.NgayTiepNhan = Convert.ToDateTime(xmlDocumentTemplate.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Ngay_TN"].Value);
                        this.ActionStatus = 1;
                    }

                }
                catch (Exception exx)
                {
                    Logger.LocalLogger.Instance().WriteMessage(exx);
                    //throw new Exception( " Tờ khai chưa được cấp số và chưa phân luồng"); 
                }

                this.Update();

                try
                {
                    XmlNode nodeMess = xmlDocumentTemplate.SelectSingleNode("Envelope/Body/Content/Root");
                    if (nodeMess.InnerText != "")
                    {
                        XmlElement mess = xmlDocumentTemplate.CreateElement("Megs");
                        XmlElement messCon = xmlDocumentTemplate.CreateElement("Meg");
                        messCon.InnerText = nodeMess.InnerText;
                        mess.AppendChild(messCon);
                        return mess.OuterXml;
                    }
                }
                catch { }
            }
            //}
            return "";

        }
        #endregion SUA THONG TIN TO KHAI ĐA DUYET :

        /// <summary>
        /// Hủy khai báo tờ khai
        /// </summary>
        /// <param name="password"></param>
        /// <returns>Thành công | Không thành công</returns>
        public bool WSHuyKhaiBao(string password)
        {
            XmlDocument doc = new XmlDocument();
            if (this.MaLoaiHinh.StartsWith("N"))
                doc.LoadXml(GenerateToXmlEnvelope(MessageTypes.ToKhaiNhap, MessageFunctions.HuyKhaiBao, new Guid(this.GUIDSTR)));
            else
                doc.LoadXml(GenerateToXmlEnvelope(MessageTypes.ToKhaiXuat, MessageFunctions.HuyKhaiBao, new Guid(this.GUIDSTR)));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + @"\XMLKinhDoanh\HuyKhaiBaoToKhai.XML");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);

            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep.Trim();
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.TenDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);

            root.SelectSingleNode("DU_LIEU").Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            root.SelectSingleNode("DU_LIEU").Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = string.Empty;
            try
            {
                kq = kdt.Send(doc.InnerXml, password);
                doc.InnerXml.XmlSaveMessage(ID, MessageTitle.HuyKhaiBaoToKhai);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);
                if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Ok"].Value == "yes")
                        {
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.HUY_KHAI_BAO;
                            this.Update();
                            return true;
                        }
                        else
                        {
                            msgError = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL";
                            return true;
                            //throw new Exception(msgError);
                        }
                    }
                }
                else
                {
                    msgError = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL";
                    throw new Exception(msgError);
                }
                return false;
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty) kq.XmlSaveMessage(ID, MessageTitle.Error);
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                }
                throw;
            }
        }

        #endregion Khai báo tờ khai



        /// <summary>
        /// Nhận phản hồi thông quan điện tử sau khi khai báo đã có số tiếp nhận
        /// </summary>
        /// <param name="password">password</param>
        /// <returns>Thành công | Không thành công</returns>
        public bool WSLayPhanHoi(string password)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GUIDSTR)));

            XmlDocument xmlDocumentTemplate = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");

            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.MaDoanhNghiep.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";

            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không thể kết nối tới hệ thống Hải quan!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);

                XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");

                if (xmlNodeResult.Attributes["Err"].Value.ToLower() == "yes")
                {
                    if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                    {
                        msgError = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                        if (msgError == "Tờ khai này không tồn tại|")
                        {
                            this.TrangThaiXuLy = -1;
                            this.ActionStatus = 0;
                            this.Update();
                            string msgContent = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                            kq.XmlSaveMessage(ID, MessageTitle.TuChoiTiepNhan, msgContent);

                        }
                        throw new Exception(msgError);
                    }
                    else
                    {
                        msgError = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                        throw new Exception(msgError);
                    }
                }

                // Chưa có phản hồi từ Hải quan
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    return false;
                }


                if (xmlNodeResult.Attributes["TrangThai"].Value == "yes")
                {

                    // BEGIN: Xử lý thông tin từ chối tiếp nhận.
                    if (xmlNodeResult.Attributes["TuChoi"] != null)
                    {
                        if (xmlNodeResult.Attributes["TuChoi"].Value == "yes")
                        {
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                            this.ActionStatus = 2;
                            this.Update();
                            string msgContent = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                            kq.XmlSaveMessage(ID, MessageTitle.TuChoiTiepNhan, msgContent);

                            return true;
                        }
                    }
                    // END: Xử lý thông tin từ chối tiếp nhận.
                    else
                    {
                        if (xmlNodeResult.Attributes["SOTK"] != null && this.SoToKhai == 0)
                        {
                            this.SoToKhai = Convert.ToInt32(xmlNodeResult.Attributes["SOTK"].Value);
                            this.MaLoaiHinh = xmlNodeResult.Attributes["MALH"].Value;
                            this.NamDK = Convert.ToInt32(xmlNodeResult.Attributes["NAMDK"].Value);
                            this.NgayDangKy = Convert.ToDateTime(xmlNodeResult.Attributes["NGAYDK"].Value);
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                            this.ActionStatus = 1;

                            if (xmlNodeResult.Attributes["SOTN"] != null)
                            {
                                this.SoTiepNhan = Convert.ToInt32(xmlNodeResult.Attributes["SOTN"].Value);
                                //this.NgayTiepNhan = this.NgayDangKy;
                            }

                            this.Update();
                            string msgContent = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), Globals.trimMaHaiQuan(this.MaHaiQuan).Trim());
                            kq.XmlSaveMessage(ID, MessageTitle.ToKhaiDuocCapSo, msgContent);

                            XmlNode nodePhanLuong = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root/PHAN_LUONG");
                            if (nodePhanLuong != null)
                            {
                                this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value.Trim();
                                this.HUONGDAN = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodePhanLuong.Attributes["HUONGDAN"].Value);

                                string tenluong = "Xanh";
                                if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                                {
                                    tenluong = "Vàng";
                                }
                                else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                                {
                                    tenluong = "Đỏ";
                                }

                                this.Update();
                                string msgContent2 = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), Globals.trimMaHaiQuan(this.MaHaiQuan).Trim(), tenluong, this.HUONGDAN);
                                kq.XmlSaveMessage(ID, MessageTitle.ToKhaiDuocPhanLuong, msgContent2);

                                //AN DINH THUE
                                AnDinhThue(xmlDocumentResult, kq);
                            }
                            return true;
                        }
                        else if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["SO_TN"] != null)
                        {
                            this.SoTiepNhan = Convert.ToInt32(xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["SO_TN"].Value);
                            this.NgayTiepNhan = Convert.ToDateTime(xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Ngay_TN"].Value);
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;

                            this.Update();
                            string msgContent = string.Format("Số tiếp nhận {0}, ngày tiếp nhận {1}", SoTiepNhan, NgayTiepNhan);
                            kq.XmlSaveMessage(ID, MessageTitle.ToKhaiDuocCapSoTiepNhan, msgContent);
                            return true;
                        }
                        else if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root/PHAN_LUONG") != null)
                        {
                            XmlNode xmlNodeResultPhanLuong = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root/PHAN_LUONG");
                            if (xmlNodeResultPhanLuong.Attributes["MALUONG"].Value != this.PhanLuong.Trim())
                            {
                                this.PhanLuong = xmlNodeResultPhanLuong.Attributes["MALUONG"].Value.Trim();
                                this.HUONGDAN = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlNodeResultPhanLuong.Attributes["HUONGDAN"].Value);
                                this.ActionStatus = 1;
                                //Hungtq, khong can cap nhat lai trang thai to khai. Khi da duoc cap so tk la DA DUYET.
                                //this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;

                                string tenluong = "Xanh";
                                if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                                {
                                    tenluong = "Vàng";
                                }
                                else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                                {
                                    tenluong = "Đỏ";
                                }

                                this.Update();
                                string msgContent = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), Globals.trimMaHaiQuan(this.MaHaiQuan).Trim(), tenluong, this.HUONGDAN);
                                kq.XmlSaveMessage(ID, MessageTitle.ToKhaiDuocPhanLuong, msgContent);
                            }

                            //AN DINH THUE
                            AnDinhThue(xmlDocumentResult, kq);

                            return true;
                        }
                        else if (xmlNodeResult.Attributes["SOTK"].Value == this.SoToKhai.ToString() && Int32.Parse(xmlNodeResult.Attributes["SOTK"].Value) > 0)
                        {
                            this.SoToKhai = Convert.ToInt32(xmlNodeResult.Attributes["SOTK"].Value);
                            this.MaLoaiHinh = xmlNodeResult.Attributes["MALH"].Value;
                            this.NamDK = Convert.ToInt32(xmlNodeResult.Attributes["NAMDK"].Value);
                            this.NgayDangKy = Convert.ToDateTime(xmlNodeResult.Attributes["NGAYDK"].Value);
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                            this.ActionStatus = 1;

                            this.Update();

                            string msgContent = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}", SoToKhai, NgayDangKy);
                            kq.XmlSaveMessage(ID, MessageTitle.ToKhaiDuocCapSo, msgContent);

                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                {
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                }
                throw;
            }

        }

        #endregion Lấy phản hồi

        /// <summary>
        /// Ấn định Thuế
        /// </summary>
        /// <param name="docNPL"></param>
        /// <param name="kq"></param>
        private void AnDinhThue(XmlDocument docNPL, string kq)
        {
            try
            {
                #region BEGIN AN DINH THUE
                //DATLMQ bổ sung In ra thông báo thuế 06/08/2011
                XmlNode xmlNodeAnDinhThue = docNPL.SelectSingleNode("Envelope/Body/Content/Root/AN_DINH_THUE");
                if (xmlNodeAnDinhThue != null)
                {
                    IsThongBaoThue = true;
                    SoQD = xmlNodeAnDinhThue.Attributes["SOQD"].Value;
                    NgayQD = Convert.ToDateTime(xmlNodeAnDinhThue.Attributes["NGAYQD"].Value);
                    NgayHetHan = Convert.ToDateTime(xmlNodeAnDinhThue.Attributes["NGAYHH"].Value);
                    TaiKhoanKhoBac = xmlNodeAnDinhThue.Attributes["TKKB"].Value;
                    TenKhoBac = xmlNodeAnDinhThue.Attributes["KHOBAC"].Value;

                    //Kiem tra thong tin an dinh thue cau to khai
                    AnDinhThue anDinhThueObj = new AnDinhThue();
                    List<AnDinhThue> dsADT = (List<AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(this.ID);
                    if (dsADT.Count > 0)
                    {
                        anDinhThueObj.ID = dsADT[0].ID;
                    }

                    //Luu an dinh thue
                    anDinhThueObj.SoQuyetDinh = SoQD;
                    anDinhThueObj.NgayQuyetDinh = NgayQD;
                    anDinhThueObj.NgayHetHan = NgayHetHan;
                    anDinhThueObj.TaiKhoanKhoBac = TaiKhoanKhoBac;
                    anDinhThueObj.TenKhoBac = TenKhoBac;
                    anDinhThueObj.GhiChu = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim(), Company.KDT.SHARE.Components.Globals.GetPhanLuong(this.PhanLuong), this.HUONGDAN);
                    anDinhThueObj.TKMD_ID = ID;
                    anDinhThueObj.TKMD_Ref = GUIDSTR;
                    if (anDinhThueObj.ID == 0)
                        anDinhThueObj.Insert();
                    else
                        anDinhThueObj.InsertUpdate();

                    #region Begin Danh sach thue chi tiet
                    ChiTiet thueObj = new ChiTiet();

                    XmlNodeList xmlNodeThue = docNPL.SelectNodes("Envelope/Body/Content/Root/AN_DINH_THUE/THUE");
                    foreach (XmlNode node in xmlNodeThue)
                    {
                        if (node.Attributes["SACTHUE"].Value.Equals("XNK"))
                        {
                            ChuongThueXNK = node.Attributes["CHUONG"].Value;
                            SoTienThueXNK = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                            LoaiThueXNK = node.Attributes["LOAI"].Value;
                            KhoanThueXNK = node.Attributes["KHOAN"].Value;
                            MucThueXNK = node.Attributes["MUC"].Value;
                            TieuMucThueXNK = node.Attributes["TIEUMUC"].Value;

                            //Tao doi tuong thue XNK
                            thueObj = new ChiTiet();
                            thueObj.SacThue = "XNK";
                            thueObj.Chuong = ChuongThueXNK;
                            thueObj.TienThue = (decimal)SoTienThueXNK;
                            thueObj.Loai = int.Parse(LoaiThueXNK);
                            thueObj.Khoan = int.Parse(KhoanThueXNK);
                            thueObj.Muc = int.Parse(MucThueXNK);
                            thueObj.TieuMuc = int.Parse(TieuMucThueXNK);
                            thueObj.IDAnDinhThue = anDinhThueObj.ID;
                            thueObj.InsertUpdateBy(null);
                        }
                        if (node.Attributes["SACTHUE"].Value.Equals("VAT"))
                        {
                            ChuongThueVAT = node.Attributes["CHUONG"].Value;
                            SoTienThueVAT = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                            LoaiThueVAT = node.Attributes["LOAI"].Value;
                            KhoanThueVAT = node.Attributes["KHOAN"].Value;
                            MucThueVAT = node.Attributes["MUC"].Value;
                            TieuMucThueVAT = node.Attributes["TIEUMUC"].Value;

                            //Tao doi tuong thue VAT
                            thueObj = new ChiTiet();
                            thueObj.SacThue = "VAT";
                            thueObj.Chuong = ChuongThueVAT;
                            thueObj.TienThue = (decimal)SoTienThueVAT;
                            thueObj.Loai = int.Parse(LoaiThueVAT);
                            thueObj.Khoan = int.Parse(KhoanThueVAT);
                            thueObj.Muc = int.Parse(MucThueVAT);
                            thueObj.TieuMuc = int.Parse(TieuMucThueVAT);
                            thueObj.IDAnDinhThue = anDinhThueObj.ID;
                            thueObj.InsertUpdateBy(null);
                        }
                        if (node.Attributes["SACTHUE"].Value.Equals("TTDB"))
                        {
                            ChuongThueTTDB = node.Attributes["CHUONG"].Value;
                            SoTienThueTTDB = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                            LoaiThueTTDB = node.Attributes["LOAI"].Value;
                            KhoanThueTTDB = node.Attributes["KHOAN"].Value;
                            MucThueTTDB = node.Attributes["MUC"].Value;
                            TieuMucThueTTDB = node.Attributes["TIEUMUC"].Value;

                            //Tao doi tuong thue Tieu thu dac biet
                            thueObj = new ChiTiet();
                            thueObj.SacThue = "TTDB";
                            thueObj.Chuong = ChuongThueTTDB;
                            thueObj.TienThue = (decimal)SoTienThueTTDB;
                            thueObj.Loai = int.Parse(LoaiThueTTDB);
                            thueObj.Khoan = int.Parse(KhoanThueTTDB);
                            thueObj.Muc = int.Parse(MucThueTTDB);
                            thueObj.TieuMuc = int.Parse(TieuMucThueTTDB);
                            thueObj.IDAnDinhThue = anDinhThueObj.ID;
                            thueObj.InsertUpdateBy(null);
                        }
                        if (node.Attributes["SACTHUE"].Value.Equals("TVCBPG"))
                        {
                            ChuongThueTVCBPG = node.Attributes["CHUONG"].Value;
                            SoTienThueTVCBPG = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                            LoaiThueTVCBPG = node.Attributes["LOAI"].Value;
                            KhoanThueTVCBPG = node.Attributes["KHOAN"].Value;
                            MucThueTVCBPG = node.Attributes["MUC"].Value;
                            TieuMucThueTVCBPG = node.Attributes["TIEUMUC"].Value;

                            //Tao doi tuong thue Thu chenh lech gia
                            thueObj = new ChiTiet();
                            thueObj.SacThue = "TVCBPG";
                            thueObj.Chuong = ChuongThueTVCBPG;
                            thueObj.TienThue = (decimal)SoTienThueTVCBPG;
                            thueObj.Loai = int.Parse(LoaiThueTVCBPG);
                            thueObj.Khoan = int.Parse(KhoanThueTVCBPG);
                            thueObj.Muc = int.Parse(MucThueTVCBPG);
                            thueObj.TieuMuc = int.Parse(TieuMucThueTVCBPG);
                            thueObj.IDAnDinhThue = anDinhThueObj.ID;
                            thueObj.InsertUpdateBy(null);
                        }
                    }


                    #endregion End Danh sach thue chi tiet

                    Company.KDT.SHARE.Components.KetQuaXuLy kqxlAnDinhThue = new Company.KDT.SHARE.Components.KetQuaXuLy();
                    kqxlAnDinhThue.ItemID = this.ID;
                    kqxlAnDinhThue.ReferenceID = new Guid(this.GUIDSTR);
                    kqxlAnDinhThue.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxlAnDinhThue.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_AnDinhThueToKhai;
                    kqxlAnDinhThue.NoiDung = string.Format("Ấn định thuế:\r\nSố quyết định: {0}\r\nNgày quyết định: {1}\r\nNgày hết hạn: {2}\r\nTài khoản kho bạc: {3}\r\nKho bạc: {4}", SoQD, NgayQD.ToString(), NgayHetHan.ToString(), TaiKhoanKhoBac, TenKhoBac);
                    kqxlAnDinhThue.Ngay = DateTime.Now;
                    kqxlAnDinhThue.Insert();

                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiAnDinhThue, kqxlAnDinhThue.NoiDung);
                }
                #endregion END AN DINH THUE
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(string.Format("Ấn định thuế tờ khai số: {0}, ngày đăng ký: {1}, mã loại hình: {2}, ID: {3}", this.SoToKhai, NgayDangKy.ToString(), MaLoaiHinh, GUIDSTR), ex); }
        }

        /// <summary>
        /// Lấy số tiếp nhận khai báo.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLaySoTiepNhan(string password)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument xmlDocument = new XmlDocument();

            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GUIDSTR), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode nodeFrom = xmlDocumentRequest.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = this.TenDoanhNghiep;
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode nodeTo = xmlDocumentRequest.GetElementsByTagName("To")[0];
            nodeTo.ChildNodes[0].InnerText = Globals.trimMaHaiQuan(this.MaHaiQuan);
            nodeTo.ChildNodes[1].InnerText = Globals.trimMaHaiQuan(this.MaHaiQuan);


            if (this.MaLoaiHinh.StartsWith("N"))
            {
                xmlDocumentRequest.GetElementsByTagName("type")[0].InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            }
            else
            {
                xmlDocumentRequest.GetElementsByTagName("type")[0].InnerText = ((int)MessageTypes.ToKhaiXuat).ToString();
            }

            xmlDocumentRequest.GetElementsByTagName("type")[0].InnerText = ((int)MessageTypes.ThongTin).ToString();
            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                //throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                return false;
            }

            if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.NamDK = this.NgayTiepNhan.Year;
                this.TrangThaiXuLy = 0;
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GUIDSTR);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.KhaiBao;
                message.MessageFrom = Globals.trimMaHaiQuan(this.MaHaiQuan).Trim();
                message.MessageTo = this.MaDoanhNghiep;
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Lấy thông báo hủy.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLayThongBaoHuy(string password)
        {

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(ConfigPhongBiLayPhanHoi(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi));

            XmlDocument xmlDocumentTemplate = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");

            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.TenDoanhNghiep;

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(Globals.trimMaHaiQuan(this.MaHaiQuan));

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();


            string kq = "";
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không thể kết nối tới hệ thống Hải quan!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);

                XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");

                if (xmlNodeResult.Attributes["Err"] == null)
                {
                    msgError = kq;
                    throw new Exception(kq);
                }
                if (xmlNodeResult.Attributes["Err"].Value.ToLower() == "no" && xmlNodeResult.Attributes["TrangThai"].Value.ToLower() == "yes"
                    && xmlNodeResult.Attributes["TRA_LOI"].Value.ToUpper() == "HUY_TK")
                {
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.SoTiepNhan = 0;
                    this.NgayTiepNhan = new DateTime(1900, 1, 1);
                    this.Update();
                    kq.XmlSaveMessage(ID, MessageTitle.HuyKhaiBaoThanhCong);
                    return true;
                }
                else
                    if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes" && xmlNodeResult.Attributes["TrangThai"].Value.ToLower() == "no" &&
                             xmlNodeResult.InnerText == "Tê khai nµy kh«ng tån t¹i")
                    {
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        this.SoTiepNhan = 0;
                        this.NgayTiepNhan = new DateTime(1900, 1, 1);
                        this.Update();
                        kq.XmlSaveMessage(ID, MessageTitle.HuyKhaiBaoThanhCong);
                        return true;
                    }
                    else if (xmlNodeResult.Attributes["Err"].Value.ToLower() == "yes")
                    {

                        msgError = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                        throw new Exception(msgError);
                    }

                // Có lỗi xác định trả về.
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {

                    kq.XmlSaveMessage(ID, MessageTitle.Error);
                    return false;
                }
                else
                {
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.SoTiepNhan = 0;
                    this.NgayTiepNhan = new DateTime(1900, 1, 1);
                    this.Update();
                    kq.XmlSaveMessage(ID, MessageTitle.HuyKhaiBaoThanhCong);
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    kq.XmlSaveMessage(ID, MessageTitle.Error);
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                }
                throw;
            }
        }

        #endregion Webservice của hải quan

        #region Dong Bo Du Lieu
        public void DongBoDuLieuKhaiDT(string MaDoanhNghiep, string MaHaiQuan, string nameConnectKDT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sqlDelete = "delete t_KDT_ToKhaiMauDich where MaDoanhNghiep=@DVGC and TrangThaiXuLy!=-1 and MaHaiQuan=@Ma_HQHD";
            SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlDelete);
            db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            db.ExecuteNonQuery(dbCommandDelete);

            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select * from DToKhaiMD where Ma_DV=@DVGC and TrangThai<>2 and Ma_HQ=@Ma_HQHD AND substring (Ma_LH,2,2) <> '%GC%'";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {

                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.CuaKhau_ID = row["Ma_CK"].ToString().Trim();
                    TKMD.DiaDiemXepHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["CangNN"].ToString());
                    TKMD.DKGH_ID = row["Ma_GH"].ToString();
                    TKMD.GiayTo = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["GiayTo"].ToString());
                    TKMD.SoVanDon = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Van_Don"].ToString());
                    TKMD.MaDoanhNghiep = MaDoanhNghiep;
                    TKMD.MaHaiQuan = MaHaiQuan;
                    TKMD.MaLoaiHinh = row["Ma_LH"].ToString();
                    TKMD.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"].ToString());
                    if (row["NgayDen"].ToString() != "")
                        TKMD.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                    if (row["Ngay_GP"].ToString() != "")
                        TKMD.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                    if (row["Ngay_HHGP"].ToString() != "")
                        TKMD.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                    if (row["Ngay_HHHD"].ToString() != "")
                        TKMD.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                    if (row["Ngay_HDTM"].ToString() != "")
                        TKMD.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM"].ToString());
                    if (row["Ngay_HD"].ToString() != "")
                        TKMD.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                    TKMD.NgayTiepNhan = Convert.ToDateTime(row["Ngay_DK"].ToString());
                    if (row["Ngay_VanDon"].ToString() != "")
                        TKMD.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                    TKMD.NguyenTe_ID = row["Ma_NT"].ToString();
                    if (TKMD.MaLoaiHinh.IndexOf("N") == 0)
                    {
                        TKMD.NuocNK_ID = row["Nuoc_NK"].ToString();
                        TKMD.NuocXK_ID = row["Nuoc_XK"].ToString();
                    }
                    else
                    {
                        TKMD.NuocNK_ID = row["Nuoc_XK"].ToString();
                        TKMD.NuocXK_ID = row["Nuoc_NK"].ToString();
                    }
                    if (row["Phi_BH"].ToString() != "")
                        TKMD.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                    if (row["Phi_VC"].ToString() != "")
                        TKMD.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                    TKMD.PTTT_ID = row["Ma_PTTT"].ToString();
                    TKMD.PTVT_ID = row["Ma_PTVT"].ToString();
                    if (row["So_Container"].ToString() != "")
                        TKMD.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                    if (row["So_container40"].ToString() != "")
                        TKMD.SoContainer40 = Convert.ToDecimal(row["So_container40"].ToString());
                    TKMD.SoGiayPhep = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["So_GP"].ToString());
                    TKMD.SoHieuPTVT = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Ten_PTVT"].ToString());
                    TKMD.SoHoaDonThuongMai = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["So_HDTM"].ToString());
                    TKMD.SoHopDong = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["So_HD"].ToString());
                    if (row["So_Kien"].ToString() != "")
                        TKMD.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                    if (row["So_PLTK"].ToString() != "")
                        TKMD.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                    TKMD.SoTiepNhan = Convert.ToInt64(row["SoTK"].ToString());
                    TKMD.TenChuHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["TenCH"].ToString());
                    TKMD.TrangThaiXuLy = 0;
                    if (row["Tr_Luong"].ToString() != "")
                        TKMD.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                    if (row["TyGia_VND"].ToString() != "")
                        TKMD.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                    if (row["TyGia_USD"].ToString() != "")
                        TKMD.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                    TKMD.TenDonViDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DV_DT"].ToString());
                    long idTK = Convert.ToInt64(row["TKID"].ToString());
                    TKMD.DongBoDuLieuKhaiDTByIDTKDK(TKMD, idTK, nameConnectKDT);
                }
            }

        }
        private void DongBoDuLieuKhaiDTByIDTKDK(ToKhaiMauDich TKMD, long IDTKOfHQ, string nameConnectKDT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select * from HQQLHGCTEMPT.dbo.DHangMDDK where  SoTK=@SoTK and Ma_LH=@Ma_LH and Ma_HQ=@Ma_HQ and NamDK=@NamDK";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, TKMD.SoTiepNhan);
            db.AddInParameter(dbCommand, "@Ma_LH", SqlDbType.VarChar, TKMD.MaLoaiHinh);
            db.AddInParameter(dbCommand, "@Ma_HQ", SqlDbType.VarChar, TKMD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, TKMD.NgayDangKy.Year);

            DataSet dsHang = db.ExecuteDataSet(dbCommand);
            TKMD.HMDCollection = new List<HangMauDich>();
            foreach (DataRow row in dsHang.Tables[0].Rows)
            {
                HangMauDich hang = new HangMauDich();
                if (row["DGia_KB"].ToString() != "")
                    hang.DonGiaKB = Convert.ToDouble(row["DGia_KB"].ToString());
                if (row["DGia_TT"].ToString() != "")
                    hang.DonGiaTT = Convert.ToDouble(row["DGia_TT"].ToString());
                hang.DVT_ID = row["Ma_DVT"].ToString();
                hang.MaHS = row["Ma_HangKB"].ToString();
                hang.MaPhu = row["Ma_Phu"].ToString().Substring(1);
                hang.SoThuTuHang = Convert.ToInt32(row["STTHang"]);
                TKMD.LoaiHangHoa = row["Ma_Phu"].ToString().Substring(0, 1);
                if (row["MienThue"].ToString() != "")
                    hang.MienThue = Convert.ToByte(row["MienThue"].ToString());
                hang.NuocXX_ID = row["Nuoc_XX"].ToString();
                if (row["Phu_Thu"].ToString() != "")
                    hang.PhuThu = Convert.ToDouble(row["Phu_Thu"].ToString());
                if (row["Luong"].ToString() != "")
                    hang.SoLuong = Convert.ToDecimal(row["Luong"].ToString());
                hang.TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Ten_Hang"].ToString());
                if (row["Thue_VAT"].ToString() != "")
                    hang.ThueGTGT = Convert.ToDouble(row["Thue_VAT"].ToString());
                if (row["TS_VAT"].ToString() != "")
                    hang.ThueSuatGTGT = Convert.ToDouble(row["TS_VAT"].ToString());
                if (row["TS_TTDB"].ToString() != "")
                    hang.ThueSuatTTDB = Convert.ToDouble(row["TS_TTDB"].ToString());
                if (row["TS_XNK"].ToString() != "")
                    hang.ThueSuatXNK = Convert.ToDouble(row["TS_XNK"].ToString());
                if (row["Thue_TTDB"].ToString() != "")
                    hang.ThueTTDB = Convert.ToDouble(row["Thue_TTDB"].ToString());
                if (row["Thue_XNK"].ToString() != "")
                    hang.ThueXNK = Convert.ToDouble(row["Thue_XNK"].ToString());
                if (row["TriGia_KB"].ToString() != "")
                    hang.TriGiaKB = Convert.ToDouble(row["TriGia_KB"].ToString());
                if (row["TGKB_VND"].ToString() != "")
                    hang.TriGiaKB_VND = Convert.ToDouble(row["TGKB_VND"].ToString());
                if (row["TriGia_ThuKhac"].ToString() != "")
                    hang.TriGiaThuKhac = Convert.ToDouble(row["TriGia_ThuKhac"].ToString());
                if (row["TriGia_TT"].ToString() != "")
                    hang.TriGiaTT = Convert.ToDouble(row["TriGia_TT"].ToString());
                if (row["TyLe_ThuKhac"].ToString() != "")
                    hang.TyLeThuKhac = Convert.ToDouble(row["TyLe_ThuKhac"].ToString());
                TKMD.HMDCollection.Add(hang);
            }
            TKMD.TrangThaiXuLy = 0;

            string sql1 = "select * from TTKTG_PP1 where  SoTK=@SoTK";
            SqlCommand dbCommand1 = (SqlCommand)db.GetSqlStringCommand(sql1);
            db.AddInParameter(dbCommand1, "@SoTK", SqlDbType.Int, TKMD.SoTiepNhan);
            DataSet dsTKTG = db.ExecuteDataSet(dbCommand1);
            TKMD.TKTGCollection = new ToKhaiTriGiaCollection();
            foreach (DataRow dr in dsTKTG.Tables[0].Rows)
            {
                ToKhaiTriGia TKTG = new ToKhaiTriGia();
                decimal TKTGID = Convert.ToDecimal(dr["TTKTG_PP1ID"]);
                TKTG.ToSo = Convert.ToInt64(dr["To_So"]);
                TKTG.LoaiToKhaiTriGia = dr["LOAI_TKTG"].ToString();
                TKTG.NgayXuatKhau = Convert.ToDateTime(dr["NGAY_XK"]);
                TKTG.CapDoThuongMai = dr["Cap_Do_TM"].ToString();
                if (dr["Quyen_SD"].ToString() == "1")
                    TKTG.QuyenSuDung = true;
                else
                    TKTG.QuyenSuDung = false;
                if (dr["Khong_XD"].ToString() == "1")
                    TKTG.KhongXacDinh = true;
                else
                    TKTG.KhongXacDinh = false;
                if (dr["Tra_Them"].ToString() == "1")
                    TKTG.TraThem = true;
                else
                    TKTG.TraThem = false;
                if (dr["Tien_Tra_16"].ToString() == "1")
                    TKTG.TienTra = true;
                else
                    TKTG.TienTra = false;
                if (dr["Co_QHDB"].ToString() == "1")
                    TKTG.CoQuanHeDacBiet = true;
                else
                    TKTG.CoQuanHeDacBiet = false;
                if (dr["Anh_Huong_QH"].ToString() == "1")
                    TKTG.CoQuanHeDacBiet = true;
                else
                    TKTG.CoQuanHeDacBiet = false;

                TKTG.GhiChep = dr["Ghi_Chep_KT"].ToString();
                TKTG.NguoiKhaiBao = dr["Nguoi_KB"].ToString();
                TKTG.NgayKhaiBao = Convert.ToDateTime(dr["Ngay_KB"]);
                TKTG.ChucDanhNguoiKhaiBao = dr["Chuc_Danh_KB"].ToString();
                TKTG.KieuQuanHe = dr["Kieu_QHDB"].ToString();

                TKTG.HTGCollection = new HangTriGiaCollection();
                string sql2 = "select * from TCT_TKTG_PP1 where  SoTK=" + TKMD.SoTiepNhan + " AND TTKTG_PP1ID =" + TKTGID;
                SqlCommand dbCommand2 = (SqlCommand)db.GetSqlStringCommand(sql2);
                DataSet dsHTG = db.ExecuteDataSet(dbCommand2);
                foreach (DataRow dr1 in dsHTG.Tables[0].Rows)
                {
                    HangTriGia HTG = new HangTriGia();
                    HTG.STTHang = Convert.ToInt32(dr1["STTHang"]);
                    foreach (HangMauDich hmd in TKMD.HMDCollection)
                    {
                        if (hmd.SoThuTuHang == HTG.STTHang)
                        {
                            HTG.TenHang = hmd.TenHang;
                            break;
                        }
                    }
                    if (dr1["GIA_HOA_DON"] != DBNull.Value)
                        HTG.GiaTrenHoaDon = Convert.ToDouble(dr1["GIA_HOA_DON"]);
                    if (dr1["TT_GIAN_TIEP"] != DBNull.Value)
                        HTG.KhoanThanhToanGianTiep = Convert.ToDouble(dr1["TT_GIAN_TIEP"]);
                    if (dr1["Tra_Truoc"] != DBNull.Value)
                        HTG.TraTruoc = Convert.ToDouble(dr1["Tra_Truoc"]);
                    if (dr1["TC_1"] != DBNull.Value)
                        HTG.TongCong1 = Convert.ToDouble(dr1["TC_1"]);
                    if (dr1["Hoa_Hong"] != DBNull.Value)
                        HTG.HoaHong = Convert.ToDouble(dr1["Hoa_Hong"]);
                    if (dr1["CP_BAO_BI"] != DBNull.Value)
                        HTG.ChiPhiBaoBi = Convert.ToDouble(dr1["CP_BAO_BI"]);
                    if (dr1["CP_Dong_Goi"] != DBNull.Value)
                        HTG.ChiPhiDongGoi = Convert.ToDouble(dr1["CP_Dong_Goi"]);
                    if (dr1["TRO_GIUP"] != DBNull.Value)
                        HTG.TroGiup = Convert.ToDouble(dr1["TRO_GIUP"]);
                    if (dr1["VAT_LIEU_MR"] != DBNull.Value)
                        HTG.VatLieu = Convert.ToDouble(dr1["VAT_LIEU_MR"]);
                    if (dr1["CONG_CU_MR"] != DBNull.Value)
                        HTG.CongCu = Convert.ToDouble(dr1["CONG_CU_MR"]);
                    if (dr1["NGUYEN_LIEU_MR"] != DBNull.Value)
                        HTG.NguyenLieu = Convert.ToDouble(dr1["NGUYEN_LIEU_MR"]);
                    if (dr1["THIET_KE_MR"] != DBNull.Value)
                        HTG.ThietKe = Convert.ToDouble(dr1["THIET_KE_MR"]);
                    if (dr1["BAN_QUYEN"] != DBNull.Value)
                        HTG.BanQuyen = Convert.ToDouble(dr1["BAN_QUYEN"]);
                    if (dr1["TIEN_TRA_SD"] != DBNull.Value)
                        HTG.TienTraSuDung = Convert.ToDouble(dr1["TIEN_TRA_SD"]);
                    if (dr1["CP_XEP_HANG"] != DBNull.Value)
                        HTG.ChiPhiVanChuyen = Convert.ToDouble(dr1["CP_XEP_HANG"]);
                    if (dr1["CP_XEP_HANG"] != DBNull.Value)
                        HTG.CuocPhiXepHang = Convert.ToDouble(dr1["CP_XEP_HANG"]);
                    if (dr1["CP_XEP_HANG"] != DBNull.Value)
                        HTG.CuocPhiBaoHiem = Convert.ToDouble(dr1["CP_XEP_HANG"]);
                    if (dr1["TC_2"] != DBNull.Value)
                        HTG.TongCong2 = Convert.ToDouble(dr1["TC_2"]);
                    if (dr1["CP_BH"] != DBNull.Value)
                        HTG.PhiBaoHiem = Convert.ToDouble(dr1["CP_BH"]);
                    if (dr1["CP_PHAT_SINH"] != DBNull.Value)
                        HTG.ChiPhiPhatSinh = Convert.ToDouble(dr1["CP_PHAT_SINH"]);
                    if (dr1["TIEN_LAI"] != DBNull.Value)
                        HTG.TienLai = Convert.ToDouble(dr1["TIEN_LAI"]);
                    if (dr1["Tien_Thue"] != DBNull.Value)
                        HTG.TienThue = Convert.ToDouble(dr1["Tien_Thue"]);
                    if (dr1["GIAM_GIA"] != DBNull.Value)
                        HTG.GiamGia = Convert.ToDouble(dr1["GIAM_GIA"]);
                    if (dr1["CP_KHONG_TANG"] != DBNull.Value)
                        HTG.ChiPhiKhongTang = Convert.ToDouble(dr1["CP_KHONG_TANG"]);
                    if (dr1["TG_NGTE"] != DBNull.Value)
                        HTG.TriGiaNguyenTe = Convert.ToDouble(dr1["TG_NGTE"]);
                    if (dr1["TG_VND"] != DBNull.Value)
                        HTG.TriGiaVND = Convert.ToDouble(dr1["TG_VND"]);
                    if (dr1["CHIET_KHAU"] != DBNull.Value)
                        HTG.ChietKhau = Convert.ToDouble(dr1["CHIET_KHAU"]);
                    if (dr1["TC_3"] != DBNull.Value)
                        HTG.TongCong3 = Convert.ToDouble(dr1["TC_3"]);
                    if (dr1["PHI_BH_MR"] != DBNull.Value)
                        HTG.ChiPhiNoiDia = Convert.ToDouble(dr1["PHI_BH_MR"]);
                    TKTG.HTGCollection.Add(HTG);
                }

                TKMD.TKTGCollection.Add(TKTG);
            }

            TKMD.InsertUpdateFullAll();

        }
        public bool InsertUpdateFullAll()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    int i = 1;
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.SoThuTuHang = i++;
                        hmd.ID = HangMauDich.checkHMDbyMaHang(hmd.MaPhu);
                       if  (hmd.ID>0)
                       {
                           hmd.Update(transaction);
                       } 
                       else
                       {
                           hmd.ID = hmd.Insert(transaction);
                       }
                    }
                    i = 1;
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.STTHang = i++;
                        if (ct.ID == 0)
                        {
                            ct.Master_ID = this.ID;
                            ct.ID = ct.InsertTransaction(transaction);
                        }
                        else
                        {
                            ct.UpdateTransaction(transaction);
                        }
                    }
                    foreach (ToKhaiTriGia tktg in this.TKTGCollection)
                    {
                        tktg.TKMD_ID = this.ID;
                        tktg.InsertUpdateFullTransaction(transaction);

                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #region Đồng bộ dữ liệu V3
        /// <summary>
        /// H.N.Khánh 24-05-2012
        /// insert update to khai Dong Bo Du Lieu
        /// </summary>
        /// <returns> lỗi </returns>
        public string InsertFullFromISyncDaTa()
        {
            string error = string.Empty;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.ID = 0;
                    this.ID = this.InsertTransaction(transaction);
                    int i = 1;
                    #region hàng mậu dịch
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.SoThuTuHang = i++;
                        hmd.TKMD_ID = this.ID;
                        hmd.ID = hmd.Insert(transaction);
                        if (this.MaLoaiHinh.Substring(0,1).ToUpper()=="N")
                        {
                            HangHoaNhap hangnhap = new HangHoaNhap();
                            hangnhap.MaHaiQuan = this.MaHaiQuan;
                            hangnhap.MaDoanhNghiep = this.MaDoanhNghiep;
                            hangnhap.Ma = hmd.MaPhu;
                            hangnhap.MaHS = hmd.MaHS;
                            hangnhap.Ten = hmd.TenHang;
                            hangnhap.DVT_ID = hmd.DVT_ID;
                            if (hangnhap.Load())
                                hangnhap.UpdateTransaction(transaction);
                            else
                                hangnhap.InsertTransaction(transaction);
                        }
                        else
                        {
                            HangHoaXuat hanghoaxuat = new HangHoaXuat();
                            hanghoaxuat.MaHaiQuan = this.MaHaiQuan;
                            hanghoaxuat.MaDoanhNghiep = this.MaDoanhNghiep;
                            hanghoaxuat.Ma = hmd.MaPhu;
                            hanghoaxuat.MaHS = hmd.MaHS;
                            hanghoaxuat.Ten = hmd.TenHang;
                            hanghoaxuat.DVT_ID = hmd.DVT_ID;
                            if (hanghoaxuat.Load())
                                hanghoaxuat.UpdateTransaction(transaction);
                            else
                                hanghoaxuat.InsertTransaction(transaction);
                        }
                        
                    }
                    #endregion
                    #region chứng từ hải quan
                    i = 1;
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.STTHang = i++;
                        ct.ID = 0;
                        ct.Master_ID = this.ID;
                        ct.ID = ct.InsertTransaction(transaction);

                    }
                    foreach (ToKhaiTriGia tktg in this.TKTGCollection)
                    {
                        tktg.TKMD_ID = this.ID;
                        tktg.ID = 0;
                        tktg.ID = tktg.InsertTransaction(transaction);
                        foreach (HangTriGia htg in tktg.HTGCollection)
                        {
                            htg.ID = 0;
                            htg.TKTG_ID = tktg.ID;
                            htg.InsertTransaction(transaction);
                        }
                    }
                    foreach (ToKhaiTriGiaPP23 tktg23 in TKTGPP23Collection)
                    {
                        tktg23.TKMD_ID = this.ID;
                        tktg23.ID = 0;
                        tktg23.ID = tktg23.InsertTransaction(transaction);
                    }
                    i = 1;
                    foreach (CO co in this.ListCO)
                    {
                        co.TKMD_ID = this.ID;
                        co.ID = 0;
                        co.ID =  co.InsertTransaction(transaction,db);
                        foreach (HangCoDetail item in co.ListHMDofCo)
                        {
                            item.Co_ID = co.ID;
                            foreach (HangMauDich hmd in this.HMDCollection)
                                if (hmd.MaPhu == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.Insert(transaction);
                        }
                    }
                    foreach (ChungTuNo chungtuno in this.ChungTuNoCollection)
                    {
                        chungtuno.ID = 0;
                        chungtuno.TKMDID = this.ID;
                        chungtuno.InsertTransaction(transaction, db);
                    }
                    foreach (ChungTuKem ctk in this.ChungTuKemCollection)
                    {
                        ctk.ID = 0;
                        ctk.TKMDID = this.ID;
                        ctk.ID = ctk.InsertTransaction(transaction, db);
                        foreach (ChungTuKemChiTiet item in ctk.listCTChiTiet)
                        {
                            item.ID = 0;
                            item.ChungTuKemID = ctk.ID;
                            item.InsertTransaction(transaction, db);
                        }
                    }
                    foreach (GiayPhep gp in this.GiayPhepCollection)
                    {
                        gp.ID = 0;
                        gp.TKMD_ID = this.ID;
                        gp.ID = gp.InsertTransaction(transaction, db);
                        foreach (HangGiayPhepDetail item in gp.ListHMDofGiayPhep)
                        {
                            item.ID = 0;
                            item.GiayPhep_ID = gp.ID;
                            foreach (HangMauDich hmd in this.HMDCollection)
                                if (hmd.MaPhu == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.Insert(transaction);
                        }
                    }
                    foreach (HoaDonThuongMai hdtm in this.HoaDonThuongMaiCollection)
                    {
                        hdtm.ID = 0;
                        hdtm.TKMD_ID = this.ID;
                        hdtm.ID = hdtm.InsertTransaction(transaction, db);
                        foreach (HoaDonThuongMaiDetail item in hdtm.ListHangMDOfHoaDon)
                        {
                            item.ID = 0;
                            item.HoaDonTM_ID = hdtm.ID;
                            foreach (HangMauDich hmd in this.HMDCollection)
                                if (hmd.MaPhu == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.Insert(transaction);
                        }
                    }
                    foreach (HopDongThuongMai hopdong in this.HopDongThuongMaiCollection)
                    {
                        hopdong.ID = 0;
                        hopdong.TKMD_ID = this.ID;
                        hopdong.InsertTransaction(transaction, db);
                        foreach (HopDongThuongMaiDetail item in hopdong.ListHangMDOfHopDong)
                        {
                            item.ID = 0;
                            item.HopDongTM_ID = hopdong.ID;
                            foreach ( HangMauDich hmd in this.HMDCollection)
                                if (hmd.MaPhu==item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.Insert(transaction);
                        }
                    }
                    foreach (DeNghiChuyenCuaKhau denghi in this.listChuyenCuaKhau)
                    {
                        denghi.ID = 0;
                        denghi.TKMD_ID = this.ID;
                        denghi.InsertTransaction(transaction, db);
                    }
                    foreach (NoiDungDieuChinhTK noidungsua in this.NoiDungDieuChinhTKCollection)
                    {
                        noidungsua.ID = 0;
                        noidungsua.TKMD_ID = this.ID;
                        noidungsua.Insert(transaction);
                    }
                    if (VanTaiDon != null)
                    {
                        VanTaiDon.ID = 0;
                        VanTaiDon.TKMD_ID = this.ID;
                        VanTaiDon.Insert(transaction);
                    }

                    
                    #endregion

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    error = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }
            return error;
        }
        /// <summary>
        /// Tìm kiếm số tờ khai đã duyệt để đồng bộ dữ liệu
        /// </summary>
        /// <param name="whereCondition"></param>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        public IDataReader SelectDynamicDongBoDuLieu(string whereCondition, string orderByExpression)
        {
            string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectDynamicDongBoDuLieu]";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }
        public int TrangThaiDongBo;
        public ToKhaiMauDichCollection SelectCollectionDynamicDongBoDuLieu(string whereCondition, string orderByExpression)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            IDataReader reader = this.SelectDynamicDongBoDuLieu(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_STATUS"))) entity.TrangThaiDongBo = reader.GetInt32(reader.GetOrdinal("MSG_STATUS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        #endregion Đồng bộ dữ liệu V3

        #endregion Dong Bo Du Lieu

        #region HỦY TỜ KHAI

        public bool WSHuyToKhai(string password)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                if (this.MaLoaiHinh.StartsWith("N"))
                    doc.LoadXml(GenerateToXmlEnvelope(MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao, new Guid(this.GUIDSTR)));
                else
                    doc.LoadXml(GenerateToXmlEnvelope(MessageTypes.ToKhaiXuat, MessageFunctions.KhaiBao, new Guid(this.GUIDSTR)));

                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProgram();
                docNPL.Load(path + @"\XMLKinhDoanh\HuyTKDaDuyet.XML");

                XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
                root.Attributes["SOTN"].Value = this.SoTiepNhan.ToString();
                root.Attributes["NAMTN"].Value = this.NamDK.ToString();

                root.SelectSingleNode("ToKhai").Attributes["MaLH"].Value = this.MaLoaiHinh;
                root.SelectSingleNode("ToKhai").Attributes["MaHQ"].Value = Globals.trimMaHaiQuan(this.MaHaiQuan);
                root.SelectSingleNode("ToKhai").Attributes["SoTK"].Value = this.SoToKhai.ToString();
                root.SelectSingleNode("ToKhai").Attributes["NgayDK"].Value = this.NgayDangKy.ToString("yyyy-MM-dd");
                root.SelectSingleNode("ToKhai").Attributes["NamDK"].Value = this.NamDK.ToString();

                root.SelectSingleNode("DoanhNghiep").Attributes["MaDN"].Value = this.MaDoanhNghiep;
                root.SelectSingleNode("DoanhNghiep").Attributes["TenDN"].Value = this.TenDoanhNghiep;

                List<HuyToKhai> huyTk = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_TKMD_ID(this.ID);

                root.SelectSingleNode("LyDo").InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(huyTk[0].LyDoHuy);

                XmlNode Content = doc.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

                string kq = string.Empty;
                try
                {
                    Message message = new Message();
                    message.ItemID = this.ID;
                    message.ReferenceID = new Guid(this.GUIDSTR);
                    message.MessageType = this.MaLoaiHinh.StartsWith("N") ? MessageTypes.ToKhaiNhap : MessageTypes.ToKhaiXuat;
                    message.MessageFunction = MessageFunctions.KhaiBao;
                    message.MessageFrom = this.MaDoanhNghiep;
                    message.MessageTo = Globals.trimMaHaiQuan(this.MaHaiQuan).Trim();
                    message.MessageContent = doc.InnerXml;
                    message.TieuDeThongBao = MessageTitle.HuyToKhai;
                    message.NoiDungThongBao = "Khai báo hủy";// string.Empty;
                    message.CreatedTime = DateTime.Now;
                    message.Insert();

                    kq = kdt.Send(doc.InnerXml, password);
                }
                catch
                {
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }

                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);

                if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        Message message = new Message();
                        message.ItemID = this.ID;
                        message.ReferenceID = new Guid(this.GUIDSTR);
                        message.MessageType = MessageTypes.ThongTin;
                        message.MessageFunction = MessageFunctions.LayPhanHoi;
                        message.MessageFrom = Globals.trimMaHaiQuan(this.MaHaiQuan).Trim();
                        message.MessageTo = this.MaDoanhNghiep;
                        message.MessageContent = kq;
                        message.TieuDeThongBao = MessageTitle.HuyToKhai;
                        message.NoiDungThongBao = "Đã nhận được thông tin";//string.Empty;
                        message.CreatedTime = DateTime.Now;
                        message.Insert();

                        this.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                        this.Update();
                        huyTk[0].TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                        //huyTk[0].Guid = "a1927534-ea80-4be3-8b30-a7e86b59e926";
                        huyTk[0].Update();
                        return true;
                    }
                }
                else
                {
                    throw new Exception(Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return false;
        }

        #endregion

        // check ty gia :
        private decimal checkTGVND(string str)
        {
            string[] strtt = str.Split(',');
            decimal d1 = Convert.ToDecimal(strtt[0]);
            decimal d2 = Convert.ToDecimal(strtt[1]);
            string strtemp = strtt[1].Substring(0, 2);
            string strtemptong = strtt[0] + "." + strtemp;
            decimal dtemp = Convert.ToDecimal(strtemptong);
            if (d2 > 0)
            {
                dtemp = Convert.ToDecimal(str);
                return dtemp;
            }
            return d1;
        }
        private string GetFrontTGVND(string str)
        {
            string[] strtt = str.Split('.');
            string d1 = strtt[0].ToString();
            return d1;
        }
        private string GetEndTGVND(string str)
        {
            string[] strtt = str.Split('.');
            string d1 = strtt[1].Substring(0, 2);
            decimal dec = Convert.ToDecimal(d1);
            if (dec <= 0)
                return "";
            return d1;

        }

        public ToKhaiMauDichCollection searchTKMDByUserName(string userName, string where)
        {
            string query = "SELECT * FROM dbo.t_KDT_ToKhaiMauDich tkmd INNER JOIN dbo.t_KDT_SXXK_LogKhaiBao lg ON lg.ID_DK = tkmd.ID " +
                           "WHERE lg.UserNameKhaiBao = '" + userName + "' AND LoaiKhaiBao = 'TK' AND ";
            if (!where.Equals(""))
                query += where;
            System.Data.Common.DbCommand dbCommand = (System.Data.Common.DbCommand)this.db.GetSqlStringCommand(query);
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                //if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) entity.HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

    }
}
