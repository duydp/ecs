﻿using System;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.Security.Cryptography;
using Company.QuanTri;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT;
using Company.BLL.Utils;
using Company.KDT.SHARE.Components;
using Janus.Windows.GridEX;
using DaiLy = Company.KDT.SHARE.Components.DaiLy;

namespace Company.Interface.DongBoDuLieu_New.QuanTri
{
    public partial class NguoiDungEditForm : BaseForm
    {
        //public User user = new User();
        public DaiLy user = new DaiLy();
        private ISyncData_SXXK myService = IsyncDaTa_V3.SyncService();
        public string passwordLogin = "", userLogin = "";
        public bool isCheck;
        public bool isEdit;
        public NguoiDungEditForm()
        {
            InitializeComponent();
            //myService.Url = WebService.LoadConfigure("WS_URL");
        }

        private bool Login()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                WSForm_DBDL wsForm = new WSForm_DBDL();
                if (!Config.IsRemember)
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return false;
                }
                passwordLogin = Config.IsRemember ? Config.Pass : wsForm.txtMatKhau.Text.Trim();
                userLogin = Config.IsRemember ? Config.User : wsForm.txtMaDoanhNghiep.Text.Trim();

                string temp = myService.LoginDaiLy(userLogin, passwordLogin);
                // Đăng nhập sai user
                if (string.IsNullOrEmpty(temp))
                {
                    MLMessages("Người dùng không hợp lệ!", "", "", false);
                    Config.Pass = "";
                    Config.IsRemember = false;
                    return false;
                }
                Company.KDT.SHARE.Components.DaiLy userDaiLy = Helpers.Deserialize<DaiLy>((myService.LoginDaiLy(userLogin, passwordLogin)));
                // 1. Đăng nhập user của doanh nghiệp khác              ||
                // 2. Sử dụng đại lý nhưng đăng nhập User của Admin     ||    
                // 3. Sử dụng server nhưng đăng nhập user của Đại lý    ||
                if ((userDaiLy == null || userDaiLy.MaDoanhNghiep != GlobalSettings.MA_DON_VI) || !userDaiLy.isAdmin)
                {
                    MLMessages("Người dùng không hợp lệ!", "", "", false);
                    Config.Pass = "";
                    Config.IsRemember = false;
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid)
                    return;
                
                    if(!Login())
                        return;
                
                if (user.ID == 0)
                {
                    if (txtMatKhau.Text.Length == 0)
                    {
                        epError.SetError(txtMatKhau, "Mật khẩu bắt buộc phải nhập");
                        epError.SetIconPadding(txtMatKhau, -8);
                        return;
                    }
                    if (editBox1.Text.Length == 0)
                    {
                        epError.SetError(editBox1, "Mật khẩu nhập lại bắt buộc phải nhập");
                        epError.SetIconPadding(editBox1, -8);
                        return;
                    }
                    if (editBox1.Text.Trim().ToUpper() != txtMatKhau.Text.Trim().ToUpper())
                    {
                        epError.SetError(editBox1, "Mật khẩu không giống nhau");
                        epError.SetIconPadding(editBox1, -8);
                        return;
                    }

                }
                else
                {
                    if (txtMatKhau.Text.Trim().Length > 0)
                    {
                        if (editBox1.Text.Trim().ToUpper() != txtMatKhau.Text.Trim().ToUpper())
                        {
                            epError.SetError(editBox1, "Mật khẩu không giống nhau");
                            epError.SetIconPadding(editBox1, -8);
                            return;
                        }
                    }
                }
                if (myService.CheckUserName(txtUser.Text.Trim()))
                {
                    //ShowMessage("Tên người dùng này đã có.Bạn hãy nhập tên khác", false);
                    MLMessages("Tên người dùng này đã tồn tại trong hệ thống", "MSG_USER01", "", false);
                    return;
                }
                //user.USER_NAME = txtUser.Text.Trim();
                user.HO_TEN = txtHoTen.Text.Trim();
                user.MO_TA = txtMoTa.Text.Trim();
                if (user.ID == 0)
                    //user.PASSWORD = EncryptPassword(txtMatKhau.Text.Trim());
                    user.PASSWORD = GetMD5Value(txtMatKhau.Text.Trim());
                else
                {
                    if (txtMatKhau.Text.Trim().Length > 0)
                        user.PASSWORD = GetMD5Value(txtMatKhau.Text.Trim());
                }

                int id = myService.InsertUpdateUserDaiLy(userLogin, passwordLogin, user.USER_NAME,user.PASSWORD, user.HO_TEN, user.MO_TA, GlobalSettings.MA_DON_VI);
                if (id == 1)
                {
                    if (user.USER_NAME == Config.User && Config.IsRemember)
                    {
                        Config.Pass = user.PASSWORD;
                    }
                    MLMessages("Lưu thành công!", "", "", false);
                }
                else
                    MLMessages("Không lưu được", "", "", false);
                this.Close();
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
        }
        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();


        }
        public string EncryptPassword(string password)
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] hashBytes = encoding.GetBytes(password);

            // compute SHA-1 hash.
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] bytePassword = sha1.ComputeHash(hashBytes);
            string cryptPassword = Convert.ToBase64String(bytePassword);
            return cryptPassword;
        }

        private void NguoiDungEditForm_Load(object sender, EventArgs e)
        {
            lblMaDN.Text = user.MaDoanhNghiep;
            txtHoTen.Text = user.HO_TEN;
            txtMoTa.Text = user.MO_TA;
            txtUser.Text = user.USER_NAME;
            if (isEdit)
            {
                txtUser.Enabled = false;
            }
//             if(userLogin.Equals("administrator"))
//                 cbIsAdmin.Visible = true;// Hiển thị checkBox isAdmin
            if (user.isAdmin)
            {
                txtUser.ReadOnly = false;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["CheckData"].Value.ToString() == "True")
            {
                e.Row.CheckState = RowCheckState.Checked;
            }
            else
                e.Row.CheckState = RowCheckState.Unchecked;
        }
      
    }
}