﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class T_KDT_THUPHI_BIENLAI_PHI : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long BL_ID { set; get; }
		public string MaKhoanMuc { set; get; }
		public string TenKhoanMuc { set; get; }
		public string MaDVT { set; get; }
		public string TenDVT { set; get; }
		public decimal SoLuongOrTrongLuong { set; get; }
		public decimal DonGia { set; get; }
		public decimal ThanhTien { set; get; }
		public string DienGiai { set; get; }
		public string SoVanDon { set; get; }
		public string SoContainer { set; get; }
		public string SoSeal { set; get; }
		public int Loai { set; get; }
		public string TinhChat { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KDT_THUPHI_BIENLAI_PHI> ConvertToCollection(IDataReader reader)
		{
			List<T_KDT_THUPHI_BIENLAI_PHI> collection = new List<T_KDT_THUPHI_BIENLAI_PHI>();
			while (reader.Read())
			{
				T_KDT_THUPHI_BIENLAI_PHI entity = new T_KDT_THUPHI_BIENLAI_PHI();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BL_ID"))) entity.BL_ID = reader.GetInt64(reader.GetOrdinal("BL_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKhoanMuc"))) entity.MaKhoanMuc = reader.GetString(reader.GetOrdinal("MaKhoanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKhoanMuc"))) entity.TenKhoanMuc = reader.GetString(reader.GetOrdinal("TenKhoanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVT"))) entity.MaDVT = reader.GetString(reader.GetOrdinal("MaDVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDVT"))) entity.TenDVT = reader.GetString(reader.GetOrdinal("TenDVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongOrTrongLuong"))) entity.SoLuongOrTrongLuong = reader.GetDecimal(reader.GetOrdinal("SoLuongOrTrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhTien"))) entity.ThanhTien = reader.GetDecimal(reader.GetOrdinal("ThanhTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("DienGiai"))) entity.DienGiai = reader.GetString(reader.GetOrdinal("DienGiai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer"))) entity.SoContainer = reader.GetString(reader.GetOrdinal("SoContainer"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSeal"))) entity.SoSeal = reader.GetString(reader.GetOrdinal("SoSeal"));
				if (!reader.IsDBNull(reader.GetOrdinal("Loai"))) entity.Loai = reader.GetInt32(reader.GetOrdinal("Loai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhChat"))) entity.TinhChat = reader.GetString(reader.GetOrdinal("TinhChat"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KDT_THUPHI_BIENLAI_PHI> collection, long id)
        {
            foreach (T_KDT_THUPHI_BIENLAI_PHI item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KDT_THUPHI_BIENLAI_PHI VALUES(@BL_ID, @MaKhoanMuc, @TenKhoanMuc, @MaDVT, @TenDVT, @SoLuongOrTrongLuong, @DonGia, @ThanhTien, @DienGiai, @SoVanDon, @SoContainer, @SoSeal, @Loai, @TinhChat)";
            string update = "UPDATE T_KDT_THUPHI_BIENLAI_PHI SET BL_ID = @BL_ID, MaKhoanMuc = @MaKhoanMuc, TenKhoanMuc = @TenKhoanMuc, MaDVT = @MaDVT, TenDVT = @TenDVT, SoLuongOrTrongLuong = @SoLuongOrTrongLuong, DonGia = @DonGia, ThanhTien = @ThanhTien, DienGiai = @DienGiai, SoVanDon = @SoVanDon, SoContainer = @SoContainer, SoSeal = @SoSeal, Loai = @Loai, TinhChat = @TinhChat WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_THUPHI_BIENLAI_PHI WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BL_ID", SqlDbType.BigInt, "BL_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKhoanMuc", SqlDbType.NVarChar, "MaKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKhoanMuc", SqlDbType.NVarChar, "TenKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT", SqlDbType.NVarChar, "MaDVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDVT", SqlDbType.NVarChar, "TenDVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, "SoLuongOrTrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGia", SqlDbType.Decimal, "DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThanhTien", SqlDbType.Decimal, "ThanhTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DienGiai", SqlDbType.NVarChar, "DienGiai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoContainer", SqlDbType.NVarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal", SqlDbType.NVarChar, "SoSeal", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Loai", SqlDbType.Int, "Loai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhChat", SqlDbType.NVarChar, "TinhChat", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BL_ID", SqlDbType.BigInt, "BL_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKhoanMuc", SqlDbType.NVarChar, "MaKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKhoanMuc", SqlDbType.NVarChar, "TenKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT", SqlDbType.NVarChar, "MaDVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDVT", SqlDbType.NVarChar, "TenDVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, "SoLuongOrTrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGia", SqlDbType.Decimal, "DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThanhTien", SqlDbType.Decimal, "ThanhTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DienGiai", SqlDbType.NVarChar, "DienGiai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoContainer", SqlDbType.NVarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal", SqlDbType.NVarChar, "SoSeal", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Loai", SqlDbType.Int, "Loai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhChat", SqlDbType.NVarChar, "TinhChat", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KDT_THUPHI_BIENLAI_PHI VALUES(@BL_ID, @MaKhoanMuc, @TenKhoanMuc, @MaDVT, @TenDVT, @SoLuongOrTrongLuong, @DonGia, @ThanhTien, @DienGiai, @SoVanDon, @SoContainer, @SoSeal, @Loai, @TinhChat)";
            string update = "UPDATE T_KDT_THUPHI_BIENLAI_PHI SET BL_ID = @BL_ID, MaKhoanMuc = @MaKhoanMuc, TenKhoanMuc = @TenKhoanMuc, MaDVT = @MaDVT, TenDVT = @TenDVT, SoLuongOrTrongLuong = @SoLuongOrTrongLuong, DonGia = @DonGia, ThanhTien = @ThanhTien, DienGiai = @DienGiai, SoVanDon = @SoVanDon, SoContainer = @SoContainer, SoSeal = @SoSeal, Loai = @Loai, TinhChat = @TinhChat WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_THUPHI_BIENLAI_PHI WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BL_ID", SqlDbType.BigInt, "BL_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKhoanMuc", SqlDbType.NVarChar, "MaKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKhoanMuc", SqlDbType.NVarChar, "TenKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT", SqlDbType.NVarChar, "MaDVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDVT", SqlDbType.NVarChar, "TenDVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, "SoLuongOrTrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGia", SqlDbType.Decimal, "DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThanhTien", SqlDbType.Decimal, "ThanhTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DienGiai", SqlDbType.NVarChar, "DienGiai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoContainer", SqlDbType.NVarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal", SqlDbType.NVarChar, "SoSeal", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Loai", SqlDbType.Int, "Loai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhChat", SqlDbType.NVarChar, "TinhChat", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BL_ID", SqlDbType.BigInt, "BL_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKhoanMuc", SqlDbType.NVarChar, "MaKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKhoanMuc", SqlDbType.NVarChar, "TenKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT", SqlDbType.NVarChar, "MaDVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDVT", SqlDbType.NVarChar, "TenDVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, "SoLuongOrTrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGia", SqlDbType.Decimal, "DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThanhTien", SqlDbType.Decimal, "ThanhTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DienGiai", SqlDbType.NVarChar, "DienGiai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoContainer", SqlDbType.NVarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal", SqlDbType.NVarChar, "SoSeal", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Loai", SqlDbType.Int, "Loai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhChat", SqlDbType.NVarChar, "TinhChat", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KDT_THUPHI_BIENLAI_PHI Load(long id)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KDT_THUPHI_BIENLAI_PHI> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KDT_THUPHI_BIENLAI_PHI> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KDT_THUPHI_BIENLAI_PHI> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<T_KDT_THUPHI_BIENLAI_PHI> SelectCollectionBy_BL_ID(long bL_ID)
		{
            IDataReader reader = SelectReaderBy_BL_ID(bL_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_BL_ID(long bL_ID)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectBy_BL_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BL_ID", SqlDbType.BigInt, bL_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_BL_ID(long bL_ID)
		{
			const string spName = "p_T_KDT_THUPHI_BIENLAI_PHI_SelectBy_BL_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BL_ID", SqlDbType.BigInt, bL_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KDT_THUPHI_BIENLAI_PHI(long bL_ID, string maKhoanMuc, string tenKhoanMuc, string maDVT, string tenDVT, decimal soLuongOrTrongLuong, decimal donGia, decimal thanhTien, string dienGiai, string soVanDon, string soContainer, string soSeal, int loai, string tinhChat)
		{
			T_KDT_THUPHI_BIENLAI_PHI entity = new T_KDT_THUPHI_BIENLAI_PHI();	
			entity.BL_ID = bL_ID;
			entity.MaKhoanMuc = maKhoanMuc;
			entity.TenKhoanMuc = tenKhoanMuc;
			entity.MaDVT = maDVT;
			entity.TenDVT = tenDVT;
			entity.SoLuongOrTrongLuong = soLuongOrTrongLuong;
			entity.DonGia = donGia;
			entity.ThanhTien = thanhTien;
			entity.DienGiai = dienGiai;
			entity.SoVanDon = soVanDon;
			entity.SoContainer = soContainer;
			entity.SoSeal = soSeal;
			entity.Loai = loai;
			entity.TinhChat = tinhChat;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@BL_ID", SqlDbType.BigInt, BL_ID);
			db.AddInParameter(dbCommand, "@MaKhoanMuc", SqlDbType.NVarChar, MaKhoanMuc);
			db.AddInParameter(dbCommand, "@TenKhoanMuc", SqlDbType.NVarChar, TenKhoanMuc);
			db.AddInParameter(dbCommand, "@MaDVT", SqlDbType.NVarChar, MaDVT);
			db.AddInParameter(dbCommand, "@TenDVT", SqlDbType.NVarChar, TenDVT);
			db.AddInParameter(dbCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, SoLuongOrTrongLuong);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
			db.AddInParameter(dbCommand, "@ThanhTien", SqlDbType.Decimal, ThanhTien);
			db.AddInParameter(dbCommand, "@DienGiai", SqlDbType.NVarChar, DienGiai);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.NVarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.NVarChar, SoSeal);
			db.AddInParameter(dbCommand, "@Loai", SqlDbType.Int, Loai);
			db.AddInParameter(dbCommand, "@TinhChat", SqlDbType.NVarChar, TinhChat);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KDT_THUPHI_BIENLAI_PHI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_BIENLAI_PHI item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KDT_THUPHI_BIENLAI_PHI(long id, long bL_ID, string maKhoanMuc, string tenKhoanMuc, string maDVT, string tenDVT, decimal soLuongOrTrongLuong, decimal donGia, decimal thanhTien, string dienGiai, string soVanDon, string soContainer, string soSeal, int loai, string tinhChat)
		{
			T_KDT_THUPHI_BIENLAI_PHI entity = new T_KDT_THUPHI_BIENLAI_PHI();			
			entity.ID = id;
			entity.BL_ID = bL_ID;
			entity.MaKhoanMuc = maKhoanMuc;
			entity.TenKhoanMuc = tenKhoanMuc;
			entity.MaDVT = maDVT;
			entity.TenDVT = tenDVT;
			entity.SoLuongOrTrongLuong = soLuongOrTrongLuong;
			entity.DonGia = donGia;
			entity.ThanhTien = thanhTien;
			entity.DienGiai = dienGiai;
			entity.SoVanDon = soVanDon;
			entity.SoContainer = soContainer;
			entity.SoSeal = soSeal;
			entity.Loai = loai;
			entity.TinhChat = tinhChat;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KDT_THUPHI_BIENLAI_PHI_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@BL_ID", SqlDbType.BigInt, BL_ID);
			db.AddInParameter(dbCommand, "@MaKhoanMuc", SqlDbType.NVarChar, MaKhoanMuc);
			db.AddInParameter(dbCommand, "@TenKhoanMuc", SqlDbType.NVarChar, TenKhoanMuc);
			db.AddInParameter(dbCommand, "@MaDVT", SqlDbType.NVarChar, MaDVT);
			db.AddInParameter(dbCommand, "@TenDVT", SqlDbType.NVarChar, TenDVT);
			db.AddInParameter(dbCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, SoLuongOrTrongLuong);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
			db.AddInParameter(dbCommand, "@ThanhTien", SqlDbType.Decimal, ThanhTien);
			db.AddInParameter(dbCommand, "@DienGiai", SqlDbType.NVarChar, DienGiai);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.NVarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.NVarChar, SoSeal);
			db.AddInParameter(dbCommand, "@Loai", SqlDbType.Int, Loai);
			db.AddInParameter(dbCommand, "@TinhChat", SqlDbType.NVarChar, TinhChat);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KDT_THUPHI_BIENLAI_PHI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_BIENLAI_PHI item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KDT_THUPHI_BIENLAI_PHI(long id, long bL_ID, string maKhoanMuc, string tenKhoanMuc, string maDVT, string tenDVT, decimal soLuongOrTrongLuong, decimal donGia, decimal thanhTien, string dienGiai, string soVanDon, string soContainer, string soSeal, int loai, string tinhChat)
		{
			T_KDT_THUPHI_BIENLAI_PHI entity = new T_KDT_THUPHI_BIENLAI_PHI();			
			entity.ID = id;
			entity.BL_ID = bL_ID;
			entity.MaKhoanMuc = maKhoanMuc;
			entity.TenKhoanMuc = tenKhoanMuc;
			entity.MaDVT = maDVT;
			entity.TenDVT = tenDVT;
			entity.SoLuongOrTrongLuong = soLuongOrTrongLuong;
			entity.DonGia = donGia;
			entity.ThanhTien = thanhTien;
			entity.DienGiai = dienGiai;
			entity.SoVanDon = soVanDon;
			entity.SoContainer = soContainer;
			entity.SoSeal = soSeal;
			entity.Loai = loai;
			entity.TinhChat = tinhChat;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@BL_ID", SqlDbType.BigInt, BL_ID);
			db.AddInParameter(dbCommand, "@MaKhoanMuc", SqlDbType.NVarChar, MaKhoanMuc);
			db.AddInParameter(dbCommand, "@TenKhoanMuc", SqlDbType.NVarChar, TenKhoanMuc);
			db.AddInParameter(dbCommand, "@MaDVT", SqlDbType.NVarChar, MaDVT);
			db.AddInParameter(dbCommand, "@TenDVT", SqlDbType.NVarChar, TenDVT);
			db.AddInParameter(dbCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, SoLuongOrTrongLuong);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
			db.AddInParameter(dbCommand, "@ThanhTien", SqlDbType.Decimal, ThanhTien);
			db.AddInParameter(dbCommand, "@DienGiai", SqlDbType.NVarChar, DienGiai);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.NVarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.NVarChar, SoSeal);
			db.AddInParameter(dbCommand, "@Loai", SqlDbType.Int, Loai);
			db.AddInParameter(dbCommand, "@TinhChat", SqlDbType.NVarChar, TinhChat);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KDT_THUPHI_BIENLAI_PHI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_BIENLAI_PHI item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KDT_THUPHI_BIENLAI_PHI(long id)
		{
			T_KDT_THUPHI_BIENLAI_PHI entity = new T_KDT_THUPHI_BIENLAI_PHI();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_BL_ID(long bL_ID)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_DeleteBy_BL_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BL_ID", SqlDbType.BigInt, bL_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KDT_THUPHI_BIENLAI_PHI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_BIENLAI_PHI item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}