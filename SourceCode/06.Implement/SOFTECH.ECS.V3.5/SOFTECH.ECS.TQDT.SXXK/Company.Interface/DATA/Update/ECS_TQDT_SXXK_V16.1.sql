delete t_VNACC_Category_Cargo where BondedAreaCode in ('34NHC10','34NHC18','34NHC31','34NHC11','34NHC12','34NHC13','34NHC14','34NHC15','34NHC16','34NHC17','34NHC19','34NHC20','34NHC21','34NHC22','34NHC23','34NHC24','34NHC25','34NHC26','34NHC27','34NHC28','34NHC29','34NHC30','34NHC32','34NHC33','34NHC34','34NHC35','34NHC36','34ABA01','34ABC01','34CCC14','34CCC15','34CCC16','34CCC17','34CCC18','34NGC04','34NGC05','34NGC06','34NGC07','34NGC08','34NGC09','34NGC10','34NGC11','34NGC12','34NGC13','34NGC14','34NGC15','34NGC16','34NGC17','34NGC18','34NGC19','34NGC20','34NGC21','34NGC22','34NGC23','34NGC24','34NGC25','34NGC26','34NGC27','34NGC28','34NGC29','34NGC30','34NGC31','34NGC32','34NGC33','34NGX16','34NGX17','34NHC37','03CDC93','03NKX74','03CDC94','03PLO04','03CDO02','03CDO03','03NKX75','03CCO03','03CDO04','03PLC60','03CDO05','18B1C01','18B2C02','18B1C03','18B2C04','18B2L01','18B1CB1','18B2CB2','18B1OZZ','18B2OZZ','03PJX26','03PLX24','03PLX26','03CDC96','03CDC95','03PJC68','03PJC69','03PJC67','03PJC70','03PJC64','03PJC66','03PJC65','03PLC62','03PLC75','03PLC65','03PLC69','03PLC66','03PLC68','03PLC64','03PLC70','03PLC73','03PLC72','03PLC67','03PLC76','03PLC63','03PLC74','03PLC77','03PLC61','03PLC71','51C2S03')
---------Da nang
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34ABA01','XN TM MAT DAT',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34ABC01','TNT-VIETRANS',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34CCC14','CTY MAY 29/3 - DD3',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34CCC15','CTY MAY 29/3 - DD2',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34CCC16','CTY VINATEX - 2',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34CCC17','CTY NHAT HOANG ANH',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34CCC18','TCTY SONG THU - 2',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC04','CN CTY ACECOOK DN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC05','CN PHONG PHU LO N',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC06','CN PHONG PHU LO M',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC07','CTY GOM SU COSAMI',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC08','CTY NHUA CHIN HUEI',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC09','CTY KEYHINGE TOYS VN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC10','CTY LD LS VIET LANG',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC11','CTY DUONG VIET',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC12','CTY ASSOCIATED VN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC13','CTY BB NGK CROWN DN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC14','CTY TNHH BAO BI VN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC15','CTY COATURE',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC16','CTY DAERYANG VN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC17','CTY DIEN TU VIET HOA',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC18','CTY FNT VN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC19','HANOI STEEL CENTER',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC20','CTY KAD INDUSTRIAL',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC21','CTY GO TOAN CAU',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC22','CTY LAFIEN VINA',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC23','CTY MATRIX VN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC24','CTY MAY MAC BA SAO',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC25','CTY TCIE VIET NAM',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC26','CTY LAP MAY MIEN NAM',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC27','CTY TACHI-S VN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC28','CTY TOKYO KEIKI PREC',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC29','CTY TRAM MAI',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC30','CTY VIET HONG',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC31','CTY VN KNITWEAR',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC32','CTY VLXD DONG NGUYEN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGC33','XN VINAFOR DA NANG',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGX16','CTY HOSO VN - KHO',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NGX17','CTY YURI ABC - DD',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','34NHC37','CTY VINACAPITAL',N'')
--------------------Hải phong
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03CDC93','CTY CP BAO VE TV1 TW',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03NKX74','CTY FUJI SEIKO HP',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03CDC94','CTY HSIN YUE HSING',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLO04','CTY LAC HONG',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03CDO02','CTY LAM SON TB O',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03CDO03','CTY PHU DAT O',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03NKX75','CTY RORZE ROBOTECH',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03CCO03','CTY TM PHUC LOI',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03CDO04','CTY TRAI DAT XANH O',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC60','CY NT HOA PHAT 2',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03CDO05','TCTY VINACOMIN O',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PJX26','CTY FUJI SEIKO HD X ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLX24','CTY SOC VIET NAM X  ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLX26','CTY VLDT SHINETSU X ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03CDC96','CTY HGS HA NOI      ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03CDC95','CTY DTPT DUC QUAN   ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PJC68','CTY RICH WAY        ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PJC69','CTY ROSVIET         ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PJC67','CTY TNHH PHI        ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PJC70','CTY SX VAN SAN VN   ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PJC64','CTY CX NGAN VUONG   ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PJC66','CTY FUJI SEIKO HD C ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PJC65','CTY CN FORVIET      ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC62','CTY DTPT THAI DUONG ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC75','CTY VLDT SHINETSU C ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC65','CTY D&CD NGOC KHANH ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC69','CTY PUYOUNG VN      ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC66','CTY DET NHUOM H.YEN ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC68','CTY PIC VIET NAM    ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC64','CTY AN CHI          ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC70','CTY SXTM HOA BINH   ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC73','CTY SOC VIET NAM C  ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC72','CTY SHINDENGEN      ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC67','CTY DONG YANG E&P VN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC76','CTY VIET ANH        ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC63','CTY INOX HOA BINH   ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC74','CTY TOKO VIET NAM   ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC77','CTY VIKOM           ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC61','CTY BAO HUNG        ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','03PLC71','CTY SX&TM LAMY      ',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','51C2S03','CANG TH THI VAI     ',N'')
----------- Thai Nguyen
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','18B1C01','CTY MANI HA NOI',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','18B2C02','NHIET DIEN AN KHANH',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','18B1C03','BQLDA TN THAI NGUYEN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','18B2C04','CTY CB KS NUI PHAO',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','18B2L01','CTY ALS THAI NGUYEN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','18B1CB1','DOI NVCC THAI NGUYEN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','18B2CB2','DOI TT YEN BINH TN',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','18B1OZZ','DIEM LUU HH XK 18B1',N'')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A202A','18B2OZZ','DIEM LUU HH XK 18B2',N'')



UPDATE t_VNACC_Category_Cargo set BondedAreaName='MIEN THUE XUAT CANH' WHERE BondedAreaCode='34ABD01'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='KHO HANG MIEN THUE' WHERE BondedAreaCode='34ABD02'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='KHO HANG MIEN THUE 4' WHERE BondedAreaCode='34ABD04'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='TCTY SONG THU - 1' WHERE BondedAreaCode='34CCC01'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY MAY 29/3 - DD1' WHERE BondedAreaCode='34CCC02'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY VINATEX - 1' WHERE BondedAreaCode='34CCC07'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY HOSO VN - DD' WHERE BondedAreaCode='34NGX04'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY YURI ABC - KHO' WHERE BondedAreaCode='34NGX09'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CB XK TS THO QUANG' WHERE BondedAreaCode='34NHC05'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CAP DIEN SUMIDEN C' WHERE BondedAreaCode='03PJC24'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CAP DIEN SUMIDEN X' WHERE BondedAreaCode='03PJX25'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CONT. MINH THANH' WHERE BondedAreaCode='03EEC03'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CONT. MINH THANH CFS' WHERE BondedAreaCode='03EEC02'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY AS.TY VIET NAM 1' WHERE BondedAreaCode='03NKC41'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY AS.TY VIET NAM 2' WHERE BondedAreaCode='03NKX62'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY CN SEIKO VN C' WHERE BondedAreaCode='03PLC19'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY CN SEIKO VN X' WHERE BondedAreaCode='03PLX33'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY CN TACTICIAN C' WHERE BondedAreaCode='03CDC60'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY CN TACTICIAN X' WHERE BondedAreaCode='03CDX03'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY DENYO VN C' WHERE BondedAreaCode='03PLC26'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY DENYO VN X' WHERE BondedAreaCode='03PLX05'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY FEDERAL MOGUL 1' WHERE BondedAreaCode='03NKC80'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY FEDERAL MOGUL 2' WHERE BondedAreaCode='03NKC86'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY FUJI SEIKO HD' WHERE BondedAreaCode='03PJX26'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY GIAY KONYA VN C' WHERE BondedAreaCode='03NKC14'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY GIAY KONYA VN X' WHERE BondedAreaCode='03NKX07'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY KIM THUY PHUC C' WHERE BondedAreaCode='03PJC32'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY KIM THUY PHUC X' WHERE BondedAreaCode='03PJX30'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY LAM SON TB X' WHERE BondedAreaCode='03CDC89'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY LIHIT LAB VN C' WHERE BondedAreaCode='03NKC57'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY LIHIT LAB VN X' WHERE BondedAreaCode='03NKX63'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY LOGISTIC XANH C1' WHERE BondedAreaCode='03TGC05'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY LOGISTIC XANH C2' WHERE BondedAreaCode='03TGC06'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY LOGISTIC XANH W' WHERE BondedAreaCode='03TGW14'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY MEIHOTECH VN C' WHERE BondedAreaCode='03NKC25'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY MEIHOTECH VN X' WHERE BondedAreaCode='03NKX50'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY NAKASHIMA VN C' WHERE BondedAreaCode='03NKC12'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY NAKASHIMA VN X' WHERE BondedAreaCode='03NKX23'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY PHU DAT C' WHERE BondedAreaCode='03CDC88'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY PTCN BECKEN VN C' WHERE BondedAreaCode='03NKC30'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY PTCN BECKEN VN X' WHERE BondedAreaCode='03NKX36'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY QTE MOLATEC C' WHERE BondedAreaCode='03CDC36'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY QTE MOLATEC O' WHERE BondedAreaCode='03CDO01'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY SHENG FANG C' WHERE BondedAreaCode='03CDC24'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY SHENG FANG X' WHERE BondedAreaCode='03CDX02'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY TRAI DAT XANH C' WHERE BondedAreaCode='03CDC65'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY VINA OKAMOTO C' WHERE BondedAreaCode='03PJC15'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY VINA OKAMOTO X' WHERE BondedAreaCode='03PJX15'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY VSM NHAT BAN C' WHERE BondedAreaCode='03PJC30'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY VSM NHAT BAN X' WHERE BondedAreaCode='03PJX10'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY YAZAKI HP TB C' WHERE BondedAreaCode='03CDC05'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY YAZAKI HP TB X' WHERE BondedAreaCode='03CDX01'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY YAZAKI HP VN C' WHERE BondedAreaCode='03NKC19'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CTY YAZAKI HP VN X' WHERE BondedAreaCode='03NKX31'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='CY NT HOA PHAT 1' WHERE BondedAreaCode='03PLC51'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='HOYA GLASS DISK VN C' WHERE BondedAreaCode='03PLC43'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='HOYA GLASS DISK VN X' WHERE BondedAreaCode='03PLX10'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='KNQ HDL HAI DUONG' WHERE BondedAreaCode='03PJW01'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='LOP BRIDGESTONE C1' WHERE BondedAreaCode='03NKC03'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='LOP BRIDGESTONE C2' WHERE BondedAreaCode='03NKC87'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='SEWS-COMPONENTS C' WHERE BondedAreaCode='03PLC41'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='SEWS-COMPONENTS X' WHERE BondedAreaCode='03PLX20'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='TBCN TOYOTA VN C' WHERE BondedAreaCode='03PLC33'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='TBCN TOYOTA VN X' WHERE BondedAreaCode='03PLX25'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='TCTY VINACOMIN C' WHERE BondedAreaCode='03CDC08'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='TONG CTY BACH DANG F' WHERE BondedAreaCode='03CCF01'
UPDATE t_VNACC_Category_Cargo set BondedAreaName='TONG CTY BACH DANG S' WHERE BondedAreaCode='03CCS12'

---------cap nhat ten loai phuong tien van chuyen
update t_VNACC_Category_Common set Name_Vn=N'Đường biển (không Container hàng rời,lỏng)' where ReferenceDB='E005' and Code='3'
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '16.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('16.1', GETDATE(), N'cap nhat Danh Muc A202A: 34,03,18 ')
END	


