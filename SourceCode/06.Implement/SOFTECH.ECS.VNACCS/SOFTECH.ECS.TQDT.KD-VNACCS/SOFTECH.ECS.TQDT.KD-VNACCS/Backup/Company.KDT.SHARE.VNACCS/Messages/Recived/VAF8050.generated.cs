using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAF8050 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute TYS { get; set; }
public PropertiesAttribute A01 { get; set; }
public PropertiesAttribute A02 { get; set; }
public PropertiesAttribute A03 { get; set; }
public PropertiesAttribute A04 { get; set; }
public PropertiesAttribute A05 { get; set; }
public PropertiesAttribute A06 { get; set; }
public PropertiesAttribute A07 { get; set; }
public PropertiesAttribute A08 { get; set; }
public PropertiesAttribute A09 { get; set; }
public PropertiesAttribute A10 { get; set; }
public GroupAttribute CN1 { get; set; }
public PropertiesAttribute A12 { get; set; }
public PropertiesAttribute A13 { get; set; }
public PropertiesAttribute A14 { get; set; }
public PropertiesAttribute A15 { get; set; }
public PropertiesAttribute A16 { get; set; }
public PropertiesAttribute A17 { get; set; }
public PropertiesAttribute A18 { get; set; }
public PropertiesAttribute A19 { get; set; }
public PropertiesAttribute A20 { get; set; }

public VAF8050()
        {
TYS = new PropertiesAttribute(1, typeof(string));
A01 = new PropertiesAttribute(11, typeof(string));
A02 = new PropertiesAttribute(210, typeof(string));
A03 = new PropertiesAttribute(4, typeof(int));
A04 = new PropertiesAttribute(10, typeof(string));
A05 = new PropertiesAttribute(10, typeof(string));
A06 = new PropertiesAttribute(12, typeof(int));
A07 = new PropertiesAttribute(8, typeof(DateTime));
A08 = new PropertiesAttribute(3, typeof(string));
A09 = new PropertiesAttribute(6, typeof(string));
A10 = new PropertiesAttribute(210, typeof(string));
A12 = new PropertiesAttribute(35, typeof(string));
A13 = new PropertiesAttribute(13, typeof(string));
A14 = new PropertiesAttribute(300, typeof(string));
A15 = new PropertiesAttribute(13, typeof(string));
A16 = new PropertiesAttribute(300, typeof(string));
A17 = new PropertiesAttribute(8, typeof(DateTime));
A18 = new PropertiesAttribute(8, typeof(DateTime));
A19 = new PropertiesAttribute(13, typeof(int));
A20 = new PropertiesAttribute(3, typeof(string));
#region CN1
List<PropertiesAttribute> listCN1 = new List<PropertiesAttribute>();
listCN1.Add(new PropertiesAttribute(35, 5, EnumGroupID.VAF8050_CN1, typeof(string)));
CN1 = new GroupAttribute("CN1", 5, listCN1);
#endregion CN1
TongSoByte = 1403;
}

}public partial class EnumGroupID
    {
public static readonly string VAF8050_CN1 = "VAF8050_CN1";
}

}
