﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Janus.Windows.CalendarCombo;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;
using CuaKhauControl=Company.Interface.Controls.CuaKhauControl;
using DonViHaiQuanControl=Company.Interface.Controls.DonViHaiQuanControl;
using LoaiHinhMauDichControl01=Company.Interface.Controls.LoaiHinhMauDichVControl;
using NguyenTeControl=Company.Interface.Controls.NguyenTeControl;
using NuocControl=Company.Interface.Controls.NuocControl;

namespace Company.Interface.KDT.GC
{
    partial class ToKhaiGCChuyenTiepNhapForm
    {
        private Panel pnlToKhaiMauDich;
        private DataSet ds;
        private DataTable dtHangMauDich;
        private DataColumn dataColumn1;
        private DataColumn dataColumn2;
        private DataColumn dataColumn3;
        private DataColumn dataColumn4;
        private DataColumn dataColumn5;
        private DataColumn dataColumn6;
        private DataColumn dataColumn7;
        private DataColumn dataColumn8;
        private DataColumn dataColumn9;
        private DataTable dtLoaiHinhMauDich;
        private DataColumn dataColumn10;
        private DataColumn dataColumn11;
        private DataTable dtPTTT;
        private DataColumn dataColumn12;
        private DataColumn dataColumn13;
        private DataTable dtCuaKhau;
        private DataColumn dataColumn14;
        private DataColumn dataColumn15;
        private DataColumn dataColumn16;
        private DataTable dtNguyenTe;
        private DataColumn dataColumn17;
        private DataColumn dataColumn18;
        private DataTable dtCompanyNuoc;
        private DataColumn dataColumn19;
        private DataColumn dataColumn20;
        private ErrorProvider epError;
        private ContainerValidator cvError;
        private ListValidationSummary lvsError;
        private UIGroupBox uiGroupBox2;
        private DataTable dtDonViHaiQuan;
        private DataColumn dataColumn21;
        private DataColumn dataColumn22;
        private UIGroupBox uiGroupBox1;
        private UIPanelManager uiPanelManager1;
        private UIPanelGroup uiPanel0;
        private UIPanel uiPanel1;
        private UIPanelInnerContainer uiPanel1Container;
        private RangeValidator rvTyGiaTT;
        private RangeValidator rvTyGiaUSD;
        private UICommandManager cmMain;
        private UICommand cmdSave;
        private UIRebar TopRebar1;
        private UICommandBar cmbToolBar;
        private UIRebar BottomRebar1;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private IContainer components;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiGCChuyenTiepNhapForm));
            this.pnlToKhaiMauDich = new System.Windows.Forms.Panel();
            this.grpLoaiHangHoa = new Janus.Windows.EditControls.UIGroupBox();
            this.radHangMau = new Janus.Windows.EditControls.UIRadioButton();
            this.radTB = new Janus.Windows.EditControls.UIRadioButton();
            this.radNPL = new Janus.Windows.EditControls.UIRadioButton();
            this.radSP = new Janus.Windows.EditControls.UIRadioButton();
            this.uiGroupBox18 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbPTTT = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox16 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbDKGH = new Janus.Windows.EditControls.UIComboBox();
            this.grbHopDongGiao = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHHHopDongGiao = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label16 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSoHongDonggiao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.ccNgayHopDongGiao = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.grbHopDongNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHHHopDongNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccNgayHopDongNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHopDongNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblSoHDDV = new System.Windows.Forms.Label();
            this.lblNgayHHDV = new System.Windows.Forms.Label();
            this.lblNgayHDDV = new System.Windows.Forms.Label();
            this.grbNuocXK = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNuocXuatKhau = new Company.Interface.Controls.NuocControl();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtTrongLuongTinh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.grbDiaDiemDoHang = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrCuaKhau = new Company.Interface.Controls.CuaKhauControl();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNguyenTe = new Company.Interface.Controls.NguyenTeControl();
            this.txtTyGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtTyGiaUSD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox12 = new Janus.Windows.EditControls.UIGroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoKien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ccThoiGianGiaoHang = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label38 = new System.Windows.Forms.Label();
            this.txtDiaDiemGiaohang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiGiao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNguoiGiao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnNoiDungChinhSua = new Janus.Windows.EditControls.UIButton();
            this.txtLyDoSua = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDeXuatKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtChucVu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDaiDienDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDaiLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDaiLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtTenDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbNguoiXK = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaDonViDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTenDonViDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbLoaiHinh = new Company.Interface.Controls.LoaiHinhChuyenTiepControlV();
            this.grbNguoiNK = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayDangKy = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtToKhaiSo = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lblPhanLuong = new System.Windows.Forms.Label();
            this.lbltrangthai = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtsotiepnhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.ds = new System.Data.DataSet();
            this.dtHangMauDich = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dtLoaiHinhMauDich = new System.Data.DataTable();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dtPTTT = new System.Data.DataTable();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dtCuaKhau = new System.Data.DataTable();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dtNguyenTe = new System.Data.DataTable();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dtCompanyNuoc = new System.Data.DataTable();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dtDonViHaiQuan = new System.Data.DataTable();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.rvTyGiaTT = new Company.Controls.CustomValidation.RangeValidator();
            this.rvTyGiaUSD = new Company.Controls.CustomValidation.RangeValidator();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbToolBar = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdChungTuKem1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuKem");
            this.cmdTruyenDuLieuTuXa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTruyenDuLieuTuXa");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdThemMotHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemMotHang");
            this.cmdThemNhieuHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemNhieuHang");
            this.cmdReadExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.ToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.InPhieuTN1 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdInTKDTSuaDoiBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKDTSuaDoiBoSung");
            this.cmdIntokhaiTQ1 = new Janus.Windows.UI.CommandBars.UICommand("cmdIntokhaiTQ");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.NhanDuLieu = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.Huy = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.cmdThemMotHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemMotHang");
            this.cmdThemNhieuHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemNhieuHang");
            this.ToKhaiViet = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.InPhieuTN2 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdReadExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.cmdChungTuKem = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuKem");
            this.cmdGiayPhep1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmdHopDongThuongMai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDongThuongMai");
            this.cmdHoaDonThuongMai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMai");
            this.cmdDeNghiChuyenCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDeNghiChuyenCuaKhau");
            this.cmdChungTuDangAnh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnh");
            this.cmdChungTuNo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuNo");
            this.cmdNPLCungUng1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLCungUng");
            this.cmdGiayPhep = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmdHopDongThuongMai = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDongThuongMai");
            this.cmdHoaDonThuongMai = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMai");
            this.cmdDeNghiChuyenCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdDeNghiChuyenCuaKhau");
            this.cmdChungTuDangAnh = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnh");
            this.cmdTruyenDuLieuTuXa = new Janus.Windows.UI.CommandBars.UICommand("cmdTruyenDuLieuTuXa");
            this.cmdSend2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.NhanDuLieu2 = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.Huy2 = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.Separator5 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdSuaToKhaiDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaToKhaiDaDuyet");
            this.cmdHuyToKhaiDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyToKhaiDaDuyet");
            this.Separator6 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdSuaToKhaiDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaToKhaiDaDuyet");
            this.cmdHuyToKhaiDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyToKhaiDaDuyet");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdInTKDTSuaDoiBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKDTSuaDoiBoSung");
            this.cmdChungTuNo = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuNo");
            this.cmdNPLCungUng = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLCungUng");
            this.cmdIntokhaiTQ = new Janus.Windows.UI.CommandBars.UICommand("cmdIntokhaiTQ");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.ilLarge = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.cmdInToKhai196 = new Janus.Windows.UI.CommandBars.UICommand("cmdInToKhai196");
            this.cmdInToKhai1961 = new Janus.Windows.UI.CommandBars.UICommand("cmdInToKhai196");
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.pnlToKhaiMauDich.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpLoaiHangHoa)).BeginInit();
            this.grpLoaiHangHoa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).BeginInit();
            this.uiGroupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).BeginInit();
            this.uiGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDongGiao)).BeginInit();
            this.grbHopDongGiao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDongNhan)).BeginInit();
            this.grbHopDongNhan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNuocXK)).BeginInit();
            this.grbNuocXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemDoHang)).BeginInit();
            this.grbDiaDiemDoHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).BeginInit();
            this.uiGroupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiXK)).BeginInit();
            this.grbNguoiXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNK)).BeginInit();
            this.grbNguoiNK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHangMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPTTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompanyNuoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaUSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(3, 37);
            this.grbMain.Size = new System.Drawing.Size(778, 781);
            // 
            // pnlToKhaiMauDich
            // 
            this.pnlToKhaiMauDich.AutoScroll = true;
            this.pnlToKhaiMauDich.AutoScrollMargin = new System.Drawing.Size(4, 4);
            this.pnlToKhaiMauDich.BackColor = System.Drawing.Color.Transparent;
            this.pnlToKhaiMauDich.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlToKhaiMauDich.Controls.Add(this.grpLoaiHangHoa);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox18);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox16);
            this.pnlToKhaiMauDich.Controls.Add(this.grbHopDongGiao);
            this.pnlToKhaiMauDich.Controls.Add(this.grbHopDongNhan);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNuocXK);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox6);
            this.pnlToKhaiMauDich.Controls.Add(this.grbDiaDiemDoHang);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox8);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox12);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox11);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox9);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox7);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox4);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox5);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguoiXK);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox3);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguoiNK);
            this.pnlToKhaiMauDich.Controls.Add(this.label21);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox10);
            this.pnlToKhaiMauDich.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlToKhaiMauDich.Location = new System.Drawing.Point(0, 0);
            this.pnlToKhaiMauDich.Name = "pnlToKhaiMauDich";
            this.pnlToKhaiMauDich.Size = new System.Drawing.Size(778, 781);
            this.pnlToKhaiMauDich.TabIndex = 0;
            // 
            // grpLoaiHangHoa
            // 
            this.grpLoaiHangHoa.BorderColor = System.Drawing.Color.Transparent;
            this.grpLoaiHangHoa.Controls.Add(this.radHangMau);
            this.grpLoaiHangHoa.Controls.Add(this.radTB);
            this.grpLoaiHangHoa.Controls.Add(this.radNPL);
            this.grpLoaiHangHoa.Controls.Add(this.radSP);
            this.grpLoaiHangHoa.Location = new System.Drawing.Point(8, 753);
            this.grpLoaiHangHoa.Name = "grpLoaiHangHoa";
            this.grpLoaiHangHoa.Size = new System.Drawing.Size(683, 21);
            this.grpLoaiHangHoa.TabIndex = 40;
            this.grpLoaiHangHoa.VisualStyleManager = this.vsmMain;
            // 
            // radHangMau
            // 
            this.radHangMau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radHangMau.Location = new System.Drawing.Point(364, -1);
            this.radHangMau.Name = "radHangMau";
            this.radHangMau.Size = new System.Drawing.Size(139, 23);
            this.radHangMau.TabIndex = 48;
            this.radHangMau.Text = "Hàng mẫu";
            this.radHangMau.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            // 
            // radTB
            // 
            this.radTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTB.Location = new System.Drawing.Point(248, -1);
            this.radTB.Name = "radTB";
            this.radTB.Size = new System.Drawing.Size(106, 23);
            this.radTB.TabIndex = 48;
            this.radTB.Text = "Thiết bị";
            this.radTB.VisualStyleManager = this.vsmMain;
            // 
            // radNPL
            // 
            this.radNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radNPL.Location = new System.Drawing.Point(118, 0);
            this.radNPL.Name = "radNPL";
            this.radNPL.Size = new System.Drawing.Size(124, 23);
            this.radNPL.TabIndex = 47;
            this.radNPL.Text = "Nguyên phụ liệu";
            this.radNPL.VisualStyleManager = this.vsmMain;
            // 
            // radSP
            // 
            this.radSP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.radSP.Checked = true;
            this.radSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radSP.Location = new System.Drawing.Point(23, -1);
            this.radSP.Name = "radSP";
            this.radSP.Size = new System.Drawing.Size(89, 23);
            this.radSP.TabIndex = 46;
            this.radSP.TabStop = true;
            this.radSP.Text = "Sản phẩm";
            this.radSP.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox18
            // 
            this.uiGroupBox18.Controls.Add(this.cbPTTT);
            this.uiGroupBox18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox18.Location = new System.Drawing.Point(223, 433);
            this.uiGroupBox18.Name = "uiGroupBox18";
            this.uiGroupBox18.Size = new System.Drawing.Size(221, 49);
            this.uiGroupBox18.TabIndex = 9;
            this.uiGroupBox18.Text = "Phương thức thanh toán";
            this.uiGroupBox18.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox18.VisualStyleManager = this.vsmMain;
            // 
            // cbPTTT
            // 
            this.cbPTTT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTTT.DisplayMember = "Name";
            this.cbPTTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTTT.Location = new System.Drawing.Point(7, 19);
            this.cbPTTT.Name = "cbPTTT";
            this.cbPTTT.Size = new System.Drawing.Size(202, 21);
            this.cbPTTT.TabIndex = 0;
            this.cbPTTT.Tag = "PhuongThucThanhToan";
            this.cbPTTT.ValueMember = "ID";
            this.cbPTTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbPTTT.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox16
            // 
            this.uiGroupBox16.Controls.Add(this.cbDKGH);
            this.uiGroupBox16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox16.Location = new System.Drawing.Point(223, 380);
            this.uiGroupBox16.Name = "uiGroupBox16";
            this.uiGroupBox16.Size = new System.Drawing.Size(221, 48);
            this.uiGroupBox16.TabIndex = 8;
            this.uiGroupBox16.Text = "Điều kiện giao hàng";
            this.uiGroupBox16.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox16.VisualStyleManager = this.vsmMain;
            // 
            // cbDKGH
            // 
            this.cbDKGH.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDKGH.DisplayMember = "ID";
            this.cbDKGH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDKGH.Location = new System.Drawing.Point(7, 19);
            this.cbDKGH.Name = "cbDKGH";
            this.cbDKGH.Size = new System.Drawing.Size(202, 21);
            this.cbDKGH.TabIndex = 0;
            this.cbDKGH.Tag = "DieuKienGiaoHang";
            this.cbDKGH.ValueMember = "ID";
            this.cbDKGH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbDKGH.VisualStyleManager = this.vsmMain;
            // 
            // grbHopDongGiao
            // 
            this.grbHopDongGiao.Controls.Add(this.ccNgayHHHopDongGiao);
            this.grbHopDongGiao.Controls.Add(this.label16);
            this.grbHopDongGiao.Controls.Add(this.label5);
            this.grbHopDongGiao.Controls.Add(this.txtSoHongDonggiao);
            this.grbHopDongGiao.Controls.Add(this.label20);
            this.grbHopDongGiao.Controls.Add(this.ccNgayHopDongGiao);
            this.grbHopDongGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbHopDongGiao.Location = new System.Drawing.Point(458, 190);
            this.grbHopDongGiao.Name = "grbHopDongGiao";
            this.grbHopDongGiao.Size = new System.Drawing.Size(242, 105);
            this.grbHopDongGiao.TabIndex = 11;
            this.grbHopDongGiao.Text = "Hợp đồng giao";
            this.grbHopDongGiao.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ccNgayHHHopDongGiao
            // 
            // 
            // 
            // 
            this.ccNgayHHHopDongGiao.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHHopDongGiao.DropDownCalendar.Name = "";
            this.ccNgayHHHopDongGiao.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHHHopDongGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHHopDongGiao.Location = new System.Drawing.Point(132, 80);
            this.ccNgayHHHopDongGiao.Name = "ccNgayHHHopDongGiao";
            this.ccNgayHHHopDongGiao.Nullable = true;
            this.ccNgayHHHopDongGiao.NullButtonText = "Xóa";
            this.ccNgayHHHopDongGiao.ShowNullButton = true;
            this.ccNgayHHHopDongGiao.Size = new System.Drawing.Size(95, 21);
            this.ccNgayHHHopDongGiao.TabIndex = 2;
            this.ccNgayHHHopDongGiao.TodayButtonText = "Hôm nay";
            this.ccNgayHHHopDongGiao.Value = new System.DateTime(2012, 3, 19, 0, 0, 0, 0);
            this.ccNgayHHHopDongGiao.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(133, 64);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "Ngày hết hạn";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Số hợp đồng";
            // 
            // txtSoHongDonggiao
            // 
            this.txtSoHongDonggiao.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoHongDonggiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHongDonggiao.Location = new System.Drawing.Point(7, 36);
            this.txtSoHongDonggiao.Name = "txtSoHongDonggiao";
            this.txtSoHongDonggiao.Size = new System.Drawing.Size(219, 21);
            this.txtSoHongDonggiao.TabIndex = 0;
            this.txtSoHongDonggiao.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHongDonggiao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHongDonggiao.VisualStyleManager = this.vsmMain;
            this.txtSoHongDonggiao.ButtonClick += new System.EventHandler(this.txtSoHongDonggiao_ButtonClick);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(10, 62);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Ngày";
            // 
            // ccNgayHopDongGiao
            // 
            // 
            // 
            // 
            this.ccNgayHopDongGiao.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHopDongGiao.DropDownCalendar.Name = "";
            this.ccNgayHopDongGiao.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHopDongGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHopDongGiao.Location = new System.Drawing.Point(8, 78);
            this.ccNgayHopDongGiao.Name = "ccNgayHopDongGiao";
            this.ccNgayHopDongGiao.Nullable = true;
            this.ccNgayHopDongGiao.NullButtonText = "Xóa";
            this.ccNgayHopDongGiao.ShowNullButton = true;
            this.ccNgayHopDongGiao.Size = new System.Drawing.Size(99, 21);
            this.ccNgayHopDongGiao.TabIndex = 1;
            this.ccNgayHopDongGiao.TodayButtonText = "Hôm nay";
            this.ccNgayHopDongGiao.Value = new System.DateTime(2012, 3, 19, 0, 0, 0, 0);
            this.ccNgayHopDongGiao.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            // 
            // grbHopDongNhan
            // 
            this.grbHopDongNhan.Controls.Add(this.ccNgayHHHopDongNhan);
            this.grbHopDongNhan.Controls.Add(this.ccNgayHopDongNhan);
            this.grbHopDongNhan.Controls.Add(this.txtSoHopDongNhan);
            this.grbHopDongNhan.Controls.Add(this.lblSoHDDV);
            this.grbHopDongNhan.Controls.Add(this.lblNgayHHDV);
            this.grbHopDongNhan.Controls.Add(this.lblNgayHDDV);
            this.grbHopDongNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbHopDongNhan.Location = new System.Drawing.Point(458, 84);
            this.grbHopDongNhan.Name = "grbHopDongNhan";
            this.grbHopDongNhan.Size = new System.Drawing.Size(241, 105);
            this.grbHopDongNhan.TabIndex = 10;
            this.grbHopDongNhan.Text = "Hợp đồng nhận";
            this.grbHopDongNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ccNgayHHHopDongNhan
            // 
            // 
            // 
            // 
            this.ccNgayHHHopDongNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHHopDongNhan.DropDownCalendar.Name = "";
            this.ccNgayHHHopDongNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHHHopDongNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHHopDongNhan.IsNullDate = true;
            this.ccNgayHHHopDongNhan.Location = new System.Drawing.Point(131, 78);
            this.ccNgayHHHopDongNhan.Name = "ccNgayHHHopDongNhan";
            this.ccNgayHHHopDongNhan.Nullable = true;
            this.ccNgayHHHopDongNhan.NullButtonText = "Xóa";
            this.ccNgayHHHopDongNhan.ShowDropDown = false;
            this.ccNgayHHHopDongNhan.ShowNullButton = true;
            this.ccNgayHHHopDongNhan.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHHHopDongNhan.TabIndex = 11;
            this.ccNgayHHHopDongNhan.TodayButtonText = "Hôm nay";
            this.ccNgayHHHopDongNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            // 
            // ccNgayHopDongNhan
            // 
            // 
            // 
            // 
            this.ccNgayHopDongNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHopDongNhan.DropDownCalendar.Name = "";
            this.ccNgayHopDongNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHopDongNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHopDongNhan.IsNullDate = true;
            this.ccNgayHopDongNhan.Location = new System.Drawing.Point(7, 78);
            this.ccNgayHopDongNhan.Name = "ccNgayHopDongNhan";
            this.ccNgayHopDongNhan.Nullable = true;
            this.ccNgayHopDongNhan.NullButtonText = "Xóa";
            this.ccNgayHopDongNhan.ShowDropDown = false;
            this.ccNgayHopDongNhan.ShowNullButton = true;
            this.ccNgayHopDongNhan.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHopDongNhan.TabIndex = 11;
            this.ccNgayHopDongNhan.TodayButtonText = "Hôm nay";
            this.ccNgayHopDongNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            // 
            // txtSoHopDongNhan
            // 
            this.txtSoHopDongNhan.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoHopDongNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDongNhan.Location = new System.Drawing.Point(6, 33);
            this.txtSoHopDongNhan.Name = "txtSoHopDongNhan";
            this.txtSoHopDongNhan.ReadOnly = true;
            this.txtSoHopDongNhan.Size = new System.Drawing.Size(221, 21);
            this.txtSoHopDongNhan.TabIndex = 0;
            this.txtSoHopDongNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDongNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHopDongNhan.VisualStyleManager = this.vsmMain;
            this.txtSoHopDongNhan.ButtonClick += new System.EventHandler(this.txtSoHopDongNhan_ButtonClick);
            // 
            // lblSoHDDV
            // 
            this.lblSoHDDV.AutoSize = true;
            this.lblSoHDDV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHDDV.Location = new System.Drawing.Point(7, 15);
            this.lblSoHDDV.Name = "lblSoHDDV";
            this.lblSoHDDV.Size = new System.Drawing.Size(67, 13);
            this.lblSoHDDV.TabIndex = 0;
            this.lblSoHDDV.Text = "Số hợp đồng";
            // 
            // lblNgayHHDV
            // 
            this.lblNgayHHDV.AutoSize = true;
            this.lblNgayHHDV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHHDV.Location = new System.Drawing.Point(131, 60);
            this.lblNgayHHDV.Name = "lblNgayHHDV";
            this.lblNgayHHDV.Size = new System.Drawing.Size(72, 13);
            this.lblNgayHHDV.TabIndex = 4;
            this.lblNgayHHDV.Text = "Ngày hết hạn";
            // 
            // lblNgayHDDV
            // 
            this.lblNgayHDDV.AutoSize = true;
            this.lblNgayHDDV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHDDV.Location = new System.Drawing.Point(8, 59);
            this.lblNgayHDDV.Name = "lblNgayHDDV";
            this.lblNgayHDDV.Size = new System.Drawing.Size(32, 13);
            this.lblNgayHDDV.TabIndex = 2;
            this.lblNgayHDDV.Text = "Ngày";
            // 
            // grbNuocXK
            // 
            this.grbNuocXK.Controls.Add(this.ctrNuocXuatKhau);
            this.grbNuocXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNuocXK.Location = new System.Drawing.Point(223, 296);
            this.grbNuocXK.Name = "grbNuocXK";
            this.grbNuocXK.Size = new System.Drawing.Size(221, 80);
            this.grbNuocXK.TabIndex = 7;
            this.grbNuocXK.Text = "Nước xuất khẩu";
            this.grbNuocXK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbNuocXK.VisualStyleManager = this.vsmMain;
            // 
            // ctrNuocXuatKhau
            // 
            this.ctrNuocXuatKhau.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXuatKhau.ErrorMessage = "\"Nước xuất khẩu\" bắt buộc phải chọn.";
            this.ctrNuocXuatKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocXuatKhau.Location = new System.Drawing.Point(7, 19);
            this.ctrNuocXuatKhau.Ma = "";
            this.ctrNuocXuatKhau.Name = "ctrNuocXuatKhau";
            this.ctrNuocXuatKhau.ReadOnly = false;
            this.ctrNuocXuatKhau.Size = new System.Drawing.Size(219, 50);
            this.ctrNuocXuatKhau.TabIndex = 0;
            this.ctrNuocXuatKhau.Tag = "NuocXK";
            this.ctrNuocXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.label10);
            this.uiGroupBox6.Controls.Add(this.label39);
            this.uiGroupBox6.Controls.Add(this.txtTrongLuongTinh);
            this.uiGroupBox6.Controls.Add(this.txtTrongLuong);
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(458, 382);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(243, 101);
            this.uiGroupBox6.TabIndex = 13;
            this.uiGroupBox6.Tag = "CangDoHang";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 13);
            this.label10.TabIndex = 45;
            this.label10.Text = "Trọng lượng tịnh";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(4, 10);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(68, 13);
            this.label39.TabIndex = 44;
            this.label39.Text = "Trọng lượng ";
            // 
            // txtTrongLuongTinh
            // 
            this.txtTrongLuongTinh.DecimalDigits = 2;
            this.txtTrongLuongTinh.Location = new System.Drawing.Point(8, 69);
            this.txtTrongLuongTinh.Name = "txtTrongLuongTinh";
            this.txtTrongLuongTinh.Size = new System.Drawing.Size(219, 21);
            this.txtTrongLuongTinh.TabIndex = 1;
            this.txtTrongLuongTinh.Tag = "TrongLuongTinh";
            this.txtTrongLuongTinh.Text = "0.00";
            this.txtTrongLuongTinh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTrongLuongTinh.Value = 0;
            this.txtTrongLuongTinh.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTrongLuongTinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTrongLuongTinh.VisualStyleManager = this.vsmMain;
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.DecimalDigits = 2;
            this.txtTrongLuong.Location = new System.Drawing.Point(7, 26);
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.Size = new System.Drawing.Size(220, 21);
            this.txtTrongLuong.TabIndex = 0;
            this.txtTrongLuong.Tag = "TrongLuong";
            this.txtTrongLuong.Text = "0.00";
            this.txtTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTrongLuong.VisualStyleManager = this.vsmMain;
            // 
            // grbDiaDiemDoHang
            // 
            this.grbDiaDiemDoHang.Controls.Add(this.ctrCuaKhau);
            this.grbDiaDiemDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDiaDiemDoHang.Location = new System.Drawing.Point(458, 296);
            this.grbDiaDiemDoHang.Name = "grbDiaDiemDoHang";
            this.grbDiaDiemDoHang.Size = new System.Drawing.Size(243, 80);
            this.grbDiaDiemDoHang.TabIndex = 12;
            this.grbDiaDiemDoHang.Tag = "CangDoHang";
            this.grbDiaDiemDoHang.Text = "Địa điểm dỡ hàng";
            this.grbDiaDiemDoHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbDiaDiemDoHang.VisualStyleManager = this.vsmMain;
            // 
            // ctrCuaKhau
            // 
            this.ctrCuaKhau.BackColor = System.Drawing.Color.Transparent;
            this.ctrCuaKhau.ErrorMessage = "\"Địa điểm dỡ hàng\" bắt buộc phải chọn.";
            this.ctrCuaKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrCuaKhau.Location = new System.Drawing.Point(8, 24);
            this.ctrCuaKhau.Ma = "";
            this.ctrCuaKhau.Name = "ctrCuaKhau";
            this.ctrCuaKhau.ReadOnly = false;
            this.ctrCuaKhau.Size = new System.Drawing.Size(235, 50);
            this.ctrCuaKhau.TabIndex = 0;
            this.ctrCuaKhau.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.Controls.Add(this.ctrNguyenTe);
            this.uiGroupBox8.Controls.Add(this.txtTyGiaTinhThue);
            this.uiGroupBox8.Controls.Add(this.label36);
            this.uiGroupBox8.Controls.Add(this.label37);
            this.uiGroupBox8.Controls.Add(this.txtTyGiaUSD);
            this.uiGroupBox8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox8.Location = new System.Drawing.Point(224, 189);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(221, 105);
            this.uiGroupBox8.TabIndex = 6;
            this.uiGroupBox8.Text = "Đồng tiền thanh toán";
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // ctrNguyenTe
            // 
            this.ctrNguyenTe.BackColor = System.Drawing.Color.Transparent;
            this.ctrNguyenTe.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.ctrNguyenTe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNguyenTe.Location = new System.Drawing.Point(8, 20);
            this.ctrNguyenTe.Ma = "";
            this.ctrNguyenTe.Name = "ctrNguyenTe";
            this.ctrNguyenTe.ReadOnly = false;
            this.ctrNguyenTe.Size = new System.Drawing.Size(215, 22);
            this.ctrNguyenTe.TabIndex = 0;
            this.ctrNguyenTe.Tag = "DongTienThanhToan";
            this.ctrNguyenTe.VisualStyleManager = this.vsmMain;
            this.ctrNguyenTe.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(this.CtrNguyenTeValueChanged);
            // 
            // txtTyGiaTinhThue
            // 
            this.txtTyGiaTinhThue.DecimalDigits = 2;
            this.txtTyGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaTinhThue.Location = new System.Drawing.Point(80, 47);
            this.txtTyGiaTinhThue.Name = "txtTyGiaTinhThue";
            this.txtTyGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaTinhThue.Size = new System.Drawing.Size(129, 21);
            this.txtTyGiaTinhThue.TabIndex = 1;
            this.txtTyGiaTinhThue.Tag = "TyGiaTinhThue";
            this.txtTyGiaTinhThue.Text = "0.00";
            this.txtTyGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtTyGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTyGiaTinhThue.VisualStyleManager = this.vsmMain;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(8, 82);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(59, 13);
            this.label36.TabIndex = 3;
            this.label36.Text = "Tỷ giá USD";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(8, 55);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(51, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "Tỷ giá TT";
            // 
            // txtTyGiaUSD
            // 
            this.txtTyGiaUSD.DecimalDigits = 0;
            this.txtTyGiaUSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaUSD.Location = new System.Drawing.Point(80, 74);
            this.txtTyGiaUSD.Name = "txtTyGiaUSD";
            this.txtTyGiaUSD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaUSD.Size = new System.Drawing.Size(129, 21);
            this.txtTyGiaUSD.TabIndex = 2;
            this.txtTyGiaUSD.Text = "0";
            this.txtTyGiaUSD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaUSD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTyGiaUSD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTyGiaUSD.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox12
            // 
            this.uiGroupBox12.Controls.Add(this.label3);
            this.uiGroupBox12.Controls.Add(this.txtSoKien);
            this.uiGroupBox12.Controls.Add(this.label2);
            this.uiGroupBox12.Controls.Add(this.label4);
            this.uiGroupBox12.Controls.Add(this.label1);
            this.uiGroupBox12.Controls.Add(this.ccThoiGianGiaoHang);
            this.uiGroupBox12.Controls.Add(this.label38);
            this.uiGroupBox12.Controls.Add(this.txtDiaDiemGiaohang);
            this.uiGroupBox12.Controls.Add(this.txtTenNguoiGiao);
            this.uiGroupBox12.Controls.Add(this.txtMaNguoiGiao);
            this.uiGroupBox12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox12.Location = new System.Drawing.Point(223, 489);
            this.uiGroupBox12.Name = "uiGroupBox12";
            this.uiGroupBox12.Size = new System.Drawing.Size(480, 129);
            this.uiGroupBox12.TabIndex = 14;
            this.uiGroupBox12.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(323, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 43;
            this.label3.Text = "Số kiện";
            // 
            // txtSoKien
            // 
            this.txtSoKien.DecimalDigits = 0;
            this.txtSoKien.Location = new System.Drawing.Point(370, 98);
            this.txtSoKien.Name = "txtSoKien";
            this.txtSoKien.Size = new System.Drawing.Size(92, 21);
            this.txtSoKien.TabIndex = 4;
            this.txtSoKien.Tag = "SoKienHang";
            this.txtSoKien.Text = "0";
            this.txtSoKien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoKien.Value = 0;
            this.txtSoKien.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtSoKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoKien.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "Địa điểm giao hàng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Ngày giao hàng";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 13);
            this.label1.TabIndex = 43;
            this.label1.Text = "Tên người chỉ định giao hàng";
            // 
            // ccThoiGianGiaoHang
            // 
            // 
            // 
            // 
            this.ccThoiGianGiaoHang.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccThoiGianGiaoHang.DropDownCalendar.Name = "";
            this.ccThoiGianGiaoHang.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccThoiGianGiaoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccThoiGianGiaoHang.Location = new System.Drawing.Point(150, 97);
            this.ccThoiGianGiaoHang.Name = "ccThoiGianGiaoHang";
            this.ccThoiGianGiaoHang.Nullable = true;
            this.ccThoiGianGiaoHang.NullButtonText = "Xóa";
            this.ccThoiGianGiaoHang.ShowNullButton = true;
            this.ccThoiGianGiaoHang.Size = new System.Drawing.Size(86, 21);
            this.ccThoiGianGiaoHang.TabIndex = 3;
            this.ccThoiGianGiaoHang.TodayButtonText = "Hôm nay";
            this.ccThoiGianGiaoHang.Value = new System.DateTime(2012, 3, 19, 0, 0, 0, 0);
            this.ccThoiGianGiaoHang.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(5, 21);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(140, 13);
            this.label38.TabIndex = 43;
            this.label38.Text = "Mã người chỉ định giao hàng";
            // 
            // txtDiaDiemGiaohang
            // 
            this.txtDiaDiemGiaohang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemGiaohang.Location = new System.Drawing.Point(151, 70);
            this.txtDiaDiemGiaohang.Name = "txtDiaDiemGiaohang";
            this.txtDiaDiemGiaohang.Size = new System.Drawing.Size(311, 21);
            this.txtDiaDiemGiaohang.TabIndex = 2;
            this.txtDiaDiemGiaohang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemGiaohang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtTenNguoiGiao
            // 
            this.txtTenNguoiGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiGiao.Location = new System.Drawing.Point(151, 43);
            this.txtTenNguoiGiao.Name = "txtTenNguoiGiao";
            this.txtTenNguoiGiao.Size = new System.Drawing.Size(311, 21);
            this.txtTenNguoiGiao.TabIndex = 1;
            this.txtTenNguoiGiao.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNguoiGiao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtMaNguoiGiao
            // 
            this.txtMaNguoiGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiGiao.Location = new System.Drawing.Point(151, 16);
            this.txtMaNguoiGiao.Name = "txtMaNguoiGiao";
            this.txtMaNguoiGiao.Size = new System.Drawing.Size(311, 21);
            this.txtMaNguoiGiao.TabIndex = 0;
            this.txtMaNguoiGiao.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiGiao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.Controls.Add(this.btnNoiDungChinhSua);
            this.uiGroupBox11.Controls.Add(this.txtLyDoSua);
            this.uiGroupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox11.Location = new System.Drawing.Point(223, 624);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(480, 129);
            this.uiGroupBox11.TabIndex = 4;
            this.uiGroupBox11.Text = "Lý do sửa";
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnNoiDungChinhSua
            // 
            this.btnNoiDungChinhSua.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoiDungChinhSua.Location = new System.Drawing.Point(383, 101);
            this.btnNoiDungChinhSua.Name = "btnNoiDungChinhSua";
            this.btnNoiDungChinhSua.Size = new System.Drawing.Size(85, 25);
            this.btnNoiDungChinhSua.TabIndex = 277;
            this.btnNoiDungChinhSua.Text = "Nội dung sửa";
            this.btnNoiDungChinhSua.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnNoiDungChinhSua.VisualStyleManager = this.vsmMain;
            this.btnNoiDungChinhSua.Click += new System.EventHandler(this.btnNoiDungChinhSua_Click);
            // 
            // txtLyDoSua
            // 
            this.txtLyDoSua.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtLyDoSua.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLyDoSua.Location = new System.Drawing.Point(3, 17);
            this.txtLyDoSua.Multiline = true;
            this.txtLyDoSua.Name = "txtLyDoSua";
            this.txtLyDoSua.Size = new System.Drawing.Size(374, 109);
            this.txtLyDoSua.TabIndex = 0;
            this.txtLyDoSua.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLyDoSua.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.Controls.Add(this.txtDeXuatKhac);
            this.uiGroupBox9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox9.Location = new System.Drawing.Point(8, 624);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(204, 129);
            this.uiGroupBox9.TabIndex = 4;
            this.uiGroupBox9.Text = "Đề xuất khác của doanh nghiệp";
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtDeXuatKhac
            // 
            this.txtDeXuatKhac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDeXuatKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeXuatKhac.Location = new System.Drawing.Point(3, 17);
            this.txtDeXuatKhac.Multiline = true;
            this.txtDeXuatKhac.Name = "txtDeXuatKhac";
            this.txtDeXuatKhac.Size = new System.Drawing.Size(198, 109);
            this.txtDeXuatKhac.TabIndex = 0;
            this.txtDeXuatKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDeXuatKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.Controls.Add(this.txtChucVu);
            this.uiGroupBox7.Controls.Add(this.label8);
            this.uiGroupBox7.Controls.Add(this.txtDaiDienDN);
            this.uiGroupBox7.Controls.Add(this.label7);
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(8, 489);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(203, 129);
            this.uiGroupBox7.TabIndex = 4;
            this.uiGroupBox7.Text = "Đại diện doanh nghiệp";
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtChucVu
            // 
            this.txtChucVu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChucVu.Location = new System.Drawing.Point(10, 95);
            this.txtChucVu.Name = "txtChucVu";
            this.txtChucVu.Size = new System.Drawing.Size(180, 21);
            this.txtChucVu.TabIndex = 0;
            this.txtChucVu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChucVu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 43;
            this.label8.Text = "Chức vụ";
            // 
            // txtDaiDienDN
            // 
            this.txtDaiDienDN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDaiDienDN.Location = new System.Drawing.Point(10, 43);
            this.txtDaiDienDN.Name = "txtDaiDienDN";
            this.txtDaiDienDN.Size = new System.Drawing.Size(181, 21);
            this.txtDaiDienDN.TabIndex = 0;
            this.txtDaiDienDN.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDaiDienDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDaiDienDN.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 13);
            this.label7.TabIndex = 43;
            this.label7.Text = "Tên người đại diện";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.txtTenDaiLy);
            this.uiGroupBox4.Controls.Add(this.txtMaDaiLy);
            this.uiGroupBox4.Controls.Add(this.label32);
            this.uiGroupBox4.Controls.Add(this.label33);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(9, 379);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(202, 104);
            this.uiGroupBox4.TabIndex = 3;
            this.uiGroupBox4.Text = "Đại lý làm TTHQ";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDaiLy
            // 
            this.txtTenDaiLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDaiLy.Location = new System.Drawing.Point(40, 47);
            this.txtTenDaiLy.Multiline = true;
            this.txtTenDaiLy.Name = "txtTenDaiLy";
            this.txtTenDaiLy.Size = new System.Drawing.Size(149, 48);
            this.txtTenDaiLy.TabIndex = 1;
            this.txtTenDaiLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDaiLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDaiLy.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDaiLy
            // 
            this.txtMaDaiLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDaiLy.Location = new System.Drawing.Point(40, 20);
            this.txtMaDaiLy.Name = "txtMaDaiLy";
            this.txtMaDaiLy.Size = new System.Drawing.Size(149, 21);
            this.txtMaDaiLy.TabIndex = 0;
            this.txtMaDaiLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDaiLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDaiLy.VisualStyleManager = this.vsmMain;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(8, 28);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(21, 13);
            this.label32.TabIndex = 0;
            this.label32.Tag = "Ma";
            this.label32.Text = "Mã";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(8, 55);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(25, 13);
            this.label33.TabIndex = 2;
            this.label33.Tag = "Ten";
            this.label33.Text = "Tên";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.txtMaDonViUyThac);
            this.uiGroupBox5.Controls.Add(this.label34);
            this.uiGroupBox5.Controls.Add(this.label35);
            this.uiGroupBox5.Controls.Add(this.txtTenDonViUyThac);
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(9, 296);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(202, 80);
            this.uiGroupBox5.TabIndex = 2;
            this.uiGroupBox5.Text = "Người ủy thác";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDonViUyThac
            // 
            this.txtMaDonViUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonViUyThac.Location = new System.Drawing.Point(40, 20);
            this.txtMaDonViUyThac.Name = "txtMaDonViUyThac";
            this.txtMaDonViUyThac.Size = new System.Drawing.Size(149, 21);
            this.txtMaDonViUyThac.TabIndex = 0;
            this.txtMaDonViUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonViUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDonViUyThac.VisualStyleManager = this.vsmMain;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(8, 28);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(21, 13);
            this.label34.TabIndex = 0;
            this.label34.Text = "Mã";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(8, 55);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(25, 13);
            this.label35.TabIndex = 2;
            this.label35.Tag = "TenDVUT";
            this.label35.Text = "Tên";
            // 
            // txtTenDonViUyThac
            // 
            this.txtTenDonViUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonViUyThac.Location = new System.Drawing.Point(40, 47);
            this.txtTenDonViUyThac.Name = "txtTenDonViUyThac";
            this.txtTenDonViUyThac.Size = new System.Drawing.Size(149, 21);
            this.txtTenDonViUyThac.TabIndex = 1;
            this.txtTenDonViUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonViUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDonViUyThac.VisualStyleManager = this.vsmMain;
            // 
            // grbNguoiXK
            // 
            this.grbNguoiXK.Controls.Add(this.txtMaDonViDoiTac);
            this.grbNguoiXK.Controls.Add(this.label9);
            this.grbNguoiXK.Controls.Add(this.label11);
            this.grbNguoiXK.Controls.Add(this.txtTenDonViDoiTac);
            this.grbNguoiXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiXK.Location = new System.Drawing.Point(10, 190);
            this.grbNguoiXK.Name = "grbNguoiXK";
            this.grbNguoiXK.Size = new System.Drawing.Size(202, 104);
            this.grbNguoiXK.TabIndex = 1;
            this.grbNguoiXK.Tag = "NguoiXuatKhau";
            this.grbNguoiXK.Text = "Người xuất khẩu";
            this.grbNguoiXK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbNguoiXK.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDonViDoiTac
            // 
            this.txtMaDonViDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonViDoiTac.Location = new System.Drawing.Point(39, 19);
            this.txtMaDonViDoiTac.Name = "txtMaDonViDoiTac";
            this.txtMaDonViDoiTac.Size = new System.Drawing.Size(150, 21);
            this.txtMaDonViDoiTac.TabIndex = 0;
            this.txtMaDonViDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonViDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Mã";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(5, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 2;
            this.label11.Tag = "TenDVUT";
            this.label11.Text = "Tên";
            // 
            // txtTenDonViDoiTac
            // 
            this.txtTenDonViDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonViDoiTac.Location = new System.Drawing.Point(39, 46);
            this.txtTenDonViDoiTac.Multiline = true;
            this.txtTenDonViDoiTac.Name = "txtTenDonViDoiTac";
            this.txtTenDonViDoiTac.Size = new System.Drawing.Size(150, 52);
            this.txtTenDonViDoiTac.TabIndex = 0;
            this.txtTenDonViDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonViDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDonViDoiTac.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Controls.Add(this.cbLoaiHinh);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(224, 84);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(221, 104);
            this.uiGroupBox3.TabIndex = 5;
            this.uiGroupBox3.Text = "Loại hình chuyển tiếp";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // cbLoaiHinh
            // 
            this.cbLoaiHinh.BackColor = System.Drawing.Color.Transparent;
            this.cbLoaiHinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLoaiHinh.Location = new System.Drawing.Point(6, 21);
            this.cbLoaiHinh.Ma = "";
            this.cbLoaiHinh.Name = "cbLoaiHinh";
            this.cbLoaiHinh.Nhom = "N";
            this.cbLoaiHinh.ReadOnly = false;
            this.cbLoaiHinh.Size = new System.Drawing.Size(205, 52);
            this.cbLoaiHinh.TabIndex = 0;
            this.cbLoaiHinh.VisualStyleManager = null;
            // 
            // grbNguoiNK
            // 
            this.grbNguoiNK.Controls.Add(this.txtTenDonVi);
            this.grbNguoiNK.Controls.Add(this.txtMaDonVi);
            this.grbNguoiNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiNK.Location = new System.Drawing.Point(10, 84);
            this.grbNguoiNK.Name = "grbNguoiNK";
            this.grbNguoiNK.Size = new System.Drawing.Size(202, 104);
            this.grbNguoiNK.TabIndex = 0;
            this.grbNguoiNK.Text = "Người nhập khẩu";
            this.grbNguoiNK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbNguoiNK.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDonVi
            // 
            this.txtTenDonVi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTenDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonVi.Location = new System.Drawing.Point(8, 48);
            this.txtTenDonVi.Multiline = true;
            this.txtTenDonVi.Name = "txtTenDonVi";
            this.txtTenDonVi.ReadOnly = true;
            this.txtTenDonVi.Size = new System.Drawing.Size(181, 48);
            this.txtTenDonVi.TabIndex = 1;
            this.txtTenDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDonVi
            // 
            this.txtMaDonVi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMaDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonVi.Location = new System.Drawing.Point(8, 20);
            this.txtMaDonVi.Name = "txtMaDonVi";
            this.txtMaDonVi.ReadOnly = true;
            this.txtMaDonVi.Size = new System.Drawing.Size(181, 21);
            this.txtMaDonVi.TabIndex = 0;
            this.txtMaDonVi.Text = "0400100457";
            this.txtMaDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDonVi.VisualStyleManager = this.vsmMain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(695, 605);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(0, 13);
            this.label21.TabIndex = 5;
            this.label21.Visible = false;
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.Controls.Add(this.ccNgayDangKy);
            this.uiGroupBox10.Controls.Add(this.txtToKhaiSo);
            this.uiGroupBox10.Controls.Add(this.label24);
            this.uiGroupBox10.Controls.Add(this.label25);
            this.uiGroupBox10.Controls.Add(this.lblPhanLuong);
            this.uiGroupBox10.Controls.Add(this.lbltrangthai);
            this.uiGroupBox10.Controls.Add(this.label6);
            this.uiGroupBox10.Controls.Add(this.label29);
            this.uiGroupBox10.Controls.Add(this.txtsotiepnhan);
            this.uiGroupBox10.Controls.Add(this.label31);
            this.uiGroupBox10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox10.Location = new System.Drawing.Point(8, 3);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(693, 75);
            this.uiGroupBox10.TabIndex = 0;
            this.uiGroupBox10.Text = "Thông tin chung";
            this.uiGroupBox10.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox10.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayDangKy
            // 
            // 
            // 
            // 
            this.ccNgayDangKy.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayDangKy.DropDownCalendar.Name = "";
            this.ccNgayDangKy.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayDangKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDangKy.IsNullDate = true;
            this.ccNgayDangKy.Location = new System.Drawing.Point(297, 41);
            this.ccNgayDangKy.Name = "ccNgayDangKy";
            this.ccNgayDangKy.Nullable = true;
            this.ccNgayDangKy.NullButtonText = "Xóa";
            this.ccNgayDangKy.ShowDropDown = false;
            this.ccNgayDangKy.ShowNullButton = true;
            this.ccNgayDangKy.Size = new System.Drawing.Size(96, 21);
            this.ccNgayDangKy.TabIndex = 11;
            this.ccNgayDangKy.TodayButtonText = "Hôm nay";
            this.ccNgayDangKy.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayDangKy.VisualStyleManager = this.vsmMain;
            // 
            // txtToKhaiSo
            // 
            this.txtToKhaiSo.BackColor = System.Drawing.Color.White;
            this.txtToKhaiSo.DecimalDigits = 0;
            this.txtToKhaiSo.Location = new System.Drawing.Point(104, 41);
            this.txtToKhaiSo.Name = "txtToKhaiSo";
            this.txtToKhaiSo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtToKhaiSo.Size = new System.Drawing.Size(95, 21);
            this.txtToKhaiSo.TabIndex = 7;
            this.txtToKhaiSo.Text = "0";
            this.txtToKhaiSo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtToKhaiSo.Value = 0;
            this.txtToKhaiSo.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtToKhaiSo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtToKhaiSo.VisualStyleManager = this.vsmMain;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(214, 47);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 13);
            this.label24.TabIndex = 10;
            this.label24.Text = "Ngày đăng ký";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(28, 46);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 13);
            this.label25.TabIndex = 6;
            this.label25.Text = "Số tờ khai";
            // 
            // lblPhanLuong
            // 
            this.lblPhanLuong.AutoSize = true;
            this.lblPhanLuong.BackColor = System.Drawing.Color.Transparent;
            this.lblPhanLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLuong.ForeColor = System.Drawing.Color.Red;
            this.lblPhanLuong.Location = new System.Drawing.Point(523, 23);
            this.lblPhanLuong.Name = "lblPhanLuong";
            this.lblPhanLuong.Size = new System.Drawing.Size(69, 13);
            this.lblPhanLuong.TabIndex = 3;
            this.lblPhanLuong.Text = "Phân luồng";
            // 
            // lbltrangthai
            // 
            this.lbltrangthai.AutoSize = true;
            this.lbltrangthai.BackColor = System.Drawing.Color.Transparent;
            this.lbltrangthai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltrangthai.ForeColor = System.Drawing.Color.Red;
            this.lbltrangthai.Location = new System.Drawing.Point(294, 23);
            this.lbltrangthai.Name = "lbltrangthai";
            this.lbltrangthai.Size = new System.Drawing.Size(133, 13);
            this.lbltrangthai.TabIndex = 3;
            this.lbltrangthai.Text = "Chưa gửi đến Hải quan";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(447, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Trạng thái";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(215, 24);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(65, 13);
            this.label29.TabIndex = 2;
            this.label29.Text = "Trạng thái";
            // 
            // txtsotiepnhan
            // 
            this.txtsotiepnhan.DecimalDigits = 0;
            this.txtsotiepnhan.Location = new System.Drawing.Point(104, 18);
            this.txtsotiepnhan.Name = "txtsotiepnhan";
            this.txtsotiepnhan.ReadOnly = true;
            this.txtsotiepnhan.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtsotiepnhan.Size = new System.Drawing.Size(95, 21);
            this.txtsotiepnhan.TabIndex = 1;
            this.txtsotiepnhan.Text = "0";
            this.txtsotiepnhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtsotiepnhan.Value = 0;
            this.txtsotiepnhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtsotiepnhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtsotiepnhan.VisualStyleManager = this.vsmMain;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(28, 23);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(77, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Số tiếp nhận";
            // 
            // ds
            // 
            this.ds.DataSetName = "NewDataSet";
            this.ds.Locale = new System.Globalization.CultureInfo("en-US");
            this.ds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtHangMauDich,
            this.dtLoaiHinhMauDich,
            this.dtPTTT,
            this.dtCuaKhau,
            this.dtNguyenTe,
            this.dtCompanyNuoc,
            this.dtDonViHaiQuan});
            // 
            // dtHangMauDich
            // 
            this.dtHangMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9});
            this.dtHangMauDich.TableName = "HangMauDich";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "hmd_SoThuTuHang";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "hmd_MaHS";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "hmd_MaPhu";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "hmd_TenHang";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "nuoc_Ten";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "donvitinh_Ten";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "hmd_Luong";
            this.dataColumn7.DataType = typeof(decimal);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "hmd_DonGiaKB";
            this.dataColumn8.DataType = typeof(decimal);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "hmd_TriGiaKB";
            this.dataColumn9.DataType = typeof(decimal);
            // 
            // dtLoaiHinhMauDich
            // 
            this.dtLoaiHinhMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10,
            this.dataColumn11});
            this.dtLoaiHinhMauDich.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "loaihinh_Ma"}, true)});
            this.dtLoaiHinhMauDich.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn10};
            this.dtLoaiHinhMauDich.TableName = "LoaiHinhMauDich";
            // 
            // dataColumn10
            // 
            this.dataColumn10.AllowDBNull = false;
            this.dataColumn10.ColumnName = "loaihinh_Ma";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "loaihinh_Ten";
            // 
            // dtPTTT
            // 
            this.dtPTTT.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn12,
            this.dataColumn13});
            this.dtPTTT.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "pttt_Ma"}, true)});
            this.dtPTTT.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn12};
            this.dtPTTT.TableName = "PhuongThucThanhToan";
            // 
            // dataColumn12
            // 
            this.dataColumn12.AllowDBNull = false;
            this.dataColumn12.ColumnName = "pttt_Ma";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "pttt_GhiChu";
            // 
            // dtCuaKhau
            // 
            this.dtCuaKhau.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16});
            this.dtCuaKhau.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "cuakhau_Ma"}, true)});
            this.dtCuaKhau.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn14};
            this.dtCuaKhau.TableName = "CuaKhau";
            // 
            // dataColumn14
            // 
            this.dataColumn14.AllowDBNull = false;
            this.dataColumn14.ColumnName = "cuakhau_Ma";
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "cuakhau_Ten";
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "cuc_ma";
            // 
            // dtNguyenTe
            // 
            this.dtNguyenTe.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn17,
            this.dataColumn18});
            this.dtNguyenTe.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nguyente_Ma"}, true)});
            this.dtNguyenTe.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn17};
            this.dtNguyenTe.TableName = "NguyenTe";
            // 
            // dataColumn17
            // 
            this.dataColumn17.AllowDBNull = false;
            this.dataColumn17.ColumnName = "nguyente_Ma";
            // 
            // dataColumn18
            // 
            this.dataColumn18.ColumnName = "nguyente_Ten";
            // 
            // dtCompanyNuoc
            // 
            this.dtCompanyNuoc.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn19,
            this.dataColumn20});
            this.dtCompanyNuoc.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nuoc_Ma"}, true)});
            this.dtCompanyNuoc.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn19};
            this.dtCompanyNuoc.TableName = "CompanyNuoc";
            // 
            // dataColumn19
            // 
            this.dataColumn19.AllowDBNull = false;
            this.dataColumn19.ColumnName = "nuoc_Ma";
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "nuoc_Ten";
            // 
            // dtDonViHaiQuan
            // 
            this.dtDonViHaiQuan.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn21,
            this.dataColumn22});
            this.dtDonViHaiQuan.TableName = "DonViHaiQuan";
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "donvihaiquan_Ma";
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "donvihaiquan_Ten";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(754, 139);
            this.uiGroupBox2.TabIndex = 258;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.pnlToKhaiMauDich);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 37);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(778, 781);
            this.uiGroupBox1.TabIndex = 5;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(241)))), ((int)(((byte)(228)))));
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.uiPanelManager1.DefaultPanelSettings.CloseButtonVisible = false;
            this.uiPanelManager1.Tag = null;
            this.uiPanelManager1.VisualStyleManager = this.vsmMain;
            this.uiPanel0.Id = new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8");
            this.uiPanel0.Panels.Add(this.uiPanel1);
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, Janus.Windows.UI.Dock.PanelDockStyle.Bottom, true, new System.Drawing.Size(970, 182), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8"), new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), 174, true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, true, new System.Drawing.Point(22, 29), new System.Drawing.Size(56, 56), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.AutoHide = true;
            this.uiPanel0.CaptionFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiPanel0.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel0.FloatingLocation = new System.Drawing.Point(22, 29);
            this.uiPanel0.FloatingSize = new System.Drawing.Size(56, 56);
            this.uiPanel0.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiPanel0.Location = new System.Drawing.Point(3, 263);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.Size = new System.Drawing.Size(970, 182);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "Thông tin hàng hóa";
            // 
            // uiPanel1
            // 
            this.uiPanel1.CaptionVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 26);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(970, 156);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Panel 1";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.dgList);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 1);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(968, 154);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(968, 154);
            this.dgList.TabIndex = 183;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "attached.ico");
            this.ImageList1.Images.SetKeyName(5, "internet.ico");
            this.ImageList1.Images.SetKeyName(6, "explorerBarItem25.Icon.ico");
            this.ImageList1.Images.SetKeyName(7, "page_edit.png");
            this.ImageList1.Images.SetKeyName(8, "page_red.png");
            this.ImageList1.Images.SetKeyName(9, "printer.png");
            // 
            // rvTyGiaTT
            // 
            this.rvTyGiaTT.ControlToValidate = this.txtTyGiaTinhThue;
            this.rvTyGiaTT.ErrorMessage = "\"Tỷ giá VND\" không hợp lệ.";
            this.rvTyGiaTT.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTyGiaTT.Icon")));
            this.rvTyGiaTT.MaximumValue = "50000";
            this.rvTyGiaTT.MinimumValue = "1";
            this.rvTyGiaTT.Tag = "rvTyGiaTT";
            this.rvTyGiaTT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvTyGiaUSD
            // 
            this.rvTyGiaUSD.ControlToValidate = this.txtTyGiaUSD;
            this.rvTyGiaUSD.ErrorMessage = "\"Tỷ giá USD\" không hợp lệ.";
            this.rvTyGiaUSD.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTyGiaUSD.Icon")));
            this.rvTyGiaUSD.MaximumValue = "50000";
            this.rvTyGiaUSD.MinimumValue = "1";
            this.rvTyGiaUSD.Tag = "rvTyGiaUSD";
            this.rvTyGiaUSD.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbToolBar});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang,
            this.cmdSave,
            this.cmdSend,
            this.cmdPrint,
            this.XacNhan,
            this.NhanDuLieu,
            this.Huy,
            this.cmdThemMotHang,
            this.cmdThemNhieuHang,
            this.ToKhaiViet,
            this.InPhieuTN2,
            this.cmdReadExcel,
            this.cmdChungTuKem,
            this.cmdGiayPhep,
            this.cmdHopDongThuongMai,
            this.cmdHoaDonThuongMai,
            this.cmdDeNghiChuyenCuaKhau,
            this.cmdChungTuDangAnh,
            this.cmdTruyenDuLieuTuXa,
            this.cmdSuaToKhaiDaDuyet,
            this.cmdHuyToKhaiDaDuyet,
            this.cmdKetQuaXuLy,
            this.cmdInTKDTSuaDoiBoSung,
            this.cmdChungTuNo,
            this.cmdNPLCungUng,
            this.cmdIntokhaiTQ,
            this.cmdInToKhai196});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.ImageList1;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmbToolBar
            // 
            this.cmbToolBar.CommandManager = this.cmMain;
            this.cmbToolBar.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdSave1,
            this.Separator1,
            this.cmdChungTuKem1,
            this.cmdTruyenDuLieuTuXa1,
            this.Separator2,
            this.cmdPrint1,
            this.Separator4});
            this.cmbToolBar.FullRow = true;
            this.cmbToolBar.Key = "cmdToolBar";
            this.cmbToolBar.Location = new System.Drawing.Point(0, 0);
            this.cmbToolBar.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbToolBar.MergeRowOrder = 1;
            this.cmbToolBar.Name = "cmbToolBar";
            this.cmbToolBar.RowIndex = 0;
            this.cmbToolBar.Size = new System.Drawing.Size(784, 34);
            this.cmbToolBar.Text = "cmdToolBar";
            this.cmbToolBar.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmbToolBar.Wrappable = Janus.Windows.UI.InheritableBoolean.True;
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            this.cmdThemHang1.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.cmdThemHang1.Text = "&Thêm hàng";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            this.cmdSave1.Text = "&Lưu";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdChungTuKem1
            // 
            this.cmdChungTuKem1.Key = "cmdChungTuKem";
            this.cmdChungTuKem1.Name = "cmdChungTuKem1";
            // 
            // cmdTruyenDuLieuTuXa1
            // 
            this.cmdTruyenDuLieuTuXa1.Key = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa1.Name = "cmdTruyenDuLieuTuXa1";
            this.cmdTruyenDuLieuTuXa1.Text = "Thông quan điện tử";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            this.cmdPrint1.Text = "&In ";
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemMotHang1,
            this.cmdThemNhieuHang1,
            this.cmdReadExcel1});
            this.cmdThemHang.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThemHang.Icon")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdThemMotHang1
            // 
            this.cmdThemMotHang1.Key = "cmdThemMotHang";
            this.cmdThemMotHang1.Name = "cmdThemMotHang1";
            // 
            // cmdThemNhieuHang1
            // 
            this.cmdThemNhieuHang1.Key = "cmdThemNhieuHang";
            this.cmdThemNhieuHang1.Name = "cmdThemNhieuHang1";
            // 
            // cmdReadExcel1
            // 
            this.cmdReadExcel1.Key = "cmdReadExcel";
            this.cmdReadExcel1.Name = "cmdReadExcel1";
            // 
            // cmdSave
            // 
            this.cmdSave.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSave.Icon")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.MergeOrder = 3;
            this.cmdSave.MergeType = Janus.Windows.UI.CommandBars.CommandMergeType.MergeItems;
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSave.Text = "Lưu";
            // 
            // cmdSend
            // 
            this.cmdSend.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend.Icon")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlG;
            this.cmdSend.Text = "Khai báo";
            this.cmdSend.ToolTipText = "Khai báo (Ctrl + G)";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ToKhai1,
            this.InPhieuTN1,
            this.cmdInTKDTSuaDoiBoSung1,
            this.cmdIntokhaiTQ1,
            this.cmdInToKhai1961});
            this.cmdPrint.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdPrint.Icon")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "In ";
            // 
            // ToKhai1
            // 
            this.ToKhai1.ImageIndex = 9;
            this.ToKhai1.Key = "ToKhaiViet";
            this.ToKhai1.Name = "ToKhai1";
            // 
            // InPhieuTN1
            // 
            this.InPhieuTN1.ImageIndex = 9;
            this.InPhieuTN1.Key = "InPhieuTN";
            this.InPhieuTN1.Name = "InPhieuTN1";
            // 
            // cmdInTKDTSuaDoiBoSung1
            // 
            this.cmdInTKDTSuaDoiBoSung1.ImageIndex = 9;
            this.cmdInTKDTSuaDoiBoSung1.Key = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung1.Name = "cmdInTKDTSuaDoiBoSung1";
            // 
            // cmdIntokhaiTQ1
            // 
            this.cmdIntokhaiTQ1.ImageKey = "printer.png";
            this.cmdIntokhaiTQ1.Key = "cmdIntokhaiTQ";
            this.cmdIntokhaiTQ1.Name = "cmdIntokhaiTQ1";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận";
            // 
            // NhanDuLieu
            // 
            this.NhanDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("NhanDuLieu.Icon")));
            this.NhanDuLieu.Key = "NhanDuLieu";
            this.NhanDuLieu.Name = "NhanDuLieu";
            this.NhanDuLieu.Text = "Nhận dữ liệu";
            // 
            // Huy
            // 
            this.Huy.Icon = ((System.Drawing.Icon)(resources.GetObject("Huy.Icon")));
            this.Huy.Key = "Huy";
            this.Huy.Name = "Huy";
            this.Huy.Text = "Hủy khai báo";
            // 
            // cmdThemMotHang
            // 
            this.cmdThemMotHang.Key = "cmdThemMotHang";
            this.cmdThemMotHang.Name = "cmdThemMotHang";
            this.cmdThemMotHang.Text = "Thêm Hàng";
            // 
            // cmdThemNhieuHang
            // 
            this.cmdThemNhieuHang.Key = "cmdThemNhieuHang";
            this.cmdThemNhieuHang.Name = "cmdThemNhieuHang";
            this.cmdThemNhieuHang.Text = "Thêm nhiều hàng";
            // 
            // ToKhaiViet
            // 
            this.ToKhaiViet.Key = "ToKhaiViet";
            this.ToKhaiViet.Name = "ToKhaiViet";
            this.ToKhaiViet.Text = "Tờ khai";
            // 
            // InPhieuTN2
            // 
            this.InPhieuTN2.Key = "InPhieuTN";
            this.InPhieuTN2.Name = "InPhieuTN2";
            this.InPhieuTN2.Text = "Phiếu tiếp nhận";
            // 
            // cmdReadExcel
            // 
            this.cmdReadExcel.Key = "cmdReadExcel";
            this.cmdReadExcel.Name = "cmdReadExcel";
            this.cmdReadExcel.Text = "Thêm hàng từ Excel";
            // 
            // cmdChungTuKem
            // 
            this.cmdChungTuKem.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdGiayPhep1,
            this.cmdHopDongThuongMai1,
            this.cmdHoaDonThuongMai1,
            this.cmdDeNghiChuyenCuaKhau1,
            this.cmdChungTuDangAnh1,
            this.cmdChungTuNo1,
            this.cmdNPLCungUng1});
            this.cmdChungTuKem.ImageIndex = 4;
            this.cmdChungTuKem.Key = "cmdChungTuKem";
            this.cmdChungTuKem.Name = "cmdChungTuKem";
            this.cmdChungTuKem.Text = "Chứng từ đính kèm";
            // 
            // cmdGiayPhep1
            // 
            this.cmdGiayPhep1.Key = "cmdGiayPhep";
            this.cmdGiayPhep1.Name = "cmdGiayPhep1";
            // 
            // cmdHopDongThuongMai1
            // 
            this.cmdHopDongThuongMai1.Key = "cmdHopDongThuongMai";
            this.cmdHopDongThuongMai1.Name = "cmdHopDongThuongMai1";
            // 
            // cmdHoaDonThuongMai1
            // 
            this.cmdHoaDonThuongMai1.Key = "cmdHoaDonThuongMai";
            this.cmdHoaDonThuongMai1.Name = "cmdHoaDonThuongMai1";
            // 
            // cmdDeNghiChuyenCuaKhau1
            // 
            this.cmdDeNghiChuyenCuaKhau1.Key = "cmdDeNghiChuyenCuaKhau";
            this.cmdDeNghiChuyenCuaKhau1.Name = "cmdDeNghiChuyenCuaKhau1";
            // 
            // cmdChungTuDangAnh1
            // 
            this.cmdChungTuDangAnh1.Key = "cmdChungTuDangAnh";
            this.cmdChungTuDangAnh1.Name = "cmdChungTuDangAnh1";
            // 
            // cmdChungTuNo1
            // 
            this.cmdChungTuNo1.Key = "cmdChungTuNo";
            this.cmdChungTuNo1.Name = "cmdChungTuNo1";
            // 
            // cmdNPLCungUng1
            // 
            this.cmdNPLCungUng1.Key = "cmdNPLCungUng";
            this.cmdNPLCungUng1.Name = "cmdNPLCungUng1";
            // 
            // cmdGiayPhep
            // 
            this.cmdGiayPhep.ImageIndex = 0;
            this.cmdGiayPhep.Key = "cmdGiayPhep";
            this.cmdGiayPhep.Name = "cmdGiayPhep";
            this.cmdGiayPhep.Text = "Giấy phép";
            // 
            // cmdHopDongThuongMai
            // 
            this.cmdHopDongThuongMai.ImageIndex = 0;
            this.cmdHopDongThuongMai.Key = "cmdHopDongThuongMai";
            this.cmdHopDongThuongMai.Name = "cmdHopDongThuongMai";
            this.cmdHopDongThuongMai.Text = "Hợp đồng thương mại";
            // 
            // cmdHoaDonThuongMai
            // 
            this.cmdHoaDonThuongMai.ImageIndex = 0;
            this.cmdHoaDonThuongMai.Key = "cmdHoaDonThuongMai";
            this.cmdHoaDonThuongMai.Name = "cmdHoaDonThuongMai";
            this.cmdHoaDonThuongMai.Text = "Hóa đơn thương mại";
            // 
            // cmdDeNghiChuyenCuaKhau
            // 
            this.cmdDeNghiChuyenCuaKhau.ImageIndex = 0;
            this.cmdDeNghiChuyenCuaKhau.Key = "cmdDeNghiChuyenCuaKhau";
            this.cmdDeNghiChuyenCuaKhau.Name = "cmdDeNghiChuyenCuaKhau";
            this.cmdDeNghiChuyenCuaKhau.Text = "Đề nghị chuyển cửa khẩu";
            // 
            // cmdChungTuDangAnh
            // 
            this.cmdChungTuDangAnh.ImageIndex = 0;
            this.cmdChungTuDangAnh.Key = "cmdChungTuDangAnh";
            this.cmdChungTuDangAnh.Name = "cmdChungTuDangAnh";
            this.cmdChungTuDangAnh.Text = "Chứng từ dạng ảnh";
            // 
            // cmdTruyenDuLieuTuXa
            // 
            this.cmdTruyenDuLieuTuXa.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend2,
            this.NhanDuLieu2,
            this.Huy2,
            this.Separator5,
            this.cmdSuaToKhaiDaDuyet1,
            this.cmdHuyToKhaiDaDuyet1,
            this.Separator6,
            this.cmdKetQuaXuLy1});
            this.cmdTruyenDuLieuTuXa.ImageIndex = 5;
            this.cmdTruyenDuLieuTuXa.Key = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa.Name = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa.Text = "Truyền dữ liệu từ xa";
            // 
            // cmdSend2
            // 
            this.cmdSend2.Key = "cmdSend";
            this.cmdSend2.Name = "cmdSend2";
            // 
            // NhanDuLieu2
            // 
            this.NhanDuLieu2.Key = "NhanDuLieu";
            this.NhanDuLieu2.Name = "NhanDuLieu2";
            // 
            // Huy2
            // 
            this.Huy2.Key = "Huy";
            this.Huy2.Name = "Huy2";
            // 
            // Separator5
            // 
            this.Separator5.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator5.Key = "Separator";
            this.Separator5.Name = "Separator5";
            // 
            // cmdSuaToKhaiDaDuyet1
            // 
            this.cmdSuaToKhaiDaDuyet1.Key = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet1.Name = "cmdSuaToKhaiDaDuyet1";
            // 
            // cmdHuyToKhaiDaDuyet1
            // 
            this.cmdHuyToKhaiDaDuyet1.Key = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet1.Name = "cmdHuyToKhaiDaDuyet1";
            // 
            // Separator6
            // 
            this.Separator6.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator6.Key = "Separator";
            this.Separator6.Name = "Separator6";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // cmdSuaToKhaiDaDuyet
            // 
            this.cmdSuaToKhaiDaDuyet.ImageIndex = 7;
            this.cmdSuaToKhaiDaDuyet.Key = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet.Name = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet.Text = "Sửa tờ khai đã duyệt";
            // 
            // cmdHuyToKhaiDaDuyet
            // 
            this.cmdHuyToKhaiDaDuyet.ImageIndex = 8;
            this.cmdHuyToKhaiDaDuyet.Key = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet.Name = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet.Text = "Hủy tờ khai đã duyệt";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.ImageIndex = 6;
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "Kết quả xử lý";
            // 
            // cmdInTKDTSuaDoiBoSung
            // 
            this.cmdInTKDTSuaDoiBoSung.Key = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung.Name = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung.Text = "In Tờ khai sửa đổi";
            // 
            // cmdChungTuNo
            // 
            this.cmdChungTuNo.ImageIndex = 0;
            this.cmdChungTuNo.Key = "cmdChungTuNo";
            this.cmdChungTuNo.Name = "cmdChungTuNo";
            this.cmdChungTuNo.Text = "Chứng từ nợ";
            // 
            // cmdNPLCungUng
            // 
            this.cmdNPLCungUng.ImageIndex = 0;
            this.cmdNPLCungUng.Key = "cmdNPLCungUng";
            this.cmdNPLCungUng.Name = "cmdNPLCungUng";
            this.cmdNPLCungUng.Text = "Nguyên phụ liệu cung ứng";
            // 
            // cmdIntokhaiTQ
            // 
            this.cmdIntokhaiTQ.Key = "cmdIntokhaiTQ";
            this.cmdIntokhaiTQ.Name = "cmdIntokhaiTQ";
            this.cmdIntokhaiTQ.Text = "In tờ khai thông quan điện tử";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbToolBar});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.cmbToolBar);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(784, 34);
            // 
            // ilLarge
            // 
            this.ilLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLarge.ImageStream")));
            this.ilLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLarge.Images.SetKeyName(0, "");
            this.ilLarge.Images.SetKeyName(1, "");
            this.ilLarge.Images.SetKeyName(2, "");
            this.ilLarge.Images.SetKeyName(3, "");
            this.ilLarge.Images.SetKeyName(4, "Add.gif");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "Add.gif");
            // 
            // cmdInToKhai196
            // 
            this.cmdInToKhai196.ImageIndex = 9;
            this.cmdInToKhai196.Key = "cmdInToKhai196";
            this.cmdInToKhai196.Name = "cmdInToKhai196";
            this.cmdInToKhai196.Text = "In tờ khai thông quan điện tử (TT 196)";
            // 
            // cmdInToKhai1961
            // 
            this.cmdInToKhai1961.Key = "cmdInToKhai196";
            this.cmdInToKhai1961.Name = "cmdInToKhai1961";
            // 
            // ToKhaiGCChuyenTiepNhapForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(784, 839);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiGCChuyenTiepNhapForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tờ khai gia công chuyển tiếp";
            this.Load += new System.EventHandler(this.ToKhaiGCChuyenTiepNhapForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.pnlToKhaiMauDich.ResumeLayout(false);
            this.pnlToKhaiMauDich.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpLoaiHangHoa)).EndInit();
            this.grpLoaiHangHoa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).EndInit();
            this.uiGroupBox18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).EndInit();
            this.uiGroupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDongGiao)).EndInit();
            this.grbHopDongGiao.ResumeLayout(false);
            this.grbHopDongGiao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDongNhan)).EndInit();
            this.grbHopDongNhan.ResumeLayout(false);
            this.grbHopDongNhan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNuocXK)).EndInit();
            this.grbNuocXK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemDoHang)).EndInit();
            this.grbDiaDiemDoHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).EndInit();
            this.uiGroupBox12.ResumeLayout(false);
            this.uiGroupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiXK)).EndInit();
            this.grbNguoiXK.ResumeLayout(false);
            this.grbNguoiXK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNK)).EndInit();
            this.grbNguoiNK.ResumeLayout(false);
            this.grbNguoiNK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHangMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPTTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompanyNuoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaUSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion		        

        private ImageList ImageList1;
        private UICommand cmdThemHang;
        internal ImageList ilLarge;
        private ImageList imageList2;
        private UICommand cmdPrint;
        private GridEX dgList;
        private UIGroupBox uiGroupBox10;
        private Label label24;
        private Label label25;
        private Label lbltrangthai;
        private Label label29;
        private NumericEditBox txtsotiepnhan;
        private Label label31;
        private NumericEditBox txtToKhaiSo;
        private UICommand cmdSend;
        private UICommand cmdSave1;
        private UICommand cmdThemHang1;
        private UICommand cmdPrint1;
        private UICommand XacNhan;
        private EditBox txtSoHopDongNhan;
        private Label lblSoHDDV;
        private Label lblNgayHDDV;
        private Label lblNgayHHDV;
        private EditBox txtSoHongDonggiao;
        private UICommand Separator1;
        private UICommand NhanDuLieu;
        private UICommand Huy;
        private UICommand Separator2;
        private UICommand cmdThemMotHang1;
        private UICommand cmdThemNhieuHang1;
        private UICommand cmdThemMotHang;
        private UICommand cmdThemNhieuHang;
        private UICommand ToKhai1;
        private UICommand InPhieuTN1;
        private UICommand ToKhai;
        private UICommand InPhieuTN2;
        private UICommand cmdReadExcel1;
        private UICommand cmdReadExcel;
        private UICommand cmdChungTuKem;
        private UICommand cmdGiayPhep;
        private UICommand cmdHopDongThuongMai;
        private UICommand cmdHoaDonThuongMai;
        private UICommand cmdDeNghiChuyenCuaKhau;
        private UICommand cmdChungTuDangAnh;
        private UICommand cmdTruyenDuLieuTuXa;
        private UICommand cmdSuaToKhaiDaDuyet;
        private UICommand cmdHuyToKhaiDaDuyet;
        private UICommand cmdKetQuaXuLy;
        private UICommand cmdChungTuKem1;
        private UICommand cmdGiayPhep1;
        private UICommand cmdHopDongThuongMai1;
        private UICommand cmdHoaDonThuongMai1;
        private UICommand cmdDeNghiChuyenCuaKhau1;
        private UICommand cmdChungTuDangAnh1;
        private UICommand cmdSend2;
        private UICommand NhanDuLieu2;
        private UICommand Huy2;
        private UICommand Separator5;
        private UICommand cmdSuaToKhaiDaDuyet1;
        private UICommand cmdHuyToKhaiDaDuyet1;
        private UICommand Separator6;
        private UICommand cmdKetQuaXuLy1;
        private UICommand cmdTruyenDuLieuTuXa1;
        private UICommand Separator4;
        private UICommand cmdChungTuDangAnhBS1;
        private Label label21;
        private Label lblPhanLuong;
        private UICommand cmdInTKDTSuaDoiBoSung;
        private UICommand cmdInTKDTSuaDoiBoSung1;
        private UICommand ToKhaiViet;
        private UIGroupBox grbNguoiNK;
        private EditBox txtTenDonVi;
        private EditBox txtMaDonVi;
        private Company.Interface.Controls.LoaiHinhChuyenTiepControlV cbLoaiHinh;
        private UIGroupBox uiGroupBox3;
        private UIGroupBox grbNguoiXK;
        private EditBox txtTenDonViDoiTac;
        private EditBox txtDaiDienDN;
        private UIGroupBox uiGroupBox4;
        private EditBox txtTenDaiLy;
        private EditBox txtMaDaiLy;
        private Label label32;
        private Label label33;
        private UIGroupBox uiGroupBox5;
        private EditBox txtMaDonViUyThac;
        private Label label34;
        private Label label35;
        private EditBox txtTenDonViUyThac;
        private UIGroupBox uiGroupBox8;
        private NguyenTeControl ctrNguyenTe;
        private NumericEditBox txtTyGiaTinhThue;
        private Label label36;
        private Label label37;
        private NumericEditBox txtTyGiaUSD;
        private UIGroupBox grbDiaDiemDoHang;
        private CuaKhauControl ctrCuaKhau;
        private UIGroupBox grbNuocXK;
        private NuocControl ctrNuocXuatKhau;
        private UIGroupBox grbHopDongNhan;
        private Label label10;
        private NumericEditBox txtTrongLuongTinh;
        private NumericEditBox txtTrongLuong;
        private NumericEditBox txtSoKien;
        private Label label38;
        private Label label39;
        private UIGroupBox grbHopDongGiao;
        private CalendarCombo ccNgayHHHopDongGiao;
        private Label label16;
        private Label label20;
        private CalendarCombo ccNgayHopDongGiao;
        private UIGroupBox uiGroupBox18;
        private UIComboBox cbPTTT;
        private UIGroupBox uiGroupBox16;
        private UIComboBox cbDKGH;
        private UIGroupBox uiGroupBox6;
        private UIGroupBox uiGroupBox7;
        private UIGroupBox uiGroupBox12;
        private EditBox txtMaNguoiGiao;
        private Label label1;
        private EditBox txtTenNguoiGiao;
        private Label label3;
        private Label label2;
        private EditBox txtDiaDiemGiaohang;
        private Label label4;
        private CalendarCombo ccThoiGianGiaoHang;
        private Label label5;
        private UIGroupBox grpLoaiHangHoa;
        private UIRadioButton radHangMau;
        private UIRadioButton radTB;
        private UIRadioButton radNPL;
        private UIRadioButton radSP;
        private CalendarCombo ccNgayDangKy;
        private Label label6;
        private CalendarCombo ccNgayHHHopDongNhan;
        private CalendarCombo ccNgayHopDongNhan;
        private UICommand cmdChungTuNo1;
        private UICommand cmdChungTuNo;
        private UICommand cmdNPLCungUng1;
        private UICommand cmdNPLCungUng;
        private EditBox txtDeXuatKhac;
        private Label label7;
        private EditBox txtMaDonViDoiTac;
        private UICommand cmdIntokhaiTQ1;
        private UICommand cmdIntokhaiTQ;
        private UIGroupBox uiGroupBox9;
        private EditBox txtChucVu;
        private Label label8;
        private UIGroupBox uiGroupBox11;
        private EditBox txtLyDoSua;
        private Label label9;
        private Label label11;
        private UIButton btnNoiDungChinhSua;
        private UICommand cmdInToKhai1961;
        private UICommand cmdInToKhai196;
    }
}
