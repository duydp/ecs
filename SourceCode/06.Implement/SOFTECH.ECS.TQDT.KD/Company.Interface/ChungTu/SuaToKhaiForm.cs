﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KD.BLL.KDT;
using Janus.Windows.GridEX;
using Company.KD.BLL;

namespace Company.Interface
{
    public partial class SuaToKhaiForm : Company.Interface.BaseForm
    {
        public bool isKhaiBoSung = false;

        public ToKhaiMauDich TKMD;
        public SuaToKhaiForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

 
        private void btnGhi_Click(object sender, EventArgs e)
        {
            TKMD.DeXuatKhac = txtDeXuatKhac.Text.Trim();
            TKMD.LyDoSua = txtDiaChiNguoiNK.Text.Trim();          
                TKMD.Update();
            ShowMessage("Lưu thành công.", false);
        }

    
        private void CoForm_Load(object sender, EventArgs e)
        {
          //  txtDiaChiNguoiNK.Text = TKMD.LyDoSua;
           txtDeXuatKhac.Text = TKMD.DeXuatKhac ;
           txtDiaChiNguoiNK.Text = TKMD.LyDoSua;
           txtSoTiepNhan.Text = TKMD.SoTiepNhan +"";
           ccNgayTiepNhan.Text = TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
           
        }

   
      

        private void label8_Click(object sender, EventArgs e)
        {

        }


        private void btnXoa_Click(object sender, EventArgs e)
        {
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                if(TKMD.MaLoaiHinh.Contains("N"))
                {
                    bool st = TKMD.WSKhaiBaoToKhai(password);
                }
                else
                {
                    TKMD.WSKhaiBaoToKhai(password);
                }
                ShowMessageTQDT("Hệ thống thông quan điện tử đã nhận được thông tin nhưng chưa có thông tin phản hồi, bấm nút Lấy phản hồi để nhận thông tin phải hồi.", false);
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Thông báo từ hệ thống ECS !",ex.Message.ToString(),false );
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();

                TKMD.WSRequestSuaToKhai(password);
                if (TKMD.SoTiepNhan!= 0 && TKMD.LyDoSua!="")
                {
                    ShowMessageTQDT("Khai báo thông tin sửa tờ khai đã duyệt thành công !\r\n Số tiếp nhận :" + TKMD.SoTiepNhan.ToString() + "\r\n Năm tiếp nhận :" + TKMD.NgayTiepNhan.Year.ToString(), false);
                    txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                    ccNgayTiepNhan.Value = TKMD.NgayTiepNhan;
                }
                else
                {
                    ShowMessageTQDT("Thông báo từ hệ thống ECS", "Chưa có thông tin phản hồi từ hệ thống thông quan điện tử.", false);
                }
            }
            catch (Exception ex)
            {

            }
        }

       
  

       
       

    }
}

