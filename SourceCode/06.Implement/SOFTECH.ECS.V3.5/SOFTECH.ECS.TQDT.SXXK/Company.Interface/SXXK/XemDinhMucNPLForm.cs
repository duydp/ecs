﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.SXXK
{
    public partial class XemDinhMucNPLForm : Company.Interface.BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public string MaNPL = "";
        public XemDinhMucNPLForm()
        {
            InitializeComponent();
        }

        private void XemDinhMucNPLForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Text = "Xem định mức NPL '" + this.MaNPL + "'";
                this.dgList.Tables[0].Columns["LuongNPL"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                this.dgList.Tables[0].Columns["LuongSP"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                this.dgList.Tables[0].Columns["DinhMuc"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
                this.dgList.Tables[0].Columns["LuongNPL"].TotalFormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

                this.dgList.DataSource = this.HSTL.GetDanhSachNPLXuatTonByMaNPL(GlobalSettings.SoThapPhan.LuongNPL, this.MaNPL);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void toolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DinhMucNPL_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }
    }
}

