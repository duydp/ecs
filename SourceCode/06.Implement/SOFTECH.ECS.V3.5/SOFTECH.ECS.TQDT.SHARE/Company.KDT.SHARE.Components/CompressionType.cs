﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public enum CompressionType
    {
        GZip = 0,
        BZip2 = 1,
        Zip = 2
    }
}
