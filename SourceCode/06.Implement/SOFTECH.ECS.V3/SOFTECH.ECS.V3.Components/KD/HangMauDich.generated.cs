﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.KD
{
    public partial class HangMauDich
    {
        #region Properties.

        public long ID { set; get; }
        public long TKMD_ID { set; get; }
        public int SoThuTuHang { set; get; }
        public string MaHS { set; get; }
        public string MaPhu { set; get; }
        public string TenHang { set; get; }
        public string NuocXX_ID { set; get; }
        public string DVT_ID { set; get; }
        public decimal SoLuong { set; get; }
        public decimal TrongLuong { set; get; }
        public double DonGiaKB { set; get; }
        public double DonGiaTT { set; get; }
        public double TriGiaKB { set; get; }
        public double TriGiaTT { set; get; }
        public double TriGiaKB_VND { set; get; }
        public double ThueSuatXNK { set; get; }
        public double ThueSuatTTDB { set; get; }
        public double ThueSuatGTGT { set; get; }
        public double ThueXNK { set; get; }
        public double ThueTTDB { set; get; }
        public double ThueGTGT { set; get; }
        public double PhuThu { set; get; }
        public double TyLeThuKhac { set; get; }
        public double TriGiaThuKhac { set; get; }
        public byte MienThue { set; get; }
        public string Ma_HTS { set; get; }
        public string DVT_HTS { set; get; }
        public decimal SoLuong_HTS { set; get; }
        public bool FOC { set; get; }
        public bool ThueTuyetDoi { set; get; }
        public string ThueSuatXNKGiam { set; get; }
        public string ThueSuatTTDBGiam { set; get; }
        public string ThueSuatVATGiam { set; get; }
        public double DonGiaTuyetDoi { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<HangMauDich> ConvertToCollection(IDataReader reader)
        {
            IList<HangMauDich> collection = new List<HangMauDich>();
            while (reader.Read())
            {
                HangMauDich entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDouble(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDouble(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDouble(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDouble(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDouble(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDouble(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDouble(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDouble(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDouble(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDouble(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDouble(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) entity.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) entity.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) entity.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static HangMauDich Load(long id)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<HangMauDich> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<HangMauDich> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<HangMauDich> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<HangMauDich> SelectCollectionBy_TKMD_ID(long tKMD_ID)
        {
            IDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertHangMauDich(long tKMD_ID, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, decimal trongLuong, double donGiaKB, double donGiaTT, double triGiaKB, double triGiaTT, double triGiaKB_VND, double thueSuatXNK, double thueSuatTTDB, double thueSuatGTGT, double thueXNK, double thueTTDB, double thueGTGT, double phuThu, double tyLeThuKhac, double triGiaThuKhac, byte mienThue, string ma_HTS, string dVT_HTS, decimal soLuong_HTS, bool fOC, bool thueTuyetDoi, string thueSuatXNKGiam, string thueSuatTTDBGiam, string thueSuatVATGiam, double donGiaTuyetDoi)
        {
            HangMauDich entity = new HangMauDich();
            entity.TKMD_ID = tKMD_ID;
            entity.SoThuTuHang = soThuTuHang;
            entity.MaHS = maHS;
            entity.MaPhu = maPhu;
            entity.TenHang = tenHang;
            entity.NuocXX_ID = nuocXX_ID;
            entity.DVT_ID = dVT_ID;
            entity.SoLuong = soLuong;
            entity.TrongLuong = trongLuong;
            entity.DonGiaKB = donGiaKB;
            entity.DonGiaTT = donGiaTT;
            entity.TriGiaKB = triGiaKB;
            entity.TriGiaTT = triGiaTT;
            entity.TriGiaKB_VND = triGiaKB_VND;
            entity.ThueSuatXNK = thueSuatXNK;
            entity.ThueSuatTTDB = thueSuatTTDB;
            entity.ThueSuatGTGT = thueSuatGTGT;
            entity.ThueXNK = thueXNK;
            entity.ThueTTDB = thueTTDB;
            entity.ThueGTGT = thueGTGT;
            entity.PhuThu = phuThu;
            entity.TyLeThuKhac = tyLeThuKhac;
            entity.TriGiaThuKhac = triGiaThuKhac;
            entity.MienThue = mienThue;
            entity.Ma_HTS = ma_HTS;
            entity.DVT_HTS = dVT_HTS;
            entity.SoLuong_HTS = soLuong_HTS;
            entity.FOC = fOC;
            entity.ThueTuyetDoi = thueTuyetDoi;
            entity.ThueSuatXNKGiam = thueSuatXNKGiam;
            entity.ThueSuatTTDBGiam = thueSuatTTDBGiam;
            entity.ThueSuatVATGiam = thueSuatVATGiam;
            entity.DonGiaTuyetDoi = donGiaTuyetDoi;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Float, DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Float, TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Float, TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Float, ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Float, ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Float, ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Float, ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Float, PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Float, TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Float, TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
            db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
            db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
            db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
            db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
            db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
            db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, ThueSuatXNKGiam);
            db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, ThueSuatTTDBGiam);
            db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, ThueSuatVATGiam);
            db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<HangMauDich> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangMauDich item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateHangMauDich(long id, long tKMD_ID, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, decimal trongLuong, double donGiaKB, double donGiaTT, double triGiaKB, double triGiaTT, double triGiaKB_VND, double thueSuatXNK, double thueSuatTTDB, double thueSuatGTGT, double thueXNK, double thueTTDB, double thueGTGT, double phuThu, double tyLeThuKhac, double triGiaThuKhac, byte mienThue, string ma_HTS, string dVT_HTS, decimal soLuong_HTS, bool fOC, bool thueTuyetDoi, string thueSuatXNKGiam, string thueSuatTTDBGiam, string thueSuatVATGiam, double donGiaTuyetDoi)
        {
            HangMauDich entity = new HangMauDich();
            entity.ID = id;
            entity.TKMD_ID = tKMD_ID;
            entity.SoThuTuHang = soThuTuHang;
            entity.MaHS = maHS;
            entity.MaPhu = maPhu;
            entity.TenHang = tenHang;
            entity.NuocXX_ID = nuocXX_ID;
            entity.DVT_ID = dVT_ID;
            entity.SoLuong = soLuong;
            entity.TrongLuong = trongLuong;
            entity.DonGiaKB = donGiaKB;
            entity.DonGiaTT = donGiaTT;
            entity.TriGiaKB = triGiaKB;
            entity.TriGiaTT = triGiaTT;
            entity.TriGiaKB_VND = triGiaKB_VND;
            entity.ThueSuatXNK = thueSuatXNK;
            entity.ThueSuatTTDB = thueSuatTTDB;
            entity.ThueSuatGTGT = thueSuatGTGT;
            entity.ThueXNK = thueXNK;
            entity.ThueTTDB = thueTTDB;
            entity.ThueGTGT = thueGTGT;
            entity.PhuThu = phuThu;
            entity.TyLeThuKhac = tyLeThuKhac;
            entity.TriGiaThuKhac = triGiaThuKhac;
            entity.MienThue = mienThue;
            entity.Ma_HTS = ma_HTS;
            entity.DVT_HTS = dVT_HTS;
            entity.SoLuong_HTS = soLuong_HTS;
            entity.FOC = fOC;
            entity.ThueTuyetDoi = thueTuyetDoi;
            entity.ThueSuatXNKGiam = thueSuatXNKGiam;
            entity.ThueSuatTTDBGiam = thueSuatTTDBGiam;
            entity.ThueSuatVATGiam = thueSuatVATGiam;
            entity.DonGiaTuyetDoi = donGiaTuyetDoi;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_HangMauDich_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Float, DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Float, TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Float, TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Float, ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Float, ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Float, ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Float, ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Float, PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Float, TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Float, TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
            db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
            db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
            db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
            db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
            db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
            db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, ThueSuatXNKGiam);
            db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, ThueSuatTTDBGiam);
            db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, ThueSuatVATGiam);
            db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<HangMauDich> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangMauDich item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateHangMauDich(long id, long tKMD_ID, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, decimal trongLuong, double donGiaKB, double donGiaTT, double triGiaKB, double triGiaTT, double triGiaKB_VND, double thueSuatXNK, double thueSuatTTDB, double thueSuatGTGT, double thueXNK, double thueTTDB, double thueGTGT, double phuThu, double tyLeThuKhac, double triGiaThuKhac, byte mienThue, string ma_HTS, string dVT_HTS, decimal soLuong_HTS, bool fOC, bool thueTuyetDoi, string thueSuatXNKGiam, string thueSuatTTDBGiam, string thueSuatVATGiam, double donGiaTuyetDoi)
        {
            HangMauDich entity = new HangMauDich();
            entity.ID = id;
            entity.TKMD_ID = tKMD_ID;
            entity.SoThuTuHang = soThuTuHang;
            entity.MaHS = maHS;
            entity.MaPhu = maPhu;
            entity.TenHang = tenHang;
            entity.NuocXX_ID = nuocXX_ID;
            entity.DVT_ID = dVT_ID;
            entity.SoLuong = soLuong;
            entity.TrongLuong = trongLuong;
            entity.DonGiaKB = donGiaKB;
            entity.DonGiaTT = donGiaTT;
            entity.TriGiaKB = triGiaKB;
            entity.TriGiaTT = triGiaTT;
            entity.TriGiaKB_VND = triGiaKB_VND;
            entity.ThueSuatXNK = thueSuatXNK;
            entity.ThueSuatTTDB = thueSuatTTDB;
            entity.ThueSuatGTGT = thueSuatGTGT;
            entity.ThueXNK = thueXNK;
            entity.ThueTTDB = thueTTDB;
            entity.ThueGTGT = thueGTGT;
            entity.PhuThu = phuThu;
            entity.TyLeThuKhac = tyLeThuKhac;
            entity.TriGiaThuKhac = triGiaThuKhac;
            entity.MienThue = mienThue;
            entity.Ma_HTS = ma_HTS;
            entity.DVT_HTS = dVT_HTS;
            entity.SoLuong_HTS = soLuong_HTS;
            entity.FOC = fOC;
            entity.ThueTuyetDoi = thueTuyetDoi;
            entity.ThueSuatXNKGiam = thueSuatXNKGiam;
            entity.ThueSuatTTDBGiam = thueSuatTTDBGiam;
            entity.ThueSuatVATGiam = thueSuatVATGiam;
            entity.DonGiaTuyetDoi = donGiaTuyetDoi;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Float, DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Float, TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Float, TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Float, ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Float, ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Float, ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Float, ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Float, PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Float, TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Float, TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
            db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
            db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
            db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
            db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
            db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
            db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, ThueSuatXNKGiam);
            db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, ThueSuatTTDBGiam);
            db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, ThueSuatVATGiam);
            db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<HangMauDich> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangMauDich item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteHangMauDich(long id)
        {
            HangMauDich entity = new HangMauDich();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<HangMauDich> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangMauDich item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}