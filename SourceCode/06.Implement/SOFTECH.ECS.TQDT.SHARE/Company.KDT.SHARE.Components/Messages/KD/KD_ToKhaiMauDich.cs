﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class KD_ToKhaiMauDich : DeclarationBase
    {
        /// <summary>
        /// Người đại diện doanh nghiệp
        /// </summary>
        [XmlElement("RepresentativePerson")]
        public RepresentativePerson RepresentativePerson { set; get; }

        ///// <summary>
        ///// Đề xuất khác
        /////</summary>
        //[XmlElement("AdditionalInformation")]
        //public AdditionalInformation AdditionalInformation { set; get; }
        /// <summary>
        /// Thông tin hàng hóa
        /// </summary>
        [XmlElement("GoodsShipment")]
        public GoodsShipment GoodsShipment { get; set; }
        /// <summary>
        /// Chứng từ giấy phép XNK đi kèm
        /// </summary>
        [XmlArray("Licenses")]
        [XmlArrayItem("License")]
        public List<License> License { get; set; }
        /// <summary>
        /// Chứng từ hợp đồng khai kèm
        /// </summary>
        [XmlArray("ContractDocuments")]
        [XmlArrayItem("ContractDocument")]
        public List<ContractDocument> ContractDocument { get; set; }

        /// <summary>
        /// Chứng từ hóa đơn thương mại
        /// </summary>
        [XmlArray("CommercialInvoices")]
        [XmlArrayItem("CommercialInvoice")]
        public List<CommercialInvoice> CommercialInvoices { get; set; }
        /// <summary>
        /// ....
        /// </summary>
        [XmlArray("CertificateOfOrigins")]
        [XmlArrayItem("CertificateOfOrigin")]
        public List<CertificateOfOrigin> CertificateOfOrigin { get; set; }
        /// <summary>
        /// Chứng từ vận đơn đi kèm
        /// </summary>
        [XmlArray("BillOfLadings")]
        [XmlArrayItem("BillOfLading")]
        public List<BillOfLading> BillOfLadings { get; set; }
        /// <summary>
        /// Đơn xin chuyển cửa khẩu
        /// </summary>
        [XmlElement("CustomsOfficeChangedRequest")]
        public List<CustomsOfficeChangedRequest> CustomsOfficeChangedRequest { get; set; }
        /// <summary>
        /// Chứng từ đính kèm
        /// </summary>
        [XmlArray("AttachDocuments")]
        [XmlArrayItem("AttachDocumentItem")]
        public List<AttachDocumentItem> AttachDocumentItem { get; set; }
        /// <summary>
        /// Chứng từ nợ (Chưa có)
        /// </summary>
        [XmlArray("AdditionalDocuments")]
        [XmlArrayItem("AdditionalDocument")]
        public List<AdditionalDocument> AdditionalDocumentEx { get; set; }

    }
}
