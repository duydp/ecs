namespace Company.Interface.SXXK
{
    partial class CapNhatHoSoThanhLyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CapNhatHoSoThanhLyForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem13 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem14 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem15 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem16 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem17 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem18 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem19 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem20 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.donViHaiQuanNewControl1 = new Company.Interface.Controls.DonViHaiQuanNewControl();
            this.txtLanThanhLy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTrangThaiTK = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSoHSTL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ccNgayThanhLy = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.txtNgayTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.sendBK1 = new System.Windows.Forms.ToolStripMenuItem();
            this.huyBangKe1 = new System.Windows.Forms.ToolStripMenuItem();
            this.xacNhanThongTin1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.cbbBangKe = new Janus.Windows.EditControls.UIComboBox();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.Luu1 = new Janus.Windows.UI.CommandBars.UICommand("Luu");
            this.cmdThanhKhoan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThanhKhoan");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdTT79KCX1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTT79KCX");
            this.cmdCanDoiNX1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCanDoiNX");
            this.cmdChungTuThanhToan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuThanhToan");
            this.cmdChungTuTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuTT");
            this.cmdTQDT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTQDT");
            this.cmdFixAndRun1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFixAndRun");
            this.KhaiBao = new Janus.Windows.UI.CommandBars.UICommand("KhaiBao");
            this.NhanDuLieu = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.Huy = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.Luu = new Janus.Windows.UI.CommandBars.UICommand("Luu");
            this.cmdThanhLy = new Janus.Windows.UI.CommandBars.UICommand("ThanhLy");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdBKToKhaiNhap1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap");
            this.cmdBKToKhaiXuat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat");
            this.cmdBCNPLXuatNhapTon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon");
            this.cmdBCNPLXuatNhapTon_TongNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_TongNPL");
            this.cmdBCThueXNK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCThueXNK");
            this.cmdBCNPLXuatNhapTon = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon");
            this.cmdBCThueXNK = new Janus.Windows.UI.CommandBars.UICommand("cmdBCThueXNK");
            this.cmdBKToKhaiXuat = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat");
            this.cmdBKToKhaiNhap = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap");
            this.cmdDelete = new Janus.Windows.UI.CommandBars.UICommand("cmdDelete");
            this.cmdRollback = new Janus.Windows.UI.CommandBars.UICommand("cmdRollback");
            this.cmdClose = new Janus.Windows.UI.CommandBars.UICommand("cmdClose");
            this.cmdThanhKhoan = new Janus.Windows.UI.CommandBars.UICommand("cmdThanhKhoan");
            this.ThanhLy1 = new Janus.Windows.UI.CommandBars.UICommand("ThanhLy");
            this.cmdRollback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdRollback");
            this.cmdClose1 = new Janus.Windows.UI.CommandBars.UICommand("cmdClose");
            this.cmdKhaiDT = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiDT");
            this.KhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("KhaiBao");
            this.Huy1 = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.cmdGuiBK = new Janus.Windows.UI.CommandBars.UICommand("cmdGuiBK");
            this.cmdHuyBK = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyBK");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.cmdCanDoiNX = new Janus.Windows.UI.CommandBars.UICommand("cmdCanDoiNX");
            this.cmdChungTuThanhToan = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuThanhToan");
            this.cmdXuatBaoCao = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatBaoCao");
            this.cmdBKTKNKNL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKTKNKNL");
            this.cmdBKTKXKSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKTKXKSP");
            this.cmdBCNLSXXK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNLSXXK");
            this.cmdBCXNTNL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCXNTNL");
            this.cmdBCTTNLNK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCTTNLNK");
            this.cmdBKTKNKNL = new Janus.Windows.UI.CommandBars.UICommand("cmdBKTKNKNL");
            this.cmdBKTKXKSP = new Janus.Windows.UI.CommandBars.UICommand("cmdBKTKXKSP");
            this.cmdBCNLSXXK = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNLSXXK");
            this.cmdBCXNTNL = new Janus.Windows.UI.CommandBars.UICommand("cmdBCXNTNL");
            this.cmdBCTTNLNK = new Janus.Windows.UI.CommandBars.UICommand("cmdBCTTNLNK");
            this.cmdFixAndRun = new Janus.Windows.UI.CommandBars.UICommand("cmdFixAndRun");
            this.cmdTT79KCX = new Janus.Windows.UI.CommandBars.UICommand("cmdTT79KCX");
            this.cmdBKToKhaiNhap_KCX1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap_KCX");
            this.cmdBKToKhaiXuat_KCX1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat_KCX");
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX_TongNPL");
            this.cmdBCNPLXuatNhapTon_KCX1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX");
            this.cmdTQDT = new Janus.Windows.UI.CommandBars.UICommand("cmdTQDT");
            this.KhaiBao2 = new Janus.Windows.UI.CommandBars.UICommand("KhaiBao");
            this.NhanDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.Huy2 = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdChungTuTT = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuTT");
            this.cmdBKToKhaiNhap_KCX = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiNhap_KCX");
            this.cmdBKToKhaiXuat_KCX = new Janus.Windows.UI.CommandBars.UICommand("cmdBKToKhaiXuat_KCX");
            this.cmdBCNPLXuatNhapTon_KCX = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX");
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_KCX_TongNPL");
            this.cmdBCNPLXuatNhapTon_TongNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdBCNPLXuatNhapTon_TongNPL");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.label6 = new System.Windows.Forms.Label();
            this.btnXoaBK = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnXoaBK);
            this.grbMain.Controls.Add(this.label6);
            this.grbMain.Controls.Add(this.uiButton2);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.label4);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(0, 68);
            this.grbMain.Size = new System.Drawing.Size(922, 379);
            this.grbMain.Click += new System.EventHandler(this.grbMain_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.donViHaiQuanNewControl1);
            this.uiGroupBox1.Controls.Add(this.txtLanThanhLy);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.lblTrangThaiTK);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.txtSoHSTL);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.ccNgayThanhLy);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(480, 104);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông tin hồ sơ thanh khoản";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // donViHaiQuanNewControl1
            // 
            this.donViHaiQuanNewControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanNewControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanNewControl1.Location = new System.Drawing.Point(130, 20);
            this.donViHaiQuanNewControl1.Ma = "";
            this.donViHaiQuanNewControl1.MaCuc = "";
            this.donViHaiQuanNewControl1.Name = "donViHaiQuanNewControl1";
            this.donViHaiQuanNewControl1.ReadOnly = true;
            this.donViHaiQuanNewControl1.Size = new System.Drawing.Size(340, 22);
            this.donViHaiQuanNewControl1.TabIndex = 1;
            this.donViHaiQuanNewControl1.Ten = "";
            this.donViHaiQuanNewControl1.VisualStyleManager = null;
            // 
            // txtLanThanhLy
            // 
            this.txtLanThanhLy.Location = new System.Drawing.Point(130, 74);
            this.txtLanThanhLy.MaxLength = 5;
            this.txtLanThanhLy.Name = "txtLanThanhLy";
            this.txtLanThanhLy.ReadOnly = true;
            this.txtLanThanhLy.Size = new System.Drawing.Size(50, 21);
            this.txtLanThanhLy.TabIndex = 7;
            this.txtLanThanhLy.Text = "0";
            this.txtLanThanhLy.Value = ((long)(0));
            this.txtLanThanhLy.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int64;
            this.txtLanThanhLy.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Lần thanh lý";
            // 
            // lblTrangThaiTK
            // 
            this.lblTrangThaiTK.AutoSize = true;
            this.lblTrangThaiTK.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThaiTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThaiTK.ForeColor = System.Drawing.Color.Blue;
            this.lblTrangThaiTK.Location = new System.Drawing.Point(333, 79);
            this.lblTrangThaiTK.Name = "lblTrangThaiTK";
            this.lblTrangThaiTK.Size = new System.Drawing.Size(91, 13);
            this.lblTrangThaiTK.TabIndex = 9;
            this.lblTrangThaiTK.Text = "Đang nhập liệu";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(182, 79);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Trạng thái thanh khoản";
            // 
            // txtSoHSTL
            // 
            this.txtSoHSTL.Location = new System.Drawing.Point(130, 47);
            this.txtSoHSTL.MaxLength = 5;
            this.txtSoHSTL.Name = "txtSoHSTL";
            this.txtSoHSTL.Size = new System.Drawing.Size(50, 21);
            this.txtSoHSTL.TabIndex = 3;
            this.txtSoHSTL.Text = "0";
            this.txtSoHSTL.Value = ((long)(0));
            this.txtSoHSTL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int64;
            this.txtSoHSTL.VisualStyleManager = this.vsmMain;
            this.txtSoHSTL.Click += new System.EventHandler(this.txtSoHSTL_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Số hồ sơ ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã hải quan";
            // 
            // ccNgayThanhLy
            // 
            this.ccNgayThanhLy.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccNgayThanhLy.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayThanhLy.DropDownCalendar.Name = "";
            this.ccNgayThanhLy.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayThanhLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayThanhLy.Location = new System.Drawing.Point(336, 47);
            this.ccNgayThanhLy.Name = "ccNgayThanhLy";
            this.ccNgayThanhLy.Nullable = true;
            this.ccNgayThanhLy.NullButtonText = "Xóa";
            this.ccNgayThanhLy.ReadOnly = true;
            this.ccNgayThanhLy.ShowNullButton = true;
            this.ccNgayThanhLy.Size = new System.Drawing.Size(88, 21);
            this.ccNgayThanhLy.TabIndex = 5;
            this.ccNgayThanhLy.TodayButtonText = "Hôm nay";
            this.ccNgayThanhLy.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayThanhLy.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(182, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ngày bắt đầu";
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(110, 52);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(162, 13);
            this.lblTrangThai.TabIndex = 274;
            this.lblTrangThai.Text = "Chưa gửi hồ sơ lên Hải quan";
            // 
            // txtNgayTiepNhan
            // 
            this.txtNgayTiepNhan.Location = new System.Drawing.Point(304, 20);
            this.txtNgayTiepNhan.Name = "txtNgayTiepNhan";
            this.txtNgayTiepNhan.Size = new System.Drawing.Size(95, 21);
            this.txtNgayTiepNhan.TabIndex = 272;
            this.txtNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Location = new System.Drawing.Point(112, 21);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(82, 21);
            this.txtSoTiepNhan.TabIndex = 270;
            this.txtSoTiepNhan.Text = "0";
            this.txtSoTiepNhan.Value = ((long)(0));
            this.txtSoTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int64;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.dgList);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 219);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(898, 122);
            this.uiGroupBox2.TabIndex = 3;
            this.uiGroupBox2.UseCompatibleTextRendering = true;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.AutomaticSort = false;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            this.dgList.ContextMenuStrip = this.contextMenuStrip1;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(2, 8);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.Size = new System.Drawing.Size(895, 112);
            this.dgList.TabIndex = 0;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.dgList_DeletingRecord);
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Enabled = false;
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sendBK1,
            this.huyBangKe1,
            this.xacNhanThongTin1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(176, 70);
            // 
            // sendBK1
            // 
            this.sendBK1.Enabled = false;
            this.sendBK1.Name = "sendBK1";
            this.sendBK1.Size = new System.Drawing.Size(175, 22);
            this.sendBK1.Text = "Gửi bảng kê";
            this.sendBK1.Visible = false;
            this.sendBK1.Click += new System.EventHandler(this.sendBK1_Click);
            // 
            // huyBangKe1
            // 
            this.huyBangKe1.Enabled = false;
            this.huyBangKe1.Name = "huyBangKe1";
            this.huyBangKe1.Size = new System.Drawing.Size(175, 22);
            this.huyBangKe1.Text = "Hủy bảng kê";
            this.huyBangKe1.Visible = false;
            this.huyBangKe1.Click += new System.EventHandler(this.huyBangKe1_Click);
            // 
            // xacNhanThongTin1
            // 
            this.xacNhanThongTin1.Enabled = false;
            this.xacNhanThongTin1.Image = ((System.Drawing.Image)(resources.GetObject("xacNhanThongTin1.Image")));
            this.xacNhanThongTin1.Name = "xacNhanThongTin1";
            this.xacNhanThongTin1.Size = new System.Drawing.Size(175, 22);
            this.xacNhanThongTin1.Text = "Xác nhận thông tin";
            this.xacNhanThongTin1.Visible = false;
            this.xacNhanThongTin1.Click += new System.EventHandler(this.xacNhanThongTin1_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "GiayPhep1.Icon.ico");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(9, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(317, 14);
            this.label4.TabIndex = 2;
            this.label4.Text = "Danh sách bảng kê đã khai của hồ sơ thanh khoản";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(844, 350);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(66, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label9);
            this.uiGroupBox3.Controls.Add(this.uiButton1);
            this.uiGroupBox3.Controls.Add(this.cbbBangKe);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(12, 122);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(896, 65);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.Text = "Thêm mới bảng kê";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(38, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Bảng kê chưa có";
            // 
            // uiButton1
            // 
            this.uiButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(738, 26);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(94, 23);
            this.uiButton1.TabIndex = 2;
            this.uiButton1.Text = "Thêm mới";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // cbbBangKe
            // 
            this.cbbBangKe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbBangKe.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbBangKe.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "BK01 - Bảng kê tờ khai nhập khẩu chưa thanh lý";
            uiComboBoxItem11.Value = "DTLTKN";
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem12.IsSeparator = false;
            uiComboBoxItem12.Text = "BK02 - Bảng kê tờ khai xuất khẩu chưa thanh lý";
            uiComboBoxItem12.Value = "DTLTKX";
            uiComboBoxItem13.FormatStyle.Alpha = 0;
            uiComboBoxItem13.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem13.IsSeparator = false;
            uiComboBoxItem13.Text = "BK03 - Bảng kê nguyên phụ liệu chưa thanh lý";
            uiComboBoxItem13.Value = "DTLNPLCHUATL";
            uiComboBoxItem14.FormatStyle.Alpha = 0;
            uiComboBoxItem14.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem14.IsSeparator = false;
            uiComboBoxItem14.Text = "BK04 - Bảng kê NPL hủy, biếu, tặng";
            uiComboBoxItem14.Value = "DTLNPLXH";
            uiComboBoxItem15.FormatStyle.Alpha = 0;
            uiComboBoxItem15.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem15.IsSeparator = false;
            uiComboBoxItem15.Text = "BK05 - Bảng kê NPL tái xuất";
            uiComboBoxItem15.Value = "DTLNPLTX";
            uiComboBoxItem16.FormatStyle.Alpha = 0;
            uiComboBoxItem16.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem16.IsSeparator = false;
            uiComboBoxItem16.Text = "BK06 - Bảng kê NPL xuất sử dụng tờ khai NKD";
            uiComboBoxItem16.Value = "DTLNPLNKD";
            uiComboBoxItem17.FormatStyle.Alpha = 0;
            uiComboBoxItem17.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem17.IsSeparator = false;
            uiComboBoxItem17.Text = "BK07 - Bảng kê nguyên phụ liệu nộp thuế tiêu thụ nội địa";
            uiComboBoxItem17.Value = "DTLNPLNT";
            uiComboBoxItem18.FormatStyle.Alpha = 0;
            uiComboBoxItem18.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem18.IsSeparator = false;
            uiComboBoxItem18.Text = "BK08 - Bảng kê nguyên phụ liệu xuất theo loại hình XGC";
            uiComboBoxItem18.Value = "DTLNPLXGC";
            uiComboBoxItem19.FormatStyle.Alpha = 0;
            uiComboBoxItem19.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem19.IsSeparator = false;
            uiComboBoxItem19.Text = "BK09 - Bảng kê nguyên phụ liệu tạm nộp thuế";
            uiComboBoxItem19.Value = "DTLCHITIETNT";
            uiComboBoxItem20.FormatStyle.Alpha = 0;
            uiComboBoxItem20.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem20.IsSeparator = false;
            uiComboBoxItem20.Text = "BK10 - Bảng kê nguyên phụ liệu tự cung ứng";
            uiComboBoxItem20.Value = "DTLNPLTCU";
            this.cbbBangKe.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem11,
            uiComboBoxItem12,
            uiComboBoxItem13,
            uiComboBoxItem14,
            uiComboBoxItem15,
            uiComboBoxItem16,
            uiComboBoxItem17,
            uiComboBoxItem18,
            uiComboBoxItem19,
            uiComboBoxItem20});
            this.cbbBangKe.Location = new System.Drawing.Point(150, 28);
            this.cbbBangKe.Name = "cbbBangKe";
            this.cbbBangKe.SelectedItemFormatStyle.BackColor = System.Drawing.Color.Silver;
            this.cbbBangKe.SelectedItemFormatStyle.BackColorGradient = System.Drawing.Color.Lavender;
            this.cbbBangKe.SelectedItemFormatStyle.BackgroundGradientMode = Janus.Windows.UI.BackgroundGradientMode.Horizontal;
            this.cbbBangKe.Size = new System.Drawing.Size(582, 21);
            this.cbbBangKe.TabIndex = 1;
            this.cbbBangKe.VisualStyleManager = this.vsmMain;
            // 
            // cmMain
            // 
            this.cmMain.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowMerge = false;
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.KhaiBao,
            this.NhanDuLieu,
            this.Huy,
            this.Luu,
            this.cmdThanhLy,
            this.cmdPrint,
            this.cmdBCNPLXuatNhapTon,
            this.cmdBCThueXNK,
            this.cmdBKToKhaiXuat,
            this.cmdBKToKhaiNhap,
            this.cmdDelete,
            this.cmdRollback,
            this.cmdClose,
            this.cmdThanhKhoan,
            this.cmdKhaiDT,
            this.cmdGuiBK,
            this.cmdHuyBK,
            this.XacNhan,
            this.cmdCanDoiNX,
            this.cmdChungTuThanhToan,
            this.cmdXuatBaoCao,
            this.cmdBKTKNKNL,
            this.cmdBKTKXKSP,
            this.cmdBCNLSXXK,
            this.cmdBCXNTNL,
            this.cmdBCTTNLNK,
            this.cmdFixAndRun,
            this.cmdTT79KCX,
            this.cmdTQDT,
            this.cmdKetQuaXuLy,
            this.cmdChungTuTT,
            this.cmdBKToKhaiNhap_KCX,
            this.cmdBKToKhaiXuat_KCX,
            this.cmdBCNPLXuatNhapTon_KCX,
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL,
            this.cmdBCNPLXuatNhapTon_TongNPL});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.KeepMergeSettings = false;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.LockCommandBars = true;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.ShowShortcutInToolTips = true;
            this.cmMain.ShowToolTipOnMenus = true;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.Luu1,
            this.cmdThanhKhoan1,
            this.cmdPrint1,
            this.cmdTT79KCX1,
            this.cmdCanDoiNX1,
            this.cmdChungTuThanhToan1,
            this.cmdChungTuTT1,
            this.cmdTQDT1,
            this.cmdFixAndRun1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.ImageList = this.ImageList1;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(922, 68);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.Wrappable = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.uiCommandBar1_CommandClick);
            // 
            // Luu1
            // 
            this.Luu1.Icon = ((System.Drawing.Icon)(resources.GetObject("Luu1.Icon")));
            this.Luu1.Key = "Luu";
            this.Luu1.Name = "Luu1";
            this.Luu1.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.Luu1.Text = "Lưu hồ sơ";
            // 
            // cmdThanhKhoan1
            // 
            this.cmdThanhKhoan1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThanhKhoan1.Icon")));
            this.cmdThanhKhoan1.Key = "cmdThanhKhoan";
            this.cmdThanhKhoan1.Name = "cmdThanhKhoan1";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            this.cmdPrint1.Text = "Báo cáo TT194";
            // 
            // cmdTT79KCX1
            // 
            this.cmdTT79KCX1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdTT79KCX1.Icon")));
            this.cmdTT79KCX1.Key = "cmdTT79KCX";
            this.cmdTT79KCX1.Name = "cmdTT79KCX1";
            this.cmdTT79KCX1.Text = "Báo cáo TT194 - KCX";
            // 
            // cmdCanDoiNX1
            // 
            this.cmdCanDoiNX1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCanDoiNX1.Icon")));
            this.cmdCanDoiNX1.Key = "cmdCanDoiNX";
            this.cmdCanDoiNX1.Name = "cmdCanDoiNX1";
            // 
            // cmdChungTuThanhToan1
            // 
            this.cmdChungTuThanhToan1.Key = "cmdChungTuThanhToan";
            this.cmdChungTuThanhToan1.Name = "cmdChungTuThanhToan1";
            // 
            // cmdChungTuTT1
            // 
            this.cmdChungTuTT1.Key = "cmdChungTuTT";
            this.cmdChungTuTT1.Name = "cmdChungTuTT1";
            // 
            // cmdTQDT1
            // 
            this.cmdTQDT1.Key = "cmdTQDT";
            this.cmdTQDT1.Name = "cmdTQDT1";
            // 
            // cmdFixAndRun1
            // 
            this.cmdFixAndRun1.Key = "cmdFixAndRun";
            this.cmdFixAndRun1.Name = "cmdFixAndRun1";
            // 
            // KhaiBao
            // 
            this.KhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("KhaiBao.Icon")));
            this.KhaiBao.Key = "KhaiBao";
            this.KhaiBao.Name = "KhaiBao";
            this.KhaiBao.Text = "Khai báo";
            // 
            // NhanDuLieu
            // 
            this.NhanDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("NhanDuLieu.Icon")));
            this.NhanDuLieu.Key = "NhanDuLieu";
            this.NhanDuLieu.Name = "NhanDuLieu";
            this.NhanDuLieu.Text = "Nhận dữ liệu";
            // 
            // Huy
            // 
            this.Huy.Icon = ((System.Drawing.Icon)(resources.GetObject("Huy.Icon")));
            this.Huy.Key = "Huy";
            this.Huy.Name = "Huy";
            this.Huy.Text = "Hủy";
            // 
            // Luu
            // 
            this.Luu.Key = "Luu";
            this.Luu.Name = "Luu";
            this.Luu.Text = "Lưu";
            // 
            // cmdThanhLy
            // 
            this.cmdThanhLy.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThanhLy.Icon")));
            this.cmdThanhLy.Key = "ThanhLy";
            this.cmdThanhLy.Name = "cmdThanhLy";
            this.cmdThanhLy.Text = "Thanh lý";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdBKToKhaiNhap1,
            this.cmdBKToKhaiXuat1,
            this.cmdBCNPLXuatNhapTon1,
            this.cmdBCNPLXuatNhapTon_TongNPL1,
            this.cmdBCThueXNK1});
            this.cmdPrint.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdPrint.Icon")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "Báo cáo TT194";
            // 
            // cmdBKToKhaiNhap1
            // 
            this.cmdBKToKhaiNhap1.Key = "cmdBKToKhaiNhap";
            this.cmdBKToKhaiNhap1.Name = "cmdBKToKhaiNhap1";
            this.cmdBKToKhaiNhap1.Shortcut = System.Windows.Forms.Shortcut.Ctrl1;
            // 
            // cmdBKToKhaiXuat1
            // 
            this.cmdBKToKhaiXuat1.Key = "cmdBKToKhaiXuat";
            this.cmdBKToKhaiXuat1.Name = "cmdBKToKhaiXuat1";
            this.cmdBKToKhaiXuat1.Shortcut = System.Windows.Forms.Shortcut.Ctrl2;
            // 
            // cmdBCNPLXuatNhapTon1
            // 
            this.cmdBCNPLXuatNhapTon1.Key = "cmdBCNPLXuatNhapTon";
            this.cmdBCNPLXuatNhapTon1.Name = "cmdBCNPLXuatNhapTon1";
            this.cmdBCNPLXuatNhapTon1.Shortcut = System.Windows.Forms.Shortcut.Ctrl3;
            // 
            // cmdBCNPLXuatNhapTon_TongNPL1
            // 
            this.cmdBCNPLXuatNhapTon_TongNPL1.Key = "cmdBCNPLXuatNhapTon_TongNPL";
            this.cmdBCNPLXuatNhapTon_TongNPL1.Name = "cmdBCNPLXuatNhapTon_TongNPL1";
            // 
            // cmdBCThueXNK1
            // 
            this.cmdBCThueXNK1.Key = "cmdBCThueXNK";
            this.cmdBCThueXNK1.Name = "cmdBCThueXNK1";
            this.cmdBCThueXNK1.Shortcut = System.Windows.Forms.Shortcut.Ctrl4;
            // 
            // cmdBCNPLXuatNhapTon
            // 
            this.cmdBCNPLXuatNhapTon.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBCNPLXuatNhapTon.Icon")));
            this.cmdBCNPLXuatNhapTon.Key = "cmdBCNPLXuatNhapTon";
            this.cmdBCNPLXuatNhapTon.Name = "cmdBCNPLXuatNhapTon";
            this.cmdBCNPLXuatNhapTon.Text = "BC03 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn";
            // 
            // cmdBCThueXNK
            // 
            this.cmdBCThueXNK.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBCThueXNK.Icon")));
            this.cmdBCThueXNK.Key = "cmdBCThueXNK";
            this.cmdBCThueXNK.Name = "cmdBCThueXNK";
            this.cmdBCThueXNK.Text = "BC04 - Báo cáo tính thuế trên NPL nhập khẩu";
            // 
            // cmdBKToKhaiXuat
            // 
            this.cmdBKToKhaiXuat.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBKToKhaiXuat.Icon")));
            this.cmdBKToKhaiXuat.Key = "cmdBKToKhaiXuat";
            this.cmdBKToKhaiXuat.Name = "cmdBKToKhaiXuat";
            this.cmdBKToKhaiXuat.Text = "BC02 - Bảng kê tờ khai xuất khẩu đưa vào thanh khoản";
            // 
            // cmdBKToKhaiNhap
            // 
            this.cmdBKToKhaiNhap.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBKToKhaiNhap.Icon")));
            this.cmdBKToKhaiNhap.Key = "cmdBKToKhaiNhap";
            this.cmdBKToKhaiNhap.Name = "cmdBKToKhaiNhap";
            this.cmdBKToKhaiNhap.Text = "BC01 - Bảng kê tờ khai nhập khẩu đưa vào thanh khoản";
            // 
            // cmdDelete
            // 
            this.cmdDelete.Key = "cmdDelete";
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Shortcut = System.Windows.Forms.Shortcut.CtrlDel;
            this.cmdDelete.Text = "Xóa";
            // 
            // cmdRollback
            // 
            this.cmdRollback.Key = "cmdRollback";
            this.cmdRollback.Name = "cmdRollback";
            this.cmdRollback.Shortcut = System.Windows.Forms.Shortcut.CtrlR;
            this.cmdRollback.Text = "Rollback";
            // 
            // cmdClose
            // 
            this.cmdClose.Key = "cmdClose";
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.cmdClose.Text = "Đóng hồ sơ";
            // 
            // cmdThanhKhoan
            // 
            this.cmdThanhKhoan.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ThanhLy1,
            this.cmdRollback1,
            this.cmdClose1});
            this.cmdThanhKhoan.Key = "cmdThanhKhoan";
            this.cmdThanhKhoan.Name = "cmdThanhKhoan";
            this.cmdThanhKhoan.Text = "Thanh khoản";
            // 
            // ThanhLy1
            // 
            this.ThanhLy1.Key = "ThanhLy";
            this.ThanhLy1.Name = "ThanhLy1";
            this.ThanhLy1.Shortcut = System.Windows.Forms.Shortcut.CtrlE;
            this.ThanhLy1.Text = "Chạy thanh khoản";
            // 
            // cmdRollback1
            // 
            this.cmdRollback1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdRollback1.Icon")));
            this.cmdRollback1.Key = "cmdRollback";
            this.cmdRollback1.Name = "cmdRollback1";
            // 
            // cmdClose1
            // 
            this.cmdClose1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdClose1.Icon")));
            this.cmdClose1.Key = "cmdClose";
            this.cmdClose1.Name = "cmdClose1";
            // 
            // cmdKhaiDT
            // 
            this.cmdKhaiDT.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.KhaiBao1,
            this.Huy1});
            this.cmdKhaiDT.Key = "cmdKhaiDT";
            this.cmdKhaiDT.Name = "cmdKhaiDT";
            this.cmdKhaiDT.Text = "Khai điện tử";
            // 
            // KhaiBao1
            // 
            this.KhaiBao1.Icon = ((System.Drawing.Icon)(resources.GetObject("KhaiBao1.Icon")));
            this.KhaiBao1.Key = "KhaiBao";
            this.KhaiBao1.Name = "KhaiBao1";
            this.KhaiBao1.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.KhaiBao1.Text = "Đăng ký số HS";
            // 
            // Huy1
            // 
            this.Huy1.Icon = ((System.Drawing.Icon)(resources.GetObject("Huy1.Icon")));
            this.Huy1.Key = "Huy";
            this.Huy1.Name = "Huy1";
            this.Huy1.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
            this.Huy1.Text = "Hủy số HS";
            // 
            // cmdGuiBK
            // 
            this.cmdGuiBK.Key = "cmdGuiBK";
            this.cmdGuiBK.Name = "cmdGuiBK";
            this.cmdGuiBK.Text = "Gửi bảng kê";
            // 
            // cmdHuyBK
            // 
            this.cmdHuyBK.Key = "cmdHuyBK";
            this.cmdHuyBK.Name = "cmdHuyBK";
            this.cmdHuyBK.Text = "Huỷ bảng kê";
            // 
            // XacNhan
            // 
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận thông tin";
            // 
            // cmdCanDoiNX
            // 
            this.cmdCanDoiNX.Key = "cmdCanDoiNX";
            this.cmdCanDoiNX.Name = "cmdCanDoiNX";
            this.cmdCanDoiNX.Shortcut = System.Windows.Forms.Shortcut.CtrlV;
            this.cmdCanDoiNX.Text = "Xem cân đối nhập xuất";
            // 
            // cmdChungTuThanhToan
            // 
            this.cmdChungTuThanhToan.Image = ((System.Drawing.Image)(resources.GetObject("cmdChungTuThanhToan.Image")));
            this.cmdChungTuThanhToan.Key = "cmdChungTuThanhToan";
            this.cmdChungTuThanhToan.Name = "cmdChungTuThanhToan";
            this.cmdChungTuThanhToan.Text = "Bảng CTTT";
            // 
            // cmdXuatBaoCao
            // 
            this.cmdXuatBaoCao.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdBKTKNKNL1,
            this.cmdBKTKXKSP1,
            this.cmdBCNLSXXK1,
            this.cmdBCXNTNL1,
            this.cmdBCTTNLNK1});
            this.cmdXuatBaoCao.Key = "cmdXuatBaoCao";
            this.cmdXuatBaoCao.Name = "cmdXuatBaoCao";
            // 
            // cmdBKTKNKNL1
            // 
            this.cmdBKTKNKNL1.Key = "cmdBKTKNKNL";
            this.cmdBKTKNKNL1.Name = "cmdBKTKNKNL1";
            // 
            // cmdBKTKXKSP1
            // 
            this.cmdBKTKXKSP1.Key = "cmdBKTKXKSP";
            this.cmdBKTKXKSP1.Name = "cmdBKTKXKSP1";
            // 
            // cmdBCNLSXXK1
            // 
            this.cmdBCNLSXXK1.Key = "cmdBCNLSXXK";
            this.cmdBCNLSXXK1.Name = "cmdBCNLSXXK1";
            // 
            // cmdBCXNTNL1
            // 
            this.cmdBCXNTNL1.Key = "cmdBCXNTNL";
            this.cmdBCXNTNL1.Name = "cmdBCXNTNL1";
            // 
            // cmdBCTTNLNK1
            // 
            this.cmdBCTTNLNK1.Key = "cmdBCTTNLNK";
            this.cmdBCTTNLNK1.Name = "cmdBCTTNLNK1";
            // 
            // cmdBKTKNKNL
            // 
            this.cmdBKTKNKNL.Key = "cmdBKTKNKNL";
            this.cmdBKTKNKNL.Name = "cmdBKTKNKNL";
            this.cmdBKTKNKNL.Text = "BC 01 - Bảng kê tờ khai nhập khẩu nguyên liệu đưa vào thanh khoản";
            // 
            // cmdBKTKXKSP
            // 
            this.cmdBKTKXKSP.Key = "cmdBKTKXKSP";
            this.cmdBKTKXKSP.Name = "cmdBKTKXKSP";
            this.cmdBKTKXKSP.Text = "BC 02- Bảng kê tờ khai xuất khẩu sản phẩm đưa vào thanh khoản";
            // 
            // cmdBCNLSXXK
            // 
            this.cmdBCNLSXXK.Key = "cmdBCNLSXXK";
            this.cmdBCNLSXXK.Name = "cmdBCNLSXXK";
            this.cmdBCNLSXXK.Text = "BC 03 - Báo cáo nguyên liệu sản xuất hàng xuất khẩu";
            // 
            // cmdBCXNTNL
            // 
            this.cmdBCXNTNL.Key = "cmdBCXNTNL";
            this.cmdBCXNTNL.Name = "cmdBCXNTNL";
            this.cmdBCXNTNL.Text = "BC 04 - Báo cáo xuất nhập tồn nguyên liệu nhập khẩu";
            // 
            // cmdBCTTNLNK
            // 
            this.cmdBCTTNLNK.Key = "cmdBCTTNLNK";
            this.cmdBCTTNLNK.Name = "cmdBCTTNLNK";
            this.cmdBCTTNLNK.Text = "BC 05 - Báo cáo tính thuế nguyên liệu nhập khẩu";
            // 
            // cmdFixAndRun
            // 
            this.cmdFixAndRun.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            this.cmdFixAndRun.Key = "cmdFixAndRun";
            this.cmdFixAndRun.Name = "cmdFixAndRun";
            this.cmdFixAndRun.Text = "Fix du lieu va chay thanh khoan";
            this.cmdFixAndRun.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdTT79KCX
            // 
            this.cmdTT79KCX.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdBKToKhaiNhap_KCX1,
            this.cmdBKToKhaiXuat_KCX1,
            this.cmdBCNPLXuatNhapTon_KCX1,
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL1});
            this.cmdTT79KCX.Key = "cmdTT79KCX";
            this.cmdTT79KCX.Name = "cmdTT79KCX";
            this.cmdTT79KCX.Text = "Báo cáo TT194 - KCX";
            // 
            // cmdBKToKhaiNhap_KCX1
            // 
            this.cmdBKToKhaiNhap_KCX1.Key = "cmdBKToKhaiNhap_KCX";
            this.cmdBKToKhaiNhap_KCX1.Name = "cmdBKToKhaiNhap_KCX1";
            // 
            // cmdBKToKhaiXuat_KCX1
            // 
            this.cmdBKToKhaiXuat_KCX1.Key = "cmdBKToKhaiXuat_KCX";
            this.cmdBKToKhaiXuat_KCX1.Name = "cmdBKToKhaiXuat_KCX1";
            // 
            // cmdBCNPLXuatNhapTon_KCX_TongNPL1
            // 
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL1.Key = "cmdBCNPLXuatNhapTon_KCX_TongNPL";
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL1.Name = "cmdBCNPLXuatNhapTon_KCX_TongNPL1";
            // 
            // cmdBCNPLXuatNhapTon_KCX1
            // 
            this.cmdBCNPLXuatNhapTon_KCX1.Key = "cmdBCNPLXuatNhapTon_KCX";
            this.cmdBCNPLXuatNhapTon_KCX1.Name = "cmdBCNPLXuatNhapTon_KCX1";
            // 
            // cmdTQDT
            // 
            this.cmdTQDT.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.KhaiBao2,
            this.NhanDuLieu1,
            this.Huy2,
            this.cmdKetQuaXuLy1});
            this.cmdTQDT.Image = ((System.Drawing.Image)(resources.GetObject("cmdTQDT.Image")));
            this.cmdTQDT.Key = "cmdTQDT";
            this.cmdTQDT.Name = "cmdTQDT";
            this.cmdTQDT.Text = "Thông quan điện tử";
            // 
            // KhaiBao2
            // 
            this.KhaiBao2.Key = "KhaiBao";
            this.KhaiBao2.Name = "KhaiBao2";
            // 
            // NhanDuLieu1
            // 
            this.NhanDuLieu1.Key = "NhanDuLieu";
            this.NhanDuLieu1.Name = "NhanDuLieu1";
            // 
            // Huy2
            // 
            this.Huy2.Key = "Huy";
            this.Huy2.Name = "Huy2";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "Kết quả xử lý";
            // 
            // cmdChungTuTT
            // 
            this.cmdChungTuTT.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdChungTuTT.Icon")));
            this.cmdChungTuTT.Key = "cmdChungTuTT";
            this.cmdChungTuTT.Name = "cmdChungTuTT";
            this.cmdChungTuTT.Text = "Chứng từ thanh toán";
            // 
            // cmdBKToKhaiNhap_KCX
            // 
            this.cmdBKToKhaiNhap_KCX.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBKToKhaiNhap_KCX.Icon")));
            this.cmdBKToKhaiNhap_KCX.Key = "cmdBKToKhaiNhap_KCX";
            this.cmdBKToKhaiNhap_KCX.Name = "cmdBKToKhaiNhap_KCX";
            this.cmdBKToKhaiNhap_KCX.Text = "BC01 - Bảng kê tờ khai nhập khẩu đưa vào thanh khoản (KCX)";
            // 
            // cmdBKToKhaiXuat_KCX
            // 
            this.cmdBKToKhaiXuat_KCX.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBKToKhaiXuat_KCX.Icon")));
            this.cmdBKToKhaiXuat_KCX.Key = "cmdBKToKhaiXuat_KCX";
            this.cmdBKToKhaiXuat_KCX.Name = "cmdBKToKhaiXuat_KCX";
            this.cmdBKToKhaiXuat_KCX.Text = "BC02 - Bảng kê tờ khai xuất khẩu đưa vào thanh khoản (KCX)";
            // 
            // cmdBCNPLXuatNhapTon_KCX
            // 
            this.cmdBCNPLXuatNhapTon_KCX.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBCNPLXuatNhapTon_KCX.Icon")));
            this.cmdBCNPLXuatNhapTon_KCX.Key = "cmdBCNPLXuatNhapTon_KCX";
            this.cmdBCNPLXuatNhapTon_KCX.Name = "cmdBCNPLXuatNhapTon_KCX";
            this.cmdBCNPLXuatNhapTon_KCX.Text = "BC03 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn (KCX)";
            // 
            // cmdBCNPLXuatNhapTon_KCX_TongNPL
            // 
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBCNPLXuatNhapTon_KCX_TongNPL.Icon")));
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL.Key = "cmdBCNPLXuatNhapTon_KCX_TongNPL";
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL.Name = "cmdBCNPLXuatNhapTon_KCX_TongNPL";
            this.cmdBCNPLXuatNhapTon_KCX_TongNPL.Text = "BC03.1 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn - Tổng NPL (KCX)";
            // 
            // cmdBCNPLXuatNhapTon_TongNPL
            // 
            this.cmdBCNPLXuatNhapTon_TongNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBCNPLXuatNhapTon_TongNPL.Icon")));
            this.cmdBCNPLXuatNhapTon_TongNPL.Key = "cmdBCNPLXuatNhapTon_TongNPL";
            this.cmdBCNPLXuatNhapTon_TongNPL.Name = "cmdBCNPLXuatNhapTon_TongNPL";
            this.cmdBCNPLXuatNhapTon_TongNPL.Text = "BC03.1 - Báo cáo Nguyên phụ liệu Xuất Nhập Tồn - Tổng NPL";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(922, 68);
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(756, 350);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(83, 23);
            this.uiButton2.TabIndex = 6;
            this.uiButton2.Text = "Xóa hồ sơ";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            this.uiButton2.Click += new System.EventHandler(this.uiButton2_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(12, 355);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(204, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Hướng dẫn: Kích đôi để xem chi tiết";
            // 
            // btnXoaBK
            // 
            this.btnXoaBK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoaBK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaBK.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoaBK.Icon")));
            this.btnXoaBK.Location = new System.Drawing.Point(650, 350);
            this.btnXoaBK.Name = "btnXoaBK";
            this.btnXoaBK.Size = new System.Drawing.Size(101, 23);
            this.btnXoaBK.TabIndex = 5;
            this.btnXoaBK.Text = "Xóa bảng kê";
            this.btnXoaBK.VisualStyleManager = this.vsmMain;
            this.btnXoaBK.Click += new System.EventHandler(this.btnXoaBK_Click);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.label15);
            this.uiGroupBox4.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox4.Controls.Add(this.txtNgayTiepNhan);
            this.uiGroupBox4.Controls.Add(this.lblTrangThai);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(498, 12);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(410, 104);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Thông tin khai báo";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(16, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Trạng thái";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(16, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Số tiếp nhận";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(207, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Ngày tiếp nhận";
            // 
            // CapNhatHoSoThanhLyForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(922, 447);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "CapNhatHoSoThanhLyForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cập nhật hồ sơ thanh khoản";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CapNhatHoSoThanhLyForm_Load);
            this.Activated += new System.EventHandler(this.CapNhatHoSoThanhLyForm_Activated);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoHSTL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayThanhLy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgList;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayTiepNhan;
        private System.Windows.Forms.Label lblTrangThai;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIComboBox cbbBangKe;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand KhaiBao;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand Luu1;
        private Janus.Windows.UI.CommandBars.UICommand NhanDuLieu;
        private Janus.Windows.UI.CommandBars.UICommand Huy;
        private Janus.Windows.UI.CommandBars.UICommand Luu;
        private Janus.Windows.UI.CommandBars.UICommand cmdThanhLy;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCThueXNK1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCThueXNK;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.UI.CommandBars.UICommand cmdDelete;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblTrangThaiTK;
        private Janus.Windows.UI.CommandBars.UICommand cmdRollback;
        private Janus.Windows.UI.CommandBars.UICommand cmdClose;
        private Janus.Windows.UI.CommandBars.UICommand cmdThanhKhoan1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThanhKhoan;
        private Janus.Windows.UI.CommandBars.UICommand ThanhLy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdClose1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiDT;
        private Janus.Windows.UI.CommandBars.UICommand KhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand Huy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdGuiBK;
        private Janus.Windows.UI.CommandBars.UICommand cmdHuyBK;
        private Janus.Windows.UI.CommandBars.UICommand cmdRollback1;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sendBK1;
        private System.Windows.Forms.ToolStripMenuItem huyBangKe1;
        private System.Windows.Forms.ToolStripMenuItem xacNhanThongTin1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCanDoiNX1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCanDoiNX;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuThanhToan;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLanThanhLy;
        private System.Windows.Forms.Label label5;
        private Company.Interface.Controls.DonViHaiQuanNewControl donViHaiQuanNewControl1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatBaoCao;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKTKNKNL1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKTKXKSP1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNLSXXK1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCXNTNL1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCTTNLNK1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKTKNKNL;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKTKXKSP;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNLSXXK;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCXNTNL;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCTTNLNK;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIButton btnXoaBK;
        private Janus.Windows.UI.CommandBars.UICommand cmdFixAndRun;
        private Janus.Windows.UI.CommandBars.UICommand cmdTT79KCX1;
        private Janus.Windows.UI.CommandBars.UICommand cmdTT79KCX;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.UI.CommandBars.UICommand cmdTQDT1;
        private Janus.Windows.UI.CommandBars.UICommand cmdTQDT;
        private Janus.Windows.UI.CommandBars.UICommand KhaiBao2;
        private Janus.Windows.UI.CommandBars.UICommand NhanDuLieu1;
        private Janus.Windows.UI.CommandBars.UICommand Huy2;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy;
        private Janus.Windows.UI.CommandBars.UICommand cmdFixAndRun1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuThanhToan1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuTT1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuTT;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap_KCX;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat_KCX;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiNhap_KCX1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBKToKhaiXuat_KCX1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX_TongNPL;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_TongNPL1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_KCX_TongNPL1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBCNPLXuatNhapTon_TongNPL;
    }
}