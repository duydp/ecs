
/****** Object:  Table [dbo].[t_KDT_BaoLanhThue]    Script Date: 05/31/2013 17:20:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_BaoLanhThue]') AND type in (N'U'))
DROP TABLE [dbo].[t_KDT_BaoLanhThue]
GO


/****** Object:  Table [dbo].[t_KDT_BaoLanhThue]    Script Date: 05/31/2013 17:20:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_BaoLanhThue](
	[ID] [bigint] NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[DonViBaoLanh] [nvarchar](15) NULL,
	[SoGiayBaoLanh] [nvarchar](50) NULL,
	[NamChungTuBaoLanh] [int] NULL,
	[NgayHieuLuc] [datetime] NULL,
	[LoaiBaoLanh] [varchar](10) NULL,
	[SoTienBaoLanh] [decimal](18, 4) NULL,
	[SoDuTienBaoLanh] [decimal](18, 4) NULL,
	[SoNgayDuocBaoLanh] [int] NULL,
	[NgayHetHieuLuc] [datetime] NULL,
	[NgayKyBaoLanh] [datetime] NULL,
 CONSTRAINT [PK_t_KDT_BaoLanhThue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_BaoLanhThue_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_BaoLanhThue_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_BaoLanhThue_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_BaoLanhThue_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_BaoLanhThue_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_BaoLanhThue_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_BaoLanhThue_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_BaoLanhThue_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_BaoLanhThue_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_BaoLanhThue_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_BaoLanhThue_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_BaoLanhThue_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_BaoLanhThue_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_BaoLanhThue_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_BaoLanhThue_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_BaoLanhThue_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_BaoLanhThue_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 31, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_BaoLanhThue_Insert]
	@ID bigint,
	@TKMD_ID bigint,
	@DonViBaoLanh nvarchar(15),
	@SoGiayBaoLanh nvarchar(50),
	@NamChungTuBaoLanh int,
	@NgayHieuLuc datetime,
	@LoaiBaoLanh varchar(10),
	@SoTienBaoLanh decimal(18, 4),
	@SoDuTienBaoLanh decimal(18, 4),
	@SoNgayDuocBaoLanh int,
	@NgayHetHieuLuc datetime,
	@NgayKyBaoLanh datetime
AS
INSERT INTO [dbo].[t_KDT_BaoLanhThue]
(
	[ID],
	[TKMD_ID],
	[DonViBaoLanh],
	[SoGiayBaoLanh],
	[NamChungTuBaoLanh],
	[NgayHieuLuc],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoDuTienBaoLanh],
	[SoNgayDuocBaoLanh],
	[NgayHetHieuLuc],
	[NgayKyBaoLanh]
)
VALUES
(
	@ID,
	@TKMD_ID,
	@DonViBaoLanh,
	@SoGiayBaoLanh,
	@NamChungTuBaoLanh,
	@NgayHieuLuc,
	@LoaiBaoLanh,
	@SoTienBaoLanh,
	@SoDuTienBaoLanh,
	@SoNgayDuocBaoLanh,
	@NgayHetHieuLuc,
	@NgayKyBaoLanh
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_BaoLanhThue_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 31, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_BaoLanhThue_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@DonViBaoLanh nvarchar(15),
	@SoGiayBaoLanh nvarchar(50),
	@NamChungTuBaoLanh int,
	@NgayHieuLuc datetime,
	@LoaiBaoLanh varchar(10),
	@SoTienBaoLanh decimal(18, 4),
	@SoDuTienBaoLanh decimal(18, 4),
	@SoNgayDuocBaoLanh int,
	@NgayHetHieuLuc datetime,
	@NgayKyBaoLanh datetime
AS

UPDATE
	[dbo].[t_KDT_BaoLanhThue]
SET
	[TKMD_ID] = @TKMD_ID,
	[DonViBaoLanh] = @DonViBaoLanh,
	[SoGiayBaoLanh] = @SoGiayBaoLanh,
	[NamChungTuBaoLanh] = @NamChungTuBaoLanh,
	[NgayHieuLuc] = @NgayHieuLuc,
	[LoaiBaoLanh] = @LoaiBaoLanh,
	[SoTienBaoLanh] = @SoTienBaoLanh,
	[SoDuTienBaoLanh] = @SoDuTienBaoLanh,
	[SoNgayDuocBaoLanh] = @SoNgayDuocBaoLanh,
	[NgayHetHieuLuc] = @NgayHetHieuLuc,
	[NgayKyBaoLanh] = @NgayKyBaoLanh
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_BaoLanhThue_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 31, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_BaoLanhThue_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@DonViBaoLanh nvarchar(15),
	@SoGiayBaoLanh nvarchar(50),
	@NamChungTuBaoLanh int,
	@NgayHieuLuc datetime,
	@LoaiBaoLanh varchar(10),
	@SoTienBaoLanh decimal(18, 4),
	@SoDuTienBaoLanh decimal(18, 4),
	@SoNgayDuocBaoLanh int,
	@NgayHetHieuLuc datetime,
	@NgayKyBaoLanh datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_BaoLanhThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_BaoLanhThue] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[DonViBaoLanh] = @DonViBaoLanh,
			[SoGiayBaoLanh] = @SoGiayBaoLanh,
			[NamChungTuBaoLanh] = @NamChungTuBaoLanh,
			[NgayHieuLuc] = @NgayHieuLuc,
			[LoaiBaoLanh] = @LoaiBaoLanh,
			[SoTienBaoLanh] = @SoTienBaoLanh,
			[SoDuTienBaoLanh] = @SoDuTienBaoLanh,
			[SoNgayDuocBaoLanh] = @SoNgayDuocBaoLanh,
			[NgayHetHieuLuc] = @NgayHetHieuLuc,
			[NgayKyBaoLanh] = @NgayKyBaoLanh
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_BaoLanhThue]
	(
			[ID],
			[TKMD_ID],
			[DonViBaoLanh],
			[SoGiayBaoLanh],
			[NamChungTuBaoLanh],
			[NgayHieuLuc],
			[LoaiBaoLanh],
			[SoTienBaoLanh],
			[SoDuTienBaoLanh],
			[SoNgayDuocBaoLanh],
			[NgayHetHieuLuc],
			[NgayKyBaoLanh]
	)
	VALUES
	(
			@ID,
			@TKMD_ID,
			@DonViBaoLanh,
			@SoGiayBaoLanh,
			@NamChungTuBaoLanh,
			@NgayHieuLuc,
			@LoaiBaoLanh,
			@SoTienBaoLanh,
			@SoDuTienBaoLanh,
			@SoNgayDuocBaoLanh,
			@NgayHetHieuLuc,
			@NgayKyBaoLanh
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_BaoLanhThue_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 31, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_BaoLanhThue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_BaoLanhThue]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_BaoLanhThue_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 31, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_BaoLanhThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_BaoLanhThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_BaoLanhThue_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 31, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_BaoLanhThue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[DonViBaoLanh],
	[SoGiayBaoLanh],
	[NamChungTuBaoLanh],
	[NgayHieuLuc],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoDuTienBaoLanh],
	[SoNgayDuocBaoLanh],
	[NgayHetHieuLuc],
	[NgayKyBaoLanh]
FROM
	[dbo].[t_KDT_BaoLanhThue]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_BaoLanhThue_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 31, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_BaoLanhThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[DonViBaoLanh],
	[SoGiayBaoLanh],
	[NamChungTuBaoLanh],
	[NgayHieuLuc],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoDuTienBaoLanh],
	[SoNgayDuocBaoLanh],
	[NgayHetHieuLuc],
	[NgayKyBaoLanh]
FROM [dbo].[t_KDT_BaoLanhThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_BaoLanhThue_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 31, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_BaoLanhThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[DonViBaoLanh],
	[SoGiayBaoLanh],
	[NamChungTuBaoLanh],
	[NgayHieuLuc],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoDuTienBaoLanh],
	[SoNgayDuocBaoLanh],
	[NgayHetHieuLuc],
	[NgayKyBaoLanh]
FROM
	[dbo].[t_KDT_BaoLanhThue]	
GO

DELETE  [dbo].[t_HaiQuan_BieuThue]
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Add 30 rows to [dbo].[t_HaiQuan_BieuThue]
SET IDENTITY_INSERT [dbo].[t_HaiQuan_BieuThue] ON
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (10, N'', N'', N'')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (15, N'B01', N'B01 - Biểu thuế nhập khẩu ưu đãi', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (16, N'B02', N'B02 - Biểu thuế nhập khẩu ưu đãi - Chương 98', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (17, N'B03', N'B03 - Biểu thuế nhập khẩu thông thường (bằng 150% thuế suất MFN)', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (18, N'B04', N'B04 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại hàng hóa ASEAN (ATIGA)', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (19, N'B05', N'B05 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Thương mại Tự do ASEAN - Trung Quốc', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (20, N'B06', N'B06 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Thương mại Tự do ASEAN - Hàn Quốc', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (21, N'B07', N'B07 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Thương mại Tự dơ ASEAN - úc - NiuDi -lân', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (22, N'B08', N'B08 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại hàng hóa ASEAN-ấn Độ', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (23, N'B09', N'B09 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định đối tác kinh tế toàn diện ASEAN-Nhật Bản', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (24, N'B10', N'B10 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định đối tác kinh tế Việt Nam-Nhật Bản', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (25, N'B11', N'B11 - Biểu thuế nhập khẩu đối với các mặt hàng được áp dụng ưu đãi thuế suất thuế nhập khẩu Việt - Lào', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (26, N'B12', N'B12 - Biểu thuế nhập khẩu ưu đãi đặc biệt đối với hàng hóa nhập khẩu có xuất xứ từ Vương Quốc Campuchia', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (27, N'B13', N'B13 - Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định thương mại Tự do Việt Nam - Chi Lê', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (28, N'B14', N'B14 - Biểu thuế NK ngoài hạn ngạch', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (29, N'B15', N'B15 - Biểu thuế nhập khẩu tuyệt đối', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (30, N'B16', N'B16 - Biểu thuế nhập khẩu hỗn hợp', N'NTD')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (31, N'B21', N'B21 - Biểu thuế VAT 0%', N'VAT')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (32, N'B22', N'B22 - Biểu thuế VAT 5%', N'VAT')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (33, N'B23', N'B23 - Biểu thuế VAT 10%', N'VAT')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (34, N'B24', N'B24 - Biểu thuế VAT 15%', N'VAT')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (35, N'B31', N'B31 - Biểu thuế xuất khẩu (B31)', N'X')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (36, N'B32', N'B32 - Biểu thuế xuất khẩu (B32)', N'X')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (37, N'B33', N'B33 - Biểu thuế xuất khẩu (B33)', N'X')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (38, N'B34', N'B34 - Biểu thuế xuất khẩu (B34)', N'X')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (39, N'B41', N'B41 - Biểu thuế TTĐB', N'TTDB')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (40, N'B51', N'B51 - Biểu thuế môi trường', N'MT')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (41, N'B61', N'B61 - Biểu thuế dành cho loại hình gia công, chế xuất với thuế suất 0%', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (42, N'B99', N'B99 - Biểu thuế khác, dành cho hàng miễn thuế', N'N')
INSERT INTO [dbo].[t_HaiQuan_BieuThue] ([ID], [MaBieuThue], [TenBieuThue], [MoTaKhac]) VALUES (44, N'B71', N'B71 - Biểu thuế tự vệ chống bán phá giá', N'CBPG')

SET IDENTITY_INSERT [dbo].[t_HaiQuan_BieuThue] OFF
COMMIT TRANSACTION
GO


UPDATE dbo.t_HaiQuan_Version SET [Version] = '8.5', [Date] = GETDATE(), Notes = N'Cập nhật biểu thuế / Cập nhật bảo lãnh thuế'