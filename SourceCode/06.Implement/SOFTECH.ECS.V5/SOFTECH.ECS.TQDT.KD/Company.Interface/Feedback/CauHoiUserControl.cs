﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.Feedback.Components;

namespace Company.Interface.Feedback
{
    public partial class CauHoiUserControl : UserControl
    {
        private DataTable dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["ThuVienCauHoi"];

        public CauHoiUserControl()
        {
            InitializeComponent();
        }

        public void DataBinding(string linhVuc)
        {
            try
            {
                dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["ThuVienCauHoi"];

                if (dt == null)
                    return;

                DataView dv = dt.Copy().DefaultView;
                dv.RowFilter = "HienThi = 1 and LinhVuc = '" + linhVuc + "'";
                dv.Sort = "NgayTao asc";

                dgList.DataSource = dv;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); throw ex; }
        }

        public DataRow CurrentRow()
        {
            Janus.Windows.GridEX.GridEXRow[] rows = dgList.GetRows();

            foreach (Janus.Windows.GridEX.GridEXRow item in rows)
            {
                if (item.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    if (item.Selected)
                    {
                        return item.DataRow.GetType() == typeof(System.Data.DataRowView) ? ((System.Data.DataRowView)item.DataRow).Row : (System.Data.DataRow)item.DataRow;
                    }
                }
            }

            return null;
        }

        public delegate void CauHoiSelectionChangedEventHandler(object sender, EventArgs e);
        public event CauHoiSelectionChangedEventHandler CauHoiSelectionChanged;
        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            if (CauHoiSelectionChanged != null)
            {
                CauHoiSelectionChanged(sender, e);
            }
        }

        private void CauHoiUserControl_Load(object sender, EventArgs e)
        {

        }
    }
}
