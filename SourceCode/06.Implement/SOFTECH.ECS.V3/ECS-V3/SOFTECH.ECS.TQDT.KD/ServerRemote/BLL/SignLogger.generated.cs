using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace ServerRemote.BLL
{
	public partial class SignLogger
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MSG_FROM { set; get; }
		public string MSG_TO { set; get; }
		public DateTime TIME_PROCESS { set; get; }
		public string MESSAGE { set; get; }
		public DateTime DATECREATE { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static SignLogger Load(long id)
		{
			const string spName = "[dbo].[p_SignLogger_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			SignLogger entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new SignLogger();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
				if (!reader.IsDBNull(reader.GetOrdinal("TIME_PROCESS"))) entity.TIME_PROCESS = reader.GetDateTime(reader.GetOrdinal("TIME_PROCESS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MESSAGE"))) entity.MESSAGE = reader.GetString(reader.GetOrdinal("MESSAGE"));
				if (!reader.IsDBNull(reader.GetOrdinal("DATECREATE"))) entity.DATECREATE = reader.GetDateTime(reader.GetOrdinal("DATECREATE"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<SignLogger> SelectCollectionAll()
		{
			List<SignLogger> collection = new List<SignLogger>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				SignLogger entity = new SignLogger();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
				if (!reader.IsDBNull(reader.GetOrdinal("TIME_PROCESS"))) entity.TIME_PROCESS = reader.GetDateTime(reader.GetOrdinal("TIME_PROCESS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MESSAGE"))) entity.MESSAGE = reader.GetString(reader.GetOrdinal("MESSAGE"));
				if (!reader.IsDBNull(reader.GetOrdinal("DATECREATE"))) entity.DATECREATE = reader.GetDateTime(reader.GetOrdinal("DATECREATE"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<SignLogger> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<SignLogger> collection = new List<SignLogger>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				SignLogger entity = new SignLogger();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
				if (!reader.IsDBNull(reader.GetOrdinal("TIME_PROCESS"))) entity.TIME_PROCESS = reader.GetDateTime(reader.GetOrdinal("TIME_PROCESS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MESSAGE"))) entity.MESSAGE = reader.GetString(reader.GetOrdinal("MESSAGE"));
				if (!reader.IsDBNull(reader.GetOrdinal("DATECREATE"))) entity.DATECREATE = reader.GetDateTime(reader.GetOrdinal("DATECREATE"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_SignLogger_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_SignLogger_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_SignLogger_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_SignLogger_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertSignLogger(string mSG_FROM, string mSG_TO, DateTime tIME_PROCESS, string mESSAGE, DateTime dATECREATE)
		{
			SignLogger entity = new SignLogger();	
			entity.MSG_FROM = mSG_FROM;
			entity.MSG_TO = mSG_TO;
			entity.TIME_PROCESS = tIME_PROCESS;
			entity.MESSAGE = mESSAGE;
			entity.DATECREATE = dATECREATE;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_SignLogger_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MSG_FROM", SqlDbType.NVarChar, MSG_FROM);
			db.AddInParameter(dbCommand, "@MSG_TO", SqlDbType.NVarChar, MSG_TO);
			db.AddInParameter(dbCommand, "@TIME_PROCESS", SqlDbType.DateTime, TIME_PROCESS.Year <= 1753 ? DBNull.Value : (object) TIME_PROCESS);
			db.AddInParameter(dbCommand, "@MESSAGE", SqlDbType.NVarChar, MESSAGE);
			db.AddInParameter(dbCommand, "@DATECREATE", SqlDbType.DateTime, DATECREATE.Year <= 1753 ? DBNull.Value : (object) DATECREATE);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<SignLogger> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SignLogger item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateSignLogger(long id, string mSG_FROM, string mSG_TO, DateTime tIME_PROCESS, string mESSAGE, DateTime dATECREATE)
		{
			SignLogger entity = new SignLogger();			
			entity.ID = id;
			entity.MSG_FROM = mSG_FROM;
			entity.MSG_TO = mSG_TO;
			entity.TIME_PROCESS = tIME_PROCESS;
			entity.MESSAGE = mESSAGE;
			entity.DATECREATE = dATECREATE;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_SignLogger_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MSG_FROM", SqlDbType.NVarChar, MSG_FROM);
			db.AddInParameter(dbCommand, "@MSG_TO", SqlDbType.NVarChar, MSG_TO);
			db.AddInParameter(dbCommand, "@TIME_PROCESS", SqlDbType.DateTime, TIME_PROCESS.Year == 1753 ? DBNull.Value : (object) TIME_PROCESS);
			db.AddInParameter(dbCommand, "@MESSAGE", SqlDbType.NVarChar, MESSAGE);
			db.AddInParameter(dbCommand, "@DATECREATE", SqlDbType.DateTime, DATECREATE.Year == 1753 ? DBNull.Value : (object) DATECREATE);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<SignLogger> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SignLogger item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateSignLogger(long id, string mSG_FROM, string mSG_TO, DateTime tIME_PROCESS, string mESSAGE, DateTime dATECREATE)
		{
			SignLogger entity = new SignLogger();			
			entity.ID = id;
			entity.MSG_FROM = mSG_FROM;
			entity.MSG_TO = mSG_TO;
			entity.TIME_PROCESS = tIME_PROCESS;
			entity.MESSAGE = mESSAGE;
			entity.DATECREATE = dATECREATE;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_SignLogger_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MSG_FROM", SqlDbType.NVarChar, MSG_FROM);
			db.AddInParameter(dbCommand, "@MSG_TO", SqlDbType.NVarChar, MSG_TO);
			db.AddInParameter(dbCommand, "@TIME_PROCESS", SqlDbType.DateTime, TIME_PROCESS.Year == 1753 ? DBNull.Value : (object) TIME_PROCESS);
			db.AddInParameter(dbCommand, "@MESSAGE", SqlDbType.NVarChar, MESSAGE);
			db.AddInParameter(dbCommand, "@DATECREATE", SqlDbType.DateTime, DATECREATE.Year == 1753 ? DBNull.Value : (object) DATECREATE);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<SignLogger> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SignLogger item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteSignLogger(long id)
		{
			SignLogger entity = new SignLogger();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_SignLogger_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_SignLogger_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<SignLogger> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SignLogger item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}