﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class KDT_SXXK_DinhMuc_Log : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaSanPham { set; get; }
		public string MaNguyenPhuLieu { set; get; }
		public string DVT_ID { set; get; }
		public decimal DinhMucSuDung { set; get; }
		public decimal TyLeHaoHut { set; get; }
		public string GhiChu { set; get; }
		public int STTHang { set; get; }
		public long Master_ID { set; get; }
		public bool IsFromVietNam { set; get; }
		public string MaDinhDanhLenhSX { set; get; }
		public DateTime DateLog { set; get; }
		public string Status { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_SXXK_DinhMuc_Log> ConvertToCollection(IDataReader reader)
		{
			List<KDT_SXXK_DinhMuc_Log> collection = new List<KDT_SXXK_DinhMuc_Log>();
			while (reader.Read())
			{
				KDT_SXXK_DinhMuc_Log entity = new KDT_SXXK_DinhMuc_Log();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsFromVietNam"))) entity.IsFromVietNam = reader.GetBoolean(reader.GetOrdinal("IsFromVietNam"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDinhDanhLenhSX"))) entity.MaDinhDanhLenhSX = reader.GetString(reader.GetOrdinal("MaDinhDanhLenhSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateLog"))) entity.DateLog = reader.GetDateTime(reader.GetOrdinal("DateLog"));
				if (!reader.IsDBNull(reader.GetOrdinal("Status"))) entity.Status = reader.GetString(reader.GetOrdinal("Status"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_SXXK_DinhMuc_Log> collection, long id)
        {
            foreach (KDT_SXXK_DinhMuc_Log item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_SXXK_DinhMuc_Log VALUES(@MaSanPham, @MaNguyenPhuLieu, @DVT_ID, @DinhMucSuDung, @TyLeHaoHut, @GhiChu, @STTHang, @Master_ID, @IsFromVietNam, @MaDinhDanhLenhSX, @DateLog, @Status)";
            string update = "UPDATE t_KDT_SXXK_DinhMuc_Log SET MaSanPham = @MaSanPham, MaNguyenPhuLieu = @MaNguyenPhuLieu, DVT_ID = @DVT_ID, DinhMucSuDung = @DinhMucSuDung, TyLeHaoHut = @TyLeHaoHut, GhiChu = @GhiChu, STTHang = @STTHang, Master_ID = @Master_ID, IsFromVietNam = @IsFromVietNam, MaDinhDanhLenhSX = @MaDinhDanhLenhSX, DateLog = @DateLog, Status = @Status WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_SXXK_DinhMuc_Log WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.VarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsFromVietNam", SqlDbType.Bit, "IsFromVietNam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDinhDanhLenhSX", SqlDbType.NVarChar, "MaDinhDanhLenhSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateLog", SqlDbType.DateTime, "DateLog", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Status", SqlDbType.NVarChar, "Status", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.VarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsFromVietNam", SqlDbType.Bit, "IsFromVietNam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDinhDanhLenhSX", SqlDbType.NVarChar, "MaDinhDanhLenhSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateLog", SqlDbType.DateTime, "DateLog", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Status", SqlDbType.NVarChar, "Status", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_SXXK_DinhMuc_Log VALUES(@MaSanPham, @MaNguyenPhuLieu, @DVT_ID, @DinhMucSuDung, @TyLeHaoHut, @GhiChu, @STTHang, @Master_ID, @IsFromVietNam, @MaDinhDanhLenhSX, @DateLog, @Status)";
            string update = "UPDATE t_KDT_SXXK_DinhMuc_Log SET MaSanPham = @MaSanPham, MaNguyenPhuLieu = @MaNguyenPhuLieu, DVT_ID = @DVT_ID, DinhMucSuDung = @DinhMucSuDung, TyLeHaoHut = @TyLeHaoHut, GhiChu = @GhiChu, STTHang = @STTHang, Master_ID = @Master_ID, IsFromVietNam = @IsFromVietNam, MaDinhDanhLenhSX = @MaDinhDanhLenhSX, DateLog = @DateLog, Status = @Status WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_SXXK_DinhMuc_Log WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.VarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsFromVietNam", SqlDbType.Bit, "IsFromVietNam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDinhDanhLenhSX", SqlDbType.NVarChar, "MaDinhDanhLenhSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateLog", SqlDbType.DateTime, "DateLog", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Status", SqlDbType.NVarChar, "Status", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.VarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsFromVietNam", SqlDbType.Bit, "IsFromVietNam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDinhDanhLenhSX", SqlDbType.NVarChar, "MaDinhDanhLenhSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateLog", SqlDbType.DateTime, "DateLog", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Status", SqlDbType.NVarChar, "Status", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_SXXK_DinhMuc_Log Load(long id)
		{
			const string spName = "[dbo].[p_KDT_SXXK_DinhMuc_Log_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_SXXK_DinhMuc_Log> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_SXXK_DinhMuc_Log> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_SXXK_DinhMuc_Log> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_DinhMuc_Log_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_DinhMuc_Log_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_DinhMuc_Log_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_DinhMuc_Log_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_SXXK_DinhMuc_Log(string maSanPham, string maNguyenPhuLieu, string dVT_ID, decimal dinhMucSuDung, decimal tyLeHaoHut, string ghiChu, int sTTHang, long master_ID, bool isFromVietNam, string maDinhDanhLenhSX, DateTime dateLog, string status)
		{
			KDT_SXXK_DinhMuc_Log entity = new KDT_SXXK_DinhMuc_Log();	
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DVT_ID = dVT_ID;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.GhiChu = ghiChu;
			entity.STTHang = sTTHang;
			entity.Master_ID = master_ID;
			entity.IsFromVietNam = isFromVietNam;
			entity.MaDinhDanhLenhSX = maDinhDanhLenhSX;
			entity.DateLog = dateLog;
			entity.Status = status;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_SXXK_DinhMuc_Log_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, GhiChu);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@IsFromVietNam", SqlDbType.Bit, IsFromVietNam);
			db.AddInParameter(dbCommand, "@MaDinhDanhLenhSX", SqlDbType.NVarChar, MaDinhDanhLenhSX);
			db.AddInParameter(dbCommand, "@DateLog", SqlDbType.DateTime, DateLog.Year <= 1753 ? DBNull.Value : (object) DateLog);
			db.AddInParameter(dbCommand, "@Status", SqlDbType.NVarChar, Status);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_SXXK_DinhMuc_Log> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_DinhMuc_Log item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_SXXK_DinhMuc_Log(long id, string maSanPham, string maNguyenPhuLieu, string dVT_ID, decimal dinhMucSuDung, decimal tyLeHaoHut, string ghiChu, int sTTHang, long master_ID, bool isFromVietNam, string maDinhDanhLenhSX, DateTime dateLog, string status)
		{
			KDT_SXXK_DinhMuc_Log entity = new KDT_SXXK_DinhMuc_Log();			
			entity.ID = id;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DVT_ID = dVT_ID;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.GhiChu = ghiChu;
			entity.STTHang = sTTHang;
			entity.Master_ID = master_ID;
			entity.IsFromVietNam = isFromVietNam;
			entity.MaDinhDanhLenhSX = maDinhDanhLenhSX;
			entity.DateLog = dateLog;
			entity.Status = status;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_SXXK_DinhMuc_Log_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, GhiChu);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@IsFromVietNam", SqlDbType.Bit, IsFromVietNam);
			db.AddInParameter(dbCommand, "@MaDinhDanhLenhSX", SqlDbType.NVarChar, MaDinhDanhLenhSX);
			db.AddInParameter(dbCommand, "@DateLog", SqlDbType.DateTime, DateLog.Year <= 1753 ? DBNull.Value : (object) DateLog);
			db.AddInParameter(dbCommand, "@Status", SqlDbType.NVarChar, Status);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_SXXK_DinhMuc_Log> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_DinhMuc_Log item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_SXXK_DinhMuc_Log(long id, string maSanPham, string maNguyenPhuLieu, string dVT_ID, decimal dinhMucSuDung, decimal tyLeHaoHut, string ghiChu, int sTTHang, long master_ID, bool isFromVietNam, string maDinhDanhLenhSX, DateTime dateLog, string status)
		{
			KDT_SXXK_DinhMuc_Log entity = new KDT_SXXK_DinhMuc_Log();			
			entity.ID = id;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DVT_ID = dVT_ID;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.GhiChu = ghiChu;
			entity.STTHang = sTTHang;
			entity.Master_ID = master_ID;
			entity.IsFromVietNam = isFromVietNam;
			entity.MaDinhDanhLenhSX = maDinhDanhLenhSX;
			entity.DateLog = dateLog;
			entity.Status = status;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_DinhMuc_Log_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, GhiChu);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@IsFromVietNam", SqlDbType.Bit, IsFromVietNam);
			db.AddInParameter(dbCommand, "@MaDinhDanhLenhSX", SqlDbType.NVarChar, MaDinhDanhLenhSX);
			db.AddInParameter(dbCommand, "@DateLog", SqlDbType.DateTime, DateLog.Year <= 1753 ? DBNull.Value : (object) DateLog);
			db.AddInParameter(dbCommand, "@Status", SqlDbType.NVarChar, Status);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_SXXK_DinhMuc_Log> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_DinhMuc_Log item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_SXXK_DinhMuc_Log(long id)
		{
			KDT_SXXK_DinhMuc_Log entity = new KDT_SXXK_DinhMuc_Log();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_DinhMuc_Log_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_SXXK_DinhMuc_Log_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_SXXK_DinhMuc_Log> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_DinhMuc_Log item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}