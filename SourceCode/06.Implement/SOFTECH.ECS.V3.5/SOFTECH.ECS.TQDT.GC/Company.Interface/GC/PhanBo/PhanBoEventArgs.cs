﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.TQDT.GC.GC.PhanBo
{
    public class PhanBoEventArgs : EventArgs
    {
        public Exception error { get; set; }
        public string Message { get; private set; }
        public int Percent { get; private set; }
        public PhanBoEventArgs(string message, int percent, Exception ex)
        {
            Message = message;
            error = ex;
            Percent = percent;
        }
        public PhanBoEventArgs(string message, int percent)
            : this(message, percent, null)
        {
        }
        public PhanBoEventArgs(Exception ex)
            : this(string.Empty, 0, ex)
        {

        }
    }
}
