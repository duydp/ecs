using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KeyServer;
using KeySecurity;
using KeyServer.User;

namespace LicenseCode
{
    public partial class frmKeysApprove : DevExpress.XtraEditors.XtraForm
    {
        public frmKeysApprove()
        {
            InitializeComponent();
        }
        public List<Key> listKey = new List<Key>();
        private ProductKey.ProductKey productServices = new ProductKey.ProductKey();
        public User User = new User();
        private void frmKeysApprove_Load(object sender, EventArgs e)
        {
            productServices.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            productServices.Timeout = 60000;
            string result;
            try
            {
                result = productServices.SeachKey(User.UserName, KeyCode.ConvertToBase64(User.PassWord), "-1", false, new DateTime(1900, 1, 1), DateTime.MaxValue, string.Empty);
                if (string.IsNullOrEmpty(result))
                    return;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Lỗi kết nối" + ex.Message);
                return;
            }
            try
            {
                listKey = new List<Key>();
                listKey = KeyCode.Deserialize<List<Key>>(KeyCode.ConvertFromBase64(result));
                gridControl1.DataSource = listKey;
                //LoadListView();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Lỗi " + result);
            }
        }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            string result = string.Empty;
            try
            {
                if (e.ClickedItem.Name.Equals("Approve"))
                {
                    //MessageBox.Show(e.ClickedItem.Text);//Write your copy code here  
                    Key k = (Key)gridView1.GetRow(0);
                    string serial = k.SerialNumber;
                    if (MessageBox.Show("Chấp nhận yêu cầu lấy key cho doanh nghiệp " + k.CustomerName + "?", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        result = productServices.ApproveKeyLicense(User.UserName, KeyCode.ConvertToBase64(User.PassWord), serial, true);
                    else
                        return;
                    if (string.IsNullOrEmpty(result))
                        MessageBox.Show("Đã chấp nhận");
                    else
                        MessageBox.Show("Lỗi xảy ra khi thực hiện: " + result);
                }
                else if (e.ClickedItem.Name.Equals("DisApprove"))
                {
                    Key k = (Key)gridView1.GetRow(0);
                    string serial = k.SerialNumber;
                    if (MessageBox.Show("Từ chối cấp key cho doanh nghiệp " + k.CustomerName + "?", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        result = productServices.ApproveKeyLicense(User.UserName, KeyCode.ConvertToBase64(User.PassWord), serial, false);
                    else
                        return;
                    if (string.IsNullOrEmpty(result))
                        MessageBox.Show("Đã từ chối");
                    else
                        MessageBox.Show("Lỗi xảy ra khi thực hiện: " + result);
                }
                else
                    return;
                if (string.IsNullOrEmpty(result))
                    gridView1.DeleteRow(0);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show();
            }  
        }




    }
}