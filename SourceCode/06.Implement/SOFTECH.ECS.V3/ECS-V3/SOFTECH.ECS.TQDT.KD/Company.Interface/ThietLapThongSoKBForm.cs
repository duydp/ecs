﻿using System;
using System.Windows.Forms;
#if KD_V3
using Company.KD.BLL.SXXK;
using Company.KD.BLL.SXXK.ToKhai;
using Company.KD.BLL;
#elif SXXK_V3
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK.ThanhKhoan;
using Company.BLL;
#elif GC_V3
using Company.GC.BLL.SXXK;
using Company.GC.BLL.SXXK.ToKhai;
using Company.GC.BLL;
#endif
using System.Threading;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Xml;



namespace Company.Interface
{
    public partial class ThietLapThongSoKBForm : BaseForm
    {
        public ThietLapThongSoKBForm()
        {
            InitializeComponent();
        }

        private void SendForm_Load(object sender, EventArgs e)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram() + "\\ConfigDoanhNghiep";
            //Hungtq update 28/01/2011.
            string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);
            doc.Load(fileName);
            //Get thong tin Server
            GlobalSettings.SERVER_NAME = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Server").InnerText;
            //Get thong tin Database
            GlobalSettings.DATABASE_NAME = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Database").InnerText;
            //Get thong tin UserName
            GlobalSettings.USER = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "UserName").InnerText;
            //Get thong tin PassWord
            GlobalSettings.PASS = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Password").InnerText;

            txtServerName.Text =  GlobalSettings.SERVER_NAME;
            txtSa.Text = GlobalSettings.USER;
            txtPass.Text = GlobalSettings.PASS;
            cbDatabaseSource.Text = GlobalSettings.DATABASE_NAME;
            SetListTable();
        }

        private void SetListTable()
        {
            if (GlobalSettings.ListTableNameSource.Count > 0)
            {
                cbDatabaseSource.Items.Clear();
                cbDatabaseSource.Items.Add("(-Làm mới-)");

                foreach (string item in GlobalSettings.ListTableNameSource)
                {
                    cbDatabaseSource.Items.Add(item);
                }

                cbDatabaseSource.Sorted = true;

                cbDatabaseSource.SelectedItem.Value = GlobalSettings.DATABASE_NAME;
            }
        }

        //private SQLDMO.Application fApp = null;
        //private SQLDMO.SQLServerClass fServer = null;
        //private string gAppName = "Sql Server BackUp utility";
        private void GetListTableSource(Janus.Windows.EditControls.UIComboBox comboBox)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string strConnString = "server=" + txtServerName.Text;
                strConnString += ";User Id=" + txtSa.Text + ";Password=" + txtPass.Text;

                //fServer = new SQLDMO.SQLServerClass();
                //fServer.Connect(txtServerName.Text, txtSa.Text, txtPass.Text);

                Company.KDT.SHARE.Components.SQL.InitializeServer(strConnString);

                comboBox.Items.Clear();
                comboBox.Items.Add("(-Làm mới-)");

                //foreach (SQLDMO.Database db in fServer.Databases)
                //{
                //    comboBox.Items.Add(db.Name);
                //}

                foreach (string dbName in Company.KDT.SHARE.Components.SQL.ListDatabases())
                {
                    comboBox.Items.Add(dbName);
                }

                comboBox.Sorted = true;

                comboBox.SelectedIndexChanged -= new EventHandler(cbDatabaseSource_SelectedIndexChanged);
                comboBox.SelectedIndex = 0;
                comboBox.SelectedIndexChanged += new EventHandler(cbDatabaseSource_SelectedIndexChanged);

            }
            catch (Exception e1)
            {
                Globals.ShowMessage("Kết nối không thành công. Bạn hãy kiểm tra lại thông tin cấu hình.\r\n\nChi tiết: " + e1.Message, false);
                //MessageBox.Show(e1.ToString(), gAppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logger.LocalLogger.Instance().WriteMessage(e1);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            try
            {
                string MSSQLConnectionString = Company.KDT.SHARE.Components.Globals.ReadNodeXmlConnectionStrings2();
                //Kiem tra SQL co phai cau hinh ket noi truc tiep file DATABASE.
                bool isAttachFile = MSSQLConnectionString.Contains("AttachDbFilename");

                //Neu nguoi dung nhap vao [user, pass] -> khong su dung ket noi truc tiep den file DATABASE.
                isAttachFile = (txtSa.Text.Trim().Length == 0 && txtPass.Text.Trim().Length == 0);

                if (!isAttachFile)
                {
                    containerValidator1.Validate();
                    if (!containerValidator1.IsValid)
                        return;
                }

                if (cbDatabaseSource.Text == "")
                {
                    errorProvider1.SetError(cbDatabaseSource, "\"Cơ sơ dữ liệu\" không được trống");
                    return;
                }
                else
                    errorProvider1.SetError(cbDatabaseSource, "");

                if (!isAttachFile)
                {
                    //Cấu hình lại connectionString.
                    string st = "Server=" + txtServerName.Text.Trim() + ";Database=" + cbDatabaseSource.Text.Trim() + ";Uid=" + txtSa.Text.Trim() + ";Pwd=" + txtPass.Text.Trim() + ";Max Pool Size=100;Min Pool Size=5";
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString = st;
                    //cấu hình WS
                    SqlConnection con = new SqlConnection(st);
                    try
                    {
                        con.Open();
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        MLMessages("Không kết nối được tới máy chủ này\r\nLý do: " + ex.Message, "MSG_PUB05", "", false);
                        return;
                    }

                    //Hungtq 14/01/2011. Luu cau hinh
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("pass", txtPass.Text.Trim());
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DATABASE_NAME", cbDatabaseSource.Text.Trim());
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("user", txtSa.Text.Trim());
                }
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ServerName", txtServerName.Text.Trim());


                /*DATLMQ update lưu cấu hình vào file config 18/01/2011.*/
                XmlDocument doc = new XmlDocument();
                string path = Company.KDT.SHARE.Components.Globals.GetPathProgram() + "\\ConfigDoanhNghiep";
                //Hungtq update 28/01/2011.
                string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);

                doc.Load(fileName);

                //Set thong tin Server
                XmlNode nodeServerName = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "SERVER");
                GlobalSettings.SERVER_NAME = nodeServerName.InnerText = txtServerName.Text.Trim();
                if (!isAttachFile)
                {
                    //Set thong tin Database
                    XmlNode nodeDatabase = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Database");
                    GlobalSettings.DATABASE_NAME = nodeDatabase.InnerText = cbDatabaseSource.Text.Trim();
                    //Set thong tin UserName
                    XmlNode nodeUserName = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "UserName");
                    GlobalSettings.USER = nodeUserName.InnerText = txtSa.Text.Trim();
                    //Set thong tin Password
                    XmlNode nodePassword = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Password");
                    GlobalSettings.PASS = nodePassword.InnerText = txtPass.Text.Trim();
                }

                doc.Save(fileName);
                ShowMessage("Lưu file cấu hình Kết nối Database thành công.", false);

                if (!isAttachFile)
                {
                    string mssql = "Server=" + txtServerName.Text.Trim() + "; database=" + cbDatabaseSource.Text.Trim() + "; uid=" + txtSa.Text.Trim() + "; pwd=" + txtPass.Text.Trim() + ";Max Pool Size=100;Min Pool Size=5";
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlConnectionStrings(mssql);
                }
                else
                {
                    string MSSQLConnectionStringModified = "";
                    string[] arr = MSSQLConnectionString.Split(new char[] { ';' });

                    arr[0] = arr[0].Split(new char[] { '=' })[0] + "=" + txtServerName.Text.Trim().ToUpper();

                    for (int i = 0; i < arr.Length; i++)
                    {
                        MSSQLConnectionStringModified += arr[i];

                        if (i < arr.Length - 1)
                            MSSQLConnectionStringModified += ";";
                    }

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlConnectionStrings(MSSQLConnectionStringModified);
                }

                //GlobalSettings.RefreshKey();
                this.Close();
                Application.Restart();
            }
            catch (Exception ex)
            {
                ShowMessageTQDT(ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbDatabaseSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDatabaseSource.SelectedIndex == 0)
            {
                GetListTableSource(cbDatabaseSource);
            }
        }

        private void cbDatabaseSource_DropDown(object sender, EventArgs e)
        {
            if (cbDatabaseSource.Items.Count <= 1)
                GetListTableSource(cbDatabaseSource);
        }

        private void cbDatabaseSource_Closed(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (cbDatabaseSource.SelectedIndex == 0)
                {
                    GetListTableSource(cbDatabaseSource);
                }

                GlobalSettings.ListTableNameSource.Clear();
                for (int i = 1; i < cbDatabaseSource.Items.Count; i++)
                {
                    GlobalSettings.ListTableNameSource.Add(cbDatabaseSource.Items[i].Text);
                }

                Cursor = Cursors.Default;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

    }
}