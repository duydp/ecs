
Alter TABLE [dbo].[t_KDT_VNACC_ChiThiHaiQuan]
	Alter column Ten nvarchar(500) NULL
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]
	@LoaiThongTin varchar(10),
	@Master_ID bigint,
	@PhanLoai varchar(1),
	@Ngay datetime,
	@Ten nvarchar(500),
	@NoiDung nvarchar(max),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChiThiHaiQuan]
(
	[LoaiThongTin],
	[Master_ID],
	[PhanLoai],
	[Ngay],
	[Ten],
	[NoiDung],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@LoaiThongTin,
	@Master_ID,
	@PhanLoai,
	@Ngay,
	@Ten,
	@NoiDung,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]
	@ID bigint,
	@LoaiThongTin varchar(10),
	@Master_ID bigint,
	@PhanLoai varchar(1),
	@Ngay datetime,
	@Ten nvarchar(500),
	@NoiDung nvarchar(max),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChiThiHaiQuan]
SET
	[LoaiThongTin] = @LoaiThongTin,
	[Master_ID] = @Master_ID,
	[PhanLoai] = @PhanLoai,
	[Ngay] = @Ngay,
	[Ten] = @Ten,
	[NoiDung] = @NoiDung,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]
	@ID bigint,
	@LoaiThongTin varchar(10),
	@Master_ID bigint,
	@PhanLoai varchar(1),
	@Ngay datetime,
	@Ten nvarchar(500),
	@NoiDung nvarchar(max),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChiThiHaiQuan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChiThiHaiQuan] 
		SET
			[LoaiThongTin] = @LoaiThongTin,
			[Master_ID] = @Master_ID,
			[PhanLoai] = @PhanLoai,
			[Ngay] = @Ngay,
			[Ten] = @Ten,
			[NoiDung] = @NoiDung,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChiThiHaiQuan]
		(
			[LoaiThongTin],
			[Master_ID],
			[PhanLoai],
			[Ngay],
			[Ten],
			[NoiDung],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@LoaiThongTin,
			@Master_ID,
			@PhanLoai,
			@Ngay,
			@Ten,
			@NoiDung,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChiThiHaiQuan]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChiThiHaiQuan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiThongTin],
	[Master_ID],
	[PhanLoai],
	[Ngay],
	[Ten],
	[NoiDung],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ChiThiHaiQuan]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiThongTin],
	[Master_ID],
	[PhanLoai],
	[Ngay],
	[Ten],
	[NoiDung],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_ChiThiHaiQuan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiThongTin],
	[Master_ID],
	[PhanLoai],
	[Ngay],
	[Ten],
	[NoiDung],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ChiThiHaiQuan]	

GO

--------------- cap nhat table T_VNACCS_Mapper--------------------
Alter Table t_VNACCS_Mapper alter column CodeV4 varchar(100) null
Alter Table t_VNACCS_Mapper alter column NameV4 nvarchar(2000) null
Alter Table t_VNACCS_Mapper alter column CodeV5 varchar(50) null
Alter Table t_VNACCS_Mapper alter column NameV5 nvarchar(2000) null
Alter Table t_VNACCS_Mapper alter column LoaiMapper varchar(50) null
Alter Table t_VNACCS_Mapper alter column Notes nvarchar(2000) null


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_Update]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_Load]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_Insert]
	@CodeV4 varchar(100),
	@NameV4 nvarchar(2000),
	@CodeV5 varchar(50),
	@NameV5 nvarchar(2000),
	@LoaiMapper varchar(50),
	@Notes nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_VNACCS_Mapper]
(
	[CodeV4],
	[NameV4],
	[CodeV5],
	[NameV5],
	[LoaiMapper],
	[Notes]
)
VALUES 
(
	@CodeV4,
	@NameV4,
	@CodeV5,
	@NameV5,
	@LoaiMapper,
	@Notes
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_Update]
	@ID bigint,
	@CodeV4 varchar(100),
	@NameV4 nvarchar(2000),
	@CodeV5 varchar(50),
	@NameV5 nvarchar(2000),
	@LoaiMapper varchar(50),
	@Notes nvarchar(2000)
AS

UPDATE
	[dbo].[t_VNACCS_Mapper]
SET
	[CodeV4] = @CodeV4,
	[NameV4] = @NameV4,
	[CodeV5] = @CodeV5,
	[NameV5] = @NameV5,
	[LoaiMapper] = @LoaiMapper,
	[Notes] = @Notes
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_InsertUpdate]
	@ID bigint,
	@CodeV4 varchar(100),
	@NameV4 nvarchar(2000),
	@CodeV5 varchar(50),
	@NameV5 nvarchar(2000),
	@LoaiMapper varchar(50),
	@Notes nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACCS_Mapper] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACCS_Mapper] 
		SET
			[CodeV4] = @CodeV4,
			[NameV4] = @NameV4,
			[CodeV5] = @CodeV5,
			[NameV5] = @NameV5,
			[LoaiMapper] = @LoaiMapper,
			[Notes] = @Notes
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACCS_Mapper]
		(
			[CodeV4],
			[NameV4],
			[CodeV5],
			[NameV5],
			[LoaiMapper],
			[Notes]
		)
		VALUES 
		(
			@CodeV4,
			@NameV4,
			@CodeV5,
			@NameV5,
			@LoaiMapper,
			@Notes
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_VNACCS_Mapper]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACCS_Mapper] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[CodeV4],
	[NameV4],
	[CodeV5],
	[NameV5],
	[LoaiMapper],
	[Notes]
FROM
	[dbo].[t_VNACCS_Mapper]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[CodeV4],
	[NameV4],
	[CodeV5],
	[NameV5],
	[LoaiMapper],
	[Notes]
FROM [dbo].[t_VNACCS_Mapper] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[CodeV4],
	[NameV4],
	[CodeV5],
	[NameV5],
	[LoaiMapper],
	[Notes]
FROM
	[dbo].[t_VNACCS_Mapper]	

GO



------------------------------ Cập nhật dữ liệu---------------------------
Delete t_VNACCS_Mapper

INSERT INTO t_VNACCS_Mapper VALUES('NKD02,NKD03,NKD04,NKD06,NKD11,NGC03,NDT01,NDT16,NDT08,NDT18,NCX03',N'Nhập Dầu khí, Nhập Kinh Doanh Đá Quí, Nhập Kinh Doanh Gắn máy, Nhập Kinh Doanh Ô tô, Nhập Kinh doanh Tại chỗ, Nhập Gia Công Kinh doanh, Nhập đầu tư, Nhập đầu tư tại chỗ, Nhập Đầu Tư nộp thuế, Nhập Đầu Tư nộp thuế tại chỗ, Nhập chế xuất tiêu dùng','A12',N'Nhập kinh doanh sản xuất','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NKD01,NKD11',N'Nhập Kinh Doanh, Nhập Kinh doanh Tại chỗ','A42',N'Chuyển tiêu thụ nội địa khác','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NCX01,NSX03,NGC09,NTA10',N'Nhập chế xuất sản xuất, Nhập sản xuất xuất khẩu vào KCX, Nhập khu chế xuất, Tạm Nhập NPL vào KCX để Gia công','E11',N'Nhập nguyên liệu của doanh nghiệp chế xuất ','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NDT11,NDT09,NDT10,NDT14,NDT17,NTA12,NCX02',N'Nhập đầu tư khu chế xuất, Nhập Đầu Tư từ Việt Nam, Nhập Kinh Doanh Đầu Tư (Trong nước), Nhập đầu tư khu công nghiệp, Nhập đầu tư liên doanh, Mua Hàng Của Nội địa (Xí nghiệp KCX), Nhập chế xuất đầu tư','E13',N'Nhập tạo tài sản cố định của doanh nghiệp chế xuất','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NSX04,NGC12,NCX04,NGC14,NTA09',N'KCX mua hàng nội địa để SXXK, Nhập TP  từ ND vào KCX, Nhập chế xuất cho mục đích khác, Nhập chế xuất tại chỗ, Tái Nhập Thành Phẩm GC vào KCX','E15',N'Nhập nguyên liệu của doanh nghiệp chế xuất từ nội địa','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NGC01,NGC08,NGC13,NGC16,NGC18,NGC19,NGC21',N'Nhập Gia Công, Nội địa mua hàng của Khu chế xuất, Nhập Gia công Tại chỗ, Nhập Gia Công từ KTM về nội địa, Nhập gia công chuyển tiếp NPL, Nhập gia công chuyển tiếp SP, Nhập gia công tự cung ứng','E21',N'Nhập nguyên liệu để gia công cho thương nhân nước ngoài','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NGC18',N'Nhập gia công chuyển tiếp NPL','E23',N'Nhập nguyên liệu gia công từ hợp đồng khác chuyển sang','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NSX01,NSX02,NSX06,NSX07',N'Nhập Để Sản Xuất Hàng Xuất Khẩu, Nhập Đầu Tư Sản xuất xuất khẩu, Nhập SXXK Tại chỗ, Nhập NPL vào kho bảo thuế để SXXK','E31',N'Nhập nguyên liệu sản xuất xuất khẩu','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NGC22',N'Nhập hàng hóa đặt Gia công ở nuớc ngoài','E41',N'Nhập sản phẩm thuê gia công ở nước ngoài','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NTA06',N'Nhập kho ngoại quan','C11',N'Hàng gửi kho ngoại quan','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NKD14,NKD16,NKD18',N'Nhập Kinh doanh từ nuớc ngoài vào KKT, Nhập Kinh doanh từ nội địa về KTM, Nhập KD giữa các Khu phi thuế quan','C21',N'Hàng đưa vào khu phi thuế quan','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XKD01,XTA04',N'Xuất Kinh Doanh, Xuất Đầu Tư Tái xuất','B12',N'Xuất sau khi đã tạm xuất','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XSX03,XCX01',N'Xuất sản xuất xuất khẩu từ KCX, Xuất chế xuất sản xuất','E42',N'Xuất sản phẩm của doanh nghiệp chế xuất','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XGC12,XTA10',N'Xuất NL từ KCX vào nội địa để GC, Tạm Xuất NPL vào Nội địa để Gia công','E46',N'Hàng của doanh nghiệp chế xuất vào nội địa để gia công','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XGC18',N'Xuất gia công chuyển tiếp NPL','E54',N'Xuất nguyên liệu gia công sang hợp đồng khác','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XGC13,XGC14,XGC16',N'Xuất gia công tại chổ, Xuất chế xuất tại chỗ, Xuất Gia công từ nội địa vào KTM','E56',N'Xuất sản phẩm gia công vào nội địa','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XSX01,XSX02,XSX06',N'Xuất khẩu hàng SX từ hàng NK, Xuất Đầu Tư Sản xuất xuất khẩu, Xuất sản xuất tại chỗ','E62',N'Xuất sản phẩm sản xuất xuất khẩu','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XGC10',N'Xuất đặt gia công hàng hoá ở nước ngoài','E82',N'Xuất nguyên liệu thuê gia công ở nước ngoài','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XTA11,XTA04',N'Xuất Đầu Tư Tái xuất thi công, Xuất Đầu Tư Tái xuất','G22',N'Tái xuất thiết bị, máy móc thuê phục vụ dự án có thời hạn','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XTA06',N'Xuất kho ngoại quan','C12',N'Hàng xuất kho ngoại quan','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XKD12,XKD13,XKD14',N'Xuất Kinh doanh từ KTM về nội địa, Xuất Kinh doanh từ KTM ra nuớc ngoài, Xuất KD giữa các Khu phi thuế quan','C22',N'Hàng đưa ra khỏi Khu phi thuế quan','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('74',N'100 MET','HMTR',N'','DVT',N'100 mét')
INSERT INTO t_VNACCS_Mapper VALUES('77',N'100 VIEN','HUNV',N'','DVT',N'100 viên/hạt')
INSERT INTO t_VNACCS_Mapper VALUES('31',N'1000 BO','KSET',N'','DVT',N'1000 bộ/hệ thống')
INSERT INTO t_VNACCS_Mapper VALUES('19, 30',N'1000 CAI, 1000 CHIEC','KPCE',N'','DVT',N'1000 cái/chiếc')
INSERT INTO t_VNACCS_Mapper VALUES('18',N'1000 CON','KUNC',N'','DVT',N'1000 con')
INSERT INTO t_VNACCS_Mapper VALUES('22',N'1000 CUON','KROL',N'','DVT',N'1000 cuộn')
INSERT INTO t_VNACCS_Mapper VALUES('24',N'1000 DOI','KPR ',N'','DVT',N'1000 đôi/cặp')
INSERT INTO t_VNACCS_Mapper VALUES('32',N'1000 GOI','KUNK',N'','DVT',N'1000 kiện/hộp/bao/gói')
INSERT INTO t_VNACCS_Mapper VALUES('20',N'1000 QUA','KUNQ',N'','DVT',N'1000 quả')
INSERT INTO t_VNACCS_Mapper VALUES('28',N'1000 M2','KMTQ',N'','DVT',N'1000m2')
INSERT INTO t_VNACCS_Mapper VALUES('13',N'THUNG','BBL ',N'','DVT',N'Barrel')
INSERT INTO t_VNACCS_Mapper VALUES('33107',N'BO, HE THONG','SET',N'SETS','DVT',N'Bộ')
INSERT INTO t_VNACCS_Mapper VALUES('17, 11',N'CAI, CHIEC','PCE',N'PIECES','DVT',N'Cái/Chiếc')
INSERT INTO t_VNACCS_Mapper VALUES('71',N'CANH (THUC VAT)','UNH',N'CANH','DVT',N'Cành')
INSERT INTO t_VNACCS_Mapper VALUES('9',N'CARAT','CT',N'CARATS','DVT',N'Cara')
INSERT INTO t_VNACCS_Mapper VALUES('50',N'CAY (THUC VAT)','UNY',N'CAY','DVT',N'Cây')
INSERT INTO t_VNACCS_Mapper VALUES('67',N'CENTIMET','CEN ',N'','DVT',N'Cen ti mét')
INSERT INTO t_VNACCS_Mapper VALUES('47,48,49',N'CHAI, LO, TUYP','UNA ',N'','DVT',N'Chai/ Lọ/ Tuýp')
INSERT INTO t_VNACCS_Mapper VALUES('10',N'CON (DONG VAT)','UNC',N'CON','DVT',N'Con')
INSERT INTO t_VNACCS_Mapper VALUES('72',N'CU','UNU',N'CU','DVT',N'Củ')
INSERT INTO t_VNACCS_Mapper VALUES('36',N'CUON','ROL',N'ROLL','DVT',N'Cuộn')
INSERT INTO t_VNACCS_Mapper VALUES('8,89,90,91',N'DOI,CAP,2 CHIEC/BO,2 CAI/BO','PR',N'PAIR','DVT',N'Đôi/Cặp')
INSERT INTO t_VNACCS_Mapper VALUES('1',N'GRAMME','GRM',N'GRAMMES','DVT',N'Gam')
INSERT INTO t_VNACCS_Mapper VALUES('5',N'HECTOLIT','HLTR',N'','DVT',N'Hectolit')
INSERT INTO t_VNACCS_Mapper VALUES('40',N'YARD','YRD',N'YARDS','DVT',N'I/át')
INSERT INTO t_VNACCS_Mapper VALUES('64',N'YARD2','YDK',N'SQUARE YARDS','DVT',N'I/át vuông')
INSERT INTO t_VNACCS_Mapper VALUES('62',N'INCH','INC ',N'','DVT',N'Inch')
INSERT INTO t_VNACCS_Mapper VALUES('39,41,75,53',N'KIEN, HOP, BAO, GOI','UNK ',N'','DVT',N'Kiện/Hộp/Bao/Gói')
INSERT INTO t_VNACCS_Mapper VALUES('21',N'1000 LIT','KL',N'KILO-LITRES','DVT',N'Kilo lít')
INSERT INTO t_VNACCS_Mapper VALUES('23',N'1000 MET','KMTR',N'','DVT',N'Kilo mét (1000 mét)')
INSERT INTO t_VNACCS_Mapper VALUES('2',N'KG','KGM',N'KILO-GRAMMES','DVT',N'Kilogam')
INSERT INTO t_VNACCS_Mapper VALUES('7',N'KW/H','KHW ',N'KILOWATT HOUR','DVT',N'Ki-lô-oát giờ')
INSERT INTO t_VNACCS_Mapper VALUES('4',N'LIT','LTR',N'LITRES','DVT',N'Lít')
INSERT INTO t_VNACCS_Mapper VALUES('73,76',N'LON, CAN','UNL ',N'','DVT',N'Lon/Can')
INSERT INTO t_VNACCS_Mapper VALUES('3',N'MET','MTR',N'METRES','DVT',N'Mét')
INSERT INTO t_VNACCS_Mapper VALUES('6',N'M3','MTQ',N'CUBIC METRES','DVT',N'Mét khối')
INSERT INTO t_VNACCS_Mapper VALUES('14',N'M2','MTK',N'SQUARE METRES','DVT',N'Mét vuông')
INSERT INTO t_VNACCS_Mapper VALUES('70',N'MILIGAM','MGRM',N'','DVT',N'Mili gram')
INSERT INTO t_VNACCS_Mapper VALUES('66',N'MILILIT (ml)','MLT',N'MILLI-LITRES','DVT',N'Mili lít')
INSERT INTO t_VNACCS_Mapper VALUES('69',N'MILIMET','MMTR',N'','DVT',N'Mili mét')
INSERT INTO t_VNACCS_Mapper VALUES('95',N'POUND','LBR',N'POUNDS','DVT',N'Pao')
INSERT INTO t_VNACCS_Mapper VALUES('45',N'FEET VUONG','FTK',N'SQUARE FEET','DVT',N'Phút vuông')
INSERT INTO t_VNACCS_Mapper VALUES('27',N'QUA','UNQ',N'QUA','DVT',N'Quả')
INSERT INTO t_VNACCS_Mapper VALUES('84,58',N'QUYEN, TAP','UNB ',N'','DVT',N'Quyển/Tập')
INSERT INTO t_VNACCS_Mapper VALUES('51',N'STER','STER',N'','DVT',N'Ster ')
INSERT INTO t_VNACCS_Mapper VALUES('15',N'TA (12 CAI)','DZN',N'DOZEN','DVT',N'Tá')
INSERT INTO t_VNACCS_Mapper VALUES('16',N'TAN','TNE',N'METRIC-TONS','DVT',N'Tấn')
INSERT INTO t_VNACCS_Mapper VALUES('12, 96, 65',N'THANH, MANH, MIENG','UNT ',N'','DVT',N'Thanh/Mảnh/Tấm/Miếng')
INSERT INTO t_VNACCS_Mapper VALUES('99',N'TUT','UND ',N'','DVT',N'Tút')
INSERT INTO t_VNACCS_Mapper VALUES('0',N'USD','USD ',N'','DVT',N'USD')
INSERT INTO t_VNACCS_Mapper VALUES('29,57',N'VIEN, HAT','UNV ',N'','DVT',N'Viên/Hạt')
INSERT INTO t_VNACCS_Mapper VALUES('42',N'TAM','TAM',N'','DVT',N'')
INSERT INTO t_VNACCS_Mapper VALUES('43',N'SOI','SOI ',N'','DVT',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B50J',N'Chi cục HQ Bắc Đai An Giang','50BJ  ',N'Chi cục HQ Bắc Đai','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C50E',N'Chi cục HQ Cảng Mỹ Thới An Giang','50CE  ',N'Chi cục HQ Cảng Mỹ Thới','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B50B',N'Chi cục HQ CK Tịnh Biên An Giang','50BB  ',N'Chi cục HQ CK Tịnh Biên','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B50D',N'Chi cục HQ CK Vĩnh Xương An Giang','50BD  ',N'Chi cục HQ CK Vĩnh Xương','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B50K',N'Chi cục HQ Khánh Bình An Giang','50BK  ',N'Chi cục HQ Khánh Bình','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B50C',N'Chi cục HQ Vĩnh Hội Đông An Giang','50BC  ',N'Chi cục HQ Vĩnh Hội Đông','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B51E',N'Chi cục HQ Phước Thắng  Vũng Tàu','51BE  ',N'Chi cục HQ Cảng Cát Lở','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C51B',N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu','51CB  ',N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C51C',N'Chi cục HQ CK Cảng Phú Mỹ Vũng Tàu','51C1',N'Chi cục HQ CK Cảng Phú Mỹ - Đội TTHHXNK và Kho ngoại quan','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C51C',N'Chi cục HQ CK Cảng Phú Mỹ Vũng Tàu','51C2',N'Chi cục HQ CK Cảng Phú Mỹ - ĐTT SP-PSA','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B18A01',N'Đội thủ tục HQ quản lý KCN Yên Phong','18A1',N'Chi cục HQ Bắc Ninh - Đội TTHQ quản lý KCN Yên Phong','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B18A02',N'Đội thủ tục HQ quản lý KCN Quế Võ','18A2',N'Chi cục HQ Bắc Ninh - Đội TTHQ quản lý KCN Quế Võ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B18A03',N'Đội Nghiệp vụ - Chi cục HQ Bắc Ninh','18A3',N'Chi cục HQ Bắc Ninh - Đội nghiệp vụ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('I18D',N'Chi cục HQ cảng nội địa Tiên Sơn','18ID  ',N'Chi cục HQ Cảng nội địa Tiên Sơn','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B18C',N'Chi cục HQ quản lý các KCN Bắc Giang','18BC  ',N'Chi cục HQ Quản lý các KCN Bắc Giang','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B18B',N'Chi cục HQ Thái Nguyên','18B1',N'Chi cục HQ Thái Nguyên','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B18B',N'Chi cục HQ Thái Nguyên','18B2',N'Chi cục HQ Thái Nguyên - Đội TT KCN Yên Bình','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C37B',N'Chi cục HQ Cảng Qui Nhơn Bình Định','37CB  ',N'Chi cục HQ CK Cảng Qui Nhơn','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('T37C  ',N'Chi cục HQ Phú Yên Bình Định','37TC  ',N'Chi cục HQ Phú Yên','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N43K',N'Chi cục HQ KCN Mỹ Phước Bình Dương','43K1',N'Chi cục HQ KCN Mỹ Phước - Đội Nghiệp vụ ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N43K',N'','43K2',N'Chi cục HQ KCN Mỹ Phước - Đội TT Khu liên hợp','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N43E',N'Chi cục HQ KCN Tân Định Bình Dương','43K3',N'Chi cục HQ KCN Mỹ Phước - Đội TT Tân Định','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N43D ',N'Chi cục HQ KCN Sóng thần Bình Dương','43ND  ',N'Chi cục HQ KCN Sóng Thần','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N43G',N'Chi cục HQ KCN Việt Hương','43NG  ',N'Chi cục HQ KCN Việt Hương','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N43F',N'Chi cục HQ KCN Viet Nam - Singapore','43NF  ',N'Chi cục HQ KCN Việt Nam - Singapore','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P43B',N'Chi cục HQ Quản lý ngoài KCN Bình Dương','43PB  ',N'Chi cục HQ Quản lý hàng hóa XNK ngoài KCN','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('I43H',N'Chi cục HQ Sóng Thần','43H1',N'Chi cục HQ Sóng Thần','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('I43H',N'','43H2',N'Chi cục HQ Sóng Thần - Đội TT Cảng Thạnh Phước','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P61A',N'Phòng Nghiệp vụ HQ Bình Phước','61A3',N'Chi cục HQ Chơn Thành - Đội Nghiệp vụ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P61A',N'Phòng Nghiệp vụ HQ Bình Phước','61A4',N'Chi cục HQ Chơn Thành - Đội Nghiệp vụ 2','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B61B',N'Chi cục HQ CK Hoàng Diệu Bình Phước','61BB  ',N'Chi cục HQ CK Hoàng Diệu - Đội NV Tổng hợp','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B61B',N'Chi cục HQ CK Hoàng Diệu Bình Phước','61BB  ',N'Chi cục HQ CK Hoàng Diệu - Đội NVCK Tân Tiến','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B61A',N'Chi cục HQ CK Hoa Lư Bình Phước','61A1',N'Chi cục HQ CK Quốc tế Hoa Lư - Đội NV Tổng hợp  ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B61A',N'Chi cục HQ CK Hoa Lư Bình Phước','61A2',N'Chi cục HQ CK Quốc tế Hoa Lư - Đội TTHQ CK Tà Vát','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C59B',N'Chi cục HQ CK Cảng Năm Căn Cà Mau','59CB  ',N'Chi cục HQ CK Cảng Năm Căn','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C54B',N'Chi cục HQ CK Cảng Cần Thơ','54CB  ',N'Chi cục HQ CK Cảng Cần Thơ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C54D',N'Chi cục HQ CK  Vĩnh Long','54CD  ',N'Chi cục HQ CK Vĩnh Long','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P54K',N'Chi cục HQ CK  Vĩnh Long','54PK  ',N'Chi cục HQ Sóc Trăng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P54H  ',N'Chi cục HQ Tây Đô Cần Thơ','54PH  ',N'Chi cục HQ Tây Đô','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P11K',N'Chi cục HQ Bắc Kạn Cao Bằng','11PK  ',N'Chi cục HQ Bắc Kạn','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B11G',N'Chi cục HQ CK Bí Hà Cao Bằng','11G1',N'Chi cục HQ CK Bí Hà ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B11C  ',N'HQ Cửa Khẩu Lý Vạn (Cao Bằng)','11G2',N'Chi cục HQ CK Bí Hà - Đội NV Lý Vạn','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B11H',N'Chi cục HQ CK Pò Peo Cao Bằng','11BH  ',N'Chi cục HQ CK Pò Peo','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B11F',N'Chi cục HQ CK Sóc Giang Cao Bằng','11BF  ',N'Chi cục HQ CK Sóc Giang','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B11B',N'Chi cục HQ CK Tà Lùng Cao Bằng','11B1',N'Chi cục HQ CK Tà Lùng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B11B01',N'Đội Nghiệp vụ 2 (thuộc HQ CK Tà Lùng)','11B2',N'Chi cục HQ CK Tà Lùng - Đội NV số 2 Nà Lạn','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B11E',N'Chi cục HQ CK Trà Lĩnh Cao Bằng','11BE  ',N'Chi cục HQ CK Trà Lĩnh','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C34E',N'Chi cục HQ CK Cảng Đà Nẵng KV II','34CE  ',N'Chi cục HQ CK Cảng Đà Nẵng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('A34B',N'Chi cục HQ Sân bay QT  Đà Nẵng','34AB  ',N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N24H',N'Chi cục HQ KCN Đà Nẵng','34NH  ',N'Chi cục HQ KCN Đà Nẵng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N34G',N'Chi cục HQ KCN Hòa khánh-Liên chiểu ĐN','34NG  ',N'Chi cục HQ KCN Hòa Khánh - Liên Chiểu','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C34C',N'Chi cục HQ CK Cảng Đà Nẵng KV I','34CC  ',N'Chi cục HQ Quản lý hàng đầu tư - gia công','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B40C',N'Chi cục HQ Buôn Mê Thuột','40BC  ',N'Chi cục HQ Buôn Mê Thuột','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B40B',N'Chi cục HQ CK BupRăng Đắc Lắc','40B1',N'Chi cục HQ CK BupRăng - Đội NV 1','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B40B',N'Chi cục HQ CK BupRăng Đắc Lắc','40B2',N'Chi cục HQ CK BupRăng - Đội NV 2','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P40D',N'Chi cục HQ Đà lạt','40D1',N'Chi cục HQ Đà Lạt - Đội Nghiệp vụ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P40D',N'Chi cục HQ Đà lạt','40D2',N'Chi cục HQ Đà Lạt - Đội Nghiệp vụ 2','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B12I',N'Chi cục HQ CK Chiềng Khương Lai Châu','12BI  ',N'Chi cục HQ CK Chiềng Khương','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B12E',N'Chi cục HQ CK Pa Háng Lai Châu','12BE  ',N'Chi cục HQ CK Lóng Sập','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B12H',N'Chi cục HQ CK Ma Lu Thàng Lai Châu','12BH  ',N'Chi cục HQ CK Ma Lu Thàng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B12H',N'Chi cục HQ CK Ma Lu Thàng Lai Châu','12BH  ',N'Chi cục HQ CK Ma Lu Thàng - Đội HQ Pô Tô','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B12B',N'Chi cục HQ CK Tây Trang Lai Châu','12BB  ',N'Chi cục HQ CK Quốc tế Tây Trang','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('12PF',N'Hải quan Thị xã Sơn La - Lai Châu','12PF  ',N'Chi cục HQ Sơn La','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('12PF',N'Hải quan Thị xã Sơn La - Lai Châu','12PF  ',N'Chi cục HQ Sơn La - Đội NV HQCK Nà Cài','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N47B',N'Chi cục HQ Biên Hoà','47NB  ',N'Chi cục HQ Biên Hoà','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N47B',N'Chi cục HQ Biên Hoà','47NB  ',N'Chi cục HQ Biên Hoà - Đội Thủ tục tàu','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('X47E',N'Chi cục HQ KCX Long Bình Đồng Nai','47XE  ',N'Chi cục HQ KCX Long Bình','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C47I  ',N'Chi cục HQ Long Bình Tân','47I1',N'Chi cục HQ Long Bình Tân - Đội nghiệp vụ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C47I  ',N'Chi cục HQ Long Bình Tân','47I2',N'Chi cục HQ Long Bình Tân - Đội nghiệp vụ 2','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C47D',N'Chi cục HQ Long Thành Đồng Nai','47D1',N'Chi cục HQ Long Thành - Đội nghiệp vụ ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C47D',N'Chi cục HQ Long Thành Đồng Nai','47D2',N'Chi cục HQ Long Thành - Đội nghiệp vụ 2','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C47D',N'Chi cục HQ Long Thành Đồng Nai','47D3',N'Chi cục HQ Long Thành - Đội nghiệp vụ 3','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N47G',N'Chi cục HQ Nhơn Trạch Đồng Nai','47NG  ',N'Chi cục HQ Nhơn Trạch','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N47M',N'Chi Cục HQ QL KCN Bình Thuận (Đồng Nai)','47NM  ',N'Chi cục HQ QL KCN Bình Thuận','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N47F',N'Chi cục HQ Thống Nhất Đồng Nai','47NF  ',N'Chi cục HQ Thống Nhất','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B49F',N'Chi cục HQ CK Thông Bình Đồng Tháp','49BF  ',N'Chi cục HQ  Thông Bình','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C49C',N'Chi cục HQ Cảng Đồng Tháp','49CC  ',N'Chi cục HQ CK Cảng Đồng Tháp','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B49G',N'Chi cục HQ Dinh Hà Đồng Tháp','49BG  ',N'Chi cục HQ CK Dinh Bà','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B49B',N'Chi cục HQ CK Thường Phước Đồng Tháp','49BB  ',N'Chi cục HQ CK Thường Phước','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B49E',N'Chi cục HQ Sở Thượng Đồng Tháp','49BE  ',N'Chi cục HQ Sở Thượng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B38C',N'HQ Cửa Khẩu Đường 18 (Gia Lai)','38BC  ',N'Chi cục HQ CK Bờ Y','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B38B',N'HQ Cửa Khẩu Đường 19 (Gia Lai)','38BB  ',N'Chi cục HQ CK Lệ Thanh','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P38D',N'Chi cục HQ Kon Tum','38PD  ',N'Chi cục HQ Kon Tum','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B10D',N'Chi cục HQ CK Phó Bảng Hà Giang','10BD  ',N'Chi cục HQ CK Phó Bảng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B10F',N'Chi cục HQ CK Săm Pun Hà Giang','10BF  ',N'Chi cục HQ CK Săm Pun','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B10B',N'Chi cục HQ CK Thanh Thủy Hà Giang','10BB  ',N'Chi cục HQ CK Thanh Thủy','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B10C',N'Chi cục HQ CK Xín Mần Hà Giang','10BC  ',N'Chi cục HQ CK Xín Mần','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('T01E',N'Chi cục HQ Bắc Hà Nội','10',N'Chi cục HQ Bắc Hà Nội - Đội Nghiệp vụ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('T01E01',N'HQ KCN Bắc Thăng Long','01NV  ',N'Chi cục HQ KCN Bắc Thăng Long','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('D01D,D01D01',N'Chi cục HQ Bưu Điện Hà Nội,HQ Mỹ Đình (thuộc HQ Bưu Điện Hà Nội)','01D1',N'Chi cục HQ Bưu Điện TP Hà Nội - HQ Mỹ Đình','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('D01D,D01D03,D01D04',N'Chi cục HQ Bưu Điện Hà Nội,HQ Bưu điện Hà Nội - FexDex, HQ Bưu điện Hà Nội - Đội xuất','01D2',N'Chi cục HQ Bưu Điện TP Hà Nội - FeDex','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('D01D,D01D01,D01D03,D01D04',N'Chi cục HQ Bưu Điện Hà Nội,HQ Mỹ Đình (thuộc HQ Bưu Điện Hà Nội), ,HQ Bưu điện Hà Nội - FexDex, HQ Bưu điện Hà Nội - Đội xuất','01D3',N'Chi cục HQ Bưu Điện TP Hà Nội - UPS','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('A01B02',N'Chi cục HQ Sân bay Nội bài-Đội Xuất','01B1',N'Chi cục HQ CK Sân bay quốc tế Nội Bài - Đội HH xuất','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('A01B04',N'Chi cục HQ CK Sân bay quốc tế Nội Bài-Đội CPN','01B2',N'Chi cục HQ CK Sân bay quốc tế Nội Bài - Đội CPN','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('A01B05',N'Chi cục HQ Sân bay Nội bài-Đội Nhập','01B3',N'Chi cục HQ CK Sân bay quốc tế Nội Bài - Đội HH nhập','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('S01I',N'Chi cục HQ Ga Yên Viên (Hà Nội)','01SI  ',N'Chi cục HQ Ga đường sắt quốc tế Yên Viên','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('A01C',N'Chi cục HQ Gia Lâm Hà Nội','01AC  ',N'Chi cục HQ Gia Lâm','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('I01K',N'Chi cục HQ Gia thụy  Hà Nội','01IK  ',N'Chi cục HQ Gia Thụy','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P01M',N'Chi cục HQ Hà Tây (Thuộc HQ Hà Nội)','01M1',N'Chi cục HQ Hà Tây','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P01M01',N'Đội Thủ Tục HQ Khu Công Nghệ Cao Hòa Lạc','01M2',N'Chi cục HQ Hà Tây - Đội TTHQ Khu CNC Hòa lạc','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P01L',N'Chi cục HQ quản lý hàng ĐT-GC Hà Nội','01PL  ',N'Chi cục HQ Quản lý hàng đầu tư - gia công','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P01J',N'HQ Việt trì (Thuộc HQ Hà Nội)','01PJ  ',N'Chi cục HQ Phú Thọ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P01O,P01O01',N'Chi cục HQ Vĩnh Phúc (Thuộc HQ Hà Nội), Hải quan Phúc Yên (Thuộc HQ Hà Nội)','01PR  ',N'Chi cục HQ Vĩnh Phúc','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P01H',N'HQ Yên bái (Thuộc HQ Hà Nội)','01BT  ',N'Chi cục HQ Yên Bái','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C30F',N'Chi cục HQ CK Cảng Vũng áng Hà Tĩnh','30F1',N'Chi cục HQ CK Cảng Vũng Áng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C30F',N'','30F2',N'Chi cục HQ CK Cảng Vũng Áng - Đội NV cảng Sơn Dương','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C30C',N'Chi cục HQ CK Cảng Xuân Hải Hà Tĩnh','30CC  ',N'Chi cục HQ CK Cảng Xuân Hải','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B30B',N'Chi cục HQ CK Cầu Treo Hà Tĩnh','30BB  ',N'Chi cục HQ CK Quốc tế Cầu Treo','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B30E',N'Chi cục HQ Hồng Lĩnh Hà Tĩnh','30BE  ',N'Chi cục HQ Hồng Lĩnh','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('E03E',N'Chi cục HQ cửa khẩu cảng Đình Vũ','03EE  ',N'Chi cục HQ CK Cảng Đình Vũ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C03C',N'Chi cục HQ CK cảng HP KV I','03CC  ',N'Chi cục HQ CK cảng Hải Phòng KV I','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C03E',N'Chi cục HQ CK cảng HP KV II','03CE  ',N'Chi cục HQ CK cảng Hải Phòng KV II','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('T03G',N'Chi cục HQ CK cảng HP KV III','03TG  ',N'Chi cục HQ CK cảng Hải Phòng KV III','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P03J',N'Chi cục HQ Hải dương','03PJ  ',N'Chi cục HQ Hải Dương','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P03L',N'Chi cục HQ Hưng yên','03PL  ',N'Chi cục HQ Hưng Yên','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N03K',N'Chi cục HQ KCX và KCN Hải Phòng','03NK  ',N'Chi cục HQ KCX và KCN','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P03A',N'Chi cục HQ quản lý hàng ĐT-GC','03PA  ',N'Chi cục HQ Quản lý hàng đầu tư - gia công - Đội TT hàng đầu tư','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P03A',N'Chi cục HQ quản lý hàng ĐT-GC','03PA  ',N'Chi cục HQ Quản lý hàng đầu tư - gia công - Đội TT hàng gia công   ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C03D',N'Chi cục HQ Thái Bình','03CD  ',N'Chi cục HQ Thái Bình','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C41C',N'Chi cục HQ Cảng Ba Ngòi Khánh Hoà','41CC  ',N'Chi cục HQ CK Cảng Cam Ranh','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C41B',N'Chi cục HQ Cảng Nha Trang Khánh Hoà','41CB  ',N'Chi cục HQ CK Cảng Nha Trang','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P41E',N'Chi cục HQ Văn Phong Khánh Hoà','41PE  ',N'Chi cục HQ Vân Phong','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C53D',N'Chi cục HQ CK Cảng Hòn Chông Kiên Giang','53CD  ',N'Chi cục HQ CK Cảng Hòn Chông','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B53C',N'Chi cục HQ CK Xà Xía Kiên Giang','53BC  ',N'Chi cục HQ CK Quốc Tế Hà Tiên','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C53H',N'Chi cục HQ Phú Quốc','53CH  ',N'Chi cục HQ Phú Quốc','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B15C',N'Chi cục HQ CK Chi Ma Lạng Sơn','15BC  ',N'Chi cục HQ CK Chi Ma','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B15B',N'Chi cục HQ CK Hữu Nghị Lạng Sơn','15B1',N'Chi cục HQ CK Hữu Nghị','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B15B',N'Chi cục HQ CK Hữu Nghị Lạng Sơn','15B2',N'Chi cục HQ CK Hữu Nghị - Đội NV Co Sâu ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B15D',N'Chi cục HQ  Cốc Nam Lạng Sơn','15BD  ',N'Chi cục HQ Cốc Nam','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('S15I',N'Chi cục HQ Ga đường sắt QT Đồng Đăng','15SI  ',N'Chi cục HQ Ga Đồng Đăng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B15E',N'Chi cục HQ Tân Thanh Lạng Sơn','150',N'Chi cục HQ Tân Thanh - Đội NV Na Hình','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B15E',N'Chi cục HQ Tân Thanh Lạng Sơn','1500',N'Chi cục HQ Tân Thanh - Đội NV Nà Nưa','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B15E',N'Chi cục HQ Tân Thanh Lạng Sơn','15000',N'Chi cục HQ Tân Thanh - Đội NV Bình Nghi','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B15E',N'Chi cục HQ Tân Thanh Lạng Sơn','150000',N'Chi cục HQ Tân Thanh - Đội NV Tân Thanh','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B13D',N'Chi cục HQ Bát Xát Lao Cai','13BD  ',N'Chi cục HQ CK Bát Xát','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B13C',N'Chi cục HQ CK Mường Khương Lao Cai','13BC  ',N'Chi cục HQ CK Mường Khương','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B13B',N'Chi cục HQ CK Lao Cai','13BB  ',N'Chi cục HQ CK Quốc tế Lào Cai - Đội Thủ tục HH XNK 1','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B13B',N'Chi cục HQ CK Lao Cai','13BB  ',N'Chi cục HQ CK Quốc tế Lào Cai - Đội Thủ tục HH XNK 2','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('S13G',N'Chi cục HQ ga Đường Sắt Lao Cai','13SG  ',N'Chi cục HQ Đường sắt LVQT Lào Cai','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B48F',N'Chi cục HQ  Bến Lức','48F1',N'Chi cục HQ Bến Lức - Đội NV KCN Long Hậu','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B48F',N'Chi cục HQ  Bến Lức','48F2',N'Chi cục HQ Bến Lức - Đội TT- Chi cục HQ Bến Lức','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C48G',N'Chi cục HQ Cảng Mỹ Tho Long An','48CG  ',N'Chi cục HQ CK Cảng Mỹ Tho','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B48C',N'Chi cục HQ Mỹ Quý Tây Long An','48BC  ',N'Chi cục HQ CK Mỹ Quý Tây','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B48D',N'Chi cục HQ Bình Hiệp Long An','48BD  ',N'Chi cục HQ CK Quốc tế Bình Hiệp','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B48I',N'CCHQ Đức Hoà (Long An)','48BI  ',N'Chi cục HQ Đức Hòa','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B48E',N'Chi cục HQ  Hưng Điền Long An','48BE  ',N'Chi cục HQ Hưng Điền','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C29C',N'Chi cục HQ CK Cảng Nghệ An','29CC  ',N'Chi cục HQ CK Cảng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B29B',N'Chi cục HQ CK Nậm Cắn Nghệ An','29BB  ',N'Chi cục HQ CK Quốc tế Nậm Cắn','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P29F',N'Chi cục HQ Vinh Nghệ An','29PF  ',N'Chi cục HQ Vinh','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B31F',N'Chi cục HQ CK Cà Roòng Quảng Bình','31BF  ',N'Chi cục HQ CK Cà Roòng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C31D',N'Chi cục HQ CK Cảng Gianh Quảng Bình','31D1',N'Chi cục HQ CK Cảng Hòn La - Đội NV Đồng Hới  ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C31D',N'Chi cục HQ CK Cảng Gianh Quảng Bình','31D2',N'Chi cục HQ CK Cảng Hòn La - Đội NV Cảng Hòn La','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C31D01',N'Đội thủ tục HQ CK Cảng Gianh Quảng Bình','31D3',N'Chi cục HQ CK Cảng Hòn La - Đội NV Cảng Gianh ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B31B',N'Chi cục HQ CK Cha Lo Quảng Bình','31BB  ',N'Chi cục HQ CK Cha Lo','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C60B',N'Chi cục HQ CK Cảng Kỳ Hà','60CB  ',N'Chi cục HQ CK Cảng Kỳ Hà','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B60D',N'Chi cục Hải quan Nam giang','60BD  ',N'Chi cục HQ CK Nam Giang','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N60C',N'Chi cục HQ KCN Điện Nam - Điện Ngọc','60NC  ',N'Chi cục HQ KCN Điện Nam - Điện Ngọc','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N35C',N'Chi cục HQ các KCN Quảng Ngãi','35NC  ',N'Chi cục HQ các KCN Quảng Ngãi','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C35B',N'Chi cục HQ CK Cảng Dung Quất','35CB  ',N'Chi cục HQ CK Cảng Dung Quất','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B20D',N'Chi cục HQ Bắc Phong Sinh Quảng Ninh','20BD  ',N'Chi cục HQ Bắc Phong Sinh','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C20D',N'HQ Cảng Biển Cái Lân (Quảng Ninh)','20CD  ',N'Chi cục HQ CK Cảng Cái Lân','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C20G',N'Chi cục HQ CK Cảng Cẩm Phả','20CG  ',N'Chi cục HQ CK Cảng Cẩm Phả','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C20F',N'Chi cục HQ CK Cảng Hòn Gai','20CF  ',N'Chi cục HQ CK Cảng Hòn Gai','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B20C',N'Chi cục HQ CK Hoành Mô Quảng Ninh','20BC  ',N'Chi cục HQ CK Hoành Mô','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B20B,B20B01',N'Chi cục HQ CK Móng Cái Quảng Ninh, HQ Cửa khẩu Bắc Luân','20B1',N'Chi cục HQ CK Móng Cái - HQ Cửa khẩu Bắc Luân ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B20B02',N'HQ Cửa khẩu Ka Long','20B2',N'Chi cục HQ CK Móng Cái - HQ Cửa khẩu Ka Long  ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C20E',N'Chi cục HQ Vạn Gia Quảng Ninh','20CE  ',N'Chi cục HQ Vạn Gia','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C32D',N'Chi cục HQ CK Cảng Cửa Việt Quảng Trị','32CD  ',N'Chi cục HQ CK Cảng Cửa Việt','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B32C',N'Chi cục HQ CK La Lay Quảng Trị','32BC  ',N'Chi cục HQ CK La Lay','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B32B',N'Chi cục HQ CK Lao Bảo Quảng Bình','32BB  ',N'Chi cục HQ CK Lao Bảo','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B32D',N'Chi cục HQ Khu Thương mại Lao Bảo','32BD  ',N'Chi cục HQ Khu thương mại Lao Bảo','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('V30G',N'Đội Kiểm soát HQ Hà Tĩnh','32VG  ',N'Đội Kiẻm soát HQ Quảng Trị','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B45E',N'Chi cục HQ CK Katum Tây Ninh','45BE  ',N'Chi cục HQ CK Kà Tum','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B45B',N'Chi cục HQ CK Mộc Bài Tây Ninh','45BB  ',N'Chi cục HQ CK Mộc Bài','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B45C',N'Chi cục HQ CK Xa Mát Tây Ninh','45BC  ',N'Chi cục HQ CK Xa Mát','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N45F',N'Chi cục HQ KCN Trảng Bàng Tây Ninh','45F1',N'Chi cục HQ KCN Trảng Bàng - Đội TTHQ KCX và CN Linh Trung 3','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N45F',N'Chi cục HQ KCN Trảng Bàng Tây Ninh','45F2',N'Chi cục HQ KCN Trảng Bàng - Đội TTHQ KCN Phước Đông','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B45D',N'Chi cục HQ Phước Tân Tây Ninh','45BD  ',N'Chi cục HQ Phước Tân','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C27F',N'Chi cục HQ Cảng Thanh Hoá','27CF  ',N'Chi cục HQ CK Cảng Thanh Hóa','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B27B',N'Chi cục HQ CK Na Mèo Thanh Hoá','27B1',N'Chi cục HQ CK Quốc tế Na Mèo','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('B27B',N'Chi cục HQ CK Na Mèo Thanh Hoá','27B2',N'Chi cục HQ CK Quốc tế Na Mèo - Đội TTHQ CK Tén Tằn','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P27E',N'Chi cục Hải quan Nam Định','27PE  ',N'Chi cục HQ Nam Định','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P27C',N'Chi cục Hải quan Ninh Bình','27PC  ',N'Chi cục HQ Ninh Bình','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('N27J',N'Chi Cục HQ KCN Hà Nam(Thuộc HQThanh Hóa)','27NJ  ',N'Chi cục HQ Quản lý các KCN Hà Nam','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C33F',N'Chi Cục HQ CK Cảng Chân Mây TT Huế','33CF  ',N'Chi cục HQ CK Cảng Chân Mây','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C33C',N'Chi cục HQ Cảng Thuận An TT Huế','33CC  ',N'Chi cục HQ CK Cảng Thuận An','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P33D',N'Chi cục Hải quan Thuỷ An','33PD  ',N'Chi cục HQ Thủy An','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('D02S',N'Chi cục HQ chuyển phát nhanh','02DS  ',N'Chi cục HQ Chuyển phát nhanh','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C02V',N'Chi Cục HQ CK Cảng Hiệp Phước (HCM)','02CV  ',N'Chi cục HQ CK Cảng Hiệp Phước','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C02I,C02I01,C02I02',N'Chi cục HQ CK Cảng Sài Gòn khu vực I, HQ Cảng Cát Lái (HQ Cảng Saigon KV I), HQ Cảng Cát Lái','02CI  ',N'Chi cục HQ CK Cảng Sài Gòn KV I','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C02C',N'Chi cục HQ CK Cảng Sài Gòn khu vực II','02CC  ',N'Chi cục HQ CK Cảng Sài Gòn KV II','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C02H,C02H01',N'Chi cục HQ CK Cảng Sài Gòn khu vực III','02CH  ',N'Chi cục HQ CK Cảng Sài Gòn KV III','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('I02K, I02K02',N'Chi cục HQ CK Cảng Sài Gòn khu vực IV (ICD Tanamexco)','02K1',N'Chi cục HQ CK Cảng Sài Gòn KV IV - ICD Tanamexco','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('I02K, I02K03',N'Chi cục HQ CK Cảng Sài Gòn khu vực IV, Chi cục HQCK Cảng SG KV4/ICD-3/Transimex','02K2',N'Chi cục HQ CK Cảng Sài Gòn KV IV - ICD Transimex','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('I02K,I02K04',N'Chi cục HQ CK Cảng Sài Gòn khu vực IV, Chi Cục HQ KV IV (ICD Gemadept)','02K3',N'Chi cục HQ CK Cảng Sài Gòn KV IV - ICD Sotrans','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('A02B  ',N'Chi cục HQ Sân bay quốc tế Tân Sơn Nhất','02B1',N'Chi cục HQ CK Sân bay Quốc tế Tân Sơn Nhất - Đội Thủ tục','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('A02B04',N'Chi cục HQ Tân Sơn Nhất - Nhà Ga Hàng Hóa','02B2',N'Chi cục HQ CK Sân bay Quốc tế Tân Sơn Nhất - Nhà ga Hàng hóa','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('C02X',N'Chi cục HQ cửa khẩu Tân Cảng (TPHCM)','02CX  ',N'Chi cục HQ CK Tân Cảng','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('X02F',N'Chi cục HQ KCX Linh Trung (Hồ Chí Minh)','02F1',N'Chi cục HQ KCX Linh Trung - HQ KCX Linh Trung I','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('X02F01',N'HQ KCX Linh Trung II (Hồ Chí Minh)','02F2',N'Chi cục HQ KCX Linh Trung - HQ KCX Linh Trung II','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('X02F02',N'HQ KCX Linh Trung - Khu Công nghệ cao','02F3',N'Chi cục HQ KCX Linh Trung - HQ KCX Linh Trung - Khu CNC    ','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('X02E',N'Chi cục HQ KCX Tân Thuận (Hồ Chí Minh)','02XE  ',N'Chi cục HQ KCX Tân Thuận','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P02G',N'Chi cục HQ quản lý hàng đầu tư HCM','02PG  ',N'Chi cục HQ Quản lý hàng đầu tư','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('P02J',N'Chi cục HQ quản lý hàng gia công HCM','02PJ  ',N'Chi cục HQ Quản lý hàng gia công','MaHQ',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NKD01,NKD02,NKD03,NKD04,NKD06,NKD11,NKD13,NKD20,NGC03,NDT03',N'Nhập Kinh Doanh, Nhập Dầu khí, Nhập Kinh Doanh Đá Quí, Nhập Kinh Doanh Gắn máy, Nhập Kinh Doanh Ô tô, Nhập Kinh doanh Tại chỗ, Nhập kinh doanh nội địa vào khu TM, Nhập kinh doanh biên giới, Nhập Gia Công Kinh doanh, Nhập đầu tư ô tô','A11',N'Nhập kinh doanh tiêu dùng','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NKD01,NKD08',N'Nhập Kinh Doanh, Nhập kinh doanh chuyển mục đích','A21',N'Chuyển tiêu thụ nội địa từ nguồn tạm nhập','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NKD19,NGC23,NGC25,NGC99,NSX08,NDT05,NDT19,NTA25,NCX05',N'Nhập hàng XKbị trả lại, Nhập hàng xuất gia công bị trả lại, Nhập trả nguyên liệu không dùng hết, Nhập gia công tạm nhập tái chế, Nhập trả hàng xuất sản xuất xuất khẩu, Nhập đầu tư sửa chữa tái chế, Nhập trả hàng đầu tư đã xuất khẩu, Tạm nhập tái chế, Nhập trả hàng xuất chế xuất','A31',N'Nhập hàng xuất khẩu bị trả lại','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NKD05',N'Nhập Đầu Tư Kinh doanh','A41',N'Nhập kinh doanh của doanh nghiệp đầu tư','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NTA01,NTA24,NTA29',N'Tạm Nhập Tái Xuất (Nhập Phải Tái Xuất), Tạm nhập xăng dầu, Tạm nhập hoán đổi xăng dầu tái xuất','G11',N'Tạm nhập hàng kinh doanh tạm nhập tái xuất','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NTA11,NTA15,NTA03',N'Nhập Đầu Tư Tạm nhập thi công, Nhập Đầu Tư Tạm nhập, Tạm nhập tàu biển','G12',N'Tạm nhập máy móc, thiết bị phục vụ thực hiện các dự án có thời hạn','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NGC04,NGC02,NGC13,NGC20,NTA14,NTA15,NGC07,NTA16,NTA19',N'Nhập Gia Công Tạm nhập, Nhập Đầu Tư Gia công, Nhập Gia công Tại chỗ, Nhập gia công chuyển tiếp TB, Nhập triển lãm, hàng mẫu, quảng cáo…, Nhập đầu tư tạm nhập, Hàng hóa tạm nhập vào Khu chế xuất, Nhập đầu tư kinh doanh cửa hàng miễn thuế, Nhập hàng bán tại cửa hàng miễn thuế','G13',N'Tạm nhập hàng miễn thuế','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NTA07,NTA08',N'Tạm nhập ủy thác, Tạm nhập viện trợ','G14',N'Tạm nhập khác','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('NGC24,NGC06,NDT19,NTA02,NTA04,NTA05',N'Nhập máy móc thiết bị tái nhập, Hàng hóa tái nhập vào Khu chế xuất, Nhập trả hàng đầu tư đã xuất khẩu, Tái nhập, Nhập đầu tư tái nhập, Tái nhập hàng xuất triển lãm','G51',N'Tái nhập hàng đã tạm xuất','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XKD01,XKD02,XKD03,XKD04,XKD06,XKD11,XKD09,XKD10,XGC03,XGC08,XSX04,XDT08,XDT09,XDT10',N'Xuất Kinh Doanh, Xuất kinh doanh dầu khí, Xuất kinh doanh đá quý, Xuất kinh doanh gắn máy, Xuất kinh doanh ô tô, Nhập kinh doanh tại chỗ, Xuất kinh doanh từ nội địa vào Khu thương mại, Xuất kinh doanh biên giới, Xuất gia công kinh doanh, Hàng trong nội địa bán cho Khu chế xuất,Khu chế xuất bán hàng vào nội địa để Sản xuất xuất khẩu, Xuất đầu tư GC khu CN, Xuất đầu tư kinh doanh Khu công nghiệp, Xuất đầu tư tại chỗ','B11',N'Xuất kinh doanh; xuất khẩu của doanh ngiệp đầu tư','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XKD14,XGC21,XSX08,XCX05,XDT11,XTA02',N'Xuất KD giữa các Khu phi thuế quan, Xuất trả hàng gia công nhập khẩu, Xuất trả hàng nhập khẩu SXXK, Xuất trả hàng nhập chế xuất, Xuất trả hàng đầu tư đã nhập khẩu, Tái Xuất','B13',N'Xuất trả hàng đã nhập khẩu','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XGC01',N'Xuất Gia Công','E52',N'Xuất sản phẩm gia công cho thương nhân nước ngoài','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XTA20,XTA16,XTA29',N'Tái xuất ( Hàng tạm nhập tái xuất), Tái Xuất Xăng Dầu, Tái xuất hoán đổi xăng dầu tái xuất','G21',N'Tái xuất hàng kinh doanh tạm nhập tái xuất','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XGC13,XGC20,XGC04,XTA14,XGC06,XTA17,XTA18',N'Xuất gia công tại chổ, Xuất gia công chuyển tiếp TB, Xuất Gia Công Tái xuất, Xuất triển lãm, hàng mẫu, quảng cáo…, Hàng hóa tái xuất hàng hóa ra nước ngoài từ Khu chế xuất, Xuất hàng bán tại cửa hàng miễn thuế, Tái xuất hàng bán tại cửa hàng miễn thuế','G23',N'Tái xuất hàng miễn thuế tạm nhập','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XTA08',N'Tạm XUẤT viện trợ','G24',N'Tái xuất khác','MaLoaiHinh',N'')
INSERT INTO t_VNACCS_Mapper VALUES('XGC24,XGC07,XTA15,XTA01',N'Xuất máy móc thiết bị ra nước ngoài thực hiện hợp đồng gia công, Hàng hóa tạm xuất ra nước ngoài (vào nội địa) từ Khu chế xuất, Xuất đầu tư tạm xuất, Tạm xuất','G61',N'Tạm xuất hàng hóa','MaLoaiHinh',N'')

------------------------ cap nhat phan loai gia hoa don
update t_VNACC_Category_Common set Name_VN=N'Giá hóa đơn cho hàng hóa phải trả tiền' where ReferenceDB='E010' and Code='A'
update t_VNACC_Category_Common set Name_VN=N'Giá hóa đơn cho hàng hóa không phải trả tiền (F.O.C)' where ReferenceDB='E010' and Code='B'
update t_VNACC_Category_Common set Name_VN=N'Giá hóa đơn cho hàng hóa bao gồm phải trả tiền và không trả tiền' where ReferenceDB='E010' and Code='C'
update t_VNACC_Category_Common set Name_VN=N'Các trường hợp khác' where ReferenceDB='E010' and Code='D'

------------------------ cap nhat t_kdt_VNACC_Chungtudinhkem

Alter table t_KDT_VNACC_ChungTuDinhKem 
alter column SoTiepNhanToKhaiPhoThong numeric (12,0)

Alter table t_KDT_VNACC_ChungTuDinhKem 
alter column TongDungLuong numeric (18,2)

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 2),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(12, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChungTuDinhKem]
(
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
)
VALUES 
(
	@LoaiChungTu,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@TrangThaiXuLy,
	@CoQuanHaiQuan,
	@NhomXuLyHoSo,
	@TieuDe,
	@SoToKhai,
	@GhiChu,
	@PhanLoaiThuTucKhaiBao,
	@SoDienThoaiNguoiKhaiBao,
	@SoQuanLyTrongNoiBoDoanhNghiep,
	@MaKetQuaXuLy,
	@TenThuTucKhaiBao,
	@NgayKhaiBao,
	@TrangThaiKhaiBao,
	@NgaySuaCuoiCung,
	@TenNguoiKhaiBao,
	@DiaChiNguoiKhaiBao,
	@SoDeLayTepDinhKem,
	@NgayHoanThanhKiemTraHoSo,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TongDungLuong,
	@PhanLuong,
	@HuongDan,
	@TKMD_ID,
	@NgayBatDau,
	@NgayCapNhatCuoiCung,
	@CanCuPhapLenh,
	@CoBaoXoa,
	@GhiChuHaiQuan,
	@HinhThucTimKiem,
	@LyDoChinhSua,
	@MaDiaDiemDen,
	@MaNhaVanChuyen,
	@MaPhanLoaiDangKy,
	@MaPhanLoaiXuLy,
	@MaThongTinXuat,
	@NgayCapPhep,
	@NgayThongBao,
	@NguoiGui,
	@NoiDungChinhSua,
	@PhuongTienVanChuyen,
	@SoChuyenDiBien,
	@SoSeri,
	@SoTiepNhanToKhaiPhoThong,
	@TrangThaiXuLyHaiQuan,
	@NguoiCapNhatCuoiCung,
	@NhomXuLyHoSoID
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 2),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(12, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
SET
	[LoaiChungTu] = @LoaiChungTu,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[NhomXuLyHoSo] = @NhomXuLyHoSo,
	[TieuDe] = @TieuDe,
	[SoToKhai] = @SoToKhai,
	[GhiChu] = @GhiChu,
	[PhanLoaiThuTucKhaiBao] = @PhanLoaiThuTucKhaiBao,
	[SoDienThoaiNguoiKhaiBao] = @SoDienThoaiNguoiKhaiBao,
	[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[TenThuTucKhaiBao] = @TenThuTucKhaiBao,
	[NgayKhaiBao] = @NgayKhaiBao,
	[TrangThaiKhaiBao] = @TrangThaiKhaiBao,
	[NgaySuaCuoiCung] = @NgaySuaCuoiCung,
	[TenNguoiKhaiBao] = @TenNguoiKhaiBao,
	[DiaChiNguoiKhaiBao] = @DiaChiNguoiKhaiBao,
	[SoDeLayTepDinhKem] = @SoDeLayTepDinhKem,
	[NgayHoanThanhKiemTraHoSo] = @NgayHoanThanhKiemTraHoSo,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TongDungLuong] = @TongDungLuong,
	[PhanLuong] = @PhanLuong,
	[HuongDan] = @HuongDan,
	[TKMD_ID] = @TKMD_ID,
	[NgayBatDau] = @NgayBatDau,
	[NgayCapNhatCuoiCung] = @NgayCapNhatCuoiCung,
	[CanCuPhapLenh] = @CanCuPhapLenh,
	[CoBaoXoa] = @CoBaoXoa,
	[GhiChuHaiQuan] = @GhiChuHaiQuan,
	[HinhThucTimKiem] = @HinhThucTimKiem,
	[LyDoChinhSua] = @LyDoChinhSua,
	[MaDiaDiemDen] = @MaDiaDiemDen,
	[MaNhaVanChuyen] = @MaNhaVanChuyen,
	[MaPhanLoaiDangKy] = @MaPhanLoaiDangKy,
	[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
	[MaThongTinXuat] = @MaThongTinXuat,
	[NgayCapPhep] = @NgayCapPhep,
	[NgayThongBao] = @NgayThongBao,
	[NguoiGui] = @NguoiGui,
	[NoiDungChinhSua] = @NoiDungChinhSua,
	[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
	[SoChuyenDiBien] = @SoChuyenDiBien,
	[SoSeri] = @SoSeri,
	[SoTiepNhanToKhaiPhoThong] = @SoTiepNhanToKhaiPhoThong,
	[TrangThaiXuLyHaiQuan] = @TrangThaiXuLyHaiQuan,
	[NguoiCapNhatCuoiCung] = @NguoiCapNhatCuoiCung,
	[NhomXuLyHoSoID] = @NhomXuLyHoSoID
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 2),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(12, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChungTuDinhKem] 
		SET
			[LoaiChungTu] = @LoaiChungTu,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[NhomXuLyHoSo] = @NhomXuLyHoSo,
			[TieuDe] = @TieuDe,
			[SoToKhai] = @SoToKhai,
			[GhiChu] = @GhiChu,
			[PhanLoaiThuTucKhaiBao] = @PhanLoaiThuTucKhaiBao,
			[SoDienThoaiNguoiKhaiBao] = @SoDienThoaiNguoiKhaiBao,
			[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[TenThuTucKhaiBao] = @TenThuTucKhaiBao,
			[NgayKhaiBao] = @NgayKhaiBao,
			[TrangThaiKhaiBao] = @TrangThaiKhaiBao,
			[NgaySuaCuoiCung] = @NgaySuaCuoiCung,
			[TenNguoiKhaiBao] = @TenNguoiKhaiBao,
			[DiaChiNguoiKhaiBao] = @DiaChiNguoiKhaiBao,
			[SoDeLayTepDinhKem] = @SoDeLayTepDinhKem,
			[NgayHoanThanhKiemTraHoSo] = @NgayHoanThanhKiemTraHoSo,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TongDungLuong] = @TongDungLuong,
			[PhanLuong] = @PhanLuong,
			[HuongDan] = @HuongDan,
			[TKMD_ID] = @TKMD_ID,
			[NgayBatDau] = @NgayBatDau,
			[NgayCapNhatCuoiCung] = @NgayCapNhatCuoiCung,
			[CanCuPhapLenh] = @CanCuPhapLenh,
			[CoBaoXoa] = @CoBaoXoa,
			[GhiChuHaiQuan] = @GhiChuHaiQuan,
			[HinhThucTimKiem] = @HinhThucTimKiem,
			[LyDoChinhSua] = @LyDoChinhSua,
			[MaDiaDiemDen] = @MaDiaDiemDen,
			[MaNhaVanChuyen] = @MaNhaVanChuyen,
			[MaPhanLoaiDangKy] = @MaPhanLoaiDangKy,
			[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
			[MaThongTinXuat] = @MaThongTinXuat,
			[NgayCapPhep] = @NgayCapPhep,
			[NgayThongBao] = @NgayThongBao,
			[NguoiGui] = @NguoiGui,
			[NoiDungChinhSua] = @NoiDungChinhSua,
			[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
			[SoChuyenDiBien] = @SoChuyenDiBien,
			[SoSeri] = @SoSeri,
			[SoTiepNhanToKhaiPhoThong] = @SoTiepNhanToKhaiPhoThong,
			[TrangThaiXuLyHaiQuan] = @TrangThaiXuLyHaiQuan,
			[NguoiCapNhatCuoiCung] = @NguoiCapNhatCuoiCung,
			[NhomXuLyHoSoID] = @NhomXuLyHoSoID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChungTuDinhKem]
		(
			[LoaiChungTu],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[TrangThaiXuLy],
			[CoQuanHaiQuan],
			[NhomXuLyHoSo],
			[TieuDe],
			[SoToKhai],
			[GhiChu],
			[PhanLoaiThuTucKhaiBao],
			[SoDienThoaiNguoiKhaiBao],
			[SoQuanLyTrongNoiBoDoanhNghiep],
			[MaKetQuaXuLy],
			[TenThuTucKhaiBao],
			[NgayKhaiBao],
			[TrangThaiKhaiBao],
			[NgaySuaCuoiCung],
			[TenNguoiKhaiBao],
			[DiaChiNguoiKhaiBao],
			[SoDeLayTepDinhKem],
			[NgayHoanThanhKiemTraHoSo],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TongDungLuong],
			[PhanLuong],
			[HuongDan],
			[TKMD_ID],
			[NgayBatDau],
			[NgayCapNhatCuoiCung],
			[CanCuPhapLenh],
			[CoBaoXoa],
			[GhiChuHaiQuan],
			[HinhThucTimKiem],
			[LyDoChinhSua],
			[MaDiaDiemDen],
			[MaNhaVanChuyen],
			[MaPhanLoaiDangKy],
			[MaPhanLoaiXuLy],
			[MaThongTinXuat],
			[NgayCapPhep],
			[NgayThongBao],
			[NguoiGui],
			[NoiDungChinhSua],
			[PhuongTienVanChuyen],
			[SoChuyenDiBien],
			[SoSeri],
			[SoTiepNhanToKhaiPhoThong],
			[TrangThaiXuLyHaiQuan],
			[NguoiCapNhatCuoiCung],
			[NhomXuLyHoSoID]
		)
		VALUES 
		(
			@LoaiChungTu,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@TrangThaiXuLy,
			@CoQuanHaiQuan,
			@NhomXuLyHoSo,
			@TieuDe,
			@SoToKhai,
			@GhiChu,
			@PhanLoaiThuTucKhaiBao,
			@SoDienThoaiNguoiKhaiBao,
			@SoQuanLyTrongNoiBoDoanhNghiep,
			@MaKetQuaXuLy,
			@TenThuTucKhaiBao,
			@NgayKhaiBao,
			@TrangThaiKhaiBao,
			@NgaySuaCuoiCung,
			@TenNguoiKhaiBao,
			@DiaChiNguoiKhaiBao,
			@SoDeLayTepDinhKem,
			@NgayHoanThanhKiemTraHoSo,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TongDungLuong,
			@PhanLuong,
			@HuongDan,
			@TKMD_ID,
			@NgayBatDau,
			@NgayCapNhatCuoiCung,
			@CanCuPhapLenh,
			@CoBaoXoa,
			@GhiChuHaiQuan,
			@HinhThucTimKiem,
			@LyDoChinhSua,
			@MaDiaDiemDen,
			@MaNhaVanChuyen,
			@MaPhanLoaiDangKy,
			@MaPhanLoaiXuLy,
			@MaThongTinXuat,
			@NgayCapPhep,
			@NgayThongBao,
			@NguoiGui,
			@NoiDungChinhSua,
			@PhuongTienVanChuyen,
			@SoChuyenDiBien,
			@SoSeri,
			@SoTiepNhanToKhaiPhoThong,
			@TrangThaiXuLyHaiQuan,
			@NguoiCapNhatCuoiCung,
			@NhomXuLyHoSoID
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]	

GO


---- Cap nha Dieu kien gia hoa don E011-------------------------

update t_VNACC_Category_Common set Name_VN=N'Tiền hàng và cước phí' where ReferenceDB ='E011' and Code='C&F'
update t_VNACC_Category_Common set Name_VN=N'Tiền hàng và phí bảo hiểm' where ReferenceDB ='E011' and Code='C&I'
update t_VNACC_Category_Common set Name_VN=N'Tiền hàng và cước phí' where ReferenceDB ='E011' and Code='CFR'
update t_VNACC_Category_Common set Name_VN=N'Tiền hàng, bảo hiểm và cước phí' where ReferenceDB ='E011' and Code='CIF'
update t_VNACC_Category_Common set Name_VN=N'Cước phí và bảo hiểm trả tới' where ReferenceDB ='E011' and Code='CIP'
update t_VNACC_Category_Common set Name_VN=N'Cước phí trả tới' where ReferenceDB ='E011' and Code='CPT'
update t_VNACC_Category_Common set Name_VN=N'Giao tại biên giới' where ReferenceDB ='E011' and Code='DAF'
update t_VNACC_Category_Common set Name_VN=N'Giao hàng tại chỗ' where ReferenceDB ='E011' and Code='DAP'
update t_VNACC_Category_Common set Name_VN=N'Giao tại cảng' where ReferenceDB ='E011' and Code='DAT'
update t_VNACC_Category_Common set Name_VN=N'Giao hàng đã nộp thuế' where ReferenceDB ='E011' and Code='DDP'
update t_VNACC_Category_Common set Name_VN=N'Giao hàng chưa nộp thuế' where ReferenceDB ='E011' and Code='DDU'
update t_VNACC_Category_Common set Name_VN=N'Giao tại cầu cảng' where ReferenceDB ='E011' and Code='DEQ'
update t_VNACC_Category_Common set Name_VN=N'Giao tại tàu' where ReferenceDB ='E011' and Code='DES'
update t_VNACC_Category_Common set Name_VN=N'Giao tại xưởng' where ReferenceDB ='E011' and Code='EXW'
update t_VNACC_Category_Common set Name_VN=N'Giao dọc mạn tàu' where ReferenceDB ='E011' and Code='FAS'
update t_VNACC_Category_Common set Name_VN=N'Giao cho người chuyên chở' where ReferenceDB ='E011' and Code='FCA'
update t_VNACC_Category_Common set Name_VN=N'Giao trên tàu' where ReferenceDB ='E011' and Code='FOB'












GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '15.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('15.4', GETDATE(), N'cap nhat table : t_kdt_VANCC_ChiThiHaiQuan,t_VNACCS_Mapper ')
END	

