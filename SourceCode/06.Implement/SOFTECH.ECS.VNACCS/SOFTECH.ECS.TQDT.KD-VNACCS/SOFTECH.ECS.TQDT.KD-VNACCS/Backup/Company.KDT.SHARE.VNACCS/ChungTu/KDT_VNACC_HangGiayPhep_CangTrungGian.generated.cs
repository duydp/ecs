using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HangGiayPhep_CangTrungGian : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long GiayPhep_ID { set; get; }
		public string TenHangCangTrungGian { set; get; }
		public string TenCangTrungGian { set; get; }
		public decimal SoLuongHangCangTrungGian { set; get; }
		public string DonViTinhSoLuongHangCangTrungGian { set; get; }
		public decimal KhoiLuongHangCangTrungGian { set; get; }
		public string DonViTinhKhoiLuongHangCangTrungGian { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_HangGiayPhep_CangTrungGian> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_HangGiayPhep_CangTrungGian> collection = new List<KDT_VNACC_HangGiayPhep_CangTrungGian>();
			while (reader.Read())
			{
				KDT_VNACC_HangGiayPhep_CangTrungGian entity = new KDT_VNACC_HangGiayPhep_CangTrungGian();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiayPhep_ID"))) entity.GiayPhep_ID = reader.GetInt64(reader.GetOrdinal("GiayPhep_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangCangTrungGian"))) entity.TenHangCangTrungGian = reader.GetString(reader.GetOrdinal("TenHangCangTrungGian"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCangTrungGian"))) entity.TenCangTrungGian = reader.GetString(reader.GetOrdinal("TenCangTrungGian"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongHangCangTrungGian"))) entity.SoLuongHangCangTrungGian = reader.GetDecimal(reader.GetOrdinal("SoLuongHangCangTrungGian"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViTinhSoLuongHangCangTrungGian"))) entity.DonViTinhSoLuongHangCangTrungGian = reader.GetString(reader.GetOrdinal("DonViTinhSoLuongHangCangTrungGian"));
				if (!reader.IsDBNull(reader.GetOrdinal("KhoiLuongHangCangTrungGian"))) entity.KhoiLuongHangCangTrungGian = reader.GetDecimal(reader.GetOrdinal("KhoiLuongHangCangTrungGian"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViTinhKhoiLuongHangCangTrungGian"))) entity.DonViTinhKhoiLuongHangCangTrungGian = reader.GetString(reader.GetOrdinal("DonViTinhKhoiLuongHangCangTrungGian"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_HangGiayPhep_CangTrungGian> collection, long id)
        {
            foreach (KDT_VNACC_HangGiayPhep_CangTrungGian item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangGiayPhep_CangTrungGian VALUES(@GiayPhep_ID, @TenHangCangTrungGian, @TenCangTrungGian, @SoLuongHangCangTrungGian, @DonViTinhSoLuongHangCangTrungGian, @KhoiLuongHangCangTrungGian, @DonViTinhKhoiLuongHangCangTrungGian, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_HangGiayPhep_CangTrungGian SET GiayPhep_ID = @GiayPhep_ID, TenHangCangTrungGian = @TenHangCangTrungGian, TenCangTrungGian = @TenCangTrungGian, SoLuongHangCangTrungGian = @SoLuongHangCangTrungGian, DonViTinhSoLuongHangCangTrungGian = @DonViTinhSoLuongHangCangTrungGian, KhoiLuongHangCangTrungGian = @KhoiLuongHangCangTrungGian, DonViTinhKhoiLuongHangCangTrungGian = @DonViTinhKhoiLuongHangCangTrungGian, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangGiayPhep_CangTrungGian WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangCangTrungGian", SqlDbType.NVarChar, "TenHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCangTrungGian", SqlDbType.NVarChar, "TenCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongHangCangTrungGian", SqlDbType.Decimal, "SoLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhSoLuongHangCangTrungGian", SqlDbType.VarChar, "DonViTinhSoLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhoiLuongHangCangTrungGian", SqlDbType.Decimal, "KhoiLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhKhoiLuongHangCangTrungGian", SqlDbType.VarChar, "DonViTinhKhoiLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangCangTrungGian", SqlDbType.NVarChar, "TenHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCangTrungGian", SqlDbType.NVarChar, "TenCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongHangCangTrungGian", SqlDbType.Decimal, "SoLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhSoLuongHangCangTrungGian", SqlDbType.VarChar, "DonViTinhSoLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhoiLuongHangCangTrungGian", SqlDbType.Decimal, "KhoiLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhKhoiLuongHangCangTrungGian", SqlDbType.VarChar, "DonViTinhKhoiLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangGiayPhep_CangTrungGian VALUES(@GiayPhep_ID, @TenHangCangTrungGian, @TenCangTrungGian, @SoLuongHangCangTrungGian, @DonViTinhSoLuongHangCangTrungGian, @KhoiLuongHangCangTrungGian, @DonViTinhKhoiLuongHangCangTrungGian, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_HangGiayPhep_CangTrungGian SET GiayPhep_ID = @GiayPhep_ID, TenHangCangTrungGian = @TenHangCangTrungGian, TenCangTrungGian = @TenCangTrungGian, SoLuongHangCangTrungGian = @SoLuongHangCangTrungGian, DonViTinhSoLuongHangCangTrungGian = @DonViTinhSoLuongHangCangTrungGian, KhoiLuongHangCangTrungGian = @KhoiLuongHangCangTrungGian, DonViTinhKhoiLuongHangCangTrungGian = @DonViTinhKhoiLuongHangCangTrungGian, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangGiayPhep_CangTrungGian WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangCangTrungGian", SqlDbType.NVarChar, "TenHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCangTrungGian", SqlDbType.NVarChar, "TenCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongHangCangTrungGian", SqlDbType.Decimal, "SoLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhSoLuongHangCangTrungGian", SqlDbType.VarChar, "DonViTinhSoLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhoiLuongHangCangTrungGian", SqlDbType.Decimal, "KhoiLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhKhoiLuongHangCangTrungGian", SqlDbType.VarChar, "DonViTinhKhoiLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangCangTrungGian", SqlDbType.NVarChar, "TenHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCangTrungGian", SqlDbType.NVarChar, "TenCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongHangCangTrungGian", SqlDbType.Decimal, "SoLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhSoLuongHangCangTrungGian", SqlDbType.VarChar, "DonViTinhSoLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhoiLuongHangCangTrungGian", SqlDbType.Decimal, "KhoiLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhKhoiLuongHangCangTrungGian", SqlDbType.VarChar, "DonViTinhKhoiLuongHangCangTrungGian", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_HangGiayPhep_CangTrungGian Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_HangGiayPhep_CangTrungGian> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_HangGiayPhep_CangTrungGian> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_HangGiayPhep_CangTrungGian> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACC_HangGiayPhep_CangTrungGian> SelectCollectionBy_GiayPhep_ID(long giayPhep_ID)
		{
            IDataReader reader = SelectReaderBy_GiayPhep_ID(giayPhep_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_GiayPhep_ID(long giayPhep_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectBy_GiayPhep_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, giayPhep_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_GiayPhep_ID(long giayPhep_ID)
		{
			const string spName = "p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectBy_GiayPhep_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, giayPhep_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_HangGiayPhep_CangTrungGian(long giayPhep_ID, string tenHangCangTrungGian, string tenCangTrungGian, decimal soLuongHangCangTrungGian, string donViTinhSoLuongHangCangTrungGian, decimal khoiLuongHangCangTrungGian, string donViTinhKhoiLuongHangCangTrungGian, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HangGiayPhep_CangTrungGian entity = new KDT_VNACC_HangGiayPhep_CangTrungGian();	
			entity.GiayPhep_ID = giayPhep_ID;
			entity.TenHangCangTrungGian = tenHangCangTrungGian;
			entity.TenCangTrungGian = tenCangTrungGian;
			entity.SoLuongHangCangTrungGian = soLuongHangCangTrungGian;
			entity.DonViTinhSoLuongHangCangTrungGian = donViTinhSoLuongHangCangTrungGian;
			entity.KhoiLuongHangCangTrungGian = khoiLuongHangCangTrungGian;
			entity.DonViTinhKhoiLuongHangCangTrungGian = donViTinhKhoiLuongHangCangTrungGian;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);
			db.AddInParameter(dbCommand, "@TenHangCangTrungGian", SqlDbType.NVarChar, TenHangCangTrungGian);
			db.AddInParameter(dbCommand, "@TenCangTrungGian", SqlDbType.NVarChar, TenCangTrungGian);
			db.AddInParameter(dbCommand, "@SoLuongHangCangTrungGian", SqlDbType.Decimal, SoLuongHangCangTrungGian);
			db.AddInParameter(dbCommand, "@DonViTinhSoLuongHangCangTrungGian", SqlDbType.VarChar, DonViTinhSoLuongHangCangTrungGian);
			db.AddInParameter(dbCommand, "@KhoiLuongHangCangTrungGian", SqlDbType.Decimal, KhoiLuongHangCangTrungGian);
			db.AddInParameter(dbCommand, "@DonViTinhKhoiLuongHangCangTrungGian", SqlDbType.VarChar, DonViTinhKhoiLuongHangCangTrungGian);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_HangGiayPhep_CangTrungGian> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangGiayPhep_CangTrungGian item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_HangGiayPhep_CangTrungGian(long id, long giayPhep_ID, string tenHangCangTrungGian, string tenCangTrungGian, decimal soLuongHangCangTrungGian, string donViTinhSoLuongHangCangTrungGian, decimal khoiLuongHangCangTrungGian, string donViTinhKhoiLuongHangCangTrungGian, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HangGiayPhep_CangTrungGian entity = new KDT_VNACC_HangGiayPhep_CangTrungGian();			
			entity.ID = id;
			entity.GiayPhep_ID = giayPhep_ID;
			entity.TenHangCangTrungGian = tenHangCangTrungGian;
			entity.TenCangTrungGian = tenCangTrungGian;
			entity.SoLuongHangCangTrungGian = soLuongHangCangTrungGian;
			entity.DonViTinhSoLuongHangCangTrungGian = donViTinhSoLuongHangCangTrungGian;
			entity.KhoiLuongHangCangTrungGian = khoiLuongHangCangTrungGian;
			entity.DonViTinhKhoiLuongHangCangTrungGian = donViTinhKhoiLuongHangCangTrungGian;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_HangGiayPhep_CangTrungGian_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);
			db.AddInParameter(dbCommand, "@TenHangCangTrungGian", SqlDbType.NVarChar, TenHangCangTrungGian);
			db.AddInParameter(dbCommand, "@TenCangTrungGian", SqlDbType.NVarChar, TenCangTrungGian);
			db.AddInParameter(dbCommand, "@SoLuongHangCangTrungGian", SqlDbType.Decimal, SoLuongHangCangTrungGian);
			db.AddInParameter(dbCommand, "@DonViTinhSoLuongHangCangTrungGian", SqlDbType.VarChar, DonViTinhSoLuongHangCangTrungGian);
			db.AddInParameter(dbCommand, "@KhoiLuongHangCangTrungGian", SqlDbType.Decimal, KhoiLuongHangCangTrungGian);
			db.AddInParameter(dbCommand, "@DonViTinhKhoiLuongHangCangTrungGian", SqlDbType.VarChar, DonViTinhKhoiLuongHangCangTrungGian);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_HangGiayPhep_CangTrungGian> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangGiayPhep_CangTrungGian item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_HangGiayPhep_CangTrungGian(long id, long giayPhep_ID, string tenHangCangTrungGian, string tenCangTrungGian, decimal soLuongHangCangTrungGian, string donViTinhSoLuongHangCangTrungGian, decimal khoiLuongHangCangTrungGian, string donViTinhKhoiLuongHangCangTrungGian, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HangGiayPhep_CangTrungGian entity = new KDT_VNACC_HangGiayPhep_CangTrungGian();			
			entity.ID = id;
			entity.GiayPhep_ID = giayPhep_ID;
			entity.TenHangCangTrungGian = tenHangCangTrungGian;
			entity.TenCangTrungGian = tenCangTrungGian;
			entity.SoLuongHangCangTrungGian = soLuongHangCangTrungGian;
			entity.DonViTinhSoLuongHangCangTrungGian = donViTinhSoLuongHangCangTrungGian;
			entity.KhoiLuongHangCangTrungGian = khoiLuongHangCangTrungGian;
			entity.DonViTinhKhoiLuongHangCangTrungGian = donViTinhKhoiLuongHangCangTrungGian;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);
			db.AddInParameter(dbCommand, "@TenHangCangTrungGian", SqlDbType.NVarChar, TenHangCangTrungGian);
			db.AddInParameter(dbCommand, "@TenCangTrungGian", SqlDbType.NVarChar, TenCangTrungGian);
			db.AddInParameter(dbCommand, "@SoLuongHangCangTrungGian", SqlDbType.Decimal, SoLuongHangCangTrungGian);
			db.AddInParameter(dbCommand, "@DonViTinhSoLuongHangCangTrungGian", SqlDbType.VarChar, DonViTinhSoLuongHangCangTrungGian);
			db.AddInParameter(dbCommand, "@KhoiLuongHangCangTrungGian", SqlDbType.Decimal, KhoiLuongHangCangTrungGian);
			db.AddInParameter(dbCommand, "@DonViTinhKhoiLuongHangCangTrungGian", SqlDbType.VarChar, DonViTinhKhoiLuongHangCangTrungGian);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_HangGiayPhep_CangTrungGian> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangGiayPhep_CangTrungGian item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_HangGiayPhep_CangTrungGian(long id)
		{
			KDT_VNACC_HangGiayPhep_CangTrungGian entity = new KDT_VNACC_HangGiayPhep_CangTrungGian();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_GiayPhep_ID(long giayPhep_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteBy_GiayPhep_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, giayPhep_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_HangGiayPhep_CangTrungGian> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangGiayPhep_CangTrungGian item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}