﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.Interface;
using System.Data;
using Logger;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.Interface.Report.SXXK
{
    public partial class Mau15_TT38_ChiTiet : DevExpress.XtraReports.UI.XtraReport
    {
        public bool isLuong = false;
        public int NamDangKy;
        public DateTime NgayDauKy;
        public DateTime NgayCuoiKy;
        string MaHang = "";
        int STT = 0;
        decimal SoLuong_G_F = 0;
        decimal TriGia_G_F = 0;
        decimal TriGiaNhapTrongKy_G_F = 0;
        decimal TriGiaXuatTrongKy_G_F = 0;
        decimal TriGiaTonCuoiKy_G_F = 0;
        public DateTime date = new DateTime(2015, 01, 01);
        public static DataTable tb_Mau15Chuan = new DataTable();
        public Mau15_TT38_ChiTiet()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable tb_DM_SP_NPL,bool isDaQuyetToan)
        {
            //NamDangKy = Int32.Parse(where);
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontToKhai);
            DataTable tb_Mau15 = tb_Mau15Chuan;
           // tb_Mau15 =  tb_Mau15Chuan;
            if (!tb_Mau15.Columns.Contains("SoTaiKhoan"))
            {
                DataColumn dc1 = new DataColumn("SoTaiKhoan");
                dc1.DataType = typeof(System.Int32);
                tb_Mau15.Columns.Add(dc1);
            }
            if (!tb_Mau15.Columns.Contains("LuongNhap_DauKy"))
            {
                DataColumn dc1 = new DataColumn("LuongNhap_DauKy");
                dc1.DataType = typeof(System.Decimal);
                DataColumn dc2 = new DataColumn("LuongNhap_TrongKy");
                dc2.DataType = typeof(System.Decimal);
                DataColumn dc3 = new DataColumn("LuongXuat_DauKy");
                dc3.DataType = typeof(System.Decimal);
                DataColumn dc4 = new DataColumn("LuongXuat_TrongKy");
                dc4.DataType = typeof(System.Decimal);
                DataColumn dc5 = new DataColumn("LuongTonCuoi");
                dc5.DataType = typeof(System.Decimal);
                DataColumn dc6 = new DataColumn("TriGiaNhap_DauKy");
                dc6.DataType = typeof(System.Decimal);
                DataColumn dc7 = new DataColumn("TriGiaNhap_TrongKy");
                dc7.DataType = typeof(System.Decimal);
                DataColumn dc8 = new DataColumn("TriGiaXuat_DauKy");
                dc8.DataType = typeof(System.Decimal);
                DataColumn dc9 = new DataColumn("TriGiaXuat_TrongKy");
                dc9.DataType = typeof(System.Decimal);
                DataColumn dc10 = new DataColumn("TriGiaTonCuoi");
                dc10.DataType = typeof(System.Decimal);
                DataColumn dc11 = new DataColumn("TriGiaTonDau");
                dc11.DataType = typeof(System.Decimal);
                DataColumn dc12 = new DataColumn("LuongTonDau");
                dc12.DataType = typeof(System.Decimal);
                //minhnd thêm CU và hủy
                DataColumn dc13 = new DataColumn("LuongCU_DauKy");
                dc13.DataType = typeof(System.Decimal);
                DataColumn dc14 = new DataColumn("LuongCU_TrongKy");
                dc14.DataType = typeof(System.Decimal);
                DataColumn dc15 = new DataColumn("LuongNPLHuy");
                dc15.DataType = typeof(System.Decimal);
                
                tb_Mau15.Columns.Add(dc1);
                tb_Mau15.Columns.Add(dc2);
                tb_Mau15.Columns.Add(dc3);
                tb_Mau15.Columns.Add(dc4);
                tb_Mau15.Columns.Add(dc5);
                tb_Mau15.Columns.Add(dc6);
                tb_Mau15.Columns.Add(dc7);
                tb_Mau15.Columns.Add(dc8);
                tb_Mau15.Columns.Add(dc9);
                tb_Mau15.Columns.Add(dc10);
                tb_Mau15.Columns.Add(dc11);
                tb_Mau15.Columns.Add(dc12);
                tb_Mau15.Columns.Add(dc13);
                tb_Mau15.Columns.Add(dc14);
                tb_Mau15.Columns.Add(dc15);
            }
            #region Xử lý chi tiết
            DataTable tbNPL = tb_Mau15.DefaultView.ToTable(true, "SoTaiKhoan", "HopDong_ID", "SoHopDong", "Ma", "Ten", "SoLuongDaNhap", "SoLuongDaDung", "SoLuongCungUng", "LuongTon");
            //int stt = 1;
            
            foreach (DataRow dr in tbNPL.Rows)
            {
                decimal luongNK_DauKy = 0;
                decimal luongNK_TrongKy = 0;
                //decimal luongNK_DauKyTong = 0;
                //decimal luongNK_TrongKyTong = 0;
                decimal triGiaNK_DauKy = 0;
                decimal triGiaNK_TrongKy = 0;
                decimal luongXK_DauKy = 0;
                decimal luongXK_DauKyTong = 0;
                decimal luongXK_TrongKy = 0;
                decimal luongXK_TrongKyTong = 0;
                decimal triGiaXK_DauKy = 0;
                decimal triGiaXK_TrongKy = 0;
                //decimal luongSD = 0;
                decimal luongTon = 0;
                decimal luongTonDau = 0;
                decimal triGiaTonDau = 0;
                decimal triGiaTon = 0;
                //minhnd thêm lượng xuất trả
                decimal luongXTDauKy = 0;
                decimal triGiaXTDauKy = 0;
                decimal luongXTTrongKy = 0;
                decimal triGiaXTTrongKy = 0;
                //minhnd thêm lượng xuất trả
                //minhnd thêm lượng cung ứng và hủy
                //decimal luongCUDauKy = 0;
                //decimal triGiaCUDauKy = 0;
                //decimal luongCUTrongKy = 0;
                //decimal triGiaCUTrongKy = 0;
                //
                decimal luongNPLHuy = 0;
                //minhnd thêm lượng cung ứng và hủy
                string DVT = "";
                try
                {
                    //Tính NPL xuất theo kỳ | tờ khai xuất định mức mã hàng
                    //Chưa + lương chuyển tiếp và xuất trả của các tờ khai xuất
                    foreach (DataRow drNPL in tb_DM_SP_NPL.Rows)
                    {

                        if (dr["Ma"].ToString() == drNPL["MaNguyenPhuLieu"].ToString())
                        {
                            try
                            {
                                int nam = int.Parse(drNPL["NgayDangKy"].ToString().Substring(6, 4));
                                int thang = int.Parse(drNPL["NgayDangKy"].ToString().Substring(3, 2));
                                int ngay = int.Parse(drNPL["NgayDangKy"].ToString().Substring(0, 2));
                                DateTime dateTK = new DateTime(nam, thang, ngay);

                                if (dateTK < NgayDauKy)
                                {
                                    try
                                    {
                                        luongXK_DauKyTong += Convert.ToDecimal(drNPL["LuongNPLCoHH"].ToString());

                                    }
                                    catch (Exception ex)
                                    {

                                        Logger.LocalLogger.Instance().WriteMessage(ex);
                                    }

                                }
                                else if (dateTK <= NgayCuoiKy)
                                {
                                    try 
	                                {	        
		                                luongXK_TrongKyTong += Convert.ToDecimal(drNPL["LuongNPLCoHH"].ToString());
	                                }
	                                catch (Exception ex)
	                                {
		
		                            Logger.LocalLogger.Instance().WriteMessage(ex);
	                                }
                                       
                                }else
                                {
                                    
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                                //throw;
                            }

                        }
                        else
                            continue;
                    }
                    foreach (DataRow drNPL in tb_Mau15.Rows)
                    {
                        if (dr["Ma"].ToString() == drNPL["Ma"].ToString())
                        {
                            try
                            {
                                int nam = int.Parse(drNPL["NgayDangKy"].ToString().Substring(6, 4));
                                int thang = int.Parse(drNPL["NgayDangKy"].ToString().Substring(3, 2));
                                int ngay = int.Parse(drNPL["NgayDangKy"].ToString().Substring(0, 2));
                                DVT = drNPL["DVT_ID"].ToString();
                                DateTime dateTK = new DateTime(nam, thang, ngay);
                                if (drNPL["SoTaiKhoan"].ToString().Contains("152") && drNPL["MaLoaiHinh"].ToString().Contains("X"))
                                {
                                    if (dateTK < NgayDauKy)
                                    {
                                        luongXTDauKy += Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                        triGiaXTDauKy += Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());
                                    }
                                    else if (dateTK <= NgayCuoiKy)
                                    {
                                        triGiaXTTrongKy += Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());
                                        luongXTTrongKy += Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                    }else
                                    {

                                    }

                                }
                                else if (drNPL["SoTaiKhoan"].ToString().Contains("155") && drNPL["MaLoaiHinh"].ToString().Contains("N"))
                                {
                                    try
                                    {
                                        if (dateTK <NgayDauKy)
                                        {

                                        }
                                        else if (dateTK <= NgayDauKy)
                                        {
                                            luongNK_TrongKy = Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                            triGiaNK_TrongKy = Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                            luongTonDau = 0;
                                            triGiaTonDau = 0;
                                            luongTon = 0;
                                            triGiaTon = 0;
                                        }
                                        else
                                        { 
                                        
                                        }


                                    }
                                    catch (Exception ex)
                                    {

                                        Logger.LocalLogger.Instance().WriteMessage(ex);
                                    }
                                }
                                else if (drNPL["SoTaiKhoan"].ToString().Contains("155") && drNPL["MaLoaiHinh"].ToString().Contains("X"))
                                {
                                    try
                                    {
                                        if (dateTK < NgayDauKy)
                                        {
                                            luongXK_DauKy = Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                            triGiaXK_DauKy = Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                            drNPL["LuongXuat_DauKy"] = luongXK_DauKy;
                                            drNPL["TriGiaXuat_DauKy"] = triGiaXK_DauKy;
                                        }
                                        else if (dateTK <=NgayCuoiKy)
                                        {
                                            luongXK_TrongKy = Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                            triGiaXK_TrongKy = Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                            drNPL["LuongXuat_TrongKy"] = luongXK_TrongKy;
                                            drNPL["TriGiaXuat_TrongKy"] = triGiaXK_TrongKy;
                                            luongTonDau = 0;
                                            triGiaTonDau = 0;
                                            luongTon = 0;
                                            triGiaTon = 0;
                                        }
                                        else
                                        {

                                        }

                                    }
                                    catch (Exception ex)
                                    {

                                        Logger.LocalLogger.Instance().WriteMessage(ex);
                                    }
                                }
                                //+ vào lượng NPL hủy
                                else if (drNPL["SoTaiKhoan"].ToString().Contains("152") && drNPL["MaLoaiHinh"].ToString().Contains("NLHUY"))
                                {
                                    //triGiaXTTrongKy += Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());
                                    luongNPLHuy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                    //Gán lại vào column
                                    drNPL["LuongNhap_DauKy"] = luongNK_DauKy;
                                    drNPL["LuongNhap_TrongKy"] = luongNK_TrongKy;
                                    drNPL["LuongXuat_DauKy"] = luongXK_DauKy;
                                    drNPL["LuongXuat_TrongKy"] = luongXK_TrongKy;
                                    //Tính lại lượng tồn
                                    drNPL["LuongTonDau"] = luongTonDau;//luongNK_DauKyTong;
                                    drNPL["LuongTonCuoi"] = (luongTonDau + luongNK_TrongKy) - (luongXK_TrongKy + luongNPLHuy);//luongNK_DauKyTong+luongNK_TrongKyTong;

                                    //Tính lại lượng tồn
                                    drNPL["TriGiaNhap_DauKy"] = triGiaNK_DauKy;
                                    drNPL["TriGiaNhap_TrongKy"] = triGiaNK_TrongKy;
                                    drNPL["TriGiaXuat_DauKy"] = triGiaXK_DauKy;
                                    drNPL["TriGiaXuat_TrongKy"] = triGiaXK_TrongKy;
                                    drNPL["TriGiaTonCuoi"] = (triGiaTonDau + triGiaNK_TrongKy) - triGiaXK_TrongKy;
                                    drNPL["TriGiaTonDau"] = triGiaTonDau;
                                }
                                else
                                {
                                    if (dateTK < NgayDauKy)
                                    {
                                        try
                                        {
                                            luongNK_DauKy = Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                            triGiaNK_DauKy = Convert.ToDecimal(drNPL["TriGiaTT"].ToString());

                                            if (luongXK_DauKyTong >= Convert.ToDecimal(drNPL["LuongSD_TK"].ToString()))
                                            {
                                                luongXK_DauKy = Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                                luongXK_DauKyTong -= Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());

                                                //luongXK_DauKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                                triGiaXK_DauKy = Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());
                                            }
                                            else
                                            {
                                                //test
                                                luongXK_DauKy = Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                                triGiaXK_DauKy = Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());
                                            }
                                            luongTonDau = Convert.ToDecimal(drNPL["LuongTon_TK"].ToString());
                                            triGiaTonDau = Convert.ToDecimal(drNPL["TonTriGia"].ToString());
                                            //Gán lại vào column
                                            drNPL["LuongNhap_DauKy"] = luongNK_DauKy;
                                            drNPL["LuongNhap_TrongKy"] = luongNK_TrongKy;
                                            drNPL["LuongXuat_DauKy"] = luongXK_DauKy;
                                            drNPL["LuongXuat_TrongKy"] = luongXK_TrongKy;
                                            //Tính lại lượng tồn
                                            drNPL["LuongTonDau"] = luongTonDau;//luongNK_DauKyTong;
                                            drNPL["LuongTonCuoi"] = (luongTonDau + luongNK_TrongKy) - luongXK_TrongKy - luongNPLHuy;//luongNK_DauKyTong+luongNK_TrongKyTong;
                                            
                                            //Tính lại lượng tồn
                                            drNPL["TriGiaNhap_DauKy"] = triGiaNK_DauKy;
                                            drNPL["TriGiaNhap_TrongKy"] = triGiaNK_TrongKy;
                                            drNPL["TriGiaXuat_DauKy"] = triGiaXK_DauKy;
                                            drNPL["TriGiaXuat_TrongKy"] = triGiaXK_TrongKy;
                                            drNPL["TriGiaTonCuoi"] = (triGiaTonDau + triGiaNK_TrongKy) - triGiaXK_TrongKy;
                                            drNPL["TriGiaTonDau"] = triGiaTonDau;
                                            //luongXT += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        }
                                        catch (Exception ex)
                                        {

                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }

                                    }
                                    else if (dateTK <= NgayCuoiKy)
                                    {
                                        luongNK_TrongKy = Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                        triGiaNK_TrongKy = Convert.ToDecimal(drNPL["TriGiaTT"].ToString());
                                        if (luongXK_TrongKyTong >= Convert.ToDecimal(drNPL["LuongSD_TK"].ToString()))
                                        {
                                            luongXK_TrongKy = Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                            luongXK_TrongKyTong -= Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());

                                            //luongXK_DauKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                            triGiaXK_TrongKy = Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());
                                        }
                                        else
                                        {
                                            luongXK_TrongKy = Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                            //luongXK_TrongKyTong -= Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                            triGiaXK_TrongKy = Convert.ToDecimal(drNPL["TriGiaSuDung"].ToString());
                                        }
                                        luongTon = Convert.ToDecimal(drNPL["LuongTon_TK"].ToString());
                                        triGiaTon = Convert.ToDecimal(drNPL["TonTriGia"].ToString());
                                        //luongXT += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());

                                        //Gán lại vào column
                                        drNPL["LuongNhap_DauKy"] = luongNK_DauKy;
                                        drNPL["LuongNhap_TrongKy"] = luongNK_TrongKy;
                                        drNPL["LuongXuat_DauKy"] = luongXK_DauKy;
                                        drNPL["LuongXuat_TrongKy"] = luongXK_TrongKy;
                                        //Tính lại lượng tồn
                                        drNPL["LuongTonCuoi"] = (luongTonDau + luongNK_TrongKy) - luongXK_TrongKy;//luongNK_DauKyTong+luongNK_TrongKyTong;
                                        drNPL["LuongTonDau"] = luongTonDau;//luongNK_DauKyTong;
                                        //Tính lại lượng tồn
                                        drNPL["TriGiaNhap_DauKy"] = triGiaNK_DauKy;
                                        drNPL["TriGiaNhap_TrongKy"] = triGiaNK_TrongKy;
                                        drNPL["TriGiaXuat_DauKy"] = triGiaXK_DauKy;
                                        drNPL["TriGiaXuat_TrongKy"] = triGiaXK_TrongKy;
                                        drNPL["TriGiaTonCuoi"] = (triGiaTonDau + triGiaNK_TrongKy) - triGiaXK_TrongKy;
                                        drNPL["TriGiaTonDau"] = triGiaTonDau;
                                    }
                                    else
                                    { 
                                    
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }

                        }
                        else
                            continue;
                    }

                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            #endregion Xử lý chi tiết
            //Xem lại chỗ này
            DataView dv = tb_Mau15.DefaultView;
            dv.Sort = "SoHopDong";
            dv.Sort = "SoTaiKhoan";
            dv.Sort = "Ma";
            tb_Mau15 = dv.ToTable();
            Int32 index = 0;
            for (int i = 0; i < tb_Mau15.Rows.Count; i++)
            {
                if (tb_Mau15.Rows[i]["Ma"].ToString() != MaHang)
                {
                    index++;
                }
                MaHang = tb_Mau15.Rows[i]["Ma"].ToString();
                if(isDaQuyetToan)
                    tb_Mau15.Rows[i]["ID"] = index;
                else
                    tb_Mau15.Rows[i]["STT"] = index;

            }

            DetailReport.DataSource = tb_Mau15;
            

            lbTenToChuc.Text = GlobalSettings.TEN_DON_VI;
            
            lbMaSo.Text = GlobalSettings.MA_DON_VI;
            lbDiaChi.Text = GlobalSettings.DIA_CHI;
            //lbNam.Text = NamDangKy.ToString();
            this.GroupHeader3.Visible = true;
            this.GroupHeader3.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("SoHopDong")
                
            });
            this.GroupHeader2.Visible = true;
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("SoTaiKhoan"),
                
                
            });
            this.GroupHeader1.Visible = false;
            if (isDaQuyetToan)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("ID"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                
            });
            }
            else 
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                
            });
            }
            
            //this.Detail1.Visible = false;
            txtHopDong.DataBindings.Add("Text", DetailReport.DataSource, "SoHopDong");
            txtSoTaiKhoan_G_H_2.DataBindings.Add("Text", DetailReport.DataSource, "SoTaiKhoan");
            lblTonDauKy_T.Text = "Tồn đầu kỳ ("+date.ToString("dd/MM/yyyy")+")";
            //lbNam.DataBindings.Add("Text",DetailReport.DataSource,"NamDangKy");
            if(isDaQuyetToan)
                txtSTT_G_F.DataBindings.Add("Text", DetailReport.DataSource, "ID");
            else
                txtSTT_G_F.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            //txtTaiKhoan.DataBindings.Add("Text", DetailReport.DataSource, "SoTaiKhoan");
            txtTenNPL.DataBindings.Add("Text",DetailReport.DataSource,"Ten");
            txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource,"Ma");
            lbl_MaHang_G_H.DataBindings.Add("Text", DetailReport.DataSource, "Ma");
            txt_MaNPL_G_F.DataBindings.Add("Text", DetailReport.DataSource, "Ma");
            txtDonGia.DataBindings.Add("Text", DetailReport.DataSource, "DonGiaKB", Company.KDT.SHARE.Components.Globals.FormatNumber(6));
            txtSoToKhai.DataBindings.Add("Text", DetailReport.DataSource, "SoToKhaiVnacc");
            txtMaLoaiHinh.DataBindings.Add("Text", DetailReport.DataSource, "MaLoaiHinh");
            txtNgayDangKy.DataBindings.Add("Text", DetailReport.DataSource,"NgayDangKy");
            txtDVT_ID.DataBindings.Add("Text", DetailReport.DataSource, "DVT_ID");
            txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongTon_TK", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            if (isLuong)
            {
                //txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonDau", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
                txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongNhap_TrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
                txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongXuat_TrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
                txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonCuoi", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            }
            else
            {
                txtTriGia.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTonDau", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
                txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaNhap_TrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
                txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaXuat_TrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
                txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTonCuoi", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            }
            //txtTriGia.DataBindings.Add("Text", DetailReport.DataSource, "TonTriGia",  Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTT", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "TonTriGia", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource,"GhiChu");
        }
        
        private void txt_SoTaiKhoan_G_H_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if(txt_SoTaiKhoan_G_H)
        }

        private void txtSTT_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            STT++;
            txtSTT_G_F.Text = STT.ToString();
        }

        private void txt_MaNPL_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void txt_MaNPL_G_F_AfterPrint(object sender, EventArgs e)
        {

        }

        private void lbl_STT_G_H_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txtTenTaiKhoan_AfterPrint(object sender, EventArgs e)
        {
            if (txtSoTaiKhoan_G_H_2.Text == "152")
                txtTenTaiKhoan.Text = "Nguyên liệu , vật liệu nhập khẩu";
            else
            {
                txtTenTaiKhoan.Text = "Thanh Phẩm , vật liệu nhập khẩu";
                STT = 0;
            }
        }

        private void txtTenTaiKhoan_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (txtSoTaiKhoan_G_H_2.Text == "152")
                txtTenTaiKhoan.Text = "Nguyên liệu , vật liệu nhập khẩu";
            else
            {
                txtTenTaiKhoan.Text = "Thành phẩm được sản xuất từ nguồn nhập khẩu";
                STT = 0;
            }
        }

        private void txtNhapTrongKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (Convert.ToDateTime(txtNgayDangKy.Text) < date)
                {
                    txtNhapTrongKy.Text = "0";
                }
                decimal c = Convert.ToDecimal(txtNhapTrongKy.Text);
                txtNhapTrongKy.Text = c.ToString("N0");
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void txtNgayDangKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                DateTime d = Convert.ToDateTime(txtNgayDangKy.Text);
                txtNgayDangKy.Text = d.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            
        }

        private void txtSoLuong_G_F_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                

            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                
            }
            
        }

        private void txtSoLuong_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if(SoLuong_G_F==0)
                    txtSoLuong_G_F.Text = "0";
                else
                    txtSoLuong_G_F.Text = SoLuong_G_F.ToString("N4");
                SoLuong_G_F = 0;
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtTonDauKy_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                SoLuong_G_F += Convert.ToDecimal(txtTonDauKy.Text);
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtTriGia_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                TriGia_G_F += Convert.ToDecimal(txtTriGia.Text);
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void txtTriGiaTonDauKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (TriGia_G_F==0)
                    txtTriGiaTonDauKy.Text = "0";
                else
                    txtTriGiaTonDauKy.Text = TriGia_G_F.ToString("N0");
                TriGia_G_F = 0;
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void txtTriGiaTonDauKy_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txtTriGiaNhapTrongKy_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    TriGiaNhapTrongKy_G_F += Convert.ToDecimal(txtTriGiaNhapTrongKy.Text);
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtTriGiaNhapTrongKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (TriGiaNhapTrongKy_G_F==0)
                    txtTriGiaNhapTrongKy.Text = "0";
                else
                    txtTriGiaNhapTrongKy.Text = TriGiaNhapTrongKy_G_F.ToString("N0");
                TriGiaNhapTrongKy_G_F = 0;
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtTriGiaXuatTrongKy_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (TriGiaXuatTrongKy_G_F==0)
                    txtTriGiaXuatTrongKy_G_F.Text = "0";
                else
                    txtTriGiaXuatTrongKy_G_F.Text = TriGiaXuatTrongKy_G_F.ToString("N0");
                    //txtTriGiaXuatTrongKy_G_F.Text = TriGiaXuatTrongKy_G_F.ToString();
                TriGiaXuatTrongKy_G_F = 0;
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtTriGiaXuatTrongKy_G_F_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    txtTriGiaXuatTrongKy_G_F.Text = TriGiaXuatTrongKy_G_F.ToString("N4");
            //    TriGiaXuatTrongKy_G_F = 0;
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtTriGiaTonCuoiKy_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (TriGiaTonCuoiKy_G_F==0)
                    txtTriGiaTonCuoiKy_G_F.Text = "0";
                else
                    txtTriGiaTonCuoiKy_G_F.Text = TriGiaTonCuoiKy_G_F.ToString("N0");
                TriGiaTonCuoiKy_G_F = 0;
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtTriGiaTonCuoiKy_G_F_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txtTonCuoiKy_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                TriGiaTonCuoiKy_G_F += Convert.ToDecimal(txtTonCuoiKy.Text);
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtNhapTrongKy_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                TriGiaNhapTrongKy_G_F += Convert.ToDecimal(txtNhapTrongKy.Text);
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtXuatTrongKy_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                TriGiaXuatTrongKy_G_F += Convert.ToDecimal(txtXuatTrongKy.Text);
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtDonGia_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtDonGia.Text)) 
                {
                    decimal c = Convert.ToDecimal(txtDonGia.Text);
                    if (c == 0)
                    {
                        txtDonGia.Text = "0";
                    }
                    else
                        txtDonGia.Text = c.ToString("N6");
                }
                
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            
        }

        private void txtTonDauKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDateTime(txtNgayDangKy.Text) > date) 
            {
                txtTonDauKy.Text = "0";
            }
        }

        private void txtTriGia_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDateTime(txtNgayDangKy.Text) > date)
            {
                txtTriGia.Text = "0";
            }
        }

        private void txtNhapTrongKy_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
        }

        private void txtXuatTrongKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDateTime(txtNgayDangKy.Text) < date)
            {
                txtXuatTrongKy.Text = "0";
            }
        }

        private void txtTonCuoiKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDateTime(txtNgayDangKy.Text) < date)
            {
                txtTonCuoiKy.Text = "0";
            }
        }

        private void txtHopDong_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            txtHopDong.Text = "Số hợp đồng: " + txtHopDong.Text;
        }

        private void txtMaLoaiHinh_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (txtMaLoaiHinh.Text.Contains("V")) 
            {
                try
                {
                    txtMaLoaiHinh.Text = txtMaLoaiHinh.Text.Substring(2, 3);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                
            }
        }

    }
}
