﻿namespace Company.Interface
{
    partial class ThongTinDNAndHQForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongTinDNAndHQForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPageHQ = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.linkGetIP = new System.Windows.Forms.LinkLabel();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.rbtIsKTX = new Janus.Windows.EditControls.UIRadioButton();
            this.rbtIsTQDT = new Janus.Windows.EditControls.UIRadioButton();
            this.chkSendV4 = new System.Windows.Forms.CheckBox();
            this.chkCKS = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnOpenWS = new System.Windows.Forms.Button();
            this.lblWS = new System.Windows.Forms.Label();
            this.txtTenDichVu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPort = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtHost = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDiaChiHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMaCuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMailHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenCucHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenNganHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label7 = new System.Windows.Forms.Label();
            this.uiTabPageDN = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkLaDNCX = new Janus.Windows.EditControls.UICheckBox();
            this.cbFont = new Janus.Windows.EditControls.UIComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSoTienKhoanTKX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienKhoanTKN = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtThongBaoHetHan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtChucVu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtNguoiLienHeDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSoFaxDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDienThoaiDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.opTiengAnh = new Janus.Windows.EditControls.UIRadioButton();
            this.opVietNam = new Janus.Windows.EditControls.UIRadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMaMid = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMailDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.uiTabPageVNACC = new Janus.Windows.UI.Tab.UITabPage();
            this.btnOpenWSVNACC = new System.Windows.Forms.Button();
            this.lblWSVNACC = new System.Windows.Forms.Label();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.linkVNACCGetIP = new System.Windows.Forms.LinkLabel();
            this.label39 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.txtVNACCPortProxy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtVNACCHostProxy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtVNACCAddress = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label43 = new System.Windows.Forms.Label();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtVNACCUserID = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtVNACCUserPass = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtVNACCUserCode = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtVNACCTerminalID = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtVNACCTerminalPass = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.uiTabDBDL = new Janus.Windows.UI.Tab.UITabPage();
            this.btnCapNhatDBDL = new Janus.Windows.EditControls.UIButton();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtSoTKDBDL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.rfvDiaChi = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenNganHQ = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.containerValidator1 = new Company.Controls.CustomValidation.ContainerValidator();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvTenCuc = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNguoiLienHe = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvEmailDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaCuc = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDiaChiHQ = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPageHQ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.uiTabPageDN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            this.uiTabPageVNACC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            this.uiTabDBDL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNganHQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenCuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiLienHe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvEmailDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaCuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChiHQ)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(397, 485);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.uiTab1);
            this.uiGroupBox1.Controls.Add(this.btnSave);
            this.uiGroupBox1.Controls.Add(this.uiButton2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(397, 485);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiTab1
            // 
            this.uiTab1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiTab1.BackColor = System.Drawing.SystemColors.Control;
            this.uiTab1.Location = new System.Drawing.Point(0, 3);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(397, 447);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPageHQ,
            this.uiTabPageDN,
            this.uiTabPageVNACC,
            this.uiTabDBDL});
            this.uiTab1.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPageHQ
            // 
            this.uiTabPageHQ.Controls.Add(this.uiGroupBox5);
            this.uiTabPageHQ.Controls.Add(this.uiGroupBox3);
            this.uiTabPageHQ.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageHQ.Name = "uiTabPageHQ";
            this.uiTabPageHQ.Size = new System.Drawing.Size(395, 425);
            this.uiTabPageHQ.TabStop = true;
            this.uiTabPageHQ.Text = "Thông tin Hải quan";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.linkGetIP);
            this.uiGroupBox5.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox5.Controls.Add(this.chkCKS);
            this.uiGroupBox5.Controls.Add(this.label22);
            this.uiGroupBox5.Controls.Add(this.label15);
            this.uiGroupBox5.Controls.Add(this.label16);
            this.uiGroupBox5.Controls.Add(this.btnOpenWS);
            this.uiGroupBox5.Controls.Add(this.lblWS);
            this.uiGroupBox5.Controls.Add(this.txtTenDichVu);
            this.uiGroupBox5.Controls.Add(this.label17);
            this.uiGroupBox5.Controls.Add(this.label18);
            this.uiGroupBox5.Controls.Add(this.txtPort);
            this.uiGroupBox5.Controls.Add(this.txtHost);
            this.uiGroupBox5.Controls.Add(this.label19);
            this.uiGroupBox5.Controls.Add(this.txtDiaChiHQ);
            this.uiGroupBox5.Controls.Add(this.label20);
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(4, 170);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(388, 242);
            this.uiGroupBox5.TabIndex = 2;
            this.uiGroupBox5.Text = "Thông số khai báo Hải quan";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // linkGetIP
            // 
            this.linkGetIP.AutoSize = true;
            this.linkGetIP.Location = new System.Drawing.Point(326, 25);
            this.linkGetIP.Name = "linkGetIP";
            this.linkGetIP.Size = new System.Drawing.Size(42, 13);
            this.linkGetIP.TabIndex = 1;
            this.linkGetIP.TabStop = true;
            this.linkGetIP.Text = "Lấy IP";
            this.linkGetIP.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkGetIP_LinkClicked);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.rbtIsKTX);
            this.uiGroupBox6.Controls.Add(this.rbtIsTQDT);
            this.uiGroupBox6.Controls.Add(this.chkSendV4);
            this.uiGroupBox6.Location = new System.Drawing.Point(7, 148);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(358, 68);
            this.uiGroupBox6.TabIndex = 39;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // rbtIsKTX
            // 
            this.rbtIsKTX.Location = new System.Drawing.Point(194, 23);
            this.rbtIsKTX.Name = "rbtIsKTX";
            this.rbtIsKTX.Size = new System.Drawing.Size(158, 39);
            this.rbtIsKTX.TabIndex = 6;
            this.rbtIsKTX.Text = "Khai báo thủ công (KTX)";
            // 
            // rbtIsTQDT
            // 
            this.rbtIsTQDT.Location = new System.Drawing.Point(6, 23);
            this.rbtIsTQDT.Name = "rbtIsTQDT";
            this.rbtIsTQDT.Size = new System.Drawing.Size(155, 39);
            this.rbtIsTQDT.TabIndex = 6;
            this.rbtIsTQDT.Text = "Khai báo thông quan điện tử (Tự động duyệt)";
            // 
            // chkSendV4
            // 
            this.chkSendV4.AutoSize = true;
            this.chkSendV4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.chkSendV4.ForeColor = System.Drawing.Color.Blue;
            this.chkSendV4.Location = new System.Drawing.Point(0, 0);
            this.chkSendV4.Name = "chkSendV4";
            this.chkSendV4.Size = new System.Drawing.Size(210, 17);
            this.chkSendV4.TabIndex = 0;
            this.chkSendV4.Text = "Khai báo chuẩn V4 (Theo TT 196)";
            this.chkSendV4.UseVisualStyleBackColor = false;
            this.chkSendV4.CheckedChanged += new System.EventHandler(this.chkSendV4_CheckedChanged);
            // 
            // chkCKS
            // 
            this.chkCKS.AutoSize = true;
            this.chkCKS.ForeColor = System.Drawing.Color.Blue;
            this.chkCKS.Location = new System.Drawing.Point(141, 125);
            this.chkCKS.Name = "chkCKS";
            this.chkCKS.Size = new System.Drawing.Size(202, 17);
            this.chkCKS.TabIndex = 5;
            this.chkCKS.Text = "Dùng địa chỉ khai báo chữ ký số";
            this.chkCKS.UseVisualStyleBackColor = true;
            this.chkCKS.CheckedChanged += new System.EventHandler(this.chkCKS_CheckedChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(368, 51);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(14, 13);
            this.label22.TabIndex = 38;
            this.label22.Text = "*";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(371, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 13);
            this.label15.TabIndex = 36;
            this.label15.Text = "*";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(368, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "*";
            // 
            // btnOpenWS
            // 
            this.btnOpenWS.AutoSize = true;
            this.btnOpenWS.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnOpenWS.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenWS.Image")));
            this.btnOpenWS.Location = new System.Drawing.Point(361, 217);
            this.btnOpenWS.Name = "btnOpenWS";
            this.btnOpenWS.Size = new System.Drawing.Size(22, 22);
            this.btnOpenWS.TabIndex = 7;
            this.btnOpenWS.UseVisualStyleBackColor = true;
            this.btnOpenWS.Click += new System.EventHandler(this.btnOpenWS_Click);
            // 
            // lblWS
            // 
            this.lblWS.BackColor = System.Drawing.Color.Transparent;
            this.lblWS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWS.ForeColor = System.Drawing.Color.Red;
            this.lblWS.Location = new System.Drawing.Point(16, 219);
            this.lblWS.Name = "lblWS";
            this.lblWS.Size = new System.Drawing.Size(346, 20);
            this.lblWS.TabIndex = 6;
            this.lblWS.Text = "http://www.dngcustoms.gov.vn/kdtservice/cisservice.asmx";
            this.lblWS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTenDichVu
            // 
            this.txtTenDichVu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDichVu.Location = new System.Drawing.Point(141, 47);
            this.txtTenDichVu.MaxLength = 200;
            this.txtTenDichVu.Name = "txtTenDichVu";
            this.txtTenDichVu.Size = new System.Drawing.Size(224, 21);
            this.txtTenDichVu.TabIndex = 2;
            this.txtTenDichVu.VisualStyleManager = this.vsmMain;
            this.txtTenDichVu.TextChanged += new System.EventHandler(this.txtTenDichVu_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(16, 52);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(105, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Tên dịch vụ khai báo";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(16, 103);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Port proxy";
            // 
            // txtPort
            // 
            this.txtPort.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPort.Location = new System.Drawing.Point(141, 98);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(224, 21);
            this.txtPort.TabIndex = 4;
            this.txtPort.VisualStyleManager = this.vsmMain;
            // 
            // txtHost
            // 
            this.txtHost.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHost.Location = new System.Drawing.Point(141, 74);
            this.txtHost.Name = "txtHost";
            this.txtHost.Size = new System.Drawing.Size(224, 21);
            this.txtHost.TabIndex = 3;
            this.txtHost.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(16, 79);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 13);
            this.label19.TabIndex = 32;
            this.label19.Text = "Host proxy";
            // 
            // txtDiaChiHQ
            // 
            this.txtDiaChiHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiHQ.Location = new System.Drawing.Point(141, 20);
            this.txtDiaChiHQ.Name = "txtDiaChiHQ";
            this.txtDiaChiHQ.Size = new System.Drawing.Size(185, 21);
            this.txtDiaChiHQ.TabIndex = 0;
            this.txtDiaChiHQ.VisualStyleManager = this.vsmMain;
            this.txtDiaChiHQ.TextChanged += new System.EventHandler(this.txtDiaChiHQ_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(16, 28);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 13);
            this.label20.TabIndex = 26;
            this.label20.Text = "Địa chỉ hải quan";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label10);
            this.uiGroupBox3.Controls.Add(this.txtMaCuc);
            this.uiGroupBox3.Controls.Add(this.txtMailHaiQuan);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.txtTenCucHQ);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtTenNganHQ);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiGroupBox3.Location = new System.Drawing.Point(4, 3);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(388, 158);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.Text = "Chi cục Hải quan khai báo";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(16, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Mã cục hải quan:";
            // 
            // txtMaCuc
            // 
            this.txtMaCuc.Location = new System.Drawing.Point(141, 18);
            this.txtMaCuc.Name = "txtMaCuc";
            this.txtMaCuc.Size = new System.Drawing.Size(227, 21);
            this.txtMaCuc.TabIndex = 0;
            this.txtMaCuc.VisualStyleManager = this.vsmMain;
            // 
            // txtMailHaiQuan
            // 
            this.txtMailHaiQuan.Location = new System.Drawing.Point(141, 128);
            this.txtMailHaiQuan.Name = "txtMailHaiQuan";
            this.txtMailHaiQuan.Size = new System.Drawing.Size(227, 21);
            this.txtMailHaiQuan.TabIndex = 4;
            this.txtMailHaiQuan.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(16, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Email Hải quan:";
            // 
            // txtTenCucHQ
            // 
            this.txtTenCucHQ.Location = new System.Drawing.Point(141, 73);
            this.txtTenCucHQ.Name = "txtTenCucHQ";
            this.txtTenCucHQ.Size = new System.Drawing.Size(227, 21);
            this.txtTenCucHQ.TabIndex = 2;
            this.txtTenCucHQ.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(16, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Tên cục hải quan:";
            // 
            // txtTenNganHQ
            // 
            this.txtTenNganHQ.Location = new System.Drawing.Point(141, 100);
            this.txtTenNganHQ.Name = "txtTenNganHQ";
            this.txtTenNganHQ.Size = new System.Drawing.Size(227, 21);
            this.txtTenNganHQ.TabIndex = 3;
            this.txtTenNganHQ.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(16, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tên ngắn hải quan:";
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(141, 45);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(227, 22);
            this.donViHaiQuanControl1.TabIndex = 1;
            this.donViHaiQuanControl1.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(16, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Chọn hải quan khai báo:";
            // 
            // uiTabPageDN
            // 
            this.uiTabPageDN.Controls.Add(this.uiGroupBox4);
            this.uiTabPageDN.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageDN.Name = "uiTabPageDN";
            this.uiTabPageDN.Size = new System.Drawing.Size(395, 425);
            this.uiTabPageDN.TabStop = true;
            this.uiTabPageDN.Text = "Thông tin Doanh nghiệp";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.chkLaDNCX);
            this.uiGroupBox4.Controls.Add(this.cbFont);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.label21);
            this.uiGroupBox4.Controls.Add(this.txtSoTienKhoanTKX);
            this.uiGroupBox4.Controls.Add(this.txtSoTienKhoanTKN);
            this.uiGroupBox4.Controls.Add(this.label24);
            this.uiGroupBox4.Controls.Add(this.label25);
            this.uiGroupBox4.Controls.Add(this.label26);
            this.uiGroupBox4.Controls.Add(this.txtThongBaoHetHan);
            this.uiGroupBox4.Controls.Add(this.txtChucVu);
            this.uiGroupBox4.Controls.Add(this.label30);
            this.uiGroupBox4.Controls.Add(this.txtNguoiLienHeDN);
            this.uiGroupBox4.Controls.Add(this.label14);
            this.uiGroupBox4.Controls.Add(this.txtSoFaxDN);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.txtDienThoaiDN);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.label11);
            this.uiGroupBox4.Controls.Add(this.opTiengAnh);
            this.uiGroupBox4.Controls.Add(this.opVietNam);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtMaMid);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.txtMailDoanhNghiep);
            this.uiGroupBox4.Controls.Add(this.txtDiaChi);
            this.uiGroupBox4.Controls.Add(this.txtTenDN);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.txtMaDN);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Controls.Add(this.label28);
            this.uiGroupBox4.Controls.Add(this.label27);
            this.uiGroupBox4.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiGroupBox4.Location = new System.Drawing.Point(4, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(388, 365);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // chkLaDNCX
            // 
            this.chkLaDNCX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLaDNCX.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.chkLaDNCX.Location = new System.Drawing.Point(110, 338);
            this.chkLaDNCX.Name = "chkLaDNCX";
            this.chkLaDNCX.Size = new System.Drawing.Size(189, 23);
            this.chkLaDNCX.TabIndex = 16;
            this.chkLaDNCX.Text = "Là doanh nghiệp CHẾ XUẤT";
            // 
            // cbFont
            // 
            this.cbFont.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbFont.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Unicode";
            uiComboBoxItem3.Value = "Unicode";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "TCVN3(ABC)";
            uiComboBoxItem4.Value = "TCVN3";
            this.cbFont.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbFont.Location = new System.Drawing.Point(110, 288);
            this.cbFont.Name = "cbFont";
            this.cbFont.Size = new System.Drawing.Size(117, 21);
            this.cbFont.TabIndex = 12;
            this.cbFont.Tag = "";
            this.cbFont.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbFont.VisualStyleManager = this.vsmMain;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Location = new System.Drawing.Point(237, 268);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 13);
            this.label23.TabIndex = 10;
            this.label23.Text = "(ngày)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(251, 241);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(25, 13);
            this.label21.TabIndex = 10;
            this.label21.Text = "TKX";
            // 
            // txtSoTienKhoanTKX
            // 
            this.txtSoTienKhoanTKX.DecimalDigits = 0;
            this.txtSoTienKhoanTKX.Location = new System.Drawing.Point(282, 236);
            this.txtSoTienKhoanTKX.Name = "txtSoTienKhoanTKX";
            this.txtSoTienKhoanTKX.Size = new System.Drawing.Size(86, 21);
            this.txtSoTienKhoanTKX.TabIndex = 10;
            this.txtSoTienKhoanTKX.Text = "0";
            this.txtSoTienKhoanTKX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienKhoanTKX.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTienKhoanTKN
            // 
            this.txtSoTienKhoanTKN.DecimalDigits = 0;
            this.txtSoTienKhoanTKN.Location = new System.Drawing.Point(110, 235);
            this.txtSoTienKhoanTKN.Name = "txtSoTienKhoanTKN";
            this.txtSoTienKhoanTKN.Size = new System.Drawing.Size(117, 21);
            this.txtSoTienKhoanTKN.TabIndex = 9;
            this.txtSoTienKhoanTKN.Text = "0";
            this.txtSoTienKhoanTKN.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienKhoanTKN.VisualStyleManager = this.vsmMain;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(78, 239);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(26, 13);
            this.label24.TabIndex = 26;
            this.label24.Text = "TKN";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(7, 264);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(74, 13);
            this.label25.TabIndex = 30;
            this.label25.Text = "Hêt hạn trước";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Location = new System.Drawing.Point(7, 238);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(72, 13);
            this.label26.TabIndex = 25;
            this.label26.Text = "Số tiền khoán";
            // 
            // txtThongBaoHetHan
            // 
            this.txtThongBaoHetHan.DecimalDigits = 0;
            this.txtThongBaoHetHan.Location = new System.Drawing.Point(110, 263);
            this.txtThongBaoHetHan.Name = "txtThongBaoHetHan";
            this.txtThongBaoHetHan.Size = new System.Drawing.Size(117, 21);
            this.txtThongBaoHetHan.TabIndex = 11;
            this.txtThongBaoHetHan.Text = "0";
            this.txtThongBaoHetHan.Value = 0;
            this.txtThongBaoHetHan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtThongBaoHetHan.VisualStyleManager = this.vsmMain;
            // 
            // txtChucVu
            // 
            this.txtChucVu.Location = new System.Drawing.Point(110, 154);
            this.txtChucVu.Name = "txtChucVu";
            this.txtChucVu.Size = new System.Drawing.Size(258, 21);
            this.txtChucVu.TabIndex = 6;
            this.txtChucVu.VisualStyleManager = this.vsmMain;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Location = new System.Drawing.Point(7, 159);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(51, 13);
            this.label30.TabIndex = 15;
            this.label30.Text = "Chức vụ:";
            // 
            // txtNguoiLienHeDN
            // 
            this.txtNguoiLienHeDN.Location = new System.Drawing.Point(110, 127);
            this.txtNguoiLienHeDN.Name = "txtNguoiLienHeDN";
            this.txtNguoiLienHeDN.Size = new System.Drawing.Size(258, 21);
            this.txtNguoiLienHeDN.TabIndex = 5;
            this.txtNguoiLienHeDN.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(7, 132);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Đại diện DN";
            // 
            // txtSoFaxDN
            // 
            this.txtSoFaxDN.Location = new System.Drawing.Point(265, 99);
            this.txtSoFaxDN.Name = "txtSoFaxDN";
            this.txtSoFaxDN.Size = new System.Drawing.Size(103, 21);
            this.txtSoFaxDN.TabIndex = 4;
            this.txtSoFaxDN.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(237, 106);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Fax:";
            // 
            // txtDienThoaiDN
            // 
            this.txtDienThoaiDN.Location = new System.Drawing.Point(110, 99);
            this.txtDienThoaiDN.Name = "txtDienThoaiDN";
            this.txtDienThoaiDN.Size = new System.Drawing.Size(121, 21);
            this.txtDienThoaiDN.TabIndex = 3;
            this.txtDienThoaiDN.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(7, 104);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Điện thoại:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(7, 318);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Ngôn ngữ hiển thị";
            // 
            // opTiengAnh
            // 
            this.opTiengAnh.Location = new System.Drawing.Point(233, 315);
            this.opTiengAnh.Name = "opTiengAnh";
            this.opTiengAnh.Size = new System.Drawing.Size(104, 23);
            this.opTiengAnh.TabIndex = 15;
            this.opTiengAnh.Text = "Tiếng Anh";
            // 
            // opVietNam
            // 
            this.opVietNam.Checked = true;
            this.opVietNam.Location = new System.Drawing.Point(110, 315);
            this.opVietNam.Name = "opVietNam";
            this.opVietNam.Size = new System.Drawing.Size(104, 23);
            this.opVietNam.TabIndex = 14;
            this.opVietNam.TabStop = true;
            this.opVietNam.Text = "Tiếng Việt";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(57, 215);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Mã MID:";
            // 
            // txtMaMid
            // 
            this.txtMaMid.Location = new System.Drawing.Point(110, 209);
            this.txtMaMid.Name = "txtMaMid";
            this.txtMaMid.Size = new System.Drawing.Size(258, 21);
            this.txtMaMid.TabIndex = 8;
            this.txtMaMid.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(7, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Email doanh nghiệp:";
            // 
            // txtMailDoanhNghiep
            // 
            this.txtMailDoanhNghiep.Location = new System.Drawing.Point(110, 181);
            this.txtMailDoanhNghiep.Name = "txtMailDoanhNghiep";
            this.txtMailDoanhNghiep.Size = new System.Drawing.Size(258, 21);
            this.txtMailDoanhNghiep.TabIndex = 7;
            this.txtMailDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(110, 72);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(258, 21);
            this.txtDiaChi.TabIndex = 2;
            this.txtDiaChi.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDN
            // 
            this.txtTenDN.Location = new System.Drawing.Point(110, 45);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Size = new System.Drawing.Size(258, 21);
            this.txtTenDN.TabIndex = 1;
            this.txtTenDN.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(7, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Địa chỉ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(7, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên doanh nghiệp:";
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(110, 18);
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.Size = new System.Drawing.Size(258, 21);
            this.txtMaDN.TabIndex = 0;
            this.txtMaDN.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(7, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã doanh nghiệp:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Location = new System.Drawing.Point(230, 293);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(155, 13);
            this.label28.TabIndex = 13;
            this.label28.Text = "(Font hệ thống Hải quan dùng)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Location = new System.Drawing.Point(7, 292);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(74, 13);
            this.label27.TabIndex = 10;
            this.label27.Text = "Font Hải quan";
            // 
            // uiTabPageVNACC
            // 
            this.uiTabPageVNACC.Controls.Add(this.btnOpenWSVNACC);
            this.uiTabPageVNACC.Controls.Add(this.lblWSVNACC);
            this.uiTabPageVNACC.Controls.Add(this.uiGroupBox9);
            this.uiTabPageVNACC.Controls.Add(this.uiGroupBox8);
            this.uiTabPageVNACC.Controls.Add(this.uiGroupBox7);
            this.uiTabPageVNACC.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageVNACC.Name = "uiTabPageVNACC";
            this.uiTabPageVNACC.Size = new System.Drawing.Size(395, 425);
            this.uiTabPageVNACC.TabStop = true;
            this.uiTabPageVNACC.Text = "Terminal - VNACCS";
            // 
            // btnOpenWSVNACC
            // 
            this.btnOpenWSVNACC.AutoSize = true;
            this.btnOpenWSVNACC.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnOpenWSVNACC.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenWSVNACC.Image")));
            this.btnOpenWSVNACC.Location = new System.Drawing.Point(366, 392);
            this.btnOpenWSVNACC.Name = "btnOpenWSVNACC";
            this.btnOpenWSVNACC.Size = new System.Drawing.Size(22, 22);
            this.btnOpenWSVNACC.TabIndex = 4;
            this.btnOpenWSVNACC.UseVisualStyleBackColor = true;
            this.btnOpenWSVNACC.Click += new System.EventHandler(this.btnOpenWSVNACC_Click);
            // 
            // lblWSVNACC
            // 
            this.lblWSVNACC.BackColor = System.Drawing.Color.Transparent;
            this.lblWSVNACC.ForeColor = System.Drawing.Color.Red;
            this.lblWSVNACC.Location = new System.Drawing.Point(14, 393);
            this.lblWSVNACC.Name = "lblWSVNACC";
            this.lblWSVNACC.Size = new System.Drawing.Size(346, 20);
            this.lblWSVNACC.TabIndex = 3;
            this.lblWSVNACC.Text = "http://#";
            this.lblWSVNACC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.linkVNACCGetIP);
            this.uiGroupBox9.Controls.Add(this.label39);
            this.uiGroupBox9.Controls.Add(this.label41);
            this.uiGroupBox9.Controls.Add(this.txtVNACCPortProxy);
            this.uiGroupBox9.Controls.Add(this.txtVNACCHostProxy);
            this.uiGroupBox9.Controls.Add(this.label42);
            this.uiGroupBox9.Controls.Add(this.txtVNACCAddress);
            this.uiGroupBox9.Controls.Add(this.label43);
            this.uiGroupBox9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox9.Location = new System.Drawing.Point(5, 240);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(387, 142);
            this.uiGroupBox9.TabIndex = 2;
            this.uiGroupBox9.Text = "Thông số khai báo Hải quan - VNACCS";
            this.uiGroupBox9.VisualStyleManager = this.vsmMain;
            // 
            // linkVNACCGetIP
            // 
            this.linkVNACCGetIP.AutoSize = true;
            this.linkVNACCGetIP.Location = new System.Drawing.Point(318, 34);
            this.linkVNACCGetIP.Name = "linkVNACCGetIP";
            this.linkVNACCGetIP.Size = new System.Drawing.Size(42, 13);
            this.linkVNACCGetIP.TabIndex = 4;
            this.linkVNACCGetIP.TabStop = true;
            this.linkVNACCGetIP.Text = "Lấy IP";
            this.linkVNACCGetIP.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkVNACCGetIP_LinkClicked);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(360, 34);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(14, 13);
            this.label39.TabIndex = 49;
            this.label39.Text = "*";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(8, 85);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(58, 13);
            this.label41.TabIndex = 46;
            this.label41.Text = "Port proxy";
            // 
            // txtVNACCPortProxy
            // 
            this.txtVNACCPortProxy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVNACCPortProxy.Location = new System.Drawing.Point(133, 80);
            this.txtVNACCPortProxy.Name = "txtVNACCPortProxy";
            this.txtVNACCPortProxy.Size = new System.Drawing.Size(224, 21);
            this.txtVNACCPortProxy.TabIndex = 3;
            this.txtVNACCPortProxy.VisualStyleManager = this.vsmMain;
            // 
            // txtVNACCHostProxy
            // 
            this.txtVNACCHostProxy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVNACCHostProxy.Location = new System.Drawing.Point(133, 56);
            this.txtVNACCHostProxy.Name = "txtVNACCHostProxy";
            this.txtVNACCHostProxy.Size = new System.Drawing.Size(224, 21);
            this.txtVNACCHostProxy.TabIndex = 2;
            this.txtVNACCHostProxy.VisualStyleManager = this.vsmMain;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(8, 61);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(60, 13);
            this.label42.TabIndex = 45;
            this.label42.Text = "Host proxy";
            // 
            // txtVNACCAddress
            // 
            this.txtVNACCAddress.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVNACCAddress.Location = new System.Drawing.Point(133, 29);
            this.txtVNACCAddress.Name = "txtVNACCAddress";
            this.txtVNACCAddress.Size = new System.Drawing.Size(185, 21);
            this.txtVNACCAddress.TabIndex = 0;
            this.txtVNACCAddress.VisualStyleManager = this.vsmMain;
            this.txtVNACCAddress.TextChanged += new System.EventHandler(this.txtVNACCAddress_TextChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(8, 37);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(82, 13);
            this.label43.TabIndex = 44;
            this.label43.Text = "Địa chỉ khai báo";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.txtVNACCUserID);
            this.uiGroupBox8.Controls.Add(this.txtVNACCUserPass);
            this.uiGroupBox8.Controls.Add(this.txtVNACCUserCode);
            this.uiGroupBox8.Controls.Add(this.label36);
            this.uiGroupBox8.Controls.Add(this.label34);
            this.uiGroupBox8.Controls.Add(this.label35);
            this.uiGroupBox8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox8.Location = new System.Drawing.Point(5, 111);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(387, 120);
            this.uiGroupBox8.TabIndex = 1;
            this.uiGroupBox8.Text = "User - Thiết lập thông tin người dùng kêt nối tương ứng Terminal";
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // txtVNACCUserID
            // 
            this.txtVNACCUserID.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVNACCUserID.Location = new System.Drawing.Point(133, 29);
            this.txtVNACCUserID.Name = "txtVNACCUserID";
            this.txtVNACCUserID.Size = new System.Drawing.Size(161, 21);
            this.txtVNACCUserID.TabIndex = 0;
            this.txtVNACCUserID.VisualStyleManager = this.vsmMain;
            // 
            // txtVNACCUserPass
            // 
            this.txtVNACCUserPass.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVNACCUserPass.Location = new System.Drawing.Point(133, 83);
            this.txtVNACCUserPass.Name = "txtVNACCUserPass";
            this.txtVNACCUserPass.PasswordChar = '*';
            this.txtVNACCUserPass.Size = new System.Drawing.Size(161, 21);
            this.txtVNACCUserPass.TabIndex = 2;
            this.txtVNACCUserPass.VisualStyleManager = this.vsmMain;
            // 
            // txtVNACCUserCode
            // 
            this.txtVNACCUserCode.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVNACCUserCode.Location = new System.Drawing.Point(133, 56);
            this.txtVNACCUserCode.Name = "txtVNACCUserCode";
            this.txtVNACCUserCode.Size = new System.Drawing.Size(161, 21);
            this.txtVNACCUserCode.TabIndex = 1;
            this.txtVNACCUserCode.VisualStyleManager = this.vsmMain;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(22, 88);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(111, 13);
            this.label36.TabIndex = 6;
            this.label36.Text = "Password - mật khẩu:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(22, 34);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(54, 13);
            this.label34.TabIndex = 4;
            this.label34.Text = "User - ID:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(22, 61);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(83, 13);
            this.label35.TabIndex = 6;
            this.label35.Text = "User code - mã:";
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.txtVNACCTerminalID);
            this.uiGroupBox7.Controls.Add(this.txtVNACCTerminalPass);
            this.uiGroupBox7.Controls.Add(this.label33);
            this.uiGroupBox7.Controls.Add(this.label32);
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(5, 8);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(387, 94);
            this.uiGroupBox7.TabIndex = 0;
            this.uiGroupBox7.Text = "Terminal - Thiết lập thông tin thiết bị đầu cuối kết nối";
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // txtVNACCTerminalID
            // 
            this.txtVNACCTerminalID.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVNACCTerminalID.Location = new System.Drawing.Point(133, 29);
            this.txtVNACCTerminalID.Name = "txtVNACCTerminalID";
            this.txtVNACCTerminalID.Size = new System.Drawing.Size(161, 21);
            this.txtVNACCTerminalID.TabIndex = 0;
            this.txtVNACCTerminalID.VisualStyleManager = this.vsmMain;
            // 
            // txtVNACCTerminalPass
            // 
            this.txtVNACCTerminalPass.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVNACCTerminalPass.Location = new System.Drawing.Point(133, 56);
            this.txtVNACCTerminalPass.Name = "txtVNACCTerminalPass";
            this.txtVNACCTerminalPass.Size = new System.Drawing.Size(161, 21);
            this.txtVNACCTerminalPass.TabIndex = 1;
            this.txtVNACCTerminalPass.VisualStyleManager = this.vsmMain;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(22, 34);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(72, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "Terminal - ID:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(22, 61);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(105, 13);
            this.label32.TabIndex = 6;
            this.label32.Text = "Terminal - mật khẩu:";
            // 
            // uiTabDBDL
            // 
            this.uiTabDBDL.Controls.Add(this.btnCapNhatDBDL);
            this.uiTabDBDL.Controls.Add(this.label29);
            this.uiTabDBDL.Controls.Add(this.label31);
            this.uiTabDBDL.Controls.Add(this.txtSoTKDBDL);
            this.uiTabDBDL.Location = new System.Drawing.Point(1, 21);
            this.uiTabDBDL.Name = "uiTabDBDL";
            this.uiTabDBDL.Size = new System.Drawing.Size(395, 425);
            this.uiTabDBDL.TabStop = true;
            this.uiTabDBDL.Text = "Đồng bộ dữ liệu Đại lý";
            // 
            // btnCapNhatDBDL
            // 
            this.btnCapNhatDBDL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhatDBDL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnCapNhatDBDL.Icon")));
            this.btnCapNhatDBDL.Location = new System.Drawing.Point(298, 9);
            this.btnCapNhatDBDL.Name = "btnCapNhatDBDL";
            this.btnCapNhatDBDL.Size = new System.Drawing.Size(86, 23);
            this.btnCapNhatDBDL.TabIndex = 8;
            this.btnCapNhatDBDL.Text = "Cập nhật";
            this.btnCapNhatDBDL.VisualStyleManager = this.vsmMain;
            this.btnCapNhatDBDL.Click += new System.EventHandler(this.btnCapNhatDBDL_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Location = new System.Drawing.Point(11, 19);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(181, 13);
            this.label29.TabIndex = 7;
            this.label29.Text = "Số tờ khai Đại lý cập nhật lên Server";
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(11, 48);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(377, 26);
            this.label31.TabIndex = 6;
            this.label31.Text = "Lưu ý: Nếu Đại lý không cập nhật đúng số lượng tờ khai đã quy định, Đại lý sẽ khô" +
                "ng thể tiếp tục khai báo.";
            // 
            // txtSoTKDBDL
            // 
            this.txtSoTKDBDL.Location = new System.Drawing.Point(205, 11);
            this.txtSoTKDBDL.Name = "txtSoTKDBDL";
            this.txtSoTKDBDL.Size = new System.Drawing.Size(77, 21);
            this.txtSoTKDBDL.TabIndex = 5;
            this.txtSoTKDBDL.Text = "0";
            this.txtSoTKDBDL.Value = 0;
            this.txtSoTKDBDL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(125, 459);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uiButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(206, 459);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 2;
            this.uiButton2.Text = "Đóng";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(200, 100);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "uiGroupBox2";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // rfvDiaChi
            // 
            this.rfvDiaChi.ControlToValidate = this.txtDiaChi;
            this.rfvDiaChi.ErrorMessage = "\"Địa chỉ\" không được để trống";
            this.rfvDiaChi.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaChi.Icon")));
            this.rfvDiaChi.Tag = "rfvDiaChi";
            // 
            // rfvMaDN
            // 
            this.rfvMaDN.ControlToValidate = this.txtMaDN;
            this.rfvMaDN.ErrorMessage = "\"Mã doanh nghiệp\" không được trống";
            this.rfvMaDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaDN.Icon")));
            this.rfvMaDN.Tag = "rfvMaDN";
            // 
            // rfvTenDN
            // 
            this.rfvTenDN.ControlToValidate = this.txtTenDN;
            this.rfvTenDN.ErrorMessage = "\"Tên doanh nghiệp \" không được trống";
            this.rfvTenDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDN.Icon")));
            this.rfvTenDN.Tag = "rfvTenDN";
            // 
            // rfvTenNganHQ
            // 
            this.rfvTenNganHQ.ControlToValidate = this.txtTenNganHQ;
            this.rfvTenNganHQ.ErrorMessage = "Chưa nhập tên ngắn hải quan";
            this.rfvTenNganHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenNganHQ.Icon")));
            this.rfvTenNganHQ.Tag = "rfvTenNganHQ";
            // 
            // containerValidator1
            // 
            this.containerValidator1.ContainerToValidate = this;
            this.containerValidator1.HostingForm = this;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // rfvTenCuc
            // 
            this.rfvTenCuc.ControlToValidate = this.txtTenCucHQ;
            this.rfvTenCuc.ErrorMessage = "Chưa nhập tên cục hải quan";
            this.rfvTenCuc.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenCuc.Icon")));
            this.rfvTenCuc.Tag = "rfvTenCuc";
            // 
            // rfvDT
            // 
            this.rfvDT.ControlToValidate = this.txtDienThoaiDN;
            this.rfvDT.ErrorMessage = "\"Điện thoại\" không được trống";
            this.rfvDT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDT.Icon")));
            this.rfvDT.Tag = "rfvDT";
            // 
            // rfvNguoiLienHe
            // 
            this.rfvNguoiLienHe.ControlToValidate = this.txtNguoiLienHeDN;
            this.rfvNguoiLienHe.ErrorMessage = "\"Người liên hệ\" không được trống";
            this.rfvNguoiLienHe.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNguoiLienHe.Icon")));
            this.rfvNguoiLienHe.Tag = "rfvNguoiLienHe";
            // 
            // rfvEmailDN
            // 
            this.rfvEmailDN.ControlToValidate = this.txtMailDoanhNghiep;
            this.rfvEmailDN.ErrorMessage = "\"Email doanh nghiệp\" không được trống";
            this.rfvEmailDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvEmailDN.Icon")));
            this.rfvEmailDN.Tag = "rfvEmailDN";
            // 
            // rfvMaCuc
            // 
            this.rfvMaCuc.ControlToValidate = this.txtMaCuc;
            this.rfvMaCuc.ErrorMessage = "\"Mã cục Hải quan\" không được trống";
            this.rfvMaCuc.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaCuc.Icon")));
            this.rfvMaCuc.Tag = "rfvMaCuc";
            // 
            // rfvDiaChiHQ
            // 
            this.rfvDiaChiHQ.ControlToValidate = this.txtDiaChiHQ;
            this.rfvDiaChiHQ.ErrorMessage = "\"Địa chỉ hải quan\" không được để trống";
            this.rfvDiaChiHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaChiHQ.Icon")));
            this.rfvDiaChiHQ.Tag = "rfvDiaChiHQ";
            // 
            // ThongTinDNAndHQForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 485);
            this.Controls.Add(this.uiGroupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(403, 510);
            this.Name = "ThongTinDNAndHQForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin Doanh nghiệp và Hải quan";
            this.Load += new System.EventHandler(this.SendForm_Load);
            this.Controls.SetChildIndex(this.uiGroupBox2, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPageHQ.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.uiTabPageDN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            this.uiTabPageVNACC.ResumeLayout(false);
            this.uiTabPageVNACC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            this.uiTabDBDL.ResumeLayout(false);
            this.uiTabDBDL.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNganHQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenCuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiLienHe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvEmailDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaCuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChiHQ)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDN;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaChi;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenNganHQ;
        private Company.Controls.CustomValidation.ContainerValidator containerValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label7;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNganHQ;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCucHQ;
        private System.Windows.Forms.Label label5;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenCuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtMailHaiQuan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMailDoanhNghiep;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMid;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaCuc;
        private Janus.Windows.EditControls.UIRadioButton opTiengAnh;
        private Janus.Windows.EditControls.UIRadioButton opVietNam;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoFaxDN;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienThoaiDN;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiLienHeDN;
        private System.Windows.Forms.Label label14;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNguoiLienHe;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvEmailDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaCuc;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageHQ;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageDN;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnOpenWS;
        private System.Windows.Forms.Label lblWS;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDichVu;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtPort;
        private Janus.Windows.GridEX.EditControls.EditBox txtHost;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiHQ;
        private System.Windows.Forms.Label label20;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaChiHQ;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienKhoanTKX;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienKhoanTKN;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThongBaoHetHan;
        private System.Windows.Forms.CheckBox chkCKS;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.EditControls.UIComboBox cbFont;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.UI.Tab.UITabPage uiTabDBDL;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTKDBDL;
        private Janus.Windows.EditControls.UIButton btnCapNhatDBDL;
        private Janus.Windows.GridEX.EditControls.EditBox txtChucVu;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.EditControls.UICheckBox chkLaDNCX;
        private System.Windows.Forms.CheckBox chkSendV4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIRadioButton rbtIsKTX;
        private Janus.Windows.EditControls.UIRadioButton rbtIsTQDT;
        private System.Windows.Forms.LinkLabel linkGetIP;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageVNACC;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.GridEX.EditControls.EditBox txtVNACCTerminalPass;
        private System.Windows.Forms.Label label32;
        private Janus.Windows.GridEX.EditControls.EditBox txtVNACCTerminalID;
        private System.Windows.Forms.Label label33;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.EditControls.EditBox txtVNACCUserID;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private Janus.Windows.GridEX.EditControls.EditBox txtVNACCUserPass;
        private System.Windows.Forms.Label label36;
        private Janus.Windows.GridEX.EditControls.EditBox txtVNACCUserCode;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private System.Windows.Forms.LinkLabel linkVNACCGetIP;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label41;
        private Janus.Windows.GridEX.EditControls.EditBox txtVNACCPortProxy;
        private Janus.Windows.GridEX.EditControls.EditBox txtVNACCHostProxy;
        private System.Windows.Forms.Label label42;
        private Janus.Windows.GridEX.EditControls.EditBox txtVNACCAddress;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button btnOpenWSVNACC;
        private System.Windows.Forms.Label lblWSVNACC;

    }
}