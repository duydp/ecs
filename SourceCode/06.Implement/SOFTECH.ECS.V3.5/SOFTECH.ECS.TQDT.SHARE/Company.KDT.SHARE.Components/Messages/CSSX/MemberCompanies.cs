﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
  public  class MemberCompanies
    {
        /// <summary>
        /// Số lượng chi nhánh
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }

        /// <summary>
        /// Công ty thành viên nhập khẩu, cung ứng nguyên liệu, vật tư để sản xuất xuất khẩu cho các đơn vị trực thuộc Công ty thành viên có CSSX
        /// </summary>
        [XmlElement("MemberCompany")]
        public List<MemberCompany> MemberCompany { get; set; }
    }
}
