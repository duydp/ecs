using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK_HangTonKho_DNCXForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        private FeedBackContent feedbackContent = null;
        //minhnd
        public KDT_SXXK_HangTonDangKy hangTonDK = new KDT_SXXK_HangTonDangKy();
        private KDT_SXXK_HangTon hangTon = new KDT_SXXK_HangTon();
        //
        public KDT_SXXK_BKHangTonKhoDangKy BKHangTon = new KDT_SXXK_BKHangTonKhoDangKy();
        private string msgInfor = string.Empty;
        DataTable dt = new DataTable();
        public BK_HangTonKho_DNCXForm()
        {
            InitializeComponent();
        }

        private void BKNPLCungUngForm_Load(object sender, EventArgs e)
        {
            LoadData();
            
        }
        private void SetCommand()
        {
            if (BKHangTon.TrangThaiXuLy == 0 || BKHangTon.TrangThaiXuLy == 1)
            {
                cmdluu.Enabled = cmdluu1.Enabled = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
        }
        private void LoadData()
        {
            txtSoTiepNhan.Text = BKHangTon.SoTiepNhan.ToString();
            clcNgayTiepNhan.Value = BKHangTon.NgayTiepNhan;
           
            if (BKHangTon.ID == 0)
            {
                BKHangTon.NamQuyetToan = HSTL.NgayBatDau.Year;
                if (0 < HSTL.NgayBatDau.Month && HSTL.NgayBatDau.Month < 4)
                    BKHangTon.QuyQuyetToan = 1;
                else if (3 < HSTL.NgayBatDau.Month && HSTL.NgayBatDau.Month < 7)
                    BKHangTon.QuyQuyetToan = 2;
                else if (6 < HSTL.NgayBatDau.Month && HSTL.NgayBatDau.Month < 9)
                    BKHangTon.QuyQuyetToan = 3;
                else
                    BKHangTon.QuyQuyetToan = 4;
                BKHangTon.TrangThaiXuLy = -1;
            }
            txtNam.Text = BKHangTon.NamQuyetToan.ToString();
            txtQuy.Text = BKHangTon.QuyQuyetToan.ToString();
            if (BKHangTon.TrangThaiXuLy == -1)
            {
                lblTrangThai.Text = "Chưa khai báo";
            }
            else if (BKHangTon.TrangThaiXuLy == 0)
            {
                lblTrangThai.Text = "Chờ duyệt";
            }
            else if (BKHangTon.TrangThaiXuLy == 1)
            {
                lblTrangThai.Text = "Đã duyệt";
            }
            else if (BKHangTon.TrangThaiXuLy == 2)
            {
                lblTrangThai.Text = "Không phê duyệt";
            }
            Company.BLL.KDT.SXXK.BCXuatNhapTon bcxnt = new BCXuatNhapTon();
            dt = bcxnt.GetBC07_KCX_XNTByLanThanhLy(HSTL.MaDoanhNghiep, HSTL.LanThanhLy, HSTL.MaHaiQuanTiepNhan,HSTL.getBKToKhaiNhap());
            dgList2.DataSource = dt;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            
        }

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case"cmdluu":
                    Save();
                    break;
                case "cmdThemMoi":
                    btnAdd_Click(null,null);
                    break;
                case"cmdKhaiBao":
                    SendV5();
                    break;
                case"cmdNhanPhanHoi":
                    FeedBackV5();
                    break;
                case"cmdKetQua":
                    KetQuaXuLyTCU();
                    break;
                case "cmdHuyHangTon":
                    SendHuyV5();
                    break;
                    


            }
        }
        private void Save()
        {
            try
            {
                if (BKHangTon == null)
                {
                    BKHangTon = new KDT_SXXK_BKHangTonKhoDangKy();
                }
                BKHangTon.NamQuyetToan = Convert.ToInt16(txtNam.Text);
                BKHangTon.QuyQuyetToan = Convert.ToInt16(txtQuy.Text);
                BKHangTon.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                BKHangTon.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                BKHangTon.LanThanhLy = HSTL.LanThanhLy;
                BKHangTon.InsertUpdate();
                ShowMessage("Lưu thành công.", false);

            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi khi lưu :"+ ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
           
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.BKHangTonSendHandler(BKHangTon, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void SendV5()
        {
            if (ShowMessage("Bạn có muốn khai báo bảng kê tự cung ứng này đến Hải quan?", true) == "Yes")
            {
                if (BKHangTon.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                //else
                //{
                //    List<KDT_SXXK_BKHangTonKhoDangKy> listHT = KDT_SXXK_BKHangTonKhoDangKy.SelectCollectionDynamic("LanThanhLy= " + HSTL.LanThanhLy, "");
                //    BKHangTon = listHT[0];
                //}
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.HangTon;
                sendXML.master_id = BKHangTon.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdluu.Enabled = cmdluu1.Enabled = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    return;
                }
                try
                {
                   

                        string returnMessage = string.Empty;
                        BKHangTon.GUIDSTR = Guid.NewGuid().ToString();
                        Company.KDT.SHARE.Components.SXXK_SanPham TuCungUng = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferObject_DNCX_HangTonKho(BKHangTon, "", DeclarationIssuer.SXCX_HangTon, false, GlobalSettings.TEN_DON_VI, HSTL.BKCollection[HSTL.getBKToKhaiNhap()].ID);

                    ObjectSend msgSend = new ObjectSend(
                             new NameBase()
                             {
                                 Name = GlobalSettings.TEN_DON_VI,
                                 Identity = BKHangTon.MaDoanhNghiep
                             },
                              new NameBase()
                              {
                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(BKHangTon.MaHaiQuan)),
                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(BKHangTon.MaHaiQuan).Trim() : BKHangTon.MaHaiQuan
                                  //Identity = BKHangTon.MaHaiQuan
                              },
                              new SubjectBase()
                              {

                                  Type = DeclarationIssuer.SXCX_HangTon,
                                  Function = DeclarationFunction.KHAI_BAO,
                                  Reference = BKHangTon.GUIDSTR,
                              },
                              TuCungUng
                            );
                        BKHangTon.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(BKHangTon.ID, MessageTitle.KhaiBaoCX_HangTon);
                            cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdluu.Enabled = cmdluu1.Enabled = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = cmdluu.Enabled = cmdluu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //btnDelete.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.HangTon;
                            sendXML.master_id = BKHangTon.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            BKHangTon.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            //MsgSend sendXML = new MsgSend();
            //sendXML.LoaiHS = LoaiKhaiBao.PhuKien_TK;
            //sendXML.master_id = BKHangTon.ID;
            //if (!sendXML.Load())
            //{
            //    ShowMessage("THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
            //    XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    return;
            //}
            while (isFeedBack)
            {
                string reference = BKHangTon.GUIDSTR;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXCX_HangTon,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXCX_HangTon,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = BKHangTon.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(BKHangTon.MaHaiQuan.Trim())),
                                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(BKHangTon.MaHaiQuan).Trim() : BKHangTon.MaHaiQuan
                                                  //Identity = BKHangTon.MaHaiQuan
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }

            }
        }
        private void KetQuaXuLyTCU()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.BKHangTon.ID;
            form.DeclarationIssuer = DeclarationIssuer.SXCX_HangTon;
            form.ShowDialog(this);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    int ID = Convert.ToInt32(dgList2.CurrentRow.Cells["ID"].Value.ToString());

            //    if (ID > 0)
            //    {
            //        if (ShowMessage("Bạn muốn xóa bảng kê cung ứng của tờ khai " + dgList2.CurrentRow.Cells["SoToKhaiXuat"].Value.ToString() + " Mã sản phẩm " + dgList2.CurrentRow.Cells["MaSanPham"].Value.ToString() + " này không?", true) == "Yes")
            //        {
            //            KDT_SXXK_BKNPLCungUng npl = new KDT_SXXK_BKNPLCungUng();
            //            npl.DeleteFull(ID);
            //        }
            //    }
            //    dgList2.CurrentRow.Delete();
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //minhnd 11/12/2014
        private void SendHuyV5()
        {
            if (ShowMessage("Bạn có muốn khai báo hủy bảng kê hàng tồn này không ?", true) == "Yes")
            {

                if (BKHangTon.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    BKHangTon = KDT_SXXK_BKHangTonKhoDangKy.Load(BKHangTon.ID); //KDT_CX_NPLXinHuyDangKy.LoadFull(BKHangTon.ID);
                    //--BKHangTon.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                }
                MsgSend sendXML = new MsgSend();
                //--sendXML.LoaiHS = LoaiKhaiBao.HuyTieuHuy;
                sendXML.master_id = BKHangTon.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdThemMoi.Enabled = cmdluu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    if (this.BKHangTon.SoTiepNhan != 0)
                    {
                        string returnMessage = string.Empty;
                        BKHangTon.GUIDSTR = Guid.NewGuid().ToString();

                        ObjectSend msgSend = CancelMessageV5(BKHangTon, HSTL.SoQuyetDinh, HSTL.BKCollection[HSTL.getBKToKhaiNhap()].ID);
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(BKHangTon.ID, "Hủy bảng kê hang tồn");
                            cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdThemMoi.Enabled = cmdluu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            sendXML.LoaiHS = LoaiKhaiBao.HuyHangTon;
                            sendXML.master_id = BKHangTon.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();
                            BKHangTon.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }

        }

        public static ObjectSend CancelMessageV5(KDT_SXXK_BKHangTonKhoDangKy BK, string soquyetDinh, long BangKeHSTL_ID)
        {
            
            string returnMessage = string.Empty;
            BK.GUIDSTR = Guid.NewGuid().ToString();
            //SXXK_HangTon TuCungUng = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferHangTon(BK, GlobalSettings.TEN_DON_VI, false, true);
            Company.KDT.SHARE.Components.SXXK_SanPham sp = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferObject_DNCX_HangTonKho(BK, "Ghi Chu", DeclarationIssuer.SXCX_HangTon, true, GlobalSettings.TEN_DON_VI, BangKeHSTL_ID);
            sp.Function = DeclarationFunction.HUY;
            sp.CustomsReference = BK.SoTiepNhan.ToString();
            sp.Acceptance = BK.NgayTiepNhan.ToString("yyyy-MM-dd");
            ObjectSend msgSend = new ObjectSend(
                 new NameBase()
                 {
                     Name = GlobalSettings.TEN_DON_VI,
                     Identity = BK.MaDoanhNghiep
                 },
                  new NameBase()
                  {
                      Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(BK.MaHaiQuan)),
                      Identity = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(BK.MaHaiQuan)
                  },
                  new SubjectBase()
                  {
                      Type = DeclarationIssuer.SXCX_HangTon,
                      Function = DeclarationFunction.HUY,
                      Reference = BK.GUIDSTR,
                  },
                  sp
                );
            return msgSend;
        }
        
    }

    

}