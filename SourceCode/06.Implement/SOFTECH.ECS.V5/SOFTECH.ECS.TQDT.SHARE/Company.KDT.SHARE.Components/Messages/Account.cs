﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class Account
    {
        //Số tài khoản
        [XmlElement("number")]
        public string Number { get; set; }
        // Ngân hàng
        [XmlElement("Bank")]
        public NameBase Bank { get; set; }

    }
}
