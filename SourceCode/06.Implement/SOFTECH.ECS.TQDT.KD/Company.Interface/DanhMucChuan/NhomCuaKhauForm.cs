﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.DuLieuChuan;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Company.BLL.KDT;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Collections.Generic;
using Company.Interface;
using System.Data;
using Company.BLL.KDT.SXXK;
namespace company.Interface.DanhMucChuan
{
    public partial class NhomCuaKhauForm : BaseForm
    {
        DataTable _dtNhomCuaKhau;
        DataTable _dtCuaKhau;
        public NhomCuaKhauForm()
        {
            InitializeComponent();
            _dtCuaKhau = CuaKhau.SelectAll().Tables[0];

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void NhomCuaKhauForm_Load(object sender, EventArgs e)
        {
          
            donViHaiQuanControl1.Ma = GlobalSettings.MA_HAI_QUAN;
            donViHaiQuanControl1.ReadOnly = false;
            cbCuc_SelectedValueChanged(null, null);
        }

        private void cbCuc_SelectedValueChanged(object sender, EventArgs e)
        {
         
           _dtNhomCuaKhau = NhomCuaKhau.GetCuaKhau(donViHaiQuanControl1.Ma).Tables[0];
           DataTable dtCuaKhauCopy;
           dtCuaKhauCopy = _dtCuaKhau.Clone();
           foreach (DataRow dr in _dtCuaKhau.Rows)
           {
             DataRow[] arrDr = _dtNhomCuaKhau.Select("ID='" + dr[0].ToString() + "'");
             if (arrDr.Length == 0)
             {
                 dtCuaKhauCopy.ImportRow(dr);
             }
           }
           gridFrom.DataSource = dtCuaKhauCopy;
           gridTo.DataSource = _dtNhomCuaKhau;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            DataTable dtFrom = (DataTable)gridFrom.DataSource;
            DataTable dtTo = (DataTable)gridTo.DataSource;            
            List<DataRow> rowsMove = new List<DataRow>();            
            foreach (GridEXRow row in gridFrom.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    DataRowView drv = (DataRowView)row.DataRow;
                    rowsMove.Add(drv.Row);
                }
            }
            foreach (DataRow dr in rowsMove)
            {

                dtTo.ImportRow(dr);
                dtFrom.Rows.Remove(dr);
               
            }
            gridFrom.Refresh();
            gridTo.Refresh();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            DataTable dtFrom = (DataTable)gridFrom.DataSource;
            DataTable dtTo = (DataTable)gridTo.DataSource;
            List<DataRow> rowsMove = new List<DataRow>();
            foreach (GridEXRow row in gridTo.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    DataRowView drv = (DataRowView)row.DataRow;
                    rowsMove.Add(drv.Row);
                }
            }
            foreach (DataRow dr in rowsMove)
            {

                dtFrom.ImportRow(dr);
                dtTo.Rows.Remove(dr);

            }
            gridFrom.Refresh();
            gridTo.Refresh();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {

            DataTable dtFrom = (DataTable)gridFrom.DataSource;
            DataTable dtTo = (DataTable)gridTo.DataSource;

            List<NhomCuaKhau> nhomcuakhauDelete = new List<NhomCuaKhau>();
            List<NhomCuaKhau> nhomcuakhauSave = new List<NhomCuaKhau>();

            string cuc = donViHaiQuanControl1.Ma; 
            foreach (DataRow dr in dtFrom.Rows)
            {
                NhomCuaKhau n = new NhomCuaKhau();
                n.Cuc_ID = cuc;
                n.CuaKhau_ID = dr[0].ToString();
                nhomcuakhauDelete.Add(n);
            }
            foreach (DataRow dr in dtTo.Rows)
            {
                NhomCuaKhau n = new NhomCuaKhau();
                n.Cuc_ID = cuc;
                n.CuaKhau_ID = dr[0].ToString();
                nhomcuakhauSave.Add(n);
            }
            try
            {
               NhomCuaKhau.DeleteCollectionItem(nhomcuakhauDelete);
               NhomCuaKhau.InsertUpdateCollection(nhomcuakhauSave);
               Globals.ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage("Lưu không thành công", false);
            }
            

        }

        private void donViHaiQuanControl1_ValueChanged(object sender, EventArgs e)
        {
            _dtNhomCuaKhau = NhomCuaKhau.GetCuaKhau(donViHaiQuanControl1.Ma).Tables[0];
            FillData();
        }

        private void FillData()
        {
            DataTable dtCuaKhauCopy;
            dtCuaKhauCopy = _dtCuaKhau.Clone();
            foreach (DataRow dr in _dtCuaKhau.Rows)
            {
                DataRow[] arrDr = _dtNhomCuaKhau.Select("ID='" + dr[0].ToString() + "'");
                if (arrDr.Length == 0)
                {
                    dtCuaKhauCopy.ImportRow(dr);
                }
            }
            gridFrom.DataSource = dtCuaKhauCopy;
            gridTo.DataSource = _dtNhomCuaKhau;
        }

        private void chAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chAll.Checked)
            {
                _dtNhomCuaKhau = NhomCuaKhau.GetCuaKhau("%").Tables[0];
                FillData();
            }
            else
            {
                donViHaiQuanControl1_ValueChanged(null, null);
            }
        }
    }
}
