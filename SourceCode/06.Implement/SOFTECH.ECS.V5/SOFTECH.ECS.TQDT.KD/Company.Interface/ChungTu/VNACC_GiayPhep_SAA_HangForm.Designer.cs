﻿namespace Company.Interface
{
    partial class VNACC_GiayPhep_SAA_HangForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout grdHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_GiayPhep_SAA_HangForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.cboTinhBiet = new Janus.Windows.EditControls.UIComboBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.ucDonViTinhSoLuongKiemDich = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucDonViTinhTongSoLuongNhapKhau = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucDonViTinhTrongLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucDonViTinhTrongLuongCaBi = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.txtKichCoCaThe = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMucDichSuDung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLoaiBaoBi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtQuyCachDongGoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMaSoHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSoLuongKiemDich = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTongSoLuongNhapKhau = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTuoi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTrongLuongTinh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTrongLuongCaBi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNoiSanXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grdHang = new Janus.Windows.GridEX.GridEX();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMaSoHangHoa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaSoHangHoa)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 418), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 418);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 394);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 394);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grdHang);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(624, 418);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.cboTinhBiet);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Controls.Add(this.ucDonViTinhSoLuongKiemDich);
            this.uiGroupBox1.Controls.Add(this.ucDonViTinhTongSoLuongNhapKhau);
            this.uiGroupBox1.Controls.Add(this.ucDonViTinhTrongLuong);
            this.uiGroupBox1.Controls.Add(this.ucDonViTinhTrongLuongCaBi);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.txtKichCoCaThe);
            this.uiGroupBox1.Controls.Add(this.txtMucDichSuDung);
            this.uiGroupBox1.Controls.Add(this.txtLoaiBaoBi);
            this.uiGroupBox1.Controls.Add(this.txtQuyCachDongGoi);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtMaSoHangHoa);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongKiemDich);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.txtTongSoLuongNhapKhau);
            this.uiGroupBox1.Controls.Add(this.txtTuoi);
            this.uiGroupBox1.Controls.Add(this.txtTrongLuongTinh);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.txtTrongLuongCaBi);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.txtNoiSanXuat);
            this.uiGroupBox1.Controls.Add(this.txtTenHangHoa);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(624, 305);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // cboTinhBiet
            // 
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Nữ";
            uiComboBoxItem1.Value = "F";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Nam";
            uiComboBoxItem2.Value = "M";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Không xác định";
            uiComboBoxItem3.Value = "U";
            this.cboTinhBiet.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3});
            this.cboTinhBiet.Location = new System.Drawing.Point(314, 64);
            this.cboTinhBiet.Name = "cboTinhBiet";
            this.cboTinhBiet.Size = new System.Drawing.Size(60, 21);
            this.cboTinhBiet.TabIndex = 5;
            this.cboTinhBiet.Enter += new System.EventHandler(this.ucCategory_OnEnter);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(282, 276);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 22;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = global::Company.Interface.Properties.Resources.disk;
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.Location = new System.Drawing.Point(115, 276);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 20;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // ucDonViTinhSoLuongKiemDich
            // 
            this.ucDonViTinhSoLuongKiemDich.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucDonViTinhSoLuongKiemDich.Code = "";
            this.ucDonViTinhSoLuongKiemDich.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhSoLuongKiemDich.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhSoLuongKiemDich.IsValidate = true;
            this.ucDonViTinhSoLuongKiemDich.Location = new System.Drawing.Point(561, 165);
            this.ucDonViTinhSoLuongKiemDich.Name = "ucDonViTinhSoLuongKiemDich";
            this.ucDonViTinhSoLuongKiemDich.Name_VN = "";
            this.ucDonViTinhSoLuongKiemDich.ShowColumnCode = true;
            this.ucDonViTinhSoLuongKiemDich.ShowColumnName = false;
            this.ucDonViTinhSoLuongKiemDich.Size = new System.Drawing.Size(53, 26);
            this.ucDonViTinhSoLuongKiemDich.TabIndex = 13;
            this.ucDonViTinhSoLuongKiemDich.TagName = "";
            this.ucDonViTinhSoLuongKiemDich.WhereCondition = "";
            this.ucDonViTinhSoLuongKiemDich.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucDonViTinhTongSoLuongNhapKhau
            // 
            this.ucDonViTinhTongSoLuongNhapKhau.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucDonViTinhTongSoLuongNhapKhau.Code = "";
            this.ucDonViTinhTongSoLuongNhapKhau.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhTongSoLuongNhapKhau.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhTongSoLuongNhapKhau.IsValidate = true;
            this.ucDonViTinhTongSoLuongNhapKhau.Location = new System.Drawing.Point(258, 165);
            this.ucDonViTinhTongSoLuongNhapKhau.Name = "ucDonViTinhTongSoLuongNhapKhau";
            this.ucDonViTinhTongSoLuongNhapKhau.Name_VN = "";
            this.ucDonViTinhTongSoLuongNhapKhau.ShowColumnCode = true;
            this.ucDonViTinhTongSoLuongNhapKhau.ShowColumnName = false;
            this.ucDonViTinhTongSoLuongNhapKhau.Size = new System.Drawing.Size(53, 26);
            this.ucDonViTinhTongSoLuongNhapKhau.TabIndex = 11;
            this.ucDonViTinhTongSoLuongNhapKhau.TagName = "";
            this.ucDonViTinhTongSoLuongNhapKhau.WhereCondition = "";
            this.ucDonViTinhTongSoLuongNhapKhau.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucDonViTinhTrongLuong
            // 
            this.ucDonViTinhTrongLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucDonViTinhTrongLuong.Code = "";
            this.ucDonViTinhTrongLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhTrongLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhTrongLuong.IsValidate = true;
            this.ucDonViTinhTrongLuong.Location = new System.Drawing.Point(258, 192);
            this.ucDonViTinhTrongLuong.Name = "ucDonViTinhTrongLuong";
            this.ucDonViTinhTrongLuong.Name_VN = "";
            this.ucDonViTinhTrongLuong.ShowColumnCode = true;
            this.ucDonViTinhTrongLuong.ShowColumnName = false;
            this.ucDonViTinhTrongLuong.Size = new System.Drawing.Size(53, 26);
            this.ucDonViTinhTrongLuong.TabIndex = 15;
            this.ucDonViTinhTrongLuong.TagName = "";
            this.ucDonViTinhTrongLuong.WhereCondition = "";
            this.ucDonViTinhTrongLuong.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucDonViTinhTrongLuongCaBi
            // 
            this.ucDonViTinhTrongLuongCaBi.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucDonViTinhTrongLuongCaBi.Code = "";
            this.ucDonViTinhTrongLuongCaBi.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhTrongLuongCaBi.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhTrongLuongCaBi.IsValidate = true;
            this.ucDonViTinhTrongLuongCaBi.Location = new System.Drawing.Point(561, 192);
            this.ucDonViTinhTrongLuongCaBi.Name = "ucDonViTinhTrongLuongCaBi";
            this.ucDonViTinhTrongLuongCaBi.Name_VN = "";
            this.ucDonViTinhTrongLuongCaBi.ShowColumnCode = true;
            this.ucDonViTinhTrongLuongCaBi.ShowColumnName = false;
            this.ucDonViTinhTrongLuongCaBi.Size = new System.Drawing.Size(53, 26);
            this.ucDonViTinhTrongLuongCaBi.TabIndex = 17;
            this.ucDonViTinhTrongLuongCaBi.TagName = "";
            this.ucDonViTinhTrongLuongCaBi.WhereCondition = "";
            this.ucDonViTinhTrongLuongCaBi.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // btnXoa
            // 
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(199, 276);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 21;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // txtKichCoCaThe
            // 
            this.txtKichCoCaThe.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtKichCoCaThe.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtKichCoCaThe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKichCoCaThe.Location = new System.Drawing.Point(418, 141);
            this.txtKichCoCaThe.Name = "txtKichCoCaThe";
            this.txtKichCoCaThe.Size = new System.Drawing.Size(196, 21);
            this.txtKichCoCaThe.TabIndex = 9;
            this.txtKichCoCaThe.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKichCoCaThe.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtKichCoCaThe.Leave += new System.EventHandler(this.txtMaSoHangHoa_Leave);
            // 
            // txtMucDichSuDung
            // 
            this.txtMucDichSuDung.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMucDichSuDung.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMucDichSuDung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMucDichSuDung.Location = new System.Drawing.Point(115, 249);
            this.txtMucDichSuDung.Name = "txtMucDichSuDung";
            this.txtMucDichSuDung.Size = new System.Drawing.Size(499, 21);
            this.txtMucDichSuDung.TabIndex = 19;
            this.txtMucDichSuDung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMucDichSuDung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMucDichSuDung.Leave += new System.EventHandler(this.txtMaSoHangHoa_Leave);
            // 
            // txtLoaiBaoBi
            // 
            this.txtLoaiBaoBi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtLoaiBaoBi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtLoaiBaoBi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoaiBaoBi.Location = new System.Drawing.Point(115, 222);
            this.txtLoaiBaoBi.Name = "txtLoaiBaoBi";
            this.txtLoaiBaoBi.Size = new System.Drawing.Size(499, 21);
            this.txtLoaiBaoBi.TabIndex = 18;
            this.txtLoaiBaoBi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLoaiBaoBi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtLoaiBaoBi.Leave += new System.EventHandler(this.txtMaSoHangHoa_Leave);
            // 
            // txtQuyCachDongGoi
            // 
            this.txtQuyCachDongGoi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtQuyCachDongGoi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtQuyCachDongGoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuyCachDongGoi.Location = new System.Drawing.Point(115, 141);
            this.txtQuyCachDongGoi.Name = "txtQuyCachDongGoi";
            this.txtQuyCachDongGoi.Size = new System.Drawing.Size(196, 21);
            this.txtQuyCachDongGoi.TabIndex = 8;
            this.txtQuyCachDongGoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtQuyCachDongGoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtQuyCachDongGoi.Leave += new System.EventHandler(this.txtMaSoHangHoa_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(10, 254);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Mục đích sử dụng";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(317, 146);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Kích cỡ cá thể";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 227);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Loại bao bì";
            // 
            // txtMaSoHangHoa
            // 
            this.txtMaSoHangHoa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSoHangHoa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSoHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSoHangHoa.Location = new System.Drawing.Point(115, 64);
            this.txtMaSoHangHoa.Name = "txtMaSoHangHoa";
            this.txtMaSoHangHoa.Size = new System.Drawing.Size(135, 21);
            this.txtMaSoHangHoa.TabIndex = 3;
            this.txtMaSoHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSoHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSoHangHoa.Leave += new System.EventHandler(this.txtMaSoHangHoa_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Quy cách đóng gói";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Nơi sản xuất";
            // 
            // txtSoLuongKiemDich
            // 
            this.txtSoLuongKiemDich.DecimalDigits = 0;
            this.txtSoLuongKiemDich.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongKiemDich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongKiemDich.Location = new System.Drawing.Point(418, 168);
            this.txtSoLuongKiemDich.MaxLength = 8;
            this.txtSoLuongKiemDich.Name = "txtSoLuongKiemDich";
            this.txtSoLuongKiemDich.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongKiemDich.Size = new System.Drawing.Size(137, 21);
            this.txtSoLuongKiemDich.TabIndex = 12;
            this.txtSoLuongKiemDich.Text = "0";
            this.txtSoLuongKiemDich.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongKiemDich.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongKiemDich.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Mã số hàng hóa";
            // 
            // txtTongSoLuongNhapKhau
            // 
            this.txtTongSoLuongNhapKhau.DecimalDigits = 0;
            this.txtTongSoLuongNhapKhau.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongSoLuongNhapKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoLuongNhapKhau.Location = new System.Drawing.Point(115, 168);
            this.txtTongSoLuongNhapKhau.MaxLength = 8;
            this.txtTongSoLuongNhapKhau.Name = "txtTongSoLuongNhapKhau";
            this.txtTongSoLuongNhapKhau.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoLuongNhapKhau.Size = new System.Drawing.Size(135, 21);
            this.txtTongSoLuongNhapKhau.TabIndex = 10;
            this.txtTongSoLuongNhapKhau.Text = "0";
            this.txtTongSoLuongNhapKhau.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoLuongNhapKhau.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoLuongNhapKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTuoi
            // 
            this.txtTuoi.DecimalDigits = 0;
            this.txtTuoi.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTuoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTuoi.Location = new System.Drawing.Point(418, 64);
            this.txtTuoi.MaxLength = 3;
            this.txtTuoi.Name = "txtTuoi";
            this.txtTuoi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTuoi.Size = new System.Drawing.Size(59, 21);
            this.txtTuoi.TabIndex = 6;
            this.txtTuoi.Text = "0";
            this.txtTuoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTuoi.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTuoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTrongLuongTinh
            // 
            this.txtTrongLuongTinh.DecimalDigits = 3;
            this.txtTrongLuongTinh.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTrongLuongTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongLuongTinh.Location = new System.Drawing.Point(115, 195);
            this.txtTrongLuongTinh.MaxLength = 10;
            this.txtTrongLuongTinh.Name = "txtTrongLuongTinh";
            this.txtTrongLuongTinh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTrongLuongTinh.Size = new System.Drawing.Size(135, 21);
            this.txtTrongLuongTinh.TabIndex = 14;
            this.txtTrongLuongTinh.Text = "0.000";
            this.txtTrongLuongTinh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTrongLuongTinh.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTrongLuongTinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(317, 173);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Số lượng kiểm dịch";
            // 
            // txtTrongLuongCaBi
            // 
            this.txtTrongLuongCaBi.DecimalDigits = 3;
            this.txtTrongLuongCaBi.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTrongLuongCaBi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongLuongCaBi.Location = new System.Drawing.Point(418, 195);
            this.txtTrongLuongCaBi.MaxLength = 10;
            this.txtTrongLuongCaBi.Name = "txtTrongLuongCaBi";
            this.txtTrongLuongCaBi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTrongLuongCaBi.Size = new System.Drawing.Size(137, 21);
            this.txtTrongLuongCaBi.TabIndex = 16;
            this.txtTrongLuongCaBi.Text = "0.000";
            this.txtTrongLuongCaBi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTrongLuongCaBi.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTrongLuongCaBi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(10, 173);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Số lượng nhập khẩu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Trọng lượng tịnh";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(385, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Tuổi";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(263, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tính biệt";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(317, 200);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Trọng lượng cả bì";
            // 
            // txtNoiSanXuat
            // 
            this.txtNoiSanXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiSanXuat.Location = new System.Drawing.Point(115, 91);
            this.txtNoiSanXuat.Multiline = true;
            this.txtNoiSanXuat.Name = "txtNoiSanXuat";
            this.txtNoiSanXuat.Size = new System.Drawing.Size(499, 44);
            this.txtNoiSanXuat.TabIndex = 7;
            this.txtNoiSanXuat.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNoiSanXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenHangHoa
            // 
            this.txtTenHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangHoa.Location = new System.Drawing.Point(115, 14);
            this.txtTenHangHoa.Multiline = true;
            this.txtTenHangHoa.Name = "txtTenHangHoa";
            this.txtTenHangHoa.Size = new System.Drawing.Size(499, 44);
            this.txtTenHangHoa.TabIndex = 1;
            this.txtTenHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHangHoa.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mô tả hàng hóa";
            // 
            // grdHang
            // 
            this.grdHang.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdHang.CardColumnHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.ColumnAutoResize = true;
            grdHang_DesignTimeLayout.LayoutString = resources.GetString("grdHang_DesignTimeLayout.LayoutString");
            this.grdHang.DesignTimeLayout = grdHang_DesignTimeLayout;
            this.grdHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grdHang.GroupByBoxVisible = false;
            this.grdHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.Location = new System.Drawing.Point(0, 305);
            this.grdHang.Name = "grdHang";
            this.grdHang.RecordNavigator = true;
            this.grdHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdHang.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdHang.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdHang.Size = new System.Drawing.Size(624, 113);
            this.grdHang.TabIndex = 1;
            this.grdHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdHang.VisualStyleManager = this.vsmMain;
            this.grdHang.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grdHang_RowDoubleClick);
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // rfvMaSoHangHoa
            // 
            this.rfvMaSoHangHoa.ControlToValidate = this.txtMaSoHangHoa;
            this.rfvMaSoHangHoa.ErrorMessage = "\"Mã số hàng hóa\" không được để trống.";
            this.rfvMaSoHangHoa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaSoHangHoa.Icon")));
            this.rfvMaSoHangHoa.Tag = "rfvMaSoHangHoa";
            // 
            // VNACC_GiayPhep_SAA_HangForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 424);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_GiayPhep_SAA_HangForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin hàng hóa";
            this.Load += new System.EventHandler(this.VNACC_GiayPhep_SAA_HangForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaSoHangHoa)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX grdHang;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTrongLuongCaBi;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangHoa;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSoHangHoa;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaSoHangHoa;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhTrongLuongCaBi;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhTrongLuong;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTrongLuongTinh;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIComboBox cboTinhBiet;
        private System.Windows.Forms.Label label5;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhTongSoLuongNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuyCachDongGoi;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoLuongNhapKhau;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTuoi;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiSanXuat;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhSoLuongKiemDich;
        private Janus.Windows.GridEX.EditControls.EditBox txtKichCoCaThe;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongKiemDich;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtMucDichSuDung;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiBaoBi;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label1;
    }
}