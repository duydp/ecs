using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhCapPhepKiemDichDongVatNK_2 : DevExpress.XtraReports.UI.XtraReport
    {
        public QuyetDinhCapPhepKiemDichDongVatNK_2()
        {
            InitializeComponent();
        }
        public void BindingReport(VAJP030 vajp)
        {
            int HangHoa = vajp.HangHoa.Count;
            if (HangHoa <= 5)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3 + Math.Round((decimal)HangHoa / 5, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            DataTable dt = new DataTable();
            if (vajp != null && vajp.HangHoa.Count > 0)
            {
                int STT = 0;
                dt.Columns.Add("STT", typeof(string));
                dt.Columns.Add("MoTaHH", typeof(string));
                dt.Columns.Add("MaSoHH", typeof(string));
                dt.Columns.Add("TinhBiet", typeof(string));
                dt.Columns.Add("Tuoi", typeof(string));
                dt.Columns.Add("NoiSanXuat", typeof(string));
                dt.Columns.Add("QuyCachDongGoi", typeof(string));
                dt.Columns.Add("TongSoLuongNK", typeof(string));
                dt.Columns.Add("DVTTongSoLuongNK", typeof(string));
                dt.Columns.Add("KichCoCaThe", typeof(string));
                dt.Columns.Add("TrongLuongTinh", typeof(string));
                dt.Columns.Add("DVTTrongLuongTinh", typeof(string));
                dt.Columns.Add("TrongLuongCaBi", typeof(string));
                dt.Columns.Add("DVTTrongLuongCaBi", typeof(string));
                dt.Columns.Add("SoLuongKiemDich", typeof(string));
                dt.Columns.Add("DVTSoLuongKiemDich", typeof(string));
                dt.Columns.Add("LoaiBaoBi", typeof(string));
                dt.Columns.Add("MucDichSD", typeof(string));
                for (int i = 0; i < vajp.HangHoa.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    dr["STT"] = STT.ToString().Trim();
                    dr["MoTaHH"] = vajp.HangHoa[i].C1.GetValue().ToString().Trim();
                    dr["MaSoHH"] = vajp.HangHoa[i].C2.GetValue().ToString().Trim();
                    dr["TinhBiet"] = vajp.HangHoa[i].C3.GetValue().ToString().Trim();
                    dr["Tuoi"] = vajp.HangHoa[i].C4.GetValue().ToString().Trim();
                    dr["NoiSanXuat"] = vajp.HangHoa[i].C5.GetValue().ToString().Trim();
                    dr["QuyCachDongGoi"] = vajp.HangHoa[i].PMD.GetValue().ToString().Trim();
                    dr["TongSoLuongNK"] = vajp.HangHoa[i].C7.GetValue().ToString().Trim();
                    dr["DVTTongSoLuongNK"] = vajp.HangHoa[i].C8.GetValue().ToString().Trim();
                    dr["KichCoCaThe"] = vajp.HangHoa[i].C9.GetValue().ToString().Trim();
                    dr["TrongLuongTinh"] = vajp.HangHoa[i].C10.GetValue().ToString().Trim();
                    dr["DVTTrongLuongTinh"] = vajp.HangHoa[i].C11.GetValue().ToString().Trim();
                    dr["TrongLuongCaBi"] = vajp.HangHoa[i].C12.GetValue().ToString().Trim();
                    dr["DVTTrongLuongCaBi"] = vajp.HangHoa[i].C13.GetValue().ToString().Trim();
                    dr["SoLuongKiemDich"] = vajp.HangHoa[i].C14.GetValue().ToString().Trim();
                    dr["DVTSoLuongKiemDich"] = vajp.HangHoa[i].C15.GetValue().ToString().Trim();
                    dr["LoaiBaoBi"] = vajp.HangHoa[i].C6.GetValue().ToString().Trim();
                    dr["MucDichSD"] = vajp.HangHoa[i].C16.GetValue().ToString().Trim();
                    dt.Rows.Add(dr);
                }
                BindingReportHang(dt);
            }
        }
        public void BindingReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblMoTaHH.DataBindings.Add("Text", DetailReport.DataSource, "MoTaHH");
            lblMaSoHH.DataBindings.Add("Text", DetailReport.DataSource, "MaSoHH");
            lblTinhBiet.DataBindings.Add("Text", DetailReport.DataSource, "TinhBiet");
            lblTuoi.DataBindings.Add("Text", DetailReport.DataSource, "Tuoi");
            lblNoiSanXuat.DataBindings.Add("Text", DetailReport.DataSource, "NoiSanXuat");
            lblQuyCachDongGoi.DataBindings.Add("Text", DetailReport.DataSource, "QuyCachDongGoi");
            lblSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "TongSoLuongNK");
            lblDVTSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "DVTTongSoLuongNK");
            lblKichCoCaThe.DataBindings.Add("Text", DetailReport.DataSource, "KichCoCaThe");
            lblTrongLuongTinh.DataBindings.Add("Text", DetailReport.DataSource, "TrongLuongTinh");
            lblDVTTrongLuong.DataBindings.Add("Text", DetailReport.DataSource, "DVTTrongLuongTinh");
            lblTrongLuongCaB.DataBindings.Add("Text", DetailReport.DataSource, "TrongLuongCaBi");
            lblDVTTrongLuongCaBi.DataBindings.Add("Text", DetailReport.DataSource, "DVTTrongLuongCaBi");
            lblSoLuongKiemDich.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongKiemDich");
            lblDVTSoLuongKD.DataBindings.Add("Text", DetailReport.DataSource, "DVTSoLuongKiemDich");
            lblLoaiBaoBi.DataBindings.Add("Text", DetailReport.DataSource, "LoaiBaoBi");
            lblMucDichSuDung.DataBindings.Add("Text", DetailReport.DataSource, "MucDichSD");
        }

        private void xrLabel1_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            cell.Text = (e.PageIndex + 3).ToString();
        }




    }
}
