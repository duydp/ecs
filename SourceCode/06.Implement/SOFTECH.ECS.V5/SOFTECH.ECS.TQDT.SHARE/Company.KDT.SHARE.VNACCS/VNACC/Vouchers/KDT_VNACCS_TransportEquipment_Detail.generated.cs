﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_TransportEquipment_Detail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TransportEquipment_ID { set; get; }
		public string SoContainer { set; get; }
		public string SoVanDon { set; get; }
		public string SoSeal { set; get; }
		public string SoSealHQ { set; get; }
		public string LoaiContainer { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_TransportEquipment_Detail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_TransportEquipment_Detail> collection = new List<KDT_VNACCS_TransportEquipment_Detail>();
			while (reader.Read())
			{
				KDT_VNACCS_TransportEquipment_Detail entity = new KDT_VNACCS_TransportEquipment_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TransportEquipment_ID"))) entity.TransportEquipment_ID = reader.GetInt64(reader.GetOrdinal("TransportEquipment_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer"))) entity.SoContainer = reader.GetString(reader.GetOrdinal("SoContainer"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSeal"))) entity.SoSeal = reader.GetString(reader.GetOrdinal("SoSeal"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSealHQ"))) entity.SoSealHQ = reader.GetString(reader.GetOrdinal("SoSealHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiContainer"))) entity.LoaiContainer = reader.GetString(reader.GetOrdinal("LoaiContainer"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_TransportEquipment_Detail> collection, long id)
        {
            foreach (KDT_VNACCS_TransportEquipment_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TransportEquipment_Details VALUES(@TransportEquipment_ID, @SoContainer, @SoVanDon, @SoSeal, @SoSealHQ, @LoaiContainer)";
            string update = "UPDATE t_KDT_VNACCS_TransportEquipment_Details SET TransportEquipment_ID = @TransportEquipment_ID, SoContainer = @SoContainer, SoVanDon = @SoVanDon, SoSeal = @SoSeal, SoSealHQ = @SoSealHQ, LoaiContainer = @LoaiContainer WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TransportEquipment_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TransportEquipment_ID", SqlDbType.BigInt, "TransportEquipment_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoContainer", SqlDbType.NVarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal", SqlDbType.NVarChar, "SoSeal", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSealHQ", SqlDbType.NVarChar, "SoSealHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiContainer", SqlDbType.NVarChar, "LoaiContainer", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TransportEquipment_ID", SqlDbType.BigInt, "TransportEquipment_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoContainer", SqlDbType.NVarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal", SqlDbType.NVarChar, "SoSeal", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSealHQ", SqlDbType.NVarChar, "SoSealHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiContainer", SqlDbType.NVarChar, "LoaiContainer", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TransportEquipment_Details VALUES(@TransportEquipment_ID, @SoContainer, @SoVanDon, @SoSeal, @SoSealHQ, @LoaiContainer)";
            string update = "UPDATE t_KDT_VNACCS_TransportEquipment_Details SET TransportEquipment_ID = @TransportEquipment_ID, SoContainer = @SoContainer, SoVanDon = @SoVanDon, SoSeal = @SoSeal, SoSealHQ = @SoSealHQ, LoaiContainer = @LoaiContainer WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TransportEquipment_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TransportEquipment_ID", SqlDbType.BigInt, "TransportEquipment_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoContainer", SqlDbType.NVarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal", SqlDbType.NVarChar, "SoSeal", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSealHQ", SqlDbType.NVarChar, "SoSealHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiContainer", SqlDbType.NVarChar, "LoaiContainer", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TransportEquipment_ID", SqlDbType.BigInt, "TransportEquipment_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoContainer", SqlDbType.NVarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal", SqlDbType.NVarChar, "SoSeal", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSealHQ", SqlDbType.NVarChar, "SoSealHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiContainer", SqlDbType.NVarChar, "LoaiContainer", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_TransportEquipment_Detail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_TransportEquipment_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_TransportEquipment_Detail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_TransportEquipment_Detail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_TransportEquipment_Detail> SelectCollectionBy_TransportEquipment_ID(long transportEquipment_ID)
		{
            IDataReader reader = SelectReaderBy_TransportEquipment_ID(transportEquipment_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TransportEquipment_ID(long transportEquipment_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectBy_TransportEquipment_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TransportEquipment_ID", SqlDbType.BigInt, transportEquipment_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TransportEquipment_ID(long transportEquipment_ID)
		{
			const string spName = "p_KDT_VNACCS_TransportEquipment_Detail_SelectBy_TransportEquipment_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TransportEquipment_ID", SqlDbType.BigInt, transportEquipment_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_TransportEquipment_Detail(long transportEquipment_ID, string soContainer, string soVanDon, string soSeal, string soSealHQ, string loaiContainer)
		{
			KDT_VNACCS_TransportEquipment_Detail entity = new KDT_VNACCS_TransportEquipment_Detail();	
			entity.TransportEquipment_ID = transportEquipment_ID;
			entity.SoContainer = soContainer;
			entity.SoVanDon = soVanDon;
			entity.SoSeal = soSeal;
			entity.SoSealHQ = soSealHQ;
			entity.LoaiContainer = loaiContainer;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TransportEquipment_ID", SqlDbType.BigInt, TransportEquipment_ID);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.NVarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.NVarChar, SoSeal);
			db.AddInParameter(dbCommand, "@SoSealHQ", SqlDbType.NVarChar, SoSealHQ);
			db.AddInParameter(dbCommand, "@LoaiContainer", SqlDbType.NVarChar, LoaiContainer);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_TransportEquipment_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TransportEquipment_Detail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_TransportEquipment_Detail(long id, long transportEquipment_ID, string soContainer, string soVanDon, string soSeal, string soSealHQ, string loaiContainer)
		{
			KDT_VNACCS_TransportEquipment_Detail entity = new KDT_VNACCS_TransportEquipment_Detail();			
			entity.ID = id;
			entity.TransportEquipment_ID = transportEquipment_ID;
			entity.SoContainer = soContainer;
			entity.SoVanDon = soVanDon;
			entity.SoSeal = soSeal;
			entity.SoSealHQ = soSealHQ;
			entity.LoaiContainer = loaiContainer;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_TransportEquipment_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TransportEquipment_ID", SqlDbType.BigInt, TransportEquipment_ID);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.NVarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.NVarChar, SoSeal);
			db.AddInParameter(dbCommand, "@SoSealHQ", SqlDbType.NVarChar, SoSealHQ);
			db.AddInParameter(dbCommand, "@LoaiContainer", SqlDbType.NVarChar, LoaiContainer);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_TransportEquipment_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TransportEquipment_Detail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_TransportEquipment_Detail(long id, long transportEquipment_ID, string soContainer, string soVanDon, string soSeal, string soSealHQ, string loaiContainer)
		{
			KDT_VNACCS_TransportEquipment_Detail entity = new KDT_VNACCS_TransportEquipment_Detail();			
			entity.ID = id;
			entity.TransportEquipment_ID = transportEquipment_ID;
			entity.SoContainer = soContainer;
			entity.SoVanDon = soVanDon;
			entity.SoSeal = soSeal;
			entity.SoSealHQ = soSealHQ;
			entity.LoaiContainer = loaiContainer;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TransportEquipment_ID", SqlDbType.BigInt, TransportEquipment_ID);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.NVarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.NVarChar, SoSeal);
			db.AddInParameter(dbCommand, "@SoSealHQ", SqlDbType.NVarChar, SoSealHQ);
			db.AddInParameter(dbCommand, "@LoaiContainer", SqlDbType.NVarChar, LoaiContainer);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_TransportEquipment_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TransportEquipment_Detail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_TransportEquipment_Detail(long id)
		{
			KDT_VNACCS_TransportEquipment_Detail entity = new KDT_VNACCS_TransportEquipment_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TransportEquipment_ID(long transportEquipment_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_DeleteBy_TransportEquipment_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TransportEquipment_ID", SqlDbType.BigInt, transportEquipment_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_TransportEquipment_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TransportEquipment_Detail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}