﻿namespace Company.Interface
{
    partial class VanTaiDonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VanTaiDonForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.cmbLoaiVanDon = new Janus.Windows.EditControls.UIComboBox();
            this.cbLoaiKien = new Janus.Windows.EditControls.UIComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtTongSoKien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDiaDiemChuyenTai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHieuChuyenDi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.chkHangRoi = new Janus.Windows.EditControls.UICheckBox();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ctrNoiPhatHanh = new Company.Interface.Controls.NuocHControl();
            this.label14 = new System.Windows.Forms.Label();
            this.lblNoiPhatHanh = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ccNgayVanDon = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label19 = new System.Windows.Forms.Label();
            this.ccNgayKhoiHanh = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtTenHangVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTenHangVanTai = new System.Windows.Forms.Label();
            this.txtMaHangVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblMaHangVanTai = new System.Windows.Forms.Label();
            this.ctrQuocTichPTVT = new Company.Interface.Controls.NuocHControl();
            this.txtTenPTVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblQuocTichPTVT = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSoHieuPTVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSoHieuPTVT = new System.Windows.Forms.Label();
            this.ccNgayDen = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.grbNguoiNhanhang = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenNguoiNhanHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMaNguoiNhanHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.grbNguoiGiaoHang = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenNguoiGiaoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMaNguoiGiaoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.grbNguoiDuocThongBao = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenNguoiNhanHangTG = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMaNguoiNhanHangTG = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.grbDiaDiemGiaoHang = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaDiemGiaoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbCuaKhauXuat = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMaDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCuaKhauXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel0Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgHang = new Janus.Windows.GridEX.GridEX();
            this.ctxMmHang = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.thêmHàngĐóngGóiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xóaHàngĐãChọnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.TaoContainer1 = new Janus.Windows.UI.CommandBars.UICommand("TaoContainer");
            this.ThemContainerExcel1 = new Janus.Windows.UI.CommandBars.UICommand("ThemContainerExcel");
            this.ThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("ThemHang");
            this.Xoa1 = new Janus.Windows.UI.CommandBars.UICommand("Xoa");
            this.Ghi1 = new Janus.Windows.UI.CommandBars.UICommand("Ghi");
            this.cmdXoaVanDon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXoaVanDon");
            this.TaoContainer = new Janus.Windows.UI.CommandBars.UICommand("TaoContainer");
            this.ThemContainerExcel = new Janus.Windows.UI.CommandBars.UICommand("ThemContainerExcel");
            this.ThemHang = new Janus.Windows.UI.CommandBars.UICommand("ThemHang");
            this.Xoa = new Janus.Windows.UI.CommandBars.UICommand("Xoa");
            this.Ghi = new Janus.Windows.UI.CommandBars.UICommand("Ghi");
            this.cmdXoaVanDon = new Janus.Windows.UI.CommandBars.UICommand("cmdXoaVanDon");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.grbCuaKhauNhap = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrCuaKhauNhap = new Company.Interface.Controls.CuaKhauHControl();
            this.txtTenDiaDiemDoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMaDiaDiemDoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvNgayVanDon = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoVanDon = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNgayDen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenPTVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenNguoiNhan = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenNguoiGiao = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDiaDiemGiaoHang = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDiaDiemDoHang = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDiaDiemXepHang = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNgayKhoiHanh = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.ctxMnContainer = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.cmbLoaiPTVT = new Janus.Windows.EditControls.UIComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.rfvDiaDiemChuyenTai = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNhanhang)).BeginInit();
            this.grbNguoiNhanhang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiGiaoHang)).BeginInit();
            this.grbNguoiGiaoHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiDuocThongBao)).BeginInit();
            this.grbNguoiDuocThongBao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemGiaoHang)).BeginInit();
            this.grbDiaDiemGiaoHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbCuaKhauXuat)).BeginInit();
            this.grbCuaKhauXuat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            this.uiPanel0Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgHang)).BeginInit();
            this.ctxMmHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbCuaKhauNhap)).BeginInit();
            this.grbCuaKhauNhap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayVanDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoVanDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenPTVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNguoiNhan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNguoiGiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaDiemGiaoHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaDiemDoHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaDiemXepHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayKhoiHanh)).BeginInit();
            this.ctxMnContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaDiemChuyenTai)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.grbCuaKhauNhap);
            this.grbMain.Controls.Add(this.grbCuaKhauXuat);
            this.grbMain.Controls.Add(this.grbDiaDiemGiaoHang);
            this.grbMain.Controls.Add(this.grbNguoiDuocThongBao);
            this.grbMain.Controls.Add(this.grbNguoiGiaoHang);
            this.grbMain.Controls.Add(this.grbNguoiNhanhang);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Location = new System.Drawing.Point(3, 35);
            this.grbMain.Size = new System.Drawing.Size(783, 532);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cmbLoaiVanDon);
            this.uiGroupBox2.Controls.Add(this.cbLoaiKien);
            this.uiGroupBox2.Controls.Add(this.label23);
            this.uiGroupBox2.Controls.Add(this.txtTongSoKien);
            this.uiGroupBox2.Controls.Add(this.label22);
            this.uiGroupBox2.Controls.Add(this.txtDiaDiemChuyenTai);
            this.uiGroupBox2.Controls.Add(this.txtSoHieuChuyenDi);
            this.uiGroupBox2.Controls.Add(this.label21);
            this.uiGroupBox2.Controls.Add(this.label20);
            this.uiGroupBox2.Controls.Add(this.chkHangRoi);
            this.uiGroupBox2.Controls.Add(this.txtSoVanDon);
            this.uiGroupBox2.Controls.Add(this.ctrNoiPhatHanh);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.lblNoiPhatHanh);
            this.uiGroupBox2.Controls.Add(this.label26);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.ccNgayVanDon);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(11, 3);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(767, 133);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // cmbLoaiVanDon
            // 
            this.cmbLoaiVanDon.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cmbLoaiVanDon.ControlAppearance.ControlTextColor = System.Drawing.Color.Red;
            this.cmbLoaiVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLoaiVanDon.ForeColor = System.Drawing.Color.Red;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Vận đơn đường biển";
            uiComboBoxItem4.Value = "1";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Vận đơn đường không";
            uiComboBoxItem5.Value = "2";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Loại khác";
            uiComboBoxItem6.Value = "9";
            this.cmbLoaiVanDon.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cmbLoaiVanDon.Location = new System.Drawing.Point(161, 20);
            this.cmbLoaiVanDon.Name = "cmbLoaiVanDon";
            this.cmbLoaiVanDon.Size = new System.Drawing.Size(217, 21);
            this.cmbLoaiVanDon.TabIndex = 5;
            this.cmbLoaiVanDon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmbLoaiVanDon.SelectedIndexChanged += new System.EventHandler(this.cmbLoaiVanDon_SelectedIndexChanged);
            // 
            // cbLoaiKien
            // 
            this.cbLoaiKien.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbLoaiKien.DisplayMember = "ID";
            this.cbLoaiKien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLoaiKien.Location = new System.Drawing.Point(527, 74);
            this.cbLoaiKien.Name = "cbLoaiKien";
            this.cbLoaiKien.Size = new System.Drawing.Size(113, 21);
            this.cbLoaiKien.TabIndex = 5;
            this.cbLoaiKien.ValueMember = "ID";
            this.cbLoaiKien.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(422, 106);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(67, 13);
            this.label23.TabIndex = 26;
            this.label23.Text = "Tổng số kiện";
            // 
            // txtTongSoKien
            // 
            this.txtTongSoKien.DecimalDigits = 0;
            this.txtTongSoKien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoKien.Location = new System.Drawing.Point(527, 101);
            this.txtTongSoKien.Name = "txtTongSoKien";
            this.txtTongSoKien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoKien.Size = new System.Drawing.Size(217, 21);
            this.txtTongSoKien.TabIndex = 3;
            this.txtTongSoKien.TabStop = false;
            this.txtTongSoKien.Text = "0";
            this.txtTongSoKien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoKien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongSoKien.VisualStyleManager = this.vsmMain;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(422, 79);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(48, 13);
            this.label22.TabIndex = 25;
            this.label22.Text = "Loại kiện";
            // 
            // txtDiaDiemChuyenTai
            // 
            this.txtDiaDiemChuyenTai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemChuyenTai.Location = new System.Drawing.Point(161, 101);
            this.txtDiaDiemChuyenTai.MaxLength = 255;
            this.txtDiaDiemChuyenTai.Name = "txtDiaDiemChuyenTai";
            this.txtDiaDiemChuyenTai.Size = new System.Drawing.Size(214, 21);
            this.txtDiaDiemChuyenTai.TabIndex = 4;
            this.txtDiaDiemChuyenTai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemChuyenTai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoHieuChuyenDi
            // 
            this.txtSoHieuChuyenDi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHieuChuyenDi.Location = new System.Drawing.Point(161, 74);
            this.txtSoHieuChuyenDi.MaxLength = 255;
            this.txtSoHieuChuyenDi.Name = "txtSoHieuChuyenDi";
            this.txtSoHieuChuyenDi.Size = new System.Drawing.Size(214, 21);
            this.txtSoHieuChuyenDi.TabIndex = 2;
            this.txtSoHieuChuyenDi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHieuChuyenDi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHieuChuyenDi.VisualStyleManager = this.vsmMain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(8, 106);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(149, 13);
            this.label21.TabIndex = 20;
            this.label21.Text = "Địa điểm chuyển tải/quá cảnh";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(8, 79);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 13);
            this.label20.TabIndex = 20;
            this.label20.Text = "Số hiệu chuyến đi";
            // 
            // chkHangRoi
            // 
            this.chkHangRoi.Location = new System.Drawing.Point(673, 74);
            this.chkHangRoi.Name = "chkHangRoi";
            this.chkHangRoi.Size = new System.Drawing.Size(71, 23);
            this.chkHangRoi.TabIndex = 6;
            this.chkHangRoi.Text = "Hàng rời";
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon.Location = new System.Drawing.Point(161, 47);
            this.txtSoVanDon.MaxLength = 255;
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(214, 21);
            this.txtSoVanDon.TabIndex = 0;
            this.txtSoVanDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoVanDon.VisualStyleManager = this.vsmMain;
            // 
            // ctrNoiPhatHanh
            // 
            this.ctrNoiPhatHanh.BackColor = System.Drawing.Color.Transparent;
            this.ctrNoiPhatHanh.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrNoiPhatHanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNoiPhatHanh.Location = new System.Drawing.Point(527, 19);
            this.ctrNoiPhatHanh.Ma = "";
            this.ctrNoiPhatHanh.Name = "ctrNoiPhatHanh";
            this.ctrNoiPhatHanh.ReadOnly = false;
            this.ctrNoiPhatHanh.Size = new System.Drawing.Size(232, 22);
            this.ctrNoiPhatHanh.TabIndex = 1;
            this.ctrNoiPhatHanh.VisualStyleManager = null;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(422, 52);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Ngày vận đơn";
            // 
            // lblNoiPhatHanh
            // 
            this.lblNoiPhatHanh.AutoSize = true;
            this.lblNoiPhatHanh.BackColor = System.Drawing.Color.Transparent;
            this.lblNoiPhatHanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoiPhatHanh.Location = new System.Drawing.Point(422, 25);
            this.lblNoiPhatHanh.Name = "lblNoiPhatHanh";
            this.lblNoiPhatHanh.Size = new System.Drawing.Size(74, 13);
            this.lblNoiPhatHanh.TabIndex = 0;
            this.lblNoiPhatHanh.Text = "Nơi phát hành";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(8, 25);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Loại vận đơn";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(8, 52);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(61, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Số vận đơn";
            // 
            // ccNgayVanDon
            // 
            // 
            // 
            // 
            this.ccNgayVanDon.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayVanDon.DropDownCalendar.Name = "";
            this.ccNgayVanDon.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayVanDon.IsNullDate = true;
            this.ccNgayVanDon.Location = new System.Drawing.Point(527, 47);
            this.ccNgayVanDon.Name = "ccNgayVanDon";
            this.ccNgayVanDon.Nullable = true;
            this.ccNgayVanDon.NullButtonText = "Xóa";
            this.ccNgayVanDon.ShowNullButton = true;
            this.ccNgayVanDon.Size = new System.Drawing.Size(217, 21);
            this.ccNgayVanDon.TabIndex = 1;
            this.ccNgayVanDon.TodayButtonText = "Hôm nay";
            this.ccNgayVanDon.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayVanDon.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(6, 128);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(81, 13);
            this.label19.TabIndex = 22;
            this.label19.Text = "Ngày khởi hành";
            // 
            // ccNgayKhoiHanh
            // 
            // 
            // 
            // 
            this.ccNgayKhoiHanh.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayKhoiHanh.DropDownCalendar.Name = "";
            this.ccNgayKhoiHanh.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKhoiHanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKhoiHanh.IsNullDate = true;
            this.ccNgayKhoiHanh.Location = new System.Drawing.Point(161, 121);
            this.ccNgayKhoiHanh.Name = "ccNgayKhoiHanh";
            this.ccNgayKhoiHanh.Nullable = true;
            this.ccNgayKhoiHanh.NullButtonText = "Xóa";
            this.ccNgayKhoiHanh.ShowNullButton = true;
            this.ccNgayKhoiHanh.Size = new System.Drawing.Size(214, 21);
            this.ccNgayKhoiHanh.TabIndex = 3;
            this.ccNgayKhoiHanh.TodayButtonText = "Hôm nay";
            this.ccNgayKhoiHanh.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKhoiHanh.VisualStyleManager = this.vsmMain;
            // 
            // txtTenHangVT
            // 
            this.txtTenHangVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangVT.Location = new System.Drawing.Point(527, 49);
            this.txtTenHangVT.MaxLength = 255;
            this.txtTenHangVT.Name = "txtTenHangVT";
            this.txtTenHangVT.Size = new System.Drawing.Size(217, 21);
            this.txtTenHangVT.TabIndex = 5;
            this.txtTenHangVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHangVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHangVT.VisualStyleManager = this.vsmMain;
            // 
            // lblTenHangVanTai
            // 
            this.lblTenHangVanTai.AutoSize = true;
            this.lblTenHangVanTai.BackColor = System.Drawing.Color.Transparent;
            this.lblTenHangVanTai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenHangVanTai.Location = new System.Drawing.Point(422, 54);
            this.lblTenHangVanTai.Name = "lblTenHangVanTai";
            this.lblTenHangVanTai.Size = new System.Drawing.Size(88, 13);
            this.lblTenHangVanTai.TabIndex = 13;
            this.lblTenHangVanTai.Text = "Tên hãng vận tải";
            // 
            // txtMaHangVT
            // 
            this.txtMaHangVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangVT.Location = new System.Drawing.Point(161, 49);
            this.txtMaHangVT.MaxLength = 255;
            this.txtMaHangVT.Name = "txtMaHangVT";
            this.txtMaHangVT.Size = new System.Drawing.Size(214, 21);
            this.txtMaHangVT.TabIndex = 4;
            this.txtMaHangVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHangVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHangVT.VisualStyleManager = this.vsmMain;
            // 
            // lblMaHangVanTai
            // 
            this.lblMaHangVanTai.AutoSize = true;
            this.lblMaHangVanTai.BackColor = System.Drawing.Color.Transparent;
            this.lblMaHangVanTai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaHangVanTai.Location = new System.Drawing.Point(8, 54);
            this.lblMaHangVanTai.Name = "lblMaHangVanTai";
            this.lblMaHangVanTai.Size = new System.Drawing.Size(84, 13);
            this.lblMaHangVanTai.TabIndex = 11;
            this.lblMaHangVanTai.Text = "Mã hãng vận tải";
            // 
            // ctrQuocTichPTVT
            // 
            this.ctrQuocTichPTVT.BackColor = System.Drawing.Color.Transparent;
            this.ctrQuocTichPTVT.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrQuocTichPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrQuocTichPTVT.Location = new System.Drawing.Point(527, 76);
            this.ctrQuocTichPTVT.Ma = "";
            this.ctrQuocTichPTVT.Name = "ctrQuocTichPTVT";
            this.ctrQuocTichPTVT.ReadOnly = false;
            this.ctrQuocTichPTVT.Size = new System.Drawing.Size(232, 22);
            this.ctrQuocTichPTVT.TabIndex = 1;
            this.ctrQuocTichPTVT.VisualStyleManager = null;
            // 
            // txtTenPTVT
            // 
            this.txtTenPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPTVT.Location = new System.Drawing.Point(161, 22);
            this.txtTenPTVT.MaxLength = 255;
            this.txtTenPTVT.Name = "txtTenPTVT";
            this.txtTenPTVT.Size = new System.Drawing.Size(214, 21);
            this.txtTenPTVT.TabIndex = 2;
            this.txtTenPTVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenPTVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenPTVT.VisualStyleManager = this.vsmMain;
            // 
            // lblQuocTichPTVT
            // 
            this.lblQuocTichPTVT.AutoSize = true;
            this.lblQuocTichPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuocTichPTVT.Location = new System.Drawing.Point(422, 81);
            this.lblQuocTichPTVT.Name = "lblQuocTichPTVT";
            this.lblQuocTichPTVT.Size = new System.Drawing.Size(79, 13);
            this.lblQuocTichPTVT.TabIndex = 15;
            this.lblQuocTichPTVT.Text = "Quốc tịch PTVT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Tên PTVT";
            // 
            // txtSoHieuPTVT
            // 
            this.txtSoHieuPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHieuPTVT.Location = new System.Drawing.Point(161, 76);
            this.txtSoHieuPTVT.MaxLength = 255;
            this.txtSoHieuPTVT.Name = "txtSoHieuPTVT";
            this.txtSoHieuPTVT.Size = new System.Drawing.Size(214, 21);
            this.txtSoHieuPTVT.TabIndex = 0;
            this.txtSoHieuPTVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHieuPTVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHieuPTVT.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ngày đến";
            // 
            // lblSoHieuPTVT
            // 
            this.lblSoHieuPTVT.AutoSize = true;
            this.lblSoHieuPTVT.BackColor = System.Drawing.Color.Transparent;
            this.lblSoHieuPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHieuPTVT.Location = new System.Drawing.Point(8, 81);
            this.lblSoHieuPTVT.Name = "lblSoHieuPTVT";
            this.lblSoHieuPTVT.Size = new System.Drawing.Size(69, 13);
            this.lblSoHieuPTVT.TabIndex = 4;
            this.lblSoHieuPTVT.Text = "Số hiệu PTVT";
            // 
            // ccNgayDen
            // 
            // 
            // 
            // 
            this.ccNgayDen.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayDen.DropDownCalendar.Name = "";
            this.ccNgayDen.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDen.IsNullDate = true;
            this.ccNgayDen.Location = new System.Drawing.Point(135, 121);
            this.ccNgayDen.Name = "ccNgayDen";
            this.ccNgayDen.Nullable = true;
            this.ccNgayDen.NullButtonText = "Xóa";
            this.ccNgayDen.ShowNullButton = true;
            this.ccNgayDen.Size = new System.Drawing.Size(217, 21);
            this.ccNgayDen.TabIndex = 3;
            this.ccNgayDen.TodayButtonText = "Hôm nay";
            this.ccNgayDen.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDen.VisualStyleManager = this.vsmMain;
            // 
            // grbNguoiNhanhang
            // 
            this.grbNguoiNhanhang.BackColor = System.Drawing.Color.Transparent;
            this.grbNguoiNhanhang.Controls.Add(this.txtTenNguoiNhanHang);
            this.grbNguoiNhanhang.Controls.Add(this.label15);
            this.grbNguoiNhanhang.Controls.Add(this.txtMaNguoiNhanHang);
            this.grbNguoiNhanhang.Controls.Add(this.label17);
            this.grbNguoiNhanhang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiNhanhang.Location = new System.Drawing.Point(11, 421);
            this.grbNguoiNhanhang.Name = "grbNguoiNhanhang";
            this.grbNguoiNhanhang.Size = new System.Drawing.Size(190, 103);
            this.grbNguoiNhanhang.TabIndex = 4;
            this.grbNguoiNhanhang.Text = "Người nhận hàng";
            this.grbNguoiNhanhang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbNguoiNhanhang.VisualStyleManager = this.vsmMain;
            // 
            // txtTenNguoiNhanHang
            // 
            this.txtTenNguoiNhanHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiNhanHang.Location = new System.Drawing.Point(33, 44);
            this.txtTenNguoiNhanHang.MaxLength = 255;
            this.txtTenNguoiNhanHang.Multiline = true;
            this.txtTenNguoiNhanHang.Name = "txtTenNguoiNhanHang";
            this.txtTenNguoiNhanHang.Size = new System.Drawing.Size(146, 49);
            this.txtTenNguoiNhanHang.TabIndex = 1;
            this.txtTenNguoiNhanHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNguoiNhanHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenNguoiNhanHang.VisualStyleManager = this.vsmMain;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(4, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Tên";
            // 
            // txtMaNguoiNhanHang
            // 
            this.txtMaNguoiNhanHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiNhanHang.Location = new System.Drawing.Point(33, 17);
            this.txtMaNguoiNhanHang.MaxLength = 255;
            this.txtMaNguoiNhanHang.Name = "txtMaNguoiNhanHang";
            this.txtMaNguoiNhanHang.Size = new System.Drawing.Size(146, 21);
            this.txtMaNguoiNhanHang.TabIndex = 0;
            this.txtMaNguoiNhanHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiNhanHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaNguoiNhanHang.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(4, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(21, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Mã";
            // 
            // grbNguoiGiaoHang
            // 
            this.grbNguoiGiaoHang.BackColor = System.Drawing.Color.Transparent;
            this.grbNguoiGiaoHang.Controls.Add(this.txtTenNguoiGiaoHang);
            this.grbNguoiGiaoHang.Controls.Add(this.label8);
            this.grbNguoiGiaoHang.Controls.Add(this.txtMaNguoiGiaoHang);
            this.grbNguoiGiaoHang.Controls.Add(this.label9);
            this.grbNguoiGiaoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiGiaoHang.Location = new System.Drawing.Point(208, 421);
            this.grbNguoiGiaoHang.Name = "grbNguoiGiaoHang";
            this.grbNguoiGiaoHang.Size = new System.Drawing.Size(185, 103);
            this.grbNguoiGiaoHang.TabIndex = 5;
            this.grbNguoiGiaoHang.Text = "Người giao hàng";
            this.grbNguoiGiaoHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbNguoiGiaoHang.VisualStyleManager = this.vsmMain;
            // 
            // txtTenNguoiGiaoHang
            // 
            this.txtTenNguoiGiaoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiGiaoHang.Location = new System.Drawing.Point(33, 44);
            this.txtTenNguoiGiaoHang.MaxLength = 255;
            this.txtTenNguoiGiaoHang.Multiline = true;
            this.txtTenNguoiGiaoHang.Name = "txtTenNguoiGiaoHang";
            this.txtTenNguoiGiaoHang.Size = new System.Drawing.Size(145, 49);
            this.txtTenNguoiGiaoHang.TabIndex = 1;
            this.txtTenNguoiGiaoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNguoiGiaoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenNguoiGiaoHang.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Tên";
            // 
            // txtMaNguoiGiaoHang
            // 
            this.txtMaNguoiGiaoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiGiaoHang.Location = new System.Drawing.Point(33, 17);
            this.txtMaNguoiGiaoHang.MaxLength = 255;
            this.txtMaNguoiGiaoHang.Name = "txtMaNguoiGiaoHang";
            this.txtMaNguoiGiaoHang.Size = new System.Drawing.Size(145, 21);
            this.txtMaNguoiGiaoHang.TabIndex = 0;
            this.txtMaNguoiGiaoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiGiaoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaNguoiGiaoHang.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Mã";
            // 
            // grbNguoiDuocThongBao
            // 
            this.grbNguoiDuocThongBao.BackColor = System.Drawing.Color.Transparent;
            this.grbNguoiDuocThongBao.Controls.Add(this.txtTenNguoiNhanHangTG);
            this.grbNguoiDuocThongBao.Controls.Add(this.label10);
            this.grbNguoiDuocThongBao.Controls.Add(this.txtMaNguoiNhanHangTG);
            this.grbNguoiDuocThongBao.Controls.Add(this.label11);
            this.grbNguoiDuocThongBao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiDuocThongBao.Location = new System.Drawing.Point(403, 422);
            this.grbNguoiDuocThongBao.Name = "grbNguoiDuocThongBao";
            this.grbNguoiDuocThongBao.Size = new System.Drawing.Size(190, 103);
            this.grbNguoiDuocThongBao.TabIndex = 6;
            this.grbNguoiDuocThongBao.Text = "Người được thông báo";
            this.grbNguoiDuocThongBao.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbNguoiDuocThongBao.VisualStyleManager = this.vsmMain;
            // 
            // txtTenNguoiNhanHangTG
            // 
            this.txtTenNguoiNhanHangTG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiNhanHangTG.Location = new System.Drawing.Point(33, 43);
            this.txtTenNguoiNhanHangTG.MaxLength = 255;
            this.txtTenNguoiNhanHangTG.Multiline = true;
            this.txtTenNguoiNhanHangTG.Name = "txtTenNguoiNhanHangTG";
            this.txtTenNguoiNhanHangTG.Size = new System.Drawing.Size(146, 48);
            this.txtTenNguoiNhanHangTG.TabIndex = 1;
            this.txtTenNguoiNhanHangTG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNguoiNhanHangTG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenNguoiNhanHangTG.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(2, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Tên";
            // 
            // txtMaNguoiNhanHangTG
            // 
            this.txtMaNguoiNhanHangTG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiNhanHangTG.Location = new System.Drawing.Point(33, 16);
            this.txtMaNguoiNhanHangTG.MaxLength = 255;
            this.txtMaNguoiNhanHangTG.Name = "txtMaNguoiNhanHangTG";
            this.txtMaNguoiNhanHangTG.Size = new System.Drawing.Size(146, 21);
            this.txtMaNguoiNhanHangTG.TabIndex = 0;
            this.txtMaNguoiNhanHangTG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiNhanHangTG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaNguoiNhanHangTG.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(4, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Mã";
            // 
            // grbDiaDiemGiaoHang
            // 
            this.grbDiaDiemGiaoHang.BackColor = System.Drawing.Color.Transparent;
            this.grbDiaDiemGiaoHang.Controls.Add(this.txtDiaDiemGiaoHang);
            this.grbDiaDiemGiaoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDiaDiemGiaoHang.Location = new System.Drawing.Point(599, 422);
            this.grbDiaDiemGiaoHang.Name = "grbDiaDiemGiaoHang";
            this.grbDiaDiemGiaoHang.Size = new System.Drawing.Size(180, 102);
            this.grbDiaDiemGiaoHang.TabIndex = 7;
            this.grbDiaDiemGiaoHang.Text = "Địa điểm giao hàng";
            this.grbDiaDiemGiaoHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbDiaDiemGiaoHang.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaDiemGiaoHang
            // 
            this.txtDiaDiemGiaoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemGiaoHang.Location = new System.Drawing.Point(9, 18);
            this.txtDiaDiemGiaoHang.MaxLength = 255;
            this.txtDiaDiemGiaoHang.Multiline = true;
            this.txtDiaDiemGiaoHang.Name = "txtDiaDiemGiaoHang";
            this.txtDiaDiemGiaoHang.Size = new System.Drawing.Size(147, 73);
            this.txtDiaDiemGiaoHang.TabIndex = 0;
            this.txtDiaDiemGiaoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemGiaoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaDiemGiaoHang.VisualStyleManager = this.vsmMain;
            // 
            // grbCuaKhauXuat
            // 
            this.grbCuaKhauXuat.BackColor = System.Drawing.Color.Transparent;
            this.grbCuaKhauXuat.Controls.Add(this.txtTenDiaDiemXepHang);
            this.grbCuaKhauXuat.Controls.Add(this.label12);
            this.grbCuaKhauXuat.Controls.Add(this.txtMaDiaDiemXepHang);
            this.grbCuaKhauXuat.Controls.Add(this.label5);
            this.grbCuaKhauXuat.Controls.Add(this.label13);
            this.grbCuaKhauXuat.Controls.Add(this.ccNgayKhoiHanh);
            this.grbCuaKhauXuat.Controls.Add(this.label19);
            this.grbCuaKhauXuat.Controls.Add(this.txtCuaKhauXuat);
            this.grbCuaKhauXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbCuaKhauXuat.Location = new System.Drawing.Point(11, 262);
            this.grbCuaKhauXuat.Name = "grbCuaKhauXuat";
            this.grbCuaKhauXuat.Size = new System.Drawing.Size(382, 153);
            this.grbCuaKhauXuat.TabIndex = 2;
            this.grbCuaKhauXuat.Text = "Cửa khẩu xuất và Cảng xếp hàng";
            this.grbCuaKhauXuat.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbCuaKhauXuat.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDiaDiemXepHang
            // 
            this.txtTenDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDiaDiemXepHang.Location = new System.Drawing.Point(161, 73);
            this.txtTenDiaDiemXepHang.MaxLength = 255;
            this.txtTenDiaDiemXepHang.Multiline = true;
            this.txtTenDiaDiemXepHang.Name = "txtTenDiaDiemXepHang";
            this.txtTenDiaDiemXepHang.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTenDiaDiemXepHang.Size = new System.Drawing.Size(214, 42);
            this.txtTenDiaDiemXepHang.TabIndex = 2;
            this.txtTenDiaDiemXepHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDiaDiemXepHang.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(99, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Tên cảng xếp hàng";
            // 
            // txtMaDiaDiemXepHang
            // 
            this.txtMaDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDiaDiemXepHang.Location = new System.Drawing.Point(161, 47);
            this.txtMaDiaDiemXepHang.MaxLength = 255;
            this.txtMaDiaDiemXepHang.Name = "txtMaDiaDiemXepHang";
            this.txtMaDiaDiemXepHang.Size = new System.Drawing.Size(214, 21);
            this.txtMaDiaDiemXepHang.TabIndex = 1;
            this.txtMaDiaDiemXepHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDiaDiemXepHang.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Cửa khẩu xuất ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Mã cảng xếp hàng";
            // 
            // txtCuaKhauXuat
            // 
            this.txtCuaKhauXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCuaKhauXuat.Location = new System.Drawing.Point(161, 20);
            this.txtCuaKhauXuat.MaxLength = 255;
            this.txtCuaKhauXuat.Name = "txtCuaKhauXuat";
            this.txtCuaKhauXuat.Size = new System.Drawing.Size(214, 21);
            this.txtCuaKhauXuat.TabIndex = 0;
            this.txtCuaKhauXuat.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCuaKhauXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(241)))), ((int)(((byte)(228)))));
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.uiPanelManager1.DefaultPanelSettings.CloseButtonVisible = false;
            this.uiPanelManager1.Tag = null;
            this.uiPanel0.Id = new System.Guid("2b7a958a-cf32-4a55-82fb-3ac003b3f28a");
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            this.uiPanel1.Id = new System.Guid("02142aa6-a8ba-4571-8552-e222dba2e66d");
            this.uiPanelManager1.Panels.Add(this.uiPanel1);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("2b7a958a-cf32-4a55-82fb-3ac003b3f28a"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(784, 230), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("02142aa6-a8ba-4571-8552-e222dba2e66d"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(772, 200), true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("2b7a958a-cf32-4a55-82fb-3ac003b3f28a"), new System.Drawing.Point(106, 220), new System.Drawing.Size(200, 200), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("02142aa6-a8ba-4571-8552-e222dba2e66d"), new System.Drawing.Point(44, 44), new System.Drawing.Size(200, 200), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.AutoHide = true;
            this.uiPanel0.FloatingLocation = new System.Drawing.Point(106, 220);
            this.uiPanel0.InnerContainer = this.uiPanel0Container;
            this.uiPanel0.Location = new System.Drawing.Point(3, 345);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.Size = new System.Drawing.Size(784, 230);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "Thông tin Container";
            // 
            // uiPanel0Container
            // 
            this.uiPanel0Container.Controls.Add(this.dgList);
            this.uiPanel0Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel0Container.Name = "uiPanel0Container";
            this.uiPanel0Container.Size = new System.Drawing.Size(782, 202);
            this.uiPanel0Container.TabIndex = 0;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(782, 202);
            this.dgList.TabIndex = 3;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // uiPanel1
            // 
            this.uiPanel1.AutoHide = true;
            this.uiPanel1.FloatingLocation = new System.Drawing.Point(44, 44);
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(3, 293);
            this.uiPanel1.Name = "uiPanel1";
            this.helpProvider1.SetShowHelp(this.uiPanel1, false);
            this.uiPanel1.Size = new System.Drawing.Size(772, 200);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Chi tiết hàng";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.dgHang);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(770, 172);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // dgHang
            // 
            this.dgHang.AlternatingColors = true;
            this.dgHang.AutoEdit = true;
            this.dgHang.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgHang.ContextMenuStrip = this.ctxMmHang;
            dgHang_DesignTimeLayout.LayoutString = resources.GetString("dgHang_DesignTimeLayout.LayoutString");
            this.dgHang.DesignTimeLayout = dgHang_DesignTimeLayout;
            this.dgHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgHang.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgHang.FrozenColumns = 3;
            this.dgHang.GroupByBoxVisible = false;
            this.dgHang.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHang.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgHang.Location = new System.Drawing.Point(0, 0);
            this.dgHang.Name = "dgHang";
            this.dgHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgHang.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgHang.Size = new System.Drawing.Size(770, 172);
            this.dgHang.TabIndex = 30;
            this.dgHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgHang.VisualStyleManager = this.vsmMain;
            this.dgHang.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListHang_LoadingRow);
            // 
            // ctxMmHang
            // 
            this.ctxMmHang.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thêmHàngĐóngGóiToolStripMenuItem,
            this.xóaHàngĐãChọnToolStripMenuItem});
            this.ctxMmHang.Name = "contextMenuStrip1";
            this.ctxMmHang.Size = new System.Drawing.Size(174, 48);
            // 
            // thêmHàngĐóngGóiToolStripMenuItem
            // 
            this.thêmHàngĐóngGóiToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("thêmHàngĐóngGóiToolStripMenuItem.Image")));
            this.thêmHàngĐóngGóiToolStripMenuItem.Name = "thêmHàngĐóngGóiToolStripMenuItem";
            this.thêmHàngĐóngGóiToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.thêmHàngĐóngGóiToolStripMenuItem.Text = "Thêm hàng đóng gói";
            this.thêmHàngĐóngGóiToolStripMenuItem.Click += new System.EventHandler(this.thêmHàngĐóngGóiToolStripMenuItem_Click);
            // 
            // xóaHàngĐãChọnToolStripMenuItem
            // 
            this.xóaHàngĐãChọnToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("xóaHàngĐãChọnToolStripMenuItem.Image")));
            this.xóaHàngĐãChọnToolStripMenuItem.Name = "xóaHàngĐãChọnToolStripMenuItem";
            this.xóaHàngĐãChọnToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.xóaHàngĐãChọnToolStripMenuItem.Text = "Xóa hàng đã chọn";
            this.xóaHàngĐãChọnToolStripMenuItem.Click += new System.EventHandler(this.xóaHàngĐãChọnToolStripMenuItem_Click);
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.TaoContainer,
            this.ThemContainerExcel,
            this.ThemHang,
            this.Xoa,
            this.Ghi,
            this.cmdXoaVanDon});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.TaoContainer1,
            this.ThemContainerExcel1,
            this.ThemHang1,
            this.Xoa1,
            this.Ghi1,
            this.cmdXoaVanDon1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(737, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // TaoContainer1
            // 
            this.TaoContainer1.Key = "TaoContainer";
            this.TaoContainer1.Name = "TaoContainer1";
            // 
            // ThemContainerExcel1
            // 
            this.ThemContainerExcel1.Key = "ThemContainerExcel";
            this.ThemContainerExcel1.Name = "ThemContainerExcel1";
            // 
            // ThemHang1
            // 
            this.ThemHang1.Key = "ThemHang";
            this.ThemHang1.Name = "ThemHang1";
            // 
            // Xoa1
            // 
            this.Xoa1.Key = "Xoa";
            this.Xoa1.Name = "Xoa1";
            // 
            // Ghi1
            // 
            this.Ghi1.Key = "Ghi";
            this.Ghi1.Name = "Ghi1";
            // 
            // cmdXoaVanDon1
            // 
            this.cmdXoaVanDon1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdXoaVanDon1.Icon")));
            this.cmdXoaVanDon1.Key = "cmdXoaVanDon";
            this.cmdXoaVanDon1.Name = "cmdXoaVanDon1";
            // 
            // TaoContainer
            // 
            this.TaoContainer.Icon = ((System.Drawing.Icon)(resources.GetObject("TaoContainer.Icon")));
            this.TaoContainer.Key = "TaoContainer";
            this.TaoContainer.Name = "TaoContainer";
            this.TaoContainer.Text = "Thêm Container";
            // 
            // ThemContainerExcel
            // 
            this.ThemContainerExcel.Icon = ((System.Drawing.Icon)(resources.GetObject("ThemContainerExcel.Icon")));
            this.ThemContainerExcel.Key = "ThemContainerExcel";
            this.ThemContainerExcel.Name = "ThemContainerExcel";
            this.ThemContainerExcel.Text = "Thêm Container Excel";
            // 
            // ThemHang
            // 
            this.ThemHang.Image = ((System.Drawing.Image)(resources.GetObject("ThemHang.Image")));
            this.ThemHang.Key = "ThemHang";
            this.ThemHang.Name = "ThemHang";
            this.ThemHang.Text = "Thêm hàng đóng gói";
            // 
            // Xoa
            // 
            this.Xoa.Icon = ((System.Drawing.Icon)(resources.GetObject("Xoa.Icon")));
            this.Xoa.Key = "Xoa";
            this.Xoa.Name = "Xoa";
            this.Xoa.Text = "Xóa Container";
            // 
            // Ghi
            // 
            this.Ghi.Icon = ((System.Drawing.Icon)(resources.GetObject("Ghi.Icon")));
            this.Ghi.Key = "Ghi";
            this.Ghi.Name = "Ghi";
            this.Ghi.Text = "Ghi vận đơn";
            // 
            // cmdXoaVanDon
            // 
            this.cmdXoaVanDon.Key = "cmdXoaVanDon";
            this.cmdXoaVanDon.Name = "cmdXoaVanDon";
            this.cmdXoaVanDon.Text = "Xóa vận đơn";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(789, 32);
            // 
            // grbCuaKhauNhap
            // 
            this.grbCuaKhauNhap.BackColor = System.Drawing.Color.Transparent;
            this.grbCuaKhauNhap.Controls.Add(this.ctrCuaKhauNhap);
            this.grbCuaKhauNhap.Controls.Add(this.txtTenDiaDiemDoHang);
            this.grbCuaKhauNhap.Controls.Add(this.label16);
            this.grbCuaKhauNhap.Controls.Add(this.txtMaDiaDiemDoHang);
            this.grbCuaKhauNhap.Controls.Add(this.label25);
            this.grbCuaKhauNhap.Controls.Add(this.label18);
            this.grbCuaKhauNhap.Controls.Add(this.ccNgayDen);
            this.grbCuaKhauNhap.Controls.Add(this.label1);
            this.grbCuaKhauNhap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbCuaKhauNhap.Location = new System.Drawing.Point(403, 262);
            this.grbCuaKhauNhap.Name = "grbCuaKhauNhap";
            this.grbCuaKhauNhap.Size = new System.Drawing.Size(375, 153);
            this.grbCuaKhauNhap.TabIndex = 3;
            this.grbCuaKhauNhap.Text = "Cửa khẩu nhập và Cảng dỡ hàng";
            this.grbCuaKhauNhap.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbCuaKhauNhap.VisualStyleManager = this.vsmMain;
            // 
            // ctrCuaKhauNhap
            // 
            this.ctrCuaKhauNhap.BackColor = System.Drawing.Color.Transparent;
            this.ctrCuaKhauNhap.ErrorMessage = "\"Cửa khẩu\" không được bỏ trống.";
            this.ctrCuaKhauNhap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrCuaKhauNhap.IsLoadData = true;
            this.ctrCuaKhauNhap.Location = new System.Drawing.Point(135, 20);
            this.ctrCuaKhauNhap.Ma = "";
            this.ctrCuaKhauNhap.Name = "ctrCuaKhauNhap";
            this.ctrCuaKhauNhap.ReadOnly = false;
            this.ctrCuaKhauNhap.Size = new System.Drawing.Size(217, 25);
            this.ctrCuaKhauNhap.TabIndex = 0;
            this.ctrCuaKhauNhap.Ten = "";
            this.ctrCuaKhauNhap.VisualStyleManager = null;
            // 
            // txtTenDiaDiemDoHang
            // 
            this.txtTenDiaDiemDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDiaDiemDoHang.Location = new System.Drawing.Point(135, 74);
            this.txtTenDiaDiemDoHang.MaxLength = 255;
            this.txtTenDiaDiemDoHang.Multiline = true;
            this.txtTenDiaDiemDoHang.Name = "txtTenDiaDiemDoHang";
            this.txtTenDiaDiemDoHang.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTenDiaDiemDoHang.Size = new System.Drawing.Size(217, 42);
            this.txtTenDiaDiemDoHang.TabIndex = 2;
            this.txtTenDiaDiemDoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDiaDiemDoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDiaDiemDoHang.VisualStyleManager = this.vsmMain;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(30, 83);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Tên cảng dở hàng";
            // 
            // txtMaDiaDiemDoHang
            // 
            this.txtMaDiaDiemDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDiaDiemDoHang.Location = new System.Drawing.Point(135, 47);
            this.txtMaDiaDiemDoHang.MaxLength = 255;
            this.txtMaDiaDiemDoHang.Name = "txtMaDiaDiemDoHang";
            this.txtMaDiaDiemDoHang.Size = new System.Drawing.Size(217, 21);
            this.txtMaDiaDiemDoHang.TabIndex = 1;
            this.txtMaDiaDiemDoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDiaDiemDoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDiaDiemDoHang.VisualStyleManager = this.vsmMain;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(30, 25);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Cửa khẩu nhập";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(30, 52);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Mã cảng dở hàng";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvNgayVanDon
            // 
            this.rfvNgayVanDon.ControlToValidate = this.ccNgayVanDon;
            this.rfvNgayVanDon.ErrorMessage = "\"Ngày giấy phép\" không được bỏ trống.";
            this.rfvNgayVanDon.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayVanDon.Icon")));
            this.rfvNgayVanDon.Tag = "rfvNgayGiayPhep";
            // 
            // rfvSoVanDon
            // 
            this.rfvSoVanDon.ControlToValidate = this.txtSoVanDon;
            this.rfvSoVanDon.ErrorMessage = "\"Số vận đơn\" không được để trống.";
            this.rfvSoVanDon.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoVanDon.Icon")));
            this.rfvSoVanDon.Tag = "rfvSoVanDon";
            // 
            // rfvNgayDen
            // 
            this.rfvNgayDen.ControlToValidate = this.ccNgayDen;
            this.rfvNgayDen.ErrorMessage = "\"Ngày đến PTVT\" không được để trống.";
            this.rfvNgayDen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayDen.Icon")));
            this.rfvNgayDen.Tag = "rfvNgayDen";
            // 
            // rfvTenPTVT
            // 
            this.rfvTenPTVT.ControlToValidate = this.txtTenPTVT;
            this.rfvTenPTVT.ErrorMessage = "\"Tên PTVT\" không được để trống.";
            this.rfvTenPTVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenPTVT.Icon")));
            this.rfvTenPTVT.Tag = "rfvTenPTVT";
            // 
            // rfvTenNguoiNhan
            // 
            this.rfvTenNguoiNhan.ControlToValidate = this.txtTenNguoiNhanHang;
            this.rfvTenNguoiNhan.ErrorMessage = "\"Tên người nhận hàng\" không được để trống.";
            this.rfvTenNguoiNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenNguoiNhan.Icon")));
            this.rfvTenNguoiNhan.Tag = "rfvTenNguoiNhan";
            // 
            // rfvTenNguoiGiao
            // 
            this.rfvTenNguoiGiao.ControlToValidate = this.txtTenNguoiGiaoHang;
            this.rfvTenNguoiGiao.ErrorMessage = "\"Tên người giao hàng\" không được để trống.";
            this.rfvTenNguoiGiao.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenNguoiGiao.Icon")));
            this.rfvTenNguoiGiao.Tag = "rfvTenNguoiGiao";
            // 
            // rfvDiaDiemGiaoHang
            // 
            this.rfvDiaDiemGiaoHang.ControlToValidate = this.txtDiaDiemGiaoHang;
            this.rfvDiaDiemGiaoHang.ErrorMessage = "\"Địa điểm giao hàng\" không được để trống.";
            this.rfvDiaDiemGiaoHang.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaDiemGiaoHang.Icon")));
            this.rfvDiaDiemGiaoHang.Tag = "rfvDiaDiemGiaoHang";
            // 
            // rfvTenDiaDiemDoHang
            // 
            this.rfvTenDiaDiemDoHang.ControlToValidate = this.txtTenDiaDiemDoHang;
            this.rfvTenDiaDiemDoHang.ErrorMessage = "\"Tên địa điểm dỡ hàng\" không được để trống.";
            this.rfvTenDiaDiemDoHang.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDiaDiemDoHang.Icon")));
            this.rfvTenDiaDiemDoHang.Tag = "rfvTenDiaDiemDoHang";
            // 
            // rfvTenDiaDiemXepHang
            // 
            this.rfvTenDiaDiemXepHang.ControlToValidate = this.txtTenDiaDiemXepHang;
            this.rfvTenDiaDiemXepHang.ErrorMessage = "\"Tên địa điểm xếp hàng\" không được để trống.";
            this.rfvTenDiaDiemXepHang.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDiaDiemXepHang.Icon")));
            this.rfvTenDiaDiemXepHang.Tag = "rfvTenDiaDiemXepHang";
            // 
            // rfvNgayKhoiHanh
            // 
            this.rfvNgayKhoiHanh.ControlToValidate = this.ccNgayKhoiHanh;
            this.rfvNgayKhoiHanh.ErrorMessage = "\"Ngày khởi hành\" không được để trống.";
            this.rfvNgayKhoiHanh.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayKhoiHanh.Icon")));
            this.rfvNgayKhoiHanh.Tag = "rfvNgayKhoiHanh";
            // 
            // ctxMnContainer
            // 
            this.ctxMnContainer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.ctxMnContainer.Name = "contextMenuStrip1";
            this.ctxMnContainer.Size = new System.Drawing.Size(150, 48);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuItem1.Text = "Thêm Container";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.Image")));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuItem2.Text = "Xóa Container";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.txtTenHangVT);
            this.uiGroupBox5.Controls.Add(this.cmbLoaiPTVT);
            this.uiGroupBox5.Controls.Add(this.lblTenHangVanTai);
            this.uiGroupBox5.Controls.Add(this.lblSoHieuPTVT);
            this.uiGroupBox5.Controls.Add(this.txtMaHangVT);
            this.uiGroupBox5.Controls.Add(this.lblMaHangVanTai);
            this.uiGroupBox5.Controls.Add(this.label4);
            this.uiGroupBox5.Controls.Add(this.txtSoHieuPTVT);
            this.uiGroupBox5.Controls.Add(this.label24);
            this.uiGroupBox5.Controls.Add(this.txtTenPTVT);
            this.uiGroupBox5.Controls.Add(this.lblQuocTichPTVT);
            this.uiGroupBox5.Controls.Add(this.ctrQuocTichPTVT);
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(11, 142);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(768, 111);
            this.uiGroupBox5.TabIndex = 1;
            this.uiGroupBox5.Text = "Phương tiện vận tải";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // cmbLoaiPTVT
            // 
            this.cmbLoaiPTVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cmbLoaiPTVT.DisplayMember = "ID";
            this.cmbLoaiPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLoaiPTVT.Location = new System.Drawing.Point(527, 22);
            this.cmbLoaiPTVT.Name = "cmbLoaiPTVT";
            this.cmbLoaiPTVT.Size = new System.Drawing.Size(217, 21);
            this.cmbLoaiPTVT.TabIndex = 3;
            this.cmbLoaiPTVT.ValueMember = "ID";
            this.cmbLoaiPTVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmbLoaiPTVT.VisualStyleManager = this.vsmMain;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(422, 27);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(63, 13);
            this.label24.TabIndex = 15;
            this.label24.Text = "Kiểu vận tải";
            // 
            // rfvDiaDiemChuyenTai
            // 
            this.rfvDiaDiemChuyenTai.ControlToValidate = this.txtDiaDiemChuyenTai;
            this.rfvDiaDiemChuyenTai.ErrorMessage = "\"Địa điểm chuyển tải/quá cảnh\" không được để trống.";
            this.rfvDiaDiemChuyenTai.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaDiemChuyenTai.Icon")));
            this.rfvDiaDiemChuyenTai.Tag = "rfvDiaDiemChuyenTai";
            // 
            // VanTaiDonForm
            // 
            this.ClientSize = new System.Drawing.Size(789, 588);
            this.Controls.Add(this.TopRebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(797, 622);
            this.Name = "VanTaiDonForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin vận tải đơn";
            this.Load += new System.EventHandler(this.VanTaiDonForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNhanhang)).EndInit();
            this.grbNguoiNhanhang.ResumeLayout(false);
            this.grbNguoiNhanhang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiGiaoHang)).EndInit();
            this.grbNguoiGiaoHang.ResumeLayout(false);
            this.grbNguoiGiaoHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiDuocThongBao)).EndInit();
            this.grbNguoiDuocThongBao.ResumeLayout(false);
            this.grbNguoiDuocThongBao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemGiaoHang)).EndInit();
            this.grbDiaDiemGiaoHang.ResumeLayout(false);
            this.grbDiaDiemGiaoHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbCuaKhauXuat)).EndInit();
            this.grbCuaKhauXuat.ResumeLayout(false);
            this.grbCuaKhauXuat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            this.uiPanel0Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgHang)).EndInit();
            this.ctxMmHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbCuaKhauNhap)).EndInit();
            this.grbCuaKhauNhap.ResumeLayout(false);
            this.grbCuaKhauNhap.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayVanDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoVanDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenPTVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNguoiNhan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNguoiGiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaDiemGiaoHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaDiemDoHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaDiemXepHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayKhoiHanh)).EndInit();
            this.ctxMnContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaDiemChuyenTai)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayVanDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenPTVT;
        private System.Windows.Forms.Label lblQuocTichPTVT;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHieuPTVT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSoHieuPTVT;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayDen;
        private Janus.Windows.EditControls.UIGroupBox grbNguoiNhanhang;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiNhanHang;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiNhanHang;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangVT;
        private System.Windows.Forms.Label lblTenHangVanTai;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangVT;
        private System.Windows.Forms.Label lblMaHangVanTai;
        private Company.Interface.Controls.NuocHControl ctrQuocTichPTVT;
        private Janus.Windows.EditControls.UIGroupBox grbNguoiGiaoHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiGiaoHang;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiGiaoHang;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIGroupBox grbNguoiDuocThongBao;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiNhanHangTG;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiNhanHangTG;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox grbDiaDiemGiaoHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemGiaoHang;
        private Janus.Windows.EditControls.UIGroupBox grbCuaKhauXuat;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiemXepHang;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDiaDiemXepHang;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.UI.Dock.UIPanelManager uiPanelManager1;
        private Janus.Windows.UI.Dock.UIPanel uiPanel0;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel0Container;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand TaoContainer;
        private Janus.Windows.UI.CommandBars.UICommand Xoa;
        private Janus.Windows.UI.CommandBars.UICommand ThemContainerExcel;
        private Janus.Windows.UI.CommandBars.UICommand TaoContainer1;
        private Janus.Windows.UI.CommandBars.UICommand Xoa1;
        private Janus.Windows.UI.CommandBars.UICommand ThemContainerExcel1;
        private Janus.Windows.EditControls.UICheckBox chkHangRoi;
        private Janus.Windows.EditControls.UIGroupBox grbCuaKhauNhap;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiemDoHang;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDiaDiemDoHang;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.UI.CommandBars.UICommand Ghi;
        private Janus.Windows.UI.CommandBars.UICommand Ghi1;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayVanDon;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoVanDon;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayDen;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenPTVT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenNguoiNhan;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenNguoiGiao;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaDiemGiaoHang;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDiaDiemDoHang;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDiaDiemXepHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHieuChuyenDi;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayKhoiHanh;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayKhoiHanh;
        private Janus.Windows.UI.CommandBars.UICommand cmdXoaVanDon;
        private Janus.Windows.UI.CommandBars.UICommand cmdXoaVanDon1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoKien;
        private Janus.Windows.UI.CommandBars.UICommand ThemHang;
        private Janus.Windows.UI.CommandBars.UICommand ThemHang1;
        private Janus.Windows.UI.Dock.UIPanel uiPanel1;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel1Container;
        private Janus.Windows.GridEX.GridEX dgHang;
        private System.Windows.Forms.ContextMenuStrip ctxMmHang;
        private System.Windows.Forms.ToolStripMenuItem thêmHàngĐóngGóiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xóaHàngĐãChọnToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ctxMnContainer;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIComboBox cbLoaiKien;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemChuyenTai;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.EditControls.UIComboBox cmbLoaiPTVT;
        private System.Windows.Forms.Label label24;
        private Company.Interface.Controls.CuaKhauHControl ctrCuaKhauNhap;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtCuaKhauXuat;
        private Janus.Windows.EditControls.UIComboBox cmbLoaiVanDon;
        private System.Windows.Forms.Label label26;
        private Company.Interface.Controls.NuocHControl ctrNoiPhatHanh;
        private System.Windows.Forms.Label lblNoiPhatHanh;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaDiemChuyenTai;
    }
}
