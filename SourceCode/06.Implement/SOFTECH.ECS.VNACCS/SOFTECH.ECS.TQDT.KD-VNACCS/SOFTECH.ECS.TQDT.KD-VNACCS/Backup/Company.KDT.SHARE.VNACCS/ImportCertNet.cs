﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

namespace Company.KDT.SHARE.VNACCS
{
    public class ImportCertNet
    {
        public static string VNPT_CA_CER = "vnptca.cer";
        public static string MIC_NATIONAL_ROOT_CA_CRT = "MICNationalRootCA.crt";
        public static string TEST_VNACCS_VCIS_CA_CER = "Test_VNACCS_VCIS_CA.cer";

        public static void InstallCertificate(string cerFileName, StoreName storeType)
        {
            X509Certificate2 certificate = new X509Certificate2(cerFileName);
            X509Store store = new X509Store(storeType, StoreLocation.LocalMachine);

            store.Open(OpenFlags.ReadWrite);
            store.Add(certificate);
            store.Close();
        }

        public static bool InstallCA()
        {
            try
            {
                bool isInstallCA = false;
                isInstallCA = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsInstallCA") == "True" ? true : false;

                if (!isInstallCA)
                {
                    //Trusted Root Certification Authorities
                    ImportCertNet.InstallCertificate(AppDomain.CurrentDomain.BaseDirectory + "CertificateAuthority\\" + TEST_VNACCS_VCIS_CA_CER, System.Security.Cryptography.X509Certificates.StoreName.Root);
                    ImportCertNet.InstallCertificate(AppDomain.CurrentDomain.BaseDirectory + "CertificateAuthority\\" + MIC_NATIONAL_ROOT_CA_CRT, System.Security.Cryptography.X509Certificates.StoreName.Root);
                    //Intermediate Certification Authorities
                    ImportCertNet.InstallCertificate(AppDomain.CurrentDomain.BaseDirectory + "CertificateAuthority\\" + VNPT_CA_CER, System.Security.Cryptography.X509Certificates.StoreName.CertificateAuthority);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }
    }
}
