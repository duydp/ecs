﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
//using Company.Interface.GC;
//using Company.Interface.SXXK;

using Janus.Windows.EditControls;
using Janus.Windows.ExplorerBar;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;
using Company.Interface.SXXK;

namespace Company.Interface
{
    partial class MainForm
    {
        private UICommandManager cmMain;
        private UIRebar TopRebar1;
        private UICommand cmdHeThong;
        private UICommand cmdHeThong1;
        private UICommand cmdThoat;
        private UICommand cmdThoat1;
        private UIRebar BottomRebar1;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private UIPanelManager pmMain;
        private UICommandBar cmbMenu;
        private UICommand cmdLoaiHinh;
        private UIPanel pnlSXXK;
        private UIPanelInnerContainer pnlSXXKContainer;
        private UIPanel pnlGiaCong;
        private UIPanelInnerContainer pnlGiaCongContainer;
        private UIPanel pnlKinhDoanh;
        private UIPanelInnerContainer pnlKinhDoanhContainer;
        private UIPanel pnlDauTu;
        internal ImageList ilSmall;
        internal ImageList ilMedium;
        internal ImageList ilLarge;
        private UICommand cmdReceiveAll;
        private Janus.Windows.ExplorerBar.ExplorerBar expGiaCong;
        private Janus.Windows.ExplorerBar.ExplorerBar expKD;
        private Janus.Windows.UI.CommandBars.UICommand cmdThoat2;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup43 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem107 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem108 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup44 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem109 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem110 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup45 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem111 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem112 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem113 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem114 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem115 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup46 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem116 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem117 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem118 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem119 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem120 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup54 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem121 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem122 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem123 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem124 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem125 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem126 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem127 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup55 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem128 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem129 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem130 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem131 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup56 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem132 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem133 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem134 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem135 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup57 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem136 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem137 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem138 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup58 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem139 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem157 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem158 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem159 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem160 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem161 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem162 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem163 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup59 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem164 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem165 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem166 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem167 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem168 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup60 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem169 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem170 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem171 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup79 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem172 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem173 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem174 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup80 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem243 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem244 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem245 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup81 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem246 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem247 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem248 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup82 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem249 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem250 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem251 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup83 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem252 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem253 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup84 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem254 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem255 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem256 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem257 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup85 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem258 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem259 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup61 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem175 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem176 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem177 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup62 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem178 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem179 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup63 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem180 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem181 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem182 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup64 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem183 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem184 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup1 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem1 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem2 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup2 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem3 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem4 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup3 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem5 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem6 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem7 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem8 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup4 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem9 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem10 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup65 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem185 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem186 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem187 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup66 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem188 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem189 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup67 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem190 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem191 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup68 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem192 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem193 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem194 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem195 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup69 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem196 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem197 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem198 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup70 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem199 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem200 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup71 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem201 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem202 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup72 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem203 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem204 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup73 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem205 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem206 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem207 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem208 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem209 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem210 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup5 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem11 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem12 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup6 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem13 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem14 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup7 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem15 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem16 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup8 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem17 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem18 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup9 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem19 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem20 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup10 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem21 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem22 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup11 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem23 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem24 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem25 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup12 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem26 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup13 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem27 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem28 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem29 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem30 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup14 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem31 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem32 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup15 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem33 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem34 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem35 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup16 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem36 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup17 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem37 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem38 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup18 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem39 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem40 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem41 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem42 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup19 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem43 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem44 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup20 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem45 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem46 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup21 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem47 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem48 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem49 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup22 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem50 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem51 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem52 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup23 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem53 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem54 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem55 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup24 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem56 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem57 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem58 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup25 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem59 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem60 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem61 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup26 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem62 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem63 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup27 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem64 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem65 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem66 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup28 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem67 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem68 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup36 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem69 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem70 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel1 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel2 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel3 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel4 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel5 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel6 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel7 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel8 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbMenu = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdHeThong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHeThong");
            this.cmdDanhMuc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDanhMuc");
            this.cmdBieuThue1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBieuThue");
            this.QuanTri1 = new Janus.Windows.UI.CommandBars.UICommand("QuanTri");
            this.Command11 = new Janus.Windows.UI.CommandBars.UICommand("Command1");
            this.Command01 = new Janus.Windows.UI.CommandBars.UICommand("Command0");
            this.cmdCuaSo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaSo");
            this.cmbDongBoDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("cmbDongBoDuLieu");
            this.cmdHeThong = new Janus.Windows.UI.CommandBars.UICommand("cmdHeThong");
            this.cmdDataVersion1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDataVersion");
            this.cmdBackUp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBackUp");
            this.cmdUpdateDatabase2 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateDatabase");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdCapNhatHS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS");
            this.Separator9 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdNhapXuat2 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXuat");
            this.Separator10 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.NhacNho1 = new Janus.Windows.UI.CommandBars.UICommand("NhacNho");
            this.cmdConfig1 = new Janus.Windows.UI.CommandBars.UICommand("cmdConfig");
            this.cmdCauHinh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.mnuQuerySQL1 = new Janus.Windows.UI.CommandBars.UICommand("mnuQuerySQL");
            this.cmdLog1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdDaily1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDaily");
            this.Separator12 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.LoginUser1 = new Janus.Windows.UI.CommandBars.UICommand("LoginUser");
            this.cmdChangePass1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePass");
            this.cmdDoiMatKhauHQ1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDoiMatKhauHQ");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdNhanPhanHoi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanPhanHoi");
            this.cmdThoat2 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdThoat = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdLoaiHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdLoaiHinh");
            this.cmdReceiveAll = new Janus.Windows.UI.CommandBars.UICommand("cmdReceiveAll");
            this.NhacNho = new Janus.Windows.UI.CommandBars.UICommand("NhacNho");
            this.DongBoDuLieu = new Janus.Windows.UI.CommandBars.UICommand("DongBoDuLieu");
            this.cmdImport = new Janus.Windows.UI.CommandBars.UICommand("cmdImport");
            this.cmdImportNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportNPL");
            this.cmdImportSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportSP");
            this.cmdImportDM1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportDM");
            this.cmdImportToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportToKhai");
            this.cmdImportHangHoa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportHangHoa");
            this.cmdNPLNhapTon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLNhapTon");
            this.cmdImportNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdImportNPL");
            this.cmdImportSP = new Janus.Windows.UI.CommandBars.UICommand("cmdImportSP");
            this.cmdImportDM = new Janus.Windows.UI.CommandBars.UICommand("cmdImportDM");
            this.cmdImportTTDM = new Janus.Windows.UI.CommandBars.UICommand("cmdImportTTDM");
            this.cmdImportToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdImportToKhai");
            this.cmdImportHangHoa = new Janus.Windows.UI.CommandBars.UICommand("cmdImportHangHoa");
            this.Command1 = new Janus.Windows.UI.CommandBars.UICommand("Command1");
            this.cmd20071 = new Janus.Windows.UI.CommandBars.UICommand("cmd2007");
            this.cmd20031 = new Janus.Windows.UI.CommandBars.UICommand("cmd2003");
            this.cmdVN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVN");
            this.cmdEL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdEL");
            this.cmd2007 = new Janus.Windows.UI.CommandBars.UICommand("cmd2007");
            this.cmd2003 = new Janus.Windows.UI.CommandBars.UICommand("cmd2003");
            this.Command0 = new Janus.Windows.UI.CommandBars.UICommand("Command0");
            this.cmdHelp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHelp");
            this.cmdHelpVideo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHelpVideo");
            this.cmdHDSDCKS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHDSDCKS");
            this.cmdHDSDVNACCS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHDSDVNACCS");
            this.Separator13 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdAutoUpdate1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.Separator5 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdTeamview1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTeamview");
            this.cmdTool1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTool");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdActivate1 = new Janus.Windows.UI.CommandBars.UICommand("cmdActivate");
            this.cmdAbout1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAbout");
            this.cmdThongBaoVNACCS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThongBaoVNACCS");
            this.cmdHelp = new Janus.Windows.UI.CommandBars.UICommand("cmdHelp");
            this.cmdAbout = new Janus.Windows.UI.CommandBars.UICommand("cmdAbout");
            this.cmdNPLNhapTon = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLNhapTon");
            this.cmdDanhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdDanhMuc");
            this.cmdGetCategoryOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGetCategoryOnline");
            this.Separator6 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdHaiQuan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHaiQuan");
            this.cmdNuoc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNuoc");
            this.cmdMaHS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdMaHS");
            this.cmdNguyenTe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNguyenTe");
            this.cmdDVT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDVT");
            this.cmdPTTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPTTT");
            this.cmdPTVT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPTVT");
            this.cmdDKGH1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDKGH");
            this.cmdCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaKhau");
            this.cmdNhomCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhomCuaKhau");
            this.cmdLoaiPhiChungTuThanhToan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLoaiPhiChungTuThanhToan");
            this.cmdBerth1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBerth");
            this.cmdCargo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCargo");
            this.cmdCityUNLOCODE1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCityUNLOCODE");
            this.cmdCommon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCommon");
            this.cmdContainerSize1 = new Janus.Windows.UI.CommandBars.UICommand("cmdContainerSize");
            this.cmdCustomsSubSection1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCustomsSubSection");
            this.cmdOGAUser1 = new Janus.Windows.UI.CommandBars.UICommand("cmdOGAUser");
            this.cmdPackagesUnit1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPackagesUnit");
            this.cmdStations1 = new Janus.Windows.UI.CommandBars.UICommand("cmdStations");
            this.cmdTaxClassificationCode1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTaxClassificationCode");
            this.cmdReloadData1 = new Janus.Windows.UI.CommandBars.UICommand("cmdReloadData");
            this.cmdUpdateCategoryOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateCategoryOnline");
            this.cmdMaHS = new Janus.Windows.UI.CommandBars.UICommand("cmdMaHS");
            this.cmdNuoc = new Janus.Windows.UI.CommandBars.UICommand("cmdNuoc");
            this.cmdHaiQuan = new Janus.Windows.UI.CommandBars.UICommand("cmdHaiQuan");
            this.cmdNguyenTe = new Janus.Windows.UI.CommandBars.UICommand("cmdNguyenTe");
            this.cmdDVT = new Janus.Windows.UI.CommandBars.UICommand("cmdDVT");
            this.cmdPTTT = new Janus.Windows.UI.CommandBars.UICommand("cmdPTTT");
            this.cmdPTVT = new Janus.Windows.UI.CommandBars.UICommand("cmdPTVT");
            this.cmdDKGH = new Janus.Windows.UI.CommandBars.UICommand("cmdDKGH");
            this.cmdCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaKhau");
            this.cmdBackUp = new Janus.Windows.UI.CommandBars.UICommand("cmdBackUp");
            this.cmdRestore = new Janus.Windows.UI.CommandBars.UICommand("cmdRestore");
            this.ThongSoKetNoi = new Janus.Windows.UI.CommandBars.UICommand("ThongSoKetNoi");
            this.TLThongTinDNHQ = new Janus.Windows.UI.CommandBars.UICommand("TLThongTinDNHQ");
            this.cmdThietLapIn = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapIn");
            this.cmdExportExccel = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdImportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdDongBoPhongKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdDongBoPhongKhai");
            this.cmdImportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdExportExccel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdCauHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.ThongSoKetNoi1 = new Janus.Windows.UI.CommandBars.UICommand("ThongSoKetNoi");
            this.TLThongTinDNHQ1 = new Janus.Windows.UI.CommandBars.UICommand("TLThongTinDNHQ");
            this.cmdCauHinhToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinhToKhai");
            this.cmdThietLapIn1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapIn");
            this.cmdChuKySo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChuKySo");
            this.cmdTimer1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTimer");
            this.cmdConfigReadExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdConfigReadExcel");
            this.cmdCauHinhToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinhToKhai");
            this.QuanTri = new Janus.Windows.UI.CommandBars.UICommand("QuanTri");
            this.QuanLyNguoiDung1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNguoiDung");
            this.QuanLyNhom1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNhom");
            this.QuanLyNguoiDung = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNguoiDung");
            this.QuanLyNhom = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNhom");
            this.LoginUser = new Janus.Windows.UI.CommandBars.UICommand("LoginUser");
            this.cmdChangePass = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePass");
            this.cmdDoiMatKhauHQ = new Janus.Windows.UI.CommandBars.UICommand("cmdDoiMatKhauHQ");
            this.cmdCuaSo = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaSo");
            this.cmdCloseAll1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.cmdCloseAllButThis1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButMe");
            this.cmdCloseAll = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.cmdCloseAllButMe = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButMe");
            this.cmdAutoUpdate = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.cmdNhapXML = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXML");
            this.cmdNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPL");
            this.cmdSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSP");
            this.cmdDM1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDM");
            this.cmdTK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTK");
            this.cmdNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdNPL");
            this.cmdSP = new Janus.Windows.UI.CommandBars.UICommand("cmdSP");
            this.cmdDM = new Janus.Windows.UI.CommandBars.UICommand("cmdDM");
            this.cmdTK = new Janus.Windows.UI.CommandBars.UICommand("cmdTK");
            this.cmdXuatDuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieu");
            this.cmdHUNGNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGNPL");
            this.cmdHUNGSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGSP");
            this.cmdHUNGDM1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGDM");
            this.cmdHUNGTK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGTK");
            this.cmdXuatNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatNPL");
            this.cmdXuatDinhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDinhMuc");
            this.cmdXuatSanPham = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatSanPham");
            this.cmdXuatToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.cmdVN = new Janus.Windows.UI.CommandBars.UICommand("cmdVN");
            this.cmdEL = new Janus.Windows.UI.CommandBars.UICommand("cmdEL");
            this.cmdHUNGNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGNPL");
            this.cmdHUNGSP = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGSP");
            this.cmdHUNGDM = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGDM");
            this.cmdHUNGTK = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGTK");
            this.cmdActivate = new Janus.Windows.UI.CommandBars.UICommand("cmdActivate");
            this.cmdXuatDuLieuDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieuDN");
            this.cmdXuatNPLDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatNPLDN");
            this.cmdXuatSPDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatSPDN");
            this.cmdXuatDMDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDMDN");
            this.cmdXuatTKDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatTKDN");
            this.cmdXuatNPLDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatNPLDN");
            this.cmdXuatSPDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatSPDN");
            this.cmXuat = new Janus.Windows.UI.CommandBars.UICommand("cmXuat");
            this.cmdXuatDMDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDMDN");
            this.cmdXuatTKDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatTKDN");
            this.cmdNhapDuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDuLieu");
            this.cmdNhapNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapNPL");
            this.cmdNhapSanPham1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapSanPham");
            this.cmdNhapDinhMuc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDinhMuc");
            this.cmdNhapToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhai");
            this.cmdNhapNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapNPL");
            this.cmdNhapSanPham = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapSanPham");
            this.cmdNhapDinhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDinhMuc");
            this.cmdNhapToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhai");
            this.QuanlyMess = new Janus.Windows.UI.CommandBars.UICommand("QuanlyMess");
            this.cmdConfig = new Janus.Windows.UI.CommandBars.UICommand("cmdConfig");
            this.mnuQuerySQL = new Janus.Windows.UI.CommandBars.UICommand("mnuQuerySQL");
            this.mnuFormSQL1 = new Janus.Windows.UI.CommandBars.UICommand("mnuFormSQL");
            this.TraCuuMaHS = new Janus.Windows.UI.CommandBars.UICommand("TraCuuMaHS");
            this.cmdNhomCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdNhomCuaKhau");
            this.cmdLog = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.cmdLoaiPhiChungTuThanhToan = new Janus.Windows.UI.CommandBars.UICommand("cmdLoaiPhiChungTuThanhToan");
            this.cmdDataVersion = new Janus.Windows.UI.CommandBars.UICommand("cmdDataVersion");
            this.cmdImportXml = new Janus.Windows.UI.CommandBars.UICommand("cmdImportXml");
            this.cmdChuKySo = new Janus.Windows.UI.CommandBars.UICommand("cmdChuKySo");
            this.cmdTimer = new Janus.Windows.UI.CommandBars.UICommand("cmdTimer");
            this.mnuFormSQL = new Janus.Windows.UI.CommandBars.UICommand("mnuFormSQL");
            this.mnuMiniSQL = new Janus.Windows.UI.CommandBars.UICommand("mnuMiniSQL");
            this.cmdNhapXuat = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXuat");
            this.cmdImportXml1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportXml");
            this.cmdNhapXML1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXML");
            this.cmdImportExcel2 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdNhapDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDuLieu");
            this.Separator8 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdXuatDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieu");
            this.cmdXuatDuLieuDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieuDN");
            this.cmdBieuThue = new Janus.Windows.UI.CommandBars.UICommand("cmdBieuThue");
            this.TraCuuMaHS1 = new Janus.Windows.UI.CommandBars.UICommand("TraCuuMaHS");
            this.Separator7 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdTraCuuXNKOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuXNKOnline");
            this.cmdTraCuuNoThueOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuNoThueOnline");
            this.cmdTraCuuVanBanOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuVanBanOnline");
            this.cmdTuVanHQOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTuVanHQOnline");
            this.cmdBieuThueXNK20181 = new Janus.Windows.UI.CommandBars.UICommand("cmdBieuThueXNK2018");
            this.cmdTraCuuXNKOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuXNKOnline");
            this.cmdTraCuuNoThueOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuNoThueOnline");
            this.cmdTraCuuVanBanOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuVanBanOnline");
            this.cmdTuVanHQOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdTuVanHQOnline");
            this.cmdGetCategoryOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdGetCategoryOnline");
            this.cmdTeamview = new Janus.Windows.UI.CommandBars.UICommand("cmdTeamview");
            this.cmdCapNhatHS = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS");
            this.cmdCapNhatHS8SoAuto1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS8SoAuto");
            this.cmdCapNhatHS8SoManual1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS8SoManual");
            this.cmdCapNhatHS8SoAuto = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS8SoAuto");
            this.cmdCapNhatHS8SoManual = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS8SoManual");
            this.cmdTool = new Janus.Windows.UI.CommandBars.UICommand("cmdTool");
            this.cmdImageResizeHelp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImageResizeHelp");
            this.cmdSignFile1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSignFile");
            this.cmdHelpSignFile1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHelpSignFile");
            this.cmdImageResizeHelp = new Janus.Windows.UI.CommandBars.UICommand("cmdImageResizeHelp");
            this.cmbDongBoDuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmbDongBoDuLieu");
            this.DongBoDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("DongBoDuLieu");
            this.cmdQuanLyDL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdQuanLyDL");
            this.cmdQuanLyDL = new Janus.Windows.UI.CommandBars.UICommand("cmdQuanLyDL");
            this.cmdDaily = new Janus.Windows.UI.CommandBars.UICommand("cmdDaily");
            this.cmdUpdateDatabase = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateDatabase");
            this.cmdCloseMe = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseMe");
            this.cmdHelpVideo = new Janus.Windows.UI.CommandBars.UICommand("cmdHelpVideo");
            this.cmdHDSDCKS = new Janus.Windows.UI.CommandBars.UICommand("cmdHDSDCKS");
            this.cmdHDSDVNACCS = new Janus.Windows.UI.CommandBars.UICommand("cmdHDSDVNACCS");
            this.cmdThongBaoVNACCS = new Janus.Windows.UI.CommandBars.UICommand("cmdThongBaoVNACCS");
            this.cmdSignFile = new Janus.Windows.UI.CommandBars.UICommand("cmdSignFile");
            this.cmdHelpSignFile = new Janus.Windows.UI.CommandBars.UICommand("cmdHelpSignFile");
            this.cmdBerth = new Janus.Windows.UI.CommandBars.UICommand("cmdBerth");
            this.cmdCargo = new Janus.Windows.UI.CommandBars.UICommand("cmdCargo");
            this.cmdCityUNLOCODE = new Janus.Windows.UI.CommandBars.UICommand("cmdCityUNLOCODE");
            this.cmdCommon = new Janus.Windows.UI.CommandBars.UICommand("cmdCommon");
            this.cmdContainerSize = new Janus.Windows.UI.CommandBars.UICommand("cmdContainerSize");
            this.cmdCustomsSubSection = new Janus.Windows.UI.CommandBars.UICommand("cmdCustomsSubSection");
            this.cmdOGAUser = new Janus.Windows.UI.CommandBars.UICommand("cmdOGAUser");
            this.cmdPackagesUnit = new Janus.Windows.UI.CommandBars.UICommand("cmdPackagesUnit");
            this.cmdStations = new Janus.Windows.UI.CommandBars.UICommand("cmdStations");
            this.cmdTaxClassificationCode = new Janus.Windows.UI.CommandBars.UICommand("cmdTaxClassificationCode");
            this.cmdBieuThueXNK2018 = new Janus.Windows.UI.CommandBars.UICommand("cmdBieuThueXNK2018");
            this.cmdUpdateCategoryOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateCategoryOnline");
            this.cmdNhanPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanPhanHoi");
            this.cmdReloadData = new Janus.Windows.UI.CommandBars.UICommand("cmdReloadData");
            this.cmdConfigReadExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdConfigReadExcel");
            this.mnuRightClick = new Janus.Windows.UI.CommandBars.UIContextMenu();
            this.cmdCloseMe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseMe");
            this.cmdCloseAllButMe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButMe");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.ilSmall = new System.Windows.Forms.ImageList(this.components);
            this.cmdExportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdThoat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.pmMain = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expSXXK = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel2 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanelContainerVNACCTK = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expKhaiBao_TheoDoi = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanelVNACCToKhai = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel3Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarVNACCS_TKMD = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanelVNACCGiayPhep = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanelContainerVNACCGP = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarVNACCS_GiayPhep = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanelVNACCHoaDon = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanelContainerVNACCHD = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarVNACCS_HoaDon = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel4 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel4Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBar1 = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel5 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel5Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarBCTongHopDuLieu = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel6 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel6Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarKhoKeToan = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlSXXK = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlSXXKContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.pnlGiaCong = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlGiaCongContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expGiaCong = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlKinhDoanh = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlKinhDoanhContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expKD = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlDauTu = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlDauTuContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expDT = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlSend = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlSendContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.ilMedium = new System.Windows.Forms.ImageList(this.components);
            this.ilLarge = new System.Windows.Forms.ImageList(this.components);
            this.statusBar = new Janus.Windows.UI.StatusBar.UIStatusBar();
            this.cmdThoat3 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.vlECS = new SoftechVersion.ValidVersion(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cmdThoat4 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdNhapXuat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXuat");
            this.cmdUpdateDatabase1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateDatabase");
            this.uiPanel3 = new Janus.Windows.UI.Dock.UIPanel();
            this.cmdPerformanceDatabase = new Janus.Windows.UI.CommandBars.UICommand("cmdPerformanceDatabase");
            this.cmdPerformanceDatabase1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPerformanceDatabase");
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mnuRightClick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expSXXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).BeginInit();
            this.uiPanel2.SuspendLayout();
            this.uiPanelContainerVNACCTK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expKhaiBao_TheoDoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelVNACCToKhai)).BeginInit();
            this.uiPanelVNACCToKhai.SuspendLayout();
            this.uiPanel3Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_TKMD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelVNACCGiayPhep)).BeginInit();
            this.uiPanelVNACCGiayPhep.SuspendLayout();
            this.uiPanelContainerVNACCGP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_GiayPhep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelVNACCHoaDon)).BeginInit();
            this.uiPanelVNACCHoaDon.SuspendLayout();
            this.uiPanelContainerVNACCHD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_HoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel4)).BeginInit();
            this.uiPanel4.SuspendLayout();
            this.uiPanel4Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel5)).BeginInit();
            this.uiPanel5.SuspendLayout();
            this.uiPanel5Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarBCTongHopDuLieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel6)).BeginInit();
            this.uiPanel6.SuspendLayout();
            this.uiPanel6Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarKhoKeToan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSXXK)).BeginInit();
            this.pnlSXXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiaCong)).BeginInit();
            this.pnlGiaCong.SuspendLayout();
            this.pnlGiaCongContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKinhDoanh)).BeginInit();
            this.pnlKinhDoanh.SuspendLayout();
            this.pnlKinhDoanhContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expKD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDauTu)).BeginInit();
            this.pnlDauTu.SuspendLayout();
            this.pnlDauTuContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSend)).BeginInit();
            this.pnlSend.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel3)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.BackColor = System.Drawing.SystemColors.Control;
            this.grbMain.Location = new System.Drawing.Point(245, 29);
            this.grbMain.Size = new System.Drawing.Size(702, 595);
            this.grbMain.Visible = false;
            this.grbMain.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbMain.Click += new System.EventHandler(this.grbMain_Click);
            // 
            // cmMain
            // 
            this.cmMain.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowMerge = false;
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbMenu});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHeThong,
            this.cmdThoat,
            this.cmdLoaiHinh,
            this.cmdReceiveAll,
            this.NhacNho,
            this.DongBoDuLieu,
            this.cmdImport,
            this.cmdImportNPL,
            this.cmdImportSP,
            this.cmdImportDM,
            this.cmdImportTTDM,
            this.cmdImportToKhai,
            this.cmdImportHangHoa,
            this.Command1,
            this.cmd2007,
            this.cmd2003,
            this.Command0,
            this.cmdHelp,
            this.cmdAbout,
            this.cmdNPLNhapTon,
            this.cmdDanhMuc,
            this.cmdMaHS,
            this.cmdNuoc,
            this.cmdHaiQuan,
            this.cmdNguyenTe,
            this.cmdDVT,
            this.cmdPTTT,
            this.cmdPTVT,
            this.cmdDKGH,
            this.cmdCuaKhau,
            this.cmdBackUp,
            this.cmdRestore,
            this.ThongSoKetNoi,
            this.TLThongTinDNHQ,
            this.cmdThietLapIn,
            this.cmdExportExccel,
            this.cmdImportExcel,
            this.cmdDongBoPhongKhai,
            this.cmdCauHinh,
            this.cmdCauHinhToKhai,
            this.QuanTri,
            this.QuanLyNguoiDung,
            this.QuanLyNhom,
            this.LoginUser,
            this.cmdChangePass,
            this.cmdDoiMatKhauHQ,
            this.cmdCuaSo,
            this.cmdCloseAll,
            this.cmdCloseAllButMe,
            this.cmdAutoUpdate,
            this.cmdNhapXML,
            this.cmdNPL,
            this.cmdSP,
            this.cmdDM,
            this.cmdTK,
            this.cmdXuatDuLieu,
            this.cmdXuatNPL,
            this.cmdXuatDinhMuc,
            this.cmdXuatSanPham,
            this.cmdXuatToKhai,
            this.cmdVN,
            this.cmdEL,
            this.cmdHUNGNPL,
            this.cmdHUNGSP,
            this.cmdHUNGDM,
            this.cmdHUNGTK,
            this.cmdActivate,
            this.cmdXuatDuLieuDN,
            this.cmdXuatNPLDN,
            this.cmdXuatSPDN,
            this.cmXuat,
            this.cmdXuatDMDN,
            this.cmdXuatTKDN,
            this.cmdNhapDuLieu,
            this.cmdNhapNPL,
            this.cmdNhapSanPham,
            this.cmdNhapDinhMuc,
            this.cmdNhapToKhai,
            this.QuanlyMess,
            this.cmdConfig,
            this.mnuQuerySQL,
            this.TraCuuMaHS,
            this.cmdNhomCuaKhau,
            this.cmdLog,
            this.cmdLoaiPhiChungTuThanhToan,
            this.cmdDataVersion,
            this.cmdImportXml,
            this.cmdChuKySo,
            this.cmdTimer,
            this.mnuFormSQL,
            this.mnuMiniSQL,
            this.cmdNhapXuat,
            this.cmdBieuThue,
            this.cmdTraCuuXNKOnline,
            this.cmdTraCuuNoThueOnline,
            this.cmdTraCuuVanBanOnline,
            this.cmdTuVanHQOnline,
            this.cmdGetCategoryOnline,
            this.cmdTeamview,
            this.cmdCapNhatHS,
            this.cmdCapNhatHS8SoAuto,
            this.cmdCapNhatHS8SoManual,
            this.cmdTool,
            this.cmdImageResizeHelp,
            this.cmbDongBoDuLieu,
            this.cmdQuanLyDL,
            this.cmdDaily,
            this.cmdUpdateDatabase,
            this.cmdCloseMe,
            this.cmdHelpVideo,
            this.cmdHDSDCKS,
            this.cmdHDSDVNACCS,
            this.cmdThongBaoVNACCS,
            this.cmdSignFile,
            this.cmdHelpSignFile,
            this.cmdBerth,
            this.cmdCargo,
            this.cmdCityUNLOCODE,
            this.cmdCommon,
            this.cmdContainerSize,
            this.cmdCustomsSubSection,
            this.cmdOGAUser,
            this.cmdPackagesUnit,
            this.cmdStations,
            this.cmdTaxClassificationCode,
            this.cmdBieuThueXNK2018,
            this.cmdUpdateCategoryOnline,
            this.cmdNhanPhanHoi,
            this.cmdReloadData,
            this.cmdConfigReadExcel,
            this.cmdPerformanceDatabase});
            this.cmMain.ContainerControl = this;
            this.cmMain.ContextMenus.AddRange(new Janus.Windows.UI.CommandBars.UIContextMenu[] {
            this.mnuRightClick});
            this.cmMain.Id = new System.Guid("eae49f54-3bfa-4a6a-8b46-89b443ba80cd");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.LockCommandBars = true;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmbMenu
            // 
            this.cmbMenu.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.AllowMerge = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.CommandBarType = Janus.Windows.UI.CommandBars.CommandBarType.Menu;
            this.cmbMenu.CommandManager = this.cmMain;
            this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHeThong1,
            this.cmdDanhMuc1,
            this.cmdBieuThue1,
            this.QuanTri1,
            this.Command11,
            this.Command01,
            this.cmdCuaSo1,
            this.cmbDongBoDuLieu1});
            this.cmbMenu.Key = "cmbMenu";
            this.cmbMenu.Location = new System.Drawing.Point(0, 0);
            this.cmbMenu.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbMenu.MergeRowOrder = 0;
            this.cmbMenu.Name = "cmbMenu";
            this.cmbMenu.RowIndex = 0;
            this.cmbMenu.Size = new System.Drawing.Size(950, 26);
            this.cmbMenu.Text = "cmbMenu";
            this.cmbMenu.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMenu_CommandClick);
            // 
            // cmdHeThong1
            // 
            this.cmdHeThong1.Key = "cmdHeThong";
            this.cmdHeThong1.Name = "cmdHeThong1";
            this.cmdHeThong1.Text = "&Hệ thống";
            // 
            // cmdDanhMuc1
            // 
            this.cmdDanhMuc1.Key = "cmdDanhMuc";
            this.cmdDanhMuc1.Name = "cmdDanhMuc1";
            this.cmdDanhMuc1.Text = "&Danh mục";
            // 
            // cmdBieuThue1
            // 
            this.cmdBieuThue1.Key = "cmdBieuThue";
            this.cmdBieuThue1.Name = "cmdBieuThue1";
            // 
            // QuanTri1
            // 
            this.QuanTri1.Key = "QuanTri";
            this.QuanTri1.Name = "QuanTri1";
            this.QuanTri1.Text = "&Quản trị";
            // 
            // Command11
            // 
            this.Command11.Key = "Command1";
            this.Command11.Name = "Command11";
            this.Command11.Text = "&Giao diện";
            // 
            // Command01
            // 
            this.Command01.Key = "Command0";
            this.Command01.Name = "Command01";
            this.Command01.Text = "&Trợ giúp";
            // 
            // cmdCuaSo1
            // 
            this.cmdCuaSo1.Key = "cmdCuaSo";
            this.cmdCuaSo1.Name = "cmdCuaSo1";
            this.cmdCuaSo1.Text = "&Cửa sổ";
            // 
            // cmbDongBoDuLieu1
            // 
            this.cmbDongBoDuLieu1.Key = "cmbDongBoDuLieu";
            this.cmbDongBoDuLieu1.Name = "cmbDongBoDuLieu1";
            // 
            // cmdHeThong
            // 
            this.cmdHeThong.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdDataVersion1,
            this.cmdBackUp1,
            this.cmdUpdateDatabase2,
            this.Separator1,
            this.cmdCapNhatHS1,
            this.Separator9,
            this.cmdNhapXuat2,
            this.Separator10,
            this.NhacNho1,
            this.cmdConfig1,
            this.cmdCauHinh1,
            this.cmdPerformanceDatabase1,
            this.mnuQuerySQL1,
            this.cmdLog1,
            this.Separator3,
            this.cmdDaily1,
            this.Separator12,
            this.LoginUser1,
            this.cmdChangePass1,
            this.cmdDoiMatKhauHQ1,
            this.Separator4,
            this.cmdNhanPhanHoi1,
            this.cmdThoat2});
            this.cmdHeThong.Key = "cmdHeThong";
            this.cmdHeThong.Name = "cmdHeThong";
            this.cmdHeThong.Text = "&Hệ thống";
            // 
            // cmdDataVersion1
            // 
            this.cmdDataVersion1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDataVersion1.Image")));
            this.cmdDataVersion1.Key = "cmdDataVersion";
            this.cmdDataVersion1.Name = "cmdDataVersion1";
            this.cmdDataVersion1.Text = "Dữ liệu phiên bản: [?]";
            // 
            // cmdBackUp1
            // 
            this.cmdBackUp1.Key = "cmdBackUp";
            this.cmdBackUp1.Name = "cmdBackUp1";
            // 
            // cmdUpdateDatabase2
            // 
            this.cmdUpdateDatabase2.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateDatabase2.Image")));
            this.cmdUpdateDatabase2.Key = "cmdUpdateDatabase";
            this.cmdUpdateDatabase2.Name = "cmdUpdateDatabase2";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdCapNhatHS1
            // 
            this.cmdCapNhatHS1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCapNhatHS1.Image")));
            this.cmdCapNhatHS1.Key = "cmdCapNhatHS";
            this.cmdCapNhatHS1.Name = "cmdCapNhatHS1";
            // 
            // Separator9
            // 
            this.Separator9.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator9.Key = "Separator";
            this.Separator9.Name = "Separator9";
            // 
            // cmdNhapXuat2
            // 
            this.cmdNhapXuat2.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhapXuat2.Image")));
            this.cmdNhapXuat2.Key = "cmdNhapXuat";
            this.cmdNhapXuat2.Name = "cmdNhapXuat2";
            // 
            // Separator10
            // 
            this.Separator10.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator10.Key = "Separator";
            this.Separator10.Name = "Separator10";
            // 
            // NhacNho1
            // 
            this.NhacNho1.Image = ((System.Drawing.Image)(resources.GetObject("NhacNho1.Image")));
            this.NhacNho1.Key = "NhacNho";
            this.NhacNho1.Name = "NhacNho1";
            // 
            // cmdConfig1
            // 
            this.cmdConfig1.Image = ((System.Drawing.Image)(resources.GetObject("cmdConfig1.Image")));
            this.cmdConfig1.ImageIndex = 37;
            this.cmdConfig1.Key = "cmdConfig";
            this.cmdConfig1.Name = "cmdConfig1";
            // 
            // cmdCauHinh1
            // 
            this.cmdCauHinh1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCauHinh1.Image")));
            this.cmdCauHinh1.Key = "cmdCauHinh";
            this.cmdCauHinh1.Name = "cmdCauHinh1";
            // 
            // mnuQuerySQL1
            // 
            this.mnuQuerySQL1.Image = ((System.Drawing.Image)(resources.GetObject("mnuQuerySQL1.Image")));
            this.mnuQuerySQL1.ImageIndex = 42;
            this.mnuQuerySQL1.Key = "mnuQuerySQL";
            this.mnuQuerySQL1.Name = "mnuQuerySQL1";
            // 
            // cmdLog1
            // 
            this.cmdLog1.Image = ((System.Drawing.Image)(resources.GetObject("cmdLog1.Image")));
            this.cmdLog1.ImageIndex = 41;
            this.cmdLog1.Key = "cmdLog";
            this.cmdLog1.Name = "cmdLog1";
            // 
            // Separator3
            // 
            this.Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator3.Key = "Separator";
            this.Separator3.Name = "Separator3";
            // 
            // cmdDaily1
            // 
            this.cmdDaily1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDaily1.Image")));
            this.cmdDaily1.Key = "cmdDaily";
            this.cmdDaily1.Name = "cmdDaily1";
            // 
            // Separator12
            // 
            this.Separator12.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator12.Key = "Separator";
            this.Separator12.Name = "Separator12";
            // 
            // LoginUser1
            // 
            this.LoginUser1.Image = ((System.Drawing.Image)(resources.GetObject("LoginUser1.Image")));
            this.LoginUser1.Key = "LoginUser";
            this.LoginUser1.Name = "LoginUser1";
            // 
            // cmdChangePass1
            // 
            this.cmdChangePass1.Image = ((System.Drawing.Image)(resources.GetObject("cmdChangePass1.Image")));
            this.cmdChangePass1.Key = "cmdChangePass";
            this.cmdChangePass1.Name = "cmdChangePass1";
            this.cmdChangePass1.Text = "Đổi mật khẩu đăng nhập";
            // 
            // cmdDoiMatKhauHQ1
            // 
            this.cmdDoiMatKhauHQ1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdDoiMatKhauHQ1.Icon")));
            this.cmdDoiMatKhauHQ1.Key = "cmdDoiMatKhauHQ";
            this.cmdDoiMatKhauHQ1.Name = "cmdDoiMatKhauHQ1";
            this.cmdDoiMatKhauHQ1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdNhanPhanHoi1
            // 
            this.cmdNhanPhanHoi1.Key = "cmdNhanPhanHoi";
            this.cmdNhanPhanHoi1.Name = "cmdNhanPhanHoi1";
            // 
            // cmdThoat2
            // 
            this.cmdThoat2.Image = ((System.Drawing.Image)(resources.GetObject("cmdThoat2.Image")));
            this.cmdThoat2.Key = "cmdThoat";
            this.cmdThoat2.Name = "cmdThoat2";
            // 
            // cmdThoat
            // 
            this.cmdThoat.Key = "cmdThoat";
            this.cmdThoat.Name = "cmdThoat";
            this.cmdThoat.Text = "Thoát";
            // 
            // cmdLoaiHinh
            // 
            this.cmdLoaiHinh.Key = "cmdLoaiHinh";
            this.cmdLoaiHinh.Name = "cmdLoaiHinh";
            this.cmdLoaiHinh.Text = "Loại hình";
            // 
            // cmdReceiveAll
            // 
            this.cmdReceiveAll.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdReceiveAll.Icon")));
            this.cmdReceiveAll.Key = "cmdReceiveAll";
            this.cmdReceiveAll.Name = "cmdReceiveAll";
            this.cmdReceiveAll.Text = "Cập nhật thông tin";
            // 
            // NhacNho
            // 
            this.NhacNho.Key = "NhacNho";
            this.NhacNho.Name = "NhacNho";
            this.NhacNho.Text = "Nhắc nhở";
            // 
            // DongBoDuLieu
            // 
            this.DongBoDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("DongBoDuLieu.Icon")));
            this.DongBoDuLieu.Key = "DongBoDuLieu";
            this.DongBoDuLieu.Name = "DongBoDuLieu";
            this.DongBoDuLieu.Text = "Truyền nhận dữ liệu";
            // 
            // cmdImport
            // 
            this.cmdImport.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportNPL1,
            this.cmdImportSP1,
            this.cmdImportDM1,
            this.cmdImportToKhai1,
            this.cmdImportHangHoa1,
            this.cmdNPLNhapTon1});
            this.cmdImport.Key = "cmdImport";
            this.cmdImport.Name = "cmdImport";
            this.cmdImport.Text = "Import from Excel";
            // 
            // cmdImportNPL1
            // 
            this.cmdImportNPL1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportNPL1.Icon")));
            this.cmdImportNPL1.Key = "cmdImportNPL";
            this.cmdImportNPL1.Name = "cmdImportNPL1";
            this.cmdImportNPL1.Text = "Nguyên phụ liệu";
            // 
            // cmdImportSP1
            // 
            this.cmdImportSP1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportSP1.Icon")));
            this.cmdImportSP1.Key = "cmdImportSP";
            this.cmdImportSP1.Name = "cmdImportSP1";
            this.cmdImportSP1.Text = "Sản phẩm";
            // 
            // cmdImportDM1
            // 
            this.cmdImportDM1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportDM1.Icon")));
            this.cmdImportDM1.Key = "cmdImportDM";
            this.cmdImportDM1.Name = "cmdImportDM1";
            this.cmdImportDM1.Text = "Định mức";
            // 
            // cmdImportToKhai1
            // 
            this.cmdImportToKhai1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportToKhai1.Icon")));
            this.cmdImportToKhai1.Key = "cmdImportToKhai";
            this.cmdImportToKhai1.Name = "cmdImportToKhai1";
            this.cmdImportToKhai1.Text = "Tờ khai";
            // 
            // cmdImportHangHoa1
            // 
            this.cmdImportHangHoa1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportHangHoa1.Icon")));
            this.cmdImportHangHoa1.Key = "cmdImportHangHoa";
            this.cmdImportHangHoa1.Name = "cmdImportHangHoa1";
            this.cmdImportHangHoa1.Text = "Hàng của tờ khai";
            // 
            // cmdNPLNhapTon1
            // 
            this.cmdNPLNhapTon1.Key = "cmdNPLNhapTon";
            this.cmdNPLNhapTon1.Name = "cmdNPLNhapTon1";
            // 
            // cmdImportNPL
            // 
            this.cmdImportNPL.Key = "cmdImportNPL";
            this.cmdImportNPL.Name = "cmdImportNPL";
            this.cmdImportNPL.Text = "Import NPL";
            // 
            // cmdImportSP
            // 
            this.cmdImportSP.Key = "cmdImportSP";
            this.cmdImportSP.Name = "cmdImportSP";
            this.cmdImportSP.Text = "Import Sản phẩm";
            // 
            // cmdImportDM
            // 
            this.cmdImportDM.Key = "cmdImportDM";
            this.cmdImportDM.Name = "cmdImportDM";
            this.cmdImportDM.Text = "Import Định mức";
            // 
            // cmdImportTTDM
            // 
            this.cmdImportTTDM.Key = "cmdImportTTDM";
            this.cmdImportTTDM.Name = "cmdImportTTDM";
            this.cmdImportTTDM.Text = "Import thông tin định mức";
            // 
            // cmdImportToKhai
            // 
            this.cmdImportToKhai.Key = "cmdImportToKhai";
            this.cmdImportToKhai.Name = "cmdImportToKhai";
            this.cmdImportToKhai.Text = "Import tờ khai";
            // 
            // cmdImportHangHoa
            // 
            this.cmdImportHangHoa.Key = "cmdImportHangHoa";
            this.cmdImportHangHoa.Name = "cmdImportHangHoa";
            this.cmdImportHangHoa.Text = "Import hàng tờ khai";
            // 
            // Command1
            // 
            this.Command1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmd20071,
            this.cmd20031,
            this.cmdVN1,
            this.cmdEL1});
            this.Command1.Key = "Command1";
            this.Command1.Name = "Command1";
            this.Command1.Text = "Giao diện";
            // 
            // cmd20071
            // 
            this.cmd20071.Image = ((System.Drawing.Image)(resources.GetObject("cmd20071.Image")));
            this.cmd20071.Key = "cmd2007";
            this.cmd20071.Name = "cmd20071";
            // 
            // cmd20031
            // 
            this.cmd20031.Image = ((System.Drawing.Image)(resources.GetObject("cmd20031.Image")));
            this.cmd20031.Key = "cmd2003";
            this.cmd20031.Name = "cmd20031";
            // 
            // cmdVN1
            // 
            this.cmdVN1.Checked = Janus.Windows.UI.InheritableBoolean.False;
            this.cmdVN1.Image = ((System.Drawing.Image)(resources.GetObject("cmdVN1.Image")));
            this.cmdVN1.ImageIndex = 35;
            this.cmdVN1.Key = "cmdVN";
            this.cmdVN1.Name = "cmdVN1";
            this.cmdVN1.Text = "Tiếng Việt";
            // 
            // cmdEL1
            // 
            this.cmdEL1.Image = ((System.Drawing.Image)(resources.GetObject("cmdEL1.Image")));
            this.cmdEL1.ImageIndex = 36;
            this.cmdEL1.Key = "cmdEL";
            this.cmdEL1.Name = "cmdEL1";
            this.cmdEL1.Text = "Tiếng Anh";
            // 
            // cmd2007
            // 
            this.cmd2007.Key = "cmd2007";
            this.cmd2007.Name = "cmd2007";
            this.cmd2007.Text = "Office 2007";
            // 
            // cmd2003
            // 
            this.cmd2003.Key = "cmd2003";
            this.cmd2003.Name = "cmd2003";
            this.cmd2003.Text = "Office 2003";
            // 
            // Command0
            // 
            this.Command0.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHelp1,
            this.cmdHelpVideo1,
            this.cmdHDSDCKS1,
            this.cmdHDSDVNACCS1,
            this.Separator13,
            this.cmdAutoUpdate1,
            this.Separator5,
            this.cmdTeamview1,
            this.cmdTool1,
            this.Separator2,
            this.cmdActivate1,
            this.cmdAbout1,
            this.cmdThongBaoVNACCS1});
            this.Command0.Key = "Command0";
            this.Command0.Name = "Command0";
            this.Command0.Text = "Trợ giúp";
            // 
            // cmdHelp1
            // 
            this.cmdHelp1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHelp1.Image")));
            this.cmdHelp1.Key = "cmdHelp";
            this.cmdHelp1.Name = "cmdHelp1";
            this.cmdHelp1.Text = "&Hướng dẫn sử dụng";
            // 
            // cmdHelpVideo1
            // 
            this.cmdHelpVideo1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHelpVideo1.Image")));
            this.cmdHelpVideo1.Key = "cmdHelpVideo";
            this.cmdHelpVideo1.Name = "cmdHelpVideo1";
            // 
            // cmdHDSDCKS1
            // 
            this.cmdHDSDCKS1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHDSDCKS1.Image")));
            this.cmdHDSDCKS1.Key = "cmdHDSDCKS";
            this.cmdHDSDCKS1.Name = "cmdHDSDCKS1";
            // 
            // cmdHDSDVNACCS1
            // 
            this.cmdHDSDVNACCS1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHDSDVNACCS1.Image")));
            this.cmdHDSDVNACCS1.Key = "cmdHDSDVNACCS";
            this.cmdHDSDVNACCS1.Name = "cmdHDSDVNACCS1";
            // 
            // Separator13
            // 
            this.Separator13.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator13.Key = "Separator";
            this.Separator13.Name = "Separator13";
            // 
            // cmdAutoUpdate1
            // 
            this.cmdAutoUpdate1.Image = ((System.Drawing.Image)(resources.GetObject("cmdAutoUpdate1.Image")));
            this.cmdAutoUpdate1.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate1.Name = "cmdAutoUpdate1";
            // 
            // Separator5
            // 
            this.Separator5.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator5.Key = "Separator";
            this.Separator5.Name = "Separator5";
            // 
            // cmdTeamview1
            // 
            this.cmdTeamview1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTeamview1.Image")));
            this.cmdTeamview1.Key = "cmdTeamview";
            this.cmdTeamview1.Name = "cmdTeamview1";
            // 
            // cmdTool1
            // 
            this.cmdTool1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTool1.Image")));
            this.cmdTool1.Key = "cmdTool";
            this.cmdTool1.Name = "cmdTool1";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdActivate1
            // 
            this.cmdActivate1.Image = ((System.Drawing.Image)(resources.GetObject("cmdActivate1.Image")));
            this.cmdActivate1.ImageIndex = 43;
            this.cmdActivate1.Key = "cmdActivate";
            this.cmdActivate1.Name = "cmdActivate1";
            // 
            // cmdAbout1
            // 
            this.cmdAbout1.Image = ((System.Drawing.Image)(resources.GetObject("cmdAbout1.Image")));
            this.cmdAbout1.Key = "cmdAbout";
            this.cmdAbout1.Name = "cmdAbout1";
            this.cmdAbout1.Text = "&Thông tin sản phẩm";
            // 
            // cmdThongBaoVNACCS1
            // 
            this.cmdThongBaoVNACCS1.Image = ((System.Drawing.Image)(resources.GetObject("cmdThongBaoVNACCS1.Image")));
            this.cmdThongBaoVNACCS1.Key = "cmdThongBaoVNACCS";
            this.cmdThongBaoVNACCS1.Name = "cmdThongBaoVNACCS1";
            // 
            // cmdHelp
            // 
            this.cmdHelp.Key = "cmdHelp";
            this.cmdHelp.Name = "cmdHelp";
            this.cmdHelp.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
            this.cmdHelp.Text = "Hướng dẫn sử dụng";
            // 
            // cmdAbout
            // 
            this.cmdAbout.Key = "cmdAbout";
            this.cmdAbout.Name = "cmdAbout";
            this.cmdAbout.Text = "Thông tin sản phẩm";
            // 
            // cmdNPLNhapTon
            // 
            this.cmdNPLNhapTon.Key = "cmdNPLNhapTon";
            this.cmdNPLNhapTon.Name = "cmdNPLNhapTon";
            this.cmdNPLNhapTon.Text = "NPL nhập tồn";
            // 
            // cmdDanhMuc
            // 
            this.cmdDanhMuc.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdGetCategoryOnline1,
            this.Separator6,
            this.cmdHaiQuan1,
            this.cmdNuoc1,
            this.cmdMaHS1,
            this.cmdNguyenTe1,
            this.cmdDVT1,
            this.cmdPTTT1,
            this.cmdPTVT1,
            this.cmdDKGH1,
            this.cmdCuaKhau1,
            this.cmdNhomCuaKhau1,
            this.cmdLoaiPhiChungTuThanhToan1,
            this.cmdBerth1,
            this.cmdCargo1,
            this.cmdCityUNLOCODE1,
            this.cmdCommon1,
            this.cmdContainerSize1,
            this.cmdCustomsSubSection1,
            this.cmdOGAUser1,
            this.cmdPackagesUnit1,
            this.cmdStations1,
            this.cmdTaxClassificationCode1,
            this.cmdReloadData1,
            this.cmdUpdateCategoryOnline1});
            this.cmdDanhMuc.Key = "cmdDanhMuc";
            this.cmdDanhMuc.Name = "cmdDanhMuc";
            this.cmdDanhMuc.Text = "DanhMuc";
            // 
            // cmdGetCategoryOnline1
            // 
            this.cmdGetCategoryOnline1.Image = ((System.Drawing.Image)(resources.GetObject("cmdGetCategoryOnline1.Image")));
            this.cmdGetCategoryOnline1.Key = "cmdGetCategoryOnline";
            this.cmdGetCategoryOnline1.Name = "cmdGetCategoryOnline1";
            this.cmdGetCategoryOnline1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // Separator6
            // 
            this.Separator6.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator6.Key = "Separator";
            this.Separator6.Name = "Separator6";
            // 
            // cmdHaiQuan1
            // 
            this.cmdHaiQuan1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHaiQuan1.Image")));
            this.cmdHaiQuan1.ImageIndex = 39;
            this.cmdHaiQuan1.Key = "cmdHaiQuan";
            this.cmdHaiQuan1.Name = "cmdHaiQuan1";
            // 
            // cmdNuoc1
            // 
            this.cmdNuoc1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNuoc1.Image")));
            this.cmdNuoc1.ImageIndex = 39;
            this.cmdNuoc1.Key = "cmdNuoc";
            this.cmdNuoc1.Name = "cmdNuoc1";
            // 
            // cmdMaHS1
            // 
            this.cmdMaHS1.Image = ((System.Drawing.Image)(resources.GetObject("cmdMaHS1.Image")));
            this.cmdMaHS1.ImageIndex = 39;
            this.cmdMaHS1.Key = "cmdMaHS";
            this.cmdMaHS1.Name = "cmdMaHS1";
            // 
            // cmdNguyenTe1
            // 
            this.cmdNguyenTe1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNguyenTe1.Image")));
            this.cmdNguyenTe1.ImageIndex = 39;
            this.cmdNguyenTe1.Key = "cmdNguyenTe";
            this.cmdNguyenTe1.Name = "cmdNguyenTe1";
            // 
            // cmdDVT1
            // 
            this.cmdDVT1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDVT1.Image")));
            this.cmdDVT1.ImageIndex = 39;
            this.cmdDVT1.Key = "cmdDVT";
            this.cmdDVT1.Name = "cmdDVT1";
            // 
            // cmdPTTT1
            // 
            this.cmdPTTT1.Image = ((System.Drawing.Image)(resources.GetObject("cmdPTTT1.Image")));
            this.cmdPTTT1.ImageIndex = 39;
            this.cmdPTTT1.Key = "cmdPTTT";
            this.cmdPTTT1.Name = "cmdPTTT1";
            // 
            // cmdPTVT1
            // 
            this.cmdPTVT1.Image = ((System.Drawing.Image)(resources.GetObject("cmdPTVT1.Image")));
            this.cmdPTVT1.ImageIndex = 39;
            this.cmdPTVT1.Key = "cmdPTVT";
            this.cmdPTVT1.Name = "cmdPTVT1";
            // 
            // cmdDKGH1
            // 
            this.cmdDKGH1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDKGH1.Image")));
            this.cmdDKGH1.ImageIndex = 39;
            this.cmdDKGH1.Key = "cmdDKGH";
            this.cmdDKGH1.Name = "cmdDKGH1";
            // 
            // cmdCuaKhau1
            // 
            this.cmdCuaKhau1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCuaKhau1.Image")));
            this.cmdCuaKhau1.ImageIndex = 39;
            this.cmdCuaKhau1.Key = "cmdCuaKhau";
            this.cmdCuaKhau1.Name = "cmdCuaKhau1";
            // 
            // cmdNhomCuaKhau1
            // 
            this.cmdNhomCuaKhau1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhomCuaKhau1.Image")));
            this.cmdNhomCuaKhau1.ImageIndex = 39;
            this.cmdNhomCuaKhau1.Key = "cmdNhomCuaKhau";
            this.cmdNhomCuaKhau1.Name = "cmdNhomCuaKhau1";
            // 
            // cmdLoaiPhiChungTuThanhToan1
            // 
            this.cmdLoaiPhiChungTuThanhToan1.Image = ((System.Drawing.Image)(resources.GetObject("cmdLoaiPhiChungTuThanhToan1.Image")));
            this.cmdLoaiPhiChungTuThanhToan1.ImageIndex = 39;
            this.cmdLoaiPhiChungTuThanhToan1.Key = "cmdLoaiPhiChungTuThanhToan";
            this.cmdLoaiPhiChungTuThanhToan1.Name = "cmdLoaiPhiChungTuThanhToan1";
            // 
            // cmdBerth1
            // 
            this.cmdBerth1.Key = "cmdBerth";
            this.cmdBerth1.Name = "cmdBerth1";
            // 
            // cmdCargo1
            // 
            this.cmdCargo1.Key = "cmdCargo";
            this.cmdCargo1.Name = "cmdCargo1";
            // 
            // cmdCityUNLOCODE1
            // 
            this.cmdCityUNLOCODE1.Key = "cmdCityUNLOCODE";
            this.cmdCityUNLOCODE1.Name = "cmdCityUNLOCODE1";
            // 
            // cmdCommon1
            // 
            this.cmdCommon1.Key = "cmdCommon";
            this.cmdCommon1.Name = "cmdCommon1";
            // 
            // cmdContainerSize1
            // 
            this.cmdContainerSize1.Key = "cmdContainerSize";
            this.cmdContainerSize1.Name = "cmdContainerSize1";
            // 
            // cmdCustomsSubSection1
            // 
            this.cmdCustomsSubSection1.Key = "cmdCustomsSubSection";
            this.cmdCustomsSubSection1.Name = "cmdCustomsSubSection1";
            // 
            // cmdOGAUser1
            // 
            this.cmdOGAUser1.Key = "cmdOGAUser";
            this.cmdOGAUser1.Name = "cmdOGAUser1";
            // 
            // cmdPackagesUnit1
            // 
            this.cmdPackagesUnit1.Key = "cmdPackagesUnit";
            this.cmdPackagesUnit1.Name = "cmdPackagesUnit1";
            // 
            // cmdStations1
            // 
            this.cmdStations1.Key = "cmdStations";
            this.cmdStations1.Name = "cmdStations1";
            // 
            // cmdTaxClassificationCode1
            // 
            this.cmdTaxClassificationCode1.Key = "cmdTaxClassificationCode";
            this.cmdTaxClassificationCode1.Name = "cmdTaxClassificationCode1";
            // 
            // cmdReloadData1
            // 
            this.cmdReloadData1.Key = "cmdReloadData";
            this.cmdReloadData1.Name = "cmdReloadData1";
            // 
            // cmdUpdateCategoryOnline1
            // 
            this.cmdUpdateCategoryOnline1.Key = "cmdUpdateCategoryOnline";
            this.cmdUpdateCategoryOnline1.Name = "cmdUpdateCategoryOnline1";
            // 
            // cmdMaHS
            // 
            this.cmdMaHS.Key = "cmdMaHS";
            this.cmdMaHS.Name = "cmdMaHS";
            this.cmdMaHS.Text = "Mã HS";
            // 
            // cmdNuoc
            // 
            this.cmdNuoc.Key = "cmdNuoc";
            this.cmdNuoc.Name = "cmdNuoc";
            this.cmdNuoc.Text = "Nước";
            // 
            // cmdHaiQuan
            // 
            this.cmdHaiQuan.Key = "cmdHaiQuan";
            this.cmdHaiQuan.Name = "cmdHaiQuan";
            this.cmdHaiQuan.Text = "Đơn vị Hải quan";
            // 
            // cmdNguyenTe
            // 
            this.cmdNguyenTe.Key = "cmdNguyenTe";
            this.cmdNguyenTe.Name = "cmdNguyenTe";
            this.cmdNguyenTe.Text = "Nguyên tệ";
            // 
            // cmdDVT
            // 
            this.cmdDVT.Key = "cmdDVT";
            this.cmdDVT.Name = "cmdDVT";
            this.cmdDVT.Text = "Đơn vị tính";
            // 
            // cmdPTTT
            // 
            this.cmdPTTT.Key = "cmdPTTT";
            this.cmdPTTT.Name = "cmdPTTT";
            this.cmdPTTT.Text = "Phương thức thanh toán";
            // 
            // cmdPTVT
            // 
            this.cmdPTVT.Key = "cmdPTVT";
            this.cmdPTVT.Name = "cmdPTVT";
            this.cmdPTVT.Text = "Phương thức vận tải";
            // 
            // cmdDKGH
            // 
            this.cmdDKGH.Key = "cmdDKGH";
            this.cmdDKGH.Name = "cmdDKGH";
            this.cmdDKGH.Text = "Điều kiện giao hàng";
            // 
            // cmdCuaKhau
            // 
            this.cmdCuaKhau.Key = "cmdCuaKhau";
            this.cmdCuaKhau.Name = "cmdCuaKhau";
            this.cmdCuaKhau.Text = "Cửa khẩu";
            // 
            // cmdBackUp
            // 
            this.cmdBackUp.Image = ((System.Drawing.Image)(resources.GetObject("cmdBackUp.Image")));
            this.cmdBackUp.Key = "cmdBackUp";
            this.cmdBackUp.Name = "cmdBackUp";
            this.cmdBackUp.Text = "Sao lưu dữ liệu";
            // 
            // cmdRestore
            // 
            this.cmdRestore.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdRestore.Icon")));
            this.cmdRestore.Key = "cmdRestore";
            this.cmdRestore.Name = "cmdRestore";
            this.cmdRestore.Text = "Phục hồi dữ liệu";
            // 
            // ThongSoKetNoi
            // 
            this.ThongSoKetNoi.Key = "ThongSoKetNoi";
            this.ThongSoKetNoi.Name = "ThongSoKetNoi";
            this.ThongSoKetNoi.Text = "Thiết lập thông số kết nối";
            // 
            // TLThongTinDNHQ
            // 
            this.TLThongTinDNHQ.Key = "TLThongTinDNHQ";
            this.TLThongTinDNHQ.Name = "TLThongTinDNHQ";
            this.TLThongTinDNHQ.Text = "Thiết lập thông tin doanh nghiệp và hải quan";
            // 
            // cmdThietLapIn
            // 
            this.cmdThietLapIn.Key = "cmdThietLapIn";
            this.cmdThietLapIn.Name = "cmdThietLapIn";
            this.cmdThietLapIn.Text = "Thiết lập thông số in báo cáo";
            // 
            // cmdExportExccel
            // 
            this.cmdExportExccel.Key = "cmdExportExccel";
            this.cmdExportExccel.Name = "cmdExportExccel";
            this.cmdExportExccel.Text = "Xuất dữ liệu ra file Excel";
            // 
            // cmdImportExcel
            // 
            this.cmdImportExcel.Key = "cmdImportExcel";
            this.cmdImportExcel.Name = "cmdImportExcel";
            this.cmdImportExcel.Text = "Nhập dữ liêu từ file Excel";
            // 
            // cmdDongBoPhongKhai
            // 
            this.cmdDongBoPhongKhai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportExcel1,
            this.cmdExportExccel1});
            this.cmdDongBoPhongKhai.Key = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai.Name = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai.Text = "Đồng bộ dữ liệu với phòng khai";
            // 
            // cmdImportExcel1
            // 
            this.cmdImportExcel1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportExcel1.Icon")));
            this.cmdImportExcel1.Key = "cmdImportExcel";
            this.cmdImportExcel1.Name = "cmdImportExcel1";
            // 
            // cmdExportExccel1
            // 
            this.cmdExportExccel1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdExportExccel1.Icon")));
            this.cmdExportExccel1.Key = "cmdExportExccel";
            this.cmdExportExccel1.Name = "cmdExportExccel1";
            // 
            // cmdCauHinh
            // 
            this.cmdCauHinh.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ThongSoKetNoi1,
            this.TLThongTinDNHQ1,
            this.cmdCauHinhToKhai1,
            this.cmdThietLapIn1,
            this.cmdChuKySo1,
            this.cmdTimer1,
            this.cmdConfigReadExcel1});
            this.cmdCauHinh.Key = "cmdCauHinh";
            this.cmdCauHinh.Name = "cmdCauHinh";
            this.cmdCauHinh.Text = "Cấu hình hệ thống";
            // 
            // ThongSoKetNoi1
            // 
            this.ThongSoKetNoi1.Image = ((System.Drawing.Image)(resources.GetObject("ThongSoKetNoi1.Image")));
            this.ThongSoKetNoi1.Key = "ThongSoKetNoi";
            this.ThongSoKetNoi1.Name = "ThongSoKetNoi1";
            this.ThongSoKetNoi1.Text = "Cấu hình thông số kết nối";
            // 
            // TLThongTinDNHQ1
            // 
            this.TLThongTinDNHQ1.Image = ((System.Drawing.Image)(resources.GetObject("TLThongTinDNHQ1.Image")));
            this.TLThongTinDNHQ1.Key = "TLThongTinDNHQ";
            this.TLThongTinDNHQ1.Name = "TLThongTinDNHQ1";
            this.TLThongTinDNHQ1.Text = "Cấu hình thông tin doanh nghiệp và hải quan";
            // 
            // cmdCauHinhToKhai1
            // 
            this.cmdCauHinhToKhai1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCauHinhToKhai1.Image")));
            this.cmdCauHinhToKhai1.Key = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai1.Name = "cmdCauHinhToKhai1";
            // 
            // cmdThietLapIn1
            // 
            this.cmdThietLapIn1.Image = ((System.Drawing.Image)(resources.GetObject("cmdThietLapIn1.Image")));
            this.cmdThietLapIn1.Key = "cmdThietLapIn";
            this.cmdThietLapIn1.Name = "cmdThietLapIn1";
            this.cmdThietLapIn1.Text = "Cấu hình thông số in báo cáo";
            // 
            // cmdChuKySo1
            // 
            this.cmdChuKySo1.Image = ((System.Drawing.Image)(resources.GetObject("cmdChuKySo1.Image")));
            this.cmdChuKySo1.Key = "cmdChuKySo";
            this.cmdChuKySo1.Name = "cmdChuKySo1";
            // 
            // cmdTimer1
            // 
            this.cmdTimer1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTimer1.Image")));
            this.cmdTimer1.Key = "cmdTimer";
            this.cmdTimer1.Name = "cmdTimer1";
            // 
            // cmdConfigReadExcel1
            // 
            this.cmdConfigReadExcel1.Key = "cmdConfigReadExcel";
            this.cmdConfigReadExcel1.Name = "cmdConfigReadExcel1";
            // 
            // cmdCauHinhToKhai
            // 
            this.cmdCauHinhToKhai.Key = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai.Name = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai.Text = "Cấu hình tham số mặc định của tờ khai";
            // 
            // QuanTri
            // 
            this.QuanTri.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.QuanLyNguoiDung1,
            this.QuanLyNhom1});
            this.QuanTri.Key = "QuanTri";
            this.QuanTri.Name = "QuanTri";
            this.QuanTri.Text = "Quản trị";
            // 
            // QuanLyNguoiDung1
            // 
            this.QuanLyNguoiDung1.Image = ((System.Drawing.Image)(resources.GetObject("QuanLyNguoiDung1.Image")));
            this.QuanLyNguoiDung1.Key = "QuanLyNguoiDung";
            this.QuanLyNguoiDung1.Name = "QuanLyNguoiDung1";
            // 
            // QuanLyNhom1
            // 
            this.QuanLyNhom1.Image = ((System.Drawing.Image)(resources.GetObject("QuanLyNhom1.Image")));
            this.QuanLyNhom1.Key = "QuanLyNhom";
            this.QuanLyNhom1.Name = "QuanLyNhom1";
            // 
            // QuanLyNguoiDung
            // 
            this.QuanLyNguoiDung.Key = "QuanLyNguoiDung";
            this.QuanLyNguoiDung.Name = "QuanLyNguoiDung";
            this.QuanLyNguoiDung.Text = "Quản lý người dùng";
            // 
            // QuanLyNhom
            // 
            this.QuanLyNhom.Key = "QuanLyNhom";
            this.QuanLyNhom.Name = "QuanLyNhom";
            this.QuanLyNhom.Text = "Quản lý nhóm người dùng";
            // 
            // LoginUser
            // 
            this.LoginUser.Key = "LoginUser";
            this.LoginUser.Name = "LoginUser";
            this.LoginUser.Text = "Đăng nhập người dùng khác";
            // 
            // cmdChangePass
            // 
            this.cmdChangePass.Key = "cmdChangePass";
            this.cmdChangePass.Name = "cmdChangePass";
            this.cmdChangePass.Text = "Đổi mật khẩu";
            // 
            // cmdDoiMatKhauHQ
            // 
            this.cmdDoiMatKhauHQ.Key = "cmdDoiMatKhauHQ";
            this.cmdDoiMatKhauHQ.Name = "cmdDoiMatKhauHQ";
            this.cmdDoiMatKhauHQ.Text = "Đổi mật khẩu kết nối với hải quan";
            // 
            // cmdCuaSo
            // 
            this.cmdCuaSo.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCloseAll1,
            this.cmdCloseAllButThis1});
            this.cmdCuaSo.Key = "cmdCuaSo";
            this.cmdCuaSo.Name = "cmdCuaSo";
            this.cmdCuaSo.Text = "Cửa sổ";
            // 
            // cmdCloseAll1
            // 
            this.cmdCloseAll1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCloseAll1.Image")));
            this.cmdCloseAll1.Key = "cmdCloseAll";
            this.cmdCloseAll1.Name = "cmdCloseAll1";
            // 
            // cmdCloseAllButThis1
            // 
            this.cmdCloseAllButThis1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCloseAllButThis1.Image")));
            this.cmdCloseAllButThis1.Key = "cmdCloseAllButMe";
            this.cmdCloseAllButThis1.Name = "cmdCloseAllButThis1";
            // 
            // cmdCloseAll
            // 
            this.cmdCloseAll.Key = "cmdCloseAll";
            this.cmdCloseAll.Name = "cmdCloseAll";
            this.cmdCloseAll.Text = "Đóng hết các cửa sổ";
            // 
            // cmdCloseAllButMe
            // 
            this.cmdCloseAllButMe.Key = "cmdCloseAllButMe";
            this.cmdCloseAllButMe.Name = "cmdCloseAllButMe";
            this.cmdCloseAllButMe.Text = "Đóng các cửa sổ khác";
            // 
            // cmdAutoUpdate
            // 
            this.cmdAutoUpdate.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate.Name = "cmdAutoUpdate";
            this.cmdAutoUpdate.Text = "&Cập nhập chương trình";
            // 
            // cmdNhapXML
            // 
            this.cmdNhapXML.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNPL1,
            this.cmdSP1,
            this.cmdDM1,
            this.cmdTK1});
            this.cmdNhapXML.Key = "cmdNhapXML";
            this.cmdNhapXML.Name = "cmdNhapXML";
            this.cmdNhapXML.Text = "Nhập dữ liệu từ đại lý";
            // 
            // cmdNPL1
            // 
            this.cmdNPL1.ImageIndex = 51;
            this.cmdNPL1.Key = "cmdNPL";
            this.cmdNPL1.Name = "cmdNPL1";
            // 
            // cmdSP1
            // 
            this.cmdSP1.ImageIndex = 52;
            this.cmdSP1.Key = "cmdSP";
            this.cmdSP1.Name = "cmdSP1";
            // 
            // cmdDM1
            // 
            this.cmdDM1.ImageIndex = 49;
            this.cmdDM1.Key = "cmdDM";
            this.cmdDM1.Name = "cmdDM1";
            // 
            // cmdTK1
            // 
            this.cmdTK1.ImageIndex = 53;
            this.cmdTK1.Key = "cmdTK";
            this.cmdTK1.Name = "cmdTK1";
            // 
            // cmdNPL
            // 
            this.cmdNPL.Key = "cmdNPL";
            this.cmdNPL.Name = "cmdNPL";
            this.cmdNPL.Text = "Nguyên phụ liệu";
            // 
            // cmdSP
            // 
            this.cmdSP.Key = "cmdSP";
            this.cmdSP.Name = "cmdSP";
            this.cmdSP.Text = "Sản phẩm";
            // 
            // cmdDM
            // 
            this.cmdDM.Key = "cmdDM";
            this.cmdDM.Name = "cmdDM";
            this.cmdDM.Text = "Định mức";
            // 
            // cmdTK
            // 
            this.cmdTK.Key = "cmdTK";
            this.cmdTK.Name = "cmdTK";
            this.cmdTK.Text = "Tờ khai";
            // 
            // cmdXuatDuLieu
            // 
            this.cmdXuatDuLieu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHUNGNPL1,
            this.cmdHUNGSP1,
            this.cmdHUNGDM1,
            this.cmdHUNGTK1});
            this.cmdXuatDuLieu.Key = "cmdXuatDuLieu";
            this.cmdXuatDuLieu.Name = "cmdXuatDuLieu";
            this.cmdXuatDuLieu.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // cmdHUNGNPL1
            // 
            this.cmdHUNGNPL1.ImageIndex = 51;
            this.cmdHUNGNPL1.Key = "cmdHUNGNPL";
            this.cmdHUNGNPL1.Name = "cmdHUNGNPL1";
            // 
            // cmdHUNGSP1
            // 
            this.cmdHUNGSP1.ImageIndex = 52;
            this.cmdHUNGSP1.Key = "cmdHUNGSP";
            this.cmdHUNGSP1.Name = "cmdHUNGSP1";
            // 
            // cmdHUNGDM1
            // 
            this.cmdHUNGDM1.ImageIndex = 49;
            this.cmdHUNGDM1.Key = "cmdHUNGDM";
            this.cmdHUNGDM1.Name = "cmdHUNGDM1";
            // 
            // cmdHUNGTK1
            // 
            this.cmdHUNGTK1.ImageIndex = 53;
            this.cmdHUNGTK1.Key = "cmdHUNGTK";
            this.cmdHUNGTK1.Name = "cmdHUNGTK1";
            // 
            // cmdXuatNPL
            // 
            this.cmdXuatNPL.Key = "cmdXuatNPL";
            this.cmdXuatNPL.Name = "cmdXuatNPL";
            this.cmdXuatNPL.Text = "Nguyên phụ liệu";
            // 
            // cmdXuatDinhMuc
            // 
            this.cmdXuatDinhMuc.Key = "cmdXuatDinhMuc";
            this.cmdXuatDinhMuc.Name = "cmdXuatDinhMuc";
            this.cmdXuatDinhMuc.Text = "Định mức";
            // 
            // cmdXuatSanPham
            // 
            this.cmdXuatSanPham.Key = "cmdXuatSanPham";
            this.cmdXuatSanPham.Name = "cmdXuatSanPham";
            this.cmdXuatSanPham.Text = "Sản phẩm";
            // 
            // cmdXuatToKhai
            // 
            this.cmdXuatToKhai.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai.Name = "cmdXuatToKhai";
            this.cmdXuatToKhai.Text = "Tờ khai";
            // 
            // cmdVN
            // 
            this.cmdVN.Key = "cmdVN";
            this.cmdVN.Name = "cmdVN";
            this.cmdVN.Text = "English";
            // 
            // cmdEL
            // 
            this.cmdEL.Key = "cmdEL";
            this.cmdEL.Name = "cmdEL";
            this.cmdEL.Text = "English";
            // 
            // cmdHUNGNPL
            // 
            this.cmdHUNGNPL.Key = "cmdHUNGNPL";
            this.cmdHUNGNPL.Name = "cmdHUNGNPL";
            this.cmdHUNGNPL.Text = "Nguyên phụ liệu";
            // 
            // cmdHUNGSP
            // 
            this.cmdHUNGSP.Key = "cmdHUNGSP";
            this.cmdHUNGSP.Name = "cmdHUNGSP";
            this.cmdHUNGSP.Text = "Sản phẩm";
            // 
            // cmdHUNGDM
            // 
            this.cmdHUNGDM.Key = "cmdHUNGDM";
            this.cmdHUNGDM.Name = "cmdHUNGDM";
            this.cmdHUNGDM.Text = "Định mức";
            // 
            // cmdHUNGTK
            // 
            this.cmdHUNGTK.Key = "cmdHUNGTK";
            this.cmdHUNGTK.Name = "cmdHUNGTK";
            this.cmdHUNGTK.Text = "Tờ khai";
            // 
            // cmdActivate
            // 
            this.cmdActivate.Key = "cmdActivate";
            this.cmdActivate.Name = "cmdActivate";
            this.cmdActivate.Shortcut = System.Windows.Forms.Shortcut.CtrlF10;
            this.cmdActivate.Text = "Kích hoạt phần mềm";
            // 
            // cmdXuatDuLieuDN
            // 
            this.cmdXuatDuLieuDN.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdXuatNPLDN1,
            this.cmdXuatSPDN1,
            this.cmdXuatDMDN1,
            this.cmdXuatTKDN1});
            this.cmdXuatDuLieuDN.Key = "cmdXuatDuLieuDN";
            this.cmdXuatDuLieuDN.Name = "cmdXuatDuLieuDN";
            this.cmdXuatDuLieuDN.Text = "Xuất dữ liệu cho doanh nghiệp";
            // 
            // cmdXuatNPLDN1
            // 
            this.cmdXuatNPLDN1.ImageIndex = 51;
            this.cmdXuatNPLDN1.Key = "cmdXuatNPLDN";
            this.cmdXuatNPLDN1.Name = "cmdXuatNPLDN1";
            this.cmdXuatNPLDN1.Text = "Nguyên phụ liệu";
            // 
            // cmdXuatSPDN1
            // 
            this.cmdXuatSPDN1.ImageIndex = 52;
            this.cmdXuatSPDN1.Key = "cmdXuatSPDN";
            this.cmdXuatSPDN1.Name = "cmdXuatSPDN1";
            this.cmdXuatSPDN1.Text = "Sản phẩm";
            // 
            // cmdXuatDMDN1
            // 
            this.cmdXuatDMDN1.ImageIndex = 49;
            this.cmdXuatDMDN1.Key = "cmdXuatDMDN";
            this.cmdXuatDMDN1.Name = "cmdXuatDMDN1";
            this.cmdXuatDMDN1.Text = "Định mức";
            // 
            // cmdXuatTKDN1
            // 
            this.cmdXuatTKDN1.ImageIndex = 53;
            this.cmdXuatTKDN1.Key = "cmdXuatTKDN";
            this.cmdXuatTKDN1.Name = "cmdXuatTKDN1";
            this.cmdXuatTKDN1.Text = "Tờ khai";
            // 
            // cmdXuatNPLDN
            // 
            this.cmdXuatNPLDN.Key = "cmdXuatNPLDN";
            this.cmdXuatNPLDN.Name = "cmdXuatNPLDN";
            this.cmdXuatNPLDN.Text = "NPL";
            // 
            // cmdXuatSPDN
            // 
            this.cmdXuatSPDN.Key = "cmdXuatSPDN";
            this.cmdXuatSPDN.Name = "cmdXuatSPDN";
            this.cmdXuatSPDN.Text = "SP";
            // 
            // cmXuat
            // 
            this.cmXuat.Key = "cmXuat";
            this.cmXuat.Name = "cmXuat";
            this.cmXuat.Text = "Xuất dữ liệu cho doanh nghiệp";
            // 
            // cmdXuatDMDN
            // 
            this.cmdXuatDMDN.Key = "cmdXuatDMDN";
            this.cmdXuatDMDN.Name = "cmdXuatDMDN";
            this.cmdXuatDMDN.Text = "ĐM";
            // 
            // cmdXuatTKDN
            // 
            this.cmdXuatTKDN.Key = "cmdXuatTKDN";
            this.cmdXuatTKDN.Name = "cmdXuatTKDN";
            this.cmdXuatTKDN.Text = "TK";
            // 
            // cmdNhapDuLieu
            // 
            this.cmdNhapDuLieu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNhapNPL1,
            this.cmdNhapSanPham1,
            this.cmdNhapDinhMuc1,
            this.cmdNhapToKhai1});
            this.cmdNhapDuLieu.Key = "cmdNhapDuLieu";
            this.cmdNhapDuLieu.Name = "cmdNhapDuLieu";
            this.cmdNhapDuLieu.Text = "Nhập dữ liệu từ doanh nghiệp";
            // 
            // cmdNhapNPL1
            // 
            this.cmdNhapNPL1.ImageIndex = 51;
            this.cmdNhapNPL1.Key = "cmdNhapNPL";
            this.cmdNhapNPL1.Name = "cmdNhapNPL1";
            // 
            // cmdNhapSanPham1
            // 
            this.cmdNhapSanPham1.ImageIndex = 52;
            this.cmdNhapSanPham1.Key = "cmdNhapSanPham";
            this.cmdNhapSanPham1.Name = "cmdNhapSanPham1";
            // 
            // cmdNhapDinhMuc1
            // 
            this.cmdNhapDinhMuc1.ImageIndex = 49;
            this.cmdNhapDinhMuc1.Key = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc1.Name = "cmdNhapDinhMuc1";
            // 
            // cmdNhapToKhai1
            // 
            this.cmdNhapToKhai1.ImageIndex = 53;
            this.cmdNhapToKhai1.Key = "cmdNhapToKhai";
            this.cmdNhapToKhai1.Name = "cmdNhapToKhai1";
            // 
            // cmdNhapNPL
            // 
            this.cmdNhapNPL.Key = "cmdNhapNPL";
            this.cmdNhapNPL.Name = "cmdNhapNPL";
            this.cmdNhapNPL.Text = "Nguyên phụ liệu";
            // 
            // cmdNhapSanPham
            // 
            this.cmdNhapSanPham.Key = "cmdNhapSanPham";
            this.cmdNhapSanPham.Name = "cmdNhapSanPham";
            this.cmdNhapSanPham.Text = "Sản phẩm";
            // 
            // cmdNhapDinhMuc
            // 
            this.cmdNhapDinhMuc.Key = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc.Name = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc.Text = "Định mức";
            // 
            // cmdNhapToKhai
            // 
            this.cmdNhapToKhai.Key = "cmdNhapToKhai";
            this.cmdNhapToKhai.Name = "cmdNhapToKhai";
            this.cmdNhapToKhai.Text = "Tờ khai";
            // 
            // QuanlyMess
            // 
            this.QuanlyMess.Key = "QuanlyMess";
            this.QuanlyMess.Name = "QuanlyMess";
            this.QuanlyMess.Text = "Quản lý Message khai báo";
            // 
            // cmdConfig
            // 
            this.cmdConfig.Key = "cmdConfig";
            this.cmdConfig.Name = "cmdConfig";
            this.cmdConfig.Text = "Thiết lập cấu hình Doanh nghiệp";
            // 
            // mnuQuerySQL
            // 
            this.mnuQuerySQL.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.mnuFormSQL1});
            this.mnuQuerySQL.Key = "mnuQuerySQL";
            this.mnuQuerySQL.Name = "mnuQuerySQL";
            this.mnuQuerySQL.Text = "Thực hiện truy vấn SQL";
            // 
            // mnuFormSQL1
            // 
            this.mnuFormSQL1.Image = ((System.Drawing.Image)(resources.GetObject("mnuFormSQL1.Image")));
            this.mnuFormSQL1.ImageIndex = 42;
            this.mnuFormSQL1.Key = "mnuFormSQL";
            this.mnuFormSQL1.Name = "mnuFormSQL1";
            // 
            // TraCuuMaHS
            // 
            this.TraCuuMaHS.ImageIndex = 1;
            this.TraCuuMaHS.Key = "TraCuuMaHS";
            this.TraCuuMaHS.Name = "TraCuuMaHS";
            this.TraCuuMaHS.Text = "Tra cứu biểu thuế (Mã HS)";
            // 
            // cmdNhomCuaKhau
            // 
            this.cmdNhomCuaKhau.Key = "cmdNhomCuaKhau";
            this.cmdNhomCuaKhau.Name = "cmdNhomCuaKhau";
            this.cmdNhomCuaKhau.Text = "Nhóm cửa khẩu";
            // 
            // cmdLog
            // 
            this.cmdLog.Key = "cmdLog";
            this.cmdLog.Name = "cmdLog";
            this.cmdLog.Text = "Nhật ký chương trình";
            // 
            // cmdLoaiPhiChungTuThanhToan
            // 
            this.cmdLoaiPhiChungTuThanhToan.Key = "cmdLoaiPhiChungTuThanhToan";
            this.cmdLoaiPhiChungTuThanhToan.Name = "cmdLoaiPhiChungTuThanhToan";
            this.cmdLoaiPhiChungTuThanhToan.SelectedImageIndex = 39;
            this.cmdLoaiPhiChungTuThanhToan.Text = "Loại phí chứng từ thanh toán";
            // 
            // cmdDataVersion
            // 
            this.cmdDataVersion.Key = "cmdDataVersion";
            this.cmdDataVersion.Name = "cmdDataVersion";
            this.cmdDataVersion.Text = "[?]";
            // 
            // cmdImportXml
            // 
            this.cmdImportXml.ImageIndex = 8;
            this.cmdImportXml.Key = "cmdImportXml";
            this.cmdImportXml.Name = "cmdImportXml";
            this.cmdImportXml.Text = "Nạp dữ liệu từ xml";
            // 
            // cmdChuKySo
            // 
            this.cmdChuKySo.Key = "cmdChuKySo";
            this.cmdChuKySo.Name = "cmdChuKySo";
            this.cmdChuKySo.Text = "Cấu hình chữ ký số";
            // 
            // cmdTimer
            // 
            this.cmdTimer.Key = "cmdTimer";
            this.cmdTimer.Name = "cmdTimer";
            this.cmdTimer.Text = "Cấu hình thời gian";
            // 
            // mnuFormSQL
            // 
            this.mnuFormSQL.ImageIndex = 38;
            this.mnuFormSQL.Key = "mnuFormSQL";
            this.mnuFormSQL.Name = "mnuFormSQL";
            this.mnuFormSQL.Text = "Form SQL";
            // 
            // mnuMiniSQL
            // 
            this.mnuMiniSQL.ImageIndex = 38;
            this.mnuMiniSQL.Key = "mnuMiniSQL";
            this.mnuMiniSQL.Name = "mnuMiniSQL";
            this.mnuMiniSQL.Text = "MiniSQL";
            // 
            // cmdNhapXuat
            // 
            this.cmdNhapXuat.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportXml1,
            this.cmdNhapXML1,
            this.cmdImportExcel2,
            this.cmdNhapDuLieu1,
            this.Separator8,
            this.cmdXuatDuLieu1,
            this.cmdXuatDuLieuDN1});
            this.cmdNhapXuat.ImageIndex = 45;
            this.cmdNhapXuat.Key = "cmdNhapXuat";
            this.cmdNhapXuat.Name = "cmdNhapXuat";
            this.cmdNhapXuat.Text = "Nhập xuất dữ liệu";
            // 
            // cmdImportXml1
            // 
            this.cmdImportXml1.Image = ((System.Drawing.Image)(resources.GetObject("cmdImportXml1.Image")));
            this.cmdImportXml1.ImageIndex = 48;
            this.cmdImportXml1.Key = "cmdImportXml";
            this.cmdImportXml1.Name = "cmdImportXml1";
            // 
            // cmdNhapXML1
            // 
            this.cmdNhapXML1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhapXML1.Image")));
            this.cmdNhapXML1.ImageIndex = 48;
            this.cmdNhapXML1.Key = "cmdNhapXML";
            this.cmdNhapXML1.Name = "cmdNhapXML1";
            // 
            // cmdImportExcel2
            // 
            this.cmdImportExcel2.Image = ((System.Drawing.Image)(resources.GetObject("cmdImportExcel2.Image")));
            this.cmdImportExcel2.ImageIndex = 48;
            this.cmdImportExcel2.Key = "cmdImportExcel";
            this.cmdImportExcel2.Name = "cmdImportExcel2";
            // 
            // cmdNhapDuLieu1
            // 
            this.cmdNhapDuLieu1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhapDuLieu1.Image")));
            this.cmdNhapDuLieu1.ImageIndex = 48;
            this.cmdNhapDuLieu1.Key = "cmdNhapDuLieu";
            this.cmdNhapDuLieu1.Name = "cmdNhapDuLieu1";
            // 
            // Separator8
            // 
            this.Separator8.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator8.Key = "Separator";
            this.Separator8.Name = "Separator8";
            // 
            // cmdXuatDuLieu1
            // 
            this.cmdXuatDuLieu1.Image = ((System.Drawing.Image)(resources.GetObject("cmdXuatDuLieu1.Image")));
            this.cmdXuatDuLieu1.ImageIndex = 45;
            this.cmdXuatDuLieu1.Key = "cmdXuatDuLieu";
            this.cmdXuatDuLieu1.Name = "cmdXuatDuLieu1";
            // 
            // cmdXuatDuLieuDN1
            // 
            this.cmdXuatDuLieuDN1.Image = ((System.Drawing.Image)(resources.GetObject("cmdXuatDuLieuDN1.Image")));
            this.cmdXuatDuLieuDN1.ImageIndex = 45;
            this.cmdXuatDuLieuDN1.Key = "cmdXuatDuLieuDN";
            this.cmdXuatDuLieuDN1.Name = "cmdXuatDuLieuDN1";
            // 
            // cmdBieuThue
            // 
            this.cmdBieuThue.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.TraCuuMaHS1,
            this.Separator7,
            this.cmdTraCuuXNKOnline1,
            this.cmdTraCuuNoThueOnline1,
            this.cmdTraCuuVanBanOnline1,
            this.cmdTuVanHQOnline1,
            this.cmdBieuThueXNK20181});
            this.cmdBieuThue.Key = "cmdBieuThue";
            this.cmdBieuThue.Name = "cmdBieuThue";
            this.cmdBieuThue.Text = "&Biểu thuế (Mã HS)";
            // 
            // TraCuuMaHS1
            // 
            this.TraCuuMaHS1.Image = ((System.Drawing.Image)(resources.GetObject("TraCuuMaHS1.Image")));
            this.TraCuuMaHS1.Key = "TraCuuMaHS";
            this.TraCuuMaHS1.Name = "TraCuuMaHS1";
            // 
            // Separator7
            // 
            this.Separator7.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator7.Key = "Separator";
            this.Separator7.Name = "Separator7";
            // 
            // cmdTraCuuXNKOnline1
            // 
            this.cmdTraCuuXNKOnline1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTraCuuXNKOnline1.Image")));
            this.cmdTraCuuXNKOnline1.Key = "cmdTraCuuXNKOnline";
            this.cmdTraCuuXNKOnline1.Name = "cmdTraCuuXNKOnline1";
            // 
            // cmdTraCuuNoThueOnline1
            // 
            this.cmdTraCuuNoThueOnline1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTraCuuNoThueOnline1.Image")));
            this.cmdTraCuuNoThueOnline1.Key = "cmdTraCuuNoThueOnline";
            this.cmdTraCuuNoThueOnline1.Name = "cmdTraCuuNoThueOnline1";
            // 
            // cmdTraCuuVanBanOnline1
            // 
            this.cmdTraCuuVanBanOnline1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTraCuuVanBanOnline1.Image")));
            this.cmdTraCuuVanBanOnline1.Key = "cmdTraCuuVanBanOnline";
            this.cmdTraCuuVanBanOnline1.Name = "cmdTraCuuVanBanOnline1";
            // 
            // cmdTuVanHQOnline1
            // 
            this.cmdTuVanHQOnline1.Image = ((System.Drawing.Image)(resources.GetObject("cmdTuVanHQOnline1.Image")));
            this.cmdTuVanHQOnline1.Key = "cmdTuVanHQOnline";
            this.cmdTuVanHQOnline1.Name = "cmdTuVanHQOnline1";
            // 
            // cmdBieuThueXNK20181
            // 
            this.cmdBieuThueXNK20181.Key = "cmdBieuThueXNK2018";
            this.cmdBieuThueXNK20181.Name = "cmdBieuThueXNK20181";
            // 
            // cmdTraCuuXNKOnline
            // 
            this.cmdTraCuuXNKOnline.ImageIndex = 47;
            this.cmdTraCuuXNKOnline.Key = "cmdTraCuuXNKOnline";
            this.cmdTraCuuXNKOnline.Name = "cmdTraCuuXNKOnline";
            this.cmdTraCuuXNKOnline.Text = "Tra cứu biểu thuế Xuất nhập khẩu trực tuyến";
            // 
            // cmdTraCuuNoThueOnline
            // 
            this.cmdTraCuuNoThueOnline.ImageIndex = 47;
            this.cmdTraCuuNoThueOnline.Key = "cmdTraCuuNoThueOnline";
            this.cmdTraCuuNoThueOnline.Name = "cmdTraCuuNoThueOnline";
            this.cmdTraCuuNoThueOnline.Text = "Tra cứu nợ thuế trực tuyến";
            // 
            // cmdTraCuuVanBanOnline
            // 
            this.cmdTraCuuVanBanOnline.ImageIndex = 47;
            this.cmdTraCuuVanBanOnline.Key = "cmdTraCuuVanBanOnline";
            this.cmdTraCuuVanBanOnline.Name = "cmdTraCuuVanBanOnline";
            this.cmdTraCuuVanBanOnline.Text = "Tra cứu văn bản trực tuyến";
            // 
            // cmdTuVanHQOnline
            // 
            this.cmdTuVanHQOnline.ImageIndex = 47;
            this.cmdTuVanHQOnline.Key = "cmdTuVanHQOnline";
            this.cmdTuVanHQOnline.Name = "cmdTuVanHQOnline";
            this.cmdTuVanHQOnline.Text = "Tư vấn Hải quan trực tuyến";
            // 
            // cmdGetCategoryOnline
            // 
            this.cmdGetCategoryOnline.ImageIndex = 44;
            this.cmdGetCategoryOnline.Key = "cmdGetCategoryOnline";
            this.cmdGetCategoryOnline.Name = "cmdGetCategoryOnline";
            this.cmdGetCategoryOnline.Text = "Cập nhật danh mục trực tuyến";
            // 
            // cmdTeamview
            // 
            this.cmdTeamview.ImageIndex = 54;
            this.cmdTeamview.Key = "cmdTeamview";
            this.cmdTeamview.Name = "cmdTeamview";
            this.cmdTeamview.Text = "Hỗ trợ trực tuyến qua Teamview";
            // 
            // cmdCapNhatHS
            // 
            this.cmdCapNhatHS.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCapNhatHS8SoAuto1,
            this.cmdCapNhatHS8SoManual1});
            this.cmdCapNhatHS.ImageIndex = 55;
            this.cmdCapNhatHS.Key = "cmdCapNhatHS";
            this.cmdCapNhatHS.Name = "cmdCapNhatHS";
            this.cmdCapNhatHS.Text = "Cập nhật biểu thuế (Mã HS 8 số)";
            // 
            // cmdCapNhatHS8SoAuto1
            // 
            this.cmdCapNhatHS8SoAuto1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCapNhatHS8SoAuto1.Image")));
            this.cmdCapNhatHS8SoAuto1.Key = "cmdCapNhatHS8SoAuto";
            this.cmdCapNhatHS8SoAuto1.Name = "cmdCapNhatHS8SoAuto1";
            // 
            // cmdCapNhatHS8SoManual1
            // 
            this.cmdCapNhatHS8SoManual1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCapNhatHS8SoManual1.Image")));
            this.cmdCapNhatHS8SoManual1.Key = "cmdCapNhatHS8SoManual";
            this.cmdCapNhatHS8SoManual1.Name = "cmdCapNhatHS8SoManual1";
            // 
            // cmdCapNhatHS8SoAuto
            // 
            this.cmdCapNhatHS8SoAuto.ImageIndex = 55;
            this.cmdCapNhatHS8SoAuto.Key = "cmdCapNhatHS8SoAuto";
            this.cmdCapNhatHS8SoAuto.Name = "cmdCapNhatHS8SoAuto";
            this.cmdCapNhatHS8SoAuto.Text = "Cập nhật mã HS 8 số tự động";
            // 
            // cmdCapNhatHS8SoManual
            // 
            this.cmdCapNhatHS8SoManual.ImageIndex = 55;
            this.cmdCapNhatHS8SoManual.Key = "cmdCapNhatHS8SoManual";
            this.cmdCapNhatHS8SoManual.Name = "cmdCapNhatHS8SoManual";
            this.cmdCapNhatHS8SoManual.Text = "Cập nhật mã HS 8 số theo lựa chọn";
            // 
            // cmdTool
            // 
            this.cmdTool.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImageResizeHelp1,
            this.cmdSignFile1,
            this.cmdHelpSignFile1});
            this.cmdTool.ImageIndex = 57;
            this.cmdTool.Key = "cmdTool";
            this.cmdTool.Name = "cmdTool";
            this.cmdTool.Text = "Công cụ hỗ trợ";
            // 
            // cmdImageResizeHelp1
            // 
            this.cmdImageResizeHelp1.Image = ((System.Drawing.Image)(resources.GetObject("cmdImageResizeHelp1.Image")));
            this.cmdImageResizeHelp1.Key = "cmdImageResizeHelp";
            this.cmdImageResizeHelp1.Name = "cmdImageResizeHelp1";
            // 
            // cmdSignFile1
            // 
            this.cmdSignFile1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSignFile1.Image")));
            this.cmdSignFile1.Key = "cmdSignFile";
            this.cmdSignFile1.Name = "cmdSignFile1";
            // 
            // cmdHelpSignFile1
            // 
            this.cmdHelpSignFile1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHelpSignFile1.Image")));
            this.cmdHelpSignFile1.Key = "cmdHelpSignFile";
            this.cmdHelpSignFile1.Name = "cmdHelpSignFile1";
            // 
            // cmdImageResizeHelp
            // 
            this.cmdImageResizeHelp.ImageIndex = 58;
            this.cmdImageResizeHelp.Key = "cmdImageResizeHelp";
            this.cmdImageResizeHelp.Name = "cmdImageResizeHelp";
            this.cmdImageResizeHelp.Text = "Hướng dẫn sử dụng điều chỉnh dung lượng ảnh";
            // 
            // cmbDongBoDuLieu
            // 
            this.cmbDongBoDuLieu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.DongBoDuLieu1,
            this.cmdQuanLyDL1});
            this.cmbDongBoDuLieu.Key = "cmbDongBoDuLieu";
            this.cmbDongBoDuLieu.Name = "cmbDongBoDuLieu";
            this.cmbDongBoDuLieu.Text = "Đồng bộ dữ liệu";
            // 
            // DongBoDuLieu1
            // 
            this.DongBoDuLieu1.Key = "DongBoDuLieu";
            this.DongBoDuLieu1.Name = "DongBoDuLieu1";
            // 
            // cmdQuanLyDL1
            // 
            this.cmdQuanLyDL1.Image = ((System.Drawing.Image)(resources.GetObject("cmdQuanLyDL1.Image")));
            this.cmdQuanLyDL1.Key = "cmdQuanLyDL";
            this.cmdQuanLyDL1.Name = "cmdQuanLyDL1";
            // 
            // cmdQuanLyDL
            // 
            this.cmdQuanLyDL.ImageIndex = 37;
            this.cmdQuanLyDL.Key = "cmdQuanLyDL";
            this.cmdQuanLyDL.Name = "cmdQuanLyDL";
            this.cmdQuanLyDL.Text = "Quản lý đại lý";
            // 
            // cmdDaily
            // 
            this.cmdDaily.Key = "cmdDaily";
            this.cmdDaily.Name = "cmdDaily";
            this.cmdDaily.Text = "Doanh nghiệp khai Đai Lý";
            // 
            // cmdUpdateDatabase
            // 
            this.cmdUpdateDatabase.ImageIndex = 59;
            this.cmdUpdateDatabase.Key = "cmdUpdateDatabase";
            this.cmdUpdateDatabase.Name = "cmdUpdateDatabase";
            this.cmdUpdateDatabase.Text = "Cập nhật Cơ sở dữ liệu";
            // 
            // cmdCloseMe
            // 
            this.cmdCloseMe.Key = "cmdCloseMe";
            this.cmdCloseMe.Name = "cmdCloseMe";
            this.cmdCloseMe.Text = "Đóng cửa sổ này";
            // 
            // cmdHelpVideo
            // 
            this.cmdHelpVideo.ImageIndex = 64;
            this.cmdHelpVideo.Key = "cmdHelpVideo";
            this.cmdHelpVideo.Name = "cmdHelpVideo";
            this.cmdHelpVideo.Text = "Video Hướng dẫn sử dụng chương trình";
            // 
            // cmdHDSDCKS
            // 
            this.cmdHDSDCKS.ImageIndex = 60;
            this.cmdHDSDCKS.Key = "cmdHDSDCKS";
            this.cmdHDSDCKS.Name = "cmdHDSDCKS";
            this.cmdHDSDCKS.Text = "Hướng dẫn đăng ký và sử dụng Chữ ký số (CA)";
            // 
            // cmdHDSDVNACCS
            // 
            this.cmdHDSDVNACCS.ImageIndex = 58;
            this.cmdHDSDVNACCS.Key = "cmdHDSDVNACCS";
            this.cmdHDSDVNACCS.Name = "cmdHDSDVNACCS";
            this.cmdHDSDVNACCS.Text = "Hướng dẫn sử dụng VNACCS";
            // 
            // cmdThongBaoVNACCS
            // 
            this.cmdThongBaoVNACCS.Key = "cmdThongBaoVNACCS";
            this.cmdThongBaoVNACCS.Name = "cmdThongBaoVNACCS";
            this.cmdThongBaoVNACCS.Text = "THÔNG BÁO TỪ HỆ THỐNG VNACCS";
            // 
            // cmdSignFile
            // 
            this.cmdSignFile.Key = "cmdSignFile";
            this.cmdSignFile.Name = "cmdSignFile";
            this.cmdSignFile.Text = "Phần mềm hỗ trợ ký chữ ký số File hỗ trợ định dạng pdf,docx,xlsx";
            // 
            // cmdHelpSignFile
            // 
            this.cmdHelpSignFile.Key = "cmdHelpSignFile";
            this.cmdHelpSignFile.Name = "cmdHelpSignFile";
            this.cmdHelpSignFile.Text = "Hướng dẫn sử dụng phần mềm ký chữ ký số File";
            // 
            // cmdBerth
            // 
            this.cmdBerth.Image = ((System.Drawing.Image)(resources.GetObject("cmdBerth.Image")));
            this.cmdBerth.Key = "cmdBerth";
            this.cmdBerth.Name = "cmdBerth";
            this.cmdBerth.Text = "Cảng biển";
            // 
            // cmdCargo
            // 
            this.cmdCargo.Image = ((System.Drawing.Image)(resources.GetObject("cmdCargo.Image")));
            this.cmdCargo.Key = "cmdCargo";
            this.cmdCargo.Name = "cmdCargo";
            this.cmdCargo.Text = "Địa điểm lưu kho chờ thông quan";
            // 
            // cmdCityUNLOCODE
            // 
            this.cmdCityUNLOCODE.Image = ((System.Drawing.Image)(resources.GetObject("cmdCityUNLOCODE.Image")));
            this.cmdCityUNLOCODE.Key = "cmdCityUNLOCODE";
            this.cmdCityUNLOCODE.Name = "cmdCityUNLOCODE";
            this.cmdCityUNLOCODE.Text = "Địa điểm xếp hàng";
            // 
            // cmdCommon
            // 
            this.cmdCommon.Image = ((System.Drawing.Image)(resources.GetObject("cmdCommon.Image")));
            this.cmdCommon.Key = "cmdCommon";
            this.cmdCommon.Name = "cmdCommon";
            this.cmdCommon.Text = "Tổng hợp danh mục";
            // 
            // cmdContainerSize
            // 
            this.cmdContainerSize.Image = ((System.Drawing.Image)(resources.GetObject("cmdContainerSize.Image")));
            this.cmdContainerSize.Key = "cmdContainerSize";
            this.cmdContainerSize.Name = "cmdContainerSize";
            this.cmdContainerSize.Text = "Container Size";
            // 
            // cmdCustomsSubSection
            // 
            this.cmdCustomsSubSection.Image = ((System.Drawing.Image)(resources.GetObject("cmdCustomsSubSection.Image")));
            this.cmdCustomsSubSection.Key = "cmdCustomsSubSection";
            this.cmdCustomsSubSection.Name = "cmdCustomsSubSection";
            this.cmdCustomsSubSection.Text = "Đội nghiệp vụ chi cục HQ";
            // 
            // cmdOGAUser
            // 
            this.cmdOGAUser.Image = ((System.Drawing.Image)(resources.GetObject("cmdOGAUser.Image")));
            this.cmdOGAUser.Key = "cmdOGAUser";
            this.cmdOGAUser.Name = "cmdOGAUser";
            this.cmdOGAUser.Text = "Kiểm dịch động vật";
            // 
            // cmdPackagesUnit
            // 
            this.cmdPackagesUnit.Image = ((System.Drawing.Image)(resources.GetObject("cmdPackagesUnit.Image")));
            this.cmdPackagesUnit.Key = "cmdPackagesUnit";
            this.cmdPackagesUnit.Name = "cmdPackagesUnit";
            this.cmdPackagesUnit.Text = "ĐVT Lượng kiện";
            // 
            // cmdStations
            // 
            this.cmdStations.Image = ((System.Drawing.Image)(resources.GetObject("cmdStations.Image")));
            this.cmdStations.Key = "cmdStations";
            this.cmdStations.Name = "cmdStations";
            this.cmdStations.Text = "Kho ngoại quan";
            // 
            // cmdTaxClassificationCode
            // 
            this.cmdTaxClassificationCode.Image = ((System.Drawing.Image)(resources.GetObject("cmdTaxClassificationCode.Image")));
            this.cmdTaxClassificationCode.Key = "cmdTaxClassificationCode";
            this.cmdTaxClassificationCode.Name = "cmdTaxClassificationCode";
            this.cmdTaxClassificationCode.Text = "Biểu thuế XNK";
            // 
            // cmdBieuThueXNK2018
            // 
            this.cmdBieuThueXNK2018.Image = ((System.Drawing.Image)(resources.GetObject("cmdBieuThueXNK2018.Image")));
            this.cmdBieuThueXNK2018.Key = "cmdBieuThueXNK2018";
            this.cmdBieuThueXNK2018.Name = "cmdBieuThueXNK2018";
            this.cmdBieuThueXNK2018.Text = "Biểu thuế XNK 2018";
            // 
            // cmdUpdateCategoryOnline
            // 
            this.cmdUpdateCategoryOnline.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateCategoryOnline.Image")));
            this.cmdUpdateCategoryOnline.Key = "cmdUpdateCategoryOnline";
            this.cmdUpdateCategoryOnline.Name = "cmdUpdateCategoryOnline";
            this.cmdUpdateCategoryOnline.Text = "Cập nhật danh mục HQ Online";
            // 
            // cmdNhanPhanHoi
            // 
            this.cmdNhanPhanHoi.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhanPhanHoi.Image")));
            this.cmdNhanPhanHoi.Key = "cmdNhanPhanHoi";
            this.cmdNhanPhanHoi.Name = "cmdNhanPhanHoi";
            this.cmdNhanPhanHoi.Text = "Nhận phản hồi từ HQ";
            // 
            // cmdReloadData
            // 
            this.cmdReloadData.Image = ((System.Drawing.Image)(resources.GetObject("cmdReloadData.Image")));
            this.cmdReloadData.Key = "cmdReloadData";
            this.cmdReloadData.Name = "cmdReloadData";
            this.cmdReloadData.Text = "Cập nhật lại dữ liệu";
            // 
            // cmdConfigReadExcel
            // 
            this.cmdConfigReadExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdConfigReadExcel.Image")));
            this.cmdConfigReadExcel.Key = "cmdConfigReadExcel";
            this.cmdConfigReadExcel.Name = "cmdConfigReadExcel";
            this.cmdConfigReadExcel.Text = "Cấu hình Thông số đọc Excel";
            // 
            // mnuRightClick
            // 
            this.mnuRightClick.CommandManager = this.cmMain;
            this.mnuRightClick.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCloseMe1,
            this.Separator2,
            this.cmdCloseAllButMe1,
            this.cmdCloseAll1});
            this.mnuRightClick.Key = "mnuRightClick";
            this.mnuRightClick.LargeImageSize = new System.Drawing.Size(16, 16);
            this.mnuRightClick.SmallImageSize = new System.Drawing.Size(16, 16);
            // 
            // cmdCloseMe1
            // 
            this.cmdCloseMe1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCloseMe1.Image")));
            this.cmdCloseMe1.Key = "cmdCloseMe";
            this.cmdCloseMe1.Name = "cmdCloseMe1";
            // 
            // cmdCloseAllButMe1
            // 
            this.cmdCloseAllButMe1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCloseAllButMe1.Image")));
            this.cmdCloseAllButMe1.Key = "cmdCloseAllButMe";
            this.cmdCloseAllButMe1.Name = "cmdCloseAllButMe1";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 24);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 494);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbMenu});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.cmbMenu);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(950, 26);
            // 
            // ilSmall
            // 
            this.ilSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSmall.ImageStream")));
            this.ilSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSmall.Images.SetKeyName(0, "");
            this.ilSmall.Images.SetKeyName(1, "");
            this.ilSmall.Images.SetKeyName(2, "");
            this.ilSmall.Images.SetKeyName(3, "");
            this.ilSmall.Images.SetKeyName(4, "");
            this.ilSmall.Images.SetKeyName(5, "");
            this.ilSmall.Images.SetKeyName(6, "");
            this.ilSmall.Images.SetKeyName(7, "");
            this.ilSmall.Images.SetKeyName(8, "");
            this.ilSmall.Images.SetKeyName(9, "");
            this.ilSmall.Images.SetKeyName(10, "");
            this.ilSmall.Images.SetKeyName(11, "");
            this.ilSmall.Images.SetKeyName(12, "");
            this.ilSmall.Images.SetKeyName(13, "");
            this.ilSmall.Images.SetKeyName(14, "");
            this.ilSmall.Images.SetKeyName(15, "");
            this.ilSmall.Images.SetKeyName(16, "");
            this.ilSmall.Images.SetKeyName(17, "");
            this.ilSmall.Images.SetKeyName(18, "");
            this.ilSmall.Images.SetKeyName(19, "");
            this.ilSmall.Images.SetKeyName(20, "");
            this.ilSmall.Images.SetKeyName(21, "");
            this.ilSmall.Images.SetKeyName(22, "");
            this.ilSmall.Images.SetKeyName(23, "");
            this.ilSmall.Images.SetKeyName(24, "");
            this.ilSmall.Images.SetKeyName(25, "");
            this.ilSmall.Images.SetKeyName(26, "");
            this.ilSmall.Images.SetKeyName(27, "");
            this.ilSmall.Images.SetKeyName(28, "");
            this.ilSmall.Images.SetKeyName(29, "");
            this.ilSmall.Images.SetKeyName(30, "");
            this.ilSmall.Images.SetKeyName(31, "");
            this.ilSmall.Images.SetKeyName(32, "");
            this.ilSmall.Images.SetKeyName(33, "");
            this.ilSmall.Images.SetKeyName(34, "");
            this.ilSmall.Images.SetKeyName(35, "vi-VN.gif");
            this.ilSmall.Images.SetKeyName(36, "en-US.gif");
            this.ilSmall.Images.SetKeyName(37, "shell32_279.ico");
            this.ilSmall.Images.SetKeyName(38, "RightDatabase32.gif");
            this.ilSmall.Images.SetKeyName(39, "folder_page.png");
            this.ilSmall.Images.SetKeyName(40, "key.png");
            this.ilSmall.Images.SetKeyName(41, "shell32_21.ico");
            this.ilSmall.Images.SetKeyName(42, "DatabaseLinkerDatabasesOrphan.png");
            this.ilSmall.Images.SetKeyName(43, "86.ico");
            this.ilSmall.Images.SetKeyName(44, "cmdAutoUpdate1.Icon.ico");
            this.ilSmall.Images.SetKeyName(45, "export.ico");
            this.ilSmall.Images.SetKeyName(46, "application_view_tile.png");
            this.ilSmall.Images.SetKeyName(47, "web_find.png");
            this.ilSmall.Images.SetKeyName(48, "import.ico");
            this.ilSmall.Images.SetKeyName(49, "cmdImportDM1.Icon.ico");
            this.ilSmall.Images.SetKeyName(50, "cmdImportHangHoa1.Icon.ico");
            this.ilSmall.Images.SetKeyName(51, "cmdImportNPL1.Icon.ico");
            this.ilSmall.Images.SetKeyName(52, "cmdImportSP1.Icon.ico");
            this.ilSmall.Images.SetKeyName(53, "cmdImportToKhai1.Icon.ico");
            this.ilSmall.Images.SetKeyName(54, "TeamViewer.ico");
            this.ilSmall.Images.SetKeyName(55, "page_edit.png");
            this.ilSmall.Images.SetKeyName(56, "cmdRestore.Icon.ico");
            this.ilSmall.Images.SetKeyName(57, "TienIch1.Icon.ico");
            this.ilSmall.Images.SetKeyName(58, "help_16.png");
            this.ilSmall.Images.SetKeyName(59, "database_save.png");
            this.ilSmall.Images.SetKeyName(60, "Demo_Rule_Unique_Value.png");
            this.ilSmall.Images.SetKeyName(61, "haiquanVN.jpg");
            this.ilSmall.Images.SetKeyName(62, "Display.ico");
            this.ilSmall.Images.SetKeyName(63, "internet explorer.ico");
            this.ilSmall.Images.SetKeyName(64, "video.png");
            this.ilSmall.Images.SetKeyName(65, "date.png");
            // 
            // cmdExportExcel1
            // 
            this.cmdExportExcel1.Key = "cmdExportExccel";
            this.cmdExportExcel1.Name = "cmdExportExcel1";
            // 
            // cmdThoat1
            // 
            this.cmdThoat1.Key = "cmdThoat";
            this.cmdThoat1.Name = "cmdThoat1";
            // 
            // pmMain
            // 
            this.pmMain.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(247)))));
            this.pmMain.ContainerControl = this;
            this.pmMain.DefaultPanelSettings.ActiveCaptionMode = Janus.Windows.UI.Dock.ActiveCaptionMode.Never;
            this.pmMain.DefaultPanelSettings.CaptionDisplayMode = Janus.Windows.UI.Dock.PanelCaptionDisplayMode.Text;
            this.pmMain.DefaultPanelSettings.CaptionHeight = 30;
            this.pmMain.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.pmMain.DefaultPanelSettings.DarkCaptionFormatStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.pmMain.DefaultPanelSettings.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.Window;
            this.pmMain.DefaultPanelSettings.TabStateStyles.FormatStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pmMain.TabbedMdi = true;
            this.pmMain.TabbedMdiSettings.TabStateStyles.SelectedFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.pmMain.VisualStyleManager = this.vsmMain;
            this.pmMain.MdiTabMouseDown += new Janus.Windows.UI.Dock.MdiTabMouseEventHandler(this.pmMain_MdiTabMouseDown);
            this.uiPanel0.Id = new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("ff6e7c0b-1418-48c4-93db-38c36983d83e");
            this.uiPanel0.Panels.Add(this.uiPanel1);
            this.uiPanel2.Id = new System.Guid("aa65e349-23e9-4c90-b1e3-2ee334b109d8");
            this.uiPanel0.Panels.Add(this.uiPanel2);
            this.uiPanelVNACCToKhai.Id = new System.Guid("416542e6-6c3e-4195-a45d-65d9c0aade20");
            this.uiPanel0.Panels.Add(this.uiPanelVNACCToKhai);
            this.uiPanelVNACCGiayPhep.Id = new System.Guid("0482b210-f49b-4a0b-8edc-6cd94bf6b874");
            this.uiPanel0.Panels.Add(this.uiPanelVNACCGiayPhep);
            this.uiPanelVNACCHoaDon.Id = new System.Guid("0f2639c9-acfc-4a1d-8165-bd4c993414dd");
            this.uiPanel0.Panels.Add(this.uiPanelVNACCHoaDon);
            this.uiPanel4.Id = new System.Guid("6b3edbc4-1bf4-4dcd-816e-0feeccd42a5c");
            this.uiPanel0.Panels.Add(this.uiPanel4);
            this.uiPanel5.Id = new System.Guid("50ddaf54-e570-4441-9576-1182500fc2d7");
            this.uiPanel0.Panels.Add(this.uiPanel5);
            this.uiPanel6.Id = new System.Guid("86049b7a-4c24-4636-a813-31276a38b6db");
            this.uiPanel0.Panels.Add(this.uiPanel6);
            this.pmMain.Panels.Add(this.uiPanel0);
            // 
            // Design Time Panel Info:
            // 
            this.pmMain.BeginPanelInfo();
            this.pmMain.AddDockPanelInfo(new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, Janus.Windows.UI.Dock.PanelDockStyle.Left, true, new System.Drawing.Size(242, 595), true);
            this.pmMain.AddDockPanelInfo(new System.Guid("ff6e7c0b-1418-48c4-93db-38c36983d83e"), new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("aa65e349-23e9-4c90-b1e3-2ee334b109d8"), new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("416542e6-6c3e-4195-a45d-65d9c0aade20"), new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("0482b210-f49b-4a0b-8edc-6cd94bf6b874"), new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("0f2639c9-acfc-4a1d-8165-bd4c993414dd"), new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("6b3edbc4-1bf4-4dcd-816e-0feeccd42a5c"), new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("50ddaf54-e570-4441-9576-1182500fc2d7"), new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("86049b7a-4c24-4636-a813-31276a38b6db"), new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), -1, true);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("96d876c4-e448-4823-b699-fcff62ff56b1"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(88, 116), new System.Drawing.Size(0, 6), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("2b3e5f09-9a24-4b99-bf7e-8ee886f8383d"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("d5e59413-5184-45bc-bbc5-9b40a268e6ec"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("52dc898d-e5c5-4c3e-964e-6134d41411e2"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("038f8df0-b141-4aac-bb44-6015ce71b26f"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("ab8bee13-8397-4584-b8e9-5cfc2b506e10"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("adc6599d-0d45-4f54-a9f5-4903d12e3180"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("ff6e7c0b-1418-48c4-93db-38c36983d83e"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("aa65e349-23e9-4c90-b1e3-2ee334b109d8"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("416542e6-6c3e-4195-a45d-65d9c0aade20"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("0482b210-f49b-4a0b-8edc-6cd94bf6b874"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("0f2639c9-acfc-4a1d-8165-bd4c993414dd"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("6b3edbc4-1bf4-4dcd-816e-0feeccd42a5c"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("50ddaf54-e570-4441-9576-1182500fc2d7"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("86049b7a-4c24-4636-a813-31276a38b6db"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.GroupStyle = Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator;
            this.uiPanel0.Location = new System.Drawing.Point(3, 29);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.SelectedPanel = this.uiPanel1;
            this.uiPanel0.Size = new System.Drawing.Size(242, 595);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "Panel 0";
            this.uiPanel0.SelectedPanelChanged += new Janus.Windows.UI.Dock.PanelActionEventHandler(this.uiPanel0_SelectedPanelChanged);
            // 
            // uiPanel1
            // 
            this.uiPanel1.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel1.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel1.Image")));
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(238, 299);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Khai báo Thông quan điện tử";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.expSXXK);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(236, 268);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // expSXXK
            // 
            this.expSXXK.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expSXXK.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expSXXK.ColumnSeparation = 20;
            this.expSXXK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expSXXK.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem107.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem107.Image")));
            explorerBarItem107.Key = "cmdKhaiBaoBCQT";
            explorerBarItem107.Text = "Khai báo";
            explorerBarItem108.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem108.Image")));
            explorerBarItem108.Key = "cmdTheoDoiBCQT";
            explorerBarItem108.Text = "Theo dõi";
            explorerBarGroup43.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem107,
            explorerBarItem108});
            explorerBarGroup43.Key = "grpBaoCaoQuyetToan";
            explorerBarGroup43.Text = "Báo cáo quyết toán";
            explorerBarItem109.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem109.Image")));
            explorerBarItem109.Key = "KhaiBao_ChotTon";
            explorerBarItem109.Text = "Khai báo";
            explorerBarItem110.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem110.Image")));
            explorerBarItem110.Key = "TheoDoi_ChotTon";
            explorerBarItem110.Text = "Theo dõi";
            explorerBarGroup44.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem109,
            explorerBarItem110});
            explorerBarGroup44.Key = "grpChotTon";
            explorerBarGroup44.Text = "Báo cáo chốt tồn";
            explorerBarItem111.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem111.Image")));
            explorerBarItem111.Key = "nplKhaiBao";
            explorerBarItem111.Text = "Khai báo ";
            explorerBarItem112.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem112.Image")));
            explorerBarItem112.Key = "nplKhaiBaoSua";
            explorerBarItem112.Text = "Khai báo sửa";
            explorerBarItem113.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem113.Image")));
            explorerBarItem113.Key = "nplKhaiBaoHuy";
            explorerBarItem113.Text = "Khai báo hủy";
            explorerBarItem114.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem114.Image")));
            explorerBarItem114.Key = "nplTheoDoi";
            explorerBarItem114.Text = "Theo dõi";
            explorerBarItem115.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem115.Image")));
            explorerBarItem115.Key = "nplDaDangKy";
            explorerBarItem115.Text = "Đã đăng ký";
            explorerBarGroup45.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem111,
            explorerBarItem112,
            explorerBarItem113,
            explorerBarItem114,
            explorerBarItem115});
            explorerBarGroup45.Key = "grpNguyenPhuLieu";
            explorerBarGroup45.Text = "Nguyên phụ liệu";
            explorerBarItem116.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem116.Image")));
            explorerBarItem116.Key = "spKhaiBao";
            explorerBarItem116.Text = "Khai báo";
            explorerBarItem117.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem117.Image")));
            explorerBarItem117.Key = "spKhaiBaoSua";
            explorerBarItem117.Text = "Khai báo sửa ";
            explorerBarItem118.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem118.Image")));
            explorerBarItem118.Key = "spKhaiBaoHuy";
            explorerBarItem118.Text = "Khai báo hủy";
            explorerBarItem119.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem119.Image")));
            explorerBarItem119.Key = "spTheoDoi";
            explorerBarItem119.Text = "Theo dõi";
            explorerBarItem120.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem120.Image")));
            explorerBarItem120.Key = "spDaDangKy";
            explorerBarItem120.Text = "Đã đăng ký";
            explorerBarGroup46.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem116,
            explorerBarItem117,
            explorerBarItem118,
            explorerBarItem119,
            explorerBarItem120});
            explorerBarGroup46.Key = "grpSanPham";
            explorerBarGroup46.Text = "Sản phẩm";
            explorerBarItem121.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem121.Image")));
            explorerBarItem121.Key = "dmDangKy";
            explorerBarItem121.Text = "Đăng ký";
            explorerBarItem122.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem122.Image")));
            explorerBarItem122.Key = "dmKhaiBao";
            explorerBarItem122.Text = "Khai báo";
            explorerBarItem123.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem123.Image")));
            explorerBarItem123.Key = "dmKhaiBaoSua";
            explorerBarItem123.Text = "Khai báo sửa";
            explorerBarItem124.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem124.Image")));
            explorerBarItem124.Key = "dmKhaiBaoHuy";
            explorerBarItem124.Text = "Khai báo hủy";
            explorerBarItem125.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem125.Image")));
            explorerBarItem125.Key = "dmKhaiBao_New";
            explorerBarItem125.Text = "Khai báo (Vân)";
            explorerBarItem126.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem126.Image")));
            explorerBarItem126.Key = "dmTheoDoi";
            explorerBarItem126.Text = "Theo dõi";
            explorerBarItem127.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem127.Image")));
            explorerBarItem127.Key = "dmDaDangKy";
            explorerBarItem127.Text = "Đã đăng ký";
            explorerBarGroup54.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem121,
            explorerBarItem122,
            explorerBarItem123,
            explorerBarItem124,
            explorerBarItem125,
            explorerBarItem126,
            explorerBarItem127});
            explorerBarGroup54.Key = "grpDinhMuc";
            explorerBarGroup54.Text = "Định mức";
            explorerBarItem128.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem128.Image")));
            explorerBarItem128.Key = "dmTTRegister";
            explorerBarItem128.Text = "Khai báo";
            explorerBarItem129.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem129.Image")));
            explorerBarItem129.Key = "dmTTEdit";
            explorerBarItem129.Text = "Khai báo sửa";
            explorerBarItem130.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem130.Image")));
            explorerBarItem130.Key = "dmTTCancel";
            explorerBarItem130.Text = "Khai báo hủy";
            explorerBarItem131.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem131.Image")));
            explorerBarItem131.Key = "dmTTManagement";
            explorerBarItem131.Text = "Theo dõi";
            explorerBarGroup55.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem128,
            explorerBarItem129,
            explorerBarItem130,
            explorerBarItem131});
            explorerBarGroup55.Key = "grpDinhMucThucTe";
            explorerBarGroup55.Text = "Định mức thực tế theo từng giai đoạn";
            explorerBarItem132.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem132.Image")));
            explorerBarItem132.Key = "LSXRegister";
            explorerBarItem132.Text = "Đăng ký";
            explorerBarItem133.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem133.Image")));
            explorerBarItem133.Key = "LSXManagement";
            explorerBarItem133.Text = "Theo dõi";
            explorerBarItem134.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem134.Image")));
            explorerBarItem134.Key = "LSXUpdate";
            explorerBarItem134.Text = "Cập nhật lệnh sản xuất";
            explorerBarItem135.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem135.Image")));
            explorerBarItem135.Key = "LSXConfig";
            explorerBarItem135.Text = "Cấu hình quy tắc";
            explorerBarGroup56.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem132,
            explorerBarItem133,
            explorerBarItem134,
            explorerBarItem135});
            explorerBarGroup56.Key = "grpLenhSanXuat";
            explorerBarGroup56.Text = "Lệnh sản xuất";
            explorerBarItem136.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem136.Image")));
            explorerBarItem136.Key = "dmKhaiBao_new";
            explorerBarItem136.Text = "Khai báo";
            explorerBarItem137.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem137.Image")));
            explorerBarItem137.Key = "dmTheoDoi_new";
            explorerBarItem137.Text = "Theo dõi";
            explorerBarItem138.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem138.Image")));
            explorerBarItem138.Key = "dmDaThanhKhoan";
            explorerBarItem138.Text = "Định mức đã thanh khoản";
            explorerBarGroup57.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem136,
            explorerBarItem137,
            explorerBarItem138});
            explorerBarGroup57.Key = "grpDinhMucNew";
            explorerBarGroup57.Text = "Định mức (New)";
            explorerBarItem139.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem139.Image")));
            explorerBarItem139.Key = "tkNhapKhau";
            explorerBarItem139.Text = "Nhập khẩu";
            explorerBarItem157.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem157.Image")));
            explorerBarItem157.Key = "tkXuatKhau";
            explorerBarItem157.Text = "Xuất khẩu";
            explorerBarItem158.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem158.Image")));
            explorerBarItem158.Key = "TheoDoiTKSXXK";
            explorerBarItem158.Text = "Theo dõi ";
            explorerBarItem159.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem159.Image")));
            explorerBarItem159.Key = "ToKhaiSXXKDangKy";
            explorerBarItem159.Text = "Đã đăng ký";
            explorerBarItem160.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem160.Image")));
            explorerBarItem160.Key = "tkHetHan";
            explorerBarItem160.Text = "Sắp hết hạn Thanh khoản";
            explorerBarItem161.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem161.Image")));
            explorerBarItem161.Key = "ChiPhiXNK";
            explorerBarItem161.Text = "Chi phí xuất nhập khẩu";
            explorerBarItem162.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem162.Image")));
            explorerBarItem162.Key = "tkNopThue";
            explorerBarItem162.Text = "Tờ khai phải nộp thuế";
            explorerBarItem163.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem163.Image")));
            explorerBarItem163.Key = "TKHangTKX";
            explorerBarItem163.Text = "Xem hàng tờ  khai xuất";
            explorerBarGroup58.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem139,
            explorerBarItem157,
            explorerBarItem158,
            explorerBarItem159,
            explorerBarItem160,
            explorerBarItem161,
            explorerBarItem162,
            explorerBarItem163});
            explorerBarGroup58.Key = "grpToKhai";
            explorerBarGroup58.Text = "Tờ khai";
            explorerBarItem164.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem164.Image")));
            explorerBarItem164.Key = "AddHSTL";
            explorerBarItem164.Text = "Tạo mới";
            explorerBarItem165.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem165.Image")));
            explorerBarItem165.Key = "UpdateHSTL";
            explorerBarItem165.Text = "Cập nhật";
            explorerBarItem166.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem166.Image")));
            explorerBarItem166.Key = "HSTLManage";
            explorerBarItem166.Text = "Theo dõi";
            explorerBarItem167.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem167.Image")));
            explorerBarItem167.Key = "HSTLClosed";
            explorerBarItem167.Text = "Đã đóng";
            explorerBarItem168.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem168.Image")));
            explorerBarItem168.Key = "QDThanhKhoanTKN";
            explorerBarItem168.Text = "Quyết định thanh khoản TKN";
            explorerBarGroup59.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem164,
            explorerBarItem165,
            explorerBarItem166,
            explorerBarItem167,
            explorerBarItem168});
            explorerBarGroup59.Key = "grpThanhLy";
            explorerBarGroup59.Text = "Hồ sơ thanh lý";
            explorerBarItem169.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem169.Image")));
            explorerBarItem169.Key = "htKhaiBao";
            explorerBarItem169.Text = "Khai báo";
            explorerBarItem170.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem170.Image")));
            explorerBarItem170.Key = "htTheoDoi";
            explorerBarItem170.Text = "Theo dõi";
            explorerBarItem171.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem171.Image")));
            explorerBarItem171.Key = "htDaDangKy";
            explorerBarItem171.Text = "Đã đăng ký";
            explorerBarGroup60.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem169,
            explorerBarItem170,
            explorerBarItem171});
            explorerBarGroup60.Key = "grpHangTon";
            explorerBarGroup60.Text = "Hàng tồn";
            explorerBarItem172.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem172.Image")));
            explorerBarItem172.Key = "tieuHuyKhaiBao";
            explorerBarItem172.Text = "Khai báo";
            explorerBarItem173.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem173.Image")));
            explorerBarItem173.Key = "tieuHuyTheoDoi";
            explorerBarItem173.Text = "Theo dõi";
            explorerBarItem174.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem174.Image")));
            explorerBarItem174.Key = "tieuHuyDaDangKy";
            explorerBarItem174.Text = "Đã đăng ký";
            explorerBarGroup79.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem172,
            explorerBarItem173,
            explorerBarItem174});
            explorerBarGroup79.Key = "grpTieuHuy";
            explorerBarGroup79.Text = "Thông tin tiêu hủy";
            explorerBarItem243.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem243.Image")));
            explorerBarItem243.Key = "gtKhaiBao";
            explorerBarItem243.Text = "Khai báo";
            explorerBarItem244.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem244.Image")));
            explorerBarItem244.Key = "gtTheoDoi";
            explorerBarItem244.Text = "Theo dõi";
            explorerBarItem245.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem245.Image")));
            explorerBarItem245.Key = "gtDaDangKy";
            explorerBarItem245.Text = "Đã đăng ký";
            explorerBarGroup80.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem243,
            explorerBarItem244,
            explorerBarItem245});
            explorerBarGroup80.Key = "grpGiaiTrinh";
            explorerBarGroup80.Text = "Giải trình chênh lệch";
            explorerBarItem246.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem246.Image")));
            explorerBarItem246.Key = "qdktKhaiBao";
            explorerBarItem246.Text = "Khai báo";
            explorerBarItem247.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem247.Image")));
            explorerBarItem247.Key = "qdktTheoDoi";
            explorerBarItem247.Text = "Theo dõi";
            explorerBarItem248.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem248.Image")));
            explorerBarItem248.Key = "qdktDaDangKy";
            explorerBarItem248.Text = "Đã đăng ký";
            explorerBarGroup81.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem246,
            explorerBarItem247,
            explorerBarItem248});
            explorerBarGroup81.Key = "grpQDKT";
            explorerBarGroup81.Text = "Quyết định kiểm tra";
            explorerBarItem249.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem249.Image")));
            explorerBarItem249.Key = "ctnxKhaiBao";
            explorerBarItem249.Text = "Khai báo";
            explorerBarItem250.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem250.Image")));
            explorerBarItem250.Key = "ctnxTheoDoi";
            explorerBarItem250.Text = "Theo dõi";
            explorerBarItem251.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem251.Image")));
            explorerBarItem251.Key = "ctnxDaDangKy";
            explorerBarItem251.Text = "Đã đăng ký";
            explorerBarGroup82.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem249,
            explorerBarItem250,
            explorerBarItem251});
            explorerBarGroup82.Key = "grpCTNX";
            explorerBarGroup82.Text = "Chứng từ nhập xuất";
            explorerBarItem252.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem252.Image")));
            explorerBarItem252.Key = "AddTLTSCD";
            explorerBarItem252.Text = "Tạo hồ sơ";
            explorerBarItem253.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem253.Image")));
            explorerBarItem253.Key = "TLTSCDManager";
            explorerBarItem253.Text = "Theo dõi";
            explorerBarGroup83.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem252,
            explorerBarItem253});
            explorerBarGroup83.Key = "grpTLTSCD";
            explorerBarGroup83.Text = "Thanh lý tài sản cố định";
            explorerBarItem254.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem254.Image")));
            explorerBarItem254.Key = "TriGiaHopDong";
            explorerBarItem254.Text = "Trị giá hợp đồng";
            explorerBarItem255.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem255.Image")));
            explorerBarItem255.Key = "TaoMoiCTTTNhap";
            explorerBarItem255.Text = "Tạo mới chứng từ thanh toán Nhập";
            explorerBarItem256.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem256.Image")));
            explorerBarItem256.Key = "TaoMoiCTTT";
            explorerBarItem256.Text = "Tạo mới chứng từ thanh toán Xuất";
            explorerBarItem257.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem257.Image")));
            explorerBarItem257.Key = "TheoDoiCTTT";
            explorerBarItem257.Text = "Theo dõi chứng từ thanh toán";
            explorerBarGroup84.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem254,
            explorerBarItem255,
            explorerBarItem256,
            explorerBarItem257});
            explorerBarGroup84.Key = "grpCTTT";
            explorerBarGroup84.Text = "Chứng từ thanh toán";
            explorerBarItem258.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem258.Image")));
            explorerBarItem258.Key = "cmdMau15";
            explorerBarItem258.Text = "Khai báo";
            explorerBarItem259.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem259.Image")));
            explorerBarItem259.Key = "cmdTheoDoi";
            explorerBarItem259.Text = "Theo dõi";
            explorerBarGroup85.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem258,
            explorerBarItem259});
            explorerBarGroup85.Key = "grpQuyetToan";
            explorerBarGroup85.Text = "Quyết Toán (2017)";
            this.expSXXK.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup43,
            explorerBarGroup44,
            explorerBarGroup45,
            explorerBarGroup46,
            explorerBarGroup54,
            explorerBarGroup55,
            explorerBarGroup56,
            explorerBarGroup57,
            explorerBarGroup58,
            explorerBarGroup59,
            explorerBarGroup60,
            explorerBarGroup79,
            explorerBarGroup80,
            explorerBarGroup81,
            explorerBarGroup82,
            explorerBarGroup83,
            explorerBarGroup84,
            explorerBarGroup85});
            this.expSXXK.GroupSeparation = 10;
            this.expSXXK.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expSXXK.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expSXXK.ImageSize = new System.Drawing.Size(16, 16);
            this.expSXXK.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expSXXK.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expSXXK.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expSXXK.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expSXXK.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expSXXK.Location = new System.Drawing.Point(0, 0);
            this.expSXXK.Name = "expSXXK";
            this.expSXXK.Size = new System.Drawing.Size(236, 268);
            this.expSXXK.TabIndex = 1;
            this.expSXXK.Text = "explorerBar1";
            this.expSXXK.TopMargin = 10;
            this.expSXXK.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expSXXK.VisualStyleManager = this.vsmMain;
            this.expSXXK.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expSXXK_ItemClick);
            // 
            // uiPanel2
            // 
            this.uiPanel2.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel2.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel2.Image")));
            this.uiPanel2.InnerContainer = this.uiPanelContainerVNACCTK;
            this.uiPanel2.Location = new System.Drawing.Point(0, 0);
            this.uiPanel2.Name = "uiPanel2";
            this.uiPanel2.Size = new System.Drawing.Size(238, 299);
            this.uiPanel2.TabIndex = 4;
            this.uiPanel2.Text = "Quản lý Nguyên phụ liệu tồn";
            // 
            // uiPanelContainerVNACCTK
            // 
            this.uiPanelContainerVNACCTK.Controls.Add(this.expKhaiBao_TheoDoi);
            this.uiPanelContainerVNACCTK.Location = new System.Drawing.Point(1, 31);
            this.uiPanelContainerVNACCTK.Name = "uiPanelContainerVNACCTK";
            this.uiPanelContainerVNACCTK.Size = new System.Drawing.Size(236, 268);
            this.uiPanelContainerVNACCTK.TabIndex = 0;
            // 
            // expKhaiBao_TheoDoi
            // 
            this.expKhaiBao_TheoDoi.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKhaiBao_TheoDoi.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expKhaiBao_TheoDoi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expKhaiBao_TheoDoi.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem175.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem175.Image")));
            explorerBarItem175.Key = "tdNPLton";
            explorerBarItem175.Text = "Theo dõi Nguyên phụ liệu Tồn";
            explorerBarItem176.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem176.Image")));
            explorerBarItem176.Key = "tkNPLton";
            explorerBarItem176.Text = "Thống kê Nguyên phụ liệu tồn";
            explorerBarItem177.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem177.Image")));
            explorerBarItem177.Key = "tdPBTKN";
            explorerBarItem177.Text = "Theo dõi Tờ khai nhập tự phân bổ";
            explorerBarGroup61.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem175,
            explorerBarItem176,
            explorerBarItem177});
            explorerBarGroup61.Key = "grpNPLTon";
            explorerBarGroup61.Text = "Nguyên Phụ Liệu Tồn";
            explorerBarItem178.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem178.Image")));
            explorerBarItem178.Key = "tkLHKNhap";
            explorerBarItem178.Text = "Tờ khai nhập";
            explorerBarItem179.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem179.Image")));
            explorerBarItem179.Key = "tkLHKXuat";
            explorerBarItem179.Text = "Tờ khai xuất";
            explorerBarGroup62.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem178,
            explorerBarItem179});
            explorerBarGroup62.Key = "grpTKLoaiHinhKhac";
            explorerBarGroup62.Text = "Tờ khai nhập từ hệ thống khác";
            explorerBarItem180.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem180.Image")));
            explorerBarItem180.Key = "thueTonTKNhap";
            explorerBarItem180.Text = "Thuế tồn tờ khai nhập";
            explorerBarItem181.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem181.Image")));
            explorerBarItem181.Key = "triGiaTKNhap";
            explorerBarItem181.Text = "Trị giá hàng tờ khai nhập";
            explorerBarItem182.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem182.Image")));
            explorerBarItem182.Key = "triGiaTKXuat";
            explorerBarItem182.Text = "Trị giá hàng tờ khai xuất";
            explorerBarGroup63.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem180,
            explorerBarItem181,
            explorerBarItem182});
            explorerBarGroup63.Key = "grpQuanLyToKhai";
            explorerBarGroup63.Text = "Quản lý tờ khai";
            explorerBarItem183.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem183.Image")));
            explorerBarItem183.Key = "itemKTDinhMuc";
            explorerBarItem183.Text = "Định mức";
            explorerBarItem184.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem184.Image")));
            explorerBarItem184.Key = "itemKTToKhai";
            explorerBarItem184.Text = "Tờ khai";
            explorerBarGroup64.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem183,
            explorerBarItem184});
            explorerBarGroup64.Key = "grpKiemTraDuLieu";
            explorerBarGroup64.Text = "Dữ liệu khai sai";
            this.expKhaiBao_TheoDoi.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup61,
            explorerBarGroup62,
            explorerBarGroup63,
            explorerBarGroup64});
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.ImageSize = new System.Drawing.Size(16, 16);
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expKhaiBao_TheoDoi.Location = new System.Drawing.Point(0, 0);
            this.expKhaiBao_TheoDoi.Name = "expKhaiBao_TheoDoi";
            this.expKhaiBao_TheoDoi.Size = new System.Drawing.Size(236, 268);
            this.expKhaiBao_TheoDoi.TabIndex = 2;
            this.expKhaiBao_TheoDoi.Text = "explorerBar1";
            this.expKhaiBao_TheoDoi.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expKhaiBao_TheoDoi.VisualStyleManager = this.vsmMain;
            this.expKhaiBao_TheoDoi.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expKhaiBao_TheoDoi_ItemClick);
            // 
            // uiPanelVNACCToKhai
            // 
            this.uiPanelVNACCToKhai.Image = ((System.Drawing.Image)(resources.GetObject("uiPanelVNACCToKhai.Image")));
            this.uiPanelVNACCToKhai.InnerContainer = this.uiPanel3Container;
            this.uiPanelVNACCToKhai.Location = new System.Drawing.Point(0, 0);
            this.uiPanelVNACCToKhai.Name = "uiPanelVNACCToKhai";
            this.uiPanelVNACCToKhai.Size = new System.Drawing.Size(238, 299);
            this.uiPanelVNACCToKhai.TabIndex = 4;
            this.uiPanelVNACCToKhai.Text = "VNACCS - Tờ khai";
            // 
            // uiPanel3Container
            // 
            this.uiPanel3Container.Controls.Add(this.explorerBarVNACCS_TKMD);
            this.uiPanel3Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel3Container.Name = "uiPanel3Container";
            this.uiPanel3Container.Size = new System.Drawing.Size(236, 268);
            this.uiPanel3Container.TabIndex = 0;
            // 
            // explorerBarVNACCS_TKMD
            // 
            this.explorerBarVNACCS_TKMD.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_TKMD.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_TKMD.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_TKMD.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBarVNACCS_TKMD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBarVNACCS_TKMD.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem1.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem1.Image")));
            explorerBarItem1.Key = "KhaiBaoTEA";
            explorerBarItem1.Text = "Khai báo (TEA)";
            explorerBarItem2.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem2.Image")));
            explorerBarItem2.Key = "TheoDoiTEA";
            explorerBarItem2.Text = "Theo dõi";
            explorerBarGroup1.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem1,
            explorerBarItem2});
            explorerBarGroup1.Key = "grpTEA";
            explorerBarGroup1.Text = "Hàng miễn thuế (TEA)";
            explorerBarItem3.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem3.Image")));
            explorerBarItem3.Key = "KhaiBaoTIA";
            explorerBarItem3.Text = "Khai báo (TIA)";
            explorerBarItem4.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem4.Image")));
            explorerBarItem4.Key = "TheoDoiTIA";
            explorerBarItem4.Text = "Theo dõi";
            explorerBarGroup2.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem3,
            explorerBarItem4});
            explorerBarGroup2.Key = "grpTIA";
            explorerBarGroup2.Text = "Hàng tạm nhập/ tái xuất (TIA)";
            explorerBarItem5.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem5.Image")));
            explorerBarItem5.Key = "ToKhaiNhap";
            explorerBarItem5.Text = "Tờ khai nhập (IDA)";
            explorerBarItem6.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem6.Image")));
            explorerBarItem6.Key = "ToKhaiXuat";
            explorerBarItem6.Text = "Tờ khai xuất (EDA)";
            explorerBarItem7.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem7.Image")));
            explorerBarItem7.Key = "TheoDoiToKhai";
            explorerBarItem7.Text = "Theo dõi ";
            explorerBarItem8.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem8.Image")));
            explorerBarItem8.Key = "TheoDoiChungTu";
            explorerBarItem8.Text = "Theo dõi chứng từ tờ khai";
            explorerBarGroup3.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem5,
            explorerBarItem6,
            explorerBarItem7,
            explorerBarItem8});
            explorerBarGroup3.Key = "grpToKhaiVNACC";
            explorerBarGroup3.Text = "Tờ khai mậu dich (VNACCS)";
            explorerBarItem9.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem9.Image")));
            explorerBarItem9.Key = "KhaiBao_TKVC";
            explorerBarItem9.Text = "Khai báo tờ khai vận chuyển (OLA)";
            explorerBarItem10.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem10.Image")));
            explorerBarItem10.Key = "TheoDoi_TKVC";
            explorerBarItem10.Text = "Theo dõi";
            explorerBarGroup4.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem9,
            explorerBarItem10});
            explorerBarGroup4.Key = "grpTKVC";
            explorerBarGroup4.Text = "Tờ khai vận chuyển (OLA)";
            explorerBarItem185.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem185.Image")));
            explorerBarItem185.Key = "ToKhaiNhapTriGiaThap";
            explorerBarItem185.Text = "Tờ khai nhập (MIC)";
            explorerBarItem186.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem186.Image")));
            explorerBarItem186.Key = "ToKhaiXuatTriGiaThap";
            explorerBarItem186.Text = "Tờ khai xuất (MEC)";
            explorerBarItem187.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem187.Image")));
            explorerBarItem187.Key = "TheoDoiTKTG";
            explorerBarItem187.Text = "Theo dõi";
            explorerBarGroup65.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem185,
            explorerBarItem186,
            explorerBarItem187});
            explorerBarGroup65.Key = "grpToKhaiTriGia";
            explorerBarGroup65.Text = "Tờ khai trị giá thấp (MIC/MEC)";
            explorerBarItem188.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem188.Image")));
            explorerBarItem188.Key = "KhaiBao_TK_KhaiBoSung_ThueHangHoa";
            explorerBarItem188.Text = "Khai báo (AMA)";
            explorerBarItem189.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem189.Image")));
            explorerBarItem189.Key = "TheoDoi_TK_KhaiBoSung_ThueHangHoa";
            explorerBarItem189.Text = "Theo dõi";
            explorerBarGroup66.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem188,
            explorerBarItem189});
            explorerBarGroup66.Key = "grpTKKhaiBoSung";
            explorerBarGroup66.Text = "Sửa đổi/ bổ sung tờ khai (AMA)";
            explorerBarItem190.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem190.Image")));
            explorerBarItem190.Key = "KhaiBao_PK";
            explorerBarItem190.Text = "Khai Báo (CFS)";
            explorerBarItem191.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem191.Image")));
            explorerBarItem191.Key = "TheoDoi_PK";
            explorerBarItem191.Text = "Theo dõi";
            explorerBarGroup67.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem190,
            explorerBarItem191});
            explorerBarGroup67.Key = "grpKho";
            explorerBarGroup67.Text = "Khai báo hàng vào kho (CFS)";
            explorerBarItem192.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem192.Image")));
            explorerBarItem192.Key = "KhaiBao_ChungTuKem_HYS";
            explorerBarItem192.Text = "Khai báo đính kèm điện tử (HYS)";
            explorerBarItem193.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem193.Image")));
            explorerBarItem193.Key = "KhaiBao_ChungTuKem_MSB";
            explorerBarItem193.Text = "Khai báo đính kèm điện tử (MSB)";
            explorerBarItem194.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem194.Image")));
            explorerBarItem194.Key = "TheoDoi_ChungTuKem";
            explorerBarItem194.Text = "Theo dõi";
            explorerBarItem195.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem195.Image")));
            explorerBarItem195.Key = "cmdSignFile";
            explorerBarItem195.Text = "Ký chữ ký số cho File đính kèm";
            explorerBarGroup68.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem192,
            explorerBarItem193,
            explorerBarItem194,
            explorerBarItem195});
            explorerBarGroup68.Key = "grpChungTuDinhKem";
            explorerBarGroup68.Text = "Chứng từ đính kèm (HSY/MSB)";
            explorerBarItem196.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem196.Image")));
            explorerBarItem196.Key = "KhaiBao_CTTT_IAS";
            explorerBarItem196.Text = "Chứng từ bảo lãnh (IAS)";
            explorerBarItem197.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem197.Image")));
            explorerBarItem197.Key = "KhaiBao_CTTT_IBA";
            explorerBarItem197.Text = "Hạn mức ngân hàng (IBA)";
            explorerBarItem198.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem198.Image")));
            explorerBarItem198.Key = "TheoDoi_ChungTuThanhToan";
            explorerBarItem198.Text = "Theo dõi";
            explorerBarItem198.Visible = false;
            explorerBarGroup69.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem196,
            explorerBarItem197,
            explorerBarItem198});
            explorerBarGroup69.Key = "grpChungTuThanhToan";
            explorerBarGroup69.Text = "Chứng từ thanh toán (IAS/IBA)";
            explorerBarItem199.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem199.Image")));
            explorerBarItem199.Key = "KhaiBao_TKVCQKVGS";
            explorerBarItem199.Text = "Khai báo";
            explorerBarItem200.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem200.Image")));
            explorerBarItem200.Key = "TheoDoi_TKVCQKVGS";
            explorerBarItem200.Text = "Theo dõi";
            explorerBarGroup70.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem199,
            explorerBarItem200});
            explorerBarGroup70.Key = "grpTKVCK";
            explorerBarGroup70.Text = "Khai báo tờ khai vận chuyển KVGS";
            explorerBarItem201.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem201.Image")));
            explorerBarItem201.Key = "cmdKhaiBaoDinhDanh";
            explorerBarItem201.Text = "Khai báo lấy số định danh hàng hóa";
            explorerBarItem202.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem202.Image")));
            explorerBarItem202.Key = "cmdTheoDoiDinhDanh";
            explorerBarItem202.Text = "Theo dõi";
            explorerBarGroup71.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem201,
            explorerBarItem202});
            explorerBarGroup71.Key = "Group1";
            explorerBarGroup71.Text = "Khai báo định danh hàng hóa";
            explorerBarItem203.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem203.Image")));
            explorerBarItem203.Key = "cmdKhaiBaoTachVanDon";
            explorerBarItem203.Text = "Khai báo";
            explorerBarItem204.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem204.Image")));
            explorerBarItem204.Key = "cmdTheoDoiTachVanDon";
            explorerBarItem204.Text = "Theo dõi";
            explorerBarGroup72.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem203,
            explorerBarItem204});
            explorerBarGroup72.Key = "grpTachVanDon";
            explorerBarGroup72.Text = "Tách vận đơn";
            explorerBarItem205.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem205.Image")));
            explorerBarItem205.Key = "cmdHangContainer";
            explorerBarItem205.Text = "Tờ khai nộp phí hàng Container";
            explorerBarItem206.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem206.Image")));
            explorerBarItem206.Key = "cmdHangRoi";
            explorerBarItem206.Text = "Tờ khai nộp phí hàng rời , lỏng";
            explorerBarItem207.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem207.Image")));
            explorerBarItem207.Key = "cmdDanhSachTKNP";
            explorerBarItem207.Text = "Danh sách tờ khai nộp phí";
            explorerBarItem208.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem208.Image")));
            explorerBarItem208.Key = "cmdTraCuuBL";
            explorerBarItem208.Text = "Tra cứu biên lai";
            explorerBarItem209.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem209.Image")));
            explorerBarItem209.Key = "cmdRegisterInformation";
            explorerBarItem209.Text = "Đăng ký thông tin doanh nghiệp";
            explorerBarItem210.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem210.Image")));
            explorerBarItem210.Key = "cmdRegisterManagement";
            explorerBarItem210.Text = "Quản lý doanh nghiệp";
            explorerBarGroup73.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem205,
            explorerBarItem206,
            explorerBarItem207,
            explorerBarItem208,
            explorerBarItem209,
            explorerBarItem210});
            explorerBarGroup73.Key = "grpThuPhiHQ";
            explorerBarGroup73.Text = "Quản lý thu phí";
            this.explorerBarVNACCS_TKMD.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup1,
            explorerBarGroup2,
            explorerBarGroup3,
            explorerBarGroup4,
            explorerBarGroup65,
            explorerBarGroup66,
            explorerBarGroup67,
            explorerBarGroup68,
            explorerBarGroup69,
            explorerBarGroup70,
            explorerBarGroup71,
            explorerBarGroup72,
            explorerBarGroup73});
            this.explorerBarVNACCS_TKMD.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_TKMD.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBarVNACCS_TKMD.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBarVNACCS_TKMD.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBarVNACCS_TKMD.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBarVNACCS_TKMD.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBarVNACCS_TKMD.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBarVNACCS_TKMD.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_TKMD.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBarVNACCS_TKMD.Location = new System.Drawing.Point(0, 0);
            this.explorerBarVNACCS_TKMD.Name = "explorerBarVNACCS_TKMD";
            this.explorerBarVNACCS_TKMD.Size = new System.Drawing.Size(236, 268);
            this.explorerBarVNACCS_TKMD.TabIndex = 1;
            this.explorerBarVNACCS_TKMD.Text = "explorerBar2";
            this.explorerBarVNACCS_TKMD.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBarVNACCS_TKMD.VisualStyleManager = this.vsmMain;
            this.explorerBarVNACCS_TKMD.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarVNACCS_TKMD_ItemClick);
            // 
            // uiPanelVNACCGiayPhep
            // 
            this.uiPanelVNACCGiayPhep.Image = ((System.Drawing.Image)(resources.GetObject("uiPanelVNACCGiayPhep.Image")));
            this.uiPanelVNACCGiayPhep.InnerContainer = this.uiPanelContainerVNACCGP;
            this.uiPanelVNACCGiayPhep.Location = new System.Drawing.Point(0, 0);
            this.uiPanelVNACCGiayPhep.Name = "uiPanelVNACCGiayPhep";
            this.uiPanelVNACCGiayPhep.Size = new System.Drawing.Size(238, 299);
            this.uiPanelVNACCGiayPhep.TabIndex = 4;
            this.uiPanelVNACCGiayPhep.Text = "VNACCS - Giấy phép";
            // 
            // uiPanelContainerVNACCGP
            // 
            this.uiPanelContainerVNACCGP.Controls.Add(this.explorerBarVNACCS_GiayPhep);
            this.uiPanelContainerVNACCGP.Location = new System.Drawing.Point(1, 31);
            this.uiPanelContainerVNACCGP.Name = "uiPanelContainerVNACCGP";
            this.uiPanelContainerVNACCGP.Size = new System.Drawing.Size(236, 268);
            this.uiPanelContainerVNACCGP.TabIndex = 0;
            // 
            // explorerBarVNACCS_GiayPhep
            // 
            this.explorerBarVNACCS_GiayPhep.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_GiayPhep.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_GiayPhep.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_GiayPhep.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBarVNACCS_GiayPhep.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBarVNACCS_GiayPhep.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem11.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem11.Image")));
            explorerBarItem11.Key = "KhaiBao_GiayPhep_SEA";
            explorerBarItem11.Text = "Khai báo";
            explorerBarItem12.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem12.Image")));
            explorerBarItem12.Key = "TheoDoi_GiayPhep_SEA";
            explorerBarItem12.Text = "Theo dõi";
            explorerBarGroup5.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem11,
            explorerBarItem12});
            explorerBarGroup5.Key = "grpGiayPhep";
            explorerBarGroup5.Text = "Giấy phép vật liệu nổ";
            explorerBarItem13.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem13.Image")));
            explorerBarItem13.Key = "KhaiBao_GiayPhep_SFA";
            explorerBarItem13.Text = "Khai báo";
            explorerBarItem14.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem14.Image")));
            explorerBarItem14.Key = "TheoDoi_GiayPhep_SFA";
            explorerBarItem14.Text = "Theo dõi";
            explorerBarGroup6.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem13,
            explorerBarItem14});
            explorerBarGroup6.Key = "grpGiayPhepSFA";
            explorerBarGroup6.Text = "Giấy phép thực phẩm nhập khẩu";
            explorerBarItem15.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem15.Image")));
            explorerBarItem15.Key = "KhaiBao_GiayPhep_SAA";
            explorerBarItem15.Text = "Khai báo";
            explorerBarItem16.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem16.Image")));
            explorerBarItem16.Key = "TheoDoi_GiayPhep_SAA";
            explorerBarItem16.Text = "Theo dõi";
            explorerBarGroup7.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem15,
            explorerBarItem16});
            explorerBarGroup7.Key = "grpGiayPhepSAA";
            explorerBarGroup7.Text = "Giấy phép kiểm dịch động vật";
            explorerBarItem17.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem17.Image")));
            explorerBarItem17.Key = "KhaiBao_GiayPhep_SMA";
            explorerBarItem17.Text = "Khai báo";
            explorerBarItem18.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem18.Image")));
            explorerBarItem18.Key = "TheoDoi_GiayPhep_SMA";
            explorerBarItem18.Text = "Theo dõi";
            explorerBarGroup8.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem17,
            explorerBarItem18});
            explorerBarGroup8.Key = "grpGiayPhepSMA";
            explorerBarGroup8.Text = "Giấy phép nhập khẩu thuốc";
            this.explorerBarVNACCS_GiayPhep.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup5,
            explorerBarGroup6,
            explorerBarGroup7,
            explorerBarGroup8});
            this.explorerBarVNACCS_GiayPhep.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_GiayPhep.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBarVNACCS_GiayPhep.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBarVNACCS_GiayPhep.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBarVNACCS_GiayPhep.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBarVNACCS_GiayPhep.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBarVNACCS_GiayPhep.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBarVNACCS_GiayPhep.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_GiayPhep.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBarVNACCS_GiayPhep.Location = new System.Drawing.Point(0, 0);
            this.explorerBarVNACCS_GiayPhep.Name = "explorerBarVNACCS_GiayPhep";
            this.explorerBarVNACCS_GiayPhep.Size = new System.Drawing.Size(236, 268);
            this.explorerBarVNACCS_GiayPhep.TabIndex = 1;
            this.explorerBarVNACCS_GiayPhep.Text = "explorerBar3";
            this.explorerBarVNACCS_GiayPhep.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBarVNACCS_GiayPhep.VisualStyleManager = this.vsmMain;
            this.explorerBarVNACCS_GiayPhep.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarVNACCS_GiayPhep_ItemClick);
            // 
            // uiPanelVNACCHoaDon
            // 
            this.uiPanelVNACCHoaDon.Image = ((System.Drawing.Image)(resources.GetObject("uiPanelVNACCHoaDon.Image")));
            this.uiPanelVNACCHoaDon.InnerContainer = this.uiPanelContainerVNACCHD;
            this.uiPanelVNACCHoaDon.Location = new System.Drawing.Point(0, 0);
            this.uiPanelVNACCHoaDon.Name = "uiPanelVNACCHoaDon";
            this.uiPanelVNACCHoaDon.Size = new System.Drawing.Size(238, 299);
            this.uiPanelVNACCHoaDon.TabIndex = 4;
            this.uiPanelVNACCHoaDon.Text = "VNACCS - Hóa đơn";
            // 
            // uiPanelContainerVNACCHD
            // 
            this.uiPanelContainerVNACCHD.Controls.Add(this.explorerBarVNACCS_HoaDon);
            this.uiPanelContainerVNACCHD.Location = new System.Drawing.Point(1, 31);
            this.uiPanelContainerVNACCHD.Name = "uiPanelContainerVNACCHD";
            this.uiPanelContainerVNACCHD.Size = new System.Drawing.Size(236, 268);
            this.uiPanelContainerVNACCHD.TabIndex = 0;
            // 
            // explorerBarVNACCS_HoaDon
            // 
            this.explorerBarVNACCS_HoaDon.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_HoaDon.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_HoaDon.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_HoaDon.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBarVNACCS_HoaDon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBarVNACCS_HoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem19.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem19.Image")));
            explorerBarItem19.Key = "KhaiBaoHoaDon";
            explorerBarItem19.Text = "Khai báo";
            explorerBarItem20.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem20.Image")));
            explorerBarItem20.Key = "TheoDoiHoaDon";
            explorerBarItem20.Text = "Theo dõi";
            explorerBarGroup9.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem19,
            explorerBarItem20});
            explorerBarGroup9.Key = "grpHoaDon";
            explorerBarGroup9.Text = "Hóa đơn";
            this.explorerBarVNACCS_HoaDon.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup9});
            this.explorerBarVNACCS_HoaDon.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarVNACCS_HoaDon.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBarVNACCS_HoaDon.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBarVNACCS_HoaDon.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBarVNACCS_HoaDon.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBarVNACCS_HoaDon.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBarVNACCS_HoaDon.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBarVNACCS_HoaDon.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarVNACCS_HoaDon.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBarVNACCS_HoaDon.Location = new System.Drawing.Point(0, 0);
            this.explorerBarVNACCS_HoaDon.Name = "explorerBarVNACCS_HoaDon";
            this.explorerBarVNACCS_HoaDon.Size = new System.Drawing.Size(236, 268);
            this.explorerBarVNACCS_HoaDon.TabIndex = 1;
            this.explorerBarVNACCS_HoaDon.Text = "explorerBar4";
            this.explorerBarVNACCS_HoaDon.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBarVNACCS_HoaDon.VisualStyleManager = this.vsmMain;
            this.explorerBarVNACCS_HoaDon.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarVNACCS_HoaDon_ItemClick);
            // 
            // uiPanel4
            // 
            this.uiPanel4.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel4.Image")));
            this.uiPanel4.InnerContainer = this.uiPanel4Container;
            this.uiPanel4.Location = new System.Drawing.Point(0, 0);
            this.uiPanel4.Name = "uiPanel4";
            this.uiPanel4.Size = new System.Drawing.Size(238, 299);
            this.uiPanel4.TabIndex = 4;
            this.uiPanel4.Text = "VNACCS -Chứng từ";
            // 
            // uiPanel4Container
            // 
            this.uiPanel4Container.Controls.Add(this.explorerBar1);
            this.uiPanel4Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel4Container.Name = "uiPanel4Container";
            this.uiPanel4Container.Size = new System.Drawing.Size(236, 268);
            this.uiPanel4Container.TabIndex = 0;
            // 
            // explorerBar1
            // 
            this.explorerBar1.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBar1.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar1.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar1.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBar1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem21.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem21.Image")));
            explorerBarItem21.Key = "KhaiBao";
            explorerBarItem21.Text = "Khai báo";
            explorerBarItem22.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem22.Image")));
            explorerBarItem22.Key = "TheoDoi";
            explorerBarItem22.Text = "Theo dõi";
            explorerBarGroup10.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem21,
            explorerBarItem22});
            explorerBarGroup10.Key = "grpChungTu";
            explorerBarGroup10.Text = "Thông báo cơ sở sản xuất";
            explorerBarItem23.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem23.Image")));
            explorerBarItem23.Key = "cmdPhieuNhapKho";
            explorerBarItem23.Text = "Phiếu nhập kho";
            explorerBarItem24.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem24.Image")));
            explorerBarItem24.Key = "cmdPhieuXuatKho";
            explorerBarItem24.Text = "Phiếu xuất kho";
            explorerBarItem25.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem25.Image")));
            explorerBarItem25.Key = "cmdTheoDoiKho";
            explorerBarItem25.Text = "Theo dõi";
            explorerBarGroup11.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem23,
            explorerBarItem24,
            explorerBarItem25});
            explorerBarGroup11.Key = "grpQuanLyKho";
            explorerBarGroup11.Text = "Quản lý kho";
            explorerBarItem26.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem26.Image")));
            explorerBarItem26.Key = "cmdTheoDoiMessage";
            explorerBarItem26.Text = "Theo dõi";
            explorerBarGroup12.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem26});
            explorerBarGroup12.Key = "grpQLMessage";
            explorerBarGroup12.Text = "Quản lý Message";
            this.explorerBar1.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup10,
            explorerBarGroup11,
            explorerBarGroup12});
            this.explorerBar1.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBar1.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBar1.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBar1.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBar1.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBar1.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBar1.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBar1.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar1.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBar1.Location = new System.Drawing.Point(0, 0);
            this.explorerBar1.Name = "explorerBar1";
            this.explorerBar1.Size = new System.Drawing.Size(236, 268);
            this.explorerBar1.TabIndex = 2;
            this.explorerBar1.Text = "explorerBar4";
            this.explorerBar1.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBar1.VisualStyleManager = this.vsmMain;
            this.explorerBar1.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBar1_ItemClick);
            // 
            // uiPanel5
            // 
            this.uiPanel5.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel5.Image")));
            this.uiPanel5.InnerContainer = this.uiPanel5Container;
            this.uiPanel5.Location = new System.Drawing.Point(0, 0);
            this.uiPanel5.Name = "uiPanel5";
            this.uiPanel5.Size = new System.Drawing.Size(238, 299);
            this.uiPanel5.TabIndex = 4;
            this.uiPanel5.Text = "Báo cáo Tổng hợp dữ liệu";
            // 
            // uiPanel5Container
            // 
            this.uiPanel5Container.Controls.Add(this.explorerBarBCTongHopDuLieu);
            this.uiPanel5Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel5Container.Name = "uiPanel5Container";
            this.uiPanel5Container.Size = new System.Drawing.Size(236, 268);
            this.uiPanel5Container.TabIndex = 0;
            // 
            // explorerBarBCTongHopDuLieu
            // 
            this.explorerBarBCTongHopDuLieu.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarBCTongHopDuLieu.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarBCTongHopDuLieu.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarBCTongHopDuLieu.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBarBCTongHopDuLieu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBarBCTongHopDuLieu.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem27.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem27.Image")));
            explorerBarItem27.Key = "ToKhaiXuatNhapKhau";
            explorerBarItem27.Text = "Thống kê Tờ khai xuất nhập khẩu";
            explorerBarItem28.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem28.Image")));
            explorerBarItem28.Key = "ToKhaiAMA";
            explorerBarItem28.Text = "Thống kê Tờ khai AMA";
            explorerBarItem29.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem29.Image")));
            explorerBarItem29.Key = "TongHangHoaXNK";
            explorerBarItem29.Text = "Báo cáo Tổng hàng hóa xuất nhập khẩu";
            explorerBarItem30.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem30.Image")));
            explorerBarItem30.Key = "ChiTietHangHoaXNK";
            explorerBarItem30.Text = "Báo cáo Chi tiết hàng hóa xuất nhập khẩu";
            explorerBarGroup13.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem27,
            explorerBarItem28,
            explorerBarItem29,
            explorerBarItem30});
            explorerBarGroup13.Key = "grpTKVNACCS";
            explorerBarGroup13.Text = "Tờ khai VNACCS";
            explorerBarItem31.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem31.Image")));
            explorerBarItem31.Key = "TongHopXNT";
            explorerBarItem31.Text = "Tổng hợp xuất nhập tồn";
            explorerBarItem32.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem32.Image")));
            explorerBarItem32.Key = "QuyDoiNPLSP";
            explorerBarItem32.Text = "Bảng quy đổi nguyên phụ liệu từ thành phẩm";
            explorerBarGroup14.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem31,
            explorerBarItem32});
            explorerBarGroup14.Key = "grpTongHop";
            explorerBarGroup14.Text = "Tổng hợp báo cáo tờ khai VNACCS";
            explorerBarItem33.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem33.Image")));
            explorerBarItem33.Key = "ThongKeTKXNK";
            explorerBarItem33.Text = "Thống kê tờ khai XNK";
            explorerBarItem34.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem34.Image")));
            explorerBarItem34.Key = "BaoCaoTongHopHHXNK";
            explorerBarItem34.Text = "Báo cáo Tổng hợp hàng hóa xuất nhập khẩu";
            explorerBarItem35.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem35.Image")));
            explorerBarItem35.Key = "BaoCaoChiTietHHXNK";
            explorerBarItem35.Text = "Báo cáo Chi tiết hàng hóa xuất nhập khẩu";
            explorerBarGroup15.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem33,
            explorerBarItem34,
            explorerBarItem35});
            explorerBarGroup15.Key = "grpTKMD";
            explorerBarGroup15.Text = "Tờ khai mậu dịch V4";
            this.explorerBarBCTongHopDuLieu.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup13,
            explorerBarGroup14,
            explorerBarGroup15});
            this.explorerBarBCTongHopDuLieu.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarBCTongHopDuLieu.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBarBCTongHopDuLieu.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBarBCTongHopDuLieu.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBarBCTongHopDuLieu.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBarBCTongHopDuLieu.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBarBCTongHopDuLieu.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBarBCTongHopDuLieu.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarBCTongHopDuLieu.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBarBCTongHopDuLieu.Location = new System.Drawing.Point(0, 0);
            this.explorerBarBCTongHopDuLieu.Name = "explorerBarBCTongHopDuLieu";
            this.explorerBarBCTongHopDuLieu.Size = new System.Drawing.Size(236, 268);
            this.explorerBarBCTongHopDuLieu.TabIndex = 3;
            this.explorerBarBCTongHopDuLieu.Text = "explorerBar4";
            this.explorerBarBCTongHopDuLieu.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBarBCTongHopDuLieu.VisualStyleManager = this.vsmMain;
            this.explorerBarBCTongHopDuLieu.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarBCTongHopDuLieu_ItemClick);
            // 
            // uiPanel6
            // 
            this.uiPanel6.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel6.Image")));
            this.uiPanel6.InnerContainer = this.uiPanel6Container;
            this.uiPanel6.Location = new System.Drawing.Point(0, 0);
            this.uiPanel6.Name = "uiPanel6";
            this.uiPanel6.Size = new System.Drawing.Size(238, 299);
            this.uiPanel6.TabIndex = 4;
            this.uiPanel6.Text = "Kho kế toán";
            // 
            // uiPanel6Container
            // 
            this.uiPanel6Container.Controls.Add(this.explorerBarKhoKeToan);
            this.uiPanel6Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel6Container.Name = "uiPanel6Container";
            this.uiPanel6Container.Size = new System.Drawing.Size(236, 268);
            this.uiPanel6Container.TabIndex = 0;
            // 
            // explorerBarKhoKeToan
            // 
            this.explorerBarKhoKeToan.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarKhoKeToan.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarKhoKeToan.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBarKhoKeToan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBarKhoKeToan.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem36.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem36.Image")));
            explorerBarItem36.Key = "cmdCauHinh";
            explorerBarItem36.Text = "Cấu hình";
            explorerBarGroup16.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem36});
            explorerBarGroup16.Key = "grpCauHinh";
            explorerBarGroup16.Text = "Cấu hình thông số mặc định";
            explorerBarItem37.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem37.Image")));
            explorerBarItem37.ImageIndex = 0;
            explorerBarItem37.Key = "cmdDanhSachKho";
            explorerBarItem37.Text = "Thêm mới";
            explorerBarItem38.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem38.Image")));
            explorerBarItem38.ImageIndex = 1;
            explorerBarItem38.Key = "cmdTheoDoiKho";
            explorerBarItem38.Text = "Theo dõi";
            explorerBarGroup17.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem37,
            explorerBarItem38});
            explorerBarGroup17.Key = "grpKho";
            explorerBarGroup17.Text = "Danh sách kho";
            explorerBarItem39.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem39.Image")));
            explorerBarItem39.Key = "cmdKhoNPL";
            explorerBarItem39.Text = "Danh mục Nguyên phụ liệu";
            explorerBarItem40.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem40.Image")));
            explorerBarItem40.Key = "cmdKhoSanPham";
            explorerBarItem40.Text = "Danh mục Sản phẩm";
            explorerBarItem41.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem41.Image")));
            explorerBarItem41.Key = "cmdKhoThietBi";
            explorerBarItem41.Text = "Danh mục Thiết bị";
            explorerBarItem42.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem42.Image")));
            explorerBarItem42.Key = "cmdKhoHangMau";
            explorerBarItem42.Text = "Danh mục Hàng mẫu";
            explorerBarGroup18.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem39,
            explorerBarItem40,
            explorerBarItem41,
            explorerBarItem42});
            explorerBarGroup18.Key = "grpDanhMucKho";
            explorerBarGroup18.Text = "Danh mục";
            explorerBarItem43.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem43.Image")));
            explorerBarItem43.Key = "cmdPhieuNhapKho";
            explorerBarItem43.Text = "Tạo mới";
            explorerBarItem44.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem44.Image")));
            explorerBarItem44.Key = "cmdTheoDoiPNK";
            explorerBarItem44.Text = "Theo dõi";
            explorerBarGroup19.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem43,
            explorerBarItem44});
            explorerBarGroup19.Key = "grpPhieuNhapKho";
            explorerBarGroup19.Text = "Phiếu Nhập kho";
            explorerBarItem45.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem45.Image")));
            explorerBarItem45.Key = "cmdPhieuXuatKho";
            explorerBarItem45.Text = "Tạo mới";
            explorerBarItem46.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem46.Image")));
            explorerBarItem46.Key = "cmdTheoDoiPXK";
            explorerBarItem46.Text = "Theo dõi";
            explorerBarGroup20.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem45,
            explorerBarItem46});
            explorerBarGroup20.Key = "grpPhieuXuatKho";
            explorerBarGroup20.Text = "Phiếu Xuất kho";
            explorerBarItem47.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem47.Image")));
            explorerBarItem47.Key = "cmdThemMoiDM";
            explorerBarItem47.Text = "Thêm mới";
            explorerBarItem48.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem48.Image")));
            explorerBarItem48.Key = "cmdTheoDoiDinhMuc";
            explorerBarItem48.Text = "Theo dõi định mức";
            explorerBarItem49.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem49.Image")));
            explorerBarItem49.Key = "cmdTinhToan";
            explorerBarItem49.Text = "Tính toán định mức";
            explorerBarGroup21.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem47,
            explorerBarItem48,
            explorerBarItem49});
            explorerBarGroup21.Key = "grpDinhMuc";
            explorerBarGroup21.Text = "Định mức";
            explorerBarItem50.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem50.Image")));
            explorerBarItem50.Key = "cmdCapNhatTonDK";
            explorerBarItem50.Text = "Cập nhật Tồn đầu kỳ";
            explorerBarItem51.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem51.Image")));
            explorerBarItem51.Key = "cmdTheKho";
            explorerBarItem51.Text = "Báo cáo Thẻ kho";
            explorerBarItem52.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem52.Image")));
            explorerBarItem52.Key = "cmdBaoCaoQT";
            explorerBarItem52.Text = "Báo cáo Quyết toán";
            explorerBarGroup22.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem50,
            explorerBarItem51,
            explorerBarItem52});
            explorerBarGroup22.Key = "grpBaoCao";
            explorerBarGroup22.Text = "Báo cáo Kho kế toán";
            this.explorerBarKhoKeToan.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup16,
            explorerBarGroup17,
            explorerBarGroup18,
            explorerBarGroup19,
            explorerBarGroup20,
            explorerBarGroup21,
            explorerBarGroup22});
            this.explorerBarKhoKeToan.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarKhoKeToan.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBarKhoKeToan.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBarKhoKeToan.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBarKhoKeToan.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBarKhoKeToan.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBarKhoKeToan.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBarKhoKeToan.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBarKhoKeToan.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBarKhoKeToan.Location = new System.Drawing.Point(0, 0);
            this.explorerBarKhoKeToan.Name = "explorerBarKhoKeToan";
            this.explorerBarKhoKeToan.Size = new System.Drawing.Size(236, 268);
            this.explorerBarKhoKeToan.TabIndex = 2;
            this.explorerBarKhoKeToan.Text = "explorerBar2";
            this.explorerBarKhoKeToan.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBarKhoKeToan.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarKhoKeToan_ItemClick);
            // 
            // pnlSXXK
            // 
            this.pnlSXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlSXXK.Icon")));
            this.pnlSXXK.InnerContainer = this.pnlSXXKContainer;
            this.pnlSXXK.Location = new System.Drawing.Point(0, 0);
            this.pnlSXXK.Name = "pnlSXXK";
            this.pnlSXXK.Size = new System.Drawing.Size(209, 335);
            this.pnlSXXK.TabIndex = 4;
            this.pnlSXXK.Text = "Loại hình SXXK";
            // 
            // pnlSXXKContainer
            // 
            this.pnlSXXKContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlSXXKContainer.Name = "pnlSXXKContainer";
            this.pnlSXXKContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlSXXKContainer.TabIndex = 0;
            // 
            // pnlGiaCong
            // 
            this.pnlGiaCong.Closed = true;
            this.pnlGiaCong.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlGiaCong.Icon")));
            this.pnlGiaCong.InnerContainer = this.pnlGiaCongContainer;
            this.pnlGiaCong.Location = new System.Drawing.Point(0, 0);
            this.pnlGiaCong.Name = "pnlGiaCong";
            this.pnlGiaCong.Size = new System.Drawing.Size(209, 335);
            this.pnlGiaCong.TabIndex = 4;
            this.pnlGiaCong.Text = "Loại hình gia công";
            // 
            // pnlGiaCongContainer
            // 
            this.pnlGiaCongContainer.Controls.Add(this.expGiaCong);
            this.pnlGiaCongContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlGiaCongContainer.Name = "pnlGiaCongContainer";
            this.pnlGiaCongContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlGiaCongContainer.TabIndex = 0;
            // 
            // expGiaCong
            // 
            this.expGiaCong.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expGiaCong.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expGiaCong.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expGiaCong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expGiaCong.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem53.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem53.Icon")));
            explorerBarItem53.Key = "hdgcNhap";
            explorerBarItem53.Text = "Khai báo";
            explorerBarItem54.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem54.Icon")));
            explorerBarItem54.Key = "hdgcManage";
            explorerBarItem54.Text = "Theo dõi";
            explorerBarItem55.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem55.Icon")));
            explorerBarItem55.Key = "hdgcRegisted";
            explorerBarItem55.Text = "Đã đăng ký";
            explorerBarGroup23.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem53,
            explorerBarItem54,
            explorerBarItem55});
            explorerBarGroup23.Key = "grpHopDong";
            explorerBarGroup23.Text = "Hợp đồng";
            explorerBarItem56.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem56.Icon")));
            explorerBarItem56.Key = "dmSend";
            explorerBarItem56.Text = "Khai báo";
            explorerBarItem57.Key = "dmManage";
            explorerBarItem57.Text = "Theo dõi";
            explorerBarItem58.Key = "dmRegisted";
            explorerBarItem58.Text = "Đã đăng ký";
            explorerBarGroup24.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem56,
            explorerBarItem57,
            explorerBarItem58});
            explorerBarGroup24.Key = "grpDinhMuc";
            explorerBarGroup24.Text = "Định mức";
            explorerBarItem59.Key = "pkgcNhap";
            explorerBarItem59.Text = "Khai báo";
            explorerBarItem60.Key = "pkgcManage";
            explorerBarItem60.Text = "Theo dõi";
            explorerBarItem61.Key = "pkgcRegisted";
            explorerBarItem61.Text = "Đã đăng ký";
            explorerBarGroup25.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem59,
            explorerBarItem60,
            explorerBarItem61});
            explorerBarGroup25.Key = "grpPhuKien";
            explorerBarGroup25.Text = "Phụ kiện";
            explorerBarItem62.Key = "tkNhapKhau_GC";
            explorerBarItem62.Text = "Nhập khẩu";
            explorerBarItem63.Key = "tkXuatKhau_GC";
            explorerBarItem63.Text = "Xuất khẩu";
            explorerBarGroup26.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem62,
            explorerBarItem63});
            explorerBarGroup26.Key = "grpToKhai";
            explorerBarGroup26.Text = "Tờ khai";
            explorerBarItem64.Key = "tkGCCTNhap";
            explorerBarItem64.Text = "Tờ khai GCCT nhập";
            explorerBarItem65.Key = "tkGCCTXuat";
            explorerBarItem65.Text = "Tờ khai GCCT xuất";
            explorerBarItem66.Key = "theodoiTKCT";
            explorerBarItem66.Text = "Theo dõi";
            explorerBarGroup27.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem64,
            explorerBarItem65,
            explorerBarItem66});
            explorerBarGroup27.Key = "grpGCCT";
            explorerBarGroup27.Text = "Tờ khai GCCT";
            this.expGiaCong.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup23,
            explorerBarGroup24,
            explorerBarGroup25,
            explorerBarGroup26,
            explorerBarGroup27});
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expGiaCong.ImageSize = new System.Drawing.Size(16, 16);
            this.expGiaCong.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expGiaCong.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expGiaCong.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expGiaCong.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expGiaCong.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expGiaCong.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expGiaCong.Location = new System.Drawing.Point(0, 0);
            this.expGiaCong.Name = "expGiaCong";
            this.expGiaCong.Size = new System.Drawing.Size(209, 335);
            this.expGiaCong.TabIndex = 1;
            this.expGiaCong.Text = "explorerBar2";
            this.expGiaCong.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expGiaCong.VisualStyleManager = this.vsmMain;
            this.expGiaCong.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expGiaCong_ItemClick);
            // 
            // pnlKinhDoanh
            // 
            this.pnlKinhDoanh.Closed = true;
            this.pnlKinhDoanh.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlKinhDoanh.Icon")));
            this.pnlKinhDoanh.InnerContainer = this.pnlKinhDoanhContainer;
            this.pnlKinhDoanh.Location = new System.Drawing.Point(0, 0);
            this.pnlKinhDoanh.Name = "pnlKinhDoanh";
            this.pnlKinhDoanh.Size = new System.Drawing.Size(209, 335);
            this.pnlKinhDoanh.TabIndex = 4;
            this.pnlKinhDoanh.Text = "Loại hình kinh doanh";
            // 
            // pnlKinhDoanhContainer
            // 
            this.pnlKinhDoanhContainer.Controls.Add(this.expKD);
            this.pnlKinhDoanhContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlKinhDoanhContainer.Name = "pnlKinhDoanhContainer";
            this.pnlKinhDoanhContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlKinhDoanhContainer.TabIndex = 0;
            // 
            // expKD
            // 
            this.expKD.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expKD.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKD.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expKD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expKD.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem67.Key = "tkNhapKhau_KD";
            explorerBarItem67.Text = "Nhập khẩu";
            explorerBarItem68.Key = "tkXuatKhau_KD";
            explorerBarItem68.Text = "Xuất khẩu";
            explorerBarGroup28.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem67,
            explorerBarItem68});
            explorerBarGroup28.Key = "grpToKhai";
            explorerBarGroup28.Text = "Tờ khai";
            this.expKD.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup28});
            this.expKD.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expKD.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expKD.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expKD.ImageSize = new System.Drawing.Size(16, 16);
            this.expKD.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expKD.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expKD.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expKD.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expKD.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKD.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expKD.Location = new System.Drawing.Point(0, 0);
            this.expKD.Name = "expKD";
            this.expKD.Size = new System.Drawing.Size(209, 335);
            this.expKD.TabIndex = 1;
            this.expKD.Text = "explorerBar1";
            this.expKD.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expKD.VisualStyleManager = this.vsmMain;
            this.expKD.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expKD_ItemClick);
            // 
            // pnlDauTu
            // 
            this.pnlDauTu.Closed = true;
            this.pnlDauTu.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlDauTu.Icon")));
            this.pnlDauTu.InnerContainer = this.pnlDauTuContainer;
            this.pnlDauTu.Location = new System.Drawing.Point(0, 0);
            this.pnlDauTu.Name = "pnlDauTu";
            this.pnlDauTu.Size = new System.Drawing.Size(209, 335);
            this.pnlDauTu.TabIndex = 4;
            this.pnlDauTu.Text = "Loại hình đầu tư";
            // 
            // pnlDauTuContainer
            // 
            this.pnlDauTuContainer.Controls.Add(this.expDT);
            this.pnlDauTuContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlDauTuContainer.Name = "pnlDauTuContainer";
            this.pnlDauTuContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlDauTuContainer.TabIndex = 0;
            // 
            // expDT
            // 
            this.expDT.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expDT.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expDT.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expDT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expDT.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem69.Key = "tkNhapKhau_DT";
            explorerBarItem69.Text = "Nhập khẩu";
            explorerBarItem70.Key = "tkXuatKhau_DT";
            explorerBarItem70.Text = "Xuất khẩu";
            explorerBarGroup36.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem69,
            explorerBarItem70});
            explorerBarGroup36.Key = "grpToKhai";
            explorerBarGroup36.Text = "Tờ khai";
            this.expDT.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup36});
            this.expDT.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expDT.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expDT.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expDT.ImageSize = new System.Drawing.Size(16, 16);
            this.expDT.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expDT.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expDT.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expDT.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expDT.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expDT.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expDT.Location = new System.Drawing.Point(0, 0);
            this.expDT.Name = "expDT";
            this.expDT.Size = new System.Drawing.Size(209, 335);
            this.expDT.TabIndex = 2;
            this.expDT.Text = "explorerBar1";
            this.expDT.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expDT.VisualStyleManager = this.vsmMain;
            this.expDT.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expDT_ItemClick);
            // 
            // pnlSend
            // 
            this.pnlSend.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlSend.Icon")));
            this.pnlSend.InnerContainer = this.pnlSendContainer;
            this.pnlSend.Location = new System.Drawing.Point(0, 0);
            this.pnlSend.Name = "pnlSend";
            this.pnlSend.Size = new System.Drawing.Size(209, 335);
            this.pnlSend.TabIndex = 4;
            this.pnlSend.Text = "Khai báo / Theo dõi tờ khai";
            // 
            // pnlSendContainer
            // 
            this.pnlSendContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlSendContainer.Name = "pnlSendContainer";
            this.pnlSendContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlSendContainer.TabIndex = 0;
            // 
            // ilMedium
            // 
            this.ilMedium.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilMedium.ImageStream")));
            this.ilMedium.TransparentColor = System.Drawing.Color.Transparent;
            this.ilMedium.Images.SetKeyName(0, "");
            this.ilMedium.Images.SetKeyName(1, "");
            this.ilMedium.Images.SetKeyName(2, "");
            this.ilMedium.Images.SetKeyName(3, "");
            // 
            // ilLarge
            // 
            this.ilLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLarge.ImageStream")));
            this.ilLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLarge.Images.SetKeyName(0, "");
            this.ilLarge.Images.SetKeyName(1, "");
            this.ilLarge.Images.SetKeyName(2, "");
            this.ilLarge.Images.SetKeyName(3, "");
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 627);
            this.statusBar.Name = "statusBar";
            uiStatusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel1.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiStatusBarPanel1.Icon")));
            uiStatusBarPanel1.Key = "DoanhNghiep";
            uiStatusBarPanel1.ProgressBarValue = 0;
            uiStatusBarPanel1.Width = 174;
            uiStatusBarPanel2.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel2.Key = "HaiQuan";
            uiStatusBarPanel2.ProgressBarValue = 0;
            uiStatusBarPanel2.ToggleKeyValue = Janus.Windows.UI.StatusBar.ToggleKeyValue.NumLock;
            uiStatusBarPanel2.Width = 200;
            uiStatusBarPanel3.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel3.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel3.FormatStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiStatusBarPanel3.FormatStyle.ForeColor = System.Drawing.Color.Red;
            uiStatusBarPanel3.Image = ((System.Drawing.Image)(resources.GetObject("uiStatusBarPanel3.Image")));
            uiStatusBarPanel3.Key = "Support";
            uiStatusBarPanel3.ProgressBarValue = 0;
            uiStatusBarPanel3.Text = "   GỬI YÊU CẦU HỖ TRỢ";
            uiStatusBarPanel3.Width = 173;
            uiStatusBarPanel4.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            uiStatusBarPanel4.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel4.Key = "Terminal";
            uiStatusBarPanel4.ProgressBarValue = 0;
            uiStatusBarPanel4.Text = "Terminal ID: ?";
            uiStatusBarPanel4.Width = 81;
            uiStatusBarPanel5.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel5.Icon = ((System.Drawing.Icon)(resources.GetObject("uiStatusBarPanel5.Icon")));
            uiStatusBarPanel5.Key = "Service";
            uiStatusBarPanel5.ProgressBarValue = 0;
            uiStatusBarPanel6.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            uiStatusBarPanel6.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel6.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            uiStatusBarPanel6.Key = "Version";
            uiStatusBarPanel6.ProgressBarValue = 0;
            uiStatusBarPanel6.Text = "V?";
            uiStatusBarPanel6.Width = 30;
            uiStatusBarPanel7.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            uiStatusBarPanel7.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel7.Key = "CKS";
            uiStatusBarPanel7.ProgressBarValue = 0;
            uiStatusBarPanel7.Text = "CKS";
            uiStatusBarPanel7.Width = 30;
            uiStatusBarPanel8.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel8.Key = "DateTime";
            uiStatusBarPanel8.ProgressBarValue = 0;
            uiStatusBarPanel8.Width = 127;
            this.statusBar.Panels.AddRange(new Janus.Windows.UI.StatusBar.UIStatusBarPanel[] {
            uiStatusBarPanel1,
            uiStatusBarPanel2,
            uiStatusBarPanel3,
            uiStatusBarPanel4,
            uiStatusBarPanel5,
            uiStatusBarPanel6,
            uiStatusBarPanel7,
            uiStatusBarPanel8});
            this.statusBar.PanelsBorderColor = System.Drawing.SystemColors.ControlDark;
            this.statusBar.Size = new System.Drawing.Size(950, 23);
            this.statusBar.TabIndex = 7;
            this.statusBar.VisualStyleManager = this.vsmMain;
            // 
            // cmdThoat3
            // 
            this.cmdThoat3.Key = "cmdThoat";
            this.cmdThoat3.Name = "cmdThoat3";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // vlECS
            // 
            this.vlECS.ExpiresInDays = 30;
            this.vlECS.ExpiresOnDate = "5/5/2009 12:00:00 AM";
            this.vlECS.NumberOfTries = 30;
            this.vlECS.RegSubKey = "SOFTWARE\\ECS_SXXK";
            this.vlECS.TrialType = SoftechVersion.TRIAL_TYPE.ttEXPIRES_ON_DATE;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // cmdThoat4
            // 
            this.cmdThoat4.Key = "cmdThoat";
            this.cmdThoat4.Name = "cmdThoat4";
            // 
            // cmdNhapXuat1
            // 
            this.cmdNhapXuat1.ImageIndex = 45;
            this.cmdNhapXuat1.Key = "cmdNhapXuat";
            this.cmdNhapXuat1.Name = "cmdNhapXuat1";
            // 
            // cmdUpdateDatabase1
            // 
            this.cmdUpdateDatabase1.Key = "cmdUpdateDatabase";
            this.cmdUpdateDatabase1.Name = "cmdUpdateDatabase1";
            // 
            // uiPanel3
            // 
            this.uiPanel3.Location = new System.Drawing.Point(3, 3);
            this.uiPanel3.Name = "uiPanel3";
            this.uiPanel3.Size = new System.Drawing.Size(200, 256);
            this.uiPanel3.TabIndex = 4;
            this.uiPanel3.Text = "VNACCS - CHỨNG TỪ";
            // 
            // cmdPerformanceDatabase
            // 
            this.cmdPerformanceDatabase.Image = ((System.Drawing.Image)(resources.GetObject("cmdPerformanceDatabase.Image")));
            this.cmdPerformanceDatabase.Key = "cmdPerformanceDatabase";
            this.cmdPerformanceDatabase.Name = "cmdPerformanceDatabase";
            this.cmdPerformanceDatabase.Text = "Tối ưu hoá cơ sở dữ liệu";
            // 
            // cmdPerformanceDatabase1
            // 
            this.cmdPerformanceDatabase1.Key = "cmdPerformanceDatabase";
            this.cmdPerformanceDatabase1.Name = "cmdPerformanceDatabase1";
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(950, 650);
            this.Controls.Add(this.uiPanel0);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "THÔNG QUAN ĐIỆN TỬ - SẢN XUẤT XUẤT KHẨU 5.0";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.statusBar, 0);
            this.Controls.SetChildIndex(this.uiPanel0, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mnuRightClick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expSXXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).EndInit();
            this.uiPanel2.ResumeLayout(false);
            this.uiPanelContainerVNACCTK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expKhaiBao_TheoDoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelVNACCToKhai)).EndInit();
            this.uiPanelVNACCToKhai.ResumeLayout(false);
            this.uiPanel3Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_TKMD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelVNACCGiayPhep)).EndInit();
            this.uiPanelVNACCGiayPhep.ResumeLayout(false);
            this.uiPanelContainerVNACCGP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_GiayPhep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelVNACCHoaDon)).EndInit();
            this.uiPanelVNACCHoaDon.ResumeLayout(false);
            this.uiPanelContainerVNACCHD.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarVNACCS_HoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel4)).EndInit();
            this.uiPanel4.ResumeLayout(false);
            this.uiPanel4Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel5)).EndInit();
            this.uiPanel5.ResumeLayout(false);
            this.uiPanel5Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarBCTongHopDuLieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel6)).EndInit();
            this.uiPanel6.ResumeLayout(false);
            this.uiPanel6Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarKhoKeToan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSXXK)).EndInit();
            this.pnlSXXK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiaCong)).EndInit();
            this.pnlGiaCong.ResumeLayout(false);
            this.pnlGiaCongContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKinhDoanh)).EndInit();
            this.pnlKinhDoanh.ResumeLayout(false);
            this.pnlKinhDoanhContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expKD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDauTu)).EndInit();
            this.pnlDauTu.ResumeLayout(false);
            this.pnlDauTuContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSend)).EndInit();
            this.pnlSend.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UICommand cmdThoat3;
        private UIPanelInnerContainer pnlDauTuContainer;
        private ExplorerBar expDT;
        private UIPanel pnlSend;
        private UIPanelInnerContainer pnlSendContainer;
        private NotifyIcon notifyIcon1;
        private BackgroundWorker backgroundWorker1;
        private UICommand NhacNho1;
        private UICommand NhacNho;
        private UICommand DongBoDuLieu;
        private UICommand cmdImport;
        private UICommand cmdImportNPL1;
        private UICommand cmdImportSP1;
        private UICommand cmdImportNPL;
        private UICommand cmdImportSP;
        private UICommand cmdImportDM1;
        private UICommand cmdImportDM;
        private UICommand cmdImportToKhai1;
        private UICommand cmdImportTTDM;
        private UICommand cmdImportToKhai;
        private UICommand cmdImportHangHoa1;
        private UICommand cmdImportHangHoa;
        private UICommand Command01;
        private UICommand Command0;
        private UICommand cmdHelp1;
        private UICommand cmdAbout1;
        private UICommand cmdHelp;
        private UICommand cmdAbout;
        private UICommand Command11;
        private UICommand Command1;
        private UICommand cmd20071;
        private UICommand cmd2007;
        private UICommand cmd2003;
        private UICommand cmd20031;
        private UICommand cmdNPLNhapTon;
        private UICommand cmdDanhMuc1;
        private UICommand cmdDanhMuc;
        private UICommand cmdMaHS;
        private UICommand cmdHaiQuan1;
        private UICommand cmdNuoc1;
        private UICommand cmdMaHS1;
        private UICommand cmdNguyenTe1;
        private UICommand cmdDVT1;
        private UICommand cmdPTTT1;
        private UICommand cmdPTVT1;
        private UICommand cmdDKGH1;
        private UICommand cmdCuaKhau1;
        private UICommand cmdNuoc;
        private UICommand cmdHaiQuan;
        private UICommand cmdNguyenTe;
        private UICommand cmdDVT;
        private UICommand cmdPTTT;
        private UICommand cmdPTVT;
        private UICommand cmdDKGH;
        private UICommand cmdCuaKhau;
        private UICommand cmdBackUp;
        private UICommand cmdRestore;
        private UICommand ThongSoKetNoi;
        private UICommand TLThongTinDNHQ;
        private UICommand cmdThietLapIn;
        private UICommand cmdExportExccel;
        private UICommand cmdExportExcel1;
        private UICommand cmdImportExcel;
        private UICommand cmdDongBoPhongKhai;
        private UICommand cmdImportExcel1;
        private UICommand cmdExportExccel1;
        private UICommand cmdCauHinh1;
        private UICommand cmdCauHinh;
        private UICommand ThongSoKetNoi1;
        private UICommand TLThongTinDNHQ1;
        private UICommand cmdCauHinhToKhai1;
        private UICommand cmdThietLapIn1;
        private UICommand cmdCauHinhToKhai;
        public Janus.Windows.UI.StatusBar.UIStatusBar statusBar;
        private UICommand QuanTri1;
        private UICommand QuanTri;
        private UICommand QuanLyNguoiDung1;
        private UICommand QuanLyNhom1;
        private UICommand QuanLyNguoiDung;
        private UICommand QuanLyNhom;
        private UICommand LoginUser1;
        private UICommand LoginUser;
        private UICommand cmdChangePass1;
        private UICommand cmdChangePass;
        private UICommand cmdDoiMatKhauHQ1;
        private UICommand cmdDoiMatKhauHQ;
        private UICommand cmdCuaSo1;
        private UICommand cmdCuaSo;
        private UICommand cmdCloseAll1;
        private UICommand cmdCloseAllButThis1;
        private UICommand cmdCloseAll;
        private UICommand cmdCloseAllButMe;
        private UIPanelGroup uiPanel0;
        private UIPanel uiPanel2;
        private UIPanelInnerContainer uiPanelContainerVNACCTK;
        private ExplorerBar expKhaiBao_TheoDoi;
        private UIPanel uiPanel1;
        private UIPanelInnerContainer uiPanel1Container;
        private ExplorerBar expSXXK;
        private UICommand Separator1;
        private UICommand Separator3;
        private UICommand Separator4;
        private UICommand cmdBackUp1;
        private SoftechVersion.ValidVersion vlECS;
        private UICommand cmdAutoUpdate1;
        private UICommand cmdAutoUpdate;
        private UICommand cmdNhapXML;
        private UICommand cmdNPL1;
        private UICommand cmdSP1;
        private UICommand cmdDM1;
        private UICommand cmdTK1;
        private UICommand cmdNPL;
        private UICommand cmdSP;
        private UICommand cmdDM;
        private UICommand cmdTK;
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private UICommand cmdXuatDuLieu;
        private UICommand cmdXuatNPL;
        private UICommand cmdXuatDinhMuc;
        private UICommand cmdXuatSanPham;
        private UICommand cmdXuatToKhai;
        private UICommand cmdVN;
        private UICommand cmdEL;
        private UICommand cmdVN1;
        private UICommand cmdEL1;
        private UICommand cmdHUNGNPL;
        private UICommand cmdHUNGNPL1;
        private UICommand cmdHUNGSP1;
        private UICommand cmdHUNGDM1;
        private UICommand cmdHUNGSP;
        private UICommand cmdHUNGDM;
        private UICommand cmdHUNGTK;
        private UICommand cmdHUNGTK1;
        private UICommand Separator5;
        private UICommand cmdActivate1;
        private UICommand cmdActivate;
        private System.Windows.Forms.Timer timer1;
        private UICommand cmdXuatDuLieuDN;
        private UICommand cmdXuatNPLDN;
        private UICommand cmdXuatSPDN;
        private UICommand cmXuat;
        private UICommand cmdXuatNPLDN1;
        private UICommand cmdXuatSPDN1;
        private UICommand cmdXuatDMDN1;
        private UICommand cmdXuatTKDN1;
        private UICommand cmdXuatDMDN;
        private UICommand cmdXuatTKDN;
        private UICommand cmdNPLNhapTon1;
        private UICommand cmdNhapDuLieu;
        private UICommand cmdNhapNPL1;
        private UICommand cmdNhapSanPham1;
        private UICommand cmdNhapDinhMuc1;
        private UICommand cmdNhapToKhai1;
        private UICommand cmdNhapNPL;
        private UICommand cmdNhapSanPham;
        private UICommand cmdNhapDinhMuc;
        private UICommand cmdNhapToKhai;
        private UICommand QuanlyMess;
        private UICommand cmdConfig;
        private UICommand cmdConfig1;
        private UICommand cmdThoat4;
        private UICommand mnuQuerySQL1;
        private UICommand mnuQuerySQL;
        private UICommand TraCuuMaHS;
        private UICommand cmdNhomCuaKhau1;
        private UICommand cmdNhomCuaKhau;
        private UICommand cmdLog1;
        private UICommand cmdLog;
        private UICommand cmdLoaiPhiChungTuThanhToan1;
        private UICommand cmdLoaiPhiChungTuThanhToan;
        private UICommand cmdDataVersion;
        private UICommand cmdDataVersion1;
        private UICommand cmdImportXml;
        private UICommand cmdChuKySo;
        private UICommand cmdChuKySo1;
        private UICommand cmdTimer1;
        private UICommand cmdTimer;
        private UICommand mnuFormSQL;
        private UICommand mnuMiniSQL;
        private UICommand mnuFormSQL1;
        private UICommand cmdNhapXuat;
        private UICommand cmdNhapXuat1;
        private UICommand cmdBieuThue;
        private UICommand cmdTraCuuXNKOnline;
        private UICommand cmdTraCuuNoThueOnline;
        private UICommand cmdTraCuuVanBanOnline;
        private UICommand cmdTuVanHQOnline;
        private UICommand cmdBieuThue1;
        private UICommand cmdGetCategoryOnline1;
        private UICommand Separator6;
        private UICommand TraCuuMaHS1;
        private UICommand Separator7;
        private UICommand cmdTraCuuXNKOnline1;
        private UICommand cmdTraCuuNoThueOnline1;
        private UICommand cmdTraCuuVanBanOnline1;
        private UICommand cmdTuVanHQOnline1;
        private UICommand cmdGetCategoryOnline;
        private UICommand cmdNhapXuat2;
        private UICommand cmdImportExcel2;
        private UICommand cmdImportXml1;
        private UICommand cmdNhapXML1;
        private UICommand cmdNhapDuLieu1;
        private UICommand Separator8;
        private UICommand cmdXuatDuLieu1;
        private UICommand cmdXuatDuLieuDN1;
        private UICommand cmdTeamview1;
        private UICommand Separator2;
        private UICommand cmdTeamview;
        private UICommand cmdCapNhatHS;
        private UICommand cmdCapNhatHS1;
        private UICommand Separator9;
        private UICommand Separator10;
        private UICommand cmdCapNhatHS8SoAuto1;
        private UICommand cmdCapNhatHS8SoManual1;
        private UICommand cmdCapNhatHS8SoAuto;
        private UICommand cmdCapNhatHS8SoManual;
        private UICommand cmdTool1;
        private UICommand cmdTool;
        private UICommand cmdImageResizeHelp1;
        private UICommand cmdImageResizeHelp;
        private UICommand cmbDongBoDuLieu;
        private UICommand cmbDongBoDuLieu1;
        private UICommand DongBoDuLieu1;
        private UICommand cmdQuanLyDL1;
        private UICommand cmdQuanLyDL;
        private UICommand cmdDaily1;
        private UICommand Separator12;
        private UICommand cmdDaily;
        private UICommand cmdUpdateDatabase;
        private UICommand cmdUpdateDatabase1;
        private UICommand cmdUpdateDatabase2;
        private UIContextMenu mnuRightClick;
        private UICommand cmdCloseMe;
        private UICommand cmdCloseMe1;
        private UICommand cmdCloseAllButMe1;
        private UIPanel uiPanelVNACCToKhai;
        private UIPanelInnerContainer uiPanel3Container;
        private UIPanel uiPanelVNACCGiayPhep;
        private UIPanelInnerContainer uiPanelContainerVNACCGP;
        private UIPanel uiPanelVNACCHoaDon;
        private UIPanelInnerContainer uiPanelContainerVNACCHD;
        private ExplorerBar explorerBarVNACCS_TKMD;
        private ExplorerBar explorerBarVNACCS_GiayPhep;
        private ExplorerBar explorerBarVNACCS_HoaDon;
        private UICommand cmdHelpVideo1;
        private UICommand Separator13;
        private UICommand cmdHelpVideo;
        private UICommand cmdHDSDCKS1;
        private UICommand cmdHDSDCKS;
        private UICommand cmdHDSDVNACCS1;
        private UICommand cmdHDSDVNACCS;
        private UICommand cmdThongBaoVNACCS1;
        private UICommand cmdThongBaoVNACCS;
        private UIPanel uiPanel3;
        private UIPanel uiPanel4;
        private UIPanelInnerContainer uiPanel4Container;
        private ExplorerBar explorerBar1;
        private UICommand cmdSignFile1;
        private UICommand cmdSignFile;
        private UICommand cmdHelpSignFile1;
        private UICommand cmdHelpSignFile;
        private UICommand cmdBerth;
        private UICommand cmdCargo;
        private UICommand cmdCityUNLOCODE;
        private UICommand cmdCommon;
        private UICommand cmdContainerSize;
        private UICommand cmdCustomsSubSection;
        private UICommand cmdOGAUser;
        private UICommand cmdPackagesUnit;
        private UICommand cmdStations;
        private UICommand cmdTaxClassificationCode;
        private UICommand cmdBerth1;
        private UICommand cmdCargo1;
        private UICommand cmdCityUNLOCODE1;
        private UICommand cmdCommon1;
        private UICommand cmdContainerSize1;
        private UICommand cmdCustomsSubSection1;
        private UICommand cmdOGAUser1;
        private UICommand cmdPackagesUnit1;
        private UICommand cmdStations1;
        private UICommand cmdTaxClassificationCode1;
        private UICommand cmdBieuThueXNK2018;
        private UICommand cmdBieuThueXNK20181;
        private UICommand cmdNhanPhanHoi1;
        private UICommand cmdUpdateCategoryOnline1;
        private UICommand cmdUpdateCategoryOnline;
        private UICommand cmdNhanPhanHoi;
        private UICommand cmdReloadData;
        private UICommand cmdReloadData1;
        private UIPanel uiPanel5;
        private UIPanelInnerContainer uiPanel5Container;
        private ExplorerBar explorerBarBCTongHopDuLieu;
        private UIPanel uiPanel6;
        private UIPanelInnerContainer uiPanel6Container;
        private ExplorerBar explorerBarKhoKeToan;
        private UICommand cmdConfigReadExcel;
        private UICommand cmdConfigReadExcel1;
        private UICommand cmdPerformanceDatabase;
        private UICommand cmdPerformanceDatabase1;
    }
}
