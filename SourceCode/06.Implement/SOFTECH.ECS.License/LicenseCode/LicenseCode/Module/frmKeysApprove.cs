using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KeyServer;
using KeySecurity;
using KeyServer.User;

namespace LicenseCode
{
    public partial class frmKeysApprove : DevExpress.XtraEditors.XtraForm
    {
        public frmKeysApprove()
        {
            InitializeComponent();
        }
        public List<Key> listKey = new List<Key>();
        private ProductKey.ProductKey productServices = new ProductKey.ProductKey();
        public User User = new User();
        private void frmKeysApprove_Load(object sender, EventArgs e)
        {
            productServices.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            productServices.Timeout = 60000;
            LoadData();
          
        }
        private void LoadData()
        {
            string result;
            try
            {
                result = productServices.SeachKey(User.UserName, KeyCode.ConvertToBase64(User.PassWord), "-1", false, new DateTime(1900, 1, 1), DateTime.MaxValue, string.Empty);
                if (string.IsNullOrEmpty(result))
                {
                    listKey = new List<Key>();
                    gridControl1.DataSource = listKey;
                    return;
                }
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Lỗi kết nối" + ex.Message);
                return;
            }
            try
            {
                listKey = new List<Key>();
                listKey = KeyCode.Deserialize<List<Key>>(KeyCode.ConvertFromBase64(result));
                gridControl1.DataSource = listKey;
                //LoadListView();
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Lỗi " + result);
            }
        }
        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            string result = string.Empty;
            try
            {
                string CustomerName = gridView1.GetFocusedRowCellValue("CustomerName").ToString();
                if (e.ClickedItem.Name.Equals("Approve"))
                {
                    string serial = gridView1.GetFocusedRowCellValue("SerialNumber").ToString();
                    if (MessageBoxControl.ShowMessageConfirm("Chấp nhận yêu cầu lấy key cho doanh nghiệp " + CustomerName + "?")=="Yes")
                        result = productServices.ApproveKeyLicense(User.UserName, KeyCode.ConvertToBase64(User.PassWord), serial, true);
                    else
                        return;
                    if (string.IsNullOrEmpty(result))
                        MessageBoxControl.ShowMessage("Đã chấp nhận");
                    else
                        MessageBoxControl.ShowMessage("Lỗi xảy ra khi thực hiện: " + result);
                }
                else if (e.ClickedItem.Name.Equals("DisApprove"))
                {
                    string serial = gridView1.GetFocusedRowCellValue("SerialNumber").ToString();
                    if (MessageBoxControl.ShowMessageConfirm("Từ chối cấp key cho doanh nghiệp " + CustomerName + "?") =="Yes")
                        result = productServices.ApproveKeyLicense(User.UserName, KeyCode.ConvertToBase64(User.PassWord), serial, false);
                    else
                        return;
                    if (string.IsNullOrEmpty(result))
                        MessageBoxControl.ShowMessage("Đã từ chối");
                    else
                        MessageBoxControl.ShowMessage("Lỗi xảy ra khi thực hiện: " + result);
                }
                else
                    return;
                if (string.IsNullOrEmpty(result))
                {
                    LoadData();
                }
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage(ex.Message);
            }
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show();
            }  
        }




    }
}