﻿using System;
using Company.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using Company.KDT.SHARE.Components;
using System.Linq;
using System.Windows.Forms;
using System.Data;


namespace Company.Interface.KDT.SXXK
{
    public partial class ThanhKhoanSendForm : BaseForm
    {

        public HoSoThanhLyDangKy hosothanhly;
        public ThanhKhoanSendForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TabBKToKhaiXuat_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {
        }
        private void LoadBkToKhaiNhap()
        {
            int sobanke = hosothanhly.getBKToKhaiNhap();
            if (sobanke > -1)
            {
                hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                DataTable dt = new Company.BLL.KDT.SXXK.BKToKhaiNhap().getBKToKhaiNhapForReport(hosothanhly.LanThanhLy, 1, hosothanhly.MaDoanhNghiep, hosothanhly.MaHaiQuanTiepNhan, string.Empty).Tables[0];
                grvBKNhap.DataSource = dt;
            }
        }
        private void LoadBKToKhaiXuat()
        {
            int sobanke = hosothanhly.getBKToKhaiXuat();
            if (sobanke > -1)
            {
                DataTable dt = new Company.BLL.KDT.SXXK.BKToKhaiXuat().getBKToKhaiXuatForReport(hosothanhly.LanThanhLy, hosothanhly.MaDoanhNghiep, hosothanhly.MaHaiQuanTiepNhan).Tables[0];
                grvBKXuat.DataSource = dt;
            }
        }
        private void LoadBKChuaThanhLy()
        {
            int  sobanke = hosothanhly.getBKNPLChuaThanhLY();
            if (sobanke > -1)
            #region MaterialLeft Bảng kê nguyên phụ liệu chưa thanh lý
            {
                hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                grvNPLChuaThanhLy.DataSource = hosothanhly.BKCollection[sobanke].bkNPLCTLCollection;

            }
            #endregion
        }
        private void LoadBKXinNopThue()
        {
            int sobanke = hosothanhly.getBKNPLNopThue();
            if (sobanke > -1)
            {
                hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                grvNPLChuaThanhLy.DataSource = hosothanhly.BKCollection[sobanke].bkNPLNTCollection;


            }
        }
        private void LoadChungTuThanhLy()
        {
            DataSet dsHD = new  Company.KDT.SXXK.ChungTuThanhToan().SelectDynamic("LanThanhLy = " + hosothanhly.LanThanhLy + " AND TriGiaCT > 0", null);
            DataTable dtHD = dsHD.Tables[0];
            dsHD.Tables.Remove(dtHD);

            DataSet dsChungTu = new Company.KDT.SXXK.ChungTuThanhToan().SelectDynamic("LanThanhLy = " + hosothanhly.LanThanhLy + " AND TriGiaCT > 0", null);
            DataTable dtChungTu = dsChungTu.Tables[0];
            dsChungTu.Tables.Remove(dtChungTu);

            dtHD.TableName = "dtHD";
            dtChungTu.TableName = "dtChungTu"; 
            DataSet ds = new DataSet();
            ds.Tables.AddRange(new DataTable[] { dtHD, dtChungTu });
            DataRelation relation1 = new DataRelation("dtHD", new DataColumn[] { dtHD.Columns["SoNgayHopDong"] }, new DataColumn[] { dtChungTu.Columns["SoNgayHopDong"] }, true);

            
            ds.Relations.Add(relation1);
            grvChungTuThanhToan.DataSource = ds.Tables["dtHD"];
            grvChungTuThanhToan.ForceInitialize();
        }

        private void ThanhKhoanSendForm_Load(object sender, EventArgs e)
        {

            LoadBKChuaThanhLy();
            LoadBkToKhaiNhap();
            LoadBKToKhaiXuat();
            LoadBKXinNopThue();
            LoadChungTuThanhLy();
        }


        

    }
}
