﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ECS.AutoUpdate;
using System.Web;
using System.Xml;
using System.Net;
using Logger;
using System.IO;
using System.Threading;
using System.Security.Principal;
using System.Security.AccessControl;
namespace AppAutoUpdate
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //Mutex MyApplicationMutex = new Mutex(true, "AppAutoUpdate");

            //if (MyApplicationMutex.WaitOne(0, false))
            //{

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());            
                //bool hasAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator); 

                #region check and download new version program
                bool bHasError = false;
                IAutoUpdater autoUpdater = new AutoUpdater();
                try
                {

                    DirectoryInfo dInfo = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
                    DirectorySecurity dSecurity = dInfo.GetAccessControl();
                    dSecurity.AddAccessRule(new FileSystemAccessRule("everyone", FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.InheritOnly, AccessControlType.Allow));
                    dInfo.SetAccessControl(dSecurity);

                    string arg = string.Empty;
                    if (args.Length != 0)
                        arg = args[0];
                    autoUpdater.Update(arg);
                }
                catch (WebException exp)
                {
                    MessageBox.Show("Không thể kết nối với hệ thống máy chủ Softech\nVui lòng kiểm tra lại kết nối của bạn.", "Cập nhật phần mềm");
                    bHasError = true;
                    Logger.LocalLogger.Instance().WriteMessage(exp);
                }
                catch (XmlException exp)
                {
                    bHasError = true;
                    MessageBox.Show("Nâp cấp phiên bản mới không thành công", "Cập nhật phần mềm");
                    Logger.LocalLogger.Instance().WriteMessage(exp);

                }
                catch (NotSupportedException exp)
                {
                    bHasError = true;
                    MessageBox.Show("Cấu hình kết nối không hổ trợ", "Cập nhật phần mềm");
                    Logger.LocalLogger.Instance().WriteMessage(exp);

                }
                catch (ArgumentException exp)
                {
                    bHasError = true;
                    MessageBox.Show("Download file không thành công", "Cập nhật phần mềm");
                    Logger.LocalLogger.Instance().WriteMessage(exp);

                }
                catch (Exception exp)
                {
                    bHasError = true;
                    //MessageBox.Show("Có lỗi trong khi xử lý", "Cập nhật phần mềm");
                    Logger.LocalLogger.Instance().WriteMessage("Có lỗi trong khi xử lý Cập nhật phần mềm", exp);

                }
                finally
                {
                    if (bHasError == true)
                    {
                        try
                        {
                            autoUpdater.RollBack();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
            //}
            #endregion
        }
    }
}
