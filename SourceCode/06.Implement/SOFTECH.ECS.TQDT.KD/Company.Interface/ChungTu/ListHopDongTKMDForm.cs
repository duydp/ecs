﻿using System;
using System.Drawing;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.BLL;
using Company.KD.BLL.Utils;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;
using Company.KD.BLL;

namespace Company.Interface
{
    public partial class ListHopDongTKMDForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------

        public ListHopDongTKMDForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NguyenTe_ID"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value.ToString());
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

   

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = TKMD.HopDongThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                //btnTaoMoi.Enabled = false;
                //btnXoa.Enabled = false;
            }
        }

  
      

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HopDongThuongMai> listHopDong = new List<HopDongThuongMai>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDongThuongMai hd = (HopDongThuongMai)i.GetRow().DataRow;
                        listHopDong.Add(hd);
                    }
                }
            }

            foreach (HopDongThuongMai hd in listHopDong)
            {
                hd.Delete();
                TKMD.HopDongThuongMaiCollection.Remove(hd);
            }

            dgList.DataSource = TKMD.HopDongThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

      
        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            HopDongForm f = new HopDongForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.ShowDialog();
            dgList.DataSource = TKMD.HopDongThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            HopDongForm f = new HopDongForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.HopDongTM =(HopDongThuongMai) e.Row.DataRow;
            f.ShowDialog();
            dgList.DataSource = TKMD.HopDongThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

     


    }
}
