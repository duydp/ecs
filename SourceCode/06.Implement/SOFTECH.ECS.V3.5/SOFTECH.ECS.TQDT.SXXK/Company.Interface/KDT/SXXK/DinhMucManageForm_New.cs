﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Company.BLL.KDT;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using System.Data;
using Company.BLL.KDT.SXXK;


namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucManageForm_New : BaseForm
    {
        /// <summary>
        /// Dùng cho kết quả tìm kiếm.
        /// </summary>
        private DinhMucDangKyCollection dmDangKyCollection = new DinhMucDangKyCollection();
        private DinhMucDangKyCollection tmpCollection = new DinhMucDangKyCollection();
        DinhMuc dm = new DinhMuc();

        /// <summary>
        /// Thông tin sản phẩm đang dược chọn.
        /// </summary>
        /// 
        private readonly DinhMucDangKy currentDMDangKy = new DinhMucDangKy();
        private string xmlCurrent = "";
        private DinhMucDangKy dmdk = new DinhMucDangKy();
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        private DinhMucMaHang dmmh = new DinhMucMaHang();

        public DinhMucManageForm_New()
        {
            this.InitializeComponent();
        }
        private int CheckNPLVaSPDuyet(DinhMucCollection collection, string mahaiquan)
        {
            foreach (DinhMuc dm in collection)
            {
                BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.Ma = dm.MaNguyenPhuLieu;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = mahaiquan;
                if (!npl.Load())
                    return 0;
                BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = dm.MaSanPham;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = mahaiquan;
                if (!sp.Load())
                    return 1;
            }
            return 2;
        }

        //-----------------------------------------------------------------------------------------
        private void DinhMucManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.khoitao_DuLieuChuan();
                this.search();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            saveFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            // Đơn vị Hải quan.
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            txtNamTiepNhan.Value = DateTime.Today.Year;

        }

        //-----------------------------------------------------------------------------------------      
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string masanpham = txtMaSP.Text;
                string manpl = txtMaNPL.Text;
                if (masanpham == "" && manpl == "")
                {
                    this.search();
                }
                else
                    this.search_Ma(masanpham, manpl);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);
                where += string.Format(" AND NgayDangKy != '1900-01-01 00:00:00.000' AND GUIDSTR = 'thanhkhoan2015'");

                if (this.txtSoTiepNhan.TextLength > 0)
                    where += " AND SoTiepNhan like '%" + this.txtSoTiepNhan.Value + "%'";

                if (this.txtNamTiepNhan.TextLength > 0)
                    where += " AND YEAR(NgayTiepNhan) = '" + this.txtNamTiepNhan.Text + "'";

                // Thực hiện tìm kiếm.
                this.dmDangKyCollection = DinhMucDangKy.SelectCollectionDynamic(where, "ID Desc");
                this.dgList.DataSource = this.dmDangKyCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //Phiph----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu. theo ma SP vaNPL
        /// </summary>
        private void search_Ma(string masanpham, string manpl)
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                this.Cursor = Cursors.WaitCursor;
                string where = " WHERE  DMDK.NgayDangKy != '1900-01-01 00:00:00.000' AND DMDK.GUIDSTR = 'thanhkhoan2015'";
                where += " AND DMMH.MaSP LIKE '%" + masanpham + "%'";
                where += " AND DMMH.MaNPL LIKE '%" + manpl + "%'";

                where += " AND  DMDK.MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND DMDK.MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);


                if (this.txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND DMDK.SoTiepNhan LIKE '%" + this.txtSoTiepNhan.Value + "%'";
                }
                // thuc hien tim kiem
                this.dmDangKyCollection = DinhMucDangKy.SelectCollectionMaSanPham_DinhMucMaHang(where, "DMDK.ID Desc");
                this.dgList.DataSource = this.dmDangKyCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    int ValueIdCell = Int32.Parse(e.Row.Cells["ID"].Value.ToString());
                    string _maSP = "";
                    string _maNPL = "";
                    string _soTK = "";
                    string _HD = "";

                    DataTable dtbDMMH = new DataTable();
                    dtbDMMH = dmmh.SelectByMasterID(ValueIdCell);

                    if (dtbDMMH.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtbDMMH.Select())
                        {
                            _maSP = _maSP + dr["MaSP"] + "\n";
                            _maNPL = _maNPL + dr["MaNPL"] + "\n";
                            _soTK = _soTK + dr["SoTKN_VNACCS"] + "\n";
                            _HD = _HD + dr["Invoid_HD"] + "\n";
                        }
                        e.Row.Cells["Style"].Text = dtbDMMH.Rows[0]["Style"].ToString();
                        e.Row.Cells["Style"].ToolTipText = _maSP;

                        e.Row.Cells["MaNPL"].Text = dtbDMMH.Rows[0]["MaNPL"].ToString();
                        e.Row.Cells["MaNPL"].ToolTipText = _maNPL;

                        e.Row.Cells["SoTKN_VNACCS"].Text = dtbDMMH.Rows[0]["SoTKN_VNACCS"].ToString();
                        e.Row.Cells["SoTKN_VNACCS"].ToolTipText = _soTK;

                        e.Row.Cells["Invoid_HD"].Text = dtbDMMH.Rows[0]["Invoid_HD"].ToString();
                        e.Row.Cells["Invoid_HD"].ToolTipText = _HD;
                    }

                }
            }
            catch (Exception ex) { }

        }
        //-----------------------------------------------------------------------------------------


        private void sendItemsSelect()
        {
            FormSendDinhMuc f = new FormSendDinhMuc();
            f.btnSend.Enabled = true;
            f.ShowDialog(this);
            this.search();
        }
        private void XacNhanThongTin()
        {
            FormSendDinhMuc f = new FormSendDinhMuc();
            f.btnXacNhan.Enabled = true;
            f.ShowDialog(this);
            this.search();
        }
        private void cancelItemsSelect()
        {
            FormSendDinhMuc f = new FormSendDinhMuc();
            f.btnHuy.Enabled = true;
            f.ShowDialog(this);
            this.search();
        }
        //-----------------------------------------------------------------------------------------
        private void downloadItemsSelect()
        {
            FormSendDinhMuc f = new FormSendDinhMuc();
            f.btnNhan.Enabled = true;
            f.ShowDialog(this);
            this.search();
        }
        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {

                case "cmdCSDaDuyet":
                    this.ConvertDaDuyet();
                    break;
                case "cmdCancel":
                    this.cancelItemsSelect();
                    break;
                case "cmdSend":
                    this.sendItemsSelect();
                    break;
                case "XacNhan":
                    this.XacNhanThongTin();
                    break;
                case "DinhMuc":
                    this.InDinhMuc();
                    break;
                case "InDM":
                    this.InDinhMuc();
                    break;
                case "Export":
                    this.ExportData();
                    break;
                case "Import":
                    this.ImportData();
                    break;
                case "Delete":
                    this.Delete();
                    break;
                case "cmdXuatDinhMucChoPhongKhai":
                    XuatDinhMucChoPhongKhai();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
            }
        }

        private void ConvertDaDuyet()
        {
            //int id = dgList.CurrentRow.Position;
            List<DinhMucMaHang> listDM = new List<DinhMucMaHang>();
            GridEXRow gr =  dgList.CurrentRow;
            int id = int.Parse(gr.Cells["ID"].Value.ToString());
            DataTable tbdm = SXXK_DinhMucMaHang.SelectDynamic(string.Format("Master_ID = {0}", id), "").Tables[0];
            if (tbdm.Rows.Count > 0)
            {
                if (int.Parse(tbdm.Rows[0]["LanThanhLy"].ToString()) != 0)
                {
                    MessageBox.Show("Định mức đã được thanh khoản. Bạn không thể sửa đổi.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    int del = SXXK_DinhMucMaHang.DeleteDynamic(string.Format(" Master_ID = {0}", id));
                    DataTable tb = DinhMucMaHang.SelectDynamic(string.Format("Master_ID = {0}", id), "").Tables[0];
                    foreach (DataRow dr in tb.Rows)
                    {
                        try
                        {
                            SXXK_DinhMucMaHang dm = new SXXK_DinhMucMaHang();
                            dm.ID = int.Parse(dr["ID"].ToString());
                            dm.LanThanhLy = 0;
                            dm.Master_ID = int.Parse(dr["Master_ID"].ToString());
                            dm.TenKH = dr["TenKH"].ToString();
                            dm.PO = dr["PO"].ToString();
                            dm.Style = dr["Style"].ToString();
                            dm.MaSP = dr["MaSP"].ToString();
                            dm.MatHang = dr["MatHang"].ToString();
                            dm.SoLuongPO = Int32.Parse(dr["SoLuongPO"].ToString());
                            dm.SLXuat = Int32.Parse(dr["SLXuat"].ToString());
                            dm.ChenhLech = Convert.ToDecimal(dr["ChenhLech"].ToString());
                            dm.MaNPL = dr["MaNPL"].ToString();
                            dm.TenNPL = dr["TenNPL"].ToString();
                            dm.Art = dr["Art"].ToString();
                            dm.DVT_NPL = dr["DVT_NPL"].ToString();
                            dm.NCC = dr["NCC"].ToString();
                            dm.SPTieuThu = Convert.ToDecimal(dr["SPTieuThu"].ToString());
                            dm.DinhMuc = Convert.ToDecimal(dr["DinhMuc"].ToString());
                            dm.TyLeHaoHut = Convert.ToDecimal(dr["TyLeHaoHut"].ToString());
                            dm.DinhMuc_HaoHut = Convert.ToDecimal(dr["DinhMuc_HaoHut"].ToString());
                            dm.NhuCauXuat = Convert.ToDecimal(dr["NhuCauXuat"].ToString());
                            dm.XuatKho = Convert.ToDecimal(dr["XuatKho"].ToString());
                            dm.ThuHoi = Convert.ToDecimal(dr["ThuHoi"].ToString());
                            dm.ThucXuatKho = Convert.ToDecimal(dr["ThucXuatKho"].ToString());
                            dm.DinhMucThucTe = Convert.ToDecimal(dr["DinhMucThucTe"].ToString());
                            dm.SoTKN_VNACCS = Convert.ToDecimal(dr["SoTKN_VNACCS"].ToString());
                            dm.Invoid_HD = dr["Invoid_HD"].ToString();
                            dm.NgayChungTu = Convert.ToDateTime(dr["NgayChungTu"].ToString());
                            dm.SoLuongNhapKho = Convert.ToDecimal(dr["SoLuongNhapKho"].ToString());
                            dm.SoLuongSuDung = Convert.ToDecimal(dr["SoLuongSuDung"].ToString());
                            dm.SoLuongThanhKhoan = Convert.ToDecimal(dr["SoLuongThanhKhoan"].ToString());
                            dm.Insert();
                        }
                        catch (Exception ex)
                        {

                            //throw;
                        }
                        
                    }
                    MessageBox.Show("Chuyển trạng thái thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else 
            {
                DataTable tb = DinhMucMaHang.SelectDynamic(string.Format("Master_ID = {0}", id), "").Tables[0];
                foreach (DataRow dr in tb.Rows)
                {
                    try
                    {
                        SXXK_DinhMucMaHang dm = new SXXK_DinhMucMaHang();
                        dm.ID = int.Parse(dr["ID"].ToString());
                        dm.LanThanhLy = 0;
                        dm.Master_ID = int.Parse(dr["Master_ID"].ToString());
                        dm.TenKH = dr["TenKH"].ToString();
                        dm.PO = dr["PO"].ToString();
                        dm.Style = dr["Style"].ToString();
                        dm.MaSP = dr["MaSP"].ToString();
                        dm.MatHang = dr["MatHang"].ToString();
                        dm.SoLuongPO = Int32.Parse(dr["SoLuongPO"].ToString());
                        dm.SLXuat = Convert.ToDecimal(dr["SLXuat"].ToString());
                        dm.ChenhLech = Convert.ToDecimal(dr["ChenhLech"].ToString());
                        dm.MaNPL = dr["MaNPL"].ToString();
                        dm.TenNPL = dr["TenNPL"].ToString();
                        dm.Art = dr["Art"].ToString();
                        dm.DVT_NPL = dr["DVT_NPL"].ToString();
                        dm.NCC = dr["NCC"].ToString();
                        dm.SPTieuThu = Convert.ToDecimal(dr["SPTieuThu"].ToString());
                        dm.DinhMuc = Convert.ToDecimal(dr["DinhMuc"].ToString());
                        dm.TyLeHaoHut = Convert.ToDecimal(dr["TyLeHaoHut"].ToString());
                        dm.DinhMuc_HaoHut = Convert.ToDecimal(dr["DinhMuc_HaoHut"].ToString());
                        dm.NhuCauXuat = Convert.ToDecimal(dr["NhuCauXuat"].ToString());
                        dm.XuatKho = Convert.ToDecimal(dr["XuatKho"].ToString());
                        dm.ThuHoi = Convert.ToDecimal(dr["ThuHoi"].ToString());
                        dm.ThucXuatKho = Convert.ToDecimal(dr["ThucXuatKho"].ToString());
                        dm.DinhMucThucTe = Convert.ToDecimal(dr["DinhMucThucTe"].ToString());
                        dm.SoTKN_VNACCS = Convert.ToDecimal(dr["SoTKN_VNACCS"].ToString());
                        dm.Invoid_HD = dr["Invoid_HD"].ToString();
                        dm.NgayChungTu = Convert.ToDateTime(dr["NgayChungTu"].ToString());
                        dm.SoLuongNhapKho = Convert.ToDecimal(dr["SoLuongNhapKho"].ToString());
                        dm.SoLuongSuDung = Convert.ToDecimal(dr["SoLuongSuDung"].ToString());
                        dm.SoLuongThanhKhoan = Convert.ToDecimal(dr["SoLuongSuDung"].ToString());
                        dm.Insert();
                    }
                    catch (Exception ex)
                    {

                        //throw;
                    }
                    
                }
                MessageBox.Show("Chuyển trạng thái thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }
        private void inPhieuTN()
        {
            if (dgList.GetRows().Length < 1) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "SẢN PHẨM";
            Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    DinhMucDangKy dmDangKySelected = (DinhMucDangKy)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = dmDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = dmDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    j++;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();
        }
        private void XuatDinhMucChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                // ShowMessage("Chưa chọn danh sách định mức cần xuất.", false);
                //msg
                //Message("MSG_DMC01","", false);
                MLMessages("Chưa chọn danh sách định mức cần xuất. ", "MSG_DMC01", "", false);
                return;
            }
            try
            {
                DinhMucDangKyCollection col = new DinhMucDangKyCollection();
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sodinhmuc = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DinhMucDangKy dmSelected = (DinhMucDangKy)i.GetRow().DataRow;
                            dmSelected.LoadDMCollection();
                            col.Add(dmSelected);
                            sodinhmuc++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    // ShowMessage("Xuất thành công " + sodinhmuc + " định mức.",false);
                    //msg
                    // Message("MSG_DMC02", "", false);
                    MLMessages("Xuất thành công " + sodinhmuc + " định mức.", "MSG_DMC02", sodinhmuc.ToString(), false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }

        }

        public void XoaDinhMucDangKy(GridEXSelectedItemCollection items)
        {

            if (items.Count == 0) return;
            string msgWarning = string.Empty;
            MsgSend sendXML = new MsgSend();

            List<DinhMucDangKy> itemsDelete = new List<DinhMucDangKy>();

            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    DinhMucDangKy item = (DinhMucDangKy)i.GetRow().DataRow;
                    sendXML.LoaiHS = "DM";
                    sendXML.master_id = item.ID;
                    if (!sendXML.Load())
                    {
                        msgWarning += string.Format(" ID = {0} [Chấp nhận xóa]\r\n", item.ID);
                        itemsDelete.Add(item);
                    }
                    else
                    {
                        msgWarning += string.Format(" ID = {0} [Không thể xóa]\r\n", item.ID);
                    }
                }
            }
            msgWarning += "\n";
            if (itemsDelete.Count == 0)
            {
                Globals.ShowMessage(msgWarning, false);
                return;
            }
            else
            {
                msgWarning += "Bạn đồng ý thực hiện không?";
                if (Globals.ShowMessage(msgWarning, true) != "Yes") return;
            }
            try
            {
                msgWarning = string.Empty;
                foreach (DinhMucDangKy item in itemsDelete)
                {
                    try
                    {
                        item.Delete_DinhMucMaHang();
                    }
                    catch (Exception ex)
                    {
                        msgWarning += string.Format("ID = {0} [thực hiện không thành công]", item.ID);
                    }
                }
                if (msgWarning != string.Empty)

                    Globals.ShowMessage(msgWarning, false);
            }
            catch (Exception ex)
            {
                Globals.ShowMessage(ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Delete()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaDinhMucDangKy(items);
            this.search();
          
        }

        private string checkDataHangImport(DinhMucDangKy dmDangky)
        {
            string st = "";
            foreach (DinhMuc dm in dmDangky.DMCollection)
            {
                BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
                ttdm.MaSanPham = dm.MaSanPham;
                ttdm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (ttdm.Load())
                {
                    if (dm.STTHang == 1)
                        st = "Danh sách có ID='" + dmDangky.ID + "'\n";
                    st += "Sản phẩm có mã '" + dm.MaSanPham + "' đã có định mức trong hệ thống.\n";
                }
            }
            return st;
        }
        private string checkDataImport(DinhMucDangKyCollection collection)
        {
            string st = "";
            foreach (DinhMucDangKy dmDangky in collection)
            {
                DinhMucDangKy dmInDatabase = DinhMucDangKy.Load(dmDangky.ID);
                if (dmInDatabase != null)
                {
                    if (dmInDatabase.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        st += "Danh sách có ID=" + dmDangky.ID + " đã được duyệt.\n";
                    }
                    else
                    {
                        string tmp = checkDataHangImport(dmDangky);
                        st += tmp;
                        if (tmp == "")
                            tmpCollection.Add(dmDangky);
                    }
                }
                else
                {
                    if (dmDangky.ID > 0)
                        dmDangky.ID = 0;
                    tmpCollection.Add(dmDangky);
                }
            }
            return st;
        }
        private void ImportData()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                tmpCollection.Clear();
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    DinhMucDangKyCollection dmDKCollection = (DinhMucDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(dmDKCollection);
                    if (st != "")
                    {
                        // if (ShowMessage("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", true) == "Yes")
                        if (MLMessages("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", "MSG_PUB04", "", true) == "Yes")
                        {
                            DinhMucDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                            //  ShowMessage("Import thành công", false);
                            //msg
                            MLMessages("Import thành công", "MSG_PUB02", "", false);
                        }
                    }
                    else
                    {
                        DinhMucDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                        // ShowMessage("Import thành công", false);
                        //msg
                        //Message("MSG_PUB02", "", false);
                        MLMessages("Import thành công", "MSG_PUB02", "", false);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage(" : " + ex.Message, false);
                }
            }
        }
        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                //ShowMessage("Chưa chọn danh sách định mức", false);
                //msg
                // Message("MSG_DMC01", "", false);
                MLMessages("Chưa chọn danh sách định mức", "MSG_DMC01", "", false);
                return;
            }
            try
            {
                DinhMucDangKyCollection col = new DinhMucDangKyCollection();
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DinhMucDangKy dmDangKySelected = (DinhMucDangKy)i.GetRow().DataRow;
                            dmDangKySelected.LoadDMCollection();
                            col.Add(dmDangKySelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }

        }
        private void InDinhMuc()
        {
            DinhMucDangKy dmDangKy = new DinhMucDangKy();
            if (dgList.GetRow() != null)
            {
                dmDangKy = (DinhMucDangKy)dgList.GetRow().DataRow;
            }
            else
                return;
            Report.ReportViewDinhMucForm f = new Company.Interface.Report.ReportViewDinhMucForm();
            f.DMDangKy = dmDangKy;
            f.ShowDialog(this);
        }


        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            
            try
            {
                long ID = (long)Convert.ToInt64(e.Row.Cells["ID"].Text);
                //long SoTiepNhan = (long)Convert.ToInt64(e.Row.Cells["SoTiepNhan"].Text);

                DinhMuc_New f = new DinhMuc_New();
                f._masterID = ID;
                f.Show();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

        }

        //-----------------------------------------------------------------------------------------



        private void DinhMucManageForm_Shown(object sender, EventArgs e)
        {
            this.search();
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (e.Row.RowType == RowType.Record)
                {
                    DinhMucDangKy dmDangKySelected = (DinhMucDangKy)e.Row.DataRow;
                    if (dmDangKySelected.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                    {
                        // if (ShowMessage("Bạn có muốn xóa thông tin đăng ký này và các định mức liên quan không?", true) == "Yes")
                        if (MLMessages("Bạn có muốn xóa thông tin đăng ký này và các định mức liên quan không?", "MSG_DEL01", "", true) == "Yes")
                        {
                            dmDangKySelected.Delete_DinhMucMaHang();
                        }
                        else
                        {
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        // ShowMessage("Danh sách đã được duyệt. Không được xóa",false);
                        MLMessages("Danh sách đã được duyệt. Không được xóa", "MSG_SEN12", "", false);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                {

                    {
                        // if (ShowMessage("Bạn có muốn xóa thông tin đăng ký này và các định mức liên quan không?", true) == "Yes")
                        if (MLMessages("Bạn có muốn xóa thông tin đăng ký này và các định mức liên quan không?", "MSG_DEL01", "", true) == "Yes")
                        {
                            GridEXSelectedItemCollection items = dgList.SelectedItems;
                            foreach (GridEXSelectedItem i in items)
                            {
                                if (i.RowType == RowType.Record)
                                {
                                    DinhMucDangKy dmDangKySelected = (DinhMucDangKy)i.GetRow().DataRow;
                                    MsgSend sendXML = new MsgSend();
                                    sendXML.LoaiHS = "DM";
                                    sendXML.master_id = dmDangKySelected.ID;
                                    if (sendXML.Load())
                                    {
                                        //ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
                                        // Message("MSG_SEN03", "", false);
                                        MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_SEN03", "", false);
                                    }
                                    else
                                    {
                                        if (dmDangKySelected.ID > 0)
                                        {
                                            dmDangKySelected.Delete();
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            e.Cancel = true;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void xacnhanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //LaySoTiepNhanDT();
        }

        private void DinhMucMenuItem_Click(object sender, EventArgs e)
        {
            InDinhMuc();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void xóaĐịnhMứcToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Delete();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.Delete();
        }

        private void mnuCSDaDuyet_Click(object sender, EventArgs e)
        {
           // this.ChuyenTrangThai();
        }

        private void uiCommandBar1_CommandClick(object sender, CommandEventArgs e)
        {

        }

        private void PhieuTNMenuItem_Click(object sender, EventArgs e)
        {
            this.inPhieuTN();
        }

        private void DinhMucMenuItem_Click_1(object sender, EventArgs e)
        {
            InDinhMuc();
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {

        }
    }
}
