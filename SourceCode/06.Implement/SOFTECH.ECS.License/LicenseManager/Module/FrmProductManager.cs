using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmProductManager : DevExpress.XtraEditors.XtraForm
    {
        public FrmProductManager()
        {
            InitializeComponent();
        }

        private void FrmProductManager_Load(object sender, EventArgs e)
        {
            this.RefreshData();
        }

        #region Adding Method
        private void DisplayGridHeader()
        {
            gridData.Columns["ID"].Caption = "Mã sản phẩm";
            gridData.Columns["Name"].Caption = "Tên sản phẩm";
            gridData.Columns["Description"].Caption = "Mô tả";
        }

        private void OpenForm(string id, bool isEdit)
        {
            if (!ClassLib.IsFormShowed("Prod_" + id))
            {
                FrmProductInfo obj = new FrmProductInfo(id);
                obj.MdiParent = this.MdiParent;
                obj.isEdit = isEdit;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Prod_" + id].Activate();
            }
        }

        public void RefreshData()
        {
            gridProduct.DataSource = Product.SelectAll().Tables[0];
            this.DisplayGridHeader();
        }
        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!ClassLib.IsFormShowed("Prod_New"))
            {
                FrmProductInfo obj = new FrmProductInfo(null);
                obj.MdiParent = this.MdiParent;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Prod_New"].Activate();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    this.OpenForm(ID, true);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    string Message = "Bạn có muốn xóa sản phẩm: " + ID;
                    if (MessageBox.Show(Message, "Xóa sản phẩm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Product.DeleteProduct(ID);
                    }
                }
            }
            this.RefreshData();
        }

        private void gridProduct_DoubleClick(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    this.OpenForm(ID, false);
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            gridData.ClearColumnsFilter();
            gridData.ClearGrouping();
            gridData.ClearSorting();
            this.RefreshData();
        }

    }
}