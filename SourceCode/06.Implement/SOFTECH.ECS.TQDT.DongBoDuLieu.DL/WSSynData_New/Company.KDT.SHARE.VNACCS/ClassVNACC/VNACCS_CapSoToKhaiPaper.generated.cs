﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACCS_CapSoToKhaiPaper : ICloneable
	{
		#region Properties.
		
		public int SoTK { set; get; }
		public string MaLoaiHinh { set; get; }
		public int NamDangKy { set; get; }
		public decimal SoTKVNACCS { set; get; }
		public decimal SoTKVNACCSFull { set; get; }
		public decimal SoTKDauTien { set; get; }
		public int SoNhanhTK { set; get; }
		public int TongSoTKChiaNho { set; get; }
		public decimal SoTKTNTX { set; get; }
		public string Temp1 { set; get; }
		public string SoTKPaper { set; get; }
		public decimal SoTKKyThuat { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<VNACCS_CapSoToKhaiPaper> ConvertToCollection(IDataReader reader)
		{
			List<VNACCS_CapSoToKhaiPaper> collection = new List<VNACCS_CapSoToKhaiPaper>();
			while (reader.Read())
			{
				VNACCS_CapSoToKhaiPaper entity = new VNACCS_CapSoToKhaiPaper();
				if (!reader.IsDBNull(reader.GetOrdinal("SoTK"))) entity.SoTK = reader.GetInt32(reader.GetOrdinal("SoTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt32(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKVNACCS"))) entity.SoTKVNACCS = reader.GetDecimal(reader.GetOrdinal("SoTKVNACCS"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKVNACCSFull"))) entity.SoTKVNACCSFull = reader.GetDecimal(reader.GetOrdinal("SoTKVNACCSFull"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKDauTien"))) entity.SoTKDauTien = reader.GetDecimal(reader.GetOrdinal("SoTKDauTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNhanhTK"))) entity.SoNhanhTK = reader.GetInt32(reader.GetOrdinal("SoNhanhTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoTKChiaNho"))) entity.TongSoTKChiaNho = reader.GetInt32(reader.GetOrdinal("TongSoTKChiaNho"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKTNTX"))) entity.SoTKTNTX = reader.GetDecimal(reader.GetOrdinal("SoTKTNTX"));
				if (!reader.IsDBNull(reader.GetOrdinal("Temp1"))) entity.Temp1 = reader.GetString(reader.GetOrdinal("Temp1"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKPaper"))) entity.SoTKPaper = reader.GetString(reader.GetOrdinal("SoTKPaper"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKKyThuat"))) entity.SoTKKyThuat = reader.GetDecimal(reader.GetOrdinal("SoTKKyThuat"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_VNACCS_CapSoToKhaiPaper VALUES(@SoTK, @MaLoaiHinh, @NamDangKy, @SoTKVNACCS, @SoTKVNACCSFull, @SoTKDauTien, @SoNhanhTK, @TongSoTKChiaNho, @SoTKTNTX, @Temp1, @SoTKPaper, @SoTKKyThuat)";
            string update = "UPDATE t_VNACCS_CapSoToKhaiPaper SET SoTK = @SoTK, MaLoaiHinh = @MaLoaiHinh, NamDangKy = @NamDangKy, SoTKVNACCS = @SoTKVNACCS, SoTKVNACCSFull = @SoTKVNACCSFull, SoTKDauTien = @SoTKDauTien, SoNhanhTK = @SoNhanhTK, TongSoTKChiaNho = @TongSoTKChiaNho, SoTKTNTX = @SoTKTNTX, Temp1 = @Temp1, SoTKPaper = @SoTKPaper, SoTKKyThuat = @SoTKKyThuat WHERE ID = @ID";
            string delete = "DELETE FROM t_VNACCS_CapSoToKhaiPaper WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@SoTK", SqlDbType.Int, "SoTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamDangKy", SqlDbType.Int, "NamDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKVNACCS", SqlDbType.Decimal, "SoTKVNACCS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKVNACCSFull", SqlDbType.Decimal, "SoTKVNACCSFull", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKDauTien", SqlDbType.Decimal, "SoTKDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhanhTK", SqlDbType.Int, "SoNhanhTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTKChiaNho", SqlDbType.Int, "TongSoTKChiaNho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKTNTX", SqlDbType.Decimal, "SoTKTNTX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Temp1", SqlDbType.NVarChar, "Temp1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKPaper", SqlDbType.NVarChar, "SoTKPaper", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKKyThuat", SqlDbType.Decimal, "SoTKKyThuat", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@SoTK", SqlDbType.Int, "SoTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamDangKy", SqlDbType.Int, "NamDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKVNACCS", SqlDbType.Decimal, "SoTKVNACCS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKVNACCSFull", SqlDbType.Decimal, "SoTKVNACCSFull", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKDauTien", SqlDbType.Decimal, "SoTKDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhanhTK", SqlDbType.Int, "SoNhanhTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTKChiaNho", SqlDbType.Int, "TongSoTKChiaNho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKTNTX", SqlDbType.Decimal, "SoTKTNTX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Temp1", SqlDbType.NVarChar, "Temp1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKPaper", SqlDbType.NVarChar, "SoTKPaper", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKKyThuat", SqlDbType.Decimal, "SoTKKyThuat", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@SoTK", SqlDbType.Int, "SoTK", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_VNACCS_CapSoToKhaiPaper VALUES(@SoTK, @MaLoaiHinh, @NamDangKy, @SoTKVNACCS, @SoTKVNACCSFull, @SoTKDauTien, @SoNhanhTK, @TongSoTKChiaNho, @SoTKTNTX, @Temp1, @SoTKPaper, @SoTKKyThuat)";
            string update = "UPDATE t_VNACCS_CapSoToKhaiPaper SET SoTK = @SoTK, MaLoaiHinh = @MaLoaiHinh, NamDangKy = @NamDangKy, SoTKVNACCS = @SoTKVNACCS, SoTKVNACCSFull = @SoTKVNACCSFull, SoTKDauTien = @SoTKDauTien, SoNhanhTK = @SoNhanhTK, TongSoTKChiaNho = @TongSoTKChiaNho, SoTKTNTX = @SoTKTNTX, Temp1 = @Temp1, SoTKPaper = @SoTKPaper, SoTKKyThuat = @SoTKKyThuat WHERE ID = @ID";
            string delete = "DELETE FROM t_VNACCS_CapSoToKhaiPaper WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@SoTK", SqlDbType.Int, "SoTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamDangKy", SqlDbType.Int, "NamDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKVNACCS", SqlDbType.Decimal, "SoTKVNACCS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKVNACCSFull", SqlDbType.Decimal, "SoTKVNACCSFull", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKDauTien", SqlDbType.Decimal, "SoTKDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhanhTK", SqlDbType.Int, "SoNhanhTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTKChiaNho", SqlDbType.Int, "TongSoTKChiaNho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKTNTX", SqlDbType.Decimal, "SoTKTNTX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Temp1", SqlDbType.NVarChar, "Temp1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKPaper", SqlDbType.NVarChar, "SoTKPaper", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKKyThuat", SqlDbType.Decimal, "SoTKKyThuat", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@SoTK", SqlDbType.Int, "SoTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamDangKy", SqlDbType.Int, "NamDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKVNACCS", SqlDbType.Decimal, "SoTKVNACCS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKVNACCSFull", SqlDbType.Decimal, "SoTKVNACCSFull", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKDauTien", SqlDbType.Decimal, "SoTKDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhanhTK", SqlDbType.Int, "SoNhanhTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTKChiaNho", SqlDbType.Int, "TongSoTKChiaNho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKTNTX", SqlDbType.Decimal, "SoTKTNTX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Temp1", SqlDbType.NVarChar, "Temp1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKPaper", SqlDbType.NVarChar, "SoTKPaper", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKKyThuat", SqlDbType.Decimal, "SoTKKyThuat", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@SoTK", SqlDbType.Int, "SoTK", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VNACCS_CapSoToKhaiPaper Load(int soTK, string maLoaiHinh)
		{
			const string spName = "[dbo].[p_VNACCS_CapSoToKhaiPaper_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, soTK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, maLoaiHinh);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<VNACCS_CapSoToKhaiPaper> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<VNACCS_CapSoToKhaiPaper> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<VNACCS_CapSoToKhaiPaper> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_VNACCS_CapSoToKhaiPaper_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACCS_CapSoToKhaiPaper_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_VNACCS_CapSoToKhaiPaper_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACCS_CapSoToKhaiPaper_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertVNACCS_CapSoToKhaiPaper(int soTK, string maLoaiHinh, int namDangKy, decimal soTKVNACCS, decimal soTKVNACCSFull, decimal soTKDauTien, int soNhanhTK, int tongSoTKChiaNho, decimal soTKTNTX, string temp1, string soTKPaper, decimal soTKKyThuat)
		{
			VNACCS_CapSoToKhaiPaper entity = new VNACCS_CapSoToKhaiPaper();	
			entity.SoTK = soTK;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NamDangKy = namDangKy;
			entity.SoTKVNACCS = soTKVNACCS;
			entity.SoTKVNACCSFull = soTKVNACCSFull;
			entity.SoTKDauTien = soTKDauTien;
			entity.SoNhanhTK = soNhanhTK;
			entity.TongSoTKChiaNho = tongSoTKChiaNho;
			entity.SoTKTNTX = soTKTNTX;
			entity.Temp1 = temp1;
			entity.SoTKPaper = soTKPaper;
			entity.SoTKKyThuat = soTKKyThuat;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_VNACCS_CapSoToKhaiPaper_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, SoTK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
			db.AddInParameter(dbCommand, "@SoTKVNACCS", SqlDbType.Decimal, SoTKVNACCS);
			db.AddInParameter(dbCommand, "@SoTKVNACCSFull", SqlDbType.Decimal, SoTKVNACCSFull);
			db.AddInParameter(dbCommand, "@SoTKDauTien", SqlDbType.Decimal, SoTKDauTien);
			db.AddInParameter(dbCommand, "@SoNhanhTK", SqlDbType.Int, SoNhanhTK);
			db.AddInParameter(dbCommand, "@TongSoTKChiaNho", SqlDbType.Int, TongSoTKChiaNho);
			db.AddInParameter(dbCommand, "@SoTKTNTX", SqlDbType.Decimal, SoTKTNTX);
			db.AddInParameter(dbCommand, "@Temp1", SqlDbType.NVarChar, Temp1);
			db.AddInParameter(dbCommand, "@SoTKPaper", SqlDbType.NVarChar, SoTKPaper);
			db.AddInParameter(dbCommand, "@SoTKKyThuat", SqlDbType.Decimal, SoTKKyThuat);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<VNACCS_CapSoToKhaiPaper> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACCS_CapSoToKhaiPaper item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVNACCS_CapSoToKhaiPaper(int soTK, string maLoaiHinh, int namDangKy, decimal soTKVNACCS, decimal soTKVNACCSFull, decimal soTKDauTien, int soNhanhTK, int tongSoTKChiaNho, decimal soTKTNTX, string temp1, string soTKPaper, decimal soTKKyThuat)
		{
			VNACCS_CapSoToKhaiPaper entity = new VNACCS_CapSoToKhaiPaper();			
			entity.SoTK = soTK;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NamDangKy = namDangKy;
			entity.SoTKVNACCS = soTKVNACCS;
			entity.SoTKVNACCSFull = soTKVNACCSFull;
			entity.SoTKDauTien = soTKDauTien;
			entity.SoNhanhTK = soNhanhTK;
			entity.TongSoTKChiaNho = tongSoTKChiaNho;
			entity.SoTKTNTX = soTKTNTX;
			entity.Temp1 = temp1;
			entity.SoTKPaper = soTKPaper;
			entity.SoTKKyThuat = soTKKyThuat;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_VNACCS_CapSoToKhaiPaper_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, SoTK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
			db.AddInParameter(dbCommand, "@SoTKVNACCS", SqlDbType.Decimal, SoTKVNACCS);
			db.AddInParameter(dbCommand, "@SoTKVNACCSFull", SqlDbType.Decimal, SoTKVNACCSFull);
			db.AddInParameter(dbCommand, "@SoTKDauTien", SqlDbType.Decimal, SoTKDauTien);
			db.AddInParameter(dbCommand, "@SoNhanhTK", SqlDbType.Int, SoNhanhTK);
			db.AddInParameter(dbCommand, "@TongSoTKChiaNho", SqlDbType.Int, TongSoTKChiaNho);
			db.AddInParameter(dbCommand, "@SoTKTNTX", SqlDbType.Decimal, SoTKTNTX);
			db.AddInParameter(dbCommand, "@Temp1", SqlDbType.NVarChar, Temp1);
			db.AddInParameter(dbCommand, "@SoTKPaper", SqlDbType.NVarChar, SoTKPaper);
			db.AddInParameter(dbCommand, "@SoTKKyThuat", SqlDbType.Decimal, SoTKKyThuat);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<VNACCS_CapSoToKhaiPaper> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACCS_CapSoToKhaiPaper item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVNACCS_CapSoToKhaiPaper(int soTK, string maLoaiHinh, int namDangKy, decimal soTKVNACCS, decimal soTKVNACCSFull, decimal soTKDauTien, int soNhanhTK, int tongSoTKChiaNho, decimal soTKTNTX, string temp1, string soTKPaper, decimal soTKKyThuat)
		{
			VNACCS_CapSoToKhaiPaper entity = new VNACCS_CapSoToKhaiPaper();			
			entity.SoTK = soTK;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NamDangKy = namDangKy;
			entity.SoTKVNACCS = soTKVNACCS;
			entity.SoTKVNACCSFull = soTKVNACCSFull;
			entity.SoTKDauTien = soTKDauTien;
			entity.SoNhanhTK = soNhanhTK;
			entity.TongSoTKChiaNho = tongSoTKChiaNho;
			entity.SoTKTNTX = soTKTNTX;
			entity.Temp1 = temp1;
			entity.SoTKPaper = soTKPaper;
			entity.SoTKKyThuat = soTKKyThuat;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACCS_CapSoToKhaiPaper_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, SoTK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
			db.AddInParameter(dbCommand, "@SoTKVNACCS", SqlDbType.Decimal, SoTKVNACCS);
			db.AddInParameter(dbCommand, "@SoTKVNACCSFull", SqlDbType.Decimal, SoTKVNACCSFull);
			db.AddInParameter(dbCommand, "@SoTKDauTien", SqlDbType.Decimal, SoTKDauTien);
			db.AddInParameter(dbCommand, "@SoNhanhTK", SqlDbType.Int, SoNhanhTK);
			db.AddInParameter(dbCommand, "@TongSoTKChiaNho", SqlDbType.Int, TongSoTKChiaNho);
			db.AddInParameter(dbCommand, "@SoTKTNTX", SqlDbType.Decimal, SoTKTNTX);
			db.AddInParameter(dbCommand, "@Temp1", SqlDbType.NVarChar, Temp1);
			db.AddInParameter(dbCommand, "@SoTKPaper", SqlDbType.NVarChar, SoTKPaper);
			db.AddInParameter(dbCommand, "@SoTKKyThuat", SqlDbType.Decimal, SoTKKyThuat);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<VNACCS_CapSoToKhaiPaper> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACCS_CapSoToKhaiPaper item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVNACCS_CapSoToKhaiPaper(int soTK, string maLoaiHinh)
		{
			VNACCS_CapSoToKhaiPaper entity = new VNACCS_CapSoToKhaiPaper();
			entity.SoTK = soTK;
			entity.MaLoaiHinh = maLoaiHinh;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACCS_CapSoToKhaiPaper_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, SoTK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_VNACCS_CapSoToKhaiPaper_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<VNACCS_CapSoToKhaiPaper> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACCS_CapSoToKhaiPaper item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}