namespace Company.Interface.Report.GC
{
    partial class BangKe03NewTT74
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTKNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuong4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuong5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongCong = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGhiChu = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblbenthue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblbennhan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblhopsoHDGC = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMathang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDateFirst = new DevExpress.XtraReports.UI.XRLabel();
            this.lbllimittimefirst = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCucHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAddThue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblAddNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayKi = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayKi2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayKi1 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNPL1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNPL2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNPL3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNPL4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNPL5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNPL6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Height = 20;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.Location = new System.Drawing.Point(0, 0);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.Size = new System.Drawing.Size(1067, 20);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTKNK,
            this.lblDVT,
            this.lblLuong1,
            this.lblLuong2,
            this.lblLuong3,
            this.lblLuong4,
            this.lblLuong5,
            this.lblTongCong,
            this.lblGhiChu});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // lblTKNK
            // 
            this.lblTKNK.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTKNK.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTKNK.Multiline = true;
            this.lblTKNK.Name = "lblTKNK";
            this.lblTKNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTKNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTKNK.Weight = 0.14058106841611998;
            // 
            // lblDVT
            // 
            this.lblDVT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVT.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVT.Name = "lblDVT";
            this.lblDVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDVT.StylePriority.UseBorders = false;
            this.lblDVT.StylePriority.UseFont = false;
            this.lblDVT.StylePriority.UsePadding = false;
            this.lblDVT.StylePriority.UseTextAlignment = false;
            this.lblDVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVT.Weight = 0.038894095595126521;
            // 
            // lblLuong1
            // 
            this.lblLuong1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuong1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuong1.Name = "lblLuong1";
            this.lblLuong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuong1.Weight = 0.11668228678537958;
            this.lblLuong1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblLuong1_BeforePrint);
            // 
            // lblLuong2
            // 
            this.lblLuong2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuong2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuong2.Name = "lblLuong2";
            this.lblLuong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuong2.Weight = 0.11715089034676664;
            this.lblLuong2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblLuong2_BeforePrint);
            // 
            // lblLuong3
            // 
            this.lblLuong3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuong3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuong3.Name = "lblLuong3";
            this.lblLuong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuong3.Weight = 0.11715089034676665;
            this.lblLuong3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblLuong3_BeforePrint);
            // 
            // lblLuong4
            // 
            this.lblLuong4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuong4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuong4.Name = "lblLuong4";
            this.lblLuong4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuong4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuong4.Weight = 0.11715089034676664;
            this.lblLuong4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblLuong4_BeforePrint);
            // 
            // lblLuong5
            // 
            this.lblLuong5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuong5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuong5.Name = "lblLuong5";
            this.lblLuong5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuong5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuong5.Weight = 0.11715089034676664;
            this.lblLuong5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblLuong5_BeforePrint);
            // 
            // lblTongCong
            // 
            this.lblTongCong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongCong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongCong.Name = "lblTongCong";
            this.lblTongCong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongCong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongCong.Weight = 0.12652296157450796;
            this.lblTongCong.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblLuong6_BeforePrint);
            // 
            // lblGhiChu
            // 
            this.lblGhiChu.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblGhiChu.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhiChu.Name = "lblGhiChu";
            this.lblGhiChu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGhiChu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblGhiChu.Weight = 0.10871602624179944;
            // 
            // lblbenthue
            // 
            this.lblbenthue.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbenthue.Location = new System.Drawing.Point(125, 165);
            this.lblbenthue.Name = "lblbenthue";
            this.lblbenthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblbenthue.Size = new System.Drawing.Size(358, 20);
            this.lblbenthue.Tag = "Bên thuê GC";
            this.lblbenthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblbenthue.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lblbennhan
            // 
            this.lblbennhan.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbennhan.Location = new System.Drawing.Point(133, 185);
            this.lblbennhan.Name = "lblbennhan";
            this.lblbennhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblbennhan.Size = new System.Drawing.Size(350, 20);
            this.lblbennhan.Tag = "Bên nhận GC";
            this.lblbennhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblbennhan.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lblhopsoHDGC
            // 
            this.lblhopsoHDGC.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblhopsoHDGC.Location = new System.Drawing.Point(150, 125);
            this.lblhopsoHDGC.Name = "lblhopsoHDGC";
            this.lblhopsoHDGC.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblhopsoHDGC.Size = new System.Drawing.Size(333, 20);
            this.lblhopsoHDGC.Tag = "Số hợp đồng";
            this.lblhopsoHDGC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblhopsoHDGC.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // xrLabel58
            // 
            this.xrLabel58.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel58.Location = new System.Drawing.Point(8, 185);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel58.Size = new System.Drawing.Size(125, 20);
            this.xrLabel58.Text = "Bên nhận gia công :";
            this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel57.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold);
            this.xrLabel57.Location = new System.Drawing.Point(75, 33);
            this.xrLabel57.Multiline = true;
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.Size = new System.Drawing.Size(916, 50);
            this.xrLabel57.Text = "BẢNG TỔNG HỢP NGUYÊN LIỆU, VẬT TƯ XUẤT TRẢ RA NƯỚC NGOÀI VÀ CHUYỂN SANG HỢP ĐỒNG " +
                "GIA CÔNG KHÁC TRONG KHI ĐANG THỰC HIỆN HỢP ĐỒNG GIA CÔNG";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel56.Location = new System.Drawing.Point(8, 165);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.Size = new System.Drawing.Size(117, 20);
            this.xrLabel56.Text = "Bên thuê gia công :";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel55.Location = new System.Drawing.Point(8, 125);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel55.Size = new System.Drawing.Size(142, 20);
            this.xrLabel55.Text = "Hợp đồng gia công số :";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel54.Location = new System.Drawing.Point(8, 205);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.Size = new System.Drawing.Size(125, 20);
            this.xrLabel54.Text = "Mặt hàng gia công :";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMathang
            // 
            this.lblMathang.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMathang.Location = new System.Drawing.Point(133, 205);
            this.lblMathang.Name = "lblMathang";
            this.lblMathang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMathang.Size = new System.Drawing.Size(350, 20);
            this.lblMathang.Tag = "Mặt hàng GC";
            this.lblMathang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblMathang.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // xrLabel52
            // 
            this.xrLabel52.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel52.Location = new System.Drawing.Point(483, 125);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.Size = new System.Drawing.Size(42, 20);
            this.xrLabel52.Text = "Ngày";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDateFirst
            // 
            this.lblDateFirst.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateFirst.Location = new System.Drawing.Point(525, 125);
            this.lblDateFirst.Name = "lblDateFirst";
            this.lblDateFirst.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDateFirst.Size = new System.Drawing.Size(191, 20);
            this.lblDateFirst.Tag = "Ngày HĐ";
            this.lblDateFirst.Text = " ";
            this.lblDateFirst.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblDateFirst.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // lbllimittimefirst
            // 
            this.lbllimittimefirst.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllimittimefirst.Location = new System.Drawing.Point(783, 125);
            this.lbllimittimefirst.Name = "lbllimittimefirst";
            this.lbllimittimefirst.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbllimittimefirst.Size = new System.Drawing.Size(275, 20);
            this.lbllimittimefirst.Tag = "Hạn HĐ";
            this.lbllimittimefirst.Text = " ";
            this.lbllimittimefirst.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lbllimittimefirst.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // xrLabel49
            // 
            this.xrLabel49.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel49.Location = new System.Drawing.Point(717, 125);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.Size = new System.Drawing.Size(66, 20);
            this.xrLabel49.Text = "Thời hạn";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel48
            // 
            this.xrLabel48.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel48.Location = new System.Drawing.Point(8, 225);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.Size = new System.Drawing.Size(184, 20);
            this.xrLabel48.Text = "Đơn vị Hải quan làm thủ tục : ";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblChiCucHQ
            // 
            this.lblChiCucHQ.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChiCucHQ.Location = new System.Drawing.Point(192, 225);
            this.lblChiCucHQ.Name = "lblChiCucHQ";
            this.lblChiCucHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHQ.Size = new System.Drawing.Size(867, 20);
            this.lblChiCucHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel46.Location = new System.Drawing.Point(483, 165);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.Size = new System.Drawing.Size(59, 20);
            this.xrLabel46.Text = "Địa chỉ :";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblAddThue
            // 
            this.lblAddThue.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddThue.Location = new System.Drawing.Point(542, 165);
            this.lblAddThue.Name = "lblAddThue";
            this.lblAddThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAddThue.Size = new System.Drawing.Size(517, 20);
            this.lblAddThue.Tag = "ĐC bên thuê";
            this.lblAddThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblAddThue.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // xrLabel44
            // 
            this.xrLabel44.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel44.Location = new System.Drawing.Point(483, 185);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.Size = new System.Drawing.Size(59, 20);
            this.xrLabel44.Text = "Địa chỉ :";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblAddNhan
            // 
            this.lblAddNhan.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddNhan.Location = new System.Drawing.Point(542, 185);
            this.lblAddNhan.Name = "lblAddNhan";
            this.lblAddNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblAddNhan.Size = new System.Drawing.Size(517, 20);
            this.lblAddNhan.Tag = "ĐC bên nhận";
            this.lblAddNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblAddNhan.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // xrLabel42
            // 
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel42.Location = new System.Drawing.Point(483, 205);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.Size = new System.Drawing.Size(66, 20);
            this.xrLabel42.Text = "Số lượng:";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoLuong
            // 
            this.lblSoLuong.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuong.Location = new System.Drawing.Point(550, 205);
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuong.Size = new System.Drawing.Size(509, 20);
            this.lblSoLuong.Tag = "Lượng hàng";
            this.lblSoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoLuong.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // xrLabel40
            // 
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.Location = new System.Drawing.Point(842, 83);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.Size = new System.Drawing.Size(50, 17);
            this.xrLabel40.Text = "Tờ số : ";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.Location = new System.Drawing.Point(717, 145);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.Size = new System.Drawing.Size(66, 20);
            this.xrLabel38.Text = "Thời hạn";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.Location = new System.Drawing.Point(783, 145);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.Size = new System.Drawing.Size(275, 20);
            this.xrLabel35.Tag = "Hạn phụ lục";
            this.xrLabel35.Text = " ";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel35.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.Location = new System.Drawing.Point(525, 145);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.Size = new System.Drawing.Size(191, 20);
            this.xrLabel32.Tag = "Ngày phụ lục";
            this.xrLabel32.Text = " ";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel32.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // xrLabel30
            // 
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.Location = new System.Drawing.Point(483, 145);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.Size = new System.Drawing.Size(42, 20);
            this.xrLabel30.Text = "Ngày";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.Location = new System.Drawing.Point(8, 145);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.Size = new System.Drawing.Size(184, 20);
            this.xrLabel23.Text = "Phụ lục hợp đồng gia công số :";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.Location = new System.Drawing.Point(192, 145);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.Size = new System.Drawing.Size(291, 20);
            this.xrLabel17.Tag = "Phụ lục HĐ";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel17.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblhopsoHDGC_PreviewClick);
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.Location = new System.Drawing.Point(842, 8);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.Size = new System.Drawing.Size(209, 17);
            this.xrLabel13.Text = "Mẫu: 03/HSTK-GC, Khổ A4";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel74,
            this.xrLabel75,
            this.xrLabel76,
            this.xrLabel77});
            this.ReportFooter.Height = 157;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel74
            // 
            this.xrLabel74.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel74.Location = new System.Drawing.Point(0, 42);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel74.Size = new System.Drawing.Size(308, 25);
            this.xrLabel74.Text = "Công chức Hải quan kiểm tra, đối chiếu";
            // 
            // xrLabel75
            // 
            this.xrLabel75.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel75.Location = new System.Drawing.Point(450, 42);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel75.Size = new System.Drawing.Size(266, 25);
            this.xrLabel75.Text = "Giám đốc doanh nghiệp";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel76
            // 
            this.xrLabel76.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel76.Location = new System.Drawing.Point(0, 17);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel76.Size = new System.Drawing.Size(308, 25);
            this.xrLabel76.Text = "Đà Nẵng, ngày ... tháng ... năm......";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel77
            // 
            this.xrLabel77.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel77.Location = new System.Drawing.Point(450, 17);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel77.Size = new System.Drawing.Size(266, 25);
            this.xrLabel77.Text = "Đà Nẵng, ngày ... tháng ... năm......";
            this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayKi
            // 
            this.lblNgayKi.BackColor = System.Drawing.Color.Transparent;
            this.lblNgayKi.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayKi.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayKi.Location = new System.Drawing.Point(817, 42);
            this.lblNgayKi.Name = "lblNgayKi";
            this.lblNgayKi.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblNgayKi.Size = new System.Drawing.Size(250, 25);
            this.lblNgayKi.Text = "............., ngày ... tháng ... năm......";
            this.lblNgayKi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNgayKi.Visible = false;
            // 
            // lblNgayKi2
            // 
            this.lblNgayKi2.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayKi2.Location = new System.Drawing.Point(817, 92);
            this.lblNgayKi2.Name = "lblNgayKi2";
            this.lblNgayKi2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayKi2.Size = new System.Drawing.Size(250, 25);
            this.lblNgayKi2.Text = "(Ký tên, đóng dấu)";
            this.lblNgayKi2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNgayKi2.Visible = false;
            // 
            // lblNgayKi1
            // 
            this.lblNgayKi1.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayKi1.Location = new System.Drawing.Point(817, 67);
            this.lblNgayKi1.Name = "lblNgayKi1";
            this.lblNgayKi1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayKi1.Size = new System.Drawing.Size(250, 25);
            this.lblNgayKi1.Text = "Giám đốc doanh nghiệp";
            this.lblNgayKi1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNgayKi1.Visible = false;
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgayKi1,
            this.lblNgayKi2,
            this.lblNgayKi});
            this.ReportFooter1.Height = 117;
            this.ReportFooter1.Name = "ReportFooter1";
            this.ReportFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.ReportFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel13,
            this.xrLabel40,
            this.lblbenthue,
            this.xrLabel17,
            this.xrLabel23,
            this.xrLabel30,
            this.xrLabel32,
            this.xrLabel35,
            this.xrLabel38,
            this.lblSoLuong,
            this.xrLabel42,
            this.lblAddNhan,
            this.xrLabel44,
            this.lblAddThue,
            this.xrLabel46,
            this.lblChiCucHQ,
            this.xrLabel48,
            this.xrLabel49,
            this.lbllimittimefirst,
            this.lblDateFirst,
            this.xrLabel52,
            this.lblMathang,
            this.xrLabel54,
            this.xrLabel55,
            this.xrLabel56,
            this.xrLabel58,
            this.lblhopsoHDGC,
            this.lblbennhan,
            this.xrLabel57});
            this.ReportHeader.Height = 255;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.Location = new System.Drawing.Point(0, 0);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.Size = new System.Drawing.Size(1067, 50);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell3,
            this.lblNPL1,
            this.lblNPL2,
            this.lblNPL3,
            this.lblNPL4,
            this.lblNPL5,
            this.lblNPL6,
            this.xrTableCell10});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.BackColor = System.Drawing.Color.Azure;
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "Tên nguyên liệu, vật tư";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell2.Weight = 0.14058106841611998;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BackColor = System.Drawing.Color.Azure;
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBackColor = false;
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "Đv tính";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell3.Weight = 0.039831302717900655;
            // 
            // lblNPL1
            // 
            this.lblNPL1.BackColor = System.Drawing.Color.Azure;
            this.lblNPL1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNPL1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNPL1.Multiline = true;
            this.lblNPL1.Name = "lblNPL1";
            this.lblNPL1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNPL1.StylePriority.UseTextAlignment = false;
            this.lblNPL1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNPL1.Weight = 0.11668228678537958;
            // 
            // lblNPL2
            // 
            this.lblNPL2.BackColor = System.Drawing.Color.Azure;
            this.lblNPL2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNPL2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNPL2.Multiline = true;
            this.lblNPL2.Name = "lblNPL2";
            this.lblNPL2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNPL2.StylePriority.UseTextAlignment = false;
            this.lblNPL2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNPL2.Weight = 0.11715089034676664;
            // 
            // lblNPL3
            // 
            this.lblNPL3.BackColor = System.Drawing.Color.Azure;
            this.lblNPL3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNPL3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNPL3.Multiline = true;
            this.lblNPL3.Name = "lblNPL3";
            this.lblNPL3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNPL3.StylePriority.UseTextAlignment = false;
            this.lblNPL3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNPL3.Weight = 0.11715089034676665;
            // 
            // lblNPL4
            // 
            this.lblNPL4.BackColor = System.Drawing.Color.Azure;
            this.lblNPL4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNPL4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNPL4.Multiline = true;
            this.lblNPL4.Name = "lblNPL4";
            this.lblNPL4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNPL4.StylePriority.UseTextAlignment = false;
            this.lblNPL4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNPL4.Weight = 0.11715089034676664;
            // 
            // lblNPL5
            // 
            this.lblNPL5.BackColor = System.Drawing.Color.Azure;
            this.lblNPL5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNPL5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNPL5.Multiline = true;
            this.lblNPL5.Name = "lblNPL5";
            this.lblNPL5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNPL5.StylePriority.UseTextAlignment = false;
            this.lblNPL5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNPL5.Weight = 0.11715089034676665;
            // 
            // lblNPL6
            // 
            this.lblNPL6.BackColor = System.Drawing.Color.Azure;
            this.lblNPL6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNPL6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNPL6.Multiline = true;
            this.lblNPL6.Name = "lblNPL6";
            this.lblNPL6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNPL6.StylePriority.UseTextAlignment = false;
            this.lblNPL6.Text = "Tổng cộng";
            this.lblNPL6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNPL6.Weight = 0.12652296157450799;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.BackColor = System.Drawing.Color.Azure;
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "Ghi chú";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell10.Weight = 0.1077788191190253;
            // 
            // xrLabel6
            // 
            this.xrLabel6.BackColor = System.Drawing.Color.Azure;
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.Location = new System.Drawing.Point(950, 50);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.Size = new System.Drawing.Size(117, 20);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.Text = "         ";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.BackColor = System.Drawing.Color.Azure;
            this.xrLabel15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel15.Location = new System.Drawing.Point(317, 50);
            this.xrLabel15.Multiline = true;
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.Size = new System.Drawing.Size(125, 20);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.Text = "Lượng";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.BackColor = System.Drawing.Color.Azure;
            this.xrLabel2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel2.Location = new System.Drawing.Point(442, 50);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.Size = new System.Drawing.Size(125, 20);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Lượng";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BackColor = System.Drawing.Color.Azure;
            this.xrLabel3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel3.Location = new System.Drawing.Point(567, 50);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.Size = new System.Drawing.Size(125, 20);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "Lượng";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.BackColor = System.Drawing.Color.Azure;
            this.xrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel4.Location = new System.Drawing.Point(692, 50);
            this.xrLabel4.Multiline = true;
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.Size = new System.Drawing.Size(125, 20);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "Lượng";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.BackColor = System.Drawing.Color.Azure;
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel5.Location = new System.Drawing.Point(817, 50);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.Size = new System.Drawing.Size(135, 20);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.BackColor = System.Drawing.Color.Azure;
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.Location = new System.Drawing.Point(0, 50);
            this.xrLabel8.Multiline = true;
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.Size = new System.Drawing.Size(150, 20);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.Text = "          ";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabel7,
            this.xrLabel8,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel15,
            this.xrLabel6,
            this.xrTable3});
            this.PageHeader.Height = 70;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.Azure;
            this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel1.Location = new System.Drawing.Point(192, 50);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Size = new System.Drawing.Size(125, 20);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Lượng";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.BackColor = System.Drawing.Color.Azure;
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabel7.Location = new System.Drawing.Point(150, 50);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.Size = new System.Drawing.Size(42, 20);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BangKe03NewTT74
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportFooter1,
            this.ReportHeader,
            this.PageHeader});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(80, 15, 25, 25);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblTKNK;
        private DevExpress.XtraReports.UI.XRTableCell lblLuong2;
        private DevExpress.XtraReports.UI.XRTableCell lblLuong3;
        private DevExpress.XtraReports.UI.XRTableCell lblLuong4;
        private DevExpress.XtraReports.UI.XRTableCell lblLuong5;
        private DevExpress.XtraReports.UI.XRTableCell lblTongCong;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChu;
        private DevExpress.XtraReports.UI.XRTableCell lblLuong1;
        private DevExpress.XtraReports.UI.XRLabel lblbennhan;
        private DevExpress.XtraReports.UI.XRLabel lblhopsoHDGC;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel lblMathang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel lblDateFirst;
        private DevExpress.XtraReports.UI.XRLabel lbllimittimefirst;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHQ;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel lblAddThue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel lblAddNhan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel lblbenthue;
        private DevExpress.XtraReports.UI.XRLabel lblNgayKi1;
        private DevExpress.XtraReports.UI.XRLabel lblNgayKi2;
        private DevExpress.XtraReports.UI.XRLabel lblNgayKi;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell lblNPL1;
        private DevExpress.XtraReports.UI.XRTableCell lblNPL2;
        private DevExpress.XtraReports.UI.XRTableCell lblNPL3;
        private DevExpress.XtraReports.UI.XRTableCell lblNPL4;
        private DevExpress.XtraReports.UI.XRTableCell lblNPL5;
        private DevExpress.XtraReports.UI.XRTableCell lblNPL6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRTableCell lblDVT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
    }
}
