-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Update]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Load]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Insert]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Insert]
	@SoTK int,
	@MaLoaiHinh varchar(50),
	@NamDangKy int,
	@SoTKVNACCS decimal(15, 0),
	@SoTKVNACCSFull decimal(12, 0),
	@SoTKDauTien decimal(15, 0),
	@SoNhanhTK int,
	@TongSoTKChiaNho int,
	@SoTKTNTX decimal(15, 0),
	@Temp1 nvarchar(255)
AS
INSERT INTO [dbo].[t_VNACCS_CapSoToKhai]
(
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1]
)
VALUES
(
	@SoTK,
	@MaLoaiHinh,
	@NamDangKy,
	@SoTKVNACCS,
	@SoTKVNACCSFull,
	@SoTKDauTien,
	@SoNhanhTK,
	@TongSoTKChiaNho,
	@SoTKTNTX,
	@Temp1
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Update]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Update]
	@SoTK int,
	@MaLoaiHinh varchar(50),
	@NamDangKy int,
	@SoTKVNACCS decimal(15, 0),
	@SoTKVNACCSFull decimal(12, 0),
	@SoTKDauTien decimal(15, 0),
	@SoNhanhTK int,
	@TongSoTKChiaNho int,
	@SoTKTNTX decimal(15, 0),
	@Temp1 nvarchar(255)
AS

UPDATE
	[dbo].[t_VNACCS_CapSoToKhai]
SET
	[NamDangKy] = @NamDangKy,
	[SoTKVNACCS] = @SoTKVNACCS,
	[SoTKVNACCSFull] = @SoTKVNACCSFull,
	[SoTKDauTien] = @SoTKDauTien,
	[SoNhanhTK] = @SoNhanhTK,
	[TongSoTKChiaNho] = @TongSoTKChiaNho,
	[SoTKTNTX] = @SoTKTNTX,
	[Temp1] = @Temp1
WHERE
	[SoTK] = @SoTK
	AND [MaLoaiHinh] = @MaLoaiHinh

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]
	@SoTK int,
	@MaLoaiHinh varchar(50),
	@NamDangKy int,
	@SoTKVNACCS decimal(15, 0),
	@SoTKVNACCSFull decimal(12, 0),
	@SoTKDauTien decimal(15, 0),
	@SoNhanhTK int,
	@TongSoTKChiaNho int,
	@SoTKTNTX decimal(15, 0),
	@Temp1 nvarchar(255)
AS
IF EXISTS(SELECT [SoTK], [MaLoaiHinh] FROM [dbo].[t_VNACCS_CapSoToKhai] WHERE [SoTK] = @SoTK AND [MaLoaiHinh] = @MaLoaiHinh)
	BEGIN
		UPDATE
			[dbo].[t_VNACCS_CapSoToKhai] 
		SET
			[NamDangKy] = @NamDangKy,
			[SoTKVNACCS] = @SoTKVNACCS,
			[SoTKVNACCSFull] = @SoTKVNACCSFull,
			[SoTKDauTien] = @SoTKDauTien,
			[SoNhanhTK] = @SoNhanhTK,
			[TongSoTKChiaNho] = @TongSoTKChiaNho,
			[SoTKTNTX] = @SoTKTNTX,
			[Temp1] = @Temp1
		WHERE
			[SoTK] = @SoTK
			AND [MaLoaiHinh] = @MaLoaiHinh
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACCS_CapSoToKhai]
	(
			[SoTK],
			[MaLoaiHinh],
			[NamDangKy],
			[SoTKVNACCS],
			[SoTKVNACCSFull],
			[SoTKDauTien],
			[SoNhanhTK],
			[TongSoTKChiaNho],
			[SoTKTNTX],
			[Temp1]
	)
	VALUES
	(
			@SoTK,
			@MaLoaiHinh,
			@NamDangKy,
			@SoTKVNACCS,
			@SoTKVNACCSFull,
			@SoTKDauTien,
			@SoNhanhTK,
			@TongSoTKChiaNho,
			@SoTKTNTX,
			@Temp1
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Delete]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Delete]
	@SoTK int,
	@MaLoaiHinh varchar(50)
AS

DELETE FROM 
	[dbo].[t_VNACCS_CapSoToKhai]
WHERE
	[SoTK] = @SoTK
	AND [MaLoaiHinh] = @MaLoaiHinh

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACCS_CapSoToKhai] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Load]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Load]
	@SoTK int,
	@MaLoaiHinh varchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1]
FROM
	[dbo].[t_VNACCS_CapSoToKhai]
WHERE
	[SoTK] = @SoTK
	AND [MaLoaiHinh] = @MaLoaiHinh
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1]
FROM [dbo].[t_VNACCS_CapSoToKhai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1]
FROM
	[dbo].[t_VNACCS_CapSoToKhai]	

GO

