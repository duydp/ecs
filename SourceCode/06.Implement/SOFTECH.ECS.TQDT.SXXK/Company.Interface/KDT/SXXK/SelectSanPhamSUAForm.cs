﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Company.BLL.DuLieuChuan;

namespace Company.Interface.KDT.SXXK
{
    public partial class SelectSanPhamSUAForm : BaseForm
    {
        public SanPhamCollection spCollection = new SanPhamCollection();
        public SanPhamDangKy SPDangKy = new SanPhamDangKy();
        public List<SanPhamSUA> listSpSUA = new List<SanPhamSUA>();
        public long masterId;
        public SelectSanPhamSUAForm()
        {
            InitializeComponent();
        }

        private void btnChonNhieuHang_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPham sp = (SanPham)i.GetRow().DataRow;
                        SanPhamSUA spSua = new SanPhamSUA();
                        spSua.Ma = sp.Ma;
                        spSua.Ten = sp.Ten;
                        spSua.MaHS = sp.MaHS;
                        spSua.STTHang = sp.STTHang;
                        spSua.DVT_ID = sp.DVT_ID;
                        spSua.IDSP = sp.ID;
                        listSpSUA.Add(spSua);
                    }
                }
            }
            this.Close();
        }

        private void btnChonAll_Click(object sender, EventArgs e)
        {
            //GridEXRow[] items = dgList.GetRows();
            //if (dgList.GetRows().Length < 0) return;
            //if (items.Length <= 0) return;
            //{
            //    foreach (GridEXRow i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            HangMauDich HMD = (HangMauDich)i.DataRow;
            //            if (!checkHangExit(HMD.ID))
            //                HMDTMPCollection.Add(HMD);
            //        }
            //    }
            //}
            //this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectSanPhamForm_Load(object sender, EventArgs e)
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            BindData();
        }
        public void BindData()
        {
            //DataSet ds = Company.BLL.KDT.SXXK.SanPham.SelectCollectionBy_Master_ID(masterId);
            SanPhamCollection ds = Company.BLL.KDT.SXXK.SanPham.SelectCollectionBy_Master_ID(masterId);
            dgList.DataSource = ds;

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }
    }
}
