﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.DuLieuChuan;
using Company.Interface.KDT.SXXK;
using Janus.Windows.GridEX;


namespace Company.Interface.DanhMucChuan
{
    public partial class DanhMucLoaiSPForm : BaseForm
    {
        public DanhMucSanPhamGiaCongCollection dmSPGCCollection = new DanhMucSanPhamGiaCongCollection();
        public DanhMucSanPhamGiaCong dmSPGC = new DanhMucSanPhamGiaCong();
        public bool isBrower = false;
        public bool isEdit = false;
        public DanhMucLoaiSPForm()
        {
            InitializeComponent();
        }

        private void DanhMucLoaiSPForm_Load(object sender, EventArgs e)
        {
            dmSPGCCollection = dmSPGC.SelectCollectionAll();
            dgList.DataSource = dmSPGCCollection;

        }

        private void addNew_Click(object sender, EventArgs e)
        {
            DanhmucSPGCEditForm f = new DanhmucSPGCEditForm();
            f.dmSPGCCollection = this.dmSPGCCollection;
            f.ShowDialog();
            try
            {
                dmSPGCCollection = dmSPGC.SelectCollectionAll();
                dgList.DataSource = dmSPGCCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                dgList.Refresh();
            }
        }

        private void addEX_Click(object sender, EventArgs e)
        {
            DanhMucSPGCReadExcelForm rex = new DanhMucSPGCReadExcelForm();            
            rex.ShowDialog();
            try
            {
                dmSPGCCollection = dmSPGC.SelectCollectionAll();
                dgList.DataSource = dmSPGCCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                dgList.Refresh();
            }
            //rex.dmSPGCCollection = this.dmSPGCCollection;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                dmSPGC.InsertUpdate(dmSPGCCollection );
                //ShowMessage("Lưu thành công.", false);
                showMsg("MSG_SAV02");
            
            }
            catch (Exception ex)
            {
                ShowMessage(setText("Lỗi: ", "Error") + ex.Message, false);
                //MLMessages("Lưu không thành công", "MSG_SAV01", "", false);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DanhMucSanPhamGiaCongCollection dmspcolltemp = new DanhMucSanPhamGiaCongCollection();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            if (ShowMessage(setText("Bạn có muốn xóa hay không ?", "Do you want to delete the selected row(s)?"), true) == "Yes")
            {
                foreach (GridEXSelectedItem item in items)
                {
                    if (item.RowType == RowType.Record)
                    {
                        DanhMucSanPhamGiaCong dmtemp = (DanhMucSanPhamGiaCong)item.GetRow().DataRow;
                        dmtemp.Delete();
                    }

                }

            }
            try
            {
                dmSPGCCollection = dmSPGC.SelectCollectionAll();
                dgList.DataSource = dmSPGCCollection;
            }
            catch { }
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
             DanhMucSanPhamGiaCongCollection dmspcolltemp = new DanhMucSanPhamGiaCongCollection();
             GridEXSelectedItemCollection items = dgList.SelectedItems;
             if (items.Count <= 0) return;
             if (ShowMessage(setText("Bạn có muốn xóa hay không ?", "Do you want to delete the selected row(s)?"), true) == "Yes")
            {
                foreach (GridEXSelectedItem item in items)
                {
                    if(item.RowType ==RowType.Record )
                    {
                        DanhMucSanPhamGiaCong dmtemp = (DanhMucSanPhamGiaCong)item.GetRow().DataRow;
                        dmtemp.Delete();
                    }

                }

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            dmSPGC = null;
        }






        private bool CheckMaSP(string masp)
        {
            foreach (DanhMucSanPhamGiaCong  dmtemp in dmSPGCCollection )
            {
                if (dmtemp.MaSanPham.ToUpper().Trim() == masp.Trim().ToUpper())
                    return true;
            }
            return false;
        }

        private void dgList_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "MaSanPham")
            {
                string s = e.Value.ToString();
                if (CheckMaSP(s))
                {
                    //ShowMessage("Loại sản phẩm này đã có.", false);
                    showMsg("MSG_0203080");
                    e.Cancel = true;
                }
                if (s.Trim().Length == 0)
                {
                    //ShowMessage("Mã loại sản phẩm không được rỗng.", false);
                    showMsg("MSG_0203081");
                    e.Cancel = true;
                }
            }

        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {

        }
       
    }
}