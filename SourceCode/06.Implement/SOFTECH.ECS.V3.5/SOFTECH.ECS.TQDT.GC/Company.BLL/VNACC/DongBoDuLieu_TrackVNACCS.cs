﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace Company.GC.BLL.VNACC
{
    public partial class DongBoDuLieu_TrackVNACCS
    {
        public static DongBoDuLieu_TrackVNACCS LoadBy(string soToKhai, string tuNgay,string denNgay,  string maDoanhNghiep)
        {
            const string spName = "[dbo].[p_DongBoDuLieu_TrackVNACCS_LoadBy]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.VarChar, soToKhai);
            db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.VarChar, tuNgay);
            db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.VarChar, denNgay);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);

            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<DongBoDuLieu_TrackVNACCS> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }
        public static List<DongBoDuLieu_TrackVNACCS> SelectCollectionSoToKhai(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);

        }
        public static List<string> SelectListSoToKhai(string where)
        {
            List<string> myData = new List<string>();
            string spName = "Select SoToKhai from t_DongBoDuLieu_TrackVNACCS where " + where;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                string a;
                a = reader.GetString(0);
                myData.Add(a);
            }
            return myData;
        }
    }
}
