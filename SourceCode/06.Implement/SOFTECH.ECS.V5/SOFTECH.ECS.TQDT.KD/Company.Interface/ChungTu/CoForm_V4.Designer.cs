﻿namespace Company.Interface
{
    partial class CoForm_V4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CoForm_V4));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrCangXepHang = new Company.Interface.Controls.CuaKhauHControl();
            this.ccNgayKhoiHanh = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtHamLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.cbLoaiCO = new Janus.Windows.EditControls.UIComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.ctrCangDoHang = new Company.Interface.Controls.CuaKhauHControl();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.ccNgayHetHan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtNguoiKy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ctrMaNuocNK = new Company.Interface.Controls.NuocHControl();
            this.label3 = new System.Windows.Forms.Label();
            this.ctrMaNuocXK = new Company.Interface.Controls.NuocHControl();
            this.label2 = new System.Windows.Forms.Label();
            this.ctrNuocCapCO = new Company.Interface.Controls.NuocHControl();
            this.txtToChucCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSoCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ccNgayCO = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.chkNoCo = new Janus.Windows.EditControls.UICheckBox();
            this.lblNgayNopCo = new System.Windows.Forms.Label();
            this.ccNgayNopCO = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiNguoiNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiNguoiXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvNgayCO = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoCO = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDiaChiXK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDiaChiNK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNguoiKyCO = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnChonGP = new Janus.Windows.EditControls.UIButton();
            this.grpTiepNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.btnKetQuaXuLy = new Janus.Windows.EditControls.UIButton();
            this.ccNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.lbTrangThai = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnKhaiBao = new Janus.Windows.EditControls.UIButton();
            this.btnLayPhanHoi = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaNguoiNhap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaNguoiXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMoTaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtThongTinMoTa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.rvHL = new Company.Controls.CustomValidation.RangeValidator();
            this.cmdThuTucHQTruocDo = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayCO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoCO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaChiXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaChiNK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiKyCO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).BeginInit();
            this.grpTiepNhan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rvHL)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.btnKhaiBao);
            this.grbMain.Controls.Add(this.btnLayPhanHoi);
            this.grbMain.Controls.Add(this.grpTiepNhan);
            this.grbMain.Controls.Add(this.chkNoCo);
            this.grbMain.Controls.Add(this.lblNgayNopCo);
            this.grbMain.Controls.Add(this.cmdThuTucHQTruocDo);
            this.grbMain.Controls.Add(this.btnChonGP);
            this.grbMain.Controls.Add(this.ccNgayNopCO);
            this.grbMain.Controls.Add(this.btnXoa);
            this.grbMain.Controls.Add(this.btnGhi);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(864, 679);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.ctrCangXepHang);
            this.uiGroupBox2.Controls.Add(this.ccNgayKhoiHanh);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.label18);
            this.uiGroupBox2.Controls.Add(this.label17);
            this.uiGroupBox2.Controls.Add(this.txtHamLuong);
            this.uiGroupBox2.Controls.Add(this.cbLoaiCO);
            this.uiGroupBox2.Controls.Add(this.label19);
            this.uiGroupBox2.Controls.Add(this.ctrCangDoHang);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.label13);
            this.uiGroupBox2.Controls.Add(this.ccNgayHetHan);
            this.uiGroupBox2.Controls.Add(this.txtNguoiKy);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.ctrMaNuocNK);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.ctrMaNuocXK);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.ctrNuocCapCO);
            this.uiGroupBox2.Controls.Add(this.txtToChucCap);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.txtSoCO);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.ccNgayCO);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(9, 67);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(848, 159);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // ctrCangXepHang
            // 
            this.ctrCangXepHang.BackColor = System.Drawing.Color.Transparent;
            this.ctrCangXepHang.ErrorMessage = "\"Cửa khẩu\" không được bỏ trống.";
            this.ctrCangXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrCangXepHang.Location = new System.Drawing.Point(454, 100);
            this.ctrCangXepHang.Ma = "";
            this.ctrCangXepHang.Name = "ctrCangXepHang";
            this.ctrCangXepHang.ReadOnly = false;
            this.ctrCangXepHang.Size = new System.Drawing.Size(285, 22);
            this.ctrCangXepHang.TabIndex = 32;
            this.ctrCangXepHang.VisualStyleManager = null;
            // 
            // ccNgayKhoiHanh
            // 
            // 
            // 
            // 
            this.ccNgayKhoiHanh.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayKhoiHanh.DropDownCalendar.Name = "";
            this.ccNgayKhoiHanh.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKhoiHanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKhoiHanh.IsNullDate = true;
            this.ccNgayKhoiHanh.Location = new System.Drawing.Point(454, 126);
            this.ccNgayKhoiHanh.Name = "ccNgayKhoiHanh";
            this.ccNgayKhoiHanh.Nullable = true;
            this.ccNgayKhoiHanh.NullButtonText = "Xóa";
            this.ccNgayKhoiHanh.ShowNullButton = true;
            this.ccNgayKhoiHanh.Size = new System.Drawing.Size(101, 21);
            this.ccNgayKhoiHanh.TabIndex = 10;
            this.ccNgayKhoiHanh.TodayButtonText = "Hôm nay";
            this.ccNgayKhoiHanh.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKhoiHanh.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(604, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Loại CO";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(370, 105);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 13);
            this.label18.TabIndex = 34;
            this.label18.Text = "Cảng xếp hàng";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(370, 132);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 13);
            this.label17.TabIndex = 31;
            this.label17.Text = "Ngày khởi hành";
            // 
            // txtHamLuong
            // 
            this.txtHamLuong.DecimalDigits = 4;
            this.txtHamLuong.Location = new System.Drawing.Point(654, 44);
            this.txtHamLuong.MaxLength = 10;
            this.txtHamLuong.Name = "txtHamLuong";
            this.txtHamLuong.Size = new System.Drawing.Size(85, 21);
            this.txtHamLuong.TabIndex = 32;
            this.txtHamLuong.Tag = "SoContainer40";
            this.txtHamLuong.Text = "0.0000";
            this.txtHamLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtHamLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtHamLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtHamLuong.VisualStyleManager = this.vsmMain;
            // 
            // cbLoaiCO
            // 
            this.cbLoaiCO.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbLoaiCO.DisplayMember = "Name";
            this.cbLoaiCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLoaiCO.Location = new System.Drawing.Point(654, 126);
            this.cbLoaiCO.Name = "cbLoaiCO";
            this.cbLoaiCO.Size = new System.Drawing.Size(85, 21);
            this.cbLoaiCO.TabIndex = 11;
            this.cbLoaiCO.ValueMember = "ID";
            this.cbLoaiCO.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbLoaiCO.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(370, 79);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 13);
            this.label19.TabIndex = 33;
            this.label19.Text = "Cảng dở hàng";
            // 
            // ctrCangDoHang
            // 
            this.ctrCangDoHang.BackColor = System.Drawing.Color.Transparent;
            this.ctrCangDoHang.ErrorMessage = "\"Cửa khẩu\" không được bỏ trống.";
            this.ctrCangDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrCangDoHang.Location = new System.Drawing.Point(454, 74);
            this.ctrCangDoHang.Ma = "";
            this.ctrCangDoHang.Name = "ctrCangDoHang";
            this.ctrCangDoHang.ReadOnly = false;
            this.ctrCangDoHang.Size = new System.Drawing.Size(285, 22);
            this.ctrCangDoHang.TabIndex = 31;
            this.ctrCangDoHang.VisualStyleManager = null;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(745, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "(%)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(587, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Hàm Lượng";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(576, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Ngày hết hạn";
            // 
            // ccNgayHetHan
            // 
            // 
            // 
            // 
            this.ccNgayHetHan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHetHan.DropDownCalendar.Name = "";
            this.ccNgayHetHan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHetHan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHetHan.IsNullDate = true;
            this.ccNgayHetHan.Location = new System.Drawing.Point(654, 17);
            this.ccNgayHetHan.Name = "ccNgayHetHan";
            this.ccNgayHetHan.Nullable = true;
            this.ccNgayHetHan.NullButtonText = "Xóa";
            this.ccNgayHetHan.ShowNullButton = true;
            this.ccNgayHetHan.Size = new System.Drawing.Size(85, 21);
            this.ccNgayHetHan.TabIndex = 2;
            this.ccNgayHetHan.TodayButtonText = "Hôm nay";
            this.ccNgayHetHan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHetHan.VisualStyleManager = this.vsmMain;
            // 
            // txtNguoiKy
            // 
            this.txtNguoiKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiKy.Location = new System.Drawing.Point(454, 43);
            this.txtNguoiKy.MaxLength = 255;
            this.txtNguoiKy.Name = "txtNguoiKy";
            this.txtNguoiKy.Size = new System.Drawing.Size(101, 21);
            this.txtNguoiKy.TabIndex = 4;
            this.txtNguoiKy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNguoiKy.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(370, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Người ký CO";
            // 
            // ctrMaNuocNK
            // 
            this.ctrMaNuocNK.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaNuocNK.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrMaNuocNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrMaNuocNK.Location = new System.Drawing.Point(169, 127);
            this.ctrMaNuocNK.Ma = "";
            this.ctrMaNuocNK.Name = "ctrMaNuocNK";
            this.ctrMaNuocNK.ReadOnly = false;
            this.ctrMaNuocNK.Size = new System.Drawing.Size(182, 22);
            this.ctrMaNuocNK.TabIndex = 9;
            this.ctrMaNuocNK.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Mã nước nhập khẩu trên CO";
            // 
            // ctrMaNuocXK
            // 
            this.ctrMaNuocXK.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaNuocXK.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrMaNuocXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrMaNuocXK.Location = new System.Drawing.Point(169, 99);
            this.ctrMaNuocXK.Ma = "";
            this.ctrMaNuocXK.Name = "ctrMaNuocXK";
            this.ctrMaNuocXK.ReadOnly = false;
            this.ctrMaNuocXK.Size = new System.Drawing.Size(182, 22);
            this.ctrMaNuocXK.TabIndex = 7;
            this.ctrMaNuocXK.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Mã nước xuất khẩu trên CO";
            // 
            // ctrNuocCapCO
            // 
            this.ctrNuocCapCO.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocCapCO.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrNuocCapCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocCapCO.Location = new System.Drawing.Point(169, 71);
            this.ctrNuocCapCO.Ma = "";
            this.ctrNuocCapCO.Name = "ctrNuocCapCO";
            this.ctrNuocCapCO.ReadOnly = false;
            this.ctrNuocCapCO.Size = new System.Drawing.Size(182, 22);
            this.ctrNuocCapCO.TabIndex = 5;
            this.ctrNuocCapCO.VisualStyleManager = this.vsmMain;
            // 
            // txtToChucCap
            // 
            this.txtToChucCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToChucCap.Location = new System.Drawing.Point(169, 44);
            this.txtToChucCap.MaxLength = 255;
            this.txtToChucCap.Name = "txtToChucCap";
            this.txtToChucCap.Size = new System.Drawing.Size(168, 21);
            this.txtToChucCap.TabIndex = 3;
            this.txtToChucCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtToChucCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtToChucCap.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tổ chức cấp";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(22, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Nước cấp CO";
            // 
            // txtSoCO
            // 
            this.txtSoCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCO.Location = new System.Drawing.Point(169, 17);
            this.txtSoCO.MaxLength = 255;
            this.txtSoCO.Name = "txtSoCO";
            this.txtSoCO.Size = new System.Drawing.Size(167, 21);
            this.txtSoCO.TabIndex = 0;
            this.txtSoCO.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoCO.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(370, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Ngày CO";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(22, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(37, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Số CO";
            // 
            // ccNgayCO
            // 
            // 
            // 
            // 
            this.ccNgayCO.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayCO.DropDownCalendar.Name = "";
            this.ccNgayCO.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayCO.IsNullDate = true;
            this.ccNgayCO.Location = new System.Drawing.Point(454, 16);
            this.ccNgayCO.Name = "ccNgayCO";
            this.ccNgayCO.Nullable = true;
            this.ccNgayCO.NullButtonText = "Xóa";
            this.ccNgayCO.ShowNullButton = true;
            this.ccNgayCO.Size = new System.Drawing.Size(101, 21);
            this.ccNgayCO.TabIndex = 1;
            this.ccNgayCO.TodayButtonText = "Hôm nay";
            this.ccNgayCO.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayCO.VisualStyleManager = this.vsmMain;
            // 
            // chkNoCo
            // 
            this.chkNoCo.BackColor = System.Drawing.Color.Transparent;
            this.chkNoCo.Location = new System.Drawing.Point(296, 599);
            this.chkNoCo.Name = "chkNoCo";
            this.chkNoCo.Size = new System.Drawing.Size(64, 23);
            this.chkNoCo.TabIndex = 15;
            this.chkNoCo.Text = "Nợ CO";
            this.chkNoCo.CheckedChanged += new System.EventHandler(this.chkNoCo_CheckedChanged);
            // 
            // lblNgayNopCo
            // 
            this.lblNgayNopCo.AutoSize = true;
            this.lblNgayNopCo.BackColor = System.Drawing.Color.Transparent;
            this.lblNgayNopCo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayNopCo.Location = new System.Drawing.Point(366, 604);
            this.lblNgayNopCo.Name = "lblNgayNopCo";
            this.lblNgayNopCo.Size = new System.Drawing.Size(69, 13);
            this.lblNgayNopCo.TabIndex = 23;
            this.lblNgayNopCo.Text = "Thời hạn nộp";
            // 
            // ccNgayNopCO
            // 
            // 
            // 
            // 
            this.ccNgayNopCO.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayNopCO.DropDownCalendar.Name = "";
            this.ccNgayNopCO.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayNopCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayNopCO.IsNullDate = true;
            this.ccNgayNopCO.Location = new System.Drawing.Point(441, 599);
            this.ccNgayNopCO.Name = "ccNgayNopCO";
            this.ccNgayNopCO.Nullable = true;
            this.ccNgayNopCO.NullButtonText = "Xóa";
            this.ccNgayNopCO.ShowNullButton = true;
            this.ccNgayNopCO.Size = new System.Drawing.Size(109, 21);
            this.ccNgayNopCO.TabIndex = 16;
            this.ccNgayNopCO.TodayButtonText = "Hôm nay";
            this.ccNgayNopCO.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayNopCO.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(109, 17);
            this.txtSoTiepNhan.MaxLength = 255;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(101, 21);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiNguoiNK
            // 
            this.txtDiaChiNguoiNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiNguoiNK.Location = new System.Drawing.Point(109, 47);
            this.txtDiaChiNguoiNK.MaxLength = 255;
            this.txtDiaChiNguoiNK.Multiline = true;
            this.txtDiaChiNguoiNK.Name = "txtDiaChiNguoiNK";
            this.txtDiaChiNguoiNK.Size = new System.Drawing.Size(243, 83);
            this.txtDiaChiNguoiNK.TabIndex = 13;
            this.txtDiaChiNguoiNK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiNguoiNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaChiNguoiNK.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiNguoiXK
            // 
            this.txtDiaChiNguoiXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiNguoiXK.Location = new System.Drawing.Point(107, 47);
            this.txtDiaChiNguoiXK.MaxLength = 255;
            this.txtDiaChiNguoiXK.Multiline = true;
            this.txtDiaChiNguoiXK.Name = "txtDiaChiNguoiXK";
            this.txtDiaChiNguoiXK.Size = new System.Drawing.Size(243, 83);
            this.txtDiaChiNguoiXK.TabIndex = 12;
            this.txtDiaChiNguoiXK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiNguoiXK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaChiNguoiXK.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(780, 650);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(697, 650);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 8;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvNgayCO
            // 
            this.rfvNgayCO.ControlToValidate = this.ccNgayCO;
            this.rfvNgayCO.ErrorMessage = "\"Ngày CO\" không được bỏ trống.";
            this.rfvNgayCO.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayCO.Icon")));
            this.rfvNgayCO.Tag = "rfvNgayCO";
            // 
            // rfvSoCO
            // 
            this.rfvSoCO.ControlToValidate = this.txtSoCO;
            this.rfvSoCO.ErrorMessage = "\"Số CO\" không được để trống.";
            this.rfvSoCO.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoCO.Icon")));
            this.rfvSoCO.Tag = "rfvSoCO";
            // 
            // rfvTenDiaChiXK
            // 
            this.rfvTenDiaChiXK.ControlToValidate = this.txtDiaChiNguoiXK;
            this.rfvTenDiaChiXK.ErrorMessage = "\"Tên địa chỉ người xuất khẩu trên CO\" không được để trống.";
            this.rfvTenDiaChiXK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDiaChiXK.Icon")));
            this.rfvTenDiaChiXK.Tag = "rfvTenDiaChiXK";
            // 
            // rfvTenDiaChiNK
            // 
            this.rfvTenDiaChiNK.ControlToValidate = this.txtDiaChiNguoiNK;
            this.rfvTenDiaChiNK.ErrorMessage = "\"Tên địa chỉ người nhập khẩu trên CO\" không được để trống.";
            this.rfvTenDiaChiNK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDiaChiNK.Icon")));
            this.rfvTenDiaChiNK.Tag = "rfvTenDiaChiNK";
            // 
            // rfvNguoiKyCO
            // 
            this.rfvNguoiKyCO.ControlToValidate = this.txtNguoiKy;
            this.rfvNguoiKyCO.ErrorMessage = "\"Người ký\" không được bỏ trống.";
            this.rfvNguoiKyCO.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNguoiKyCO.Icon")));
            this.rfvNguoiKyCO.Tag = "rfvNguoiKyCO";
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(616, 650);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 7;
            this.btnXoa.Text = "Xoá CO";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnChonGP
            // 
            this.btnChonGP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChonGP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonGP.Icon = ((System.Drawing.Icon)(resources.GetObject("btnChonGP.Icon")));
            this.btnChonGP.Location = new System.Drawing.Point(503, 650);
            this.btnChonGP.Name = "btnChonGP";
            this.btnChonGP.Size = new System.Drawing.Size(107, 23);
            this.btnChonGP.TabIndex = 6;
            this.btnChonGP.Text = "Chọn CO đã có";
            this.btnChonGP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnChonGP.VisualStyleManager = this.vsmMain;
            this.btnChonGP.Click += new System.EventHandler(this.btnChonCO_Click);
            // 
            // grpTiepNhan
            // 
            this.grpTiepNhan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.grpTiepNhan.Controls.Add(this.btnKetQuaXuLy);
            this.grpTiepNhan.Controls.Add(this.txtSoTiepNhan);
            this.grpTiepNhan.Controls.Add(this.ccNgayTiepNhan);
            this.grpTiepNhan.Controls.Add(this.lbTrangThai);
            this.grpTiepNhan.Controls.Add(this.label9);
            this.grpTiepNhan.Controls.Add(this.label11);
            this.grpTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTiepNhan.Location = new System.Drawing.Point(8, 8);
            this.grpTiepNhan.Name = "grpTiepNhan";
            this.grpTiepNhan.Size = new System.Drawing.Size(847, 53);
            this.grpTiepNhan.TabIndex = 0;
            this.grpTiepNhan.Text = "Thông tin khai báo";
            this.grpTiepNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grpTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // btnKetQuaXuLy
            // 
            this.btnKetQuaXuLy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKetQuaXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKetQuaXuLy.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKetQuaXuLy.Icon")));
            this.btnKetQuaXuLy.Location = new System.Drawing.Point(717, 17);
            this.btnKetQuaXuLy.Name = "btnKetQuaXuLy";
            this.btnKetQuaXuLy.Size = new System.Drawing.Size(109, 23);
            this.btnKetQuaXuLy.TabIndex = 2;
            this.btnKetQuaXuLy.Text = "Kết quả xử lý";
            this.btnKetQuaXuLy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKetQuaXuLy.Click += new System.EventHandler(this.btnKetQuaXuLy_Click);
            // 
            // ccNgayTiepNhan
            // 
            // 
            // 
            // 
            this.ccNgayTiepNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayTiepNhan.DropDownCalendar.Name = "";
            this.ccNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTiepNhan.IsNullDate = true;
            this.ccNgayTiepNhan.Location = new System.Drawing.Point(305, 17);
            this.ccNgayTiepNhan.Name = "ccNgayTiepNhan";
            this.ccNgayTiepNhan.Nullable = true;
            this.ccNgayTiepNhan.NullButtonText = "Xóa";
            this.ccNgayTiepNhan.ReadOnly = true;
            this.ccNgayTiepNhan.ShowNullButton = true;
            this.ccNgayTiepNhan.Size = new System.Drawing.Size(90, 21);
            this.ccNgayTiepNhan.TabIndex = 1;
            this.ccNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.ccNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // lbTrangThai
            // 
            this.lbTrangThai.AutoSize = true;
            this.lbTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTrangThai.Location = new System.Drawing.Point(405, 22);
            this.lbTrangThai.Name = "lbTrangThai";
            this.lbTrangThai.Size = new System.Drawing.Size(56, 13);
            this.lbTrangThai.TabIndex = 2;
            this.lbTrangThai.Text = "Trạng thái";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(23, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Số tiếp nhận";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(216, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Ngày tiếp nhận";
            // 
            // btnKhaiBao
            // 
            this.btnKhaiBao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKhaiBao.Icon")));
            this.btnKhaiBao.Location = new System.Drawing.Point(7, 650);
            this.btnKhaiBao.Name = "btnKhaiBao";
            this.btnKhaiBao.Size = new System.Drawing.Size(88, 23);
            this.btnKhaiBao.TabIndex = 2;
            this.btnKhaiBao.Text = "Khai  báo";
            this.btnKhaiBao.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKhaiBao.Click += new System.EventHandler(this.btnKhaiBao_Click);
            // 
            // btnLayPhanHoi
            // 
            this.btnLayPhanHoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLayPhanHoi.Icon")));
            this.btnLayPhanHoi.Location = new System.Drawing.Point(101, 650);
            this.btnLayPhanHoi.Name = "btnLayPhanHoi";
            this.btnLayPhanHoi.Size = new System.Drawing.Size(109, 23);
            this.btnLayPhanHoi.TabIndex = 3;
            this.btnLayPhanHoi.Text = "Lấy phản hồi";
            this.btnLayPhanHoi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnLayPhanHoi.Click += new System.EventHandler(this.btnLayPhanHoi_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtMaNguoiNhap);
            this.uiGroupBox1.Controls.Add(this.txtDiaChiNguoiNK);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Location = new System.Drawing.Point(8, 232);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(368, 138);
            this.uiGroupBox1.TabIndex = 32;
            this.uiGroupBox1.Text = "Thông tin người nhập khẩu trên CO";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtMaNguoiNhap
            // 
            this.txtMaNguoiNhap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiNhap.Location = new System.Drawing.Point(109, 20);
            this.txtMaNguoiNhap.MaxLength = 255;
            this.txtMaNguoiNhap.Name = "txtMaNguoiNhap";
            this.txtMaNguoiNhap.Size = new System.Drawing.Size(243, 21);
            this.txtMaNguoiNhap.TabIndex = 13;
            this.txtMaNguoiNhap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiNhap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tên, địa chỉ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(10, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Mã";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.txtMaNguoiXuat);
            this.uiGroupBox3.Controls.Add(this.txtDiaChiNguoiXK);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Location = new System.Drawing.Point(382, 232);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(476, 138);
            this.uiGroupBox3.TabIndex = 32;
            this.uiGroupBox3.Text = "Thông tin người xuất khẩu trên CO";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // txtMaNguoiXuat
            // 
            this.txtMaNguoiXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiXuat.Location = new System.Drawing.Point(107, 17);
            this.txtMaNguoiXuat.MaxLength = 255;
            this.txtMaNguoiXuat.Name = "txtMaNguoiXuat";
            this.txtMaNguoiXuat.Size = new System.Drawing.Size(243, 21);
            this.txtMaNguoiXuat.TabIndex = 13;
            this.txtMaNguoiXuat.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(21, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Mã";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(21, 52);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Tên, địa chỉ";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtMoTaHangHoa);
            this.uiGroupBox4.Location = new System.Drawing.Point(8, 376);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(850, 131);
            this.uiGroupBox4.TabIndex = 33;
            this.uiGroupBox4.Text = "Thông tin mô tả hàng hóa";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtMoTaHangHoa
            // 
            this.txtMoTaHangHoa.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMoTaHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoTaHangHoa.Location = new System.Drawing.Point(3, 17);
            this.txtMoTaHangHoa.MaxLength = 255;
            this.txtMoTaHangHoa.Multiline = true;
            this.txtMoTaHangHoa.Name = "txtMoTaHangHoa";
            this.txtMoTaHangHoa.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMoTaHangHoa.Size = new System.Drawing.Size(823, 111);
            this.txtMoTaHangHoa.TabIndex = 13;
            this.txtMoTaHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMoTaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.txtThongTinMoTa);
            this.uiGroupBox5.Location = new System.Drawing.Point(9, 513);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(849, 80);
            this.uiGroupBox5.TabIndex = 34;
            this.uiGroupBox5.Text = "Thông tin khác";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtThongTinMoTa
            // 
            this.txtThongTinMoTa.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtThongTinMoTa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThongTinMoTa.Location = new System.Drawing.Point(3, 17);
            this.txtThongTinMoTa.MaxLength = 255;
            this.txtThongTinMoTa.Multiline = true;
            this.txtThongTinMoTa.Name = "txtThongTinMoTa";
            this.txtThongTinMoTa.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtThongTinMoTa.Size = new System.Drawing.Size(822, 60);
            this.txtThongTinMoTa.TabIndex = 14;
            this.txtThongTinMoTa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThongTinMoTa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtThongTinMoTa.VisualStyleManager = this.vsmMain;
            // 
            // rvHL
            // 
            this.rvHL.ControlToValidate = this.txtHamLuong;
            this.rvHL.ErrorMessage = "\"Hàm lượng\" không hợp lệ.";
            this.rvHL.Icon = ((System.Drawing.Icon)(resources.GetObject("rvHL.Icon")));
            this.rvHL.MaximumValue = "100";
            this.rvHL.MinimumValue = "0";
            this.rvHL.Tag = "rvHL";
            this.rvHL.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // cmdThuTucHQTruocDo
            // 
            this.cmdThuTucHQTruocDo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdThuTucHQTruocDo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdThuTucHQTruocDo.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThuTucHQTruocDo.Icon")));
            this.cmdThuTucHQTruocDo.Location = new System.Drawing.Point(357, 650);
            this.cmdThuTucHQTruocDo.Name = "cmdThuTucHQTruocDo";
            this.cmdThuTucHQTruocDo.Size = new System.Drawing.Size(140, 23);
            this.cmdThuTucHQTruocDo.TabIndex = 6;
            this.cmdThuTucHQTruocDo.Text = "Thủ tục HQ trước đó";
            this.cmdThuTucHQTruocDo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmdThuTucHQTruocDo.Click += new System.EventHandler(this.cmdThuTucHQTruocDo_Click);
            // 
            // CoForm_V4
            // 
            this.AcceptButton = this.btnGhi;
            this.ClientSize = new System.Drawing.Size(864, 679);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "CoForm_V4";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin CO";
            this.Load += new System.EventHandler(this.CoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayCO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoCO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaChiXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaChiNK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiKyCO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).EndInit();
            this.grpTiepNhan.ResumeLayout(false);
            this.grpTiepNhan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rvHL)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCO;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiXK;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtToChucCap;
        private System.Windows.Forms.Label label1;
        private Company.Interface.Controls.NuocHControl ctrMaNuocNK;
        private System.Windows.Forms.Label label3;
        private Company.Interface.Controls.NuocHControl ctrMaNuocXK;
        private System.Windows.Forms.Label label2;
        private Company.Interface.Controls.NuocHControl ctrNuocCapCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiNK;
        private Janus.Windows.EditControls.UIComboBox cbLoaiCO;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayCO;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoCO;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDiaChiXK;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDiaChiNK;
        private System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.Label lblNgayNopCo;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayNopCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiKy;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UICheckBox chkNoCo;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNguoiKyCO;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnChonGP;
        private Janus.Windows.EditControls.UIGroupBox grpTiepNhan;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label lbTrangThai;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.EditControls.UIButton btnKhaiBao;
        private Janus.Windows.EditControls.UIButton btnLayPhanHoi;
        private Janus.Windows.EditControls.UIButton btnKetQuaXuLy;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayHetHan;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayKhoiHanh;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiNhap;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiXuat;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtThongTinMoTa;
        private Janus.Windows.GridEX.EditControls.EditBox txtMoTaHangHoa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtHamLuong;
        private System.Windows.Forms.Label label18;
        private Company.Interface.Controls.CuaKhauHControl ctrCangXepHang;
        private System.Windows.Forms.Label label19;
        private Company.Interface.Controls.CuaKhauHControl ctrCangDoHang;
        private Company.Controls.CustomValidation.RangeValidator rvHL;
        private Janus.Windows.EditControls.UIButton cmdThuTucHQTruocDo;
    }
}
