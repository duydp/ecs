using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class PhanBoReport : DevExpress.XtraReports.UI.XtraReport
    {
        private int STT = 0;
        public DataTable dt = new DataTable();
        private int SoToKhaiNhap = 0;
        public bool First = false;
        public bool Last = false;
        public PhanBoReport()
        {
            InitializeComponent();
        }
        public void BindReport(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan)
        {
            ReportHeader.Visible = this.First;
            ReportFooter.Visible = this.Last;
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = this.dt;

            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblTieuDe.Text = "BẢNG PHÂN BỔ NGUYÊN PHỤ LIỆU HÀNG XUẤT KHẨU \r\nTỜ KHAI XUẤT KHẨU SỐ "+ soToKhai +"/" + namDangKy;

            lblTenNPL.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TenNPL");
            lblDVT.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DVT");
            lblDinhMuc1.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc0","{0:n"+ GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc2.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc1", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc3.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc2", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc4.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".DinhMuc3", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblLuongSD1.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD0", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD2.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD1", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD3.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD2", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD4.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".LuongSD3", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblSanPham1.Text = dt.Columns["DinhMuc0"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD0"].Caption;
            if (dt.Columns["DinhMuc1"].Caption != " ")
                lblSanPham2.Text = dt.Columns["DinhMuc1"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD1"].Caption;
            else
                lblSanPham2.Text = "";
            if (dt.Columns["DinhMuc2"].Caption != " ") 
                lblSanPham3.Text = dt.Columns["DinhMuc2"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD2"].Caption;
            else
                lblSanPham3.Text = "";
            if (dt.Columns["DinhMuc3"].Caption != " ") 
                lblSanPham4.Text = dt.Columns["DinhMuc3"].Caption + "\r\nSố lượng: " + dt.Columns["LuongSD3"].Caption;
            else lblSanPham4.Text = "";
            lblTongNhuCau.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".TongNhuCau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblPhanBo.DataBindings.Add("Text", this.DataSource, this.dt.TableName + ".PhanBo");
            xrLabel2.Text = GlobalSettings.TieudeNgay;
        }
      


        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblMaNPL.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString(); ;
        }

        private void PhanBoReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT =0;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }



       
        //private void lblTenDVT_NPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    lblTenDVT_NPL.Text = DonViTinh.GetName(GetCurrentColumnValue("DVT_NPL"));
        //}

    }
}
