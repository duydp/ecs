﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.SXXK
{
    public partial class XemDinhMucNPLForm : Company.Interface.BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public string MaNPL = "";
        public XemDinhMucNPLForm()
        {
            InitializeComponent();
        }

        private void XemDinhMucNPLForm_Load(object sender, EventArgs e)
        {
            this.Text = "Xem định mức NPL '" + this.MaNPL + "'";
            this.dgList.Tables[0].Columns["LuongNPL"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            this.dgList.Tables[0].Columns["LuongSP"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
            this.dgList.Tables[0].Columns["DinhMuc"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            this.dgList.Tables[0].Columns["LuongNPL"].TotalFormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

            this.dgList.DataSource = this.HSTL.GetDanhSachNPLXuatTonByMaNPL(GlobalSettings.SoThapPhan.LuongNPL, this.MaNPL);
        }
    }
}

