INSERT t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES ('A528','DMTL',N'Trường hợp người khai khai tờ khai liên quan đến Danh mục thiết bị đồng bộ đã đăng ký','')
INSERT t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES ('A528','XDMS',N'Trường hợp người khai khai tờ khai liên quan đến mặt hàng đã xác định trước mã số','')
INSERT t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES ('A528','XDTG',N'Trường hợp người khai khai tờ khai liên quan đến mặt hàng đã xác định trước trị giá','')
INSERT t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES ('A528','XDXX',N' Trường hợp người khai khai tờ khai liên quan đến mặt hàng đã xác định trước xuất xứ','')

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '27.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('27.1',GETDATE(), N'CẬP NHẬT GIẤY PHÉP')
END