using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public class DieuKienGiaoHang : BaseClass
    {
        public static DataSet SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_DieuKienGiaoHang ORDER BY ID";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }
        //Lypt update date 19/01/2010
        public static string SelectName(string id)
        {
            //string query = "SELECT MoTa FROM t_HaiQuan_DieuKienGiaoHang WHERE ID='" + id + "'";
            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //return db.ExecuteScalar(dbCommand).ToString();

            try
            {
                string query = string.Format("[ID] = '{0}'", id);

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["DieuKienGiaoHang"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["MoTa"].ToString();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(id, ex); }

            return string.Empty;
        }
        public static void Update(DataSet ds)
        {
            string insert = "Insert INTO t_HaiQuan_DieuKienGiaoHang VALUES(@Id,@Mota,@DateCreated,@DateModified)";
            string update = "UPDATE t_HaiQuan_DieuKienGiaoHang SET Mota = @Mota, DateModified = @DateModified WHERE Id=@Id";
            string delete = "DELETE FROM t_HaiQuan_DieuKienGiaoHang WHERE ID = @Id";

            DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@Mota", SqlDbType.NVarChar, "Mota", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@Mota", SqlDbType.NVarChar, "Mota", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
    }
}