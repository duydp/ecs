﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Transactions;
using System.Globalization;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.BLL.Utils;
using Company.KDT.SHARE.Components.Utils;

namespace Company.BLL.KDT.SXXK
{
    public partial class SanPhamDangKySUA
    {
        private string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            this.GUIDSTR = (System.Guid.NewGuid().ToString());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            nodeFrom.ChildNodes[0].InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DoiTac.GetName(this.MaDoanhNghiep);
            this.Update();
            return doc.InnerXml;
        }
        private string ConfigPhongBiPhanHoi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;

            return doc.InnerXml;
        }
        public string WSRequestXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucSanPham, MessgaseFunction.HoiTrangThai));

            XmlDocument docSP = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docSP.Load(path + "\\TemplateXML\\LayPhanHoiSanPham.xml");


            XmlNode nodeHQNhan = docSP.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);

            XmlNode nodeDN = docSP.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            XmlNode nodeDuLieu = docSP.SelectSingleNode("Root/SXXK/DU_LIEU");
            nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docSP.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";

        }

        public string WSDownLoad(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiPhanHoi(MessgaseType.DanhMucSanPham, MessgaseFunction.HoiTrangThai));
            
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);

            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);


            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }
        #region Khai báo sửa sản phẩm
        
        public string WSSendXMLSuaSP(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucSanPham, MessgaseFunction.KhaiBao));
            XmlDocument docSP = new XmlDocument();
            docSP.LoadXml(ConvertCollectionToXML());


            //luu vao string
            XmlNode root = doc.ImportNode(docSP.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                //Gởi sửa ĐM cho YONE
                //XmlDocument docAVN = new XmlDocument();
                //string path = EntityBase.GetPathProram();
                //docAVN.Load(path + "\\TemplateXML\\SUA_SP.xml");
                //kq = kdt.Send(docAVN.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";
        }
        
        private string ConvertCollectionToXML()
        {
            //load du lieu
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\KhaiBaoSanPhamSUA.xml");
            XmlNode nplNode = docNPL.GetElementsByTagName("SP")[0];

            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);


            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DoiTac.GetName(this.MaDoanhNghiep);

            List<SanPhamSUA> listSP = new List<SanPhamSUA>();
            listSP = (List<SanPhamSUA>)SanPhamSUA.SelectCollectionBy_Master_IDSUA(this.ID);
            foreach (SanPhamSUA sp in listSP)
            {
                XmlNode node = docNPL.CreateElement("SP.ITEM");
                XmlAttribute sttAtt = docNPL.CreateAttribute("STT");
                sttAtt.Value = sp.STTHang.ToString();
                node.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docNPL.CreateAttribute("MA_SP");
                maAtt.Value = sp.Ma;
                node.Attributes.Append(maAtt);

                XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
                MaHSAtt.Value = sp.MaHS;
                node.Attributes.Append(MaHSAtt);

                XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_SP");
                TenAtt.Value = sp.Ten;
                node.Attributes.Append(TenAtt);

                XmlAttribute DVTAtt = docNPL.CreateAttribute("MA_DVT");
                DVTAtt.Value = sp.DVT_ID;
                node.Attributes.Append(DVTAtt);

                nplNode.AppendChild(node);
            }
            return docNPL.InnerXml;
        }

        public string LayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            doc.GetElementsByTagName("function")[0].InnerText = "5";

            //HungTQ, Update 21/04/2010.
            doc.GetElementsByTagName("declarationType")[0].InnerText = "1";

            string kq = "";
            int i = 0;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
                docNPL = new XmlDocument();
                docNPL.LoadXml(kq);
                XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (nodeRoot.Attributes["Err"].Value == "no")
                {
                    if (nodeRoot.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else if(FontConverter.TCVN2Unicode(nodeRoot.InnerText).Equals("Chứng từ không xử lý")){
                    return doc.InnerXml;
                }
                else
                {
                    throw new Exception(FontConverter.TCVN2Unicode(nodeRoot.InnerText) + "|" + "Web Service");
                }
            }

            if (i > 1)
                return doc.InnerXml;

            /*XU LY THONG TIN MESSAGE PHAN HOI TU HAI QUAN*/
            XmlNode nodeRoot1 = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
            if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
            {
                XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == "SUA_SP")//THONG_TIN_DANG_KY.SP)
                {
                    #region Lấy số tiếp nhận của danh sách SP

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                        this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                        this.NgayTiepNhan = DateTime.Today;
                        this.NamDK = Convert.ToInt16(nodeDuLieu.Attributes["NAM_TN"].Value);
                        this.TrangThaiXuLy = 0;
                        this.Update();
                    }
                    KetQuaXuLy kqxl = new KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = "SP";
                    kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                    kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                    #endregion Lấy số tiếp nhận của danh sách SP
                }
            }
            else if (nodeRoot1 != null && nodeRoot1.Attributes.GetNamedItem("TuChoi") != null
                && nodeRoot1.Attributes["TuChoi"].Value == "yes")// Từ chối
            {
                //if () {
                    this.HUONGDAN = FontConverter.TCVN2Unicode(nodeRoot1.InnerText);
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                    this.Update();

                    KetQuaXuLy kqxl = new KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = "SP";
                    kqxl.LoaiThongDiep = "Sản phẩm bị từ chối";//KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                    kqxl.NoiDung = FontConverter.TCVN2Unicode(nodeRoot1.InnerText);
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                //}
            }
            else if (nodeRoot1 != null && nodeRoot1.SelectSingleNode("PHAN_LUONG") != null)
            {

                XmlNode nodePhanLuong = docNPL.SelectSingleNode("Envelope/Body/Content/Root").SelectSingleNode("PHAN_LUONG");
                this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                this.HUONGDAN = nodePhanLuong.Attributes["HUONGDAN"].Value;
                this.Update();
                string tenluong = "Xanh";
                if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                    tenluong = "Vàng";
                else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                    tenluong = "Đỏ";
                KetQuaXuLy kqxl = new KetQuaXuLy();
                kqxl.ItemID = this.ID;
                kqxl.ReferenceID = new Guid(this.GUIDSTR);
                kqxl.LoaiChungTu = "SP";//KetQuaXuLy.LoaiChungTu_ToKhai;
                kqxl.LoaiThongDiep = "Sản phẩm được phân luồng";//KetQuaXuLy.LoaiThongDiep_ToKhaiDuocPhanLuong;
                kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nHải quan: {2}\r\nPhân luồng: {3}\r\nHướng dẫn: {4}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), this.MaHaiQuan.Trim(), tenluong, this.HUONGDAN);
                kqxl.Ngay = DateTime.Now;
                kqxl.Insert();
            }
            else if (nodeRoot1 != null && nodeRoot1.Attributes["TrangThai"].Value == "yes"
                && nodeRoot1.Attributes["SOTN"].Value!="")
            {
                KetQuaXuLy kqxl = new KetQuaXuLy();
                kqxl.ItemID = this.ID;
                kqxl.ReferenceID = new Guid(this.GUIDSTR);
                kqxl.LoaiChungTu = "SP";//KetQuaXuLy.LoaiChungTu_ToKhai;
                kqxl.LoaiThongDiep = "Sản phẩm sửa được duyệt";//KetQuaXuLy.LoaiThongDiep_ToKhaiDuocPhanLuong;
                kqxl.NoiDung = string.Format("Sản phẩm sửa được duyệt: Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nHải quan: {2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), this.MaHaiQuan.Trim());
                kqxl.Ngay = DateTime.Now;
                kqxl.Insert();

                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                this.Update();
                //cập nhật lại sản phẩm gốc
                List<SanPhamSUA> spSUAList = (List<SanPhamSUA>)SanPhamSUA.SelectCollectionBy_Master_IDSUA(this.ID);
                SanPham spGoc = new SanPham();
                foreach (SanPhamSUA spSUA in spSUAList)
                {
                    //Cập nhật vào bảng t_KDT_SXXK_SanPham
                    spGoc = SanPham.Load(spSUA.IDSP);
                    spGoc.Ten = spSUA.Ten;
                    spGoc.MaHS = spSUA.MaHS;
                    spGoc.DVT_ID = spSUA.DVT_ID;
                    spGoc.Update();
                    // Cập nhật vào bảng t_SXXK_SanPham
                    BLL.SXXK.SanPham spSXXK = BLL.SXXK.SanPham.getSanPham(this.MaHaiQuan, this.MaDoanhNghiep, spGoc.Ma);
                    if (spSXXK != null)
                    {
                        spSXXK.Ten = spSUA.Ten;
                        spSXXK.MaHS = spSUA.MaHS;
                        spSXXK.DVT_ID = spSUA.DVT_ID;
                        spSXXK.Update();
                    }
                }

            }
            else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
            {
                #region Lỗi

                XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
                string errorSt = "";
                if (stMucLoi == "XML_LEVEL")
                    errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                else if (stMucLoi == "DATA_LEVEL")
                    errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                else if (stMucLoi == "SERVICE_LEVEL")
                    errorSt = "Lỗi do Web service trả về ";
                else if (stMucLoi == "DOTNET_LEVEL")
                    errorSt = "Lỗi do hệ thống của hải quan ";
                throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);

                #endregion
            }
            return "";

        }
        
        #endregion Khai báo sửa sản phẩm
    }
}
