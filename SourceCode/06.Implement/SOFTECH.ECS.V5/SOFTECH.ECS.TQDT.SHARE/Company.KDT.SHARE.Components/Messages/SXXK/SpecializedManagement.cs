﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class SpecializedManagement_Old   : GoodsMeasure
    {
        /// <summary>
        /// Kiểu đơn vị tính 
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }
        /// <summary>
        /// Mã HTS
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }
        /// <summary>
        ///  Đơn giá HTS
        /// </summary>
        [XmlElement("unitPrice")]
        public string UnitPrice { get; set; }

    }
}
