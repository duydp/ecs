﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class Product
    {
        /// <summary>
        /// Thời gian sản xuất (thời gian)
        /// </summary>
        [XmlElement("time")]
        public string Time { get; set; }

        /// <summary>
        /// Thời gian sản xuất (ĐVT)
        /// </summary>
        [XmlElement("measureUnitTime")]
        public string MeasureUnitTime { get; set; }

        /// <summary>
        /// Mã SP
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }

        /// <summary>
        /// Tên SP
        /// </summary>
        [XmlElement("tariffClassification")]
        public string TariffClassification { get; set; }



        /// <summary>
        /// Chu kỳ sản xuất (thời gian)
        /// </summary>
        [XmlElement("period")]
        public string Period { get; set; }

        /// <summary>
        /// Chu kỳ sản xuất (ĐVT)
        /// </summary>
        [XmlElement("measureUnit")]
        public string MeasureUnit { get; set; }

        /// <summary>
        /// Số lượng sản phẩm
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }
    }
}
