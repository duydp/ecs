using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.KDT.SXXK;
using GemBox.Spreadsheet;
using System.Reflection;
using System.Runtime.InteropServices;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.Interface.CauHinh;
using Company.KDT.SHARE.Components;
namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuReadExcelForm : BaseForm
    {
        public NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
        public NguyenPhuLieuDangKy nplDangKy = new NguyenPhuLieuDangKy();
        public bool isEdit = false;
        public bool isCancel = false;
        public NguyenPhuLieuReadExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog(this);
                txtFilePath.Text = openFileDialog1.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    MLMessages("LỖI KHI CHỌN FILE. DOANH NGHIỆP HÃY KIỂM TRA LẠI ĐƯỜNG DẪN HOẶC ĐÓNG FILE TRƯỚC KHI ĐỌC.", "MSG_EXC03", "", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return null;
                }
                List<String> Collection = new List<string>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                MLMessages("LỖI KHI ĐỌC SHEET. DOANH NGHIỆP HÃY KIỂM TRA LẠI TÊN SHEET TRƯỚC KHI CHỌN FILE.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private int checkNPLExit(string maNPL)
        {
            for (int i = 0; i < this.NPLCollection.Count; i++)
            {
                if (this.NPLCollection[i].Ma.ToUpper() == maNPL.ToUpper()) return i;
            }
            return -1;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {

            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtRow, "DÒNG BẮT ĐẦU PHẢI LỚN HƠN 0");
                }
                else
                {
                    error.SetError(txtRow, "STARTED ROW MUST BE GREATER THAN 0 ");
                }

                error.SetIconPadding(txtRow, 8);
                return;

            }
            int hsLen = 8;
            try
            {
                hsLen = int.Parse(txtLengthHS.Text);
            }
            catch { }
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);
            }
            catch
            {
                MLMessages("LỖI KHI ĐỌC FILE. BẠN HÃY KIỂM TRA LẠI ĐƯỜNG DẪN HOẶC ĐÓNG FILE TRƯỚC KHI ĐỌC.", "MSG_EXC03", "", false);
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("KHÔNG TỒN TẠI SHEET \"" + cbbSheetName.Text + "\"", "MSG_EXC01", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
            int maHangCol = ConvertCharToInt(maHangColumn);
            char tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
            int tenHangCol = ConvertCharToInt(tenHangColumn);
            char maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
            int maHSCol = ConvertCharToInt(maHSColumn);
            char dvtColumn = Convert.ToChar(txtDVTColumn.Text);
            int dvtCol = ConvertCharToInt(dvtColumn);
            string errorTotal = "";
            string errorMaHangHoa = "";
            string errorMaHangHoaSpecialChar = "";
            string errorMaHangHoaExits = "";
            string errorMaHangHoaNotExits = "";
            string errorMaHangHoaValid = "";
            string errorTenHangHoa = "";
            string errorMaHS = "";
            string errorMaHSExits = "";
            string errorDVT = "";
            string errorDVTExits = "";

            string errorNPL = "";

            List<VNACC_Category_Nation> XuatXuCollection = VNACC_Category_Nation.SelectCollectionAll();
            List<VNACC_Category_HSCode> HSCollection = VNACC_Category_HSCode.SelectCollectionAll();
            List<HaiQuan_DonViTinh> DVTCollection = HaiQuan_DonViTinh.SelectCollectionAll();
            List<NguyenPhuLieu> Collection = new List<NguyenPhuLieu>();
            Company.BLL.SXXK.NguyenPhuLieuCollection NPLAllCollection = Company.BLL.SXXK.NguyenPhuLieu.SelectCollectionDynamicBy(" MaHaiQuan ='" + GlobalSettings.MA_HAI_QUAN + "' AND MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "'", "");
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        NguyenPhuLieu npl = new NguyenPhuLieu();
                        bool isAdd = true;
                        bool isExits = false;
                        try
                        {
                            npl.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                            if (npl.Ma.Trim().Length == 0)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                isAdd = false;
                            }
                            else if (Helpers.ValidateSpecialChar(npl.Ma))
                            {
                                errorMaHangHoaSpecialChar += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (isEdit || isCancel)
                                {
                                    // KIỂM TRA MÃ NPL ĐÃ ĐĂNG KÝ
                                    foreach (Company.BLL.SXXK.NguyenPhuLieu item in NPLAllCollection)
                                    {
                                        if (item.Ma == npl.Ma)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        // KIỂM TRA MÃ NPL ĐÃ ĐƯỢC NHẬP TRƯỚC ĐÓ BỎ QUA ĐÃ HỦY , CHỜ DUYỆT , ĐÃ DUYỆT
                                        NguyenPhuLieuDangKyCollection NPLDKCollection = NguyenPhuLieuDangKy.SelectCollectionDynamicAll("MaHaiQuan ='" + GlobalSettings.MA_HAI_QUAN + "' AND MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "' AND Ma ='" + npl.Ma + "' AND t_KDT_SXXK_NguyenPhuLieuDangKy.ID NOT IN (" + nplDangKy.ID + ") AND TrangThaiXuLy NOT IN (10,0,1)", "");
                                        if (NPLDKCollection.Count >= 1)
                                        {
                                            errorNPL += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]-[" + NPLDKCollection[0].SoTiepNhan + "]-[" + NPLDKCollection[0].NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(NPLDKCollection[0].TrangThaiXuLy).ToUpper() + "]\n";
                                            isExits = true;
                                            isAdd = false;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        // KIỂM TRA MÃ NPL ĐÃ NHẬP TRÊN LƯỚI
                                        foreach (NguyenPhuLieu item in nplDangKy.NPLCollection)
                                        {
                                            if (npl.Ma == item.Ma)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        if (isExits)
                                        {
                                            errorMaHangHoaValid += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                            isAdd = false;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorMaHangHoaNotExits += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                        isAdd = false;
                                    }
                                }
                                else
                                {
                                    // KIỂM TRA MÃ NPL ĐÃ ĐĂNG KÝ
                                    foreach (Company.BLL.SXXK.NguyenPhuLieu item in NPLAllCollection)
                                    {
                                        if (item.Ma == npl.Ma)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        // KIỂM TRA MÃ NPL ĐÃ ĐƯỢC NHẬP TRƯỚC ĐÓ BỎ QUA ĐÃ HỦY 
                                        NguyenPhuLieuDangKyCollection NPLDKCollection = NguyenPhuLieuDangKy.SelectCollectionDynamicAll("MaHaiQuan ='" + GlobalSettings.MA_HAI_QUAN + "' AND MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "' AND Ma ='" + npl.Ma + "' AND t_KDT_SXXK_NguyenPhuLieuDangKy.ID NOT IN (" + nplDangKy.ID + ") AND TrangThaiXuLy NOT IN (10)", "");
                                        if (NPLDKCollection.Count >= 1)
                                        {
                                            errorNPL += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]-[" + NPLDKCollection[0].SoTiepNhan + "]-[" + NPLDKCollection[0].NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(NPLDKCollection[0].TrangThaiXuLy).ToUpper() + "]\n";
                                            isExits = true;
                                            isAdd = false;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        // KIỂM TRA MÃ NPL ĐÃ NHẬP TRÊN LƯỚI
                                        foreach (NguyenPhuLieu item in nplDangKy.NPLCollection)
                                        {
                                            if (npl.Ma == item.Ma)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        if (isExits)
                                        {
                                            errorMaHangHoaValid += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                            isAdd = false;
                                        }
                                    }
                                    if (isExits)
                                    {
                                        errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            npl.Ten = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                            if (npl.Ten.Trim().Length == 0)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + npl.Ten + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            npl.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                            if (npl.MaHS.Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + npl.MaHS + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (VNACC_Category_HSCode item in HSCollection)
                                {
                                    if (item.HSCode == npl.MaHS)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorMaHSExits += "[" + (wsr.Index + 1) + "]-[" + npl.MaHS + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + npl.MaHS + "]\n";
                            isAdd = false;
                        }
                        string DonViTinh = String.Empty;
                        try
                        {
                            isExits = false;
                            DonViTinh = Convert.ToString(wsr.Cells[dvtCol].Value.ToString().ToUpper());
                            if (DonViTinh.Trim().Length == 0)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (HaiQuan_DonViTinh item in DVTCollection)
                                {
                                    if (item.Ten == DonViTinh)
                                    {
                                        npl.DVT_ID = DonViTinh_GetID(DonViTinh);
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                            isAdd = false;
                        }
                        if (isAdd)
                            Collection.Add(npl);
                    }
                    catch (Exception ex)
                    {
                        this.SaveDefault();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        return;
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorMaHangHoa))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorMaHangHoaSpecialChar))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaSpecialChar + " CHỨA CÁC KÝ TỰ ĐẶC BIỆT HOẶC UNICODE HOẶC XUỐNG DÒNG ";
            if (!String.IsNullOrEmpty(errorMaHangHoaExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaExits + " ĐÃ ĐƯỢC ĐĂNG KÝ ";
            if (!String.IsNullOrEmpty(errorMaHangHoaNotExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaNotExits + "CHƯA ĐƯỢC ĐĂNG KÝ .CHỈ NHỮNG NPL ĐÃ ĐĂNG KÝ MỚI ĐƯỢC PHÉP KHAI BÁO SỬA HOẶC HỦY ";
            if (!String.IsNullOrEmpty(errorMaHangHoaValid))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaValid + " ĐÃ ĐƯỢC NHẬP LIỆU TRÊN LƯỚI ";
            if (!String.IsNullOrEmpty(errorNPL))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + errorNPL + " ĐÃ ĐƯỢC NHẬP LIỆU TRƯỚC ĐÓ .";
            if (!String.IsNullOrEmpty(errorTenHangHoa))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVT + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVTExits + " KHÔNG HỢP LỆ ";
            if (!String.IsNullOrEmpty(errorMaHS))
                errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHS + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorMaHSExits))
                errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHSExits + " KHÔNG HỢP LỆ";
            if (String.IsNullOrEmpty(errorTotal))
            {
                foreach (NguyenPhuLieu item in Collection)
                {
                    this.NPLCollection.Add(item);
                }
                ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", "NHẬP HÀNG TỪ EXCEL THÀNH CÔNG ", false);
            }
            else
            {
                ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", "NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG ." + errorTotal, false);
                return;
            }
            this.SaveDefault();
            this.Close();
        }

        private void NguyenPhuLieuReadExcelForm_Load(object sender, EventArgs e)
        {
            try
            {
                FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
                f.ReadDefaultNPL(cbbSheetName, txtRow, txtMaHangColumn, txtTenHangColumn, txtMaHSColumn, txtDVTColumn, null, null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SaveDefault()
        {
            try
            {
                FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
                f.SaveConfigNPL(txtRow.Text, txtMaHangColumn.Text, txtTenHangColumn.Text, txtMaHSColumn.Text, txtDVTColumn.Text, String.Empty, String.Empty, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void linkExcelMau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_SXXK("NPL");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }


    }
}