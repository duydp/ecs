-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_Hang_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_Hang_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_Hang_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_Hang_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_Hang_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_Hang_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Insert]
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@Ma2 nvarchar(30),
	@TenHang nvarchar(200),
	@Mau nvarchar(30),
	@Size nvarchar(30),
	@DVTLuong1 varchar(4),
	@SoLuong1 numeric(12, 4),
	@SoLuongThucNhap numeric(12, 4),
	@DonGiaNgoaiTe numeric(24, 6),
	@DonGiaHoaDon numeric(24, 6),
	@ThanhTienNgoaiTe numeric(24, 4),
	@TriGiaHoaDon numeric(24, 4),
	@GhiChu nvarchar(200),
	@MaTTDonGia varchar(4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_PhieuNhapKho_Hang]
(
	[TKMD_ID],
	[MaSoHang],
	[Ma2],
	[TenHang],
	[Mau],
	[Size],
	[DVTLuong1],
	[SoLuong1],
	[SoLuongThucNhap],
	[DonGiaNgoaiTe],
	[DonGiaHoaDon],
	[ThanhTienNgoaiTe],
	[TriGiaHoaDon],
	[GhiChu],
	[MaTTDonGia]
)
VALUES 
(
	@TKMD_ID,
	@MaSoHang,
	@Ma2,
	@TenHang,
	@Mau,
	@Size,
	@DVTLuong1,
	@SoLuong1,
	@SoLuongThucNhap,
	@DonGiaNgoaiTe,
	@DonGiaHoaDon,
	@ThanhTienNgoaiTe,
	@TriGiaHoaDon,
	@GhiChu,
	@MaTTDonGia
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@Ma2 nvarchar(30),
	@TenHang nvarchar(200),
	@Mau nvarchar(30),
	@Size nvarchar(30),
	@DVTLuong1 varchar(4),
	@SoLuong1 numeric(12, 4),
	@SoLuongThucNhap numeric(12, 4),
	@DonGiaNgoaiTe numeric(24, 6),
	@DonGiaHoaDon numeric(24, 6),
	@ThanhTienNgoaiTe numeric(24, 4),
	@TriGiaHoaDon numeric(24, 4),
	@GhiChu nvarchar(200),
	@MaTTDonGia varchar(4)
AS

UPDATE
	[dbo].[t_KDT_GC_PhieuNhapKho_Hang]
SET
	[TKMD_ID] = @TKMD_ID,
	[MaSoHang] = @MaSoHang,
	[Ma2] = @Ma2,
	[TenHang] = @TenHang,
	[Mau] = @Mau,
	[Size] = @Size,
	[DVTLuong1] = @DVTLuong1,
	[SoLuong1] = @SoLuong1,
	[SoLuongThucNhap] = @SoLuongThucNhap,
	[DonGiaNgoaiTe] = @DonGiaNgoaiTe,
	[DonGiaHoaDon] = @DonGiaHoaDon,
	[ThanhTienNgoaiTe] = @ThanhTienNgoaiTe,
	[TriGiaHoaDon] = @TriGiaHoaDon,
	[GhiChu] = @GhiChu,
	[MaTTDonGia] = @MaTTDonGia
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@Ma2 nvarchar(30),
	@TenHang nvarchar(200),
	@Mau nvarchar(30),
	@Size nvarchar(30),
	@DVTLuong1 varchar(4),
	@SoLuong1 numeric(12, 4),
	@SoLuongThucNhap numeric(12, 4),
	@DonGiaNgoaiTe numeric(24, 6),
	@DonGiaHoaDon numeric(24, 6),
	@ThanhTienNgoaiTe numeric(24, 4),
	@TriGiaHoaDon numeric(24, 4),
	@GhiChu nvarchar(200),
	@MaTTDonGia varchar(4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_PhieuNhapKho_Hang] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_PhieuNhapKho_Hang] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[MaSoHang] = @MaSoHang,
			[Ma2] = @Ma2,
			[TenHang] = @TenHang,
			[Mau] = @Mau,
			[Size] = @Size,
			[DVTLuong1] = @DVTLuong1,
			[SoLuong1] = @SoLuong1,
			[SoLuongThucNhap] = @SoLuongThucNhap,
			[DonGiaNgoaiTe] = @DonGiaNgoaiTe,
			[DonGiaHoaDon] = @DonGiaHoaDon,
			[ThanhTienNgoaiTe] = @ThanhTienNgoaiTe,
			[TriGiaHoaDon] = @TriGiaHoaDon,
			[GhiChu] = @GhiChu,
			[MaTTDonGia] = @MaTTDonGia
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_PhieuNhapKho_Hang]
		(
			[TKMD_ID],
			[MaSoHang],
			[Ma2],
			[TenHang],
			[Mau],
			[Size],
			[DVTLuong1],
			[SoLuong1],
			[SoLuongThucNhap],
			[DonGiaNgoaiTe],
			[DonGiaHoaDon],
			[ThanhTienNgoaiTe],
			[TriGiaHoaDon],
			[GhiChu],
			[MaTTDonGia]
		)
		VALUES 
		(
			@TKMD_ID,
			@MaSoHang,
			@Ma2,
			@TenHang,
			@Mau,
			@Size,
			@DVTLuong1,
			@SoLuong1,
			@SoLuongThucNhap,
			@DonGiaNgoaiTe,
			@DonGiaHoaDon,
			@ThanhTienNgoaiTe,
			@TriGiaHoaDon,
			@GhiChu,
			@MaTTDonGia
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_PhieuNhapKho_Hang]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_PhieuNhapKho_Hang] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[Ma2],
	[TenHang],
	[Mau],
	[Size],
	[DVTLuong1],
	[SoLuong1],
	[SoLuongThucNhap],
	[DonGiaNgoaiTe],
	[DonGiaHoaDon],
	[ThanhTienNgoaiTe],
	[TriGiaHoaDon],
	[GhiChu],
	[MaTTDonGia]
FROM
	[dbo].[t_KDT_GC_PhieuNhapKho_Hang]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[Ma2],
	[TenHang],
	[Mau],
	[Size],
	[DVTLuong1],
	[SoLuong1],
	[SoLuongThucNhap],
	[DonGiaNgoaiTe],
	[DonGiaHoaDon],
	[ThanhTienNgoaiTe],
	[TriGiaHoaDon],
	[GhiChu],
	[MaTTDonGia]
FROM [dbo].[t_KDT_GC_PhieuNhapKho_Hang] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[Ma2],
	[TenHang],
	[Mau],
	[Size],
	[DVTLuong1],
	[SoLuong1],
	[SoLuongThucNhap],
	[DonGiaNgoaiTe],
	[DonGiaHoaDon],
	[ThanhTienNgoaiTe],
	[TriGiaHoaDon],
	[GhiChu],
	[MaTTDonGia]
FROM
	[dbo].[t_KDT_GC_PhieuNhapKho_Hang]	

GO

