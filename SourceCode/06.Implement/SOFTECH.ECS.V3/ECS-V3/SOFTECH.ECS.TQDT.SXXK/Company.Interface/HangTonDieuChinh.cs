﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.Interface
{
    public partial class HangTonDieuChinh : BaseForm
    {
        public Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection NplTonSelect = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection();

        public HangTonDieuChinh()
        {
            InitializeComponent();
        }

        private void HangTonDieuChinh_Load(object sender, EventArgs e)
        {
            try
            {
                dgList.Tables[0].Columns["Luong"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
                dgList.Tables[0].Columns["Ton"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
                dgList.Tables[0].Columns["Luong"].TotalFormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
                dgList.Tables[0].Columns["Ton"].TotalFormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;

                dgList.DataSource = NplTonSelect;

                dgList.Refetch();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnDieuChinh_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection tkSelectSaiSoLuongTon_DangThanhKhoan = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection();
                Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection tkSelectSaiSoLuongTon_ChuaThanhKhoan = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection();
                bool tiepTucXuLy = false;

                foreach (GridEXRow row in dgList.GetCheckedRows())
                {
                    if (row.RowType == RowType.Record)
                    {
                        if (row.CheckState == RowCheckState.Checked)
                        {
                            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl = (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon)row.DataRow;

                            //To khai dua vao thanh khoan lan dau tien nhung luong nhap <> luong ton
                            if (npl.SoLanThanhLy != 0 && npl.LanThanhLy != 0 && npl.TienTrinhChayThanhLy != "")
                                tkSelectSaiSoLuongTon_DangThanhKhoan.Add(npl);
                            //To khai khong tham gia vao thanh khoan nhung luong nhap <> luong ton
                            else if (npl.SoLanThanhLy == 0 && npl.SaiSoLuong == 0 && npl.TienTrinhChayThanhLy == "" && npl.Luong != npl.Ton)
                                tkSelectSaiSoLuongTon_ChuaThanhKhoan.Add(npl);
                            //To khai khong tham gia vao thanh khoan nhung luong nhap <> luong nhap ban dau.
                            else if (npl.SoLanThanhLy == 0 && npl.SaiSoLuong == 1 && npl.TienTrinhChayThanhLy == "" && npl.Luong != npl.SoLuongDangKy)
                                tkSelectSaiSoLuongTon_ChuaThanhKhoan.Add(npl);
                        }
                    }
                }

                if (tkSelectSaiSoLuongTon_DangThanhKhoan.Count == 0 && tkSelectSaiSoLuongTon_ChuaThanhKhoan.Count == 0)
                {
                    Globals.ShowMessage("Không có tờ khai nào được chọn.", false);
                    return;
                }
                else
                {
                    /*HUNGTQ, 01/08/2011. Cap nhat Luong nhap cua NPL bi lech giua t_KDT_SXXK_NPLNhapTon so voi t_SXXK_NPLNhapTonThucTe
                         * Ly do: gia tri bi lech do lam tron so thap phan. Du lieu sai o bang t_KDT_SXXK_NPLNhapTon. Lay gia tri dung trong
                         * bang t_SXXK_NPLNhapTonThucTe cap nhat lai "Luong nhap" cho t_KDT_SXXK_NPLNhapTon.
                         */

                    //To khai dua vao thanh khoan lan dau tien nhung luong nhap <> luong ton
                    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                    {
                        connection.Open();
                        SqlTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            foreach (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon item in tkSelectSaiSoLuongTon_DangThanhKhoan)
                            {
                                if (tiepTucXuLy == false)
                                {
                                    if (item.TrangThaiThanhKhoan == 401)//401: Dong ho so
                                    {
                                        if (Globals.ShowMessage("Trong danh sách có tờ khai '" + string.Format("{0}/{1}/{2}", item.SoToKhai, item.MaLoaiHinh, item.NamDangKy) + "' đã đóng hồ sơ. Bạn có muốn tiếp tục cập nhật không?.", true) != "Yes")
                                            return;
                                        else
                                            tiepTucXuLy = true;
                                    }
                                }

                                //Neu to khai chua dong ho so, lay thong tin luong ton cua to khai lan thanh ly gan nhat truoc do.
                                /*t_KDT_SXXK_NPLNhapTon*/
                                Company.BLL.KDT.SXXK.NPLNhapTon kdtNPLNhapTon = new Company.BLL.KDT.SXXK.NPLNhapTon();
                                kdtNPLNhapTon.SoToKhai = item.SoToKhai;
                                kdtNPLNhapTon.MaLoaiHinh = item.MaLoaiHinh;
                                kdtNPLNhapTon.NamDangKy = item.NamDangKy;
                                kdtNPLNhapTon.MaNPL = item.MaNPL;
                                kdtNPLNhapTon.MaHaiQuan = item.MaHaiQuan;

                                //Kiem tra neu co Lan thanh ly > Lan thanh ly hien tai => lay thong tin ton cua lan thanh ly hien tai.
                                //Neu kiem tra khong co lan thanh ly nao > Lan thanh ly hien tai => lay thong tin ton cua lan thanh ly truoc thanh ly hien tai (nghia la thanh ly hien tai - 1).
                                kdtNPLNhapTon.LanThanhLy = item.LanThanhLy + 1;
                                kdtNPLNhapTon.Load();
                                if (kdtNPLNhapTon != null)
                                {
                                    kdtNPLNhapTon.LanThanhLy = item.LanThanhLy;
                                    kdtNPLNhapTon.Load();
                                }
                                else
                                {
                                    if (kdtNPLNhapTon == null)
                                    {
                                        kdtNPLNhapTon.LanThanhLy = item.LanThanhLy;
                                        kdtNPLNhapTon.Load();
                                    }
                                }

                                if (kdtNPLNhapTon.LanThanhLy <= 0) break;

                                if (kdtNPLNhapTon != null)
                                {
                                    if (item.SaiSoLuong == 1 && item.SoLuongDangKy != item.Luong)
                                    {
                                        item.Luong = item.SoLuongDangKy;
                                    }

                                    item.Ton = kdtNPLNhapTon.TonCuoi;
                                    item.ThueXNKTon = kdtNPLNhapTon.TonCuoiThueXNK;
                                    
                                    item.UpdateTransaction(transaction);
                                }
                            }

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();

                            throw new Exception(ex.Message);
                        }
                        finally
                        {
                            connection.Close();
                        }

                    }

                    //To khai khong tham gia vao thanh khoan nhung luong nhap <> luong ton
                    using (SqlConnection connection2 = (SqlConnection)db.CreateConnection())
                    {
                        connection2.Open();
                        SqlTransaction transaction2 = connection2.BeginTransaction();
                        try
                        {
                            foreach (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon item in tkSelectSaiSoLuongTon_ChuaThanhKhoan)
                            {
                                if (tiepTucXuLy == false)
                                {
                                    if (item.TrangThaiThanhKhoan == 401)
                                    {
                                        if (Globals.ShowMessage("Trong danh sách có tờ khai '" + string.Format("{0}/{1}/{2}", item.SoToKhai, item.MaLoaiHinh, item.NamDangKy) + "' đã đóng hồ sơ. Bạn có muốn tiếp tục cập nhật không?.", true) != "Yes")
                                            return;
                                        else
                                            tiepTucXuLy = true;
                                    }
                                }

                                //Neu to khai chua dong ho so, lay thong tin luong ton cua to khai lan thanh ly gan nhat truoc do.
                                /*t_KDT_SXXK_NPLNhapTon*/
                                if (item.LanThanhLy > 0)
                                {
                                    Company.BLL.KDT.SXXK.NPLNhapTon kdtNPLNhapTon = new Company.BLL.KDT.SXXK.NPLNhapTon();
                                    kdtNPLNhapTon.SoToKhai = item.SoToKhai;
                                    kdtNPLNhapTon.MaLoaiHinh = item.MaLoaiHinh;
                                    kdtNPLNhapTon.NamDangKy = item.NamDangKy;
                                    kdtNPLNhapTon.MaNPL = item.MaNPL;
                                    kdtNPLNhapTon.MaHaiQuan = item.MaHaiQuan;

                                    //Kiem tra neu co Lan thanh ly > Lan thanh ly hien tai => lay thong tin ton cua lan thanh ly hien tai.
                                    //Neu kiem tra khong co lan thanh ly nao > Lan thanh ly hien tai => lay thong tin ton cua lan thanh ly truoc thanh ly hien tai (nghia la thanh ly hien tai - 1).
                                    kdtNPLNhapTon.LanThanhLy = item.LanThanhLy + 1;
                                    kdtNPLNhapTon.Load();
                                    if (kdtNPLNhapTon != null)
                                    {
                                        kdtNPLNhapTon.LanThanhLy = item.LanThanhLy;
                                        kdtNPLNhapTon.Load();
                                    }
                                    else
                                    {
                                        if (kdtNPLNhapTon == null)
                                        {
                                            kdtNPLNhapTon.LanThanhLy = item.LanThanhLy;
                                            kdtNPLNhapTon.Load();
                                        }
                                    }

                                    if (kdtNPLNhapTon.LanThanhLy <= 0) break;

                                    if (kdtNPLNhapTon != null)
                                    {
                                        item.Ton = kdtNPLNhapTon.TonCuoi;
                                        item.ThueXNKTon = kdtNPLNhapTon.TonCuoiThueXNK;

                                        item.UpdateTransaction(transaction2);
                                    }
                                }
                                //Neu chua tham gia lan thanh ly nao, luong ton = luong nhap.
                                else
                                {
                                    if (item.SoLanThanhLy == 0 && item.SaiSoLuong == 1 && item.TienTrinhChayThanhLy == "" && (item.Luong != item.Ton || item.SoLuongDangKy != item.Luong))
                                    {
                                        item.Luong = item.SoLuongDangKy;
                                        item.Ton = item.Luong;
                                        item.ThueXNKTon = item.ThueXNK;
                                    }
                                    else
                                    {
                                        item.Ton = item.Luong;
                                        item.ThueXNKTon = item.ThueXNK;
                                    }

                                    item.UpdateTransaction(transaction2);
                                }
                            }

                            transaction2.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction2.Rollback();

                            throw new Exception(ex.Message);
                        }
                        finally
                        {
                            connection2.Close();
                        }

                    }

                    Globals.ShowMessage("Quá trình thực hiện thành công. Bạn hãy chạy lại thanh khoản để cập nhật thông tin.", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Điều chỉnh lượng tồn", ex);

                Globals.ShowMessage("Quá trình thực hiện điều chỉnh có lỗi: " + ex.Message, false);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TenNPL"].Text == "")
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                }

                else if (e.Row.Cells["SaiSoLuong"].Value.ToString() != "0")
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                }

                else if (e.Row.Cells["SaiSoLuong"].Value.ToString() == "0"
                    && e.Row.Cells["SoLanThanhLy"].Value.ToString() == "0"
                    && (Convert.ToDecimal(e.Row.Cells["Luong"].Value) != Convert.ToDecimal(e.Row.Cells["Ton"].Value)))
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                }

                else if (e.Row.Cells["TienTrinhChayThanhLy"].Value.ToString().Contains("Sai"))
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                    e.Row.Cells["TienTrinhChayThanhLy"].ToolTipText = e.Row.Cells["TienTrinhChayThanhLy"].Text;
                }
            }
        }
    }
}
