﻿namespace Company.Interface.Report.VNACCS
{
    partial class ToKhaiBoSungHangHoaXuatKhauCQHQ_Page_3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTrang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoTrang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotokhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotokhaidautien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSonhanhtokhaichianho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongsotokhaichianho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotokhaitamnhaptaixuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaphanloaikiemtra = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaloaihinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaphanloaihanghoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMahieuphuongthucvanchuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMasothuedaidien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTencoquanhaiquantiepnhantokhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNhomxulyhoso = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaydangky = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiodangky = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaythaydoidangky = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiothaydoidangky = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoihantainhapxuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBieuthitruonghophethan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSodong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMasohanghoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaquanlyrieng = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMataixacnhangia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMotahanghoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoluong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadvtinh1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoluong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadvtinh2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrigia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDongia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadongtien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonvidongia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrigiathueS = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadongtienS = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadongtienM = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrigiathueM = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoluongthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadvtinhchuanthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDongiathue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMatientedongiathue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonvisoluongthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThuesuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloainhapthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotienthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMatientesotienthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotienmiengiam = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMatientesotiengiamthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSodonghangtaixuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDanhmucmienthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSodongmienthue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienlephi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienbaohiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoluongtienlephi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadvtienlephi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoluongtienbaohiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMadvtienbaohiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblKhoantienlephi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblKhoantienbaohiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMavanvanphapquy1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMavanvanphapquy2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMavanvanphapquy3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMavanvanphapquy4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMavanvanphapquy5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMamiengiam = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieukhoanmien = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 26F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrTable5,
            this.lblTenThongTinXuat});
            this.ReportHeader.HeightF = 110F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail1.HeightF = 413F;
            this.Detail1.Name = "Detail1";
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(125F, 0F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(576.21F, 21.25F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "Bản xác nhận nội dung khai bổ sung xuất khẩu (cơ quan Hải quan thực hiện)";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Arial", 9F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(750F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable5.SizeF = new System.Drawing.SizeF(55.40309F, 21.25001F);
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UsePadding = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTrang,
            this.xrTableCell201,
            this.lblTongSoTrang});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 0.6;
            // 
            // lblTrang
            // 
            this.lblTrang.Name = "lblTrang";
            this.lblTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrang.StylePriority.UsePadding = false;
            this.lblTrang.StylePriority.UseTextAlignment = false;
            this.lblTrang.Text = "3";
            this.lblTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTrang.Weight = 1.2811263099159538;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Text = "/";
            this.xrTableCell201.Weight = 0.75;
            // 
            // lblTongSoTrang
            // 
            this.lblTongSoTrang.Name = "lblTongSoTrang";
            this.lblTongSoTrang.Text = "3";
            this.lblTongSoTrang.Weight = 1.5;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 37.5F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow20,
            this.xrTableRow23,
            this.xrTableRow22,
            this.xrTableRow5,
            this.xrTableRow70});
            this.xrTable1.SizeF = new System.Drawing.SizeF(795.0001F, 66F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.lblSotokhai,
            this.xrTableCell41,
            this.xrTableCell47,
            this.xrTableCell176,
            this.lblSotokhaidautien,
            this.xrTableCell177,
            this.lblSonhanhtokhaichianho,
            this.xrTableCell178,
            this.lblTongsotokhaichianho});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.44000000000000017;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 0.22150588621117057;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "Số tờ khai";
            this.xrTableCell38.Weight = 0.69519988887066164;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Weight = 0.050000010094922054;
            // 
            // lblSotokhai
            // 
            this.lblSotokhai.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSotokhai.Name = "lblSotokhai";
            this.lblSotokhai.StylePriority.UseFont = false;
            this.lblSotokhai.Text = "NNNNNNNNN1NE";
            this.lblSotokhai.Weight = 1.2357998850163763;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 0.13791076673899538;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "Số tờ khai đầu tiên";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell47.Weight = 1.1215999280343758;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Weight = 0.11289999999180062;
            // 
            // lblSotokhaidautien
            // 
            this.lblSotokhaidautien.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSotokhaidautien.Name = "lblSotokhaidautien";
            this.lblSotokhaidautien.StylePriority.UseFont = false;
            this.lblSotokhaidautien.Text = "XXXXXXXXX1XE";
            this.lblSotokhaidautien.Weight = 1.2273997652086184;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Text = "-";
            this.xrTableCell177.Weight = 0.13370001591400016;
            // 
            // lblSonhanhtokhaichianho
            // 
            this.lblSonhanhtokhaichianho.Name = "lblSonhanhtokhaichianho";
            this.lblSonhanhtokhaichianho.Text = "NE";
            this.lblSonhanhtokhaichianho.Weight = 0.25870000156096395;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Text = "/";
            this.xrTableCell178.Weight = 0.12330006867640209;
            // 
            // lblTongsotokhaichianho
            // 
            this.lblTongsotokhaichianho.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongsotokhaichianho.Name = "lblTongsotokhaichianho";
            this.lblTongsotokhaichianho.StylePriority.UseFont = false;
            this.lblTongsotokhaichianho.Text = "NE";
            this.lblTongsotokhaichianho.Weight = 2.6419840903833505;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170,
            this.lblSotokhaitamnhaptaixuat});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.44000000000000017;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Weight = 0.22150588621117057;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            this.xrTableCell169.Weight = 1.9809941137888296;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Weight = 0.13791625976562494;
            // 
            // lblSotokhaitamnhaptaixuat
            // 
            this.lblSotokhaitamnhaptaixuat.Name = "lblSotokhaitamnhaptaixuat";
            this.lblSotokhaitamnhaptaixuat.Text = "NNNNNNNNN1NE";
            this.lblSotokhaitamnhaptaixuat.Weight = 5.6195840469360121;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell189,
            this.lblMaphanloaikiemtra,
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell193,
            this.lblMaloaihinh,
            this.lblMaphanloaihanghoa,
            this.lblMahieuphuongthucvanchuyen,
            this.xrTableCell195,
            this.xrTableCell200,
            this.xrTableCell199,
            this.lblMasothuedaidien});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 0.44000000000000017;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Weight = 0.22150588621117057;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Text = "Mã phân loại kiểm tra";
            this.xrTableCell188.Weight = 1.1659432654588686;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Weight = 0.11285126000864798;
            // 
            // lblMaphanloaikiemtra
            // 
            this.lblMaphanloaikiemtra.Multiline = true;
            this.lblMaphanloaikiemtra.Name = "lblMaphanloaikiemtra";
            this.lblMaphanloaikiemtra.Text = "XX E";
            this.lblMaphanloaikiemtra.Weight = 0.60219962424556739;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Weight = 0.10000563116833086;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Text = "Mã loại hình";
            this.xrTableCell192.Weight = 0.926099862744497;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Weight = 0.10239998208045287;
            // 
            // lblMaloaihinh
            // 
            this.lblMaloaihinh.Name = "lblMaloaihinh";
            this.lblMaloaihinh.Text = "XXE";
            this.lblMaloaihinh.Weight = 0.3437999682619991;
            // 
            // lblMaphanloaihanghoa
            // 
            this.lblMaphanloaihanghoa.Name = "lblMaphanloaihanghoa";
            this.lblMaphanloaihanghoa.Text = "X";
            this.lblMaphanloaihanghoa.Weight = 0.20489999031460793;
            // 
            // lblMahieuphuongthucvanchuyen
            // 
            this.lblMahieuphuongthucvanchuyen.Name = "lblMahieuphuongthucvanchuyen";
            this.lblMahieuphuongthucvanchuyen.Text = "X";
            this.lblMahieuphuongthucvanchuyen.Weight = 0.23789830042940907;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Weight = 0.29108181177870329;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Text = "Mã số hàng hóa đại diện của tờ khai";
            this.xrTableCell200.Weight = 1.8678403274363316;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Weight = 0.10829271003411883;
            // 
            // lblMasothuedaidien
            // 
            this.lblMasothuedaidien.Name = "lblMasothuedaidien";
            this.lblMasothuedaidien.Text = "XXXE";
            this.lblMasothuedaidien.Weight = 1.6751816865289324;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell179,
            this.xrTableCell180,
            this.lblTencoquanhaiquantiepnhantokhai,
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell185,
            this.lblNhomxulyhoso});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.44000000000000017;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Weight = 0.22150588621117057;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            this.xrTableCell180.Weight = 2.1189106613319848;
            // 
            // lblTencoquanhaiquantiepnhantokhai
            // 
            this.lblTencoquanhaiquantiepnhantokhai.Name = "lblTencoquanhaiquantiepnhantokhai";
            this.lblTencoquanhaiquantiepnhantokhai.Text = "XXXXXXXXXE";
            this.lblTencoquanhaiquantiepnhantokhai.Weight = 1.6771873334220411;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Weight = 0.29108152551845734;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Text = "Mã bộ phận xử lý tờ khai";
            this.xrTableCell184.Weight = 1.6905344940545168;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Weight = 0.28559812693840819;
            // 
            // lblNhomxulyhoso
            // 
            this.lblNhomxulyhoso.Name = "lblNhomxulyhoso";
            this.lblNhomxulyhoso.Text = "XE";
            this.lblNhomxulyhoso.Weight = 1.6751822792250581;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.lblNgaydangky,
            this.xrTableCell15,
            this.lblGiodangky,
            this.xrTableCell19,
            this.xrTableCell54,
            this.xrTableCell55,
            this.lblNgaythaydoidangky,
            this.lblGiothaydoidangky});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.Weight = 0.43999999999999984;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.221505962505113;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "Ngày đăng ký";
            this.xrTableCell50.Weight = 1.4243273888274213;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.1316668607398559;
            // 
            // lblNgaydangky
            // 
            this.lblNgaydangky.Name = "lblNgaydangky";
            this.lblNgaydangky.Text = "dd/MM/yyyy";
            this.lblNgaydangky.Weight = 0.7104164944365341;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 0.14999983592969796;
            // 
            // lblGiodangky
            // 
            this.lblGiodangky.Name = "lblGiodangky";
            this.lblGiodangky.Text = "hh:mm:ss";
            this.lblGiodangky.Weight = 1.3796874825087764;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.29108149102965164;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "Ngày thay đổi đăng ký";
            this.xrTableCell54.Weight = 1.3252201662307113;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Weight = 0.65091239333803363;
            // 
            // lblNgaythaydoidangky
            // 
            this.lblNgaythaydoidangky.Name = "lblNgaythaydoidangky";
            this.lblNgaythaydoidangky.Text = "dd/MM/yyyy";
            this.lblNgaythaydoidangky.Weight = 0.7948821329231;
            // 
            // lblGiothaydoidangky
            // 
            this.lblGiothaydoidangky.Name = "lblGiothaydoidangky";
            this.lblGiothaydoidangky.Text = "hh:mm:ss";
            this.lblGiothaydoidangky.Weight = 0.88030009823274147;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell5,
            this.xrTableCell7,
            this.lblThoihantainhapxuat,
            this.xrTableCell16,
            this.lblBieuthitruonghophethan,
            this.xrTableCell48});
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.StylePriority.UseBorders = false;
            this.xrTableRow70.Weight = 0.43999999999999984;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Weight = 0.221505962505113;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrTableCell5.Weight = 1.4243271599456118;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 0.13166701332772868;
            // 
            // lblThoihantainhapxuat
            // 
            this.lblThoihantainhapxuat.Name = "lblThoihantainhapxuat";
            this.lblThoihantainhapxuat.Text = "dd/MM/yyyy";
            this.lblThoihantainhapxuat.Weight = 0.7104164944365341;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "-";
            this.xrTableCell16.Weight = 0.14999998851757096;
            // 
            // lblBieuthitruonghophethan
            // 
            this.lblBieuthitruonghophethan.Name = "lblBieuthitruonghophethan";
            this.lblBieuthitruonghophethan.Text = "X";
            this.lblBieuthitruonghophethan.Weight = 1.6707692209361045;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Weight = 3.6513144670329742;
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26,
            this.xrTableRow21,
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow12,
            this.xrTableRow11,
            this.xrTableRow10,
            this.xrTableRow9,
            this.xrTableRow8,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow17,
            this.xrTableRow16,
            this.xrTableRow15,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow24,
            this.xrTableRow25});
            this.xrTable2.SizeF = new System.Drawing.SizeF(795.0001F, 257F);
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell131});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.StylePriority.UseBorders = false;
            this.xrTableRow26.Weight = 0.32000000000000006;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Weight = 2.9999999999999996;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell163,
            this.xrTableCell171,
            this.lblSodong1,
            this.xrTableCell167,
            this.xrTableCell172,
            this.xrTableCell173});
            this.xrTableRow21.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.StylePriority.UseBorders = false;
            this.xrTableRow21.StylePriority.UseFont = false;
            this.xrTableRow21.StylePriority.UseTextAlignment = false;
            this.xrTableRow21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow21.Weight = 0.44000000000000006;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Weight = 0.0905661296470944;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Text = "<";
            this.xrTableCell171.Weight = 0.037735843678817119;
            // 
            // lblSodong1
            // 
            this.lblSodong1.Name = "lblSodong1";
            this.lblSodong1.StylePriority.UseTextAlignment = false;
            this.lblSodong1.Text = "XE";
            this.lblSodong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSodong1.Weight = 0.11061316209250768;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.StylePriority.UseTextAlignment = false;
            this.xrTableCell167.Text = ">";
            this.xrTableCell167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell167.Weight = 0.10657692510355038;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.StylePriority.UseTextAlignment = false;
            this.xrTableCell172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell172.Weight = 0.89644991754360892;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Multiline = true;
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.StylePriority.UseTextAlignment = false;
            this.xrTableCell173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell173.Weight = 1.7580580219344213;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell4,
            this.lblMasohanghoa,
            this.xrTableCell22,
            this.xrTableCell21,
            this.xrTableCell24,
            this.lblMaquanlyrieng,
            this.xrTableCell25,
            this.xrTableCell27,
            this.xrTableCell26,
            this.xrTableCell29,
            this.lblMataixacnhangia,
            this.xrTableCell8});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.43999999999999995;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Weight = 0.12830196571191049;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.Text = "Mã số hàng hóa";
            this.xrTableCell2.Weight = 0.32625775677444141;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 0.032263927823846468;
            // 
            // lblMasohanghoa
            // 
            this.lblMasohanghoa.Name = "lblMasohanghoa";
            this.lblMasohanghoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMasohanghoa.StylePriority.UsePadding = false;
            this.lblMasohanghoa.Text = "XXXXXXXXX1XE";
            this.lblMasohanghoa.Weight = 0.45083324586617435;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Weight = 0.0565325141142628;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UsePadding = false;
            this.xrTableCell21.Text = "Mã quản lý riêng";
            this.xrTableCell21.Weight = 0.35314092919502638;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 0.056603762571685909;
            // 
            // lblMaquanlyrieng
            // 
            this.lblMaquanlyrieng.Name = "lblMaquanlyrieng";
            this.lblMaquanlyrieng.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaquanlyrieng.StylePriority.UsePadding = false;
            this.lblMaquanlyrieng.Text = "XXXXXXE";
            this.lblMaquanlyrieng.Weight = 0.31609372505387995;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Weight = 0.04643278564061093;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.Text = "Mã phân loại tái xác nhận giá";
            this.xrTableCell27.Weight = 0.56137557380743275;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Weight = 0.055394119816647769;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell29.StylePriority.UsePadding = false;
            this.xrTableCell29.Text = "[";
            this.xrTableCell29.Weight = 0.037735837283738874;
            // 
            // lblMataixacnhangia
            // 
            this.lblMataixacnhangia.Name = "lblMataixacnhangia";
            this.lblMataixacnhangia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMataixacnhangia.StylePriority.UsePadding = false;
            this.lblMataixacnhangia.Text = "X";
            this.lblMataixacnhangia.Weight = 0.056603758914917368;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell8.StylePriority.UsePadding = false;
            this.xrTableCell8.Text = "]";
            this.xrTableCell8.Weight = 0.52243009742542434;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell11,
            this.xrTableCell13,
            this.lblMotahanghoa});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1.5999999999999999;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 0.12830196571191049;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell11.StylePriority.UsePadding = false;
            this.xrTableCell11.Text = "Mô tả hàng hóa";
            this.xrTableCell11.Weight = 0.32625772798427782;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Weight = 0.032263911500380438;
            // 
            // lblMotahanghoa
            // 
            this.lblMotahanghoa.Name = "lblMotahanghoa";
            this.lblMotahanghoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMotahanghoa.StylePriority.UsePadding = false;
            this.lblMotahanghoa.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6X" +
                "XXXXXXXX7XXXXXXXXX8XXXXXXXXX9XXXXXXXXXE";
            this.lblMotahanghoa.Weight = 2.5131763948034309;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell34,
            this.xrTableCell33,
            this.xrTableCell35,
            this.xrTableCell69,
            this.lblSoluong1,
            this.xrTableCell67,
            this.lblMadvtinh1});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.43999999999999995;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Weight = 0.12830196571191049;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Weight = 0.32625772798427782;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Weight = 0.032263911500380438;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Weight = 0.62829409870085773;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Weight = 0.28881649660364406;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell35.StylePriority.UsePadding = false;
            this.xrTableCell35.Text = "Số lượng (1)";
            this.xrTableCell35.Weight = 0.37721323918472593;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Weight = 0.028770477186492116;
            // 
            // lblSoluong1
            // 
            this.lblSoluong1.Name = "lblSoluong1";
            this.lblSoluong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoluong1.StylePriority.UsePadding = false;
            this.lblSoluong1.StylePriority.UseTextAlignment = false;
            this.lblSoluong1.Text = "123,456,789,012";
            this.lblSoluong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoluong1.Weight = 0.61104830892062934;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Weight = 0.037735615959356067;
            // 
            // lblMadvtinh1
            // 
            this.lblMadvtinh1.Name = "lblMadvtinh1";
            this.lblMadvtinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMadvtinh1.StylePriority.UsePadding = false;
            this.lblMadvtinh1.Text = "XXXE";
            this.lblMadvtinh1.Weight = 0.54129815824772554;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.xrTableCell40,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell70,
            this.lblSoluong2,
            this.xrTableCell68,
            this.lblMadvtinh2});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.43999999999999995;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Weight = 0.12830196571191049;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Weight = 0.32625772798427782;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Weight = 0.032263911500380438;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Weight = 0.62829409870085773;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Weight = 0.28881649660364406;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell45.StylePriority.UsePadding = false;
            this.xrTableCell45.Text = "Số lượng (2)";
            this.xrTableCell45.Weight = 0.37721346950603496;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Weight = 0.028770362025837604;
            // 
            // lblSoluong2
            // 
            this.lblSoluong2.Name = "lblSoluong2";
            this.lblSoluong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoluong2.StylePriority.UsePadding = false;
            this.lblSoluong2.StylePriority.UseTextAlignment = false;
            this.lblSoluong2.Text = "123,456,789,012";
            this.lblSoluong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoluong2.Weight = 0.61104853924193836;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Weight = 0.037735155316738114;
            // 
            // lblMadvtinh2
            // 
            this.lblMadvtinh2.Name = "lblMadvtinh2";
            this.lblMadvtinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMadvtinh2.StylePriority.UsePadding = false;
            this.lblMadvtinh2.Text = "XXXE";
            this.lblMadvtinh2.Weight = 0.5412982734083801;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell52,
            this.xrTableCell53,
            this.xrTableCell56,
            this.lblTrigia,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell62,
            this.lblDongia,
            this.xrTableCell63,
            this.xrTableCell65,
            this.lblMadongtien,
            this.xrTableCell66,
            this.lblDonvidongia});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.43999999999999995;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Weight = 0.12830196571191049;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell53.StylePriority.UsePadding = false;
            this.xrTableCell53.Text = "Trị giá hóa đơn";
            this.xrTableCell53.Weight = 0.32625772798427782;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Weight = 0.032263911500380438;
            // 
            // lblTrigia
            // 
            this.lblTrigia.Name = "lblTrigia";
            this.lblTrigia.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrigia.StylePriority.UsePadding = false;
            this.lblTrigia.StylePriority.UseTextAlignment = false;
            this.lblTrigia.Text = "12,345,678,901,234,567,890";
            this.lblTrigia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTrigia.Weight = 0.62829424265167577;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Weight = 0.288816352652826;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell59.StylePriority.UsePadding = false;
            this.xrTableCell59.Text = "Đơn giá hóa đơn";
            this.xrTableCell59.Weight = 0.37721346950603496;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Weight = 0.028770355989221591;
            // 
            // lblDongia
            // 
            this.lblDongia.Name = "lblDongia";
            this.lblDongia.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblDongia.StylePriority.UsePadding = false;
            this.lblDongia.StylePriority.UseTextAlignment = false;
            this.lblDongia.Text = "123,456,789";
            this.lblDongia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblDongia.Weight = 0.36102624551677687;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.037735618977664115;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell65.StylePriority.UsePadding = false;
            this.xrTableCell65.Text = "-";
            this.xrTableCell65.Weight = 0.0830188566863394;
            // 
            // lblMadongtien
            // 
            this.lblMadongtien.Name = "lblMadongtien";
            this.lblMadongtien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMadongtien.StylePriority.UsePadding = false;
            this.lblMadongtien.Text = "XXE";
            this.lblMadongtien.Weight = 0.12926737552838824;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell66.StylePriority.UsePadding = false;
            this.xrTableCell66.Text = "-";
            this.xrTableCell66.Weight = 0.037735614450202057;
            // 
            // lblDonvidongia
            // 
            this.lblDonvidongia.Name = "lblDonvidongia";
            this.lblDonvidongia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblDonvidongia.StylePriority.UsePadding = false;
            this.lblDonvidongia.Text = "XXXE";
            this.lblDonvidongia.Weight = 0.54129826284430194;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125,
            this.xrTableCell126});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.43999999999999995;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Weight = 0.12830196571191049;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell126.StylePriority.UsePadding = false;
            this.xrTableCell126.Text = "Thuế xuất khẩu";
            this.xrTableCell126.Weight = 2.8716980342880896;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell114,
            this.lblTrigiathueS,
            this.xrTableCell127,
            this.lblMadongtienS,
            this.xrTableCell117,
            this.xrTableCell118,
            this.lblMadongtienM,
            this.xrTableCell120,
            this.lblTrigiathueM});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.43999999999999995;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Weight = 0.16603778018410387;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell113.StylePriority.UsePadding = false;
            this.xrTableCell113.Text = "Trị giá tính thuế(S)";
            this.xrTableCell113.Weight = 0.35687003545677631;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Weight = 0.042532110492156591;
            // 
            // lblTrigiathueS
            // 
            this.lblTrigiathueS.Name = "lblTrigiathueS";
            this.lblTrigiathueS.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrigiathueS.StylePriority.UsePadding = false;
            this.lblTrigiathueS.StylePriority.UseTextAlignment = false;
            this.lblTrigiathueS.Text = "12,345,678,901,234,567";
            this.lblTrigiathueS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTrigiathueS.Weight = 0.54967815203651682;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Weight = 0.037735495458543877;
            // 
            // lblMadongtienS
            // 
            this.lblMadongtienS.Name = "lblMadongtienS";
            this.lblMadongtienS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMadongtienS.StylePriority.UsePadding = false;
            this.lblMadongtienS.Text = "XXE";
            this.lblMadongtienS.Weight = 0.25108062687297317;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell117.StylePriority.UsePadding = false;
            this.xrTableCell117.Text = "Trị giá tính thuế(M)";
            this.xrTableCell117.Weight = 0.37721346950603496;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Weight = 0.028770355989221591;
            // 
            // lblMadongtienM
            // 
            this.lblMadongtienM.Name = "lblMadongtienM";
            this.lblMadongtienM.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMadongtienM.StylePriority.UsePadding = false;
            this.lblMadongtienM.Text = "XXE";
            this.lblMadongtienM.Weight = 0.14788943972631491;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell120.StylePriority.UsePadding = false;
            this.xrTableCell120.Text = "-";
            this.xrTableCell120.Weight = 0.05660376373261073;
            // 
            // lblTrigiathueM
            // 
            this.lblTrigiathueM.Name = "lblTrigiathueM";
            this.lblTrigiathueM.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrigiathueM.StylePriority.UsePadding = false;
            this.lblTrigiathueM.StylePriority.UseTextAlignment = false;
            this.lblTrigiathueM.Text = "12,345,678,901,234,567,890";
            this.lblTrigiathueM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTrigiathueM.Weight = 0.985588770544747;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99,
            this.xrTableCell100,
            this.lblSoluongthue,
            this.xrTableCell122,
            this.lblMadvtinhchuanthue,
            this.xrTableCell104,
            this.xrTableCell105,
            this.lblDongiathue,
            this.xrTableCell107,
            this.lblMatientedongiathue,
            this.xrTableCell109,
            this.lblDonvisoluongthue});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.43999999999999995;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Weight = 0.16603783776443112;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell100.StylePriority.UsePadding = false;
            this.xrTableCell100.Text = "Số lượng tính thuế";
            this.xrTableCell100.Weight = 0.39940208836860563;
            // 
            // lblSoluongthue
            // 
            this.lblSoluongthue.Name = "lblSoluongthue";
            this.lblSoluongthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoluongthue.StylePriority.UsePadding = false;
            this.lblSoluongthue.StylePriority.UseTextAlignment = false;
            this.lblSoluongthue.Text = "123,456,789,012";
            this.lblSoluongthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoluongthue.Weight = 0.54967803687586225;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Weight = 0.037735610619198362;
            // 
            // lblMadvtinhchuanthue
            // 
            this.lblMadvtinhchuanthue.Name = "lblMadvtinhchuanthue";
            this.lblMadvtinhchuanthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMadvtinhchuanthue.StylePriority.UsePadding = false;
            this.lblMadvtinhchuanthue.Text = "XXXE";
            this.lblMadvtinhchuanthue.Weight = 0.25108062687297311;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell104.StylePriority.UsePadding = false;
            this.xrTableCell104.Text = "Đơn giá tính thuế";
            this.xrTableCell104.Weight = 0.37721346950603496;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Weight = 0.028770355989221591;
            // 
            // lblDongiathue
            // 
            this.lblDongiathue.Name = "lblDongiathue";
            this.lblDongiathue.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblDongiathue.StylePriority.UsePadding = false;
            this.lblDongiathue.StylePriority.UseTextAlignment = false;
            this.lblDongiathue.Text = "123,456,789,012,345,678";
            this.lblDongiathue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblDongiathue.Weight = 0.61104854829686261;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Weight = 0.037735611780123204;
            // 
            // lblMatientedongiathue
            // 
            this.lblMatientedongiathue.Name = "lblMatientedongiathue";
            this.lblMatientedongiathue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMatientedongiathue.StylePriority.UsePadding = false;
            this.lblMatientedongiathue.Text = "XXE";
            this.lblMatientedongiathue.Weight = 0.13249877629575754;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Weight = 0.07702796911860757;
            // 
            // lblDonvisoluongthue
            // 
            this.lblDonvisoluongthue.Name = "lblDonvisoluongthue";
            this.lblDonvisoluongthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblDonvisoluongthue.StylePriority.UsePadding = false;
            this.lblDonvisoluongthue.Text = "XXXE";
            this.lblDonvisoluongthue.Weight = 0.33177106851232185;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell88,
            this.lblThuesuat,
            this.xrTableCell90,
            this.lblPhanloainhapthue});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.43999999999999995;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Weight = 0.16603776578902207;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell87.StylePriority.UsePadding = false;
            this.xrTableCell87.Text = "Thuế suất";
            this.xrTableCell87.Weight = 0.19829853581477239;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Weight = 0.052487547541427371;
            // 
            // lblThuesuat
            // 
            this.lblThuesuat.Name = "lblThuesuat";
            this.lblThuesuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblThuesuat.StylePriority.UsePadding = false;
            this.lblThuesuat.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXXE";
            this.lblThuesuat.Weight = 0.93046464383761551;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell90.StylePriority.UsePadding = false;
            this.xrTableCell90.Text = "-";
            this.xrTableCell90.Weight = 0.056645707518233113;
            // 
            // lblPhanloainhapthue
            // 
            this.lblPhanloainhapthue.Name = "lblPhanloainhapthue";
            this.lblPhanloainhapthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblPhanloainhapthue.StylePriority.UsePadding = false;
            this.lblPhanloainhapthue.Text = "X";
            this.lblPhanloainhapthue.Weight = 1.5960657994989291;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.lblSotienthue,
            this.xrTableCell77,
            this.lblMatientesotienthue});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.43999999999999995;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Weight = 0.16603780897426751;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell74.StylePriority.UsePadding = false;
            this.xrTableCell74.Text = "Số tiền thuế";
            this.xrTableCell74.Weight = 0.28852188472192081;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Weight = 0.032263911500380438;
            // 
            // lblSotienthue
            // 
            this.lblSotienthue.Name = "lblSotienthue";
            this.lblSotienthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSotienthue.StylePriority.UsePadding = false;
            this.lblSotienthue.StylePriority.UseTextAlignment = false;
            this.lblSotienthue.Text = "1,234,567,890,123,456";
            this.lblSotienthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSotienthue.Weight = 0.6282942066639714;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Weight = 0.037735730655544072;
            // 
            // lblMatientesotienthue
            // 
            this.lblMatientesotienthue.Name = "lblMatientesotienthue";
            this.lblMatientesotienthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMatientesotienthue.StylePriority.UsePadding = false;
            this.lblMatientesotienthue.Text = "XXE";
            this.lblMatientesotienthue.Weight = 1.8471464574839156;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTableCell81,
            this.lblSotienmiengiam,
            this.xrTableCell83,
            this.lblMatientesotiengiamthue});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.43999999999999995;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Weight = 0.16603775139394025;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell80.StylePriority.UsePadding = false;
            this.xrTableCell80.Text = "Số tiền miễn giảm";
            this.xrTableCell80.Weight = 0.35686994908628544;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Weight = 0.04253216807248382;
            // 
            // lblSotienmiengiam
            // 
            this.lblSotienmiengiam.Name = "lblSotienmiengiam";
            this.lblSotienmiengiam.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSotienmiengiam.StylePriority.UsePadding = false;
            this.lblSotienmiengiam.StylePriority.UseTextAlignment = false;
            this.lblSotienmiengiam.Text = "1,234,567,890,123,456";
            this.lblSotienmiengiam.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSotienmiengiam.Weight = 0.5496779433078306;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.037735730655544072;
            // 
            // lblMatientesotiengiamthue
            // 
            this.lblMatientesotiengiamthue.Name = "lblMatientesotiengiamthue";
            this.lblMatientesotiengiamthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMatientesotiengiamthue.StylePriority.UsePadding = false;
            this.lblMatientesotiengiamthue.Text = "XXE";
            this.lblMatientesotiengiamthue.Weight = 1.8471464574839156;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.xrTableCell92,
            this.xrTableCell93,
            this.lblSodonghangtaixuat});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 0.43999999999999995;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Weight = 0.12830196571191049;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell92.StylePriority.UsePadding = false;
            this.xrTableCell92.Text = "Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng";
            this.xrTableCell92.Weight = 1.2756323029454955;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Weight = 0.020577940901049085;
            // 
            // lblSodonghangtaixuat
            // 
            this.lblSodonghangtaixuat.Name = "lblSodonghangtaixuat";
            this.lblSodonghangtaixuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSodonghangtaixuat.StylePriority.UsePadding = false;
            this.lblSodonghangtaixuat.Text = "XE";
            this.lblSodonghangtaixuat.Weight = 1.5754877904415445;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell135,
            this.xrTableCell136,
            this.xrTableCell137,
            this.lblDanhmucmienthue,
            this.xrTableCell139,
            this.lblSodongmienthue});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.43999999999999995;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Weight = 0.12830196571191049;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell136.StylePriority.UsePadding = false;
            this.xrTableCell136.Text = "Danh mục miễn thuế xuất khẩu";
            this.xrTableCell136.Weight = 0.61777306235477358;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Weight = 0.046324350844419215;
            // 
            // lblDanhmucmienthue
            // 
            this.lblDanhmucmienthue.Name = "lblDanhmucmienthue";
            this.lblDanhmucmienthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblDanhmucmienthue.StylePriority.UsePadding = false;
            this.lblDanhmucmienthue.Text = "XXXXXXXXX1XE";
            this.lblDanhmucmienthue.Weight = 0.39819001151103728;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell139.StylePriority.UsePadding = false;
            this.xrTableCell139.Text = "-";
            this.xrTableCell139.Weight = 0.051352671924070906;
            // 
            // lblSodongmienthue
            // 
            this.lblSodongmienthue.Name = "lblSodongmienthue";
            this.lblSodongmienthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSodongmienthue.StylePriority.UsePadding = false;
            this.lblSodongmienthue.Text = "XXE";
            this.lblSodongmienthue.Weight = 1.7580579376537882;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129,
            this.xrTableCell130,
            this.xrTableCell219,
            this.xrTableCell132,
            this.lblTienlephi,
            this.xrTableCell141,
            this.xrTableCell95,
            this.xrTableCell142,
            this.xrTableCell94,
            this.lblTienbaohiem});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.43999999999999995;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Weight = 0.12830196571191049;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell130.StylePriority.UsePadding = false;
            this.xrTableCell130.Text = "Tiền lệ phí";
            this.xrTableCell130.Weight = 0.2507860126694002;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell219.StylePriority.UsePadding = false;
            this.xrTableCell219.StylePriority.UseTextAlignment = false;
            this.xrTableCell219.Text = "Đơn giá";
            this.xrTableCell219.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell219.Weight = 0.20754714153004533;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Weight = 0.056603766170456309;
            // 
            // lblTienlephi
            // 
            this.lblTienlephi.Name = "lblTienlephi";
            this.lblTienlephi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblTienlephi.StylePriority.UsePadding = false;
            this.lblTienlephi.Text = "XXXXXXXXX1XXXXXXXXX2E";
            this.lblTienlephi.Weight = 0.70409166468637729;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Weight = 0.0566036161830221;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell95.StylePriority.UsePadding = false;
            this.xrTableCell95.Text = "Tiền bảo hiểm";
            this.xrTableCell95.Weight = 0.31609353547997082;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell142.StylePriority.UsePadding = false;
            this.xrTableCell142.StylePriority.UseTextAlignment = false;
            this.xrTableCell142.Text = "Đơn giá";
            this.xrTableCell142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell142.Weight = 0.18794229852402633;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Weight = 0.056603767331381137;
            // 
            // lblTienbaohiem
            // 
            this.lblTienbaohiem.Name = "lblTienbaohiem";
            this.lblTienbaohiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblTienbaohiem.StylePriority.UsePadding = false;
            this.lblTienbaohiem.Text = "XXXXXXXXX1XXXXXXXXX2E";
            this.lblTienbaohiem.Weight = 1.03542623171341;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.xrTableCell98,
            this.xrTableCell220,
            this.xrTableCell123,
            this.lblSoluongtienlephi,
            this.xrTableCell147,
            this.lblMadvtienlephi,
            this.xrTableCell143,
            this.xrTableCell144,
            this.xrTableCell145,
            this.lblSoluongtienbaohiem,
            this.xrTableCell148,
            this.lblMadvtienbaohiem});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.43999999999999995;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Weight = 0.12830196571191049;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Weight = 0.2507860126694002;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell220.StylePriority.UsePadding = false;
            this.xrTableCell220.StylePriority.UseTextAlignment = false;
            this.xrTableCell220.Text = "Số lượng";
            this.xrTableCell220.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell220.Weight = 0.20754714153004536;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Weight = 0.056603766170456309;
            // 
            // lblSoluongtienlephi
            // 
            this.lblSoluongtienlephi.Name = "lblSoluongtienlephi";
            this.lblSoluongtienlephi.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoluongtienlephi.StylePriority.UsePadding = false;
            this.lblSoluongtienlephi.StylePriority.UseTextAlignment = false;
            this.lblSoluongtienlephi.Text = "123,456,789,012";
            this.lblSoluongtienlephi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoluongtienlephi.Weight = 0.47187895712273309;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Weight = 0.03773581209234575;
            // 
            // lblMadvtienlephi
            // 
            this.lblMadvtienlephi.Name = "lblMadvtienlephi";
            this.lblMadvtienlephi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMadvtienlephi.StylePriority.UsePadding = false;
            this.lblMadvtienlephi.Text = "XXXE";
            this.lblMadvtienlephi.Weight = 0.25108051165432071;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Weight = 0.27285462518228976;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell144.StylePriority.UsePadding = false;
            this.xrTableCell144.StylePriority.UseTextAlignment = false;
            this.xrTableCell144.Text = "Số lượng";
            this.xrTableCell144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell144.Weight = 0.23118118003154375;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Weight = 0.0566037673313811;
            // 
            // lblSoluongtienbaohiem
            // 
            this.lblSoluongtienbaohiem.Name = "lblSoluongtienbaohiem";
            this.lblSoluongtienbaohiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoluongtienbaohiem.StylePriority.UsePadding = false;
            this.lblSoluongtienbaohiem.StylePriority.UseTextAlignment = false;
            this.lblSoluongtienbaohiem.Text = "123,456,789,012";
            this.lblSoluongtienbaohiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoluongtienbaohiem.Weight = 0.45639225655564519;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Weight = 0.03773607602151155;
            // 
            // lblMadvtienbaohiem
            // 
            this.lblMadvtienbaohiem.Name = "lblMadvtienbaohiem";
            this.lblMadvtienbaohiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMadvtienbaohiem.StylePriority.UsePadding = false;
            this.lblMadvtienbaohiem.Text = "XXXE";
            this.lblMadvtienbaohiem.Weight = 0.54129792792641684;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell150,
            this.xrTableCell151,
            this.xrTableCell221,
            this.xrTableCell153,
            this.lblKhoantienlephi,
            this.xrTableCell155,
            this.xrTableCell156,
            this.xrTableCell157,
            this.xrTableCell158,
            this.xrTableCell159,
            this.lblKhoantienbaohiem,
            this.xrTableCell161,
            this.xrTableCell162});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.43999999999999995;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Weight = 0.12830196571191049;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Weight = 0.21719010856536619;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell221.StylePriority.UsePadding = false;
            this.xrTableCell221.StylePriority.UseTextAlignment = false;
            this.xrTableCell221.Text = "Khoản tiền";
            this.xrTableCell221.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell221.Weight = 0.24114298805375203;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Weight = 0.056603766170456295;
            // 
            // lblKhoantienlephi
            // 
            this.lblKhoantienlephi.Name = "lblKhoantienlephi";
            this.lblKhoantienlephi.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblKhoantienlephi.StylePriority.UsePadding = false;
            this.lblKhoantienlephi.StylePriority.UseTextAlignment = false;
            this.lblKhoantienlephi.Text = "1,234,567,890,123,456";
            this.lblKhoantienlephi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblKhoantienlephi.Weight = 0.47187901470306026;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Weight = 0.03773581209234575;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell156.StylePriority.UsePadding = false;
            this.xrTableCell156.Text = "VND";
            this.xrTableCell156.Weight = 0.25108051165432066;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Weight = 0.27285462518228976;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell158.StylePriority.UsePadding = false;
            this.xrTableCell158.StylePriority.UseTextAlignment = false;
            this.xrTableCell158.Text = "Khoản tiền";
            this.xrTableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell158.Weight = 0.23118141035285272;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Weight = 0.056603767331381109;
            // 
            // lblKhoantienbaohiem
            // 
            this.lblKhoantienbaohiem.Name = "lblKhoantienbaohiem";
            this.lblKhoantienbaohiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblKhoantienbaohiem.StylePriority.UsePadding = false;
            this.lblKhoantienbaohiem.StylePriority.UseTextAlignment = false;
            this.lblKhoantienbaohiem.Text = "1,234,567,890,123,456";
            this.lblKhoantienbaohiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblKhoantienbaohiem.Weight = 0.45639225655564519;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Weight = 0.03773607602151155;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell162.StylePriority.UsePadding = false;
            this.xrTableCell162.Text = "VND";
            this.xrTableCell162.Weight = 0.54129769760510782;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell164,
            this.xrTableCell165,
            this.xrTableCell166,
            this.xrTableCell203,
            this.lblMavanvanphapquy1,
            this.xrTableCell204,
            this.lblMavanvanphapquy2,
            this.xrTableCell206,
            this.lblMavanvanphapquy3,
            this.xrTableCell207,
            this.lblMavanvanphapquy4,
            this.xrTableCell211,
            this.lblMavanvanphapquy5});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.43999999999999995;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Weight = 0.12830196571191049;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell165.StylePriority.UsePadding = false;
            this.xrTableCell165.Text = "Mã văn bản pháp luật khác";
            this.xrTableCell165.Weight = 0.54161012400150033;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Weight = 0.076162937064663816;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell203.StylePriority.UsePadding = false;
            this.xrTableCell203.Text = "1";
            this.xrTableCell203.Weight = 0.0566037674473771;
            // 
            // lblMavanvanphapquy1
            // 
            this.lblMavanvanphapquy1.Name = "lblMavanvanphapquy1";
            this.lblMavanvanphapquy1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMavanvanphapquy1.StylePriority.UsePadding = false;
            this.lblMavanvanphapquy1.Text = "XE";
            this.lblMavanvanphapquy1.Weight = 0.13207544668755325;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell204.StylePriority.UsePadding = false;
            this.xrTableCell204.Text = "2";
            this.xrTableCell204.Weight = 0.056603767360380169;
            // 
            // lblMavanvanphapquy2
            // 
            this.lblMavanvanphapquy2.Name = "lblMavanvanphapquy2";
            this.lblMavanvanphapquy2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMavanvanphapquy2.StylePriority.UsePadding = false;
            this.lblMavanvanphapquy2.Text = "XE";
            this.lblMavanvanphapquy2.Weight = 0.13207542756177515;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell206.StylePriority.UsePadding = false;
            this.xrTableCell206.Text = "3";
            this.xrTableCell206.Weight = 0.05660374811860569;
            // 
            // lblMavanvanphapquy3
            // 
            this.lblMavanvanphapquy3.Name = "lblMavanvanphapquy3";
            this.lblMavanvanphapquy3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMavanvanphapquy3.StylePriority.UsePadding = false;
            this.lblMavanvanphapquy3.Text = "XE";
            this.lblMavanvanphapquy3.Weight = 0.13207543464332039;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell207.StylePriority.UsePadding = false;
            this.xrTableCell207.Text = "4";
            this.xrTableCell207.Weight = 0.056603762513687927;
            // 
            // lblMavanvanphapquy4
            // 
            this.lblMavanvanphapquy4.Name = "lblMavanvanphapquy4";
            this.lblMavanvanphapquy4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMavanvanphapquy4.StylePriority.UsePadding = false;
            this.lblMavanvanphapquy4.Text = "XE";
            this.lblMavanvanphapquy4.Weight = 0.13207544903840196;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell211.StylePriority.UsePadding = false;
            this.xrTableCell211.Text = "5";
            this.xrTableCell211.Weight = 0.056603767331381172;
            // 
            // lblMavanvanphapquy5
            // 
            this.lblMavanvanphapquy5.Name = "lblMavanvanphapquy5";
            this.lblMavanvanphapquy5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMavanvanphapquy5.StylePriority.UsePadding = false;
            this.lblMavanvanphapquy5.Text = "XE";
            this.lblMavanvanphapquy5.Weight = 1.4426044025194429;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell213,
            this.xrTableCell214,
            this.xrTableCell225});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.43999999999999995;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Weight = 0.12830196571191049;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell214.StylePriority.UsePadding = false;
            this.xrTableCell214.Text = "Miễn / Giảm / Không chịu thuế xuất khẩu";
            this.xrTableCell214.Weight = 0.95739569490123211;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.Weight = 1.9143023393868577;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell215,
            this.xrTableCell216,
            this.lblMamiengiam,
            this.lblDieukhoanmien});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.43999999999999995;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Weight = 0.12830196571191049;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Weight = 0.32625778556460505;
            // 
            // lblMamiengiam
            // 
            this.lblMamiengiam.Name = "lblMamiengiam";
            this.lblMamiengiam.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMamiengiam.StylePriority.UsePadding = false;
            this.lblMamiengiam.Text = "XXXXE";
            this.lblMamiengiam.Weight = 0.21535222054207845;
            // 
            // lblDieukhoanmien
            // 
            this.lblDieukhoanmien.Name = "lblDieukhoanmien";
            this.lblDieukhoanmien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblDieukhoanmien.StylePriority.UsePadding = false;
            this.lblDieukhoanmien.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieukhoanmien.Weight = 2.3300880281814065;
            // 
            // ToKhaiBoSungHangHoaXuatKhauCQHQ_Page_3
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(15, 16, 26, 100);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        internal DevExpress.XtraReports.UI.XRTable xrTable1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell lblSotokhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell lblSotokhaidautien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell lblSonhanhtokhaichianho;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell lblTongsotokhaichianho;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell lblSotokhaitamnhaptaixuat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell lblMaphanloaikiemtra;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell lblMaloaihinh;
        private DevExpress.XtraReports.UI.XRTableCell lblMaphanloaihanghoa;
        private DevExpress.XtraReports.UI.XRTableCell lblMahieuphuongthucvanchuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell lblMasothuedaidien;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell lblTencoquanhaiquantiepnhantokhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell lblNhomxulyhoso;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaydangky;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell lblGiodangky;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaythaydoidangky;
        private DevExpress.XtraReports.UI.XRTableCell lblGiothaydoidangky;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell lblThoihantainhapxuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell lblBieuthitruonghophethan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblTrang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoTrang;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell lblSodong1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell lblMasohanghoa;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell lblMaquanlyrieng;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell lblMataixacnhangia;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell lblMotahanghoa;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell lblSoluong1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell lblMadvtinh1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell lblSoluong2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell lblMadvtinh2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell lblTrigia;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell lblDongia;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell lblMadongtien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell lblDonvidongia;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell lblTrigiathueS;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell lblMadongtienS;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell lblMadongtienM;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell lblTrigiathueM;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell lblSoluongthue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell lblMadvtinhchuanthue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell lblDongiathue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell lblMatientedongiathue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell lblDonvisoluongthue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell lblThuesuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloainhapthue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell lblSotienthue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell lblMatientesotienthue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell lblSotienmiengiam;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell lblMatientesotiengiamthue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell lblSodonghangtaixuat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell lblDanhmucmienthue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell lblSodongmienthue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell lblTienlephi;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell lblTienbaohiem;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell lblSoluongtienlephi;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell lblMadvtienlephi;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell lblSoluongtienbaohiem;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell lblMadvtienbaohiem;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell lblKhoantienlephi;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell lblKhoantienbaohiem;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell lblMavanvanphapquy1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell lblMavanvanphapquy2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell lblMavanvanphapquy3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell lblMavanvanphapquy4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableCell lblMavanvanphapquy5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell lblMamiengiam;
        private DevExpress.XtraReports.UI.XRTableCell lblDieukhoanmien;
    }
}
