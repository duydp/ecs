﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.BLL.KDT;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace DongBoDuLieu.BLL.TKMD_V3
{
    public class HopDongTM
    {
        public static long InsertUpdateHDTM(long TKMD_ID, DataRow drHD, SqlTransaction transaction, SqlDatabase db)
        {
            HopDongThuongMai HDTM = ConvertToHDTM(drHD);
            List<HopDongThuongMai> listHDTM = HopDongThuongMai.SelectCollectionDynamic("TKMD_ID = " + TKMD_ID, null, transaction, db);
            if (listHDTM != null && listHDTM.Count > 0)
            {
                return 0;
            }
            else
            {
                HDTM.TKMD_ID = TKMD_ID;
                long ID = HDTM.InsertTransaction(transaction, db);
                if (ID > 0)
                {

                    ToKhaiMauDich tkmd = new ToKhaiMauDich();
                    tkmd.ID = TKMD_ID;
                    tkmd.Load(transaction, db);
                    tkmd.LoadHMDCollection(transaction, db);
                    foreach (Company.BLL.KDT.HangMauDich hmd in tkmd.HMDCollection)
                    {
                        HopDongThuongMaiDetail detail = new HopDongThuongMaiDetail();
                        detail.DonGiaKB = (double)hmd.DonGiaKB;
                        detail.DVT_ID = hmd.DVT_ID;
                        detail.NuocXX_ID = hmd.NuocXX_ID;
                        detail.MaHS = hmd.MaHS;
                        detail.MaPhu = hmd.MaPhu;
                        detail.SoLuong = hmd.SoLuong;
                        detail.SoThuTuHang = hmd.SoThuTuHang;
                        detail.TriGiaKB = (double)hmd.TriGiaKB;
                        detail.HMD_ID = hmd.ID;
                        detail.TenHang = hmd.TenHang;
                        detail.HopDongTM_ID = ID;
                        detail.InsertUpdate(transaction, db);
                    }
                }
                return ID;
            }
        }
        private static HopDongThuongMai ConvertToHDTM(DataRow drHD)
        {
            string LoaiHinh = drHD["Ma_LH"].ToString().Trim().Substring(0, 1);


            HopDongThuongMai hd = new HopDongThuongMai();
            hd.SoHopDongTM = drHD["So_CT"].ToString();
            hd.NgayHopDongTM = Convert.ToDateTime(drHD["Ngay_CT"]);
            if (!string.IsNullOrEmpty(drHD["HanThanhToan"].ToString()))
                hd.ThoiHanThanhToan = Convert.ToDateTime(drHD["HanThanhToan"]);
            else
                hd.ThoiHanThanhToan = new DateTime(1900, 1, 1);
                
            //hd.SoHopDongTM = drHD["HanThanhToan"].ToString();
            hd.DKGH_ID = drHD["Ma_GH"].ToString();
            hd.DiaDiemGiaoHang = drHD["DiaDiem"].ToString();
            hd.PTTT_ID = drHD["Ma_PTTT"].ToString();
            hd.TongTriGia = System.Convert.ToDecimal(drHD["TongTriGia"]);
            hd.NguyenTe_ID = drHD["Ma_NT"].ToString();

            //Trên database HQ - Mã doanh nghiệp luôn là mã người mua
            // Tùy chỉnh lại đối với tờ khai xuất Doanh nghiệp là người bán
            if (LoaiHinh.ToUpper() == "X")
            {
                hd.MaDonViBan = drHD["MaNguoiMua"].ToString();
                hd.TenDonViBan = drHD["TenNguoiMua"].ToString();
                hd.MaDonViMua = drHD["MaNguoiBan"].ToString();
                hd.TenDonViMua = drHD["TenNguoiBan"].ToString();

            }
            else
            {
                hd.MaDonViMua = drHD["MaNguoiMua"].ToString();
                hd.TenDonViMua = drHD["TenNguoiMua"].ToString();
                hd.MaDonViBan = drHD["MaNguoiBan"].ToString();
                hd.TenDonViBan = drHD["TenNguoiBan"].ToString();
            }
            
            hd.ThongTinKhac = drHD["GhiChu"].ToString();
            return hd;
        }

    }
}
