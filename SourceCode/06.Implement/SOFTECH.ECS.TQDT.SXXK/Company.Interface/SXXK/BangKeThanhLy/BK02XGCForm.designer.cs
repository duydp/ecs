namespace Company.Interface.SXXK.BangKe
{
    partial class BK02XGCForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgTKXTL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BK02XGCForm));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgTKXTL = new Janus.Windows.GridEX.GridEX();
            this.label5 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.ccToDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label7 = new System.Windows.Forms.Label();
            this.ccFromDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label6 = new System.Windows.Forms.Label();
            this.btnNext = new Janus.Windows.EditControls.UIButton();
            this.btnFinish = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGoTo = new Janus.Windows.EditControls.UIButton();
            this.dgTKX = new Janus.Windows.GridEX.GridEX();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.btnBack = new Janus.Windows.EditControls.UIButton();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKXTL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.btnBack);
            this.grbMain.Controls.Add(this.btnAdd);
            this.grbMain.Controls.Add(this.btnDelete);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnFinish);
            this.grbMain.Controls.Add(this.btnNext);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.label4);
            this.grbMain.Size = new System.Drawing.Size(737, 412);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(9, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(266, 14);
            this.label4.TabIndex = 1;
            this.label4.Text = "Danh sách tờ khai xuất chưa thanh khoản";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgTKXTL);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(392, 86);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(333, 290);
            this.uiGroupBox3.TabIndex = 6;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgTKXTL
            // 
            this.dgTKXTL.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKXTL.AlternatingColors = true;
            this.dgTKXTL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgTKXTL.AutomaticSort = false;
            this.dgTKXTL.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKXTL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKXTL.ColumnAutoResize = true;
            dgTKXTL_DesignTimeLayout.LayoutString = resources.GetString("dgTKXTL_DesignTimeLayout.LayoutString");
            this.dgTKXTL.DesignTimeLayout = dgTKXTL_DesignTimeLayout;
            this.dgTKXTL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKXTL.GroupByBoxVisible = false;
            this.dgTKXTL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKXTL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKXTL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKXTL.ImageList = this.ImageList1;
            this.dgTKXTL.Location = new System.Drawing.Point(2, 8);
            this.dgTKXTL.Name = "dgTKXTL";
            this.dgTKXTL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKXTL.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKXTL.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKXTL.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKXTL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKXTL.Size = new System.Drawing.Size(330, 281);
            this.dgTKXTL.TabIndex = 0;
            this.dgTKXTL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKXTL.VisualStyleManager = this.vsmMain;
            this.dgTKXTL.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.dgTKXTL_DeletingRecord);
            this.dgTKXTL.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKXTL_RowDoubleClick);
            this.dgTKXTL.RecordUpdated += new System.EventHandler(this.dgTKXTL_RecordUpdated);
            this.dgTKXTL.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKXTL_LoadingRow);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(389, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(286, 14);
            this.label5.TabIndex = 5;
            this.label5.Text = "Danh sách tờ khai xuất đưa vào thanh khoản";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.btnSearch);
            this.uiGroupBox4.Controls.Add(this.ccToDate);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.ccFromDate);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(12, 9);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(712, 54);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Tìm kiếm";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // btnSearch
            // 
            this.btnSearch.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSearch.Icon")));
            this.btnSearch.Location = new System.Drawing.Point(428, 18);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(93, 23);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyleManager = this.vsmMain;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // ccToDate
            // 
            this.ccToDate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccToDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccToDate.DropDownCalendar.Name = "";
            this.ccToDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccToDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccToDate.Location = new System.Drawing.Point(329, 20);
            this.ccToDate.Name = "ccToDate";
            this.ccToDate.Nullable = true;
            this.ccToDate.NullButtonText = "Xóa";
            this.ccToDate.ShowNullButton = true;
            this.ccToDate.Size = new System.Drawing.Size(86, 21);
            this.ccToDate.TabIndex = 3;
            this.ccToDate.TodayButtonText = "Hôm nay";
            this.ccToDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccToDate.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(264, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "đến ngày";
            // 
            // ccFromDate
            // 
            this.ccFromDate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccFromDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccFromDate.DropDownCalendar.Name = "";
            this.ccFromDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccFromDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccFromDate.Location = new System.Drawing.Point(165, 20);
            this.ccFromDate.Name = "ccFromDate";
            this.ccFromDate.Nullable = true;
            this.ccFromDate.NullButtonText = "Xóa";
            this.ccFromDate.ShowNullButton = true;
            this.ccFromDate.Size = new System.Drawing.Size(86, 21);
            this.ccFromDate.TabIndex = 1;
            this.ccFromDate.TodayButtonText = "Hôm nay";
            this.ccFromDate.Value = new System.DateTime(2008, 1, 1, 0, 0, 0, 0);
            this.ccFromDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccFromDate.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Chọn tờ khai từ ngày";
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Icon = ((System.Drawing.Icon)(resources.GetObject("btnNext.Icon")));
            this.btnNext.Location = new System.Drawing.Point(427, 382);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(105, 23);
            this.btnNext.TabIndex = 9;
            this.btnNext.Text = "Tiếp tục";
            this.btnNext.VisualStyleManager = this.vsmMain;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Enabled = false;
            this.btnFinish.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinish.Icon = ((System.Drawing.Icon)(resources.GetObject("btnFinish.Icon")));
            this.btnFinish.Location = new System.Drawing.Point(538, 382);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(105, 23);
            this.btnFinish.TabIndex = 10;
            this.btnFinish.Text = "Hoàn thành";
            this.btnFinish.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(649, 382);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 23);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.btnGoTo);
            this.uiGroupBox2.Controls.Add(this.dgTKX);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 86);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(343, 290);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Location = new System.Drawing.Point(128, 12);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(100, 21);
            this.txtSoToKhai.TabIndex = 1;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.Value = 0;
            this.txtSoToKhai.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tìm đến tờ khai số:";
            // 
            // btnGoTo
            // 
            this.btnGoTo.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGoTo.Icon")));
            this.btnGoTo.Location = new System.Drawing.Point(234, 11);
            this.btnGoTo.Name = "btnGoTo";
            this.btnGoTo.Size = new System.Drawing.Size(89, 23);
            this.btnGoTo.TabIndex = 2;
            this.btnGoTo.Text = "Tìm";
            this.btnGoTo.VisualStyleManager = this.vsmMain;
            this.btnGoTo.Click += new System.EventHandler(this.btnGoTo_Click);
            // 
            // dgTKX
            // 
            this.dgTKX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgTKX.AutomaticSort = false;
            this.dgTKX.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKX.ColumnAutoResize = true;
            dgTKX_DesignTimeLayout.LayoutString = resources.GetString("dgTKX_DesignTimeLayout.LayoutString");
            this.dgTKX.DesignTimeLayout = dgTKX_DesignTimeLayout;
            this.dgTKX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKX.GroupByBoxVisible = false;
            this.dgTKX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKX.ImageList = this.ImageList1;
            this.dgTKX.Location = new System.Drawing.Point(2, 38);
            this.dgTKX.Name = "dgTKX";
            this.dgTKX.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKX.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKX.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKX.Size = new System.Drawing.Size(340, 251);
            this.dgTKX.TabIndex = 3;
            this.dgTKX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKX.VisualStyleManager = this.vsmMain;
            this.dgTKX.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKX_RowDoubleClick);
            this.dgTKX.RecordUpdated += new System.EventHandler(this.dgTKX_RecordUpdated);
            this.dgTKX.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKX_LoadingRow);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDelete.Icon")));
            this.btnDelete.Location = new System.Drawing.Point(360, 242);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(26, 23);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.VisualStyleManager = this.vsmMain;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAdd.Icon")));
            this.btnAdd.Location = new System.Drawing.Point(360, 213);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(26, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.VisualStyleManager = this.vsmMain;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.Enabled = false;
            this.btnBack.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Icon = ((System.Drawing.Icon)(resources.GetObject("btnBack.Icon")));
            this.btnBack.Location = new System.Drawing.Point(316, 382);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(105, 23);
            this.btnBack.TabIndex = 8;
            this.btnBack.Text = "Trở lại";
            this.btnBack.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label3.Location = new System.Drawing.Point(12, 382);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(298, 23);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ghi chú: Kích đôi vào tờ khai để xem thông tin chi tiết tờ khai";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BK02XGCForm
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(737, 412);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BK02XGCForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tạo bảng kê tờ khai xuất khẩu thanh lý";
            this.Load += new System.EventHandler(this.BK02WizardForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKXTL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX dgTKXTL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.CalendarCombo.CalendarCombo ccToDate;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.CalendarCombo.CalendarCombo ccFromDate;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIButton btnNext;
        private Janus.Windows.EditControls.UIButton btnFinish;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgTKX;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.EditControls.UIButton btnBack;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private Janus.Windows.EditControls.UIButton btnGoTo;
        private System.Windows.Forms.Label label3;
    }
}