﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_ChuyenCuaKhau_SoTKXuat : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiVanChuyen TKVC;
        private KDT_VNACC_TKVC_TKXuat tkXuat = null;
        DataTable dt = new DataTable();
        public VNACC_ChuyenCuaKhau_SoTKXuat()
        {
            InitializeComponent();
        }

        private void VNACC_ChuyenCuaKhau_SoTKXuat_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKVC.TKXuatCollection);
                grList.DataSource = dt;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;


                TKVC.TKXuatCollection.Clear();
                if (dt.Rows.Count == 0) return;
                foreach (DataRow i in dt.Rows)
                {
                    KDT_VNACC_TKVC_TKXuat TKX = new KDT_VNACC_TKVC_TKXuat();
                    if (i["ID"].ToString().Length != 0)
                    {
                        TKX.ID = Convert.ToInt32(i["ID"].ToString());
                        TKX.Master_ID = Convert.ToInt32(i["Master_ID"].ToString());
                    }
                    TKX.SoToKhaiXuat = Convert.ToDecimal(i["SoToKhaiXuat"].ToString());
                    TKVC.TKXuatCollection.Add(TKX);

                }
                ShowMessage("Lưu thành công", false);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACC_TKVC_TKXuat> ItemColl = new List<KDT_VNACC_TKVC_TKXuat>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ItemColl.Add((KDT_VNACC_TKVC_TKXuat)i.GetRow().DataRow);
                    }

                }
                foreach (KDT_VNACC_TKVC_TKXuat item in ItemColl)
                {
                    if (item.ID > 0)
                    {
                        if (ShowMessage("Bạn có muốn xóa số tờ khai " + item.SoToKhaiXuat + " này không?", true) == "Yes")
                        {
                            item.Delete();
                            TKVC.TKXuatCollection.Remove(item);
                        }
                    }
                }
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

    }
}
