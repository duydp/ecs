﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class DutyTaxFee
    {
        /// <summary>
        /// Số thuế tự tính tối đa 4 chữ số thập phân
        /// </summary>
        [XmlElement("adValoremTaxBase")]
        public string AdValoremTaxBase { get; set; }
        /// <summary>
        /// Điều chỉnh giảm
        /// </summary>
        [XmlElement("deduct")]
        public string Deduct { get; set; }
        /// <summary>
        /// .....
        /// </summary>
        [XmlElement("dutyRegime")]
        public string DutyRegime { get; set; }
        /// <summary>
        /// ....
        /// </summary>
        [XmlElement("specificTaxBase")]
        public string SpecificTaxBase { get; set; }
        /// <summary>
        /// Thuế suất (%) n..4,1
        /// </summary>
        [XmlElement("tax")]
        public string Tax { get; set; }
        /// <summary>
        /// loại thuế
        /// 1. Thuế XNK
        /// 2. Thuế VAT
        /// 3. Thuế Tiêu Thụ Đặc Biệt
        /// 4.
        /// 5.Chênh Lệch giá ( Phí Thu Khác)
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }


        [XmlElement("AdditionalInformation")]
        public List<AdditionalInformation> AdditionalInformations { get; set; }

    }
}
