﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.WareHouse;

namespace Company.Interface.GC.WareHouse
{
    public partial class WarehouseCardReportForm : Company.Interface.BaseForm
    {
        public HopDong HD = new HopDong();
        public T_KHOKETOAN_PHIEUXNKHO PhieuXNKho;
        public T_KHOKETOAN_NGUYENPHULIEU NPL = new T_KHOKETOAN_NGUYENPHULIEU();
        public T_KHOKETOAN_SANPHAM SP = new T_KHOKETOAN_SANPHAM();
        public WarehouseCardReportForm()
        {
            InitializeComponent();
            cbHopDong.DataSource = HopDong.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "ID");
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            cbHopDong.SelectedIndex = -1;

            cbbMaKho.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            cbbMaKho.DisplayMember = "MAKHO";
            cbbMaKho.ValueMember = "MAKHO";
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                HD = HopDong.Load(Convert.ToInt64(cbHopDong.Value.ToString()));
                clcTuNgay.Value = HD.NgayDangKy;
                clcDenNgay.Value = HD.NgayGiaHan.Year == 1900 ? HD.NgayHetHan : HD.NgayGiaHan;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }

        private void cbbLoaiHang_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbHopDong.Value!=null)
                {
                    if (cbbLoaiHang.SelectedValue.ToString()=="N")
                    {
                        cbbMaHangHoa.DataSource = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionDynamic("HOPDONG_ID =" + HD.ID, "MANPL");
                        cbbMaHangHoa.DisplayMember = "MANPL";
                        cbbMaHangHoa.ValueMember = "MANPL";
                        cbbMaHangHoa.DropDownList.Columns[0].Visible = true;
                        cbbMaHangHoa.DropDownList.Columns[1].Visible = true;

                        cbbMaHangHoa.DropDownList.Columns[2].Visible = false;
                        cbbMaHangHoa.DropDownList.Columns[3].Visible = false;
                        cbbMaHangHoa.SelectedIndex = 0;
                    }
                    else if (cbbLoaiHang.SelectedValue.ToString() == "S")
                    {
                        cbbMaHangHoa.DataSource = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic("HOPDONG_ID =" + HD.ID, "MASP");
                        cbbMaHangHoa.DisplayMember = "MASP";
                        cbbMaHangHoa.ValueMember = "MASP";
                        cbbMaHangHoa.DropDownList.Columns[0].Visible = false;
                        cbbMaHangHoa.DropDownList.Columns[1].Visible = false;

                        cbbMaHangHoa.DropDownList.Columns[2].Visible = true;
                        cbbMaHangHoa.DropDownList.Columns[3].Visible = true;
                        cbbMaHangHoa.SelectedIndex = 0;
                    }
                    else if (cbbLoaiHang.SelectedValue.ToString() == "T")
                    {

                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaHangHoa_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbLoaiHang.SelectedValue.ToString() == "N")
                {
                    List<T_KHOKETOAN_NGUYENPHULIEU> Collection = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionDynamic("HOPDONG_ID =" + HD.ID + " AND MANPL ='" + cbbMaHangHoa.Value.ToString() + "'", "MANPL");
                    txtTenHang.Text = Collection[0].TENNPL;
                    NPL = Collection[0];
                }
                else if (cbbLoaiHang.SelectedValue.ToString() == "S")
                {
                    List<T_KHOKETOAN_SANPHAM> Collection = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic("HOPDONG_ID =" + HD.ID + " AND MASP ='" + cbbMaHangHoa.Value.ToString() + "'", "MASP");
                    txtTenHang.Text = Collection[0].TENSP;
                    SP = Collection[0];
                }
                else if (cbbLoaiHang.SelectedValue.ToString() == "T")
                {

                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaKho_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaKho.Value != null)
                {
                    List<T_KHOKETOAN_DANHSACHKHO> KHOCollection = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
                    foreach (T_KHOKETOAN_DANHSACHKHO item in KHOCollection)
                    {
                        if (item.MAKHO == cbbMaKho.Value.ToString())
                        {
                            txtTenKho.Text = item.TENKHO.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string GetCondition()
        {
            string where = "";
            long   HOPDONG_ID = Convert.ToInt64(cbHopDong.Value.ToString());
            where += " PXNK.HOPDONG_ID =" + HOPDONG_ID; ;
            string DateFrom = clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:000");
            string DateTo = clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
            where += " AND PXNK.NGAYCT BETWEEN '" + DateFrom + "' AND '" + DateTo + "'";
            string MAKHO = cbbMaKho.Value.ToString();
            where += "AND PXNK.MAKHO ='" + MAKHO + "'";
            string MAHANGHOA = cbbMaHangHoa.Text.ToString();
            where += "AND HH.MAHANG ='" + MAHANGHOA + "'";
            return where;
        }
        //private void BinData()
        //{
        //    try
        //    {
        //        string DateFrom = clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:000");
        //        string DateTo = clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
        //        grList.Refresh();
        //        grList.DataSource = T_KHOKETOAN_PHIEUXNKHO.SelectCollectionDynamic("HOPDONG_ID = " + cbHopDong.Value.ToString() + " AND MAKHO='" + cbbMaKho.Value.ToString() + "' AND NGAYCT BETWEEN '" + DateFrom + "' AND '" + DateTo + "'", "ID");
        //        grList.Refetch();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //    }
        //}
        private void BinDataDetail()
        {
            try
            {
                string DateFrom = clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:000");
                string DateTo = clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
                dgList.Refresh();
                dgList.DataSource = T_KHOKETOAN_PHIEUXNKHO_HANGHOA.SelectDynamicBCQT("PHIEUXNKHO_ID IN (SELECT ID FROM dbo.T_KHOKETOAN_PHIEUXNKHO WHERE HOPDONG_ID = " + cbHopDong.Value.ToString() + " AND MAKHO='" + cbbMaKho.Value.ToString() + "' AND NGAYCT BETWEEN '" + DateFrom + "' AND '" + DateTo + "') AND MAHANG ='" + cbbMaHangHoa.Text.ToString() + "'", "ID").Tables[0];
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbHopDong, errorProvider, "Hợp đồng", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHang, errorProvider, "Loại hàng hóa");
                isValid &= ValidateControl.ValidateChoose(cbbMaHangHoa, errorProvider, "Mã hàng hóa");
                isValid &= ValidateControl.ValidateChoose(cbbMaKho, errorProvider, "Mã kho");
                isValid &= ValidateControl.ValidateDate(clcTuNgay, errorProvider, "Từ ngày");
                isValid &= ValidateControl.ValidateDate(clcDenNgay, errorProvider, "Đến ngày");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                //BinData();
                BinDataDetail();
                string DateFrom = clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:000");
                string DateTo = clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
                ReportViewBCXNT_KhoKeToan f = new ReportViewBCXNT_KhoKeToan();
                f.dt = T_KHOKETOAN_PHIEUXNKHO.SelectDynamicCartReport(" PXNK.HOPDONG_ID = " + cbHopDong.Value.ToString() + " AND PXNK.MAKHO='" + cbbMaKho.Value.ToString() + "' AND PXNK.NGAYCT BETWEEN '" + DateFrom + "' AND '" + DateTo + "' AND HH.MAHANG ='" + cbbMaHangHoa.Text.ToString() + "'", "").Tables[0];
                f.HD = HD;
                if (cbbLoaiHang.SelectedValue.ToString() == "N")
                {
                    f.NPL = NPL;
                }
                else if (cbbLoaiHang.SelectedValue.ToString() == "S")
                {
                    f.SP = SP;
                }
                f.BindReport();
                f.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.Cells["LOAIHANG"].Value != null)
                {
                    switch (e.Row.Cells["LOAIHANG"].Value.ToString())
                    {
                        case "N":
                            e.Row.Cells["LOAIHANG"].Text = "Nguyên phụ liệu";
                            break;
                        case "S":
                            e.Row.Cells["LOAIHANG"].Text = "Sản phẩm";
                            break;
                        case "T":
                            e.Row.Cells["LOAIHANG"].Text = "Thiết bị";
                            break;
                        case "H":
                            e.Row.Cells["LOAIHANG"].Text = "Hàng mẫu";
                            break;
                    }
                }
                if (e.Row.Cells["LOAICHUNGTU"].Text != String.Empty)
                {
                    switch (e.Row.Cells["LOAICHUNGTU"].Text.ToString())
                    {
                        case "N":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu nhập kho";
                            break;
                        case "X":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu xuất kho";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.Cells["LOAIHANG"].Value != null)
                {
                    switch (e.Row.Cells["LOAIHANG"].Value.ToString())
                    {
                        case "N":
                            e.Row.Cells["LOAIHANG"].Text = "Nguyên phụ liệu";
                            break;
                        case "S":
                            e.Row.Cells["LOAIHANG"].Text = "Sản phẩm";
                            break;
                        case "T":
                            e.Row.Cells["LOAIHANG"].Text = "Thiết bị";
                            break;
                        case "H":
                            e.Row.Cells["LOAIHANG"].Text = "Hàng mẫu";
                            break;
                    }
                }
                if (e.Row.Cells["LOAICHUNGTU"].Text != String.Empty)
                {
                    switch (e.Row.Cells["LOAICHUNGTU"].Text.ToString())
                    {
                        case "N":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu nhập kho";
                            break;
                        case "X":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu xuất kho";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                PhieuXNKho = new T_KHOKETOAN_PHIEUXNKHO();
                PhieuXNKho = (T_KHOKETOAN_PHIEUXNKHO)e.Row.DataRow;
                PhieuXNKho.HangCollection = T_KHOKETOAN_PHIEUXNKHO_HANGHOA.SelectCollectionBy_PHIEUXNKHO_ID(PhieuXNKho.ID);
                WareHouseExportForm f = new WareHouseExportForm();
                long HopDong_ID = HopDong.GetID(PhieuXNKho.SOHOPDONG);
                HopDong HD = HopDong.Load(HopDong_ID);
                f.HD = HD;
                f.PhieuXNKho = PhieuXNKho;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                //if (uiTab1.SelectedTab.Name == "uiTabPage1")
                //{
                //    SaveFileDialog sfNPL = new SaveFileDialog();
                //    sfNPL.FileName = "Danh sách Phiếu xuất nhập kho NPL : " +cbbMaHangHoa.Text.ToString()+ " Từ ngày (" + clcTuNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ") Đến ngày (" + clcDenNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ").xls";
                //    sfNPL.Filter = "Excel files| *.xls";
                //    if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                //    {
                //        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                //        gridEXExporter1.GridEX = grList;
                //        System.IO.Stream str = sfNPL.OpenFile();
                //        gridEXExporter1.Export(str);
                //        str.Close();

                //        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                //        {
                //            System.Diagnostics.Process.Start(sfNPL.FileName);
                //        }
                //    }
                //}
                //else
                //{
                    SaveFileDialog sfNPL = new SaveFileDialog();
                    sfNPL.FileName = "Danh sách Hàng hóa Phiếu xuất nhập kho NPL : " + cbbMaHangHoa.Text.ToString() + " Từ ngày (" + clcTuNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ") Đến ngày (" + clcDenNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ").xls";
                    sfNPL.Filter = "Excel files| *.xls";
                    if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {

        }
    }
}
