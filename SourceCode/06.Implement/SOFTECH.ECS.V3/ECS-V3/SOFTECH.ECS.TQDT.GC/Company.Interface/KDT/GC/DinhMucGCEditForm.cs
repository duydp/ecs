﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.KDT.SXXK;
using System.Data;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class DinhMucGCEditForm : BaseForm
    {
        public Company.GC.BLL.KDT.GC.DinhMuc DMDetail = new Company.GC.BLL.KDT.GC.DinhMuc();                
        public DinhMucDangKy dmdk=new DinhMucDangKy();             
        public DinhMucGCEditForm()
        {
            InitializeComponent();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            txtDonViTinhSP.DataSource = this._DonViTinh;
            txtDonViTinhSP.DisplayMember = "Ten";
            txtDonViTinhSP.ValueMember = "ID";
            txtDonViTinhSP.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            txtDonViTinhNPL.DataSource = this._DonViTinh;
            txtDonViTinhNPL.DisplayMember = "Ten";
            txtDonViTinhNPL.ValueMember = "ID";
            txtDonViTinhNPL.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
        }

        private void txtMaSP_Leave(object sender, EventArgs e)
        {
            if (txtMaSP.Text.Trim() == "")
                return;
            Company.GC.BLL.GC.SanPham sp  = (new Company.GC.BLL.GC.SanPham());
            sp.Ma = txtMaSP.Text.Trim();
            sp.HopDong_ID = dmdk.ID_HopDong;
            sp.Load();
            if (sp != null && sp.Ten.Length > 0)
            {
                txtMaSP.Text = sp.Ma;
                txtTenSP.Text = sp.Ten.Trim();
                txtMaHSSP.Text = sp.MaHS.Trim();
                txtDonViTinhSP.SelectedValue = sp.DVT_ID;
                txtMaNPL.Focus();

                error.SetError(txtMaHSNPL, null);
                error.SetError(txtMaSP, null);
                error.SetError(txtTenNPL, null);

            }
            else
            {
                error.SetError(txtMaSP, setText("Không tồn tại sản phẩm này.", "This value is not exist"));
                txtMaSP.Clear();
                txtMaSP.Focus();
                return;
            }
            //if (txtMaSP.Text.Trim() == "")
            //    return;
            //long id = dmdk.GetIDDinhMucExit(txtMaSP.Text.Trim(), dmdk.ID, this.dmdk.ID_HopDong);
            //if (id > 0)
            //{
            //    showMsg("MSG_2702034", new string[] { txtMaSP.Text, id.ToString() });
            //    //ShowMessage("Sản phẩm : " + txtMaSP.Text.Trim() + " đã được khai báo trong danh sách định mức có ID= " + id, false);
            //    txtMaSP.Clear();
            //    txtMaSP.Focus();
            //    return;
            //}
            //else if (id < 0)
            //{
            //    showMsg("MSG_240230", txtMaSP.Text.Trim());
            //    //ShowMessage("Sản phẩm : " + txtMaSP.Text.Trim() + " đã được khai báo định mức.", false);
            //    txtMaSP.Clear();
            //    txtMaSP.Focus();
            //    return;
            //}
            //Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
            //dmDuyet.HopDong_ID = dmdk.ID_HopDong;
            //dmDuyet.MaSanPham = txtMaSP.Text.Trim();
            //if (dmDuyet.CheckExitsDinhMucSanPham())
            //{
            //    showMsg("MSG_240230", txtMaSP.Text.Trim());
            //    //ShowMessage("Sản phẩm : " + txtMaSP.Text.Trim() + " đã được khai báo định mức.", false);
            //    txtMaSP.Clear();
            //    txtMaSP.Focus();
            //    return;
            //}
            //Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
            //sp.Ma = txtMaSP.Text.Trim();
            //sp.HopDong_ID = dmdk.ID_HopDong;
            //sp.Load();
            //if (sp != null && sp.Ten.Length > 0)
            //{
            //    txtMaSP.Text = sp.Ma;
            //    txtTenSP.Text = sp.Ten.Trim();
            //    txtMaHSSP.Text = sp.MaHS.Trim();
            //    txtDonViTinhSP.SelectedValue = sp.DVT_ID; ;
            //    txtMaNPL.Focus();
            //    error.SetError(txtMaSP, null);
            //    error.SetError(txtTenSP, null);
            //    error.SetError(txtMaHSSP, null);
            //}
            //else
            //{
            //    error.SetError(txtMaSP, setText("Không tồn tại sản phẩm này.","This value is not exist"));
            //    txtMaSP.Clear();
            //    txtMaSP.Focus();
            //    return;
            //}
        }

        private void txtMaSP_ButtonClick(object sender, EventArgs e)
        {            
            DinhMucSelectSanPhamForm f = new DinhMucSelectSanPhamForm();
            f.SPDetail.HopDong_ID = dmdk.ID_HopDong;      
            HopDong hd=new HopDong();
            hd.ID = dmdk.ID_HopDong;
            f.HD = hd;
            f.ShowDialog();
            if (f.SPDetail!=null && f.SPDetail.Ma != "")
            {
                txtMaSP.Text = f.SPDetail.Ma.Trim();
                txtTenSP.Text = f.SPDetail.Ten.Trim();
                txtMaHSSP.Text = f.SPDetail.MaHS.Trim();
                txtDonViTinhSP.SelectedValue = f.SPDetail.DVT_ID;
                txtMaNPL.Focus();
            }
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {            
            DinhMucSelectNguyenPhuLieuForm f = new DinhMucSelectNguyenPhuLieuForm();            
            f.isBrower = true;
            f.npl.HopDong_ID = dmdk.ID_HopDong;
            HopDong hd = new HopDong();
            hd.ID = dmdk.ID_HopDong;
            f.HD = hd;
            f.ShowDialog();
            if (f.npl!=null && f.npl.Ma != "")
            {
                txtMaNPL.Text = f.npl.Ma.Trim();
                txtTenNPL.Text = f.npl.Ten.Trim();
                txtMaHSNPL.Text = f.npl.MaHS.Trim();
                txtDonViTinhNPL.SelectedValue = f.npl.DVT_ID;
                txtDinhMuc.Focus();
            }
        }

        private void DinhMucGCEditForm_Load(object sender, EventArgs e)
        {
            txtDinhMuc.DecimalDigits = Convert.ToInt32(GlobalSettings.SoThapPhan.DinhMuc);
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            txtTyLeHH.DecimalDigits = Convert.ToInt32(GlobalSettings.SoThapPhan.TLHH);
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
            dgList.Tables[0].Columns["NPL_TuCungUng"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
         
            {            
                txtMaSP.Text = DMDetail.MaSanPham;                          
                //load thong tin npl
                txtMaNPL.Text = DMDetail.MaNguyenPhuLieu;
                txtMaSP_Leave(null, null);
                txtMaNPL_Leave(null, null);
                txtDinhMuc.Text = DMDetail.DinhMucSuDung.ToString();
                txtTyLeHH.Text = DMDetail.TyLeHaoHut.ToString();
                txtNPLtucung.Text = DMDetail.NPL_TuCungUng.ToString();
                txtGhiChu.Text = DMDetail.GhiChu.Trim();                
            }
            if (this.OpenType==OpenFormType.View)
            {
                btnAdd.Enabled = false;
                btnDelete.Enabled = false;             
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
            dgList.DataSource = dmdk.DMCollection;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            // 1. Kiểm tra dữ liệu.
            //cvError.Validate();
            //if (!cvError.IsValid) return;

            //if (!this.existNPL)
            //{
            //    error.SetIconPadding(txtMaNPL, -8);
            //    error.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
            //    return;
            //}
            //if (!this.existSP)
            //{
            //    error.SetIconPadding(txtMaSP, -8);
            //    error.SetError(txtMaSP, "Không tồn tại sản phẩm này.");
            //    return;
            //}

            //// 2. Cập nhật dữ liệu.
            //this.DMDetail.MaSanPham = txtMaSP.Text;
            //this.DMDetail.MaNguyenPhuLieu = txtMaNPL.Text;
            //this.DMDetail.DinhMucSuDung = Convert.ToDecimal(txtDinhMuc.Value);
            //this.DMDetail.TyLeHaoHut = Convert.ToDecimal(txtTyLeHH.Value);

            //// 3. Đóng.
            //this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DMDetail = null;
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //txtMaSP.Text = "";
            //txtTenSP.Text = "";
            //txtMaHSSP.Text = "";
            //txtDinhMuc.Text = "0";
            //txtMaHSNPL.Text = "";
            //txtMaNPL.Text = "";
            //txtTenNPL.Text = "";
            //txtTyLeHH.Text = "0.000";
            ////txtGhiChu.Text = "";
            //txtNPLtucung.Text = "0";
            //btnAdd.Text = "Thêm";
            //this.DMDetail = new DinhMuc();
            DinhMucCollection dmcoll = new DinhMucCollection();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count < 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa định mức của sản phẩm này không?", true) == "Yes")
            {                
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.DinhMuc dm = (Company.GC.BLL.KDT.GC.DinhMuc)row.GetRow().DataRow;
                        if (dm.ID > 0)
                        {
                            try
                            {
                                dm.Delete(dmdk.ID_HopDong);
                            }
                            catch { }
                        }
                        dmcoll.Add(dm);                       
                       
                    }
                }
                foreach (Company.GC.BLL.KDT.GC.DinhMuc dmm in dmcoll)
                {
                    dmdk.DMCollection.Remove(dmm);

                }
                dgList.DataSource = dmdk.DMCollection;                 
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
        }
        

        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtMaSP_Leave(null, null);
            txtMaNPL_Leave(null, null);
            //if (Convert.ToDecimal(txtDinhMuc.Text) == 0)
            //{
            //    error.SetError(txtDinhMuc, "Định mức phải lớn hơn 0");
            //    return;
            //}
            txtMaNPL.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            {               
                checkExitsDinhMucAndSTTHang();
            }
          
            
        }
        private void checkExitsDinhMucAndSTTHang()
        {
            long id = dmdk.GetIDDinhMucExit(txtMaSP.Text.Trim(), dmdk.ID, this.dmdk.ID_HopDong);
            if (id > 0)
            {
                showMsg("MSG_2702034", new string[] { txtMaSP.Text.Trim(), id.ToString() });
                //ShowMessage("Sản phẩm : " + txtMaSP.Text.Trim() + " đã được khai báo trong danh sách định mức có ID= " + id, false);
                //dmCollection.Clear();
                return;
            }
            else if (id < 0)
            {
                showMsg("MSG_240230", txtMaSP.Text.Trim());
                //ShowMessage("Sản phẩm : " + txtMaSP.Text.Trim() + " đã được khai báo định mức.", false);
                //dmCollection.Clear();
                return;
            }
            Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
            dmDuyet.HopDong_ID = dmdk.ID_HopDong;
            dmDuyet.MaSanPham = txtMaSP.Text.Trim();
            dmDuyet.MaNguyenPhuLieu = txtMaNPL.Text.Trim();
            if (dmDuyet.CheckExitDMbySPandNPL())
            {
                showMsg("MSG_240230", txtMaSP.Text.Trim());
                //ShowMessage("Sản phẩm : " + txtMaSP.Text.Trim() + " đã được khai báo định mức.", false);
                //dmCollection.Clear();
                return;
            }
            dmdk.DMCollection.Remove(DMDetail);                   
            foreach (Company.GC.BLL.KDT.GC.DinhMuc dm in dmdk.DMCollection)
            {
                if (dm.MaNguyenPhuLieu==txtMaNPL.Text.Trim() && dm.MaSanPham==txtMaSP.Text.Trim())
                {
                    showMsg("MSG_2702035");
                    //ShowMessage("Đã có định mức này trong danh sách.", false);  
                    if(DMDetail.MaSanPham.Trim().Length>0)
                        dmdk.DMCollection.Add(DMDetail);
                    return;
                }
            }
            DMDetail.MaSanPham = txtMaSP.Text.Trim();
            DMDetail.MaNguyenPhuLieu = txtMaNPL.Text.Trim();
            DMDetail.TyLeHaoHut = Convert.ToDecimal(txtTyLeHH.Text);
            DMDetail.DinhMucSuDung = Convert.ToDecimal(txtDinhMuc.Text);
            DMDetail.GhiChu = txtGhiChu.Text.Trim();
            DMDetail.NPL_TuCungUng = Convert.ToDecimal(txtNPLtucung.Text);
            DMDetail.DVT_ID = txtDonViTinhNPL.SelectedValue.ToString(); ;
            DMDetail.TenNPL = txtTenNPL.Text.Trim();
            DMDetail.TenSanPham = txtTenSP.Text.Trim();
            dmdk.DMCollection.Add(DMDetail);            
            reset();       
        }
        private void reset()
        {

            txtMaNPL.Text = "";
            txtTenNPL.Text = "";
            txtTyLeHH.Text ="0";
            txtDinhMuc.Text = "0";
            txtTenNPL.Text = "";            
            txtMaHSNPL.Text = "";
            txtGhiChu.Text = "";
            txtNPLtucung.Text = "0";
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
            DMDetail = new Company.GC.BLL.KDT.GC.DinhMuc();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                DMDetail = (DinhMuc)e.Row.DataRow;
                //load thong tin sp
                txtMaNPL.Text = DMDetail.MaNguyenPhuLieu;
                txtMaSP.Text = DMDetail.MaSanPham;
                txtMaSP_Leave(null, null);
                txtMaNPL_Leave(null, null);
                
                txtDinhMuc.Text = DMDetail.DinhMucSuDung.ToString();
                txtTyLeHH.Text = DMDetail.TyLeHaoHut.ToString();
                txtNPLtucung.Text = DMDetail.NPL_TuCungUng.ToString();
                txtGhiChu.Text = DMDetail.GhiChu;
                txtDinhMuc.Focus();
            }
        }

        private void dgList_DeletingRecord(object sender, Janus.Windows.GridEX.RowActionCancelEventArgs e)
        {
            
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            if (txtMaNPL.Text.Trim() == "")
                return;
            Company.GC.BLL.GC.NguyenPhuLieu npl = (new Company.GC.BLL.GC.NguyenPhuLieu());
            npl.Ma = txtMaNPL.Text.Trim();
            npl.HopDong_ID = dmdk.ID_HopDong;
            npl.Load();
            if (npl != null && npl.Ten.Length > 0)
            {
                txtMaNPL.Text = npl.Ma;
                txtTenNPL.Text = npl.Ten.Trim();
                txtMaHSNPL.Text = npl.MaHS.Trim();
                txtDonViTinhNPL.SelectedValue = npl.DVT_ID;
                txtDinhMuc.Focus();

                error.SetError(txtMaHSNPL , null);
                error.SetError(txtMaNPL, null);
                error.SetError(txtTenNPL , null);
               
            }
            else
            {
                error.SetError(txtMaNPL,setText("Không tồn tại nguyên phụ liệu này.","This value is not exist"));
                txtMaNPL.Clear();
                txtMaNPL.Focus();
                return;
            }
            if (txtMaNPL.Text.Trim() == "")
                return;
            //long id = dmdk.GetIDDinhMucExit(txtMaNPL.Text.Trim(), dmdk.ID, this.dmdk.ID_HopDong);
            //if (id > 0)
            //{
            //    showMsg("MSG_2702034", new string[] { txtMaNPL.Text, id.ToString() });
            //    //ShowMessage("Sản phẩm : " + txtMaNPL.Text.Trim() + " đã được khai báo trong danh sách định mức có ID= " + id, false);
            //    txtMaNPL.Clear();
            //    txtMaNPL.Focus();
            //    return;
            //}
            //else if (id < 0)
            //{
            //    showMsg("MSG_240230", txtMaNPL.Text.Trim());
            //    //ShowMessage("Sản phẩm : " + txtMaNPL.Text.Trim() + " đã được khai báo định mức.", false);
            //    txtMaNPL.Clear();
            //    txtMaNPL.Focus();
            //    return;
            //}
            Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
            dmDuyet.HopDong_ID = dmdk.ID_HopDong;
            dmDuyet.MaSanPham = txtMaSP.Text.Trim();
            dmDuyet.MaNguyenPhuLieu = txtMaNPL.Text.Trim();
            if (dmDuyet.CheckExitDMbySPandNPL())
            {
                //showMsg("MSG_240230", txtMaNPL.Text.Trim());
                //ShowMessage("Sản phẩm : " + txtMaNPL.Text.Trim() + " đã được khai báo định mức.", false);
                ShowMessage("Sản phẩm: " + txtMaSP.Text.Trim() + " và Nguyên phụ liệu: " + txtMaNPL.Text + " đã được khai báo định mức", false);
                txtMaNPL.Clear();
                txtMaNPL.Focus();
                return;
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")                  
            //if (ShowMessage("Bạn có muốn xóa định mức của sản phẩm này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.DinhMuc dm = (Company.GC.BLL.KDT.GC.DinhMuc)row.GetRow().DataRow;
                        if (dm.ID > 0)
                        {
                            dm.Delete(dmdk.ID_HopDong);
                        }
                    }
                }                   
            }
            else
            {
                e.Cancel = true;
            }            
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            DinhMucSelectSanPhamCopyForm f = new DinhMucSelectSanPhamCopyForm();
            f.SPDetail.HopDong_ID = dmdk.ID_HopDong;
            HopDong hd = new HopDong();
            hd.ID = dmdk.ID_HopDong;
            f.HD = hd;
            f.ShowDialog();
            if (f.SPDetail != null && f.SPDetail.Ma != "")
            {
                txtMaSP.Text = f.SPDetail.Ma.Trim();
                txtTenSP.Text = f.SPDetail.Ten.Trim();
                txtMaHSSP.Text = f.SPDetail.MaHS.Trim();
                txtDonViTinhSP.SelectedValue = f.SPDetail.DVT_ID;
                txtMaNPL.Focus();
            }
        }

        private void copyDinhMuc_Click(object sender, EventArgs e)
        {
            if (txtMaSP.Text.Trim() == "")
            {
                showMsg("MSG_2702036");
                //ShowMessage("Bạn chưa chọn sản phẩm để nhập định mức.", false);                
                return;
            }
            if (dgList.GetRow() != null)
            {
                if (dgList.GetRow().RowType == RowType.Record)
                {
                    DinhMuc dm = (DinhMuc)dgList.GetRow().DataRow;
                    if (dm.MaSanPham.ToUpper().Trim() == txtMaSP.Text.Trim().ToUpper())
                    {
                        showMsg("MSG_2702037", txtMaSP.Text.Trim());
                        //ShowMessage("Sản phẩm : " + txtMaSP.Text.Trim()+" đã được khai báo trên lưới.", false);
                        return;
                    }
                    long id = dmdk.GetIDDinhMucExit(txtMaSP.Text.Trim(), dmdk.ID, this.dmdk.ID_HopDong);
                    if (id > 0)
                    {
                        showMsg("MSG_2702034", new string[] { txtMaSP.Text.Trim(), id.ToString()});
                        //ShowMessage("Sản phẩm : " + txtMaSP.Text.Trim() + " đã được khai báo trong danh sách định mức có ID= " + id, false);
                        //dmCollection.Clear();
                        return;
                    }
                    else if (id < 0)
                    {
                        showMsg("MSG_240230", txtMaSP.Text.Trim());
                        //ShowMessage("Sản phẩm : " + txtMaSP.Text.Trim() + " đã được khai báo định mức.", false);
                        //dmCollection.Clear();
                        return;
                    }
                    Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
                    dmDuyet.HopDong_ID = dmdk.ID_HopDong;
                    dmDuyet.MaSanPham = txtMaSP.Text.Trim();
                    if (dmDuyet.CheckExitsDinhMucSanPham())
                    {
                        showMsg("MSG_240230", txtMaSP.Text.Trim());
                        //ShowMessage("Sản phẩm : " + txtMaSP.Text.Trim() + " đã được khai báo định mức.", false);
                        //dmCollection.Clear();
                        return;
                    }
                    DinhMucCollection DMCollectionCopy = new DinhMucCollection();
                    foreach (DinhMuc dmCopy in dmdk.DMCollection)
                    {
                        if (dmCopy.MaSanPham.ToUpper().Trim() == dm.MaSanPham.ToUpper().Trim())
                        {
                            DinhMuc dmSaoChep = new DinhMuc();
                            dmSaoChep.MaSanPham = txtMaSP.Text.Trim();
                            dmSaoChep.MaNguyenPhuLieu = dmCopy.MaNguyenPhuLieu;
                            dmSaoChep.NPL_TuCungUng = dmCopy.NPL_TuCungUng;
                            dmSaoChep.TenNPL = dmCopy.TenNPL;
                            dmSaoChep.TenSanPham = dmCopy.TenSanPham;
                            dmSaoChep.TyLeHaoHut = dmCopy.TyLeHaoHut;
                            dmSaoChep.GhiChu = dmCopy.GhiChu;
                            dmSaoChep.DVT_ID = dmCopy.DVT_ID;
                            dmSaoChep.DinhMucSuDung = dmCopy.DinhMucSuDung;
                            DMCollectionCopy.Add(dmSaoChep);
                        }
                    }
                    CopyDinhMucGCForm copyDmForm = new CopyDinhMucGCForm();
                    copyDmForm.DMCollectionCopy=DMCollectionCopy;
                    copyDmForm.DMDangKy=dmdk;
                    if (dmdk.TrangThaiXuLy ==TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        copyDmForm.OpenType = OpenFormType.Edit;
                    }
                    else
                        copyDmForm.OpenType = OpenFormType.View;
                    copyDmForm.ShowDialog();
                       try { dgList.Refetch(); }
                       catch { dgList.Refresh(); }
                }
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {

            if (txtMaSP.Text.Trim() == "")
            {
                showMsg("MSG_2702036");
                //ShowMessage("Bạn chưa chọn sản phẩm để nhập định mức.", false);                
                return;
            }

            CopyDinhMucGCDaDuyetForm f = new CopyDinhMucGCDaDuyetForm();
            HopDong HD=new HopDong();
            HD.ID=dmdk.ID_HopDong;
            HD = HopDong.Load(HD.ID);
            f.HD = HD;
            f.ShowDialog();
            if (f.SP != null)
            {
                DataTable dsDinhMuc = new Company.GC.BLL.GC.DinhMuc().getDinhMuc(HD.ID, f.SP.Ma);
                foreach (DataRow row in dsDinhMuc.Rows)
                {
                    DinhMuc dm = new DinhMuc();
                    dm.DinhMucSuDung = Convert.ToDecimal(row["DinhMucSuDung"]);
                    dm.MaNguyenPhuLieu = row["MaNguyenPhuLieu"].ToString();
                    dm.MaSanPham = txtMaSP.Text.Trim();
                    dm.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);
                    dm.TenNPL = row["TenNPL"].ToString();
                    dm.TenSanPham = txtTenSP.Text.Trim();
                    dm.DVT_ID = row["DVT_ID"].ToString();                    
                    dmdk.DMCollection.Add(dm);
                }

                try { dgList.DataSource = dmdk.DMCollection; dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
        }
    }
}