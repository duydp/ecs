﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu;
using Infragistics.Excel;

namespace Company.Interface
{
    public partial class InToKhaiSuaDoiBoSungForm : BaseForm
    {
        public static Company.KD.BLL.KDT.ToKhaiMauDich TKMD = new Company.KD.BLL.KDT.ToKhaiMauDich();
        private int IDSoDieuChinh;
        private List<NoiDungDieuChinhTKDetail> ListNoiDungEditTKDetail = new List<NoiDungDieuChinhTKDetail>();
        private const int soDongSuaDoi = 19;
        private int index;
        public InToKhaiSuaDoiBoSungForm()
        {
            InitializeComponent();
        }

        private void InToKhaiSuaDoiBoSungForm_Load(object sender, EventArgs e)
        {
            try
            {
                //Update by KhanhHN
                //không mở form khi không có nội dung sửa đổi tờ khai
                DataTable dt = NoiDungDieuChinhTK.SelectBy_TKMD_ID(TKMD.ID).Tables[0];
                if (dt.Rows.Count <= 0)
                {
                    ShowMessage("Không có nội dung sửa đổi bổ sung", false);
                    this.Close();
                    return;
                }

                cmbSoDieuChinh.DataSource = dt;
                cmbSoDieuChinh.ValueMember = "ID";
                cmbSoDieuChinh.DisplayMember = "SoDieuChinh";
                cmbSoDieuChinh.SelectedIndex = 0;

                lblMaToKhai.Text = TKMD.ID.ToString();
                lblSoToKhai.Text = TKMD.SoToKhai.ToString();
                lblMaLoaiHinh.Text = TKMD.MaLoaiHinh;
                lblNgayDangKy.Text = TKMD.NgayDangKy.ToShortDateString();

                ListNoiDungEditTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(Convert.ToInt32(cmbSoDieuChinh.SelectedItem.Value));
                if (ListNoiDungEditTKDetail.Count > 19)
                {
                    int count = (ListNoiDungEditTKDetail.Count - 1) / 19 + 1;
                    for (int i = 0; i < count; i++)
                        this.AddItemComboBox();
                }
                cboTrang.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi trong quá trình In dữ liệu sửa đổi, bổ sung.\r\nChi tiết lỗi: " + ex.Message, false);
                return;
            }
        }

        private void BindReport(string ts)
        {
            //Lấy đường dẫn file gốc
            string destFile = "";
            string fileName = Company.KDT.SHARE.Components.Globals.FILE_EXCEL_TOKHAI_SUADOI_BOSUNG;
            string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

            //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
            try
            {
                Workbook workBook = Workbook.Load(sourcePath);
                Worksheet workSheet = workBook.Worksheets[0];

                workSheet.Rows[5].Cells[6].Value = GlobalSettings.TEN_HAI_QUAN;
                workSheet.Rows[5].Cells[6].CellFormat.Font.Name = "Times New Roman";
                workSheet.Rows[5].Cells[6].CellFormat.Font.Height = 10 * 20;

                if (TKMD.MaLoaiHinh.Substring(0, 1) == "N")
                {
                    workSheet.Rows[7].Cells[6].Value = GlobalSettings.TEN_DON_VI;
                    workSheet.Rows[7].Cells[6].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[7].Cells[6].CellFormat.Font.Height = 10 * 20;
                }
                else
                {
                    workSheet.Rows[7].Cells[6].Value = TKMD.TenDonViDoiTac;
                    workSheet.Rows[7].Cells[6].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[7].Cells[6].CellFormat.Font.Height = 10 * 20;
                }

                workSheet.Rows[8].Cells[1].Value = GlobalSettings.MA_DON_VI;
                workSheet.Rows[8].Cells[1].CellFormat.Font.Name = "Times New Roman";
                workSheet.Rows[8].Cells[1].CellFormat.Font.Height = 10 * 20;

                workSheet.Rows[9].Cells[3].Value = TKMD.SoToKhai;
                workSheet.Rows[9].Cells[3].CellFormat.Font.Name = "Times New Roman";
                workSheet.Rows[9].Cells[3].CellFormat.Font.Height = 10 * 20;

                workSheet.Rows[9].Cells[9].Value = TKMD.MaLoaiHinh;
                workSheet.Rows[9].Cells[9].CellFormat.Font.Name = "Times New Roman";
                workSheet.Rows[9].Cells[9].CellFormat.Font.Height = 10 * 20;

                workSheet.Rows[10].Cells[4].Value = TKMD.NgayTiepNhan;
                workSheet.Rows[10].Cells[4].CellFormat.Font.Name = "Times New Roman";
                workSheet.Rows[10].Cells[4].CellFormat.Font.Height = 10 * 20;

                workSheet.Rows[9].Cells[15].Value = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKMD.ID);
                workSheet.Rows[9].Cells[15].CellFormat.Font.Name = "Times New Roman";
                workSheet.Rows[9].Cells[15].CellFormat.Font.Height = 10 * 20;

                //Gán giá trị tương ứng vào các ô trong bảng nội dung sửa đổi
                fillNoiDungTKChinh(workSheet, 13, 1, IDSoDieuChinh, ts);
                fillNoiDungTKSua(workSheet, 13, 1, IDSoDieuChinh, ts);

                workSheet.Rows[33].Cells[1].Value = "Ngày " + TKMD.NgayDangKy.Day.ToString() + " tháng " + TKMD.NgayDangKy.Month.ToString() + " năm " + TKMD.NgayDangKy.Year.ToString();
                workSheet.Rows[33].Cells[1].CellFormat.Font.Name = "Times New Roman";
                workSheet.Rows[33].Cells[1].CellFormat.Font.Height = 10 * 20;

                workSheet.Rows[33].Cells[14].Value = "Ngày " + TKMD.NgayDangKy.Day.ToString() + " tháng " + TKMD.NgayDangKy.Month.ToString() + " năm " + TKMD.NgayDangKy.Year.ToString();
                workSheet.Rows[33].Cells[14].CellFormat.Font.Name = "Times New Roman";
                workSheet.Rows[33].Cells[14].CellFormat.Font.Height = 10 * 20;

                //Chọn đường dẫn file cần lưu
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                string dateNow = DateTime.Now.ToShortDateString().Replace("/", "");
                if (ListNoiDungEditTKDetail.Count > 19)
                    saveFileDialog1.FileName = "ToKhaiSuaBoSung" + "_TK" + TKMD.SoToKhai + "_NamDK" + TKMD.NamDK + "_" + dateNow + "_Trang" + ts.ToString();
                else
                    saveFileDialog1.FileName = "ToKhaiSuaBoSung" + "_TK" + TKMD.SoToKhai + "_NamDK" + TKMD.NamDK + "_" + dateNow;
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    destFile = saveFileDialog1.FileName;
                    //Ghi nội dung file gốc vào file cần lưu
                    try
                    {
                        byte[] sourceFile = Globals.ReadFile(sourcePath);
                        Globals.WriteFile(destFile, sourceFile);
                        workBook.Save(destFile);
                        ShowMessage("Lưu thành công Tờ khai sửa đổi bổ sung", false);
                        System.Diagnostics.Process.Start(destFile);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(ex.Message, false);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                return;
            }
        }

        public void AddItemComboBox()
        {
            cboTrang.Items.Add("Trang " + cboTrang.Items.Count, cboTrang.Items.Count);
        }

        private void btnThuchien_Click(object sender, EventArgs e)
        {
            IDSoDieuChinh = Convert.ToInt32(cmbSoDieuChinh.SelectedItem.Value);
            bool ok = true;
            if (ListNoiDungEditTKDetail.Count <= 19)
                index = 1;
            else
                index = cboTrang.SelectedIndex + 1;

            NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                if ((ndDieuChinhTK.ID == IDSoDieuChinh) && (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET))
                {
                    ShowMessage("Thông tin sửa đổi, bổ sung có ID = " + ndDieuChinhTK.ID.ToString() + " chưa được duyệt. Không thể in tờ khai sửa đổi bổ sung", false);
                    ok = false;
                }
            }

            if (ok)
            {
                BindReport(index.ToString());
            }
            else
                return;
        }

        private void fillNoiDungTKChinh(Worksheet worksheet, int row, int column, int idSDC, string ts)
        {
            ListNoiDungEditTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(idSDC);
            int begin = (Convert.ToInt32(ts) - 1) * soDongSuaDoi;
            int end = Convert.ToInt32(ts) * soDongSuaDoi;
            if (end > ListNoiDungEditTKDetail.Count) end = ListNoiDungEditTKDetail.Count;
            List<NoiDungDieuChinhTKDetail> ListInTKSuaDoiBoSung = new List<NoiDungDieuChinhTKDetail>();
            for (int i = begin; i < end; i++)
            {
                ListInTKSuaDoiBoSung.Add(ListNoiDungEditTKDetail[i]);
            }

            int stt = 0;
            foreach (NoiDungDieuChinhTKDetail ndDieuChinhTKChiTiet in ListInTKSuaDoiBoSung)
            {
                if (row >= 13 && row <= 31)
                {
                    worksheet.Rows[row].Cells[0].Value = ((stt + 1) + ((Convert.ToInt32(ts) - 1) * soDongSuaDoi)).ToString();

                    worksheet.Rows[row].Cells[column].Value = ndDieuChinhTKChiTiet.NoiDungTKChinh;
                    if (worksheet.Rows[row].Cells[column].Value.ToString().Length > 54)
                    {
                        worksheet.Rows[row].Cells[column].CellFormat.Font.Name = "Times New Roman";
                        worksheet.Rows[row].Cells[column].CellFormat.Font.Height = 8 * 20;
                    }
                    else
                    {
                        worksheet.Rows[row].Cells[column].CellFormat.Font.Name = "Times New Roman";
                        worksheet.Rows[row].Cells[column].CellFormat.Font.Height = 10 * 20;
                    }
                    row++;
                    stt++;
                    #region OldCode
                    //int thuong1 = worksheet.Rows[row].Cells[column].Value.ToString().Length / 54;
                    //int sodu1 = worksheet.Rows[row].Cells[column].Value.ToString().Length % 54;
                    //if (thuong1 == 0)
                    //{
                    //    Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row, column + 10);
                    //    mergedCellRegion1.Value = ndDieuChinhTKChiTiet.NoiDungTKChinh;
                    //    worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                    //    mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                    //    mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                    //}
                    //else
                    //{
                    //    if (sodu1 == 0)
                    //    {
                    //        Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row + thuong1 - 1, column + 10);
                    //        mergedCellRegion1.Value = ndDieuChinhTKChiTiet.NoiDungTKChinh;
                    //        worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                    //        mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                    //        mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                    //    }
                    //    else
                    //        {
                    //            thuong1 += 1;
                    //            Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row + thuong1 - 1, column + 10);
                    //            mergedCellRegion1.Value = ndDieuChinhTKChiTiet.NoiDungTKChinh;
                    //            worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                    //            mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                    //            mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                    //        }
                    //    }

                    //    if (thuong1 == 0)
                    //    {
                    //        row += 1;
                    //        j++;
                    //    }
                    //    else
                    //    {
                    //        row += thuong1;
                    //        j++;
                    //    }
                    #endregion
                }
            }
        }

        private void fillNoiDungTKSua(Worksheet worksheet, int row, int column, int idsdc, string ts)
        {
            ListNoiDungEditTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(idsdc);
            //ListNoiDungEditTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(idSDC);
            int begin = (Convert.ToInt32(ts) - 1) * soDongSuaDoi;
            int end = Convert.ToInt32(ts) * soDongSuaDoi;
            if (end > ListNoiDungEditTKDetail.Count) end = ListNoiDungEditTKDetail.Count;
            List<NoiDungDieuChinhTKDetail> ListInTKSuaDoiBoSung = new List<NoiDungDieuChinhTKDetail>();
            for (int i = begin; i < end; i++)
            {
                ListInTKSuaDoiBoSung.Add(ListNoiDungEditTKDetail[i]);
            }

            //int stt = 0;
            foreach (NoiDungDieuChinhTKDetail ndDieuChinhTKChiTiet in ListInTKSuaDoiBoSung)
            {
                if (row >= 13 && row <= 31)
                {
                    //worksheet.Rows[row].Cells[0].Value = ((stt + 1) + ((Convert.ToInt32(ts) - 1) * soDongSuaDoi)).ToString();

                    worksheet.Rows[row].Cells[column + 11].Value = ndDieuChinhTKChiTiet.NoiDungTKSua;
                    if (worksheet.Rows[row].Cells[column + 11].Value.ToString().Length > 54)
                    {
                        worksheet.Rows[row].Cells[column + 11].CellFormat.Font.Name = "Times New Roman";
                        worksheet.Rows[row].Cells[column + 11].CellFormat.Font.Height = 8 * 20;
                    }
                    else
                    {
                        worksheet.Rows[row].Cells[column + 11].CellFormat.Font.Name = "Times New Roman";
                        worksheet.Rows[row].Cells[column + 11].CellFormat.Font.Height = 10 * 20;
                    }
                    row++;
                    #region OldCode
                    ////int thuong1 = worksheet.Rows[row].Cells[column + 11].Value.ToString().Length / 54;
                    ////int sodu1 = worksheet.Rows[row].Cells[column + 11].Value.ToString().Length % 54;
                    //if (thuong1 == 0)
                    //{
                    //    Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row, column + 20);
                    //    mergedCellRegion1.Value = ndDieuChinhTKChiTiet.NoiDungTKSua;
                    //    worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                    //    mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                    //    mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                    //}
                    //else
                    //{
                    //    if (sodu1 == 0)
                    //    {
                    //        Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row + thuong1 - 1, column + 20);
                    //        mergedCellRegion1.Value = ndDieuChinhTKChiTiet.NoiDungTKSua;
                    //        worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                    //        mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                    //        mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                    //    }
                    //    else
                    //    {
                    //        thuong1 += 1;
                    //        Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row + thuong1 - 1, column + 20);
                    //        mergedCellRegion1.Value = ndDieuChinhTKChiTiet.NoiDungTKSua;
                    //        worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                    //        mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                    //        mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                    //    }
                    //}

                    //if (thuong1 == 0)
                    //{
                    //    row += 1;
                    //    j++;
                    //}
                    //else
                    //{
                    //    row += thuong1;
                    //    j++;
                    //}
                    #endregion
                }
            }
        }

        private void cmbSoDieuChinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            IDSoDieuChinh = Convert.ToInt32(cmbSoDieuChinh.SelectedItem.Value);
            ListNoiDungEditTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(IDSoDieuChinh);
            if (ListNoiDungEditTKDetail.Count <= 19)
            {
                lblSoTrang.Visible = false;
                cboTrang.Visible = false;
                //index = 0;
            }
            else
            {
                lblSoTrang.Visible = true;
                cboTrang.Visible = true;
                if (ListNoiDungEditTKDetail.Count > 19)
                {
                    int count = (ListNoiDungEditTKDetail.Count - 1) / 19 + 1;
                    for (int i = 0; i < count; i++)
                        this.AddItemComboBox();
                }
                cboTrang.SelectedIndex = 0;
            }

        }
    }
}
