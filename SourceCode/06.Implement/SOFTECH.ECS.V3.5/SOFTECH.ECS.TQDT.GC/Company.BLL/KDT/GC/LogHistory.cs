﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.QuanTri;
using Company.KDT.SHARE.Components;

namespace Company.GC.BLL.KDT.GC
{
   public partial class LogHistory
    {
       public KDT_SXXK_LogKhaiBao logKhaiBao = new KDT_SXXK_LogKhaiBao();
       public KDT_GC_DinhMuc_Log log = new KDT_GC_DinhMuc_Log();
       public KDT_GC_LoaiPhuKien_Log logLoaiPhuKien = new KDT_GC_LoaiPhuKien_Log();
       public KDT_GC_HangPhuKien_Log logHPK = new KDT_GC_HangPhuKien_Log();

       #region Properties.
       public string Status { set; get; }
       public string Notes { set; get; }
       public string UserName { set; get; }
       public DinhMucCollection DMCollectionLog = new DinhMucCollection();
       #endregion
       public void LogDinhMuc(DinhMucDangKy dmDangKy, String Notes , String UserName)
       {
           try
           {
               logKhaiBao = new KDT_SXXK_LogKhaiBao();
               logKhaiBao.ID_DK = dmDangKy.ID;
               logKhaiBao.GUIDSTR_DK = dmDangKy.GUIDSTR;
               logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.DinhMuc;
               logKhaiBao.UserNameKhaiBao = UserName;
               logKhaiBao.NgayKhaiBao = DateTime.Now;
               logKhaiBao.UserNameSuaDoi = UserName;
               logKhaiBao.NgaySuaDoi = DateTime.Now;
               logKhaiBao.GhiChu = Notes;
               foreach (DinhMuc item in dmDangKy.DMCollection)
               {
                   log = new KDT_GC_DinhMuc_Log();
                   log.MaSanPham = item.MaSanPham;
                   log.MaNguyenPhuLieu = item.MaNguyenPhuLieu;
                   log.DVT_ID = item.DVT_ID;
                   log.DinhMucSuDung = item.DinhMucSuDung;
                   log.TyLeHaoHut = item.TyLeHaoHut;
                   log.GhiChu = item.GhiChu;
                   log.STTHang = item.STTHang;
                   log.NPL_TuCungUng = item.NPL_TuCungUng;
                   log.TenSanPham = item.TenSanPham;
                   log.TenNPL = item.TenNPL;
                   log.MaDDSX = item.MaDDSX;
                   log.DateLog = DateTime.Now;
                   log.Status = Notes;
                   logKhaiBao.DMCollection.Add(log);
               }
               logKhaiBao.InsertUpdateFull();
           }
           catch (Exception ex)
           {
               Logger.LocalLogger.Instance().WriteMessage(ex);
           }
       }
       public void LogDinhMuc(DinhMucDangKy dmDangKy,DinhMucCollection DMCollectionLog ,String Notes, String UserName)
       {
           try
           {
               logKhaiBao = new KDT_SXXK_LogKhaiBao();
               logKhaiBao.ID_DK = dmDangKy.ID;
               logKhaiBao.GUIDSTR_DK = dmDangKy.GUIDSTR;
               logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.DinhMuc;
               logKhaiBao.UserNameKhaiBao = UserName;
               logKhaiBao.NgayKhaiBao = DateTime.Now;
               logKhaiBao.UserNameSuaDoi = UserName;
               logKhaiBao.NgaySuaDoi = DateTime.Now;
               logKhaiBao.GhiChu = Notes;
               foreach (DinhMuc item in DMCollectionLog)
               {
                   log = new KDT_GC_DinhMuc_Log();
                   log.MaSanPham = item.MaSanPham;
                   log.MaNguyenPhuLieu = item.MaNguyenPhuLieu;
                   log.DVT_ID = item.DVT_ID;
                   log.DinhMucSuDung = item.DinhMucSuDung;
                   log.TyLeHaoHut = item.TyLeHaoHut;
                   log.GhiChu = item.GhiChu;
                   log.STTHang = item.STTHang;
                   log.NPL_TuCungUng = item.NPL_TuCungUng;
                   log.TenSanPham = item.TenSanPham;
                   log.TenNPL = item.TenNPL;
                   log.MaDDSX = item.MaDDSX;
                   log.DateLog = DateTime.Now;
                   log.Status = Notes;
                   logKhaiBao.DMCollection.Add(log);
               }
               logKhaiBao.InsertUpdateFull();
           }
           catch (Exception ex)
           {
               Logger.LocalLogger.Instance().WriteMessage(ex);
           }
       }

       public void LogPhuKien(PhuKienDangKy pkDangKy, String Notes, String UserName)
       {
           try
           {
               logKhaiBao = new KDT_SXXK_LogKhaiBao();
               logKhaiBao.ID_DK = pkDangKy.ID;
               logKhaiBao.GUIDSTR_DK = pkDangKy.GUIDSTR;
               logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.PhuKien;
               logKhaiBao.UserNameKhaiBao = UserName;
               logKhaiBao.NgayKhaiBao = DateTime.Now;
               logKhaiBao.UserNameSuaDoi = UserName;
               logKhaiBao.NgaySuaDoi = DateTime.Now;
               logKhaiBao.GhiChu = Notes;
               foreach (LoaiPhuKien item in pkDangKy.PKCollection)
               {
                   logLoaiPhuKien = new KDT_GC_LoaiPhuKien_Log();
                   logLoaiPhuKien.MaPhuKien = item.MaPhuKien;
                   logLoaiPhuKien.NoiDung = item.NoiDung;
                   logLoaiPhuKien.ThongTinCu = item.ThongTinCu;
                   logLoaiPhuKien.ThongTinMoi = item.ThongTinMoi;
                   logLoaiPhuKien.Status = item.NoiDung;
                   logLoaiPhuKien.DateLog = DateTime.Now;
                   if (item.HPKCollection.Count==0)
                   {
                       HangPhuKienCollection HPKCollection = new HangPhuKienCollection();
                       HangPhuKien hang = new HangPhuKien();
                       hang.Master_ID = item.ID;
                       HPKCollection = hang.SelectCollectionBy_Master_ID();
                       item.HPKCollection = HPKCollection;
                   }
                   foreach (HangPhuKien ite in item.HPKCollection)
                   {
                       logHPK = new KDT_GC_HangPhuKien_Log();
                       logHPK.MaHang = ite.MaHang;
                       logHPK.TenHang = ite.TenHang;
                       logHPK.MaHS = ite.MaHS;
                       logHPK.ThongTinCu = ite.ThongTinCu;
                       logHPK.SoLuong = ite.SoLuong;
                       logHPK.DonGia = ite.DonGia;
                       logHPK.DVT_ID = ite.DVT_ID;
                       logHPK.NuocXX_ID = ite.NuocXX_ID;
                       logHPK.TriGia = ite.TriGia;
                       logHPK.NguyenTe_ID = ite.NguyenTe_ID;
                       logHPK.STTHang = ite.STTHang;
                       logHPK.TinhTrang = ite.TinhTrang;
                       logHPK.NhomSP = ite.NhomSP;
                       logHPK.Status = item.NoiDung;
                       logHPK.DateLog = DateTime.Now;
                       logLoaiPhuKien.HPKCollection.Add(logHPK);
                   }
                   logKhaiBao.LoaiPKCollection.Add(logLoaiPhuKien);
               }
               logKhaiBao.InsertUpdateFullPhuKien();
           }
           catch (Exception ex)
           {
               Logger.LocalLogger.Instance().WriteMessage(ex);
           }
       }
       public void LogPhuKien(PhuKienDangKy pkDangKy, LoaiPhuKienCollection PKCollection, String Notes, String UserName)
       {
           try
           {
               logKhaiBao = new KDT_SXXK_LogKhaiBao();
               logKhaiBao.ID_DK = pkDangKy.ID;
               logKhaiBao.GUIDSTR_DK = pkDangKy.GUIDSTR;
               logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.PhuKien;
               logKhaiBao.UserNameKhaiBao = UserName;
               logKhaiBao.NgayKhaiBao = DateTime.Now;
               logKhaiBao.UserNameSuaDoi = UserName;
               logKhaiBao.NgaySuaDoi = DateTime.Now;
               logKhaiBao.GhiChu = Notes;
               foreach (LoaiPhuKien item in PKCollection)
               {
                   logLoaiPhuKien = new KDT_GC_LoaiPhuKien_Log();
                   logLoaiPhuKien.MaPhuKien = item.MaPhuKien;
                   logLoaiPhuKien.NoiDung = item.NoiDung;
                   logLoaiPhuKien.ThongTinCu = item.ThongTinCu;
                   logLoaiPhuKien.ThongTinMoi = item.ThongTinMoi;
                   logLoaiPhuKien.Status = item.NoiDung;
                   logLoaiPhuKien.DateLog = DateTime.Now;
                   foreach (HangPhuKien ite in item.HPKCollection)
                   {
                       logHPK = new KDT_GC_HangPhuKien_Log();
                       logHPK.MaHang = ite.MaHang;
                       logHPK.TenHang = ite.TenHang;
                       logHPK.MaHS = ite.MaHS;
                       logHPK.ThongTinCu = ite.ThongTinCu;
                       logHPK.SoLuong = ite.SoLuong;
                       logHPK.DonGia = ite.DonGia;
                       logHPK.DVT_ID = ite.DVT_ID;
                       logHPK.NuocXX_ID = ite.NuocXX_ID;
                       logHPK.TriGia = ite.TriGia;
                       logHPK.NguyenTe_ID = ite.NguyenTe_ID;
                       logHPK.STTHang = ite.STTHang;
                       logHPK.TinhTrang = ite.TinhTrang;
                       logHPK.NhomSP = ite.NhomSP;
                       logHPK.Status = item.NoiDung;
                       logHPK.DateLog = DateTime.Now;
                       logLoaiPhuKien.HPKCollection.Add(logHPK);
                   }
                   logKhaiBao.LoaiPKCollection.Add(logLoaiPhuKien);
               }
               logKhaiBao.InsertUpdateFullPhuKien();
           }
           catch (Exception ex)
           {
               Logger.LocalLogger.Instance().WriteMessage(ex);
           }
       }

       public void LogHDGC(HopDong HD, String Notes, String UserName)
       {
           try
           {
               logKhaiBao = new KDT_SXXK_LogKhaiBao();
               logKhaiBao.ID_DK = HD.ID;
               logKhaiBao.GUIDSTR_DK = HD.GUIDSTR;
               logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.HopDong;
               logKhaiBao.UserNameKhaiBao = UserName;
               logKhaiBao.NgayKhaiBao = DateTime.Now;
               logKhaiBao.UserNameSuaDoi = UserName;
               logKhaiBao.NgaySuaDoi = DateTime.Now;
               logKhaiBao.GhiChu = Notes;
               HD.LoadCollection();
               foreach (NhomSanPham item in HD.NhomSPCollection)
               {
                   KDT_GC_NhomSanPham_Log entity = new KDT_GC_NhomSanPham_Log();
                   entity.MaSanPham = item.MaSanPham;
                   entity.SoLuong = item.SoLuong;
                   entity.GiaGiaCong = item.GiaGiaCong;
                   entity.STTHang = item.STTHang;
                   entity.TenSanPham = item.TenSanPham;
                   entity.TriGia = item.TriGia;
                   entity.Status = Notes;
                   entity.DateLog = DateTime.Now;
                   logKhaiBao.NhomSanPhamCollection.Add(entity);

               }
               foreach (NguyenPhuLieu item in HD.NPLCollection)
               {
                   KDT_GC_NguyenPhuLieu_Log entity = new KDT_GC_NguyenPhuLieu_Log();
                   entity.Ma = item.Ma;
                   entity.Ten = item.Ten;
                   entity.MaHS = item.MaHS;
                   entity.DVT_ID = item.DVT_ID;
                   entity.SoLuongDangKy = item.SoLuongDangKy;
                   entity.STTHang = item.STTHang;
                   entity.DonGia = item.DonGia;
                   entity.GhiChu = item.GhiChu;
                   logKhaiBao.NPLCollection.Add(entity);
               }
               foreach (SanPham item in HD.SPCollection)
               {
                   KDT_GC_SanPham_Log entity = new KDT_GC_SanPham_Log();
                   entity.Ma = item.Ma;
                   entity.Ten = item.Ten;
                   entity.MaHS = item.MaHS;
                   entity.DVT_ID = item.DVT_ID;
                   entity.SoLuongDangKy = item.SoLuongDangKy;
                   entity.NhomSanPham_ID = item.NhomSanPham_ID;
                   entity.STTHang = item.STTHang;
                   entity.DonGia = item.DonGia;
                   logKhaiBao.SanPhamCollection.Add(entity);
               }
               foreach (ThietBi item in HD.TBCollection)
               {
                   KDT_GC_ThietBi_Log entity = new KDT_GC_ThietBi_Log();
                   entity.Ma = item.Ma;
                   entity.Ten = item.Ten;
                   entity.MaHS = item.MaHS;
                   entity.DVT_ID = item.DVT_ID;
                   entity.SoLuongDangKy = item.SoLuongDangKy;
                   entity.NuocXX_ID = item.NuocXX_ID;
                   entity.DonGia = item.DonGia;
                   entity.TriGia = item.TriGia;
                   entity.NguyenTe_ID = item.NguyenTe_ID;
                   entity.STTHang = item.STTHang;
                   entity.TinhTrang = item.TinhTrang;
                   entity.GhiChu = item.GhiChu;
                   logKhaiBao.TBCollection.Add(entity);
               }
               foreach (HangMau item in HD.HangMauCollection)
               {
                   KDT_GC_HangMau_Log entity = new KDT_GC_HangMau_Log();
                   entity.Ma = item.Ma;
                   entity.Ten = item.Ten;
                   entity.MaHS = item.MaHS;
                   entity.DVT_ID = item.DVT_ID;
                   entity.SoLuongDangKy = item.SoLuongDangKy;
                   entity.STTHang = item.STTHang;
                   entity.GhiChu = item.GhiChu;
                   logKhaiBao.HMCollection.Add(entity);
               }
               logKhaiBao.InsertUpdateHDGC();
           }
           catch (Exception ex)
           {
               Logger.LocalLogger.Instance().WriteMessage(ex);
           }
       }
       public void LogHDGC(HopDong HD,List<NhomSanPham> NhomSPCollection, String Notes, String UserName)
       {
           try
           {
               logKhaiBao = new KDT_SXXK_LogKhaiBao();
               logKhaiBao.ID_DK = HD.ID;
               logKhaiBao.GUIDSTR_DK = HD.GUIDSTR;
               logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.HopDong;
               logKhaiBao.UserNameKhaiBao = UserName;
               logKhaiBao.NgayKhaiBao = DateTime.Now;
               logKhaiBao.UserNameSuaDoi = UserName;
               logKhaiBao.NgaySuaDoi = DateTime.Now;
               logKhaiBao.GhiChu = Notes;
               HD.LoadCollection();
               foreach (NhomSanPham item in NhomSPCollection)
               {
                   KDT_GC_NhomSanPham_Log entity = new KDT_GC_NhomSanPham_Log();
                   entity.MaSanPham = item.MaSanPham;
                   entity.SoLuong = item.SoLuong;
                   entity.GiaGiaCong = item.GiaGiaCong;
                   entity.STTHang = item.STTHang;
                   entity.TenSanPham = item.TenSanPham;
                   entity.TriGia = item.TriGia;
                   entity.Status = Notes;
                   entity.DateLog = DateTime.Now;
                   logKhaiBao.NhomSanPhamCollection.Add(entity);

               }
               logKhaiBao.InsertUpdateHDGC();
           }
           catch (Exception ex)
           {
               Logger.LocalLogger.Instance().WriteMessage(ex);
           }
       }
       public void LogHDGC(HopDong HD, SanPhamCollection SPCollection, String Notes, String UserName)
       {
           try
           {
               logKhaiBao = new KDT_SXXK_LogKhaiBao();
               logKhaiBao.ID_DK = HD.ID;
               logKhaiBao.GUIDSTR_DK = HD.GUIDSTR;
               logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.HopDong;
               logKhaiBao.UserNameKhaiBao = UserName;
               logKhaiBao.NgayKhaiBao = DateTime.Now;
               logKhaiBao.UserNameSuaDoi = UserName;
               logKhaiBao.NgaySuaDoi = DateTime.Now;
               logKhaiBao.GhiChu = Notes;
               foreach (SanPham item in SPCollection)
               {
                   KDT_GC_SanPham_Log entity = new KDT_GC_SanPham_Log();
                   entity.Ma = item.Ma;
                   entity.Ten = item.Ten;
                   entity.MaHS = item.MaHS;
                   entity.DVT_ID = item.DVT_ID;
                   entity.SoLuongDangKy = item.SoLuongDangKy;
                   entity.NhomSanPham_ID = item.NhomSanPham_ID;
                   entity.STTHang = item.STTHang;
                   entity.DonGia = item.DonGia;
                   logKhaiBao.SanPhamCollection.Add(entity);
               }
               logKhaiBao.InsertUpdateHDGC();
           }
           catch (Exception ex)
           {
               Logger.LocalLogger.Instance().WriteMessage(ex);
           }
       }
       public void LogHDGC(HopDong HD, List<NguyenPhuLieu> NPLCollection , String Notes, String UserName)
       {
           try
           {
               logKhaiBao = new KDT_SXXK_LogKhaiBao();
               logKhaiBao.ID_DK = HD.ID;
               logKhaiBao.GUIDSTR_DK = HD.GUIDSTR;
               logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.HopDong;
               logKhaiBao.UserNameKhaiBao = UserName;
               logKhaiBao.NgayKhaiBao = DateTime.Now;
               logKhaiBao.UserNameSuaDoi = UserName;
               logKhaiBao.NgaySuaDoi = DateTime.Now;
               logKhaiBao.GhiChu = Notes;
               foreach (NguyenPhuLieu item in NPLCollection)
               {
                   KDT_GC_NguyenPhuLieu_Log entity = new KDT_GC_NguyenPhuLieu_Log();
                   entity.Ma = item.Ma;
                   entity.Ten = item.Ten;
                   entity.MaHS = item.MaHS;
                   entity.DVT_ID = item.DVT_ID;
                   entity.SoLuongDangKy = item.SoLuongDangKy;
                   entity.STTHang = item.STTHang;
                   entity.DonGia = item.DonGia;
                   entity.GhiChu = item.GhiChu;
                   logKhaiBao.NPLCollection.Add(entity);
               }
               logKhaiBao.InsertUpdateHDGC();
           }
           catch (Exception ex)
           {
               Logger.LocalLogger.Instance().WriteMessage(ex);
           }
       }
       public void LogHDGC(HopDong HD, List<ThietBi> TBCollection, String Notes, String UserName)
       {
           try
           {
               logKhaiBao = new KDT_SXXK_LogKhaiBao();
               logKhaiBao.ID_DK = HD.ID;
               logKhaiBao.GUIDSTR_DK = HD.GUIDSTR;
               logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.HopDong;
               logKhaiBao.UserNameKhaiBao = UserName;
               logKhaiBao.NgayKhaiBao = DateTime.Now;
               logKhaiBao.UserNameSuaDoi = UserName;
               logKhaiBao.NgaySuaDoi = DateTime.Now;
               logKhaiBao.GhiChu = Notes;
               foreach (ThietBi item in TBCollection)
               {
                   KDT_GC_ThietBi_Log entity = new KDT_GC_ThietBi_Log();
                   entity.Ma = item.Ma;
                   entity.Ten = item.Ten;
                   entity.MaHS = item.MaHS;
                   entity.DVT_ID = item.DVT_ID;
                   entity.SoLuongDangKy = item.SoLuongDangKy;
                   entity.NuocXX_ID = item.NuocXX_ID;
                   entity.DonGia = item.DonGia;
                   entity.TriGia = item.TriGia;
                   entity.NguyenTe_ID = item.NguyenTe_ID;
                   entity.STTHang = item.STTHang;
                   entity.TinhTrang = item.TinhTrang;
                   entity.GhiChu = item.GhiChu;
                   logKhaiBao.TBCollection.Add(entity);
               }
               logKhaiBao.InsertUpdateHDGC();
           }
           catch (Exception ex)
           {
               Logger.LocalLogger.Instance().WriteMessage(ex);
           }
       }
       public void LogHDGC(HopDong HD, List<HangMau> HMCollection, String Notes, String UserName)
       {
           try
           {
               logKhaiBao = new KDT_SXXK_LogKhaiBao();
               logKhaiBao.ID_DK = HD.ID;
               logKhaiBao.GUIDSTR_DK = HD.GUIDSTR;
               logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.HopDong;
               logKhaiBao.UserNameKhaiBao = UserName;
               logKhaiBao.NgayKhaiBao = DateTime.Now;
               logKhaiBao.UserNameSuaDoi = UserName;
               logKhaiBao.NgaySuaDoi = DateTime.Now;
               logKhaiBao.GhiChu = Notes;
               foreach (HangMau item in HMCollection)
               {
                   KDT_GC_HangMau_Log entity = new KDT_GC_HangMau_Log();
                   entity.Ma = item.Ma;
                   entity.Ten = item.Ten;
                   entity.MaHS = item.MaHS;
                   entity.DVT_ID = item.DVT_ID;
                   entity.SoLuongDangKy = item.SoLuongDangKy;
                   entity.STTHang = item.STTHang;
                   entity.GhiChu = item.GhiChu;
                   logKhaiBao.HMCollection.Add(entity);
               }
               logKhaiBao.InsertUpdateHDGC();
           }
           catch (Exception ex)
           {
               Logger.LocalLogger.Instance().WriteMessage(ex);
           }
       }

    }
}
