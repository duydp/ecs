﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.BLL.SXXK.ThanhKhoan;
using System.Collections;
using System.Collections.Generic;
namespace Company.BLL.SXXK.ToKhai
{
    public partial class ToKhaiMauDich
    {
        private List<PhanBoToKhaiXuat> _pbToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
        public decimal SoToKhaiVNACCS { get; set; }
        public List<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection
        {
            set { _pbToKhaiXuatCollection = value; }
            get { return _pbToKhaiXuatCollection; }
        }
        public Company.KDT.SHARE.VNACCS.CapSoToKhai TrangThaiVNACCS { get; set; }
        //Hungtq updated 03/03/2012.
        /// <summary>
        /// Lay danh sach to khai nhap khau. Bo sung them thong tin Phan luong.
        /// </summary>
        /// <param name="maDoanhNghiep"></param>
        /// <param name="maHaiQuan"></param>
        /// <param name="minDate"></param>
        /// <param name="maxDate"></param>
        /// <param name="userName"></param>
        /// <param name="tenChuHang"></param>
        /// <returns></returns>
        public ToKhaiMauDichCollection GetDSTKNChuaThanhLyByDate_PhanLuong(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate, string userName, string tenChuHang)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKNChuaThanhLyByDate(maDoanhNghiep, maHaiQuan, minDate, maxDate, userName, tenChuHang);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();


                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVNACCS"))) entity.SoToKhaiVNACCS = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVNACCS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                }
                catch (Exception ex)
                {

                    //throw;
                }
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //Hungtq updated 27/07/2012.
        //Lay danh sach to khai nhap khau khong the tham gia thanh khoan.
        public ToKhaiMauDichCollection GetDSTKN_KhongTheThanhKhoan_ByDate_PhanLuong(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate, string userName, string tenChuHang)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKNKhongTheThanhKhoanByDate(maDoanhNghiep, maHaiQuan, minDate, maxDate, userName, tenChuHang);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVNACCS"))) entity.SoToKhaiVNACCS = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVNACCS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //Hungtq updated 03/03/2012.
        /// <summary>
        /// Lay danh sach to khai xuat khau. Bo sung them thong tin Phan luong.
        /// </summary>
        /// <param name="maDoanhNghiep"></param>
        /// <param name="maHaiQuan"></param>
        /// <param name="minDate"></param>
        /// <param name="maxDate"></param>
        /// <returns></returns>
        public ToKhaiMauDichCollection GetDSTKXChuaThanhLyByDate_PhanLuong(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXChuaThanhLyByDate(maDoanhNghiep, maHaiQuan, minDate, maxDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVNACCS"))) entity.SoToKhaiVNACCS = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVNACCS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVNACCS"))) entity.SoToKhaiVNACCS = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVNACCS"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));

                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public ToKhaiMauDichCollection GetDSTKXChuaThanhLyByDate_PhanLuong_DungChoCTTT(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXChuaThanhLyByDate_DungChoCTTT(maDoanhNghiep, maHaiQuan, minDate, maxDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVNACCS"))) entity.SoToKhaiVNACCS = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVNACCS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public DataTable GetDSTK_DungChoCTTT(string MaDoanhNghiep, string maHaiQuan, DateTime FromDate, DateTime ToDate, bool IsTKNhap)
        {
            string sql = string.Format(@"SELECT CASE WHEN c.MaLoaiHinh like '%V%' then (Select top 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai  where SoTK = c.SoToKhai) Else c.SoToKhai End as SoToKhaiVNACCS, c.SoToKhai,c.MaLoaiHinh,c.NgayDangKy,c.MaHaiQuan, TongTriGia AS TongTriGia,
case
 when SUM(d.TriGia) is NOT NULL THEN SUM(d.TriGia)
 ELSE 0
 END AS GiaTriDaPhanBo ,
 CASE 
 WHEN SUM(d.TriGia) is NOT NULL THEN TongTriGia - SUM(d.TriGia) 
 ELSE TongTriGia 
 end AS GiaTriConLai 
 
  FROM
(SELECT a.SoToKhai,a.MaHaiQuan,a.MaLoaiHinh,a.NamDangKy, a.NgayDangKy, SUM(b.TriGiaKB) AS TongTriGia
from t_sxxk_tokhaimaudich a INNER JOIN t_SXXK_HangMauDich b
ON a.SoToKhai = B.SoToKhai AND A.NamDangKy = B.NamDangKy AND A.MaHaiQuan = B.MaHaiQuan AND a.MaLoaiHinh = b.MaLoaiHinh
where  a.MaDoanhNghiep = @MaDoanhNghiep
 and a.MaHaiQuan = @MaHaiQuan AND (a.NgayDangKy between (@FromDate) and (@ToDate) ) AND a.MaLoaiHinh LIKE '{0}%'
 GROUP BY a.SoToKhai,a.MaLoaiHinh,a.NamDangKy,a.MaHaiQuan,a.NgayDangKy
) AS c
 LEFT JOIN t_CTTT_ChungTuChiTiet d ON  c.SoToKhai = d.SoToKhai AND c.MaLoaiHinh = d.MaLoaiHinh
AND year(c.NgayDangKy) = year(d.NgayDangKy)
GROUP BY c.SoToKhai,c.MaLoaiHinh,c.NgayDangKy,TongTriGia,c.MaHaiQuan ORDER BY c.NgayDangKy", IsTKNhap ? "N" : "X");
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            dbCommand.CommandTimeout = 1800;
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@FromDate", SqlDbType.DateTime, FromDate);
            this.db.AddInParameter(dbCommand, "@ToDate", SqlDbType.DateTime, ToDate);
            return db.ExecuteDataSet(dbCommand).Tables[0];
            //IDataReader reader = db.ExecuteReader(dbCommand);
            //ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            //while (reader.Read())
            //{
            //    ToKhaiMauDich entity = new ToKhaiMauDich();

            //    if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
            //    if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
            //    if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
            //    if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
            //    if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
            //    if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
            //    if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
            //    if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
            //    collection.Add(entity);
            //}
            //reader.Close();
            //return collection;
        }
        private IDataReader SelectReaderDSTKXChuaThanhLyByDate_DungChoCTTT(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            string spName = "p_SXXK_GetDSTKXChuaThanhLyDate_DungChoCTTT";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 600;

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, minDate);
            this.db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, maxDate);

            return this.db.ExecuteReader(dbCommand);
        }

        //Hungtq updated 30/07/2012.
        //Lay danh sach to khai xuat khau khong the tham gia thanh khoan.
        public ToKhaiMauDichCollection GetDSTKX_KhongTheThanhKhoan_ByDate_PhanLuong(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXKhongTheThanhKhoanByDate(maDoanhNghiep, maHaiQuan, minDate, maxDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVNACCS"))) entity.SoToKhaiVNACCS = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVNACCS"));
                //minhnd
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
                

                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public bool IsToKhaiDaChayThanhKhoanVaChuaDongHoSo()
        {
            return TheoDoiTrangThaiThanhKhoanTKN(this.MaDoanhNghiep, this.SoToKhai, this.NgayDangKy.Year, Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaChayThanhKhoan, this.MaLoaiHinh);
        }

        public bool IsToKhaiDaThanhKhoanVaDongHoSo()
        {
            return TheoDoiTrangThaiThanhKhoanTKN(this.MaDoanhNghiep, this.SoToKhai, this.NgayDangKy.Year, Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo, this.MaLoaiHinh);
        }

        public bool IsToKhaiXuatDaThanhKhoanVaDongHoSo()
        {
            return TheoDoiTrangThaiThanhKhoanTKX_DongHoSo(this.MaDoanhNghiep, this.SoToKhai, this.NgayDangKy.Year);
        }

        public bool IsToKhaiDaThanhKhoanVaDongHoSo(SqlTransaction transaction, SqlDatabase db)
        {
            return TheoDoiTrangThaiThanhKhoanTKN(transaction, db, this.MaDoanhNghiep, this.SoToKhai, this.NgayDangKy.Year, Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo);
        }

        private bool TheoDoiTrangThaiThanhKhoanTKN(string maDoanhNghiep, long soToKhai, int namDangKy, Company.KDT.SHARE.Components.TrangThaiThanhKhoan trangThaiThanhKhoan, string MaLoaiHinh)
        {
            string spName = "";

            if (trangThaiThanhKhoan == Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo)
                spName = "select dbo.f_KDT_ToKhaimauDich_TheoDoiThanhKhoan_DongHoSo(@MaDoanhNghiep,@SoToKhai,@NamDangKy,@MaLoaiHinh)";
            else
                spName = "select  f_KDT_ToKhaimauDich_TheoDoiThanhKhoan_ThamGiaThanhKhoan(@MaDoanhNghiep,@SoToKhai,@NamDangKy)";

            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, soToKhai);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, namDangKy);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, MaLoaiHinh);
            object result = db.ExecuteScalar(dbCommand);

            return result != null ? (System.Convert.ToInt32(result) > 0 ? true : false) : false;
        }

        private bool TheoDoiTrangThaiThanhKhoanTKN(SqlTransaction transaction, SqlDatabase db, string maDoanhNghiep, long soToKhai, int namDangKy, Company.KDT.SHARE.Components.TrangThaiThanhKhoan trangThaiThanhKhoan)
        {
            string spName = "";

            if (trangThaiThanhKhoan == Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo)
                spName = "f_KDT_ToKhaimauDich_TheoDoiThanhKhoan_DongHoSo";
            else
                spName = "f_KDT_ToKhaimauDich_TheoDoiThanhKhoan_ThamGiaThanhKhoan";

            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, soToKhai);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, namDangKy);

            object result = db.ExecuteScalar(dbCommand);

            return result != null ? (System.Convert.ToInt32(result) > 0 ? true : false) : false;
        }

        private bool TheoDoiTrangThaiThanhKhoanTKX_DongHoSo(string maDoanhNghiep, long soToKhai, int namDangKy)
        {
            string spName = "p_KDT_ToKhaimauDich_TheoDoiThanhKhoanTKX_DongHoSo";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, soToKhai);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, namDangKy);

            object result = db.ExecuteScalar(dbCommand);

            return result != null ? (System.Convert.ToInt32(result) > 0 ? true : false) : false;
        }

        public static void NhapDuLieuXML(ToKhaiMauDichCollection tkmdCollection)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (ToKhaiMauDich tkmd in tkmdCollection)
                    {
                        tkmd.TrangThai = 1;
                        // tkmd.InsertUpdateFull( ,transaction,tkmdCollection );
                        tkmd.InsertUpdateFull(transaction);
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }


        }

        public void InsertUpdateFull(SqlTransaction transaction)
        {
            this.InsertUpdate();
            this.TrangThaiVNACCS.InsertUpdate(transaction);
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                hmd.InsertUpdateTransaction(transaction);
                if (this.MaLoaiHinh.StartsWith("N"))
                {
                    BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon = new NPLNhapTon();
                    nplTon.MaHaiQuan = this.MaHaiQuan;
                    nplTon.MaNPL = hmd.MaPhu; ;
                    nplTon.SoToKhai = this.SoToKhai;
                    nplTon.MaLoaiHinh = this.MaLoaiHinh;
                    nplTon.NamDangKy = this.NamDangKy;
                    nplTon.MaDoanhNghiep = this.MaDoanhNghiep;
                    nplTon.Ton = hmd.SoLuong;
                    nplTon.Luong = hmd.SoLuong;
                    nplTon.ThueXNK = (double)hmd.ThueXNK;
                    nplTon.ThueVAT = (double)hmd.ThueGTGT;
                    nplTon.ThueTTDB = (double)hmd.ThueTTDB;
                    nplTon.TenNPL = hmd.TenHang;
                    nplTon.PhuThu = (double)hmd.PhuThu;
                    nplTon.ThueCLGia = (double)hmd.TriGiaThuKhac;
                    nplTon.ThueXNKTon = nplTon.ThueXNK;
                    nplTon.InsertUpdateTransaction(transaction);

                    BLL.SXXK.NPLNhapTonThucTe nplTonTT = new NPLNhapTonThucTe();
                    nplTonTT.MaHaiQuan = this.MaHaiQuan;
                    nplTonTT.MaNPL = hmd.MaPhu; ;
                    nplTonTT.SoToKhai = this.SoToKhai;
                    nplTonTT.MaLoaiHinh = this.MaLoaiHinh;
                    nplTonTT.NamDangKy = this.NamDangKy;
                    nplTonTT.MaDoanhNghiep = this.MaDoanhNghiep;
                    nplTonTT.Luong = hmd.SoLuong;
                    nplTonTT.Ton = nplTon.Ton;
                    nplTonTT.ThueXNK = (double)hmd.ThueXNK;
                    nplTonTT.ThueVAT = (double)hmd.ThueGTGT;
                    nplTonTT.ThueTTDB = (double)hmd.ThueTTDB;
                    nplTonTT.PhuThu = (double)hmd.PhuThu;
                    nplTonTT.ThueCLGia = (double)hmd.TriGiaThuKhac;
                    nplTonTT.ThueXNKTon = nplTon.ThueXNK;
                    nplTonTT.InsertUpdate(transaction);
                }
            }

        }

        public void InsertUpdateFull(SqlTransaction transaction, SqlDatabase db)
        {
            //To khai
            this.InsertUpdateTransaction(transaction, db);
            this.TrangThaiVNACCS.InsertUpdate(transaction, db);
            //Hang mau dich
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                hmd.InsertUpdateTransaction(transaction);

                if (this.MaLoaiHinh.StartsWith("N"))
                {
                    //Kiem tra TK nay da tham gia thanh khoan va da dong ho so chua?. Neu dong ho so -> khong duoc phep cap nhat lai thong tin so luong & ton.
                    if (!IsToKhaiDaThanhKhoanVaDongHoSo())
                    {
                        BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon = new NPLNhapTon();
                        nplTon.MaHaiQuan = this.MaHaiQuan;
                        nplTon.MaNPL = hmd.MaPhu; ;
                        nplTon.SoToKhai = this.SoToKhai;
                        nplTon.MaLoaiHinh = this.MaLoaiHinh;
                        nplTon.NamDangKy = this.NamDangKy;
                        nplTon.MaDoanhNghiep = this.MaDoanhNghiep;
                        nplTon.Ton = hmd.SoLuong;
                        nplTon.Luong = hmd.SoLuong;
                        nplTon.ThueXNK = (double)hmd.ThueXNK;
                        nplTon.ThueVAT = (double)hmd.ThueGTGT;
                        nplTon.ThueTTDB = (double)hmd.ThueTTDB;
                        nplTon.TenNPL = hmd.TenHang;
                        nplTon.PhuThu = (double)hmd.PhuThu;
                        nplTon.ThueCLGia = (double)hmd.TriGiaThuKhac;
                        nplTon.ThueXNKTon = nplTon.ThueXNK;
                        nplTon.InsertUpdateTransaction(transaction);

                        BLL.SXXK.NPLNhapTonThucTe nplTonTT = new NPLNhapTonThucTe();
                        nplTonTT.MaHaiQuan = this.MaHaiQuan;
                        nplTonTT.MaNPL = hmd.MaPhu; ;
                        nplTonTT.SoToKhai = this.SoToKhai;
                        nplTonTT.MaLoaiHinh = this.MaLoaiHinh;
                        nplTonTT.NamDangKy = this.NamDangKy;
                        nplTonTT.MaDoanhNghiep = this.MaDoanhNghiep;
                        nplTonTT.Luong = hmd.SoLuong;
                        nplTonTT.Ton = nplTon.Ton;
                        nplTonTT.ThueXNK = (double)hmd.ThueXNK;
                        nplTonTT.ThueVAT = (double)hmd.ThueGTGT;
                        nplTonTT.ThueTTDB = (double)hmd.ThueTTDB;
                        nplTonTT.PhuThu = (double)hmd.PhuThu;
                        nplTonTT.ThueCLGia = (double)hmd.TriGiaThuKhac;
                        nplTonTT.ThueXNKTon = nplTon.ThueXNK;
                        nplTonTT.InsertUpdate(transaction);
                    }
                }
            }

        }

        public void InsertUpdateFull(string connectionString)
        {
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    //To khai
                    this.InsertUpdateTransaction(transaction, db);
                    this.TrangThaiVNACCS.InsertUpdate(transaction, db);
                    //Hang mau dich
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.InsertUpdateTransaction(transaction, db);

                        if (this.MaLoaiHinh.StartsWith("N"))
                        {
                            //Kiem tra TK nay da tham gia thanh khoan va da dong ho so chua?. Neu dong ho so -> khong duoc phep cap nhat lai thong tin so luong & ton.
                            if (!IsToKhaiDaThanhKhoanVaDongHoSo(transaction, db))
                            {
                                BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon = new NPLNhapTon();
                                nplTon.MaHaiQuan = this.MaHaiQuan;
                                nplTon.MaNPL = hmd.MaPhu; ;
                                nplTon.SoToKhai = this.SoToKhai;
                                nplTon.MaLoaiHinh = this.MaLoaiHinh;
                                nplTon.NamDangKy = this.NamDangKy;
                                nplTon.MaDoanhNghiep = this.MaDoanhNghiep;
                                nplTon.Ton = hmd.SoLuong;
                                nplTon.Luong = hmd.SoLuong;
                                nplTon.ThueXNK = (double)hmd.ThueXNK;
                                nplTon.ThueVAT = (double)hmd.ThueGTGT;
                                nplTon.ThueTTDB = (double)hmd.ThueTTDB;
                                nplTon.TenNPL = hmd.TenHang;
                                nplTon.PhuThu = (double)hmd.PhuThu;
                                nplTon.ThueCLGia = (double)hmd.TriGiaThuKhac;
                                nplTon.ThueXNKTon = nplTon.ThueXNK;
                                nplTon.InsertUpdateTransaction(transaction, db);

                                BLL.SXXK.NPLNhapTonThucTe nplTonTT = new NPLNhapTonThucTe();
                                nplTonTT.MaHaiQuan = this.MaHaiQuan;
                                nplTonTT.MaNPL = hmd.MaPhu; ;
                                nplTonTT.SoToKhai = this.SoToKhai;
                                nplTonTT.MaLoaiHinh = this.MaLoaiHinh;
                                nplTonTT.NamDangKy = this.NamDangKy;
                                nplTonTT.MaDoanhNghiep = this.MaDoanhNghiep;
                                nplTonTT.Luong = hmd.SoLuong;
                                nplTonTT.Ton = nplTon.Ton;
                                nplTonTT.ThueXNK = (double)hmd.ThueXNK;
                                nplTonTT.ThueVAT = (double)hmd.ThueGTGT;
                                nplTonTT.ThueTTDB = (double)hmd.ThueTTDB;
                                nplTonTT.PhuThu = (double)hmd.PhuThu;
                                nplTonTT.ThueCLGia = (double)hmd.TriGiaThuKhac;
                                nplTonTT.ThueXNKTon = nplTon.ThueXNK;
                                nplTonTT.InsertUpdate(transaction, db);
                            }
                        }
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateFull(string connectionString, int cnt)
        {
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    //To khai
                    this.InsertUpdateTransaction(transaction, db);
                    this.TrangThaiVNACCS.InsertUpdate(transaction, db);
                    //Hang mau dich
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.InsertUpdateTransaction(transaction, db);

                        if (this.MaLoaiHinh.StartsWith("N"))
                        {
                            //Kiem tra TK nay da tham gia thanh khoan va da dong ho so chua?. Neu dong ho so -> khong duoc phep cap nhat lai thong tin so luong & ton.
                            if (!IsToKhaiDaThanhKhoanVaDongHoSo(transaction, db))
                            {
                                BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon = new NPLNhapTon();
                                nplTon.MaHaiQuan = this.MaHaiQuan;
                                nplTon.MaNPL = hmd.MaPhu; ;
                                nplTon.SoToKhai = this.SoToKhai;
                                nplTon.MaLoaiHinh = this.MaLoaiHinh;
                                nplTon.NamDangKy = this.NamDangKy;
                                nplTon.MaDoanhNghiep = this.MaDoanhNghiep;
                                nplTon.Ton = hmd.SoLuong;
                                nplTon.Luong = hmd.SoLuong;
                                nplTon.ThueXNK = (double)hmd.ThueXNK;
                                nplTon.ThueVAT = (double)hmd.ThueGTGT;
                                nplTon.ThueTTDB = (double)hmd.ThueTTDB;
                                nplTon.TenNPL = hmd.TenHang;
                                nplTon.PhuThu = (double)hmd.PhuThu;
                                nplTon.ThueCLGia = (double)hmd.TriGiaThuKhac;
                                nplTon.ThueXNKTon = nplTon.ThueXNK;
                                nplTon.InsertUpdateTransaction(transaction, db);

                                BLL.SXXK.NPLNhapTonThucTe nplTonTT = new NPLNhapTonThucTe();
                                nplTonTT.MaHaiQuan = this.MaHaiQuan;
                                nplTonTT.MaNPL = hmd.MaPhu; ;
                                nplTonTT.SoToKhai = this.SoToKhai;
                                nplTonTT.MaLoaiHinh = this.MaLoaiHinh;
                                nplTonTT.NamDangKy = this.NamDangKy;
                                nplTonTT.MaDoanhNghiep = this.MaDoanhNghiep;
                                nplTonTT.Luong = hmd.SoLuong;
                                nplTonTT.Ton = nplTon.Ton;
                                nplTonTT.ThueXNK = (double)hmd.ThueXNK;
                                nplTonTT.ThueVAT = (double)hmd.ThueGTGT;
                                nplTonTT.ThueTTDB = (double)hmd.ThueTTDB;
                                nplTonTT.PhuThu = (double)hmd.PhuThu;
                                nplTonTT.ThueCLGia = (double)hmd.TriGiaThuKhac;
                                nplTonTT.ThueXNKTon = nplTon.ThueXNK;
                                nplTonTT.InsertUpdate(transaction, db);
                            }
                        }
                    }

                    transaction.Commit();

                    //TODO: HungTQ, Update 27/04/2010.
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("{0}.SXXK To khai: MaHaiQuan={1}, MaDoanhNghiep={2}, SoToKhai={3}, NgayDangKy={4}, MaLoaiHinh={5}, NamDangKy={6}", cnt, this.MaHaiQuan, this.MaDoanhNghiep, this.SoToKhai, this.NgayDangKy.ToString("dd/MM/yyyy hh:mm:ss tt"), this.MaLoaiHinh, this.NamDangKy)));

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_ToKhaiMauDich_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, this._Xuat_NPL_SP);
            db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, this._ThanhLy);
            db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, this._NGAY_THN_THX);
            db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, this.TrangThaiThanhKhoan);
            db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this.ChungTu);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this.PhanLuong);
            db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, this.NgayHoanThanh);
            //Bổ sung thêm
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this.TrangThai);
            db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            db.AddInParameter(dbCommand, "@HeSoNhan", SqlDbType.Float, this._HeSoNhan);
            if (transaction != null)
            {
                return db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_ToKhaiMauDich_Update";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, this._Xuat_NPL_SP);
            db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, this._ThanhLy);
            db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, this._NGAY_THN_THX);
            db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, this.TrangThaiThanhKhoan);
            db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this.ChungTu);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this.PhanLuong);
            db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, this.NgayHoanThanh);
            //Bổ sung thêm
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this.TrangThai);
            db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            db.AddInParameter(dbCommand, "@HeSoNhan", SqlDbType.Float, this._HeSoNhan);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_ToKhaiMauDich_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, this._Xuat_NPL_SP);
            db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, this._ThanhLy);
            db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, this._NGAY_THN_THX);
            db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, this.TrangThaiThanhKhoan);
            db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this.ChungTu);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this.PhanLuong);
            db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, this.NgayHoanThanh);
            //Bổ sung thêm
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this.TrangThai);
            db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            db.AddInParameter(dbCommand, "@HeSoNhan", SqlDbType.Float, this._HeSoNhan);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public ToKhaiMauDichCollection SelectCollectionNhacNhoDynamic(string whereCondition, string orderByExpression)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public static void getDanhSachNhacNho()
        {
            string where = "";
        }
        public void UpdateMaHang(string maMoi, string maCu, decimal soluong)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string sql = "UPDATE t_SXXK_HangMauDich SET MaPhu = '" + maMoi + "' , SoLuong=@SoLuong WHERE " +
                                 "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                                 "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + this.MaHaiQuan + "' AND MaPhu = '" + maCu + "'";
                    SqlCommand sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
                    db.ExecuteNonQuery(sqlCmd, transaction);

                    sql = "UPDATE t_SXXK_HangMauDichDieuChinh SET Ma_NPL_SP = '" + maMoi + "', Luong=@SoLuong WHERE " +
                         "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                         "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + this.MaHaiQuan + "' AND Ma_NPL_SP = '" + maCu + "'";
                    sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
                    db.ExecuteNonQuery(sqlCmd, transaction);

                    sql = "UPDATE t_SXXK_ThanhLy_NPLNhapTon SET MaNPL = '" + maMoi + "' , Luong=@SoLuong WHERE " +
                          "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                          "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + this.MaHaiQuan + "' AND MaNPL = '" + maCu + "'";
                    sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
                    db.ExecuteNonQuery(sqlCmd, transaction);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void UpdateMaHang(string maMoi, string maCu, decimal soluong, SqlTransaction transaction)
        {
            string sql = "UPDATE t_SXXK_HangMauDich SET MaPhu = '" + maMoi + "' , SoLuong=@SoLuong WHERE " +
                         "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                         "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + this.MaHaiQuan + "' AND MaPhu = '" + maCu + "'";
            SqlCommand sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
            db.ExecuteNonQuery(sqlCmd, transaction);

            //sql = "UPDATE t_SXXK_HangMauDichDieuChinh SET Ma_NPL_SP = '" + maMoi + "', Luong=@SoLuong WHERE " +
            //     "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
            //     "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + this.MaHaiQuan + "' AND Ma_NPL_SP = '" + maCu + "'";
            //sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
            //db.ExecuteNonQuery(sqlCmd, transaction);

            sql = "UPDATE t_SXXK_ThanhLy_NPLNhapTon SET MaNPL = '" + maMoi + "' , Luong=@SoLuong WHERE " +
                  "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                  "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + this.MaHaiQuan + "' AND MaNPL = '" + maCu + "'";
            sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
            db.ExecuteNonQuery(sqlCmd, transaction);

            sql = "UPDATE t_SXXK_NPLNhapTonThucTe SET MaNPL = '" + maMoi + "', Luong=@SoLuong WHERE " +
                 "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                 "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + this.MaHaiQuan + "' AND MaNPL = '" + maCu + "'";
            sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(sqlCmd, "@SoLuong", DbType.Decimal, soluong);
            db.ExecuteNonQuery(sqlCmd, transaction);
        }
        public bool InsertUpdateFullHang()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction);
                    this.TrangThaiVNACCS.InsertUpdate(transaction, db);
                    HangMauDich hangDelete = new HangMauDich();
                    hangDelete.SoToKhai = this.SoToKhai;
                    hangDelete.MaLoaiHinh = this.MaLoaiHinh;
                    hangDelete.MaHaiQuan = this.MaHaiQuan;
                    hangDelete.NamDangKy = this.NamDangKy;
                    hangDelete.DeleteByForeigKeyTransaction(transaction);
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        if (hmd.MaPhu != hmd.MaTMP)
                            this.UpdateMaHang(hmd.MaPhu, hmd.MaTMP, hmd.SoLuong, transaction);
                        hmd.InsertTransaction(transaction);
                        if (this.MaLoaiHinh.StartsWith("N"))
                        {
                            int k = 0;
                            while (true)
                            {
                                HangMauDichDieuChinh hmdDC = new HangMauDichDieuChinh();
                                hmdDC.MaHaiQuan = this.MaHaiQuan;
                                hmdDC.SoToKhai = this.SoToKhai;
                                hmdDC.MaLoaiHinh = this.MaLoaiHinh;
                                hmdDC.NamDangKy = this.NamDangKy;
                                hmdDC.MA_NPL_SP = hmdDC.MaHang = hmd.MaPhu;
                                hmdDC.DGIA_KB = (double)hmd.DonGiaKB;
                                hmdDC.DGIA_TT = (double)hmd.DonGiaTT;
                                hmdDC.DVT_ID = hmd.DVT_ID;
                                hmdDC.MaHSKB = hmdDC.MaHS = hmd.MaHS;
                                hmdDC.NuocXX_ID = hmd.NuocXX_ID;
                                hmdDC.Luong = hmd.SoLuong;
                                hmdDC.TenHang = hmd.TenHang;
                                hmdDC.TS_VAT = hmd.ThueSuatGTGT;
                                hmdDC.TS_TTDB = hmd.ThueSuatTTDB;
                                hmdDC.TS_XNK = hmd.ThueSuatXNK;
                                hmdDC.TRIGIA_KB = (double)hmd.TriGiaKB;
                                hmdDC.TGKB_VND = (double)hmd.TriGiaKB_VND;
                                hmdDC.TRIGIA_TT = (double)hmd.TriGiaTT;
                                hmdDC.TYLE_THUKHAC = hmd.TyLeThuKhac;
                                hmdDC.DinhMuc = 0;
                                hmdDC.MA_DG = "HD";
                                hmdDC.LOAITSXNK = 0;
                                hmdDC.MA_THKE = "";
                                hmdDC.TL_QUYDOI = 0;
                                hmdDC.MIENTHUE = hmd.MienThue;
                                hmdDC.PHU_THU = (double)hmd.PhuThu;
                                hmdDC.THUE_VAT = (double)hmd.ThueGTGT;
                                hmdDC.THUE_XNK = (double)hmd.ThueXNK;
                                hmdDC.THUE_TTDB = (double)hmd.ThueTTDB;
                                hmdDC.TRIGIA_THUKHAC = (double)hmd.TriGiaThuKhac;
                                hmdDC.LanDieuChinh = k;
                                hmdDC.STTHang = hmd.SoThuTuHang;
                                if (hmdDC.UpdateTransaction(transaction) > 0) ++k;
                                else break;
                            }
                            //cap nhat bang nguyen phu lieu ton 
                            BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon = new NPLNhapTon();
                            nplTon.MaHaiQuan = this.MaHaiQuan;
                            nplTon.MaNPL = hmd.MaPhu; ;
                            nplTon.SoToKhai = this.SoToKhai;
                            nplTon.MaLoaiHinh = this.MaLoaiHinh;
                            nplTon.NamDangKy = this.NamDangKy;
                            nplTon.MaDoanhNghiep = this.MaDoanhNghiep; ;
                            nplTon.Load(transaction);
                            nplTon.Luong = hmd.SoLuong;
                            nplTon.Ton = hmd.Ton;
                            nplTon.ThueXNK = (double)hmd.ThueXNK;
                            nplTon.ThueVAT = (double)hmd.ThueGTGT;
                            nplTon.ThueTTDB = (double)hmd.ThueTTDB;
                            nplTon.TenNPL = hmd.TenHang;
                            nplTon.PhuThu = (double)hmd.PhuThu;
                            nplTon.ThueCLGia = (double)hmd.TriGiaThuKhac;
                            nplTon.InsertUpdateTransaction(transaction);
                        }
                    }


                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }
        public static long UpdateSTTHang(DataSet dsHang)
        {
            HangMauDich hmdIndatabase = new HangMauDich();
            DataSet dsHangInDatabase = hmdIndatabase.SelectAll();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (dsHang != null)
                        foreach (DataRow row in dsHang.Tables[0].Rows)
                        {
                            foreach (DataRow rowIndatabase in dsHangInDatabase.Tables[0].Rows)
                            {
                                if (row["MA_HQ"].ToString().Trim() == rowIndatabase["MaHaiQuan"].ToString().Trim() && row["MA_LH"].ToString().Trim() == rowIndatabase["MaLoaiHinh"].ToString().Trim()
                                    && row["NAMDK"].ToString().Trim() == rowIndatabase["NamDangKy"].ToString().Trim() && row["SOTK"].ToString() == rowIndatabase["SoToKhai"].ToString() &&
                                     row["MA_NPL_SP"].ToString().Trim().ToUpper() == rowIndatabase["MaPhu"].ToString().Trim().ToUpper())
                                {
                                    HangMauDich hmd = new HangMauDich();
                                    hmd.MaHaiQuan = row["MA_HQ"].ToString();
                                    hmd.MaLoaiHinh = row["MA_LH"].ToString();
                                    hmd.NamDangKy = (short)Convert.ToInt32(row["NAMDK"].ToString());
                                    hmd.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                                    hmd.MaPhu = row["MA_NPL_SP"].ToString();
                                    hmd.SoThuTuHang = Convert.ToInt16(row["STTHANG"]);
                                    hmd.UpdateSTTHangTransaction(transaction);
                                    break;
                                }
                            }
                        }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return 1;
        }

        public static long DongBoDuLieuHaiQuanToKhaiMoi(string mahaiquan, string madv, DataSet ds, DataSet dsHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            ToKhaiMauDich tkmdGet = new ToKhaiMauDich();
            DataSet dsToKhaiCu = tkmdGet.SelectDynamic("madoanhnghiep='" + madv + "' and mahaiquan='" + mahaiquan + "'", "");

            //if (ds.Tables[0].Rows.Count > 0)
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        if (ds != null)
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                                tkmd.MaLoaiHinh = Convert.ToString(row["Ma_LH"]);
                                tkmd.MaDaiLyTTHQ = Convert.ToString(row["MA_DVKT"]);
                                tkmd.MaHaiQuan = mahaiquan;
                                //tkmd.TenDaiLyTTHQ = Convert.ToString(row[0]["TenDLTTHQ"]);
                                tkmd.MaDonViUT = Convert.ToString(row["MA_DVUT"]);
                                tkmd.TenDonViDoiTac = Convert.ToString(row["DV_DT"]);
                                tkmd.SoGiayPhep = Convert.ToString(row["So_GP"]);
                                tkmd.SoToKhai = Convert.ToInt32(row["SOTK"]);
                                tkmd.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"]);
                                tkmd.NamDangKy = (short)tkmd.NgayDangKy.Year;
                                tkmd.MaDoanhNghiep = Convert.ToString(row["Ma_DV"]);
                                try
                                {
                                    tkmd.NGAY_THN_THX = Convert.ToDateTime(row["NGAY_THN_THX"]);
                                }
                                catch { tkmd.NGAY_THN_THX = new DateTime(1900, 1, 1); }
                                if (row["Ngay_GP"].ToString() != "")
                                    tkmd.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                                try
                                {
                                    if (row["Ngay_HHGP"].ToString() != "")
                                        tkmd.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                                }
                                catch (Exception exx) { }
                                tkmd.SoHopDong = Convert.ToString(row["So_HD"].ToString());
                                if (row["Ngay_HD"].ToString() != "")
                                    tkmd.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                                if (row["Ngay_HHHD"].ToString() != "")
                                    tkmd.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                                tkmd.SoHoaDonThuongMai = Convert.ToString(row["So_HDTM"]);
                                if (row["Ngay_HDTM"].ToString() != "")
                                    tkmd.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM".ToString()]);
                                tkmd.PTVT_ID = Convert.ToString(row["Ma_PTVT"]);
                                tkmd.SoHieuPTVT = Convert.ToString(row["Ten_PTVT"]);
                                if (row["NgayDen"].ToString() != "")
                                    tkmd.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                                tkmd.LoaiVanDon = Convert.ToString(row["Van_Don"]);
                                if (row["Ngay_VanDon"].ToString() != "")
                                    tkmd.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                                tkmd.NuocXK_ID = Convert.ToString(row["Nuoc_XK"]);
                                tkmd.NuocNK_ID = Convert.ToString(row["Nuoc_NK"]);
                                tkmd.DiaDiemXepHang = Convert.ToString(row["CangNN"]);
                                tkmd.DKGH_ID = Convert.ToString(row["Ma_GH"]);
                                tkmd.CuaKhau_ID = Convert.ToString(row["Ma_CK"]);
                                tkmd.NguyenTe_ID = Convert.ToString(row["Ma_NT"]);
                                tkmd.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                                tkmd.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                                tkmd.PTTT_ID = Convert.ToString(row["Ma_PTTT"]);
                                if (row["SoHang"].ToString() != "")
                                    tkmd.SoHang = Convert.ToInt16(row["SoHang"].ToString());
                                if (row["So_PLTK"].ToString() != "")
                                    tkmd.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                                tkmd.TenChuHang = Convert.ToString(row["TenCH"]);
                                if (row["So_Container"].ToString() != "")
                                    tkmd.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                                if (row["So_Container40"].ToString() != "")
                                    tkmd.SoContainer40 = Convert.ToDecimal(row["So_Container40"].ToString());
                                if (row["So_Kien"].ToString() != "")
                                    tkmd.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                                if (row["Tr_Luong"].ToString() != "")
                                    tkmd.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                                if (row["TongTGKB"].ToString() != "")
                                    tkmd.TongTriGiaKhaiBao = Convert.ToDecimal(row["TongTGKB"].ToString());
                                if (row["TongTGTT"].ToString() != "")
                                    tkmd.TongTriGiaTinhThue = Convert.ToDecimal(row["TongTGTT"]);
                                //tkmd.LoaiToKhaiGiaCong = Convert.ToString(row[0]["LoaiTKGC"]);

                                if (row["LePhi_HQ"].ToString() != "")
                                    tkmd.LePhiHaiQuan = Convert.ToDecimal(row["LePhi_HQ"].ToString());
                                if (row["Phi_BH"].ToString() != "")
                                    tkmd.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                                if (row["Phi_VC"].ToString() != "")
                                    tkmd.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                                tkmd.ThanhLy = row["THANH_LY"].ToString();
                                tkmd.Xuat_NPL_SP = row["XUAT_NPL_SP"].ToString();
                                tkmd.TrangThaiThanhKhoan = row["TTTK"].ToString();
                                tkmd.ChungTu = row["GIAYTO"].ToString();
                                if (row["NGAY_HOANTHANH"].ToString() != "")
                                    tkmd.NgayHoanThanh = Convert.ToDateTime(row["NGAY_HOANTHANH"].ToString());
                                try
                                {
                                    DataRow[] rowTTDM = dsToKhaiCu.Tables[0].Select("MaLoaiHinh='" + tkmd.MaLoaiHinh + "' and NamDangKy=" + tkmd.NamDangKy + " and MaHaiQuan='" + tkmd.MaHaiQuan + "' and  SoToKhai=" + tkmd.SoToKhai);
                                    if (rowTTDM == null || rowTTDM.Length == 0)
                                        tkmd.InsertTransaction(transaction);
                                }
                                catch { }
                            }
                        // xoa hang cu                        
                        //cap nnhat hang mau dich    
                        short stt = 1;
                        string mahq = "";
                        string maloaihinh = "";
                        short namdk = 0;
                        int sotk = 0;
                        if (dsHang != null)
                            foreach (DataRow row in dsHang.Tables[0].Rows)
                            {
                                HangMauDich hmd = new HangMauDich();
                                hmd.MaHaiQuan = row["MA_HQ"].ToString();
                                hmd.MaLoaiHinh = row["MA_LH"].ToString();
                                hmd.NamDangKy = (short)Convert.ToInt32(row["NAMDK"].ToString());
                                hmd.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                                hmd.MaPhu = row["MA_NPL_SP"].ToString();
                                hmd.TenHang = row["Ten_Hang"].ToString();
                                hmd.MaHS = row["MA_HANGKB"].ToString();
                                if (hmd.MaHS.Length < 10)
                                {
                                    for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
                                        hmd.MaHS += "0";
                                }
                                hmd.DVT_ID = row["Ma_DVT"].ToString();
                                hmd.NuocXX_ID = row["Nuoc_XX"].ToString();
                                hmd.SoLuong = Convert.ToDecimal(row["Luong"]);
                                try
                                {
                                    hmd.DonGiaKB = Convert.ToDecimal(row["DGia_KB"]);
                                }
                                catch (Exception exx) { }
                                try
                                {
                                    hmd.DonGiaTT = Convert.ToDecimal(row["DGia_TT"]);
                                }
                                catch (Exception exx) { }
                                hmd.TriGiaKB = Convert.ToDecimal(row["TriGia_KB"]);
                                hmd.TriGiaTT = Convert.ToDecimal(row["TriGia_TT"]);
                                hmd.TriGiaKB_VND = Convert.ToDecimal(row["TGKB_VND"]);
                                if (row["TS_XNK"].ToString() != "")
                                    hmd.ThueSuatXNK = Convert.ToDecimal(row["TS_XNK"]);
                                if (row["TS_TTDB"].ToString() != "")
                                    hmd.ThueSuatTTDB = Convert.ToDecimal(row["TS_TTDB"]);
                                if (row["TS_VAT"].ToString() != "")
                                    hmd.ThueSuatGTGT = Convert.ToDecimal(row["TS_VAT"]);
                                if (row["Thue_XNK"].ToString() != "")
                                    hmd.ThueXNK = Convert.ToDecimal(row["Thue_XNK"]);
                                if (row["Thue_TTDB"].ToString() != "")
                                    hmd.ThueTTDB = Convert.ToDecimal(row["Thue_TTDB"]);
                                if (row["Thue_VAT"].ToString() != "")
                                    hmd.ThueGTGT = Convert.ToDecimal(row["Thue_VAT"]);
                                if (row["Phu_Thu"].ToString() != "")
                                    hmd.PhuThu = Convert.ToDecimal(row["Phu_Thu"]);
                                if (row["MienThue"].ToString() != "")
                                    hmd.MienThue = Convert.ToByte(row["MienThue"]);
                                if (row["TyLe_ThuKhac"].ToString() != "")
                                    hmd.TyLeThuKhac = Convert.ToDecimal(row["TyLe_ThuKhac"]);
                                if (row["TriGia_ThuKhac"].ToString() != "")
                                    hmd.TriGiaThuKhac = Convert.ToDecimal(row["TriGia_ThuKhac"]);
                                hmd.SoThuTuHang = Convert.ToInt16(row["STTHANG"]);
                                try
                                {
                                    DataRow[] rowTTDM = dsToKhaiCu.Tables[0].Select("MaLoaiHinh='" + hmd.MaLoaiHinh + "' and NamDangKy=" + hmd.NamDangKy + " and MaHaiQuan='" + hmd.MaHaiQuan + "' and  SoToKhai=" + hmd.SoToKhai);
                                    if (rowTTDM == null || rowTTDM.Length == 0)
                                        hmd.InsertTransaction(transaction);
                                }
                                catch { }
                            }
                        //ThongTinDieuChinh.updateDatabaseMoi(dsThongTinDieuChinh, transaction, dsToKhaiCu);
                        //HangMauDichDieuChinh.updateDatabaseMoi(dshangDieuChinh, transaction, dsToKhaiCu);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return 1;//;ds.Tables[0].Rows.Count;
        }
        public DataTable GetToKhaiSapHetHanTK(int hanThanhKhoan, int thoiGianThanhKhoan, string maDN, string maHQ)
        {
            string sql = "SELECT CASE WHEN a.MaLoaiHinh LIKE '%V%' THEN (SELECT TOP 1 SoTKVNACCS FROM  t_VNACCS_CapSoToKhai WHERE SoTK = a.SoToKhai) " +
    "ELSE a.SoToKhai END AS SoToKhaiVNACCS, " +
            " a.SoToKhai, a.MaLoaiHinh,b.NgayDangKy, a.NamDangKy, a.MaHaiQuan, a.MaNPL, c.Ten as TenNPL, a.Luong, a.Ton , a.ThueXNK, (a.Ton / a.Luong)*a.ThueXNK as ThueTon ," +
                                " @HanThanhKhoan - DATEDIFF(day, NgayDangKy, GETDATE()) as SoNgay, " +
                                 "Case when a.MaLoaiHinh like '%V%' then (select top 1 GhiChu from t_KDT_VNACC_ToKhaiMauDich where SoToKhai = (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = a.SoToKhai)) else (select top 1 SoHopDong from t_SXXK_ToKhaiMauDich where SoToKhai = a.SoToKhai)  end AS SoHopDong " +
                        "FROM dbo.t_SXXK_ThanhLy_NPLNhapTon a INNER JOIN t_SXXK_ToKhaiMauDich b " +
                        "ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh " +
                        "AND a.NamDangKY = b.NamDangKy AND a.MaHaiQuan = b.MaHaiQuan " +
                        "LEFT JOIN t_SXXK_NguyenPhuLieu c ON a.MaNPL = c.Ma " +
                        "WHERE (275 - DATEDIFF(day, b.NgayDangKy, GETDATE())<= @ThoiGianTK) " +
                                "AND (@HanThanhKhoan - DATEDIFF(day, b.NgayDangKy, GETDATE()) >= 0) " +
                                "AND b.ThanhLy != 'H' AND (a.MaLoaiHinh LIKE 'NSX%' OR a.MaLoaiHinh LIKE 'NV%') AND a.MaHaiQuan = '" + maHQ + "' AND a.MaDoanhNghiep ='" + maDN + "'"
                                + " ORDER BY @HanThanhKhoan - DATEDIFF(day, NgayDangKy, GETDATE())";

            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HanThanhKhoan", SqlDbType.Int, hanThanhKhoan);
            db.AddInParameter(dbCommand, "@ThoiGianTK", SqlDbType.Int, thoiGianThanhKhoan);
            return this.db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public DataTable GetToKhaiQuaHanTK(int hanThanhKhoan, string maDN, string maHQ)
        {
            string sql = "SELECT CASE WHEN a.MaLoaiHinh LIKE '%V%' THEN (SELECT TOP 1 SoTKVNACCS FROM  t_VNACCS_CapSoToKhai WHERE SoTK = a.SoToKhai) " +
    "ELSE a.SoToKhai END AS SoToKhaiVNACCS, " + "a.SoToKhai, a.MaLoaiHinh,b.NgayDangKy, a.NamDangKy, a.MaHaiQuan, a.MaNPL, c.Ten as TenNPL," +
                        " a.Luong, a.Ton , a.ThueXNK, (a.Ton / a.Luong)*a.ThueXNK as ThueTon, " +
                        " N'Quá hạn ' + Cast( DATEDIFF(day, NgayDangKy, GETDATE()-@HanThanhKhoan) as nvarchar(10)) + N' ngày' as SoNgay " +
                        " FROM dbo.t_SXXK_ThanhLy_NPLNhapTon a INNER JOIN t_SXXK_ToKhaiMauDich b " +
                        " ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh " +
                        " AND a.NamDangKY = b.NamDangKy AND a.MaHaiQuan = b.MaHaiQuan " +
                        " INNER JOIN t_SXXK_NguyenPhuLieu c ON a.MaNPL = c.Ma AND a.MaHaiQuan=c.MaHaiQuan AND a.MaDoanhNghiep=c.MaDoanhNghiep " +
                        " WHERE (DATEDIFF(day, b.NgayDangKy, GETDATE())> @HanThanhKhoan) " +
                                " AND a.Ton >0 " +
                                " AND b.ThanhLy != 'H' AND a.MaLoaiHinh LIKE 'NSX%' AND a.MaHaiQuan = '" + maHQ + "' AND a.MaDoanhNghiep ='" + maDN + "'";

            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HanThanhKhoan", SqlDbType.Int, hanThanhKhoan);
            //db.AddInParameter(dbCommand, "@ThoiGianTK", SqlDbType.Int, thoiGianThanhKhoan);
            return this.db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public DataTable GetToKhaiBaoCaoThueTon(int hanThanhKhoan, int sotokhai, string maDN, string maHQ)
        {
            string sql = "SELECT a.SoToKhai,a.MaLoaiHinh,b.NgayDangKy, a.NamDangKy, a.MaHaiQuan, a.MaNPL, c.Ten as TenNPL, a.Luong, a.Ton ,   (a.Ton / a.Luong)*a.ThueXNK as ThueTon, " +
                "b.SoHopDong,b.TyGiaUSD,b.NguyenTe_ID, " +
                "d.Ten as DVT, " +
                " h.NuocXX_ID,h.MaPhu,h.TenHang,h.DonGiaKB,h.SoLuong,h.DonGiaTT,h.TriGiaKB,h.TriGiaTT,h.ThueSuatXNK,h.ThueSuatTTDB,h.ThueSuatGTGT,h.ThueXNK ,h.ThueTTDB,h.ThueGTGT,h.TyLeThuKhac, " +
                " N'Quá hạn ' + Cast( DATEDIFF(day, NgayDangKy, GETDATE()-@HanThanhKhoan) as nvarchar(10)) + N' ngày' as SoNgay " +
                        " FROM dbo.t_SXXK_ThanhLy_NPLNhapTon a INNER JOIN t_SXXK_ToKhaiMauDich b " +
                        " ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh " +
                        " AND a.NamDangKY = b.NamDangKy AND a.MaHaiQuan = b.MaHaiQuan " +
                        " INNER JOIN t_SXXK_NguyenPhuLieu c ON a.MaNPL = c.Ma " +
                        " INNER JOIN t_SXXK_HangMauDich h ON b.MaHaiQuan  = h.MaHaiQuan " +
                        " AND  b.SoToKhai  = h.SoToKhai " +
                        " AND  b.NamDangKy  = h.NamDangKy " +
                        " AND  b.MaLoaiHinh  = h.MaLoaiHinh " +
                        " INNER JOIN t_HaiQuan_DonViTinh d ON d.ID = h.DVT_ID " +
                        " WHERE (DATEDIFF(day, b.NgayDangKy, GETDATE())> @HanThanhKhoan) " +
                                " AND a.Ton >0 " +
                                " AND a.SoToKhai = @SoToKhai " +
                                " AND b.ThanhLy != 'H' AND a.MaLoaiHinh LIKE 'NSX%' AND a.MaHaiQuan = '" + maHQ + "' AND a.MaDoanhNghiep ='" + maDN + "'";

            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HanThanhKhoan", SqlDbType.Int, hanThanhKhoan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, sotokhai);
            return this.db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public bool InsertUpdateFullHang(HangMauDich hmd, bool create)//true la tao moi
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    {
                        this.TrangThaiVNACCS.InsertUpdate(transaction, db);
                        if (hmd.MaPhu != hmd.MaTMP)
                            this.UpdateMaHang(hmd.MaPhu, hmd.MaTMP, hmd.SoLuong, transaction);
                        hmd.InsertUpdateTransaction(transaction);
                        if (this.MaLoaiHinh.StartsWith("N"))
                        {
                            //cap nhat bang nguyen phu lieu ton 
                            BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon = new NPLNhapTon();
                            nplTon.MaHaiQuan = this.MaHaiQuan;
                            nplTon.MaNPL = hmd.MaPhu; ;
                            nplTon.SoToKhai = this.SoToKhai;
                            nplTon.MaLoaiHinh = this.MaLoaiHinh;
                            nplTon.NamDangKy = this.NamDangKy;
                            nplTon.MaDoanhNghiep = this.MaDoanhNghiep;
                            if (!nplTon.Load(transaction)) nplTon.Ton = hmd.SoLuong;
                            nplTon.Luong = hmd.SoLuong;
                            //nplTon.Ton = nplTon.Ton;
                            nplTon.ThueXNK = (double)hmd.ThueXNK;
                            nplTon.ThueVAT = (double)hmd.ThueGTGT;
                            nplTon.ThueTTDB = (double)hmd.ThueTTDB;
                            nplTon.TenNPL = hmd.TenHang;
                            nplTon.PhuThu = (double)hmd.PhuThu;
                            nplTon.ThueCLGia = (double)hmd.TriGiaThuKhac;
                            nplTon.ThueXNKTon = nplTon.ThueXNK;
                            nplTon.InsertUpdateTransaction(transaction);

                            BLL.SXXK.NPLNhapTonThucTe nplTonTT = new NPLNhapTonThucTe();
                            nplTonTT.MaHaiQuan = this.MaHaiQuan;
                            nplTonTT.MaNPL = hmd.MaPhu; ;
                            nplTonTT.SoToKhai = this.SoToKhai;
                            nplTonTT.MaLoaiHinh = this.MaLoaiHinh;
                            nplTonTT.NamDangKy = this.NamDangKy;
                            nplTonTT.MaDoanhNghiep = this.MaDoanhNghiep;
                            nplTonTT.Luong = hmd.SoLuong;
                            nplTonTT.Ton = nplTon.Ton;
                            nplTonTT.ThueXNK = (double)hmd.ThueXNK;
                            nplTonTT.ThueVAT = (double)hmd.ThueGTGT;
                            nplTonTT.ThueTTDB = (double)hmd.ThueTTDB;
                            nplTonTT.PhuThu = (double)hmd.PhuThu;
                            nplTonTT.ThueCLGia = (double)hmd.TriGiaThuKhac;
                            nplTonTT.ThueXNKTon = nplTon.ThueXNK;
                            nplTonTT.NgayDangKy = this.NgayDangKy;
                            nplTonTT.InsertUpdate(transaction);
                        }
                        hmd.MaTMP = hmd.MaPhu;
                    }


                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }
        public DataTable GetDanhSachDoiTac(string maDoanhNghiep)
        {
            string sql = "SELECT DISTINCT TenDonViDoiTac FROM t_SXXK_ToKhaiMauDich WHERE MaDoanhNghiep = '" + maDoanhNghiep + "'";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            return this.db.ExecuteDataSet(dbCommand).Tables[0];
        }

        //public void UpdateLuongTon()
        //{
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        try
        //        {
        //            foreach (HangMauDich HMDTon in this.HMDCollection)
        //            {
        //                if (this.MaLoaiHinh.StartsWith("N"))
        //                {
        //                    NguyenPhuLieuTon NPLTon = new NguyenPhuLieuTon();
        //                    NPLTon.Ma = HMDTon.MaPhu;
        //                    NPLTon.MaDoanhNghiep = this.MaDoanhNghiep;
        //                    NPLTon.MaHaiQuan = this.MaHaiQuan;
        //                    NPLTon.Load(transaction);
        //                    double LuongTon = NPLTon.LuongTon + Convert.ToDouble(HMDTon.SoLuong);
        //                    if (HMDTon.SoThuTuHang > 0)
        //                        LuongTon = NPLTon.LuongTon - Convert.ToDouble(HMDTon.SoLuong);
        //                    NPLTon.LuongTon = LuongTon;
        //                    NPLTon.UpdateTransaction(transaction);
        //                }
        //                else
        //                {
        //                    if (this.MaLoaiHinh == "XSX05")
        //                    {
        //                        NguyenPhuLieuTon NPLTon = new NguyenPhuLieuTon();
        //                        NPLTon.Ma = HMDTon.MaPhu;
        //                        NPLTon.MaDoanhNghiep = this.MaDoanhNghiep;
        //                        NPLTon.MaHaiQuan = this.MaHaiQuan;
        //                        NPLTon.Load(transaction);
        //                        double LuongTon = NPLTon.LuongTon - Convert.ToDouble(HMDTon.SoLuong);
        //                        if (HMDTon.SoThuTuHang > 0)
        //                            LuongTon = NPLTon.LuongTon + Convert.ToDouble(HMDTon.SoLuong);
        //                        NPLTon.LuongTon = LuongTon;
        //                        NPLTon.UpdateTransaction(transaction);
        //                    }
        //                    else
        //                    {
        //                        DataSet dsDM = Company.BLL.SXXK.DinhMuc.getDinhMucOfSanPham(HMDTon.MaPhu, MaHaiQuan, MaDoanhNghiep, HMDTon.SoLuong, transaction);

        //                        if (dsDM.Tables[0].Rows.Count == 0)
        //                            throw new Exception("Sản phẩm : " + HMDTon.MaPhu + " chưa có định mức nên không thể cập nhật vào lượng tồn được.");
        //                        foreach (DataRow rowDMIndex in dsDM.Tables[0].Rows)
        //                        {
        //                            string MaNPL = rowDMIndex["MaNguyenPhuLieu"].ToString();
        //                            double SoLuong = Convert.ToDouble(rowDMIndex["SoLuong"].ToString());
        //                            NguyenPhuLieuTon NPLTon = new NguyenPhuLieuTon();
        //                            NPLTon.Ma = MaNPL;
        //                            NPLTon.MaDoanhNghiep = this.MaDoanhNghiep;
        //                            NPLTon.MaHaiQuan = this.MaHaiQuan;
        //                            double LuongTon = NPLTon.LuongTon - SoLuong;
        //                            if (HMDTon.SoThuTuHang > 0)
        //                                LuongTon = NPLTon.LuongTon + SoLuong;
        //                            NPLTon.LuongTon = LuongTon;
        //                            NPLTon.UpdateTransaction(transaction);
        //                        }
        //                    }
        //                }
        //            }
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }

        //}

        //public bool CapNhatLuongTonKhiXoaToKhai(ToKhaiMauDichCollection collection)
        //{          
        //    bool ret;
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        try
        //        {
        //            bool ret01 = true;
        //            foreach (ToKhaiMauDich item in collection)
        //            {
        //                foreach (HangMauDich HMDDelete in item.HMDCollection)
        //                {
        //                    HMDDelete.TinhToanLuongTonSauKhiXoa(HMDDelete, item.MaLoaiHinh, item.MaHaiQuan, item.MaDoanhNghiep,transaction);
        //                }                      
        //            }
        //            if (ret01)
        //            {
        //                transaction.Commit();
        //                ret = true;
        //            }
        //            else
        //            {
        //                transaction.Rollback();
        //                ret = false;
        //            }
        //        }
        //        catch(Exception ex)
        //        {
        //            ret = false;
        //            transaction.Rollback();
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //    return ret;
        //}
        public void UpdateTrangThai(string trangThai, SqlTransaction transaction)
        {
            string sql = "UPDATE t_SXXK_ToKhaiMauDich SET ThanhLy = '" + trangThai + "' WHERE " +
                         "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                         "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + this.MaHaiQuan + "'";
            SqlCommand sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
            db.ExecuteNonQuery(sqlCmd, transaction);

        }

        public bool Load(SqlTransaction trans)
        {
            string spName = "p_SXXK_ToKhaiMauDich_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);

            IDataReader reader = this.db.ExecuteReader(dbCommand, trans);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) this._Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) this._NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) this.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) this.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) this.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));

                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool Load(SqlTransaction trans, SqlDatabase db)
        {
            string spName = "p_SXXK_ToKhaiMauDich_Load";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);

            IDataReader reader = null;
            if (trans != null)
                reader = db.ExecuteReader(dbCommand, trans);
            else
                reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) this._Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) this._NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) this.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) this.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) this.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));

                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public void LoadBy(string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy)
        {
            string spName = "p_SXXK_ToKhaiMauDich_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, namDangKy);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) this._Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) this._NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) this.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) this.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) this.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));

                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                reader.Close();
            }
            reader.Close();
        }
        public void LoadVNACCSBy(string maHaiQuan, string madoanhNghiep, string sotokhaiVNACCS)
        {
            string sql = "Select * from t_sxxk_tokhaimaudich where mahaiquan=@MaHaiQuan and madoanhnghiep=@MaDoanhNghiep and LoaiVanDon=@sotokhaiVNACCS";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madoanhNghiep);
            db.AddInParameter(dbCommand, "@sotokhaiVNACCS", SqlDbType.VarChar, sotokhaiVNACCS);
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) this._Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) this._NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) this.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) this.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) this.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));

                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                reader.Close();
            }
            reader.Close();
        }

        public static ToKhaiMauDich LoadBy(SqlTransaction trans, SqlDatabase db, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy)
        {
            string spName = "p_SXXK_ToKhaiMauDich_Load";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, namDangKy);

            IDataReader reader = db.ExecuteReader(dbCommand, trans);
            if (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) entity._Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity._NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));

                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return entity;
            }
            reader.Close();
            return null;
        }

        public void LoadPhanBo()
        {
            _pbToKhaiXuatCollection = (List<PhanBoToKhaiXuat>)PhanBoToKhaiXuat.SelectPhanBoToKhaiXuatOfTKMD(this.MaHaiQuan, this.MaLoaiHinh, this.SoToKhai, this.NamDangKy);
        }
        public void LoadPhanBo(SqlTransaction transaction)
        {
            _pbToKhaiXuatCollection = (List<PhanBoToKhaiXuat>)PhanBoToKhaiXuat.SelectPhanBoToKhaiXuatOfTKMD(this.MaHaiQuan, this.MaLoaiHinh, this.SoToKhai, this.NamDangKy, transaction);
        }
        public static ToKhai.ToKhaiMauDichCollection SelectToKhaiChuaPhanBo(string MaHaiQuan, string MaDoanhNghiep)
        {
            string sql = "select * from t_SXXK_ToKhaiMauDich where " +
                         "(rtrim(MaHaiQuan)+cast(SoToKhai as varchar)+rtrim(MaLoaiHinh)+cast(NamDangKy as varchar)) not in " +
                         "( " +
                         "select distinct (rtrim(MaHaiQuanXuat)+cast(SoToKhaiXuat as varchar)+rtrim(MaLoaiHinhXuat)+cast(NamDangKyXuat as varchar)) " +
                         "from dbo.t_SXXK_PhanBoToKhaiXuat " +
                         "where MaHaiQuanXuat=@MaHaiQuan and MaDoanhNghiep=@MaDoanhNghiep " +
                         ") and  MaHaiQuan=@MaHaiQuan and MaDoanhNghiep=@MaDoanhNghiep and trangthai=1 " +
                         " and maloaihinh like 'X%'" +
                         "order by ngaydangky,mahaiquan,sotokhai,maloaihinh";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) entity.Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) entity.HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;

        }
        public static ToKhai.ToKhaiMauDichCollection SelectToKhaiDaPhanBo(string MaHaiQuan, string MaDoanhNghiep)
        {
            string sql = "select * from t_SXXK_ToKhaiMauDich where " +
                         "(rtrim(MaHaiQuan)+cast(SoToKhai as varchar)+rtrim(MaLoaiHinh)+cast(NamDangKy as varchar)) in " +
                         "( " +
                         "select distinct (rtrim(MaHaiQuanXuat)+cast(SoToKhaiXuat as varchar)+rtrim(MaLoaiHinhXuat)+cast(NamDangKyXuat as varchar)) " +
                         "from dbo.t_SXXK_PhanBoToKhaiXuat " +
                         "where MaHaiQuanXuat=@MaHaiQuan and MaDoanhNghiep=@MaDoanhNghiep " +
                         ") and  MaHaiQuan=@MaHaiQuan and MaDoanhNghiep=@MaDoanhNghiep and trangthai=0" +
                         " and maloaihinh like 'X%'" +
                         "order by ngaydangky,mahaiquan,sotokhai,maloaihinh";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) entity.Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) entity.HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;

        }
        public static ToKhai.ToKhaiMauDichCollection SelectToKhaiKhongCoNPLTon(int NamDangKy)
        {
            string spName = "p_SXXK_ToKhaiMauDich_SelectByTKKhongCoNPLTon";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Char, NamDangKy);
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) entity.Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) entity.HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;

        }
        public void Delete()
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    if (this.MaLoaiHinh.StartsWith("X"))
                    {
                        foreach (PhanBoToKhaiXuat item in PhanBoToKhaiXuatCollection)
                        {
                            item.DeletePhanBo(transaction);
                        }
                        UpdateTrangThaiChuaPhanBo(transaction);
                    }

                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public void Delete(SqlTransaction transaction)
        {
            if (this.MaLoaiHinh.StartsWith("XSX") && this.TrangThai == 0)
            {
                foreach (PhanBoToKhaiXuat item in PhanBoToKhaiXuatCollection)
                {
                    item.DeletePhanBo(transaction);
                }
                UpdateTrangThaiChuaPhanBo(transaction);
            }
            else
            {
                HangMauDich hmd = new HangMauDich();
                hmd.DeleteHangTransaction(transaction);
                this.DeleteTransaction(transaction);
            }

        }

        public void DeletePhanBo(SqlTransaction transaction)
        {
            if (this.MaLoaiHinh.StartsWith("XSX"))
            {
                foreach (PhanBoToKhaiXuat item in PhanBoToKhaiXuatCollection)
                {
                    item.DeletePhanBo(transaction);
                }
            }
        }
        public void UpdateTrangThaiChuaPhanBo(SqlTransaction transaction)
        {
            string sql = "select * from  t_SXXK_ToKhaiMauDich where MaHaiQuan=@MaHaiQuan and MaDoanhNghiep=@MaDoanhNghiep and MaLoaiHinh like 'X%' and NgayDangKy>=@NgayDangKy and trangthai=0";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);

            DataSet ds = db.ExecuteDataSet(dbCommand, transaction);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ToKhai.ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.SoToKhai = Convert.ToInt32(row["SoToKhai"]);
                TKMD.NamDangKy = Convert.ToInt16(row["NamDangKy"]);
                TKMD.MaLoaiHinh = (row["MaLoaiHinh"].ToString());
                TKMD.MaHaiQuan = (row["MaHaiQuan"].ToString());
                TKMD.NgayDangKy = Convert.ToDateTime(row["NgayDangKy"].ToString());
                bool ok = true;
                if (TKMD.NgayDangKy == this.NgayDangKy)
                {
                    if (TKMD.SoToKhai >= this.SoToKhai)
                    {
                        if (TKMD.SoToKhai == this.SoToKhai)
                        {
                            if (TKMD.MaLoaiHinh.CompareTo(this.MaLoaiHinh) > 0)
                                ok = true;
                            else
                                ok = false;
                        }
                        else
                            ok = true;
                    }
                    else
                        ok = false;
                }
                if (ok)
                {
                    TKMD.Load(transaction);
                    TKMD.LoadPhanBo(transaction);
                    TKMD.DeletePhanBo(transaction);
                    TKMD.TrangThai = 1;
                    TKMD.UpdateTransaction(transaction);
                }
            }
        }
        public void DeletePhanBo()
        {
            this.LoadPhanBo();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (PhanBoToKhaiXuat item in PhanBoToKhaiXuatCollection)
                    {
                        item.DeletePhanBo(transaction);
                    }
                    this.TrangThai = 1;
                    this.UpdateTransaction(transaction);
                    UpdateTrangThaiChuaPhanBo(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        public void DeletePhanBoCacToKhaiSau(IList<PhanBoToKhaiNhap> PhanBoToKhaiNhapCollection)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    UpdateTrangThaiChuaPhanBo(transaction);
                    foreach (PhanBoToKhaiNhap pbTKNhap in PhanBoToKhaiNhapCollection)
                    {
                        pbTKNhap.InsertUpdate();
                        NPLNhapTonThucTe npl = NPLNhapTonThucTe.Load(pbTKNhap.SoToKhaiNhap, pbTKNhap.MaLoaiHinhNhap, pbTKNhap.NamDangKyNhap, pbTKNhap.MaHaiQuanNhap, pbTKNhap.MaNPL, transaction);
                        npl.Ton = Convert.ToDecimal(pbTKNhap.LuongTonCuoi);
                        npl.ThueXNKTon = Math.Round(Convert.ToDouble(npl.Ton * Convert.ToDecimal(npl.ThueXNK) / (npl.Luong)), MidpointRounding.AwayFromZero);
                        npl.InsertUpdate(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        public bool DeleteCollection(ToKhaiMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiMauDich item in collection)
                    {
                        item.Delete(transaction);
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public static DataTable GetToKhaiXGC(string where)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("ECS_GC");
            string sql = "select * from t_KDT_ToKhaiMauDich where TrangThaiXuLy=1 and MaLoaiHinh LIKE 'XGC%' AND TrangThaiPhanBo=1";
            if (where != "")
            {
                sql = sql + " and " + where;
            }
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbCommand).Tables[0];

        }
        public static DataTable GetHangMauDichCuaToKhaiXGCByID(long id)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("ECS_GC");
            string sql = "select * from t_KDT_HangMauDich where TKMD_ID=" + id;

            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbCommand).Tables[0];

        }
        public void InsertUpdateToKhaiGiaCong()
        {
            if (this.HMDCollection.Count == 0)
                this.LoadHMDCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.TrangThaiVNACCS.InsertUpdate(transaction, db);
                    this.InsertUpdateTransaction(transaction);
                    short STTHang = 1;
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {

                        hmd.MaHaiQuan = this.MaHaiQuan;
                        hmd.MaLoaiHinh = this.MaLoaiHinh;

                        hmd.NamDangKy = (short)this.NgayDangKy.Year;
                        hmd.SoThuTuHang = STTHang;
                        hmd.SoToKhai = this.SoToKhai;
                        STTHang++;
                    }

                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.InsertUpdateTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public static decimal CheckThanhKhoanHet(int soToKhai, string maLoaiHinh, int namDangKy, int lanThanhLy)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT SUM(TonCuoi) as TonCuoi FROM t_KDT_SXXK_NPLNhapTon WHERE SoToKhai = " + soToKhai + " AND MaLoaiHinh = '" + maLoaiHinh + "' AND NamDangKy = " + namDangKy + " AND LanThanhLy = " + lanThanhLy;
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            decimal temp = 0;
            try
            {
                temp = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            {
                temp = 0;
            }
            return temp;

        }
        public static bool CheckDaTungThanhKhoan(int soToKhai, string maLoaiHinh, int namDangKy, int lanThanhLy)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT count(*) FROM t_KDT_SXXK_NPLNhapTon WHERE SoToKhai = " + soToKhai + " AND MaLoaiHinh = '" + maLoaiHinh + "' AND NamDangKy = " + namDangKy + " AND LanThanhLy < " + lanThanhLy + " AND TonDau > TonCuoi";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            decimal temp = 0;
            try
            {
                temp = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            {
                temp = 0;
            }
            return temp > 0;

        }
        public DataTable GetQuyetDinhToKhaiNhap()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT LanThanhLy, SUM(TonDauThueXNK - TonCuoiThueXNK) AS TienThueKhongThu FROM t_KDT_SXXK_NPLNhapTon WHERE MaDoanhNghiep = '" + this.MaDoanhNghiep + "' AND SoToKhai =" + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' AND NamDangKy =" + this.NamDangKy + " AND TonCuoi < TonDau AND LanThanhLy IN (SELECT LanThanhLy FROM t_KDT_SXXK_HoSoThanhLyDangKy) GROUP BY LanThanhLy";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;
        }

        public void Saved(ToKhaiMauDichCollection collection)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (ToKhaiMauDich item in collection)
                    {
                        InsertUpdateTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        public static DateTime GetNgayHoanThanh(int SoToKhai, string MaLoaiHinh, string MaHaiQuan, DateTime NgayDangKy, SqlTransaction transaction)
        {

            string sql = string.Format(@"Select Case
                                                When (NgayHoanThanh is null or Year(NgayHoanThanh) = 1900) then NgayDangKy
                                                Else NgayHoanThanh
                                                End as NgayHoanThanh
                                        From t_SXXK_ToKhaiMauDich where SoToKhai = {0} and MaHaiQuan = '{1}' and MaLoaiHinh = '{2}' AND Cast(NgayDangKy as DateTime) = Cast('{3}' as DateTime)", SoToKhai, MaHaiQuan, MaLoaiHinh, NgayDangKy.ToString("yyyy-MM-dd"));
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {

                connection.Open();
                SqlTransaction Transaction = connection.BeginTransaction();
                try
                {

                    SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
                    SqlDataReader reader;
                    if (transaction == null)
                        reader = (SqlDataReader)db.ExecuteReader(dbCommand, Transaction);
                    else
                        reader = (SqlDataReader)db.ExecuteReader(dbCommand, transaction);
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) return reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                        else
                            return new DateTime(1900, 1, 1);
                    }
                    dbCommand.Dispose();
                    return new DateTime(1900, 1, 1);
                }

                catch (System.Exception ex)
                {
                    connection.Close();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
    }

}

