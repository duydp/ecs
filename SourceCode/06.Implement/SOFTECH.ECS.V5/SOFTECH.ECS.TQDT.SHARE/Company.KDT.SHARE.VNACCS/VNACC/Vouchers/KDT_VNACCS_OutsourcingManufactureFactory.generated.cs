﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_OutsourcingManufactureFactory : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long StorageAreasProduction_ID { set; get; }
		public string TenDoiTac { set; get; }
		public string MaDoiTac { set; get; }
		public string DiaChiDoiTac { set; get; }

        public List<KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument> ContractDocumentCollection = new List<KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument>();
        public List<KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory> ManufactureFactoryCollection = new List<KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_OutsourcingManufactureFactory> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_OutsourcingManufactureFactory> collection = new List<KDT_VNACCS_OutsourcingManufactureFactory>();
			while (reader.Read())
			{
				KDT_VNACCS_OutsourcingManufactureFactory entity = new KDT_VNACCS_OutsourcingManufactureFactory();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("StorageAreasProduction_ID"))) entity.StorageAreasProduction_ID = reader.GetInt64(reader.GetOrdinal("StorageAreasProduction_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoiTac"))) entity.TenDoiTac = reader.GetString(reader.GetOrdinal("TenDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoiTac"))) entity.MaDoiTac = reader.GetString(reader.GetOrdinal("MaDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac"))) entity.DiaChiDoiTac = reader.GetString(reader.GetOrdinal("DiaChiDoiTac"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_OutsourcingManufactureFactory> collection, long id)
        {
            foreach (KDT_VNACCS_OutsourcingManufactureFactory item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_OutsourcingManufactureFactory VALUES(@StorageAreasProduction_ID, @TenDoiTac, @MaDoiTac, @DiaChiDoiTac)";
            string update = "UPDATE t_KDT_VNACCS_OutsourcingManufactureFactory SET StorageAreasProduction_ID = @StorageAreasProduction_ID, TenDoiTac = @TenDoiTac, MaDoiTac = @MaDoiTac, DiaChiDoiTac = @DiaChiDoiTac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_OutsourcingManufactureFactory WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, "StorageAreasProduction_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoiTac", SqlDbType.NVarChar, "TenDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoiTac", SqlDbType.NVarChar, "MaDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, "DiaChiDoiTac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, "StorageAreasProduction_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoiTac", SqlDbType.NVarChar, "TenDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoiTac", SqlDbType.NVarChar, "MaDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, "DiaChiDoiTac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_OutsourcingManufactureFactory VALUES(@StorageAreasProduction_ID, @TenDoiTac, @MaDoiTac, @DiaChiDoiTac)";
            string update = "UPDATE t_KDT_VNACCS_OutsourcingManufactureFactory SET StorageAreasProduction_ID = @StorageAreasProduction_ID, TenDoiTac = @TenDoiTac, MaDoiTac = @MaDoiTac, DiaChiDoiTac = @DiaChiDoiTac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_OutsourcingManufactureFactory WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, "StorageAreasProduction_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoiTac", SqlDbType.NVarChar, "TenDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoiTac", SqlDbType.NVarChar, "MaDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, "DiaChiDoiTac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, "StorageAreasProduction_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoiTac", SqlDbType.NVarChar, "TenDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoiTac", SqlDbType.NVarChar, "MaDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, "DiaChiDoiTac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_OutsourcingManufactureFactory Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_OutsourcingManufactureFactory> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_OutsourcingManufactureFactory> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_OutsourcingManufactureFactory> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_OutsourcingManufactureFactory> SelectCollectionBy_StorageAreasProduction_ID(long storageAreasProduction_ID)
		{
            IDataReader reader = SelectReaderBy_StorageAreasProduction_ID(storageAreasProduction_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_StorageAreasProduction_ID(long storageAreasProduction_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectBy_StorageAreasProduction_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, storageAreasProduction_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_StorageAreasProduction_ID(long storageAreasProduction_ID)
		{
			const string spName = "p_KDT_VNACCS_OutsourcingManufactureFactory_SelectBy_StorageAreasProduction_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, storageAreasProduction_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_OutsourcingManufactureFactory(long storageAreasProduction_ID, string tenDoiTac, string maDoiTac, string diaChiDoiTac)
		{
			KDT_VNACCS_OutsourcingManufactureFactory entity = new KDT_VNACCS_OutsourcingManufactureFactory();	
			entity.StorageAreasProduction_ID = storageAreasProduction_ID;
			entity.TenDoiTac = tenDoiTac;
			entity.MaDoiTac = maDoiTac;
			entity.DiaChiDoiTac = diaChiDoiTac;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, StorageAreasProduction_ID);
			db.AddInParameter(dbCommand, "@TenDoiTac", SqlDbType.NVarChar, TenDoiTac);
			db.AddInParameter(dbCommand, "@MaDoiTac", SqlDbType.NVarChar, MaDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_OutsourcingManufactureFactory> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_OutsourcingManufactureFactory item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_OutsourcingManufactureFactory(long id, long storageAreasProduction_ID, string tenDoiTac, string maDoiTac, string diaChiDoiTac)
		{
			KDT_VNACCS_OutsourcingManufactureFactory entity = new KDT_VNACCS_OutsourcingManufactureFactory();			
			entity.ID = id;
			entity.StorageAreasProduction_ID = storageAreasProduction_ID;
			entity.TenDoiTac = tenDoiTac;
			entity.MaDoiTac = maDoiTac;
			entity.DiaChiDoiTac = diaChiDoiTac;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_OutsourcingManufactureFactory_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, StorageAreasProduction_ID);
			db.AddInParameter(dbCommand, "@TenDoiTac", SqlDbType.NVarChar, TenDoiTac);
			db.AddInParameter(dbCommand, "@MaDoiTac", SqlDbType.NVarChar, MaDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_OutsourcingManufactureFactory> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_OutsourcingManufactureFactory item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_OutsourcingManufactureFactory(long id, long storageAreasProduction_ID, string tenDoiTac, string maDoiTac, string diaChiDoiTac)
		{
			KDT_VNACCS_OutsourcingManufactureFactory entity = new KDT_VNACCS_OutsourcingManufactureFactory();			
			entity.ID = id;
			entity.StorageAreasProduction_ID = storageAreasProduction_ID;
			entity.TenDoiTac = tenDoiTac;
			entity.MaDoiTac = maDoiTac;
			entity.DiaChiDoiTac = diaChiDoiTac;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, StorageAreasProduction_ID);
			db.AddInParameter(dbCommand, "@TenDoiTac", SqlDbType.NVarChar, TenDoiTac);
			db.AddInParameter(dbCommand, "@MaDoiTac", SqlDbType.NVarChar, MaDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_OutsourcingManufactureFactory> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_OutsourcingManufactureFactory item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_OutsourcingManufactureFactory(long id)
		{
			KDT_VNACCS_OutsourcingManufactureFactory entity = new KDT_VNACCS_OutsourcingManufactureFactory();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int DeleteBy_StorageAreasProduction_ID(long storageAreasProduction_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_DeleteBy_StorageAreasProduction_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, storageAreasProduction_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_OutsourcingManufactureFactory> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_OutsourcingManufactureFactory item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert();
                    }
                    else
                    {
                        this.Update();
                    }

                    foreach (KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument item in ContractDocumentCollection)
                    {
                        item.OutsourcingManufactureFactory_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }
                    foreach (KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory item in ManufactureFactoryCollection )
                    {
                        item.OutsourcingManufactureFactory_ID = this.ID;
                        item.InsertUpdateFull();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                }

        }
        public void DeleteFull()
        {
            try
            {
                foreach (KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument item in ContractDocumentCollection)
                {
                    item.OutsourcingManufactureFactory_ID = this.ID;
                    item.DeleteBy_OutsourcingManufactureFactory_ID(this.ID);
                }
                foreach (KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory item in ManufactureFactoryCollection)
                {
                    item.OutsourcingManufactureFactory_ID = this.ID;
                    item.DeleteFull();
                }
                this.DeleteBy_StorageAreasProduction_ID(this.ID);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
            }
        }
	}	
}