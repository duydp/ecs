using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.GC.BLL.KDT.GC
{
    public partial class HangChuyenTiep
    {
        public DonViTinhQuyDoi DVT_QuyDoi { get; set; }
        public ThuTucHQTruocDo ThuTucHQTruocDo { get; set; }
        public bool Load(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_HangChuyenTiep_Load";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            IDataReader reader = db.ExecuteReader(dbCommand, transaction);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) this.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) this.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) this.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_NuocXX"))) this.ID_NuocXX = reader.GetString(reader.GetOrdinal("ID_NuocXX"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_DVT"))) this.ID_DVT = reader.GetString(reader.GetOrdinal("ID_DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) this.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) this.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) this.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) this.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) this.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) this.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) this.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) this.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) this.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) this.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) this.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) this.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) this.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) this.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) this.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) this.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) this.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) this.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) this.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) this.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //cap nhat lai so luong npl, sp, tb
        public void UpdateSoLuong(SqlTransaction transaction, long ID_HD, string LoaiHangHoa, string MaLoaiHinh, HangChuyenTiep HCTCu)
        {
            decimal i = 1;
            decimal SoLuongCu = HCTCu.SoLuong;
            if (!MaLoaiHinh.EndsWith("N") && !MaLoaiHinh.Substring(0, 1).Equals("N"))
            {
                i = -1;
            }
            else
            {
                i = 1;
            }

            if (LoaiHangHoa == "N")
            {
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = ID_HD;
                npl.Ma = this.MaHang.Trim();
                npl.Load(transaction);
                if (npl.Ma.Trim().ToUpper() == HCTCu.MaHang.Trim().ToUpper() || HCTCu.MaHang.Trim().Length == 0)
                {
                    if (i > 0)
                        npl.SoLuongDaNhap += (this.SoLuong) - SoLuongCu;
                    else
                        npl.SoLuongDaDung += (this.SoLuong) - SoLuongCu;
                }
                else
                {
                    if (i > 0)
                        npl.SoLuongDaNhap += this.SoLuong;
                    else
                        npl.SoLuongDaDung += this.SoLuong;
                    Company.GC.BLL.GC.NguyenPhuLieu nplCu = new Company.GC.BLL.GC.NguyenPhuLieu();
                    nplCu.HopDong_ID = ID_HD;
                    nplCu.Ma = HCTCu.MaHang;
                    nplCu.Load(transaction);
                    if (i > 0)
                        nplCu.SoLuongDaNhap -= HCTCu.SoLuong;
                    else
                        nplCu.SoLuongDaDung -= HCTCu.SoLuong;
                    nplCu.UpdateTransaction(transaction);
                }
                npl.UpdateTransaction(transaction);
            }
            else if (LoaiHangHoa == "T")
            {
                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                tb.HopDong_ID = ID_HD;
                tb.Ma = this.MaHang.Trim();
                tb.Load(transaction);
                if (tb.Ma.Trim().ToUpper() == HCTCu.MaHang.Trim().ToUpper() || HCTCu.MaHang.Trim().Length == 0)
                {
                    tb.SoLuongDaNhap += this.SoLuong * i + SoLuongCu;
                }
                else
                {
                    if (i > 0)
                        tb.SoLuongDaNhap += this.SoLuong;
                    else
                        tb.SoLuongDaNhap -= this.SoLuong;
                    Company.GC.BLL.GC.ThietBi tbCu = new Company.GC.BLL.GC.ThietBi();
                    tbCu.HopDong_ID = ID_HD;
                    tbCu.Ma = HCTCu.MaHang;
                    tbCu.Load(transaction);
                    if (i > 0)
                        tbCu.SoLuongDaNhap -= HCTCu.SoLuong;
                    else
                        tbCu.SoLuongDaNhap += HCTCu.SoLuong;
                }
                tb.UpdateTransaction(transaction);
            }
            else if (LoaiHangHoa == "S")
            {
                if (MaLoaiHinh.EndsWith("X") || MaLoaiHinh.Substring(0, 1).Equals("X"))
                {
                    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                    sp.HopDong_ID = ID_HD;
                    sp.Ma = this.MaHang.Trim();
                    sp.Load(transaction);
                    if (sp.Ma.Trim().ToUpper() == HCTCu.MaHang.Trim().ToUpper() || HCTCu.MaHang.Trim().Length == 0)
                    {
                        sp.SoLuongDaXuat += (this.SoLuong * i + SoLuongCu) * (-1);
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham spCu = new Company.GC.BLL.GC.SanPham();
                        spCu.HopDong_ID = ID_HD;
                        spCu.Ma = HCTCu.MaHang;
                        spCu.Load(transaction);
                        spCu.SoLuongDaXuat -= HCTCu.SoLuong;
                        spCu.UpdateTransaction(transaction);

                        sp.SoLuongDaXuat += this.SoLuong;
                    }
                    sp.UpdateTransaction(transaction);
                    if (HCTCu.ID > 0)
                    {
                        DataSet dsLuongNPLCu = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HCTCu.MaHang, HCTCu.SoLuong, ID_HD, transaction);
                        foreach (DataRow row in dsLuongNPLCu.Tables[0].Rows)
                        {
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.HopDong_ID = ID_HD;
                            npl.Ma = row["MaNguyenPhuLieu"].ToString();
                            npl.Load(transaction);
                            npl.SoLuongDaDung -= Convert.ToDecimal(row["LuongCanDung"]);
                            npl.UpdateTransaction(transaction);
                        }
                    }
                    DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(this.MaHang.Trim(), this.SoLuong, ID_HD, transaction);
                    foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl.HopDong_ID = ID_HD;
                        npl.Ma = row["MaNguyenPhuLieu"].ToString();
                        npl.Load(transaction);
                        npl.SoLuongDaDung += Convert.ToDecimal(row["LuongCanDung"]);
                        npl.UpdateTransaction(transaction);
                    }
                }

            }
            else if (LoaiHangHoa == "H")
            {

                Company.GC.BLL.GC.HangMau hangMau = Company.GC.BLL.GC.HangMau.Load(ID_HD, this.MaHang.Trim());                
                if (hangMau == null) return;
                if (hangMau.Ma.Trim().ToUpper() == HCTCu.MaHang.Trim().ToUpper() || HCTCu.MaHang.Trim().Length == 0)
                {
                    if (i > 0)
                        hangMau.SoLuongDaNhap += (this.SoLuong) - SoLuongCu;
                    else
                        hangMau.SoLuongDaDung += (this.SoLuong) - SoLuongCu;
                }
                else
                {
                    if (i > 0)
                        hangMau.SoLuongDaNhap += this.SoLuong;
                    else
                        hangMau.SoLuongDaDung += this.SoLuong;
                    Company.GC.BLL.GC.HangMau hangmauCu = Company.GC.BLL.GC.HangMau.Load(ID_HD,HCTCu.MaHang);
                    if (hangmauCu == null) return;
                    if (i > 0)
                        hangmauCu.SoLuongDaNhap -= HCTCu.SoLuong;
                    else
                        hangmauCu.SoLuongDaDung -= HCTCu.SoLuong;
                    hangmauCu.Update(transaction);
                }
                hangMau.Update(transaction);
            }
        }

        public void UpdateSoLuong(SqlTransaction transaction, string dbName, long ID_HD, string LoaiHangHoa, string MaLoaiHinh, HangChuyenTiep HCTCu)
        {
            decimal i = 1;
            decimal SoLuongCu = HCTCu.SoLuong;
            if (!MaLoaiHinh.EndsWith("N") && !MaLoaiHinh.Substring(0, 1).Equals("N"))
            {
                i = -1;
            }
            else
            {
                i = 1;
            }

            if (LoaiHangHoa == "N")
            {
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = ID_HD;
                npl.Ma = this.MaHang.Trim();
                npl.Load(dbName);

                if (npl.Ma.Trim().ToUpper() == HCTCu.MaHang.Trim().ToUpper() || HCTCu.MaHang.Trim().Length == 0)
                {
                    if (i > 0)
                        npl.SoLuongDaNhap += (this.SoLuong) - SoLuongCu;
                    else
                        npl.SoLuongDaDung += (this.SoLuong) - SoLuongCu;
                }
                else
                {
                    if (i > 0)
                        npl.SoLuongDaNhap += this.SoLuong;
                    else
                        npl.SoLuongDaDung += this.SoLuong;

                    Company.GC.BLL.GC.NguyenPhuLieu nplCu = new Company.GC.BLL.GC.NguyenPhuLieu();
                    nplCu.HopDong_ID = ID_HD;
                    nplCu.Ma = HCTCu.MaHang;
                    nplCu.Load(dbName);

                    if (i > 0)
                        nplCu.SoLuongDaNhap -= HCTCu.SoLuong;
                    else
                        nplCu.SoLuongDaDung -= HCTCu.SoLuong;
                    //===
                    nplCu.UpdateTransaction(transaction, dbName);
                }
                //===
                npl.UpdateTransaction(transaction, dbName);
            }
            else if (LoaiHangHoa == "T")
            {
                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                tb.HopDong_ID = ID_HD;
                tb.Ma = this.MaHang.Trim();
                tb.Load(dbName);

                if (tb.Ma.Trim().ToUpper() == HCTCu.MaHang.Trim().ToUpper() || HCTCu.MaHang.Trim().Length == 0)
                {
                    tb.SoLuongDaNhap += this.SoLuong * i + SoLuongCu;
                }
                else
                {
                    if (i > 0)
                        tb.SoLuongDaNhap += this.SoLuong;
                    else
                        tb.SoLuongDaNhap -= this.SoLuong;

                    Company.GC.BLL.GC.ThietBi tbCu = new Company.GC.BLL.GC.ThietBi();
                    tbCu.HopDong_ID = ID_HD;
                    tbCu.Ma = HCTCu.MaHang;
                    tbCu.Load(dbName);

                    if (i > 0)
                        tbCu.SoLuongDaNhap -= HCTCu.SoLuong;
                    else
                        tbCu.SoLuongDaNhap += HCTCu.SoLuong;
                }
                //===
                tb.UpdateTransaction(transaction, dbName);
            }
            else
            {
                if (MaLoaiHinh.EndsWith("X") || MaLoaiHinh.Substring(0, 1).Equals("X"))
                {
                    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                    sp.HopDong_ID = ID_HD;
                    sp.Ma = this.MaHang.Trim();
                    sp.Load(dbName);

                    if (sp.Ma.Trim().ToUpper() == HCTCu.MaHang.Trim().ToUpper() || HCTCu.MaHang.Trim().Length == 0)
                    {
                        sp.SoLuongDaXuat += (this.SoLuong * i + SoLuongCu) * (-1);
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham spCu = new Company.GC.BLL.GC.SanPham();
                        spCu.HopDong_ID = ID_HD;
                        spCu.Ma = HCTCu.MaHang;
                        spCu.Load(dbName);

                        spCu.SoLuongDaXuat -= HCTCu.SoLuong;
                        //===
                        spCu.UpdateTransaction(transaction, dbName);

                        sp.SoLuongDaXuat += this.SoLuong;
                    }
                    //===
                    sp.UpdateTransaction(transaction, dbName);

                    if (HCTCu.ID > 0)
                    {
                        DataSet dsLuongNPLCu = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HCTCu.MaHang, HCTCu.SoLuong, ID_HD, transaction);
                        foreach (DataRow row in dsLuongNPLCu.Tables[0].Rows)
                        {
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.HopDong_ID = ID_HD;
                            npl.Ma = row["MaNguyenPhuLieu"].ToString();
                            npl.Load(dbName);

                            npl.SoLuongDaDung -= Convert.ToDecimal(row["LuongCanDung"]);
                            //===
                            npl.UpdateTransaction(transaction, dbName);
                        }
                    }

                    DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(this.MaHang.Trim(), this.SoLuong, ID_HD, transaction);
                    foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl.HopDong_ID = ID_HD;
                        npl.Ma = row["MaNguyenPhuLieu"].ToString();
                        npl.Load(dbName);

                        npl.SoLuongDaDung += Convert.ToDecimal(row["LuongCanDung"]);
                        //===
                        npl.UpdateTransaction(transaction, dbName);
                    }
                }

            }
        }

        public void Delete(SqlTransaction transaction, long ID_HD, string LoaiHangHoa, string MaLoaiHinh)
        {
            decimal i = 1;
            if (!MaLoaiHinh.Trim().EndsWith("N") || !MaLoaiHinh.Trim().Substring(0, 1).Equals("N"))
                i = -1;
            if (LoaiHangHoa == "N")
            {
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = ID_HD;
                npl.Ma = this.MaHang.Trim();
                npl.Load(transaction);
                if (i > 0)
                    npl.SoLuongDaNhap -= (this.SoLuong);
                else
                    npl.SoLuongDaDung -= (this.SoLuong);
                npl.UpdateTransaction(transaction);
            }
            else if (LoaiHangHoa == "S")
            {
                if (MaLoaiHinh.Trim().EndsWith("X") || MaLoaiHinh.Trim().Substring(0, 1).Equals("X"))
                {
                    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                    sp.HopDong_ID = ID_HD;
                    sp.Ma = this.MaHang.Trim();
                    sp.Load(transaction);
                    sp.SoLuongDaXuat -= this.SoLuong;
                    sp.UpdateTransaction(transaction);
                    if (this.ID > 0)
                    {
                        DataSet dsLuongNPLCu = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(this.MaHang, this.SoLuong, ID_HD, transaction);
                        foreach (DataRow row in dsLuongNPLCu.Tables[0].Rows)
                        {
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.HopDong_ID = ID_HD;
                            npl.Ma = row["MaNguyenPhuLieu"].ToString();
                            npl.Load(transaction);
                            npl.SoLuongDaDung -= Convert.ToDecimal(row["LuongCanDung"]);
                            npl.UpdateTransaction(transaction);
                        }
                    }
                }
            }
            else if (LoaiHangHoa == "T")
            {
                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                tb.HopDong_ID = ID_HD;
                tb.Ma = this.MaHang.Trim();
                tb.Load(transaction);
                tb.SoLuongDaNhap += this.SoLuong * i;
                tb.UpdateTransaction(transaction);
            }
            this.Delete(transaction);
        }

        public void Delete(long ID_HD, string LoaiHangHoa, string MaLoaiHinh)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    Delete(transaction, ID_HD, LoaiHangHoa, MaLoaiHinh);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public decimal GetSoluongHangTKGCCT(long ID, string maHang)
        {
            string sql = " SELECT SoLuong "
                         + " from   t_KDT_GC_HangChuyenTiep  "
                         + " where  Master_ID =@ID AND MaHang=@MaHang ";
            decimal temp = 0;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@MaHang", SqlDbType.Char, maHang);
            try
            {
                temp = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            { }
            return temp;

        }

        public HangChuyenTiep SaoChepHCT()
        {
            HangChuyenTiep entity = new HangChuyenTiep();
            entity.ID = this.ID;
            entity.SoThuTuHang = this.SoThuTuHang;
            entity.MaHS = this.MaHS;
            entity.MaHang = this.MaHang;
            entity.TenHang = this.TenHang;
            entity.SoLuong = this.SoLuong;
            entity.DonGia = this.DonGia;
            entity.ID_NuocXX = this.ID_NuocXX;
            entity.ID_DVT = this.ID_DVT;
            entity.TriGia = this.TriGia;
            entity.Master_ID = this.Master_ID;
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public List<HangChuyenTiep> SelectCollectionBy_Master_ID()
        {
            string spName = "p_KDT_GC_HangChuyenTiep_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this.Master_ID);

            List<HangChuyenTiep> collection = new List<HangChuyenTiep>();
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                HangChuyenTiep entity = new HangChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_NuocXX"))) entity.ID_NuocXX = reader.GetString(reader.GetOrdinal("ID_NuocXX"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_DVT"))) entity.ID_DVT = reader.GetString(reader.GetOrdinal("ID_DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public List<HangChuyenTiep> SelectCollectionBy_Master_ID_KTX()
        {
            string spName = "p_KDT_GC_HangChuyenTiep_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this.Master_ID);

            List<HangChuyenTiep> collection = new List<HangChuyenTiep>();
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                HangChuyenTiep entity = new HangChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_NuocXX"))) entity.ID_NuocXX = reader.GetString(reader.GetOrdinal("ID_NuocXX"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_DVT"))) entity.ID_DVT = reader.GetString(reader.GetOrdinal("ID_DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public List<HangChuyenTiep> SelectCollectionBy_Master_ID(SqlTransaction transaction, string dbName)
        {
            string spName = "p_KDT_GC_HangChuyenTiep_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(dbName);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this.Master_ID);

            List<HangChuyenTiep> collection = new List<HangChuyenTiep>();
            IDataReader reader = null;
            if (transaction != null)
                reader = db.ExecuteReader(dbCommand, transaction);
            else
                reader = db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                HangChuyenTiep entity = new HangChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_NuocXX"))) entity.ID_NuocXX = reader.GetString(reader.GetOrdinal("ID_NuocXX"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_DVT"))) entity.ID_DVT = reader.GetString(reader.GetOrdinal("ID_DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public List<HangChuyenTiep> SelectCollectionBy_Master_IDKTX(SqlTransaction transaction, string dbName)
        {
            string spName = "p_KDT_GC_HangChuyenTiep_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(dbName);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this.Master_ID);

            List<HangChuyenTiep> collection = new List<HangChuyenTiep>();
            IDataReader reader = null;
            if (transaction != null)
                reader = db.ExecuteReader(dbCommand, transaction);
            else
                reader = db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                HangChuyenTiep entity = new HangChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_NuocXX"))) entity.ID_NuocXX = reader.GetString(reader.GetOrdinal("ID_NuocXX"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_DVT"))) entity.ID_DVT = reader.GetString(reader.GetOrdinal("ID_DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public int DeleteBy_Master_ID(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_HangChuyenTiep_DeleteBy_Master_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);

            return db.ExecuteNonQuery(dbCommand, transaction);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NChar, MaHS);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@ID_NuocXX", SqlDbType.Char, ID_NuocXX);
            db.AddInParameter(dbCommand, "@ID_DVT", SqlDbType.Char, ID_DVT);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Money, TriGia);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
            db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
            db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
            db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
            db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, ThueSuatXNKGiam);
            db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, ThueSuatTTDBGiam);
            db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, ThueSuatVATGiam);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        public long InsertTransaction(SqlTransaction transaction, string databaseName)
        {
            const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(databaseName);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NChar, MaHS);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@ID_NuocXX", SqlDbType.Char, ID_NuocXX);
            db.AddInParameter(dbCommand, "@ID_DVT", SqlDbType.Char, ID_DVT);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Money, TriGia);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
            db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
            db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
            db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
            db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, ThueSuatXNKGiam);
            db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, ThueSuatTTDBGiam);
            db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, ThueSuatVATGiam);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransactionKTX(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NChar, MaHS);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@ID_NuocXX", SqlDbType.Char, ID_NuocXX);
            db.AddInParameter(dbCommand, "@ID_DVT", SqlDbType.Char, ID_DVT);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Money, TriGia);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        public long InsertTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            const string spName = "[dbo].[p_KDT_GC_HangChuyenTiep_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(databaseName);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NChar, MaHS);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@ID_NuocXX", SqlDbType.Char, ID_NuocXX);
            db.AddInParameter(dbCommand, "@ID_DVT", SqlDbType.Char, ID_DVT);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Money, TriGia);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        public int InsertUpdate(SqlTransaction transaction, string databaseName)
        {
            const string spName = "p_KDT_GC_HangChuyenTiep_InsertUpdateBy";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(databaseName);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NChar, MaHS);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@ID_NuocXX", SqlDbType.Char, ID_NuocXX);
            db.AddInParameter(dbCommand, "@ID_DVT", SqlDbType.Char, ID_DVT);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Money, TriGia);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
            db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
            db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
            db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
            db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.VarChar, ThueSuatXNKGiam);
            db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.VarChar, ThueSuatTTDBGiam);
            db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.VarChar, ThueSuatVATGiam);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateKTX(SqlTransaction transaction, string databaseName)
        {
            const string spName = "p_KDT_GC_HangChuyenTiep_InsertUpdateByKTX";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(databaseName);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NChar, MaHS);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@ID_NuocXX", SqlDbType.Char, ID_NuocXX);
            db.AddInParameter(dbCommand, "@ID_DVT", SqlDbType.Char, ID_DVT);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Money, TriGia);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

    }
}