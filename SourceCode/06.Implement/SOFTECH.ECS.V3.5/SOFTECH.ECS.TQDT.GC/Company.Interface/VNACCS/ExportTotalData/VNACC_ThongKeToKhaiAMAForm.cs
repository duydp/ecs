﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.VNACCS.ExportTotalData
{
    public partial class VNACC_ThongKeToKhaiAMAForm : BaseForm
    {
        public VNACC_ThongKeToKhaiAMAForm()
        {
            InitializeComponent();
        }

        private void VNACC_ThongKeToKhaiAMAForm_Load(object sender, EventArgs e)
        {
            ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            clcDenNgay.Value = clcTuNgay.Value.AddDays(30);
            cbbLoaiBC.SelectedIndex = 0;
            btnTimKiem_Click(null, null);
        }
        private string GetSeachWhere()
        {
            try
            {
                //this.Cursor = Cursors.WaitCursor;

                string where = String.Empty;
                where = "TKBS.MaNguoiKhai='" + GlobalSettings.MA_DON_VI + "' ";
                //where = where + " AND TrangThaiXuLy =3 ";
                if (!String.IsNullOrEmpty(ctrMaHaiQuan.Code.Trim()))
                    where = where + " AND TKBS.CoQuanHaiQuan = '" + ctrMaHaiQuan.Code.Trim() + "' ";
                if (!String.IsNullOrEmpty(txtSoTKVNACCS.Text.Trim()))
                    where = where + " AND TKBS.SoToKhai LIKE '%" + txtSoTKVNACCS.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(txtSoTKBS.Text.Trim()))
                    where = where + " AND TKBS.SoToKhaiBoSung LIKE '%" + txtSoTKBS.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(cbbLoaiBC.SelectedValue.ToString()))
                {
                    string LoaiBaoCao = cbbLoaiBC.SelectedValue.ToString();
                    switch (LoaiBaoCao)
                    {
                        case "0":                            
                            break;
                        case "I":
                            where = where + " AND TKBS.PhanLoaiXuatNhapKhau='I' ";
                            break;
                        case "E":
                            where = where + " AND TKBS.PhanLoaiXuatNhapKhau='E' ";
                            break;
                    }
                }

                DateTime fromDate = clcTuNgay.Value;
                DateTime toDate = clcDenNgay.Value;
                where = where + " AND (TKBS.NgayKhaiBao Between '" + fromDate.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + toDate.ToString("yyyy-MM-dd 23:59:59") + "')";

                if (!String.IsNullOrEmpty(txtMaHangHoa.Text.Trim()))
                    where = where + " AND HMDKBS.MoTaHangHoaTruocKhiKhaiBoSung LIKE '%" + txtMaHangHoa.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(txtTenHangHoa.Text.Trim()))
                    where = where + " AND HMDKBS.MoTaHangHoaTruocKhiKhaiBoSung LIKE '%" + txtTenHangHoa.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(txtMaHS.Text.Trim()))
                    where = where + " AND HMDKBS.MaSoHangHoaTruocKhiKhaiBoSung LIKE '%" + txtMaHS.Text.Trim() + "%' ";

                return where;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1 ";
            }

        }
        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                grList.Refresh();
                grList.DataSource = KDT_VNACC_ToKhaiMauDich_KhaiBoSung.ThongKeToKhaiAMA(GetSeachWhere(), "").Tables[0];
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Bảng Thống kê Tờ khai khai bổ sung AMA  Từ ngày_" + clcTuNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + " Đến ngày _" + clcDenNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = grList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
