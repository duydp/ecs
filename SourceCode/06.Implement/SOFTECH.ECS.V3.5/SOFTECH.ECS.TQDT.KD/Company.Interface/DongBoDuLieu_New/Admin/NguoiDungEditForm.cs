﻿using System;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.Security.Cryptography;
using Company.QuanTri;
using DuLieuChuan = Company.KDT.SHARE.Components.DuLieuChuan;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KD.BLL.Utils;
#elif KD_V3 || KD_V4
using Company.BLL.KDT;
using Company.BLL.Utils;
#endif
using Company.KDT.SHARE.Components;
using Janus.Windows.GridEX;
using Company.Interface;
using Helper.Controls;
using Company.Interface.DongBoDuLieu_New;

namespace DongBoDuLieu_New.Admin
{
    public partial class NguoiDungEditForm : Company.Interface.BaseForm
    {
        public DaiLy user = new DaiLy();
        public string passwordLogin = "", userLogin = "";
        public bool isCheck;

        public NguoiDungEditForm()
        {
            InitializeComponent();
            ServiceDongBoDuLieu.GetUrlServiceDongBoDuLieu();
        }

        private bool Login()
        {
            try
            {
               // this.Cursor = Cursors.WaitCursor;

               // Company.Interface.WSDBDLForm wsForm = new Company.Interface.WSDBDLForm();
               // if (GlobalSettings.PASSWOR_DONGBO == "")
               // {
               //     wsForm.ShowDialog(this);
               //     if (!wsForm.IsReady) return false;
               // }
               // passwordLogin = GlobalSettings.PASSWOR_DONGBO != "" ? GlobalSettings.PASSWOR_DONGBO : wsForm.txtMatKhau.Text.Trim();
               // userLogin = GlobalSettings.USERNAME_DONGBO != "" ? GlobalSettings.USERNAME_DONGBO : wsForm.txtMaDoanhNghiep.Text.Trim();

               //DaiLy userDaiLy = ServiceDongBoDuLieu.myService.LoginDaiLy(userLogin, passwordLogin);
               // if (userDaiLy == null || (userDaiLy.MaDoanhNghiep != GlobalSettings.MA_DON_VI && !userDaiLy.USER_NAME.Equals("administrator")))
               // {
               //     MessageBoxControlV.ShowMessage("Người dùng không hợp lệ!", false);
               //     userLogin = "";
                return false;
               // }

               // return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid)
                    return;
                if (userLogin == "")
                {
                    if (!Login())
                        return;
                }
                if (user.ID == 0)
                {
                    if (txtMatKhau.Text.Length == 0)
                    {
                        epError.SetError(txtMatKhau, "Mật khẩu bắt buộc phải nhập");
                        epError.SetIconPadding(txtMatKhau, -8);
                        return;
                    }
                    if (editBox1.Text.Length == 0)
                    {
                        epError.SetError(editBox1, "Mật khẩu nhập lại bắt buộc phải nhập");
                        epError.SetIconPadding(editBox1, -8);
                        return;
                    }
                    if (editBox1.Text.Trim().ToUpper() != txtMatKhau.Text.Trim().ToUpper())
                    {
                        epError.SetError(editBox1, "Mật khẩu không giống nhau");
                        epError.SetIconPadding(editBox1, -8);
                        return;
                    }

                }
                else
                {
                    if (txtMatKhau.Text.Trim().Length > 0)
                    {
                        if (editBox1.Text.Trim().ToUpper() != txtMatKhau.Text.Trim().ToUpper())
                        {
                            epError.SetError(editBox1, "Mật khẩu không giống nhau");
                            epError.SetIconPadding(editBox1, -8);
                            return;
                        }
                    }
                }
                if (ServiceDongBoDuLieu.myService.CheckUserName(txtUser.Text.Trim()))
                {
                    MessageBoxControlV.ShowMessage("Tên người dùng này đã tồn tại trong hệ thống", false);
                    return;
                }
                user.USER_NAME = txtUser.Text.Trim();
                user.HO_TEN = txtHoTen.Text.Trim();
                user.MO_TA = txtMoTa.Text.Trim();
                if (user.ID == 0)
                    //user.PASSWORD = EncryptPassword(txtMatKhau.Text.Trim());
                    user.PASSWORD = ServiceDongBoDuLieu.GetMD5Value(txtMatKhau.Text.Trim());
                else
                {
                    if (txtMatKhau.Text.Trim().Length > 0)
                        user.PASSWORD = ServiceDongBoDuLieu.GetMD5Value(txtMatKhau.Text.Trim());
                }

                int id = ServiceDongBoDuLieu.myService.InsertUpdateUserDaiLy(userLogin, passwordLogin, user.USER_NAME, user.PASSWORD, user.HO_TEN, user.MO_TA, cbIsAdmin.Checked, txtMaDN.Text);
                if (id == 1)
                    MessageBoxControlV.ShowMessage("Lưu thành công!", false);
                else
                    MessageBoxControlV.ShowMessage("Không lưu được", false);
                this.Close();
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
        }

        private void NguoiDungEditForm_Load(object sender, EventArgs e)
        {
            txtMaDN.Text = user.MaDoanhNghiep;
            txtHoTen.Text = user.HO_TEN;
            txtMoTa.Text = user.MO_TA;
            txtUser.Text = user.USER_NAME;
            cbIsAdmin.Visible = true;// Hiển thị checkBox isAdmin
            cbIsAdmin.Checked = user.isAdmin;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["CheckData"].Value.ToString() == "True")
            {
                e.Row.CheckState = RowCheckState.Checked;
            }
            else
                e.Row.CheckState = RowCheckState.Unchecked;
        }

    }
}