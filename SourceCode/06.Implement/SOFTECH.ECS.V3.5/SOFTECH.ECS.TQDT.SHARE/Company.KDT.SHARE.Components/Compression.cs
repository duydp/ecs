﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.BZip2;
using ICSharpCode.SharpZipLib.Zip;

namespace Company.KDT.SHARE.Components
{
    public class Compression
    {
        // Fields
        public static CompressionType CompressionProvider;

        // Methods
        public static byte[] Compress(byte[] bytesToCompress)
        {
            MemoryStream inputStream = new MemoryStream();
            Stream stream2 = OutputStream(inputStream);
            stream2.Write(bytesToCompress, 0, bytesToCompress.Length);
            stream2.Close();
            return inputStream.ToArray();
        }

        public static string Compress(string stringToCompress)
        {
            return Convert.ToBase64String(CompressToByte(stringToCompress));
        }

        public static byte[] CompressToByte(string stringToCompress)
        {
            return Compress(Encoding.Unicode.GetBytes(stringToCompress));
        }

        public static string DeCompress(string stringToDecompress)
        {
            string str = string.Empty;
            if (stringToDecompress == null)
            {
                throw new ArgumentNullException("stringToDecompress", "You tried to use an empty string");
            }
            try
            {
                byte[] bytes = DeCompress(Convert.FromBase64String(stringToDecompress.Trim()));
                str = Encoding.Unicode.GetString(bytes, 0, bytes.Length);
            }
            catch (NullReferenceException exception1)
            {
                return exception1.Message;
            }
            return str;
        }

        public static byte[] DeCompress(byte[] bytesToDecompress)
        {
            byte[] buffer = new byte[0x1000];
            Stream stream = InputStream(new MemoryStream(bytesToDecompress));
            MemoryStream stream2 = new MemoryStream();
            while (true)
            {
                int count = stream.Read(buffer, 0, buffer.Length);
                if (count <= 0)
                {
                    stream.Close();
                    byte[] buffer2 = stream2.ToArray();
                    stream2.Close();
                    return buffer2;
                }
                stream2.Write(buffer, 0, count);
            }
        }

        private static Stream InputStream(Stream inputStream)
        {
            switch (CompressionProvider)
            {
                case CompressionType.GZip:
                    return new GZipInputStream(inputStream);

                case CompressionType.BZip2:
                    return new BZip2InputStream(inputStream);

                case CompressionType.Zip:
                    return new ZipInputStream(inputStream);
            }
            return new GZipInputStream(inputStream);
        }

        private static Stream OutputStream(Stream inputStream)
        {
            switch (CompressionProvider)
            {
                case CompressionType.GZip:
                    return new GZipOutputStream(inputStream);

                case CompressionType.BZip2:
                    return new BZip2OutputStream(inputStream);

                case CompressionType.Zip:
                    return new ZipOutputStream(inputStream);
            }
            return new GZipOutputStream(inputStream);
        }
    }
}
