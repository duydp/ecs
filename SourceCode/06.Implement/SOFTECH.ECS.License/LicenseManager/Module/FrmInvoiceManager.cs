using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmInvoiceManager : DevExpress.XtraEditors.XtraForm
    {
        DataSet ds = new DataSet();

        public FrmInvoiceManager()
        {
            InitializeComponent();
        }

        #region Adding Method
        private void DisplayGridHeader()
        {
            gridData.Columns["ID"].Caption = "Mã hóa đơn";
            gridData.Columns["CustomerID"].Caption = "Mã khách hàng";
            gridData.Columns["CustomerName"].Caption = "Tên khách hàng";
            gridData.Columns["EmployeeID"].Caption = "Mã nhân viên";
            gridData.Columns["UserName"].Caption = "Tên đăng nhập";
            gridData.Columns["CreateDate"].Caption = "Ngày tạo";
            gridData.Columns["Notes"].Caption = "Thông tin khác";

            gridData.Columns["EmployeeID"].Visible = false;
        }

        private void OpenForm(string id, bool isEdit)
        {
            if (!ClassLib.IsFormShowed("Inv_" + id))
            {
                FrmInvoiceInfo obj = new FrmInvoiceInfo(id);
                obj.MdiParent = this.MdiParent;
                obj.isEdit = isEdit;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Inv_" + id].Activate();
            }
        }

        public void RefreshData()
        {

            ds = new DataSet();
            ds.Tables.Add(Invoice.SelectAllBy().Tables[0].Copy());
            ds.Tables[0].TableName = "tblInvoice";
            ds.Tables.Add(this.tblInvoiceDetail());
            ds.Tables.Add(this.tblCustomer());
            ds.Tables.Add(this.tblEmployee());
            ds.Tables.Add(this.tblProduct());
            ds.Tables.Add(this.tblLicense());
            try
            {
                ds.Relations.Add("Chi tiết", ds.Tables["tblInvoice"].Columns["ID"], ds.Tables["tblInvoiceDetail"].Columns["InvoiceID"]);
                ds.Relations.Add("Khách hàng", ds.Tables["tblInvoice"].Columns["CustomerID"], ds.Tables["tblCustomer"].Columns["ID"], false);
                ds.Relations.Add("Nhân viên", ds.Tables["tblInvoice"].Columns["EmployeeID"], ds.Tables["tblEmployee"].Columns["ID"], false);

                ds.Tables["tblInvoiceDetail"].ChildRelations.Add(
                        "Sản phẩm", ds.Tables["tblInvoiceDetail"].Columns["ProductID"], ds.Tables["tblProduct"].Columns["ID"], false
                    );
                ds.Tables["tblInvoiceDetail"].ChildRelations.Add(
                        "Bản quyền", ds.Tables["tblInvoiceDetail"].Columns["LicenseID"], ds.Tables["tblLicense"].Columns["ID"], false
                    );
            }
            //catch (Exception ex) { MessageBox.Show(ex.Message + "\r\n-> Chi tiết:\r\n" + ex.StackTrace); }
            catch { }
            gridInvoice.DataSource = ds.Tables[0];

            if (txtName.Text.Trim().Length != 0)
                Invoke(new MethodInvoker(delegate { btnSearch_Click(null, EventArgs.Empty); }));

            this.DisplayGridHeader();
        }

        private DataTable tblCustomer()
        {
            DataTable dt = Customer.SelectAll().Tables[0].Copy();

            dt.Columns.Remove("CompanyOrDepartment");
            dt.Columns.Remove("PostalCode");
            dt.Columns.Remove("ContactTitle");
            dt.Columns.Remove("FaxNumber");

            dt.Columns["ID"].Caption = "Mã khách hàng";
            dt.Columns["Name"].Caption = "Tên khách hàng";
            dt.Columns["ContactFirstName"].Caption = "Tên";
            dt.Columns["ContactLastName"].Caption = "Họ";
            dt.Columns["BillingAddress"].Caption = "Địa chỉ";
            dt.Columns["StateOrProvince"].Caption = "Quận/Huyện";
            dt.Columns["City"].Caption = "Tỉnh/TP";
            dt.Columns["PhoneNumber"].Caption = "Số ĐT";
            dt.Columns["Email"].Caption = "Email";

            dt.TableName = "tblCustomer";

            return dt;
        }

        private DataTable tblProduct()
        {
            DataTable dt = Product.SelectAll().Tables[0].Copy();
            dt.Columns["ID"].Caption = "Mã sản phẩm";
            dt.Columns["Name"].Caption = "Tên sản phẩm";
            dt.Columns["Description"].Caption = "Mô tả";
            dt.TableName = "tblProduct";

            return dt;
        }

        private DataTable tblInvoiceDetail()
        {
            DataTable dt = InvoiceDetail.SelectAll().Tables[0].Copy();
            dt.Columns["ID"].Caption = "Mã hóa đơn";
            dt.Columns["ProductID"].Caption = "Mã sản phẩm";
            dt.Columns["LicenseID"].Caption = "Mã bản quyền";
            dt.Columns["SerialCode"].Caption = "Serial";
            dt.Columns["PaymentTerms"].Caption = "Thời hạn";
            dt.Columns["Price"].Caption = "Giá thành";

            dt.TableName = "tblInvoiceDetail";

            return dt;
        }

        private DataTable tblLicense()
        {
            DataTable dt = Activation.Server.License.SelectAll().Tables[0].Copy();
            dt.Columns["ID"].Caption = "Mã bản quyền";
            dt.Columns["Name"].Caption = "Tên bản quyền";
            dt.Columns["Description"].Caption = "Mô tả";
            dt.Columns["NumberClient"].Caption = "Số máy";
            dt.Columns["Term"].Caption = "Thời hạn";
            dt.TableName = "tblLicense";

            return dt;
        }

        private DataTable tblEmployee()
        {
            DataTable dt = Employee.SelectAll().Tables[0].Copy();
            dt.Columns.Remove("UserName");
            dt.Columns.Remove("PassWord");
            dt.Columns["ID"].Caption = "Mã nhân viên";
            dt.Columns["FirstName"].Caption = "Tên";
            dt.Columns["LastName"].Caption = "Họ";
            dt.Columns["Department"].Caption = "Bộ phận";
            dt.Columns["Title"].Caption = "Chức danh";
            dt.Columns["Email"].Caption = "Thư điện tử";
            dt.TableName = "tblEmployee";

            return dt;
        }

        #endregion

        private void FrmInvoiceManager_Load(object sender, EventArgs e)
        {
            this.RefreshData();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!ClassLib.IsFormShowed("Inv_New"))
            {
                FrmInvoiceInfo obj = new FrmInvoiceInfo(null);
                obj.MdiParent = this.MdiParent;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Inv_New"].Activate();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    this.OpenForm(ID, true);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            gridData.ClearColumnsFilter();
            gridData.ClearGrouping();
            gridData.ClearSorting();
            this.RefreshData();
        }

        private void gridInvoice_DoubleClick(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    this.OpenForm(ID, false);
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    string Message = "Bạn có muốn xóa hóa đơn: " + ID;
                    if (MessageBox.Show(Message, "Xóa hóa đơn", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Invoice.DeleteInvoice(ID);
                    }
                }
            }
            this.RefreshData();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                DataView dv = ds.Tables["tblInvoice"].DefaultView;
                dv.RowFilter = "CustomerName like '%" + txtName.Text.Trim() + "%'";
                gridInvoice.DataSource = dv;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

    }
}