﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK;
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class ReportTriGiaToKhaiNhap : DevExpress.XtraReports.UI.XtraReport
    {
        private int STT = 0;
        public ReportTriGiaToKhaiNhap()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dt, DateTime fromDate, DateTime toDate)
        {
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblNgay.Text = "TỪ NGÀY " + fromDate.ToString("dd/MM/yyyy") + " ĐẾN NGÀY " + toDate.ToString("dd/MM/yyyy");
            lblNgayIn.Text = DateTime.Today.ToString("dd/MM/yyyy");
            this.DataSource = dt;
            lblSoToKhai.DataBindings.Add("Text", this.DataSource, "SoToKhai");
            lblMaLoaiHinh.DataBindings.Add("Text", this.DataSource, "MaLoaiHinh");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, "NgayDangKy", "{0:dd/MM/yyyy}");
            lblTriGiaHang.DataBindings.Add("Text", this.DataSource, "TongTriGia", "{0:n2}");
            lblTongSoThue.DataBindings.Add("Text", this.DataSource, "TongTriGia", "{0:n2}");
            lblNguyenTe.DataBindings.Add("Text", this.DataSource, "NguyenTe_Id");
            lblNuocXK.DataBindings.Add("Text", this.DataSource, "NuocXK_Id");
            lblNhaXK.DataBindings.Add("Text", this.DataSource, "TenDonViDoiTac");
            lblSoHopDong.DataBindings.Add("Text", this.DataSource, "SoHopDong");
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        private void ReportTriGiaToKhaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

    }
}
