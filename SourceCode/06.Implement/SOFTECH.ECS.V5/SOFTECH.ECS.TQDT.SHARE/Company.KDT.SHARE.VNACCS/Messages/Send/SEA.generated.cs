using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class SEA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute SMC { get; set; }
public PropertiesAttribute APN { get; set; }
public PropertiesAttribute FNC { get; set; }
public PropertiesAttribute APT { get; set; }
public PropertiesAttribute EIC { get; set; }
public PropertiesAttribute APP { get; set; }
public PropertiesAttribute ENC { get; set; }
public PropertiesAttribute ENN { get; set; }
public PropertiesAttribute EDN { get; set; }
public PropertiesAttribute RCN { get; set; }
public PropertiesAttribute IP { get; set; }
public PropertiesAttribute ID { get; set; }
public PropertiesAttribute BAP { get; set; }
public PropertiesAttribute IMA { get; set; }
public PropertiesAttribute BCC { get; set; }
public PropertiesAttribute BAN { get; set; }
public PropertiesAttribute BAF { get; set; }
public PropertiesAttribute BAM { get; set; }
public PropertiesAttribute PCN { get; set; }
public PropertiesAttribute PCD { get; set; }
public PropertiesAttribute SCN { get; set; }
public PropertiesAttribute SCD { get; set; }
public PropertiesAttribute MTT { get; set; }
public PropertiesAttribute IPC { get; set; }
public PropertiesAttribute IPN { get; set; }
public PropertiesAttribute ESI { get; set; }
public PropertiesAttribute EEI { get; set; }
public PropertiesAttribute AD { get; set; }
public PropertiesAttribute RMK { get; set; }
public PropertiesAttribute PIC { get; set; }

public SEA()
        {
SMC = new PropertiesAttribute(13, typeof(string));
APN = new PropertiesAttribute(12, typeof(int));
FNC = new PropertiesAttribute(1, typeof(int));
APT = new PropertiesAttribute(4, typeof(string));
EIC = new PropertiesAttribute(1, typeof(string));
APP = new PropertiesAttribute(6, typeof(string));
ENC = new PropertiesAttribute(13, typeof(string));
ENN = new PropertiesAttribute(300, typeof(string));
EDN = new PropertiesAttribute(17, typeof(string));
RCN = new PropertiesAttribute(17, typeof(string));
IP = new PropertiesAttribute(35, typeof(string));
ID = new PropertiesAttribute(8, typeof(DateTime));
BAP = new PropertiesAttribute(7, typeof(string));
IMA = new PropertiesAttribute(300, typeof(string));
BCC = new PropertiesAttribute(2, typeof(string));
BAN = new PropertiesAttribute(20, typeof(string));
BAF = new PropertiesAttribute(20, typeof(string));
BAM = new PropertiesAttribute(210, typeof(string));
PCN = new PropertiesAttribute(17, typeof(string));
PCD = new PropertiesAttribute(8, typeof(DateTime));
SCN = new PropertiesAttribute(17, typeof(string));
SCD = new PropertiesAttribute(8, typeof(DateTime));
MTT = new PropertiesAttribute(2, typeof(string));
IPC = new PropertiesAttribute(6, typeof(string));
IPN = new PropertiesAttribute(35, typeof(string));
ESI = new PropertiesAttribute(8, typeof(DateTime));
EEI = new PropertiesAttribute(8, typeof(DateTime));
AD = new PropertiesAttribute(750, typeof(string));
RMK = new PropertiesAttribute(996, typeof(string));
PIC = new PropertiesAttribute(300, typeof(string));
TongSoByte = 3201;
}

}public partial class EnumGroupID
    {
}

}
