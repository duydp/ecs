using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Cryptography = Company.KDT.SHARE.Components.Cryptography;
namespace Company.KDT.SHARE.Components
{
    public enum HashAlgorithmName
    {
        MD5 = 1,
        SHA1 = 2,
        SHA256 = 3,
        SHA384 = 4,
        SHA512 = 5,
        SHA256RSA = 6,
    }
    public enum OutputDataType
    {
        Base64 = 1,
        Hex = 2
    }
    public class Cryptography
    {

        public Cryptography()
        {
        }

        public static HashAlgorithm GetHashAlgorithm(X509Certificate2 certificate)
        {
            if (certificate.SignatureAlgorithm.FriendlyName.ToUpper().Contains("MD5"))
                return Cryptography.GetHashAlgorithm(HashAlgorithmName.MD5);
            if (certificate.SignatureAlgorithm.FriendlyName.ToUpper().Contains("SHA1"))
                return Cryptography.GetHashAlgorithm(HashAlgorithmName.SHA1);
            throw new System.Exception("Unknown hash algorithm!");
        }

        public static HashAlgorithm GetHashAlgorithm(string certificate)
        {
            System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = new System.Security.Cryptography.X509Certificates.X509Certificate2();
            x509Certificate2.Import(certificate);
            return Cryptography.GetHashAlgorithm(x509Certificate2);
        }

        public static HashAlgorithm GetHashAlgorithm(HashAlgorithmName name)
        {
            switch (name)
            {
                case HashAlgorithmName.MD5:
                    return new System.Security.Cryptography.MD5CryptoServiceProvider();

                case HashAlgorithmName.SHA1:
                    return new System.Security.Cryptography.SHA1Managed();
            }
            throw new System.Exception("Unknown hash algorithm!");
        }

        public static string GetHashAlgorithmName(System.Security.Cryptography.X509Certificates.X509Certificate2 certificate)
        {
            if (certificate.SignatureAlgorithm.FriendlyName.ToUpper().Contains("MD5"))
                return 1.ToString();
            if (certificate.SignatureAlgorithm.FriendlyName.ToUpper().Contains("SHA1"))
                return 2.ToString();
            throw new System.Exception("Unknown hash algorithm!");
        }
        static X509Certificate2 X509Certificate2 = null;
        public static X509Certificate2 GetStoreX509Certificate2(string name)
        {
            if (X509Certificate2 != null && X509Certificate2.SubjectName.Name.ToString() == name) return X509Certificate2;
            X509Certificate2 x509Certificate2_1 = null;
            X509Store x509Store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            x509Store.Open(OpenFlags.ReadOnly);
            X509Certificate2Enumerator x509Certificate2Enumerator = x509Store.Certificates.GetEnumerator();
            while (x509Certificate2Enumerator.MoveNext())
            {
                X509Certificate2 x509Certificate2_2 = x509Certificate2Enumerator.Current;
                if (x509Certificate2_2.SubjectName.Name.ToString() == name)
                {
                    System.DateTime dateTime = System.DateTime.Now;
                    if (!(dateTime > x509Certificate2_2.NotAfter || dateTime < x509Certificate2_2.NotBefore))
                        x509Certificate2_1 = x509Certificate2_2;
                }
            }
            X509Certificate2 = x509Certificate2_1;
            return x509Certificate2_1;
        }

        public static X509Certificate2 GetStoreX509Certificate2New(string name)
        {
          //  if (X509Certificate2 != null && X509Certificate2.SubjectName.Name.ToString() == name) return X509Certificate2;
            X509Certificate2 x509Certificate2_1 = null;
            X509Store x509Store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            x509Store.Open(OpenFlags.ReadOnly);
            X509Certificate2Enumerator x509Certificate2Enumerator = x509Store.Certificates.GetEnumerator();
            while (x509Certificate2Enumerator.MoveNext())
            {
                X509Certificate2 x509Certificate2_2 = x509Certificate2Enumerator.Current;
                if (x509Certificate2_2.SubjectName.Name.ToString() == name)
                {
                    System.DateTime dateTime = System.DateTime.Now;
                    if (!(dateTime > x509Certificate2_2.NotAfter || dateTime < x509Certificate2_2.NotBefore))
                        x509Certificate2_1 = x509Certificate2_2;
                }
            }
            X509Certificate2 = x509Certificate2_1;
            return x509Certificate2_1;
        }

        public static List<System.Security.Cryptography.X509Certificates.X509Certificate2> GetX509CertificatedNames()
        {
            try
            {
                System.Collections.Generic.List<System.Security.Cryptography.X509Certificates.X509Certificate2> list =
    new System.Collections.Generic.List<System.Security.Cryptography.X509Certificates.X509Certificate2>();
                System.Security.Cryptography.X509Certificates.X509Store x509Store = new System.Security.Cryptography.X509Certificates.X509Store(System.Security.Cryptography.X509Certificates.StoreName.My, System.Security.Cryptography.X509Certificates.StoreLocation.CurrentUser);
                x509Store.Open(System.Security.Cryptography.X509Certificates.OpenFlags.ReadOnly);
                System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator x509Certificate2Enumerator = x509Store.Certificates.GetEnumerator();
                System.DateTime dateTime = System.DateTime.Now;

                while (x509Certificate2Enumerator.MoveNext())
                {
                    System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = x509Certificate2Enumerator.Current;
                    string s = x509Certificate2.SubjectName.Name.ToString();
                    if (s != "" && !(dateTime > x509Certificate2.NotAfter || dateTime < x509Certificate2.NotBefore))
                        list.Add(x509Certificate2);
                }
                return list;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        public static string SignHash(string data, HashAlgorithmName algorithmName, System.Security.Cryptography.X509Certificates.X509Certificate2 cert, OutputDataType datatype)
        {
            System.Security.Cryptography.HashAlgorithm hashAlgorithm = Cryptography.GetHashAlgorithm(algorithmName);
            byte[] bArr1 = System.Text.Encoding.UTF8.GetBytes(data);
            byte[] bArr2 = hashAlgorithm.ComputeHash(bArr1);
            System.Security.Cryptography.RSACryptoServiceProvider rsacryptoServiceProvider = (System.Security.Cryptography.RSACryptoServiceProvider)cert.PrivateKey;
            byte[] bArr3 = rsacryptoServiceProvider.SignHash(bArr2, System.Security.Cryptography.CryptoConfig.MapNameToOID(algorithmName.ToString()));
            return Cryptography.ToString(bArr3, datatype);
        }

        public static string ToBase64String(byte[] r)
        {
            return System.Convert.ToBase64String(r);
        }

        public static string ToHexString(byte[] r)
        {
            string s = System.String.Empty;
            for (int i = 0; i < r.Length; i++)
            {
                s += r[i].ToString("X2");
            }
            return s;
        }

        public static string ToString(byte[] r, OutputDataType type)
        {
            switch (type)
            {
                case OutputDataType.Base64:
                    return Cryptography.ToBase64String(r);

                case OutputDataType.Hex:
                    return Cryptography.ToHexString(r);
            }
            throw new System.Exception("Unknown output type!");
        }

    }

}

