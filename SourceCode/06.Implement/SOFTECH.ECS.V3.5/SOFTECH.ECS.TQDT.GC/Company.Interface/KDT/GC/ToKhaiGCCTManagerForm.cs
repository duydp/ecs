﻿using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.SXXK;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Company.GC.BLL.Utils;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.QuanLyChungTu;
using Infragistics.Excel;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.KDT.GC
{
    public partial class ToKhaiGCCTManagerForm : BaseForm
    {
        private List<ToKhaiChuyenTiep> tkctCollection = new List<ToKhaiChuyenTiep>();
        private List<ToKhaiChuyenTiep> tkctTempCollection = new List<ToKhaiChuyenTiep>();
        private string xmlCurrent = "";
        ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        public ToKhaiChuyenTiep TKCT_Select = null;
        public bool isChonTK_V4 = false;
        string msgInfor = string.Empty;
        FeedBackContent feedbackContent = null;
        int status = -1;
        public HopDong HD;

        public ToKhaiGCCTManagerForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            try
            {
                //string where = string.Format("[ID] LIKE '{0}'", GlobalSettings.MA_CUC_HAI_QUAN);
                this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void SetComboTrangthai(Janus.Windows.EditControls.UIComboBox cboTrangThai)
        {
            try
            {
                cboTrangThai.Items.Add("Không phê duyệt", Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET);
                cboTrangThai.Items.Add("Sửa tờ khai", Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET);
                cboTrangThai.Items.Add("Chờ hủy", Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY);
                cboTrangThai.Items.Add("Đã hủy", Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }

        private void ToKhaiGCCTManagerForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //An nut xac nhan
                XacNhan.Visible = Janus.Windows.UI.InheritableBoolean.False;
                //An nut in to khai (cu)
                ToKhaiViet.Visible = Janus.Windows.UI.InheritableBoolean.False;

                //txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
                this.khoitao_DuLieuChuan();
                //cbStatus.SelectedIndex = 0;
                //this.search();
                //SetComboTrangthai();

                dgList.RootTable.Columns["NgayDangKy"].Width = 120;

                Janus.Windows.UI.CommandBars.UICommand cmdCapNhatNPLTonTK = new Janus.Windows.UI.CommandBars.UICommand();
                cmdCapNhatNPLTonTK.Text = "Cập nhật nguyên phụ liệu tồn của tờ khai đã duyệt";
                cmdCapNhatNPLTonTK.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdCapNhatNPLTonTK_Click);
                uiContextMenu1.Commands.Add(cmdCapNhatNPLTonTK);

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        void cmdCapNhatNPLTonTK_Click(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                CapNhatNPLTonToKhai();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void CapNhatNPLTonToKhai()
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;

            try
            {
                ToKhaiChuyenTiep TKCT = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;

                if (TKCT == null)
                    return;

                if ((int)TKCT.SoToKhai == 0)
                {
                    ShowMessage("Tờ khai chưa được cấp số tờ khai.", false);
                    return;
                }

                if (ShowMessage("Bạn có chắc chắn muốn cập nhật thông tin nguyên phụ liệu tồn của tờ khai chuyển tiếp '" + TKCT.SoToKhai + "/" + TKCT.MaLoaiHinh.Trim() + "/" + TKCT.NamDK + "' này không?", true) == "Yes")
                {
                    if (TKCT.MaLoaiHinh.Substring(0, 1) == "N")
                    {
                        if (Company.GC.BLL.GC.PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap((int)TKCT.SoToKhai, TKCT.MaLoaiHinh, TKCT.MaHaiQuanTiepNhan, (short)TKCT.NgayDangKy.Year, TKCT.IDHopDong))
                        {
                            ShowMessage("Tờ khai chuyển tiếp có id = " + TKCT.ID + " đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                            return;
                        }

                        //Thuc hien xoa du lieu ton cu cua to khai
                        Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase db = (Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase)Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase();
                        using (System.Data.SqlClient.SqlConnection connection = (System.Data.SqlClient.SqlConnection)db.CreateConnection())
                        {
                            connection.Open();
                            System.Data.SqlClient.SqlTransaction transaction = connection.BeginTransaction();
                            try
                            {
                                if (TKCT.MaLoaiHinh.Substring(0, 1) == "N" && TKCT.LoaiHangHoa == "N")
                                {
                                    Company.GC.BLL.GC.NPLNhapTonThucTe.DeleteNPLTonByToKhai(
                                        (int)TKCT.SoToKhai,
                                        (short)TKCT.NgayDangKy.Year,
                                        TKCT.MaLoaiHinh,
                                        TKCT.MaHaiQuanTiepNhan, transaction);

                                }
                                transaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            finally
                            {
                                connection.Close();
                            }
                        }

                        //Load hang to khai
                        if (TKCT.HCTCollection.Count == 0)
                            TKCT.LoadHCTCollection();

                        TKCT.TransferToNPLTon(TKCT.NgayDangKy);

                        //Luu log xu ly cap nhat tay
                        string noiDung1 = string.Format("Thực hiện cập nhật lại thông tin nguyên phụ liệu tồn cũ của tờ khai chuyển tiếp tại Hợp đồng đã đăng ký: '{0}'. Tờ khai: {1}", TKCT.SoHopDongDV, TKCT.SoToKhai + "/" + TKCT.MaLoaiHinh + "/" + TKCT.NamDK);
                        Company.KDT.SHARE.Components.MessageTypes type1 = Company.KDT.SHARE.Components.MessageTypes.ToKhaiChuyenTiepNhap;
                        //if (TKCT.MaLoaiHinh.Substring(0, 1).Equals("X")) type1 = Company.KDT.SHARE.Components.MessageTypes.ToKhaiXuat;
                        Company.KDT.SHARE.Components.Globals.SaveMessage("",
                            TKCT.ID, TKCT.GUIDSTR, type1,
                            Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                            Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay,
                            noiDung1);

                        ShowMessage("Đã cập nhật thành công.", false);
                    }
                    //Khong kiem tra to khai xuat
                    //else
                    //{
                    //    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKCT.ID))
                    //    {
                    //        ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                    //        return;
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessageTQDT("Có lỗi khi thực hiện cập nhật nguyên phụ liệu tồn của tờ khai:\r\n" + ex.Message, false, true);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private ToKhaiChuyenTiep getTKCTByID(long id)
        {
            try
            {
                foreach (ToKhaiChuyenTiep tk in this.tkctCollection)
                {
                    if (tk.ID == id) return tk;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
                return null;
            }
        }
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    //if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Contains("XV"))
                    //   {
                    //       VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
                    //       f.TKMD = this.getTKMDVNACCS(System.Convert.ToInt32(e.Row.Cells["sotokhai"].Value.ToString().Trim()));
                    //       f.ShowDialog();
                    //   }
                    //else if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Contains("NV"))
                    //{
                    //    VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
                    //    f.TKMD = this.getTKMDVNACCS(System.Convert.ToInt32(e.Row.Cells["sotokhai"].Value.ToString().Trim()));
                    //    f.ShowDialog();
                    //}
                    //else
                    //{
                    ToKhaiGCChuyenTiepNhapForm f = new ToKhaiGCChuyenTiepNhapForm();
                    f.OpenType = OpenFormType.Edit;
                    f.TKCT = (ToKhaiChuyenTiep)e.Row.DataRow;

                    f.TKCT = ToKhaiChuyenTiep.Load(f.TKCT.ID);

                    f.TKCT.LoaiHangHoa = "N";
                    if (f.TKCT.MaLoaiHinh.IndexOf("SP") > 0 || f.TKCT.MaLoaiHinh.IndexOf("19") > 0)
                    {
                        if (f.TKCT.MaLoaiHinh.Trim().EndsWith("X") || f.TKCT.MaLoaiHinh.Substring(0, 1).Equals("X")) f.TKCT.LoaiHangHoa = "S";
                    }
                    else if (f.TKCT.MaLoaiHinh.IndexOf("TB") > 0 || f.TKCT.MaLoaiHinh.IndexOf("20") > 0)
                        f.TKCT.LoaiHangHoa = "T";

                    f.TKCT.LoadHCTCollection();
                    f.TKCT.LoadChungTuKem();

                    if (f.TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                        f.OpenType = OpenFormType.View;
                    if (f.TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                        f.OpenType = OpenFormType.Edit;
                    if (isChonTK_V4)
                    {
                        TKCT_Select = f.TKCT;
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                        return;
                    }
                    f.ShowDialog();
                    search();
                    //}
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private KDT_VNACC_ToKhaiMauDich getTKMDVNACCS(int Sotokhai)
        {
            try
            {
                KDT_VNACC_ToKhaiMauDich TKVNACC = new KDT_VNACC_ToKhaiMauDich();
                decimal sotokhaiVnacc = CapSoToKhai.GetSoTKVNACCS(Sotokhai);
                TKVNACC = KDT_VNACC_ToKhaiMauDich.Load(KDT_VNACC_ToKhaiMauDich.GetID(sotokhaiVnacc));
                TKVNACC.LoadFull();
                return TKVNACC;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
                return null;
            }
        }
        private void search()
        {
            //// Xây dựng điều kiện tìm kiếm.
            //string where = "1 = 1";
            //where += string.Format(" AND MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);

            //if (txtSoTiepNhan.TextLength > 0)
            //{
            //    where += " AND SoTiepNhan = " + txtSoTiepNhan.Value;
            //}

            //if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
            //    if (txtNamTiepNhan.TextLength > 0)
            //    {
            //        where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
            //    }
            //}
            //if (cbStatus.SelectedValue != null)
            //    where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;

            //// Thực hiện tìm kiếm.            
            //this.tkctCollection = ToKhaiChuyenTiep.SelectCollectionDynamic(where, "");
            //dgList.DataSource = this.tkctCollection;
            //this.setCommandStatus();

            //this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
        }

        private void setCommandStatus()
        {
            switch (Convert.ToInt32(status))
            {
                case -1:
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdCancel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSingleDownload.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                    cmdXuatToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    btnXoa.Enabled = true;
                    cmdCSDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    break;
                case 0:
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdCancel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSingleDownload.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                    cmdXuatToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    btnXoa.Enabled = false;
                    cmdCSDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    break;
                case 1:
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdCancel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSingleDownload.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    cmdXuatToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    btnXoa.Enabled = false;
                    cmdCSDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    break;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }
        private void Cancel()
        {
            MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            WSForm wsForm = new WSForm();
            string password = "";
            ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
            try
            {
                if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
                {
                    this.Cursor = Cursors.WaitCursor;
                    tkct = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                    tkct = ToKhaiChuyenTiep.Load(tkct.ID);

                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = tkct.ID;
                    if (sendXML.Load())
                    {
                        showMsg("MSG_STN01");
                        //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.","MSG_WRN05","", false);
                        return;
                    }
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();


                    long soTiepNhan = tkct.SoTiepNhan;

                    xmlCurrent = tkct.WSCancel(password);
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = tkct.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 3;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(password);
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private ToKhaiChuyenTiep search(object id)
        {
            try
            {
                foreach (ToKhaiChuyenTiep tkct in this.tkctCollection)
                {
                    if (tkct.ID == Convert.ToInt64(id))
                    {
                        return tkct;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
            return null;
        }
        private void Download()
        {
            //    MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            //    ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
            //    WSForm wsForm = new WSForm();
            //    string password = "";
            //    try
            //    {
            //        this.Cursor = Cursors.WaitCursor;
            //        GridEXSelectedItemCollection items = dgList.SelectedItems;
            //        foreach (GridEXSelectedItem i in items)
            //        {
            //            if (i.RowType == RowType.Record)
            //            {
            //                tkct = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
            //                tkct = ToKhaiChuyenTiep.Load(tkct.ID);

            //                sendXML.LoaiHS = "TKCT";
            //                sendXML.master_id = tkct.ID;
            //                if (sendXML.Load())
            //                {
            //                    showMsg("MSG_STN01");
            //                    //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_WRN05", "", false);
            //                    return;
            //                }


            //                int ttxl = tkct.TrangThaiXuLy;

            //                if (GlobalSettings.PassWordDT == "")
            //                {
            //                    wsForm.ShowDialog(this);
            //                    if (!wsForm.IsReady) return;
            //                }
            //                if (GlobalSettings.PassWordDT != "")
            //                    password = GlobalSettings.PassWordDT;
            //                else
            //                    password = wsForm.txtMatKhau.Text.Trim();
            //                this.Cursor = Cursors.WaitCursor;
            //                LayPhanHoi(password);

            //                break;
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        this.Cursor = Cursors.Default;
            //        ShowMessage(ex.Message, false);
            //    }
            //    finally
            //    {
            //        this.Cursor = Cursors.Default;
            //    }
        }

        private void Send()
        {
            ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
            MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            WSForm wsForm = new WSForm();
            string password = "";
            try
            {
                if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
                {
                    this.Cursor = Cursors.WaitCursor;
                    tkct = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                    tkct = ToKhaiChuyenTiep.Load(tkct.ID);
                    tkct.LoadHCTCollection();
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = tkct.ID;
                    if (sendXML.Load())
                    {
                        showMsg("MSG_STN01");
                        XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        // ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                        //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_WRN05", "", false);
                        return;
                    }

                    try
                    {
                        if (GlobalSettings.PassWordDT == "")
                        {
                            wsForm.ShowDialog(this);
                            if (!wsForm.IsReady) return;
                        }
                        if (GlobalSettings.PassWordDT != "")
                            password = GlobalSettings.PassWordDT;
                        else
                            password = wsForm.txtMatKhau.Text.Trim();
                        //xmlCurrent = SOFTECH.ECS.V3.Components.GC.SHARE.ProcessXML.SendTKNhapGCCT(password, tkct.ID);
                        sendXML = new MsgSend();
                        sendXML.LoaiHS = "TKCT";
                        sendXML.master_id = tkct.ID;
                        sendXML.msg = xmlCurrent;
                        sendXML.func = 1;
                        xmlCurrent = "";
                        sendXML.InsertUpdate();
                        LayPhanHoi(password);

                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                        {
                            string[] msg = ex.Message.Split('|');
                            if (msg.Length == 2)
                            {
                                if (msg[1] == "DOTNET_LEVEL")
                                {
                                    //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                    //{
                                    //    HangDoi hd = new HangDoi();
                                    //    hd.ID = TKMD.ID;
                                    //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                    //    hd.TrangThai = TKMD.TrangThaiXuLy;
                                    //    hd.ChucNang = ChucNang.KHAI_BAO;
                                    //    hd.PassWord = password;
                                    //    MainForm.AddToQueueForm(hd);
                                    //    MainForm.ShowQueueForm();
                                    //}
                                    showMsg("MSG_WRN12");
                                    //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN15", "", false);
                                    return;
                                }
                                else
                                {
                                    showMsg("MSG_2702016", msg[0]);
                                    //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN12", msg[0], false);
                                }
                            }
                            else
                            {
                                if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                                    showMsg("MSG_WRN13", ex.Message);
                                //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN16", ex.Message, false);
                                else
                                {
                                    GlobalSettings.PassWordDT = "";
                                    showMsg("MSG_2702004", ex.Message);
                                    //ShowMessage(ex.Message, false);
                                }
                                setCommandStatus();
                            }
                        }

                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Lỗi khi khai báo thông tin tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LaySoTiepNhanDT()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            ToKhaiChuyenTiep tkmd = new ToKhaiChuyenTiep();
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            string password = "";
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
                {
                    tkmd = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                    tkmd = ToKhaiChuyenTiep.Load(tkmd.ID);
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = tkmd.ID;
                    if (!sendXML.Load())
                    {
                        showMsg("MSG_STN01");
                        //MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.","MSG_STN01", "", false);
                        return;
                    }
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    this.Cursor = Cursors.WaitCursor;
                    {
                        xmlCurrent = tkmd.LayPhanHoi(password, sendXML.msg);
                    }
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        doc.LoadXml(xmlCurrent);
                        node = doc.SelectSingleNode("Megs");
                        if (node == null)
                        {
                            if (showMsg("MSG_STN02", true) == "Yes")
                            //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", "MSG_STN02", "", true) == "Yes")
                            {
                                this.Refresh();
                                LayPhanHoi(password);
                            }
                            return;
                        }
                    }

                    if (sendXML.func == 1)
                    {
                        showMsg("MSG_SEN02", tkmd.SoTiepNhan);
                        //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + tkmd.SoTiepNhan, "MSG_SEN02", tkmd.SoTiepNhan.ToString(), false);
                        this.search();
                    }
                    else if (sendXML.func == 3)
                    {
                        showMsg("MSG_2702029");
                        //MLMessages("Đã hủy tờ khai này", "MSG_CAN01", "", false);
                        this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (tkmd.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            string mess = "";
                            if (node != null)
                            {
                                if (node.HasChildNodes)
                                    mess += "\nCó phản hồi từ hải quan : \r\n";
                                foreach (XmlNode nodeCon in node.ChildNodes)
                                {
                                    mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                                }
                            }
                            showMsg("MSG_SEN11", new string[] { tkmd.SoToKhai.ToString(), mess });
                            //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", "MSG_SEN03", "", false);                            
                            this.search();
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            string mess = "";
                            if (node != null)
                            {
                                if (node.HasChildNodes)
                                    mess += "\nCó phản hồi từ hải quan : \r\n";
                                foreach (XmlNode nodeCon in node.ChildNodes)
                                {
                                    mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                                }
                            }
                            showMsg("MSG_SEN04", mess);
                            //MLMessages("Hải quan chưa xử lý tờ khai này!", "MSG_STN06", "", false);
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            showMsg("MSG_SEN05");
                            //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN05", "", false);
                            this.search();
                        }
                    }
                    setCommandStatus();
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0],"","", false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
            MsgSend sendXML = new MsgSend();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
                {
                    tkct = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;

                    tkct = ToKhaiChuyenTiep.Load(tkct.ID);
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = tkct.ID;
                    sendXML.Load();
                    this.Cursor = Cursors.WaitCursor;
                    {
                        //xmlCurrent = tkmd.LayPhanHoi(pass, sendXML.msg);
                        //xmlCurrent = SOFTECH.ECS.V3.Components.GC.ToKhaiChuyenTiep.LayPhanHoi(pass, sendXML.msg);
                    }
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        doc.LoadXml(xmlCurrent);
                        node = doc.SelectSingleNode("Megs");
                        if (node == null)
                        {
                            if (showMsg("MSG_STN02", true) == "Yes")
                            //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", "MSG_STN02", "", true) == "Yes")
                            {
                                this.Refresh();
                                LayPhanHoi(pass);
                            }
                            return;
                        }
                    }

                    if (sendXML.func == 1)
                    {
                        showMsg("MSG_SEN02", tkct.SoTiepNhan);
                        //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + tkmd.SoTiepNhan, "MSG_SEN02", "", false);
                        this.search();
                    }
                    else if (sendXML.func == 3)
                    {
                        showMsg("MSG_2702029");
                        //MLMessages("Đã hủy tờ khai này", "MSG_CAN01", "", false);
                        this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (tkct.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            string mess = "";
                            if (node != null)
                            {
                                if (node.HasChildNodes)
                                    mess += "\nCó phản hồi từ hải quan : \r\n";
                                foreach (XmlNode nodeCon in node.ChildNodes)
                                {
                                    mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                                }
                            }
                            showMsg("MSG_SEN11", new string[] { tkct.SoToKhai.ToString(), mess });
                            //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số tờ khai :" + tkmd.SoToKhai.ToString(), "MSG_SEN03", tkmd.SoToKhai.ToString(), false);                            
                            this.search();
                        }
                        else if (tkct.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            string mess = "";
                            if (node != null)
                            {
                                if (node.HasChildNodes)
                                    mess += "\nCó phản hồi từ hải quan : \r\n";
                                foreach (XmlNode nodeCon in node.ChildNodes)
                                {
                                    mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                                }
                            }
                            //showMsg("MSG_SEN04", mess);
                            //MLMessages("Hải quan chưa xử lý tờ khai này!", "MSG_STN06", "", false);

                            if (mess != "")
                                ShowMessage(mess, false);
                            else
                                ShowMessage("Hải quan chưa xử lý tờ khai chuyển tiếp này!", false);
                        }
                        else if (tkct.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            showMsg("MSG_SEN05");
                            //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN05", "", false);
                            this.search();
                        }
                    }
                    setCommandStatus();
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", "", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            showMsg("MSG_2702004", ex.Message);
                            //GlobalSettings.PassWordDT = "";
                            ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận  Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void cmMain_CommandClick_1(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

            if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
            {
                TKCT = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
            }
            else
            {
                ShowMessage("Chưa có tờ khai được chọn", false);
                return;
            }

            switch (e.Command.Key)
            {
                case "cmdSend":
                    SendV3();
                    break;
                case "cmdSingleDownload":
                    FeedBackV3();
                    break;
                case "cmdCancel":
                    if (ShowMessage("Bạn có thật sự muốn hủy khai báo tờ khai này không?", true) == "Yes")
                        CancelV3();
                    break;
                case "XacNhan":
                    FeedBackV3();
                    break;
                case "XacNhan1":
                    FeedBackV3();
                    break;
                case "cmdXuatToKhai":
                    XuatToKhaiGCCTChoPhongKhai();
                    break;
                case "ToKhaiViet":
                    try
                    {
                        Print();
                    }
                    catch { ShowMessage(" Lỗi dữ liệu, vui lòng kiểm tra lại ", false); }
                    break;
                case "cmdInTKGCCT":
                    try
                    {
                        this.PrintGCCTA4();
                    }
                    catch { ShowMessage(" Lỗi dữ liệu, vui lòng kiểm tra lại ", false); }
                    break;
                case "cmdInGCCTTQ":
                    try
                    {
                        this.PrintGCCTTQ();
                    }
                    catch { ShowMessage(" Không có dữ liệu ", false); }
                    break;
                case "cmdInSuaDoiBoSung":
                    this.InToKhaiDienTuSuaDoiBoSung();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdCSDaDuyet":
                    ChuyenTrangThai();
                    break;
                case "Copy":
                    SaoChepToKhai();
                    break;
                case "CopyHang":
                    SaoChepHangHoa();
                    break;
                case "CopyTK":
                    SaoChepHangHoa();
                    break;
                case "inTKGCCT_196":
                    InTKGCCT_196();
                    break;
                case "cmdInBangKe_HD":
                    InBangKe_HD_ToKhai();
                    break;
                case "cmdExportExcel":
                    btnExportExcel_Click(null,null);
                    break;
                default:
                    break;
            }
        }

        private void InBangKe_HD_ToKhai()
        {
            try
            {
                GridEXRow row = dgList.GetRow();
                if (row.RowType == RowType.Record)
                {
                    //ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                    //this.TKMD = GetTKMDSelected();
                    TKCT = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                    if (TKCT.ID == 0)
                        return;

                    if (TKCT != null && TKCT.HopDongThuongMaiCollection != null && TKCT.HopDongThuongMaiCollection.Count > 0)
                    {
                        Company.Interface.Report.BangKe_HD_TheoTKCT f = new Company.Interface.Report.BangKe_HD_TheoTKCT();
                        f.tkct = (ToKhaiChuyenTiep)row.DataRow;
                        f.BindReport_HopDong();
                        f.ShowPreview();
                    }
                    else
                        ShowMessage("Không tồn tại hợp đồng trong tờ khai này", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void fillNoiDungTKChinh(Worksheet worksheet, int row, int column)
        {
            int j = 1;
            NoiDungChinhSuaTKCTForm ndDieuChinhTKForm = new NoiDungChinhSuaTKCTForm();
            string where = "TKMD_ID = " + TKCT.ID + " AND MALOAIHINH = '" + TKCT.MaLoaiHinh + "'";
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionDynamic(where, "");
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                NoiDungChinhSuaTKCTDetailForm ndDieuChinhTKDetailForm = new NoiDungChinhSuaTKCTDetailForm();
                ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(ndDieuChinhTK.ID);
                foreach (NoiDungDieuChinhTKDetail ndDieuChinhTKChiTiet in ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail)
                {
                    if (row >= 13 && row <= 31)
                    {
                        worksheet.Rows[row].Cells[column].Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                        int thuong1 = worksheet.Rows[row].Cells[column].Value.ToString().Length / 54;
                        int sodu1 = worksheet.Rows[row].Cells[column].Value.ToString().Length % 54;
                        if (thuong1 == 0)
                        {
                            Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row, column + 10);
                            mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                            worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                            mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                            mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                            //j = thuong1;
                        }
                        else
                        {
                            if (sodu1 == 0)
                            {
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row + thuong1 - 1, column + 10);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                                worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                            else
                            {
                                thuong1 += 1;
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row + thuong1 - 1, column + 10);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                                worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                        }

                        if (thuong1 == 0)
                        {
                            row += 1;
                            j++;
                        }
                        else
                        {
                            row += thuong1;
                            j++;
                        }
                    }
                }
                //}
            }
        }

        private void fillNoiDungTKSua(Worksheet worksheet, int row, int column)
        {
            int j = 1;
            NoiDungChinhSuaTKCTForm ndDieuChinhTKForm = new NoiDungChinhSuaTKCTForm();
            string where = "TKMD_ID = " + TKCT.ID + " AND MALOAIHINH = '" + TKCT.MaLoaiHinh + "'";
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionDynamic(where, "");
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                //if (ndDieuChinhTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                //{
                NoiDungChinhSuaTKCTDetailForm ndDieuChinhTKDetailForm = new NoiDungChinhSuaTKCTDetailForm();
                ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(ndDieuChinhTK.ID);
                foreach (NoiDungDieuChinhTKDetail ndDieuChinhTKChiTiet in ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail)
                {
                    if (row >= 13 && row <= 31)
                    {
                        worksheet.Rows[row].Cells[column + 11].Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                        int thuong1 = worksheet.Rows[row].Cells[column + 11].Value.ToString().Length / 54;
                        int sodu1 = worksheet.Rows[row].Cells[column + 11].Value.ToString().Length % 54;
                        if (thuong1 == 0)
                        {
                            Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row, column + 20);
                            mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                            worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                            mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                            mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                            //j = thuong1;
                        }
                        else
                        {
                            if (sodu1 == 0)
                            {
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row + thuong1 - 1, column + 20);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                                worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                            else
                            {
                                thuong1 += 1;
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row + thuong1 - 1, column + 20);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                                worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                        }

                        if (thuong1 == 0)
                        {
                            row += 1;
                            j++;
                        }
                        else
                        {
                            row += thuong1;
                            j++;
                        }
                    }
                }
                //}
            }
        }
        private void checkTrangThaiXuLyTK()
        {
            NoiDungChinhSuaTKCTForm noiDungDieuChinhTKForm = new NoiDungChinhSuaTKCTForm();
            string where = "TKMD_ID = " + TKCT.ID + " AND MALOAIHINH = '" + TKCT.MaLoaiHinh + "'";
            noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionDynamic(where, "");
            //Nếu là tờ khai sửa đã được duyệt
            if (TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                        ndDieuChinhTK.Update();
                    }
                }
            }
            //Nếu là tờ khai sửa đang chờ duyệt
            else if (TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                        ndDieuChinhTK.Update();
                    }
                }
            }
            //Nếu là tờ khai sửa mới
            else
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        ndDieuChinhTK.Update();
                    }
                }
            }
        }
        private void InToKhaiDienTuSuaDoiBoSung()
        {
            checkTrangThaiXuLyTK();
            bool ok = true;
            NoiDungChinhSuaTKCTForm ndDieuChinhTKForm = new NoiDungChinhSuaTKCTForm();
            string where = "TKMD_ID = " + TKCT.ID + " AND MALOAIHINH = '" + TKCT.MaLoaiHinh + "'";
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionDynamic(where, "");
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                {
                    ShowMessage("Thông tin sửa đổi, bổ sung chưa được duyệt. Không thể in tờ khai sửa đổi bổ sung", false);
                    ok = false;
                }
            }
            if (ok)
            {
                //Lấy đường dẫn file gốc
                string destFile = "";
                string fileName = "Mau_so_8._Mau_To_khai_sua_doi_bo_sung.xls";
                string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

                //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
                try
                {
                    Workbook workBook = Workbook.Load(sourcePath);
                    Worksheet workSheet = workBook.Worksheets[0];

                    workSheet.Rows[5].Cells[6].Value = GlobalSettings.TEN_HAI_QUAN;
                    workSheet.Rows[5].Cells[6].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[5].Cells[6].CellFormat.Font.Height = 10 * 20;

                    if (TKCT.MaLoaiHinh.Contains("N"))
                    {
                        workSheet.Rows[7].Cells[6].Value = GlobalSettings.TEN_DON_VI;
                        workSheet.Rows[7].Cells[6].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[7].Cells[6].CellFormat.Font.Height = 10 * 20;
                    }
                    else
                    {
                        workSheet.Rows[7].Cells[6].Value = TKCT.TenKH;
                        workSheet.Rows[7].Cells[6].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[7].Cells[6].CellFormat.Font.Height = 10 * 20;
                    }

                    workSheet.Rows[9].Cells[3].Value = TKCT.SoToKhai;
                    workSheet.Rows[9].Cells[3].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[9].Cells[3].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[9].Cells[9].Value = TKCT.MaLoaiHinh;
                    workSheet.Rows[9].Cells[9].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[9].Cells[9].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[10].Cells[4].Value = TKCT.NgayTiepNhan;
                    workSheet.Rows[10].Cells[4].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[10].Cells[4].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[9].Cells[15].Value = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKCT.ID, TKCT.MaLoaiHinh);
                    workSheet.Rows[9].Cells[15].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[9].Cells[15].CellFormat.Font.Height = 10 * 20;

                    //Gán giá trị tương ứng vào các ô trong bảng nội dung sửa đổi
                    fillNoiDungTKChinh(workSheet, 13, 1);
                    fillNoiDungTKSua(workSheet, 13, 1);

                    //Chọn đường dẫn file cần lưu
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                    string dateNow = DateTime.Now.ToShortDateString().Replace("/", "");
                    saveFileDialog1.FileName = "ToKhaiSuaBoSung" + "_TK" + TKCT.SoToKhai + "_NamDK" + TKCT.NamDK + "_" + dateNow;
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        destFile = saveFileDialog1.FileName;
                        //Ghi nội dung file gốc vào file cần lưu
                        try
                        {
                            byte[] sourceFile = Globals.ReadFile(sourcePath);
                            Globals.WriteFile(destFile, sourceFile);
                            workBook.Save(destFile);
                            ShowMessage("Save file thành công", false);
                        }
                        catch (Exception ex)
                        {
                            ShowMessage(ex.Message, false);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message, false);
                    return;
                }
            }
        }
        private void InTKGCCT_196()
        {

            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                //tkct.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
                //tkct.Load();
                tkct.LoadHCTCollection();
                tkct.LoadPhuLucHD();
                string maloaihinh = dgList.CurrentRow.Cells["MaLoaiHinh"].Value.ToString().Trim();

                string maLH = maloaihinh.Substring(0, 1);

                if (maLH == "N")
                {
                    Report.ReportViewGCCTTQ_Nhan_TT196Form f = new Company.Interface.Report.ReportViewGCCTTQ_Nhan_TT196Form();
                    f.TKCT = (ToKhaiChuyenTiep)row.DataRow;
                    f.TKCT.LoadHCTCollection();
                    f.Show();
                }
                else
                {
                    Report.ReportViewGCCTTQ_Giao_TT196Form f = new Company.Interface.Report.ReportViewGCCTTQ_Giao_TT196Form();
                    f.TKCT = (ToKhaiChuyenTiep)row.DataRow;
                    f.TKCT.LoadHCTCollection();
                    f.Show();
                }


            }
        }
        private void SaoChepToKhai()
        {
            if (dgList.GetRow().DataRow != null && dgList.GetRow().RowType == RowType.Record)
            {
                ToKhaiGCChuyenTiepNhapForm f = new ToKhaiGCChuyenTiepNhapForm();
                ToKhaiChuyenTiep TKCT = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                f.TKCT.ID = TKCT.ID;
                f.TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                f.TKCT.ID = 0;
                f.TKCT.SoTiepNhan = 0;
                f.TKCT.TrangThaiXuLy = -1;
                f.TKCT.SoToKhai = 0;
                f.TKCT.PhanLuong = "";
                f.OpenType = OpenFormType.Edit;
                f.WindowState = FormWindowState.Maximized;
                f.ShowDialog();
            }
        }
        private void SaoChepHangHoa()
        {

            if (dgList.GetRow().DataRow != null && dgList.GetRow().RowType == RowType.Record)
            {
                ToKhaiGCChuyenTiepNhapForm f = new ToKhaiGCChuyenTiepNhapForm();
                ToKhaiChuyenTiep TKCT = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                f.TKCT.ID = TKCT.ID;
                f.TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                f.TKCT.LoadHCTCollection();
                f.TKCT.ID = 0;
                f.TKCT.SoTiepNhan = 0;
                f.TKCT.TrangThaiXuLy = -1;
                f.TKCT.SoToKhai = 0;
                f.TKCT.PhanLuong = "";
                f.TKCT.GUIDSTR = Guid.NewGuid().ToString();
                foreach (HangChuyenTiep HCT in f.TKCT.HCTCollection)
                    HCT.ID = 0;

                f.OpenType = OpenFormType.Edit;
                f.WindowState = FormWindowState.Maximized;
                f.ShowDialog();
            }
        }
        private void ChuyenTrangThai()
        {
            try
            {
                if (dgList.SelectedItems.Count > 0)
                {
                    List<ToKhaiChuyenTiep> tkctColl = new List<ToKhaiChuyenTiep>();
                    foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                    {
                        if (grItem.RowType == RowType.Record)
                        {
                            tkctColl.Add((ToKhaiChuyenTiep)grItem.GetRow().DataRow);
                        }
                    }

                    for (int i = 0; i < tkctColl.Count; i++)
                    {

                        tkctColl[i].LoadHCTCollection();

                        //string msg = "Bạn có muốn chuyển trạng thái của tờ khai được chọn sang đã duyệt không?";
                        //msg += "\n\nSố thứ tự tờ khai: " + tkctColl[i].ID.ToString();
                        //msg += "\n----------------------";
                        //msg += "\nCó " + tkctColl[i].HCTCollection.Count.ToString() + " sản phẩm đăng ký";

                        string[] args = new string[2];
                        args[0] = tkctColl[i].ID.ToString();
                        args[1] = tkctColl[i].HCTCollection.Count.ToString();

                        if (showMsg("MSG_0203073", args, true) == "Yes")
                        {
                            ChuyenTrangThaiTKCT obj = new ChuyenTrangThaiTKCT(tkctColl[i]);
                            obj.ShowDialog();
                        }
                    }
                    this.search();
                }
                else
                {
                    showMsg("MSG_240233");
                    //MLMessages("Không có dữ liệu được chọn!", "MSG_WRN14", "", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void XuatToKhaiGCCTChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                showMsg("MSG_0203007");
                //MLMessages("Chưa chọn danh sách tờ khai cần xuất ra file.","MSG_WRN14","", false);
                return;
            }
            try
            {
                List<ToKhaiChuyenTiep> col = new List<ToKhaiChuyenTiep>();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<ToKhaiChuyenTiep>));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sotokhai = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiChuyenTiep tkmdSelected = (ToKhaiChuyenTiep)i.GetRow().DataRow;
                            tkmdSelected.LoadHCTCollection();
                            col.Add(tkmdSelected);
                            sotokhai++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    showMsg("MSG_EXC05", sotokhai);
                    //MLMessages("Xuất ra file thành công " + sotokhai + " tờ khai GCCT.", "MSG_EXC05", sotokhai.ToString(), false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }

        }
        private void inPhieuTN()
        {
            try
            {
                if (!(dgList.GetRows().Length > 0 && dgList.GetRow().RowType == RowType.Record)) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
                phieuTNForm.TenPhieu = "HỢP ĐỒNG";
                Company.Interface.Report.GC.PhieuTN phieuTNAll = new Company.Interface.Report.GC.PhieuTN();
                string[,] arrPhieuTN = new string[items.Count, 2];
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ToKhaiChuyenTiep tkctDangKySelected = (ToKhaiChuyenTiep)i.GetRow().DataRow;
                        arrPhieuTN[j, 0] = tkctDangKySelected.SoTiepNhan.ToString();
                        arrPhieuTN[j, 1] = tkctDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                        j++;
                        break;
                    }
                }
                phieuTNForm.phieuTN = arrPhieuTN;
                phieuTNForm.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void Print()
        {
            try
            {
                GridEXRow row = dgList.GetRow();
                if (row.RowType == RowType.Record)
                {
                    Report.ReportViewTKCTForm f = new Company.Interface.Report.ReportViewTKCTForm();
                    f.TKCT = (ToKhaiChuyenTiep)row.DataRow;
                    f.TKCT.LoadHCTCollection();
                    f.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void PrintGCCTA4()
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                Report.ReportViewGCCTA4Form f = new Report.ReportViewGCCTA4Form();
                f.TKCT = (ToKhaiChuyenTiep)row.DataRow;
                f.TKCT.LoadHCTCollection();
                f.Show();
            }
        }
        private void PrintGCCTTQ()
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                Report.ReportViewGCCTTQForm f = new Report.ReportViewGCCTTQForm();
                f.TKCT = (ToKhaiChuyenTiep)row.DataRow;
                f.TKCT.LoadHCTCollection();
                f.Show();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string maLH = e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim();
                    e.Row.Cells["MaLoaiHinh"].Text = maLH + " - " + this.LoaiHinhMauDich_GetName(maLH);

                    if (e.Row.Cells["NgayTiepNhan"].Text != "")
                    {
                        DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Value);
                        if (dt.Year <= 1900)
                            e.Row.Cells["NgayTiepNhan"].Text = "";
                    }

                    DateTime dtNgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text);
                    if (dtNgayDangKy.Year <= 1900)
                        e.Row.Cells["NgayDangKy"].Text = "";
                    //if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim().Contains("V"))
                    //{
                    //    e.Row.Cells["SoToKhai"].Text = e.Row.Cells["HuongDan_PL"].Value.ToString();
                    //}


                    #region Begin TrangThaiXuLy
                    switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                    {
                        case -1:
                            e.Row.Cells["TrangThaiXuLy"].Text = setText("Chưa khai báo", "Not declared yet");
                            break;
                        case 0:
                            e.Row.Cells["TrangThaiXuLy"].Text = setText("Chờ duyệt", "Wait for approval");
                            break;
                        case 1:
                            e.Row.Cells["TrangThaiXuLy"].Text = setText("Đã duyệt", "Approved");
                            break;
                        case 2:
                            e.Row.Cells["TrangThaiXuLy"].Text = setText("Không phê duyệt", "Not approved");
                            break;
                        case 5:
                            e.Row.Cells["TrangThaiXuLy"].Text = setText("Sửa tờ khai", "Edit");
                            break;
                        case 10:
                            e.Row.Cells["TrangThaiXuLy"].Text = setText("Đã hủy", "Deleted");
                            break;
                        case 11:
                            e.Row.Cells["TrangThaiXuLy"].Text = setText("Chờ hủy", "Wait for delete");
                            break;
                    }
                    #endregion End TrangThaiXuLy

                    #region Begin PhanLuong
                    if (e.Row.Cells["PhanLuong"].Value != null && e.Row.Cells["PhanLuong"].Value != "")
                    {
                        switch (Convert.ToInt32(e.Row.Cells["PhanLuong"].Value))
                        {
                            case 1:
                                if (GlobalSettings.NGON_NGU == "0")
                                    e.Row.Cells["PhanLuong"].Text = "Luồng xanh";
                                else
                                    e.Row.Cells["PhanLuong"].Text = "Green";
                                break;
                            case 2:
                                if (GlobalSettings.NGON_NGU == "0")
                                    e.Row.Cells["PhanLuong"].Text = "Luồng vàng";
                                else
                                    e.Row.Cells["PhanLuong"].Text = "Yellow";
                                break;
                            case 3:
                                if (GlobalSettings.NGON_NGU == "0")
                                    e.Row.Cells["PhanLuong"].Text = "Luồng đỏ";
                                else
                                    e.Row.Cells["PhanLuong"].Text = "Red";
                                break;
                        }

                        e.Row.Cells["HuongDan"].ToolTipText = e.Row.Cells["HuongDan"].Text;
                    }
                    #endregion End PhanLuong
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    int j = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)i.GetRow().DataRow;
                            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                            sendXML.LoaiHS = "TKCT";
                            sendXML.master_id = tkct.ID;
                            if (sendXML.Load())
                            {
                                j = i.Position + 1;
                                showMsg("MSG_2702012", i.Position + 1);
                                //MLMessages("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Không được xóa, sửa?", "MSG_STN11",j.ToString(), false);
                                //if (st == "Yes")
                                //{
                                //    if (tkct.ID > 0)
                                //    {
                                //        tkct.Delete();
                                //    }
                                //}
                            }
                            else
                            {
                                if (tkct.ID > 0)
                                {
                                    if (tkct.MaLoaiHinh.Contains("X"))
                                    {
                                        if (PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(tkct.ID))
                                        {
                                            ShowMessage("Tờ khai chuyển tiếp có id = " + tkct.ID + " này đã được phân bổ nên không thể xóa được.", false);
                                            continue;
                                        }
                                    }

                                    //Load thong tin chung tu kem
                                    tkct.LoadChungTuKem();

                                    //Xoa chung tu kem
                                    Company.KDT.SHARE.QuanLyChungTu.AnHanThue.DeleteBy_TKMD_ID(tkct.ID);
                                    Company.KDT.SHARE.QuanLyChungTu.SoContainer.DeleteBy_TKMD_ID(tkct.ID);
                                    Company.KDT.SHARE.QuanLyChungTu.DamBaoNghiaVuNT.DeleteBy_TKMD_ID(tkct.ID);
                                    Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau.DeleteBy_TKMD_ID(tkct.ID);

                                    foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuNo ctn in tkct.ChungTuNoCollection)
                                    {
                                        ctn.Delete();
                                    }

                                    Company.GC.BLL.KDT.GC.PhuLucHopDong.DeleteBy_TKCT_ID(tkct.ID);

                                    tkct.Delete();
                                }
                            }
                        }
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(status) == TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
            //    txtNamTiepNhan.Text = string.Empty;
            //    txtNamTiepNhan.Value = 0;
            //    txtNamTiepNhan.Enabled = false;
            //    txtSoTiepNhan.Value = 0;
            //    txtSoTiepNhan.Text = string.Empty;
            //    txtSoTiepNhan.Enabled = false;
            //}
            //else
            //{
            //    txtNamTiepNhan.Value = DateTime.Today.Year;
            //    txtNamTiepNhan.Enabled = true;
            //    txtSoTiepNhan.Enabled = true;
            //}
            //this.search();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (tkctCollection.Count <= 0) return;
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    //int j = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)i.GetRow().DataRow;
                            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                            sendXML.LoaiHS = "TKCT";
                            sendXML.master_id = tkct.ID;
                            if (sendXML.Load())
                            {
                                //j = i.Position + 1;
                                showMsg("MSG_2702012", i.Position + 1);
                                //MLMessages("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Bạn không được xóa?", "MSG_STN09", j.ToString(),false);
                                //if (st == "Yes")
                                //{
                                //    if (tkct.ID > 0)
                                //    {
                                //        tkct.Delete();
                                //    }
                                //}
                            }
                            else
                            {
                                if (tkct.ID > 0)
                                {
                                    if (tkct.MaLoaiHinh.Contains("X"))
                                    {
                                        if (PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(tkct.ID))
                                        {
                                            ShowMessage("Tờ khai chuyển tiếp có id = " + tkct.ID + " này đã được phân bổ nên không thể xóa được.", false);
                                            continue;
                                        }
                                    }

                                    PhuLucHopDong.DeleteBy_TKCT_ID(tkct.ID);

                                    tkct.Delete();
                                }
                            }
                        }
                    }
                    this.search();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgList.GetRow() == null) return;
                ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = tkct.ID;
                form.DeclarationIssuer = tkct.MaLoaiHinh.Substring(0, 1).ToUpper() == "N" ? DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_NHAP : DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_XUAT;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }

        #region Send V3 Create by LANNT
        private void FeedBackV3()
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBackCT(TKCT, TKCT.GUIDSTR);

                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, isFeedBack);
                        }
                        count--;
                    }
                    else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || TKCT.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else isFeedBack = false;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        setCommandStatus();
                }
            }
            btnSearch_Click(null, null);
        }

        private void CancelV3()
        {

            ObjectSend msgSend = SingleMessage.CancelMessageCT(TKCT);
            SendMessageForm dlgSendForm = new SendMessageForm();

            dlgSendForm.Send += SendMessage;
            bool isSend = dlgSendForm.DoSend(msgSend);

            if (isSend)
            {
                TKCT.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                FeedBackV3();
                dlgSendForm.Message.XmlSaveMessage(TKCT.ID, MessageTitle.HuyKhaiBaoToKhai);
            }

        }
        private void SendV3()
        {
            try
            {


                if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
                {
                    TKCT = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                    TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                    TKCT.LoadHCTCollection();
                }
                if (TKCT.HCTCollection.Count == 0)
                {
                    this.ShowMessage("Bạn chưa nhập hàng khai báo\r\nVui lòng nhập thông tin hàng trước khi khai báo", false);
                    return;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                sendXML.master_id = TKCT.ID;
                if (sendXML.Load())
                {
                    MLMessages("Tờ khai đã gửi đến Hải quan. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", "MSG_SEN03", "", false);
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSingleDownload.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }

                if (TKCT.SoToKhai != 0 && TKCT.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
                {
                    string msg = "Tờ khai này đã có số tờ khai, không thể khai báo mới tờ khai.\r\nBạn hãy chuyển tờ khai sang trạng thái khác.";
                    ShowMessage(msg, false);
                    return;
                }
                else
                {

                    ObjectSend msgSend = SingleMessage.SendMessageCT(TKCT);

                    TKCT.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;



                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessage;

                    bool isSend = dlgSendForm.DoSend(msgSend);

                    //Lưu ý phải để dòng code lưu message sau kết quả trả về.
                    dlgSendForm.Message.XmlSaveMessage(TKCT.ID, TKCT.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET ? MessageTitle.KhaiBaoToKhai : MessageTitle.KhaiBaoSuaTK);

                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        dlgSendForm.Message.XmlSaveMessage(TKCT.ID, MessageTitle.KhaiBaoToKhai);
                        sendXML = new MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                        sendXML.master_id = TKCT.ID;
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        TKCT.Update();
                        FeedBackV3();

                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);


                }
                setCommandStatus();


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new SendEventArgs(ex));
            }
        }

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);

            //this.Invoke(new MethodInvoker(
            //    delegate
            //    {
            //        setCommandStatus();
            //    }));
        }
        /// <summary>
        /// Xử lý message tờ khai trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.ToKhaiSendHandlerCT(TKCT, ref msgInfor, sender, e);

        }

        #endregion

        private void timToKhai1_Search(object sender, Company.Interface.Controls.StringEventArgs e)
        {
            try
            {
                string where = "1 = 1";
                string maHQ = donViHaiQuanControl1.Ma;
                if (string.IsNullOrEmpty(maHQ)) maHQ = GlobalSettings.MA_HAI_QUAN;
                where += string.Format(" AND MaHaiQuanTiepNhan = '{0}' and MaDoanhNghiep='{1}'", maHQ, GlobalSettings.MA_DON_VI);

                // Thực hiện tìm kiếm.            
                if (e != null)
                {

                    //minhnd replace for TKCT
                    string test = e.String.Replace("LoaiVanDon", "HuongDan_PL");
                    //test = test.Replace("LoaiVanDon", "HuongDan_PL");
                    where += test;
                    status = e.Status;

                    if (!String.IsNullOrEmpty(timToKhai1.toKhaiProviders.NamTiepNhan))
                    {
                        //Bo sung by Hungtq, 16/07/2012
                        if (status > -1)
                            where += " AND YEAR(NgayTiepNhan) = " + (timToKhai1.toKhaiProviders.NamTiepNhan != "" ? "" + timToKhai1.toKhaiProviders.NamTiepNhan : "" + DateTime.Now.Year);
                    }
                    if (HD != null)
                    {
                        if (HD.ID != 0)
                        {
                            where += " AND IDHopDong = " + HD.ID;
                            where += " AND SoToKhai NOT IN (SELECT ToKhai FROM t_KDT_GC_PhieuNhapKho WHERE SoHopDong ='"+HD.SoHopDong+"' )";
                        }
                    }

                }
                else
                {
                    where += " and trangthaixuly=" + status;

                    //Update by Hungtq 06/07/2012
                    if (status > -1)
                        where += " AND YEAR(NgayTiepNhan) = " + timToKhai1.toKhaiProviders.NamTiepNhan;
                    if (timToKhai1.cboPhanLuong.SelectedValue != null && timToKhai1.cboPhanLuong.SelectedIndex != 0)
                        where += " AND PhanLuong=" + timToKhai1.cboPhanLuong.SelectedValue;
                    if (!string.IsNullOrEmpty(timToKhai1.txtSoTiepNhan.Text))
                    {
                        where += " AND SoTiepNhan like '%" + timToKhai1.txtSoTiepNhan.Value + "%'";
                    }
                    if (timToKhai1.txtSoToKhai.TextLength > 0)
                    {
                        where += " AND SoToKhai like '%" + timToKhai1.txtSoToKhai.Text + "%'";
                    }
                    if (HD != null)
                    {
                        if (HD.ID != 0)
                        {
                            where += " AND IDHopDong = " + HD.ID;
                            where += " AND SoToKhai NOT IN (SELECT ToKhai FROM t_KDT_GC_PhieuNhapKho WHERE SoHopDong ='" + HD.SoHopDong + "' )";
                        }
                    }
                }
                this.tkctCollection = ToKhaiChuyenTiep.SelectCollectionDynamic(where, "ID desc");
                this.tkctTempCollection = ToKhaiChuyenTiep.SelectCollectionDynamicAndHCT(where, "ID desc");
                dgList.DataSource = this.tkctCollection;

                this.setCommandStatus();
            }
            catch (Exception ex) 
            { 
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = "Danh sách tờ khai chuyển tiếp " + DateTime.Now.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
                sf.Filter = "Excel File | *.xls ";
                if (ShowMessage("Bạn có muốn xuất kèm theo hàng hóa của tờ khai không", true) == "No")
                {
                    if (sf.ShowDialog(this) == DialogResult.OK)
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter grdExport = new Janus.Windows.GridEX.Export.GridEXExporter();
                        grdExport.GridEX = dgList;

                        try
                        {
                            System.IO.Stream str = sf.OpenFile();
                            grdExport.Export(str);
                            str.Close();
                            if (ShowMessage("Bạn có muốn mở File này không", true) == "Yes")
                            {
                                System.Diagnostics.Process.Start(sf.FileName);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi trong quá trình mở File" + ex.Message, false);
                        }
                    }
                    else
                    {
                        ShowMessage("Xuất Excel không thành công", false);
                    }
                }
                else
                {
                    if (sf.ShowDialog(this) == DialogResult.Yes || sf.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        dgList.Tables[0].Columns.Add(new GridEXColumn("SoThuTuHang", ColumnType.Text, EditType.NoEdit) { Caption = "STT" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("MaHS", ColumnType.Text, EditType.NoEdit) { Caption = "Mã HS" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("MaHang", ColumnType.Text, EditType.NoEdit) { Caption = "Mã NPL" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TenHang", ColumnType.Text, EditType.NoEdit) { Caption = "Tên hàng" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("ID_NuocXX", ColumnType.Text, EditType.NoEdit) { Caption = "Xuất xứ" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("SoLuong", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DVT", ColumnType.Text, EditType.NoEdit) { Caption = "Đơn vị tính" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DonGia", ColumnType.Text, EditType.NoEdit) { Caption = "Đơn giá KB" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TriGia", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá KB" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DonGiaTT", ColumnType.Text, EditType.NoEdit) { Caption = "Đơn giá TT" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TriGiaTT", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá TT" });
                        //dgList.Refresh();
                        //dgList.Refetch();
                        dgList.DataSource = tkctTempCollection;
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sf.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        dgList.Tables[0].Columns.Remove("SoThuTuHang");
                        dgList.Tables[0].Columns.Remove("MaHS");
                        dgList.Tables[0].Columns.Remove("MaHang");
                        dgList.Tables[0].Columns.Remove("TenHang");
                        dgList.Tables[0].Columns.Remove("ID_NuocXX");
                        dgList.Tables[0].Columns.Remove("DVT");
                        dgList.Tables[0].Columns.Remove("SoLuong");
                        dgList.Tables[0].Columns.Remove("DonGia");
                        dgList.Tables[0].Columns.Remove("TriGia");
                        dgList.Tables[0].Columns.Remove("DonGiaTT");
                        dgList.Tables[0].Columns.Remove("TriGiaTT");
                        dgList.Refresh();
                        timToKhai1_Search(null, null);
                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sf.FileName);
                        }
                    }
                    else
                    {
                        ShowMessage("Xuất Excel không thành công", false);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void donViHaiQuanControl1_ValueChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }

    }
}