﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class SelectNPL_DMSUAForm : BaseForm
    {
        public NguyenPhuLieu NguyenPhuLieuSelected = new NguyenPhuLieu();
        public SelectNPL_DMSUAForm()
        {
            InitializeComponent();
        }

        private void SelectNPL_DMSUAForm_Load(object sender, EventArgs e)
        {

            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            BindData();
        }

        public void BindData()
        {
            DataSet ds = DinhMucDangKy.getNPL(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            dgList.DataSource = ds.Tables[0];

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string maNPL = e.Row.Cells["Ma"].Text;
                this.NguyenPhuLieuSelected.Ma = maNPL;
                this.NguyenPhuLieuSelected.Ten = e.Row.Cells["Ten"].Text;
                this.NguyenPhuLieuSelected.DVT_ID = e.Row.Cells["DVT_ID"].Text;
                //this.NguyenPhuLieuSelected.STTHang = e.Row.Cells["STTHang"].Text;
                this.Hide();
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }
    }
}
