﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.Messages.Send;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.Messages.Send;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.Messages.Send;
#endif

namespace Company.Interface
{
    public partial class ChungThuGiamDinhForm : Company.Interface.BaseForm
    {

        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        public ChungThuGiamDinh ChungTuGiamDinh;
        private FeedBackContent feedbackContent;
        private string msgInfor = string.Empty;
        public ChungThuGiamDinhForm()
        {
            InitializeComponent();
        }

      
        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HangGiamDinh> hangcollection = new List<HangGiamDinh>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangGiamDinh hgd = new HangGiamDinh();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hgd.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hgd.ID = Convert.ToInt64(row["ID"]);
                        hangcollection.Add(hgd);
                    }
                }
                foreach (HangGiamDinh hangtemp in hangcollection)
                {
                    try
                    {
                        if (hangtemp.ID > 0)
                        {
                            hangtemp.Delete();
                        }
                        foreach (HangGiamDinh hgd in ChungTuGiamDinh.ListHang)
                        {
                            if (hgd.HMD_ID == hangtemp.HMD_ID)
                            {
                                ChungTuGiamDinh.ListHang.Remove(hgd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }
        private void BindData()
        {
            //Company.KD.BLL.KDT.HangMauDich hmd = new Company.KD.BLL.KDT.HangMauDich();
            //hmd.TKMD_ID = TKMD.ID;

            dgList.DataSource = ChungTuGiamDinh.ConvertListToDataSet(HangMauDich.SelectBy_TKMD_ID(TKMD.ID).Tables[0]);
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            if (ChungTuGiamDinh.ListHang.Count == 0)
            {
                ShowMessage("Chưa chọn thông tin hàng hóa.", false);
                return;
            }

            //Ngonnt 24/01
            //Kiểm tra vận đơn
            bool check = true;
            string msg = "";
            int count = 0;
            GridEXRow[] rows = dgList.GetRows();
            foreach (GridEXRow row in rows)
            {
                DataRowView rowview = (DataRowView)row.DataRow;
                foreach (HangGiamDinh item in ChungTuGiamDinh.ListHang)
                {
                    if (rowview["HMD_ID"].ToString().Trim() == item.HMD_ID.ToString().Trim())
                    {
                        item.SoVanTaiDon = rowview["SoVanTaiDon"].ToString();

                        string re = "";
                        if (item.SoVanTaiDon.Length == 0) re += "'Số vận tải đơn'";
                        if (re.Length != 0)
                        {
                            re += " không được để trống";
                            msg += String.Format("Dòng hàng : " + (count + 1) + "\n" + re + "\n");
                            check &= false;
                        }
                        count++;
                    }
                }
            }

            if (!check)
            {
                if (msg != "")
                    ShowMessage(msg, false);
                return;
            }
            //Ngonnt 24/01

            //TKMD.HopDongThuongMaiCollection.Remove(HopDongTM);
            //HopDongTM.Delete();
//             if (ChungTuGiamDinh.ID == 0 && checkSoHopDong(txtMaCoQuanGiamDinh.Text))
//             {
//                 ShowMessage("Số hợp đồng này đã tồn tại.", false);
//                 return;
//             }
            ChungTuGiamDinh.NoiDung = txtNoiDungGiamDinh.Text.Trim();
            ChungTuGiamDinh.KetQua = txtKetQuaGiamDinh.Text.Trim();
            ChungTuGiamDinh.MaCoQuanGD = txtMaCoQuanGiamDinh.Text.Trim();
            ChungTuGiamDinh.TenCoQuanGD = txtTenCoQuanGiamDinh.Text.Trim();
            ChungTuGiamDinh.DiaDiem = txtDiaDiemGiamDinh.Text.Trim();
            ChungTuGiamDinh.CanBoGD = txtTenCanBoGiamDinh.Text.Trim();
            ChungTuGiamDinh.ThongTinKhac = txtThongTinKhac.Text.Trim();
            ChungTuGiamDinh.TKMD_ID = TKMD.ID;
            if (isKhaiBoSung)
                ChungTuGiamDinh.LoaiKB = 1;
            else
                ChungTuGiamDinh.LoaiKB = 0;
            GridEXRow[] rowCollection = dgList.GetRows();
            foreach (GridEXRow row in rowCollection)
            {
                DataRowView rowview = (DataRowView)row.DataRow;
                foreach (HangGiamDinh item in ChungTuGiamDinh.ListHang)
                {
                    if (rowview["HMD_ID"].ToString().Trim() == item.HMD_ID.ToString().Trim())
                    {
                        //item.GhiChu = row.Cells["GhiChu"].Text;

                        item.MaHS = rowview["MaHS"].ToString();
                        item.MaPhu = rowview["MaPhu"].ToString();
                        item.TenHang = rowview["TenHang"].ToString();
                        item.NuocXX_ID = rowview["NuocXX_ID"].ToString();
                        item.DVT_ID = rowview["DVT_ID"].ToString();
                        item.SoLuong = Convert.ToDecimal(rowview["SoLuong"]);
                        item.GhiChu = rowview["GhiChu"].ToString();

                        item.SoVanTaiDon = rowview["SoVanTaiDon"].ToString();
                        item.NgayVanDon = Convert.ToDateTime(rowview["NgayVanDon"]);
                        item.TinhTrangContainer = Convert.ToBoolean(rowview["TinhTrangContainer"]);
                        item.SoHieuContainer = rowview["SoHieuContainer"].ToString();
                        break;
                    }
                }
            }
            try
            {
                if (string.IsNullOrEmpty(ChungTuGiamDinh.GuidStr)) ChungTuGiamDinh.GuidStr = Guid.NewGuid().ToString();
                if (ChungTuGiamDinh.NgayDangKy.Year < 1900) ChungTuGiamDinh.NgayDangKy = new DateTime(1900, 1, 1);
                bool isAddNew = ChungTuGiamDinh.ID == 0;
                ChungTuGiamDinh.InsertUpdateFull();
                TKMD.ChungThuGD = ChungTuGiamDinh;
                BindData();
                ShowMessage("Lưu thành công.", false);
                if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET && TKMD.SoToKhai > 0)
                    btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }


        }
        public List<HangMauDich> ConvertListToHangMauDichCollection(List<HangGiamDinh> listHangGD, List<HangMauDich> hangCollection)
        {

            List<HangMauDich> tmp = new List<HangMauDich>();

            foreach (HangGiamDinh item in listHangGD)
            {
                foreach (HangMauDich HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        private void btnChonHang_Click(object sender, EventArgs e)
        {
            SelectHangMauDichForm f = new SelectHangMauDichForm();
            f.TKMD = TKMD;
            //Linhhtn - Không dùng đoạn này vì sẽ gây mất hàng khi kích nút Chọn hàng
            //if (HopDongTM.ListHangMDOfHopDong.Count > 0)
            //{
            //    f.TKMD.HMDCollection = ConvertListToHangMauDichCollection(HopDongTM.ListHangMDOfHopDong, TKMD.HMDCollection);
            //}
            f.ShowDialog(this);
            if (f.HMDTMPCollection.Count > 0)
            {
                foreach (HangMauDich HMD in f.HMDTMPCollection)
                {
                    bool ok = false;
                    foreach (HangGiamDinh hanggd in ChungTuGiamDinh.ListHang)
                    {
                        if (hanggd.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        HangGiamDinh Hanggd = new HangGiamDinh();
                        Hanggd.HMD_ID = HMD.ID;
                        Hanggd.ChungThu_ID = ChungTuGiamDinh.ID;
                        Hanggd.MaPhu = HMD.MaPhu;
                        Hanggd.MaHS = HMD.MaHS;
                        Hanggd.TenHang = HMD.TenHang;
                        Hanggd.DVT_ID = HMD.DVT_ID;
                        Hanggd.SoLuong = HMD.SoLuong;
                        Hanggd.NuocXX_ID = HMD.NuocXX_ID;
                        Hanggd.SoVanTaiDon = TKMD.SoVanDon;
                        Hanggd.NgayVanDon = TKMD.NgayVanDon;
                        if (TKMD.VanTaiDon != null && TKMD.VanTaiDon.ContainerCollection != null && TKMD.VanTaiDon.ContainerCollection.Count > 0)
                        {
                            Hanggd.SoHieuContainer = TKMD.VanTaiDon.ContainerCollection[0].SoHieu;
                        }
                        else
                            Hanggd.SoHieuContainer = "";
                        Hanggd.TinhTrangContainer = true;
                        ChungTuGiamDinh.ListHang.Add(Hanggd);
                    }
                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void HopDongForm_Load(object sender, EventArgs e)
        {

            //Update by KHANHHN 05/03/2012
            if (ChungTuGiamDinh == null || ChungTuGiamDinh.ID == 0)
                ChungTuGiamDinh = new ChungThuGiamDinh { TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO };

            // an cac button truoc khi load
            btnKhaiBao.Enabled = false;
            btnLayPhanHoi.Enabled = false;


            //DATLMQ bổ sung lấy HĐ từ TKMD 13/12/2010
            if (ChungTuGiamDinh == null || ChungTuGiamDinh.ID <= 0)
            {
               /* ChungTuGiamDinh.SoHopDongTM = TKMD.SoHopDong;*/
                /*txtMaCoQuanGiamDinh.Text = ChungTuGiamDinh.SoHopDongTM;*/

                /*ChungTuGiamDinh.NgayHopDongTM = TKMD.NgayHopDong;*/

                /*ChungTuGiamDinh.ThoiHanThanhToan = TKMD.NgayHetHanHopDong;*/
                //DATLMQ kiểm tra loại hình tờ khai 30/12/2010
                if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
              
                }
                else
                {
                 
                }

                txtDiaDiemGiamDinh.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);
                //if (HopDongTM.ID == 0)
                //{
                //    ccThoiHanTT.Text = ccNgayHopDong.Text = DateTime.Now.ToLongDateString();
                //}
            }

            else if (ChungTuGiamDinh != null && ChungTuGiamDinh.ID > 0)
            {
                txtDiaDiemGiamDinh.Text = ChungTuGiamDinh.DiaDiem;
                txtTenCanBoGiamDinh.Text = ChungTuGiamDinh.CanBoGD;
                txtMaCoQuanGiamDinh.Text = ChungTuGiamDinh.MaCoQuanGD;
                txtTenCoQuanGiamDinh.Text = ChungTuGiamDinh.TenCoQuanGD;
                txtNoiDungGiamDinh.Text = ChungTuGiamDinh.NoiDung;
                txtKetQuaGiamDinh.Text = ChungTuGiamDinh.KetQua;
                txtThongTinKhac.Text = ChungTuGiamDinh.ThongTinKhac;
                ChungTuGiamDinh.LoadHang();
                BindData();
            }

            if (TKMD.ID <= 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            else if (ChungTuGiamDinh.SoTiepNhan > 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
            }
            else if (TKMD.SoToKhai > 0 && int.Parse(TKMD.PhanLuong != "" ? TKMD.PhanLuong : "0") == 0)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;
            }
            else if (TKMD.PhanLuong != "")
            {
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            setCommandStatus(TKMD, isKhaiBoSung, ChungTuGiamDinh);
            btnThuTucHQTruocDo.Visible = Company.KDT.SHARE.Components.Globals.IsTest;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (ChungTuGiamDinh.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }

            try
            {
                if ((ChungTuGiamDinh.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO) || (ChungTuGiamDinh.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET))
                    ChungTuGiamDinh.GuidStr = Guid.NewGuid().ToString();
                else if (ChungTuGiamDinh.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    return;
                }

                ObjectSend msgSend = SingleMessage.BoSungChungThuGiamDinh(TKMD, ChungTuGiamDinh);

                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendMessageForm.Message.XmlSaveMessage(ChungTuGiamDinh.ID, MessageTitle.KhaiBaoBoSungHopDong);
                    ChungTuGiamDinh.TrangThai = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    ChungTuGiamDinh.Update();
                    setCommandStatus(TKMD, isKhaiBoSung, ChungTuGiamDinh);
                    btnLayPhanHoi_Click(null, null);
                }
                else if (isSend && feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    ShowMessageTQDT(msgInfor, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }



        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            #region V3

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBack(TKMD, ChungTuGiamDinh.GuidStr);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                isFeedBack = sendMessageForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.setCommandStatus(TKMD,isKhaiBoSung, ChungTuGiamDinh);
                }


            }
//             ObjectSend msgSend = SingleMessage.FeedBack(TKMD, HopDongTM.GuidStr);
//             SendMessageForm sendMessageForm = new SendMessageForm();
//             sendMessageForm.Send += SendMessage;
//             sendMessageForm.DoSend(msgSend);

            #endregion

        }


        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form HOP DONG.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool setCommandStatus(ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.ChungThuGiamDinh ChungThuGD)
        {
            if (ChungThuGD == null)
                return false;

            
            if (ChungThuGD.LoaiKB == 1)
            {

                switch (ChungThuGD.TrangThai)
                {
                    case -1:
                        lbTrangThai.Text = "Chưa Khai Báo";
                        break;
                    case -3:
                        lbTrangThai.Text = "Đã Khai báo nhưng chưa có phản hồi từ Hải quan";
                        break;
                    case 0:
                        lbTrangThai.Text = "Chờ Duyệt";
                        break;
                    case 1:
                        lbTrangThai.Text = "Đã Duyệt";
                        break;
                    case 2:
                        lbTrangThai.Text = "Hải quan không phê duyệt";
                        break;

                }
            }
            else
                lbTrangThai.Text = string.Empty;
            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else if (tkmd.SoToKhai > 0 && TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {

                bool khaiBaoEnable = (ChungThuGD.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     ChungThuGD.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET);

                btnKhaiBao.Enabled = btnXoa.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = ChungThuGD.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   ChungThuGD.TrangThai == TrangThaiXuLy.DA_DUYET ||
                                   ChungThuGD.TrangThai == TrangThaiXuLy.CHO_DUYET || ChungThuGD.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;
            }
            txtSoTiepNhan.Text = ChungThuGD.SoTiepNhan > 0 ? ChungThuGD.SoTiepNhan.ToString() : string.Empty;
            if (ChungThuGD.NgayDangKy.Year > 1900) ccNgayTiepNhan.Value = ChungThuGD.NgayDangKy;
            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (ChungTuGiamDinh.GuidStr != null && ChungTuGiamDinh.GuidStr != "")
            {
                ThongDiepForm f = new ThongDiepForm();
                f.DeclarationIssuer = AdditionalDocumentType.CHUNG_THU_GIAM_DINH;
                f.ItemID = ChungTuGiamDinh.ID;
                f.ShowDialog(this);
            }

            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            List<HangGiamDinh> HangCollection = new List<HangGiamDinh>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangGiamDinh hdtmdtmp = new HangGiamDinh();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HangCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HangGiamDinh hdtmtmp in HangCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HangGiamDinh hdtmd in ChungTuGiamDinh.ListHang)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                ChungTuGiamDinh.ListHang.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            List<HangGiamDinh> HangCollection = new List<HangGiamDinh>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangGiamDinh hdtmdtmp = new HangGiamDinh();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HangCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HangGiamDinh hdtmtmp in HangCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HangGiamDinh hdtmd in ChungTuGiamDinh.ListHang)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                ChungTuGiamDinh.ListHang.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }
        #region  V3

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(ChungTuGiamDinh.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                ChungTuGiamDinh.TrangThai = TrangThaiXuLy.KHONG_PHE_DUYET;
                                noidung = "Hải quan từ chối tiếp nhận.\r\nLý do: " + noidung;
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            //noidung = noidung.Replace(string.Format("Message [{0}]", HopDongTM.GuidStr), string.Empty);
                            //this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                ChungTuGiamDinh.SoTiepNhan = long.Parse(feedbackContent.CustomsReference);
                                if (feedbackContent.Acceptance.Length == 10)
                                    ChungTuGiamDinh.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd", null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    ChungTuGiamDinh.NgayDangKy = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                else
                                    ChungTuGiamDinh.NgayDangKy = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);

                                //ChungTuGiamDinh.NamTiepNhan = ChungTuGiamDinh.NgayDangKy.Year;
                                //HopDongTM.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);
                                e.FeedBackMessage.XmlSaveMessage(ChungTuGiamDinh.ID, MessageTitle.KhaiBaoBoSungHopDongDuocChapNhan, noidung);
                                ChungTuGiamDinh.TrangThai = TrangThaiXuLy.CHO_DUYET;
                                noidung = "Hải quan cấp số tiếp nhận\r\nSố tiếp nhận: " + ChungTuGiamDinh.SoTiepNhan.ToString() + "\r\nNgày tiếp nhận: " + ChungTuGiamDinh.NgayDangKy.ToString();
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(ChungTuGiamDinh.ID, MessageTitle.KhaiBaoBoSungHopDongThanhCong, noidung);
                                ChungTuGiamDinh.TrangThai = TrangThaiXuLy.DA_DUYET;
                                noidung = "Hợp đồng đã được duyệt.\r\n Thông tin trả về từ hải quan: " + noidung;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(ChungTuGiamDinh.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }

                    if (isUpdate)
                    {
                        ChungTuGiamDinh.Update();
                        setCommandStatus(TKMD, isKhaiBoSung, ChungTuGiamDinh);
                    }
                    msgInfor = noidung;
                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion

        private void dgList_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "DVT_ID")
            {
                int val = DonViTinhID(e.Value.ToString());
                if (val != -1)
                    e.Value = Convert.ToInt32(val);
            }
        }

        private void btnThuTucHQTruocDo_Click(object sender, EventArgs e)
        {
            if (ChungTuGiamDinh.ID == 0)
                ShowMessage("Vui lòng lưu thông tin Chứng thư trước khi thêm chứng từ HQ trước đó", false);
            else
            {
                ListChungTuTruocDoForm f = new ListChungTuTruocDoForm();
                f.Master_ID = ChungTuGiamDinh.ID;
                f.Type = "CTGD";
                f.ShowDialog(this);
            }
        }
    }
}

