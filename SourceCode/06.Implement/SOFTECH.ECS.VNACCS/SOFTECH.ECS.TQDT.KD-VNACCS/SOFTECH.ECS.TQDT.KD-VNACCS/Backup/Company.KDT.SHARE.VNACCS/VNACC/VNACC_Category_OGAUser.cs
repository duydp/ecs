using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_OGAUser 
	{
        protected static List<VNACC_Category_OGAUser> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_OGAUser> collection = new List<VNACC_Category_OGAUser>();
            while (reader.Read())
            {
                VNACC_Category_OGAUser entity = new VNACC_Category_OGAUser();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("OfficeOfApplicationCode"))) entity.OfficeOfApplicationCode = reader.GetString(reader.GetOrdinal("OfficeOfApplicationCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("OfficeOfApplicationName"))) entity.OfficeOfApplicationName = reader.GetString(reader.GetOrdinal("OfficeOfApplicationName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_OGAUser> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [OfficeOfApplicationCode], OfficeOfApplicationName FROM [dbo].[t_VNACC_Category_OGAUser]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_OGAUser> SelectCollectionBy(List<VNACC_Category_OGAUser> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_OGAUser o)
            {
                return o.TableID.Contains(tableID);
            });
        }
	}	
}