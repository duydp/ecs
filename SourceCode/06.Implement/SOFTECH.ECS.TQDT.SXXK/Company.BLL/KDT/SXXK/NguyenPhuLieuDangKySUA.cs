﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Transactions;
using System.Globalization;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.BLL.Utils;
using Company.KDT.SHARE.Components.Utils;

namespace Company.BLL.KDT.SXXK
{
    public partial class NguyenPhuLieuDangKySUA
    {
        private string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            this.GUIDSTR = (System.Guid.NewGuid().ToString());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            nodeFrom.ChildNodes[0].InnerText = Company.BLL.DuLieuChuan.DoiTac.GetName(this.MaDoanhNghiep);
            this.Update();
            return doc.InnerXml;
        }
        private string ConfigPhongBiPhanHoi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;

            return doc.InnerXml;
        }
        public string WSRequestXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucSanPham, MessgaseFunction.HoiTrangThai));

            XmlDocument docSP = new XmlDocument();
            string path = EntityBase.GetPathProram();
            docSP.Load(path + "\\TemplateXML\\LayPhanHoiNguyenPhuLieu.xml");


            XmlNode nodeHQNhan = docSP.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);

            XmlNode nodeDN = docSP.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            XmlNode nodeDuLieu = docSP.SelectSingleNode("Root/SXXK/DU_LIEU");
            nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docSP.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";

        }

        public string WSDownLoad(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiPhanHoi(MessgaseType.DanhMucNguyenPhuLieu, MessgaseFunction.HoiTrangThai));
            
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram();
            docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);

            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);


            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }
        #region Khai báo sửa nguyên phụ liệu
        
        public string WSSendXMLSuaNPL(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucNguyenPhuLieu, MessgaseFunction.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXML());


            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";
        }
        
        private string ConvertCollectionToXML()
        {
            //load du lieu
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram();
            docNPL.Load(path + "\\TemplateXML\\KhaiBaoNguyenPhuLieuSUA.xml");
            XmlNode nplNode = docNPL.GetElementsByTagName("NPL")[0];

            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);


            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = Company.BLL.DuLieuChuan.DoiTac.GetName(this.MaDoanhNghiep);

            List<NguyenPhuLieuSUA> listNPL = new List<NguyenPhuLieuSUA>();
            listNPL = (List<NguyenPhuLieuSUA>)NguyenPhuLieuSUA.SelectCollectionBy_Master_IDSUA(this.ID);
            foreach (NguyenPhuLieuSUA npl in listNPL)
            {
                XmlNode node = docNPL.CreateElement("NPL.ITEM");
                XmlAttribute sttAtt = docNPL.CreateAttribute("STT");
                sttAtt.Value = npl.STTHang.ToString();
                node.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docNPL.CreateAttribute("MA_NPL");
                maAtt.Value = npl.Ma;
                node.Attributes.Append(maAtt);

                XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
                MaHSAtt.Value = npl.MaHS;
                node.Attributes.Append(MaHSAtt);

                XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_NPL");
                TenAtt.Value = npl.Ten;
                node.Attributes.Append(TenAtt);

                XmlAttribute DVTAtt = docNPL.CreateAttribute("MA_DVT");
                DVTAtt.Value = npl.DVT_ID;
                node.Attributes.Append(DVTAtt);

                nplNode.AppendChild(node);
            }
            return docNPL.InnerXml;
        }

        public string LayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();

            doc.LoadXml(xml);
            doc.GetElementsByTagName("function")[0].InnerText = "5";

            //HungTQ, Update 21/04/2010.
            doc.GetElementsByTagName("declarationType")[0].InnerText = "1";

            string kq = "";
            int i = 0;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
                docNPL = new XmlDocument();
                docNPL.LoadXml(kq);
                XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (nodeRoot.Attributes["Err"].Value == "no")
                {
                    if (nodeRoot.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else if (FontConverter.TCVN2Unicode(nodeRoot.InnerText).Equals("Chứng từ không xử lý"))
                {
                    return doc.InnerXml;
                }
                else
                {
                    throw new Exception(FontConverter.TCVN2Unicode(nodeRoot.InnerText) + "|" + "Web Service");
                }
            }

            if (i > 1)
                return doc.InnerXml;

            /*XU LY THONG TIN MESSAGE PHAN HOI TU HAI QUAN*/

            XmlNode nodeRoot1 = docNPL.SelectSingleNode("Envelope/Body/Content/Root");


            if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
            {
                XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == "SUA_NPL")//THONG_TIN_DANG_KY.SP)
                {
                    #region Lấy số tiếp nhận của danh sách NPL

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                        this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                        this.NgayTiepNhan = DateTime.Today;
                        this.NamDK = Convert.ToInt16(nodeDuLieu.Attributes["NAM_TN"].Value);
                        this.TrangThaiXuLy = 0;
                        this.Update();
                    }
                    Company.KD.BLL.KDT.KetQuaXuLy kqxl = new Company.KD.BLL.KDT.KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = "NPL";
                    kqxl.LoaiThongDiep = Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                    kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                    #endregion Lấy số tiếp nhận của danh sách NPL
                }
            }
            else
                if (nodeRoot1 != null && nodeRoot1.Attributes.GetNamedItem("TuChoi") != null
                && nodeRoot1.Attributes["TuChoi"].Value == "yes")// Từ chối
                {
                    //if () {
                    this.HUONGDAN = FontConverter.TCVN2Unicode(nodeRoot1.InnerText);
                    this.TrangThaiXuLy = BLL.TrangThaiXuLy.KHONG_PHE_DUYET;
                    this.Update();

                    Company.KD.BLL.KDT.KetQuaXuLy kqxl = new Company.KD.BLL.KDT.KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = "NPL";
                    kqxl.LoaiThongDiep = "Nguyên phụ liệu bị từ chối";//Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                    kqxl.NoiDung = FontConverter.TCVN2Unicode(nodeRoot1.InnerText);
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                    //}
                }
                else
                    if (nodeRoot1 != null && nodeRoot1.SelectSingleNode("PHAN_LUONG") != null)
                    {

                        XmlNode nodePhanLuong = docNPL.SelectSingleNode("Envelope/Body/Content/Root").SelectSingleNode("PHAN_LUONG");
                        this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                        this.HUONGDAN = nodePhanLuong.Attributes["HUONGDAN"].Value;
                        this.Update();
                        string tenluong = "Xanh";
                        if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                            tenluong = "Vàng";
                        else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                            tenluong = "Đỏ";
                        Company.KD.BLL.KDT.KetQuaXuLy kqxl = new Company.KD.BLL.KDT.KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = "NPL";//Company.KD.BLL.KDT.KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = "Nguyên phụ liệu được phân luồng";//Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_ToKhaiDuocPhanLuong;
                        kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nHải quan: {2}\r\nPhân luồng: {3}\r\nHướng dẫn: {4}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), this.MaHaiQuan.Trim(), tenluong, this.HUONGDAN);
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                    }
                    else
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
                        {
                            #region Lỗi

                            XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                            XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                            string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
                            string errorSt = "";
                            if (stMucLoi == "XML_LEVEL")
                                errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                            else if (stMucLoi == "DATA_LEVEL")
                                errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                            else if (stMucLoi == "SERVICE_LEVEL")
                                errorSt = "Lỗi do Web service trả về ";
                            else if (stMucLoi == "DOTNET_LEVEL")
                                errorSt = "Lỗi do hệ thống của hải quan ";
                            throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);

                            #endregion
                        }
                       
                        else
                            if (nodeRoot1 != null && nodeRoot1.Attributes["TrangThai"].Value == "yes"
               && nodeRoot1.Attributes["SOTN"].Value != "")
                            {
                                Company.KD.BLL.KDT.KetQuaXuLy kqxl = new Company.KD.BLL.KDT.KetQuaXuLy();
                                kqxl.ItemID = this.ID;
                                kqxl.ReferenceID = new Guid(this.GUIDSTR);
                                kqxl.LoaiChungTu = "NPL";//Company.KD.BLL.KDT.KetQuaXuLy.LoaiChungTu_ToKhai;
                                kqxl.LoaiThongDiep = "Nguyên phụ liệu sửa được duyệt";//Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_ToKhaiDuocPhanLuong;
                                kqxl.NoiDung = string.Format("Nguyên phụ liệu sửa được duyệt: Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nHải quan: {2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), this.MaHaiQuan.Trim());
                                kqxl.Ngay = DateTime.Now;
                                kqxl.Insert();

                                this.TrangThaiXuLy = BLL.TrangThaiXuLy.DA_DUYET;
                                this.Update();
                                //cập nhật lại sản phẩm gốc
                                List<NguyenPhuLieuSUA> nplSUAList = (List<NguyenPhuLieuSUA>)NguyenPhuLieuSUA.SelectCollectionBy_Master_IDSUA(this.ID);
                                NguyenPhuLieu nplGoc = new NguyenPhuLieu();
                                foreach (NguyenPhuLieuSUA nplSUA in nplSUAList)
                                {
                                    //Cập nhật vào bảng t_KDT_SXXK_SanPham
                                    nplGoc = NguyenPhuLieu.Load(nplSUA.IDNPL);
                                    nplGoc.Ten = nplSUA.Ten;
                                    nplGoc.MaHS = nplSUA.MaHS;
                                    nplGoc.DVT_ID = nplSUA.DVT_ID;
                                    nplGoc.Update();
                                    // Cập nhật vào bảng t_SXXK_SanPham
                                    BLL.SXXK.NguyenPhuLieu nplSXXK = BLL.SXXK.NguyenPhuLieu.getNguyenPhuLieu(this.MaHaiQuan, this.MaDoanhNghiep, nplGoc.Ma);
                                    if (nplSXXK != null)
                                    {
                                        nplSXXK.Ten = nplSUA.Ten;
                                        nplSXXK.MaHS = nplSUA.MaHS;
                                        nplSXXK.DVT_ID = nplSUA.DVT_ID;
                                        nplSXXK.Update();
                                    }
                                }

                            }
            return "";

        }
        
        #endregion Khai báo sửa nguyên phụ liệu
    }
}
