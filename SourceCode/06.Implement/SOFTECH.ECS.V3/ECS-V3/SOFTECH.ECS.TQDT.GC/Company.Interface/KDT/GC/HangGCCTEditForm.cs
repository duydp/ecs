﻿using System;
using System.Drawing;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL.Utils;
using System.Data;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class HangGCCTEditForm : BaseForm
    {
        public bool IsEdited = false;
        public bool IsDeleted = false;
        private bool IsExit = true;
        //-----------------------------------------------------------------------------------------
        public HangChuyenTiep HCT = new HangChuyenTiep();
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        public string LoaiHangHoa = "";
        public string NhomLoaiHinh = string.Empty;
        public ToKhaiChuyenTiep TKCT;
        public decimal TyGiaTT;

        //-----------------------------------------------------------------------------------------				
        // Tính thuế
        private decimal luong;
        private decimal dgnt;
        private decimal tgnt;

        private decimal tgtt_nk;
        private decimal tgtt_ttdb;
        private decimal tgtt_gtgt;
        private decimal clg;

        private decimal ts_nk;
        private decimal ts_ttdb;
        private decimal ts_gtgt;
        private decimal tl_clg;

        private decimal tt_nk;
        private decimal tt_ttdb;
        private decimal tt_gtgt;
        private decimal st_clg;
        //-----------------------------------------------------------------------------------------
        private string MaHangOld = "";
        //private NguyenPhuLieuRegistedForm NPLRegistedForm;
        //private SanPhamRegistedForm SPRegistedForm;
        private decimal LuongCon = 0;

        public HangGCCTEditForm()
        {
            InitializeComponent();
        }

        private void KiemTraTonTaiHang()
        {
            switch (this.TKCT.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text;
                    if (npl.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
                case "SP":
                    if (this.TKCT.MaLoaiHinh.EndsWith("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text;
                        if (npl1.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text;
                        if (sp.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phầm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                case "TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text;
                    if (tb.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
            }

            KiemTraTonTaiHangUpdate();
        }
        private void KiemTraTonTaiHangUpdate()
        {
            switch (this.TKCT.MaLoaiHinh.Substring(3, 2))
            {
                case "18"://"PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text;
                    if (npl.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
                case "19"://"SP":
                    if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text;
                        if (npl1.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text;
                        if (sp.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phầm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                case "20"://"TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text;
                    if (tb.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
            }
        }

        private void khoitao_GiaoDien()
        {
            if (this.NhomLoaiHinh.StartsWith("N")) SetVisibleHTS(false);
            else
            {
                //if (this.LoaiHangHoa != "S" || this.TKCT.MaMid == "") SetVisibleHTS(false);
                if (this.LoaiHangHoa != "S" || GlobalSettings.MaMID.Trim() == "") SetVisibleHTS(false);

            }
            if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
            {
                txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            }
            else
            {
                txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                txtSoLuong_HTS.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
            }
            if (NhomLoaiHinh == "NGC")
            {
                lblSoLuongDaCo.Text = setText("Lượng còn được nhập", "The import quanlity remain ");
            }
            else
            {
                lblSoLuongDaCo.Text = setText("Lượng còn được xuất", "The export quanlity remain");
            }

            txtTriGiaKB.DecimalDigits = GlobalSettings.TriGiaNT;
            txtTGNT.DecimalDigits = GlobalSettings.TriGiaNT;
            txtTGTT_NK.DecimalDigits = GlobalSettings.TriGiaNT;
            txtTGTT_TTDB.DecimalDigits = GlobalSettings.TriGiaNT;
            txtTGTT_GTGT.DecimalDigits = GlobalSettings.TriGiaNT;
            txtCLG.DecimalDigits = GlobalSettings.TriGiaNT;
        }
        private void SetVisibleHTS(bool b)
        {
            txtMa_HTS.Visible = lbl_MaHTS.Visible = txtSoLuong_HTS.Visible = lblSoLuong_HTS.Visible = cbbDVT_HTS.Visible = lblDVT_HTS.Visible = b;
            if (!b)
            {
                txtMaHS.Width = txtTenHang.Width = cbDonViTinh.Width = txtLuong.Width = txtMaHang.Width;
            }
        }
        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            cbbDVT_HTS.DataSource = this._DonViTinh;
            cbbDVT_HTS.SelectedValue = "15 ";
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;

            //Ma loai hinh cu
            switch (this.TKCT.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                    break;
                case "SP":
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                    break;
                case "TB":
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                    break;
            }

            //Ma loai hinh moi
            switch (this.TKCT.MaLoaiHinh.Substring(3, 2))
            {
                case "18"://"PL":
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                    break;
                case "19"://"SP":
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                    break;
                case "20"://"TB":
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                    break;
            }
        }

        private void HangMauDichEditForm_Load(object sender, EventArgs e)
        {
            revMaHS.ValidationExpression = @"\d{8,10}";

            ctrNuocXX.Ma = this.HCT.ID_NuocXX;
            NhomLoaiHinh = TKCT.MaLoaiHinh.Substring(0, 3);

            LoaiHangHoa = "N";
            if (TKCT.MaLoaiHinh.IndexOf("SP") > 0 || TKCT.MaLoaiHinh.IndexOf("19") > 0)
                LoaiHangHoa = "S";
            else if (TKCT.MaLoaiHinh.IndexOf("TB") > 0 || TKCT.MaLoaiHinh.IndexOf("20") > 0)
                LoaiHangHoa = "T";

            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();
            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.TKCT.NguyenTe_ID + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.TKCT.NguyenTe_ID);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.TKCT.NguyenTe_ID);

            // Tính lại thuế.
            //this.HCT.TinhThue(this.TyGiaTT);
            lblTyGiaTT.Text += " : " + this.TyGiaTT.ToString("N");

            if (!TKCT.MaLoaiHinh.EndsWith("N") || !TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
            {
                lblSoLuongDaCo.Text = setText("Lượng còn được giao", "Quantity can be exported");
            }

            // Bind data.
            txtMaHang.Text = this.HCT.MaHang;
            this.txtMaHang_Leave(null, null);
            txtTenHang.Text = this.HCT.TenHang;
            txtMaHS.Text = this.HCT.MaHS;
            txtMa_HTS.Text = this.HCT.Ma_HTS;
            txtSoLuong_HTS.Value = this.HCT.SoLuong_HTS;
            cbDonViTinh.SelectedValue = this.HCT.ID_DVT;
            cbbDVT_HTS.SelectedValue = this.HCT.DVT_HTS;
            //luu lai ma hang
            MaHangOld = this.HCT.MaHang;

            txtDGNT.Value = this.dgnt = this.HCT.DonGia;
            txtLuong.Value = this.luong = this.HCT.SoLuong;
            txtTGNT.Value = this.HCT.TriGia;
            txtTriGiaKB.Value = this.HCT.TriGiaKB_VND;
            ctrNuocXX.Ma = this.HCT.ID_NuocXX;
            txtTGTT_NK.Value = this.HCT.TriGiaTT;
            txtTS_NK.Value = this.ts_nk = this.HCT.ThueSuatXNK;
            txtTS_TTDB.Value = this.ts_ttdb = this.HCT.ThueSuatTTDB;
            txtTS_GTGT.Value = this.ts_gtgt = this.HCT.ThueSuatGTGT;
            txtTL_CLG.Value = this.tl_clg = this.HCT.TyLeThuKhac;

            txtTSXNKGiam.Text = HCT.ThueSuatXNKGiam;
            txtTSTTDBGiam.Text = HCT.ThueSuatTTDBGiam;
            txtTSVatGiam.Text = HCT.ThueSuatVATGiam;
            this.tinhthue2();
            if (this.OpenType == OpenFormType.View)
                btnGhi.Enabled = false;
            if (TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                btnGhi.Enabled = true;
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
            this.luong = Convert.ToDecimal(txtLuong.Value);
            txtSoLuong_HTS.Value = Math.Round(this.luong / 12);
            LuongCon = Convert.ToDecimal(txtSoLuongConDcNhap.Text);
            if (this.luong > LuongCon)
            {
                if (NhomLoaiHinh.StartsWith("N"))
                {
                    //if (!(MLMessages("Bạn đã nhập quá số lượng có thể nhập.Bạn có muốn tiếp tục không?","MSG_WRN16","", true) == "Yes"))
                    if (!(showMsg("MSG_WRN07", true) == "Yes"))
                    {
                        luong = 0;
                        txtLuong.Value = "0";
                    }
                }
                else
                {
                    //if (!(MLMessages("Bạn đã xuất quá số lượng có thể xuất.Bạn có muốn tiếp tục không?", "MSG_WRN16", "", true) == "Yes"))
                    if (!(showMsg("MSG_WRN08", true) == "Yes"))
                    {
                        luong = 0;
                        txtLuong.Value = "0";
                    }
                }
            }
            LuongCon = 0;
            this.tinhthue();
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            this.dgnt = Convert.ToDecimal(txtDGNT.Value);
            this.tinhthue();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            if (this.LoaiHangHoa == "N")
            {
                Company.Interface.GC.NguyenPhuLieuRegistedForm f2 = new Company.Interface.GC.NguyenPhuLieuRegistedForm();
                f2.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                f2.isBrower = true;
                f2.ShowDialog();
                if (f2.NguyenPhuLieuSelected.Ma != "")
                {
                    txtMaHang.Text = f2.NguyenPhuLieuSelected.Ma;
                    txtTenHang.Text = f2.NguyenPhuLieuSelected.Ten;
                    txtMaHS.Text = f2.NguyenPhuLieuSelected.MaHS;
                    cbDonViTinh.SelectedValue = f2.NguyenPhuLieuSelected.DVT_ID;
                    if (NhomLoaiHinh == "NGC")
                    {
                        LuongCon = f2.NguyenPhuLieuSelected.SoLuongDangKy - f2.NguyenPhuLieuSelected.SoLuongDaDung - f2.NguyenPhuLieuSelected.SoLuongCungUng;
                        if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == f2.NguyenPhuLieuSelected.Ma.ToUpper().Trim())
                            LuongCon += HCT.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    else
                    {
                        LuongCon = f2.NguyenPhuLieuSelected.SoLuongDaNhap - f2.NguyenPhuLieuSelected.SoLuongDaDung;// +f2.NguyenPhuLieuSelected.SoLuongCungUng;
                        if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == f2.NguyenPhuLieuSelected.Ma.ToUpper().Trim())
                            LuongCon += HCT.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                }
            }
            else if (LoaiHangHoa == "S")
            {
                Company.Interface.GC.SanPhamRegistedForm f3 = new Company.Interface.GC.SanPhamRegistedForm();
                f3.isBrower = true;
                f3.SanPhamSelected.HopDong_ID = HD.ID;
                f3.ShowDialog();
                if (f3.SanPhamSelected.Ma != "")
                {
                    txtMaHang.Text = f3.SanPhamSelected.Ma;
                    txtTenHang.Text = f3.SanPhamSelected.Ten;
                    txtMaHS.Text = f3.SanPhamSelected.MaHS;
                    cbDonViTinh.SelectedValue = f3.SanPhamSelected.DVT_ID;
                    LuongCon = f3.SanPhamSelected.SoLuongDangKy - f3.SanPhamSelected.SoLuongDaXuat;
                    if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == f3.SanPhamSelected.Ma.ToUpper().Trim())
                        LuongCon += HCT.SoLuong;
                    txtSoLuongConDcNhap.Text = LuongCon.ToString();
                }
            }
            else
            {
                Company.Interface.GC.ThietBiRegistedForm ftb = new Company.Interface.GC.ThietBiRegistedForm();
                ftb.ThietBiSelected.HopDong_ID = this.HD.ID;
                ftb.isBrower = true;
                ftb.ShowDialog();
                if (ftb.ThietBiSelected.Ma != "")
                {
                    txtMaHang.Text = ftb.ThietBiSelected.Ma;
                    txtTenHang.Text = ftb.ThietBiSelected.Ten;
                    txtMaHS.Text = ftb.ThietBiSelected.MaHS;
                    cbDonViTinh.SelectedValue = ftb.ThietBiSelected.DVT_ID;
                    if (NhomLoaiHinh == "NGC")
                    {
                        LuongCon = ftb.ThietBiSelected.SoLuongDangKy - ftb.ThietBiSelected.SoLuongDaNhap;
                        if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == ftb.ThietBiSelected.Ma.ToUpper().Trim())
                            LuongCon += HCT.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    else
                    {
                        LuongCon = ftb.ThietBiSelected.SoLuongDaNhap;
                        if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == ftb.ThietBiSelected.Ma.ToUpper().Trim())
                            LuongCon += HCT.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                }
            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (TKCT.ID > 0)
            {
                if (TKCT.MaLoaiHinh.Contains("XGC"))
                {
                    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKCT.ID))
                    {
                        showMsg("MSG_ALL01");
                        //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                        return;
                    }
                }
            }
            KiemTraTonTaiHang();
            this.tinhthue();
            txtLuong.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "This value is invalid"));
                return;
            }
            if (checkMaHangExit(txtMaHang.Text.Trim()))
            {
                showMsg("MSG_0203054");
                //ShowMessage("Đã có hàng này trong danh sách.", false);
                return;
            }

            this.HCT.MaHS = txtMaHS.Text;
            this.HCT.Ma_HTS = txtMa_HTS.Text;
            this.HCT.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
            this.HCT.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
            this.HCT.MaHang = txtMaHang.Text;
            this.HCT.TenHang = txtTenHang.Text;
            this.HCT.ID_NuocXX = ctrNuocXX.Ma;
            this.HCT.ID_DVT = cbDonViTinh.SelectedValue.ToString();
            this.HCT.SoLuong = Convert.ToDecimal(txtLuong.Text);
            this.HCT.DonGia = Convert.ToDecimal(txtDGNT.Text);
            this.HCT.TriGia = Convert.ToDecimal(txtTGNT.Text);
            this.HCT.Ma_HTS = txtMa_HTS.Text;
            this.HCT.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
            this.HCT.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
            HCT.TriGiaKB_VND = Convert.ToDecimal(txtTriGiaKB.Value);
            HCT.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Value);
            HCT.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Value);
            HCT.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Value);
            HCT.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Value);
            HCT.TyLeThuKhac = Convert.ToDecimal(txtTL_CLG.Value);
            HCT.ThueXNK = Math.Round(Convert.ToDecimal(txtTienThue_NK.Value), 0);
            HCT.ThueTTDB = Math.Round(Convert.ToDecimal(txtTienThue_TTDB.Value), 0);
            HCT.ThueGTGT = Math.Round(Convert.ToDecimal(txtTienThue_GTGT.Value), 0);
            HCT.DonGiaTT = HCT.TriGiaTT / Convert.ToDecimal(HCT.SoLuong);
            HCT.TriGiaThuKhac = Convert.ToDecimal(txtTien_CLG.Value);
            HCT.ThueSuatXNKGiam = txtTSXNKGiam.Text.Trim();
            HCT.ThueSuatTTDBGiam = txtTSTTDBGiam.Text.Trim();
            HCT.ThueSuatVATGiam = txtTSVatGiam.Text.Trim();
            if (chkMienThue.Checked)
                HCT.MienThue = 1;
            else
                HCT.MienThue = 0;
            // Tính thuế.                      
            this.IsEdited = true;
            txtMaHang.Text = "";
            txtTenHang.Text = "";
            txtMaHS.Text = "";
            txtLuong.Value = 0;
            txtDGNT.Value = 0;
            this.Close();
        }
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangChuyenTiep HCT in this.TKCT.HCTCollection)
            {
                if (HCT.MaHang.Trim() == maHang && maHang != MaHangOld) return true;
            }
            return false;
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.IsDeleted = true;
            this.Close();
        }

        private void txtMaHang_Leave(object sender, EventArgs e)
        {
            if (LoaiHangHoa == "N")
            {
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = txtMaHang.Text.Trim();
                if (npl.Load())
                {
                    txtMaHang.Text = npl.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = npl.MaHS;
                    txtTenHang.Text = npl.Ten;
                    cbDonViTinh.SelectedValue = npl.DVT_ID;
                    if (NhomLoaiHinh == "NGC")
                    {
                        LuongCon = npl.SoLuongDangKy - npl.SoLuongDaNhap - npl.SoLuongCungUng; ;
                        if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == npl.Ma.ToUpper().Trim())
                            LuongCon += HCT.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    else
                    {
                        LuongCon = npl.SoLuongDaNhap - npl.SoLuongDaDung;//+ npl.SoLuongCungUng; ;
                        if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == npl.Ma.ToUpper().Trim())
                            LuongCon += HCT.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                    txtSoLuongConDcNhap.Text = "0";
                    return;
                }
            }
            else if (LoaiHangHoa == "S")
            {
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = txtMaHang.Text.Trim();
                if (sp.Load())
                {
                    txtMaHang.Text = sp.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    cbDonViTinh.SelectedValue = sp.DVT_ID;
                    LuongCon = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                    if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == sp.Ma.ToUpper().Trim())
                        LuongCon += HCT.SoLuong;
                    txtSoLuongConDcNhap.Text = LuongCon.ToString();
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại sản phẩm này.", "This value is not exist"));

                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                    txtSoLuongConDcNhap.Text = "0";
                    return;
                }
            }
            else if (LoaiHangHoa == "T")
            {
                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                tb.HopDong_ID = HD.ID;
                tb.Ma = txtMaHang.Text.Trim();
                if (tb.Load())
                {
                    txtMaHang.Text = tb.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = tb.MaHS;
                    txtTenHang.Text = tb.Ten;
                    cbDonViTinh.SelectedValue = tb.DVT_ID;
                    if (NhomLoaiHinh.StartsWith("NGC"))
                    {
                        LuongCon = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                        if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == tb.Ma.ToUpper().Trim())
                            LuongCon += HCT.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    else
                    {
                        LuongCon = tb.SoLuongDaNhap;
                        if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == tb.Ma.ToUpper().Trim())
                            LuongCon += HCT.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));

                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                    txtSoLuongConDcNhap.Text = "0";
                    return;
                }
            }
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS, setText("Mã HS không hợp lệ.", "This value is invalid"));
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }
            string MoTa = MaHS.CheckExist(txtMaHS.Text);
            if (MoTa == "")
            {
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS, setText("Mã HS không có trong danh mục mã HS.", "This value is not exist"));
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }
        }
        private decimal tinhthue2()
        {
            this.dgnt = Convert.ToDecimal(txtDGNT.Text);
            this.luong = Convert.ToDecimal(txtLuong.Text);
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;

            this.tgnt = this.dgnt * Convert.ToDecimal(this.luong);
            this.tgtt_nk = Convert.ToDecimal(txtTGTT_NK.Value);
            if (tgtt_nk == 0)
            {
                tgtt_nk = tgnt * TyGiaTT;
            }

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private decimal tinhthue()
        {
            this.tgnt = this.dgnt * Convert.ToDecimal(this.luong);
            this.tgtt_nk = this.tgnt * this.TyGiaTT;

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            this.tinhthue2();
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            tinhthue2();
        }

        private void txtTS_TTDB_Leave(object sender, EventArgs e)
        {
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            tinhthue2();
        }

        private void txtTS_GTGT_Leave(object sender, EventArgs e)
        {
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            tinhthue2();
        }

        private void txtTL_CLG_Leave(object sender, EventArgs e)
        {
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;
            tinhthue2();
        }

        private void txtTGNT_Leave(object sender, EventArgs e)
        {
            decimal trigia = Convert.ToDecimal(txtTGNT.Text);
            decimal luong = Convert.ToDecimal(txtLuong.Text);
            decimal dongia = trigia / luong;
            txtDGNT.Text = dongia.ToString();
            this.dgnt = Convert.ToDecimal(txtDGNT.Value);
            this.tinhthue();
        }

    }
}
