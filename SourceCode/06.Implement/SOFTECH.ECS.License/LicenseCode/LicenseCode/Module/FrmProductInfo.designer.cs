namespace LicenseCode.Module
{
    partial class FrmProductInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProductInfo));
            this.panelTop = new DevExpress.XtraEditors.PanelControl();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblCaption = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.txtGroupDesc = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupProdInfo = new DevExpress.XtraEditors.GroupControl();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtGroupCreatedDate = new DevExpress.XtraEditors.TextEdit();
            this.txtGroupCreator = new DevExpress.XtraEditors.TextEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtProductPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtProductCreator = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtProductCreatedDate = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtProductDesc = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.txtProductName = new DevExpress.XtraEditors.TextEdit();
            this.cmbGroupName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbGroupManager = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtProductID = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupProdInfo)).BeginInit();
            this.groupProdInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupCreatedDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupCreator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductCreator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductCreatedDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGroupName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGroupManager.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductID.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelTop.Controls.Add(this.pnlTop);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(703, 67);
            this.panelTop.TabIndex = 1;
            // 
            // pnlTop
            // 
            this.pnlTop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTop.BackgroundImage")));
            this.pnlTop.Controls.Add(this.lblCaption);
            this.pnlTop.Controls.Add(this.pictureBox1);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(703, 67);
            this.pnlTop.TabIndex = 10;
            // 
            // lblCaption
            // 
            this.lblCaption.Appearance.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblCaption.Appearance.Options.UseFont = true;
            this.lblCaption.Appearance.Options.UseForeColor = true;
            this.lblCaption.Location = new System.Drawing.Point(72, 8);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(234, 31);
            this.lblCaption.TabIndex = 2;
            this.lblCaption.Text = "Thông tin sản phẩm";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(69, 67);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(179, 209);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(59, 22);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.Location = new System.Drawing.Point(114, 209);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(59, 22);
            this.btnEdit.TabIndex = 9;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtGroupDesc
            // 
            this.txtGroupDesc.Location = new System.Drawing.Point(119, 82);
            this.txtGroupDesc.Name = "txtGroupDesc";
            this.txtGroupDesc.Size = new System.Drawing.Size(202, 58);
            this.txtGroupDesc.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 33);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(100, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "Tên nhóm sản phẩm:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(13, 85);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(31, 13);
            this.labelControl3.TabIndex = 13;
            this.labelControl3.Text = "Mô tả:";
            // 
            // groupProdInfo
            // 
            this.groupProdInfo.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupProdInfo.AppearanceCaption.Options.UseFont = true;
            this.groupProdInfo.Controls.Add(this.btnAdd);
            this.groupProdInfo.Controls.Add(this.cmbGroupManager);
            this.groupProdInfo.Controls.Add(this.cmbGroupName);
            this.groupProdInfo.Controls.Add(this.labelControl6);
            this.groupProdInfo.Controls.Add(this.labelControl8);
            this.groupProdInfo.Controls.Add(this.labelControl7);
            this.groupProdInfo.Controls.Add(this.labelControl2);
            this.groupProdInfo.Controls.Add(this.labelControl3);
            this.groupProdInfo.Controls.Add(this.txtGroupDesc);
            this.groupProdInfo.Controls.Add(this.txtGroupCreatedDate);
            this.groupProdInfo.Controls.Add(this.txtGroupCreator);
            this.groupProdInfo.Controls.Add(this.btnEdit);
            this.groupProdInfo.Controls.Add(this.btnSave);
            this.groupProdInfo.Controls.Add(this.btnCancel);
            this.groupProdInfo.Location = new System.Drawing.Point(0, 67);
            this.groupProdInfo.Name = "groupProdInfo";
            this.groupProdInfo.Size = new System.Drawing.Size(339, 341);
            this.groupProdInfo.TabIndex = 14;
            this.groupProdInfo.Text = "Thông tin nhóm sản phẩm";
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(48, 209);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(60, 22);
            this.btnAdd.TabIndex = 15;
            this.btnAdd.Text = "Thêm";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(13, 59);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(70, 13);
            this.labelControl6.TabIndex = 13;
            this.labelControl6.Text = "Quản lý nhóm:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(13, 175);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(48, 13);
            this.labelControl8.TabIndex = 13;
            this.labelControl8.Text = "Ngày tạo:";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(13, 149);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(51, 13);
            this.labelControl7.TabIndex = 13;
            this.labelControl7.Text = "Người tạo:";
            // 
            // txtGroupCreatedDate
            // 
            this.txtGroupCreatedDate.Location = new System.Drawing.Point(119, 172);
            this.txtGroupCreatedDate.Name = "txtGroupCreatedDate";
            this.txtGroupCreatedDate.Properties.Mask.EditMask = "F";
            this.txtGroupCreatedDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtGroupCreatedDate.Size = new System.Drawing.Size(201, 20);
            this.txtGroupCreatedDate.TabIndex = 4;
            // 
            // txtGroupCreator
            // 
            this.txtGroupCreator.Location = new System.Drawing.Point(119, 146);
            this.txtGroupCreator.Name = "txtGroupCreator";
            this.txtGroupCreator.Size = new System.Drawing.Size(201, 20);
            this.txtGroupCreator.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(244, 209);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 22);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.txtProductPrefix);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.txtProductID);
            this.groupControl1.Controls.Add(this.txtProductName);
            this.groupControl1.Controls.Add(this.txtProductCreator);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl11);
            this.groupControl1.Controls.Add(this.txtProductCreatedDate);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.txtProductDesc);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.simpleButton3);
            this.groupControl1.Controls.Add(this.simpleButton2);
            this.groupControl1.Controls.Add(this.simpleButton1);
            this.groupControl1.Location = new System.Drawing.Point(345, 67);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(355, 341);
            this.groupControl1.TabIndex = 14;
            this.groupControl1.Text = "Thông tin sản phẩm";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(12, 85);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(78, 13);
            this.labelControl10.TabIndex = 13;
            this.labelControl10.Text = "Tiền tố (3 kí tự):";
            // 
            // txtProductPrefix
            // 
            this.txtProductPrefix.Location = new System.Drawing.Point(118, 82);
            this.txtProductPrefix.Name = "txtProductPrefix";
            this.txtProductPrefix.Size = new System.Drawing.Size(201, 20);
            this.txtProductPrefix.TabIndex = 2;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(12, 175);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(48, 13);
            this.labelControl9.TabIndex = 13;
            this.labelControl9.Text = "Ngày tạo:";
            // 
            // txtProductCreator
            // 
            this.txtProductCreator.Location = new System.Drawing.Point(118, 146);
            this.txtProductCreator.Name = "txtProductCreator";
            this.txtProductCreator.Size = new System.Drawing.Size(201, 20);
            this.txtProductCreator.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(12, 149);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(51, 13);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "Người tạo:";
            // 
            // txtProductCreatedDate
            // 
            this.txtProductCreatedDate.Location = new System.Drawing.Point(118, 172);
            this.txtProductCreatedDate.Name = "txtProductCreatedDate";
            this.txtProductCreatedDate.Properties.Mask.EditMask = "F";
            this.txtProductCreatedDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txtProductCreatedDate.Size = new System.Drawing.Size(201, 20);
            this.txtProductCreatedDate.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(12, 59);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(71, 13);
            this.labelControl4.TabIndex = 13;
            this.labelControl4.Text = "Tên sản phẩm:";
            // 
            // txtProductDesc
            // 
            this.txtProductDesc.Location = new System.Drawing.Point(118, 108);
            this.txtProductDesc.Name = "txtProductDesc";
            this.txtProductDesc.Size = new System.Drawing.Size(202, 32);
            this.txtProductDesc.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 111);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(31, 13);
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = "Mô tả:";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(141, 209);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(53, 22);
            this.simpleButton3.TabIndex = 9;
            this.simpleButton3.Text = "Sửa";
            this.simpleButton3.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(200, 209);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(59, 22);
            this.simpleButton2.TabIndex = 5;
            this.simpleButton2.Text = "Lưu";
            this.simpleButton2.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(265, 209);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(54, 22);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "Hủy";
            this.simpleButton1.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // txtProductName
            // 
            this.txtProductName.Location = new System.Drawing.Point(118, 56);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.Size = new System.Drawing.Size(201, 20);
            this.txtProductName.TabIndex = 1;
            // 
            // cmbGroupName
            // 
            this.cmbGroupName.Location = new System.Drawing.Point(119, 30);
            this.cmbGroupName.Name = "cmbGroupName";
            this.cmbGroupName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbGroupName.Size = new System.Drawing.Size(202, 20);
            this.cmbGroupName.TabIndex = 0;
            // 
            // cmbGroupManager
            // 
            this.cmbGroupManager.Location = new System.Drawing.Point(119, 56);
            this.cmbGroupManager.Name = "cmbGroupManager";
            this.cmbGroupManager.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbGroupManager.Size = new System.Drawing.Size(202, 20);
            this.cmbGroupManager.TabIndex = 1;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(12, 33);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(67, 13);
            this.labelControl11.TabIndex = 13;
            this.labelControl11.Text = "Mã sản phẩm:";
            // 
            // txtProductID
            // 
            this.txtProductID.Location = new System.Drawing.Point(118, 30);
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.Size = new System.Drawing.Size(201, 20);
            this.txtProductID.TabIndex = 0;
            // 
            // FrmProductInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 408);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupProdInfo);
            this.Controls.Add(this.panelTop);
            this.Name = "FrmProductInfo";
            this.Text = "Thông tin sản phẩm";
            this.Load += new System.EventHandler(this.FrmProductInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupProdInfo)).EndInit();
            this.groupProdInfo.ResumeLayout(false);
            this.groupProdInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupCreatedDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupCreator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductCreator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductCreatedDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGroupName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGroupManager.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductID.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelTop;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.MemoEdit txtGroupDesc;
        private System.Windows.Forms.Panel pnlTop;
        private DevExpress.XtraEditors.LabelControl lblCaption;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.GroupControl groupProdInfo;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtGroupCreator;
        private DevExpress.XtraEditors.TextEdit txtGroupCreatedDate;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtProductPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtProductCreator;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtProductCreatedDate;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.MemoEdit txtProductDesc;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.TextEdit txtProductName;
        private DevExpress.XtraEditors.ComboBoxEdit cmbGroupManager;
        private DevExpress.XtraEditors.ComboBoxEdit cmbGroupName;
        private DevExpress.XtraEditors.TextEdit txtProductID;
        private DevExpress.XtraEditors.LabelControl labelControl11;
    }
}