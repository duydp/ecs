using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components.Feedback.Components
{
	public partial class LinhVuc
	{
		#region Properties.
		
		public int ID { set; get; }
		public string Ma { set; get; }
		public string Ten { set; get; }
		public bool HienThi { set; get; }
		public bool MacDinh { set; get; }
		public int ThuTu { set; get; }
		public int ParentID { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<LinhVuc> ConvertToCollection(IDataReader reader)
        {
            IList<LinhVuc> collection = new List<LinhVuc>();
            while (reader.Read())
            {
                LinhVuc entity = new LinhVuc();
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("HienThi"))) entity.HienThi = reader.GetBoolean(reader.GetOrdinal("HienThi"));
                if (!reader.IsDBNull(reader.GetOrdinal("MacDinh"))) entity.MacDinh = reader.GetBoolean(reader.GetOrdinal("MacDinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThuTu"))) entity.ThuTu = reader.GetInt32(reader.GetOrdinal("ThuTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ParentID"))) entity.ParentID = Convert.ToInt32(reader.GetString(reader.GetOrdinal("ParentID")));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static bool Find(IList<LinhVuc> collection, string ma)
        {
            foreach (LinhVuc item in collection)
            {
                if (item.Ma == ma)
                {
                    return true;
                }
            }

            return false;
        }

        public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_GopY_LinhVuc VALUES(@Ma, @Ten, @HienThi, @MacDinh, @ThuTu, @ParentID)";
            string update = "UPDATE t_GopY_LinhVuc SET Ten = @Ten, HienThi = @HienThi, MacDinh = @MacDinh, ThuTu = @ThuTu, ParentID = @ParentID WHERE Ma = @Ma";
            string delete = "DELETE FROM t_GopY_LinhVuc WHERE Ma = @Ma";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@HienThi", SqlDbType.Bit, "HienThi", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MacDinh", SqlDbType.Bit, "MacDinh", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@ThuTu", SqlDbType.Int, "ThuTu", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@ParentID", SqlDbType.VarChar, "ParentID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@HienThi", SqlDbType.Bit, "HienThi", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MacDinh", SqlDbType.Bit, "MacDinh", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@ThuTu", SqlDbType.Int, "ThuTu", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@ParentID", SqlDbType.VarChar, "ParentID", DataRowVersion.Current);

            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

        public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_GopY_LinhVuc VALUES(@Ma, @Ten, @HienThi, @MacDinh, @ThuTu, @ParentID)";
            string update = "UPDATE t_GopY_LinhVuc SET Ten = @Ten, HienThi = @HienThi, MacDinh = @MacDinh, ThuTu = @ThuTu, ParentID = @ParentID WHERE Ma = @Ma";
            string delete = "DELETE FROM t_GopY_LinhVuc WHERE Ma = @Ma";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@HienThi", SqlDbType.Bit, "HienThi", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MacDinh", SqlDbType.Bit, "MacDinh", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@ThuTu", SqlDbType.Int, "ThuTu", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@ParentID", SqlDbType.VarChar, "ParentID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@HienThi", SqlDbType.Bit, "HienThi", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MacDinh", SqlDbType.Bit, "MacDinh", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@ThuTu", SqlDbType.Int, "ThuTu", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@ParentID", SqlDbType.VarChar, "ParentID", DataRowVersion.Current);

            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

        #endregion
		

		#region Select methods.
		
		public static LinhVuc Load(int id)
		{
			const string spName = "[dbo].[p_GopY_LinhVuc_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
			LinhVuc entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new LinhVuc();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThi"))) entity.HienThi = reader.GetBoolean(reader.GetOrdinal("HienThi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MacDinh"))) entity.MacDinh = reader.GetBoolean(reader.GetOrdinal("MacDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThuTu"))) entity.ThuTu = reader.GetInt32(reader.GetOrdinal("ThuTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("ParentID"))) entity.ParentID = reader.GetInt32(reader.GetOrdinal("ParentID"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<LinhVuc> SelectCollectionAll()
		{
			List<LinhVuc> collection = new List<LinhVuc>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				LinhVuc entity = new LinhVuc();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThi"))) entity.HienThi = reader.GetBoolean(reader.GetOrdinal("HienThi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MacDinh"))) entity.MacDinh = reader.GetBoolean(reader.GetOrdinal("MacDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThuTu"))) entity.ThuTu = reader.GetInt32(reader.GetOrdinal("ThuTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("ParentID"))) entity.ParentID = reader.GetInt32(reader.GetOrdinal("ParentID"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<LinhVuc> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<LinhVuc> collection = new List<LinhVuc>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				LinhVuc entity = new LinhVuc();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThi"))) entity.HienThi = reader.GetBoolean(reader.GetOrdinal("HienThi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MacDinh"))) entity.MacDinh = reader.GetBoolean(reader.GetOrdinal("MacDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThuTu"))) entity.ThuTu = reader.GetInt32(reader.GetOrdinal("ThuTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("ParentID"))) entity.ParentID = reader.GetInt32(reader.GetOrdinal("ParentID"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_GopY_LinhVuc_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GopY_LinhVuc_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_GopY_LinhVuc_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GopY_LinhVuc_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertLinhVuc(string ma, string ten, bool hienThi, bool macDinh, int thuTu, int parentID)
		{
			LinhVuc entity = new LinhVuc();	
			entity.Ma = ma;
			entity.Ten = ten;
			entity.HienThi = hienThi;
			entity.MacDinh = macDinh;
			entity.ThuTu = thuTu;
			entity.ParentID = parentID;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_GopY_LinhVuc_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@HienThi", SqlDbType.Bit, HienThi);
			db.AddInParameter(dbCommand, "@MacDinh", SqlDbType.Bit, MacDinh);
			db.AddInParameter(dbCommand, "@ThuTu", SqlDbType.Int, ThuTu);
			db.AddInParameter(dbCommand, "@ParentID", SqlDbType.Int, ParentID);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<LinhVuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LinhVuc item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateLinhVuc(int id, string ma, string ten, bool hienThi, bool macDinh, int thuTu, int parentID)
		{
			LinhVuc entity = new LinhVuc();			
			entity.ID = id;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.HienThi = hienThi;
			entity.MacDinh = macDinh;
			entity.ThuTu = thuTu;
			entity.ParentID = parentID;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_GopY_LinhVuc_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@HienThi", SqlDbType.Bit, HienThi);
			db.AddInParameter(dbCommand, "@MacDinh", SqlDbType.Bit, MacDinh);
			db.AddInParameter(dbCommand, "@ThuTu", SqlDbType.Int, ThuTu);
			db.AddInParameter(dbCommand, "@ParentID", SqlDbType.Int, ParentID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<LinhVuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LinhVuc item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateLinhVuc(int id, string ma, string ten, bool hienThi, bool macDinh, int thuTu, int parentID)
		{
			LinhVuc entity = new LinhVuc();			
			entity.ID = id;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.HienThi = hienThi;
			entity.MacDinh = macDinh;
			entity.ThuTu = thuTu;
			entity.ParentID = parentID;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GopY_LinhVuc_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@HienThi", SqlDbType.Bit, HienThi);
			db.AddInParameter(dbCommand, "@MacDinh", SqlDbType.Bit, MacDinh);
			db.AddInParameter(dbCommand, "@ThuTu", SqlDbType.Int, ThuTu);
			db.AddInParameter(dbCommand, "@ParentID", SqlDbType.Int, ParentID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<LinhVuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LinhVuc item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteLinhVuc(int id)
		{
			LinhVuc entity = new LinhVuc();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GopY_LinhVuc_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_GopY_LinhVuc_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<LinhVuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LinhVuc item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}