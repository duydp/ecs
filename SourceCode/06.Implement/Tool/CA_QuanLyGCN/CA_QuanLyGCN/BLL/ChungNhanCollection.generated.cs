﻿using System;
using System.Text;
using System.Collections;
using System.ComponentModel;

namespace CA_QuanLyGCN
{
    public class ChungNhanCollections : CollectionBase
    {
        #region Constructor

        public ChungNhanCollections() { }
        #endregion Constructor

        #region Indexer Methods

        public ChungNhan this[int index]
        {
            get { return (ChungNhan)List[index]; }
            set { List[index] = value; }
        }

        #endregion Indexer Methods

        #region Private Methods

        public int Add(ChungNhan value)
        {
            return List.Add(value);
        }

        public void Insert(int index, ChungNhan value)
        {
            List.Insert(index, value);
        }

        public void Remove(ChungNhan value)
        {
            List.Remove(value);
        }

        public bool Contains(string id)
        {
            for (int i = 0; i < List.Count; i++)
            {
                ChungNhan ChungNhanEntity = (ChungNhan)List[i];
                if (ChungNhanEntity.Id.Equals(id) && (ChungNhanEntity.EntityState != EntityState.Deleted))
                {
                    return true;
                }
            }
            return false;
        }

        public ChungNhan SearchById(int id)
        {
            for (int i = 0; i < List.Count; i++)
            {
                ChungNhan ChungNhanEntity = (ChungNhan)List[i];
                if ((ChungNhanEntity.Id.Equals(id)) && (ChungNhanEntity.EntityState != EntityState.Deleted))
                {
                    return ChungNhanEntity;
                }
            }
            return null;
        }
/*
        public ChungNhan SearchByTen(string name)
        {
            for (int i = 0; i < List.Count; i++)
            {
                ChungNhan ChungNhanEntity = (ChungNhan)List[i];
                if ((ChungNhanEntity.Name.ToLower().Trim().Equals(name.ToLower().Trim())) && (ChungNhanEntity.EntityState != EntityState.Deleted))
                {
                    return ChungNhanEntity;
                }
            }
            return null;
        }
*/
        public void Save()
        {
            for (int i = 0; i < List.Count; i++)
            {
                ChungNhan ChungNhanEntity = (ChungNhan)List[i];
                switch (ChungNhanEntity.EntityState)
                {
                    case EntityState.Deleted:
                        ChungNhanEntity.Delete();
                        break;
                    case EntityState.Added:
                        ChungNhanEntity.Insert();
                        break;
                    case EntityState.Updated:
                        ChungNhanEntity.Update();
                        break;
                }
                ChungNhanEntity.EntityState = EntityState.Unchanged;
            }

        }

        #endregion Private Methods
						
		#region Clone Method

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public ChungNhanCollections Clone()
        {
            ChungNhanCollections _tmpCollections = new ChungNhanCollections();

            foreach (ChungNhan item in this)
            {
                _tmpCollections.Add((ChungNhan)item.Clone());
            }

            return _tmpCollections;
        }

        #endregion

    }
}

