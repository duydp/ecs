using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
//using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Globalization;

namespace Company.Interface.Report.GC
{
    public partial class BangKe01H_HQGC : DevExpress.XtraReports.UI.XtraReport
    {
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        private int STT = 0;
        
        public BangKe01H_HQGC()
        {
            InitializeComponent();
        }

        public void BindReport(string where)
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + "," + dr["TenSanPham"].ToString();
                }
            }
            lblMathang.Text = loaiSP;
            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;
            DataTable  dtToKhai = new Company.GC.BLL.KDT.ToKhaiMauDich().GetToKhaiNhapInfo(this.HD.ID).Tables[0];
            dtToKhai.TableName = "BangKe01H_HQGC"; 
            this.DataSource = dtToKhai ;
            //lblSoToKhai.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".SoToKhai","{0:N0}");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".NgayDangKy", "{0:dd/MM/yyyy}"); 
            lblMaHang.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".MaHang" );
            lblTenHang.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".TenHang");
            lblLuong.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".SoLuong", "{0:N2}");
            lblTongLuong.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".SoLuong", "{0:N2}");
            lblDVT.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".DVT");
            lblSumLuong.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".SoLuong", "{0:N2}");
            xrLabel19.Text = GlobalSettings.TieudeNgay;
            //GlobalSettings.KhoiTao_GiaTriMacDinh();
            
          

            //lblTongLuongNPLSuDung.Summary.Running = SummaryRunning.Group;
            //lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Sum;
            //lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";
            //if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            //{
            //    this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            //    new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            //    new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            //    new DevExpress.XtraReports.UI.GroupField("MaNPL", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            //    new DevExpress.XtraReports.UI.GroupField("DonGiaTT", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            //    new DevExpress.XtraReports.UI.GroupField("ThueNKNop", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});

            //}
            //else
            //{
            //    this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            //    new DevExpress.XtraReports.UI.GroupField("MaNPL", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            //    new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            //    new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            //    new DevExpress.XtraReports.UI.GroupField("DonGiaTT", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            //    new DevExpress.XtraReports.UI.GroupField("ThueNKNop", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            //}
            
            //lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            //lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            //if(SoHoSo>0)
            //    lblSHSTK.Text =  SoHoSo +"";
            //lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            ////lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
            //lblSoToKhai.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            //lblNgayDangKy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            //lblNgayThucNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayThucNhap", "{0:dd/MM/yy}");
            //lblTenHang.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DonGiaTT", "{0:g20}");
            //lblLuong.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TyGiaTT", "{0:n3}");
            //lblThueSuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueSuat", "{0:n0}");
            //lblThueNKNop.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueNKNop", "{0:n0}");
            //lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            //lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            //lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            //lblNgayThucXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayThucXuat", "{0:dd/MM/yy}");
            //lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTon", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            ////lblTongTienThueHoan.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TienThueHoan", "{0:n0}");
            ////lblTongTienThueTKTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TienThueTKTiep", "{0:n0}");
            ////xrTableCell21.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
            //xrTableCell22.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //xrTableCell23.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            //xrTableCell24.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DonGiaTT", "{0:g20}");
            //xrTableCell25.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TyGiaTT", "{0:n3}");
            //xrTableCell26.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueSuat", "{0:n0}");
            //xrTableCell27.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueNKNop", "{0:n0}");
            //lblTongThueKhongThu.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TienThueHoan", "{0:n0}");
        }

        private void BangKe01H_HQGC_AfterPrint(object sender, EventArgs e)
        {

        }

        private void BangKe01H_HQGC_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        private void lblSTT1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblSTT1.Text = this.STT + "";
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
        }

        private void lblSoToKhai1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //try
            //{
            //    lblSoToKhai1.Text = GetCurrentColumnValue("SoToKhai").ToString() + "/" + GetCurrentColumnValue("MaLoaiHinh");
            //}
            //catch { }
        }

        private void lblSoToKhai_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                lblSoToKhai.Text = GetCurrentColumnValue("SoToKhai").ToString() + "/" + GetCurrentColumnValue("MaLoaiHinh");
            }
            catch { }
        }
        //private decimal total2 = 0;
        //private void lblTongThueKhongThu_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{

        //    lblTongThueKhongThu.Text = this.TongTienThueHoan1.ToString("N0");
        //}

        //private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        //{
        //    //total1 += Convert.ToDecimal(lblTongTienThueHoan.Text);
        //    total2 += Convert.ToDecimal(lblTongTienThueTKTiep.Text);
        //}

        //private void lblTongThueLanSauTK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    ((XRLabel)sender).Text = total2.ToString("N0"); 
        //}

        //private void lblThueSuat_AfterPrint(object sender, EventArgs e)
        //{
        //    ((XRLabel)sender).Text = ((XRLabel)sender).Text + "%";
        //}

        //private void xrTableCell26_AfterPrint(object sender, EventArgs e)
        //{
        //    ((XRLabel)sender).Text = ((XRLabel)sender).Text + "%";
        //}

        //private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    lblMaHang.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();
        //}

        //private void xrTableCell21_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    xrTableCell21.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();

        //}

        //private void BCThueXNK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    this.STT = 0;
        //    this.TongTienThueHoan1 = 0;
        //}

        //private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    this.STT++;
        //    this.TongLuongSuDung = 0;
        //}

        //private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    lblSTT.Text = this.STT + "";
        //}

        //private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    if (GetCurrentColumnValue("SoToKhaiXuat").ToString() == "0") e.Cancel = true;
        //}

        //private void lblTongTienThueTKTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    this.TongTienThueTKTiep = (decimal)GetCurrentColumnValue("ThueNKNop") - this.TongTienThueHoan;
        //    lblTongTienThueTKTiep.Text = this.TongTienThueTKTiep.ToString("N0");
        //}

        //private void lblTongTienThueHoan_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    this.TongTienThueHoan = Math.Round(this.TongLuongSuDung * (decimal)GetCurrentColumnValue("ThueXNK") / (decimal)GetCurrentColumnValue("Luong"), MidpointRounding.AwayFromZero);
        //    if (this.TongLuongSuDung == (decimal)GetCurrentColumnValue("LuongNhap")) this.TongTienThueHoan = (decimal)GetCurrentColumnValue("ThueNKNop");
        //    lblTongTienThueHoan.Text = this.TongTienThueHoan.ToString("N0");
        //    this.TongTienThueHoan1 += Math.Round(this.TongTienThueHoan, MidpointRounding.AwayFromZero);
        //}

        //private void lblLuongNPLSuDung_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    this.TongLuongSuDung += (decimal)GetCurrentColumnValue("LuongNPLSuDung");
        //}

        //private void lblTongTonCuoi_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    decimal t = (decimal)GetCurrentColumnValue("LuongNhap") - this.TongLuongSuDung;
        //    lblTongTonCuoi.Text = t.ToString("n" + GlobalSettings.SoThapPhan.LuongNPL);
        //}

    }
}
