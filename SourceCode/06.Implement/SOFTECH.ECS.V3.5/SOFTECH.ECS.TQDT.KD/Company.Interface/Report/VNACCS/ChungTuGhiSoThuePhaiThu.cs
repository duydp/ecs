﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class ChungTuGhiSoThuePhaiThu : DevExpress.XtraReports.UI.XtraReport
    {
        public ChungTuGhiSoThuePhaiThu()
        {
            InitializeComponent();
        }
        public void BindReport(VAF8010 vaf801)
        {
            lblTenCoQuanHQ.Text = vaf801.CSM.GetValue().ToString();
            lblTenChiCucHQ.Text = vaf801.CBN.GetValue().ToString();
            lblSoChungTu.Text = vaf801.SPN.GetValue().ToString();
            lblTenDonViXNK.Text = vaf801.IEN.GetValue().ToString();
            lblMaDonViXNK.Text = vaf801.IEC.GetValue().ToString();
            lblMaBuuChinh.Text = vaf801.PCD.GetValue().ToString();
            lblDiaChiNguoiXNK.Text = vaf801.ADB.GetValue().ToString();
            lblSoDienThoai.Text = vaf801.TEL.GetValue().ToString();
            lblSoToKhai.Text = vaf801.ICN.GetValue().ToString();
            if (Convert.ToDateTime(vaf801.DDC.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDKToKhai.Text = Convert.ToDateTime(vaf801.DDC.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDKToKhai.Text = "";
            }
            
            lblMaLoaiHinh.Text = vaf801.DKC.GetValue().ToString();
            lblTenNHBaoLanh.Text = vaf801.SBN.GetValue().ToString();
            lblMaNHBaoLanh.Text = vaf801.SBK.GetValue().ToString();
            lblKyHieuChungTuBaoLanh.Text = vaf801.SBS.GetValue().ToString();
            lblSoChungTuBaoLanh.Text = vaf801.SRN.GetValue().ToString();
            lblLoaiBaoLanh.Text = vaf801.TSN.GetValue().ToString();
            lblTenNHTraThay.Text = vaf801.BNM.GetValue().ToString();
            lblMaNHTraThay.Text = vaf801.BCD.GetValue().ToString();
            lblKyHieuChungTuHanMuc.Text = vaf801.BPS.GetValue().ToString();
            lblSoHieuHanMuc.Text = vaf801.BPN.GetValue().ToString();
            BindReportTax(vaf801);
            lblTongSoTienThue.Text = vaf801.TAM.GetValue().ToString();
            lblTongSoTienMien.Text = vaf801.TA2.GetValue().ToString();
            lblTongSoTienGiam.Text = vaf801.TA3.GetValue().ToString();
            lblTongSoThuePhaiNop.Text = vaf801.TA4.GetValue().ToString();
            lblMaTienTe.Text = vaf801.CCC.GetValue().ToString();
            lblTyGia.Text = vaf801.EXR.GetValue().ToString();
            lblSoNgayAnHan.Text = vaf801.LOP.GetValue().ToString();

            if (Convert.ToDateTime(vaf801.EFD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHetHieuLuc.Text = Convert.ToDateTime(vaf801.EFD.GetValue()).ToString("dd/MM/yyyy"); ;
            }
            else
            {
                lblNgayHetHieuLuc.Text = "";
            }
            lblSoTKKhoBac.Text = vaf801.TAN.GetValue().ToString();
            lblTenKhoBac.Text = vaf801.TNM.GetValue().ToString();
            lblLaiSuatPhatChamNop.Text = vaf801.ARR.GetValue().ToString();
            if (Convert.ToDateTime(vaf801.DOP.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayPhatHanhChungTu.Text = Convert.ToDateTime(vaf801.DOP.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayPhatHanhChungTu.Text = "";
            }
           

        }
        private void BindReportTax(VAF8010 vaf801)
        {

            try
            {
                string[] chuong = { "Chi", "theo", "chương", "của bộ", "chủ", "quản" };
                DataTable dt = new DataTable();
                dt.Columns.Add("SacThue", typeof(string));
                dt.Columns.Add("Chuong", typeof(string));
                dt.Columns.Add("TieuMuc", typeof(string));
                dt.Columns.Add("SoTienThue", typeof(string));
                dt.Columns.Add("SoTienMien", typeof(string));
                dt.Columns.Add("SoTienGiam", typeof(string));
                dt.Columns.Add("SoThuePhaiNop", typeof(string));
                for (int i = 0; i < vaf801.TN1.listAttribute[0].ListValue.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["SacThue"] = vaf801.TN1.listAttribute[0].GetValueCollection(i).ToString();
                    dr["Chuong"] = chuong[i];
                    dr["TieuMuc"] = vaf801.TN1.listAttribute[1].GetValueCollection(i).ToString().Trim();
                    dr["SoTienThue"] = vaf801.TN1.listAttribute[2].GetValueCollection(i).ToString().Trim();
                    dr["SoTienMien"] = vaf801.TN1.listAttribute[3].GetValueCollection(i).ToString().Trim();
                    dr["SoTienGiam"] = vaf801.TN1.listAttribute[4].GetValueCollection(i).ToString().Trim();
                    dr["SoThuePhaiNop"] = vaf801.TN1.listAttribute[5].GetValueCollection(i).ToString().Trim();
                    dt.Rows.Add(dr);
                }

                while (dt.Rows.Count < 6)
                {


                    DataRow dr = dt.NewRow();
                    dr["SacThue"] = "";
                    dr["Chuong"] = chuong[dt.Rows.Count];
                    dr["TieuMuc"] = "";
                    dr["SoTienThue"] = "";
                    dr["SoTienMien"] = "";
                    dr["SoTienGiam"] = "";
                    dr["SoThuePhaiNop"] = "";
                    dt.Rows.Add(dr);
                }
                DetailReport.DataSource = dt;
                lblTenSacThue.DataBindings.Add("Text", DetailReport.DataSource, "SacThue");
                lblChuong.DataBindings.Add("Text", DetailReport.DataSource, "Chuong");
                lblTieuMuc.DataBindings.Add("Text", DetailReport.DataSource, "TieuMuc");
                lblTienThue.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThue");
                lblSoTienMienThue.DataBindings.Add("Text", DetailReport.DataSource, "SoTienMien");
                lblSoTienGiamThue.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiam");
                lblSoThuePhaiNop.DataBindings.Add("Text", DetailReport.DataSource, "SoThuePhaiNop");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }

        }

        private void lblTieuMuc_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTieuMuc.Text == "0")
                lblTieuMuc.Text = "";
            
        }

        private void lblTienThue_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTienThue.Text == "0")
               lblTienThue.Text = "";
            
        }

        private void lblSoTienMienThue_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTienThue.Text == "")
                lblSoTienMienThue.Text = "";
        }

        private void lblSoTienGiamThue_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTienThue.Text == "")
                lblSoTienGiamThue.Text = "";
        }

        private void lblSoThuePhaiNop_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblSoThuePhaiNop.Text == "0")
                lblSoThuePhaiNop.Text = "";
        }

        private void lblTongSoTienThue_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTongSoTienThue.Text == "0")
                lblTongSoTienThue.Text = "";
        }

        private void lblTongSoTienMien_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTongSoTienMien.Text == "0")
                lblTongSoTienMien.Text = "";
        }

        private void lblTongSoTienGiam_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTongSoTienGiam.Text == "0")
                lblTongSoTienGiam.Text = "";
        }

        private void lblTongSoThuePhaiNop_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTongSoThuePhaiNop.Text == "0")
                lblTongSoThuePhaiNop.Text = "";
        }
    }
}
