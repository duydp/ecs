DELETE t_VNACC_Category_Cargo where BondedAreaCode in ('02B1D02','02B1F02','02B1W04','02B4A01','02B4A02','02B4AB4','02B4C01','02B4C02','02B4OZZ','02B4W01','02B4W02','02B1A01','02B1A02','02B1AB1','02B1D01','02B1C01','02B1F01','02B1W01','02B1W02','02B1W03')
go
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1A01')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1A01','KHO TCS')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1A02')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1A02','KHO SCSC')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1AB1')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1AB1','DOI TT HH XNK TSN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1D01')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1D01','KHO CFS SCSC')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1C01')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1C01','MIEN  THUE CASCO')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1F01')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1F01','BAO THUE CATERING')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1W01')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1W01','KNQ TCS')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1W02')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1W02','KNQSCSC')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1W03')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1W03','KNQ WF41')
GO
IF NOT EXISTS(Select * From t_VNACCS_Mapper where LoaiMapper='MaHQ' and CodeV4='C27F01')
insert into t_VNACCS_Mapper values('C27F01',N'Chi cục hải quan cảng nghi sơn', '27F2',N'Đội thủ tục HQ Cảng Nghi Sơn','MaHQ','')

Go
update t_VNACC_Category_CityUNLOCODE set CityNameOrStateName='....' where LOCODE like'%ZZZ%'

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '20.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('20.9',GETDATE(), N' Cap nhat ma luu kho cho thong quan')
END