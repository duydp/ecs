Option Strict Off
Option Explicit On
Module SDX
	' ====================================================================
	
	Declare Function SDX_Find Lib "SDX.dll" () As Integer
	
	' Find SecureDongle X attached to the computer
	' Return value:
	'  <0  Error code
	'  =0  No SecureDongle X attached
	'  >0  The number of attached SecureDongle X(s)
	
	' ====================================================================
	
	Declare Function SDX_Open Lib "SDX.dll" (ByVal mode As Integer, ByVal uid As Integer, ByRef hid As Integer) As Integer
	
	' Open specified SecureDongle X
	' Input:
	' mode -- This parameter indicates the way to open SDX
	' mode = 0,  open the first found SecureDongle X
	' mode > 0,  open the SDX according to the UID. The mode value is the SDX number,
	'            for example: uid=12345678, mode=2, this will open the second SDX with UID 12345678
	' mode = -1, open the SDX according to the HID, and *hid can not be 0
   ' Define a Constant for ease of use:
   Public Const HID_MODE As Short = -1
	
	' uid -- UserID,You need to specify the SDX UID and this UID is generated with SDX_GenUID
	' hid -- Hardware ID,Open SDX with HID of *hid
	' The SDX HID will be returned to *hid regardless of how the SDX was opened.
	' Return value:
	'  >=0 Success. The opened SDX handle is returned.
	'  < 0 Error code. Please refer to latter section
	
	' ====================================================================
	
	Declare Sub SDX_Close Lib "SDX.dll" (ByVal handle As Integer)
	
	' Close specified SecureDongle X
	' Input:
	' handle -- SDX  handle. It is the handle returned from SDX_Open
	' Return value:
	' N/A
	
	' ====================================================================
	
    Declare Function SDX_Read Lib "SDX.dll" (ByVal handle As Integer, ByVal block_index As Short, ByVal buffer512 As String) As Integer
	
	' Read SecureDongle X content
	' Input:
	' handle      -- SDX handle. It is the handle returned from SDX_Open.
	' block_index -- Block index. Specify the block to read. The value range is 0-4.
	' buffer512   -- Read buffer. The buffer must be at least 512 bytes to accommodate the 512 bytes block size.
	' Return value:
	' Error code. Please refer to latter section
	
	' ====================================================================
	
    Declare Function SDX_Write Lib "SDX.dll" (ByVal handle As Integer, ByVal block_index As Short, ByVal buffer512 As String) As Integer
	
	' Write to SecureDongle X
	' Input:
	' handle      -- SDX handle. It is the handle returned from SDX_Open
	' block_index -- Block index. Specify the block to read. The value range is 0-4.
	' buffer512   -- Read buffer. The buffer must be at least 512 bytes to accommodate the 512 bytes block size.
	' Return value:
	' Error code. Please refer to latter section
	
	' ====================================================================
	
    Declare Function SDX_Transform Lib "SDX.dll" (ByVal handle As Integer, ByVal dataLen As Integer, ByVal data As String) As Integer
	
	' Data Hashing function
	' Input:
	' handle -- SDX handle. It is the handle returned from SDX_Open
	' len    -- Length of buffer to transform. Maximum 64 characters
	' buffer -- Content of data that user want to transform.
	' SDX will return the result of transform to *buffer.
	' Return value:
	' Error code. Please refer to latter section
	
	' ====================================================================
	
	Declare Function SDX_GetVersion Lib "SDX.dll" (ByVal handle As Object) As Integer
	
	' Get SecureDongle X hardware version
	' Input:
	' handle -- SDX handle. It is the handle returned from SDX_Open
	' Return value:
	'  > 0 Success. The hardware version is returned.
	'  < 0 Error code. Please refer to latter section
	
	' ====================================================================
	
    Declare Function SDX_RSAEncrypt Lib "SDX.dll" (ByVal handle As Integer, ByVal startIndex As Integer, ByVal bufferData As String, ByRef dataLen As Integer, ByVal key512 As String) As Integer
	
	' Encrypt with RSA and write to SecureDongle X
	' Input:
	' handle     -- SDX handle. It is the handle returned from SDX_Open
	' startIndex -- Start index. Specify the start index to write cipher text into SDX. The value range is 0-2559.
	' bufferData -- Plaintext that will be encrypted.
	' len        -- Length of Data buffer to encrypt. On success, SDX will write the length of cipher text to len.
	' Key512     -- Decryption key. The key must 512 Byte. On success, SDX will write the decryption key to Key512.
	' Return value:
	' Error code. Please refer to latter section
	
	' ====================================================================
	
    Declare Function SDX_RSADecrypt Lib "SDX.dll" (ByVal handle As Integer, ByVal startIndex As Integer, ByVal bufferData As String, ByRef dataLen As Integer, ByVal key512 As String) As Integer
	
	' Decrypt with RSA and write to SecureDongle X
	' Input:
	' handle     -- SDX handle. It is the handle returned from SDX_Open
	' startIndex -- Start index. Specify the start index to read cipher text into SDX. The value range is 0-2559.
	' bufferData -- If success, SDX will write bufferData with plaintext.
	' len        -- Length of cipher text to decrypt. If success SDX will write the length of plaintext to len.
	' Key512     -- Key that is used to decrypt. The key size must 512 Bytes.
	' Return value:
	' Error code. Please refer to latter section
	
	
	' Error codes ========================================================
	
    Public Const SDXERR_SUCCESS As Short = 0                                            ' Success
    Public Const SDXERR_NO_SUCH_DEVICE As Integer = &HA0100001                          ' Specified SDX is not found (parameter error)
    Public Const SDXERR_NOT_OPENED_DEVICE As Integer = &HA0100002                       ' Need to call SDX_Open first to open the SDX, then call this function (operation error)
    Public Const SDXERR_WRONG_UID As Integer = &HA0100003                               ' Wrong UID(parameter error)
    Public Const SDXERR_WRONG_INDEX As Integer = &HA0100004                             ' Block index error (parameter error)
    Public Const SDXERR_TOO_LONG_SEED As Integer = &HA0100005                           ' Seed character string is longer than 64 bytes when calling GenUID (parameter error)
    Public Const SDXERR_WRITE_PROTECT As Integer = &HA0100006                           ' Tried to write to write-protected dongle(operation error)
    Public Const SDXERR_WRONG_START_INDEX As Integer = &HA0100007                       ' Start index wrong (parameter error)
    Public Const SDXERR_INVALID_LEN As Integer = &HA0100008                             ' Invalid length (parameter error)
    Public Const SDXERR_TOO_LONG_ENCRYPTION_DATA As Integer = &HA0100009                ' Chipertext is too long (cryptography error)
    Public Const SDXERR_GENERATE_KEY As Integer = &HA010000A                            ' Generate key error (cryptography error)
    Public Const SDXERR_INVALID_KEY As Integer = &HA010000B                             ' Invalid key (cryptography error)
    Public Const SDXERR_FAILED_ENCRYPTION As Integer = &HA010000C                       ' Failed to encrypt string (cryptography error)
    Public Const SDXERR_FAILED_WRITE_KEY As Integer = &HA010000D                        ' Failed to write key (cryptography error)
    Public Const SDXERR_FAILED_DECRYPTION As Integer = &HA010000E                       ' Failed to decrypt string (Cryptography error)
    Public Const SDXERR_OPEN_DEVICE As Integer = &HA010000F                             ' Open device error (Windows error)
    Public Const SDXERR_READ_REPORT As Integer = &HA0100010                             ' Read record error(Windows error)
    Public Const SDXERR_WRITE_REPORT As Integer = &HA0100011                            ' Write record error(Windows error)
    Public Const SDXERR_SETUP_DI_GET_DEVICE_INTERFACE_DETAIL As Integer = &HA0100012    ' Internal error (Windows error)
    Public Const SDXERR_GET_ATTRIBUTES As Integer = &HA0100013                          ' Internal error (Windows error)
    Public Const SDXERR_GET_PREPARSED_DATA As Integer = &HA0100014                      ' Internal error (Windows error)
    Public Const SDXERR_GETCAPS As Integer = &HA0100015                                 ' Internal error (Windows error)
    Public Const SDXERR_FREE_PREPARSED_DATA As Integer = &HA0100016                     ' Internal error (Windows error)
    Public Const SDXERR_FLUSH_QUEUE As Integer = &HA0100017                             ' Internal error (Windows error)
    Public Const SDXERR_SETUP_DI_CLASS_DEVS As Integer = &HA0100018                     ' Internal error (Windows error)
    Public Const SDXERR_GET_SERIAL As Integer = &HA0100019                              ' Internal error (Windows error)
    Public Const SDXERR_GET_PRODUCT_STRING As Integer = &HA010001A                      ' Internal error (Windows error)
    Public Const SDXERR_TOO_LONG_DEVICE_DETAIL As Integer = &HA010001B                  ' Internal error
    Public Const SDXERR_UNKNOWN_DEVICE As Integer = &HA0100020                          ' Unknown device(hardware error)
    Public Const SDXERR_VERIFY As Integer = &HA0100021                                  ' Verification error(hardware error)
    Public Const SDXERR_UNKNOWN_ERROR As Integer = &HA010FFFF                           ' Unknown error(hardware error)
End Module