using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;

namespace DongBoDuLieu.BLL.GC
{
    public class NPL
    {
        public static DataSet GetDataSetNguyenPhuLieu(Company.GC.BLL.KDT.GC.HopDong HD, string matKhau)
        {
            
            StringBuilder queryNPL = new StringBuilder();
            queryNPL.Append("SELECT ");
            queryNPL.Append("NPL.So_HD			AS SoHopDong, ");
            queryNPL.Append("NPL.Ma_HQHD		AS MaHaiQuan, ");
            queryNPL.Append("NPL.DVGC			AS MaDoanhNghiep, ");
            queryNPL.Append("NPL.Ngay_Ky		AS NgayKy, ");
            queryNPL.Append("NPL.P_Code		AS Ma, ");
            queryNPL.Append("NPL.HS_Code		AS MaHS, ");
            queryNPL.Append("NPL.Ten_NPL		AS Ten, ");
            queryNPL.Append("NPL.Ma_DVT AS		DVT_ID, ");
            queryNPL.Append("NPL.SL_DK AS		SoLuongDangKy ");
            queryNPL.Append("FROM DNPLHD NPL ");
            queryNPL.Append("WHERE NPL.Ma_HQHD = '" + HD.MaHaiQuan + "' AND DVGC = '" + HD.MaDoanhNghiep + "' ");
            queryNPL.Append(" and So_HD='" + HD.SoHopDong + "'");
            DataSet dsNPL = WebServiceDB.getDataBySQL(queryNPL.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep,matKhau);
            return dsNPL;

            //TODO: NguyenPhuLieu
            /*
            StringBuilder queryNPL = new StringBuilder();
            queryNPL.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryNPL.Append("SELECT ");
            queryNPL.Append("NPL.So_HD			AS SoHopDong, ");
            queryNPL.Append("NPL.Ma_HQHD		AS MaHaiQuan, ");
            queryNPL.Append("NPL.DVGC			AS MaDoanhNghiep, ");
            queryNPL.Append("NPL.Ngay_Ky		AS NgayKy, ");
            queryNPL.Append("NPL.P_Code		AS Ma, ");
            queryNPL.Append("NPL.HS_Code		AS MaHS, ");
            queryNPL.Append("NPL.Ten_NPL		AS Ten, ");
            queryNPL.Append("NPL.Ma_DVT AS		DVT_ID, ");
            queryNPL.Append("NPL.SL_DK AS		SoLuongDangKy ");
            queryNPL.Append("FROM DNPLHD NPL ");
            queryNPL.Append("WHERE NPL.Ma_HQHD = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' ");
            queryNPL.Append(" and So_HD=''" + HD.SoHopDong + "''");
            queryNPL.Append(" ') AS a;");
            DataSet dsNPL = WebServiceDB.getDataBySQL(queryNPL.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsNPL;
            */
        }

        public static DataSet GetNguyenPhuLieuAtTMP(long Id_HopDong)
        {
            string sql = "select * from t_GC_NguyenPhuLieu where HopDong_ID=" + Id_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }

        public static DataSet GetNguyenPhuLieuAtDN(long Id_HopDong)
        {
            string sql = "select * from t_GC_NguyenPhuLieu where HopDong_ID=" + Id_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }
    }
}
