using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.GC.BLL.DuLieuChuan;
using Janus.Windows.GridEX;

namespace Company.Interface.GC
{
    public partial class SanPhamRegistedGCForm : BaseForm
    {
        public SanPham SanPhamSelected;
        public bool isBrower = true;
        //2 hien da duyet nhung chua dieu chinh
        public int isDisplayAll = 0;//0 hien tat ca . 1 hien cac npl da duyet nhhung chua dieu chinh va bo sung . 

        public SanPhamRegistedGCForm()
        {
            InitializeComponent();
            SanPhamSelected = new SanPham();
        }

        public void BindData()
        {          
            if (isDisplayAll == 0)
                dgList.DataSource = SanPhamSelected.SelectCollectionBy_HopDong_ID();
            else if (isDisplayAll == 1)
                dgList.DataSource = SanPhamSelected.SelectCollectionDaDuyetVaBoSungBy_HopDong_ID();
            else if (isDisplayAll == 2)
                dgList.DataSource = SanPhamSelected.SelectCollectionDaDuyetBy_HopDong_ID();
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll();
            lblHint.Visible = this.CalledForm != string.Empty;
        }

        //-----------------------------------------------------------------------------------------

        private void SanPhamRegistedForm_Load(object sender, EventArgs e)
        {
            // Sản phẩm đã đăng ký.
            lblHint.Visible = !isBrower;
            BindData();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                e.Row.Cells["NhomSanPham_ID"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSanPham_ID"].Value.ToString());
            }

        }

        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (isBrower)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    this.SanPhamSelected = (SanPham)e.Row.DataRow;
                    this.Close();
                }
            }
            else
            {

            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //-----------------------------------------------------------------------------------------
    }
}