﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.GC.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.GC.BLL.DataTransferObjectMapper;

namespace Company.Interface.GC
{
    public partial class BaoCaoChotTonForm : BaseForm
    {
        public string LoaiHangHoa = "1";
#if SXXK_V4
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#elif GC_V4
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#endif
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public KDT_VNACCS_TotalInventoryReport totalInventoryReport = new KDT_VNACCS_TotalInventoryReport();
        public KDT_VNACCS_TotalInventoryReport_Detail totalInventoryReport_Detail = new KDT_VNACCS_TotalInventoryReport_Detail();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public BaoCaoChotTonForm()
        {
            InitializeComponent();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            txtDonViTinh.DataSource = this._DonViTinh;
            txtDonViTinh.DisplayMember = "Ten";
            txtDonViTinh.ValueMember = "ID";
            txtDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
        }

        private void BaoCaoChotTonForm_Load(object sender, EventArgs e)
        {
            if (totalInventoryReport == null || totalInventoryReport.ID == 0)
            {
                txtMaDN.Text = GlobalSettings.MA_DON_VI;
                txtTenDN.Text = GlobalSettings.TEN_DON_VI;
                txtDiaChi.Text = GlobalSettings.DIA_CHI;
                clcNgayTN.Value = DateTime.Now;
                clcNgayChotTon.Value = DateTime.Now;
                cbbQuy.SelectedValue = "1";
                //ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                cbbType.SelectedIndex = 0;
            }
            else
            {
                txtSoTiepNhan.Text = totalInventoryReport.SoTiepNhan.ToString();
                clcNgayTN.Value = totalInventoryReport.NgayTiepNhan;
                txtMaDN.Text = totalInventoryReport.MaDoanhNghiep;
                txtTenDN.Text = GlobalSettings.TEN_DON_VI;
                txtDiaChi.Text = GlobalSettings.DIA_CHI;
                //ctrCoQuanHaiQuan.Code = totalInventoryReport.MaHaiQuan;
                clcNgayChotTon.Value = totalInventoryReport.NgayChotTon;
                cbbQuy.SelectedValue = totalInventoryReport.LoaiBaoCao.ToString();
                //
                cbbType.SelectedValue = totalInventoryReport.LoaiBaoCao.ToString();
                //
                txtGiaiTrinh.Text = totalInventoryReport.GhiChuKhac;
                totalInventoryReport.DetailCollection = KDT_VNACCS_TotalInventoryReport_Detail.SelectCollectionBy_TotalInventory_ID(totalInventoryReport.ID);
                BindData();
            }
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdSend":
                    this.SendV5(false);
                    break;
                case "cmdCancel":
                    this.SendV5(true);
                    break;
                case "cmdFeedback":
                    this.FeedBackV5();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
            }
        }
        private void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_TotalInventoryReport", "", Convert.ToInt32(totalInventoryReport.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = this.totalInventoryReport.ID;
                form.DeclarationIssuer = DeclarationIssuer.TotalInventoryReport_GC;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = totalInventoryReport.GuidString;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.TotalInventoryReport_GC,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.TotalInventoryReport_GC,
                };
                subjectBase.Type = DeclarationIssuer.TotalInventoryReport_GC;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = totalInventoryReport.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(totalInventoryReport.MaHaiQuan.Trim())),
                                                  Identity = totalInventoryReport.MaHaiQuan
                                              }, subjectBase, null);
                if (totalInventoryReport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(totalInventoryReport.MaHaiQuan));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }
        }

        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.TotalInventoryReportSendHandler(totalInventoryReport, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void SendV5(bool IsCancel)
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (totalInventoryReport.ID == 0)
                {
                    ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    totalInventoryReport.DetailCollection = KDT_VNACCS_TotalInventoryReport_Detail.SelectCollectionBy_TotalInventory_ID(totalInventoryReport.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ScrapInformation;
                sendXML.master_id = totalInventoryReport.ID;

                if (sendXML.Load())
                {
                    if (!IsCancel)
                    {
                        ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    totalInventoryReport.GuidString = Guid.NewGuid().ToString();
                    GoodsItems_VNACCS totalInventoryReport_VNACCS = new GoodsItems_VNACCS();
                    totalInventoryReport_VNACCS = Mapper_V4.ToDataTransferTotalInventoryReport(totalInventoryReport, totalInventoryReport_Detail, GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = totalInventoryReport.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(totalInventoryReport.MaHaiQuan)),
                              Identity = totalInventoryReport.MaHaiQuan
                          },
                          new SubjectBase()
                          {

                              Type = totalInventoryReport_VNACCS.Issuer,
                              //Function = DeclarationFunction.SUA,
                              Reference = totalInventoryReport.GuidString,
                          },
                          totalInventoryReport_VNACCS
                        );
                    if (!IsCancel)
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(totalInventoryReport.ID, MessageTitle.RegisterScrapInformation);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.ScrapInformation;
                        sendXML.master_id = totalInventoryReport.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        totalInventoryReport.Update();
                        FeedBackV5();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMaNPL, errorProvider, "Mã hàng hóa ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPL, errorProvider, "Tên hàng hóa ", isOnlyWarning);
                //ctrDVT.SetValidate = !isOnlyWarning; ctrDVT.IsOnlyWarning = isOnlyWarning;
                //isValid &= ctrDVT.IsValidate;
                isValid &= ValidateControl.ValidateNull(txtLuongSoSach, errorProvider, "Số lượng sổ sách", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtLuongThucTe, errorProvider, "Số lượng thực tế", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(cbbLoaiHangHoa, errorProvider, "Loại hàng hóa", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void BindData()
        {
            try
            {
                dgList2.Refetch();
                dgList2.DataSource = totalInventoryReport.DetailCollection;
                dgList2.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Save()
        {
            try
            {
                totalInventoryReport.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text);
                totalInventoryReport.NgayTiepNhan = clcNgayTN.Value;
                totalInventoryReport.MaHaiQuan = GlobalSettings.MA_HAI_QUAN_VNACCS;//ctrCoQuanHaiQuan.Code;
                totalInventoryReport.MaDoanhNghiep = txtMaDN.Text;
                totalInventoryReport.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                totalInventoryReport.NgayChotTon = clcNgayChotTon.Value;
                totalInventoryReport.LoaiBaoCao = Convert.ToDecimal(cbbQuy.SelectedValue.ToString());
                totalInventoryReport.GhiChuKhac = txtGiaiTrinh.Text;
                totalInventoryReport.InsertUpdateFull();
                ShowMessage("Lưu thành công ", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (totalInventoryReport == null)
                {
                    totalInventoryReport = new KDT_VNACCS_TotalInventoryReport();
                }
                totalInventoryReport_Detail.TenHangHoa = txtTenNPL.Text;
                totalInventoryReport_Detail.MaHangHoa = txtMaNPL.Text;
                totalInventoryReport_Detail.DVT = txtDonViTinh.Name;
                totalInventoryReport_Detail.SoLuongTonKhoSoSach = Convert.ToDecimal(txtLuongSoSach.Text);
                totalInventoryReport_Detail.SoLuongTonKhoThucTe = Convert.ToDecimal(txtLuongThucTe.Text);
                totalInventoryReport.DetailCollection.Add(totalInventoryReport_Detail);
                BindData();
                totalInventoryReport_Detail = new KDT_VNACCS_TotalInventoryReport_Detail();
                txtTenNPL.Text = String.Empty;
                txtMaNPL.Text = String.Empty;
                txtLuongThucTe.Text = String.Empty;
                txtLuongSoSach.Text = String.Empty;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {

                GridEXSelectedItemCollection items = dgList2.SelectedItems;
                List<KDT_VNACCS_TotalInventoryReport_Detail> ItemColl = new List<KDT_VNACCS_TotalInventoryReport_Detail>();
                if (dgList2.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_TotalInventoryReport_Detail)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_TotalInventoryReport_Detail item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        totalInventoryReport.DetailCollection.Remove(item);
                    }
                    dgList2.Refetch();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            this.LoaiHangHoa = cbbLoaiHang.SelectedValue.ToString();
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;//VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
#if SXXK_V4
            if (this.LoaiHangHoa == "T" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            {

                if (this.NPLRegistedForm_CX == null)
                    this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.ShowDialog(this);
                if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    txtMaHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    txtTenHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                    txtDonViTinh.Name = DonViTinh_GetName(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID);
                    //ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                   
                }

            }
            else if (this.LoaiHangHoa == "N")
            {
                if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; 
                this.NPLRegistedForm.ShowDialog(this);
                if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                {
                    txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                    txtTenHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                    txtDonViTinh.Name = DonViTinh_GetName(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID);

                }
            }
            else 
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                    txtTenHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                    txtDonViTinh.Name = DonViTinh_GetName(this.SPRegistedForm.SanPhamSelected.DVT_ID);
                }
            }
#elif GC_V4
            try
            {
                switch (this.LoaiHangHoa)
                {
                    case "1":
                        this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                        this.NPLRegistedForm.isBrower = true;
                        this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        if (TKMD != null)
                        {
                            this.NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = TKMD.HopDong_ID;
                        }
                        else
                        {
                            this.NPLRegistedForm.isDisplayAll = 3;
                        }
                        this.NPLRegistedForm.ShowDialog();
                        if (!string.IsNullOrEmpty(this.NPLRegistedForm.NguyenPhuLieuSelected.Ma))
                        {
                            txtMaNPL.Text =     this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                            txtTenNPL.Text =    this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                            txtDonViTinh.Name = DonViTinh_GetName(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID);
                            //cbbDVT.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                        }
                        break;
                    case "2":
                        this.SPRegistedForm = new SanPhamRegistedForm();
                        this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        this.SPRegistedForm.SanPhamSelected.HopDong_ID = TKMD.HopDong_ID;
                        this.SPRegistedForm.ShowDialog();
                        if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                        {
                            txtMaNPL.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                            txtTenNPL.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                            txtDonViTinh.Name = DonViTinh_GetName(this.SPRegistedForm.SanPhamSelected.DVT_ID);
                            //cbbDVT.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                        }
                        break;

                    case "3":
                        ThietBiRegistedForm ftb = new ThietBiRegistedForm();
                        ftb.ThietBiSelected.HopDong_ID = TKMD.HopDong_ID;
                        ftb.isBrower = true;
                        ftb.ShowDialog();
                        if (!string.IsNullOrEmpty(ftb.ThietBiSelected.Ma))
                        {
                            txtMaNPL.Text = ftb.ThietBiSelected.Ma;
                            txtTenNPL.Text = ftb.ThietBiSelected.Ten;
                            txtDonViTinh.Name = DonViTinh_GetName(ftb.ThietBiSelected.DVT_ID);
                            //cbbDVT.Code = this.DVT_VNACC(ftb.ThietBiSelected.DVT_ID.PadRight(3));
                        }
                        break;
                    case "4":
                        HangMauRegistedForm hangmauForm = new HangMauRegistedForm();
                        hangmauForm.isBrower = true;
                        hangmauForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        hangmauForm.HangMauSelected.HopDong_ID = TKMD.HopDong_ID;
                        hangmauForm.ShowDialog();
                        if (!string.IsNullOrEmpty(hangmauForm.HangMauSelected.Ma))
                        {
                            txtMaNPL.Text = hangmauForm.HangMauSelected.Ma;
                            txtTenNPL.Text = hangmauForm.HangMauSelected.Ten;
                            txtDonViTinh.Text = DonViTinh_GetName(hangmauForm.HangMauSelected.DVT_ID);
                            //cbbDVT.Code = this.DVT_VNACC(hangmauForm.HangMauSelected.DVT_ID.PadRight(3));
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xử lý thông tin . \n Lỗi tại : " + ex, false);
            }

#endif            
        }

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {

        }

        private void txtDonViTinh_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtMaNPL_TextChanged(object sender, EventArgs e)
        {

        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
