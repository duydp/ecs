﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.KDT.SHARE.VNACCS.LogMessages;

namespace Company.Interface
{
    public partial class VNACC_GiayPhepForm_SAA : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_GiayPhep_SAA GiayPhep = new KDT_VNACC_GiayPhep_SAA();

        public VNACC_GiayPhepForm_SAA()
        {
            InitializeComponent();

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_OGAProcedure.SAA.ToString());
        }

        private void VNACC_GiayPhepForm_Load(object sender, EventArgs e)
        {
            dtNgayKiemDich.Value = dtThoiGianKiemDich.Value = DateTime.Now;
            dtNgayGiamSat.Value = dtThoiGianGiamSat.Value = DateTime.Now;

            SetIDControl();

            SetMaxLengthControl();

            if (GiayPhep.ID != 0)
            {
                SetGiayPhep();
            }

            ValidateForm(true);

            SetAutoRemoveUnicodeAndUpperCaseControl();
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdTinhTrangThamTra":
                    this.ShowTinhTrangThamTraKiemTra();
                    break;
                case "cmdCangTrungGian":
                    this.ShowThemCangTrungGian();
                    break;
                case "cmdThemHang":
                    this.ShowThemHang();
                    break;
                case "cmdLuu":
                    this.SaveGiayPhep();
                    break;
                case "cmdChiThiHaiQuan":
                    ShowChiThiHaiQuan();
                    break;

                case "cmdKhaiBao":
                    SendVnaccsIDC(false);
                    break;
                case "cmdKetQuaHQ":
                    ShowKetQuaTraVe();
                    break;
            }
        }
        private void ShowKetQuaTraVe()
        {
            SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
            f.isSend = false;
            f.isRep = true;
            f.inputMSGID = GiayPhep.InputMessageID;
            f.ShowDialog(this);
        }

        private void ShowTinhTrangThamTraKiemTra()
        {
            VNACC_GiayPhep_SAA_ThamTraForm f = new VNACC_GiayPhep_SAA_ThamTraForm();
            f.GiayPhep_SAA = GiayPhep;
            f.ShowDialog();
        }

        private void ShowChiThiHaiQuan()
        {
            VNACC_ChiThiHaiQuanForm f = new VNACC_ChiThiHaiQuanForm();
            f.Master_ID = GiayPhep.ID;
            f.LoaiThongTin = ELoaiThongTin.GP_SAA;
            f.ShowDialog();
        }

        private void ShowThemCangTrungGian()
        {
            VNACC_GiayPhep_SAA_CangTrungGianForm f = new VNACC_GiayPhep_SAA_CangTrungGianForm();
            f.GiayPhep_SAA = GiayPhep;
            f.ShowDialog();
        }

        private void ShowThemHang()
        {
            VNACC_GiayPhep_SAA_HangForm f = new VNACC_GiayPhep_SAA_HangForm();
            f.GiayPhep_SAA = GiayPhep;
            f.ShowDialog();
        }

        private void SaveGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!ValidateForm(false))
                    return;

                GetGiayPhep();

                GiayPhep.InsertUpdateFull();

                GiayPhep.InsertUpdateFull_CangTrungGian();

                GiayPhep.InsertUpdateFull_TinhTrangThamTra();

                Helper.Controls.MessageBoxControlV.ShowMessage(Company.KDT.SHARE.Components.ThongBao.APPLICATION_SAVE_DATA_SUCCESS_0Param, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GiayPhep.MaNguoiKhai = txtMaNguoiKhai.Text; //Mã người khai
                GiayPhep.MaBuuChinhNguoiKhai = txtMaBuuChinhNguoiKhai.Text; //Mã bưu chính của người khai
                GiayPhep.DiaChiNguoiKhai = txtDiaChiNguoiKhai.Text; //Địa chỉ của người khai
                GiayPhep.MaNuocNguoiKhai = ucMaNuocNguoiKhai.Code; //Mã nước của người khai
                GiayPhep.SoDienThoaiNguoiKhai = txtSoDienThoaiNguoiKhai.Text; //Số điện thoại của người khai
                GiayPhep.SoFaxNguoiKhai = txtSoFaxNguoiKhai.Text; //Số fax của người khai
                GiayPhep.EmailNguoiKhai = txtEmailNguoiKhai.Text; //Email của người khai
                //GiayPhep.SoDonXinCapPhep = txtSoDonXinCapPhep.Text != "" ? Convert.ToInt32(txtSoDonXinCapPhep.Text) : 0; //Số đơn xin cấp phép
                GiayPhep.ChucNangChungTu = Convert.ToInt32(ucChucNangChungTu.Code); //Chức năng của chứng từ
                GiayPhep.LoaiGiayPhep = ucLoaiGiayPhep.Code; //Loại giấy phép
                GiayPhep.MaDonViCapPhep = ucMaDonViCapPhep.Code; //Mã đơn vị cấp phép
                GiayPhep.SoHopDong = txtSoHopDong.Text; //Số hợp đồng
                GiayPhep.TenThuongNhanXuatKhau = txtTenThuongNhanXuatKhau.Text; //Tổ chức/cá nhân xuất khẩu
                GiayPhep.MaBuuChinhXuatKhau = txtMaBuuChinhXuatKhau.Text; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Mã bưu chính)
                GiayPhep.SoNhaTenDuongXuatKhau = txtSoNhaTenDuongXuatKhau.Text; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Số nhà, tên đường)
                GiayPhep.PhuongXaXuatKhau = txtPhuongXaXuatKhau.Text; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Phường, xã)
                GiayPhep.QuanHuyenXuatKhau = txtQuanHuyenXuatKhau.Text; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Quận, huyện, thị xã)
                GiayPhep.TinhThanhPhoXuatKhau = txtTinhThanhPhoXuatKhau.Text; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Tỉnh, thành phố)
                GiayPhep.MaQuocGiaXuatKhau = ucMaQuocGiaXuatKhau.Code; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Mã quốc gia)
                GiayPhep.SoDienThoaiXuatKhau = txtSoDienThoaiXuatKhau.Text; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Số điện thoại)
                GiayPhep.SoFaxXuatKhau = txtSoFaxXuatKhau.Text; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Số fax)
                GiayPhep.EmailXuatKhau = txtEmailXuatKhau.Text; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Email)
                GiayPhep.NuocXuatKhau = ucNuocXuatKhau.Code; //Nước xuất khẩu
                GiayPhep.MaThuongNhanNhapKhau = txtMaThuongNhanNhapKhau.Text; //Mã tổ chức/cá nhân nhập khẩu
                GiayPhep.TenThuongNhanNhapKhau = txtTenThuongNhanNhapKhau.Text; //Tên tổ chức/cá nhân nhập khẩu
                GiayPhep.SoDangKyKinhDoanh = txtSoDangKyKinhDoanh.Text; //Số đăng ký kinh doanh
                GiayPhep.MaBuuChinhNhapKhau = txtMaBuuChinhNhapKhau.Text; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Mã bưu chính)
                GiayPhep.DiaChiThuongNhanNhapKhau = txtDiaChiThuongNhanNhapKhau.Text; //Địa chỉ tổ chức/cá nhân nhập khẩu
                GiayPhep.MaQuocGiaNhapKhau = ucMaQuocGiaNhapKhau.Code; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Mã quốc gia)
                GiayPhep.SoDienThoaiNhapKhau = txtSoDienThoaiNhapKhau.Text; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Số điện thoại)
                GiayPhep.SoFaxNhapKhau = txtSoFaxNhapKhau.Text; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Số fax)
                GiayPhep.EmailNhapKhau = txtEmailNhapKhau.Text; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Email)
                GiayPhep.NuocQuaCanh = ucNuocQuaCanh.Code; //Nước quá cảnh
                GiayPhep.PhuongTienVanChuyen = ucPhuongTienVanChuyen.Code; //Phương tiện vận chuyển
                GiayPhep.MaCuaKhauNhap = ucCuaKhauNhap.Code; //Mã cửa khẩu nhập
                GiayPhep.TenCuaKhauNhap = ucCuaKhauNhap.Name_VN; //Cửa khẩu nhập
                GiayPhep.SoGiayChungNhan = txtSoGiayChungNhan.Text; //Số giấy chứng nhận
                GiayPhep.DiaDiemKiemDich = txtDiaDiemKiemDich.Text; //Địa điểm kiểm dịch
                GiayPhep.BenhMienDich = txtBenhMienDich.Text; //Bệnh miễn dịch
                GiayPhep.NgayTiemPhong = dtNgayTiemPhong.Value; //Ngày tiêm phòng
                GiayPhep.KhuTrung = txtKhuTrung.Text; //Khử trùng
                GiayPhep.NongDo = txtNongDo.Text; //Nồng độ
                GiayPhep.DiaDiemNuoiTrong = txtDiaDiemNuoiTrong.Text; //Địa điểm nuôi trồng
                GiayPhep.NgayKiemDich = dtNgayKiemDich.Value; //Ngày kiểm dịch

                DateTime thoiGianKiemDich = new DateTime(dtNgayKiemDich.Value.Year, dtNgayKiemDich.Value.Month, dtNgayKiemDich.Value.Day, dtThoiGianKiemDich.Value.Hour, dtThoiGianKiemDich.Value.Minute, dtThoiGianKiemDich.Value.Second, System.Globalization.Calendar.CurrentEra);
                GiayPhep.ThoiGianKiemDich = thoiGianKiemDich; //Thời gian kiểm dịch
                GiayPhep.DiaDiemGiamSat = txtDiaDiemGiamSat.Text; //Địa điểm giám sát
                GiayPhep.NgayGiamSat = dtNgayGiamSat.Value; //Ngày giám sát

                DateTime thoiGianGiamSat = new DateTime(dtNgayGiamSat.Value.Year, dtNgayGiamSat.Value.Month, dtNgayGiamSat.Value.Day, dtThoiGianGiamSat.Value.Hour, dtThoiGianGiamSat.Value.Minute, dtThoiGianGiamSat.Value.Second, System.Globalization.Calendar.CurrentEra);
                GiayPhep.ThoiGianGiamSat = thoiGianGiamSat; //Thời gian giám sát
                GiayPhep.SoBanCanCap = Convert.ToInt32(txtSoBanCanCap.Value); //Số bản cần cấp
                GiayPhep.VatDungKhac = txtVatDungKhac.Text; //Vật dụng khác
                GiayPhep.HoSoLienQuan = txtHoSoLienQuan.Text; //Hồ sơ liên quan
                GiayPhep.NoiChuyenDen = txtNoiChuyenDen.Text; //Nơi chuyển đến 
                GiayPhep.NguoiKiemTra = txtNguoiKiemTra.Text; //Người kiểm tra
                GiayPhep.KetQuaKiemTra = txtKetQuaKiemTra.Text; //Kết quả kiểm tra
                GiayPhep.TenTau = txtTenTau.Text; //Tên tàu
                GiayPhep.QuocTich = ucQuocTich.Code; //Quốc tịch
                GiayPhep.TenThuyenTruong = txtTenThuyenTruong.Text; //Tên thuyền trưởng
                GiayPhep.TenBacSi = txtTenBacSi.Text; //Tên bác sĩ
                GiayPhep.SoThuyenVien = Convert.ToInt32(txtSoThuyenVien.Value); //Số thuyền viên
                GiayPhep.SoHanhKhach = Convert.ToInt32(txtSoHanhKhach.Value); //Số hành khách
                GiayPhep.CangRoiCuoiCung = txtCangRoiCuoiCung.Text; //Cảng rời cuối cùng
                GiayPhep.CangDenTiepTheo = txtCangDenTiepTheo.Text; //Cảng đến tiếp theo
                GiayPhep.CangBocHangDauTien = txtCangBocHangDauTien.Text; //Cảng bốc hàng đầu tiên
                GiayPhep.NgayRoiCang = dtNgayRoiCanh.Value; //Ngày rời cảnh
                GiayPhep.TenHangCangDauTien = txtTenHangCangDauTien.Text; //Tên hàng cảng đầu tiên
                GiayPhep.SoLuongHangCangDauTien = Convert.ToInt32(txtSoLuongHangCangDauTien.Value); //Số lượng hàng cảng đầu tiên
                GiayPhep.DonViTinhSoLuongHangCangDauTien = ucDonViTinhSoLuongHangCangDauTien.Code; //Đơn vị tính số lượng
                GiayPhep.KhoiLuongHangCangDauTien = Convert.ToDecimal(txtKhoiLuongHangCangDauTien.Value); //Khối lượng hàng cảng đầu tiên
                GiayPhep.DonViTinhKhoiLuongHangCangDauTien = ucDonViTinhKhoiLuongHangCangDauTien.Code; //Đơn vị tính khối lượng
                GiayPhep.TenHangCanBoc = txtTenHangCanBoc.Text; //Tên hàng cần bốc
                GiayPhep.SoLuongHangCanBoc = Convert.ToInt32(txtSoLuongHangCanBoc.Value); //Số lượng hàng cần bốc
                GiayPhep.DonViTinhSoLuongHangCanBoc = ucDonViTinhSoLuongHangCanBoc.Code; //Đơn vị tính số lượng
                GiayPhep.KhoiLuongHangCanBoc = Convert.ToDecimal(txtKhoiLuongHangCanBoc.Value); //Khối lượng hàng cần bốc
                GiayPhep.DonViTinhKhoiLuongHangCanBoc = ucDonViTinhKhoiLuongHangCanBoc.Code; //Đơn vị tính khối lượng

                errorProvider.Clear();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
                throw ex;
            }
            finally { Cursor = Cursors.Default; }
        }

        private void SetGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                errorProvider.Clear();

                txtMaNguoiKhai.Text = GiayPhep.MaNguoiKhai; //Mã người khai
                txtMaBuuChinhNguoiKhai.Text = GiayPhep.MaBuuChinhNguoiKhai; //Mã bưu chính của người khai
                txtDiaChiNguoiKhai.Text = GiayPhep.DiaChiNguoiKhai; //Địa chỉ của người khai
                ucMaNuocNguoiKhai.Code = GiayPhep.MaNuocNguoiKhai; //Mã nước của người khai
                txtSoDienThoaiNguoiKhai.Text = GiayPhep.SoDienThoaiNguoiKhai; //Số điện thoại của người khai
                txtSoFaxNguoiKhai.Text = GiayPhep.SoFaxNguoiKhai; //Số fax của người khai
                txtEmailNguoiKhai.Text = GiayPhep.EmailNguoiKhai; //Email của người khai
                txtSoDonXinCapPhep.Text = GiayPhep.SoDonXinCapPhep.ToString(); //Số đơn xin cấp phép
                ucChucNangChungTu.Code = GiayPhep.ChucNangChungTu.ToString(); //Chức năng của chứng từ
                ucLoaiGiayPhep.Code = GiayPhep.LoaiGiayPhep; //Loại giấy phép
                ucMaDonViCapPhep.Code = GiayPhep.MaDonViCapPhep; //Mã đơn vị cấp phép
                txtSoHopDong.Text = GiayPhep.SoHopDong; //Số hợp đồng
                txtTenThuongNhanXuatKhau.Text = GiayPhep.TenThuongNhanXuatKhau; //Tổ chức/cá nhân xuất khẩu
                txtMaBuuChinhXuatKhau.Text = GiayPhep.MaBuuChinhXuatKhau; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Mã bưu chính)
                txtSoNhaTenDuongXuatKhau.Text = GiayPhep.SoNhaTenDuongXuatKhau; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Số nhà, tên đường)
                txtPhuongXaXuatKhau.Text = GiayPhep.PhuongXaXuatKhau; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Phường, xã)
                txtQuanHuyenXuatKhau.Text = GiayPhep.QuanHuyenXuatKhau; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Quận, huyện, thị xã)
                txtTinhThanhPhoXuatKhau.Text = GiayPhep.TinhThanhPhoXuatKhau; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Tỉnh, thành phố)
                ucMaQuocGiaXuatKhau.Code = GiayPhep.MaQuocGiaXuatKhau; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Mã quốc gia)
                txtSoDienThoaiXuatKhau.Text = GiayPhep.SoDienThoaiXuatKhau; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Số điện thoại)
                txtSoFaxXuatKhau.Text = GiayPhep.SoFaxXuatKhau; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Số fax)
                txtEmailXuatKhau.Text = GiayPhep.EmailXuatKhau; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Email)
                ucNuocXuatKhau.Code = GiayPhep.NuocXuatKhau; //Nước xuất khẩu
                txtMaThuongNhanNhapKhau.Text = GiayPhep.MaThuongNhanNhapKhau; //Mã tổ chức/cá nhân nhập khẩu
                txtTenThuongNhanNhapKhau.Text = GiayPhep.TenThuongNhanNhapKhau; //Tên tổ chức/cá nhân nhập khẩu
                txtSoDangKyKinhDoanh.Text = GiayPhep.SoDangKyKinhDoanh; //Số đăng ký kinh doanh
                txtMaBuuChinhNhapKhau.Text = GiayPhep.MaBuuChinhNhapKhau; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Mã bưu chính)
                txtDiaChiThuongNhanNhapKhau.Text = GiayPhep.DiaChiThuongNhanNhapKhau; //Địa chỉ tổ chức/cá nhân nhập khẩu
                ucMaQuocGiaNhapKhau.Code = GiayPhep.MaQuocGiaNhapKhau; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Mã quốc gia)
                txtSoDienThoaiNhapKhau.Text = GiayPhep.SoDienThoaiNhapKhau; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Số điện thoại)
                txtSoFaxNhapKhau.Text = GiayPhep.SoFaxNhapKhau; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Số fax)
                txtEmailNhapKhau.Text = GiayPhep.EmailNhapKhau; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Email)
                ucNuocQuaCanh.Code = GiayPhep.NuocQuaCanh; //Nước quá cảnh
                ucPhuongTienVanChuyen.Code = GiayPhep.PhuongTienVanChuyen; //Phương tiện vận chuyển
                ucCuaKhauNhap.Code = GiayPhep.MaCuaKhauNhap; //Mã cửa khẩu nhập
                ucCuaKhauNhap.Name_VN = GiayPhep.TenCuaKhauNhap; //Cửa khẩu nhập
                txtSoGiayChungNhan.Text = GiayPhep.SoGiayChungNhan; //Số giấy chứng nhận
                txtDiaDiemKiemDich.Text = GiayPhep.DiaDiemKiemDich; //Địa điểm kiểm dịch
                txtBenhMienDich.Text = GiayPhep.BenhMienDich; //Bệnh miễn dịch
                dtNgayTiemPhong.Value = GiayPhep.NgayTiemPhong; //Ngày tiêm phòng
                txtKhuTrung.Text = GiayPhep.KhuTrung; //Khử trùng
                txtNongDo.Text = GiayPhep.NongDo; //Nồng độ
                txtDiaDiemNuoiTrong.Text = GiayPhep.DiaDiemNuoiTrong; //Địa điểm nuôi trồng
                dtNgayKiemDich.Value = GiayPhep.NgayKiemDich; //Ngày kiểm dịch
                dtThoiGianKiemDich.Value = GiayPhep.ThoiGianKiemDich; //Thời gian kiểm dịch
                txtDiaDiemGiamSat.Text = GiayPhep.DiaDiemGiamSat; //Địa điểm giám sát
                dtNgayGiamSat.Value = GiayPhep.NgayGiamSat; //Ngày giám sát
                dtThoiGianGiamSat.Value = GiayPhep.ThoiGianGiamSat; //Thời gian giám sát
                txtSoBanCanCap.Value = GiayPhep.SoBanCanCap; //Số bản cần cấp
                txtVatDungKhac.Text = GiayPhep.VatDungKhac; //Vật dụng khác
                txtHoSoLienQuan.Text = GiayPhep.HoSoLienQuan; //Hồ sơ liên quan
                txtNoiChuyenDen.Text = GiayPhep.NoiChuyenDen; //Nơi chuyển đến 
                txtNguoiKiemTra.Text = GiayPhep.NguoiKiemTra; //Người kiểm tra
                txtKetQuaKiemTra.Text = GiayPhep.KetQuaKiemTra; //Kết quả kiểm tra
                txtTenTau.Text = GiayPhep.TenTau; //Tên tàu
                ucQuocTich.Code = GiayPhep.QuocTich; //Quốc tịch
                txtTenThuyenTruong.Text = GiayPhep.TenThuyenTruong; //Tên thuyền trưởng
                txtTenBacSi.Text = GiayPhep.TenBacSi; //Tên bác sĩ
                txtSoThuyenVien.Value = GiayPhep.SoThuyenVien; //Số thuyền viên
                txtSoHanhKhach.Value = GiayPhep.SoHanhKhach; //Số hành khách
                txtCangRoiCuoiCung.Text = GiayPhep.CangRoiCuoiCung; //Cảng rời cuối cùng
                txtCangDenTiepTheo.Text = GiayPhep.CangDenTiepTheo; //Cảng đến tiếp theo
                txtCangBocHangDauTien.Text = GiayPhep.CangBocHangDauTien; //Cảng bốc hàng đầu tiên
                dtNgayRoiCanh.Value = GiayPhep.NgayRoiCang; //Ngày rời cảnh
                txtTenHangCangDauTien.Text = GiayPhep.TenHangCangDauTien; //Tên hàng cảng đầu tiên
                txtSoLuongHangCangDauTien.Value = GiayPhep.SoLuongHangCangDauTien; //Số lượng hàng cảng đầu tiên
                ucDonViTinhSoLuongHangCangDauTien.Code = GiayPhep.DonViTinhSoLuongHangCangDauTien; //Đơn vị tính số lượng
                txtKhoiLuongHangCangDauTien.Value = GiayPhep.KhoiLuongHangCangDauTien; //Khối lượng hàng cảng đầu tiên
                ucDonViTinhKhoiLuongHangCangDauTien.Code = GiayPhep.DonViTinhKhoiLuongHangCangDauTien; //Đơn vị tính khối lượng
                txtTenHangCanBoc.Text = GiayPhep.TenHangCanBoc; //Tên hàng cần bốc
                txtSoLuongHangCanBoc.Value = GiayPhep.SoLuongHangCanBoc; //Số lượng hàng cần bốc
                ucDonViTinhSoLuongHangCanBoc.Code = GiayPhep.DonViTinhSoLuongHangCanBoc; //Đơn vị tính số lượng
                txtKhoiLuongHangCanBoc.Value = GiayPhep.KhoiLuongHangCanBoc; //Khối lượng hàng cần bốc
                ucDonViTinhKhoiLuongHangCanBoc.Code = GiayPhep.DonViTinhKhoiLuongHangCanBoc; //Đơn vị tính khối lượng
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNguoiKhai.Tag = "SMC"; //Mã người khai
                txtMaBuuChinhNguoiKhai.Tag = "SPC"; //Mã bưu chính của người khai
                txtDiaChiNguoiKhai.Tag = "ADS"; //Địa chỉ của người khai
                ucMaNuocNguoiKhai.TagName = "SCC"; //Mã nước của người khai
                txtSoDienThoaiNguoiKhai.Tag = "SPN"; //Số điện thoại của người khai
                txtSoFaxNguoiKhai.Tag = "SFN"; //Số fax của người khai
                txtEmailNguoiKhai.Tag = "SBE"; //Email của người khai
                txtSoDonXinCapPhep.Tag = "APN"; //Số đơn xin cấp phép
                ucChucNangChungTu.TagName = "FNC"; //Chức năng của chứng từ
                ucLoaiGiayPhep.TagName = "APT"; //Loại giấy phép
                ucMaDonViCapPhep.TagName = "APP"; //Mã đơn vị cấp phép
                txtSoHopDong.Tag = "CON"; //Số hợp đồng
                txtTenThuongNhanXuatKhau.Tag = "EXN"; //Tổ chức/cá nhân xuất khẩu
                txtMaBuuChinhXuatKhau.Tag = "EPC"; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Mã bưu chính)
                txtSoNhaTenDuongXuatKhau.Tag = "EXA"; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Số nhà, tên đường)
                txtPhuongXaXuatKhau.Tag = "EXS"; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Phường, xã)
                txtQuanHuyenXuatKhau.Tag = "EXD"; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Quận, huyện, thị xã)
                txtTinhThanhPhoXuatKhau.Tag = "EXC"; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Tỉnh, thành phố)
                ucMaQuocGiaXuatKhau.TagName = "ECC"; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Mã quốc gia)
                txtSoDienThoaiXuatKhau.Tag = "EPN"; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Số điện thoại)
                txtSoFaxXuatKhau.Tag = "EXF"; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Số fax)
                txtEmailXuatKhau.Tag = "EXE"; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Email)
                ucNuocXuatKhau.TagCode = "EOC"; //Nước xuất khẩu
                txtMaThuongNhanNhapKhau.Tag = "IMC"; //Mã tổ chức/cá nhân nhập khẩu
                txtTenThuongNhanNhapKhau.Tag = "IMN"; //Tên tổ chức/cá nhân nhập khẩu
                txtSoDangKyKinhDoanh.Tag = "RCN"; //Số đăng ký kinh doanh
                txtMaBuuChinhNhapKhau.Tag = "BPC"; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Mã bưu chính)
                txtDiaChiThuongNhanNhapKhau.Tag = "IMA"; //Địa chỉ tổ chức/cá nhân nhập khẩu
                ucMaQuocGiaNhapKhau.TagName = "BCC"; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Mã quốc gia)
                txtSoDienThoaiNhapKhau.Tag = "BPN"; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Số điện thoại)
                txtSoFaxNhapKhau.Tag = "PFX"; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Số fax)
                txtEmailNhapKhau.Tag = "BEM"; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Email)
                ucNuocQuaCanh.TagCode = "TCC"; //Nước quá cảnh
                ucPhuongTienVanChuyen.TagName = "MTT"; //Phương tiện vận chuyển
                ucCuaKhauNhap.TagCode = "IBC"; //Mã cửa khẩu nhập
                ucCuaKhauNhap.TagName = "IBN"; //Cửa khẩu nhập
                txtSoGiayChungNhan.Tag = "PMN"; //Số giấy chứng nhận
                txtDiaDiemKiemDich.Tag = "QLN"; //Địa điểm kiểm dịch
                txtBenhMienDich.Tag = "VCD"; //Bệnh miễn dịch
                dtNgayTiemPhong.Tag = "DVC"; //Ngày tiêm phòng
                txtKhuTrung.Tag = "ATT"; //Khử trùng
                txtNongDo.Tag = "CCT"; //Nồng độ
                txtDiaDiemNuoiTrong.Tag = "PDC"; //Địa điểm nuôi trồng
                dtNgayKiemDich.Tag = "QRT"; //Ngày kiểm dịch
                dtThoiGianKiemDich.Tag = "QRH"; //Thời gian kiểm dịch
                txtDiaDiemGiamSat.Tag = "CTL"; //Địa điểm giám sát
                dtNgayGiamSat.Tag = "CLT"; //Ngày giám sát
                dtThoiGianGiamSat.Tag = "CLH"; //Thời gian giám sát
                txtSoBanCanCap.Tag = "NMC"; //Số bản cần cấp
                txtVatDungKhac.Tag = "OEQ"; //Vật dụng khác
                txtHoSoLienQuan.Tag = "AD"; //Hồ sơ liên quan
                txtNoiChuyenDen.Tag = "ALC"; //Nơi chuyển đến 
                txtNguoiKiemTra.Tag = "TES"; //Người kiểm tra
                txtKetQuaKiemTra.Tag = "TER"; //Kết quả kiểm tra
                txtTenTau.Tag = "SHN"; //Tên tàu
                ucQuocTich.TagName = "NAT"; //Quốc tịch
                txtTenThuyenTruong.Tag = "CTN"; //Tên thuyền trưởng
                txtTenBacSi.Tag = "DTN"; //Tên bác sĩ
                txtSoThuyenVien.Tag = "CRN"; //Số thuyền viên
                txtSoHanhKhach.Tag = "PGN"; //Số hành khách
                txtCangRoiCuoiCung.Tag = "LDP"; //Cảng rời cuối cùng
                txtCangDenTiepTheo.Tag = "NAP"; //Cảng đến tiếp theo
                txtCangBocHangDauTien.Tag = "DCP"; //Cảng bốc hàng đầu tiên
                dtNgayRoiCanh.Tag = "DOD"; //Ngày rời cảnh
                txtTenHangCangDauTien.Tag = "GDC"; //Tên hàng cảng đầu tiên
                txtSoLuongHangCangDauTien.Tag = "FQ"; //Số lượng hàng cảng đầu tiên
                ucDonViTinhSoLuongHangCangDauTien.TagName = "FQU"; //Đơn vị tính số lượng
                txtKhoiLuongHangCangDauTien.Tag = "FW"; //Khối lượng hàng cảng đầu tiên
                ucDonViTinhKhoiLuongHangCangDauTien.TagName = "FWU"; //Đơn vị tính khối lượng
                txtTenHangCanBoc.Tag = "DGD"; //Tên hàng cần bốc
                txtSoLuongHangCanBoc.Tag = "DQ"; //Số lượng hàng cần bốc
                ucDonViTinhSoLuongHangCanBoc.TagName = "DQU"; //Đơn vị tính số lượng
                txtKhoiLuongHangCanBoc.Tag = "DW"; //Khối lượng hàng cần bốc
                ucDonViTinhKhoiLuongHangCanBoc.TagName = "DWU"; //Đơn vị tính khối lượng                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            bool warning = false;

            try
            {
                Cursor = Cursors.WaitCursor;

                ucChucNangChungTu.SetValidate = !isOnlyWarning; ucChucNangChungTu.SetOnlyWarning = isOnlyWarning;
                warning = ucChucNangChungTu.IsOnlyWarning;
                isValid &= ucChucNangChungTu.IsValidate; //"Chức năng của chứng từ");

                ucMaDonViCapPhep.SetValidate = !isOnlyWarning; ucMaDonViCapPhep.SetOnlyWarning = isOnlyWarning;
                isValid &= ucLoaiGiayPhep.IsValidate; //"Loại giấy phép"

                ucCuaKhauNhap.ShowColumnCode = true; ucCuaKhauNhap.ShowColumnName = false;
                ucCuaKhauNhap.SetValidate = !isOnlyWarning; ucCuaKhauNhap.SetOnlyWarning = isOnlyWarning;
                warning = ucCuaKhauNhap.IsOnlyWarning;
                isValid &= ucCuaKhauNhap.IsValidate; //"Mã cửa khẩu nhập");

                ucMaDonViCapPhep.ShowColumnCode = true; ucMaDonViCapPhep.ShowColumnName = false;
                ucMaDonViCapPhep.SetValidate = !isOnlyWarning; ucMaDonViCapPhep.SetOnlyWarning = isOnlyWarning;
                warning = ucMaDonViCapPhep.IsOnlyWarning;
                isValid &= ucMaDonViCapPhep.IsValidate; //"Mã đơn vị cấp phép"

                isValid &= ValidateControl.ValidateNull(txtMaNguoiKhai, errorProvider, "Mã người khai", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaThuongNhanNhapKhau, errorProvider, "Mã tổ chức/cá nhân nhập khẩu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenThuongNhanXuatKhau, errorProvider, "Tổ chức/cá nhân xuất khẩu", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNguoiKhai.MaxLength = 13;
                txtMaBuuChinhNguoiKhai.MaxLength = 7;
                txtDiaChiNguoiKhai.MaxLength = 300;
                //txtMaNuocNguoiKhai.MaxLength = 2;
                txtSoDienThoaiNguoiKhai.MaxLength = 20;
                txtSoFaxNguoiKhai.MaxLength = 20;
                txtEmailNguoiKhai.MaxLength = 210;
                txtSoDonXinCapPhep.MaxLength = 12;
                //txtChucNangChungTu.MaxLength = 1;
                //ucLoaiGiayPhep.MaxLength = 4;
                //ucMaDonViCapPhep.MaxLength = 6;
                txtSoHopDong.MaxLength = 35;
                txtTenThuongNhanXuatKhau.MaxLength = 70;
                txtMaBuuChinhXuatKhau.MaxLength = 9;
                txtSoNhaTenDuongXuatKhau.MaxLength = 35;
                txtPhuongXaXuatKhau.MaxLength = 35;
                txtQuanHuyenXuatKhau.MaxLength = 35;
                txtTinhThanhPhoXuatKhau.MaxLength = 35;
                //txtMaQuocGiaXuatKhau.MaxLength = 2;
                txtSoDienThoaiXuatKhau.MaxLength = 20;
                txtSoFaxXuatKhau.MaxLength = 20;
                txtEmailXuatKhau.MaxLength = 210;
                //txtNuocXuatKhau.MaxLength = 2;
                txtMaThuongNhanNhapKhau.MaxLength = 13;
                txtTenThuongNhanNhapKhau.MaxLength = 300;
                txtSoDangKyKinhDoanh.MaxLength = 35;
                txtMaBuuChinhNhapKhau.MaxLength = 7;
                txtDiaChiThuongNhanNhapKhau.MaxLength = 300;
                //txtMaQuocGiaNhapKhau.MaxLength = 2;
                txtSoDienThoaiNhapKhau.MaxLength = 20;
                txtSoFaxNhapKhau.MaxLength = 20;
                txtEmailNhapKhau.MaxLength = 210;
                //txtNuocQuaCanh.MaxLength = 2;
                //txtPhuongTienVanChuyen.MaxLength = 2;
                //txtMaCuaKhauNhap.MaxLength = 6;
                //txtTenCuaKhauNhap.MaxLength = 35;
                txtSoGiayChungNhan.MaxLength = 35;
                txtDiaDiemKiemDich.MaxLength = 150;
                txtBenhMienDich.MaxLength = 100;
                //txtNgayTiemPhong.MaxLength = 8;
                txtKhuTrung.MaxLength = 100;
                txtNongDo.MaxLength = 100;
                txtDiaDiemNuoiTrong.MaxLength = 70;
                //txtNgayKiemDich.MaxLength = 8;
                //txtThoiGianKiemDich.MaxLength = 4;
                txtDiaDiemGiamSat.MaxLength = 150;
                //txtNgayGiamSat.MaxLength = 8;
                //txtThoiGianGiamSat.MaxLength = 4;
                txtSoBanCanCap.MaxLength = 2;
                txtVatDungKhac.MaxLength = 150;
                txtHoSoLienQuan.MaxLength = 750;
                txtNoiChuyenDen.MaxLength = 250;
                txtNguoiKiemTra.MaxLength = 50;
                txtKetQuaKiemTra.MaxLength = 996;
                txtTenTau.MaxLength = 35;
                //txtQuocTich.MaxLength = 2;
                txtTenThuyenTruong.MaxLength = 50;
                txtTenBacSi.MaxLength = 50;
                txtSoThuyenVien.MaxLength = 4;
                txtSoHanhKhach.MaxLength = 4;
                txtCangRoiCuoiCung.MaxLength = 100;
                txtCangDenTiepTheo.MaxLength = 100;
                txtCangBocHangDauTien.MaxLength = 100;
                //txtNgayRoiCanh.MaxLength = 8;
                txtTenHangCangDauTien.MaxLength = 100;
                txtSoLuongHangCangDauTien.MaxLength = 8;
                //txtDonViTinhSoLuongHangCangDauTien.MaxLength = 3;
                txtKhoiLuongHangCangDauTien.MaxLength = 10;
                //txtDonViTinhKhoiLuongHangCangDauTien.MaxLength = 3;
                txtTenHangCanBoc.MaxLength = 100;
                txtSoLuongHangCanBoc.MaxLength = 8;
                //txtDonViTinhSoLuongHangCanBoc.MaxLength = 3;
                txtKhoiLuongHangCanBoc.MaxLength = 10;
                //txtDonViTinhKhoiLuongHangCanBoc.MaxLength = 3;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        #region Send VNACCS

        /// <summary>
        /// Khai báo thông tin
        /// </summary>
        /// <param name="KhaiBaoSua"></param>
        private void SendVnaccsIDC(bool KhaiBaoSua)
        {
            try
            {
                if (GiayPhep.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }

                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến Hải quan? ", true) == "Yes")
                {
                    GiayPhep.InputMessageID = HelperVNACCS.NewInputMSGID(); //Tạo mới GUID ID
                    GiayPhep.InsertUpdateFull(); //Lưu thông tin GUID ID vừa tạo

                    MessagesSend msg; //Form khai báo
                    SAA saa = VNACCMaperFromObject.SAAMapper(GiayPhep); //Set Mapper
                    if (saa == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    msg = MessagesSend.Load<SAA>(saa, GiayPhep.InputMessageID);

                    MsgLog.SaveMessages(msg, GiayPhep.ID, EnumThongBao.SendMess, ""); //Lưu thông tin trước khai báo
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true; //Có khai báo
                    f.isRep = true; //Có nhận phản hồi
                    f.inputMSGID = GiayPhep.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result) //Có kêt quả trả về
                    {
                        string ketqua = "Khai báo thông tin thành công";

                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                decimal soTiepNhan = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                ketqua = ketqua + Environment.NewLine;
                                ketqua += "Số đơn xin cấp phép: " + soTiepNhan;

                                GiayPhep.SoDonXinCapPhep = soTiepNhan;
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật số giấp phép: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }

                        TuDongCapNhatThongTin();
                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        #endregion

        #region Cập nhật thông tin

        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            ProcessMessages.GetDataResult_GiayPhep(msgResult, "", GiayPhep);
            GiayPhep.InsertUpdateFull();
            SetGiayPhep();
            //setCommandStatus();
        }

        private void TuDongCapNhatThongTin()
        {
            if (GiayPhep != null && GiayPhep.SoDonXinCapPhep > 0 && GiayPhep.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}' And MessagesInputID = '{1}'", GiayPhep.SoDonXinCapPhep.ToString(), GiayPhep.InputMessageID), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }

        #endregion

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            txtMaNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaBuuChinhNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaNuocNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoDienThoaiNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoFaxNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            ucLoaiGiayPhep.TextChanged += new EventHandler(SetTextChanged_Handler);
            ucMaDonViCapPhep.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoHopDong.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenThuongNhanXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaBuuChinhXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoNhaTenDuongXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtPhuongXaXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtQuanHuyenXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTinhThanhPhoXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucMaQuocGiaXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoDienThoaiXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoFaxXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtNuocXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaThuongNhanNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoDangKyKinhDoanh.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaBuuChinhNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaQuocGiaNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoDienThoaiNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoFaxNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtNuocQuaCanh.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtPhuongTienVanChuyen.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaCuaKhauNhap.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtTenCuaKhauNhap.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoGiayChungNhan.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtDiaDiemKiemDich.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtBenhMienDich.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtKhuTrung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtNongDo.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtDiaDiemNuoiTrong.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtThoiGianKiemDich.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtDiaDiemGiamSat.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtThoiGianGiamSat.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtVatDungKhac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtNoiChuyenDen.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtNguoiKiemTra.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenTau.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtQuocTich.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenThuyenTruong.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenBacSi.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtCangRoiCuoiCung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtCangDenTiepTheo.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtCangBocHangDauTien.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenHangCangDauTien.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtDonViTinhSoLuongHangCangDauTien.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtDonViTinhKhoiLuongHangCangDauTien.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenHangCanBoc.TextChanged += new EventHandler(SetTextChanged_Handler);

            //UpperCase
            txtMaNguoiKhai.CharacterCasing = CharacterCasing.Upper; //Mã người khai
            txtMaBuuChinhNguoiKhai.CharacterCasing = CharacterCasing.Upper; //Mã bưu chính của người khai
            ucMaNuocNguoiKhai.IsUpperCase = true; //Mã nước của người khai
            txtSoDienThoaiNguoiKhai.CharacterCasing = CharacterCasing.Upper; //Số điện thoại của người khai
            txtSoFaxNguoiKhai.CharacterCasing = CharacterCasing.Upper; //Số fax của người khai
            txtSoDonXinCapPhep.CharacterCasing = CharacterCasing.Upper; //Số đơn xin cấp phép
            ucChucNangChungTu.IsUpperCase = true; //Chức năng của chứng từ
            ucLoaiGiayPhep.IsUpperCase = true; //Loại giấy phép
            ucMaDonViCapPhep.IsUpperCase = true; //Mã đơn vị cấp phép
            txtSoHopDong.CharacterCasing = CharacterCasing.Upper; //Số hợp đồng
            txtTenThuongNhanXuatKhau.CharacterCasing = CharacterCasing.Upper; //Tổ chức/cá nhân xuất khẩu
            txtMaBuuChinhXuatKhau.CharacterCasing = CharacterCasing.Upper; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Mã bưu chính)
            txtSoNhaTenDuongXuatKhau.CharacterCasing = CharacterCasing.Upper; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Số nhà, tên đường)
            txtPhuongXaXuatKhau.CharacterCasing = CharacterCasing.Upper; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Phường, xã)
            txtQuanHuyenXuatKhau.CharacterCasing = CharacterCasing.Upper; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Quận, huyện, thị xã)
            txtTinhThanhPhoXuatKhau.CharacterCasing = CharacterCasing.Upper; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Tỉnh, thành phố)
            ucMaQuocGiaXuatKhau.IsUpperCase = true; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Mã quốc gia)
            txtSoDienThoaiXuatKhau.CharacterCasing = CharacterCasing.Upper; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Số điện thoại)
            txtSoFaxXuatKhau.CharacterCasing = CharacterCasing.Upper; //Địa chỉ của tổ chức/cá nhân xuất khẩu (Số fax)
            ucNuocXuatKhau.IsUpperCase = true; //Nước xuất khẩu
            txtMaThuongNhanNhapKhau.CharacterCasing = CharacterCasing.Upper; //Mã tổ chức/cá nhân nhập khẩu
            txtSoDangKyKinhDoanh.CharacterCasing = CharacterCasing.Upper; //Số đăng ký kinh doanh
            txtMaBuuChinhNhapKhau.CharacterCasing = CharacterCasing.Upper; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Mã bưu chính)
            ucMaQuocGiaNhapKhau.IsUpperCase = true; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Mã quốc gia)
            txtSoDienThoaiNhapKhau.CharacterCasing = CharacterCasing.Upper; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Số điện thoại)
            txtSoFaxNhapKhau.CharacterCasing = CharacterCasing.Upper; //Địa chỉ của tổ chức/cá nhân nhập khẩu (Số fax)
            ucNuocQuaCanh.IsUpperCase = true; //Nước quá cảnh
            ucPhuongTienVanChuyen.IsUpperCase = true; //Phương tiện vận chuyển
            ucCuaKhauNhap.IsUpperCase = true; //Mã cửa khẩu nhập
            //txtTenCuaKhauNhap.CharacterCasing = CharacterCasing.Upper; //Cửa khẩu nhập
            txtSoGiayChungNhan.CharacterCasing = CharacterCasing.Upper; //Số giấy chứng nhận
            txtDiaDiemKiemDich.CharacterCasing = CharacterCasing.Upper; //Địa điểm kiểm dịch
            txtBenhMienDich.CharacterCasing = CharacterCasing.Upper; //Bệnh miễn dịch
            //dtNgayTiemPhong.CharacterCasing = CharacterCasing.Upper; //Ngày tiêm phòng
            txtKhuTrung.CharacterCasing = CharacterCasing.Upper; //Khử trùng
            txtNongDo.CharacterCasing = CharacterCasing.Upper; //Nồng độ
            txtDiaDiemNuoiTrong.CharacterCasing = CharacterCasing.Upper; //Địa điểm nuôi trồng
            //dtNgayKiemDich.CharacterCasing = CharacterCasing.Upper; //Ngày kiểm dịch
            //dtThoiGianKiemDich.CharacterCasing = CharacterCasing.Upper; //Thời gian kiểm dịch
            txtDiaDiemGiamSat.CharacterCasing = CharacterCasing.Upper; //Địa điểm giám sát
            //dtNgayGiamSat.CharacterCasing = CharacterCasing.Upper; //Ngày giám sát
            //dtThoiGianGiamSat.CharacterCasing = CharacterCasing.Upper; //Thời gian giám sát
            txtSoBanCanCap.CharacterCasing = CharacterCasing.Upper; //Số bản cần cấp
            txtVatDungKhac.CharacterCasing = CharacterCasing.Upper; //Vật dụng khác
            txtNoiChuyenDen.CharacterCasing = CharacterCasing.Upper; //Nơi chuyển đến 
            txtNguoiKiemTra.CharacterCasing = CharacterCasing.Upper; //Người kiểm tra
            txtTenTau.CharacterCasing = CharacterCasing.Upper; //Tên tàu
            ucQuocTich.IsUpperCase = true; //Quốc tịch
            txtTenThuyenTruong.CharacterCasing = CharacterCasing.Upper; //Tên thuyền trưởng
            txtTenBacSi.CharacterCasing = CharacterCasing.Upper; //Tên bác sĩ
            txtSoThuyenVien.CharacterCasing = CharacterCasing.Upper; //Số thuyền viên
            txtSoHanhKhach.CharacterCasing = CharacterCasing.Upper; //Số hành khách
            txtCangRoiCuoiCung.CharacterCasing = CharacterCasing.Upper; //Cảng rời cuối cùng
            txtCangDenTiepTheo.CharacterCasing = CharacterCasing.Upper; //Cảng đến tiếp theo
            txtCangBocHangDauTien.CharacterCasing = CharacterCasing.Upper; //Cảng bốc hàng đầu tiên
            //dtNgayRoiCanh.CharacterCasing = CharacterCasing.Upper; //Ngày rời cảnh
            txtTenHangCangDauTien.CharacterCasing = CharacterCasing.Upper; //Tên hàng cảng đầu tiên
            txtSoLuongHangCangDauTien.CharacterCasing = CharacterCasing.Upper; //Số lượng hàng cảng đầu tiên
            ucDonViTinhSoLuongHangCangDauTien.IsUpperCase = true; //Đơn vị tính số lượng
            txtKhoiLuongHangCangDauTien.CharacterCasing = CharacterCasing.Upper; //Khối lượng hàng cảng đầu tiên
            ucDonViTinhKhoiLuongHangCangDauTien.IsUpperCase = true; //Đơn vị tính khối lượng            
            txtTenHangCanBoc.CharacterCasing = CharacterCasing.Upper; //Tên hàng cần bốc
            txtSoLuongHangCanBoc.CharacterCasing = CharacterCasing.Upper; //Số lượng hàng cần bốc
            ucDonViTinhSoLuongHangCanBoc.IsUpperCase = true; //Đơn vị tính số lượng
            txtKhoiLuongHangCanBoc.CharacterCasing = CharacterCasing.Upper; //Khối lượng hàng cần bốc
            ucDonViTinhKhoiLuongHangCanBoc.IsUpperCase = true; //Đơn vị tính khối lượng
        }

        private void ucPhuongTienVanChuyen_EditValueChanged(object sender, EventArgs e)
        {
            if (ucPhuongTienVanChuyen.Code == "LAND" || ucPhuongTienVanChuyen.Code == "04") //Duong bo: LAND/ 04
                ucCuaKhauNhap.CategoryType = ECategory.A601;
            else if (ucPhuongTienVanChuyen.Code == "RAIL" || ucPhuongTienVanChuyen.Code == "03") //Duong sat: RAIL/ 03
                ucCuaKhauNhap.CategoryType = ECategory.A620;
            else
                ucCuaKhauNhap.CategoryType = ECategory.A016;
            ucCuaKhauNhap.ReLoadData();
        }

    }
}
