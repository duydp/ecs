﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Transactions;
using System.Globalization;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Nodes.GC;
using Company.KDT.SHARE.Components.Utils;

namespace Company.KDT.SHARE.QuanLyChungTu.GCCT
{
    public partial class HoaDon
    {
        public List<HoaDonChiTiet> ListHangMDOfHoaDon = new List<HoaDonChiTiet>();

        public void LoadListHangDMOfHoaDon()
        {
            ListHangMDOfHoaDon = (List<HoaDonChiTiet>)HoaDonChiTiet.SelectCollectionBy_HoaDonTM_ID(this.ID);
        }

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.Insert(transaction);
                        else
                            this.Update(transaction);
                        foreach (HoaDonChiTiet item in ListHangMDOfHoaDon)
                        {
                            item.HoaDonTM_ID = this.ID;
                            if (id == 0)
                                item.ID = 0;

                            if (item.ID == 0)
                                item.Insert(transaction);
                            else
                                item.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        this.ID = id;
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public static XmlNode ConvertCollectionHoaDonThuongMaiToXML_TKX(XmlDocument doc, List<HoaDon> HoaDonThuongMaiCollection, long TKMD_ID, List<HangChuyenTiep> listHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            if (HoaDonThuongMaiCollection == null || HoaDonThuongMaiCollection.Count == 0)
            {
                HoaDonThuongMaiCollection = (List<HoaDon>)HoaDon.SelectCollectionBy_ID_TK(TKMD_ID);
            }
            XmlElement CHUNG_TU_HOADON = doc.CreateElement("CHUNG_TU_HOADON");
            foreach (HoaDon HoaDonTM in HoaDonThuongMaiCollection)
            {
                XmlElement hopdongTMItem = doc.CreateElement("CHUNG_TU_HOADON.ITEM");
                CHUNG_TU_HOADON.AppendChild(hopdongTMItem);

                XmlAttribute SO_CT = doc.CreateAttribute("SO_CT");
                SO_CT.Value = FontConverter.Unicode2TCVN(HoaDonTM.SoHoaDon.Trim());
                hopdongTMItem.Attributes.Append(SO_CT);

                XmlAttribute NGAY_CT = doc.CreateAttribute("NGAY_CT");
                NGAY_CT.Value = HoaDonTM.NgayHoaDon.ToString("yyyy-MM-dd");
                hopdongTMItem.Attributes.Append(NGAY_CT);

                XmlAttribute MA_GH = doc.CreateAttribute("MA_GH");
                MA_GH.Value = HoaDonTM.DKGH_ID;
                hopdongTMItem.Attributes.Append(MA_GH);

                XmlAttribute MA_PTTT = doc.CreateAttribute("MA_PTTT");
                MA_PTTT.Value = HoaDonTM.PTTT_ID; ;
                hopdongTMItem.Attributes.Append(MA_PTTT);

                XmlAttribute MA_NT = doc.CreateAttribute("MA_NT");
                MA_NT.Value = HoaDonTM.NguyenTe_ID;
                hopdongTMItem.Attributes.Append(MA_NT);

                XmlAttribute MA_DV_DT = doc.CreateAttribute("MA_DV_DT");
                MA_DV_DT.Value = HoaDonTM.MaDonViMua.Trim();
                hopdongTMItem.Attributes.Append(MA_DV_DT);

                XmlAttribute TEN_DV_DT = doc.CreateAttribute("TEN_DV_DT");
                TEN_DV_DT.Value = FontConverter.Unicode2TCVN(HoaDonTM.TenDonViMua.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV_DT);

                XmlAttribute MA_DV = doc.CreateAttribute("MA_DV");
                MA_DV.Value = HoaDonTM.MaDonViBan.Trim();
                hopdongTMItem.Attributes.Append(MA_DV);

                XmlAttribute TEN_DV = doc.CreateAttribute("TEN_DV");
                TEN_DV.Value = FontConverter.Unicode2TCVN(HoaDonTM.TenDonViBan.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV);

                XmlAttribute GHI_CHU = doc.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = FontConverter.Unicode2TCVN(HoaDonTM.ThongTinKhac.Trim());
                hopdongTMItem.Attributes.Append(GHI_CHU);

                if (HoaDonTM.ListHangMDOfHoaDon == null || HoaDonTM.ListHangMDOfHoaDon.Count == 0)
                {
                    HoaDonTM.LoadListHangDMOfHoaDon();
                }

                foreach (HoaDonChiTiet hangHoaDon in HoaDonTM.ListHangMDOfHoaDon)
                {

                    HangChuyenTiep hmd = BaseClass.GetHangChuyenTiepByIDHCT(hangHoaDon.HMD_ID, listHang);

                    if (hmd == null) continue;

                    //Cap nhat thong tin hang thay doi cua rieng giay phep.
                    if (hmd != null)
                    {
                        hmd.MaHS = hangHoaDon.MaHS;
                        hmd.MaHang = hangHoaDon.MaPhu;
                        hmd.TenHang = hangHoaDon.TenHang;
                        hmd.ID_NuocXX = hangHoaDon.NuocXX_ID;
                        hmd.ID_DVT = hangHoaDon.DVT_ID;
                        hmd.SoLuong = hangHoaDon.SoLuong;
                        hmd.DonGia = (decimal)hangHoaDon.DonGiaKB;
                        hmd.TriGia = (decimal)hangHoaDon.TriGiaKB;
                    }

                    XmlElement hangItem = doc.CreateElement("CHUNG_TU_HOADON.HANG");
                    hopdongTMItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = doc.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaHang.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = doc.CreateAttribute("TEN_HANG");
                    TEN_HANG.Value = FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = doc.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute NUOC_XX = doc.CreateAttribute("NUOC_XX");
                    NUOC_XX.Value = hmd.ID_NuocXX;
                    hangItem.Attributes.Append(NUOC_XX);

                    XmlAttribute MA_DVT = doc.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.ID_DVT.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = doc.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute DGIA_KB = doc.CreateAttribute("DGIA_KB");
                    DGIA_KB.Value = BaseClass.Round(hmd.DonGia, 9);
                    hangItem.Attributes.Append(DGIA_KB);

                    XmlAttribute TRIGIA_KB = doc.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGia, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);

                    XmlAttribute GIATRITANG = doc.CreateAttribute("GIATRITANG");
                    GIATRITANG.Value = BaseClass.Round(hangHoaDon.GiaTriDieuChinhTang, 9);
                    hangItem.Attributes.Append(GIATRITANG);

                    XmlAttribute GIATRIGIAM = doc.CreateAttribute("GIATRIGIAM");
                    GIATRIGIAM.Value = BaseClass.Round(hangHoaDon.GiaiTriDieuChinhGiam, 9);
                    hangItem.Attributes.Append(GIATRIGIAM);

                    XmlAttribute GHI_CHUhang = doc.CreateAttribute("GHI_CHU");
                    GHI_CHUhang.Value = FontConverter.Unicode2TCVN(hangHoaDon.GhiChu != null ? hangHoaDon.GhiChu.Trim() : "");
                    hangItem.Attributes.Append(GHI_CHUhang);
                }
            }
            if (CHUNG_TU_HOADON.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_HOADON;
        }

        public static XmlNode ConvertCollectionHoaDonThuongMaiToXML_TKN(XmlDocument doc, List<HoaDon> HoaDonThuongMaiCollection, long TKMD_ID, List<HangChuyenTiep> listHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            if (HoaDonThuongMaiCollection == null || HoaDonThuongMaiCollection.Count == 0)
            {
                HoaDonThuongMaiCollection = (List<HoaDon>)HoaDon.SelectCollectionBy_ID_TK(TKMD_ID);
            }
            XmlElement CHUNG_TU_HOADON = doc.CreateElement("CHUNG_TU_HOADON");
            foreach (HoaDon HoaDonTM in HoaDonThuongMaiCollection)
            {
                XmlElement hopdongTMItem = doc.CreateElement("CHUNG_TU_HOADON.ITEM");
                CHUNG_TU_HOADON.AppendChild(hopdongTMItem);

                XmlAttribute SO_CT = doc.CreateAttribute("SO_CT");
                SO_CT.Value = FontConverter.Unicode2TCVN(HoaDonTM.SoHoaDon.Trim());
                hopdongTMItem.Attributes.Append(SO_CT);

                XmlAttribute NGAY_CT = doc.CreateAttribute("NGAY_CT");
                NGAY_CT.Value = HoaDonTM.NgayHoaDon.ToString("yyyy-MM-dd");
                hopdongTMItem.Attributes.Append(NGAY_CT);

                XmlAttribute MA_GH = doc.CreateAttribute("MA_GH");
                MA_GH.Value = HoaDonTM.DKGH_ID;
                hopdongTMItem.Attributes.Append(MA_GH);

                XmlAttribute MA_PTTT = doc.CreateAttribute("MA_PTTT");
                MA_PTTT.Value = HoaDonTM.PTTT_ID; ;
                hopdongTMItem.Attributes.Append(MA_PTTT);

                XmlAttribute MA_NT = doc.CreateAttribute("MA_NT");
                MA_NT.Value = HoaDonTM.NguyenTe_ID;
                hopdongTMItem.Attributes.Append(MA_NT);

                XmlAttribute MA_DV = doc.CreateAttribute("MA_DV");
                MA_DV.Value = HoaDonTM.MaDonViMua.Trim();
                hopdongTMItem.Attributes.Append(MA_DV);

                XmlAttribute TEN_DV = doc.CreateAttribute("TEN_DV");
                TEN_DV.Value = FontConverter.Unicode2TCVN(HoaDonTM.TenDonViMua.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV);

                XmlAttribute MA_DV_DT = doc.CreateAttribute("MA_DV_DT");
                MA_DV_DT.Value = HoaDonTM.MaDonViBan.Trim();
                hopdongTMItem.Attributes.Append(MA_DV_DT);

                XmlAttribute TEN_DV_DT = doc.CreateAttribute("TEN_DV_DT");
                TEN_DV_DT.Value = FontConverter.Unicode2TCVN(HoaDonTM.TenDonViBan.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV_DT);

                XmlAttribute GHI_CHU = doc.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = FontConverter.Unicode2TCVN(HoaDonTM.ThongTinKhac.Trim());
                hopdongTMItem.Attributes.Append(GHI_CHU);

                if (HoaDonTM.ListHangMDOfHoaDon == null || HoaDonTM.ListHangMDOfHoaDon.Count == 0)
                {
                    HoaDonTM.LoadListHangDMOfHoaDon();
                }

                foreach (HoaDonChiTiet hangHoaDon in HoaDonTM.ListHangMDOfHoaDon)
                {

                    HangChuyenTiep hmd = BaseClass.GetHangChuyenTiepByIDHCT(hangHoaDon.HMD_ID, listHang);

                    if (hmd == null) continue;

                    //Cap nhat thong tin hang thay doi cua rieng giay phep.
                    if (hmd != null)
                    {
                        hmd.MaHS = hangHoaDon.MaHS;
                        hmd.MaHang = hangHoaDon.MaPhu;
                        hmd.TenHang = hangHoaDon.TenHang;
                        hmd.ID_NuocXX = hangHoaDon.NuocXX_ID;
                        hmd.ID_DVT = hangHoaDon.DVT_ID;
                        hmd.SoLuong = hangHoaDon.SoLuong;
                        hmd.DonGia = (decimal)hangHoaDon.DonGiaKB;
                        hmd.TriGia = (decimal)hangHoaDon.TriGiaKB;
                    }

                    XmlElement hangItem = doc.CreateElement("CHUNG_TU_HOADON.HANG");
                    hopdongTMItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = doc.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaHang.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = doc.CreateAttribute("TEN_HANG");
                    TEN_HANG.Value = FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = doc.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute NUOC_XX = doc.CreateAttribute("NUOC_XX");
                    NUOC_XX.Value = hmd.ID_NuocXX;
                    hangItem.Attributes.Append(NUOC_XX);

                    XmlAttribute MA_DVT = doc.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.ID_DVT.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = doc.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute DGIA_KB = doc.CreateAttribute("DGIA_KB");
                    DGIA_KB.Value = BaseClass.Round(hmd.DonGia, 9);
                    hangItem.Attributes.Append(DGIA_KB);

                    XmlAttribute TRIGIA_KB = doc.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGia, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);

                    XmlAttribute GIATRITANG = doc.CreateAttribute("GIATRITANG");
                    GIATRITANG.Value = BaseClass.Round(hangHoaDon.GiaTriDieuChinhTang, 9);
                    hangItem.Attributes.Append(GIATRITANG);

                    XmlAttribute GIATRIGIAM = doc.CreateAttribute("GIATRIGIAM");
                    GIATRIGIAM.Value = BaseClass.Round(hangHoaDon.GiaiTriDieuChinhGiam, 9);
                    hangItem.Attributes.Append(GIATRIGIAM);

                    XmlAttribute GHI_CHUhang = doc.CreateAttribute("GHI_CHU");
                    GHI_CHUhang.Value = FontConverter.Unicode2TCVN(hangHoaDon.GhiChu != null ? hangHoaDon.GhiChu.Trim() : "");
                    hangItem.Attributes.Append(GHI_CHUhang);
                }
            }
            if (CHUNG_TU_HOADON.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_HOADON;
        }

        public string WSSend(string password, string maHQ, long soTK, string maLH, int namDK, long TKMD_ID, List<HangChuyenTiep> listHang)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\KhaiBoSung.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.KhaiBao).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GuidStr;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;


            XmlNode nodeTO_KHAI = doc.GetElementsByTagName("TO_KHAI")[0];
            nodeTO_KHAI.ChildNodes[0].Attributes["MA_LH"].Value = maLH;
            nodeTO_KHAI.ChildNodes[0].Attributes["MA_HQ"].Value = maHQ;
            nodeTO_KHAI.ChildNodes[0].Attributes["SOTK"].Value = soTK + "";
            nodeTO_KHAI.ChildNodes[0].Attributes["NAMDK"].Value = namDK + "";


            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            List<HoaDon> listHoaDonThuongMai = new List<HoaDon>();
            listHoaDonThuongMai.Add(this);
            nodeDulieu.AppendChild(HoaDon.ConvertCollectionHoaDonThuongMaiToXML_TKN(doc, listHoaDonThuongMai, TKMD_ID, listHang));


            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }

        public bool WSKhaiBaoHoaDonTM(string password, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy, long TKMD_ID, string maDoanhNghiep, List<HoaDonChiTiet> list, MessageTypes messageType, MessageFunctions messageFunction, List<HangChuyenTiep> listHang)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocument.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            // Message ID.
            XmlNode xmlNodeMessageID = xmlDocument.GetElementsByTagName("messageId")[0];
            xmlNodeMessageID.InnerText = Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode xmlNodeFrom = xmlDocument.GetElementsByTagName("From")[0];

            XmlNode xmlNodeFromName = xmlNodeFrom.ChildNodes[0];
            xmlNodeFromName.InnerText = maDoanhNghiep;
            XmlNode xmlNodeFromIdentity = xmlNodeFrom.ChildNodes[1];
            xmlNodeFromIdentity.InnerText = maDoanhNghiep;

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode xmlNodeTo = xmlDocument.GetElementsByTagName("To")[0];

            XmlNode xmlNodeToName = xmlNodeTo.ChildNodes[0];
            xmlNodeToName.InnerText = maHaiQuan;
            XmlNode xmlNodeToIdentity = xmlNodeTo.ChildNodes[1];
            xmlNodeToIdentity.InnerText = maHaiQuan;

            // Subject.
            XmlNode xmlNodeSubject = xmlDocument.GetElementsByTagName("Subject")[0];

            // Message Type.
            XmlNode xmlNodeType = xmlNodeSubject.ChildNodes[0];
            xmlNodeType.InnerText = ((int)messageType).ToString();

            // Message Function.
            XmlNode xmlNodeFunction = xmlNodeSubject.ChildNodes[1];
            xmlNodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference.
            XmlNode nodeReference = xmlNodeSubject.ChildNodes[2];
            GuidStr = nodeReference.InnerText = System.Guid.NewGuid().ToString();
            this.Update();
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode xmlNodeThongTin = xmlDocument.GetElementsByTagName("THONG_TIN")[0];
            xmlNodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDoanhNghiep;
            xmlNodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDoanhNghiep;

            xmlNodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHaiQuan;
            xmlNodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHaiQuan;

            XmlNode xmlNodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];

            xmlNodeToKhai.Attributes["MA_LH"].Value = maLoaiHinh;
            xmlNodeToKhai.Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeToKhai.Attributes["SOTK"].Value = soToKhai + "";
            xmlNodeToKhai.Attributes["NAMDK"].Value = namDangKy + "";


            XmlNode xmlNodeDulieu = xmlDocument.GetElementsByTagName("DU_LIEU")[0];
            List<HoaDon> listHoaDonThuongMai = new List<HoaDon>();
            listHoaDonThuongMai.Add(this);
            xmlNodeDulieu.AppendChild(HoaDon.ConvertCollectionHoaDonThuongMaiToXML_TKN(xmlDocument, listHoaDonThuongMai, TKMD_ID, listHang));

            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                // Lưu message.
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ToKhaiNhap;
                message.MessageFunction = MessageFunctions.KhaiBao;
                message.MessageFrom = maDoanhNghiep;
                message.MessageTo = maHaiQuan.Trim();
                message.MessageContent = xmlDocument.InnerXml;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungHoaDonTM;
                message.NoiDungThongBao = string.Empty;
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Khai báo.
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    string noiDung = FontConverter.TCVN2Unicode(docResult.GetElementsByTagName("Root")[0].InnerText.Trim());
                    XmlNode node1 = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);

                    // Lưu message.
                    Message message = new Message();
                    message.ItemID = this.ID;
                    message.ReferenceID = new Guid(this.GuidStr);
                    message.MessageType = MessageTypes.ThongTin;
                    message.MessageFunction = MessageFunctions.LayPhanHoi;
                    message.MessageFrom = maHaiQuan.Trim();
                    message.MessageTo = maDoanhNghiep;
                    message.MessageContent = xmlDocument.InnerXml;
                    message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungHoaDonTM;
                    message.NoiDungThongBao = noiDung;// string.Empty;
                    message.CreatedTime = DateTime.Now;
                    message.Insert();

                    return true;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return false;

        }

        public bool WSLaySoTiepNhan(string password)
        {
            KDTService kdt = WebService.GetWS();

            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }

            if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungHoaDonTMThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }

            return false;
        }

        public bool WSLayPhanHoi(string password)
        {
            KDTService kdt = WebService.GetWS();

            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }
            if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
            {
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.TuChoiTiepNhan;
                string lydo = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), lydo);
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Cập lại số tiếp nhận
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.Update();

                return true;
            }
            else if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungHoaDonTMThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }
            else if (xmlNodeResult.Attributes["SOTN"] != null)
            {
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungHoaDonTMDuocChapNhan;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã được Hải quan chấp nhận.", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }

            return false;
        }

        public static XmlNode ConvertCollectionHoaDonThuongMaiToXML(XmlDocument doc, List<HoaDon> HoaDonThuongMaiCollection, long TKMD_ID, List<HangChuyenTiep> listHang)
        {
            if (HoaDonThuongMaiCollection == null || HoaDonThuongMaiCollection.Count == 0)
            {
                HoaDonThuongMaiCollection = (List<HoaDon>)HoaDon.SelectCollectionBy_ID_TK(TKMD_ID);
            }
            XmlElement CommercialInvoices = doc.CreateElement(NodeHoaDon.CommercialInvoices);

            foreach (HoaDon HoaDonTM in HoaDonThuongMaiCollection)
            {
                XmlElement CommercialInvoice = doc.CreateElement(NodeHoaDon.CommercialInvoice);
                CommercialInvoices.AppendChild(CommercialInvoice);
                XmlElement reference = doc.CreateElement(NodeHoaDon.reference);// Số Hóa Đơn
                CommercialInvoice.AppendChild(reference);
                reference.InnerText = HoaDonTM.SoHoaDon;
                XmlElement issue = doc.CreateElement(NodeHoaDon.issue);
                CommercialInvoice.AppendChild(issue);
                issue.InnerText = HoaDonTM.NgayHoaDon.ToString(NgayThang.yyyyMMdd);
                //
                XmlElement Seller = doc.CreateElement(NodeHoaDon.Seller);
                CommercialInvoice.AppendChild(Seller);
                XmlElement name = doc.CreateElement(NodeHoaDon.name);
                Seller.AppendChild(name);
                name.InnerText = HoaDonTM.TenDonViBan;
                XmlElement identity = doc.CreateElement(NodeHoaDon.identity);
                Seller.AppendChild(identity);
                identity.InnerText = HoaDonTM.MaDonViBan;
                //
                XmlElement Buyer = doc.CreateElement(NodeHoaDon.Buyer);
                CommercialInvoice.AppendChild(Buyer);
                XmlElement nameBuyer = doc.CreateElement(NodeHoaDon.name);
                Buyer.AppendChild(nameBuyer);
                nameBuyer.InnerText = HoaDonTM.TenDonViMua;
                XmlElement identityBuyer = doc.CreateElement(NodeHoaDon.identity);
                Buyer.AppendChild(identityBuyer);
                identityBuyer.InnerText = HoaDonTM.MaDonViMua;
                //
                XmlElement AdditionalDocument = doc.CreateElement("AdditionalDocument");
                CommercialInvoice.AppendChild(AdditionalDocument);
                XmlElement referenceAdditionalDocument = doc.CreateElement(NodeHoaDon.reference);
                AdditionalDocument.AppendChild(referenceAdditionalDocument);
                XmlElement issueAdditionalDocument= doc.CreateElement(NodeHoaDon.issue);
                AdditionalDocument.AppendChild(issueAdditionalDocument);
                referenceAdditionalDocument.InnerText = HoaDonTM.SoHoaDon;
                issue.InnerText = HoaDonTM.NgayHoaDon.ToString(NgayThang.yyyyMMdd);
                //
                XmlElement Payment = doc.CreateElement(NodeHoaDon.Payment);
                CommercialInvoice.AppendChild(Payment);
                XmlElement method = doc.CreateElement(NodeHoaDon.method);
                Payment.AppendChild(method);
                method.InnerText = HoaDonTM.PTTT_ID;    
                //
                XmlElement CurrencyExchange = doc.CreateElement(NodeHoaDon.CurrencyExchange);
                CommercialInvoice.AppendChild(CurrencyExchange);
                XmlElement currencyType = doc.CreateElement("currencyType");
                CurrencyExchange.AppendChild(currencyType);
                currencyType.InnerText = HoaDonTM.NguyenTe_ID;
                //   
                XmlElement TradeTerm = doc.CreateElement(NodeHoaDon.TradeTerm);
                CommercialInvoice.AppendChild(TradeTerm);
                XmlElement condition = doc.CreateElement(NodeHoaDon.condition);
                TradeTerm.AppendChild(condition);
                condition.InnerText = HoaDonTM.DKGH_ID;
                
                if (HoaDonTM.ListHangMDOfHoaDon == null || HoaDonTM.ListHangMDOfHoaDon.Count == 0)
                {
                    HoaDonTM.LoadListHangDMOfHoaDon();
                }

                foreach (HoaDonChiTiet hangHoaDon in HoaDonTM.ListHangMDOfHoaDon)
                {
                    XmlElement CommercialInvoiceItem = doc.CreateElement(NodeHoaDon.CommercialInvoiceItem);
                    CommercialInvoice.AppendChild(CommercialInvoiceItem);
                    XmlElement sequence = doc.CreateElement(NodeHoaDon.sequence);
                    CommercialInvoiceItem.AppendChild(sequence);
                    sequence.InnerText = hangHoaDon.SoThuTuHang.ToString();
                    //
                    XmlElement unitPrice2 = doc.CreateElement("unitPrice");
                    CommercialInvoiceItem.AppendChild(unitPrice2);
                    unitPrice2.InnerText = BaseClass.Round(hangHoaDon.DonGiaKB, 9);
                     //Trị giá tính thuế
                    XmlElement statisticalValue = doc.CreateElement(NodeHoaDon.statisticalValue);
                    CommercialInvoiceItem.AppendChild(statisticalValue);
                    statisticalValue.InnerText = BaseClass.Round(hangHoaDon.TriGiaKB,9);
                    //
                    XmlElement Origin = doc.CreateElement(NodeHoaDon.Origin);
                    CommercialInvoiceItem.AppendChild(Origin);
                    XmlElement originCountry = doc.CreateElement(NodeHoaDon.originCountry);
                    Origin.AppendChild(originCountry);
                    originCountry.InnerText = hangHoaDon.NuocXX_ID;
                    //Thông tin hàng hóa
                    XmlElement Commodity = doc.CreateElement(NodeHoaDon.Commodity);
                    CommercialInvoiceItem.AppendChild(Commodity);
                    XmlElement description = doc.CreateElement(NodeHoaDon.description);
                    Commodity.AppendChild(description);
                    description.InnerText = hangHoaDon.TenHang;
                    XmlElement identification = doc.CreateElement(NodeHoaDon.identification);
                    Commodity.AppendChild(identification);
                    identification.InnerText = hangHoaDon.MaPhu;
                    XmlElement tariffClassification = doc.CreateElement(NodeHoaDon.tariffClassification);
                    Commodity.AppendChild(tariffClassification);
                    tariffClassification.InnerText = hangHoaDon.MaHS;
                    //
                    XmlElement GoodsMeasure = doc.CreateElement(NodeHoaDon.GoodsMeasure);
                    CommercialInvoiceItem.AppendChild(GoodsMeasure);
                    XmlElement quantity = doc.CreateElement(NodeHoaDon.quantity);
                    GoodsMeasure.AppendChild(quantity);
                    quantity.InnerText = BaseClass.Round(hangHoaDon.SoLuong,9);
                    XmlElement measureUnit = doc.CreateElement(NodeHoaDon.measureUnit);
                    GoodsMeasure.AppendChild(measureUnit);
                    measureUnit.InnerText = hangHoaDon.DVT_ID;
                    //Các khoản điều chỉnh
                    XmlElement ValuationAdjustment = doc.CreateElement(NodeHoaDon.ValuationAdjustment);
                    CommercialInvoiceItem.AppendChild(ValuationAdjustment);
                    XmlElement addition = doc.CreateElement(NodeHoaDon.addition);
                    ValuationAdjustment.AppendChild(addition);
                    XmlElement deduction = doc.CreateElement(NodeHoaDon.deduction);
                    ValuationAdjustment.AppendChild(deduction);
                    //Ghi chú khác
                    XmlElement AdditionalInformation = doc.CreateElement(NodeHoaDon.AdditionalInformation);
                    CommercialInvoiceItem.AppendChild(AdditionalInformation);
                    XmlElement content = doc.CreateElement(NodeHoaDon.content);
                    AdditionalInformation.AppendChild(content);
    
                }

            }
            if (CommercialInvoices.ChildNodes.Count == 0)
                return null;
            return CommercialInvoices;
        }
    }
}