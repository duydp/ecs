using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.Interface.SXXK.BangKe;
using Janus.Windows.GridEX;
using Janus.Windows.EditControls;
using Company.BLL.KDT;
using Company.BLL;
using Company.Interface.Report.SXXK;
using System.IO;
using Company.Interface.Report;
using Company.KDT.SHARE.Components;
using Company.Interface.KDT.SXXK;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.BLL.DataTransferObjectMapper;
using Company.KDT.SXXK;
using Company.KDT.SHARE.QuanLyChungTu.CTTT;
using Company.Interface.Report.SXXK.TT38;
using System.Data.Common;

namespace Company.Interface.SXXK
{
    public partial class CapNhatHoSoThanhLyForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        List<Company.KDT.SHARE.Components.Common.HSTKBoSung> hstkBS = null;
        private string xmlCurrent = "";
        public bool isDinhMucChuaDangKy = false;
        public CapNhatHoSoThanhLyForm()
        {
            InitializeComponent();

            //minhnd 24/10/2014
            //lbNgayChenhLech.Text = GlobalSettings.CHENHLECH_THN_THX;

        }

        /// <summary>
        /// Kiem tra so luong chung tu (bang ke) kem theo.
        /// </summary>
        private void CheckSoLuongChungTu()
        {
            try
            {
                numSoLuongChungTu.Value = HSTL.ChungTuTTs != null ? HSTL.ChungTuTTs.Count : 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        /// <summary>
        /// Hien thi thong tin Ngay khai bao vao control
        /// </summary>
        private void LoadNgayKhaiBao()
        {
            try
            {
                dtNgayKhaiBao.Text = hstkBS != null && hstkBS.Count != 0 && hstkBS[0].NgayKhaiBao.Year != 1900 ? hstkBS[0].NgayKhaiBao.ToString("dd/MM/yyyy") : "";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void setCommandStatus()
        {
            if (HSTL.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Luu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                if (GlobalSettings.NGON_NGU == "0")
                    lblTrangThai.Text = "Đã duyệt";
                else
                    lblTrangThai.Text = "Approved";
            }
            else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                sendBK1.Enabled = false;
                huyBangKe1.Enabled = false; ;
            }
            else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                if (GlobalSettings.NGON_NGU == "0")
                    lblTrangThai.Text = "Chờ duyệt";
                else
                    lblTrangThai.Text = "Wait for Approval";

                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                sendBK1.Enabled = true;
                huyBangKe1.Enabled = true;
            }
            else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                sendBK1.Enabled = false;
                huyBangKe1.Enabled = false; ;
            }
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "HSTL";
            sendXML.master_id = HSTL.ID;
            if (sendXML.Load())
            {

                if (GlobalSettings.NGON_NGU == "0")
                    lblTrangThai.Text = "Đang chờ xác nhận hồ sơ ";
                else
                    lblTrangThai.Text = "Waiting for confirmation";

                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                sendBK1.Enabled = false;
                huyBangKe1.Enabled = false;
            }

        }
        private void CapNhatHoSoThanhLyForm_Load(object sender, EventArgs e)
        {
            //Cập nhật tiêu đề
            Text += (GlobalSettings.ThanhKhoanNhieuChiCuc ? " - Đang áp đụng Thanh khoản cho nhiều Chi cục Hải Quan" : "");

            if (GlobalSettings.TKhoanKCX != 0)
            {
                cmdPrint.Visible = cmdPrint1.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdTT38_2015.Visible = cmdTT38_20151.Visible = Janus.Windows.UI.InheritableBoolean.False;
            }
            else
            {
                cmdTT79KCX.Visible = cmdTT79KCX1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            }
            //minhnd set thanh khoản HOATHO
            if (!HSTL.HSTL_TheoMaHang)
            {
                cmdThanhKhoanTheoMaHang1.Visible = cmdThanhKhoanTheoMaHang.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdTT38_2015.Visible = cmdTT38_20151.Visible = Janus.Windows.UI.InheritableBoolean.False;
            }
            else
            {
                ThanhLy1.Visible = Janus.Windows.UI.InheritableBoolean.False;
                lblHinhThuc.Text = "Thành khoản theo PO";
                cmdPrint1.Visible = cmdPrint.Visible = Janus.Windows.UI.InheritableBoolean.False;
            }
            try
            {
                hstkBS = (List<Company.KDT.SHARE.Components.Common.HSTKBoSung>)Company.KDT.SHARE.Components.Common.HSTKBoSung.SelectCollectionBy_HoSoDangKy_ID(HSTL.ID);

                if (HSTL.ChungTuTTs.Count == 0)
                {
                    HSTL.LoadChungTuTTs();
                }

                GlobalSettings.KhoiTao_GiaTriMacDinh();
                ccNgayThanhLy.ReadOnly = false;
                txtNgayTiepNhan.ReadOnly = true;
                txtSoHSTL.Value = this.HSTL.SoHoSo;
                if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                {
                    if (HSTL.SoQuyetDinh != null && HSTL.SoQuyetDinh.Contains("/"))
                    {
                        string[] st = HSTL.SoQuyetDinh.Split('/');
                        txtQuy.Text = st[0].ToString();
                        txtNam.Text = st[1].ToString();
                    }
                }
                else
                    txtSoQuyetDinh.Text = this.HSTL.SoQuyetDinh;
                txtLanThanhLy.Value = this.HSTL.LanThanhLy;
                ccNgayThanhLy.Value = this.HSTL.NgayBatDau;
                refreshTrangThai();
                dgList.DataSource = this.HSTL.BKCollection;
                bindCombobox();
                if (GlobalSettings.NGON_NGU == "1")
                {
                    this.SetCombo();
                }

                //TODO: Hungtq updated 13/03/2013.
                if (hstkBS != null && hstkBS.Count != 0 && hstkBS[0].NgayKhaiBao != new DateTime(1900, 1, 1))
                    this.dtNgayKhaiBao.Text = hstkBS[0].NgayKhaiBao.ToString("dd/MM/yyyy");

                this.txtSoTiepNhan.Text = HSTL.SoTiepNhan.ToString();
                if (this.HSTL.NgayTiepNhan != new DateTime(1900, 1, 1))
                    this.txtNgayTiepNhan.Text = HSTL.NgayTiepNhan.ToString("dd/MM/yyyy");
                if (HSTL.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                {
                    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    Luu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    if (GlobalSettings.NGON_NGU == "0")
                        lblTrangThai.Text = "Đã duyệt";
                    else
                        lblTrangThai.Text = "Approved";
                    sendBK1.Enabled = false;
                    huyBangKe1.Enabled = false; ;
                }
                else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    sendBK1.Enabled = false;
                    huyBangKe1.Enabled = false;
                }
                else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    if (GlobalSettings.NGON_NGU == "0")
                        lblTrangThai.Text = "Chờ duyệt";
                    else
                        lblTrangThai.Text = "Wait for Approval";

                    KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    sendBK1.Enabled = true;
                    huyBangKe1.Enabled = true;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = "HSTL";
                sendXML.master_id = HSTL.ID;
                if (sendXML.Load())
                {
                    if (GlobalSettings.NGON_NGU == "0")
                        lblTrangThai.Text = "Đang chờ xác nhận hồ sơ ";
                    else
                        lblTrangThai.Text = "Waiting for confirmation";

                    KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    sendBK1.Enabled = false;
                    huyBangKe1.Enabled = false;
                }
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleThanhKhoan.ThanhKhoan)))
                {
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    uiCommandBar1.Visible = false;
                }
                /* Lock LanNT. Do not show
                if (this.IsMdiChild) cmdFixAndRun1.Visible = Janus.Windows.UI.InheritableBoolean.False;
                else cmdFixAndRun1.Visible = Janus.Windows.UI.InheritableBoolean.True;
                 */
                //HSTL.LoadChungTuTTs();
                setCommandStatus();

                LoadNgayKhaiBao();

                NewColumnTongSoToKhai();

                Janus.Windows.UI.InheritableBoolean state = Janus.Windows.UI.InheritableBoolean.False;
                //cmdBKToKhaiNhap_196.Visible = state;
                //cmdBKToKhaiXuat_196.Visible = state;
                //cmdBCNPLXuatNhapTon_196.Visible = state;
                cmdBCNPLXuatNhapTon_TongNPL_196.Visible = state;
                //cmdBCThueXNK_196.Visible = state;
                chkKhaiBaoBangKe.Checked = Company.KDT.SHARE.Components.Globals.KhaiBaoThanhKoanLoai2;
                if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                    cmdBangKe.Visible = Janus.Windows.UI.InheritableBoolean.True;
                else
                    cmdBangKe.Visible = Janus.Windows.UI.InheritableBoolean.False;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void NewColumnTongSoToKhai()
        {
            try
            {
                if (!dgList.RootTable.Columns.Contains("TongSoTK"))
                {
                    GridEXColumn colTongTK = new GridEXColumn();
                    colTongTK.Key = "TongSoTK";
                    colTongTK.BoundMode = ColumnBoundMode.Unbound;
                    colTongTK.DataTypeCode = TypeCode.Int32;
                    colTongTK.ColumnType = ColumnType.Text;
                    colTongTK.EditType = EditType.NoEdit;
                    colTongTK.Caption = "Tổng tờ khai";
                    colTongTK.HeaderAlignment = Janus.Windows.GridEX.TextAlignment.Center;
                    colTongTK.CellStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
                    dgList.RootTable.Columns.Add(colTongTK);
                }

                string maBK = "";
                for (int k = 0; k < dgList.RowCount; k++)
                {
                    maBK = dgList.GetRow(k).Cells["MaBangKe"].Text;
                    if (maBK != "")
                        dgList.GetRow(k).Cells["TongSoTK"].Text = TinhTongSoTK(maBK).ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private int TinhTongSoTK(string mabangKe)
        {
            int cnt = 0;
            try
            {
                int i = 0;
                switch (mabangKe)
                {
                    case "DTLTKN":
                        i = this.HSTL.getBKToKhaiNhap();
                        this.HSTL.BKCollection[i].LoadChiTietBangKe();
                        cnt = this.HSTL.BKCollection[i].bkTKNCollection.Count;
                        break;
                    case "DTLTKX":
                        i = this.HSTL.getBKToKhaiXuat();
                        this.HSTL.BKCollection[i].LoadChiTietBangKe();
                        cnt = this.HSTL.BKCollection[i].bkTKXColletion.Count;
                        break;
                    case "DTLNPLCHUATL":
                        i = this.HSTL.getBKNPLChuaThanhLY();
                        this.HSTL.BKCollection[i].LoadChiTietBangKe();
                        cnt = this.HSTL.BKCollection[i].bkNPLCTLCollection.Count;
                        break;
                    case "DTLNPLXH":
                        i = this.HSTL.getBKNPLXinHuy();
                        this.HSTL.BKCollection[i].LoadChiTietBangKe();
                        cnt = this.HSTL.BKCollection[i].bkNPLXHCollection.Count;
                        break;
                    case "DTLNPLTX":
                        i = this.HSTL.getBKNPLTaiXuat();
                        this.HSTL.BKCollection[i].LoadChiTietBangKe();
                        cnt = this.HSTL.BKCollection[i].bkNPLTXCollection.Count;
                        break;
                    case "DTLNPLNKD":
                        i = this.HSTL.getBKNPLNhapKinhDoanh();
                        this.HSTL.BKCollection[i].LoadChiTietBangKe();
                        cnt = this.HSTL.BKCollection[i].bkNPLNKDCollection.Count;
                        break;
                    case "DTLNPLNT":
                        i = this.HSTL.getBKNPLNopThue();
                        this.HSTL.BKCollection[i].LoadChiTietBangKe();
                        cnt = this.HSTL.BKCollection[i].bkNPLNTCollection.Count;
                        break;
                    case "DTLNPLXGC":
                        i = this.HSTL.getBKNPLXuatGiaCong();
                        this.HSTL.BKCollection[i].LoadChiTietBangKe();
                        cnt = this.HSTL.BKCollection[i].bkNPLXGCCollection.Count;
                        break;
                    case "DTLCHITIETNT":
                        i = this.HSTL.getBKNPLTamNopThue();
                        this.HSTL.BKCollection[i].LoadChiTietBangKe();
                        cnt = this.HSTL.BKCollection[i].bkNPLTNTCollection.Count;
                        break;
                    case "DTLNPLTCU":
                        i = this.HSTL.getBKNPLTuCungUng();
                        this.HSTL.BKCollection[i].LoadChiTietBangKe();
                        cnt = this.HSTL.BKCollection[i].bkNPLTCUCollection.Count;
                        break;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return cnt;
        }

        private void SetCombo()
        {
            cbbBangKe.Items.Clear();
            cbbBangKe.Items.Add("BK03-List of Unliquidated Raw Materials", (object)"DTLNPLCHUATL");
            cbbBangKe.Items.Add("BK04-List of Destroyed and Presented Raw Materials", (object)"DTLNPLXH");
            cbbBangKe.Items.Add("BK05-List of Re-Exported Raw Materials", (object)"DTLNPLTX");
            cbbBangKe.Items.Add("BK06-List of Raw Materials of Import Trade Customs Declaration Forms", (object)"DTLNPLNKD");
            cbbBangKe.Items.Add("BK07-List of Raw Materials Begged to Pay Domestic Consumption Tax", (object)"DTLNPLNT");
            cbbBangKe.Items.Add("BK08-List of Raw Materials Exported under Export Processing Type", (object)"DTLNPLXGC");
            cbbBangKe.Items.Add("BK09-List of Raw Materials Temporary Prepaid Tax", (object)"DTLCHITIETNT");
            cbbBangKe.Items.Add("BK10-List of Self-Supply Raw Materials", (object)"DTLNPLTCU");
            cbbBangKe.Items.Sort();
            cbbBangKe.SelectedIndex = 0;
        }
        private void refreshTrangThai()
        {

            if (this.HSTL.TrangThaiThanhKhoan == 0)
            {
                cmdRollback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdClose1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThanhLy1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXuatBaoCao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCanDoiNX1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFixAndRun1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                if (GlobalSettings.NGON_NGU == "0")
                    lblTrangThaiTK.Text = "Đang nhập liệu";
                else
                    lblTrangThaiTK.Text = "Inputing data";
                txtSoHSTL.ReadOnly = false;
                ccNgayThanhLy.ReadOnly = false;
                btnThemMoi.Enabled = uiButton2.Enabled = true;
                dgList.AllowDelete = InheritableBoolean.True;
            }
            else if (this.HSTL.TrangThaiThanhKhoan > 0 && this.HSTL.TrangThaiThanhKhoan < (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaChayThanhKhoan)
            {
                cmdRollback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdClose1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThanhLy1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXuatBaoCao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCanDoiNX1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFixAndRun1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                if (GlobalSettings.NGON_NGU == "0")
                    lblTrangThaiTK.Text = "Đang chạy thanh khoản";
                else
                    lblTrangThaiTK.Text = "Inputing data";


                txtSoHSTL.ReadOnly = false;
                ccNgayThanhLy.ReadOnly = false;
                btnThemMoi.Enabled = uiButton2.Enabled = true;
                dgList.AllowDelete = InheritableBoolean.True;
            }
            else if (this.HSTL.TrangThaiThanhKhoan == (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaChayThanhKhoan)
            {
                cmdRollback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdClose1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThanhLy1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdXuatBaoCao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCanDoiNX1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFixAndRun1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                if (GlobalSettings.NGON_NGU == "0")
                    lblTrangThaiTK.Text = "Đã chạy thanh khoản";
                else
                    lblTrangThaiTK.Text = "Liquidated";

                txtSoHSTL.ReadOnly = false;
                ccNgayThanhLy.ReadOnly = false;
                btnThemMoi.Enabled = uiButton2.Enabled = true;
                dgList.AllowDelete = InheritableBoolean.True;
            }
            else if (this.HSTL.TrangThaiThanhKhoan == (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo)
            {
                cmdRollback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdClose1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThanhLy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdXuatBaoCao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCanDoiNX1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFixAndRun1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                if (GlobalSettings.NGON_NGU == "0")
                    lblTrangThaiTK.Text = "Đã đóng hồ sơ";
                else
                    lblTrangThaiTK.Text = "Closed Liquidatation";


                txtSoHSTL.ReadOnly = true;
                ccNgayThanhLy.ReadOnly = true;
                btnThemMoi.Enabled = uiButton2.Enabled = false;
                dgList.AllowDelete = InheritableBoolean.False;
            }
            if (this.HSTL.LanThanhLy < this.HSTL.GetLanThanhLyMoiNhatByUserName(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN))
            {
                if (GlobalSettings.NGON_NGU == "0")
                    ThanhLy1.Text = "Chạy lại thanh khoản và đóng hồ sơ";
                else
                    ThanhLy1.Text = "Closed Liquidatation";

                cmdClose1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            if (this.HSTL.TrangThaiXuLy == 0 || this.HSTL.TrangThaiXuLy == 1)
            {
                cmdRollback1.Enabled = ThanhLy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }

        }
        private void bindComboboxDelete(string maBangKe)
        {
            switch (maBangKe)
            {
                case "DTLTKN":
                    break;
                case "DTLTKX":
                    break;
                case "DTLNPLCHUATL":
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK03 - Bảng kê nguyên phụ liệu chưa thanh lý", (object)"DTLNPLCHUATL");
                    else
                        cbbBangKe.Items.Add("BK03-List of Unliquidated Raw Materials", (object)"DTLNPLCHUATL");

                    break;
                case "DTLNPLXH":
                    //cbbBangKe.Items.Add("BK04 - Bảng kê nguyên phụ liệu hủy, biếu, tặng", (object)"DTLNPLXH");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK04 - Bảng kê nguyên phụ liệu hủy, biếu, tặng", (object)"DTLNPLXH");
                    else
                        cbbBangKe.Items.Add("BK04 - List of Destroyed and Presented Raw Materials", (object)"DTLNPLXH");
                    break;
                case "DTLNPLTX":
                    //cbbBangKe.Items.Add("BK05 - Bảng kê nguyên phụ liệu tái xuất", (object)"DTLNPLTX");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK05 - Bảng kê nguyên phụ liệu tái xuất", (object)"DTLNPLTX");
                    else
                        cbbBangKe.Items.Add("BK05 - BK05-List of Re-Exported Raw Materials", (object)"DTLNPLTX");
                    break;
                case "DTLNPLNKD":
                    // cbbBangKe.Items.Add("BK06 - Bảng kê nguyên phụ liệu sử dụng tờ khai NKD", (object)"DTLNPLNKD");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK06 - Bảng kê nguyên phụ liệu sử dụng tờ khai NKD", (object)"DTLNPLNKD");
                    else
                        cbbBangKe.Items.Add("BK06 - List of Raw Materials of Import Trade Customs Declaration Forms", (object)"DTLNPLNKD");
                    break;
                case "DTLNPLNT":
                    //cbbBangKe.Items.Add("BK07 - Bảng kê nguyên phụ liệu nộp thuế tiêu thụ nội địa", (object)"DTLNPLNT");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK07 - Bảng kê nguyên phụ liệu nộp thuế tiêu thụ nội địa", (object)"DTLNPLNT");
                    else
                        cbbBangKe.Items.Add("BK07 - List of Raw Materials Begged to Pay Domestic Consumption Tax", (object)"DTLNPLNT");
                    break;
                case "DTLNPLXGC":
                    //cbbBangKe.Items.Add("BK08 - Bảng kê nguyên phụ liệu xuất theo loại hình XGC", (object)"DTLNPLXGC");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK08 - Bảng kê nguyên phụ liệu xuất theo loại hình XGC", (object)"DTLNPLXGC");
                    else
                        cbbBangKe.Items.Add("BK08 - List of Raw Materials Exported under Export Processing Type", (object)"DTLNPLXGC");
                    break;
                case "DTLCHITIETNT":
                    //cbbBangKe.Items.Add("BK09 - Bảng kê nguyên phụ liệu tạm nộp thuế", (object)"DTLCHITIETNT");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK09 - Bảng kê nguyên phụ liệu tạm nộp thuế", (object)"DTLCHITIETNT");
                    else
                        cbbBangKe.Items.Add("BK09 - List of Raw Materials Temporary Prepaid Tax", (object)"DTLCHITIETNT");
                    break;
                default:
                    //cbbBangKe.Items.Add("BK10 - Bảng kê nguyên phụ liệu tự cung ứng", (object)"DTLNPLTCU");
                    if (GlobalSettings.NGON_NGU == "0")
                        cbbBangKe.Items.Add("BK10 - Bảng kê nguyên phụ liệu tự cung ứng", (object)"DTLNPLTCU");
                    else
                        cbbBangKe.Items.Add("BK10 - List of Self-Supply Raw Materials", (object)"DTLNPLTCU");
                    break;
            }
            cbbBangKe.Items.Sort();
            cbbBangKe.SelectedValue = cbbBangKe.Items[0].Value;

            CheckSoLuongChungTu();
        }
        private void bindCombobox()
        {
            foreach (BangKeHoSoThanhLy bk in this.HSTL.BKCollection)
            {
                switch (bk.MaBangKe)
                {
                    case "DTLTKN":
                        if (cbbBangKe.Items[(object)"DTLTKN"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLTKN"]);
                        break;
                    case "DTLTKX":
                        if (cbbBangKe.Items[(object)"DTLTKX"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLTKX"]);
                        break;
                    case "DTLNPLCHUATL":
                        if (cbbBangKe.Items[(object)"DTLNPLCHUATL"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLCHUATL"]);
                        break;
                    case "DTLNPLXH":
                        if (cbbBangKe.Items[(object)"DTLNPLXH"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLXH"]);
                        break;
                    case "DTLNPLTX":
                        if (cbbBangKe.Items[(object)"DTLNPLTX"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLTX"]);
                        break;
                    case "DTLNPLNKD":
                        if (cbbBangKe.Items[(object)"DTLNPLNKD"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLNKD"]);
                        break;
                    case "DTLNPLNT":
                        if (cbbBangKe.Items[(object)"DTLNPLNT"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLNT"]);
                        break;
                    case "DTLNPLXGC":
                        if (cbbBangKe.Items[(object)"DTLNPLXGC"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLXGC"]);
                        break;
                    case "DTLCHITIETNT":
                        if (cbbBangKe.Items[(object)"DTLCHITIETNT"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLCHITIETNT"]);
                        break;
                    default:
                        if (cbbBangKe.Items[(object)"DTLNPLTCU"] != null)
                            cbbBangKe.Items.Remove(cbbBangKe.Items[(object)"DTLNPLTCU"]);
                        break;
                }

            }
            cbbBangKe.Items.Sort();
            cbbBangKe.SelectedValue = cbbBangKe.Items[0].Value;

            CheckSoLuongChungTu();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                switch (e.Row.Cells["MaBangKe"].Text)
                {
                    case "DTLTKN":
                        BK01Form bk01 = new BK01Form();
                        int i = this.HSTL.getBKToKhaiNhap();
                        this.HSTL.BKCollection[i].LoadChiTietBangKe();
                        bk01.bkTKNCollection = this.HSTL.BKCollection[i].bkTKNCollection;
                        bk01.HSTL = this.HSTL;
                        bk01.ShowDialog(this);
                        break;
                    case "DTLTKX":
                        BK02Form bk02 = new BK02Form();
                        this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].LoadChiTietBangKe();
                        bk02.bkTKXCollection = this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].bkTKXColletion;
                        bk02.HSTL = this.HSTL;
                        bk02.ShowDialog(this);
                        break;
                    case "DTLNPLCHUATL":
                        BK03Form bk03 = new BK03Form();
                        this.HSTL.BKCollection[this.HSTL.getBKNPLChuaThanhLY()].LoadChiTietBangKe();
                        bk03.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLChuaThanhLY()].bkNPLCTLCollection;
                        bk03.HSTL = this.HSTL;
                        bk03.ShowDialog(this);
                        break;
                    case "DTLNPLXH":
                        BK04Form bk04 = new BK04Form();
                        this.HSTL.BKCollection[this.HSTL.getBKNPLXinHuy()].LoadChiTietBangKe();
                        bk04.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLXinHuy()].bkNPLXHCollection;
                        bk04.HSTL = this.HSTL;
                        bk04.ShowDialog(this);
                        break;
                    case "DTLNPLTX":
                        BK05Form bk05 = new BK05Form();
                        this.HSTL.BKCollection[this.HSTL.getBKNPLTaiXuat()].LoadChiTietBangKe();
                        bk05.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLTaiXuat()].bkNPLTXCollection;
                        bk05.HSTL = this.HSTL;
                        bk05.ShowDialog(this);
                        break;
                    case "DTLNPLNKD":
                        BK06Form bk06 = new BK06Form();
                        this.HSTL.BKCollection[this.HSTL.getBKNPLNhapKinhDoanh()].LoadChiTietBangKe();
                        bk06.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLNhapKinhDoanh()].bkNPLNKDCollection;
                        bk06.HSTL = this.HSTL;
                        bk06.ShowDialog(this);
                        break;
                    case "DTLNPLNT":
                        BK07Form bk07 = new BK07Form();
                        this.HSTL.BKCollection[this.HSTL.getBKNPLNopThue()].LoadChiTietBangKe();
                        bk07.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLNopThue()].bkNPLNTCollection;
                        bk07.HSTL = this.HSTL;
                        bk07.ShowDialog(this);
                        break;
                    case "DTLNPLXGC":
                        BK08Form bk08 = new BK08Form();
                        this.HSTL.BKCollection[this.HSTL.getBKNPLXuatGiaCong()].LoadChiTietBangKe();
                        bk08.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLXuatGiaCong()].bkNPLXGCCollection;
                        bk08.HSTL = this.HSTL;
                        bk08.ShowDialog(this);
                        break;
                    case "DTLCHITIETNT":
                        BK09Form bk09 = new BK09Form();
                        this.HSTL.BKCollection[this.HSTL.getBKNPLTamNopThue()].LoadChiTietBangKe();
                        bk09.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLTamNopThue()].bkNPLTNTCollection;
                        bk09.HSTL = this.HSTL;
                        bk09.ShowDialog(this);
                        break;
                    default:
                        BK10_NEWForm bk10 = new BK10_NEWForm();
                        this.HSTL.BKCollection[this.HSTL.getBKNPLTuCungUng()].LoadChiTietBangKe();
                        bk10.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLTuCungUng()].bkNPLTCUCollection;
                        bk10.HSTL = this.HSTL;
                        bk10.ShowDialog(this);
                        //BK10Form bk10 = new BK10Form();
                        //this.HSTL.BKCollection[this.HSTL.getBKNPLTuCungUng()].LoadChiTietBangKe();
                        //bk10.bkCollection = this.HSTL.BKCollection[this.HSTL.getBKNPLTuCungUng()].bkNPLTCUCollection;
                        //bk10.HSTL = this.HSTL;
                        //bk10.ShowDialog(this);
                        break;
                }
                dgList.Refetch();
                NewColumnTongSoToKhai();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            switch (e.Row.Cells["MaBangKe"].Text)
            {
                case "DTLTKN":
                    //e.Row.Cells["TenBangKe"].Text = "BK01 - Bảng kê tờ khai nhập khẩu thanh lý";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK01 - Bảng kê tờ khai nhập khẩu thanh lý";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK01 - List of Unliquidated  import declaration materials";
                    break;
                case "DTLTKX":
                    //e.Row.Cells["TenBangKe"].Text = "BK02 - Bảng kê tờ khai xuất khẩu thanh lý";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK02 - Bảng kê tờ khai xuất khẩu thanh lý";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK02 - List of Unliquidated  export declaration materials";
                    break;
                case "DTLNPLCHUATL":
                    //e.Row.Cells["TenBangKe"].Text = "BK03 - Bảng kê nguyên phụ liệu chưa thanh lý";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK03 - Bảng kê nguyên phụ liệu chưa thanh lý";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK03 - List of Unliquidated Raw Materials";
                    break;
                case "DTLNPLXH":
                    //e.Row.Cells["TenBangKe"].Text = "BK04 - Bảng kê nguyên phụ liệu hủy, biếu, tặng";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK04 - Bảng kê nguyên phụ liệu hủy, biếu, tặng";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK04 - List of Destroyed and Presented Raw Materials";
                    break;
                case "DTLNPLTX":
                    //e.Row.Cells["TenBangKe"].Text = "BK05 - Bảng kê nguyên phụ liệu tái xuất";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK05 - Bảng kê nguyên phụ liệu tái xuất";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK05 - List of Re-Exported Raw Materials";
                    break;
                case "DTLNPLNKD":
                    // e.Row.Cells["TenBangKe"].Text = "BK06 - Bảng kê nguyên phụ liệu xuất sử dụng tờ khai NKD";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK06 - Bảng kê nguyên phụ liệu xuất sử dụng tờ khai NKD";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK06 - List of Raw Materials of Import Trade Customs Declaration Forms";
                    break;
                case "DTLNPLNT":
                    //e.Row.Cells["TenBangKe"].Text = "BK07 - Bảng kê nguyên phụ liệu nộp thuế tiêu thụ nội địa";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK07 - Bảng kê nguyên phụ liệu nộp thuế tiêu thụ nội địa";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK07 - List of Raw Materials Begged to Pay Domestic Consumption Tax";
                    break;
                case "DTLNPLXGC":
                    //e.Row.Cells["TenBangKe"].Text = "BK08 - Bảng kê nguyên phụ liệu xuất theo loại hình XGC";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK08 - Bảng kê nguyên phụ liệu xuất theo loại hình XGC";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK08 - List of Raw Materials Exported under Export Processing Type";
                    break;
                case "DTLCHITIETNT":
                    //e.Row.Cells["TenBangKe"].Text = "BK09 - Bảng kê nguyên phụ liệu tạm nộp thuế";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK09 - Bảng kê nguyên phụ liệu tạm nộp thuế";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK09 - List of Raw Materials Temporary Prepaid Tax";
                    break;
                default:
                    //e.Row.Cells["TenBangKe"].Text = "BK10 - Bảng kê nguyên phụ liệu tự cung ứng";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TenBangKe"].Text = "BK10 - Bảng kê nguyên phụ liệu tự cung ứng";
                    else
                        e.Row.Cells["TenBangKe"].Text = "BK10 - List of Self-Supply Raw Materials";
                    break;
            }
            switch (e.Row.Cells["TrangThaiXL"].Value.ToString())
            {
                case "1":
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TrangThaiXL"].Text = "Đã khai báo";
                    else
                        e.Row.Cells["TrangThaiXL"].Text = "Declarated";

                    break;
                case "-1":
                    // e.Row.Cells["TrangThaiXL"].Text = "Chưa khai báo";
                    if (GlobalSettings.NGON_NGU == "0")
                        e.Row.Cells["TrangThaiXL"].Text = "Chưa khai báo";
                    else
                        e.Row.Cells["TrangThaiXL"].Text = "Not declarated yet";
                    break;
            }
        }

        private void btnThemMoi_Click(object sender, EventArgs e)
        {
            switch (cbbBangKe.SelectedValue.ToString())
            {
                case "DTLTKN":
                    break;
                case "DTLTKX":
                    break;
                case "DTLNPLCHUATL":
                    BK03Form bk03 = new BK03Form();
                    bk03.HSTL = this.HSTL;
                    bk03.ShowDialog(this);
                    this.HSTL = bk03.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLNPLXH":
                    BK04Form bk04 = new BK04Form();
                    bk04.HSTL = this.HSTL;
                    bk04.ShowDialog(this);
                    this.HSTL = bk04.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLNPLTX":
                    BK05Form bk05 = new BK05Form();
                    bk05.HSTL = this.HSTL;
                    bk05.ShowDialog(this);
                    this.HSTL = bk05.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLNPLNKD":
                    BK06Form bk06 = new BK06Form();
                    bk06.HSTL = this.HSTL;
                    bk06.ShowDialog(this);
                    this.HSTL = bk06.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLNPLNT":
                    BK07Form bk07 = new BK07Form();
                    bk07.HSTL = this.HSTL;
                    bk07.ShowDialog(this);
                    this.HSTL = bk07.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLNPLXGC":
                    BK08Form bk08 = new BK08Form();
                    bk08.HSTL = this.HSTL;
                    bk08.ShowDialog(this);
                    this.HSTL = bk08.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLCHITIETNT":
                    BK09Form bk09 = new BK09Form();
                    bk09.HSTL = this.HSTL;
                    bk09.ShowDialog(this);
                    this.HSTL = bk09.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    break;
                case "DTLNPLTCU":
                    BK10_NEWForm bk10 = new BK10_NEWForm();
                    bk10.HSTL = this.HSTL;
                    bk10.ShowDialog(this);
                    this.HSTL = bk10.HSTL;
                    dgList.DataSource = this.HSTL.BKCollection;
                    dgList.Refetch();
                    bindCombobox();
                    //BKNPLCungUngForm bkNPL = new BKNPLCungUngForm();
                    //bkNPL.HSTL = this.HSTL;
                    //bkNPL.ShowDialog(this);
                    //this.HSTL = bkNPL.HSTL;
                    //dgList.DataSource = this.HSTL.BKCollection;
                    //dgList.Refetch();
                    //bindCombobox();
                    break;
                //default:
                //    BK10Form bk10 = new BK10Form();
                //    bk10.HSTL = this.HSTL;
                //    bk10.ShowDialog(this);
                //    this.HSTL = bk10.HSTL;
                //    dgList.DataSource = this.HSTL.BKCollection;
                //    dgList.Refetch();
                //    bindCombobox();
                //    break;
            }

            NewColumnTongSoToKhai();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            //Nhận dữ liệu HSTL từ hải quan
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {

            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["MaBangKe"].Text == "DTLTKN" || e.Row.Cells["MaBangKe"].Text == "DTLTKX")
                {
                    ShowMessage(setText("Bảng kê BK01 và BK02 không xóa được.", "BK01,BK02 list can not delete?"), false);
                    e.Cancel = true;
                    return;
                }
                else
                {
                    if (ShowMessage(setText("Bạn có muốn xóa bảng kê này không?", "Do you want to delete this list ?"), true) != "Yes")
                    // if (MLMessages("Bạn có muốn xóa bảng kê này không?", "MSG_DEL01", "", true) != "Yes")
                    {
                        e.Cancel = true;
                        return;
                    }
                    BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                    bk.ID = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    bk.Delete();
                    bindComboboxDelete(e.Row.Cells["MaBangKe"].Text);

                }
            }
        }
        private void refreshSTTHang()
        {
            for (int i = 0; i < this.HSTL.BKCollection.Count; i++)
            {
                this.HSTL.BKCollection[i].STTHang = i + 1;
                this.HSTL.BKCollection[i].SoHSTL = Convert.ToInt32(this.HSTL.SoHoSo);
                this.HSTL.BKCollection[i].TrangThaiXL = Convert.ToInt32(this.HSTL.TrangThaiXuLy);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ccNgayThanhLy.Text))
            {
                MLMessages("Ngày bắt đầu không được để trống.", "MSG_SAV02", "", false);
                ccNgayThanhLy.Focus();
                return;
            }

            this.HSTL.NgayBatDau = ccNgayThanhLy.Value;
            this.HSTL.SoHoSo = Convert.ToInt32(txtSoHSTL.Value);
            if (HSTL.NgayTiepNhan.Year < 1900) HSTL.NgayTiepNhan = new DateTime(1900, 1, 1);
            if (HSTL.NgayKetThuc.Year < 1900) HSTL.NgayKetThuc = new DateTime(1900, 1, 1);
            if (HSTL.NgayQuyetDinh.Year < 1900) HSTL.NgayQuyetDinh = new DateTime(1900, 1, 1);
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                if (txtQuy.Text == "0" || txtNam.Text == "")
                {
                    ShowMessage("Nhập quý và năm quyết toán hồ sơ.", false);
                    return;
                }
                this.HSTL.SoQuyetDinh = txtQuy.Text + "/" + txtNam.Text;
            }
            else
            {
                this.HSTL.SoQuyetDinh = txtSoQuyetDinh.Text.Trim();
            }

            this.HSTL.Update();

            //Neu chua co du lieu HSTK Bo sung -> them moi
            if (this.HSTL.TrangThaiThanhKhoan != 401)
            {
                if (hstkBS == null || (hstkBS != null && hstkBS.Count == 0))
                    Company.KDT.SHARE.Components.Common.HSTKBoSung.InsertHSTKBoSung(HSTL.ID, new DateTime(1900, 1, 1), HSTL.ChungTuTTs.Count, "");
                //Cap nhat HSTK Bo sung 
                else
                {
                    hstkBS[0].SoLuongChungTuKemTheo = HSTL.ChungTuTTs.Count;

                    Company.KDT.SHARE.Components.Common.HSTKBoSung.UpdateCollection(hstkBS);
                }

                refreshSTTHang();
                bool r = new BangKeHoSoThanhLy().InsertUpdate(this.HSTL.BKCollection);
                if (r)
                    //ShowMessage("Cập nhật hồ sơ thanh lý thành công.", false);
                    MLMessages("Lưu hồ sơ thanh lý thành công.", "MSG_SAV02", "", false);
                else
                    //ShowMessage("Cập nhật không thành công.", false);
                    MLMessages("Lưu hồ sơ thanh lý không thành công.", "MSG_SAV01", "", false);

                CheckSoLuongChungTu();
            }
            else
                MLMessages("Lưu hồ sơ thanh lý thành công.", "MSG_SAV02", "", false);
        }
        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "HSTL";
                sendXML.master_id = HSTL.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = HSTL.LayPhanHoi(pass, sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    //ShowMessage("Chưa có phản hồi từ hải quan.", false);
                    MLMessages("Chưa có phản hồi từ hải quan.", "MSG_THK88", "", false);

                    return;
                }
                if (sendXML.func == 1)
                {
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + HSTL.SoTiepNhan, false);
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + HSTL.SoTiepNhan, "MSG_THK20", "" + HSTL.SoTiepNhan, false);
                    txtSoTiepNhan.Text = HSTL.SoTiepNhan.ToString();
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ duyệt";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait for approval";
                    }
                }
                else if (sendXML.func == 3)
                {
                    //ShowMessage("Hủy khai báo thành công", false);
                    MLMessages("Hủy khai báo thành công.", "MSG_THK21", "", false);
                    txtSoTiepNhan.Text = "0";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chưa khai báo";
                    }
                    else
                    {
                        lblTrangThai.Text = "Not yet declarated";
                    }

                }
                //else if (sendXML.func == 2)
                //{
                //    if (HSTL.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                //    {
                //        this.ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                //    }
                //    else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                //    {
                //        this.ShowMessage("Hải quan chưa duyệt danh sách này", false);
                //    }
                //    else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                //    {
                //        this.ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                //    }
                //}
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Có lỗi do hệ thống phía hải quan.", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = HSTL.ID;
                                //hd.LoaiToKhai = LoaiToKhai.HO_SO_THANH_KHOAN;
                                //hd.TrangThai = HSTL.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                //hd.PassWord = pass;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            if (sendXML.func == 1)
                            {
                                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            }
                            else if (sendXML.func == 3)
                            {
                                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            }

                        }
                    }
                    else
                    {
                        ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void cmdSend_Click(object sender, EventArgs e)
        {
            try
            {

                //ThanhKhoanSendForm f = new ThanhKhoanSendForm();
                //f.hosothanhly = this.HSTL;
                //f.ShowDialog();
                //return;

                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ThanhKhoan;
                sendXML.master_id = HSTL.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    return;
                }

                // HSTL.BKCollection[HSTL.getBKNPLNhapKinhDoanh()].LoadChiTietBangKe();
                HSTL.GuidStr = Guid.NewGuid().ToString();
                ObjectSend msgSend = new ObjectSend(
                    new NameBase() { Identity = HSTL.MaDoanhNghiep.Trim(), Name = GlobalSettings.TEN_DON_VI },
                    new NameBase() { Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(HSTL.MaHaiQuanTiepNhan.Trim()).Trim() : HSTL.MaHaiQuanTiepNhan.Trim(), Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HSTL.MaHaiQuanTiepNhan.Trim()) },
                     new SubjectBase()
                     {
                         Type = Company.KDT.SHARE.Components.Globals.LaDNCX ? DeclarationIssuer.HO_SO_THANH_KHOAN_KCX : DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                         Function = DeclarationFunction.KHAI_BAO,
                         Reference = HSTL.GuidStr,
                     },
                    // GlobalSettings.SendV4 ? Mapper_V4.ToDataTransferSXXK_HoSoThanhKhoan(HSTL, GlobalSettings.TEN_DON_VI) : Mapper.ToDataTransferSXXK_HoSoThanhKhoan(HSTL, GlobalSettings.TEN_DON_VI));
                    Mapper_V4.ToDataTransferSXXK_HoSoThanhKhoan(HSTL, GlobalSettings.TEN_DON_VI));
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);
                if (isSend)
                {
                    HSTL.InsertUpdate();
                    //TODO: Hungtq updated 13/03/1023. Lưu ngày khai báo
                    //Neu chua co du lieu HSTK Bo sung -> them moi
                    if (hstkBS == null || (hstkBS != null && hstkBS.Count == 0))
                        Company.KDT.SHARE.Components.Common.HSTKBoSung.InsertHSTKBoSung(HSTL.ID, System.DateTime.Now, HSTL.ChungTuTTs.Count, "");
                    //Cap nhat HSTK Bo sung 
                    else
                    {
                        hstkBS[0].NgayKhaiBao = System.DateTime.Now;
                        hstkBS[0].SoLuongChungTuKemTheo = HSTL.ChungTuTTs.Count;

                        Company.KDT.SHARE.Components.Common.HSTKBoSung.UpdateCollection(hstkBS);
                    }
                    LoadNgayKhaiBao();

                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    sendXML.InsertUpdate();
                    sendMessageForm.Message.XmlSaveMessage(HSTL.ID, "Khai báo");
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void LayPhanHoiBangKe(string pass)
        {
            MsgSend sendXML = new MsgSend();
            int i = 0;
            this.Cursor = Cursors.WaitCursor;
            string st = "";
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                ++i;
                try
                {
                    sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                    switch (sendXML.LoaiHS)
                    {
                        case "DTLTKN":
                            st = "Danh sách tờ khai nhập";
                            break;
                        case "DTLTKX":
                            st = "Danh sách tờ khai xuất";
                            break;
                        case "DTLNPLCHUATL":
                            st = "Danh sách nguyên phụ liệu chưa thanh lý";
                            break;
                        case "DTLNPLXH":
                            break;
                        case "DTLNPLTX":
                            st = "Danh sách nguyên phụ liệu tái xuất";
                            break;
                        case "DTLNPLNKD":
                            st = "Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh";
                            break;
                        case "DTLNPLNT":
                            st = "Danh sách nguyên phụ liệu nộp thuế";
                            break;
                        case "DTLNPLXGC":
                            st = "Danh sách nguyên phụ liệu xuất sang tờ khai gia công";
                            break;
                        case "DTLCHITIETNT":
                            st = "Danh sách nguyên phụ liệu tạm nộp thuế";
                            break;
                    }
                    sendXML.master_id = HSTL.ID;
                    if (!sendXML.Load())
                    {
                        continue;
                    }
                    xmlCurrent = HSTL.LayPhanHoiBangKe(pass, sendXML.msg);
                    if (xmlCurrent != "")
                        --i;
                    else
                        sendXML.Delete();
                }
                catch (Exception ex)
                {
                    --i;
                    this.Cursor = Cursors.Default;
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                if (ShowMessage("Khai báo không thành công " + st + ".Có lỗi do hệ thống phía hải quan.\nBạn có muốn gửi tiếp những bảng kê còn lại không?", true) == "Yes")
                                {
                                }
                                else
                                    break;
                            }
                            else
                            {
                                ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                                sendXML.Delete();
                            }
                        }
                        else
                        {

                            ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                        }
                        #endregion FPTService
                    }
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo bảng kê hồ sơ thanh lý " + st + ". Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                }
            }
            if (i > 0)
                //  ShowMessage("Thực hiện thành công " + i.ToString() + " bảng kê", false);
                MLMessages("Thực hiện thành công " + i.ToString() + " bảng kê", "MSG_THK23", "", false);
            else
                //  ShowMessage("Chưa có phản hồi từ hải quan", false);
                MLMessages("Chưa có phản hồi từ hải quan.", "MSG_THK88", "", false);
            this.HSTL.LoadBKCollection();
            dgList.DataSource = HSTL.BKCollection;
            this.Cursor = Cursors.Default;
        }
        private void LayPhanHoiBangKe()
        {
            MsgSend sendXML = new MsgSend();
            if (dgList.GetCheckedRows().Length == 0)
            {
                // ShowMessage("Chưa chọn bảng kê để gửi.", false);
                MLMessages("Chưa chọn bảng kê để gửi.", "MSG_THK57", "", false);
                return;
            }
            if (HSTL.SoTiepNhan == 0)
            {
                // ShowMessage("Chưa đăng ký số hồ sơ thanh lý.", false);
                MLMessages("Chưa đăng ký số hồ sơ thanh lý.", "MSG_THK58", "", false);
                return;
            }
            //Kiểm tra xem có bảng kê nào đã gửi nhưng chưa có phản hồi

            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            this.Cursor = Cursors.WaitCursor;
            int i = 0;
            string st = "";
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                ++i;
                try
                {
                    sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                    switch (sendXML.LoaiHS)
                    {
                        case "DTLTKN":
                            st = "Danh sách tờ khai nhập";
                            break;
                        case "DTLTKX":
                            st = "Danh sách tờ khai xuất";
                            break;
                        case "DTLNPLCHUATL":
                            st = "Danh sách nguyên phụ liệu chưa thanh lý";
                            break;
                        case "DTLNPLXH":
                            break;
                        case "DTLNPLTX":
                            st = "Danh sách nguyên phụ liệu tái xuất";
                            break;
                        case "DTLNPLNKD":
                            st = "Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh";
                            break;
                        case "DTLNPLNT":
                            st = "Danh sách nguyên phụ liệu nộp thuế";
                            break;
                        case "DTLNPLXGC":
                            st = "Danh sách nguyên phụ liệu xuất sang tờ khai gia công";
                            break;
                        case "DTLCHITIETNT":
                            st = "Danh sách nguyên phụ liệu tạm nộp thuế";
                            break;
                    }
                    sendXML.master_id = HSTL.ID;
                    if (!sendXML.Load())
                    {
                        continue;
                    }
                    xmlCurrent = HSTL.LayPhanHoiBangKe(wsForm.txtMatKhau.Text, sendXML.msg);
                    if (xmlCurrent != "")
                        --i;
                    else
                        sendXML.Delete();
                }
                catch (Exception ex)
                {
                    --i;
                    this.Cursor = Cursors.Default;
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                if (ShowMessage("Khai báo không thành công " + st + ".Có lỗi do hệ thống phía hải quan.\nBạn có muốn gửi tiếp những bảng kê còn lại không?", true) == "Yes")
                                {
                                }
                                else
                                    break;
                            }
                            else
                            {
                                ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                                sendXML.Delete();
                            }
                        }
                        else
                        {

                            ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                        }
                        #endregion FPTService
                    }
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo bảng kê hồ sơ thanh lý " + st + ". Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                }
            }
            if (i > 0)
                // ShowMessage("Thực hiện thành công " + i.ToString() + " bảng kê", false);
                MLMessages("Thực hiện thành công " + i.ToString() + " bảng kê", "MSG_THK23", "", false);
            else
                // ShowMessage("Chưa có phản hồi từ hải quan", false);
                MLMessages("Chưa có phản hồi từ hải quan.", "MSG_THK88", "", false);
            this.HSTL.LoadBKCollection();
            dgList.DataSource = HSTL.BKCollection;
            this.Cursor = Cursors.Default;
        }
        private void LayPhanHoiMotBangKe(string mabangke, string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = mabangke;
                sendXML.master_id = HSTL.ID;
                if (!sendXML.Load())
                {
                    // ShowMessage("Bảng kê này chưa gửi thông tin tới hải quan.", false);
                    MLMessages("Bảng kê này chưa gửi thông tin tới hải quan.", "MSG_THK89", "", false);
                    return;
                }
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = HSTL.LayPhanHoiBangKe(pass, sendXML.msg);
                if (xmlCurrent != "")
                {
                    //ShowMessage("Chưa có phản hồi từ hải quan.", false);
                    MLMessages("Chưa có phản hồi từ hải quan.", "MSG_THK88", "", false);
                    return;
                }
                if (sendXML.func == 1)
                {
                    //ShowMessage("Bảng kê đã được khai báo.", false);        
                    MLMessages("Bảng kê đã được khai báo.", "MSG_THK90", "", false);
                }
                else if (sendXML.func == 3)
                {
                    //ShowMessage("Hủy bảng kê thành công", false);
                    MLMessages("Hủy bảng kê thành công", "MSG_THK62", "", false);
                }
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Khai báo không thành công.\nCó lỗi do hệ thống phía hải quan.\nBạn có muốn gửi tiếp những bảng kê còn lại không?", true) == "Yes")
                            {
                            }
                            else
                                return;
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                        }
                    }
                    else
                    {

                        ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            this.HSTL.LoadBKCollection();
            dgList.DataSource = HSTL.BKCollection;
            this.Cursor = Cursors.Default;
        }
        private void LayPhanHoiHoSo()
        {
            MsgSend sendXML = new MsgSend();
            WSForm wsForm = new WSForm();
            try
            {
                sendXML.LoaiHS = "HSTL";
                sendXML.master_id = HSTL.ID;
                if (!sendXML.Load())
                {
                    // ShowMessage("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                    MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_SEN03", "", false);
                    return;
                }
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = HSTL.LayPhanHoi("wsForm.txtMatKhau.Text.Trim()", sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    ShowMessage("Chưa có phản hồi từ hải quan.", false);
                    return;
                }
                if (sendXML.func == 1)
                {
                    ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + HSTL.SoTiepNhan, false);
                    txtSoTiepNhan.Text = HSTL.SoTiepNhan.ToString();
                    lblTrangThai.Text = "Chờ duyệt";
                }
                else if (sendXML.func == 3)
                {
                    ShowMessage("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "0";
                    lblTrangThai.Text = "Chưa khai báo";

                }
                else if (sendXML.func == 2)
                {
                    if (HSTL.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        this.ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                    }
                    else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        this.ShowMessage("Hải quan chưa duyệt danh sách này", false);
                    }
                    else if (HSTL.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        this.ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                    }
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
                this.HSTL.LoadBKCollection();
                dgList.DataSource = HSTL.BKCollection;
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.", true) == "Yes")
                            {
                                //HangDoi hd = new HangDoi();
                                //hd.ID = HSTL.ID;
                                //hd.LoaiToKhai = LoaiToKhai.HO_SO_THANH_KHOAN;
                                //hd.TrangThai = HSTL.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                //hd.PassWord = "wsForm.txtMatKhau.Text.Trim()";
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            if (sendXML.func == 1)
                            {
                                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            }
                            else if (sendXML.func == 3)
                            {
                                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            }
                        }
                    }
                    else
                    {
                        ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách NPL. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        string msgInfor = string.Empty;
        private void XacNhanThongTin()
        {

            ObjectSend msgSend = new ObjectSend(
             new NameBase()
             {
                 Identity = HSTL.MaDoanhNghiep.Trim(),
                 Name = GlobalSettings.TEN_DON_VI
             },
             new NameBase()
             {
                 Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(HSTL.MaHaiQuanTiepNhan.Trim()).Trim() : HSTL.MaHaiQuanTiepNhan.Trim(),
                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HSTL.MaHaiQuanTiepNhan.Trim())
             },
              new SubjectBase()
              {
                  Issuer = DeclarationIssuer.HO_SO_THANH_KHOAN,
                  Reference = HSTL.GuidStr,
                  Type = DeclarationIssuer.HO_SO_THANH_KHOAN,
                  Function = DeclarationFunction.HOI_TRANG_THAI,
                  //DeclarationStatus = "99"
              },
              null);
            // 
            //             SendMessageForm sendMessageForm = new SendMessageForm();
            //             sendMessageForm.Send += SendMessage;
            //             sendMessageForm.DoSend(msgSend);

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                /*  ObjectSend msgSend = SingleMessage.FeedBackMessageV3(TKMD);*/
                msgInfor = string.Empty;
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.DUYET_LUONG_CHUNG_TU && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) setCommandStatus();
                }
            }

        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                switch (e.Command.Key)
                {
                    case "Huy":
                        this.HuyHoSoKhaiBao();
                        break;
                    case "XacNhan":
                        this.XacNhanThongTin();
                        break;
                    case "NhanDuLieu":
                        this.XacNhanThongTin();
                        break;
                    case "KhaiBao":
                        this.cmdSend_Click(null, null);
                        break;
                    case "Luu":
                        this.btnSave_Click(null, null);
                        break;
                    case "ThanhLy":
                        if (HSTL.HSTL_TheoMaHang)
                        {
                            MessageBox.Show("Bạn đang thanh lý theo PO", "Thanh Khoản", MessageBoxButtons.OK, MessageBoxIcon.Error); ;
                            break;
                        }
                        //if (MainForm.versionHD != 2)
                        //{
                        //    try
                        //    {
                        //        string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                        //        if (st != "")
                        //        {
                        //            ShowMessage(st, false);
                        //            this.Cursor = Cursors.Default;
                        //            return;
                        //        }

                        //    }
                        //    catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
                        //}
                        ////this.HSTL = HoSoThanhLyDangKy.Load(this.HSTL.ID);
                        refreshTrangThai();
                        if (this.HSTL.TrangThaiThanhKhoan == (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo)
                        {
                            ShowMessage("Hồ sơ đã đóng, bạn không thể chạy thanh khoản.", false);
                            break;
                        }
                        this.HSTL.isDinhMucChuaDangKy = ShowMessage("Doanh nghiệp có muốn Chạy thanh khoản với những định mức đã nhập liệu nhưng chưa khai báo đến HQ không ?", true) == "Yes";
                        this.ThanhLy(false);
                        break;
                    case "cmdThanhKhoan":
                        if (HSTL.HSTL_TheoMaHang)
                        {
                            MessageBox.Show("Bạn đang thanh lý theo PO", "Thanh Khoản", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
                            this.ThanhLy(true);
                            break;
                        }
                        if (MainForm.versionHD != 2)
                        {
                            try
                            {
                                string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                                if (st != "")
                                {
                                    ShowMessage(st, false);
                                    this.Cursor = Cursors.Default;
                                    return;
                                }

                            }
                            catch { }
                        }
                        this.HSTL = HoSoThanhLyDangKy.Load(this.HSTL.ID);
                        refreshTrangThai();
                        if (this.HSTL.TrangThaiThanhKhoan == (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo)
                        {
                            ShowMessage("Hồ sơ đã đóng, bạn không thể chạy thanh khoản.", false);
                            break;
                        }
                        this.HSTL.isDinhMucChuaDangKy = ShowMessage("Doanh nghiệp có muốn Chạy thanh khoản với những định mức đã nhập liệu nhưng chưa khai báo đến HQ không ?", true) == "Yes";
                        this.ThanhLy(false);
                        break;
                    case "cmdThanhKhoanTheoMaHang":
                        if (MainForm.versionHD != 2)
                        {
                            try
                            {
                                string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                                if (st != "")
                                {
                                    ShowMessage(st, false);
                                    this.Cursor = Cursors.Default;
                                    return;
                                }

                            }
                            catch { }
                        }
                        this.HSTL = HoSoThanhLyDangKy.Load(this.HSTL.ID);
                        refreshTrangThai();
                        if (this.HSTL.TrangThaiThanhKhoan == (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo)
                        {
                            ShowMessage("Hồ sơ đã đóng, bạn không thể chạy thanh khoản.", false);
                            break;
                        }
                        this.ThanhLy(true);
                        break;
                    case "cmdFixAndRun":
                        try
                        {
                            string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                            if (st != "")
                            {
                                ShowMessage(st, false);
                                this.Cursor = Cursors.Default;
                                return;
                            }

                        }
                        catch { }
                        this.HSTL = HoSoThanhLyDangKy.Load(this.HSTL.ID);
                        refreshTrangThai();
                        if (this.HSTL.TrangThaiThanhKhoan == (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo)
                        {
                            ShowMessage("Hồ sơ đã đóng, bạn không thể fix dữ liệu và chạy thanh khoản.", false);
                            break;
                        }
                        this.FixDuLieuVaThanhLy();
                        break;
                    case "cmdBCNPLXuatNhapTon":
                        this.ShowBKNPLXuatNhapKhau();
                        break;
                    case "cmdBCNPLXuatNhapTon_TongNPL":
                        this.ShowBKNPLXuatNhapKhau_TongNPL();
                        break;
                    case "cmdBCThueXNK":
                        this.ShowBKThueXNK();
                        break;
                    case "cmdBKToKhaiXuat":
                        this.ShowBKToKhaiXuat();
                        break;
                    case "cmdBKToKhaiNhap":
                        this.ShowBKToKhaiNhap();
                        break;
                    case "cmdBCNPLXuatNhapTon_196":
                        this.ShowBKNPLXuatNhapKhau_196();
                        break;
                    case "cmdBCNPLXuatNhapTon_TongNPL_196":
                        this.ShowBKNPLXuatNhapKhau_TongNPL_196();
                        break;
                    case "cmdBaoCaoTonSauThanhKhoan":
                        this.ShowBaoCaoTonSauThanhKhoan();
                        break;

                    case "cmdBCThueXNK_196":
                        this.ShowBKThueXNK_196();
                        break;

                    case "cmdBKToKhaiXuat_196":
                        this.ShowBKToKhaiXuat_196();
                        break;
                    case "cmdBKToKhaiNhap_196":
                        this.ShowBKToKhaiNhap_196();
                        break;

                    /*TT128/2013*/
                    case "cmdBCNPLXuatNhapTon_128":
                        this.ShowBKNPLXuatNhapKhau_TT128_2013();
                        break;
                    case "cmdBCNPLXuatNhapTon_TongNPL_128":
                        this.ShowBKNPLXuatNhapKhau_TT128_2013_TongNPL();
                        break;
                    case "cmdBCThueNK_128":
                        this.ShowBKThueXNK_TT128_2013();
                        break;
                    case "cmdBKToKhaiXuat_128":
                        this.ShowBKToKhaiXuat_TT128_2013();
                        break;
                    case "cmdBKToKhaiNhap_128":
                        this.ShowBKToKhaiNhap_TT128_2013();
                        break;

                    //case "cmdPrint":
                    //    this.ShowBCThongTu194();
                    //    break;
                    case "cmdTT79KCX":
                        this.ShowBCThongTu79KCX();
                        break;
                    case "cmdBKToKhaiXuat_KCX":
                        this.ShowBKToKhaiXuat_KCX();
                        break;
                    case "cmdBKToKhaiNhap_KCX":
                        this.ShowBKToKhaiNhap_KCX();
                        break;
                    case "cmdBCNPLXuatNhapTon_KCX":
                        this.ShowBKNPLXuatNhapKhau_KCX();
                        break;
                    case "cmdBCNPLXuatNhapTon_KCX_TongNPL":
                        this.ShowBKNPLXuatNhapKhau_KCX_TongNPL();
                        break;
                    /*TT128/2013*/
                    case "cmdBCNPLXuatNhapTon_KCX_128":
                        this.ShowBKNPLXuatNhapKhau_KCX_TT128_2013();
                        break;
                    case "cmdBCNPLXuatNhapTon_KCX_TongNPL_128":
                        this.ShowBKNPLXuatNhapKhau_KCX_TT128_2013_TongNPL();
                        break;
                    case "cmdBCNPLXuatNhapTon_KCX_TT22":
                        this.ShowBKNPLXuatNhapKhau_KCX_TT22();
                        break;

                    /*TT38/2015*/
                    case "cmdBKToKhaiNhap_TT38":
                        this.ShowBKToKhaiNhap_TT38();
                        break;
                    case "cmdBKToKhaiXuat_TT38":
                        this.ShowBKToKhaiXuat_TT38();
                        break;
                    case "cmdBKToKhaiNhapXinHoan_TT38":
                        //string where = "SoToKhaiNhap in (";
                        //this.ShowBKToKhaiNhapXinHoan_TT38(where);
                        ToKhaiNhapXinHoan_TT38 frm1 = new ToKhaiNhapXinHoan_TT38();
                        frm1.LanThanhLy = HSTL.LanThanhLy;
                        frm1.NamThanhLy = HSTL.NgayBatDau.Year;
                        frm1.ShowDialog();
                        break;
                    case "cmdBKToKhaiNhapKhongThu_TT38":
                        ToKhaiNhapXinHoan_TT38 frm = new ToKhaiNhapXinHoan_TT38();
                        frm.LanThanhLy = HSTL.LanThanhLy;
                        frm.NamThanhLy = HSTL.NgayBatDau.Year;
                        frm.Hoan_KhongThu = false;
                        frm.Text = "Bảng tờ khai nhập khẩu không thu thuế";
                        frm.ShowDialog();
                        break;
                    case "cmdBCNPLXuatNhapTonTheoTK_TT38":
                        this.ShowBCNPLXuatNhapTonTheoTK_TT38();
                        break;
                    case "cmdBCNPLXuatNhapTonTheoNPL_TT38":
                        this.ShowBCNPLXuatNhapTonTheoNPL_TT38();
                        break;
                    case "cmdBCThueNPLNhapKhau_TT38":
                        this.ShowBCThueNPLNhapKhau_TT38();
                        break;
                    /*TT38/2015*/
                    case "cmdRollback":
                        try
                        {

                            // if (ShowMessage("Bạn có muốn rollback không, khi rollback thì dữ liệu tồn sẽ thay đổi.", true) == "Yes")
                            if (MLMessages("Bạn có muốn rollback không, khi rollback thì dữ liệu tồn sẽ thay đổi.", "MSG_THK91", "", true) == "Yes")
                            {

                                this.HSTL.RollBackNhieuHSTK();
                                this.HSTL = HoSoThanhLyDangKy.Load(this.HSTL.ID);
                                refreshTrangThai();
                                //ShowMessage("Hồ sơ đã rollback thành công.", false);
                                MLMessages("Hồ sơ đã rollback thành công.", "MSG_THK92", "", false);
                            }
                        }
                        catch (Exception ex)
                        {
                            //ShowMessage("Hồ sơ rollback không thành công.\n Lỗi :" + ex.Message, false);
                            MLMessages("Hồ sơ rollback không thành công.\n Lỗi :" + ex.Message, "MSG_THK93", "", false);
                        }
                        break;
                    case "cmdClose":
                        DongHSTLForm f1 = new DongHSTLForm();
                        f1.HSTL = this.HSTL;
                        f1.ShowDialog(this);
                        this.HSTL = f1.HSTL;
                        refreshTrangThai();
                        txtSoHSTL.Value = this.HSTL.SoHoSo;
                        ccNgayThanhLy.Value = this.HSTL.NgayBatDau;
                        this.HSTL.Update();
                        break;
                    case "cmdCanDoiNX":
                        CanDoiNhapXuatForm fff = new CanDoiNhapXuatForm();
                        fff.HSTL = this.HSTL;
                        fff.Show();
                        break;
                    case "cmdXemSPXuat":
                        CanDoiSPXuatForm f = new CanDoiSPXuatForm();
                        f.HSTL = this.HSTL;
                        f.Show();
                        break;
                    case "cmdChungTuThanhToan":
                        ShowChungTuThanhToanForm();
                        break;
                    case "cmdXuatBaoCao":
                        ShowBCThongTu929();
                        break;
                    case "cmdBKTKNKNL":
                        ShowBangKeToKhaiNhapKhauNPL();
                        break;
                    case "cmdBKTKXKSP":
                        ShowBangKeToKhaiXuatKhauSP();
                        break;
                    case "cmdBCNLSXXK":
                        ShowBaoCaoNPLSXXK();
                        break;
                    case "cmdBCXNTNL":
                        ShowBaoCaoXuatNhapTonNPL();
                        break;
                    case "cmdBCTTNLNK":
                        ShowBaoCaoTinhThueNPLNhapKhau();
                        break;
                    case "cmdChungTuTT":
                        ShowCTTT();
                        break;
                    case "cmdKetQuaXuLy":
                        KetQuaXuLyTK();
                        break;
                    case "cmdHangTonKho":
                        ShowHangTonKho();
                        break;
                    case "cmdBKTaiXUat":
                        ShowBKTaiXuat();
                        break;
                    case "cmdBKTieuHuy":
                        ShowBKTieuHuy();
                        break;
                    case "cmdNgayTK":
                        btnNgayDuaVaoTK_Click(null, null);
                        break;
                    case "cmdDonGiaTT":
                        btnDonGiaTT_Click(null, null);
                        break;
                    case "cmdViewNPLCU":
                        btnNPL_CU_Click(null, null);
                        break;
                    case "cmdSendNPLCU":
                        btnNPLCungUng_Click(null, null);
                        break;
                    case "cmdUpdateTK":
                        btnUpdate_Click(null, null);
                        break;
                }
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(HSTL.MaHaiQuanTiepNhan, new SendEventArgs(ex));
            }
            this.Cursor = Cursors.Default;
        }

        private void ShowBCThueNPLNhapKhau_TT38()
        {
            MessageBox.Show("Chức năng đang phát triển.", "Thông báo", MessageBoxButtons.OK);
            Company.Interface.Report.SXXK.BCThueXNK_TT128_2013 bc1 = new Company.Interface.Report.SXXK.BCThueXNK_TT128_2013();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.BindReport("");
            bc1.ShowPreview();
        }

        private void ShowBCNPLXuatNhapTonTheoNPL_TT38()
        {
            MessageBox.Show("Chức năng đang phát triển.", "Thông báo", MessageBoxButtons.OK);
            BCNPLXuatNhapTon_TT38_2015_TongNPL bc = new BCNPLXuatNhapTon_TT38_2015_TongNPL();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.BindReport("");
            bc.ShowPreview();
        }

        private void ShowBCNPLXuatNhapTonTheoTK_TT38()
        {
            MessageBox.Show("Chức năng đang phát triển.", "Thông báo", MessageBoxButtons.OK);
            BCNPLXuatNhapTon_TT38_2015 bc = new BCNPLXuatNhapTon_TT38_2015();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.BindReport("");
            bc.ShowPreview();
        }

        private void ShowBKToKhaiXNKHoan_TT38()
        {
            throw new NotImplementedException();
        }

        private void ShowBKToKhaiNhap_TT38()
        {
            //Company.Interface.Report.SXXK.BKToKhaiXNKKhongThu_TT38 bc1 = new Company.Interface.Report.SXXK.BKToKhaiXNKKhongThu_TT38();
            Company.Interface.Report.SXXK.BKToKhaiNhap_TT38 bc1 = new Company.Interface.Report.SXXK.BKToKhaiNhap_TT38();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.NamThanhLy = this.HSTL.NgayBatDau.Year;
            bc1.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            bc1.BindReport("");
            bc1.ShowRibbonPreview();
        }

        private void ShowBKToKhaiXuat_TT38()
        {

            Company.Interface.Report.SXXK.BKToKhaiXuat_TT38 bc1 = new Company.Interface.Report.SXXK.BKToKhaiXuat_TT38();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.NamThanhLy = this.HSTL.NgayBatDau.Year;
            bc1.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            bc1.BindReport("");
            bc1.ShowRibbonPreview();
        }
        private void ShowBKToKhaiNhapXinHoan_TT38(string ListTKN)
        {

            Company.Interface.Report.SXXK.BKToKhaiNhapXinHoan_TT38 bc1 = new Company.Interface.Report.SXXK.BKToKhaiNhapXinHoan_TT38();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.NamThanhLy = this.HSTL.NgayBatDau.Year;
            bc1.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            bc1.WhereCondition = ListTKN;
            bc1.BindReport("");
            bc1.ShowRibbonPreview();
        }
        private void ShowBKTaiXuat()
        {
            Company.Interface.KDT.SXXK.TaiXuat_ManagerForm frm = new Company.Interface.KDT.SXXK.TaiXuat_ManagerForm();
            frm.HSTL = HSTL;
            frm.ListTXDK = KDT_CX_TaiXuatDangKy.SelectCollectionDynamic("LanThanhLy = " + HSTL.LanThanhLy, "");
            frm.ShowDialog();
        }
        private void ShowBKTieuHuy()
        {
            Company.Interface.KDT.SXXK.NPLXinHuy_ManagerForm frm = new Company.Interface.KDT.SXXK.NPLXinHuy_ManagerForm();
            frm.HSTL = HSTL;
            frm.ListTXDK = KDT_CX_NPLXinHuyDangKy.SelectCollectionDynamic("LanThanhLy = " + HSTL.LanThanhLy, "");
            frm.ShowDialog();
        }
        private void ShowHangTonKho()
        {
            BK_HangTonKho_DNCXForm f = new BK_HangTonKho_DNCXForm();
            f.HSTL = HSTL;
            List<KDT_SXXK_BKHangTonKhoDangKy> bk = KDT_SXXK_BKHangTonKhoDangKy.SelectCollectionDynamic("LanThanhLy =" + HSTL.LanThanhLy, "");
            if (bk.Count > 0)
                f.BKHangTon = bk[0];
            f.ShowDialog();
        }
        private void KetQuaXuLyTK()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = HSTL.ID;
            form.DeclarationIssuer = Company.KDT.SHARE.Components.Globals.LaDNCX ? DeclarationIssuer.HO_SO_THANH_KHOAN_KCX : DeclarationIssuer.HO_SO_THANH_KHOAN_FULL;
            form.ShowDialog(this);
        }
        private void ShowCTTT()
        {
            ChungTuTTListForm ctttList = new ChungTuTTListForm();
            ctttList.HSTL = HSTL;
            ctttList.ShowDialog(this);
        }
        private void ShowBaoCaoTinhThueNPLNhapKhau()
        {

        }

        private void ShowBaoCaoXuatNhapTonNPL()
        {

        }

        private void ShowBaoCaoNPLSXXK()
        {
            BCNPLSXXK_929 BCNPLSXXK = new BCNPLSXXK_929();
            BCNPLSXXK.LanThanhLy = this.HSTL.LanThanhLy;
            BCNPLSXXK.SoHoSo = this.HSTL.SoHoSo;
            BCNPLSXXK.BindReport("");
            BCNPLSXXK.ShowPreview();
        }

        private void ShowBangKeToKhaiXuatKhauSP()
        {
            BangKeToKhaiXuat_929 BKToKhaiXuatKhau = new BangKeToKhaiXuat_929();
            BKToKhaiXuatKhau.SoHoSo = this.HSTL.SoHoSo;
            BKToKhaiXuatKhau.BindReport(this.HSTL.LanThanhLy);
            BKToKhaiXuatKhau.ShowPreview();
        }

        private void ShowBangKeToKhaiNhapKhauNPL()
        {
            BangKeToKhaiNhap_929 BKToKhaiNhapKhau = new BangKeToKhaiNhap_929();
            BKToKhaiNhapKhau.LanThanhLy = this.HSTL.LanThanhLy;
            BKToKhaiNhapKhau.SoHoSo = this.HSTL.SoHoSo;
            BKToKhaiNhapKhau.BindReport("");
            BKToKhaiNhapKhau.ShowPreview();
        }

        private void ShowBCThongTu929()
        {
            if (GlobalSettings.SoThapPhan.TachLam2 == 1)
            {
                ChonToKhaiNhapForm1 ChonToKhai = new ChonToKhaiNhapForm1();
                ChonToKhai.HSTL = this.HSTL;
                ChonToKhai.ShowDialog(this);
                PreviewFormBC929 XuatDoi = new PreviewFormBC929();
                XuatDoi.LanThanhLy = this.HSTL.LanThanhLy;
                XuatDoi.SoHoSo = this.HSTL.SoHoSo;
                XuatDoi.TKNHoanThueCollection = ChonToKhai.TKNHoanThueCollection;
                XuatDoi.TKNKhongThuCollection = ChonToKhai.TKNKhongThuCollection;
                XuatDoi.Show();
            }
            else
            {
                PreviewForm929 XuatDon = new PreviewForm929();
                XuatDon.LanThanhLy = this.HSTL.LanThanhLy;
                XuatDon.SoHoSo = this.HSTL.SoHoSo;
                XuatDon.Show();
            }
        }

        private void ShowChungTuThanhToanForm()
        {
            ChungTuThanhToanForm_Modified f = new ChungTuThanhToanForm_Modified();
            f.HSTL = this.HSTL;
            f.IsOpenFromChayThanhLy = true;
            f.Show();
        }

        private void ShowBCThongTu194()
        {
            if (GlobalSettings.SoThapPhan.TachLam2 == 1)
            {
                ChonToKhaiNhapForm1 f = new ChonToKhaiNhapForm1();
                f.HSTL = this.HSTL;
                f.ShowDialog(this);
                PreviewFormITG f1 = new PreviewFormITG();
                f1.LanThanhLy = this.HSTL.LanThanhLy;
                f1.SoHoSo = this.HSTL.SoHoSo;
                f1.TKNHoanThueCollection = f.TKNHoanThueCollection;
                f1.TKNKhongThuCollection = f.TKNKhongThuCollection;
                f1.Show();
            }
            else
            {
                PreviewForm f = new PreviewForm();
                f.LanThanhLy = this.HSTL.LanThanhLy;
                f.SoHoSo = this.HSTL.SoHoSo;
                f.Show();
            }
        }

        private void ShowBCThongTu79KCX()
        {

            PreviewForm_KCX f = new PreviewForm_KCX();
            f.LanThanhLy = this.HSTL.LanThanhLy;
            f.SoHoSoString = HSTL.getSoHoSo(this.HSTL.ID);
            f.SoHoSo = this.HSTL.SoHoSo;

            foreach (BangKeHoSoThanhLy bk in BangKeHoSoThanhLy.SelectCollectionBy_MaterID(this.HSTL.ID))
            {
                if (bk.MaBangKe.Contains("TKN"))
                {
                    f.BangKeNhapID = bk.ID;
                    break;
                }
            }
            f.Show();

        }

        #region BaoCao 194
        private void ShowBKToKhaiNhap()
        {
            Company.Interface.Report.SXXK.BangKeToKhaiNhap bc1 = new Company.Interface.Report.SXXK.BangKeToKhaiNhap();
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.BindReport("");
            bc1.ShowPreview();
        }

        private void ShowBKToKhaiXuat()
        {
            //Company.Interface.Report.SXXK.BangKeToKhaiXuat bc1 = new Company.Interface.Report.SXXK.BangKeToKhaiXuat();
            //Hungtq update 16/02/2011.
            Company.Interface.Report.SXXK.BangKeToKhaiXuat_TT194 bc1 = new Company.Interface.Report.SXXK.BangKeToKhaiXuat_TT194();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.BindReport(this.HSTL.LanThanhLy);
            bc1.ShowPreview();
        }

        private void ShowBKThueXNK()
        {
            //Company.Interface.Report.SXXK.BCThueXNK bc1 = new Company.Interface.Report.SXXK.BCThueXNK();
            //Hungtq update 16/02/2011.
            Company.Interface.Report.SXXK.BCThueXNK_TT194 bc1 = new Company.Interface.Report.SXXK.BCThueXNK_TT194();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.BindReport("");
            bc1.ShowPreview();
        }

        private void ShowBKNPLXuatNhapKhau()
        {
            //BCNPLXuatNhapTon bc = new BCNPLXuatNhapTon();
            //Hungtq update 16/02/2011.
            BCNPLXuatNhapTon_TT194 bc = new BCNPLXuatNhapTon_TT194();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.BindReport("");
            bc.ShowPreview();
        }

        private void ShowBKNPLXuatNhapKhau_TongNPL()
        {
            //Hungtq update 16/02/2011.
            BCNPLXuatNhapTon_TT194_TongNPL bc = new BCNPLXuatNhapTon_TT194_TongNPL();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.BindReport("");
            bc.ShowPreview();
        }
        #endregion

        #region BaoCao_196

        private void ShowBKToKhaiNhap_196()
        {
            //ShowMessage("Chức năng đang cập nhật.", false);
            Company.Interface.Report.SXXK.BangKeToKhaiNhap_196 bc1 = new Company.Interface.Report.SXXK.BangKeToKhaiNhap_196();
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.BindReport("");
            bc1.ShowPreview();
        }

        private void ShowBKToKhaiXuat_196()
        {
            //ShowMessage("Chức năng đang cập nhật.", false);
            //Hungtq update 16/02/2011.
            Company.Interface.Report.SXXK.BangKeToKhaiXuat_TT196 bc1 = new Company.Interface.Report.SXXK.BangKeToKhaiXuat_TT196();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.BindReport(this.HSTL.LanThanhLy);
            bc1.ShowPreview();
        }
        private void ShowBKNPLXuatNhapKhau_KCX_TT22()
        {
            //ShowMessage("Chức năng đang cập nhật.", false);

            //Hungtq update 16/02/2011.
            BCNPLXuatNhapTon_CX_TT22 bc = new BCNPLXuatNhapTon_CX_TT22();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.BangKeHSTL_ID = Convert.ToInt32(HSTL.BKCollection[HSTL.getBKToKhaiNhap()].ID);
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.NamThanhLy = this.HSTL.NgayBatDau.Year;
            bc.MaDoanhNghiep = this.HSTL.MaDoanhNghiep;
            bc.BindingReport();
            bc.ShowRibbonPreviewDialog();
        }
        private void ShowBKNPLXuatNhapKhau_196()
        {
            //ShowMessage("Chức năng đang cập nhật.", false);

            //Hungtq update 16/02/2011.
            BCNPLXuatNhapTon_TT196 bc = new BCNPLXuatNhapTon_TT196();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.NamThanhLy = this.HSTL.NgayBatDau.Year;
            bc.MaDoanhNghiep = this.HSTL.MaDoanhNghiep;
            bc.BindingReport();
            bc.ShowRibbonPreviewDialog();
        }
        private void ShowBaoCaoTonSauThanhKhoan()
        {
            //ShowMessage("Chức năng đang cập nhật.", false);

            //Hungtq update 16/02/2011.
            BaoCaoTonSauThanhKhoan bc = new BaoCaoTonSauThanhKhoan();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.NamThanhLy = this.HSTL.NgayBatDau.Year;
            bc.MaDoanhNghiep = this.HSTL.MaDoanhNghiep;
            bc.BindingReport();
            bc.ShowRibbonPreviewDialog();
        }

        private void ShowBKNPLXuatNhapKhau_TongNPL_196()
        {
            ShowMessage("Chức năng đang cập nhật.", false);
            //Hungtq update 16/02/2011.
            //BCNPLXuatNhapTon_TT196_TongNPL bc = new BCNPLXuatNhapTon_TT196_TongNPL();
            //bc.SoHoSo = this.HSTL.SoHoSo;
            //bc.LanThanhLy = this.HSTL.LanThanhLy;
            //bc.BindReport("");
            //bc.ShowPreview();
        }
        private void ShowBKThueXNK_196_To_TT38()
        {
            //Company.Interface.Report.SXXK.BCThueXNK bc1 = new Company.Interface.Report.SXXK.BCThueXNK();
            //Hungtq update 16/02/2011.
            //ShowMessage("Chức năng đang cập nhật.", false);
            Company.Interface.Report.SXXK.BCThueXNK_TT196_To_TT38 bc1 = new Company.Interface.Report.SXXK.BCThueXNK_TT196_To_TT38();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.NamThanhLy = this.HSTL.NgayBatDau.Year;
            bc1.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            bc1.BindReport("");
            bc1.ShowRibbonPreview();
        }
        private void ShowBKThueXNK_196()
        {
            //Company.Interface.Report.SXXK.BCThueXNK bc1 = new Company.Interface.Report.SXXK.BCThueXNK();
            //Hungtq update 16/02/2011.
            //ShowMessage("Chức năng đang cập nhật.", false);
            Company.Interface.Report.SXXK.BCThueXNK_TT196 bc1 = new Company.Interface.Report.SXXK.BCThueXNK_TT196();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.NamThanhLy = this.HSTL.NgayBatDau.Year;
            bc1.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            bc1.BindReport("");
            bc1.ShowRibbonPreview();
        }

        #endregion

        #region BaoCao TT128/2013
        private void ShowBKToKhaiNhap_TT128_2013()
        {
            Company.Interface.Report.SXXK.BangKeToKhaiNhap_TT128_2013 bc1 = new Company.Interface.Report.SXXK.BangKeToKhaiNhap_TT128_2013();
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.BindReport("");
            bc1.ShowPreview();
        }

        private void ShowBKToKhaiXuat_TT128_2013()
        {
            Company.Interface.Report.SXXK.BangKeToKhaiXuat_TT128_2013 bc1 = new Company.Interface.Report.SXXK.BangKeToKhaiXuat_TT128_2013();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.BindReport(this.HSTL.LanThanhLy, HSTL.BKCollection[0].ID);
            bc1.ShowPreview();
        }

        private void ShowBKThueXNK_TT128_2013()
        {
            Company.Interface.Report.SXXK.BCThueXNK_TT128_2013 bc1 = new Company.Interface.Report.SXXK.BCThueXNK_TT128_2013();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.LanThanhLy = this.HSTL.LanThanhLy;

            bc1.BindReport("");
            bc1.ShowPreview();
        }

        private void ShowBKNPLXuatNhapKhau_TT128_2013()
        {
            BCNPLXuatNhapTon_TT128_2013 bc = new BCNPLXuatNhapTon_TT128_2013();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.BindReport("");
            bc.ShowPreview();
        }

        private void ShowBKNPLXuatNhapKhau_TT128_2013_TongNPL()
        {
            BCNPLXuatNhapTon_TT128_2013_TongNPL bc = new BCNPLXuatNhapTon_TT128_2013_TongNPL();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.BindReport("");
            bc.ShowPreview();
        }
        #endregion

        #region BAO CAO KHU CHE XUAT

        private void ShowBKToKhaiNhap_KCX()
        {
            Company.Interface.Report.SXXK.BangKeToKhaiNhap bc1 = new Company.Interface.Report.SXXK.BangKeToKhaiNhap();
            bc1.LanThanhLy = this.HSTL.LanThanhLy;
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.BindReport("");
            bc1.ShowPreview();
        }

        private void ShowBKToKhaiXuat_KCX()
        {
            Company.Interface.Report.SXXK.BangKeToKhaiXuat bc1 = new Company.Interface.Report.SXXK.BangKeToKhaiXuat();
            bc1.SoHoSo = this.HSTL.SoHoSo;
            bc1.BindReport(this.HSTL.LanThanhLy);
            bc1.ShowPreview();
        }

        private void ShowBKNPLXuatNhapKhau_KCX()
        {
            Company.Interface.Report.SXXK.BCNPLXuatNhapTon_KCX_194 bc = new Company.Interface.Report.SXXK.BCNPLXuatNhapTon_KCX_194();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.SoHoSoString = HSTL.getSoHoSo(this.HSTL.ID);

            foreach (BangKeHoSoThanhLy bk in BangKeHoSoThanhLy.SelectCollectionBy_MaterID(this.HSTL.ID))
            {
                if (bk.MaBangKe.Contains("TKN"))
                {
                    bc.BangKeNhapID = bk.ID;
                    break;
                }
            }

            bc.BindReport("");
            bc.ShowPreview();
        }

        private void ShowBKNPLXuatNhapKhau_KCX_TongNPL()
        {
            Company.Interface.Report.SXXK.BCNPLXuatNhapTon_KCX_194_TongNPL bc = new Company.Interface.Report.SXXK.BCNPLXuatNhapTon_KCX_194_TongNPL();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.SoHoSoString = HSTL.getSoHoSo(this.HSTL.ID);

            foreach (BangKeHoSoThanhLy bk in BangKeHoSoThanhLy.SelectCollectionBy_MaterID(this.HSTL.ID))
            {
                if (bk.MaBangKe.Contains("TKN"))
                {
                    bc.BangKeNhapID = bk.ID;
                    break;
                }
            }

            bc.BindReport("");
            bc.ShowPreview();
        }

        private void ShowBKNPLXuatNhapKhau_KCX_TT128_2013()
        {
            //Company.Interface.Report.SXXK.BCNPLXuatNhapTon_KCX_194 bc = new Company.Interface.Report.SXXK.BCNPLXuatNhapTon_KCX_194();
            Company.Interface.Report.SXXK.BCNPLXuatNhapTon_KCX_128_2013 bc = new Company.Interface.Report.SXXK.BCNPLXuatNhapTon_KCX_128_2013();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.SoHoSoString = HSTL.getSoHoSo(this.HSTL.ID);

            foreach (BangKeHoSoThanhLy bk in BangKeHoSoThanhLy.SelectCollectionBy_MaterID(this.HSTL.ID))
            {
                if (bk.MaBangKe.Contains("TKN"))
                {
                    bc.BangKeNhapID = bk.ID;
                    break;
                }
            }

            bc.BindReport("");
            bc.ShowPreview();
        }

        private void ShowBKNPLXuatNhapKhau_KCX_TT128_2013_TongNPL()
        {
            //Company.Interface.Report.SXXK.BCNPLXuatNhapTon_KCX_194_TongNPL bc = new Company.Interface.Report.SXXK.BCNPLXuatNhapTon_KCX_194_TongNPL();
            Company.Interface.Report.SXXK.BCNPLXuatNhapTon_KCX_128_2013_TongNPL bc = new Company.Interface.Report.SXXK.BCNPLXuatNhapTon_KCX_128_2013_TongNPL();
            bc.SoHoSo = this.HSTL.SoHoSo;
            bc.LanThanhLy = this.HSTL.LanThanhLy;
            bc.SoHoSoString = HSTL.getSoHoSo(this.HSTL.ID);

            foreach (BangKeHoSoThanhLy bk in BangKeHoSoThanhLy.SelectCollectionBy_MaterID(this.HSTL.ID))
            {
                if (bk.MaBangKe.Contains("TKN"))
                {
                    bc.BangKeNhapID = bk.ID;
                    break;
                }
            }

            bc.BindReport("");
            bc.ShowPreview();
        }

        #endregion

        private void FixDuLieuVaThanhLy()
        {
            //TODO: Hungtq updated 29/03/2013: Kiểm tra và Cập nhật năm đăng ký của các tờ khai đã duyệt có NamDK = 0 trong bảng t_KDT_ToKhaiMauDich
            Company.BLL.KDT.ToKhaiMauDich.UpdateNamDK();


            DataSet ds = this.HSTL.GetDSSPChuaCoDM();
            if (ds.Tables[0].Rows.Count > 0)
            {
                //   ShowMessage("Có " + ds.Tables[0].Rows.Count + " mặt hàng chưa có định mức.", false);
                MLMessages("Có " + ds.Tables[0].Rows.Count + " mặt hàng chưa có định mức.", "MSG_THK94", "", false);
                SanPhamChuaCoDMForm f = new SanPhamChuaCoDMForm();
                f.ds = ds;
                f.ShowDialog(this);
                this.HSTL.TrangThaiThanhKhoan = 100;
                refreshTrangThai();
            }
            else
            {
                if (this.HSTL.LanThanhLy < this.HSTL.GetLanThanhLyMoiNhatByUserName(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN))
                {
                    try
                    {
                        this.HSTL.FixDuLieuThanhKhoan();
                        ChayLaiThanhLyForm f = new ChayLaiThanhLyForm();
                        f.HSTL = this.HSTL;
                        f.ShowDialog(this);
                        this.HSTL = f.HSTL;
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi: " + ex.Message, false);
                        return;
                    }
                }
                else
                {
                    try
                    {
                        this.HSTL.FixDuLieuThanhKhoan();
                        ChayThanhLyForm f = new ChayThanhLyForm();
                        f.HSTL = this.HSTL;
                        f.ShowDialog(this);
                        this.HSTL = f.HSTL;
                        this.HSTL.Update();
                    }
                    catch (Exception ex1)
                    {
                        ShowMessage("Lỗi: " + ex1.Message, false);
                        return;
                    }

                }
                refreshTrangThai();
            }
        }
        private void ThanhLy(bool ThanhLyTheoMaHang)
        {
            int count = HSTL.CheckHSTKTruocDoDaDong();
            if (count > 0)
            {
                ShowMessage("Có " + count + " bộ hồ sơ trước đó chưa đóng, không thể sửa bộ hiện tại.", false);
                return;
            }
            DataSet ds;
            #region Kiểm tra sai lệch hàng tờ khai V4 và tờ khai VNACCS
            if (ThanhLyTheoMaHang)
            {
                ds = new DataSet();
                ds = this.HSTL.GetDSSPCoDMDuThua(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, HSTL.NgayBatDau.Year);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ShowMessage("Có " + ds.Tables[0].Rows.Count + " mặt hàng dư thừa thông tin trong Định mức \"Đã đăng ký\".\r\nBạn có muốn DỪNG thực hiện thanh khoản để kiểm tra lại thông tin không?", true) == "Yes")
                    {
                        //Đồng ý dừng thực hiện thanh khoản và hiển thị danh sách mã SP có ĐM dư thừa.
                        SanPhamCoDMThuaForm f = new SanPhamCoDMThuaForm();
                        f.ds = ds;
                        f.ShowDialog(this);
                        this.HSTL.TrangThaiThanhKhoan = 100;
                        refreshTrangThai();
                        return;
                    }
                }
            }
            #endregion
            //Hungtq updated 10/05/2013
            #region Kiem tra trung to khai trong bang ke to khai nhap, bang ke to khai xuat.
            string msgDuplicateTK = "Danh sách số tờ khai bị trùng trong 'Bảng kê tờ khai nhập' tham gia thanh khoản:\r\n";
            DataSet dsBKDuplicateTK = HoSoThanhLyDangKy.CheckDuplicateBKTKN(HSTL.ID);
            if (dsBKDuplicateTK != null && dsBKDuplicateTK.Tables.Count > 0 && dsBKDuplicateTK.Tables[0].Rows.Count > 0)
            {
                for (int t = 0; t < dsBKDuplicateTK.Tables[0].Rows.Count; t++)
                {
                    msgDuplicateTK += string.Format("Tờ khai: {0}/ {1}/ {2}\r\n",
                        dsBKDuplicateTK.Tables[0].Rows[t]["SoToKhai"].ToString(),
                        dsBKDuplicateTK.Tables[0].Rows[t]["MaLoaiHinh"].ToString(),
                        dsBKDuplicateTK.Tables[0].Rows[t]["NgayDangKy"].ToString());
                }

                ShowMessage(msgDuplicateTK, true, true, msgDuplicateTK);
                return;
            }

            msgDuplicateTK = "Danh sách số tờ khai bị trùng trong 'Bảng kê tờ khai xuất' tham gia thanh khoản:\r\n";
            dsBKDuplicateTK = HoSoThanhLyDangKy.CheckDuplicateBKTKX(HSTL.ID);
            if (dsBKDuplicateTK != null && dsBKDuplicateTK.Tables.Count > 0 && dsBKDuplicateTK.Tables[0].Rows.Count > 0)
            {
                for (int t = 0; t < dsBKDuplicateTK.Tables[0].Rows.Count; t++)
                {
                    msgDuplicateTK += string.Format("Tờ khai: {0}/ {1}/ {2}\r\n",
                        dsBKDuplicateTK.Tables[0].Rows[t]["SoToKhai"].ToString(),
                        dsBKDuplicateTK.Tables[0].Rows[t]["MaLoaiHinh"].ToString(),
                        dsBKDuplicateTK.Tables[0].Rows[t]["NgayDangKy"].ToString());
                }

                ShowMessage(msgDuplicateTK, true, true, msgDuplicateTK);
                return;
            }
            #endregion

            try
            {
                DataTable dtNPLSai = BKToKhaiNhap.KiemTraLuongNhapNPL(HSTL.LanThanhLy);
                if (dtNPLSai != null && dtNPLSai.Rows.Count > 0)
                {
                    string MsgSaiSoLieu = string.Format("Có {0} sản phẩm sai số liệu khai báo. \r\nBạn có muốn tự sửa đổi và tiếp tục thực hiện thanh khoản ?", dtNPLSai.Rows.Count);
                    string result = ShowMessage(MsgSaiSoLieu, true);
                    if (result != "Yes")
                    {
                        NPLSaiLechForm fNPLSai = new NPLSaiLechForm();
                        fNPLSai.dt = dtNPLSai;
                        fNPLSai.ShowDialog(this);
                        return;
                    }

                }
            }
            // bỏ qua nếu lỗi
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            #region Kiểm tra danh sách SP chưa có DM
            DataSet dsSP = new DataSet();
            if (ThanhLyTheoMaHang)
            {
                //DataSet ds = new DataSet();
                dsSP = HSTL.GetDSSPChuaCoDM_TT38();
            }
            else
            {
                //DataSet ds = new DataSet();
                if (this.HSTL.isDinhMucChuaDangKy)
                {
                    dsSP = this.HSTL.GetDSSPChuaNhapDM();
                }
                else
                {
                    dsSP = this.HSTL.GetDSSPChuaCoDM();
                }
            }

            if (dsSP.Tables[0].Rows.Count > 0)
            {
                //   ShowMessage("Có " + ds.Tables[0].Rows.Count + " mặt hàng chưa có định mức.", false);
                MLMessages("Có " + dsSP.Tables[0].Rows.Count + " mặt hàng chưa có định mức.", "MSG_THK94", "", false);
                SanPhamChuaCoDMForm f = new SanPhamChuaCoDMForm();
                f.ds = dsSP;
                f.ShowDialog(this);
                this.HSTL.TrangThaiThanhKhoan = 100;
                refreshTrangThai();
                return;
            }
            else
            {
                dsSP = this.HSTL.GetDSSPChuaCapNhatLenhSX();
                if (dsSP.Tables[0].Rows.Count > 0)
                {
                    MLMessages("Có " + dsSP.Tables[0].Rows.Count + " mặt hàng chưa cập nhật lệnh sản xuất của định mức.", "MSG_THK94", "", false);
                    SanPhamChuaCoDMForm f = new SanPhamChuaCoDMForm();
                    f.ds = dsSP;
                    f.isLenhSX = true;
                    f.ShowDialog(this);
                    this.HSTL.TrangThaiThanhKhoan = 100;
                    refreshTrangThai();
                    return;
                }
            }
            #endregion


            //Hungtq updated 24/08/2012
            #region Kiem tra DM sai/ thua
            if (!ThanhLyTheoMaHang)
            {

                ds = new DataSet();
                ds = this.HSTL.GetDSSPCoDMDuThua(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, HSTL.NgayBatDau.Year);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ShowMessage("Có " + ds.Tables[0].Rows.Count + " mặt hàng dư thừa thông tin trong Định mức \"Đã đăng ký\".\r\nBạn có muốn DỪNG thực hiện thanh khoản để kiểm tra lại thông tin không?", true) == "Yes")
                    {
                        //Đồng ý dừng thực hiện thanh khoản và hiển thị danh sách mã SP có ĐM dư thừa.
                        SanPhamCoDMThuaForm f = new SanPhamCoDMThuaForm();
                        f.ds = ds;
                        f.ShowDialog(this);
                        this.HSTL.TrangThaiThanhKhoan = 100;
                        refreshTrangThai();
                        return;
                    }
                }
            }

            #endregion


            //Hungtq updated 24/08/2012
            #region Kiểm tra dữ liệu tờ khai sửa chưa cập nhật lại dữ liệu sau khi duyệt.
            ds = new DataSet();
            ds = HSTL.GetToKhaiSua_ChuaCapNhat(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = ds.Tables[0].Rows[i];

                    ToKhaiMauDich tkSua = new ToKhaiMauDich();
                    tkSua.SoToKhai = dr["SoToKhai"] != DBNull.Value ? Convert.ToInt32(dr["SoToKhai"]) : 0;
                    tkSua.MaLoaiHinh = dr["MaLoaiHinh"] != DBNull.Value ? dr["MaLoaiHinh"].ToString() : "";
                    tkSua.NamDK = dr["NamDangKy"] != DBNull.Value ? Convert.ToInt32(dr["NamDangKy"]) : 0;
                    tkSua.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    tkSua.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    tkSua.Load(tkSua.MaHaiQuan, tkSua.MaDoanhNghiep, tkSua.SoToKhai, tkSua.NamDK, tkSua.MaLoaiHinh);

                    if (tkSua.ID > 0)
                        tkSua.CapNhatThongTinHangToKhaiSua();
                }
            }
            #endregion

            if (ThanhLyTheoMaHang)
            {
                #region minhnd DM Over 3%
                DataTable dmOver = HSTL.GetDMOver3();
                if (dmOver.Rows.Count > 0)
                {
                    if (ShowMessage("Có " + dmOver.Rows.Count + " định mức NPL >3% hao hụt.\r\nBạn có muốn DỪNG thực hiện thanh khoản để kiểm tra lại thông tin không?", true) == "Yes")
                    {
                        //Đồng ý dừng thực hiện thanh khoản và hiển thị danh sách mã NPL có ĐM >3%.
                        NPL_DM_Over_3_Form f = new NPL_DM_Over_3_Form();
                        f.dtDM = dmOver;
                        f.ShowDialog(this);
                        this.HSTL.TrangThaiThanhKhoan = 100;
                        refreshTrangThai();
                        return;
                    }
                }
                #endregion minhnd DM Over 3%
            }

            //TODO: Hungtq updated 29/03/2013: Kiểm tra và Cập nhật năm đăng ký của các tờ khai đã duyệt có NamDK = 0 trong bảng t_KDT_ToKhaiMauDich
            Company.BLL.KDT.ToKhaiMauDich.UpdateNamDK();

            //TODO: Hungtq updated 01/04/2013: Kiểm tra và Cập nhật ngày hoàn thành của các tờ khai đã duyệt (Nhập tay vào) có NgayHoanThanh = 01/01/1900 trong bảng t_SXXK_ToKhaiMauDich
            Company.BLL.KDT.ToKhaiMauDich.UpdateNgayHoanThanh();

            #region cap nhat cac to khai chua co trong bang ton
            //DataTable dtNPLTonSai = HSTL.GetDSNPLNhapTon(GlobalSettings.SoThapPhan.LuongNPL).Tables["t_NPLNhapTon"];
            //try
            //{

            //    foreach (DataRow dr in dtNPLTonSai.Rows)
            //    {
            //        if (dr["ThueXNK"] == DBNull.Value)
            //        {
            //            string sotk = dr["SoToKhai"].ToString();
            //            if (dr["MaLoaiHinh"].ToString().Contains("V"))
            //                sotk = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(dr["SoToKhai"].ToString())).ToString();
            //            Company.BLL.KDT.ToKhaiMauDich TK = new Company.BLL.KDT.ToKhaiMauDich();
            //            TK.SoToKhai = dr["SoToKhai"] != DBNull.Value ? Convert.ToInt32(dr["SoToKhai"]) : 0;
            //            TK.MaLoaiHinh = dr["MaLoaiHinh"] != DBNull.Value ? dr["MaLoaiHinh"].ToString() : "";
            //            TK.NamDK = dr["NamDangKy"] != DBNull.Value ? Convert.ToInt32(dr["NamDangKy"]) : 0;
            //            TK.MaHaiQuan = dr["MaLoaiHinh"] != DBNull.Value ? dr["MaLoaiHinh"].ToString() : "";
            //            TK.MaDoanhNghiep = this.MaDoanhNghiep;
            //            TK.Load(TK.MaHaiQuan, TK.MaDoanhNghiep, TK.SoToKhai, TK.NamDK, TK.MaLoaiHinh);
            //            TK.LoadHMDCollection();
            //            TK.CapNhatThongTinHangToKhaiSua();
            //        }
            //        else
            //        {
            //            string sotk = dr["SoToKhai"].ToString();
            //            if (dr["MaLoaiHinh"].ToString().Contains("V"))
            //                sotk = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(dr["SoToKhai"].ToString())).ToString();
            //            Company.BLL.KDT.ToKhaiMauDich TK = new Company.BLL.KDT.ToKhaiMauDich();
            //            TK.SoToKhai = dr["SoToKhai"] != DBNull.Value ? Convert.ToInt32(dr["SoToKhai"]) : 0;
            //            TK.MaLoaiHinh = dr["MaLoaiHinh"] != DBNull.Value ? dr["MaLoaiHinh"].ToString() : "";
            //            TK.NamDK = dr["NamDangKy"] != DBNull.Value ? Convert.ToInt32(dr["NamDangKy"]) : 0;
            //            TK.MaHaiQuan = dr["MaLoaiHinh"] != DBNull.Value ? dr["MaLoaiHinh"].ToString() : "";
            //            TK.MaDoanhNghiep = this.MaDoanhNghiep;
            //            TK.Load(TK.MaHaiQuan, TK.MaDoanhNghiep, TK.SoToKhai, TK.NamDK, TK.MaLoaiHinh);
            //            TK.LoadHMDCollection();
            //            TK.CapNhatThongTinHangToKhaiSua();
            //        }
            //    }
            //}
            //catch (Exception exx)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage("Lỗi cập nhật danh sách tờ khai chưa có NPL trong bảng tồn ", exx);
            //    throw;
            //}
            #endregion
            //try
            //{
            if (this.HSTL.LanThanhLy < this.HSTL.GetLanThanhLyMoiNhatByUserName(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN))
            {
                ChayLaiThanhLyForm f = new ChayLaiThanhLyForm();
                f.HSTL = this.HSTL;
                if (ThanhLyTheoMaHang)
                {
                    f.ThanhLyTheoMaHang = true;
                }
                f.ShowDialog(this);
                //this.HSTL.ChayThanhLy(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.NPLKoTK);
                //ShowMessage("Chạy thanh khoản thành công.", false);
                this.HSTL = f.HSTL;
            }
            else
            {

                ChayThanhLyForm f = new ChayThanhLyForm();
                f.HSTL = this.HSTL;
                if (ThanhLyTheoMaHang)
                {
                    f.ThanhKhoanMH = true;
                }
                f.ShowDialog(this);
                //this.HSTL.ChayThanhLy(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.NPLKoTK);
                //ShowMessage("Chạy thanh khoản thành công.", false);
                //this.HSTL = f.HSTL;

                //Updated by Hungtq, 25/05/2012.
                if (this.HSTL.NgayTiepNhan.Year <= 1900)
                    HSTL.NgayTiepNhan = new DateTime(1900, 1, 1);
                if (this.HSTL.NgayBatDau.Year <= 1900)
                    HSTL.NgayBatDau = DateTime.Today;
                if (this.HSTL.NgayKetThuc.Year <= 1900)
                    HSTL.NgayKetThuc = new DateTime(1900, 1, 1);
                if (this.HSTL.NgayQuyetDinh.Year <= 1900)
                    HSTL.NgayQuyetDinh = new DateTime(1900, 1, 1);

                this.HSTL.Update();
            }
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage("Lỗi: " + ex.Message, false);
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            refreshTrangThai();

        }
        private void HuyHoSoKhaiBao()
        {
            DeclarationBase declaredCancel = new DeclarationBase()
            {
                Issuer = DeclarationIssuer.HO_SO_THANH_KHOAN,
                Reference = HSTL.GuidStr,
                Issue = DateTime.Now.ToString("yyyy-MM-dd hh:MM:ss"),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(HSTL.SoTiepNhan),
                Acceptance = HSTL.NgayTiepNhan.ToString("yyyy-MM-dd"),
                DeclarationOffice = HSTL.MaHaiQuanTiepNhan.Trim(),
                AdditionalInformations = new List<AdditionalInformation>()
            };
            declaredCancel.AdditionalInformations.Add(
                new AdditionalInformation()
                {
                    Content = new Content() { Text = "." }
                }
                );
            ObjectSend msgSend = new ObjectSend(
                      new NameBase()
                      {
                          Identity = HSTL.MaDoanhNghiep.Trim(),
                          Name = GlobalSettings.TEN_DON_VI
                      },
                      new NameBase()
                      {
                          Identity = HSTL.MaHaiQuanTiepNhan.Trim(),
                          Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HSTL.MaHaiQuanTiepNhan.Trim())
                      },
                       new SubjectBase()
                       {
                           Issuer = DeclarationIssuer.HO_SO_THANH_KHOAN,
                           Reference = HSTL.GuidStr,
                           Type = DeclarationIssuer.HO_SO_THANH_KHOAN,
                           Function = DeclarationFunction.HOI_TRANG_THAI
                       },
                       declaredCancel);
            SendMessageForm sendMessageForm = new SendMessageForm();
            sendMessageForm.Send += SendMessage;
            sendMessageForm.DoSend(msgSend);
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.HSTL.TrangThaiThanhKhoan == (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo)
                {
                    // ShowMessage("Hồ sơ đã đóng bạn không được xoá.", false);
                    MLMessages("Hồ sơ đã đóng bạn không được xoá.", "MSG_THK95", "", false);
                    return;
                }
                if (ShowMessage(setText("Bạn có muốn xóa hồ sơ thanh khoản không?", "Do you want to delete this liquidation document ?"), true) == "Yes")
                //  if (MLMessages("Bạn có muốn xóa hồ sơ thanh khoản không?", "MSG_DEL01", "", true) == "Yes")
                {
                    //TODO: Hungtq updated 13/03/2013.
                    Company.KDT.SHARE.Components.Common.HSTKBoSung.DeleteBy_HoSoDangKy_ID(HSTL.ID);

                    this.HSTL.Delete();
                    // if (ShowMessage("Bạn có muốn tạo mới hồ sơ không?", true) == "Yes")
                    if (MLMessages("Bạn có muốn tạo mới hồ sơ không?", "MSG_THK96", "", true) == "Yes")
                    {
                        Form[] forms = this.ParentForm.MdiChildren;
                        for (int i = 0; i < forms.Length; i++)
                        {
                            if (forms[i].Name.ToString().Equals("TaoHoSoThanhLyForm"))
                            {
                                forms[i].Activate();
                                return;
                            }
                        }

                        TaoHoSoThanhLyForm f = new TaoHoSoThanhLyForm();
                        f.MdiParent = this.ParentForm;
                        f.Show();
                    }
                    this.Close();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnSendBK_Click(object sender, EventArgs e)
        {
            if (dgList.GetCheckedRows().Length == 0)
            {
                ShowMessage("Chưa chọn bảng kê để gửi.", false);
                return;
            }
            if (HSTL.SoTiepNhan == 0)
            {
                ShowMessage("Chưa đăng ký số hồ sơ thanh lý.", false);
                return;
            }
            //Kiểm tra xem có bảng kê nào đã gửi nhưng chưa có phản hồi
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                sendXML.master_id = HSTL.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " đang chờ phản hồi.", false);
                    return;
                }
                else
                {
                    if (row.Cells["trangthaixl"].Value.ToString() == "1")
                    {
                        ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " đã được khai báo.", false);
                        return;
                    }
                }
            }

            string st = "";
            int i = 0;

            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                ++i;
                try
                {
                    switch (row.Cells["mabangke"].Text.Trim())
                    {
                        case "DTLTKN":
                            xmlCurrent = HSTL.WSSendBKToKhaiNhap1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách tờ khai nhập";
                            break;
                        case "DTLTKX":
                            xmlCurrent = HSTL.WSSendBKToKhaiXuat1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách tờ khai xuất";
                            break;
                        case "DTLNPLCHUATL":
                            xmlCurrent = HSTL.WSSendBKNPLChuaThanhLy1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách nguyên phụ liệu chưa thanh lý";
                            break;
                        case "DTLNPLXH":
                            xmlCurrent = HSTL.WSSendBKNPLHuy1("wsForm.txtMatKhau.Text.Trim()");
                            break;
                        case "DTLNPLTX":
                            xmlCurrent = HSTL.WSSendBKNPLTaiXuat1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách nguyên phụ liệu tái xuất";
                            break;
                        case "DTLNPLNKD":
                            xmlCurrent = HSTL.WSSendBKNPLNhapKinhDoanh1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh";
                            break;
                        case "DTLNPLNT":
                            xmlCurrent = HSTL.WSSendBKNPLNopThue1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách nguyên phụ liệu nộp thuế";
                            break;
                        case "DTLNPLXGC":
                            xmlCurrent = HSTL.WSSendBKNPLXuatGiaCong1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách nguyên phụ liệu xuất sang tờ khai gia công";
                            break;
                        case "DTLCHITIETNT":
                            xmlCurrent = HSTL.WSSendBKNPLTamNopThue1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách nguyên phụ liệu tạm nộp thuế";
                            break;
                    }
                    if (xmlCurrent != "")
                    {
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                        sendXML.master_id = HSTL.ID;
                        sendXML.msg = xmlCurrent;
                        sendXML.func = 1;
                        xmlCurrent = "";
                        sendXML.InsertUpdate();
                    }
                }
                catch (Exception ex)
                {
                    --i;
                    this.Cursor = Cursors.Default;
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                if (ShowMessage("Khai báo không thành công " + st + ".Có lỗi do hệ thống phía hải quan.\nBạn có muốn gửi tiếp những bảng kê còn lại không?", true) == "Yes")
                                {
                                }
                                else
                                    break;
                            }
                            else
                            {
                                ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            }
                        }
                        else
                        {

                            ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                        }
                        #endregion FPTService
                    }
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo bảng kê hồ sơ thanh lý " + st + ". Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                }
            }
            if (i > 0)
            {
                LayPhanHoiBangKe("wsForm.txtMatKhau.Text.Trim()");
            }
            else
                ShowMessage("Không gửi thông tin bảng kê được.", false);
            this.HSTL.LoadBKCollection();
            dgList.DataSource = this.HSTL.BKCollection;
            this.Cursor = Cursors.Default;
        }

        private void btnHuyBK_Click(object sender, EventArgs e)
        {
            if (dgList.GetCheckedRows().Length == 0)
            {
                ShowMessage("Chưa chọn bảng kê để gửi.", false);
                return;
            }
            if (HSTL.SoTiepNhan == 0)
            {
                ShowMessage("Chưa đăng ký số hồ sơ thanh lý.", false);
                return;
            }
            //Kiểm tra xem có bảng kê nào đã gửi nhưng chưa có phản hồi
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                sendXML.master_id = HSTL.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " đang chờ phản hồi.", false);
                    return;
                }
                else
                {
                    if (row.Cells["trangthaixl"].Value.ToString() == "0")
                    {
                        ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " chưa được khai báo.", false);
                        return;
                    }
                }
            }
            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            this.Cursor = Cursors.WaitCursor;
            string st = "";
            int i = 0;

            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                ++i;
                try
                {
                    switch (row.Cells["mabangke"].Text.Trim())
                    {
                        case "DTLTKN":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachTKN1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách tờ khai nhập";
                            break;
                        case "DTLTKX":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachTKX1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách tờ khai xuất";
                            break;
                        case "DTLNPLCHUATL":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLChuaThanhLy1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách nguyên phụ liệu chưa thanh lý";
                            break;
                        case "DTLNPLXH":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLXinHuy1("wsForm.txtMatKhau.Text.Trim()");
                            break;
                        case "DTLNPLTX":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLTaiXuat1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách nguyên phụ liệu tái xuất";
                            break;
                        case "DTLNPLNKD":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLNKD1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh";
                            break;
                        case "DTLNPLNT":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLNopThue1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách nguyên phụ liệu nộp thuế";
                            break;
                        case "DTLNPLXGC":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLXuatGiaCong1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách nguyên phụ liệu xuất sang tờ khai gia công";
                            break;
                        case "DTLCHITIETNT":
                            xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLTamNopThue1("wsForm.txtMatKhau.Text.Trim()");
                            st = "Danh sách nguyên phụ liệu tạm nộp thuế";
                            break;
                    }
                    if (xmlCurrent != "")
                    {
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                        sendXML.master_id = HSTL.ID;
                        sendXML.msg = xmlCurrent;
                        sendXML.func = 3;
                        xmlCurrent = "";
                        sendXML.InsertUpdate();
                    }
                }
                catch (Exception ex)
                {
                    --i;
                    this.Cursor = Cursors.Default;
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                if (ShowMessage("Hủy khai báo không thành công " + st + ".Có lỗi do hệ thống phía hải quan.\nBạn có muốn hủy tiếp những bảng kê còn lại không?", true) == "Yes")
                                {
                                }
                                else
                                    break;
                            }
                            else
                            {
                                ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            }
                        }
                        else
                        {

                            ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                        }
                        #endregion FPTService
                    }
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi hủy khai báo bảng kê hồ sơ thanh lý. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                }
            }
            if (i > 0)
            {
                LayPhanHoiBangKe("wsForm.txtMatKhau.Text.Trim()");
            }
            else
                ShowMessage("Chưa gửi thông tin hủy bảng kê tới hải quan được.\nHãy thực hiện lại.", false);
            this.Cursor = Cursors.Default;
        }

        private void sendBK1_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
            sendXML.master_id = HSTL.ID;
            if (sendXML.Load())
            {
                ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " đang chờ phản hồi.", false);
                return;
            }
            else
            {
                if (row.Cells["trangthaixl"].Value.ToString() == "1")
                {
                    ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " đã được khai báo.", false);
                    return;
                }
            }
            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            string st = "";
            this.Cursor = Cursors.WaitCursor;
            try
            {
                switch (row.Cells["mabangke"].Text.Trim())
                {
                    case "DTLTKN":
                        xmlCurrent = HSTL.WSSendBKToKhaiNhap1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách tờ khai nhập";
                        break;
                    case "DTLTKX":
                        xmlCurrent = HSTL.WSSendBKToKhaiXuat1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách tờ khai xuất";
                        break;
                    case "DTLNPLCHUATL":
                        xmlCurrent = HSTL.WSSendBKNPLChuaThanhLy1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách nguyên phụ liệu chưa thanh lý";
                        break;
                    case "DTLNPLXH":
                        xmlCurrent = HSTL.WSSendBKNPLHuy1("wsForm.txtMatKhau.Text.Trim()");
                        break;
                    case "DTLNPLTX":
                        xmlCurrent = HSTL.WSSendBKNPLTaiXuat1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách nguyên phụ liệu tái xuất";
                        break;
                    case "DTLNPLNKD":
                        xmlCurrent = HSTL.WSSendBKNPLNhapKinhDoanh1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh";
                        break;
                    case "DTLNPLNT":
                        xmlCurrent = HSTL.WSSendBKNPLNopThue1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách nguyên phụ liệu nộp thuế";
                        break;
                    case "DTLNPLXGC":
                        xmlCurrent = HSTL.WSSendBKNPLXuatGiaCong1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách nguyên phụ liệu xuất sang tờ khai gia công";
                        break;
                    case "DTLCHITIETNT":
                        xmlCurrent = HSTL.WSSendBKNPLTamNopThue1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách nguyên phụ liệu tạm nộp thuế";
                        break;
                }
                if (xmlCurrent != "")
                {
                    sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                    sendXML.master_id = HSTL.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 1;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Khai báo không thành công " + st + ".Có lỗi do hệ thống phía hải quan.\nBạn có muốn gửi tiếp những bảng kê còn lại không?", true) == "Yes")
                            {
                            }
                            else
                                return;
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {

                        ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo bảng kê hồ sơ thanh lý " + st + ". Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            LayPhanHoiMotBangKe(sendXML.LoaiHS, "wsForm.txtMatKhau.Text.Trim()");
            this.Cursor = Cursors.Default;
        }

        private void huyBangKe1_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
            sendXML.master_id = HSTL.ID;
            if (sendXML.Load())
            {
                ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " đang chờ phản hồi.", false);
                return;
            }
            else
            {
                if (row.Cells["trangthaixl"].Value.ToString() == "0")
                {
                    ShowMessage("Bảng kê " + row.Cells["tenbangke"].Text.Trim() + " chưa được khai báo.", false);
                    return;
                }
            }
            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            this.Cursor = Cursors.WaitCursor;
            string st = "";
            try
            {
                switch (row.Cells["mabangke"].Text.Trim())
                {
                    case "DTLTKN":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachTKN1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách tờ khai nhập";
                        break;
                    case "DTLTKX":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachTKX1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách tờ khai xuất";
                        break;
                    case "DTLNPLCHUATL":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLChuaThanhLy1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách nguyên phụ liệu chưa thanh lý";
                        break;
                    case "DTLNPLXH":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLXinHuy1("wsForm.txtMatKhau.Text.Trim()");
                        break;
                    case "DTLNPLTX":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLTaiXuat1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách nguyên phụ liệu tái xuất";
                        break;
                    case "DTLNPLNKD":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLNKD1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh";
                        break;
                    case "DTLNPLNT":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLNopThue1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách nguyên phụ liệu nộp thuế";
                        break;
                    case "DTLNPLXGC":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLXuatGiaCong1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách nguyên phụ liệu xuất sang tờ khai gia công";
                        break;
                    case "DTLCHITIETNT":
                        xmlCurrent = HSTL.HuyKhaiBaoDanhSachNPLTamNopThue1("wsForm.txtMatKhau.Text.Trim()");
                        st = "Danh sách nguyên phụ liệu tạm nộp thuế";
                        break;
                }
                if (xmlCurrent != "")
                {
                    sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
                    sendXML.master_id = HSTL.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 3;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Khai báo không thành công " + st + ".Có lỗi do hệ thống phía hải quan.\nBạn có muốn gửi tiếp những bảng kê còn lại không?", true) == "Yes")
                            {
                            }
                            else
                                return;
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {

                        ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo bảng kê hồ sơ thanh lý " + st + ". Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            LayPhanHoiMotBangKe(sendXML.LoaiHS, "wsForm.txtMatKhau.Text.Trim()");
            this.Cursor = Cursors.Default;
        }

        private void xacNhanThongTin1_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = row.Cells["mabangke"].Text.Trim();
            sendXML.master_id = HSTL.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Bảng kê này chưa có gửi thông tin tới hải quan.", false);
                return;
            }
            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            this.Cursor = Cursors.WaitCursor;
            LayPhanHoiMotBangKe(sendXML.LoaiHS, "wsForm.txtMatKhau.Text.Trim()");
            this.Cursor = Cursors.Default;
        }

        private void uiCommandBar1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        //private void CapNhatHoSoThanhLyForm_Activated(object sender, EventArgs e)
        //{
        //    //this.HSTL.Load();
        //    //this.refreshTrangThai();
        //}

        private void txtSoHSTL_Click(object sender, EventArgs e)
        {

        }

        private void btnXoaBK_Click(object sender, EventArgs e)
        {
            BangKeHoSoThanhLyCollection bkColl = new BangKeHoSoThanhLyCollection();
            if (dgList.GetCheckedRows().Length < 1)
            {
                ShowMessage(setText("Chưa có bảng kê nào được chọn  để xóa .", "Please select list to delete before?"), false);
                return;
            }
            if (ShowMessage(setText("Bạn có muốn xóa bảng kê này không?", "Do you want to delete this list ?"), true) != "Yes")
            {
                return;
            }
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                BangKeHoSoThanhLy bk = (BangKeHoSoThanhLy)row.DataRow;
                if (row.RowType == RowType.Record)
                {
                    if (bk.MaBangKe == "DTLTKN" || bk.MaBangKe == "DTLTKX")
                    {
                        ShowMessage(setText("Bảng kê BK01 và BK02 không xóa được, sẽ bỏ qua", "BK01,BK02 list can not delete, so skip?"), false);
                    }
                    else
                    {
                        if (bk.ID > 0)
                        {
                            bk.Delete();

                        }
                        bkColl.Add(bk);


                    }
                }
            }
            foreach (BangKeHoSoThanhLy bkDelete in bkColl)
            {
                this.HSTL.BKCollection.Remove(bkDelete);
                bindComboboxDelete(bkDelete.MaBangKe);
            }
            dgList.DataSource = this.HSTL.BKCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            bindCombobox();

            NewColumnTongSoToKhai();
        }

        #region  V3

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// FeedBackContent 
        FeedBackContent feedbackContent = null;
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    bool isDelete = false;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function)
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(HSTL.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                HSTL.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                msgInfor = "Hải quan từ chối tiếp nhận" + "\r\n" + noidung;
                                isDelete = true;
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            this.ShowMessageTQDT(string.Format("THÔNG BÁO TỪ HỆ THỐNG hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');
                                if (vals.Length > 1)
                                {
                                    HSTL.SoTiepNhan = long.Parse(vals[0].Trim());

                                }
                                else
                                {
                                    HSTL.SoTiepNhan = System.Convert.ToInt32(feedbackContent.CustomsReference);
                                }
                                HSTL.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);

                                noidung += string.Format("\r\nSố tiếp nhận: {0}\r\nNgày tiếp nhận : {1}", HSTL.SoTiepNhan, HSTL.NgayTiepNhan);
                                e.FeedBackMessage.XmlSaveMessage(HSTL.ID, "Cấp số tiếp nhận", noidung);

                                HSTL.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                                string sfmt = string.Format("Số tiếp nhận {0}\r\nNgày tiếp nhận {1} ", HSTL.SoTiepNhan, HSTL.NgayTiepNhan.ToString("dd-MM-yyyy"));
                                msgInfor = sfmt;
                                //this.ShowMessageTQDT(sfmt, false);
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(HSTL.ID, "Chấp nhận bản khai", noidung);
                                HSTL.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                msgInfor = "chấp nhận bản khai";
                                isDelete = true;
                            }
                            break;
                        case DeclarationFunction.DUYET_LUONG_CHUNG_TU:
                            {
                                foreach (AdditionalInformation item in feedbackContent.AdditionalInformations)
                                {
                                    HSTL.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                    //                                     if (item.Statement == AdditionalInformationStatement.LUONG_XANH)
                                    //                                         HSTL. = TrangThaiPhanLuong.LUONG_XANH;
                                    //                                     else if (item.Statement == AdditionalInformationStatement.LUONG_VANG)
                                    //                                         HSTL.PhanLuong = TrangThaiPhanLuong.LUONG_VANG;
                                    //                                     else if (item.Statement == AdditionalInformationStatement.LUONG_DO)
                                    //                                         HSTL.PhanLuong = TrangThaiPhanLuong.LUONG_DO;
                                    e.FeedBackMessage.XmlSaveMessage(HSTL.ID, "Phân luồng hồ sơ TK", noidung);
                                    this.ShowMessageTQDT("Nội dung: " + noidung, false);
                                    msgInfor = noidung;
                                }
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(HSTL.ID, MessageTitle.Error, noidung);
                                msgInfor = noidung;
                                break;
                            }
                    }
                    if (isUpdate)
                    {
                        HSTL.Update();
                        setCommandStatus();
                    }
                    if (isDelete)
                    {
                        SingleMessage.DeleteMsgSend(LoaiKhaiBao.ThanhKhoan, HSTL.ID);
                    }

                }
                else
                {
                    SingleMessage.SendMail(HSTL.MaHaiQuanTiepNhan, e);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion

        private void chkKhaiBaoBangKe_CheckedChanged(object sender, EventArgs e)
        {
            Company.KDT.SHARE.Components.Globals.KhaiBaoThanhKoanLoai2 = chkKhaiBaoBangKe.Checked;
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("KhaiBaoThanhKoanLoai2", Company.KDT.SHARE.Components.Globals.KhaiBaoThanhKoanLoai2.ToString());
        }

        private void lblLinkCapNhatNgayTK_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CapNhatToKhaiTK tk = new CapNhatToKhaiTK();
            tk.ShowDialog();
        }

        private void btnNPLCungUng_Click(object sender, EventArgs e)
        {
            BKNPLCungUng_ManagerForm f = new BKNPLCungUng_ManagerForm();
            f.HSTL = HSTL;
            f.ShowDialog();
            //BKNPLCungUngForm bkNPL = new BKNPLCungUngForm();
            //bkNPL.HSTL = this.HSTL;
            //bkNPL.ShowDialog(this);
            //this.HSTL = bkNPL.HSTL;
            //dgList.DataSource = this.HSTL.BKCollection;
            //dgList.Refetch();
            //bindCombobox();
        }

        private void btnNgayDuaVaoTK_Click(object sender, EventArgs e)
        {
            CapNhatToKhaiTK tk = new CapNhatToKhaiTK();
            tk.ShowDialog();
        }

        private void btnDonGiaTT_Click(object sender, EventArgs e)
        {
            CapNhatToKhaiTK tk = new CapNhatToKhaiTK();
            tk.ConvertDonGiaTT = true;
            tk.LanThanhLy = this.HSTL.LanThanhLy;
            tk.ShowDialog();
        }
        /// <summary>
        /// minhnd 24/10/2014
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void linkChenhLech_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CauHinhInForm cauHinh = new CauHinhInForm();
            cauHinh.ShowDialog();
            //lbNgayChenhLech.Text = GlobalSettings.CHENHLECH_THN_THX;
        }

        private void btnNPL_CU_Click(object sender, EventArgs e)
        {
            try
            {
                BKNPL_CU_NEWForm bk = new BKNPL_CU_NEWForm(this.HSTL.LanThanhLy);
                bk.ShowDialog();
            }
            catch (Exception ex)
            {

                //throw;
            }

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                HSTL.CapNhatSaiSoLuongTonDau(GlobalSettings.SoThapPhan.LuongNPL);
                ShowMessage("Cập nhật thành công ", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
