using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Windows.Forms;
using Janus.Windows.GridEX.EditControls ; 
namespace Company.Interface
{
    public class DocExcel
    {
        public void insertThongSoHangTK(string sheetName, string beginRow, 
                                                                string productCode,  string productName, 
                                                                string hSCode, string unitCal, 
                                                                string quantity, string price, 
                                                                string madeIn, string triGiaTT, string TSXNK, string TSGTGT, string TSTTDB, string TLTK)
        {
            XmlDocument doc = new XmlDocument() ;

            doc.Load(Application.StartupPath +"\\ExcelSetting.xml");

            XmlNode sheet = doc.SelectSingleNode("TSTK/Tensheet");
            sheet.InnerText = sheetName;

            XmlNode row = doc.SelectSingleNode("TSTK/Dongbatdau");
            row.InnerText = beginRow;

            XmlNode code = doc.SelectSingleNode("TSTK/MaHang");
            code.InnerText = productCode;

            XmlNode name = doc.SelectSingleNode("TSTK/TenHang");
            name.InnerText = productName;

            XmlNode hs = doc.SelectSingleNode("TSTK/MaHS");
            hs.InnerText = hSCode;

            XmlNode dvt = doc.SelectSingleNode("TSTK/DVT");
            dvt.InnerText = unitCal;

            XmlNode quan = doc.SelectSingleNode("TSTK/Soluong");
            quan.InnerText = quantity;

            XmlNode priceunit = doc.SelectSingleNode("TSTK/Dongia");
            priceunit.InnerText = price;

            XmlNode made = doc.SelectSingleNode("TSTK/Xuatxu");
            made.InnerText = madeIn;

            XmlNode tgtt = doc.SelectSingleNode("TSTK/TGTT");
            tgtt.InnerText = triGiaTT;

            XmlNode tsXNK = doc.SelectSingleNode("TSTK/TSXNK");
            tsXNK.InnerText = TSXNK;

            XmlNode tsGTGT = doc.SelectSingleNode("TSTK/TSGTGT");
            tsGTGT.InnerText = TSGTGT;

            XmlNode tsTTDB = doc.SelectSingleNode("TSTK/TSTTDB");
            tsTTDB.InnerText = TSTTDB;

            XmlNode tltk = doc.SelectSingleNode("TSTK/TLTK");
            tltk.InnerText = TLTK;

            doc.Save(Application.StartupPath + "\\ExcelSetting.xml"); 
           
        }

        public void ReadDefault(EditBox sheetName,
                                NumericEditBox beginRow,
                                EditBox productCode,
                                EditBox productName,
                                EditBox hSCode,
                                EditBox unitCal,
                                EditBox quantity,
                                EditBox price,
                                EditBox madeIn,
                                EditBox TGTT,
                                EditBox TSXNK,
                                EditBox TSGTGT,
                                EditBox TSTTDB,
                                EditBox TLTK
                                )


        {
            XmlDocument doc = new XmlDocument();

            doc.Load(Application.StartupPath + "\\ExcelSetting.xml");
            XmlNode sheet = doc.SelectSingleNode("TSTK/Tensheet");
             sheetName.Text  = sheet.InnerText;

            XmlNode row = doc.SelectSingleNode("TSTK/Dongbatdau");
            beginRow.Text = row.InnerText;

            XmlNode code = doc.SelectSingleNode("TSTK/MaHang");
            productCode.Text = code.InnerText;

            XmlNode name = doc.SelectSingleNode("TSTK/TenHang");
            productName.Text = name.InnerText;

            XmlNode hs = doc.SelectSingleNode("TSTK/MaHS");
            hSCode.Text = hs.InnerText;

            XmlNode dvt = doc.SelectSingleNode("TSTK/DVT");
            unitCal.Text = dvt.InnerText;

            XmlNode quan = doc.SelectSingleNode("TSTK/Soluong");
            quantity.Text = quan.InnerText;

            XmlNode priceunit = doc.SelectSingleNode("TSTK/Dongia");
            price.Text = priceunit.InnerText;

            XmlNode made = doc.SelectSingleNode("TSTK/Xuatxu");
            madeIn.Text = made.InnerText;

            XmlNode tgTT = doc.SelectSingleNode("TSTK/TGTT");
            TGTT.Text = tgTT.InnerText;

            XmlNode tsXNK = doc.SelectSingleNode("TSTK/TSXNK");
            TSXNK.Text = tsXNK.InnerText;

            XmlNode tsGTGT = doc.SelectSingleNode("TSTK/TSGTGT");
            TSGTGT.Text = tsGTGT.InnerText;

            XmlNode tsTTDB = doc.SelectSingleNode("TSTK/TSTTDB");
            TSTTDB.Text = tsTTDB.InnerText;

            XmlNode tltk = doc.SelectSingleNode("TSTK/TLTK");
            TLTK.Text = tltk.InnerText;
        }


        public void insertThongSo(string sheetName, 
            string beginRow,  string productCode, string productName, string hSCode, string unitCal )
        {
            XmlDocument doc = new XmlDocument();

            doc.Load(Application.StartupPath + "\\ExcelSetting.xml");

            XmlNode sheet = doc.SelectSingleNode("TSTK/Tensheet");
            sheet.InnerText = sheetName;

            XmlNode row = doc.SelectSingleNode("TSTK/Dongbatdau");
            row.InnerText = beginRow;

            XmlNode code = doc.SelectSingleNode("TSTK/MaHang");
            code.InnerText = productCode;

            XmlNode name = doc.SelectSingleNode("TSTK/TenHang");
            name.InnerText = productName;

            XmlNode hs = doc.SelectSingleNode("TSTK/MaHS");
            hs.InnerText = hSCode;

            XmlNode dvt = doc.SelectSingleNode("TSTK/DVT");
            dvt.InnerText = unitCal;

          

            doc.Save(Application.StartupPath + "\\ExcelSetting.xml");

        }

        public void ReadDefaultCon(EditBox sheetName,
                                NumericEditBox beginRow,
                                EditBox productCode,
                                EditBox productName,
                                EditBox hSCode,
                                EditBox unitCal
                           
                                )
        {
            XmlDocument doc = new XmlDocument();

            doc.Load(Application.StartupPath + "\\ExcelSetting.xml");
            XmlNode sheet = doc.SelectSingleNode("TSTK/Tensheet");
            sheetName.Text = sheet.InnerText;

            XmlNode row = doc.SelectSingleNode("TSTK/Dongbatdau");
            beginRow.Text = row.InnerText;

            XmlNode code = doc.SelectSingleNode("TSTK/MaHang");
            productCode.Text = code.InnerText;

            XmlNode name = doc.SelectSingleNode("TSTK/TenHang");
            productName.Text = name.InnerText;

            XmlNode hs = doc.SelectSingleNode("TSTK/MaHS");
            hSCode.Text = hs.InnerText;

            XmlNode dvt = doc.SelectSingleNode("TSTK/DVT");
            unitCal.Text = dvt.InnerText;
          
        }
    }
}
