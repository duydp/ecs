namespace Company.Interface.SXXK.BangKe
{
    partial class BK01WizardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgTKN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKNTL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BK01WizardForm));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnBack = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnFinish = new Janus.Windows.EditControls.UIButton();
            this.btnNext = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTenChuHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.ccToDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label7 = new System.Windows.Forms.Label();
            this.ccFromDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.lblChenhLechNgay = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnExport = new Janus.Windows.EditControls.UIButton();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGoTo = new Janus.Windows.EditControls.UIButton();
            this.btnXuatGhiChu_DM = new Janus.Windows.EditControls.UIButton();
            this.btnExportTL = new Janus.Windows.EditControls.UIButton();
            this.txtSoToKhaiTL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSearchTL = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.label5 = new System.Windows.Forms.Label();
            this.btnLayTKN = new Janus.Windows.EditControls.UIButton();
            this.btnPhanBoTuDong = new Janus.Windows.EditControls.UIButton();
            this.btnAddTKNFromDM = new Janus.Windows.EditControls.UIButton();
            this.btnGetTKN_From_DM = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgTKN = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgTKNTL = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKNTL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox8);
            this.grbMain.Controls.Add(this.uiGroupBox7);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Size = new System.Drawing.Size(1187, 723);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.ImageSize = new System.Drawing.Size(20, 20);
            this.btnBack.Location = new System.Drawing.Point(839, 14);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(70, 23);
            this.btnBack.TabIndex = 297;
            this.btnBack.Text = "Trở lại";
            this.btnBack.VisualStyleManager = this.vsmMain;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(1111, 14);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(64, 23);
            this.btnClose.TabIndex = 296;
            this.btnClose.Text = "Đóng";
            this.btnClose.TextHorizontalAlignment = Janus.Windows.EditControls.TextAlignment.Near;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinish.Image = ((System.Drawing.Image)(resources.GetObject("btnFinish.Image")));
            this.btnFinish.ImageSize = new System.Drawing.Size(20, 20);
            this.btnFinish.Location = new System.Drawing.Point(1008, 14);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(97, 23);
            this.btnFinish.TabIndex = 295;
            this.btnFinish.Text = "Hoàn thành";
            this.btnFinish.VisualStyleManager = this.vsmMain;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Enabled = false;
            this.btnNext.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.ImageSize = new System.Drawing.Size(20, 20);
            this.btnNext.Location = new System.Drawing.Point(921, 14);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(83, 23);
            this.btnNext.TabIndex = 304;
            this.btnNext.Text = "Tiếp tục";
            this.btnNext.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.txtTenChuHang);
            this.uiGroupBox4.Controls.Add(this.btnSearch);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.ccToDate);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.ccFromDate);
            this.uiGroupBox4.Controls.Add(this.lblChenhLechNgay);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1187, 73);
            this.uiGroupBox4.TabIndex = 305;
            this.uiGroupBox4.Text = "Tìm kiếm";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(15, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(440, 13);
            this.label8.TabIndex = 275;
            this.label8.Text = "Lưu ý: Chênh lệch ngày giữa Tờ khai Xuất/ Nhập đã thiết lập trong cấu hình = ";
            // 
            // txtTenChuHang
            // 
            this.txtTenChuHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenChuHang.Location = new System.Drawing.Point(483, 21);
            this.txtTenChuHang.Name = "txtTenChuHang";
            this.txtTenChuHang.Size = new System.Drawing.Size(100, 21);
            this.txtTenChuHang.TabIndex = 274;
            this.txtTenChuHang.VisualStyleManager = this.vsmMain;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearch.Location = new System.Drawing.Point(589, 21);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(93, 23);
            this.btnSearch.TabIndex = 272;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyleManager = this.vsmMain;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(395, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 273;
            this.label1.Text = "Tên chủ hàng";
            // 
            // ccToDate
            // 
            this.ccToDate.BackColor = System.Drawing.Color.White;
            this.ccToDate.CustomFormat = "dd/MM/yyyy";
            this.ccToDate.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.ccToDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccToDate.DropDownCalendar.Name = "";
            this.ccToDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccToDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccToDate.Location = new System.Drawing.Point(295, 20);
            this.ccToDate.Name = "ccToDate";
            this.ccToDate.Nullable = true;
            this.ccToDate.NullButtonText = "Xóa";
            this.ccToDate.ShowNullButton = true;
            this.ccToDate.Size = new System.Drawing.Size(94, 21);
            this.ccToDate.TabIndex = 271;
            this.ccToDate.TodayButtonText = "Hôm nay";
            this.ccToDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccToDate.VisualStyleManager = this.vsmMain;
            this.ccToDate.ValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(231, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 270;
            this.label7.Text = "đến ngày";
            // 
            // ccFromDate
            // 
            this.ccFromDate.BackColor = System.Drawing.Color.White;
            this.ccFromDate.CustomFormat = "dd/MM/yyyy";
            this.ccFromDate.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.ccFromDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccFromDate.DropDownCalendar.Name = "";
            this.ccFromDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccFromDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccFromDate.Location = new System.Drawing.Point(127, 20);
            this.ccFromDate.Name = "ccFromDate";
            this.ccFromDate.Nullable = true;
            this.ccFromDate.NullButtonText = "Xóa";
            this.ccFromDate.ShowNullButton = true;
            this.ccFromDate.Size = new System.Drawing.Size(96, 21);
            this.ccFromDate.TabIndex = 269;
            this.ccFromDate.TodayButtonText = "Hôm nay";
            this.ccFromDate.Value = new System.DateTime(2008, 1, 1, 0, 0, 0, 0);
            this.ccFromDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccFromDate.VisualStyleManager = this.vsmMain;
            this.ccFromDate.ValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblChenhLechNgay
            // 
            this.lblChenhLechNgay.AutoSize = true;
            this.lblChenhLechNgay.BackColor = System.Drawing.Color.Transparent;
            this.lblChenhLechNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChenhLechNgay.ForeColor = System.Drawing.Color.Red;
            this.lblChenhLechNgay.Location = new System.Drawing.Point(460, 50);
            this.lblChenhLechNgay.Name = "lblChenhLechNgay";
            this.lblChenhLechNgay.Size = new System.Drawing.Size(58, 13);
            this.lblChenhLechNgay.TabIndex = 269;
            this.lblChenhLechNgay.Text = "0 (ngày).";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 13);
            this.label6.TabIndex = 269;
            this.label6.Text = "Chọn tờ khai từ ngày";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(7, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(294, 14);
            this.label4.TabIndex = 307;
            this.label4.Text = "Danh sách tờ khai nhập chưa thanh khoản hết";
            // 
            // btnExport
            // 
            this.btnExport.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExport.Image")));
            this.btnExport.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExport.Location = new System.Drawing.Point(375, 18);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(89, 23);
            this.btnExport.TabIndex = 317;
            this.btnExport.Text = "Xuất Excel";
            this.btnExport.VisualStyleManager = this.vsmMain;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.Location = new System.Drawing.Point(128, 18);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhai.Size = new System.Drawing.Size(146, 21);
            this.txtSoToKhai.TabIndex = 312;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.Value = ((long)(0));
            this.txtSoToKhai.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int64;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            this.txtSoToKhai.TextChanged += new System.EventHandler(this.btnGoTo_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 311;
            this.label2.Text = "Tìm đến tờ khai số:";
            // 
            // btnGoTo
            // 
            this.btnGoTo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoTo.Image = ((System.Drawing.Image)(resources.GetObject("btnGoTo.Image")));
            this.btnGoTo.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGoTo.Location = new System.Drawing.Point(280, 18);
            this.btnGoTo.Name = "btnGoTo";
            this.btnGoTo.Size = new System.Drawing.Size(89, 23);
            this.btnGoTo.TabIndex = 310;
            this.btnGoTo.Text = "Tìm";
            this.btnGoTo.VisualStyleManager = this.vsmMain;
            this.btnGoTo.Click += new System.EventHandler(this.btnGoTo_Click);
            // 
            // btnXuatGhiChu_DM
            // 
            this.btnXuatGhiChu_DM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatGhiChu_DM.Image = ((System.Drawing.Image)(resources.GetObject("btnXuatGhiChu_DM.Image")));
            this.btnXuatGhiChu_DM.Location = new System.Drawing.Point(609, 14);
            this.btnXuatGhiChu_DM.Name = "btnXuatGhiChu_DM";
            this.btnXuatGhiChu_DM.Size = new System.Drawing.Size(117, 23);
            this.btnXuatGhiChu_DM.TabIndex = 310;
            this.btnXuatGhiChu_DM.Text = "Tìm ghi chú DM";
            this.btnXuatGhiChu_DM.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXuatGhiChu_DM.Click += new System.EventHandler(this.btnXuatGhiChu_DM_Click);
            // 
            // btnExportTL
            // 
            this.btnExportTL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportTL.Image = ((System.Drawing.Image)(resources.GetObject("btnExportTL.Image")));
            this.btnExportTL.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportTL.Location = new System.Drawing.Point(384, 16);
            this.btnExportTL.Name = "btnExportTL";
            this.btnExportTL.Size = new System.Drawing.Size(89, 23);
            this.btnExportTL.TabIndex = 316;
            this.btnExportTL.Text = "Xuất Excel";
            this.btnExportTL.VisualStyleManager = this.vsmMain;
            this.btnExportTL.Click += new System.EventHandler(this.btnExportTL_Click);
            // 
            // txtSoToKhaiTL
            // 
            this.txtSoToKhaiTL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhaiTL.Location = new System.Drawing.Point(131, 18);
            this.txtSoToKhaiTL.Name = "txtSoToKhaiTL";
            this.txtSoToKhaiTL.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhaiTL.Size = new System.Drawing.Size(152, 21);
            this.txtSoToKhaiTL.TabIndex = 315;
            this.txtSoToKhaiTL.Text = "0";
            this.txtSoToKhaiTL.Value = ((long)(0));
            this.txtSoToKhaiTL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int64;
            this.txtSoToKhaiTL.VisualStyleManager = this.vsmMain;
            this.txtSoToKhaiTL.TextChanged += new System.EventHandler(this.btnSearchTL_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label9.Location = new System.Drawing.Point(9, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 13);
            this.label9.TabIndex = 314;
            this.label9.Text = "Tìm đến tờ khai số:";
            // 
            // btnSearchTL
            // 
            this.btnSearchTL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchTL.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTL.Image")));
            this.btnSearchTL.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearchTL.Location = new System.Drawing.Point(289, 17);
            this.btnSearchTL.Name = "btnSearchTL";
            this.btnSearchTL.Size = new System.Drawing.Size(89, 23);
            this.btnSearchTL.TabIndex = 313;
            this.btnSearchTL.Text = "Tìm";
            this.btnSearchTL.VisualStyleManager = this.vsmMain;
            this.btnSearchTL.Click += new System.EventHandler(this.btnSearchTL_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageSize = new System.Drawing.Size(25, 25);
            this.btnDelete.Location = new System.Drawing.Point(6, 199);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(28, 23);
            this.btnDelete.TabIndex = 311;
            this.btnDelete.VisualStyleManager = this.vsmMain;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // uiButton2
            // 
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Image = ((System.Drawing.Image)(resources.GetObject("uiButton2.Image")));
            this.uiButton2.ImageSize = new System.Drawing.Size(25, 25);
            this.uiButton2.Location = new System.Drawing.Point(6, 170);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(28, 23);
            this.uiButton2.TabIndex = 310;
            this.uiButton2.VisualStyleManager = this.vsmMain;
            this.uiButton2.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(560, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(289, 14);
            this.label5.TabIndex = 312;
            this.label5.Text = "Danh sách tờ khai nhập đưa vào thanh khoản";
            // 
            // btnLayTKN
            // 
            this.btnLayTKN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLayTKN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayTKN.Image = ((System.Drawing.Image)(resources.GetObject("btnLayTKN.Image")));
            this.btnLayTKN.Location = new System.Drawing.Point(182, 14);
            this.btnLayTKN.Name = "btnLayTKN";
            this.btnLayTKN.Size = new System.Drawing.Size(170, 23);
            this.btnLayTKN.TabIndex = 313;
            this.btnLayTKN.Text = "Lấy TKN từ bảng phân bổ";
            this.btnLayTKN.VisualStyleManager = this.vsmMain;
            this.btnLayTKN.Click += new System.EventHandler(this.btnLayTKN_Click);
            // 
            // btnPhanBoTuDong
            // 
            this.btnPhanBoTuDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPhanBoTuDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhanBoTuDong.Image = ((System.Drawing.Image)(resources.GetObject("btnPhanBoTuDong.Image")));
            this.btnPhanBoTuDong.Location = new System.Drawing.Point(483, 14);
            this.btnPhanBoTuDong.Name = "btnPhanBoTuDong";
            this.btnPhanBoTuDong.Size = new System.Drawing.Size(120, 23);
            this.btnPhanBoTuDong.TabIndex = 315;
            this.btnPhanBoTuDong.Text = "Phân bổ tự động";
            this.btnPhanBoTuDong.VisualStyleManager = this.vsmMain;
            this.btnPhanBoTuDong.Click += new System.EventHandler(this.btnPhanBoTuDong_Click);
            // 
            // btnAddTKNFromDM
            // 
            this.btnAddTKNFromDM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddTKNFromDM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddTKNFromDM.Image = ((System.Drawing.Image)(resources.GetObject("btnAddTKNFromDM.Image")));
            this.btnAddTKNFromDM.Location = new System.Drawing.Point(6, 14);
            this.btnAddTKNFromDM.Name = "btnAddTKNFromDM";
            this.btnAddTKNFromDM.Size = new System.Drawing.Size(170, 23);
            this.btnAddTKNFromDM.TabIndex = 313;
            this.btnAddTKNFromDM.Text = "Lấy TKN Từ DM theo PO";
            this.btnAddTKNFromDM.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddTKNFromDM.Click += new System.EventHandler(this.btnAddTKNFromDM_Click);
            // 
            // btnGetTKN_From_DM
            // 
            this.btnGetTKN_From_DM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGetTKN_From_DM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetTKN_From_DM.Image = ((System.Drawing.Image)(resources.GetObject("btnGetTKN_From_DM.Image")));
            this.btnGetTKN_From_DM.Location = new System.Drawing.Point(358, 14);
            this.btnGetTKN_From_DM.Name = "btnGetTKN_From_DM";
            this.btnGetTKN_From_DM.Size = new System.Drawing.Size(121, 23);
            this.btnGetTKN_From_DM.TabIndex = 313;
            this.btnGetTKN_From_DM.Text = "Lấy TKN từ DM";
            this.btnGetTKN_From_DM.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGetTKN_From_DM.Click += new System.EventHandler(this.btnGetTKN_From_DM_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 73);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1187, 37);
            this.uiGroupBox1.TabIndex = 316;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.btnClose);
            this.uiGroupBox5.Controls.Add(this.btnFinish);
            this.uiGroupBox5.Controls.Add(this.btnBack);
            this.uiGroupBox5.Controls.Add(this.btnNext);
            this.uiGroupBox5.Controls.Add(this.btnAddTKNFromDM);
            this.uiGroupBox5.Controls.Add(this.btnPhanBoTuDong);
            this.uiGroupBox5.Controls.Add(this.btnXuatGhiChu_DM);
            this.uiGroupBox5.Controls.Add(this.btnLayTKN);
            this.uiGroupBox5.Controls.Add(this.btnGetTKN_From_DM);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 680);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1187, 43);
            this.uiGroupBox5.TabIndex = 317;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox6.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 110);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(518, 570);
            this.uiGroupBox6.TabIndex = 318;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.dgTKN);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 57);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(512, 510);
            this.uiGroupBox2.TabIndex = 320;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // dgTKN
            // 
            this.dgTKN.AlternatingColors = true;
            this.dgTKN.AutoEdit = true;
            this.dgTKN.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKN.ColumnAutoResize = true;
            dgTKN_DesignTimeLayout.LayoutString = resources.GetString("dgTKN_DesignTimeLayout.LayoutString");
            this.dgTKN.DesignTimeLayout = dgTKN_DesignTimeLayout;
            this.dgTKN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKN.GroupByBoxVisible = false;
            this.dgTKN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKN.ImageList = this.ImageList1;
            this.dgTKN.Location = new System.Drawing.Point(3, 8);
            this.dgTKN.Name = "dgTKN";
            this.dgTKN.RecordNavigator = true;
            this.dgTKN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKN.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKN.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKN.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKN.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKN.Size = new System.Drawing.Size(506, 499);
            this.dgTKN.TabIndex = 19;
            this.dgTKN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTKN.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKN_RowDoubleClick);
            this.dgTKN.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgTKNTL_DeletingRecords);
            this.dgTKN.RecordUpdated += new System.EventHandler(this.dgTKN_RecordUpdated);
            this.dgTKN.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKN_LoadingRow);
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.btnExport);
            this.uiGroupBox9.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox9.Controls.Add(this.btnGoTo);
            this.uiGroupBox9.Controls.Add(this.label2);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox9.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(512, 49);
            this.uiGroupBox9.TabIndex = 319;
            this.uiGroupBox9.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.uiButton2);
            this.uiGroupBox7.Controls.Add(this.btnDelete);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(518, 110);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(39, 570);
            this.uiGroupBox7.TabIndex = 319;
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.AutoScroll = true;
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox8.Controls.Add(this.uiGroupBox10);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox8.Location = new System.Drawing.Point(557, 110);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(630, 570);
            this.uiGroupBox8.TabIndex = 320;
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgTKNTL);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 55);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(624, 512);
            this.uiGroupBox3.TabIndex = 321;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgTKNTL
            // 
            this.dgTKNTL.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKNTL.AlternatingColors = true;
            this.dgTKNTL.AutoEdit = true;
            this.dgTKNTL.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKNTL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKNTL.ColumnAutoResize = true;
            dgTKNTL_DesignTimeLayout.LayoutString = resources.GetString("dgTKNTL_DesignTimeLayout.LayoutString");
            this.dgTKNTL.DesignTimeLayout = dgTKNTL_DesignTimeLayout;
            this.dgTKNTL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKNTL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKNTL.GroupByBoxVisible = false;
            this.dgTKNTL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKNTL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKNTL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKNTL.ImageList = this.ImageList1;
            this.dgTKNTL.Location = new System.Drawing.Point(3, 8);
            this.dgTKNTL.Name = "dgTKNTL";
            this.dgTKNTL.RecordNavigator = true;
            this.dgTKNTL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKNTL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKNTL.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKNTL.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKNTL.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKNTL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKNTL.Size = new System.Drawing.Size(618, 501);
            this.dgTKNTL.TabIndex = 19;
            this.dgTKNTL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTKNTL.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKNTL_RowDoubleClick);
            this.dgTKNTL.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgTKNTL_DeletingRecords);
            this.dgTKNTL.RecordUpdated += new System.EventHandler(this.dgTKNTL_RecordUpdated);
            this.dgTKNTL.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKNTL_LoadingRow);
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.AutoScroll = true;
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.btnExportTL);
            this.uiGroupBox10.Controls.Add(this.txtSoToKhaiTL);
            this.uiGroupBox10.Controls.Add(this.btnSearchTL);
            this.uiGroupBox10.Controls.Add(this.label9);
            this.uiGroupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox10.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(624, 47);
            this.uiGroupBox10.TabIndex = 320;
            this.uiGroupBox10.VisualStyleManager = this.vsmMain;
            // 
            // BK01WizardForm
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(1187, 723);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BK01WizardForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Bảng kê tờ khai nhập đưa vào thanh khoản";
            this.Load += new System.EventHandler(this.BK01WizardForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKNTL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton btnBack;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnFinish;
        private Janus.Windows.EditControls.UIButton btnNext;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.CalendarCombo.CalendarCombo ccToDate;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.CalendarCombo.CalendarCombo ccFromDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenChuHang;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIButton btnGoTo;
        private Janus.Windows.EditControls.UIButton btnLayTKN;
        private Janus.Windows.EditControls.UIButton btnPhanBoTuDong;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblChenhLechNgay;
        private Janus.Windows.EditControls.UIButton btnAddTKNFromDM;
        private Janus.Windows.EditControls.UIButton btnGetTKN_From_DM;
        private Janus.Windows.EditControls.UIButton btnXuatGhiChu_DM;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhaiTL;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIButton btnSearchTL;
        private Janus.Windows.EditControls.UIButton btnExportTL;
        private Janus.Windows.EditControls.UIButton btnExport;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgTKN;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX dgTKNTL;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
    }
}