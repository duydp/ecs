using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;



namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhThongQuanHangHoaXuatKhau_3 : DevExpress.XtraReports.UI.XtraReport
    {
        public int spl;
        public QuyetDinhThongQuanHangHoaXuatKhau_3()
        {
            InitializeComponent();
        }
        public void BindReport(VAE1LF0 vae1lf)
        {
            //lblSoTrang.Text = vae1lf.K69.GetValue().ToString();
            int HangHoa = vae1lf.HangMD.Count;
            if (HangHoa <= 2)
            {
                lblSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblSoTrang.Text = System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 2, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblSotokhai.Text = vae1lf.ECN.GetValue().ToString();
            lblSotokhaidautien.Text = vae1lf.FIC.GetValue().ToString();
            lblSonhanhtokhaichianho.Text = vae1lf.BNO.GetValue().ToString();
            lblTongsotokhaichianho.Text = vae1lf.DNO.GetValue().ToString();
            lblSotokhaitamnhaptaixuat.Text = vae1lf.TDN.GetValue().ToString();
            lblMaphanloaikiemtra.Text = vae1lf.K07.GetValue().ToString();
            lblMaloaihinh.Text = vae1lf.ECB.GetValue().ToString();
            lblMaphanloaihanghoa.Text = vae1lf.CCC.GetValue().ToString();
            lblMahieuphuongthucvanchuyen.Text = vae1lf.MTC.GetValue().ToString();
            lblMasothuedaidien.Text = vae1lf.K01.GetValue().ToString();
            lblTencoquanhaiquantiepnhantokhai.Text = vae1lf.K08.GetValue().ToString();
            lblNhomxulyhoso.Text = vae1lf.CHB.GetValue().ToString();
            if
            (Convert.ToDateTime(vae1lf.K10.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaydangky.Text = Convert.ToDateTime(vae1lf.K10.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaydangky.Text = "";
            }

            if (vae1lf.AD1.GetValue().ToString() != "" && vae1lf.AD1.GetValue().ToString() != "0")
            {
                lblGiodangky.Text = vae1lf.AD1.GetValue().ToString().Substring(0, 2) + ":" + vae1lf.AD1.GetValue().ToString().Substring(2, 2) + ":" + vae1lf.AD1.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiodangky.Text = "";
            }

            if
            (Convert.ToDateTime(vae1lf.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaythaydoidangky.Text = Convert.ToDateTime(vae1lf.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaythaydoidangky.Text = "";
            }
            if (vae1lf.AD3.GetValue().ToString() != "" && vae1lf.AD3.GetValue().ToString() != "0")
            {
                lblGiothaydoidangky.Text = vae1lf.AD3.GetValue().ToString().Substring(0, 2) + ":" + vae1lf.AD3.GetValue().ToString().Substring(2, 2) + ":" + vae1lf.AD3.GetValue().ToString().Substring(4, 2);
            }
               
                else
                {
                    lblGiothaydoidangky.Text = "";
                }
                
             

            if
            (Convert.ToDateTime(vae1lf.RID.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoihantainhapxuat.Text = Convert.ToDateTime(vae1lf.RID.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoihantainhapxuat.Text = "";
            }
            lblBieuthitruonghophethan.Text = vae1lf.AAA.GetValue().ToString();
            #region bind hang
            DataTable dt = new DataTable();
            dt.Columns.Add("Sodong1", typeof(string));
            dt.Columns.Add("Masohanghoa", typeof(string));
            dt.Columns.Add("Maquanlyrieng", typeof(string));
            dt.Columns.Add("Mataixacnhangia", typeof(string));
            dt.Columns.Add("Motahanghoa", typeof(string));
            dt.Columns.Add("Soluong1", typeof(string));
            dt.Columns.Add("Madvtinh1", typeof(string));
            dt.Columns.Add("Soluong2", typeof(string));
            dt.Columns.Add("Madvtinh2", typeof(string));
            dt.Columns.Add("Trigia", typeof(string));
            dt.Columns.Add("Dongia", typeof(string));
            dt.Columns.Add("Madongtien", typeof(string));
            dt.Columns.Add("Donvidongia", typeof(string));
            dt.Columns.Add("TrigiathueS", typeof(string));
            dt.Columns.Add("MadongtienS", typeof(string));
            dt.Columns.Add("MadongtienM", typeof(string));
            dt.Columns.Add("TrigiathueM", typeof(string));
            dt.Columns.Add("Soluongthue", typeof(string));
            dt.Columns.Add("Madvtinhchuanthue", typeof(string));
            dt.Columns.Add("Dongiathue", typeof(string));
            dt.Columns.Add("Matientedongiathue", typeof(string));
            dt.Columns.Add("Donvisoluongthue", typeof(string));
            dt.Columns.Add("Thuesuat", typeof(string));
            dt.Columns.Add("Phanloainhapthue", typeof(string));
            dt.Columns.Add("Sotienthue", typeof(string));
            dt.Columns.Add("Matientesotienthue", typeof(string));
            dt.Columns.Add("Sotienmiengiam", typeof(string));
            dt.Columns.Add("Matientesotiengiamthue", typeof(string));
            dt.Columns.Add("Sodonghangtaixuat", typeof(string));
            dt.Columns.Add("Danhmucmienthue", typeof(string));
            dt.Columns.Add("Sodongmienthue", typeof(string));
            dt.Columns.Add("Tienlephi", typeof(string));
            dt.Columns.Add("Tienbaohiem", typeof(string));
            dt.Columns.Add("Soluongtienlephi", typeof(string));
            dt.Columns.Add("Madvtienlephi", typeof(string));
            dt.Columns.Add("Soluongtienbaohiem", typeof(string));
            dt.Columns.Add("Madvtienbaohiem", typeof(string));
            dt.Columns.Add("Khoantienlephi", typeof(string));
            dt.Columns.Add("Khoantienbaohiem", typeof(string));
            dt.Columns.Add("Mavanvanphapquy1", typeof(string));
            dt.Columns.Add("Mavanvanphapquy2", typeof(string));
            dt.Columns.Add("Mavanvanphapquy3", typeof(string));
            dt.Columns.Add("Mavanvanphapquy4", typeof(string));
            dt.Columns.Add("Mavanvanphapquy5", typeof(string));
            dt.Columns.Add("Mamiengiam", typeof(string));
            dt.Columns.Add("Dieukhoanmien", typeof(string));

            //int begin = spl * 2;
            //int end = spl * 2 + 2;
            for (int i = 0; i < vae1lf.HangMD.Count; i++)
            {
                //if (i < vae1lf.HangMD.Count)
                //{
                    DataRow dr = dt.NewRow();

                    dr["Sodong1"] = vae1lf.HangMD[i].R01.GetValue().ToString();
                    dr["Masohanghoa"] = vae1lf.HangMD[i].CMD.GetValue().ToString();
                    dr["Maquanlyrieng"] = vae1lf.HangMD[i].COC.GetValue().ToString();
                    dr["Mataixacnhangia"] = vae1lf.HangMD[i].R03.GetValue().ToString();
                    dr["Motahanghoa"] = vae1lf.HangMD[i].CMN.GetValue().ToString();
                    dr["Soluong1"] = vae1lf.HangMD[i].QN1.GetValue().ToString();
                    dr["Madvtinh1"] = vae1lf.HangMD[i].QT1.GetValue().ToString();
                    dr["Soluong2"] = vae1lf.HangMD[i].QN2.GetValue().ToString();
                    dr["Madvtinh2"] = vae1lf.HangMD[i].QT2.GetValue().ToString();
                    dr["Trigia"] = vae1lf.HangMD[i].BPR.GetValue().ToString();
                    dr["Dongia"] = vae1lf.HangMD[i].UPR.GetValue().ToString();
                    dr["Madongtien"] = vae1lf.HangMD[i].UPC.GetValue().ToString();
                    dr["Donvidongia"] = vae1lf.HangMD[i].TSC.GetValue().ToString();
                    dr["TrigiathueS"] = vae1lf.HangMD[i].R07.GetValue().ToString();
                    dr["MadongtienS"] = vae1lf.HangMD[i].AD9.GetValue().ToString();
                    dr["MadongtienM"] = vae1lf.HangMD[i].R14.GetValue().ToString();
                    dr["TrigiathueM"] = vae1lf.HangMD[i].R15.GetValue().ToString();
                    dr["Soluongthue"] = vae1lf.HangMD[i].TSQ.GetValue().ToString();
                    dr["Madvtinhchuanthue"] = vae1lf.HangMD[i].TSU.GetValue().ToString();
                    dr["Dongiathue"] = vae1lf.HangMD[i].CVU.GetValue().ToString();
                    dr["Matientedongiathue"] = vae1lf.HangMD[i].ADA.GetValue().ToString();
                    dr["Donvisoluongthue"] = vae1lf.HangMD[i].QCV.GetValue().ToString();
                    dr["Thuesuat"] = vae1lf.HangMD[i].TRA.GetValue().ToString();
                    dr["Phanloainhapthue"] = vae1lf.HangMD[i].TRM.GetValue().ToString();
                    dr["Sotienthue"] = vae1lf.HangMD[i].TAX.GetValue().ToString();
                    dr["Matientesotienthue"] = vae1lf.HangMD[i].ADB.GetValue().ToString();
                    dr["Sotienmiengiam"] = vae1lf.HangMD[i].REG.GetValue().ToString();
                    dr["Matientesotiengiamthue"] = vae1lf.HangMD[i].ADC.GetValue().ToString();
                    dr["Sodonghangtaixuat"] = vae1lf.HangMD[i].TDL.GetValue().ToString();
                    dr["Danhmucmienthue"] = vae1lf.HangMD[i].TXN.GetValue().ToString();
                    dr["Sodongmienthue"] = vae1lf.HangMD[i].TXR.GetValue().ToString();
                    dr["Tienlephi"] = vae1lf.HangMD[i].CUP.GetValue().ToString();
                    dr["Tienbaohiem"] = vae1lf.HangMD[i].IUP.GetValue().ToString();
                    dr["Soluongtienlephi"] = vae1lf.HangMD[i].CQU.GetValue().ToString();
                    dr["Madvtienlephi"] = vae1lf.HangMD[i].CQC.GetValue().ToString();
                    dr["Soluongtienbaohiem"] = vae1lf.HangMD[i].IQU.GetValue().ToString();
                    dr["Madvtienbaohiem"] = vae1lf.HangMD[i].IQC.GetValue().ToString();
                    dr["Khoantienlephi"] = vae1lf.HangMD[i].CPR.GetValue().ToString();
                    dr["Khoantienbaohiem"] = vae1lf.HangMD[i].IPR.GetValue().ToString();
                    dr["Mavanvanphapquy1"] = vae1lf.HangMD[i].OL_.listAttribute[0].GetValueCollection(0).ToString();
                    dr["Mavanvanphapquy2"] = vae1lf.HangMD[i].OL_.listAttribute[0].GetValueCollection(1).ToString();
                    dr["Mavanvanphapquy3"] = vae1lf.HangMD[i].OL_.listAttribute[0].GetValueCollection(2).ToString();
                    dr["Mavanvanphapquy4"] = vae1lf.HangMD[i].OL_.listAttribute[0].GetValueCollection(3).ToString();
                    dr["Mavanvanphapquy5"] = vae1lf.HangMD[i].OL_.listAttribute[0].GetValueCollection(4).ToString();
                    dr["Mamiengiam"] = vae1lf.HangMD[i].RE.GetValue().ToString();
                    dr["Dieukhoanmien"] = vae1lf.HangMD[i].TRL.GetValue().ToString();
                    dt.Rows.Add(dr);
                }
                
            //}
            BindReportHang(dt);
            #endregion
        }
        public void BindReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSodong1.DataBindings.Add("Text", DetailReport.DataSource, "Sodong1");
            lblMasohanghoa.DataBindings.Add("Text", DetailReport.DataSource, "Masohanghoa");
            lblMaquanlyrieng.DataBindings.Add("Text", DetailReport.DataSource, "Maquanlyrieng");
            lblMataixacnhangia.DataBindings.Add("Text", DetailReport.DataSource, "Mataixacnhangia");
            lblMotahanghoa.DataBindings.Add("Text", DetailReport.DataSource, "Motahanghoa");
            lblSoluong1.DataBindings.Add("Text", DetailReport.DataSource, "Soluong1");
            lblMadvtinh1.DataBindings.Add("Text", DetailReport.DataSource, "Madvtinh1");
            lblSoluong2.DataBindings.Add("Text", DetailReport.DataSource, "Soluong2");
            lblMadvtinh2.DataBindings.Add("Text", DetailReport.DataSource, "Madvtinh2");
            lblTrigia.DataBindings.Add("Text", DetailReport.DataSource, "Trigia");
            lblDongia.DataBindings.Add("Text", DetailReport.DataSource, "Dongia");
            lblMadongtien.DataBindings.Add("Text", DetailReport.DataSource, "Madongtien");
            lblDonvidongia.DataBindings.Add("Text", DetailReport.DataSource, "Donvidongia");
            lblTrigiathueS.DataBindings.Add("Text", DetailReport.DataSource, "TrigiathueS");
            lblMadongtienS.DataBindings.Add("Text", DetailReport.DataSource, "MadongtienS");
            lblMadongtienM.DataBindings.Add("Text", DetailReport.DataSource, "MadongtienM");
            lblTrigiathueM.DataBindings.Add("Text", DetailReport.DataSource, "TrigiathueM");
            lblSoluongthue.DataBindings.Add("Text", DetailReport.DataSource, "Soluongthue");
            lblMadvtinhchuanthue.DataBindings.Add("Text", DetailReport.DataSource, "Madvtinhchuanthue");
            lblDongiathue.DataBindings.Add("Text", DetailReport.DataSource, "Dongiathue");
            lblMatientedongiathue.DataBindings.Add("Text", DetailReport.DataSource, "Matientedongiathue");
            lblDonvisoluongthue.DataBindings.Add("Text", DetailReport.DataSource, "Donvisoluongthue");
            lblThuesuat.DataBindings.Add("Text", DetailReport.DataSource, "Thuesuat");
            lblPhanloainhapthue.DataBindings.Add("Text", DetailReport.DataSource, "Phanloainhapthue");
            lblSotienthue.DataBindings.Add("Text", DetailReport.DataSource, "Sotienthue");
            lblMatientesotienthue.DataBindings.Add("Text", DetailReport.DataSource, "Matientesotienthue");
            lblSotienmiengiam.DataBindings.Add("Text", DetailReport.DataSource, "Sotienmiengiam");
            lblMatientesotiengiamthue.DataBindings.Add("Text", DetailReport.DataSource, "Matientesotiengiamthue");
            lblSodonghangtaixuat.DataBindings.Add("Text", DetailReport.DataSource, "Sodonghangtaixuat");
            lblDanhmucmienthue.DataBindings.Add("Text", DetailReport.DataSource, "Danhmucmienthue");
            lblSodongmienthue.DataBindings.Add("Text", DetailReport.DataSource, "Sodongmienthue");
            lblTienlephi.DataBindings.Add("Text", DetailReport.DataSource, "Tienlephi");
            lblTienbaohiem.DataBindings.Add("Text", DetailReport.DataSource, "Tienbaohiem");
            lblSoluongtienlephi.DataBindings.Add("Text", DetailReport.DataSource, "Soluongtienlephi");
            lblMadvtienlephi.DataBindings.Add("Text", DetailReport.DataSource, "Madvtienlephi");
            lblSoluongtienbaohiem.DataBindings.Add("Text", DetailReport.DataSource, "Soluongtienbaohiem");
            lblMadvtienbaohiem.DataBindings.Add("Text", DetailReport.DataSource, "Madvtienbaohiem");
            lblKhoantienlephi.DataBindings.Add("Text", DetailReport.DataSource, "Khoantienlephi");
            lblKhoantienbaohiem.DataBindings.Add("Text", DetailReport.DataSource, "Khoantienbaohiem");
            lblMavanvanphapquy1.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy1");
            lblMavanvanphapquy2.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy2");
            lblMavanvanphapquy3.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy3");
            lblMavanvanphapquy4.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy4");
            lblMavanvanphapquy5.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy5");
            lblMamiengiam.DataBindings.Add("Text", DetailReport.DataSource, "Mamiengiam");
            lblDieukhoanmien.DataBindings.Add("Text", DetailReport.DataSource, "Dieukhoanmien");

        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
        }

        private void lblTrang_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            cell.Text = (e.PageIndex + 3).ToString();
        }


        //public void BindingReport(VAE1LF0_HANG hmd) 
        //{


        //    lblMasohanghoa.Text = hmd.CMD.GetValue().ToString();
        //    lblSodong1.Text = hmd.R01.GetValue().ToString();
        //    lblMasohanghoa.Text = hmd.CMD.GetValue().ToString();
        //    lblMaquanlyrieng.Text = hmd.COC.GetValue().ToString();
        //    lblMataixacnhangia.Text = hmd.R03.GetValue().ToString();
        //    lblMotahanghoa.Text = hmd.CMN.GetValue().ToString();
        //    lblSoluong1.Text = hmd.QN1.GetValue().ToString();
        //    lblMadvtinh1.Text = hmd.QT1.GetValue().ToString();
        //    lblSoluong2.Text = hmd.QN2.GetValue().ToString();
        //    lblMadvtinh2.Text = hmd.QT2.GetValue().ToString();
        //    lblTrigia.Text = hmd.BPR.GetValue().ToString();
        //    lblDongia.Text = hmd.UPR.GetValue().ToString();
        //    lblMadongtien.Text = hmd.UPC.GetValue().ToString();
        //    lblDonvidongia.Text = hmd.TSC.GetValue().ToString();
        //    lblTrigiathueS.Text = hmd.R07.GetValue().ToString();
        //    lblMadongtienS.Text = hmd.AD9.GetValue().ToString();
        //    lblMadongtienM.Text = hmd.R14.GetValue().ToString();
        //    lblTrigiathueM.Text = hmd.R15.GetValue().ToString();
        //    lblSoluongthue.Text = hmd.TSQ.GetValue().ToString();
        //    lblMadvtinhchuanthue.Text = hmd.TSU.GetValue().ToString();
        //    lblDongiathue.Text = hmd.CVU.GetValue().ToString();
        //    lblMatientedongiathue.Text = hmd.ADA.GetValue().ToString();
        //    lblDonvisoluongthue.Text = hmd.QCV.GetValue().ToString();
        //    lblThuesuat.Text = hmd.TRA.GetValue().ToString();
        //    lblPhanloainhapthue.Text = hmd.TRM.GetValue().ToString();
        //    lblSotienthue.Text = hmd.TAX.GetValue().ToString();
        //    lblMatientesotienthue.Text = hmd.ADB.GetValue().ToString();
        //    lblSotienmiengiam.Text = hmd.REG.GetValue().ToString();
        //    lblMatientesotiengiamthue.Text = hmd.ADC.GetValue().ToString();
        //    lblSodonghangtaixuat.Text = hmd.TDL.GetValue().ToString();
        //    lblDanhmucmienthue.Text = hmd.TXN.GetValue().ToString();
        //    lblSodongmienthue.Text = hmd.TXR.GetValue().ToString();
        //    lblTienlephi.Text = hmd.CUP.GetValue().ToString();
        //    lblTienbaohiem.Text = hmd.IUP.GetValue().ToString();
        //    lblSoluongtienlephi.Text = hmd.CQU.GetValue().ToString();
        //    lblMadvtienlephi.Text = hmd.CQC.GetValue().ToString();
        //    lblSoluongtienbaohiem.Text = hmd.IQU.GetValue().ToString();
        //    lblMadvtienbaohiem.Text = hmd.IQC.GetValue().ToString();
        //    lblKhoantienlephi.Text = hmd.CPR.GetValue().ToString();
        //    lblKhoantienbaohiem.Text = hmd.IPR.GetValue().ToString();

        //    lblMamiengiam.Text = hmd.RE.GetValue().ToString();
        //    lblDieukhoanmien.Text = hmd.TRL.GetValue().ToString();

        //    for (int i = 0; i < hmd.OL_.listAttribute[0].ListValue.Count; i++)
        //    {
        //        switch (i)
        //        {
        //            case 0:
        //                lblMavanvanphapquy1.Text = hmd.OL_.listAttribute[0].GetValueCollection(i).ToString();
        //                break;
        //            case 1:
        //                lblMavanvanphapquy2.Text = hmd.OL_.listAttribute[0].GetValueCollection(i).ToString();
        //                break;
        //            case 2:
        //                lblMavanvanphapquy3.Text = hmd.OL_.listAttribute[0].GetValueCollection(i).ToString();
        //                break;
        //            case 3:
        //                lblMavanvanphapquy4.Text = hmd.OL_.listAttribute[0].GetValueCollection(i).ToString();

        //                break;
        //            case 4:
        //                lblMavanvanphapquy5.Text = hmd.OL_.listAttribute[0].GetValueCollection(i).ToString();
        //                break;

        //        }

        //    }


        //}
    }
}
