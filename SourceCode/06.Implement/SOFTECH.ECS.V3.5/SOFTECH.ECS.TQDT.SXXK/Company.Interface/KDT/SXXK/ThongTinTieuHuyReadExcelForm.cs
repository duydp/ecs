﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.KDT.SXXK
{
    public partial class ThongTinTieuHuyReadExcelForm : Company.Interface.BaseForm
    {
        public KDT_VNACCS_ScrapInformation scrapInformation;
        public KDT_VNACCS_ScrapInformation_Detail scrapInformation_Detail ;
        public List<KDT_VNACCS_ScrapInformation_Detail> Collection = new List<KDT_VNACCS_ScrapInformation_Detail>();
        public ThongTinTieuHuyReadExcelForm()
        {
            InitializeComponent();
        }

        private void ThongTinTieuHuyReadExcelForm_Load(object sender, EventArgs e)
        {

        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    MLMessages("LỖI KHI CHỌN FILE. DOANH NGHIỆP HÃY KIỂM TRA LẠI ĐƯỜNG DẪN HOẶC ĐÓNG FILE TRƯỚC KHI ĐỌC.", "MSG_EXC03", "", false);
                    return null;
                }
                List<String> Collection = new List<string>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                MLMessages("LỖI KHI ĐỌC SHEET. DOANH NGHIỆP HÃY KIỂM TRA LẠI TÊN SHEET TRƯỚC KHI CHỌN FILE.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog sf = new OpenFileDialog();
                sf.ShowDialog(this);
                txtFilePath.Text = sf.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int beginRow = Convert.ToInt32(txtRow.Value) -1 ;
            if (beginRow < 0)
            {
                return;
            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                MLMessages("LỖI KHI CHỌN FILE. DOANH NGHIỆP HÃY KIỂM TRA LẠI ĐƯỜNG DẪN HOẶC ĐÓNG FILE TRƯỚC KHI ĐỌC.", "MSG_EXC03", "", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("KHÔNG TỒN TẠI SHEET \"" + cbbSheetName.Text + "\"MSG_EXC01", "", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;

            char MaHHColumn = Convert.ToChar(txtMaHangColumn.Text.Trim());
            int MaHHCol = ConvertCharToInt(MaHHColumn);

            char TenHHColumn = Convert.ToChar(txtTenHangColumn.Text.Trim());
            int TenHHCol = ConvertCharToInt(TenHHColumn);

            char DVTColumn = Convert.ToChar(txtDVTColumn.Text.Trim());
            int DVTCol = ConvertCharToInt(DVTColumn);

            char LuongTieuHuyColumn = Convert.ToChar(txtSLTHColumn.Text.Trim());
            int LuongTieuHuyCol = ConvertCharToInt(LuongTieuHuyColumn);

            char LoaiHHColumn = Convert.ToChar(txtLoaiHHColumn.Text.Trim());
            int LoaiHHCol = ConvertCharToInt(LoaiHHColumn);

            string errorTotal = "";
            string errorMaHangHoa = "";
            string errorTenHangHoa = "";
            string errorDVT = "";
            string errorDVTExits = "";
            string errorLoaiHangHoa = "";
            string errorSoLuong = "";
            string errorSoLuongValid = "";
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        Collection = new List<KDT_VNACCS_ScrapInformation_Detail>();
                        KDT_VNACCS_ScrapInformation_Detail scrapInformationDetail = new KDT_VNACCS_ScrapInformation_Detail();
                        bool isAdd = true;
                        bool isExits = false;
                        try
                        {
                            scrapInformationDetail.MaHangHoa = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                            if (scrapInformationDetail.MaHangHoa.Trim().Length == 0)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + scrapInformationDetail.MaHangHoa + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + scrapInformationDetail.MaHangHoa + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            scrapInformationDetail.TenHangHoa = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                            if (scrapInformationDetail.TenHangHoa.Trim().Length == 0)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + scrapInformationDetail.TenHangHoa + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + scrapInformationDetail.TenHangHoa + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            scrapInformationDetail.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                            if (scrapInformationDetail.DVT.Trim().Length == 0)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + scrapInformationDetail.DVT + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                List<VNACC_Category_QuantityUnit> DVTCollection = VNACC_Category_QuantityUnit.SelectCollectionAll();
                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                {
                                    if (item.Code == scrapInformationDetail.DVT)
                                    {
                                        isExits = true;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + scrapInformationDetail.DVT + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + scrapInformationDetail.DVT + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            scrapInformationDetail.LoaiHangHoa = Convert.ToDecimal(wsr.Cells[LoaiHHCol].Value);
                            if (scrapInformationDetail.LoaiHangHoa.ToString().Trim().Length == 0)
                            {
                                errorLoaiHangHoa += "[" + (wsr.Index + 1) + "]-[" + scrapInformationDetail.LoaiHangHoa + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorLoaiHangHoa += "[" + (wsr.Index + 1) + "]-[" + scrapInformationDetail.LoaiHangHoa + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            scrapInformationDetail.SoLuong = Convert.ToDecimal(wsr.Cells[LuongTieuHuyCol].Value);
                            if (scrapInformationDetail.SoLuong.ToString().Trim().Length == 0)
                            {
                                errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + scrapInformationDetail.SoLuong + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(scrapInformationDetail.SoLuong, 4) != scrapInformationDetail.SoLuong)
                                {
                                    errorSoLuongValid += "[" + (wsr.Index + 1) + "]-[" + scrapInformationDetail.SoLuong + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + scrapInformationDetail.SoLuong + "]\n";
                            isAdd = false;
                        }
                        if (isAdd)
                            Collection.Add(scrapInformationDetail);
                    }
                    catch (Exception ex)
                    {
                        return;
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorMaHangHoa))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorTenHangHoa))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVT + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorLoaiHangHoa))
                errorTotal += "\n - [STT] -[LOẠI HÀNG HÓA] : \n" + errorLoaiHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG . NGUYÊN PHỤ LIỆU NHẬP : 1 , SẢN PHẨM NHẬP : 2\n";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVTExits + " KHÔNG HỢP LỆ. ĐVT PHẢI LÀ ĐVT VNACCS (VÍ DỤ : CÁI/CHIẾC LÀ : PCE)\n";
            if (!String.IsNullOrEmpty(errorSoLuong))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG HỦY] : \n" + errorSoLuong + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorSoLuongValid))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG HỦY] : \n" + errorSoLuongValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (String.IsNullOrEmpty(errorTotal))
            {
                foreach (KDT_VNACCS_ScrapInformation_Detail goodItem in Collection)
                {
                    scrapInformation.ScrapInformationCollection.Add(goodItem);
                }
                ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG","NHẬP HÀNG TỪ EXCEL THÀNH CÔNG .", false);
            }
            else
            {
                ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG","NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG ." + errorTotal, false);
                return;
            }
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkExcelMau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_ScrapInformation();
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
