﻿namespace Company.Interface.VNACCS
{
    partial class VNACC_TIA_HangForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grListHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TIA_HangForm));
            this.UIGroup = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaSoHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.ctrSoLuongDaTNTX = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrSoLuongBanDau = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoLuongDaTNTX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongBanDau = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListHang = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UIGroup)).BeginInit();
            this.UIGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListHang)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 393), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 393);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 369);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 369);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.UIGroup);
            this.grbMain.Size = new System.Drawing.Size(634, 393);
            // 
            // UIGroup
            // 
            this.UIGroup.BackColor = System.Drawing.Color.Transparent;
            this.UIGroup.Controls.Add(this.ctrMaSoHang);
            this.UIGroup.Controls.Add(this.btnGhi);
            this.UIGroup.Controls.Add(this.ctrSoLuongDaTNTX);
            this.UIGroup.Controls.Add(this.ctrSoLuongBanDau);
            this.UIGroup.Controls.Add(this.txtSoLuongDaTNTX);
            this.UIGroup.Controls.Add(this.txtSoLuongBanDau);
            this.UIGroup.Controls.Add(this.label3);
            this.UIGroup.Controls.Add(this.label4);
            this.UIGroup.Controls.Add(this.label5);
            this.UIGroup.Controls.Add(this.label6);
            this.UIGroup.Controls.Add(this.label2);
            this.UIGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.UIGroup.Location = new System.Drawing.Point(0, 0);
            this.UIGroup.Name = "UIGroup";
            this.UIGroup.Size = new System.Drawing.Size(634, 153);
            this.UIGroup.TabIndex = 2;
            this.UIGroup.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaSoHang
            // 
            this.ctrMaSoHang.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaSoHang.Appearance.Options.UseBackColor = true;
            this.ctrMaSoHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ctrMaSoHang.Code = "";
            this.ctrMaSoHang.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaSoHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaSoHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaSoHang.IsOnlyWarning = false;
            this.ctrMaSoHang.IsValidate = true;
            this.ctrMaSoHang.Location = new System.Drawing.Point(188, 13);
            this.ctrMaSoHang.Name = "ctrMaSoHang";
            this.ctrMaSoHang.Name_VN = "";
            this.ctrMaSoHang.SetOnlyWarning = false;
            this.ctrMaSoHang.SetValidate = false;
            this.ctrMaSoHang.ShowColumnCode = true;
            this.ctrMaSoHang.ShowColumnName = false;
            this.ctrMaSoHang.Size = new System.Drawing.Size(160, 21);
            this.ctrMaSoHang.TabIndex = 80;
            this.ctrMaSoHang.TagName = "";
            this.ctrMaSoHang.Where = null;
            this.ctrMaSoHang.WhereCondition = "";
            // 
            // btnGhi
            // 
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = ((System.Drawing.Image)(resources.GetObject("btnGhi.Image")));
            this.btnGhi.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGhi.Location = new System.Drawing.Point(188, 116);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(70, 23);
            this.btnGhi.TabIndex = 7;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // ctrSoLuongDaTNTX
            // 
            this.ctrSoLuongDaTNTX.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrSoLuongDaTNTX.Appearance.Options.UseBackColor = true;
            this.ctrSoLuongDaTNTX.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrSoLuongDaTNTX.Code = "";
            this.ctrSoLuongDaTNTX.ColorControl = System.Drawing.Color.Empty;
            this.ctrSoLuongDaTNTX.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrSoLuongDaTNTX.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrSoLuongDaTNTX.IsOnlyWarning = false;
            this.ctrSoLuongDaTNTX.IsValidate = true;
            this.ctrSoLuongDaTNTX.Location = new System.Drawing.Point(371, 78);
            this.ctrSoLuongDaTNTX.Name = "ctrSoLuongDaTNTX";
            this.ctrSoLuongDaTNTX.Name_VN = "";
            this.ctrSoLuongDaTNTX.SetOnlyWarning = false;
            this.ctrSoLuongDaTNTX.SetValidate = false;
            this.ctrSoLuongDaTNTX.ShowColumnCode = true;
            this.ctrSoLuongDaTNTX.ShowColumnName = false;
            this.ctrSoLuongDaTNTX.Size = new System.Drawing.Size(79, 21);
            this.ctrSoLuongDaTNTX.TabIndex = 5;
            this.ctrSoLuongDaTNTX.TagName = "";
            this.ctrSoLuongDaTNTX.Where = null;
            this.ctrSoLuongDaTNTX.WhereCondition = "";
            // 
            // ctrSoLuongBanDau
            // 
            this.ctrSoLuongBanDau.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrSoLuongBanDau.Appearance.Options.UseBackColor = true;
            this.ctrSoLuongBanDau.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrSoLuongBanDau.Code = "";
            this.ctrSoLuongBanDau.ColorControl = System.Drawing.Color.Empty;
            this.ctrSoLuongBanDau.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrSoLuongBanDau.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrSoLuongBanDau.IsOnlyWarning = false;
            this.ctrSoLuongBanDau.IsValidate = true;
            this.ctrSoLuongBanDau.Location = new System.Drawing.Point(370, 48);
            this.ctrSoLuongBanDau.Name = "ctrSoLuongBanDau";
            this.ctrSoLuongBanDau.Name_VN = "";
            this.ctrSoLuongBanDau.SetOnlyWarning = false;
            this.ctrSoLuongBanDau.SetValidate = false;
            this.ctrSoLuongBanDau.ShowColumnCode = true;
            this.ctrSoLuongBanDau.ShowColumnName = false;
            this.ctrSoLuongBanDau.Size = new System.Drawing.Size(79, 21);
            this.ctrSoLuongBanDau.TabIndex = 2;
            this.ctrSoLuongBanDau.TagName = "";
            this.ctrSoLuongBanDau.Where = null;
            this.ctrSoLuongBanDau.WhereCondition = "";
            // 
            // txtSoLuongDaTNTX
            // 
            this.txtSoLuongDaTNTX.DecimalDigits = 20;
            this.txtSoLuongDaTNTX.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongDaTNTX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongDaTNTX.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongDaTNTX.Location = new System.Drawing.Point(188, 77);
            this.txtSoLuongDaTNTX.MaxLength = 15;
            this.txtSoLuongDaTNTX.Name = "txtSoLuongDaTNTX";
            this.txtSoLuongDaTNTX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongDaTNTX.Size = new System.Drawing.Size(147, 21);
            this.txtSoLuongDaTNTX.TabIndex = 4;
            this.txtSoLuongDaTNTX.Text = "0";
            this.txtSoLuongDaTNTX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongDaTNTX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongDaTNTX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuongBanDau
            // 
            this.txtSoLuongBanDau.DecimalDigits = 20;
            this.txtSoLuongBanDau.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongBanDau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongBanDau.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongBanDau.Location = new System.Drawing.Point(188, 47);
            this.txtSoLuongBanDau.MaxLength = 15;
            this.txtSoLuongBanDau.Name = "txtSoLuongBanDau";
            this.txtSoLuongBanDau.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongBanDau.Size = new System.Drawing.Size(147, 21);
            this.txtSoLuongBanDau.TabIndex = 1;
            this.txtSoLuongBanDau.Text = "0";
            this.txtSoLuongBanDau.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongBanDau.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongBanDau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(341, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 56;
            this.label3.Text = "ĐVT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(341, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 55;
            this.label4.Text = "ĐVT";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 13);
            this.label5.TabIndex = 54;
            this.label5.Text = "Số lượng đã Tạm nhập / Tái xuất";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 13);
            this.label6.TabIndex = 53;
            this.label6.Text = "Số lượng ban đầu";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 48;
            this.label2.Text = "Mã số hàng hóa";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(553, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(477, 15);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(70, 23);
            this.btnXoa.TabIndex = 8;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 346);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(634, 47);
            this.uiGroupBox1.TabIndex = 4;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.grListHang);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 153);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(634, 193);
            this.uiGroupBox2.TabIndex = 5;
            this.uiGroupBox2.Text = "Danh sách hàng hóa";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // grListHang
            // 
            this.grListHang.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHang.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListHang.ColumnAutoResize = true;
            grListHang_DesignTimeLayout.LayoutString = resources.GetString("grListHang_DesignTimeLayout.LayoutString");
            this.grListHang.DesignTimeLayout = grListHang_DesignTimeLayout;
            this.grListHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListHang.GroupByBoxVisible = false;
            this.grListHang.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListHang.Location = new System.Drawing.Point(3, 17);
            this.grListHang.Name = "grListHang";
            this.grListHang.RecordNavigator = true;
            this.grListHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHang.Size = new System.Drawing.Size(628, 173);
            this.grListHang.TabIndex = 1;
            this.grListHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListHang.VisualStyleManager = this.vsmMain;
            this.grListHang.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListHang_RowDoubleClick);
            // 
            // VNACC_TIA_HangForm
            // 
            this.ClientSize = new System.Drawing.Size(840, 399);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_TIA_HangForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin hàng hóa ";
            this.Load += new System.EventHandler(this.VNACC_TIA_HangForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UIGroup)).EndInit();
            this.UIGroup.ResumeLayout(false);
            this.UIGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListHang)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox UIGroup;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrSoLuongDaTNTX;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrSoLuongBanDau;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongDaTNTX;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongBanDau;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX grListHang;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaSoHang;
    }
}
