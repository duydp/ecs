﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class LocationNameBase
    {
       /// <summary>
       /// Tên khu vực, quốc gia
       /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }
       /// <summary>
       /// Mã quốc gia
       /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }
    }
}
