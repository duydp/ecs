﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL.SXXK;
using DonViHaiQuanControl=Company.Interface.Controls.DonViHaiQuanControl;

namespace Company.Interface.GC
{
    partial class PhieuXuatKhoForm
    {
        private UIGroupBox uiGroupBox3;
        private ImageList ImageList1;
        private IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhieuXuatKhoForm));
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrDVT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtPO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtStyle = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoPhieu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtXuatTaiKho = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDonViNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtKhachHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.clcNgayXuatKho = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label9 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDong = new Janus.Windows.EditControls.UIButton();
            this.btnLuu = new Janus.Windows.EditControls.UIButton();
            this.btnIn = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbDonViTinh = new Janus.Windows.EditControls.UIComboBox();
            this.txtThucNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhanTram = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtLuong1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtNhuCau = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDinhMuc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnThem = new Janus.Windows.EditControls.UIButton();
            this.txtMoTaHH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMa1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNPL2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSize = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNhaCC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMauSac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtArt = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnXuatNhapTon = new Janus.Windows.EditControls.UIButton();
            this.label24 = new System.Windows.Forms.Label();
            this.txtSoHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label25 = new System.Windows.Forms.Label();
            this.clcNgayHoaDon = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.BackColor = System.Drawing.Color.MistyRose;
            this.grbMain.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.grbMain.Controls.Add(this.btnXuatNhapTon);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnIn);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.btnDong);
            this.grbMain.Controls.Add(this.btnLuu);
            this.grbMain.Size = new System.Drawing.Size(1016, 579);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgList);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(9, 161);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(960, 217);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 4;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(3, 8);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(954, 206);
            this.dgList.TabIndex = 0;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.MistyRose;
            this.uiGroupBox4.Controls.Add(this.ctrDVT);
            this.uiGroupBox4.Controls.Add(this.txtPO);
            this.uiGroupBox4.Controls.Add(this.txtSoLuong);
            this.uiGroupBox4.Controls.Add(this.txtStyle);
            this.uiGroupBox4.Controls.Add(this.txtSoPhieu);
            this.uiGroupBox4.Controls.Add(this.txtXuatTaiKho);
            this.uiGroupBox4.Controls.Add(this.txtDonViNhan);
            this.uiGroupBox4.Controls.Add(this.txtKhachHang);
            this.uiGroupBox4.Controls.Add(this.txtSoHoaDon);
            this.uiGroupBox4.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox4.Controls.Add(this.clcNgayHoaDon);
            this.uiGroupBox4.Controls.Add(this.clcNgayXuatKho);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.label16);
            this.uiGroupBox4.Controls.Add(this.label10);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label25);
            this.uiGroupBox4.Controls.Add(this.label24);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Location = new System.Drawing.Point(14, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(988, 144);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // ctrDVT
            // 
            this.ctrDVT.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDVT.Appearance.Options.UseBackColor = true;
            this.ctrDVT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ctrDVT.Code = "";
            this.ctrDVT.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVT.IsOnlyWarning = false;
            this.ctrDVT.IsValidate = true;
            this.ctrDVT.Location = new System.Drawing.Point(665, 93);
            this.ctrDVT.Name = "ctrDVT";
            this.ctrDVT.Name_VN = "";
            this.ctrDVT.SetOnlyWarning = false;
            this.ctrDVT.SetValidate = false;
            this.ctrDVT.ShowColumnCode = true;
            this.ctrDVT.ShowColumnName = false;
            this.ctrDVT.Size = new System.Drawing.Size(79, 21);
            this.ctrDVT.TabIndex = 22;
            this.ctrDVT.TagName = "";
            this.ctrDVT.Where = null;
            this.ctrDVT.WhereCondition = "";
            // 
            // txtPO
            // 
            this.txtPO.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtPO.Location = new System.Drawing.Point(100, 93);
            this.txtPO.Name = "txtPO";
            this.txtPO.Size = new System.Drawing.Size(154, 21);
            this.txtPO.TabIndex = 21;
            this.txtPO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtPO.ButtonClick += new System.EventHandler(this.txtPO_ButtonClick);
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.DecimalDigits = 20;
            this.txtSoLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong.Location = new System.Drawing.Point(485, 94);
            this.txtSoLuong.MaxLength = 15;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong.Size = new System.Drawing.Size(115, 21);
            this.txtSoLuong.TabIndex = 27;
            this.txtSoLuong.Text = "0";
            this.txtSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtStyle
            // 
            this.txtStyle.Location = new System.Drawing.Point(100, 66);
            this.txtStyle.Name = "txtStyle";
            this.txtStyle.Size = new System.Drawing.Size(271, 21);
            this.txtStyle.TabIndex = 21;
            this.txtStyle.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSoPhieu
            // 
            this.txtSoPhieu.DecimalDigits = 20;
            this.txtSoPhieu.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoPhieu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoPhieu.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoPhieu.Location = new System.Drawing.Point(485, 12);
            this.txtSoPhieu.MaxLength = 15;
            this.txtSoPhieu.Name = "txtSoPhieu";
            this.txtSoPhieu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoPhieu.Size = new System.Drawing.Size(154, 21);
            this.txtSoPhieu.TabIndex = 27;
            this.txtSoPhieu.Text = "0";
            this.txtSoPhieu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoPhieu.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoPhieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtXuatTaiKho
            // 
            this.txtXuatTaiKho.Location = new System.Drawing.Point(485, 66);
            this.txtXuatTaiKho.Name = "txtXuatTaiKho";
            this.txtXuatTaiKho.Size = new System.Drawing.Size(259, 21);
            this.txtXuatTaiKho.TabIndex = 21;
            this.txtXuatTaiKho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtDonViNhan
            // 
            this.txtDonViNhan.Location = new System.Drawing.Point(485, 39);
            this.txtDonViNhan.Name = "txtDonViNhan";
            this.txtDonViNhan.Size = new System.Drawing.Size(259, 21);
            this.txtDonViNhan.TabIndex = 21;
            this.txtDonViNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtKhachHang
            // 
            this.txtKhachHang.Location = new System.Drawing.Point(100, 39);
            this.txtKhachHang.Name = "txtKhachHang";
            this.txtKhachHang.Size = new System.Drawing.Size(271, 21);
            this.txtKhachHang.TabIndex = 21;
            this.txtKhachHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Location = new System.Drawing.Point(828, 9);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(154, 21);
            this.txtSoHopDong.TabIndex = 21;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // clcNgayXuatKho
            // 
            this.clcNgayXuatKho.Location = new System.Drawing.Point(100, 12);
            this.clcNgayXuatKho.Name = "clcNgayXuatKho";
            this.clcNgayXuatKho.ReadOnly = false;
            this.clcNgayXuatKho.Size = new System.Drawing.Size(154, 21);
            this.clcNgayXuatKho.TabIndex = 20;
            this.clcNgayXuatKho.TagName = "";
            this.clcNgayXuatKho.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayXuatKho.WhereCondition = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(403, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Số lượng";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "Ngày xuất kho";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(403, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Đơn vị nhận";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(403, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Xuất tại kho";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "P.O";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Style";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(747, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Số hợp đồng";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(403, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số phiếu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Khách hàng";
            // 
            // btnDong
            // 
            this.btnDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDong.Icon")));
            this.btnDong.Location = new System.Drawing.Point(919, 543);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(64, 25);
            this.btnDong.TabIndex = 6;
            this.btnDong.Text = "Đóng";
            this.btnDong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnDong.VisualStyleManager = this.vsmMain;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLuu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLuu.Icon")));
            this.btnLuu.Location = new System.Drawing.Point(791, 543);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(58, 25);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnIn
            // 
            this.btnIn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIn.Enabled = false;
            this.btnIn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIn.Image = global::Company.Interface.Properties.Resources.fileprint;
            this.btnIn.Location = new System.Drawing.Point(855, 544);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(58, 25);
            this.btnIn.TabIndex = 8;
            this.btnIn.Text = "In";
            this.btnIn.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnIn.Click += new System.EventHandler(this.btnIn_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.MistyRose;
            this.uiGroupBox1.Controls.Add(this.cbDonViTinh);
            this.uiGroupBox1.Controls.Add(this.txtThucNhan);
            this.uiGroupBox1.Controls.Add(this.txtPhanTram);
            this.uiGroupBox1.Controls.Add(this.txtLuong1);
            this.uiGroupBox1.Controls.Add(this.txtNhuCau);
            this.uiGroupBox1.Controls.Add(this.txtDinhMuc);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.btnThem);
            this.uiGroupBox1.Controls.Add(this.txtMoTaHH);
            this.uiGroupBox1.Controls.Add(this.txtMa1);
            this.uiGroupBox1.Controls.Add(this.txtMaNPL2);
            this.uiGroupBox1.Controls.Add(this.txtSize);
            this.uiGroupBox1.Controls.Add(this.txtNhaCC);
            this.uiGroupBox1.Controls.Add(this.txtMauSac);
            this.uiGroupBox1.Controls.Add(this.txtGhiChu);
            this.uiGroupBox1.Controls.Add(this.txtArt);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.label19);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label21);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Controls.Add(this.label22);
            this.uiGroupBox1.Controls.Add(this.label20);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label17);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Location = new System.Drawing.Point(14, 150);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(988, 384);
            this.uiGroupBox1.TabIndex = 9;
            this.uiGroupBox1.Text = "Thông tin NPL";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // cbDonViTinh
            // 
            this.cbDonViTinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDonViTinh.DisplayMember = "Ten";
            this.cbDonViTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDonViTinh.Location = new System.Drawing.Point(321, 71);
            this.cbDonViTinh.Name = "cbDonViTinh";
            this.cbDonViTinh.Size = new System.Drawing.Size(89, 21);
            this.cbDonViTinh.TabIndex = 28;
            this.cbDonViTinh.ValueMember = "ID";
            this.cbDonViTinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbDonViTinh.VisualStyleManager = this.vsmMain;
            // 
            // txtThucNhan
            // 
            this.txtThucNhan.DecimalDigits = 20;
            this.txtThucNhan.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThucNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThucNhan.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtThucNhan.Location = new System.Drawing.Point(784, 98);
            this.txtThucNhan.MaxLength = 15;
            this.txtThucNhan.Name = "txtThucNhan";
            this.txtThucNhan.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThucNhan.Size = new System.Drawing.Size(105, 21);
            this.txtThucNhan.TabIndex = 27;
            this.txtThucNhan.Text = "0";
            this.txtThucNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThucNhan.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtThucNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtPhanTram
            // 
            this.txtPhanTram.DecimalDigits = 20;
            this.txtPhanTram.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtPhanTram.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhanTram.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtPhanTram.Location = new System.Drawing.Point(784, 71);
            this.txtPhanTram.MaxLength = 15;
            this.txtPhanTram.Name = "txtPhanTram";
            this.txtPhanTram.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPhanTram.Size = new System.Drawing.Size(105, 21);
            this.txtPhanTram.TabIndex = 27;
            this.txtPhanTram.Text = "0";
            this.txtPhanTram.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhanTram.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPhanTram.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtPhanTram.Leave += new System.EventHandler(this.txtPhanTram_Leave);
            // 
            // txtLuong1
            // 
            this.txtLuong1.DecimalDigits = 20;
            this.txtLuong1.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtLuong1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuong1.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtLuong1.Location = new System.Drawing.Point(100, 71);
            this.txtLuong1.MaxLength = 15;
            this.txtLuong1.Name = "txtLuong1";
            this.txtLuong1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtLuong1.Size = new System.Drawing.Size(115, 21);
            this.txtLuong1.TabIndex = 27;
            this.txtLuong1.Text = "0";
            this.txtLuong1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLuong1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtLuong1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtNhuCau
            // 
            this.txtNhuCau.DecimalDigits = 20;
            this.txtNhuCau.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtNhuCau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNhuCau.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtNhuCau.Location = new System.Drawing.Point(550, 98);
            this.txtNhuCau.MaxLength = 15;
            this.txtNhuCau.Name = "txtNhuCau";
            this.txtNhuCau.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNhuCau.Size = new System.Drawing.Size(133, 21);
            this.txtNhuCau.TabIndex = 27;
            this.txtNhuCau.Text = "0";
            this.txtNhuCau.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNhuCau.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtNhuCau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtDinhMuc
            // 
            this.txtDinhMuc.DecimalDigits = 20;
            this.txtDinhMuc.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDinhMuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDinhMuc.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDinhMuc.Location = new System.Drawing.Point(550, 71);
            this.txtDinhMuc.MaxLength = 15;
            this.txtDinhMuc.Name = "txtDinhMuc";
            this.txtDinhMuc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDinhMuc.Size = new System.Drawing.Size(133, 21);
            this.txtDinhMuc.TabIndex = 27;
            this.txtDinhMuc.Text = "0";
            this.txtDinhMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDinhMuc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDinhMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDinhMuc.VisualStyleManager = this.vsmMain;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = global::Company.Interface.Properties.Resources.clear;
            this.btnXoa.Location = new System.Drawing.Point(848, 130);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(55, 25);
            this.btnXoa.TabIndex = 5;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Image = global::Company.Interface.Properties.Resources._83;
            this.btnThem.Location = new System.Drawing.Point(784, 130);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(58, 25);
            this.btnThem.TabIndex = 5;
            this.btnThem.Text = "Ghi";
            this.btnThem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtMoTaHH
            // 
            this.txtMoTaHH.Location = new System.Drawing.Point(550, 20);
            this.txtMoTaHH.Name = "txtMoTaHH";
            this.txtMoTaHH.Size = new System.Drawing.Size(339, 21);
            this.txtMoTaHH.TabIndex = 21;
            this.txtMoTaHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtMa1
            // 
            this.txtMa1.Location = new System.Drawing.Point(100, 18);
            this.txtMa1.Name = "txtMa1";
            this.txtMa1.Size = new System.Drawing.Size(115, 21);
            this.txtMa1.TabIndex = 21;
            this.txtMa1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMa1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtMaNPL2
            // 
            this.txtMaNPL2.Location = new System.Drawing.Point(321, 20);
            this.txtMaNPL2.Name = "txtMaNPL2";
            this.txtMaNPL2.Size = new System.Drawing.Size(89, 21);
            this.txtMaNPL2.TabIndex = 21;
            this.txtMaNPL2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNPL2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSize
            // 
            this.txtSize.Location = new System.Drawing.Point(321, 45);
            this.txtSize.Name = "txtSize";
            this.txtSize.Size = new System.Drawing.Size(89, 21);
            this.txtSize.TabIndex = 21;
            this.txtSize.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSize.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtNhaCC
            // 
            this.txtNhaCC.Location = new System.Drawing.Point(550, 45);
            this.txtNhaCC.Name = "txtNhaCC";
            this.txtNhaCC.Size = new System.Drawing.Size(133, 21);
            this.txtNhaCC.TabIndex = 21;
            this.txtNhaCC.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNhaCC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtMauSac
            // 
            this.txtMauSac.Location = new System.Drawing.Point(784, 45);
            this.txtMauSac.Name = "txtMauSac";
            this.txtMauSac.Size = new System.Drawing.Size(105, 21);
            this.txtMauSac.TabIndex = 21;
            this.txtMauSac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMauSac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(100, 98);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(310, 21);
            this.txtGhiChu.TabIndex = 21;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtArt
            // 
            this.txtArt.Location = new System.Drawing.Point(100, 45);
            this.txtArt.Name = "txtArt";
            this.txtArt.Size = new System.Drawing.Size(115, 21);
            this.txtArt.TabIndex = 21;
            this.txtArt.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtArt.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(236, 23);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Mã NPL";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(236, 76);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "ĐVT số lượng";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(236, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Size";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(707, 103);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 13);
            this.label21.TabIndex = 3;
            this.label21.Text = "Thực nhận";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(464, 76);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "Định mức";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(9, 103);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "Ghi chú";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(464, 103);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(46, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "Nhu cầu";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Số lượng";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(707, 76);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "Phần trăm";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(707, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Màu sắc";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(464, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Nhà cung cấp";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(464, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Mô tả NPL";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(9, 49);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Art";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(9, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Mã SP";
            // 
            // btnXuatNhapTon
            // 
            this.btnXuatNhapTon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuatNhapTon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatNhapTon.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXuatNhapTon.Icon")));
            this.btnXuatNhapTon.Location = new System.Drawing.Point(645, 544);
            this.btnXuatNhapTon.Name = "btnXuatNhapTon";
            this.btnXuatNhapTon.Size = new System.Drawing.Size(140, 25);
            this.btnXuatNhapTon.TabIndex = 10;
            this.btnXuatNhapTon.Text = "Xuất - Nhập - Tồn";
            this.btnXuatNhapTon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXuatNhapTon.Click += new System.EventHandler(this.btnXuatNhapTon_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(749, 44);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(61, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Số hóa đơn";
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.Location = new System.Drawing.Point(828, 36);
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Size = new System.Drawing.Size(154, 21);
            this.txtSoHoaDon.TabIndex = 21;
            this.txtSoHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(750, 71);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(74, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Ngày hóa đơn";
            // 
            // clcNgayHoaDon
            // 
            this.clcNgayHoaDon.Location = new System.Drawing.Point(828, 66);
            this.clcNgayHoaDon.Name = "clcNgayHoaDon";
            this.clcNgayHoaDon.ReadOnly = false;
            this.clcNgayHoaDon.Size = new System.Drawing.Size(154, 21);
            this.clcNgayHoaDon.TabIndex = 20;
            this.clcNgayHoaDon.TagName = "";
            this.clcNgayHoaDon.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayHoaDon.WhereCondition = "";
            // 
            // PhieuXuatKhoForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.BackColor = System.Drawing.Color.MistyRose;
            this.ClientSize = new System.Drawing.Size(1016, 579);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PhieuXuatKhoForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Phiếu xuất kho";
            this.Load += new System.EventHandler(this.PhieuXuatKhoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private UIGroupBox uiGroupBox4;
        private UIButton btnDong;
        private Label label3;
        private GridEX dgList;
        private Label label6;
        private Label label1;
        private Label label2;
        private Label label16;
        private Label label10;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayXuatKho;
        private EditBox txtStyle;
        private EditBox txtXuatTaiKho;
        private EditBox txtDonViNhan;
        private EditBox txtKhachHang;
        public UIButton btnLuu;
        private EditBox txtPO;
        private Label label9;
        private Label label7;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVT;
        private EditBox txtSoHopDong;
        private Label label4;
        public UIButton btnIn;
        private UIGroupBox uiGroupBox1;
        private EditBox txtMoTaHH;
        private EditBox txtArt;
        private Label label5;
        private Label label13;
        private Label label14;
        private Label label18;
        private EditBox txtSize;
        private EditBox txtMauSac;
        private Label label11;
        private Label label12;
        private Label label8;
        public UIButton btnThem;
        private EditBox txtGhiChu;
        private Label label21;
        private Label label15;
        private Label label22;
        private Label label20;
        private Label label17;
        private NumericEditBox txtThucNhan;
        private NumericEditBox txtPhanTram;
        private NumericEditBox txtLuong1;
        private NumericEditBox txtNhuCau;
        private NumericEditBox txtDinhMuc;
        public UIButton btnXoa;
        private NumericEditBox txtSoPhieu;
        private EditBox txtMaNPL2;
        private Label label23;
        private EditBox txtMa1;
        private Label label19;
        private NumericEditBox txtSoLuong;
        private EditBox txtNhaCC;
        public UIButton btnXuatNhapTon;
        private UIComboBox cbDonViTinh;
        private EditBox txtSoHoaDon;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHoaDon;
        private Label label25;
        private Label label24;
    }
}