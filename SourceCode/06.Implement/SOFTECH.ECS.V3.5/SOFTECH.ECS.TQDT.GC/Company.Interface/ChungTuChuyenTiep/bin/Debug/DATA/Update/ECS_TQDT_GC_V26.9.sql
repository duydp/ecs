INSERT t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES ('A528','HDGC',N'Hợp đồng gia công','')
INSERT t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES ('A528','PLHD',N'Phụ lục hợp đồng','')
GO
IF NOT EXISTS (SELECT 1 FROM sys.columns 
          WHERE Name = N'MaDDSX'
          AND Object_ID = Object_ID(N't_KDT_GC_DinhMuc'))
BEGIN
	ALTER TABLE t_KDT_GC_DinhMuc
	ADD MaDDSX NVARCHAR(50) NULL
END
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_SelectBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectBy_Master_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_DeleteBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_DeleteBy_Master_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Insert]
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@GhiChu nvarchar(250),
	@STTHang int,
	@Master_ID bigint,
	@NPL_TuCungUng numeric(18, 8),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@MaDDSX nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_DinhMuc]
(
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX]
)
VALUES 
(
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DVT_ID,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@GhiChu,
	@STTHang,
	@Master_ID,
	@NPL_TuCungUng,
	@TenSanPham,
	@TenNPL,
	@MaDDSX
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Update]
	@ID bigint,
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@GhiChu nvarchar(250),
	@STTHang int,
	@Master_ID bigint,
	@NPL_TuCungUng numeric(18, 8),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@MaDDSX nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_GC_DinhMuc]
SET
	[MaSanPham] = @MaSanPham,
	[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
	[DVT_ID] = @DVT_ID,
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[GhiChu] = @GhiChu,
	[STTHang] = @STTHang,
	[Master_ID] = @Master_ID,
	[NPL_TuCungUng] = @NPL_TuCungUng,
	[TenSanPham] = @TenSanPham,
	[TenNPL] = @TenNPL,
	[MaDDSX] = @MaDDSX
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_InsertUpdate]
	@ID bigint,
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@GhiChu nvarchar(250),
	@STTHang int,
	@Master_ID bigint,
	@NPL_TuCungUng numeric(18, 8),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@MaDDSX nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_DinhMuc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_DinhMuc] 
		SET
			[MaSanPham] = @MaSanPham,
			[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
			[DVT_ID] = @DVT_ID,
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[GhiChu] = @GhiChu,
			[STTHang] = @STTHang,
			[Master_ID] = @Master_ID,
			[NPL_TuCungUng] = @NPL_TuCungUng,
			[TenSanPham] = @TenSanPham,
			[TenNPL] = @TenNPL,
			[MaDDSX] = @MaDDSX
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_DinhMuc]
		(
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DVT_ID],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[GhiChu],
			[STTHang],
			[Master_ID],
			[NPL_TuCungUng],
			[TenSanPham],
			[TenNPL],
			[MaDDSX]
		)
		VALUES 
		(
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DVT_ID,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@GhiChu,
			@STTHang,
			@Master_ID,
			@NPL_TuCungUng,
			@TenSanPham,
			@TenNPL,
			@MaDDSX
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_DinhMuc]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_DeleteBy_Master_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_DeleteBy_Master_ID]
	@Master_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_DinhMuc]
WHERE
	[Master_ID] = @Master_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_DinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX]
FROM
	[dbo].[t_KDT_GC_DinhMuc]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_SelectBy_Master_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectBy_Master_ID]
	@Master_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX]
FROM
	[dbo].[t_KDT_GC_DinhMuc]
WHERE
	[Master_ID] = @Master_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX]
FROM [dbo].[t_KDT_GC_DinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX]
FROM
	[dbo].[t_KDT_GC_DinhMuc]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '26.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('26.9',GETDATE(), N'CẬP NHẬT PROCEDURE ĐỊNH MỨC')
END