using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK10Form : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public BKNPLTuCungUngCollection bkCollection = new BKNPLTuCungUngCollection();
        private string DVT_ID; 
        bool existNPL;
        private string maNPL;
        private int Postion;
        public BK10Form()
        {
            InitializeComponent();
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            NguyenPhuLieuRegistedForm f = new NguyenPhuLieuRegistedForm();
            f.CalledForm = "Bk10Form";
            f.MaHaiQuan = this.HSTL.MaHaiQuanTiepNhan;
            f.ShowDialog(this);
            if (f.NguyenPhuLieuSelected.Ma != "")
            {
                txtMaNPL.Text = f.NguyenPhuLieuSelected.Ma;
                txtTenNPL.Text = f.NguyenPhuLieuSelected.Ten;
                txtMaHS.Text = f.NguyenPhuLieuSelected.MaHS;
                txtDonViTinh.Text = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(f.NguyenPhuLieuSelected.DVT_ID);
            }
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            Company.BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
            nplSXXK.MaHaiQuan = this.HSTL.MaHaiQuanTiepNhan;
            nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            nplSXXK.Ma = txtMaNPL.Text;
            if (nplSXXK.Load())
            {
                this.existNPL = true;
                txtMaNPL.Text = nplSXXK.Ma;
                txtTenNPL.Text = nplSXXK.Ten;
                txtMaHS.Text = nplSXXK.MaHS;
                this.DVT_ID = nplSXXK.DVT_ID;
                txtDonViTinh.Text = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(nplSXXK.DVT_ID);
                epError.SetError(txtMaNPL, string.Empty);
            }
            else
            {
                this.existNPL = false;
                epError.SetIconPadding(txtMaNPL, -8);
                epError.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
                txtTenNPL.Text = txtMaHS.Text = txtDonViTinh.Text = string.Empty;
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (this.bkCollection.Count == 0)
            {
                ShowMessage("Bạn chưa nhập NPL tự cung ứng.", false);
                return;
            }

            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (this.HSTL.getBKNPLTuCungUng() < 0)
                {
                    BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                    bk.MaterID = this.HSTL.ID;
                    bk.STTHang = this.HSTL.BKCollection.Count + 1;
                    bk.MaBangKe = "DTLNPLTCU";
                    bk.TenBangKe = "DTLNPLTCU";
                    bk.bkNPLTCUCollection = this.bkCollection;
                    this.HSTL.BKCollection.Add(bk);
                }
                else
                {
                    this.HSTL.BKCollection[this.HSTL.getBKNPLTuCungUng()].bkNPLTCUCollection = this.bkCollection;
                }
                this.HSTL.BKCollection[this.HSTL.getBKNPLTuCungUng()].InsertUpdate_BKNPLTCU(this.bkCollection);
                this.Close();
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private int checkExitNPL(string maNPL)
        {
            for (int i = 0; i < this.bkCollection.Count; i++)
            {
                BKNPLTuCungUng bk = this.bkCollection[i];
                if (bk.MaNPL.Trim() == maNPL.Trim()) return i; 
            }
            return -1;
        }
        private bool checkExitNPL(string maNPL,string oldMaNPL)
        {
            if (maNPL == oldMaNPL) return false;
            foreach (BKNPLTuCungUng bk in this.bkCollection)
                if (bk.MaNPL.Trim() == maNPL.Trim()) return true;
            return false;
        }
        private void cmdAdd_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (cvError.IsValid)
            {
                if (!this.existNPL)
                {
                    epError.SetIconPadding(txtMaNPL, -8);
                    epError.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
                    return;
                }


                if (checkExitNPL(txtMaNPL.Text)>=0)
                {
                    this.bkCollection[checkExitNPL(txtMaNPL.Text)].LuongTuCungUng += Convert.ToDecimal(txtLuong.Text);
                }
                else
                {
                    BKNPLTuCungUng bk = new BKNPLTuCungUng();
                    bk.BangKeHoSoThanhLy_ID = 0;
                    bk.MaNPL = txtMaNPL.Text;
                    bk.TenNPL = txtTenNPL.Text;
                    bk.DVT_ID = this.DVT_ID;
                    bk.TenDVT = txtDonViTinh.Text;
                    bk.NgayHoaDong = calendarCombo1.Value;
                    bk.SoHoaDong = txtSoHoaDon.Text;
                    bk.LuongTuCungUng = Convert.ToDecimal(txtLuong.Text);
                    this.bkCollection.Add(bk);
                }
                dgList2.Refetch();
                txtMaNPL.Text = "";
                txtTenNPL.Text = "";
                txtDonViTinh.Text = "";
                txtMaHS.Text = "";
                txtSoHoaDon.Text = "";
                txtLuong.Value = 0;
                epError.Clear();
            }
        }

        private void BK10Form_Load(object sender, EventArgs e)
        {
            dgList2.Tables[0].Columns["LuongTuCungUng"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            if (this.HSTL.TrangThaiThanhKhoan == 401)
            {
                btnAdd.Enabled = cmdSave.Enabled = false;
                dgList2.AllowDelete = InheritableBoolean.False;
            }
            dgList2.DataSource = this.bkCollection;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (cvError.IsValid)
            {
                if (!this.existNPL)
                {
                    epError.SetIconPadding(txtMaNPL, -8);
                    epError.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
                    return;
                }
                if (checkExitNPL(txtMaNPL.Text,this.maNPL))
                {
                    //ShowMessage("Nguyên phụ liệu này đã có rồi.", false);
                    MLMessages("Nguyên phụ liệu này đã có rồi.", "MSG_PUB41", "", false);
                    return;
                }
                BKNPLTuCungUng bk = new BKNPLTuCungUng();
                bk.BangKeHoSoThanhLy_ID = 0;
                bk.MaNPL = txtMaNPL.Text;
                bk.TenNPL = txtTenNPL.Text;
                bk.DVT_ID = this.DVT_ID;
                bk.TenDVT = txtDonViTinh.Text;
                bk.NgayHoaDong = calendarCombo1.Value;
                bk.SoHoaDong = txtSoHoaDon.Text;
                bk.LuongTuCungUng = Convert.ToDecimal(txtLuong.Text);
                this.bkCollection[this.Postion] = bk;
                dgList2.Refetch();
                txtMaNPL.Text = "";
                txtTenNPL.Text = "";
                txtDonViTinh.Text = "";
                txtMaHS.Text = "";
                txtSoHoaDon.Text = "";
                txtLuong.Value = 0;
                btnSave.Enabled = false;
                btnAdd.Enabled = true;
                epError.Clear();
            }
        }

        private void dgList2_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                BKNPLTuCungUng bk = (BKNPLTuCungUng)e.Row.DataRow;
                txtMaNPL.Text = bk.MaNPL;
                Company.BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                nplSXXK.MaHaiQuan = this.HSTL.MaHaiQuanTiepNhan;
                nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                nplSXXK.Ma = txtMaNPL.Text;
                nplSXXK.Load();
                txtTenNPL.Text = nplSXXK.Ten;
                txtMaHS.Text = nplSXXK.MaHS;
                txtDonViTinh.Text = bk.TenDVT;
                txtSoHoaDon.Text = bk.SoHoaDong;
                calendarCombo1.Value = bk.NgayHoaDong;
                txtLuong.Value = bk.LuongTuCungUng;
                this.Postion = e.Row.Position;
                this.maNPL = bk.MaNPL;
                this.existNPL = true;
                btnAdd.Enabled = false;
                btnSave.Enabled = true;
            }
        }

        private void txtMaNPL_TextChanged(object sender, EventArgs e)
        {

        }

    }
}