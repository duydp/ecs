﻿namespace Company.Interface
{
    partial class VNACC_ToKhaiMauDichNhapForm_OLD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ToKhaiMauDichNhapForm));
            Janus.Windows.GridEX.GridEXLayout grListHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListChiThi_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.grbDonVi = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoDienThoaiDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaBuuChinhDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNhomXuLyHS = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaPhuongThucVT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaPhanLoaiHH = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrPhanLoaiCNTC = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaLoaiHinh = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoToKhaiDauTien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongSoTKChiaNho = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoNhanhToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoToKhaiTNTX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.clcNgayDangKy = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcThoiHanTaiNhap = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtCoQuanHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.grbDoiTac = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaNuoc = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtDiaChiDoiTac2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiDoiTac1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDiaChiDoiTac4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiDoiTac3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMaDaiLyHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaBuuChinhDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTenDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrDiaDiemXepHang = new Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap();
            this.ctrDiaDiemNhanHang = new Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap();
            this.txtMaPTVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.numericEditBox1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.clcNgayHangDen = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.txtSoHieuKyHieu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDDLuuKho = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenPTVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox12 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrPhanLoaiHD = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTHoaDon = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txtSoTiepNhanHD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.clcNgayPhatHanhHD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label44 = new System.Windows.Forms.Label();
            this.txtTongTriGiaHD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtMaDieuKienGiaHD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhanLoaiGiaHD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhuongThucTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.uiGroupBox14 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayKhoiHanhVC = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayDen = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtDiaDiemDichVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoQuanLyNoiBoDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaVanBanPhapQuy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaKetQuaKiemTra = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaXDThoiHanNopThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtKyHieuCTBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtSoCTBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamPhatHanhBL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtMaNHBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label38 = new System.Windows.Forms.Label();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaLyDoDeNghi = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrNguoiNopThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtKyHieuCTHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtNanPhatHanhHM = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoCTHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtMaNHTraThueThay = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaDVTTrongLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaDVTSoLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdChiThiHQ1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHQ");
            this.cmdToKhaiTriGia1 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGia");
            this.cmdVanDon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmDinhKemDT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDinhKemDT");
            this.cmdTrungChuyen1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTrungChuyen");
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdPhanHoi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoi");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdChiThiHQ = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHQ");
            this.cmdGiayPhep = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoi");
            this.cmdDinhKemDT = new Janus.Windows.UI.CommandBars.UICommand("cmdDinhKemDT");
            this.cmdTrungChuyen = new Janus.Windows.UI.CommandBars.UICommand("cmdTrungChuyen");
            this.cmdToKhaiTriGia = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGia");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.hang = new Janus.Windows.UI.Dock.UIPanel();
            this.hangContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.grListHang = new Janus.Windows.GridEX.GridEX();
            this.chiThi = new Janus.Windows.UI.Dock.UIPanel();
            this.chiThiContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.grListChiThi = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbDonVi)).BeginInit();
            this.grbDonVi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDoiTac)).BeginInit();
            this.grbDoiTac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).BeginInit();
            this.uiGroupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).BeginInit();
            this.uiGroupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hang)).BeginInit();
            this.hang.SuspendLayout();
            this.hangContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chiThi)).BeginInit();
            this.chiThi.SuspendLayout();
            this.chiThiContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListChiThi)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(3, 31);
            this.grbMain.Size = new System.Drawing.Size(874, 672);
            // 
            // grbDonVi
            // 
            this.grbDonVi.BackColor = System.Drawing.Color.Transparent;
            this.grbDonVi.Controls.Add(this.txtDiaChiDonVi);
            this.grbDonVi.Controls.Add(this.txtMaDonVi);
            this.grbDonVi.Controls.Add(this.label3);
            this.grbDonVi.Controls.Add(this.txtSoDienThoaiDonVi);
            this.grbDonVi.Controls.Add(this.txtMaBuuChinhDonVi);
            this.grbDonVi.Controls.Add(this.label5);
            this.grbDonVi.Controls.Add(this.txtTenDonVi);
            this.grbDonVi.Controls.Add(this.label4);
            this.grbDonVi.Controls.Add(this.label1);
            this.grbDonVi.Controls.Add(this.label2);
            this.grbDonVi.Location = new System.Drawing.Point(3, 121);
            this.grbDonVi.Name = "grbDonVi";
            this.grbDonVi.Size = new System.Drawing.Size(286, 167);
            this.grbDonVi.TabIndex = 1;
            this.grbDonVi.Text = "Người nhập khẩu";
            this.grbDonVi.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiDonVi
            // 
            this.txtDiaChiDonVi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDonVi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDonVi.Location = new System.Drawing.Point(54, 90);
            this.txtDiaChiDonVi.MaxLength = 100;
            this.txtDiaChiDonVi.Multiline = true;
            this.txtDiaChiDonVi.Name = "txtDiaChiDonVi";
            this.txtDiaChiDonVi.Size = new System.Drawing.Size(226, 40);
            this.txtDiaChiDonVi.TabIndex = 2;
            this.txtDiaChiDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDonVi
            // 
            this.txtMaDonVi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDonVi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonVi.Location = new System.Drawing.Point(54, 17);
            this.txtMaDonVi.MaxLength = 13;
            this.txtMaDonVi.Name = "txtMaDonVi";
            this.txtMaDonVi.Size = new System.Drawing.Size(226, 21);
            this.txtMaDonVi.TabIndex = 0;
            this.txtMaDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Địa chỉ";
            // 
            // txtSoDienThoaiDonVi
            // 
            this.txtSoDienThoaiDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDienThoaiDonVi.Location = new System.Drawing.Point(185, 135);
            this.txtSoDienThoaiDonVi.MaxLength = 255;
            this.txtSoDienThoaiDonVi.Name = "txtSoDienThoaiDonVi";
            this.txtSoDienThoaiDonVi.Size = new System.Drawing.Size(95, 21);
            this.txtSoDienThoaiDonVi.TabIndex = 5;
            this.txtSoDienThoaiDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDienThoaiDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaBuuChinhDonVi
            // 
            this.txtMaBuuChinhDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBuuChinhDonVi.Location = new System.Drawing.Point(54, 136);
            this.txtMaBuuChinhDonVi.MaxLength = 255;
            this.txtMaBuuChinhDonVi.Name = "txtMaBuuChinhDonVi";
            this.txtMaBuuChinhDonVi.Size = new System.Drawing.Size(79, 21);
            this.txtMaBuuChinhDonVi.TabIndex = 4;
            this.txtMaBuuChinhDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaBuuChinhDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(132, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Điện thoại";
            // 
            // txtTenDonVi
            // 
            this.txtTenDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonVi.Location = new System.Drawing.Point(54, 44);
            this.txtTenDonVi.MaxLength = 100;
            this.txtTenDonVi.Multiline = true;
            this.txtTenDonVi.Name = "txtTenDonVi";
            this.txtTenDonVi.Size = new System.Drawing.Size(226, 40);
            this.txtTenDonVi.TabIndex = 1;
            this.txtTenDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDonVi.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Bưu chính";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Mã";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Tên";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtMaUyThac);
            this.uiGroupBox2.Controls.Add(this.txtTenUyThac);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 294);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(286, 96);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.Text = "Người ủy thác nhập khẩu";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtMaUyThac
            // 
            this.txtMaUyThac.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaUyThac.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaUyThac.Location = new System.Drawing.Point(54, 18);
            this.txtMaUyThac.MaxLength = 13;
            this.txtMaUyThac.Name = "txtMaUyThac";
            this.txtMaUyThac.Size = new System.Drawing.Size(227, 21);
            this.txtMaUyThac.TabIndex = 0;
            this.txtMaUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenUyThac
            // 
            this.txtTenUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenUyThac.Location = new System.Drawing.Point(53, 44);
            this.txtTenUyThac.MaxLength = 100;
            this.txtTenUyThac.Multiline = true;
            this.txtTenUyThac.Name = "txtTenUyThac";
            this.txtTenUyThac.Size = new System.Drawing.Size(228, 40);
            this.txtTenUyThac.TabIndex = 1;
            this.txtTenUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 33;
            this.label12.Text = "Mã";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 50);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Tên";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.ctrNhomXuLyHS);
            this.uiGroupBox3.Controls.Add(this.ctrMaPhuongThucVT);
            this.uiGroupBox3.Controls.Add(this.ctrMaPhanLoaiHH);
            this.uiGroupBox3.Controls.Add(this.ctrPhanLoaiCNTC);
            this.uiGroupBox3.Controls.Add(this.ctrMaLoaiHinh);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhaiDauTien);
            this.uiGroupBox3.Controls.Add(this.txtTongSoTKChiaNho);
            this.uiGroupBox3.Controls.Add(this.txtSoNhanhToKhai);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhaiTNTX);
            this.uiGroupBox3.Controls.Add(this.clcNgayDangKy);
            this.uiGroupBox3.Controls.Add(this.clcThoiHanTaiNhap);
            this.uiGroupBox3.Controls.Add(this.txtCoQuanHaiQuan);
            this.uiGroupBox3.Controls.Add(this.label41);
            this.uiGroupBox3.Controls.Add(this.label18);
            this.uiGroupBox3.Controls.Add(this.label28);
            this.uiGroupBox3.Controls.Add(this.label30);
            this.uiGroupBox3.Controls.Add(this.label29);
            this.uiGroupBox3.Controls.Add(this.label27);
            this.uiGroupBox3.Controls.Add(this.label23);
            this.uiGroupBox3.Controls.Add(this.label26);
            this.uiGroupBox3.Controls.Add(this.label20);
            this.uiGroupBox3.Controls.Add(this.label25);
            this.uiGroupBox3.Controls.Add(this.label24);
            this.uiGroupBox3.Controls.Add(this.label19);
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(869, 115);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // ctrNhomXuLyHS
            // 
            this.ctrNhomXuLyHS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A014;
            this.ctrNhomXuLyHS.Code = "";
            this.ctrNhomXuLyHS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNhomXuLyHS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrNhomXuLyHS.IsValidate = false;
            this.ctrNhomXuLyHS.Location = new System.Drawing.Point(691, 6);
            this.ctrNhomXuLyHS.Name = "ctrNhomXuLyHS";
            this.ctrNhomXuLyHS.Name_VN = "";
            this.ctrNhomXuLyHS.ShowColumnCode = true;
            this.ctrNhomXuLyHS.ShowColumnName = true;
            this.ctrNhomXuLyHS.Size = new System.Drawing.Size(170, 26);
            this.ctrNhomXuLyHS.TabIndex = 40;
            this.ctrNhomXuLyHS.WhereCondition = "";
            // 
            // ctrMaPhuongThucVT
            // 
            this.ctrMaPhuongThucVT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E005;
            this.ctrMaPhuongThucVT.Code = "";
            this.ctrMaPhuongThucVT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhuongThucVT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhuongThucVT.IsValidate = false;
            this.ctrMaPhuongThucVT.Location = new System.Drawing.Point(597, 82);
            this.ctrMaPhuongThucVT.Name = "ctrMaPhuongThucVT";
            this.ctrMaPhuongThucVT.Name_VN = "";
            this.ctrMaPhuongThucVT.ShowColumnCode = true;
            this.ctrMaPhuongThucVT.ShowColumnName = true;
            this.ctrMaPhuongThucVT.Size = new System.Drawing.Size(264, 26);
            this.ctrMaPhuongThucVT.TabIndex = 39;
            this.ctrMaPhuongThucVT.WhereCondition = "";
            // 
            // ctrMaPhanLoaiHH
            // 
            this.ctrMaPhanLoaiHH.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E003;
            this.ctrMaPhanLoaiHH.Code = "";
            this.ctrMaPhanLoaiHH.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhanLoaiHH.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhanLoaiHH.IsValidate = false;
            this.ctrMaPhanLoaiHH.Location = new System.Drawing.Point(539, 32);
            this.ctrMaPhanLoaiHH.Name = "ctrMaPhanLoaiHH";
            this.ctrMaPhanLoaiHH.Name_VN = "";
            this.ctrMaPhanLoaiHH.ShowColumnCode = true;
            this.ctrMaPhanLoaiHH.ShowColumnName = true;
            this.ctrMaPhanLoaiHH.Size = new System.Drawing.Size(322, 26);
            this.ctrMaPhanLoaiHH.TabIndex = 38;
            this.ctrMaPhanLoaiHH.WhereCondition = "";
            // 
            // ctrPhanLoaiCNTC
            // 
            this.ctrPhanLoaiCNTC.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E004;
            this.ctrPhanLoaiCNTC.Code = "";
            this.ctrPhanLoaiCNTC.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrPhanLoaiCNTC.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrPhanLoaiCNTC.IsValidate = false;
            this.ctrPhanLoaiCNTC.Location = new System.Drawing.Point(147, 57);
            this.ctrPhanLoaiCNTC.Name = "ctrPhanLoaiCNTC";
            this.ctrPhanLoaiCNTC.Name_VN = "";
            this.ctrPhanLoaiCNTC.ShowColumnCode = true;
            this.ctrPhanLoaiCNTC.ShowColumnName = true;
            this.ctrPhanLoaiCNTC.Size = new System.Drawing.Size(202, 26);
            this.ctrPhanLoaiCNTC.TabIndex = 37;
            this.ctrPhanLoaiCNTC.WhereCondition = "";
            // 
            // ctrMaLoaiHinh
            // 
            this.ctrMaLoaiHinh.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E001;
            this.ctrMaLoaiHinh.Code = "";
            this.ctrMaLoaiHinh.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaLoaiHinh.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaLoaiHinh.IsValidate = false;
            this.ctrMaLoaiHinh.Location = new System.Drawing.Point(72, 32);
            this.ctrMaLoaiHinh.Name = "ctrMaLoaiHinh";
            this.ctrMaLoaiHinh.Name_VN = "";
            this.ctrMaLoaiHinh.ShowColumnCode = true;
            this.ctrMaLoaiHinh.ShowColumnName = true;
            this.ctrMaLoaiHinh.Size = new System.Drawing.Size(307, 26);
            this.ctrMaLoaiHinh.TabIndex = 37;
            this.ctrMaLoaiHinh.WhereCondition = "";
            // 
            // txtSoToKhaiDauTien
            // 
            this.txtSoToKhaiDauTien.DecimalDigits = 12;
            this.txtSoToKhaiDauTien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhaiDauTien.Enabled = false;
            this.txtSoToKhaiDauTien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhaiDauTien.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhaiDauTien.Location = new System.Drawing.Point(274, 9);
            this.txtSoToKhaiDauTien.MaxLength = 15;
            this.txtSoToKhaiDauTien.Name = "txtSoToKhaiDauTien";
            this.txtSoToKhaiDauTien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhaiDauTien.Size = new System.Drawing.Size(76, 21);
            this.txtSoToKhaiDauTien.TabIndex = 11;
            this.txtSoToKhaiDauTien.Text = "0";
            this.txtSoToKhaiDauTien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhaiDauTien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhaiDauTien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongSoTKChiaNho
            // 
            this.txtTongSoTKChiaNho.DecimalDigits = 20;
            this.txtTongSoTKChiaNho.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongSoTKChiaNho.Enabled = false;
            this.txtTongSoTKChiaNho.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTKChiaNho.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongSoTKChiaNho.Location = new System.Drawing.Point(389, 9);
            this.txtTongSoTKChiaNho.MaxLength = 15;
            this.txtTongSoTKChiaNho.Name = "txtTongSoTKChiaNho";
            this.txtTongSoTKChiaNho.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoTKChiaNho.Size = new System.Drawing.Size(24, 21);
            this.txtTongSoTKChiaNho.TabIndex = 11;
            this.txtTongSoTKChiaNho.Text = "0";
            this.txtTongSoTKChiaNho.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoTKChiaNho.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoTKChiaNho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoNhanhToKhai
            // 
            this.txtSoNhanhToKhai.DecimalDigits = 20;
            this.txtSoNhanhToKhai.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoNhanhToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoNhanhToKhai.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoNhanhToKhai.Location = new System.Drawing.Point(355, 9);
            this.txtSoNhanhToKhai.MaxLength = 15;
            this.txtSoNhanhToKhai.Name = "txtSoNhanhToKhai";
            this.txtSoNhanhToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoNhanhToKhai.Size = new System.Drawing.Size(24, 21);
            this.txtSoNhanhToKhai.TabIndex = 11;
            this.txtSoNhanhToKhai.Text = "0";
            this.txtSoNhanhToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoNhanhToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoNhanhToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.DecimalDigits = 12;
            this.txtSoToKhai.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhai.Enabled = false;
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhai.Location = new System.Drawing.Point(72, 9);
            this.txtSoToKhai.MaxLength = 15;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhai.Size = new System.Drawing.Size(76, 21);
            this.txtSoToKhai.TabIndex = 11;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoToKhaiTNTX
            // 
            this.txtSoToKhaiTNTX.DecimalDigits = 12;
            this.txtSoToKhaiTNTX.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhaiTNTX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhaiTNTX.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhaiTNTX.Location = new System.Drawing.Point(148, 86);
            this.txtSoToKhaiTNTX.MaxLength = 15;
            this.txtSoToKhaiTNTX.Name = "txtSoToKhaiTNTX";
            this.txtSoToKhaiTNTX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhaiTNTX.Size = new System.Drawing.Size(89, 21);
            this.txtSoToKhaiTNTX.TabIndex = 3;
            this.txtSoToKhaiTNTX.Text = "0";
            this.txtSoToKhaiTNTX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhaiTNTX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhaiTNTX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // clcNgayDangKy
            // 
            // 
            // 
            // 
            this.clcNgayDangKy.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayDangKy.DropDownCalendar.Name = "";
            this.clcNgayDangKy.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayDangKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayDangKy.IsNullDate = true;
            this.clcNgayDangKy.Location = new System.Drawing.Point(500, 9);
            this.clcNgayDangKy.Name = "clcNgayDangKy";
            this.clcNgayDangKy.Nullable = true;
            this.clcNgayDangKy.NullButtonText = "Xóa";
            this.clcNgayDangKy.ShowNullButton = true;
            this.clcNgayDangKy.Size = new System.Drawing.Size(89, 21);
            this.clcNgayDangKy.TabIndex = 35;
            this.clcNgayDangKy.TodayButtonText = "Hôm nay";
            this.clcNgayDangKy.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcThoiHanTaiNhap
            // 
            // 
            // 
            // 
            this.clcThoiHanTaiNhap.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcThoiHanTaiNhap.DropDownCalendar.Name = "";
            this.clcThoiHanTaiNhap.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcThoiHanTaiNhap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcThoiHanTaiNhap.IsNullDate = true;
            this.clcThoiHanTaiNhap.Location = new System.Drawing.Point(324, 86);
            this.clcThoiHanTaiNhap.Name = "clcThoiHanTaiNhap";
            this.clcThoiHanTaiNhap.Nullable = true;
            this.clcThoiHanTaiNhap.NullButtonText = "Xóa";
            this.clcThoiHanTaiNhap.ShowNullButton = true;
            this.clcThoiHanTaiNhap.Size = new System.Drawing.Size(89, 21);
            this.clcThoiHanTaiNhap.TabIndex = 4;
            this.clcThoiHanTaiNhap.TodayButtonText = "Hôm nay";
            this.clcThoiHanTaiNhap.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcThoiHanTaiNhap.VisualStyleManager = this.vsmMain;
            // 
            // txtCoQuanHaiQuan
            // 
            this.txtCoQuanHaiQuan.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtCoQuanHaiQuan.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCoQuanHaiQuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCoQuanHaiQuan.Location = new System.Drawing.Point(457, 58);
            this.txtCoQuanHaiQuan.MaxLength = 12;
            this.txtCoQuanHaiQuan.Name = "txtCoQuanHaiQuan";
            this.txtCoQuanHaiQuan.Size = new System.Drawing.Size(132, 21);
            this.txtCoQuanHaiQuan.TabIndex = 2;
            this.txtCoQuanHaiQuan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCoQuanHaiQuan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(5, 63);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(134, 13);
            this.label41.TabIndex = 33;
            this.label41.Text = "Phân loại cá nhân /tổ chức";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(4, 39);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Mã loại hình";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(426, 89);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(168, 13);
            this.label28.TabIndex = 33;
            this.label28.Text = "Mã hiệu phương thức vận chuyển";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(238, 90);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(90, 13);
            this.label30.TabIndex = 33;
            this.label30.Text = "Thời hạn tái nhập";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(424, 13);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(75, 13);
            this.label29.TabIndex = 33;
            this.label29.Text = "Ngày khai báo";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(362, 62);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(92, 13);
            this.label27.TabIndex = 33;
            this.label27.Text = "Cơ quan Hải quan";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Enabled = false;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(378, 13);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 13);
            this.label23.TabIndex = 33;
            this.label23.Text = "/";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(601, 13);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(90, 13);
            this.label26.TabIndex = 33;
            this.label26.Text = "Mã bộ phận xữ lý";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(179, 13);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "Số tờ khai đầu tiên";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(402, 39);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(115, 13);
            this.label25.TabIndex = 33;
            this.label25.Text = "Mã phân loại hàng hóa";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(5, 90);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(142, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "Số tờ khai tạm nhập tái xuất";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(3, 13);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 33;
            this.label19.Text = "Số tờ khai";
            // 
            // grbDoiTac
            // 
            this.grbDoiTac.BackColor = System.Drawing.Color.Transparent;
            this.grbDoiTac.Controls.Add(this.ctrMaNuoc);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac2);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac1);
            this.grbDoiTac.Controls.Add(this.txtMaDoiTac);
            this.grbDoiTac.Controls.Add(this.label6);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac4);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac3);
            this.grbDoiTac.Controls.Add(this.label15);
            this.grbDoiTac.Controls.Add(this.label14);
            this.grbDoiTac.Controls.Add(this.label13);
            this.grbDoiTac.Controls.Add(this.txtMaDaiLyHQ);
            this.grbDoiTac.Controls.Add(this.txtMaBuuChinhDoiTac);
            this.grbDoiTac.Controls.Add(this.label17);
            this.grbDoiTac.Controls.Add(this.label16);
            this.grbDoiTac.Controls.Add(this.txtTenDoiTac);
            this.grbDoiTac.Controls.Add(this.label8);
            this.grbDoiTac.Controls.Add(this.label9);
            this.grbDoiTac.Controls.Add(this.label10);
            this.grbDoiTac.Location = new System.Drawing.Point(2, 396);
            this.grbDoiTac.Name = "grbDoiTac";
            this.grbDoiTac.Size = new System.Drawing.Size(287, 269);
            this.grbDoiTac.TabIndex = 3;
            this.grbDoiTac.Text = "Người xuất khẩu";
            this.grbDoiTac.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // ctrMaNuoc
            // 
            this.ctrMaNuoc.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ctrMaNuoc.Code = "";
            this.ctrMaNuoc.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaNuoc.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaNuoc.IsValidate = false;
            this.ctrMaNuoc.Location = new System.Drawing.Point(58, 239);
            this.ctrMaNuoc.Name = "ctrMaNuoc";
            this.ctrMaNuoc.Name_VN = "";
            this.ctrMaNuoc.ShowColumnCode = true;
            this.ctrMaNuoc.ShowColumnName = false;
            this.ctrMaNuoc.Size = new System.Drawing.Size(79, 26);
            this.ctrMaNuoc.TabIndex = 38;
            this.ctrMaNuoc.WhereCondition = "";
            // 
            // txtDiaChiDoiTac2
            // 
            this.txtDiaChiDoiTac2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDoiTac2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDoiTac2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac2.Location = new System.Drawing.Point(58, 136);
            this.txtDiaChiDoiTac2.MaxLength = 100;
            this.txtDiaChiDoiTac2.Multiline = true;
            this.txtDiaChiDoiTac2.Name = "txtDiaChiDoiTac2";
            this.txtDiaChiDoiTac2.Size = new System.Drawing.Size(223, 40);
            this.txtDiaChiDoiTac2.TabIndex = 3;
            this.txtDiaChiDoiTac2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiDoiTac1
            // 
            this.txtDiaChiDoiTac1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDoiTac1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDoiTac1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac1.Location = new System.Drawing.Point(58, 91);
            this.txtDiaChiDoiTac1.MaxLength = 100;
            this.txtDiaChiDoiTac1.Multiline = true;
            this.txtDiaChiDoiTac1.Name = "txtDiaChiDoiTac1";
            this.txtDiaChiDoiTac1.Size = new System.Drawing.Size(223, 40);
            this.txtDiaChiDoiTac1.TabIndex = 2;
            this.txtDiaChiDoiTac1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDoiTac
            // 
            this.txtMaDoiTac.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDoiTac.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoiTac.Location = new System.Drawing.Point(58, 17);
            this.txtMaDoiTac.MaxLength = 13;
            this.txtMaDoiTac.Name = "txtMaDoiTac";
            this.txtMaDoiTac.Size = new System.Drawing.Size(223, 21);
            this.txtMaDoiTac.TabIndex = 0;
            this.txtMaDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Địa chỉ (1)";
            // 
            // txtDiaChiDoiTac4
            // 
            this.txtDiaChiDoiTac4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac4.Location = new System.Drawing.Point(208, 215);
            this.txtDiaChiDoiTac4.MaxLength = 255;
            this.txtDiaChiDoiTac4.Name = "txtDiaChiDoiTac4";
            this.txtDiaChiDoiTac4.Size = new System.Drawing.Size(73, 21);
            this.txtDiaChiDoiTac4.TabIndex = 6;
            this.txtDiaChiDoiTac4.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiDoiTac3
            // 
            this.txtDiaChiDoiTac3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac3.Location = new System.Drawing.Point(58, 215);
            this.txtDiaChiDoiTac3.MaxLength = 255;
            this.txtDiaChiDoiTac3.Name = "txtDiaChiDoiTac3";
            this.txtDiaChiDoiTac3.Size = new System.Drawing.Size(105, 21);
            this.txtDiaChiDoiTac3.TabIndex = 5;
            this.txtDiaChiDoiTac3.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(162, 219);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "Quốc gia";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1, 218);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Tỉnh/Thành";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1, 139);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Địa chỉ (2)";
            // 
            // txtMaDaiLyHQ
            // 
            this.txtMaDaiLyHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDaiLyHQ.Location = new System.Drawing.Point(207, 242);
            this.txtMaDaiLyHQ.MaxLength = 255;
            this.txtMaDaiLyHQ.Name = "txtMaDaiLyHQ";
            this.txtMaDaiLyHQ.Size = new System.Drawing.Size(74, 21);
            this.txtMaDaiLyHQ.TabIndex = 8;
            this.txtMaDaiLyHQ.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDaiLyHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaBuuChinhDoiTac
            // 
            this.txtMaBuuChinhDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBuuChinhDoiTac.Location = new System.Drawing.Point(58, 183);
            this.txtMaBuuChinhDoiTac.MaxLength = 255;
            this.txtMaBuuChinhDoiTac.Name = "txtMaBuuChinhDoiTac";
            this.txtMaBuuChinhDoiTac.Size = new System.Drawing.Size(79, 21);
            this.txtMaBuuChinhDoiTac.TabIndex = 4;
            this.txtMaBuuChinhDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaBuuChinhDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(137, 246);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 13);
            this.label17.TabIndex = 32;
            this.label17.Text = "Mã đại lý HQ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1, 246);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Mã nước";
            // 
            // txtTenDoiTac
            // 
            this.txtTenDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoiTac.Location = new System.Drawing.Point(58, 44);
            this.txtTenDoiTac.MaxLength = 100;
            this.txtTenDoiTac.Multiline = true;
            this.txtTenDoiTac.Name = "txtTenDoiTac";
            this.txtTenDoiTac.Size = new System.Drawing.Size(223, 40);
            this.txtTenDoiTac.TabIndex = 1;
            this.txtTenDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1, 187);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Bưu chính";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Mã";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Tên";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox12);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox14);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox11);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox10);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox13);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.grbDoiTac);
            this.uiGroupBox1.Controls.Add(this.grbDonVi);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(874, 672);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.ctrDiaDiemXepHang);
            this.uiGroupBox5.Controls.Add(this.ctrDiaDiemNhanHang);
            this.uiGroupBox5.Controls.Add(this.txtMaPTVC);
            this.uiGroupBox5.Controls.Add(this.numericEditBox1);
            this.uiGroupBox5.Controls.Add(this.label7);
            this.uiGroupBox5.Controls.Add(this.label55);
            this.uiGroupBox5.Controls.Add(this.label54);
            this.uiGroupBox5.Controls.Add(this.label57);
            this.uiGroupBox5.Controls.Add(this.label56);
            this.uiGroupBox5.Controls.Add(this.label53);
            this.uiGroupBox5.Controls.Add(this.clcNgayHangDen);
            this.uiGroupBox5.Controls.Add(this.label52);
            this.uiGroupBox5.Controls.Add(this.label51);
            this.uiGroupBox5.Controls.Add(this.label50);
            this.uiGroupBox5.Controls.Add(this.txtSoHieuKyHieu);
            this.uiGroupBox5.Controls.Add(this.txtSoVanDon);
            this.uiGroupBox5.Controls.Add(this.txtMaDDLuuKho);
            this.uiGroupBox5.Controls.Add(this.txtTenPTVC);
            this.uiGroupBox5.Location = new System.Drawing.Point(295, 121);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(577, 119);
            this.uiGroupBox5.TabIndex = 4;
            this.uiGroupBox5.Text = "Vận đơn";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // ctrDiaDiemXepHang
            // 
            this.ctrDiaDiemXepHang.Code = "";
            this.ctrDiaDiemXepHang.CountryCode = null;
            this.ctrDiaDiemXepHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDiaDiemXepHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDiaDiemXepHang.IsValidate = false;
            this.ctrDiaDiemXepHang.Location = new System.Drawing.Point(347, 36);
            this.ctrDiaDiemXepHang.Name = "ctrDiaDiemXepHang";
            this.ctrDiaDiemXepHang.Name_VN = "";
            this.ctrDiaDiemXepHang.ShowColumnCode = true;
            this.ctrDiaDiemXepHang.ShowColumnName = true;
            this.ctrDiaDiemXepHang.Size = new System.Drawing.Size(222, 26);
            this.ctrDiaDiemXepHang.TabIndex = 37;
            this.ctrDiaDiemXepHang.WhereCondition = "";
            // 
            // ctrDiaDiemNhanHang
            // 
            this.ctrDiaDiemNhanHang.Code = "";
            this.ctrDiaDiemNhanHang.CountryCode = null;
            this.ctrDiaDiemNhanHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDiaDiemNhanHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDiaDiemNhanHang.IsValidate = false;
            this.ctrDiaDiemNhanHang.Location = new System.Drawing.Point(347, 10);
            this.ctrDiaDiemNhanHang.Name = "ctrDiaDiemNhanHang";
            this.ctrDiaDiemNhanHang.Name_VN = "";
            this.ctrDiaDiemNhanHang.ShowColumnCode = true;
            this.ctrDiaDiemNhanHang.ShowColumnName = true;
            this.ctrDiaDiemNhanHang.Size = new System.Drawing.Size(222, 26);
            this.ctrDiaDiemNhanHang.TabIndex = 37;
            this.ctrDiaDiemNhanHang.WhereCondition = "";
            // 
            // txtMaPTVC
            // 
            this.txtMaPTVC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaPTVC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaPTVC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPTVC.Location = new System.Drawing.Point(259, 66);
            this.txtMaPTVC.MaxLength = 12;
            this.txtMaPTVC.Name = "txtMaPTVC";
            this.txtMaPTVC.Size = new System.Drawing.Size(89, 21);
            this.txtMaPTVC.TabIndex = 5;
            this.txtMaPTVC.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaPTVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // numericEditBox1
            // 
            this.numericEditBox1.DecimalDigits = 12;
            this.numericEditBox1.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox1.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.numericEditBox1.Location = new System.Drawing.Point(496, 93);
            this.numericEditBox1.MaxLength = 15;
            this.numericEditBox1.Name = "numericEditBox1";
            this.numericEditBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox1.Size = new System.Drawing.Size(73, 21);
            this.numericEditBox1.TabIndex = 8;
            this.numericEditBox1.Text = "0";
            this.numericEditBox1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(408, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "Số container";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(7, 97);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(71, 13);
            this.label55.TabIndex = 36;
            this.label55.Text = "Ký hiệu và số";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(7, 70);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(70, 13);
            this.label54.TabIndex = 36;
            this.label54.Text = "Ngày hàng đi";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(348, 70);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(53, 13);
            this.label57.TabIndex = 36;
            this.label57.Text = "Tên PTVC";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(210, 70);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(49, 13);
            this.label56.TabIndex = 36;
            this.label56.Text = "Mã PTVC";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(237, 43);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(96, 13);
            this.label53.TabIndex = 36;
            this.label53.Text = "Địa điểm xếp hàng";
            // 
            // clcNgayHangDen
            // 
            // 
            // 
            // 
            this.clcNgayHangDen.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayHangDen.DropDownCalendar.Name = "";
            this.clcNgayHangDen.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayHangDen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayHangDen.IsNullDate = true;
            this.clcNgayHangDen.Location = new System.Drawing.Point(83, 66);
            this.clcNgayHangDen.Name = "clcNgayHangDen";
            this.clcNgayHangDen.Nullable = true;
            this.clcNgayHangDen.NullButtonText = "Xóa";
            this.clcNgayHangDen.ShowNullButton = true;
            this.clcNgayHangDen.Size = new System.Drawing.Size(89, 21);
            this.clcNgayHangDen.TabIndex = 4;
            this.clcNgayHangDen.TodayButtonText = "Hôm nay";
            this.clcNgayHangDen.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(237, 17);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(90, 13);
            this.label52.TabIndex = 36;
            this.label52.Text = "Địa điểm dở hàng";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(7, 43);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(179, 13);
            this.label51.TabIndex = 36;
            this.label51.Text = "Mã địa điểm lưu kho chờ thông quan";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(7, 20);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(19, 13);
            this.label50.TabIndex = 36;
            this.label50.Text = "Số";
            // 
            // txtSoHieuKyHieu
            // 
            this.txtSoHieuKyHieu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHieuKyHieu.Location = new System.Drawing.Point(83, 93);
            this.txtSoHieuKyHieu.MaxLength = 255;
            this.txtSoHieuKyHieu.Name = "txtSoHieuKyHieu";
            this.txtSoHieuKyHieu.Size = new System.Drawing.Size(318, 21);
            this.txtSoHieuKyHieu.TabIndex = 7;
            this.txtSoHieuKyHieu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHieuKyHieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon.Location = new System.Drawing.Point(51, 15);
            this.txtSoVanDon.MaxLength = 255;
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(180, 21);
            this.txtSoVanDon.TabIndex = 0;
            this.txtSoVanDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoVanDon.ButtonClick += new System.EventHandler(this.txtSoVanDon_ButtonClick);
            // 
            // txtMaDDLuuKho
            // 
            this.txtMaDDLuuKho.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDDLuuKho.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDDLuuKho.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDDLuuKho.Location = new System.Drawing.Point(187, 39);
            this.txtMaDDLuuKho.MaxLength = 12;
            this.txtMaDDLuuKho.Name = "txtMaDDLuuKho";
            this.txtMaDDLuuKho.Size = new System.Drawing.Size(44, 21);
            this.txtMaDDLuuKho.TabIndex = 1;
            this.txtMaDDLuuKho.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDDLuuKho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenPTVC
            // 
            this.txtTenPTVC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTenPTVC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenPTVC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPTVC.Location = new System.Drawing.Point(403, 65);
            this.txtTenPTVC.MaxLength = 12;
            this.txtTenPTVC.Name = "txtTenPTVC";
            this.txtTenPTVC.Size = new System.Drawing.Size(166, 21);
            this.txtTenPTVC.TabIndex = 6;
            this.txtTenPTVC.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenPTVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox12
            // 
            this.uiGroupBox12.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox12.Controls.Add(this.ctrPhanLoaiHD);
            this.uiGroupBox12.Controls.Add(this.ctrMaTTHoaDon);
            this.uiGroupBox12.Controls.Add(this.label42);
            this.uiGroupBox12.Controls.Add(this.label43);
            this.uiGroupBox12.Controls.Add(this.txtSoTiepNhanHD);
            this.uiGroupBox12.Controls.Add(this.clcNgayPhatHanhHD);
            this.uiGroupBox12.Controls.Add(this.label44);
            this.uiGroupBox12.Controls.Add(this.txtTongTriGiaHD);
            this.uiGroupBox12.Controls.Add(this.txtSoHoaDon);
            this.uiGroupBox12.Controls.Add(this.label45);
            this.uiGroupBox12.Controls.Add(this.txtMaDieuKienGiaHD);
            this.uiGroupBox12.Controls.Add(this.txtPhanLoaiGiaHD);
            this.uiGroupBox12.Controls.Add(this.txtPhuongThucTT);
            this.uiGroupBox12.Controls.Add(this.label46);
            this.uiGroupBox12.Controls.Add(this.label47);
            this.uiGroupBox12.Controls.Add(this.label48);
            this.uiGroupBox12.Controls.Add(this.label49);
            this.uiGroupBox12.Location = new System.Drawing.Point(295, 243);
            this.uiGroupBox12.Name = "uiGroupBox12";
            this.uiGroupBox12.Size = new System.Drawing.Size(369, 147);
            this.uiGroupBox12.TabIndex = 5;
            this.uiGroupBox12.Text = "Hóa đơn";
            this.uiGroupBox12.VisualStyleManager = this.vsmMain;
            // 
            // ctrPhanLoaiHD
            // 
            this.ctrPhanLoaiHD.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E008;
            this.ctrPhanLoaiHD.Code = "";
            this.ctrPhanLoaiHD.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrPhanLoaiHD.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrPhanLoaiHD.IsValidate = false;
            this.ctrPhanLoaiHD.Location = new System.Drawing.Point(101, 10);
            this.ctrPhanLoaiHD.Name = "ctrPhanLoaiHD";
            this.ctrPhanLoaiHD.Name_VN = "";
            this.ctrPhanLoaiHD.ShowColumnCode = true;
            this.ctrPhanLoaiHD.ShowColumnName = true;
            this.ctrPhanLoaiHD.Size = new System.Drawing.Size(89, 26);
            this.ctrPhanLoaiHD.TabIndex = 0;
            this.ctrPhanLoaiHD.WhereCondition = "";
            // 
            // ctrMaTTHoaDon
            // 
            this.ctrMaTTHoaDon.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTHoaDon.Code = "";
            this.ctrMaTTHoaDon.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTHoaDon.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTHoaDon.IsValidate = false;
            this.ctrMaTTHoaDon.Location = new System.Drawing.Point(285, 118);
            this.ctrMaTTHoaDon.Name = "ctrMaTTHoaDon";
            this.ctrMaTTHoaDon.Name_VN = "";
            this.ctrMaTTHoaDon.ShowColumnCode = true;
            this.ctrMaTTHoaDon.ShowColumnName = false;
            this.ctrMaTTHoaDon.Size = new System.Drawing.Size(79, 24);
            this.ctrMaTTHoaDon.TabIndex = 8;
            this.ctrMaTTHoaDon.WhereCondition = "";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(4, 17);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(98, 13);
            this.label42.TabIndex = 35;
            this.label42.Text = "Phân loại hình thức";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(189, 17);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(67, 13);
            this.label43.TabIndex = 35;
            this.label43.Text = "Số tiếp nhận";
            // 
            // txtSoTiepNhanHD
            // 
            this.txtSoTiepNhanHD.DecimalDigits = 12;
            this.txtSoTiepNhanHD.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTiepNhanHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhanHD.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTiepNhanHD.Location = new System.Drawing.Point(258, 13);
            this.txtSoTiepNhanHD.MaxLength = 15;
            this.txtSoTiepNhanHD.Name = "txtSoTiepNhanHD";
            this.txtSoTiepNhanHD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTiepNhanHD.Size = new System.Drawing.Size(106, 21);
            this.txtSoTiepNhanHD.TabIndex = 1;
            this.txtSoTiepNhanHD.Text = "0";
            this.txtSoTiepNhanHD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhanHD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTiepNhanHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // clcNgayPhatHanhHD
            // 
            // 
            // 
            // 
            this.clcNgayPhatHanhHD.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayPhatHanhHD.DropDownCalendar.Name = "";
            this.clcNgayPhatHanhHD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayPhatHanhHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayPhatHanhHD.IsNullDate = true;
            this.clcNgayPhatHanhHD.Location = new System.Drawing.Point(101, 67);
            this.clcNgayPhatHanhHD.Name = "clcNgayPhatHanhHD";
            this.clcNgayPhatHanhHD.Nullable = true;
            this.clcNgayPhatHanhHD.NullButtonText = "Xóa";
            this.clcNgayPhatHanhHD.ShowNullButton = true;
            this.clcNgayPhatHanhHD.Size = new System.Drawing.Size(89, 21);
            this.clcNgayPhatHanhHD.TabIndex = 3;
            this.clcNgayPhatHanhHD.TodayButtonText = "Hôm nay";
            this.clcNgayPhatHanhHD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayPhatHanhHD.VisualStyleManager = this.vsmMain;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(4, 44);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(61, 13);
            this.label44.TabIndex = 35;
            this.label44.Text = "Số hóa đơn";
            // 
            // txtTongTriGiaHD
            // 
            this.txtTongTriGiaHD.BackColor = System.Drawing.Color.White;
            this.txtTongTriGiaHD.DecimalDigits = 20;
            this.txtTongTriGiaHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTriGiaHD.FormatString = "G20";
            this.txtTongTriGiaHD.Location = new System.Drawing.Point(101, 120);
            this.txtTongTriGiaHD.Name = "txtTongTriGiaHD";
            this.txtTongTriGiaHD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTriGiaHD.Size = new System.Drawing.Size(178, 21);
            this.txtTongTriGiaHD.TabIndex = 7;
            this.txtTongTriGiaHD.Text = "0";
            this.txtTongTriGiaHD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongTriGiaHD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTriGiaHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongTriGiaHD.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoHoaDon.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHoaDon.Location = new System.Drawing.Point(101, 40);
            this.txtSoHoaDon.MaxLength = 12;
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Size = new System.Drawing.Size(263, 21);
            this.txtSoHoaDon.TabIndex = 2;
            this.txtSoHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(189, 71);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(31, 13);
            this.label45.TabIndex = 35;
            this.label45.Text = "PTTT";
            // 
            // txtMaDieuKienGiaHD
            // 
            this.txtMaDieuKienGiaHD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDieuKienGiaHD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDieuKienGiaHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDieuKienGiaHD.Location = new System.Drawing.Point(101, 94);
            this.txtMaDieuKienGiaHD.MaxLength = 12;
            this.txtMaDieuKienGiaHD.Name = "txtMaDieuKienGiaHD";
            this.txtMaDieuKienGiaHD.Size = new System.Drawing.Size(88, 21);
            this.txtMaDieuKienGiaHD.TabIndex = 5;
            this.txtMaDieuKienGiaHD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDieuKienGiaHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhanLoaiGiaHD
            // 
            this.txtPhanLoaiGiaHD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtPhanLoaiGiaHD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtPhanLoaiGiaHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhanLoaiGiaHD.Location = new System.Drawing.Point(258, 94);
            this.txtPhanLoaiGiaHD.MaxLength = 12;
            this.txtPhanLoaiGiaHD.Name = "txtPhanLoaiGiaHD";
            this.txtPhanLoaiGiaHD.Size = new System.Drawing.Size(106, 21);
            this.txtPhanLoaiGiaHD.TabIndex = 6;
            this.txtPhanLoaiGiaHD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhanLoaiGiaHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhuongThucTT
            // 
            this.txtPhuongThucTT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtPhuongThucTT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtPhuongThucTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhuongThucTT.Location = new System.Drawing.Point(258, 67);
            this.txtPhuongThucTT.MaxLength = 12;
            this.txtPhuongThucTT.Name = "txtPhuongThucTT";
            this.txtPhuongThucTT.Size = new System.Drawing.Size(106, 21);
            this.txtPhuongThucTT.TabIndex = 4;
            this.txtPhuongThucTT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhuongThucTT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(5, 125);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(61, 13);
            this.label46.TabIndex = 35;
            this.label46.Text = "Tổng trị giá";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(4, 71);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(84, 13);
            this.label47.TabIndex = 35;
            this.label47.Text = "Ngày phát hành";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(189, 97);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(67, 13);
            this.label48.TabIndex = 35;
            this.label48.Text = "Phân loại giá";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(5, 98);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(68, 13);
            this.label49.TabIndex = 35;
            this.label49.Text = "Điều kiện giá";
            // 
            // uiGroupBox14
            // 
            this.uiGroupBox14.Controls.Add(this.clcNgayKhoiHanhVC);
            this.uiGroupBox14.Controls.Add(this.clcNgayDen);
            this.uiGroupBox14.Controls.Add(this.txtDiaDiemDichVC);
            this.uiGroupBox14.Controls.Add(this.label59);
            this.uiGroupBox14.Controls.Add(this.label58);
            this.uiGroupBox14.Location = new System.Drawing.Point(292, 484);
            this.uiGroupBox14.Name = "uiGroupBox14";
            this.uiGroupBox14.Size = new System.Drawing.Size(372, 68);
            this.uiGroupBox14.TabIndex = 9;
            this.uiGroupBox14.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayKhoiHanhVC
            // 
            // 
            // 
            // 
            this.clcNgayKhoiHanhVC.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayKhoiHanhVC.DropDownCalendar.Name = "";
            this.clcNgayKhoiHanhVC.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayKhoiHanhVC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayKhoiHanhVC.IsNullDate = true;
            this.clcNgayKhoiHanhVC.Location = new System.Drawing.Point(103, 12);
            this.clcNgayKhoiHanhVC.Name = "clcNgayKhoiHanhVC";
            this.clcNgayKhoiHanhVC.Nullable = true;
            this.clcNgayKhoiHanhVC.NullButtonText = "Xóa";
            this.clcNgayKhoiHanhVC.ShowNullButton = true;
            this.clcNgayKhoiHanhVC.Size = new System.Drawing.Size(89, 21);
            this.clcNgayKhoiHanhVC.TabIndex = 0;
            this.clcNgayKhoiHanhVC.TodayButtonText = "Hôm nay";
            this.clcNgayKhoiHanhVC.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcNgayDen
            // 
            // 
            // 
            // 
            this.clcNgayDen.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayDen.DropDownCalendar.Name = "";
            this.clcNgayDen.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayDen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayDen.IsNullDate = true;
            this.clcNgayDen.Location = new System.Drawing.Point(277, 40);
            this.clcNgayDen.Name = "clcNgayDen";
            this.clcNgayDen.Nullable = true;
            this.clcNgayDen.NullButtonText = "Xóa";
            this.clcNgayDen.ShowNullButton = true;
            this.clcNgayDen.Size = new System.Drawing.Size(89, 21);
            this.clcNgayDen.TabIndex = 2;
            this.clcNgayDen.TodayButtonText = "Hôm nay";
            this.clcNgayDen.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtDiaDiemDichVC
            // 
            this.txtDiaDiemDichVC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaDiemDichVC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaDiemDichVC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemDichVC.Location = new System.Drawing.Point(205, 40);
            this.txtDiaDiemDichVC.MaxLength = 12;
            this.txtDiaDiemDichVC.Name = "txtDiaDiemDichVC";
            this.txtDiaDiemDichVC.Size = new System.Drawing.Size(69, 21);
            this.txtDiaDiemDichVC.TabIndex = 1;
            this.txtDiaDiemDichVC.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemDichVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(6, 42);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(195, 13);
            this.label59.TabIndex = 35;
            this.label59.Text = "Địa điểm đích cho vận chuyển bảo thuế";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(6, 14);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(81, 13);
            this.label58.TabIndex = 35;
            this.label58.Text = "Ngày khởi hành";
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.Controls.Add(this.txtSoQuanLyNoiBoDN);
            this.uiGroupBox11.Location = new System.Drawing.Point(289, 555);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(375, 43);
            this.uiGroupBox11.TabIndex = 12;
            this.uiGroupBox11.Text = "Số quản lý nội bộ doanh nghiệp";
            this.uiGroupBox11.VisualStyleManager = this.vsmMain;
            // 
            // txtSoQuanLyNoiBoDN
            // 
            this.txtSoQuanLyNoiBoDN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoQuanLyNoiBoDN.Location = new System.Drawing.Point(8, 16);
            this.txtSoQuanLyNoiBoDN.MaxLength = 255;
            this.txtSoQuanLyNoiBoDN.Name = "txtSoQuanLyNoiBoDN";
            this.txtSoQuanLyNoiBoDN.Size = new System.Drawing.Size(362, 21);
            this.txtSoQuanLyNoiBoDN.TabIndex = 0;
            this.txtSoQuanLyNoiBoDN.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoQuanLyNoiBoDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.Controls.Add(this.txtGhiChu);
            this.uiGroupBox10.Location = new System.Drawing.Point(292, 602);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(580, 63);
            this.uiGroupBox10.TabIndex = 13;
            this.uiGroupBox10.Text = "Ghi chú";
            this.uiGroupBox10.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(6, 16);
            this.txtGhiChu.MaxLength = 100;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(568, 41);
            this.txtGhiChu.TabIndex = 0;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.Controls.Add(this.txtMaVanBanPhapQuy);
            this.uiGroupBox9.Controls.Add(this.txtMaKetQuaKiemTra);
            this.uiGroupBox9.Controls.Add(this.label40);
            this.uiGroupBox9.Controls.Add(this.label39);
            this.uiGroupBox9.Location = new System.Drawing.Point(292, 440);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(372, 38);
            this.uiGroupBox9.TabIndex = 8;
            this.uiGroupBox9.VisualStyleManager = this.vsmMain;
            // 
            // txtMaVanBanPhapQuy
            // 
            this.txtMaVanBanPhapQuy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaVanBanPhapQuy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaVanBanPhapQuy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaVanBanPhapQuy.Location = new System.Drawing.Point(269, 11);
            this.txtMaVanBanPhapQuy.MaxLength = 12;
            this.txtMaVanBanPhapQuy.Name = "txtMaVanBanPhapQuy";
            this.txtMaVanBanPhapQuy.Size = new System.Drawing.Size(92, 21);
            this.txtMaVanBanPhapQuy.TabIndex = 1;
            this.txtMaVanBanPhapQuy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaVanBanPhapQuy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaKetQuaKiemTra
            // 
            this.txtMaKetQuaKiemTra.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaKetQuaKiemTra.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaKetQuaKiemTra.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaKetQuaKiemTra.Location = new System.Drawing.Point(101, 10);
            this.txtMaKetQuaKiemTra.MaxLength = 12;
            this.txtMaKetQuaKiemTra.Name = "txtMaKetQuaKiemTra";
            this.txtMaKetQuaKiemTra.Size = new System.Drawing.Size(54, 21);
            this.txtMaKetQuaKiemTra.TabIndex = 0;
            this.txtMaKetQuaKiemTra.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaKetQuaKiemTra.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(158, 15);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(111, 13);
            this.label40.TabIndex = 36;
            this.label40.Text = "Mã văn bản pháp quy";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(2, 16);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(101, 13);
            this.label39.TabIndex = 36;
            this.label39.Text = "Mã kết quả kiểm tra";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.Controls.Add(this.ctrMaXDThoiHanNopThue);
            this.uiGroupBox8.Controls.Add(this.txtKyHieuCTBaoLanh);
            this.uiGroupBox8.Controls.Add(this.label34);
            this.uiGroupBox8.Controls.Add(this.txtSoCTBaoLanh);
            this.uiGroupBox8.Controls.Add(this.txtNamPhatHanhBL);
            this.uiGroupBox8.Controls.Add(this.label35);
            this.uiGroupBox8.Controls.Add(this.label36);
            this.uiGroupBox8.Controls.Add(this.label37);
            this.uiGroupBox8.Controls.Add(this.txtMaNHBaoLanh);
            this.uiGroupBox8.Controls.Add(this.label38);
            this.uiGroupBox8.Location = new System.Drawing.Point(670, 431);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(202, 165);
            this.uiGroupBox8.TabIndex = 11;
            this.uiGroupBox8.Text = "Bảo lảnh thuế";
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaXDThoiHanNopThue
            // 
            this.ctrMaXDThoiHanNopThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E019;
            this.ctrMaXDThoiHanNopThue.Code = "";
            this.ctrMaXDThoiHanNopThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaXDThoiHanNopThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaXDThoiHanNopThue.IsValidate = false;
            this.ctrMaXDThoiHanNopThue.Location = new System.Drawing.Point(92, 9);
            this.ctrMaXDThoiHanNopThue.Name = "ctrMaXDThoiHanNopThue";
            this.ctrMaXDThoiHanNopThue.Name_VN = "";
            this.ctrMaXDThoiHanNopThue.ShowColumnCode = true;
            this.ctrMaXDThoiHanNopThue.ShowColumnName = true;
            this.ctrMaXDThoiHanNopThue.Size = new System.Drawing.Size(104, 26);
            this.ctrMaXDThoiHanNopThue.TabIndex = 0;
            this.ctrMaXDThoiHanNopThue.WhereCondition = "";
            // 
            // txtKyHieuCTBaoLanh
            // 
            this.txtKyHieuCTBaoLanh.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtKyHieuCTBaoLanh.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtKyHieuCTBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuCTBaoLanh.Location = new System.Drawing.Point(92, 100);
            this.txtKyHieuCTBaoLanh.MaxLength = 12;
            this.txtKyHieuCTBaoLanh.Name = "txtKyHieuCTBaoLanh";
            this.txtKyHieuCTBaoLanh.Size = new System.Drawing.Size(104, 21);
            this.txtKyHieuCTBaoLanh.TabIndex = 3;
            this.txtKyHieuCTBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKyHieuCTBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(-1, 139);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(66, 13);
            this.label34.TabIndex = 33;
            this.label34.Text = "Số chứng từ";
            // 
            // txtSoCTBaoLanh
            // 
            this.txtSoCTBaoLanh.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoCTBaoLanh.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoCTBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCTBaoLanh.Location = new System.Drawing.Point(92, 131);
            this.txtSoCTBaoLanh.MaxLength = 12;
            this.txtSoCTBaoLanh.Name = "txtSoCTBaoLanh";
            this.txtSoCTBaoLanh.Size = new System.Drawing.Size(104, 21);
            this.txtSoCTBaoLanh.TabIndex = 4;
            this.txtSoCTBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCTBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNamPhatHanhBL
            // 
            this.txtNamPhatHanhBL.DecimalDigits = 12;
            this.txtNamPhatHanhBL.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtNamPhatHanhBL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamPhatHanhBL.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtNamPhatHanhBL.Location = new System.Drawing.Point(92, 69);
            this.txtNamPhatHanhBL.MaxLength = 15;
            this.txtNamPhatHanhBL.Name = "txtNamPhatHanhBL";
            this.txtNamPhatHanhBL.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNamPhatHanhBL.Size = new System.Drawing.Size(104, 21);
            this.txtNamPhatHanhBL.TabIndex = 2;
            this.txtNamPhatHanhBL.Text = "0";
            this.txtNamPhatHanhBL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNamPhatHanhBL.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtNamPhatHanhBL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(1, 108);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(89, 13);
            this.label35.TabIndex = 33;
            this.label35.Text = "Ký hiệu chứng từ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(1, 77);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(80, 13);
            this.label36.TabIndex = 33;
            this.label36.Text = "Năm phát hành";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(1, 47);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(75, 13);
            this.label37.TabIndex = 33;
            this.label37.Text = "Mã ngân hàng";
            // 
            // txtMaNHBaoLanh
            // 
            this.txtMaNHBaoLanh.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaNHBaoLanh.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNHBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNHBaoLanh.Location = new System.Drawing.Point(92, 39);
            this.txtMaNHBaoLanh.MaxLength = 12;
            this.txtMaNHBaoLanh.Name = "txtMaNHBaoLanh";
            this.txtMaNHBaoLanh.Size = new System.Drawing.Size(104, 21);
            this.txtMaNHBaoLanh.TabIndex = 1;
            this.txtMaNHBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNHBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(1, 19);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(83, 13);
            this.label38.TabIndex = 33;
            this.label38.Text = "Mã XD nộp thuế";
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.Controls.Add(this.ctrMaLyDoDeNghi);
            this.uiGroupBox7.Controls.Add(this.ctrNguoiNopThue);
            this.uiGroupBox7.Controls.Add(this.txtKyHieuCTHanMuc);
            this.uiGroupBox7.Controls.Add(this.label33);
            this.uiGroupBox7.Controls.Add(this.txtNanPhatHanhHM);
            this.uiGroupBox7.Controls.Add(this.txtSoCTHanMuc);
            this.uiGroupBox7.Controls.Add(this.label32);
            this.uiGroupBox7.Controls.Add(this.label31);
            this.uiGroupBox7.Controls.Add(this.label22);
            this.uiGroupBox7.Controls.Add(this.txtMaNHTraThueThay);
            this.uiGroupBox7.Controls.Add(this.label21);
            this.uiGroupBox7.Controls.Add(this.label60);
            this.uiGroupBox7.Location = new System.Drawing.Point(668, 246);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(204, 184);
            this.uiGroupBox7.TabIndex = 10;
            this.uiGroupBox7.Text = "Hạn mức";
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaLyDoDeNghi
            // 
            this.ctrMaLyDoDeNghi.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E016;
            this.ctrMaLyDoDeNghi.Code = "";
            this.ctrMaLyDoDeNghi.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaLyDoDeNghi.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaLyDoDeNghi.IsValidate = false;
            this.ctrMaLyDoDeNghi.Location = new System.Drawing.Point(94, 38);
            this.ctrMaLyDoDeNghi.Name = "ctrMaLyDoDeNghi";
            this.ctrMaLyDoDeNghi.Name_VN = "";
            this.ctrMaLyDoDeNghi.ShowColumnCode = true;
            this.ctrMaLyDoDeNghi.ShowColumnName = true;
            this.ctrMaLyDoDeNghi.Size = new System.Drawing.Size(103, 26);
            this.ctrMaLyDoDeNghi.TabIndex = 1;
            this.ctrMaLyDoDeNghi.WhereCondition = "";
            // 
            // ctrNguoiNopThue
            // 
            this.ctrNguoiNopThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E015;
            this.ctrNguoiNopThue.Code = "";
            this.ctrNguoiNopThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNguoiNopThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrNguoiNopThue.IsValidate = false;
            this.ctrNguoiNopThue.Location = new System.Drawing.Point(94, 11);
            this.ctrNguoiNopThue.Name = "ctrNguoiNopThue";
            this.ctrNguoiNopThue.Name_VN = "";
            this.ctrNguoiNopThue.ShowColumnCode = true;
            this.ctrNguoiNopThue.ShowColumnName = true;
            this.ctrNguoiNopThue.Size = new System.Drawing.Size(104, 26);
            this.ctrNguoiNopThue.TabIndex = 0;
            this.ctrNguoiNopThue.WhereCondition = "";
            // 
            // txtKyHieuCTHanMuc
            // 
            this.txtKyHieuCTHanMuc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtKyHieuCTHanMuc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtKyHieuCTHanMuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuCTHanMuc.Location = new System.Drawing.Point(94, 127);
            this.txtKyHieuCTHanMuc.MaxLength = 12;
            this.txtKyHieuCTHanMuc.Name = "txtKyHieuCTHanMuc";
            this.txtKyHieuCTHanMuc.Size = new System.Drawing.Size(104, 21);
            this.txtKyHieuCTHanMuc.TabIndex = 4;
            this.txtKyHieuCTHanMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKyHieuCTHanMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(1, 162);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(66, 13);
            this.label33.TabIndex = 33;
            this.label33.Text = "Số chứng từ";
            // 
            // txtNanPhatHanhHM
            // 
            this.txtNanPhatHanhHM.DecimalDigits = 12;
            this.txtNanPhatHanhHM.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtNanPhatHanhHM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNanPhatHanhHM.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtNanPhatHanhHM.Location = new System.Drawing.Point(94, 98);
            this.txtNanPhatHanhHM.MaxLength = 15;
            this.txtNanPhatHanhHM.Name = "txtNanPhatHanhHM";
            this.txtNanPhatHanhHM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNanPhatHanhHM.Size = new System.Drawing.Size(104, 21);
            this.txtNanPhatHanhHM.TabIndex = 3;
            this.txtNanPhatHanhHM.Text = "0";
            this.txtNanPhatHanhHM.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNanPhatHanhHM.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtNanPhatHanhHM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoCTHanMuc
            // 
            this.txtSoCTHanMuc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoCTHanMuc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoCTHanMuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCTHanMuc.Location = new System.Drawing.Point(94, 156);
            this.txtSoCTHanMuc.MaxLength = 12;
            this.txtSoCTHanMuc.Name = "txtSoCTHanMuc";
            this.txtSoCTHanMuc.Size = new System.Drawing.Size(104, 21);
            this.txtSoCTHanMuc.TabIndex = 5;
            this.txtSoCTHanMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCTHanMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(1, 133);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(89, 13);
            this.label32.TabIndex = 33;
            this.label32.Text = "Ký hiệu chứng từ";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(1, 103);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(80, 13);
            this.label31.TabIndex = 33;
            this.label31.Text = "Năm phát hành";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(1, 74);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(75, 13);
            this.label22.TabIndex = 33;
            this.label22.Text = "Mã ngân hàng";
            // 
            // txtMaNHTraThueThay
            // 
            this.txtMaNHTraThueThay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaNHTraThueThay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNHTraThueThay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNHTraThueThay.Location = new System.Drawing.Point(94, 68);
            this.txtMaNHTraThueThay.MaxLength = 12;
            this.txtMaNHTraThueThay.Name = "txtMaNHTraThueThay";
            this.txtMaNHTraThueThay.Size = new System.Drawing.Size(104, 21);
            this.txtMaNHTraThueThay.TabIndex = 2;
            this.txtMaNHTraThueThay.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNHTraThueThay.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(1, 19);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 13);
            this.label21.TabIndex = 33;
            this.label21.Text = "Người nộp thuế";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(1, 46);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(85, 13);
            this.label60.TabIndex = 33;
            this.label60.Text = "Mã lý do đề nghị";
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.Controls.Add(this.ctrMaDVTTrongLuong);
            this.uiGroupBox13.Controls.Add(this.txtTrongLuong);
            this.uiGroupBox13.Location = new System.Drawing.Point(481, 392);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(183, 45);
            this.uiGroupBox13.TabIndex = 7;
            this.uiGroupBox13.Text = "Tổng trọng lượng hàng";
            this.uiGroupBox13.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaDVTTrongLuong
            // 
            this.ctrMaDVTTrongLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E006;
            this.ctrMaDVTTrongLuong.Code = "";
            this.ctrMaDVTTrongLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTTrongLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTTrongLuong.IsValidate = false;
            this.ctrMaDVTTrongLuong.Location = new System.Drawing.Point(118, 14);
            this.ctrMaDVTTrongLuong.Name = "ctrMaDVTTrongLuong";
            this.ctrMaDVTTrongLuong.Name_VN = "";
            this.ctrMaDVTTrongLuong.ShowColumnCode = true;
            this.ctrMaDVTTrongLuong.ShowColumnName = true;
            this.ctrMaDVTTrongLuong.Size = new System.Drawing.Size(56, 26);
            this.ctrMaDVTTrongLuong.TabIndex = 1;
            this.ctrMaDVTTrongLuong.WhereCondition = "";
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.DecimalDigits = 20;
            this.txtTrongLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTrongLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTrongLuong.Location = new System.Drawing.Point(3, 17);
            this.txtTrongLuong.MaxLength = 15;
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTrongLuong.Size = new System.Drawing.Size(110, 21);
            this.txtTrongLuong.TabIndex = 0;
            this.txtTrongLuong.Text = "0";
            this.txtTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.ctrMaDVTSoLuong);
            this.uiGroupBox6.Controls.Add(this.txtSoLuong);
            this.uiGroupBox6.Location = new System.Drawing.Point(292, 392);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(186, 45);
            this.uiGroupBox6.TabIndex = 6;
            this.uiGroupBox6.Text = "Số lượng";
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaDVTSoLuong
            // 
            this.ctrMaDVTSoLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E018;
            this.ctrMaDVTSoLuong.Code = "";
            this.ctrMaDVTSoLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTSoLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTSoLuong.IsValidate = false;
            this.ctrMaDVTSoLuong.Location = new System.Drawing.Point(117, 14);
            this.ctrMaDVTSoLuong.Name = "ctrMaDVTSoLuong";
            this.ctrMaDVTSoLuong.Name_VN = "";
            this.ctrMaDVTSoLuong.ShowColumnCode = true;
            this.ctrMaDVTSoLuong.ShowColumnName = true;
            this.ctrMaDVTSoLuong.Size = new System.Drawing.Size(56, 26);
            this.ctrMaDVTSoLuong.TabIndex = 1;
            this.ctrMaDVTSoLuong.WhereCondition = "";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.DecimalDigits = 20;
            this.txtSoLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong.Location = new System.Drawing.Point(3, 17);
            this.txtSoLuong.MaxLength = 15;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong.Size = new System.Drawing.Size(110, 21);
            this.txtSoLuong.TabIndex = 0;
            this.txtSoLuong.Text = "0";
            this.txtSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang,
            this.cmdChiThiHQ,
            this.cmdGiayPhep,
            this.cmdLuu,
            this.cmdKhaiBao,
            this.cmdPhanHoi,
            this.cmdDinhKemDT,
            this.cmdTrungChuyen,
            this.cmdToKhaiTriGia});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("7efa1b81-a632-4adb-89e9-9280c46f7b4f");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.Tag = null;
            this.cmbMain.TopRebar = this.TopRebar1;
            this.cmbMain.VisualStyleManager = this.vsmMain;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdChiThiHQ1,
            this.cmdToKhaiTriGia1,
            this.cmdVanDon1,
            this.cmDinhKemDT1,
            this.cmdTrungChuyen1,
            this.cmdLuu1,
            this.cmdKhaiBao1,
            this.cmdPhanHoi1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(880, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            // 
            // cmdChiThiHQ1
            // 
            this.cmdChiThiHQ1.Key = "cmdChiThiHQ";
            this.cmdChiThiHQ1.Name = "cmdChiThiHQ1";
            // 
            // cmdToKhaiTriGia1
            // 
            this.cmdToKhaiTriGia1.Key = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia1.Name = "cmdToKhaiTriGia1";
            // 
            // cmdVanDon1
            // 
            this.cmdVanDon1.Key = "cmdGiayPhep";
            this.cmdVanDon1.Name = "cmdVanDon1";
            // 
            // cmDinhKemDT1
            // 
            this.cmDinhKemDT1.Key = "cmdDinhKemDT";
            this.cmDinhKemDT1.Name = "cmDinhKemDT1";
            // 
            // cmdTrungChuyen1
            // 
            this.cmdTrungChuyen1.Key = "cmdTrungChuyen";
            this.cmdTrungChuyen1.Name = "cmdTrungChuyen1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdPhanHoi1
            // 
            this.cmdPhanHoi1.Key = "cmdPhanHoi";
            this.cmdPhanHoi1.Name = "cmdPhanHoi1";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThemHang.Icon")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdChiThiHQ
            // 
            this.cmdChiThiHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdChiThiHQ.Icon")));
            this.cmdChiThiHQ.Key = "cmdChiThiHQ";
            this.cmdChiThiHQ.Name = "cmdChiThiHQ";
            this.cmdChiThiHQ.Text = "Chỉ thị HQ";
            // 
            // cmdGiayPhep
            // 
            this.cmdGiayPhep.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdGiayPhep.Icon")));
            this.cmdGiayPhep.Key = "cmdGiayPhep";
            this.cmdGiayPhep.Name = "cmdGiayPhep";
            this.cmdGiayPhep.Text = "Giấy phép";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLuu.Icon")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdKhaiBao.Icon")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai Báo";
            // 
            // cmdPhanHoi
            // 
            this.cmdPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdPhanHoi.Icon")));
            this.cmdPhanHoi.Key = "cmdPhanHoi";
            this.cmdPhanHoi.Name = "cmdPhanHoi";
            this.cmdPhanHoi.Text = "Nhận phản hồi";
            // 
            // cmdDinhKemDT
            // 
            this.cmdDinhKemDT.Key = "cmdDinhKemDT";
            this.cmdDinhKemDT.Name = "cmdDinhKemDT";
            this.cmdDinhKemDT.Text = "Đính kèm ĐT";
            // 
            // cmdTrungChuyen
            // 
            this.cmdTrungChuyen.Key = "cmdTrungChuyen";
            this.cmdTrungChuyen.Name = "cmdTrungChuyen";
            this.cmdTrungChuyen.Text = "Trung Chuyển";
            // 
            // cmdToKhaiTriGia
            // 
            this.cmdToKhaiTriGia.Key = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia.Name = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia.Text = "Tờ khai trị giá ";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmbMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(880, 28);
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(241)))), ((int)(((byte)(228)))));
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.uiPanelManager1.DefaultPanelSettings.CloseButtonVisible = false;
            this.uiPanelManager1.Tag = null;
            this.hang.Id = new System.Guid("f34ec2f8-2da8-4597-ada5-b9a92baeb3fc");
            this.uiPanelManager1.Panels.Add(this.hang);
            this.chiThi.Id = new System.Guid("b70c206b-d720-4e66-b9c5-053406436943");
            this.uiPanelManager1.Panels.Add(this.chiThi);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("f34ec2f8-2da8-4597-ada5-b9a92baeb3fc"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(784, 199), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("b70c206b-d720-4e66-b9c5-053406436943"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(200, 200), true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("f34ec2f8-2da8-4597-ada5-b9a92baeb3fc"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("b70c206b-d720-4e66-b9c5-053406436943"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // hang
            // 
            this.hang.AutoHide = true;
            this.hang.InnerContainer = this.hangContainer;
            this.hang.Location = new System.Drawing.Point(3, 548);
            this.hang.Name = "hang";
            this.hang.Size = new System.Drawing.Size(784, 199);
            this.hang.TabIndex = 4;
            this.hang.Text = "Thông tin hàng";
            // 
            // hangContainer
            // 
            this.hangContainer.Controls.Add(this.grListHang);
            this.hangContainer.Location = new System.Drawing.Point(1, 27);
            this.hangContainer.Name = "hangContainer";
            this.hangContainer.Size = new System.Drawing.Size(782, 171);
            this.hangContainer.TabIndex = 0;
            // 
            // grListHang
            // 
            this.grListHang.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            grListHang_DesignTimeLayout.LayoutString = resources.GetString("grListHang_DesignTimeLayout.LayoutString");
            this.grListHang.DesignTimeLayout = grListHang_DesignTimeLayout;
            this.grListHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListHang.GroupByBoxVisible = false;
            this.grListHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListHang.Location = new System.Drawing.Point(0, 0);
            this.grListHang.Name = "grListHang";
            this.grListHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHang.Size = new System.Drawing.Size(782, 171);
            this.grListHang.TabIndex = 1;
            this.grListHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListHang.VisualStyleManager = this.vsmMain;
            this.grListHang.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListHang_RowDoubleClick);
            // 
            // chiThi
            // 
            this.chiThi.AutoHide = true;
            this.chiThi.InnerContainer = this.chiThiContainer;
            this.chiThi.Location = new System.Drawing.Point(0, 0);
            this.chiThi.Name = "chiThi";
            this.chiThi.Size = new System.Drawing.Size(200, 200);
            this.chiThi.TabIndex = 4;
            this.chiThi.Text = "Chỉ thị HQ";
            // 
            // chiThiContainer
            // 
            this.chiThiContainer.Controls.Add(this.grListChiThi);
            this.chiThiContainer.Location = new System.Drawing.Point(1, 27);
            this.chiThiContainer.Name = "chiThiContainer";
            this.chiThiContainer.Size = new System.Drawing.Size(198, 172);
            this.chiThiContainer.TabIndex = 0;
            // 
            // grListChiThi
            // 
            this.grListChiThi.ColumnAutoResize = true;
            grListChiThi_DesignTimeLayout.LayoutString = resources.GetString("grListChiThi_DesignTimeLayout.LayoutString");
            this.grListChiThi.DesignTimeLayout = grListChiThi_DesignTimeLayout;
            this.grListChiThi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListChiThi.GroupByBoxVisible = false;
            this.grListChiThi.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListChiThi.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListChiThi.Location = new System.Drawing.Point(0, 0);
            this.grListChiThi.Name = "grListChiThi";
            this.grListChiThi.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListChiThi.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListChiThi.Size = new System.Drawing.Size(198, 172);
            this.grListChiThi.TabIndex = 1;
            this.grListChiThi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListChiThi.VisualStyleManager = this.vsmMain;
            // 
            // VNACC_ToKhaiMauDichNhapForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(880, 724);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ToKhaiMauDichNhapForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tờ khai nhập";
            this.Load += new System.EventHandler(this.VNACC_ToKhaiMauDichXuatForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbDonVi)).EndInit();
            this.grbDonVi.ResumeLayout(false);
            this.grbDonVi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDoiTac)).EndInit();
            this.grbDoiTac.ResumeLayout(false);
            this.grbDoiTac.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).EndInit();
            this.uiGroupBox12.ResumeLayout(false);
            this.uiGroupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).EndInit();
            this.uiGroupBox14.ResumeLayout(false);
            this.uiGroupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            this.uiGroupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hang)).EndInit();
            this.hang.ResumeLayout(false);
            this.hangContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chiThi)).EndInit();
            this.chiThi.ResumeLayout(false);
            this.chiThiContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListChiThi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox grbDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDonVi;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhDonVi;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonVi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIGroupBox grbDoiTac;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoiTac;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhDoiTac;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoiTac;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac4;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDaiLyHQ;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDonVi;
        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayDangKy;
        private Janus.Windows.CalendarCombo.CalendarCombo clcThoiHanTaiNhap;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.GridEX.EditControls.EditBox txtKyHieuCTHanMuc;
        private System.Windows.Forms.Label label33;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCTHanMuc;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNHTraThueThay;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.EditControls.EditBox txtKyHieuCTBaoLanh;
        private System.Windows.Forms.Label label34;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCTBaoLanh;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNHBaoLanh;
        private System.Windows.Forms.Label label38;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoQuanLyNoiBoDN;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang1;
        private Janus.Windows.UI.CommandBars.UICommand cmdVanDon1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPhanHoi1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHQ;
        private Janus.Windows.UI.CommandBars.UICommand cmdGiayPhep;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdPhanHoi;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox12;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayPhatHanhHD;
        private System.Windows.Forms.Label label44;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTriGiaHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHoaDon;
        private System.Windows.Forms.Label label45;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDieuKienGiaHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhuongThucTT;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox13;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTrongLuong;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayHangDen;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDDLuuKho;
        private System.Windows.Forms.Label label56;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHieuKyHieu;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenPTVC;
        private System.Windows.Forms.Label label57;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox14;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayKhoiHanhVC;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayDen;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemDichVC;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoTKChiaNho;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoNhanhToKhai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhaiDauTien;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhaiTNTX;
        private Janus.Windows.GridEX.EditControls.EditBox txtCoQuanHaiQuan;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhanHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhanLoaiGiaHD;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHQ1;
        private Janus.Windows.UI.CommandBars.UICommand cmDinhKemDT1;
        private Janus.Windows.UI.CommandBars.UICommand cmdTrungChuyen1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDinhKemDT;
        private Janus.Windows.UI.CommandBars.UICommand cmdTrungChuyen;
        private Janus.Windows.UI.Dock.UIPanelManager uiPanelManager1;
        private Janus.Windows.UI.Dock.UIPanel hang;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer hangContainer;
        private Janus.Windows.UI.Dock.UIPanel chiThi;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer chiThiContainer;
        private Janus.Windows.GridEX.GridEX grListHang;
        private Janus.Windows.GridEX.GridEX grListChiThi;
        private Janus.Windows.UI.CommandBars.UICommand cmdToKhaiTriGia1;
        private Janus.Windows.UI.CommandBars.UICommand cmdToKhaiTriGia;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox1;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaKetQuaKiemTra;
        private System.Windows.Forms.Label label39;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaVanBanPhapQuy;
        private System.Windows.Forms.Label label40;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhuongThucVT;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhanLoaiHH;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaLoaiHinh;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrPhanLoaiHD;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTHoaDon;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTSoLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTTrongLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrNguoiNopThue;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrPhanLoaiCNTC;
        private System.Windows.Forms.Label label41;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaPTVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaNuoc;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrNhomXuLyHS;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaLyDoDeNghi;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaXDThoiHanNopThue;
        private System.Windows.Forms.Label label60;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamPhatHanhBL;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNanPhatHanhHM;
        private Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap ctrDiaDiemNhanHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap ctrDiaDiemXepHang;
    }
}