﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.Interface.VNACCS;
namespace Company.Interface
{
    public partial class VNACC_TKXuatTriGiaForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_TKTriGiaThap TKTriGiaThap = new KDT_VNACCS_TKTriGiaThap();
        public VNACC_TKXuatTriGiaForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.IDA.ToString());
            BoSungNghiepVu();

        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdLuu":
                    Save();
                    break;
                case "cmdMEC":
                    SendVnaccs(false);
                    break;
                case "cmdMEE":
                    SendVnaccs(true);
                    break;
                case "cmdMED":
                    //SendVnaccsMED();
                    break;
                case "cmdThongTinHQ":
                    ShowKetQuaTraVe();
                    break;
                case "cmdKetQuaXuLy":
                    ShowKetQuaXuLy();
                    break;
                case "cmdInTamMEC":
                    InToKhaiTam();
                    break;
            }
        }
        private void InToKhaiTam()
        {
            try
            {
                MsgLog msg = MsgLog.GetThongTinToKhaiTam(TKTriGiaThap.InputMessageID, "MEC");
                if (msg != null && msg.ID > 0)
                    ProcessReport.ShowReport(msg.ID, "VAD1AC0", new ResponseVNACCS(msg.Log_Messages).ResponseData);
                else
                    ShowMessage("Không tìm thấy bản khai tạm", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ShowKetQuaXuLy()
        {
            try
            {
                VNACC_KetQuaXuLy f = new VNACC_KetQuaXuLy(TKTriGiaThap.ID, "MEC");
                //f.master_id = TKMD.ID;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ShowKetQuaTraVe()
        {
            try
            {
                SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
                f.isSend = false;
                f.isRep = true;
                f.inputMSGID = TKTriGiaThap.InputMessageID;
                f.SoToKhai = this.TKTriGiaThap.SoToKhai.ToString().Length > 11 ? this.TKTriGiaThap.SoToKhai.ToString().Substring(0, 11) : string.Empty;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void GetTKTG()
        {
            try
            {
                TKTriGiaThap.MaLoaiHinh = "E";
                // TKTriGiaThap.SoToKhai = Convert.ToInt32(txtSoToKhai.Value);
                TKTriGiaThap.CoQuanHaiQuan = ctrCoQuanHaiQuan.Code;
                TKTriGiaThap.NhomXuLyHS = ctrNhomXuLyHS.Code;

                //don vi
                TKTriGiaThap.MaDonVi = txtMaDonVi.Text;
                TKTriGiaThap.TenDonVi = txtTenDonVi.Text;
                TKTriGiaThap.MaBuuChinhDonVi = txtMaBuuChinhDonVi.Text;
                TKTriGiaThap.DiaChiDonVi = txtDiaChiDonVi.Text;
                TKTriGiaThap.SoDienThoaiDonVi = txtSoDienThoaiDonVi.Text;

                // doi tac
                TKTriGiaThap.MaDoiTac = txtMaDoiTac.Text;
                TKTriGiaThap.TenDoiTac = txtTenDoiTac.Text;
                TKTriGiaThap.MaBuuChinhDoiTac = txtMaBuuChinhDoiTac.Text;
                TKTriGiaThap.DiaChiDoiTac1 = txtDiaChiDoiTac1.Text;
                TKTriGiaThap.DiaChiDoiTac2 = txtDiaChiDoiTac2.Text;
                TKTriGiaThap.DiaChiDoiTac3 = txtDiaChiDoiTac3.Text;
                TKTriGiaThap.DiaChiDoiTac4 = txtDiaChiDoiTac4.Text;
                TKTriGiaThap.MaNuocDoiTac = ctrMaNuoc.Code;

                //so kien va trong luong
                TKTriGiaThap.SoLuong = Convert.ToInt32(txtSoLuong.Value);
                TKTriGiaThap.TrongLuong = Convert.ToInt32(txtTrongLuong.Value);
                TKTriGiaThap.SoHouseAWB = txtSoHouseAWB.Text;
                // van don
                TKTriGiaThap.MaDDLuuKho = ctrMaDDLuuKho.Code;
                //  TKTriGiaThap.MaCangDoHang = ctrMaDiaDiemDoHang.Code;
                TKTriGiaThap.MaDDNhanHangCuoiCung = ctrMaDDNhanHangCuoiCung.Code;
                TKTriGiaThap.MaDiaDiemXepHang = ctrMaDiaDiemXepHang.Code;

                //Mo ta Hang Hoa
                TKTriGiaThap.MoTaHangHoa = txtMoTaHangHoa.Text;
                //Tong tri gia
                TKTriGiaThap.TongTriGiaTinhThue = Convert.ToDecimal(txtTongTriGiaTinhThue.Value);
                TKTriGiaThap.MaTTTongTriGiaTinhThue = ctrMaTTTongTriGiaTinhThue.Code;
                TKTriGiaThap.GiaKhaiBao = Convert.ToDecimal(txtGiaKhaiBao.Value);

                TKTriGiaThap.GhiChu = txtGhiChu.Text;
                TKTriGiaThap.SoQuanLyNoiBoDN = txtSoQuanLyNoiBoDN.Text;
                TKTriGiaThap.MaDDNhanHangCuoiCung = "CNSHA";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void SetTKTG()
        {
            try
            {
                txtSoToKhai.Text = TKTriGiaThap.SoToKhai.ToString();
                ctrCoQuanHaiQuan.Code = TKTriGiaThap.CoQuanHaiQuan;
                ctrNhomXuLyHS.Code = TKTriGiaThap.NhomXuLyHS;
                //Don vi
                txtMaDonVi.Text = TKTriGiaThap.MaDonVi;
                txtTenDonVi.Text = TKTriGiaThap.TenDonVi;
                txtMaBuuChinhDonVi.Text = TKTriGiaThap.MaBuuChinhDonVi;
                txtDiaChiDonVi.Text = TKTriGiaThap.DiaChiDonVi;
                txtSoDienThoaiDonVi.Text = TKTriGiaThap.SoDienThoaiDonVi;
                //doi tac
                txtMaDoiTac.Text = TKTriGiaThap.MaDoiTac;
                txtTenDoiTac.Text = TKTriGiaThap.TenDoiTac;
                txtMaBuuChinhDoiTac.Text = TKTriGiaThap.MaBuuChinhDoiTac;
                txtDiaChiDoiTac1.Text = TKTriGiaThap.DiaChiDoiTac1;
                txtDiaChiDoiTac2.Text = TKTriGiaThap.DiaChiDoiTac2;
                txtDiaChiDoiTac3.Text = TKTriGiaThap.DiaChiDoiTac3;
                txtDiaChiDoiTac4.Text = TKTriGiaThap.DiaChiDoiTac4;
                ctrMaNuoc.Code = TKTriGiaThap.MaNuocDoiTac;
                //so kien va trong luong
                txtSoLuong.Text = TKTriGiaThap.SoLuong.ToString();
                txtTrongLuong.Text = TKTriGiaThap.TrongLuong.ToString();
                txtSoHouseAWB.Text = TKTriGiaThap.SoHouseAWB;
                //van don
                ctrMaDDLuuKho.Code = TKTriGiaThap.MaDDLuuKho;
                // ctrMaDiaDiemDoHang.Code = TKTriGiaThap.MaCangDoHang;
                ctrMaDiaDiemXepHang.Code = TKTriGiaThap.MaDiaDiemXepHang;
                ctrMaDDNhanHangCuoiCung.Code = TKTriGiaThap.MaDDNhanHangCuoiCung;
                // Mo ta hang hoa
                txtMoTaHangHoa.Text = TKTriGiaThap.MoTaHangHoa;
                // Tong tri gia
                txtTongTriGiaTinhThue.Text = TKTriGiaThap.TongTriGiaTinhThue.ToString();
                ctrMaTTTongTriGiaTinhThue.Code = TKTriGiaThap.MaTTTongTriGiaTinhThue;
                txtGiaKhaiBao.Text = TKTriGiaThap.GiaKhaiBao.ToString();

                txtGhiChu.Text = TKTriGiaThap.GhiChu;
                txtSoQuanLyNoiBoDN.Text = TKTriGiaThap.SoQuanLyNoiBoDN;
                //van don
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void Save()
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                GetTKTG();
                if (TKTriGiaThap.ID != 0)
                    TKTriGiaThap.Update();
                else
                    TKTriGiaThap.Insert();
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }


        }
        private void LoadData()
        {
            try
            {
                SetTKTG();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void VNACC_TKNhapTriGiaForm_Load(object sender, EventArgs e)
        {
            try
            {
                SetIDControl();
                TuDongCapNhatThongTin();
                SetTKTG();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }



        #region Bổ sung nghiệp vụ

        private Janus.Windows.UI.CommandBars.UICommand cmdMEC;
        private Janus.Windows.UI.CommandBars.UICommand cmdMED;
        private Janus.Windows.UI.CommandBars.UICommand cmdMEE;
        private void BoSungNghiepVu()
        {
            this.cmdMEC = new Janus.Windows.UI.CommandBars.UICommand("cmdMEC");
            this.cmdMEC.Key = "cmdMEC";
            this.cmdMEC.Name = "cmdMEC";
            this.cmdMEC.Text = "nghiệp vụ MEC (Khai báo tờ khai)";

            this.cmdMED = new Janus.Windows.UI.CommandBars.UICommand("cmdMED");
            this.cmdMED.Key = "cmdMED";
            this.cmdMED.Name = "cmdMED";
            this.cmdMED.Text = "nghiệp vụ MED (Gọi tờ khai) ";

            this.cmdMEE = new Janus.Windows.UI.CommandBars.UICommand("cmdMEE");
            this.cmdMEE.Key = "cmdMEE";
            this.cmdMEE.Name = "cmdMEE";
            this.cmdMEE.Text = "nghiệp vụ MEE (Khai báo sửa tờ khai) ";

            /*this.cmd.Text = "Cập nhật thông tin ";*/
            this.cmdKhaiBao.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
            {
                this.cmdMEC,
                this.cmdMED,
                this.cmdMEE
            });
        }

        #endregion Bổ sung nghiệp vụ

        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            try
            {
                //TKTriGiaThap = (KDT_VNACCS_TKTriGiaThap)ProcessMessages.GetDataResult_ToKhaiTriGiaThap(msgResult, EnumNghiepVu.MEC, TKTriGiaThap);
                //if (TKTriGiaThapNew.GetType() == typeof(KDT_VNACCS_TKTriGiaThap))
                //{
                //    TKTriGiaThap = HelperVNACCS.UpdateObject<KDT_VNACCS_TKTriGiaThap>(TKTriGiaThap, TKTriGiaThapNew);
                //}
                // TKTriGiaThap.InsertUpdate();
                //SetTKTG();

                ProcessMessages.GetDataResult_ToKhaiTriGiaThap(msgResult, "", TKTriGiaThap);
                TKTriGiaThap.InsertUpdate();
                SetTKTG();
                //setCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void TuDongCapNhatThongTin()
        {
            if (TKTriGiaThap != null && TKTriGiaThap.SoToKhai > 0 && TKTriGiaThap.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}'", TKTriGiaThap.SoToKhai.ToString()), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = "E";
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.Delete();
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = "E";
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
        
                    }
                }

            }
        }

        private void SendVnaccs(bool KhaiBaoSua)
        {
            try
            {
                if (TKTriGiaThap.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {
                    MEC MEC = VNACCMaperFromObject.MECMapper(TKTriGiaThap);
                    if (MEC == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        TKTriGiaThap.InputMessageID = HelperVNACCS.NewInputMSGID();
                        TKTriGiaThap.InsertUpdate();

                        msg = MessagesSend.Load<MEC>(MEC, TKTriGiaThap.InputMessageID);
                    }
                    else
                        msg = MessagesSend.Load<MEC>(MEC, true, EnumNghiepVu.MEE, TKTriGiaThap.InputMessageID);

                    MsgLog.SaveMessages(msg, TKTriGiaThap.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = true;
                    f.inputMSGID = TKTriGiaThap.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result)
                    {
                        TuDongCapNhatThongTin();
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        //private void SendVnaccsMED()
        //{
        //    try
        //    {
        //        if (TKTriGiaThap.ID == 0)
        //        {
        //            this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
        //            return;
        //        }
        //        if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
        //        {
        //            MED MED = VNACCMaperFromObject.MEDMapper(TKTriGiaThap.SoToKhai);
        //            if (MED == null)
        //            {
        //                this.ShowMessage("Lỗi khi tạo messages !", false);
        //                return;
        //            }
        //            MessagesSend msg;
        //            msg = MessagesSend.Load<MED>(MED,TKTriGiaThap.InputMessageID);


        //            MsgLog.SaveMessages(msg, TKTriGiaThap.ID, EnumThongBao.SendMess, "");
        //            SendVNACCFrm f = new SendVNACCFrm(msg);
        //            f.ShowDialog(this);
        //            if (f.DialogResult == DialogResult.Cancel)
        //            {
        //                this.ShowMessage(f.msgFeedBack, false);
        //            }
        //            else
        //            {
        //                if (f.feedback.Error == null || f.feedback.Error.Count == 0)
        //                {
        //                    if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
        //                    {
        //                        CapNhatThongTin(f.feedback.ResponseData);
        //                        f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
        //                    }
        //                    else
        //                    {
        //                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
        //                        {
        //                            try
        //                            {
        //                                decimal SoToKhai = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
        //                                TKTriGiaThap.SoToKhai = SoToKhai;
        //                                TKTriGiaThap.InsertUpdate();
        //                            }
        //                            catch (System.Exception ex)
        //                            {
        //                                f.msgFeedBack += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
        //                                Logger.LocalLogger.Instance().WriteMessage(ex);
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    foreach (ErrorVNACCS error in f.feedback.Error)
        //                    {
        //                        f.msgFeedBack += Environment.NewLine + "Lỗi : " + error.loadError();
        //                    }
        //                }

        //                this.ShowMessageTQDT(f.msgFeedBack, false);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.ShowMessage(ex.Message, false);
        //    }
        //}


        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                txtSoToKhai.Tag = "ECN"; //Số tờ khai
                //ctrPhanLoaiBaoCaoSua.Tag = "JKN"; //Phân loại xuất báo cáo sửa đổi
                ctrCoQuanHaiQuan.TagCode = "CH"; //Cơ quan Hải quan
                ctrNhomXuLyHS.TagName = "CHB"; //Nhóm xử lý hồ sơ
                txtMaDonVi.Tag = "EPC"; //Mã người xuất khẩu
                txtTenDonVi.Tag = "EPN"; //Tên người xuất khẩu
                txtMaBuuChinhDonVi.Tag = "EPP"; //Mã bưu chính
                txtDiaChiDonVi.Tag = "EPA"; //Địa chỉ người xuất khẩu
                txtSoDienThoaiDonVi.Tag = "EPT"; //Số điện thoại người xuất khẩu
                txtMaDoiTac.Tag = "CGC"; //Mã người nhập khẩu
                txtTenDoiTac.Tag = "CGN"; //Tên người nhập khẩu
                txtMaBuuChinhDoiTac.Tag = "CGP"; //Mã bưu chính
                txtDiaChiDoiTac1.Tag = "CGA"; //Địa chỉ 1(Street and number/P.O.BOX)
                txtDiaChiDoiTac2.Tag = "CAT"; //Địa chỉ 2(Street and number/P.O.BOX)
                txtDiaChiDoiTac3.Tag = "CAC"; //Địa chỉ 3(City name)
                txtDiaChiDoiTac4.Tag = "CAS"; //Địa chỉ 4(Country sub-entity, name)
                ctrMaNuoc.TagName = "CGK"; //Mã nước(Country, coded)
                txtSoHouseAWB.Tag = "AWB"; //Số House AWB
                txtSoLuong.Tag = "NO"; //Số lượng
                txtTrongLuong.Tag = "GW"; //Tổng trọng lượng hàng (Gross)
                ctrMaDDLuuKho.TagName = "ST"; //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
                ctrMaDDNhanHangCuoiCung.TagName = "DSC"; //Mã địa điểm nhận hàng cuối cùng
                ctrMaDiaDiemXepHang.TagName = "PSC"; //Mã địa điểm xếp hàng
                txtTongTriGiaTinhThue.Tag = "FCD"; //Mã đồng tiền của tổng trị giá tính thuế
                ctrMaTTTongTriGiaTinhThue.TagName = "FKK"; //Tổng trị giá tính thuế
                txtGiaKhaiBao.Tag = "SKK"; //Giá khai báo
                txtMoTaHangHoa.Tag = "CMN"; //Mô tả hàng hóa
                txtGhiChu.Tag = "NT"; //Phần ghi chú
                txtSoQuanLyNoiBoDN.Tag = "REF"; //Số quản lý của nội bộ doanh nghiệp

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

    







    }
}
