﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PDFSignedDigital;
using System.IO;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.Reflection;
using System.Configuration;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Core;
using System.Security.Cryptography.X509Certificates;
using iTextSharp.text.log;
using Org.BouncyCastle.Security;
using iTextSharp.text.pdf.security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.Security;
using iTextSharp.text;
using iTextSharp.text.log;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using X509Certificate = Org.BouncyCastle.X509.X509Certificate;
using System.Security;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Xml;
using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Security.Cryptography.Xml;
using System.IO.Packaging;
namespace PDFSignedDigital
{
    public partial class Main : BaseForm
    {
        DataTable dt;
        bool IsSelectFolder = true;
        public Main()
        {
            InitializeComponent();
        }
        private static void UpdateSetting(string key, string value)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings[key].Value = value;
            configuration.Save(ConfigurationSaveMode.Full);

            ConfigurationManager.RefreshSection("appSettings");
        }
        private DataTable CreateDataTable()
        {
            DataTable dtFile = new DataTable();
            DataColumn dtColFileName = new DataColumn("FileName");
            dtColFileName.DataType = typeof(System.String);

            DataColumn dtColFileNameSign = new DataColumn("FileNameSigned");
            dtColFileNameSign.DataType = typeof(System.String);

            DataColumn dtColFilePath = new DataColumn("FilePath");
            dtColFilePath.DataType = typeof(System.String);

            dtFile.Columns.Add(dtColFileName);
            dtFile.Columns.Add(dtColFileNameSign);
            dtFile.Columns.Add(dtColFilePath);

            return dtFile;
        }
        private void btnFolderSign_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsSelectFolder)
                {
                    FolderBrowserDialog targetDirectory = new FolderBrowserDialog();
                    if (targetDirectory.ShowDialog() != DialogResult.OK)
                        return;
                    txtFolderSign.Text = targetDirectory.SelectedPath;
                    txtFolderSigned.Text = targetDirectory.SelectedPath;
                }
                if (chkFolder.Checked)
                {
                    UpdateSetting("FolderSign", txtFolderSign.Text);
                    UpdateSetting("FolderSigned", txtFolderSigned.Text);
                }
                dt = CreateDataTable();
                string[] getFile = Directory.GetFiles(txtFolderSign.Text);
                foreach (string filePath in getFile)
                {
                    DataRow dr = dt.NewRow();
                    string fileName = Path.GetFileName(filePath);
                    if (fileName.Contains("pdf"))
                    {
                        PdfReader reader = new PdfReader(filePath);
                        dr["FileName"] = fileName;
                        string fileSigned = fileName.Replace(".pdf", " Signed.pdf");
                        dr["FileNameSigned"] = txtFolderSigned.Text + "\\" + fileSigned;//filePath.Replace(fileName,fileSigned);
                        dr["FilePath"] = filePath;
                        dt.Rows.Add(dr);
                    }
                    else if (fileName.Contains(".docx"))
                    {
                        object TemFilePath = filePath;
                        dr["FileName"] = fileName;
                        string fileSigned = fileName.Replace(".docx", " Signed.docx");
                        dr["FileNameSigned"] = txtFolderSigned.Text + "\\" + fileSigned;
                        dr["FilePath"] = filePath;
                        dt.Rows.Add(dr);
                    }
                    else if (fileName.Contains(".xlsx"))
                    {
                        object TemFilePath = filePath;
                        dr["FileName"] = fileName;
                        string fileSigned = fileName.Replace(".xlsx", " Signed.xlsx");
                        dr["FileNameSigned"] = txtFolderSigned.Text + "\\" + fileSigned;
                        dr["FilePath"] = filePath;
                        dt.Rows.Add(dr);
                    }
                    else
                    {

                    }
                }
                if (dt.Rows.Count == 0)
                {
                    MessageBoxControl.ShowMessage("Không tìm thấy File  trong thư mục lưu File cần ký .Vui lòng kiểm tra lại thư mục ");
                }
                dgListFileSign.DataSource = dt;
                IsSelectFolder = true;
            }
            catch (Exception ex)
            {
                MessageBoxControl.ShowMessage("Lỗi lấy danh sách File  trong thư mục lưu file cần ký. \n\r" + ex);
            }

        }
        private String SecureStringToString(SecureString value)
        {
            IntPtr valuePtr = IntPtr.Zero;
            try
            {
                valuePtr = Marshal.SecureStringToGlobalAllocUnicode(value);
                return Marshal.PtrToStringUni(valuePtr);
            }  
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(valuePtr);
            }
        }
        private void ExportCert()
        {
            X509Store store = new X509Store(StoreLocation.CurrentUser);
            store.Open(OpenFlags.MaxAllowed);

            X509Certificate2Collection CertColl = store.Certificates;//.Find(X509FindType.FindByIssuerName, "My_CA", true);
            int i = 1;
            var secure = new SecureString();
            foreach (char c in txtPassword.Text)
            {
                secure.AppendChar(c);
            }
            foreach (X509Certificate2  cer in CertColl)
            {
                i++;
                Byte[] certData = cer.Export(X509ContentType.Pfx, secure);
                UpdateSetting("CertifaceData", Convert.ToBase64String(certData));
                File.WriteAllBytes(txtFolderSigned.Text.ToString() + "\\" + " Certiface " + i + ".pfx", certData);
            }
        }
        private void btnFolderSigned_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog targetDirectory = new FolderBrowserDialog();
            if (targetDirectory.ShowDialog() != DialogResult.OK)
                return;
            txtFolderSigned.Text = targetDirectory.SelectedPath;
            foreach (DataRow dr in dt.Rows)
            {
                dr["FileNameSigned"] = txtFolderSigned.Text + "\\" + dr["FileName"];
            }
        }
        private void Sign(string filename, System.Security.Cryptography.X509Certificates.X509Certificate x509Certificate)
        {
            // Open the package.
            Package package = Package.Open(filename);

            // Create the PackageDigitalSignatureManager
            PackageDigitalSignatureManager dsm =
              new PackageDigitalSignatureManager(package);

            //Specify that the certificate is embedded in the signature held
            //in the XML Signature part.

            //Certificate embedding options include:
            // InSignaturePart – Certificate is embedded in the signature.
            // InCertificatePart – Certificate is embedded in a 
            //                     separate certificate part

            dsm.CertificateOption =
                CertificateEmbeddingOption.InSignaturePart;

            //Initialize a list to hold the part URIs to sign.

            System.Collections.Generic.List<Uri> partsToSign =
                    new System.Collections.Generic.List<Uri>();

            //Add each part to the list, except relationships parts.
            foreach (PackagePart packagePart in package.GetParts())
            {
                if (!PackUriHelper.IsRelationshipPartUri(packagePart.Uri))
                    partsToSign.Add(packagePart.Uri);
            }

            //Create list of selectors for the list of relationships

            List<PackageRelationshipSelector> relationshipSelectors =
                 new List<PackageRelationshipSelector>();

            //Create one selector for each package-level relationship, based on id

            foreach (PackageRelationship relationship in package.GetRelationships())
            {
                relationshipSelectors.Add(new
                    PackageRelationshipSelector(relationship.SourceUri,
                    PackageRelationshipSelectorType.Id, relationship.Id));
            }

            //Sign package using components created above

            PackageDigitalSignature signature = dsm.Sign(partsToSign,
                 x509Certificate, relationshipSelectors);

            //After signing, close the package.
            //The signature will be persisted in the package.
            package.Close();
        }
        private void VerifyCeriface(string filename)
        {
            // Open the package.

            Package package = Package.Open(filename);

            // Create the PackageDigitalSignatureManager

            PackageDigitalSignatureManager dsm =
                new PackageDigitalSignatureManager(package);

            // Verify the collection of certificates in the package (one, in this case)

            foreach (PackageDigitalSignature signature in dsm.Signatures)
            {
                if (PackageDigitalSignatureManager.VerifyCertificate(signature.Signer)
                    != X509ChainStatusFlags.NoError)
                {
                    // Application-specific code for error handling 
                    // or certificate validation 
                }
            }

            // For this example, if all certificates are valid,
            // verify all signatures in the package.

            VerifyResult vResult = dsm.VerifySignatures(false);
            Console.WriteLine("Result " + vResult.ToString());

            // Close the package.

            package.Close();
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtFolderSign, errorProvider, "Thư mục lưu file cần ký", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtFolderSigned, errorProvider, "Thư mục lưu file đã ký", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtCertiface, errorProvider, "File Certiface", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtPassword, errorProvider, "Password", isOnlyWarning);

            }
            catch (Exception ex)
            {
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public bool AddSignature(string PathFileName, System.Security.Cryptography.X509Certificates.X509Certificate2 certificate, string PathOutput)
      {
      try
      {
        List<Uri> partsToSign = new List<Uri>();
        List<PackageRelationshipSelector> relationshipsToSign = new List<PackageRelationshipSelector>();
        List<Uri> uriList = new List<Uri>();
        if (File.Exists(PathFileName.ToString()))
        {
            File.Delete(PathFileName.ToString());
        }
        File.Copy(PathFileName, PathOutput);
        Package package = Package.Open(PathOutput, FileMode.Open, FileAccess.ReadWrite);
        IEnumerator<PackageRelationship> enumerator = package.GetRelationshipsByType("http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument").GetEnumerator();
        while (enumerator.MoveNext())
          this.AddSignableItems(enumerator.Current, partsToSign, relationshipsToSign);
        if (enumerator != null)
          enumerator.Dispose();
        new PackageDigitalSignatureManager(package)
        {
          CertificateOption = CertificateEmbeddingOption.InSignaturePart
        }.Sign((IEnumerable<Uri>)partsToSign, (System.Security.Cryptography.X509Certificates.X509Certificate)certificate);
        package.Close();
      }
      catch (Exception ex)
      {
      }
      return true;
    }
        private void AddSignableItems(PackageRelationship relationship, List<Uri> partsToSign, List<PackageRelationshipSelector> relationshipsToSign)
 {
     PackageRelationshipSelector relationshipSelector = new PackageRelationshipSelector(relationship.SourceUri, PackageRelationshipSelectorType.Id, relationship.Id);
     relationshipsToSign.Add(relationshipSelector);
     if (relationship.TargetMode != TargetMode.Internal)
         return;
     PackagePart part = relationship.Package.GetPart(PackUriHelper.ResolvePartUri(relationship.SourceUri, relationship.TargetUri));
     if (partsToSign.Contains(part.Uri))
         return;
     partsToSign.Add(part.Uri);
     IEnumerator<PackageRelationship> enumerator;
     try
     {
         enumerator = part.GetRelationships().GetEnumerator();
         while (enumerator.MoveNext())
             this.AddSignableItems(enumerator.Current, partsToSign, relationshipsToSign);
     }
     finally
     {
     }
 }
        private bool SignExcel(string FileTempName, string FileName, X509Certificate2 pk)
        {
            var source = Path.Combine(txtFolderSign.Text, FileName);
            var destination = Path.Combine(txtFolderSigned.Text, FileName.Replace(".xlsx", " Signed.xlsx"));
            object TempFilePath = txtFolderSigned.Text + "\\" + FileName.Replace(".xlsx", " Signed.xlsx");
            if (File.Exists(TempFilePath.ToString()))
            {
                File.Delete(TempFilePath.ToString());
            }
            File.Copy(source, destination);
            Microsoft.Office.Interop.Excel.Application xlApp;
            Microsoft.Office.Interop.Excel.Workbooks xlWBs;
            Microsoft.Office.Interop.Excel.Workbook xlWB;
            Microsoft.Office.Interop.Excel.Sheets xlSheets;
            Microsoft.Office.Interop.Excel.Worksheet xlWS;
            Microsoft.Office.Interop.Excel.PivotTable xlPT;

            xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlApp.Visible = true;
            xlWBs = xlApp.Workbooks;

            xlWB = xlWBs.Open(TempFilePath.ToString(), Missing.Value, Missing.Value,
            Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
            Missing.Value, Missing.Value, Missing.Value, Missing.Value);

            //Microsoft.Office.Interop.Excel.Application xlapp = null;
            //xlapp = new Microsoft.Office.Interop.Excel.Application();
            //Microsoft.Office.Interop.Excel.Workbook xlwb = xlapp.Workbooks.Add("");
            //object sigID = "{00000000-0000-0000-0000-000000000000}";
            //xlapp.DisplayAlerts = false;
            //xlwb.Signatures.AddNonVisibleSignature(sigID);
            //xlapp.DisplayAlerts = true;
            SignatureSet signatureSet = xlWB.Signatures;

            RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)pk.PrivateKey;
            CspParameters cspp = new CspParameters();
            cspp.KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName;
            cspp.ProviderName = rsa.CspKeyContainerInfo.ProviderName;

            cspp.ProviderType = rsa.CspKeyContainerInfo.ProviderType;
            var secure = new SecureString();
            foreach (char c in txtPassword.Text)
            {
                secure.AppendChar(c);
            }

            cspp.KeyPassword = secure;
            cspp.Flags = CspProviderFlags.UseExistingKey;

            RSACryptoServiceProvider rsa2 = new RSACryptoServiceProvider(cspp);

            rsa.PersistKeyInCsp = true;
            this.MinimumSize = new System.Drawing.Size(623, 603);
            Microsoft.Office.Core.Signature objSignature = signatureSet.Add();
            if (objSignature == null)
            {
                return false;
            }
            else
            {
                signatureSet.Commit();
                object saveChanges = true;
                xlWB.Close(Missing.Value, Missing.Value, Missing.Value);
                xlApp.Quit();
                return true;
            }
        }
        public bool CheckSigned(string filePath)
        {

            if (!File.Exists(filePath))
            {
                MessageBoxControl.ShowMessage("File not found");
                return false;
            }

            System.Security.Cryptography.X509Certificates.X509Certificate2 theCertificate;

            try
            {
                System.Security.Cryptography.X509Certificates.X509Certificate theSigner = System.Security.Cryptography.X509Certificates.X509Certificate.CreateFromSignedFile(filePath);
                theCertificate = new System.Security.Cryptography.X509Certificates.X509Certificate2(theSigner);
                return true;
            }
            catch (Exception ex)
            {
                MessageBoxControl.ShowMessage("No digital signature found: " + ex.Message);

                return false;
            }
        }
        public void SignXmlFile(string FileName, string SignedFileName, string SubjectName)
    {
        if (null == FileName)
            throw new ArgumentNullException("FileName");
        if (null == SignedFileName)
            throw new ArgumentNullException("SignedFileName");
        if (null == SubjectName)
            throw new ArgumentNullException("SubjectName");

        // Load the certificate from the certificate store.
        X509Certificate2 cert = GetCertificateBySubject(SubjectName);

        // Create a new XML document.
        XmlDocument doc = new XmlDocument();

        // Format the document to ignore white spaces.
        doc.PreserveWhitespace = false;

        // Load the passed XML file using it's name.
        doc.Load(new XmlTextReader(FileName));

        // Create a SignedXml object.
        SignedXml signedXml = new SignedXml(doc);

        // Add the key to the SignedXml document. 
        signedXml.SigningKey = cert.PrivateKey;

        // Create a reference to be signed.
        Reference reference = new Reference();
        reference.Uri = "";

        // Add an enveloped transformation to the reference.
        XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
        reference.AddTransform(env);

        // Add the reference to the SignedXml object.
        signedXml.AddReference(reference);

        // Create a new KeyInfo object.
        KeyInfo keyInfo = new KeyInfo();

        // Load the certificate into a KeyInfoX509Data object
        // and add it to the KeyInfo object.
        keyInfo.AddClause(new KeyInfoX509Data(cert));

        // Add the KeyInfo object to the SignedXml object.
        signedXml.KeyInfo = keyInfo;

        // Compute the signature.
        signedXml.ComputeSignature();

        // Get the XML representation of the signature and save
        // it to an XmlElement object.
        XmlElement xmlDigitalSignature = signedXml.GetXml();

        // Append the element to the XML document.
        doc.DocumentElement.AppendChild(doc.ImportNode(xmlDigitalSignature, true));


        if (doc.FirstChild is XmlDeclaration)
        {
            doc.RemoveChild(doc.FirstChild);
        }

        // Save the signed XML document to a file specified
        // using the passed string.
        using (XmlTextWriter xmltw = new XmlTextWriter(SignedFileName, new UTF8Encoding(false)))
        {
            doc.WriteTo(xmltw);
            xmltw.Close();
        }

    }

    // Verify the signature of an XML file against an asymetric 
    // algorithm and return the result.
         public static Boolean VerifyXmlFile(String FileName, String CertificateSubject)
    {
        // Check the args.
        if (null == FileName)
            throw new ArgumentNullException("FileName");
        if (null == CertificateSubject)
            throw new ArgumentNullException("CertificateSubject");

        // Load the certificate from the store.
        X509Certificate2 cert = GetCertificateBySubject(CertificateSubject);

        // Create a new XML document.
        XmlDocument xmlDocument = new XmlDocument();

        // Load the passed XML file into the document. 
        xmlDocument.Load(FileName);

        // Create a new SignedXml object and pass it
        // the XML document class.
        SignedXml signedXml = new SignedXml(xmlDocument);

        // Find the "Signature" node and create a new
        // XmlNodeList object.
        XmlNodeList nodeList = xmlDocument.GetElementsByTagName("Signature");

        // Load the signature node.
        signedXml.LoadXml((XmlElement)nodeList[0]);

        // Check the signature and return the result.
        return signedXml.CheckSignature(cert, true);

    }


      public static X509Certificate2 GetCertificateBySubject(string CertificateSubject)
        {
        // Check the args.
        if (null == CertificateSubject)
            MessageBoxControl.ShowMessage("CertificateSubject");
            //throw new ArgumentNullException("CertificateSubject");


        // Load the certificate from the certificate store.
        X509Certificate2 cert = null;

        X509Store store = new X509Store("My", StoreLocation.CurrentUser);

        try
        {
            // Open the store.
            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

            // Get the certs from the store.
            X509Certificate2Collection CertCol = store.Certificates;

            // Find the certificate with the specified subject.
            foreach (X509Certificate2 c in CertCol)
            {
                if (c.Subject == CertificateSubject)
                {
                    cert = c;
                    break;
                }
            }

            // Throw an exception of the certificate was not found.
            if (cert == null)
            {
                MessageBoxControl.ShowMessage("The certificate could not be found.");
                //throw new CryptographicException("The certificate could not be found.");
            }
        }
        catch (Exception ex)
        {
            MessageBoxControl.ShowMessage(ex.Message);
        }
        finally
        {
            // Close the store even if an exception was thrown.
            store.Close();
        }

        return cert;
    }

    // Create example data to sign.
    public static void CreateSomeXml(string FileName)
    {
        // Check the args.
        if (null == FileName)
            throw new ArgumentNullException("FileName");

        // Create a new XmlDocument object.
        XmlDocument document = new XmlDocument();

        // Create a new XmlNode object.
        XmlNode node = document.CreateNode(XmlNodeType.Element, "", "MyElement", "samples");

        // Add some text to the node.
        node.InnerText = "Example text to be signed.";

        // Append the node to the document.
        document.AppendChild(node);

        // Save the XML document to the file name specified.
        using (XmlTextWriter xmltw = new XmlTextWriter(FileName, new UTF8Encoding(false)))
        {
            document.WriteTo(xmltw);

            xmltw.Close();
        }
    }

        private bool SignWord(string FileTempName,string FileName,X509Certificate2 pk)
        {
            object Visible = false;
            object readonlyfile = false;

            //string FolderTemp = System.Windows.Forms.Application.StartupPath + "\\FolderTemp";
            //if (!Directory.Exists(FolderTemp))
            //{
            //    Directory.CreateDirectory(FolderTemp);
            //}
            var source = Path.Combine(txtFolderSign.Text, FileName);
            var destination = Path.Combine(txtFolderSigned.Text, FileName.Replace(".docx"," Signed.docx"));
            object TempFilePath = txtFolderSigned.Text + "\\" + FileName.Replace(".docx", " Signed.docx");
            if (File.Exists(TempFilePath.ToString()))
            {
                File.Delete(TempFilePath.ToString());
            }
            File.Copy(source, destination);
            try
            {
                object missing = System.Reflection.Missing.Value;
                Microsoft.Office.Interop.Word.ApplicationClass wordapp = new
                Microsoft.Office.Interop.Word.ApplicationClass();
                Microsoft.Office.Interop.Word.Document wordDocument = wordapp.Documents.Open(ref
                TempFilePath, ref missing,
                ref readonlyfile, ref missing, ref missing,
                ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing,
                ref Visible, ref missing, ref missing,
                ref missing, ref missing);
                SignatureSet signatureSet = wordDocument.Signatures;
                wordapp.DisplayAlerts = WdAlertLevel.wdAlertsNone;
                RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)pk.PrivateKey;
                CspParameters cspp = new CspParameters();
                cspp.KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName;
                cspp.ProviderName = rsa.CspKeyContainerInfo.ProviderName;

                cspp.ProviderType = rsa.CspKeyContainerInfo.ProviderType;
                var secure = new SecureString();
                foreach (char c in txtPassword.Text)
                {
                    secure.AppendChar(c);
                }

                cspp.KeyPassword = secure;
                cspp.Flags = CspProviderFlags.UseExistingKey;

                RSACryptoServiceProvider rsa2 = new RSACryptoServiceProvider(cspp);

                rsa.PersistKeyInCsp = true;
                this.MinimumSize = new System.Drawing.Size(623, 603);
                Microsoft.Office.Core.Signature objSignature = signatureSet.Add();
                wordapp.DisplayAlerts = WdAlertLevel.wdAlertsNone;
                if (objSignature == null)
                {
                    return false;
                }
                else
                {
                    signatureSet.Commit();
                    object saveChanges = true;
                    wordDocument.Close(ref saveChanges, ref missing, ref missing);
                    wordapp.Quit(ref missing, ref missing, ref missing);
                    return true;
                }
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        private void signDocument(string fileNameInput)
        {
            Microsoft.Office.Interop.Word._Application appWord = null;
            Microsoft.Office.Interop.Word._Document docWord = null;

            object missing = System.Reflection.Missing.Value;
            object fileName = fileNameInput;
            object readOnly = false;
            object isVisible = true;

            appWord = new Microsoft.Office.Interop.Word.Application();
            appWord.Visible = true;

            docWord = appWord.Documents.Open(ref fileName, ref missing,
                ref readOnly, ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing,
                ref missing, ref isVisible, ref missing, ref missing,
                ref missing, ref missing);

            docWord.Activate();

            object sigID = "{00000000-0000-0000-0000-000000000000}";

            docWord.ActiveWindow.Application.ActiveDocument.Signatures.AddNonVisibleSignature(sigID);
            docWord.ActiveWindow.Application.ActiveDocument.Signatures.Commit();

        }
        public void Sign(String src, String dest,
                         ICollection<X509Certificate> chain, X509Certificate2 pk,
                         String digestAlgorithm, CryptoStandard subfilter,
                         String reason, String location,
                         ICollection<ICrlClient> crlList,
                         IOcspClient ocspClient,
                         ITSAClient tsaClient,
                         int estimatedSize)
        {
            // Creating the reader and the stamper
            PdfReader reader = null;
            PdfStamper stamper = null;
            FileStream os = null;
            try
            {
                reader = new PdfReader(src);
                os = new FileStream(dest, FileMode.Create);
                stamper = PdfStamper.CreateSignature(reader, os, '\0');
                // Creating the appearance

                int llx = 36;
                int lly = 700;
                int urx = 200;
                int ury = 806;
                System.Drawing.Rectangle rect1 = new System.Drawing.Rectangle(llx, lly, urx, ury);

                PdfSignatureAppearance appearance = stamper.SignatureAppearance;             
                appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(36, 700, 806, 806), 1, "sig");
                //appearance.Reason = reason;
                //appearance.Location = location;
                appearance.CertificationLevel = PdfSignatureAppearance.CERTIFIED_FORM_FILLING;
                appearance.SignatureRenderingMode = iTextSharp.text.pdf.PdfSignatureAppearance.RenderingMode.DESCRIPTION;
                // Creating the signature
                IExternalSignature pks = new X509Certificate2Signature(pk, digestAlgorithm);
                //
                RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)pk.PrivateKey;

                CspParameters cspp = new CspParameters();
                cspp.KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName;
                cspp.ProviderName = rsa.CspKeyContainerInfo.ProviderName;
                // cspp.ProviderName = "Microsoft Smart Card Key Storage Provider";

                cspp.ProviderType = rsa.CspKeyContainerInfo.ProviderType;
                var secure = new SecureString();
                foreach (char c in txtPassword.Text)
                {
                    secure.AppendChar(c);
                }
                
                cspp.KeyPassword = secure;
                cspp.Flags = CspProviderFlags.UseExistingKey;

                RSACryptoServiceProvider rsa2 = new RSACryptoServiceProvider(cspp);

                rsa.PersistKeyInCsp = true;
                MakeSignature.SignDetached(appearance, pks, chain, crlList, ocspClient, tsaClient, estimatedSize,
                                           subfilter);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (stamper != null)
                    stamper.Close();
                if (os != null)
                    os.Close();
            }
        }
        private void btnSign_Click(object sender, EventArgs e)
        {
            btnCheck_Click(null,null);
            if (!ValidateForm(false))
                return;
            if (dt.Rows.Count == 0)
            {
                MessageBoxControl.ShowMessage("Không tìm thấy File trong thư mục lưu File cần ký .Vui lòng kiểm tra lại thư mục ");
                return;
            }

            try
            {
                #region Signed Old
                //Cert myCertiface = null;
                //try
                //{
                //    byte[] cerData = Convert.FromBase64String(ConfigurationSettings.AppSettings.Get("CertifaceData"));
                //    if (cerData.Length > 0)
                //    {
                //        //
                //        myCertiface = new Cert(cerData, txtPassword.Text);
                //    }
                //    else
                //    {
                //        myCertiface = new Cert(txtCertiface.Text, txtPassword.Text);   
                //    }
                //}      
                //catch (Exception ex)
                //{
                //    MessageBoxControl.ShowMessage("Mật khẩu không đúng hoặc File Cerifacae không hợp lệ .\r\n" + ex.Message);
                //}
                //foreach (DataRow dr in dt.Rows)
                //{
                //    if (dr["FileName"].ToString().Contains(".pdf"))
                //    {
                //        MetaData MyMD = new MetaData();
                //        MyMD.Author = dr["Author"].ToString();
                //        MyMD.Title = dr["Title"].ToString();
                //        MyMD.Subject = dr["Subject"].ToString();
                //        MyMD.Keywords = dr["Keywords"].ToString();
                //        MyMD.Creator = dr["Creator"].ToString();
                //        MyMD.Producer = dr["Producer"].ToString();
                //        PDFSigner pdfs = new PDFSigner(dr["FilePath"].ToString(), dr["FileNameSigned"].ToString(), myCertiface, MyMD);
                //        pdfs.Sign(dr["Reasontext"].ToString(), dr["Contacttext"].ToString(), dr["Locationtext"].ToString(), false);
                //    }
                //    else
                //    {
                //        //SignWordDocument(dr["FilePath"].ToString());
                //    }
                //}
                #endregion
                #region
                LoggerFactory.GetInstance().SetLogger(new SysoLogger());


                X509Store x509Store = new X509Store("My");
                x509Store.Open(OpenFlags.ReadOnly);
                X509Certificate2Collection certificates = x509Store.Certificates;
                IList<X509Certificate> chain = new List<X509Certificate>();
                X509Certificate2 pk = null;
                ItemSign items = (ItemSign)cbSigns.SelectedItem;
                if (certificates.Count > 0)
                {
                    foreach (X509Certificate2 cer in certificates)
                    {

                        if (cer.SubjectName.Name == items.Value)
                        {
                            X509Certificate2Enumerator certificatesEn = certificates.GetEnumerator();
                            certificatesEn.MoveNext();
                            pk = certificatesEn.Current;

                            X509Chain x509chain = new X509Chain();
                            x509chain.Build(pk);

                            foreach (X509ChainElement x509ChainElement in x509chain.ChainElements)
                            {
                                chain.Add(DotNetUtilities.FromX509Certificate(x509ChainElement.Certificate));
                            }
                        }
                    }
                }
                x509Store.Close();


                IOcspClient ocspClient = new OcspClientBouncyCastle();
                ITSAClient tsaClient = null;
                for (int i = 0; i < chain.Count; i++)
                {
                    X509Certificate cert = chain[i];
                    String tsaUrl = CertificateUtil.GetTSAURL(cert);
                    if (tsaUrl != null)
                    {
                        tsaClient = new TSAClientBouncyCastle(tsaUrl);
                        break;
                    }
                }
                IList<ICrlClient> crlList = new List<ICrlClient>();
                crlList.Add(new CrlClientOnline(chain));
                ItemSign item = (ItemSign)cbSigns.SelectedItem;
                foreach (DataRow dr in dt.Rows)
                {
                    if (pk.SubjectName.Name == item.Value)
                    {
                        if (dr["FileName"].ToString().Contains(".pdf"))
                        {
                            Sign(dr["FilePath"].ToString(), dr["FileNameSigned"].ToString(), chain, pk, DigestAlgorithms.SHA1, CryptoStandard.CMS, "Test",
                                     "Ghent",
                                     crlList, ocspClient, tsaClient, 0);

                        }
                        else if (dr["FileName"].ToString().Contains(".docx"))
                        {
                            SignWord(dr["FilePath"].ToString(), dr["FileName"].ToString(), pk);
                        }
                        else if (dr["FileName"].ToString().Contains(".xlsx"))
                        {
                            SignExcel(dr["FilePath"].ToString(), dr["FileName"].ToString(), pk);
                        }
                        else if (dr["FileName"].ToString().Contains(".xml"))
                        {
                            SignXmlFile(dr["FileName"].ToString(), dr["FilePath"].ToString(), item.Name);
                        }
                    }
                }
                #endregion
                MessageBoxControl.ShowMessage("Ký  thành công ");
            }
            catch (Exception ex)
            {
                MessageBoxControl.ShowMessage("Ký không thành công \r\n" + ex.Message);
            }
        }
        private void btnOpenFolder_Click(object sender, EventArgs e)
        {
            Process.Start(txtFolderSigned.Text);
        }

        private void Main_Load(object sender, EventArgs e)
        {
            //Load Config
            btnRefersh_Click(null, null);
            foreach (ItemSign item in cbSigns.Items)
            {
                if (item.Value == ConfigurationSettings.AppSettings.Get("ChuKySo"))
                {
                    cbSigns.SelectedItem = item;
                    break;
                }
            }
            try
            {
                txtFolderSign.Text = ConfigurationSettings.AppSettings.Get("FolderSign");
                txtFolderSigned.Text = ConfigurationSettings.AppSettings.Get("FolderSigned");
                chkFolder.Checked = Convert.ToBoolean(ConfigurationSettings.AppSettings.Get("IsRememberPath"));
                if (chkFolder.Checked)
                {
                    IsSelectFolder = false;
                    btnFolderSign_Click(null, null);
                }
                chkCertiface.Checked = Convert.ToBoolean(ConfigurationSettings.AppSettings.Get("IsRememberCer"));
                chkPassword.Checked = Convert.ToBoolean(ConfigurationSettings.AppSettings.Get("IsRememerPassword"));
                txtPassword.Text = DecryptString(ConfigurationSettings.AppSettings.Get("PasswordSign"), "KEYWORD");
            }
            catch (Exception)
            {
                LoggerFactory.GetInstance().Logger();
            }
        }

        private void chkPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPassword.Checked)
            {
                UpdateSetting("IsRememerPassword", "True");
                UpdateSetting("PasswordSign", EncryptString(txtPassword.Text, "KEYWORD"));
            }
            else
            {
                UpdateSetting("IsRememerPassword", "False");
                UpdateSetting("PasswordSign", EncryptString(txtPassword.Text, "KEYWORD"));
            }
        }

        private void chkFolder_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFolder.Checked)
            {
                UpdateSetting("IsRememberPath", "True");
                UpdateSetting("FolderSign", txtFolderSign.Text);
                UpdateSetting("FolderSigned", txtFolderSigned.Text);
            }
            else
            {
                UpdateSetting("IsRememberPath", "False");
                UpdateSetting("FolderSign", "");
                UpdateSetting("FolderSigned", "");
            }
        }
        private string GetName(string signName)
        {
            string[] infors = signName.Split(',');
            string name = string.Empty;
            string mst = string.Empty;
            foreach (string info in infors)
            {
                int index = info.IndexOf("CN=");
                if (index > -1)
                {
                    name = info.Substring(index + 3);
                }
                index = info.IndexOf("OID");

                if (index > -1)
                {
                    index = info.IndexOf("=");
                    if (index > 0)
                        mst = info.Substring(index + 1);
                }
            }
            if (!string.IsNullOrEmpty(mst)) return string.Format("{0} - [{1}]", name, mst);
            else return string.Format("{0}", name);
        }
        public static string EncryptString(string plainText, string keyword)
        {
            byte[] text = Encoding.ASCII.GetBytes(plainText);

            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            byte[] salt = Encoding.ASCII.GetBytes(keyword);

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(keyword, salt);

            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();

            CryptoStream encStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);
            encStream.Write(text, 0, text.Length);

            encStream.FlushFinalBlock();

            byte[] CipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            encStream.Close();

            return Convert.ToBase64String(CipherBytes);
        }
        public static string DecryptString(string plainText, string keyWord)
        {
            byte[] text = Convert.FromBase64String(plainText);

            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            byte[] salt = Encoding.ASCII.GetBytes(keyWord);

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(keyWord, salt);

            ICryptoTransform decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream(text);

            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            text = new byte[text.Length];
            int DecryptedCount = cryptoStream.Read(text, 0, text.Length);

            memoryStream.Close();
            cryptoStream.Close();


            return Encoding.ASCII.GetString(text);
        }
        public static System.Security.Cryptography.X509Certificates.X509Certificate2 GetStoreX509Certificate2(string name)
        {
            //if (System.Security.Cryptography.X509Certificates.X509Certificate2()  x509Certificate2 != null && System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2.SubjectName.Name.ToString() == name) return System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2;
            X509Certificate2 x509Certificate2_1 = null;
            X509Store x509Store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            x509Store.Open(OpenFlags.ReadOnly);
            X509Certificate2Enumerator x509Certificate2Enumerator = x509Store.Certificates.GetEnumerator();
            while (x509Certificate2Enumerator.MoveNext())
            {
                X509Certificate2 x509Certificate2_2 = x509Certificate2Enumerator.Current;
                if (x509Certificate2_2.SubjectName.Name.ToString() == name)
                {
                    System.DateTime dateTime = System.DateTime.Now;
                    if (!(dateTime > x509Certificate2_2.NotAfter || dateTime < x509Certificate2_2.NotBefore))
                        x509Certificate2_1 = x509Certificate2_2;
                }
            }
            System.Security.Cryptography.X509Certificates.X509Certificate2 X509Certificate2 = x509Certificate2_1;
            return x509Certificate2_1;
        }
        public static List<System.Security.Cryptography.X509Certificates.X509Certificate2> GetX509CertificatedNames()
        {
            System.Collections.Generic.List<System.Security.Cryptography.X509Certificates.X509Certificate2> list =
                new System.Collections.Generic.List<System.Security.Cryptography.X509Certificates.X509Certificate2>();
            System.Security.Cryptography.X509Certificates.X509Store x509Store = new System.Security.Cryptography.X509Certificates.X509Store(System.Security.Cryptography.X509Certificates.StoreName.My, System.Security.Cryptography.X509Certificates.StoreLocation.CurrentUser);
            x509Store.Open(System.Security.Cryptography.X509Certificates.OpenFlags.ReadOnly);
            System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator x509Certificate2Enumerator = x509Store.Certificates.GetEnumerator();
            System.DateTime dateTime = System.DateTime.Now;

            while (x509Certificate2Enumerator.MoveNext())
            {
                System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = x509Certificate2Enumerator.Current;
                string s = x509Certificate2.SubjectName.Name.ToString();
                if (s != "" && !(dateTime > x509Certificate2.NotAfter || dateTime < x509Certificate2.NotBefore))
                    list.Add(x509Certificate2);
            }
            return list;
        }
        int DropDownWidth(ComboBox myCombo)
        {
            int maxWidth = 0;
            int temp = 0;
            Label label1 = new Label();

            foreach (var obj in myCombo.Items)
            {
                label1.Text = obj.ToString();
                temp = label1.PreferredWidth;
                if (temp > maxWidth)
                {
                    maxWidth = temp;
                }
            }
            label1.Dispose();
            return maxWidth;
        }
        private void btnRefersh_Click(object sender, EventArgs e)
        {

            List<System.Security.Cryptography.X509Certificates.X509Certificate2> items = GetX509CertificatedNames();
            cbSigns.Items.Clear();

            foreach (System.Security.Cryptography.X509Certificates.X509Certificate2 item in items)
            {
                string signName = item.SubjectName.Name.ToString();
                string name = "";
                name = GetName(signName);
                cbSigns.Items.Add(new ItemSign() { Name = name, Value = signName });
            }
            try
            {
                cbSigns.DropDownWidth = DropDownWidth(cbSigns);
            }
            catch (Exception)
            {

                MessageBoxControl.ShowMessage("Thiết bị chữ ký số đã được tháo ra . Vui lòng gắn thiết bị chữ ký số vào để cấu hình.");
            }

        }

        private void cbSigns_TextChanged(object sender, EventArgs e)
        {
            ItemSign sign = (ItemSign)cbSigns.SelectedItem;
            X509Certificate2 x059 = GetStoreX509Certificate2(sign.Value);

            if (x059 != null)
            {
                lblProduct.Text = GetName(x059.IssuerName.Name);
            }
        }
        static string _GetX509CertificatedName = string.Empty;
        public static string GetX509CertificatedName
        {
            get
            {
                return _GetX509CertificatedName;
            }
            set
            {
                _GetX509CertificatedName = value;
            }
        }
        public string GetSignature(string content ,string Password,string name)
        {
            string ret = "";
                try
                {
                    if (!string.IsNullOrEmpty(name))
                    {

                        System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = GetStoreX509Certificate2(name);
                        if (x509Certificate2 != null)
                        {
                            RSACryptographyHelper rsacryptography = new RSACryptographyHelper(x509Certificate2);

                            string sData = rsacryptography.SignHash(content, Password);
                            string sCert = rsacryptography.RawData;
                            //ret.Data = sData;
                            //ret.FileCert = sCert;
                        }
                        else
                            return "Bạn đã thiết lập cấu hình chữ ký số\r\nLỗi là do thiết bị (Usb token) này chưa được gắn và";
                            //throw new Exception("Bạn đã thiết lập cấu hình chữ ký số\r\nLỗi là do thiết bị (Usb token) này chưa được gắn vào");
                    }
                    else return "Bạn đã thiết lập cấu hình chữ ký số\r\nLỗi là do loại chữ ký số chưa được xác định";//throw new Exception("Bạn đã thiết lập cấu hình chữ ký số\r\nLỗi là do loại chữ ký số chưa được xác định");
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("wrong PIN") || ex.Message.Contains("PIN is incorrect"))
                    {
                        return "Mật khẩu xác nhận bị sai\r\nVui lòng thử lại";
                        //throw new Exception("Mật khẩu xác nhận bị sai\r\nVui lòng thử lại");
                    }
                    else
                    {
                        return "Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin";
                        //throw new Exception("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin");
                    }
                }
                return ret = "Mật khẩu được xác nhận là hợp lệ";
        }
        private string Check()
        {
            string msg = string.Empty;
            try
            {
                ItemSign item = (ItemSign)cbSigns.SelectedItem;
                string Result = GetSignature("Test",txtPassword.Text,item.Value);
                return Result;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            MessageBoxControl.ShowMessage(Check());
        }

        private void chkCertiface_CheckedChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cbSigns.Text))
            {
                MessageBoxControl.ShowMessage("Chưa có chữ ký số được chọn\r\nBạn có muốn lưu không?");
            }
            else
            {
                if (chkCertiface.Checked)
                {
                    ItemSign sign = (ItemSign)cbSigns.SelectedItem;
                    UpdateSetting("ChuKySo", sign.Value);
                    UpdateSetting("IsRememberCer", "True");
                    //UpdateSetting("PasswordSign", EncryptString(txtPassword.Text, "KEYWORD"));
                }
                else
                {
                    UpdateSetting("ChuKySo", "");
                    UpdateSetting("IsRememberCer","False");
                    //UpdateSetting("PasswordSign", "");
                }
            }
        }
    }
}