﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;

using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface
{
    public partial class HuyToKhaiCTForm : BaseForm
    {

        public ToKhaiChuyenTiep TKCT = null;

        public HuyToKhaiCT huyTK = null;

        public HuyToKhaiCTForm()
        {
            InitializeComponent();
        }

        private void HuyToKhaiForm_Load(object sender, EventArgs e)
        {
            string sfmtWhere = string.Format("TKCT_ID={0}", TKCT.ID);
            List<HuyToKhaiCT> listHuyTK = (List<HuyToKhaiCT>)HuyToKhaiCT.SelectCollectionDynamic(sfmtWhere, "");

            if (listHuyTK.Count > 0)
                huyTK = listHuyTK[0];
            if (huyTK != null)
            {
                txtLyDoHuy.Text = huyTK.LyDoHuy;
                if (huyTK.SoTiepNhan > 0)
                {
                    txtSoTiepNhan.Text = huyTK.SoTiepNhan.ToString();
                    ccNgayTiepNhan.Text = huyTK.NgayTiepNhan.ToShortDateString();
                }
                if (huyTK.TrangThai == TrangThaiXuLy.CHO_HUY)
                {
                    btnKhaiBao.Enabled = false;
                    btnGhi.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
                }
                else if (huyTK.TrangThai == TrangThaiXuLy.DA_HUY)
                {
                    lblTrangThai.Text = TrangThaiXuLy.strDAHUY;
                    btnGhi.Enabled = false;
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = false;
                }
                else if (huyTK.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    btnGhi.Enabled = true;
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = false;
                    lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                }
            }
            else
            {
                lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                huyTK = new Company.KDT.SHARE.QuanLyChungTu.HuyToKhaiCT();
                btnKhaiBao.Enabled = false;
                btnGhi.Enabled = true;
                btnLayPhanHoi.Enabled = false;
            }
            lblSoTK.Text = TKCT.SoToKhai.ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (txtLyDoHuy.Text.Trim() == "")
            {
                ShowMessage("Phải nhập lý do hủy!", false);
                return;
            }
            try
            {
                huyTK.LyDoHuy = txtLyDoHuy.Text.Trim();
                huyTK.TKCT_ID = TKCT.ID;

                huyTK.Guid = Guid.NewGuid().ToString();

                if (huyTK.NgayTiepNhan.Year < 1900)
                    huyTK.NgayTiepNhan = new DateTime(1900, 1, 1);
                if (huyTK.ID == 0)
                {
                    huyTK.Insert();
                }
                else
                    huyTK.Update();
                ShowMessage("Lưu thành công.", false);
                btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new SendEventArgs(ex));
            }
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            SendV3();
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            FeedBackV3();
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.TKCT.ID;
            bool isToKhaiNhap = TKCT.MaLoaiHinh.StartsWith("N");
            form.DeclarationIssuer = isToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_NHAP : DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_XUAT;
            form.ShowDialog(this);

        }
        #region Khai báo hủy V3
        private void SendV3()
        {

            TKCT.ActionStatus = (int)ActionStatus.ToKhaiXinHuy;
            ObjectSend msgSend = SingleMessage.CancelMessageCT(TKCT);

            SendMessageForm dlgSend = new SendMessageForm();
            //TKCT.ActionStatus = (int)ActionStatus.ToKhaiXinHuy;
            
            dlgSend.Send += SendMessage;
            bool isSend = dlgSend.DoSend(msgSend, true);
            if (isSend)
            {
                TKCT.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                dlgSend.Message.XmlSaveMessage(TKCT.ID, MessageTitle.HuyToKhai);
                TKCT.Update();
            }
        }
        private void FeedBackV3()
        {
            ObjectSend msgSend = SingleMessage.FeedBackCT(TKCT, TKCT.GUIDSTR);
            SendMessageForm dlgSendForm = new SendMessageForm();
            dlgSendForm.Send += SendMessage;
            dlgSendForm.DoSend(msgSend);

        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    FeedBackContent feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function)
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {
                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                TKCT.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                btnKhaiBao.Enabled = true;
                                btnLayPhanHoi.Enabled = false;
                                lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                                this.ShowMessageTQDT(noidung, false);
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            btnKhaiBao.Enabled = false;
                            btnLayPhanHoi.Enabled = true;
                            btnGhi.Enabled = false;
                            lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
                            this.ShowMessage(noidung, false);
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {

                                string[] vals = noidung.Split('/');

                                huyTK.SoTiepNhan = long.Parse(feedbackContent.CustomsReference);

                                huyTK.NgayTiepNhan = SingleMessage.GetDate(feedbackContent.Acceptance, feedbackContent.Issue);

                                huyTK.NamTiepNhan = huyTK.NgayTiepNhan.Year;

                                noidung = string.Format("Khai báo hủy tờ khai\r\nSố tiếp nhận: {0} \r\nNgày tiếp nhận: {1}\r\nNăm tiếp nhận: {2}\r\nLoại hình: {3}\r\nHải quan: {4}", new object[] {
                                    huyTK.SoTiepNhan, huyTK.NgayTiepNhan.ToString("dd-MM-yyyy HH:mm:ss"), TKCT.NamDK, TKCT.MaLoaiHinh, TKCT.MaHaiQuanTiepNhan });

                                e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.ToKhaiDuocCapSoTiepNhan, noidung);

                                if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                                {
                                    noidung = "Tờ khai được cấp số tiếp nhận\r\n" + noidung;
                                    TKCT.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;

                                }
                                else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                {
                                    noidung = "Tờ khai được cấp số chờ hủy khai báo\r\n" + noidung;
                                    TKCT.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }

                                txtSoTiepNhan.Text = this.huyTK.SoTiepNhan.ToString("N0");
                                ccNgayTiepNhan.Value = this.huyTK.NgayTiepNhan;
                                ccNgayTiepNhan.Text = this.huyTK.NgayTiepNhan.ToShortDateString();
                                lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;

                                this.ShowMessageTQDT(noidung, false);
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {

                                TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                noidung = "Tờ khai được chấp nhận hủy\r\n" + noidung;
                                e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.HuyToKhaiThanhCong, noidung);
                                this.ShowMessage(noidung, false);
                                e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.ToKhaiDuocPhanLuong, noidung);
                            }
                            break;
                        default:
                            e.FeedBackMessage.XmlSaveMessage(TKCT.ID, MessageTitle.Error, noidung);
                            break;
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        TKCT.Update();
                }
                else
                {
                    SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new SendEventArgs(e.Error));
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                }
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new SendEventArgs(ex));
            }
        }
        #endregion
    }
}

