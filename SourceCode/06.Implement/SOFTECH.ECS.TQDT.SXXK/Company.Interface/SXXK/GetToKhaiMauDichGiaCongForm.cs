﻿

using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean=Janus.Windows.UI.InheritableBoolean;
using Company.Interface.Report;
using System.Data;


namespace Company.Interface.SXXK
{
	public partial class GetToKhaiMauDichGiaCongForm : BaseForm
	{
        public DataTable dtToKhaiXGC = new DataTable();
        public GridEXRow[] rowCollection;
        public GetToKhaiMauDichGiaCongForm()
		{
			InitializeComponent();
		}

		//-----------------------------------------------------------------------------------------

   
		private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
		{
            BindData();
           
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        public void BindData()
        {
            try
            {
                ToKhaiMauDich tkmd= new ToKhaiMauDich();
                string where = "MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' and MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'";
                dtToKhaiXGC = ToKhaiMauDich.GetToKhaiXGC(where);                               
                int i = 0;
                DataRow row    ;
                while (i < dtToKhaiXGC.Rows.Count)
                {                
                  row   =dtToKhaiXGC.Rows[i];  
                  ToKhaiMauDich TKMD=new ToKhaiMauDich();
                  TKMD.SoToKhai = Convert.ToInt32(row["SoToKhai"].ToString());
                  TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                  TKMD.MaLoaiHinh = row["MaLoaiHinh"].ToString();
                  TKMD.NamDangKy = (short)Convert.ToDateTime(row["NgayDangKy"].ToString()).Year;                                     
                  if(TKMD.Load())
                  {                      
                      dtToKhaiXGC.Rows.Remove(row);                      
                      if(i>0)i--;
                      
                  }else{ i++;    }

                }               
                dgList.DataSource =dtToKhaiXGC;
                dgList.Refetch();
                
            }
            catch( Exception ex)
            {
                dgList.Refresh();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string loaiHinh = (e.Row.Cells["MaLoaiHinh"].Value.ToString());
                DateTime time = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text);
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + GlobalSettings.MA_HAI_QUAN + "/" + time.Year.ToString();

                string StatusPhanBo = e.Row.Cells["TrangThai"].Text;
                if (GlobalSettings.NGON_NGU == "0")
                {
                    if (StatusPhanBo == "1")
                    {
                        e.Row.Cells["TrangThai"].Text = "Đã phân bổ";
                    }
                    else
                        e.Row.Cells["TrangThai"].Text = "Chưa phân bổ";

                    if (Convert.ToDateTime(e.Row.Cells["NGAY_THN_THX"].Value).Year <= 1900)
                    {
                        e.Row.Cells["NGAY_THN_THX"].Text = "";
                    }
                }
                else
                {
                    if (StatusPhanBo == "1")
                    {
                        e.Row.Cells["TrangThai"].Text = "Allocated";
                    }
                    else
                        e.Row.Cells["TrangThai"].Text = "Not yet Allocated";

                    if (Convert.ToDateTime(e.Row.Cells["NGAY_THN_THX"].Value).Year <= 1900)
                    {
                        e.Row.Cells["NGAY_THN_THX"].Text = "";
                    }
                }
            }
        }

        private void btnGetTKXGC_Click(object sender, EventArgs e)
        {
            GridEXRow[] rowCollection= dgList.GetCheckedRows();
            if (rowCollection.Length <1 )
                {
                    ShowMessage(setText("Bạn chưa chọn tờ khai muốn đồng bộ.","You have not selected the declaration yet !"),false);
                    return;
                }
            foreach (GridEXRow row in rowCollection)
            {
                DataRowView dataRow = (DataRowView)row.DataRow;
                DataRow dtRow=    dataRow.Row;
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.SoToKhai = Convert.ToInt32(dataRow["SoToKhai"].ToString());
                TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                TKMD.MaLoaiHinh = dataRow["MaLoaiHinh"].ToString();
                TKMD.NamDangKy = (short)Convert.ToDateTime(dataRow["NgayDangKy"].ToString()).Year;
                if (TKMD.Load())
                {
                    ShowMessage("Tờ khai : "+TKMD.SoToKhai+"/"+TKMD.MaHaiQuan+"/"+TKMD.MaLoaiHinh+"/"+TKMD.NamDangKy +" đã có trong hệ thống nên sẽ được bỏ qua.", false);
                    continue;
                }
                TKMD.ChiTietDonViDoiTac = dataRow["ChiTietDonViDoiTac"].ToString();
                TKMD.ChungTu = dataRow["GiayTo"].ToString();
                TKMD.CuaKhau_ID = dataRow["CuaKhau_ID"].ToString();
                TKMD.DiaDiemXepHang = dataRow["DiaDiemXepHang"].ToString();
                TKMD.DKGH_ID = dataRow["DKGH_ID"].ToString();
                TKMD.LoaiVanDon = dataRow["LoaiVanDon"].ToString();
                TKMD.MaDaiLyTTHQ = dataRow["MaDaiLyTTHQ"].ToString();
                TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                TKMD.MaDonViUT = dataRow["MaDonViUT"].ToString();
                TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                TKMD.MaLoaiHinh = dataRow["MaLoaiHinh"].ToString();
                TKMD.NGAY_THN_THX = Convert.ToDateTime(dataRow["NGAY_THN_THX"].ToString());
                TKMD.NgayDangKy = Convert.ToDateTime(dataRow["NgayDangKy"].ToString());
                TKMD.NgayDenPTVT = Convert.ToDateTime(dataRow["NgayDenPTVT"].ToString());
                TKMD.NgayGiayPhep = Convert.ToDateTime(dataRow["NgayGiayPhep"].ToString());
                TKMD.NgayHetHanGiayPhep = Convert.ToDateTime(dataRow["NgayHetHanGiayPhep"].ToString());
                TKMD.NgayHetHanHopDong = Convert.ToDateTime(dataRow["NgayHetHanHopDong"].ToString());
                TKMD.NgayHoaDonThuongMai = Convert.ToDateTime(dataRow["NgayHoaDonThuongMai"].ToString());
                TKMD.NgayHoanThanh = TKMD.NgayDangKy;
                TKMD.NgayHopDong = Convert.ToDateTime(dataRow["NgayHopDong"].ToString());
                TKMD.NgayVanDon = Convert.ToDateTime(dataRow["NgayVanDon"].ToString());
                TKMD.NguyenTe_ID = dataRow["NguyenTe_ID"].ToString();
                TKMD.NuocNK_ID = dataRow["NuocNK_ID"].ToString();
                TKMD.NuocXK_ID = dataRow["NuocXK_ID"].ToString();
                TKMD.PhiBaoHiem = Convert.ToDecimal(dataRow["PhiBaoHiem"].ToString());
                TKMD.PhiKhac = Convert.ToDecimal(dataRow["PhiKhac"].ToString());
                TKMD.PhiVanChuyen = Convert.ToDecimal(dataRow["PhiVanChuyen"].ToString());
                TKMD.PhiXepDoHang = Convert.ToDecimal(dataRow["PhiXepDoHang"].ToString());
                TKMD.PTTT_ID = dataRow["PTTT_ID"].ToString();
                TKMD.PTVT_ID = dataRow["PTVT_ID"].ToString();
                TKMD.QuocTichPTVT_ID = dataRow["QuocTichPTVT_ID"].ToString();
                TKMD.SoContainer20 = Convert.ToDecimal(dataRow["SoContainer20"].ToString());
                TKMD.SoContainer40 = Convert.ToDecimal(dataRow["SoContainer40"].ToString());
                TKMD.SoGiayPhep = dataRow["SoGiayPhep"].ToString();
                TKMD.SoHang = Convert.ToInt16(dataRow["SoHang"].ToString());
                TKMD.SoHieuPTVT = dataRow["SoHieuPTVT"].ToString();
                TKMD.SoHoaDonThuongMai = dataRow["SoHoaDonThuongMai"].ToString();
                TKMD.SoHopDong = dataRow["SoHopDong"].ToString();
                TKMD.SoKien = Convert.ToDecimal(dataRow["SoKien"].ToString());
                TKMD.SoLuongPLTK = Convert.ToInt16(dataRow["SoLuongPLTK"].ToString());
                TKMD.SoTienKhoan = Convert.ToDecimal(dataRow["SoTienKhoan"]);
                TKMD.SoVanDon = dataRow["SoVanDon"].ToString();
                TKMD.TenChuHang = dataRow["TenChuHang"].ToString();
                TKMD.TenDaiLyTTHQ = dataRow["TenDaiLyTTHQ"].ToString();
                TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                TKMD.TenDonViDoiTac = dataRow["TenDonViDoiTac"].ToString();
                TKMD.TongTriGiaKhaiBao = Convert.ToDecimal(dataRow["TongTriGiaKhaiBao"].ToString());
                TKMD.TongTriGiaTinhThue = Convert.ToDecimal(dataRow["TongTriGiaTinhThue"].ToString());
                TKMD.TrangThai = 2;
                TKMD.TrangThaiThanhKhoan = "";
                TKMD.TrongLuong = Convert.ToDecimal(dataRow["TrongLuong"].ToString());
                TKMD.TrongLuongNet = Convert.ToDouble(dataRow["TrongLuongNet"].ToString());
                TKMD.TyGiaTinhThue = Convert.ToDecimal(dataRow["TyGiaTinhThue"].ToString());
                TKMD.TyGiaUSD = Convert.ToDecimal(dataRow["TyGiaUSD"].ToString());
                TKMD.Xuat_NPL_SP = "S";
                TKMD.ThanhLy = "";
                TKMD.HMDCollection = new HangMauDichCollection();
                long idToKhaiXuatGC = Convert.ToInt64(dataRow["ID"].ToString());
                DataTable tableHang = ToKhaiMauDich.GetHangMauDichCuaToKhaiXGCByID(idToKhaiXuatGC);
                foreach (DataRow rowHang in tableHang.Rows)
                {
                    HangMauDich HMD = new HangMauDich();
                    HMD.DonGiaKB = Convert.ToDecimal(rowHang["DonGiaKB"].ToString());
                    HMD.DonGiaTT = Convert.ToDecimal(rowHang["DonGiaTT"].ToString());
                    HMD.DVT_ID = rowHang["DVT_ID"].ToString();
                    HMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    HMD.MaHS = rowHang["MaHS"].ToString();
                    HMD.MaLoaiHinh = TKMD.MaLoaiHinh;
                    HMD.MaPhu = rowHang["MaPhu"].ToString();
                    HMD.MaTMP = HMD.MaPhu;
                    HMD.MienThue = Convert.ToByte(rowHang["MienThue"].ToString());
                    HMD.NamDangKy = TKMD.NamDangKy;
                    HMD.NuocXX_ID = rowHang["NuocXX_ID"].ToString();
                    HMD.SoLuong = Convert.ToDecimal(rowHang["SoLuong"].ToString());
                    HMD.SoThuTuHang =Convert.ToInt16(rowHang["SoThuTuHang"].ToString());
                    HMD.SoToKhai = TKMD.SoToKhai;
                    HMD.TenHang = rowHang["TenHang"].ToString();
                    HMD.TriGiaKB = Convert.ToDecimal(rowHang["TriGiaKB"].ToString());
                    HMD.TriGiaKB_VND = Convert.ToDecimal(rowHang["TriGiaKB_VND"].ToString());
                    HMD.TriGiaTT = Convert.ToDecimal(rowHang["TriGiaTT"].ToString());
                    HMD.TyLeThuKhac = Convert.ToDecimal(rowHang["TyLeThuKhac"].ToString());

                    //Company.BLL.SXXK.SanPham SP = new Company.BLL.SXXK.SanPham();
                    //SP.Ma = HMD.MaPhu;
                    //SP.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    //SP.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    //if (!SP.Load())
                    //{
                    //   // string result=ShowMessage("Mã hàng : "+SP.Ma+" trong tờ khai "+TKMD.SoToKhai+"/"+TKMD.MaHaiQuan+"/"+TKMD.MaLoaiHinh+"/"+TKMD.NamDangKy +"không có trong danh mục sản phẩm của hệ thống.Bạn có muốn tiếp tục chuyển mặt hàng của tờ khai này không ?", true);
                    //    string result = MLMessages("Mã hàng : " + SP.Ma + " trong tờ khai " + TKMD.SoToKhai + "/" + TKMD.MaHaiQuan + "/" + TKMD.MaLoaiHinh + "/" + TKMD.NamDangKy + " không có trong danh mục sản phẩm của hệ thống.Bạn có muốn tiếp tục chuyển mặt hàng của tờ khai này không ?", "MSG_TK04", "" + TKMD.SoToKhai, true);
                    //    if (result != "Yes")
                    //    {
                    //        continue;
                    //    }
                    //}
                    TKMD.HMDCollection.Add(HMD);                   
                }
                try
                {
                    TKMD.InsertUpdateToKhaiGiaCong();
                    
                }
                catch (Exception ex)
                {
                  //  ShowMessage("Có lỗi khi chuyển tờ khai : "+TKMD.SoToKhai+"/"+TKMD.MaHaiQuan+"/"+TKMD.MaLoaiHinh+"/"+TKMD.NamDangKy+". Lỗi : "+ex.Message , false);
                    MLMessages("Có lỗi khi chuyển tờ khai : " + TKMD.SoToKhai + "/" + TKMD.MaHaiQuan + "/" + TKMD.MaLoaiHinh + "/" + TKMD.NamDangKy + ". Lỗi : " + ex.Message, "MSG_TK05", "" + TKMD.SoToKhai + "/" + TKMD.MaHaiQuan + "/" + TKMD.MaLoaiHinh + "/" + TKMD.NamDangKy, false);
                }           
            
                }
                BindData();

                            
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        


      

       
	
	}
}
