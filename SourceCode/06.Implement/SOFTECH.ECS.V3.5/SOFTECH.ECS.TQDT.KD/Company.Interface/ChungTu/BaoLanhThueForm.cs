﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components.Messages.Send;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif
using Logger;

namespace Company.Interface
{

    public partial class BaoLanhThueForm : Company.Interface.BaseForm
    {
        public bool isAddNew = true;
        public ToKhaiMauDich TKMD;
        public BaoLanhThue BaoLanhThue = new BaoLanhThue() {};
        public BaoLanhThueForm()
        {
            InitializeComponent();
        }
        private void Set()
        {
            txtDonViBaoLanh.Text = BaoLanhThue.DonViBaoLanh;
            ccNgayHetHieuLuc.Value = BaoLanhThue.NgayHetHieuLuc;
            ccNgayHieuLuc.Value = BaoLanhThue.NgayHieuLuc;
            cbLoaiBaoLanh.SelectedValue = BaoLanhThue.LoaiBaoLanh;
            txtSoGiayBaoLanh.Text = BaoLanhThue.SoGiayBaoLanh;
            ccNgayKyBaoLanh.Value = BaoLanhThue.NgayKyBaoLanh;
            txtNamChungTuBaoLanh.Value = BaoLanhThue.NamChungTuBaoLanh == 0  ? DateTime.Now.Year : BaoLanhThue.NamChungTuBaoLanh;
            txtSoNgayDuocBaoLanh.Value = BaoLanhThue.SoNgayDuocBaoLanh;
            txtSoDuTienBaoLanh.Value = BaoLanhThue.SoDuTienBaoLanh;
            txtSoTienBaoLanh.Value = BaoLanhThue.SoTienBaoLanh;

        
        }
        private void Get()
        {
            BaoLanhThue.DonViBaoLanh = txtDonViBaoLanh.Text;
            BaoLanhThue.NgayHetHieuLuc = ccNgayHetHieuLuc.Value;
            BaoLanhThue.NgayHieuLuc = ccNgayHieuLuc.Value;
            BaoLanhThue.LoaiBaoLanh = cbLoaiBaoLanh.SelectedValue.ToString();
            BaoLanhThue.SoGiayBaoLanh = txtSoGiayBaoLanh.Text;
            BaoLanhThue.NgayKyBaoLanh = ccNgayKyBaoLanh.Value;
            BaoLanhThue.NamChungTuBaoLanh = Convert.ToInt16(txtNamChungTuBaoLanh.Value);
            BaoLanhThue.SoNgayDuocBaoLanh = Convert.ToInt16(txtSoNgayDuocBaoLanh.Value);
            BaoLanhThue.SoDuTienBaoLanh = Convert.ToDecimal(txtSoDuTienBaoLanh.Value);
            BaoLanhThue.SoTienBaoLanh = Convert.ToDecimal(txtSoTienBaoLanh.Value);

        }
        private void ChungTuNoForm_Load(object sender, EventArgs e)
        {
            try
            { 
                //Load danh sach Loai chung tu
            if (BaoLanhThue != null) Set();
            else
            {
                txtNamChungTuBaoLanh.Value = DateTime.Now.Year;
                BaoLanhThue = new BaoLanhThue();
            }
            }
            catch (System.Exception ex)
            {
                ShowMessage(ex.Message, false);
                LocalLogger.Instance().WriteMessage(ex);
            }
           
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có thật sự muốn xóa không?", true) == "Yes")
            {
                BaoLanhThue.Delete();
                /*TKMD.ChungTuNoCollection.Remove(BaoLanhThue);*/
                TKMD.BaoLanhThue = null;
                BaoLanhThue = new BaoLanhThue();
                Set();
            }
            }
            catch (System.Exception ex)
            {
                ShowMessage(ex.Message,false);
                LocalLogger.Instance().WriteMessage(ex);
            }
           
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            { 
                bool isValidate = ValidateControl.ValidateNull(txtDonViBaoLanh, error, "Số chứng từ");
            isValidate &= ValidateControl.ValidateDate(ccNgayHetHieuLuc, error, "Ngày chứng từ");
            isValidate &= ValidateControl.ValidateDate(ccNgayHieuLuc, error, "Ngày hết hạn");
            isValidate &= ValidateControl.ValidateNull(cbLoaiBaoLanh, error, "Loại chứng từ");
            isValidate &= ValidateControl.ValidateNull(txtSoGiayBaoLanh, error, "Nơi cấp");
            //isValidate &= ValidateControl.ValidateNull(txtToChucCap, error, "Tổ chức cấp");
//             if (!string.IsNullOrEmpty(ccNgayKyBaoLanh.Text))
//                 isValidate = ValidateControl.ValidateDate(ccNgayKyBaoLanh, error, "Thời hạn nộp");
            if (!isValidate) return;
            Get();
            if (BaoLanhThue.ID == 0)
                TKMD.BaoLanhThue = BaoLanhThue;
            BaoLanhThue.TKMD_ID = TKMD.ID;
            BaoLanhThue.InsertUpdate();
            ShowMessage("Lưu thông tin thành công", false);
            }
            catch (System.Exception ex)
            {

                ShowMessage(ex.Message, false);
                LocalLogger.Instance().WriteMessage(ex);
            }
          
            /*DialogResult = DialogResult.OK;*/
        }

    
     
    }
}
