using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class BOA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute KND { get; set; }
public PropertiesAttribute CGO { get; set; }
public PropertiesAttribute DPR { get; set; }
public PropertiesAttribute BRT { get; set; }
public PropertiesAttribute PRT { get; set; }
public PropertiesAttribute ARR { get; set; }
public PropertiesAttribute ARB { get; set; }
public PropertiesAttribute ARP { get; set; }
public PropertiesAttribute DTM { get; set; }
public PropertiesAttribute TIM { get; set; }

public BOA()
        {
KND = new PropertiesAttribute(1, typeof(int));
CGO = new PropertiesAttribute(12, typeof(int));
DPR = new PropertiesAttribute(4, typeof(string));
BRT = new PropertiesAttribute(6, typeof(string));
PRT = new PropertiesAttribute(6, typeof(string));
ARR = new PropertiesAttribute(4, typeof(string));
ARB = new PropertiesAttribute(6, typeof(string));
ARP = new PropertiesAttribute(6, typeof(string));
DTM = new PropertiesAttribute(8, typeof(DateTime));
TIM = new PropertiesAttribute(4, typeof(int));
TongSoByte = 77;
}

}public partial class EnumGroupID
    {
}

}
