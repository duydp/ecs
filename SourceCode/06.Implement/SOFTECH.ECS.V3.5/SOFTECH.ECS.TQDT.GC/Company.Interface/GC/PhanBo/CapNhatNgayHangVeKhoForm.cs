﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.GC.PhanBo
{
    public partial class CapNhatNgayHangVeKhoForm : BaseForm
    {
        public CapNhatNgayHangVeKhoForm()
        {
            InitializeComponent();
        }
        public int ID_HopDong;
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbHopDong.Text.Length > 0)
                {
                    ID_HopDong = Convert.ToInt32(cbHopDong.Value.ToString());
                }
                dgList.DataSource = NPLNhapTonThucTe.SelectTKNhapBy_HD_ID(ID_HopDong).Tables[0];
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXRow[] listCheck = dgList.GetCheckedRows();
                foreach (GridEXRow item in listCheck)
                {
                    DataRowView dr = (DataRowView)item.DataRow;
                    DateTime NgayHangVeKho = (DateTime)dgList.GetRow(item.Position).Cells["NgayHangVeKho"].Value;
                    int SoTK = Convert.ToInt32(dgList.GetRow(item.Position).Cells["SoToKhai"].Value.ToString());
                    long HopDong_ID = Convert.ToInt64(dgList.GetRow(item.Position).Cells["ID_HopDong"].Value.ToString());
                    int MuaVN = Convert.ToInt32(dgList.GetRow(item.Position).Cells["MuaVN"].Value.ToString());
                    NPLNhapTonThucTe.Update_NgayHangVeKho(SoTK,NgayHangVeKho,HopDong_ID,MuaVN);
                }
                ShowMessage("Cập nhật thành công ", false);
                btnSearch_Click(null,null);
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BindHopDong()
        {
            try
            {
                HopDong hd = new HopDong();
                hd.SoHopDong = "";
                hd.ID = 0;
                List<HopDong> collection;
                {
                    string where = string.Format(" MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                    collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
                }
                collection.Add(hd);
                cbHopDong.DataSource = collection;
                cbHopDong.DisplayMember = "SoHopDong";
                cbHopDong.ValueMember = "ID";
                cbHopDong.SelectedIndex = collection.Count - 1;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void CapNhatNgayHangVeKhoForm_Load(object sender, EventArgs e)
        {
            BindHopDong();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                //int MuaVN = Convert.ToInt32(e.Row.Cells["MuaVN"].Value);
                //if (MuaVN == 1)
                //    e.Row.Cells["MuaVN"].Row.CheckState = RowCheckState.Checked;
                //e.Row.Cells["MuaVN"].Row.CheckState = RowCheckState.Unchecked;
            }
        }
    }
}
