using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.Common
{
	public partial class HSTKBoSung : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long HoSoDangKy_ID { set; get; }
		public DateTime NgayKhaiBao { set; get; }
		public int SoLuongChungTuKemTheo { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<HSTKBoSung> ConvertToCollection(IDataReader reader)
		{
			IList<HSTKBoSung> collection = new List<HSTKBoSung>();
			while (reader.Read())
			{
				HSTKBoSung entity = new HSTKBoSung();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoDangKy_ID"))) entity.HoSoDangKy_ID = reader.GetInt64(reader.GetOrdinal("HoSoDangKy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongChungTuKemTheo"))) entity.SoLuongChungTuKemTheo = reader.GetInt32(reader.GetOrdinal("SoLuongChungTuKemTheo"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<HSTKBoSung> collection, long id)
        {
            foreach (HSTKBoSung item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_SXXK_HSTKBoSung VALUES(@HoSoDangKy_ID, @NgayKhaiBao, @SoLuongChungTuKemTheo, @GhiChu)";
            string update = "UPDATE t_KDT_SXXK_HSTKBoSung SET HoSoDangKy_ID = @HoSoDangKy_ID, NgayKhaiBao = @NgayKhaiBao, SoLuongChungTuKemTheo = @SoLuongChungTuKemTheo, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_SXXK_HSTKBoSung WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoDangKy_ID", SqlDbType.BigInt, "HoSoDangKy_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongChungTuKemTheo", SqlDbType.Int, "SoLuongChungTuKemTheo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoDangKy_ID", SqlDbType.BigInt, "HoSoDangKy_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongChungTuKemTheo", SqlDbType.Int, "SoLuongChungTuKemTheo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_SXXK_HSTKBoSung VALUES(@HoSoDangKy_ID, @NgayKhaiBao, @SoLuongChungTuKemTheo, @GhiChu)";
            string update = "UPDATE t_KDT_SXXK_HSTKBoSung SET HoSoDangKy_ID = @HoSoDangKy_ID, NgayKhaiBao = @NgayKhaiBao, SoLuongChungTuKemTheo = @SoLuongChungTuKemTheo, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_SXXK_HSTKBoSung WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoDangKy_ID", SqlDbType.BigInt, "HoSoDangKy_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongChungTuKemTheo", SqlDbType.Int, "SoLuongChungTuKemTheo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoDangKy_ID", SqlDbType.BigInt, "HoSoDangKy_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongChungTuKemTheo", SqlDbType.Int, "SoLuongChungTuKemTheo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HSTKBoSung Load(long id)
		{
			const string spName = "[dbo].[p_KDT_SXXK_HSTKBoSung_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<HSTKBoSung> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<HSTKBoSung> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<HSTKBoSung> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<HSTKBoSung> SelectCollectionBy_HoSoDangKy_ID(long hoSoDangKy_ID)
		{
            IDataReader reader = SelectReaderBy_HoSoDangKy_ID(hoSoDangKy_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_HoSoDangKy_ID(long hoSoDangKy_ID)
		{
			const string spName = "[dbo].[p_KDT_SXXK_HSTKBoSung_SelectBy_HoSoDangKy_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HoSoDangKy_ID", SqlDbType.BigInt, hoSoDangKy_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_HSTKBoSung_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_HSTKBoSung_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_HSTKBoSung_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_HSTKBoSung_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_HoSoDangKy_ID(long hoSoDangKy_ID)
		{
			const string spName = "p_KDT_SXXK_HSTKBoSung_SelectBy_HoSoDangKy_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HoSoDangKy_ID", SqlDbType.BigInt, hoSoDangKy_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertHSTKBoSung(long hoSoDangKy_ID, DateTime ngayKhaiBao, int soLuongChungTuKemTheo, string ghiChu)
		{
			HSTKBoSung entity = new HSTKBoSung();	
			entity.HoSoDangKy_ID = hoSoDangKy_ID;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.SoLuongChungTuKemTheo = soLuongChungTuKemTheo;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_SXXK_HSTKBoSung_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@HoSoDangKy_ID", SqlDbType.BigInt, HoSoDangKy_ID);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@SoLuongChungTuKemTheo", SqlDbType.Int, SoLuongChungTuKemTheo);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HSTKBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HSTKBoSung item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHSTKBoSung(long id, long hoSoDangKy_ID, DateTime ngayKhaiBao, int soLuongChungTuKemTheo, string ghiChu)
		{
			HSTKBoSung entity = new HSTKBoSung();			
			entity.ID = id;
			entity.HoSoDangKy_ID = hoSoDangKy_ID;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.SoLuongChungTuKemTheo = soLuongChungTuKemTheo;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_SXXK_HSTKBoSung_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HoSoDangKy_ID", SqlDbType.BigInt, HoSoDangKy_ID);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@SoLuongChungTuKemTheo", SqlDbType.Int, SoLuongChungTuKemTheo);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HSTKBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HSTKBoSung item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHSTKBoSung(long id, long hoSoDangKy_ID, DateTime ngayKhaiBao, int soLuongChungTuKemTheo, string ghiChu)
		{
			HSTKBoSung entity = new HSTKBoSung();			
			entity.ID = id;
			entity.HoSoDangKy_ID = hoSoDangKy_ID;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.SoLuongChungTuKemTheo = soLuongChungTuKemTheo;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_HSTKBoSung_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HoSoDangKy_ID", SqlDbType.BigInt, HoSoDangKy_ID);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@SoLuongChungTuKemTheo", SqlDbType.Int, SoLuongChungTuKemTheo);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HSTKBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HSTKBoSung item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHSTKBoSung(long id)
		{
			HSTKBoSung entity = new HSTKBoSung();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_HSTKBoSung_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_HoSoDangKy_ID(long hoSoDangKy_ID)
		{
			const string spName = "[dbo].[p_KDT_SXXK_HSTKBoSung_DeleteBy_HoSoDangKy_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HoSoDangKy_ID", SqlDbType.BigInt, hoSoDangKy_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_SXXK_HSTKBoSung_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HSTKBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HSTKBoSung item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}