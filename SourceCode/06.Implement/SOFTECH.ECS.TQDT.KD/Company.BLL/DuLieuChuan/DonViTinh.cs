using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KD.BLL.DuLieuChuan
{
    public class DonViTinh : BaseClass
    {
        public static DataSet SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_DonViTinh ORDER BY Ten";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }

        public static string GetName(object id)
        {
            string query = string.Format("SELECT * FROM t_HaiQuan_DonViTinh WHERE ID = '{0}' ORDER BY Ten", id.ToString().PadRight(3));
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
               string s =  reader["Ten"].ToString();
               reader.Close();
               return s;
            }
            reader.Close();
            return "";
        }
        public static void Update(DataSet ds)
        {
            string insert = "Insert INTO t_HaiQuan_DonViTinh VALUES(@Id,@Ten)";
            string update = "UPDATE t_HaiQuan_DonViTinh SET Ten = @Ten WHERE Id=@Id";
            string delete = "DELETE FROM t_HaiQuan_DonViTinh WHERE ID = @Id";

            DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);

            DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);

            DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
        public static string GetID(object id)
        {
            string query = string.Format("SELECT * FROM t_HaiQuan_DonViTinh WHERE Ten = '{0}' ORDER BY Ten", id.ToString());
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                string s = reader["ID"].ToString();
                reader.Close();
                return s;
            }
            reader.Close();
            return "";
        }
    }
}