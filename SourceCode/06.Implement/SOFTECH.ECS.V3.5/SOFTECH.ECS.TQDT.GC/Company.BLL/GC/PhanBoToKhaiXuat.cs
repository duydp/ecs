﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;


namespace Company.GC.BLL.GC
{
    public partial class PhanBoToKhaiXuat
    {
        private List<PhanBoToKhaiNhap> _PhanBoToKhaiNhapCollection;
        public List<PhanBoToKhaiNhap> PhanBoToKhaiNhapCollection
        {
            set { _PhanBoToKhaiNhapCollection = value; }
            get { return _PhanBoToKhaiNhapCollection; }
        }
        public void LoadPhanBoToKhaiNhapCollection()
        {
            _PhanBoToKhaiNhapCollection = (List<PhanBoToKhaiNhap>)PhanBoToKhaiNhap.SelectCollectionBy_TKXuat_ID(this.ID);
        }

        public static void InsertUpdateFull(ToKhaiMauDich TKMD, DataTable tableTon)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead))
                {
                    try
                    {
                        TKMD.TrangThaiPhanBo = 1;//=1 da thuc hien phan bo
                        TKMD.UpdateTransaction(transaction);
                        foreach (PhanBoToKhaiXuat pbToKhaiXuat in TKMD.PhanBoToKhaiXuatCollection)
                        {
                            if (pbToKhaiXuat.ID == 0)
                                pbToKhaiXuat.Insert(transaction);
                            else
                                pbToKhaiXuat.Update(transaction);
                            foreach (PhanBoToKhaiNhap pbTKNhap in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                            {
                                //if (pbTKNhap.MaNPL == "830890900024")
                                //{

                                //}
                                pbTKNhap.TKXuat_ID = pbToKhaiXuat.ID;
                                if (pbTKNhap.ID == 0)
                                    pbTKNhap.Insert(transaction);
                                else
                                    pbTKNhap.Update(transaction);
                            }
                        }
                        foreach (DataRow row in tableTon.Rows)
                        {
                            NPLNhapTonThucTe npl = new NPLNhapTonThucTe();
                            npl.ID = Convert.ToInt64(row["id"]);
                            npl.Luong = Convert.ToDecimal(row["Luong"]);
                            npl.MaDoanhNghiep = (row["MaDoanhNghiep"].ToString());
                            npl.MaHaiQuan = (row["MaHaiQuan"].ToString());
                            npl.MaLoaiHinh = (row["MaLoaiHinh"].ToString());
                            npl.MaNPL = (row["MaNPL"].ToString());
                            npl.NamDangKy = Convert.ToInt16(row["NamDangKy"]);
                            npl.NgayDangKy = Convert.ToDateTime(row["NgayDangKy"]);
                            npl.NgayHangVeKho = Convert.ToDateTime(row["NgayHangVeKho"]);
                            npl.SoToKhai = Convert.ToInt32(row["SoToKhai"]);
                            npl.Ton = Convert.ToDecimal(row["Ton"]);
                            npl.ID_HopDong = Convert.ToInt64(row["ID_HopDong"]);
                            npl.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        public static void InsertUpdateFull(ToKhaiChuyenTiep TKCT, DataTable tableTon)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead))
                {
                    try
                    {
                        //TKCT.TrangThaiPhanBo = 1;//=1 da thuc hien phan bo
                        TKCT.Update(transaction);
                        foreach (PhanBoToKhaiXuat pbToKhaiXuat in TKCT.PhanBoToKhaiXuatCollection)
                        {
                            if (pbToKhaiXuat.ID == 0)
                                pbToKhaiXuat.Insert(transaction);
                            else
                                pbToKhaiXuat.Update(transaction);
                            foreach (PhanBoToKhaiNhap pbTKNhap in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                            {
                                pbTKNhap.TKXuat_ID = pbToKhaiXuat.ID;
                                if (pbTKNhap.ID == 0)
                                    pbTKNhap.Insert(transaction);
                                else
                                    pbTKNhap.Update(transaction);
                            }
                        }
                        foreach (DataRow row in tableTon.Rows)
                        {
                            NPLNhapTonThucTe npl = new NPLNhapTonThucTe();
                            npl.ID = Convert.ToInt64(row["id"]);
                            npl.Luong = Convert.ToDecimal(row["Luong"]);
                            npl.MaDoanhNghiep = (row["MaDoanhNghiep"].ToString());
                            npl.MaHaiQuan = (row["MaHaiQuan"].ToString());
                            npl.MaLoaiHinh = (row["MaLoaiHinh"].ToString());
                            npl.MaNPL = (row["MaNPL"].ToString());
                            npl.NamDangKy = Convert.ToInt16(row["NamDangKy"]);
                            npl.NgayDangKy = Convert.ToDateTime(row["NgayDangKy"]);
                            npl.NgayHangVeKho = Convert.ToDateTime(row["NgayHangVeKho"]);
                            npl.SoToKhai = Convert.ToInt32(row["SoToKhai"]);
                            npl.Ton = Convert.ToDecimal(row["Ton"]);
                            npl.ID_HopDong = Convert.ToInt64(row["ID_HopDong"]);
                            //npl.MuaVN = Convert.ToInt32(row["MuaVN"]);
                            npl.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        public static DataSet SelectViewPhanBoToKhaiXuatOfTKMDAndMaNPL(long idTKX, string MaNPL)
        {
            string spName = "select NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL, sum(LuongPhanBo) as LuongPhanBo from v_GC_PhanBo where ID_TKMD = " + idTKX + " AND (MaLoaiHinhXuat LIKE 'XGC%' OR MaLoaiHinhXuat LIKE 'XV%') AND MaNPL='" + MaNPL + "'" +
                 " GROUP BY NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            dbCommand.CommandTimeout = 10000;
            return db.ExecuteDataSet(dbCommand);
        }
        public static DataSet SelectViewPhanBoToKhaiXuatOfTKMDAndMaNPL(long idTKX, string MaNPL, string LoaiHinhXuat,long id_HopDong, SqlTransaction transaction)
        {
            #region sql old
            //string spName = "[dbo].[p_XemPhanBoToKhai]";
//            string SpName = string.Format(@"
//Declare @ID_TKX  BIGINT,
//	@MaNPL	VARCHAR(30),
//	@MaLoaiHinhXuat VARCHAR(5)
//	
//
//SET @ID_TKX={0}
//SET @MaNPL='{1}'
//SET @MaLoaiHinhXuat='{2}'
//BEGIN
//	-- SET NOCOUNT ON added to prevent extra result sets from
//	-- interfering with SELECT statements.
//	
//	IF(@MaLoaiHinhXuat LIKE '%18' OR @MaLoaiHinhXuat LIKE '%19' OR @MaLoaiHinhXuat LIKE '%20' OR @MaLoaiHinhXuat LIKE 'PH%' OR @MaLoaiHinhXuat LIKE 'XVE54')
//	BEGIN
//		select NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL, sum(LuongPhanBo) as LuongPhanBo
//		FROM
//		-- To Khai xuat GC Chuyen Tiep va To khai nhap GC Binh Thuong
//(SELECT     pbtkx_1_1.MaSP, pbtkx_1_1.SoLuongXuat, pbtkx_1_1.MaDoanhNghiep, pbtkn_2.SoToKhaiNhap, pbtkn_2.MaLoaiHinhNhap, pbtkn_2.MaHaiQuanNhap, 
//                      pbtkn_2.NamDangKyNhap, pbtkn_2.DinhMucChung, pbtkn_2.LuongTonDau, pbtkn_2.LuongPhanBo, pbtkn_2.LuongCungUng, pbtkn_2.LuongTonCuoi, 
//                      pbtkn_2.TKXuat_ID, pbtkx_1_1.ID_TKMD, pbtkn_2.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
//                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
//                      hangNhap.TenHang AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, hangNhap.DonGiaKB AS DonGiaTT, hangNhap.ThueSuatXNK AS ThueSuat, 
//                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.ID_DVT AS DVT_S, hangNhap.TyGiaTinhThue AS TyGiaTT, 
//                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
//FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
//                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_2
//                       WHERE    (t_GC_PhanBoToKhaiXuat_2.ID_TKMD = @ID_TKX) AND (t_GC_PhanBoToKhaiXuat_2.MaDoanhNghiep = @MaLoaiHinhXuat)) AS pbtkx_1_1 INNER JOIN
//                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
//                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
//                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_2
//                            WHERE      (MaLoaiHinhNhap LIKE 'N%') AND t_GC_PhanBoToKhaiNhap_2.MaNPL = @MaNPL) AS pbtkn_2 ON pbtkx_1_1.ID = pbtkn_2.TKXuat_ID INNER JOIN
//                      dbo.v_GC_HangToKhai AS hangNhap ON pbtkn_2.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn_2.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
//                      pbtkn_2.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn_2.MaNPL = hangNhap.MaPhu INNER JOIN
//                      dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_1_1.ID_TKMD = hangXuat.ID AND pbtkx_1_1.MaSP = hangXuat.MaHang
//UNION
//-- Tờ khai Xuất GC Chuyển tiếp được cung ứng từ tờ khai nhập GC chuyển tiếp 
//SELECT     pbtkx_2.MaSP, pbtkx_2.SoLuongXuat, pbtkx_2.MaDoanhNghiep, pbtkn_1_1.SoToKhaiNhap, pbtkn_1_1.MaLoaiHinhNhap, pbtkn_1_1.MaHaiQuanNhap, 
//                      pbtkn_1_1.NamDangKyNhap, pbtkn_1_1.DinhMucChung, pbtkn_1_1.LuongTonDau, pbtkn_1_1.LuongPhanBo, pbtkn_1_1.LuongCungUng, 
//                      pbtkn_1_1.LuongTonCuoi, pbtkn_1_1.TKXuat_ID, pbtkx_2.ID_TKMD, pbtkn_1_1.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, 
//                      hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat, 
//                      hangXuat.SoToKhai AS SoToKhaiXuat, hangNhap.TenHang AS TenNPL, hangNhap.ID_DVT AS DVT_NPL, hangNhap.DonGia AS DonGiaTT, 
//                      0 AS ThueSuat, hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.ID_DVT AS DVT_SP, 
//                      hangNhap.TyGiaVND AS TyGiaTT, hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
//FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
//                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
//                       WHERE      (t_GC_PhanBoToKhaiXuat_1.ID_TKMD = @ID_TKX) AND (t_GC_PhanBoToKhaiXuat_1.MaDoanhNghiep = @MaLoaiHinhXuat)) AS pbtkx_2 INNER JOIN
//                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
//                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
//                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
//                            WHERE      (MaLoaiHinhNhap LIKE 'PH%' OR MaLoaiHinhNhap LIKE 'NVE23' OR MaLoaiHinhNhap LIKE '%19' OR MaLoaiHinhNhap LIKE '%18' OR MaLoaiHinhNhap LIKE '%20')AND t_GC_PhanBoToKhaiNhap_1.MaNPL = @MaNPL ) AS pbtkn_1_1 ON pbtkx_2.ID = pbtkn_1_1.TKXuat_ID INNER JOIN
//                      dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1_1.SoToKhaiNhap = hangNhap.SoToKhai AND 
//                      pbtkn_1_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND pbtkn_1_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND 
//                      pbtkn_1_1.MaNPL = hangNhap.MaHang INNER JOIN
//                      dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_2.ID_TKMD = hangXuat.ID AND pbtkx_2.MaSP = hangXuat.MaHang) AS A
//		GROUP BY NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL                   
//
//	END
//	ELSE
//	BEGIN
//		select NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL, sum(LuongPhanBo) as LuongPhanBo
//		FROM
//		(SELECT     pbtkx_1.MaSP, pbtkx_1.SoLuongXuat, pbtkx_1.MaDoanhNghiep, pbtkn.SoToKhaiNhap, pbtkn.MaLoaiHinhNhap, pbtkn.MaHaiQuanNhap, 
//                      pbtkn.NamDangKyNhap, pbtkn.DinhMucChung, pbtkn.LuongTonDau, pbtkn.LuongPhanBo, pbtkn.LuongCungUng, pbtkn.LuongTonCuoi, pbtkn.TKXuat_ID, 
//                      pbtkx_1.ID_TKMD, pbtkn.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
//                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
//                      hangNhap.TenHang AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, hangNhap.DonGiaKB AS DonGiaTT, hangNhap.ThueSuatXNK AS ThueSuat, 
//                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, hangNhap.TyGiaTinhThue AS TyGiaTT, 
//                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
//FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
//                       FROM          dbo.t_GC_PhanBoToKhaiXuat
//                       WHERE      ((MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%' )AND dbo.t_GC_PhanBoToKhaiXuat.ID_TKMD = @ID_TKX)) AS pbtkx_1 INNER JOIN
//                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
//                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
//                            FROM          dbo.t_GC_PhanBoToKhaiNhap
//                            WHERE      (MaLoaiHinhNhap LIKE 'N%' AND dbo.t_GC_PhanBoToKhaiNhap.MaNPL = @MaNPL)) AS pbtkn ON pbtkx_1.ID = pbtkn.TKXuat_ID INNER JOIN
//                      dbo.v_GC_HangToKhai AS hangNhap ON pbtkn.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
//                      pbtkn.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn.MaNPL = hangNhap.MaPhu INNER JOIN
//                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_1.ID_TKMD = hangXuat.TKMD_ID AND pbtkx_1.MaSP = hangXuat.MaPhu
//UNION
//SELECT     pbtkx.MaSP, pbtkx.SoLuongXuat, pbtkx.MaDoanhNghiep, pbtkn_1.SoToKhaiNhap, pbtkn_1.MaLoaiHinhNhap, pbtkn_1.MaHaiQuanNhap, 
//                      pbtkn_1.NamDangKyNhap, pbtkn_1.DinhMucChung, pbtkn_1.LuongTonDau, pbtkn_1.LuongPhanBo, pbtkn_1.LuongCungUng, pbtkn_1.LuongTonCuoi, 
//                      pbtkn_1.TKXuat_ID, pbtkx.ID_TKMD, pbtkn_1.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
//                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
//                      hangNhap.TenHang AS TenNPL, hangNhap.ID_DVT AS DVT_NPL, hangNhap.DonGia AS DonGiaTT, 0 AS ThueSuat, 
//                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, hangNhap.TyGiaVND AS TyGiaTT, 
//                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
//FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
//                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
//                       WHERE      ((MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%' ) AND ID_TKMD = @ID_TKX))  AS pbtkx INNER JOIN
//                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
//                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
//                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
//                            WHERE      ((MaLoaiHinhNhap LIKE 'PH%' OR  MaLoaiHinhNhap LIKE 'NVE23' OR MaLoaiHinhNhap LIKE '%19' oR MaLoaiHinhNhap LIKE '%18' OR MaLoaiHinhNhap LIKE '%20') AND (MaNPL = @MaNPL))) 
//                            AS pbtkn_1 ON pbtkx.ID = pbtkn_1.TKXuat_ID INNER JOIN
//                      dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1.SoToKhaiNhap = hangNhap.SoToKhai AND 
//                      pbtkn_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND pbtkn_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND 
//                      pbtkn_1.MaNPL = hangNhap.MaHang INNER JOIN
//                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx.ID_TKMD = hangXuat.TKMD_ID AND pbtkx.MaSP = hangXuat.MaPhu
//UNION
//SELECT     pbtkx_3.MaSP, pbtkx_3.SoLuongXuat, pbtkx_3.MaDoanhNghiep, pbtkn_1_2.SoToKhaiNhap, pbtkn_1_2.MaLoaiHinhNhap, pbtkn_1_2.MaHaiQuanNhap, 
//                      pbtkn_1_2.NamDangKyNhap, pbtkn_1_2.DinhMucChung, pbtkn_1_2.LuongTonDau, pbtkn_1_2.LuongPhanBo, pbtkn_1_2.LuongCungUng, 
//                      pbtkn_1_2.LuongTonCuoi, pbtkn_1_2.TKXuat_ID, pbtkx_3.ID_TKMD, pbtkn_1_2.MaNPL, '01/01/1900' AS NgayDangKyNhap, 
//                      hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, 
//                      hangXuat.SoToKhai AS SoToKhaiXuat, hangNhap.Ten AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, 0 AS DonGiaTT, 0 AS ThueSuat, 
//                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, 0 AS TyGiaTT, 
//                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
//FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
//                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
//                       WHERE      (MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%' )) AS pbtkx_3 INNER JOIN
//                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
//                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
//                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
//                            WHERE      (MaLoaiHinhNhap LIKE '')) AS pbtkn_1_2 ON pbtkx_3.ID = pbtkn_1_2.TKXuat_ID INNER JOIN
//                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_3.ID_TKMD = hangXuat.TKMD_ID AND pbtkx_3.MaSP = hangXuat.MaPhu INNER JOIN
//                      dbo.t_GC_NguyenPhuLieu AS hangNhap ON pbtkn_1_2.MaNPL = hangNhap.Ma AND hangNhap.HopDong_ID = hangXuat.IDHopDong) AS B
//        GROUP BY NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL
//		END
//	
//END
//
            //", idTKX, MaNPL, LoaiHinhXuat);
            #endregion sql old
            #region sql
            string SpName = string.Format(@"
Declare @ID_TKX  BIGINT,
	@MaNPL	VARCHAR(30),
	@MaLoaiHinhXuat VARCHAR(5),
    @ID_HopDong  BIGINT 
	

SET @ID_TKX={0}
SET @MaNPL='{1}'
SET @MaLoaiHinhXuat='{2}'
SET @ID_HopDong = '{3}'
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
    IF ( @MaLoaiHinhXuat LIKE '%18'
         OR @MaLoaiHinhXuat LIKE '%19'
         OR @MaLoaiHinhXuat LIKE '%20'
         OR @MaLoaiHinhXuat LIKE 'PH%'
         OR @MaLoaiHinhXuat LIKE 'XVE54'
       ) 
        BEGIN
            SELECT  NgayDangKyNhap ,
                    SoToKhaiNhap ,
                    NamDangKyNhap ,
                    MaLoaiHinhNhap ,
                    MaNPL ,
                    SUM(LuongPhanBo) AS LuongPhanBo
            FROM    -- To Khai xuat GC Chuyen Tiep va To khai nhap GC Binh Thuong
                    ( SELECT    pbtkx_1_1.MaSP ,
                                pbtkx_1_1.SoLuongXuat ,
                                pbtkx_1_1.MaDoanhNghiep ,
                                pbtkn_2.SoToKhaiNhap ,
                                pbtkn_2.MaLoaiHinhNhap ,
                                pbtkn_2.MaHaiQuanNhap ,
                                pbtkn_2.NamDangKyNhap ,
                                pbtkn_2.DinhMucChung ,
                                pbtkn_2.LuongTonDau ,
                                pbtkn_2.LuongPhanBo ,
                                pbtkn_2.LuongCungUng ,
                                pbtkn_2.LuongTonCuoi ,
                                pbtkn_2.TKXuat_ID ,
                                pbtkx_1_1.ID_TKMD ,
                                pbtkn_2.MaNPL ,
                                hangNhap.NgayDangKy AS NgayDangKyNhap ,
                                hangXuat.MaLoaiHinh AS MaLoaiHinhXuat ,
                                YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat ,
                                hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat ,
                                hangXuat.SoToKhai AS SoToKhaiXuat ,
                                hangNhap.TenHang AS TenNPL ,
                                hangNhap.DVT_ID AS DVT_NPL ,
                                hangNhap.DonGiaKB AS DonGiaTT ,
                                hangNhap.ThueSuatXNK AS ThueSuat ,
                                hangXuat.NgayDangKy AS NgayDangKyXuat ,
                                hangXuat.TenHang AS TenSP ,
                                hangXuat.ID_DVT AS DVT_S ,
                                hangNhap.TyGiaTinhThue AS TyGiaTT ,
                                hangXuat.NgayDangKy AS NgayThucXuat ,
                                hangXuat.IDHopDong
                      FROM      ( SELECT    ID ,
                                            ID_TKMD ,
                                            MaSP ,
                                            SoLuongXuat ,
                                            MaDoanhNghiep
                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_2
                                  WHERE     ( t_GC_PhanBoToKhaiXuat_2.ID_TKMD =@ID_TKX )
                                            AND ( t_GC_PhanBoToKhaiXuat_2.MaDoanhNghiep = @MaLoaiHinhXuat )
                                ) AS pbtkx_1_1
                                INNER JOIN ( SELECT ID ,
                                                    SoToKhaiNhap ,
                                                    MaLoaiHinhNhap ,
                                                    MaHaiQuanNhap ,
                                                    NamDangKyNhap ,
                                                    MaNPL ,
                                                    DinhMucChung ,
                                                    LuongTonDau ,
                                                    LuongPhanBo ,
                                                    LuongCungUng ,
                                                    LuongTonCuoi ,
                                                    TKXuat_ID ,
                                                    MaDoanhNghiep
                                             FROM   dbo.t_GC_PhanBoToKhaiNhap
                                                    AS t_GC_PhanBoToKhaiNhap_2
                                             WHERE  ( MaLoaiHinhNhap LIKE 'N%' )
                                                    AND t_GC_PhanBoToKhaiNhap_2.MaNPL = @MaNPL  AND TKXuat_ID in (SELECT    ID 
                                            
                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_2
                                  WHERE     ( t_GC_PhanBoToKhaiXuat_2.ID_TKMD =@ID_TKX )
                                            AND ( t_GC_PhanBoToKhaiXuat_2.MaDoanhNghiep = @MaLoaiHinhXuat ))
                                           ) AS pbtkn_2 ON pbtkx_1_1.ID = pbtkn_2.TKXuat_ID
                                INNER JOIN (
SELECT     t.*, hmd.TKMD_ID, hmd.SoThuTuHang, hmd.MaHS, 
                      hmd.MaPhu, hmd.NuocXX_ID, hmd.TenHang, hmd.DVT_ID, 
                      hmd.SoLuong, hmd.TrongLuong AS Expr1, hmd.DonGiaKB, 
                      hmd.DonGiaTT, hmd.TriGiaKB, hmd.TriGiaTT, hmd.TriGiaKB_VND, 
                      hmd.ThueSuatXNK, hmd.ThueSuatTTDB, hmd.ThueSuatGTGT, 
                      hmd.ThueXNK, hmd.ThueTTDB, hmd.ThueGTGT, hmd.PhuThu, 
                      hmd.TyLeThuKhac, hmd.TriGiaThuKhac, hmd.MienThue, 
                      hmd.Ma_HTS, hmd.DVT_HTS, hmd.SoLuong_HTS
FROM        dbo.t_KDT_ToKhaiMauDich as t INNER JOIN
                      dbo.t_KDT_HangMauDich as hmd ON t.ID = hmd.TKMD_ID  where t.SoToKhai in (SELECT 
                                                    SoToKhaiNhap
                                                    
                                             FROM   dbo.t_GC_PhanBoToKhaiNhap
                                                    AS t_GC_PhanBoToKhaiNhap_2
                                             WHERE  ( MaLoaiHinhNhap LIKE 'N%' )
                                                    AND t_GC_PhanBoToKhaiNhap_2.MaNPL = @MaNPL)
) AS hangNhap ON pbtkn_2.SoToKhaiNhap = hangNhap.SoToKhai
                                                              AND pbtkn_2.MaLoaiHinhNhap = hangNhap.MaLoaiHinh
                                                              AND pbtkn_2.NamDangKyNhap = YEAR(hangNhap.NgayDangKy)
                                                              AND pbtkn_2.MaNPL = hangNhap.MaPhu
                                INNER JOIN (
SELECT     a.ID, a.IDHopDong, a.SoTiepNhan, a.NgayTiepNhan, a.TrangThaiXuLy, a.MaHaiQuanTiepNhan, a.SoToKhai, a.CanBoDangKy, a.NgayDangKy, 
                      a.MaDoanhNghiep, a.SoHopDongDV, a.NgayHDDV, a.NgayHetHanHDDV, a.NguoiChiDinhDV, a.MaKhachHang, a.TenKH, a.SoHDKH, a.NgayHDKH, 
                      a.NgayHetHanHDKH, a.NguoiChiDinhKH, a.MaDaiLy, a.MaLoaiHinh, a.DiaDiemXepHang, a.TyGiaVND, a.TyGiaUSD, a.LePhiHQ, a.SoBienLai, 
                      a.NgayBienLai, a.ChungTu, a.NguyenTe_ID, a.MaHaiQuanKH, a.ID_Relation, b.Master_ID, b.SoThuTuHang, b.MaHang, b.MaHS, b.SoLuong, 
                      b.TenHang, b.ID_NuocXX, b.ID_DVT, b.DonGia, b.TriGia
FROM         dbo.t_KDT_GC_ToKhaiChuyenTiep AS a INNER JOIN
                      dbo.t_KDT_GC_HangChuyenTiep AS b ON a.ID = b.Master_ID and a.ID in ( SELECT   
                                            ID_TKMD 
                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_2
                                  WHERE     ( t_GC_PhanBoToKhaiXuat_2.ID_TKMD =@ID_TKX )
                                            AND ( t_GC_PhanBoToKhaiXuat_2.MaDoanhNghiep = @MaLoaiHinhXuat ))

) AS hangXuat ON pbtkx_1_1.ID_TKMD = hangXuat.ID
                                                              AND pbtkx_1_1.MaSP = hangXuat.MaHang
                      UNION
-- Tờ khai Xuất GC Chuyển tiếp được cung ứng từ tờ khai nhập GC chuyển tiếp 
                      SELECT    pbtkx_2.MaSP ,
                                pbtkx_2.SoLuongXuat ,
                                pbtkx_2.MaDoanhNghiep ,
                                pbtkn_1_1.SoToKhaiNhap ,
                                pbtkn_1_1.MaLoaiHinhNhap ,
                                pbtkn_1_1.MaHaiQuanNhap ,
                                pbtkn_1_1.NamDangKyNhap ,
                                pbtkn_1_1.DinhMucChung ,
                                pbtkn_1_1.LuongTonDau ,
                                pbtkn_1_1.LuongPhanBo ,
                                pbtkn_1_1.LuongCungUng ,
                                pbtkn_1_1.LuongTonCuoi ,
                                pbtkn_1_1.TKXuat_ID ,
                                pbtkx_2.ID_TKMD ,
                                pbtkn_1_1.MaNPL ,
                                hangNhap.NgayDangKy AS NgayDangKyNhap ,
                                hangXuat.MaLoaiHinh AS MaLoaiHinhXuat ,
                                YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat ,
                                hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat ,
                                hangXuat.SoToKhai AS SoToKhaiXuat ,
                                hangNhap.TenHang AS TenNPL ,
                                hangNhap.ID_DVT AS DVT_NPL ,
                                hangNhap.DonGia AS DonGiaTT ,
                                0 AS ThueSuat ,
                                hangXuat.NgayDangKy AS NgayDangKyXuat ,
                                hangXuat.TenHang AS TenSP ,
                                hangXuat.ID_DVT AS DVT_SP ,
                                hangNhap.TyGiaVND AS TyGiaTT ,
                                hangXuat.NgayDangKy AS NgayThucXuat ,
                                hangXuat.IDHopDong
                      FROM      ( SELECT    ID ,
                                            ID_TKMD ,
                                            MaSP ,
                                            SoLuongXuat ,
                                            MaDoanhNghiep
                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                                  WHERE     ( t_GC_PhanBoToKhaiXuat_1.ID_TKMD =@ID_TKX )
                                            AND ( t_GC_PhanBoToKhaiXuat_1.MaDoanhNghiep = @MaLoaiHinhXuat )
                                ) AS pbtkx_2
                                INNER JOIN ( SELECT ID ,
                                                    SoToKhaiNhap ,
                                                    MaLoaiHinhNhap ,
                                                    MaHaiQuanNhap ,
                                                    NamDangKyNhap ,
                                                    MaNPL ,
                                                    DinhMucChung ,
                                                    LuongTonDau ,
                                                    LuongPhanBo ,
                                                    LuongCungUng ,
                                                    LuongTonCuoi ,
                                                    TKXuat_ID ,
                                                    MaDoanhNghiep
                                             FROM   dbo.t_GC_PhanBoToKhaiNhap
                                                    AS t_GC_PhanBoToKhaiNhap_1
                                             WHERE  ( MaLoaiHinhNhap LIKE 'PH%'
                                                      OR MaLoaiHinhNhap LIKE 'NVE23'
                                                      OR MaLoaiHinhNhap LIKE '%19'
                                                      OR MaLoaiHinhNhap LIKE '%18'
                                                      OR MaLoaiHinhNhap LIKE '%20'
                                                    )
                                                    AND t_GC_PhanBoToKhaiNhap_1.MaNPL = @MaNPL
                                           ) AS pbtkn_1_1 ON pbtkx_2.ID = pbtkn_1_1.TKXuat_ID
                                INNER JOIN dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1_1.SoToKhaiNhap = hangNhap.SoToKhai
                                                              AND pbtkn_1_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh
                                                              AND pbtkn_1_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy)
                                                              AND pbtkn_1_1.MaNPL = hangNhap.MaHang
                                INNER JOIN dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_2.ID_TKMD = hangXuat.ID
                                                              AND pbtkx_2.MaSP = hangXuat.MaHang
                    ) AS A
            GROUP BY NgayDangKyNhap ,
                    SoToKhaiNhap ,
                    NamDangKyNhap ,
                    MaLoaiHinhNhap ,
                    MaNPL                   

        END
    ELSE 
        BEGIN
		--minhnd
            SELECT  NgayDangKyNhap ,
                    SoToKhaiNhap ,
                    NamDangKyNhap ,
                    MaLoaiHinhNhap ,
                    MaNPL ,
                    SUM(LuongPhanBo) AS LuongPhanBo
            FROM    ( SELECT    pbtkx_1.MaSP ,
                                pbtkx_1.SoLuongXuat ,
                                pbtkx_1.MaDoanhNghiep ,
                                pbtkn.SoToKhaiNhap ,
                                pbtkn.MaLoaiHinhNhap ,
                                pbtkn.MaHaiQuanNhap ,
                                pbtkn.NamDangKyNhap ,
                                pbtkn.DinhMucChung ,
                                pbtkn.LuongTonDau ,
                                pbtkn.LuongPhanBo ,
                                pbtkn.LuongCungUng ,
                                pbtkn.LuongTonCuoi ,
                                pbtkn.TKXuat_ID ,
                                pbtkx_1.ID_TKMD ,
                                pbtkn.MaNPL ,
                                hangNhap.NgayDangKy AS NgayDangKyNhap ,
                                hangXuat.MaLoaiHinh AS MaLoaiHinhXuat ,
                                YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat ,
                                hangXuat.MaHaiQuan AS MaHaiQuanXuat ,
                                hangXuat.SoToKhai AS SoToKhaiXuat ,
                                hangNhap.TenHang AS TenNPL ,
                                hangNhap.DVT_ID AS DVT_NPL ,
                                hangNhap.DonGiaKB AS DonGiaTT ,
                                hangNhap.ThueSuatXNK AS ThueSuat ,
                                hangXuat.NgayDangKy AS NgayDangKyXuat ,
                                hangXuat.TenHang AS TenSP ,
                                hangXuat.DVT_ID AS DVT_SP ,
                                hangNhap.TyGiaTinhThue AS TyGiaTT ,
                                hangXuat.NgayDangKy AS NgayThucXuat ,
                                hangXuat.IDHopDong
                      FROM      ( SELECT    ID ,
                                            ID_TKMD ,
                                            MaSP ,
                                            SoLuongXuat ,
                                            MaDoanhNghiep
                                  FROM      dbo.t_GC_PhanBoToKhaiXuat
                                  WHERE     ( ( MaDoanhNghiep LIKE 'XGC%'
                                                OR MaDoanhNghiep LIKE 'XV%'
                                              )
                                              AND dbo.t_GC_PhanBoToKhaiXuat.ID_TKMD =@ID_TKX
                                            ) AND MaDoanhNghiep = @MaLoaiHinhXuat
                                ) AS pbtkx_1
                                INNER JOIN ( SELECT ID ,
                                                    SoToKhaiNhap ,
                                                    MaLoaiHinhNhap ,
                                                    MaHaiQuanNhap ,
                                                    NamDangKyNhap ,
                                                    MaNPL ,
                                                    DinhMucChung ,
                                                    LuongTonDau ,
                                                    LuongPhanBo ,
                                                    LuongCungUng ,
                                                    LuongTonCuoi ,
                                                    TKXuat_ID ,
                                                    MaDoanhNghiep
                                             FROM   dbo.t_GC_PhanBoToKhaiNhap
                                             WHERE  ( MaLoaiHinhNhap LIKE 'N%'
                                                      AND dbo.t_GC_PhanBoToKhaiNhap.MaNPL = @MaNPL
                                                    ) AND TKXuat_ID in(SELECT    ID
                                  FROM      dbo.t_GC_PhanBoToKhaiXuat
                                  WHERE     ( ( MaDoanhNghiep LIKE 'XGC%'
                                                OR MaDoanhNghiep LIKE 'XV%'
                                              )
                                              AND dbo.t_GC_PhanBoToKhaiXuat.ID_TKMD =@ID_TKX
                                            ) AND MaDoanhNghiep = @MaLoaiHinhXuat)

                                           ) AS pbtkn ON pbtkx_1.ID = pbtkn.TKXuat_ID
                                INNER JOIN (
SELECT     t.*, hmd.TKMD_ID, hmd.SoThuTuHang, hmd.MaHS, 
                      hmd.MaPhu, hmd.NuocXX_ID, hmd.TenHang, hmd.DVT_ID, 
                      hmd.SoLuong, hmd.TrongLuong AS Expr1, hmd.DonGiaKB, 
                      hmd.DonGiaTT, hmd.TriGiaKB, hmd.TriGiaTT, hmd.TriGiaKB_VND, 
                      hmd.ThueSuatXNK, hmd.ThueSuatTTDB, hmd.ThueSuatGTGT, 
                      hmd.ThueXNK, hmd.ThueTTDB, hmd.ThueGTGT, hmd.PhuThu, 
                      hmd.TyLeThuKhac, hmd.TriGiaThuKhac, hmd.MienThue, 
                      hmd.Ma_HTS, hmd.DVT_HTS, hmd.SoLuong_HTS
FROM         dbo.t_KDT_ToKhaiMauDich AS t INNER JOIN
                      dbo.t_KDT_HangMauDich AS hmd ON t.ID = hmd.TKMD_ID where t.IDHopDong = @ID_HopDong-- and SoToKhai = 4403   
) AS hangNhap ON pbtkn.SoToKhaiNhap = hangNhap.SoToKhai
                                                              AND pbtkn.MaLoaiHinhNhap = hangNhap.MaLoaiHinh
                                                              AND pbtkn.NamDangKyNhap = YEAR(hangNhap.NgayDangKy)
                                                              AND pbtkn.MaNPL = hangNhap.MaPhu
                                INNER JOIN (
SELECT     t.*, hmd.TKMD_ID, hmd.SoThuTuHang, hmd.MaHS, 
                      hmd.MaPhu, hmd.NuocXX_ID, hmd.TenHang, hmd.DVT_ID, 
                      hmd.SoLuong, hmd.TrongLuong AS Expr1, hmd.DonGiaKB, 
                      hmd.DonGiaTT, hmd.TriGiaKB, hmd.TriGiaTT, hmd.TriGiaKB_VND, 
                      hmd.ThueSuatXNK, hmd.ThueSuatTTDB, hmd.ThueSuatGTGT, 
                      hmd.ThueXNK, hmd.ThueTTDB, hmd.ThueGTGT, hmd.PhuThu, 
                      hmd.TyLeThuKhac, hmd.TriGiaThuKhac, hmd.MienThue, 
                      hmd.Ma_HTS, hmd.DVT_HTS, hmd.SoLuong_HTS
FROM         dbo.t_KDT_ToKhaiMauDich AS t INNER JOIN
                      dbo.t_KDT_HangMauDich AS hmd ON t.ID = hmd.TKMD_ID WHERE t.IDHopDong = @ID_HopDong
) AS hangXuat ON pbtkx_1.ID_TKMD = hangXuat.TKMD_ID AND pbtkx_1.MaSP = hangXuat.MaPhu 
                      UNION
                      SELECT    pbtkx.MaSP ,
                                pbtkx.SoLuongXuat ,
                                pbtkx.MaDoanhNghiep ,
                                pbtkn_1.SoToKhaiNhap ,
                                pbtkn_1.MaLoaiHinhNhap ,
                                pbtkn_1.MaHaiQuanNhap ,
                                pbtkn_1.NamDangKyNhap ,
                                pbtkn_1.DinhMucChung ,
                                pbtkn_1.LuongTonDau ,
                                pbtkn_1.LuongPhanBo ,
                                pbtkn_1.LuongCungUng ,
                                pbtkn_1.LuongTonCuoi ,
                                pbtkn_1.TKXuat_ID ,
                                pbtkx.ID_TKMD ,
                                pbtkn_1.MaNPL ,
                                hangNhap.NgayDangKy AS NgayDangKyNhap ,
                                hangXuat.MaLoaiHinh AS MaLoaiHinhXuat ,
                                YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat ,
                                hangXuat.MaHaiQuan AS MaHaiQuanXuat ,
                                hangXuat.SoToKhai AS SoToKhaiXuat ,
                                hangNhap.TenHang AS TenNPL ,
                                hangNhap.ID_DVT AS DVT_NPL ,
                                hangNhap.DonGia AS DonGiaTT ,
                                0 AS ThueSuat ,
                                hangXuat.NgayDangKy AS NgayDangKyXuat ,
                                hangXuat.TenHang AS TenSP ,
                                hangXuat.DVT_ID AS DVT_SP ,
                                hangNhap.TyGiaVND AS TyGiaTT ,
                                hangXuat.NgayDangKy AS NgayThucXuat ,
                                hangXuat.IDHopDong
                      FROM      ( SELECT    ID ,
                                            ID_TKMD ,
                                            MaSP ,
                                            SoLuongXuat ,
                                            MaDoanhNghiep
                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                                  WHERE     ( ( MaDoanhNghiep LIKE 'XGC%'
                                                OR MaDoanhNghiep LIKE 'XV%'
                                              )
                                              AND ID_TKMD =@ID_TKX
                                            )
                                ) AS pbtkx
                                INNER JOIN ( SELECT ID ,
                                                    SoToKhaiNhap ,
                                                    MaLoaiHinhNhap ,
                                                    MaHaiQuanNhap ,
                                                    NamDangKyNhap ,
                                                    MaNPL ,
                                                    DinhMucChung ,
                                                    LuongTonDau ,
                                                    LuongPhanBo ,
                                                    LuongCungUng ,
                                                    LuongTonCuoi ,
                                                    TKXuat_ID ,
                                                    MaDoanhNghiep
                                             FROM   dbo.t_GC_PhanBoToKhaiNhap
                                                    AS t_GC_PhanBoToKhaiNhap_1
                                             WHERE  ( ( MaLoaiHinhNhap LIKE 'PH%'
                                                        OR MaLoaiHinhNhap LIKE 'NVE23'
                                                        OR MaLoaiHinhNhap LIKE '%19'
                                                        OR MaLoaiHinhNhap LIKE '%18'
                                                        OR MaLoaiHinhNhap LIKE '%20'
                                                      )
                                                      AND ( MaNPL = @MaNPL )
                                                    ) AND TKXuat_ID in (SELECT
                                            ID_TKMD 
                                            
                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                                  WHERE     ( ( MaDoanhNghiep LIKE 'XGC%'
                                                OR MaDoanhNghiep LIKE 'XV%'
                                              )
                                              AND ID_TKMD =@ID_TKX
                                            ))
                                           ) AS pbtkn_1 ON pbtkx.ID = pbtkn_1.TKXuat_ID	  
                                INNER JOIN (SELECT     a.ID, a.IDHopDong, a.SoTiepNhan, a.NgayTiepNhan, a.TrangThaiXuLy, a.MaHaiQuanTiepNhan, a.SoToKhai, a.CanBoDangKy, a.NgayDangKy, 
                      a.MaDoanhNghiep, a.SoHopDongDV, a.NgayHDDV, a.NgayHetHanHDDV, a.NguoiChiDinhDV, a.MaKhachHang, a.TenKH, a.SoHDKH, a.NgayHDKH, 
                      a.NgayHetHanHDKH, a.NguoiChiDinhKH, a.MaDaiLy, a.MaLoaiHinh, a.DiaDiemXepHang, a.TyGiaVND, a.TyGiaUSD, a.LePhiHQ, a.SoBienLai, 
                      a.NgayBienLai, a.ChungTu, a.NguyenTe_ID, a.MaHaiQuanKH, a.ID_Relation, b.Master_ID, b.SoThuTuHang, b.MaHang, b.MaHS, b.SoLuong, 
                      b.TenHang, b.ID_NuocXX, b.ID_DVT, b.DonGia, b.TriGia
FROM         dbo.t_KDT_GC_ToKhaiChuyenTiep AS a INNER JOIN
                      dbo.t_KDT_GC_HangChuyenTiep AS b ON a.ID = b.Master_ID   where a.IDHopDong = @ID_HopDong and a.SoToKhai in (SELECT 
                                                    SoToKhaiNhap 
                                                    
                                             FROM   dbo.t_GC_PhanBoToKhaiNhap
                                                    AS t_GC_PhanBoToKhaiNhap_1
                                             WHERE  ( ( MaLoaiHinhNhap LIKE 'PH%'
                                                        OR MaLoaiHinhNhap LIKE 'NVE23'
                                                        OR MaLoaiHinhNhap LIKE '%19'
                                                        OR MaLoaiHinhNhap LIKE '%18'
                                                        OR MaLoaiHinhNhap LIKE '%20'
                                                      )
                                                      AND ( MaNPL = @MaNPL )
                                                    ) AND TKXuat_ID in (SELECT
                                            ID_TKMD 
                                            
                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                                  WHERE     ( ( MaDoanhNghiep LIKE 'XGC%'
                                                OR MaDoanhNghiep LIKE 'XV%'
                                              )
                                              AND ID_TKMD =@ID_TKX
                                            )))) AS hangNhap ON pbtkn_1.SoToKhaiNhap = hangNhap.SoToKhai
                                                              AND pbtkn_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh
                                                              AND pbtkn_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy)
                                                              AND pbtkn_1.MaNPL = hangNhap.MaHang
                                INNER JOIN (
SELECT     t.*, hmd.TKMD_ID, hmd.SoThuTuHang, hmd.MaHS, 
                      hmd.MaPhu, hmd.NuocXX_ID, hmd.TenHang, hmd.DVT_ID, 
                      hmd.SoLuong, hmd.TrongLuong AS Expr1, hmd.DonGiaKB, 
                      hmd.DonGiaTT, hmd.TriGiaKB, hmd.TriGiaTT, hmd.TriGiaKB_VND, 
                      hmd.ThueSuatXNK, hmd.ThueSuatTTDB, hmd.ThueSuatGTGT, 
                      hmd.ThueXNK, hmd.ThueTTDB, hmd.ThueGTGT, hmd.PhuThu, 
                      hmd.TyLeThuKhac, hmd.TriGiaThuKhac, hmd.MienThue, 
                      hmd.Ma_HTS, hmd.DVT_HTS, hmd.SoLuong_HTS
FROM         dbo.t_KDT_ToKhaiMauDich as t INNER JOIN
                      dbo.t_KDT_HangMauDich as hmd ON t.ID = hmd.TKMD_ID where t.IDHopDong = @ID_HopDong AND t.ID in (SELECT  
                                            ID_TKMD 
                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                                  WHERE     ( ( MaDoanhNghiep LIKE 'XGC%'
                                                OR MaDoanhNghiep LIKE 'XV%'
                                              )
                                              AND ID_TKMD =@ID_TKX
                                            ))
) AS hangXuat ON pbtkx.ID_TKMD = hangXuat.TKMD_ID
                                                              AND pbtkx.MaSP = hangXuat.MaPhu
                      UNION
                      SELECT    pbtkx_3.MaSP ,
                                pbtkx_3.SoLuongXuat ,
                                pbtkx_3.MaDoanhNghiep ,
                                pbtkn_1_2.SoToKhaiNhap ,
                                pbtkn_1_2.MaLoaiHinhNhap ,
                                pbtkn_1_2.MaHaiQuanNhap ,
                                pbtkn_1_2.NamDangKyNhap ,
                                pbtkn_1_2.DinhMucChung ,
                                pbtkn_1_2.LuongTonDau ,
                                pbtkn_1_2.LuongPhanBo ,
                                pbtkn_1_2.LuongCungUng ,
                                pbtkn_1_2.LuongTonCuoi ,
                                pbtkn_1_2.TKXuat_ID ,
                                pbtkx_3.ID_TKMD ,
                                pbtkn_1_2.MaNPL ,
                                '01/01/1900' AS NgayDangKyNhap ,
                                hangXuat.MaLoaiHinh AS MaLoaiHinhXuat ,
                                YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat ,
                                hangXuat.MaHaiQuan AS MaHaiQuanXuat ,
                                hangXuat.SoToKhai AS SoToKhaiXuat ,
                                hangNhap.Ten AS TenNPL ,
                                hangNhap.DVT_ID AS DVT_NPL ,
                                0 AS DonGiaTT ,
                                0 AS ThueSuat ,
                                hangXuat.NgayDangKy AS NgayDangKyXuat ,
                                hangXuat.TenHang AS TenSP ,
                                hangXuat.DVT_ID AS DVT_SP ,
                                0 AS TyGiaTT ,
                                hangXuat.NgayDangKy AS NgayThucXuat ,
                                hangXuat.IDHopDong
                      FROM      ( SELECT    ID ,
                                            ID_TKMD ,
                                            MaSP ,
                                            SoLuongXuat ,
                                            MaDoanhNghiep
                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                                  WHERE     ( MaDoanhNghiep LIKE 'XGC%'
                                              OR MaDoanhNghiep LIKE 'XV%'
                                            )
                                ) AS pbtkx_3
                                INNER JOIN ( SELECT ID ,
                                                    SoToKhaiNhap ,
                                                    MaLoaiHinhNhap ,
                                                    MaHaiQuanNhap ,
                                                    NamDangKyNhap ,
                                                    MaNPL ,
                                                    DinhMucChung ,
                                                    LuongTonDau ,
                                                    LuongPhanBo ,
                                                    LuongCungUng ,
                                                    LuongTonCuoi ,
                                                    TKXuat_ID ,
                                                    MaDoanhNghiep
                                             FROM   dbo.t_GC_PhanBoToKhaiNhap
                                                    AS t_GC_PhanBoToKhaiNhap_1
                                             WHERE  ( MaLoaiHinhNhap LIKE '' )
                                           ) AS pbtkn_1_2 ON pbtkx_3.ID = pbtkn_1_2.TKXuat_ID
                                INNER JOIN dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_3.ID_TKMD = hangXuat.TKMD_ID
                                                              AND pbtkx_3.MaSP = hangXuat.MaPhu
                                INNER JOIN dbo.t_GC_NguyenPhuLieu AS hangNhap ON pbtkn_1_2.MaNPL = hangNhap.Ma
                                                              AND hangNhap.HopDong_ID = hangXuat.IDHopDong
                    ) AS B
            GROUP BY NgayDangKyNhap ,
                    SoToKhaiNhap ,
                    NamDangKyNhap ,
                    MaLoaiHinhNhap ,
                    MaNPL
        END
	
END



            ", idTKX, MaNPL, LoaiHinhXuat,id_HopDong);
            #endregion sql
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(SpName);
            //db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
            // db.AddInParameter(dbCommand, "@ID_TKX", SqlDbType.BigInt, idTKX);
            // db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.VarChar, LoaiHinhXuat);
            dbCommand.CommandTimeout = 10000;
            return db.ExecuteDataSet(dbCommand, transaction);

        }
        public static DataSet SelectViewPhanBoToKhaiXuatOfTKMDAndMaNPL_New(long idTKX, string MaNPL, string LoaiHinhXuat, SqlTransaction transaction)
        {
            //string spName = "[dbo].[p_XemPhanBoToKhai]";
//            string SpName = string.Format(@"DECLARE @ID_TKX BIGINT ,
//    @MaNPL VARCHAR(30) ,
//    @MaLoaiHinhXuat VARCHAR(5)
//	
//
//SET @ID_TKX = {0}
//SET @MaNPL = '{1}'
//SET @MaLoaiHinhXuat = '{2}'
//BEGIN
//	-- SET NOCOUNT ON added to prevent extra result sets from
//	-- interfering with SELECT statements.
//	
//    IF ( @MaLoaiHinhXuat LIKE '%18'
//         OR @MaLoaiHinhXuat LIKE '%19'
//         OR @MaLoaiHinhXuat LIKE '%20'
//         OR @MaLoaiHinhXuat LIKE 'PH%'
//         OR @MaLoaiHinhXuat LIKE 'XVE54'
//       ) 
//        BEGIN
//            SELECT  NgayDangKyNhap ,
//                    SoToKhaiNhap ,
//                    NamDangKyNhap ,
//                    MaLoaiHinhNhap ,
//                    MaNPL ,
//                    SUM(LuongPhanBo) AS LuongPhanBo ,
//                    SUM(LuongPhanBo) * DonGiaTT AS TriGia
//            FROM    -- To Khai xuat GC Chuyen Tiep va To khai nhap GC Binh Thuong
//                    ( SELECT    pbtkx_1_1.MaSP ,
//                                pbtkx_1_1.SoLuongXuat ,
//                                pbtkx_1_1.MaDoanhNghiep ,
//                                pbtkn_2.SoToKhaiNhap ,
//                                pbtkn_2.MaLoaiHinhNhap ,
//                                pbtkn_2.MaHaiQuanNhap ,
//                                pbtkn_2.NamDangKyNhap ,
//                                pbtkn_2.DinhMucChung ,
//                                pbtkn_2.LuongTonDau ,
//                                pbtkn_2.LuongPhanBo ,
//                                pbtkn_2.LuongCungUng ,
//                                pbtkn_2.LuongTonCuoi ,
//                                pbtkn_2.TKXuat_ID ,
//                                pbtkx_1_1.ID_TKMD ,
//                                pbtkn_2.MaNPL ,
//                                hangNhap.NgayDangKy AS NgayDangKyNhap ,
//                                hangXuat.MaLoaiHinh AS MaLoaiHinhXuat ,
//                                YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat ,
//                                hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat ,
//                                hangXuat.SoToKhai AS SoToKhaiXuat ,
//                                hangNhap.TenHang AS TenNPL ,
//                                hangNhap.DVT_ID AS DVT_NPL ,
//                                hangNhap.DonGiaKB AS DonGiaTT ,
//                                hangNhap.ThueSuatXNK AS ThueSuat ,
//                                hangXuat.NgayDangKy AS NgayDangKyXuat ,
//                                hangXuat.TenHang AS TenSP ,
//                                hangXuat.ID_DVT AS DVT_S ,
//                                hangNhap.TyGiaTinhThue AS TyGiaTT ,
//                                hangXuat.NgayDangKy AS NgayThucXuat ,
//                                hangXuat.IDHopDong
//                      FROM      ( SELECT    ID ,
//                                            ID_TKMD ,
//                                            MaSP ,
//                                            SoLuongXuat ,
//                                            MaDoanhNghiep
//                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_2
//                                  WHERE     ( t_GC_PhanBoToKhaiXuat_2.ID_TKMD = @ID_TKX )
//                                            AND ( t_GC_PhanBoToKhaiXuat_2.MaDoanhNghiep = @MaLoaiHinhXuat )
//                                ) AS pbtkx_1_1
//                                INNER JOIN ( SELECT ID ,
//                                                    SoToKhaiNhap ,
//                                                    MaLoaiHinhNhap ,
//                                                    MaHaiQuanNhap ,
//                                                    NamDangKyNhap ,
//                                                    MaNPL ,
//                                                    DinhMucChung ,
//                                                    LuongTonDau ,
//                                                    LuongPhanBo ,
//                                                    LuongCungUng ,
//                                                    LuongTonCuoi ,
//                                                    TKXuat_ID ,
//                                                    MaDoanhNghiep
//                                             FROM   dbo.t_GC_PhanBoToKhaiNhap
//                                                    AS t_GC_PhanBoToKhaiNhap_2
//                                             WHERE  ( MaLoaiHinhNhap LIKE 'N%' )
//                                                    AND t_GC_PhanBoToKhaiNhap_2.MaNPL = @MaNPL
//                                           ) AS pbtkn_2 ON pbtkx_1_1.ID = pbtkn_2.TKXuat_ID
//                                INNER JOIN dbo.v_GC_HangToKhai AS hangNhap ON pbtkn_2.SoToKhaiNhap = hangNhap.SoToKhai
//                                                              AND pbtkn_2.MaLoaiHinhNhap = hangNhap.MaLoaiHinh
//                                                              AND pbtkn_2.NamDangKyNhap = YEAR(hangNhap.NgayDangKy)
//                                                              AND pbtkn_2.MaNPL = hangNhap.MaPhu
//                                INNER JOIN dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_1_1.ID_TKMD = hangXuat.ID
//                                                              AND pbtkx_1_1.MaSP = hangXuat.MaHang
//                      UNION
//-- Tờ khai Xuất GC Chuyển tiếp được cung ứng từ tờ khai nhập GC chuyển tiếp 
//                      SELECT    pbtkx_2.MaSP ,
//                                pbtkx_2.SoLuongXuat ,
//                                pbtkx_2.MaDoanhNghiep ,
//                                pbtkn_1_1.SoToKhaiNhap ,
//                                pbtkn_1_1.MaLoaiHinhNhap ,
//                                pbtkn_1_1.MaHaiQuanNhap ,
//                                pbtkn_1_1.NamDangKyNhap ,
//                                pbtkn_1_1.DinhMucChung ,
//                                pbtkn_1_1.LuongTonDau ,
//                                pbtkn_1_1.LuongPhanBo ,
//                                pbtkn_1_1.LuongCungUng ,
//                                pbtkn_1_1.LuongTonCuoi ,
//                                pbtkn_1_1.TKXuat_ID ,
//                                pbtkx_2.ID_TKMD ,
//                                pbtkn_1_1.MaNPL ,
//                                hangNhap.NgayDangKy AS NgayDangKyNhap ,
//                                hangXuat.MaLoaiHinh AS MaLoaiHinhXuat ,
//                                YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat ,
//                                hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat ,
//                                hangXuat.SoToKhai AS SoToKhaiXuat ,
//                                hangNhap.TenHang AS TenNPL ,
//                                hangNhap.ID_DVT AS DVT_NPL ,
//                                hangNhap.DonGia AS DonGiaTT ,
//                                0 AS ThueSuat ,
//                                hangXuat.NgayDangKy AS NgayDangKyXuat ,
//                                hangXuat.TenHang AS TenSP ,
//                                hangXuat.ID_DVT AS DVT_SP ,
//                                hangNhap.TyGiaVND AS TyGiaTT ,
//                                hangXuat.NgayDangKy AS NgayThucXuat ,
//                                hangXuat.IDHopDong
//                      FROM      ( SELECT    ID ,
//                                            ID_TKMD ,
//                                            MaSP ,
//                                            SoLuongXuat ,
//                                            MaDoanhNghiep
//                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
//                                  WHERE     ( t_GC_PhanBoToKhaiXuat_1.ID_TKMD = @ID_TKX )
//                                            AND ( t_GC_PhanBoToKhaiXuat_1.MaDoanhNghiep = @MaLoaiHinhXuat )
//                                ) AS pbtkx_2
//                                INNER JOIN ( SELECT ID ,
//                                                    SoToKhaiNhap ,
//                                                    MaLoaiHinhNhap ,
//                                                    MaHaiQuanNhap ,
//                                                    NamDangKyNhap ,
//                                                    MaNPL ,
//                                                    DinhMucChung ,
//                                                    LuongTonDau ,
//                                                    LuongPhanBo ,
//                                                    LuongCungUng ,
//                                                    LuongTonCuoi ,
//                                                    TKXuat_ID ,
//                                                    MaDoanhNghiep
//                                             FROM   dbo.t_GC_PhanBoToKhaiNhap
//                                                    AS t_GC_PhanBoToKhaiNhap_1
//                                             WHERE  ( MaLoaiHinhNhap LIKE 'PH%'
//                                                      OR MaLoaiHinhNhap LIKE 'NVE23'
//                                                      OR MaLoaiHinhNhap LIKE '%19'
//                                                      OR MaLoaiHinhNhap LIKE '%18'
//                                                      OR MaLoaiHinhNhap LIKE '%20'
//                                                    )
//                                                    AND t_GC_PhanBoToKhaiNhap_1.MaNPL = @MaNPL
//                                           ) AS pbtkn_1_1 ON pbtkx_2.ID = pbtkn_1_1.TKXuat_ID
//                                INNER JOIN dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1_1.SoToKhaiNhap = hangNhap.SoToKhai
//                                                              AND pbtkn_1_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh
//                                                              AND pbtkn_1_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy)
//                                                              AND pbtkn_1_1.MaNPL = hangNhap.MaHang
//                                INNER JOIN dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_2.ID_TKMD = hangXuat.ID
//                                                              AND pbtkx_2.MaSP = hangXuat.MaHang
//                    ) AS A
//            WHERE   A.SoToKhaiNhap <> 0
//                    AND A.LuongPhanBo <> 0
//            GROUP BY NgayDangKyNhap ,
//                    SoToKhaiNhap ,
//                    NamDangKyNhap ,
//                    MaLoaiHinhNhap ,
//                    MaNPL,
//                    A.DonGiaTT                 
//
//        END
//    ELSE 
//        BEGIN
//            SELECT  NgayDangKyNhap ,
//                    SoToKhaiNhap ,
//                    NamDangKyNhap ,
//                    MaLoaiHinhNhap ,
//                    MaNPL ,
//                    SUM(LuongPhanBo) AS LuongPhanBo,
//                    SUM(B.LuongPhanBo) * B.DonGiaTT AS TriGia
//            FROM    ( SELECT    pbtkx_1.MaSP ,
//                                pbtkx_1.SoLuongXuat ,
//                                pbtkx_1.MaDoanhNghiep ,
//                                pbtkn.SoToKhaiNhap ,
//                                pbtkn.MaLoaiHinhNhap ,
//                                pbtkn.MaHaiQuanNhap ,
//                                pbtkn.NamDangKyNhap ,
//                                pbtkn.DinhMucChung ,
//                                pbtkn.LuongTonDau ,
//                                pbtkn.LuongPhanBo ,
//                                pbtkn.LuongCungUng ,
//                                pbtkn.LuongTonCuoi ,
//                                pbtkn.TKXuat_ID ,
//                                pbtkx_1.ID_TKMD ,
//                                pbtkn.MaNPL ,
//                                hangNhap.NgayDangKy AS NgayDangKyNhap ,
//                                hangXuat.MaLoaiHinh AS MaLoaiHinhXuat ,
//                                YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat ,
//                                hangXuat.MaHaiQuan AS MaHaiQuanXuat ,
//                                hangXuat.SoToKhai AS SoToKhaiXuat ,
//                                hangNhap.TenHang AS TenNPL ,
//                                hangNhap.DVT_ID AS DVT_NPL ,
//                                hangNhap.DonGiaKB AS DonGiaTT ,
//                                hangNhap.ThueSuatXNK AS ThueSuat ,
//                                hangXuat.NgayDangKy AS NgayDangKyXuat ,
//                                hangXuat.TenHang AS TenSP ,
//                                hangXuat.DVT_ID AS DVT_SP ,
//                                hangNhap.TyGiaTinhThue AS TyGiaTT ,
//                                hangXuat.NgayDangKy AS NgayThucXuat ,
//                                hangXuat.IDHopDong
//                      FROM      ( SELECT    ID ,
//                                            ID_TKMD ,
//                                            MaSP ,
//                                            SoLuongXuat ,
//                                            MaDoanhNghiep
//                                  FROM      dbo.t_GC_PhanBoToKhaiXuat
//                                  WHERE     ( ( MaDoanhNghiep LIKE 'XGC%'
//                                                OR MaDoanhNghiep LIKE 'XV%'
//                                              )
//                                              AND dbo.t_GC_PhanBoToKhaiXuat.ID_TKMD = @ID_TKX
//                                            )
//                                ) AS pbtkx_1
//                                INNER JOIN ( SELECT ID ,
//                                                    SoToKhaiNhap ,
//                                                    MaLoaiHinhNhap ,
//                                                    MaHaiQuanNhap ,
//                                                    NamDangKyNhap ,
//                                                    MaNPL ,
//                                                    DinhMucChung ,
//                                                    LuongTonDau ,
//                                                    LuongPhanBo ,
//                                                    LuongCungUng ,
//                                                    LuongTonCuoi ,
//                                                    TKXuat_ID ,
//                                                    MaDoanhNghiep
//                                             FROM   dbo.t_GC_PhanBoToKhaiNhap
//                                             WHERE  ( MaLoaiHinhNhap LIKE 'N%'
//                                                      AND dbo.t_GC_PhanBoToKhaiNhap.MaNPL = @MaNPL
//                                                    )
//                                           ) AS pbtkn ON pbtkx_1.ID = pbtkn.TKXuat_ID
//                                INNER JOIN dbo.v_GC_HangToKhai AS hangNhap ON pbtkn.SoToKhaiNhap = hangNhap.SoToKhai
//                                                              AND pbtkn.MaLoaiHinhNhap = hangNhap.MaLoaiHinh
//                                                              AND pbtkn.NamDangKyNhap = YEAR(hangNhap.NgayDangKy)
//                                                              AND pbtkn.MaNPL = hangNhap.MaPhu
//                                INNER JOIN dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_1.ID_TKMD = hangXuat.TKMD_ID
//                                                              AND pbtkx_1.MaSP = hangXuat.MaPhu
//                      UNION
//                      SELECT    pbtkx.MaSP ,
//                                pbtkx.SoLuongXuat ,
//                                pbtkx.MaDoanhNghiep ,
//                                pbtkn_1.SoToKhaiNhap ,
//                                pbtkn_1.MaLoaiHinhNhap ,
//                                pbtkn_1.MaHaiQuanNhap ,
//                                pbtkn_1.NamDangKyNhap ,
//                                pbtkn_1.DinhMucChung ,
//                                pbtkn_1.LuongTonDau ,
//                                pbtkn_1.LuongPhanBo ,
//                                pbtkn_1.LuongCungUng ,
//                                pbtkn_1.LuongTonCuoi ,
//                                pbtkn_1.TKXuat_ID ,
//                                pbtkx.ID_TKMD ,
//                                pbtkn_1.MaNPL ,
//                                hangNhap.NgayDangKy AS NgayDangKyNhap ,
//                                hangXuat.MaLoaiHinh AS MaLoaiHinhXuat ,
//                                YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat ,
//                                hangXuat.MaHaiQuan AS MaHaiQuanXuat ,
//                                hangXuat.SoToKhai AS SoToKhaiXuat ,
//                                hangNhap.TenHang AS TenNPL ,
//                                hangNhap.ID_DVT AS DVT_NPL ,
//                                hangNhap.DonGia AS DonGiaTT ,
//                                0 AS ThueSuat ,
//                                hangXuat.NgayDangKy AS NgayDangKyXuat ,
//                                hangXuat.TenHang AS TenSP ,
//                                hangXuat.DVT_ID AS DVT_SP ,
//                                hangNhap.TyGiaVND AS TyGiaTT ,
//                                hangXuat.NgayDangKy AS NgayThucXuat ,
//                                hangXuat.IDHopDong
//                      FROM      ( SELECT    ID ,
//                                            ID_TKMD ,
//                                            MaSP ,
//                                            SoLuongXuat ,
//                                            MaDoanhNghiep
//                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
//                                  WHERE     ( ( MaDoanhNghiep LIKE 'XGC%'
//                                                OR MaDoanhNghiep LIKE 'XV%'
//                                              )
//                                              AND ID_TKMD = @ID_TKX
//                                            )
//                                ) AS pbtkx
//                                INNER JOIN ( SELECT ID ,
//                                                    SoToKhaiNhap ,
//                                                    MaLoaiHinhNhap ,
//                                                    MaHaiQuanNhap ,
//                                                    NamDangKyNhap ,
//                                                    MaNPL ,
//                                                    DinhMucChung ,
//                                                    LuongTonDau ,
//                                                    LuongPhanBo ,
//                                                    LuongCungUng ,
//                                                    LuongTonCuoi ,
//                                                    TKXuat_ID ,
//                                                    MaDoanhNghiep
//                                             FROM   dbo.t_GC_PhanBoToKhaiNhap
//                                                    AS t_GC_PhanBoToKhaiNhap_1
//                                             WHERE  ( ( MaLoaiHinhNhap LIKE 'PH%'
//                                                        OR MaLoaiHinhNhap LIKE 'NVE23'
//                                                        OR MaLoaiHinhNhap LIKE '%19'
//                                                        OR MaLoaiHinhNhap LIKE '%18'
//                                                        OR MaLoaiHinhNhap LIKE '%20'
//                                                      )
//                                                      AND ( MaNPL = @MaNPL )
//                                                    )
//                                           ) AS pbtkn_1 ON pbtkx.ID = pbtkn_1.TKXuat_ID
//                                INNER JOIN dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1.SoToKhaiNhap = hangNhap.SoToKhai
//                                                              AND pbtkn_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh
//                                                              AND pbtkn_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy)
//                                                              AND pbtkn_1.MaNPL = hangNhap.MaHang
//                                INNER JOIN dbo.v_GC_HangToKhai AS hangXuat ON pbtkx.ID_TKMD = hangXuat.TKMD_ID
//                                                              AND pbtkx.MaSP = hangXuat.MaPhu
//                      UNION
//                      SELECT    pbtkx_3.MaSP ,
//                                pbtkx_3.SoLuongXuat ,
//                                pbtkx_3.MaDoanhNghiep ,
//                                pbtkn_1_2.SoToKhaiNhap ,
//                                pbtkn_1_2.MaLoaiHinhNhap ,
//                                pbtkn_1_2.MaHaiQuanNhap ,
//                                pbtkn_1_2.NamDangKyNhap ,
//                                pbtkn_1_2.DinhMucChung ,
//                                pbtkn_1_2.LuongTonDau ,
//                                pbtkn_1_2.LuongPhanBo ,
//                                pbtkn_1_2.LuongCungUng ,
//                                pbtkn_1_2.LuongTonCuoi ,
//                                pbtkn_1_2.TKXuat_ID ,
//                                pbtkx_3.ID_TKMD ,
//                                pbtkn_1_2.MaNPL ,
//                                '01/01/1900' AS NgayDangKyNhap ,
//                                hangXuat.MaLoaiHinh AS MaLoaiHinhXuat ,
//                                YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat ,
//                                hangXuat.MaHaiQuan AS MaHaiQuanXuat ,
//                                hangXuat.SoToKhai AS SoToKhaiXuat ,
//                                hangNhap.Ten AS TenNPL ,
//                                hangNhap.DVT_ID AS DVT_NPL ,
//                                0 AS DonGiaTT ,
//                                0 AS ThueSuat ,
//                                hangXuat.NgayDangKy AS NgayDangKyXuat ,
//                                hangXuat.TenHang AS TenSP ,
//                                hangXuat.DVT_ID AS DVT_SP ,
//                                0 AS TyGiaTT ,
//                                hangXuat.NgayDangKy AS NgayThucXuat ,
//                                hangXuat.IDHopDong
//                      FROM      ( SELECT    ID ,
//                                            ID_TKMD ,
//                                            MaSP ,
//                                            SoLuongXuat ,
//                                            MaDoanhNghiep
//                                  FROM      dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
//                                  WHERE     ( MaDoanhNghiep LIKE 'XGC%'
//                                              OR MaDoanhNghiep LIKE 'XV%'
//                                            )
//                                ) AS pbtkx_3
//                                INNER JOIN ( SELECT ID ,
//                                                    SoToKhaiNhap ,
//                                                    MaLoaiHinhNhap ,
//                                                    MaHaiQuanNhap ,
//                                                    NamDangKyNhap ,
//                                                    MaNPL ,
//                                                    DinhMucChung ,
//                                                    LuongTonDau ,
//                                                    LuongPhanBo ,
//                                                    LuongCungUng ,
//                                                    LuongTonCuoi ,
//                                                    TKXuat_ID ,
//                                                    MaDoanhNghiep
//                                             FROM   dbo.t_GC_PhanBoToKhaiNhap
//                                                    AS t_GC_PhanBoToKhaiNhap_1
//                                             WHERE  ( MaLoaiHinhNhap LIKE '' )
//                                           ) AS pbtkn_1_2 ON pbtkx_3.ID = pbtkn_1_2.TKXuat_ID
//                                INNER JOIN dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_3.ID_TKMD = hangXuat.TKMD_ID
//                                                              AND pbtkx_3.MaSP = hangXuat.MaPhu
//                                INNER JOIN dbo.t_GC_NguyenPhuLieu AS hangNhap ON pbtkn_1_2.MaNPL = hangNhap.Ma
//                                                              AND hangNhap.HopDong_ID = hangXuat.IDHopDong
//                    ) AS B
//            WHERE   B.SoToKhaiNhap <> 0
//                    AND B.LuongPhanBo <> 0
//            GROUP BY NgayDangKyNhap ,
//                    SoToKhaiNhap ,
//                    NamDangKyNhap ,
//                    MaLoaiHinhNhap ,
//                    MaNPL,
//                    B.DonGiaTT
//        END
//	
//END", idTKX, MaNPL, LoaiHinhXuat);
//            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
//            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(SpName);
//            //db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
//            // db.AddInParameter(dbCommand, "@ID_TKX", SqlDbType.BigInt, idTKX);
//            // db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.VarChar, LoaiHinhXuat);
//            dbCommand.CommandTimeout = 10000;
//            return db.ExecuteDataSet(dbCommand, transaction);
            const string spName = "[dbo].[p_ViewPhanBoToKhaiXuatOfTKAndMaNPL]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.NVarChar, idTKX);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
            dbCommand.CommandTimeout = 600;
            return db.ExecuteDataSet(dbCommand);
        }
        public static DataSet SelectViewPhanBoToKhaiXuatOfTKCTAndMaNPL(long idTKX, string MaNPL)
        {
     //       string spName = "select NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL, sum(LuongPhanBo) as LuongPhanBo ,SUM(LuongPhanBo) * DonGiaTT AS TriGia from v_GC_PhanBo where ID_TKMD = " + idTKX + " AND ( MaLoaiHinhXuat LIKE 'PH%'  OR MaDoanhNghiep IN ('XGC18', 'XGC19','XVE54') ) AND MaNPL='" + MaNPL + "'" +
     //            " GROUP BY NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL ,DonGiaTT";

     //       spName = "";
     //       spName = "        SELECT  CASE WHEN ( SELECT TOP 1 " +
     //                "               SoToKhai " +
     //                 "       FROM    dbo.t_KDT_ToKhaiMauDich " +
     //                 "       WHERE   SoToKhai = SoToKhaiNhap " +
     //                  "              AND YEAR(NgayDangKy) = NamDangKyNhap " +
     //                 "     ) IS NOT NULL " +
     //             "    THEN ( SELECT TOP 1 " +
     //             "                   NgayDangKy " +
     //              "          FROM    dbo.t_KDT_ToKhaiMauDich " +
     //            "            WHERE   SoToKhai = SoToKhaiNhap " +
     //            "                    AND YEAR(NgayDangKy) = NamDangKyNhap " +
     //             "         ) "+
     //             "    ELSE ( SELECT TOP 1 " +
     //             "                   NgayDangKy " +
     //            "            FROM    dbo.t_KDT_GC_ToKhaiChuyenTiep " +
     //            "            WHERE   SoToKhai = SoToKhaiNhap " +
     //            "                    AND YEAR(NgayDangKy) = NamDangKyNhap " +
     //           "           ) "+
     //         "   END AS NgayDangKyNhap , " +
     //         "   SoToKhaiNhap ," +
     //         "   NamDangKyNhap , " +
     //          "  MaLoaiHinhNhap ," +
     //         "   MaNPL , " +
     //         "   SUM(LuongPhanBo) AS LuongPhanBo " +
     //"    FROM    dbo.t_GC_PhanBoToKhaiNhap " +
     //"    WHERE   TKXuat_ID IN ( SELECT   ID " +
     // "                          FROM     dbo.t_GC_PhanBoToKhaiXuat " +
     //"                           WHERE    ID_TKMD = "+idTKX+" )" +
     //"            AND MaNPL = '"+MaNPL+"' " +
     //"    GROUP BY SoToKhaiNhap , " +
     //"            MaLoaiHinhNhap , " +
     //"            NamDangKyNhap , " +
     // "           MaLoaiHinhNhap , " +
     //"            MaNPL;";
            const string spName = "[dbo].[p_ViewPhanBoToKhaiXuatOfTKAndMaNPL]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.NVarChar, idTKX);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
            dbCommand.CommandTimeout = 600;
            return db.ExecuteDataSet(dbCommand);
        }
        public static IList<PhanBoToKhaiXuat> SelectPhanBoToKhaiXuatOfTKMD(long ID_TKMD)
        {
            IList<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            PhanBoToKhaiXuatCollection = PhanBoToKhaiXuat.SelectCollectionDynamic("ID_TKMD=" + ID_TKMD + " AND (MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%')", "");
            foreach (PhanBoToKhaiXuat pbTKXuat in PhanBoToKhaiXuatCollection)
            {
                pbTKXuat.LoadPhanBoToKhaiNhapCollection();
            }
            return PhanBoToKhaiXuatCollection;
        }
        public static IList<PhanBoToKhaiXuat> SelectPhanBoToKhaiXuatOfTKCT(long ID_TKMD)
        {
            IList<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            PhanBoToKhaiXuatCollection = PhanBoToKhaiXuat.SelectCollectionDynamic("ID_TKMD=" + ID_TKMD + " AND (MaDoanhNghiep LIKE 'PH%' OR MaDoanhNghiep IN ('XGC18', 'XGC19','XVE54'))", "");
            foreach (PhanBoToKhaiXuat pbTKXuat in PhanBoToKhaiXuatCollection)
            {
                pbTKXuat.LoadPhanBoToKhaiNhapCollection();
            }
            return PhanBoToKhaiXuatCollection;
        }
        public static IList<PhanBoToKhaiXuat> SelectPhanBoToKhaiXuatOfTKMD(long ID_TKMD, SqlTransaction transaction)
        {
            IList<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            PhanBoToKhaiXuatCollection = PhanBoToKhaiXuat.SelectCollectionDynamic("ID_TKMD=" + ID_TKMD + " AND (MaDoanhNghiep LIKE 'XGC%' OR MaDoanhNghiep LIKE 'XV%')", "", transaction);
            foreach (PhanBoToKhaiXuat pbTKXuat in PhanBoToKhaiXuatCollection)
            {
                pbTKXuat.LoadPhanBoToKhaiNhapCollection(transaction);
            }
            return PhanBoToKhaiXuatCollection;
        }
        public static IList<PhanBoToKhaiXuat> SelectPhanBoToKhaiXuatOfTKCT(long ID_TKMD, SqlTransaction transaction)
        {
            IList<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            PhanBoToKhaiXuatCollection = PhanBoToKhaiXuat.SelectCollectionDynamic("ID_TKMD=" + ID_TKMD + " AND (MaDoanhNghiep LIKE 'PH%' OR MaDoanhNghiep IN ('XGC18', 'XGC19','XVE54'))", "", transaction);
            foreach (PhanBoToKhaiXuat pbTKXuat in PhanBoToKhaiXuatCollection)
            {
                pbTKXuat.LoadPhanBoToKhaiNhapCollection(transaction);
            }
            return PhanBoToKhaiXuatCollection;
        }
        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiXuat_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand, transaction);
        }
        public static IList<PhanBoToKhaiXuat> SelectCollectionDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction)
        {
            IList<PhanBoToKhaiXuat> collection = new List<PhanBoToKhaiXuat>();

            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression, transaction);
            while (reader.Read())
            {
                PhanBoToKhaiXuat entity = new PhanBoToKhaiXuat();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_TKMD"))) entity.ID_TKMD = reader.GetInt64(reader.GetOrdinal("ID_TKMD"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongXuat"))) entity.SoLuongXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }
        public void DeletePhanBo(SqlTransaction transaction)
        {
            foreach (PhanBoToKhaiNhap pbToKhaiNhap in this.PhanBoToKhaiNhapCollection)
            {
                if (pbToKhaiNhap.SoToKhaiNhap > 0)
                {
                    NPLNhapTonThucTe nplTon = NPLNhapTonThucTe.Load(pbToKhaiNhap.SoToKhaiNhap, pbToKhaiNhap.MaLoaiHinhNhap, (short)pbToKhaiNhap.NamDangKyNhap, pbToKhaiNhap.MaHaiQuanNhap, pbToKhaiNhap.MaNPL, transaction);
                    if (nplTon == null)
                    {
                        throw new Exception("Không có tờ khai : " + pbToKhaiNhap.SoToKhaiNhap + "/" + pbToKhaiNhap.MaLoaiHinhNhap + "/" + pbToKhaiNhap.NamDangKyNhap + "không có trong hệ thống.");
                    }
                    nplTon.Ton += Convert.ToDecimal(pbToKhaiNhap.LuongPhanBo);
                    nplTon.Update(transaction);
                }
                pbToKhaiNhap.Delete(transaction);

            }
            this.Delete(transaction);
        }

        public void LoadPhanBoToKhaiNhapCollection(SqlTransaction transaction)
        {
            PhanBoToKhaiNhapCollection = (List<PhanBoToKhaiNhap>)PhanBoToKhaiNhap.SelectCollectionBy_TKXuat_ID(this.ID, transaction);
        }

        public static void PhanBoChoToKhaiXuatGC(ToKhaiMauDich TKMD, int SoThapPhanNPL, int SoThapPhanDM, ref int start)
        {
            if (TKMD.HMDCollection.Count == 0)
            {
                TKMD.LoadHMDCollection();
            }
            start += 1;

            //TKMD.HMDCollection.Sort(new SortHangMauDich());
            TKMD.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            DataTable tableNPLTonThucTe = NPLNhapTonThucTe.SelectNPLTonThucTeNgayDangKyNhoHonTKXuat(TKMD.NgayDangKy, TKMD.IDHopDong);
            start += 1;

            foreach (HangMauDich HMD in TKMD.HMDCollection)
            {
                PhanBoToKhaiXuat pbTKXuat = new PhanBoToKhaiXuat();
                pbTKXuat.MaDoanhNghiep = TKMD.MaLoaiHinh;
                pbTKXuat.MaSP = HMD.MaPhu.Trim();
                pbTKXuat.SoLuongXuat = HMD.SoLuong;
                pbTKXuat.ID_TKMD = TKMD.ID;
                pbTKXuat.PhanBoToKhaiNhapCollection = new List<PhanBoToKhaiNhap>();
                TKMD.PhanBoToKhaiXuatCollection.Add(pbTKXuat);
                SanPham sp = new SanPham();
                sp.HopDong_ID = TKMD.IDHopDong;
                sp.Ma = HMD.MaPhu.Trim();
                if (!sp.Load())
                {
                    throw new Exception("Mã sản phẩm : " + HMD.MaPhu.Trim() + " trong tờ khai xuất số : " + TKMD.SoToKhai + "/" + TKMD.MaLoaiHinh + "/" + TKMD.NgayDangKy.Year + " không có trong hợp đồng này.Hãy kiểm tra lại mã sản phẩm này.");
                }

                DataTable dsDinhMucSanPham = DinhMuc.getDinhMuc(TKMD.IDHopDong, HMD.MaPhu.Trim(), HMD.SoLuong, SoThapPhanNPL, SoThapPhanDM);
                if (dsDinhMucSanPham.Rows.Count == 0)
                {
                    throw new Exception("Mã sản phẩm : " + HMD.MaPhu.Trim() + " trong tờ khai xuất số : " + TKMD.SoToKhai + "/" + TKMD.MaLoaiHinh + "/" + TKMD.NgayDangKy.Year + " chưa có định mức trong hợp đồng này.Hãy kiểm tra lại định mức của mã sản phẩm này.");
                }

                foreach (DataRow rowDinhMuc in dsDinhMucSanPham.Rows)
                {
                    string MaNPL = rowDinhMuc["MaNguyenPhuLieu"].ToString();

                    //if(MaNPL == "830890900024")
                    //{
                    //}

                    decimal NhuCau = Convert.ToDecimal(rowDinhMuc["NhuCau"]);
                    //DataRow[] RowToKhaiCollection = tableNPLTonThucTe.Select("MaNPL='" + MaNPL + "' and Ton>0", "NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan");

                    ArrayList al = new ArrayList();
                    //Uu tien lay cac tk loai hinh chuyen tiep truoc
                    al.AddRange(tableNPLTonThucTe.Select("(MaLoaiHinh = 'NGC18' OR MaLoaiHinh = 'PHPLN') AND MaNPL='" + MaNPL + "' and Ton>0", "NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan"));
                    //Tiep theo lay cac tk loai hinh con lai
                    al.AddRange(tableNPLTonThucTe.Select("(MaLoaiHinh <> 'NGC18' AND MaLoaiHinh <> 'PHPLN') AND MaNPL='" + MaNPL + "' and Ton>0", "NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan"));
                    DataRow[] RowToKhaiCollection = (DataRow[])al.ToArray(typeof(DataRow));

                    if (RowToKhaiCollection.Length == 0)
                    {
                        PhanBoToKhaiNhap pbToKhaiNhap = new PhanBoToKhaiNhap();
                        pbToKhaiNhap.DinhMucChung = Convert.ToDecimal(rowDinhMuc["DinhMucChung"]);
                        pbToKhaiNhap.LuongCungUng = Convert.ToDouble(NhuCau);
                        pbToKhaiNhap.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                        pbToKhaiNhap.MaNPL = MaNPL;
                        pbToKhaiNhap.MuaVN = 0;
                        pbTKXuat.PhanBoToKhaiNhapCollection.Add(pbToKhaiNhap);
                        continue;
                    }

                    foreach (DataRow rowTK in RowToKhaiCollection)
                    {
                        PhanBoToKhaiNhap pbToKhaiNhap = new PhanBoToKhaiNhap();
                        decimal LuongTon = Convert.ToDecimal(rowTK["Ton"].ToString());
                        if (LuongTon > 0)
                        {
                            pbToKhaiNhap.DinhMucChung = Convert.ToDecimal(rowDinhMuc["DinhMucChung"]);
                            pbToKhaiNhap.LuongCungUng = 0;
                            pbToKhaiNhap.LuongTonDau = Convert.ToDouble(LuongTon);
                            bool ok = true;//true la du --> thoat vong lap//false la thieu phai dung to khai khac
                            if (NhuCau > LuongTon)
                            {
                                pbToKhaiNhap.LuongPhanBo = Convert.ToDouble(LuongTon);
                                NhuCau -= LuongTon;
                                rowTK["Ton"] = 0;
                                ok = false;
                                pbToKhaiNhap.LuongTonCuoi = 0;
                            }
                            else
                            {
                                pbToKhaiNhap.LuongPhanBo = Convert.ToDouble(NhuCau);
                                rowTK["Ton"] = (LuongTon - NhuCau);
                                NhuCau = 0;
                                pbToKhaiNhap.LuongTonCuoi = Convert.ToDouble(rowTK["Ton"]);
                            }
                            pbToKhaiNhap.MuaVN = 0;
                            pbToKhaiNhap.MaDoanhNghiep = rowTK["MaDoanhNghiep"].ToString();
                            pbToKhaiNhap.MaHaiQuanNhap = rowTK["MaHaiQuan"].ToString();
                            pbToKhaiNhap.MaLoaiHinhNhap = rowTK["MaLoaiHinh"].ToString();
                            pbToKhaiNhap.MaNPL = MaNPL;
                            pbToKhaiNhap.NamDangKyNhap = Convert.ToInt16(rowTK["NamDangKy"]);
                            pbToKhaiNhap.SoToKhaiNhap = Convert.ToInt32(rowTK["SoToKhai"]);

                            //if(pbToKhaiNhap.SoToKhaiNhap==)
                            //{
                            //}

                            pbTKXuat.PhanBoToKhaiNhapCollection.Add(pbToKhaiNhap);
                            if (ok)
                                break;
                        }
                    }

                    if (NhuCau > 0)
                    {
                        pbTKXuat.PhanBoToKhaiNhapCollection[pbTKXuat.PhanBoToKhaiNhapCollection.Count - 1].LuongCungUng = Convert.ToDouble(NhuCau);
                    }
                }
            }
            start += 1;

            InsertUpdateFull(TKMD, tableNPLTonThucTe);
            start += 1;
        }

        public static void PhanBoChoToKhaiChuyenTiep(ToKhaiChuyenTiep TKCT, int SoThapPhanNPL, int SoThapPhanDM, ref int start)
        {
            if (TKCT.HCTCollection.Count == 0)
            {
                TKCT.LoadHCTCollection();
            }
            start += 1;

            //TKMD.HMDCollection.Sort(new SortHangMauDich());
            TKCT.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            DataTable tableNPLTonThucTe = NPLNhapTonThucTe.SelectNPLTonThucTeNgayDangKyNhoHonTKXuat(TKCT.NgayDangKy, TKCT.IDHopDong);
            start += 1;

            foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
            {
                PhanBoToKhaiXuat pbTKXuat = new PhanBoToKhaiXuat();
                pbTKXuat.MaDoanhNghiep = TKCT.MaLoaiHinh;
                pbTKXuat.MaSP = HCT.MaHang.Trim();
                pbTKXuat.SoLuongXuat = HCT.SoLuong;
                pbTKXuat.ID_TKMD = TKCT.ID;
                pbTKXuat.PhanBoToKhaiNhapCollection = new List<PhanBoToKhaiNhap>();
                TKCT.PhanBoToKhaiXuatCollection.Add(pbTKXuat);
                SanPham sp = new SanPham();
                sp.HopDong_ID = TKCT.IDHopDong;
                sp.Ma = HCT.MaHang.Trim();
                if (!sp.Load())
                {
                    throw new Exception("Mã sản phẩm : " + HCT.MaHang.Trim() + " trong tờ khai xuất số : " + TKCT.SoToKhai + "/" + TKCT.MaLoaiHinh + "/" + TKCT.NgayDangKy.Year + " không có trong hợp đồng này.Hãy kiểm tra lại mã sản phẩm này.");
                }
                
                DataTable dsDinhMucSanPham = DinhMuc.getDinhMuc(TKCT.IDHopDong, HCT.MaHang.Trim(), HCT.SoLuong, SoThapPhanNPL, SoThapPhanDM);
                if (dsDinhMucSanPham.Rows.Count == 0)
                {
                    throw new Exception("Mã sản phẩm : " + HCT.MaHang.Trim() + " trong tờ khai xuất số : " + TKCT.SoToKhai + "/" + TKCT.MaLoaiHinh + "/" + TKCT.NgayDangKy.Year + " chưa có định mức trong hợp đồng này.Hãy kiểm tra lại định mức của mã sản phẩm này.");
                }

                foreach (DataRow rowDinhMuc in dsDinhMucSanPham.Rows)
                {
                    string MaNPL = rowDinhMuc["MaNguyenPhuLieu"].ToString();
                    decimal NhuCau = Convert.ToDecimal(rowDinhMuc["NhuCau"]);
                    
                    //DataRow[] RowToKhaiCollection = tableNPLTonThucTe.Select("MaNPL='" + MaNPL + "' and Ton>0", "NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan");

                    ArrayList al = new ArrayList();
                    //Uu tien lay cac tk loai hinh chuyen tiep truoc
                    al.AddRange(tableNPLTonThucTe.Select("(MaLoaiHinh = 'NGC18' OR MaLoaiHinh = 'PHPLN') AND MaNPL='" + MaNPL + "' and Ton>0", "NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan"));
                    //Tiep theo lay cac tk loai hinh con lai
                    al.AddRange(tableNPLTonThucTe.Select("(MaLoaiHinh <> 'NGC18' AND MaLoaiHinh <> 'PHPLN') AND MaNPL='" + MaNPL + "' and Ton>0", "NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan"));
                    DataRow[] RowToKhaiCollection = (DataRow[])al.ToArray(typeof(DataRow));

                    if (RowToKhaiCollection.Length == 0)
                    {
                        PhanBoToKhaiNhap pbToKhaiNhap = new PhanBoToKhaiNhap();
                        pbToKhaiNhap.DinhMucChung = Convert.ToDecimal(rowDinhMuc["DinhMucChung"]);
                        pbToKhaiNhap.LuongCungUng = Convert.ToDouble(NhuCau);
                        pbToKhaiNhap.MaDoanhNghiep = TKCT.MaDoanhNghiep;
                        pbToKhaiNhap.MaNPL = MaNPL;
                        pbToKhaiNhap.MuaVN = 0;
                        pbTKXuat.PhanBoToKhaiNhapCollection.Add(pbToKhaiNhap);
                        continue;
                    }

                    foreach (DataRow rowTK in RowToKhaiCollection)
                    {
                        PhanBoToKhaiNhap pbToKhaiNhap = new PhanBoToKhaiNhap();
                        decimal LuongTon = Convert.ToDecimal(rowTK["Ton"].ToString());
                        if (LuongTon > 0)
                        {
                            pbToKhaiNhap.DinhMucChung = Convert.ToDecimal(rowDinhMuc["DinhMucChung"]);
                            pbToKhaiNhap.LuongCungUng = 0;
                            pbToKhaiNhap.LuongTonDau = Convert.ToDouble(LuongTon);
                            bool ok = true;//true la du --> thoat vong lap//false la thieu phai dung to khai khac
                            if (NhuCau > LuongTon)
                            {
                                pbToKhaiNhap.LuongPhanBo = Convert.ToDouble(LuongTon);
                                NhuCau -= LuongTon;
                                rowTK["Ton"] = 0;
                                ok = false;
                                pbToKhaiNhap.LuongTonCuoi = 0;
                            }
                            else
                            {
                                pbToKhaiNhap.LuongPhanBo = Convert.ToDouble(NhuCau);
                                rowTK["Ton"] = (LuongTon - NhuCau);
                                NhuCau = 0;
                                pbToKhaiNhap.LuongTonCuoi = Convert.ToDouble(rowTK["Ton"]);
                            }
                            pbToKhaiNhap.MaDoanhNghiep = rowTK["MaDoanhNghiep"].ToString();
                            pbToKhaiNhap.MaHaiQuanNhap = rowTK["MaHaiQuan"].ToString();
                            pbToKhaiNhap.MaLoaiHinhNhap = rowTK["MaLoaiHinh"].ToString();
                            pbToKhaiNhap.MaNPL = MaNPL;
                            pbToKhaiNhap.NamDangKyNhap = Convert.ToInt16(rowTK["NamDangKy"]);
                            pbToKhaiNhap.SoToKhaiNhap = Convert.ToInt32(rowTK["SoToKhai"]);
                            pbToKhaiNhap.MuaVN = 0;
                            //if(pbToKhaiNhap.SoToKhaiNhap==)
                            //{
                            //}

                            pbTKXuat.PhanBoToKhaiNhapCollection.Add(pbToKhaiNhap);
                            if (ok)
                                break;
                        }
                    }

                    if (NhuCau > 0)
                    {
                        pbTKXuat.PhanBoToKhaiNhapCollection[pbTKXuat.PhanBoToKhaiNhapCollection.Count - 1].LuongCungUng = Convert.ToDouble(NhuCau);
                    }
                }
            }
            start += 1;

            InsertUpdateFull(TKCT, tableNPLTonThucTe);
            start += 1;
        }

        public void XoaPhanBo(SqlTransaction transaction)
        {
            foreach (PhanBoToKhaiNhap pbToKhaiNhap in this.PhanBoToKhaiNhapCollection)
            {
                if (pbToKhaiNhap.SoToKhaiNhap > 0)
                {
                    NPLNhapTonThucTe nplTon = NPLNhapTonThucTe.Load(pbToKhaiNhap.SoToKhaiNhap, pbToKhaiNhap.MaLoaiHinhNhap, (short)pbToKhaiNhap.NamDangKyNhap, pbToKhaiNhap.MaHaiQuanNhap, pbToKhaiNhap.MaNPL, transaction);
                    if (nplTon == null)
                    {
                        throw new Exception("Không có tờ khai : " + pbToKhaiNhap.SoToKhaiNhap + "/" + pbToKhaiNhap.MaLoaiHinhNhap + "/" + pbToKhaiNhap.NamDangKyNhap + "không có trong hệ thống.");
                    }
                    nplTon.Ton += Convert.ToDecimal(pbToKhaiNhap.LuongPhanBo);
                    nplTon.Update(transaction);
                }
                pbToKhaiNhap.Delete(transaction);
            }
            this.Delete(transaction);
        }

        public static void XoaPhanBoToKhaiXuatGC(ToKhaiMauDich TKMD)
        {
            if (TKMD.PhanBoToKhaiXuatCollection.Count == 0)
            {
                TKMD.LoadPhanBoToKhaiXuatCollection();
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        //bool ret01 = true;
                        foreach (PhanBoToKhaiXuat item in TKMD.PhanBoToKhaiXuatCollection)
                        {
                            item.XoaPhanBo(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

        }

        public static DataSet SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndMaNPLAndTonHonO(string maHaiQuan, string MaDoanhNghiep, string MaNPL)
        {
            string sql = "select * from t_GC_NPLNhapTonThucTe where MaDoanhNghiep='" + MaDoanhNghiep + "' and MaHaiQuan='" + maHaiQuan + "' and Ton>0 and ngaydangky<@ngaydangky and MaNPL=@MaNPL order by NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);

            return db.ExecuteDataSet(dbCommand);
        }
        public static void PhanBoChoToKhaiTaiXuat(ToKhaiMauDich TKMD, int SoThapPhanNPL)
        {
            if (TKMD.HMDCollection.Count == 0)
            {
                TKMD.LoadHMDCollection();
            }
            //TKMD.HMDCollection.Sort(new SortHangMauDich());


            DataTable tableTK = NPLNhapTonThucTe.SelectBy_HopDongAndTonHonO(TKMD.IDHopDong, TKMD.NgayDangKy, SoThapPhanNPL).Tables[0];


            foreach (PhanBoToKhaiXuat pbToKhaiXuat in TKMD.PhanBoToKhaiXuatCollection)
            {
                foreach (PhanBoToKhaiNhap pbToKhaiNhap in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                {
                    DataRow rowToKhaiNhap = tableTK.Select("SoToKhai=" + pbToKhaiNhap.SoToKhaiNhap + " and MaLoaiHinh='" + pbToKhaiNhap.MaLoaiHinhNhap + "' and NamDangKy=" + pbToKhaiNhap.NamDangKyNhap + " and MaHaiQuan='" + pbToKhaiNhap.MaHaiQuanNhap + "' and MaNPL='" + pbToKhaiNhap.MaNPL.Trim() + "'")[0];
                    pbToKhaiNhap.LuongTonDau = Convert.ToDouble(rowToKhaiNhap["Ton"]);
                    pbToKhaiNhap.LuongTonCuoi = pbToKhaiNhap.LuongTonDau - pbToKhaiNhap.LuongPhanBo;
                    rowToKhaiNhap["Ton"] = pbToKhaiNhap.LuongTonCuoi;
                }
            }
            InsertUpdateFull(TKMD, tableTK);
        }
        public static void PhanBoChoToKhaiTaiXuat(ToKhaiChuyenTiep TKCT, int SoThapPhanNPL)
        {
            if (TKCT.HCTCollection.Count == 0)
            {
                TKCT.LoadHCTCollection();
            }
            //TKCT.HCTCollection.Sort(new SortHangMauDich());


            DataTable tableTK = NPLNhapTonThucTe.SelectBy_HopDongAndTonHonO(TKCT.IDHopDong, TKCT.NgayDangKy, SoThapPhanNPL).Tables[0];


            foreach (PhanBoToKhaiXuat pbToKhaiXuat in TKCT.PhanBoToKhaiXuatCollection)
            {
                foreach (PhanBoToKhaiNhap pbToKhaiNhap in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                {
                    DataRow rowToKhaiNhap = tableTK.Select("SoToKhai=" + pbToKhaiNhap.SoToKhaiNhap + " and MaLoaiHinh='" + pbToKhaiNhap.MaLoaiHinhNhap + "' and NamDangKy=" + pbToKhaiNhap.NamDangKyNhap + " and MaHaiQuan='" + pbToKhaiNhap.MaHaiQuanNhap + "' and MaNPL='" + pbToKhaiNhap.MaNPL.Trim() + "'")[0];
                    pbToKhaiNhap.LuongTonDau = Convert.ToDouble(rowToKhaiNhap["Ton"]);
                    pbToKhaiNhap.LuongTonCuoi = pbToKhaiNhap.LuongTonDau - pbToKhaiNhap.LuongPhanBo;
                    rowToKhaiNhap["Ton"] = pbToKhaiNhap.LuongTonCuoi;
                }
            }
            InsertUpdateFull(TKCT, tableTK);
        }

        public static bool CheckPhanBoToKhaiXuat(long ID_TKMD)
        {
            string sql = "select id  from t_GC_PhanBoToKhaiXuat where ID_TKMD=@ID_TKMD AND (MaDoanhNghiep lIKE 'XGC%' or MaDoanhNghiep like 'XV%')";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null) return false;
            return true;
        }
        public static bool CheckPhanBoToKhaiChuyenTiepXuat(long ID_TKMD)
        {
            string sql = "select id  from t_GC_PhanBoToKhaiXuat where ID_TKMD=@ID_TKMD AND (MaDoanhNghiep lIKE 'PH%' OR MaDoanhNghiep IN ('XGC18', 'XGC19', 'XVE54'))";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null) return false;
            return true;
        }


    }
}