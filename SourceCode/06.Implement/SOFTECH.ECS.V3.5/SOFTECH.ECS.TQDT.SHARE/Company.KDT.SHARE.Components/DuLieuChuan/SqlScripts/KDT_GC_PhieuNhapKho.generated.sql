-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhieuNhapKho_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Insert]
	@TKMD_ID bigint,
	@NgayNhapKho datetime,
	@SoPhieu numeric(18, 0),
	@SoHopDong nvarchar(30),
	@KhachHang nvarchar(200),
	@NhapTaiKho nvarchar(200),
	@NgoaiTe varchar(20),
	@TyGia numeric(18, 4),
	@LyDo nvarchar(200),
	@NhaCungCap nvarchar(200),
	@ToKhai numeric(12, 0),
	@SoInvoice nvarchar(50),
	@NgayInvoice datetime,
	@SoBL nvarchar(50),
	@SoKien numeric(18, 0),
	@TongVAT numeric(26, 4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_PhieuNhapKho]
(
	[TKMD_ID],
	[NgayNhapKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[NhapTaiKho],
	[NgoaiTe],
	[TyGia],
	[LyDo],
	[NhaCungCap],
	[ToKhai],
	[SoInvoice],
	[NgayInvoice],
	[SoBL],
	[SoKien],
	[TongVAT]
)
VALUES 
(
	@TKMD_ID,
	@NgayNhapKho,
	@SoPhieu,
	@SoHopDong,
	@KhachHang,
	@NhapTaiKho,
	@NgoaiTe,
	@TyGia,
	@LyDo,
	@NhaCungCap,
	@ToKhai,
	@SoInvoice,
	@NgayInvoice,
	@SoBL,
	@SoKien,
	@TongVAT
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayNhapKho datetime,
	@SoPhieu numeric(18, 0),
	@SoHopDong nvarchar(30),
	@KhachHang nvarchar(200),
	@NhapTaiKho nvarchar(200),
	@NgoaiTe varchar(20),
	@TyGia numeric(18, 4),
	@LyDo nvarchar(200),
	@NhaCungCap nvarchar(200),
	@ToKhai numeric(12, 0),
	@SoInvoice nvarchar(50),
	@NgayInvoice datetime,
	@SoBL nvarchar(50),
	@SoKien numeric(18, 0),
	@TongVAT numeric(26, 4)
AS

UPDATE
	[dbo].[t_KDT_GC_PhieuNhapKho]
SET
	[TKMD_ID] = @TKMD_ID,
	[NgayNhapKho] = @NgayNhapKho,
	[SoPhieu] = @SoPhieu,
	[SoHopDong] = @SoHopDong,
	[KhachHang] = @KhachHang,
	[NhapTaiKho] = @NhapTaiKho,
	[NgoaiTe] = @NgoaiTe,
	[TyGia] = @TyGia,
	[LyDo] = @LyDo,
	[NhaCungCap] = @NhaCungCap,
	[ToKhai] = @ToKhai,
	[SoInvoice] = @SoInvoice,
	[NgayInvoice] = @NgayInvoice,
	[SoBL] = @SoBL,
	[SoKien] = @SoKien,
	[TongVAT] = @TongVAT
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayNhapKho datetime,
	@SoPhieu numeric(18, 0),
	@SoHopDong nvarchar(30),
	@KhachHang nvarchar(200),
	@NhapTaiKho nvarchar(200),
	@NgoaiTe varchar(20),
	@TyGia numeric(18, 4),
	@LyDo nvarchar(200),
	@NhaCungCap nvarchar(200),
	@ToKhai numeric(12, 0),
	@SoInvoice nvarchar(50),
	@NgayInvoice datetime,
	@SoBL nvarchar(50),
	@SoKien numeric(18, 0),
	@TongVAT numeric(26, 4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_PhieuNhapKho] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_PhieuNhapKho] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[NgayNhapKho] = @NgayNhapKho,
			[SoPhieu] = @SoPhieu,
			[SoHopDong] = @SoHopDong,
			[KhachHang] = @KhachHang,
			[NhapTaiKho] = @NhapTaiKho,
			[NgoaiTe] = @NgoaiTe,
			[TyGia] = @TyGia,
			[LyDo] = @LyDo,
			[NhaCungCap] = @NhaCungCap,
			[ToKhai] = @ToKhai,
			[SoInvoice] = @SoInvoice,
			[NgayInvoice] = @NgayInvoice,
			[SoBL] = @SoBL,
			[SoKien] = @SoKien,
			[TongVAT] = @TongVAT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_PhieuNhapKho]
		(
			[TKMD_ID],
			[NgayNhapKho],
			[SoPhieu],
			[SoHopDong],
			[KhachHang],
			[NhapTaiKho],
			[NgoaiTe],
			[TyGia],
			[LyDo],
			[NhaCungCap],
			[ToKhai],
			[SoInvoice],
			[NgayInvoice],
			[SoBL],
			[SoKien],
			[TongVAT]
		)
		VALUES 
		(
			@TKMD_ID,
			@NgayNhapKho,
			@SoPhieu,
			@SoHopDong,
			@KhachHang,
			@NhapTaiKho,
			@NgoaiTe,
			@TyGia,
			@LyDo,
			@NhaCungCap,
			@ToKhai,
			@SoInvoice,
			@NgayInvoice,
			@SoBL,
			@SoKien,
			@TongVAT
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_PhieuNhapKho]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_PhieuNhapKho] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayNhapKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[NhapTaiKho],
	[NgoaiTe],
	[TyGia],
	[LyDo],
	[NhaCungCap],
	[ToKhai],
	[SoInvoice],
	[NgayInvoice],
	[SoBL],
	[SoKien],
	[TongVAT]
FROM
	[dbo].[t_KDT_GC_PhieuNhapKho]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[NgayNhapKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[NhapTaiKho],
	[NgoaiTe],
	[TyGia],
	[LyDo],
	[NhaCungCap],
	[ToKhai],
	[SoInvoice],
	[NgayInvoice],
	[SoBL],
	[SoKien],
	[TongVAT]
FROM [dbo].[t_KDT_GC_PhieuNhapKho] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayNhapKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[NhapTaiKho],
	[NgoaiTe],
	[TyGia],
	[LyDo],
	[NhaCungCap],
	[ToKhai],
	[SoInvoice],
	[NgayInvoice],
	[SoBL],
	[SoKien],
	[TongVAT]
FROM
	[dbo].[t_KDT_GC_PhieuNhapKho]	

GO

