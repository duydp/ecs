﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu.WareHouse
{
	public partial class T_KHOKETOAN_DANHSACHKHO : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MAKHO { set; get; }
		public string TENKHO { set; get; }
		public string TAIKHOANKHO { set; get; }
		public string GHICHU { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KHOKETOAN_DANHSACHKHO> ConvertToCollection(IDataReader reader)
		{
			List<T_KHOKETOAN_DANHSACHKHO> collection = new List<T_KHOKETOAN_DANHSACHKHO>();
			while (reader.Read())
			{
				T_KHOKETOAN_DANHSACHKHO entity = new T_KHOKETOAN_DANHSACHKHO();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAKHO"))) entity.MAKHO = reader.GetString(reader.GetOrdinal("MAKHO"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENKHO"))) entity.TENKHO = reader.GetString(reader.GetOrdinal("TENKHO"));
				if (!reader.IsDBNull(reader.GetOrdinal("TAIKHOANKHO"))) entity.TAIKHOANKHO = reader.GetString(reader.GetOrdinal("TAIKHOANKHO"));
				if (!reader.IsDBNull(reader.GetOrdinal("GHICHU"))) entity.GHICHU = reader.GetString(reader.GetOrdinal("GHICHU"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KHOKETOAN_DANHSACHKHO> collection, long id)
        {
            foreach (T_KHOKETOAN_DANHSACHKHO item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KHOKETOAN_DANHSACHKHO VALUES(@MAKHO, @TENKHO, @TAIKHOANKHO, @GHICHU)";
            string update = "UPDATE T_KHOKETOAN_DANHSACHKHO SET MAKHO = @MAKHO, TENKHO = @TENKHO, TAIKHOANKHO = @TAIKHOANKHO, GHICHU = @GHICHU WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_DANHSACHKHO WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TAIKHOANKHO", SqlDbType.NVarChar, "TAIKHOANKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TAIKHOANKHO", SqlDbType.NVarChar, "TAIKHOANKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KHOKETOAN_DANHSACHKHO VALUES(@MAKHO, @TENKHO, @TAIKHOANKHO, @GHICHU)";
            string update = "UPDATE T_KHOKETOAN_DANHSACHKHO SET MAKHO = @MAKHO, TENKHO = @TENKHO, TAIKHOANKHO = @TAIKHOANKHO, GHICHU = @GHICHU WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_DANHSACHKHO WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TAIKHOANKHO", SqlDbType.NVarChar, "TAIKHOANKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TAIKHOANKHO", SqlDbType.NVarChar, "TAIKHOANKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KHOKETOAN_DANHSACHKHO Load(long id)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_DANHSACHKHO_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KHOKETOAN_DANHSACHKHO> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KHOKETOAN_DANHSACHKHO> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KHOKETOAN_DANHSACHKHO> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_DANHSACHKHO_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_DANHSACHKHO_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_DANHSACHKHO_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_DANHSACHKHO_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KHOKETOAN_DANHSACHKHO(string mAKHO, string tENKHO, string tAIKHOANKHO, string gHICHU)
		{
			T_KHOKETOAN_DANHSACHKHO entity = new T_KHOKETOAN_DANHSACHKHO();	
			entity.MAKHO = mAKHO;
			entity.TENKHO = tENKHO;
			entity.TAIKHOANKHO = tAIKHOANKHO;
			entity.GHICHU = gHICHU;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KHOKETOAN_DANHSACHKHO_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TENKHO", SqlDbType.NVarChar, TENKHO);
			db.AddInParameter(dbCommand, "@TAIKHOANKHO", SqlDbType.NVarChar, TAIKHOANKHO);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KHOKETOAN_DANHSACHKHO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_DANHSACHKHO item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KHOKETOAN_DANHSACHKHO(long id, string mAKHO, string tENKHO, string tAIKHOANKHO, string gHICHU)
		{
			T_KHOKETOAN_DANHSACHKHO entity = new T_KHOKETOAN_DANHSACHKHO();			
			entity.ID = id;
			entity.MAKHO = mAKHO;
			entity.TENKHO = tENKHO;
			entity.TAIKHOANKHO = tAIKHOANKHO;
			entity.GHICHU = gHICHU;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KHOKETOAN_DANHSACHKHO_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TENKHO", SqlDbType.NVarChar, TENKHO);
			db.AddInParameter(dbCommand, "@TAIKHOANKHO", SqlDbType.NVarChar, TAIKHOANKHO);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KHOKETOAN_DANHSACHKHO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_DANHSACHKHO item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KHOKETOAN_DANHSACHKHO(long id, string mAKHO, string tENKHO, string tAIKHOANKHO, string gHICHU)
		{
			T_KHOKETOAN_DANHSACHKHO entity = new T_KHOKETOAN_DANHSACHKHO();			
			entity.ID = id;
			entity.MAKHO = mAKHO;
			entity.TENKHO = tENKHO;
			entity.TAIKHOANKHO = tAIKHOANKHO;
			entity.GHICHU = gHICHU;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_DANHSACHKHO_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TENKHO", SqlDbType.NVarChar, TENKHO);
			db.AddInParameter(dbCommand, "@TAIKHOANKHO", SqlDbType.NVarChar, TAIKHOANKHO);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KHOKETOAN_DANHSACHKHO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_DANHSACHKHO item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KHOKETOAN_DANHSACHKHO(long id)
		{
			T_KHOKETOAN_DANHSACHKHO entity = new T_KHOKETOAN_DANHSACHKHO();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_DANHSACHKHO_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_DANHSACHKHO_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KHOKETOAN_DANHSACHKHO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_DANHSACHKHO item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}