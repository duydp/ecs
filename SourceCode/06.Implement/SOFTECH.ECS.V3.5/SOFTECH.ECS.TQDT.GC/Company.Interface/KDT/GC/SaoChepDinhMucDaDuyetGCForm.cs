﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.GC.BLL.GC;

namespace Company.Interface.KDT.GC
{
    public partial class SaoChepDinhMucDaDuyetGCForm : Company.Interface.BaseForm
    {
        public DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
        public DinhMucCollection DMCollectionCopy = new Company.GC.BLL.GC.DinhMucCollection();
        public DinhMucCollection DMCollectionRemove = new Company.GC.BLL.GC.DinhMucCollection();
        public KDT_LenhSanXuat_SP SP = new KDT_LenhSanXuat_SP();
        public long HopDong_ID;
        public SaoChepDinhMucDaDuyetGCForm()
        {
            InitializeComponent();
        }

        private void SaoChepDinhMucDaDuyetGCForm_Load(object sender, EventArgs e)
        {
            this.Text = "SAO CHÉP ĐỊNH MỨC CHO MÃ SẢN PHẨM : " + SP.MaSanPham.ToString();
            BindData();
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = dm.SelectCollectionDynamic("HopDong_ID =" + HopDong_ID + " AND MaSanPham NOT IN ('" + SP.MaSanPham.ToString() + "')", "");
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAdd = true;
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    isAdd = true;
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    DinhMuc dinhmuc = (DinhMuc)item.DataRow;
                    //KIỂM TRA CHỌN TRÙNG NHIỀU MÃ NPL KHI CHỌN
                    foreach (DinhMuc it in DMCollectionCopy)
                    {
                        if (it.MaNguyenPhuLieu == dinhmuc.MaNguyenPhuLieu)
                        {
                            if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC CHỌN TRÊN LƯỚI .\nBẠN CÓ MUỐN SAO CHÉP THAY THẾ BẰNG ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                            {
                                it.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                                it.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                                isAdd = false;
                            }
                            else if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC CHỌN TRÊN LƯỚI .\nBẠN CÓ GHỘP ĐỊNH MỨC CỦA MÃ NPL NÀY KHÔNG ?", true) == "Yes")
                            {
                                it.DinhMucSuDung = it.DinhMucSuDung + dinhmuc.DinhMucSuDung;
                                it.TyLeHaoHut = it.TyLeHaoHut + dinhmuc.TyLeHaoHut;
                                isAdd = false;
                            }
                            else
                            {
                                isAdd = false;
                            }
                        }
                    }
                    if (isAdd)
                        DMCollectionCopy.Add(dinhmuc);
                }
                DinhMucCollection DMSPCollection = new DinhMucCollection();
                DMSPCollection = dm.SelectCollectionDynamic("HopDong_ID =" + HopDong_ID + " AND MaSanPham ='" + SP.MaSanPham.ToString() + "' AND LenhSanXuat_ID =" + SP.LenhSanXuat_ID + "", "");
                if (DMCollectionCopy.Count > 0)
                {
                    //KIỂM TRA TRÙNG MÃ NPL ĐÃ NHẬP ĐỊNH MỨC TRƯỚC ĐÓ
                    foreach (DinhMuc ite in DMSPCollection)
                    {
                        foreach (DinhMuc dinhmuc in DMCollectionCopy)
                        {
                            if (ite.MaSanPham == SP.MaSanPham && ite.MaNguyenPhuLieu == dinhmuc.MaNguyenPhuLieu)
                            {
                                if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC NHẬP ĐỊNH MỨC .\nBẠN CÓ MUỐN SAO CHÉP THAY THẾ BẰNG ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                                {
                                    ite.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                                    ite.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                                    DMCollectionRemove.Add(dinhmuc);
                                }
                                else if ((ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC NHẬP ĐỊNH MỨC .\nBẠN CÓ MUỐN GHỘP ĐỊNH MỨC CỦA MÃ NPL KHÔNG ?", true) == "Yes"))
                                {
                                    ite.DinhMucSuDung = ite.DinhMucSuDung + dinhmuc.DinhMucSuDung;
                                    ite.TyLeHaoHut = ite.TyLeHaoHut + dinhmuc.TyLeHaoHut;
                                    DMCollectionRemove.Add(dinhmuc);
                                }
                            }
                        }
                    }
                    foreach (DinhMuc item in DMCollectionRemove)
                    {
                        DMCollectionCopy.Remove(item);
                    }
                }
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    Company.GC.BLL.GC.DinhMuc DM = (Company.GC.BLL.GC.DinhMuc)e.Row.DataRow;
                    if (DM.TenNPL != null && DM.TenNPL.Trim().Length == 0)
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                        NPL.HopDong_ID = DM.HopDong_ID;
                        NPL.Ma = DM.MaNguyenPhuLieu;
                        NPL.Load();
                        DM.TenNPL = NPL.Ten;
                    }

                    if (DM.TenSanPham == null || DM.TenSanPham.Trim().Length == 0)
                    {
                        Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
                        SP.HopDong_ID = DM.HopDong_ID;
                        SP.Ma = DM.MaSanPham;
                        SP.Load();
                        DM.TenSanPham = SP.Ten;
                    }
                    Company.GC.BLL.KDT.GC.HopDong hd = new Company.GC.BLL.KDT.GC.HopDong();
                    hd.ID = (long)Convert.ToInt64(e.Row.Cells["HopDong_ID"].Value);
                    hd = Company.GC.BLL.KDT.GC.HopDong.Load(hd.ID);
                    e.Row.Cells["HopDong_ID"].Text = hd.SoHopDong;
                    try
                    {
                        KDT_LenhSanXuat lenhhXS = new KDT_LenhSanXuat();
                        lenhhXS = KDT_LenhSanXuat.Load(Convert.ToInt64(e.Row.Cells["LenhSanXuat_ID"].Value));
                        if (lenhhXS != null)
                            e.Row.Cells["LenhSanXuat_ID"].Text = lenhhXS.SoLenhSanXuat;
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
