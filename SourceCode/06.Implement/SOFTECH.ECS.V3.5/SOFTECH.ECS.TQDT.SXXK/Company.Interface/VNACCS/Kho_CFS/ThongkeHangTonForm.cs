﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using System.Reflection;
using Company.Interface.Report;
using Janus.Windows.GridEX;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using System.IO;
using System.Diagnostics;
using Company.Controls.CustomValidation;
namespace Company.Interface.GC
{
    public enum EWorkType
    {
        PhanBo,
        XoaPhanBo
    }

    public partial class ThongkeHangTonForm : BaseForm
    {
        private NPLNhapTonThucTe NPLTon = new NPLNhapTonThucTe();
        public IList<NPLNhapTonThucTe> Collection = new List<NPLNhapTonThucTe>();
        //ToKhaiMauDichCollection TKMDChuaPhanBoCollection = new ToKhaiMauDichCollection();
        ToKhaiMauDichCollection TKMDDaPhanBoCollection = new ToKhaiMauDichCollection();
        DataTable dtDanhSachToKhaiChuaPhanBo;
        DataTable dtDanhSachToKhaiDaPhanBo;
        public HopDong HD;
        public double ValueOld = 0;
        public bool check = true;

        public ThongkeHangTonForm()
        {
            InitializeComponent();

            this.uiTabSanPham.SelectedTabChanged -= new Janus.Windows.UI.Tab.TabEventHandler(this.uiTabSanPham_SelectedTabChanged);

            InitialBackgrounWorker();
        }

        private void ThongkeHangTonForm_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                //txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
                this.uiTabSanPham.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.uiTabSanPham_SelectedTabChanged);
                ccFrom.Value = new DateTime(DateTime.Today.Year, 1, 1);

                dgList.Tables[0].Columns["LuongTon"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                if (dgList.Tables[0].Columns.Contains("TongTonTheoTK"))
                    dgList.Tables[0].Columns["TongTonTheoTK"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                if (dgList.Tables[0].Columns.Contains("ChenhLech"))
                    dgList.Tables[0].Columns["ChenhLech"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

                dgListNPLTK.Tables[0].Columns["Luong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                dgListNPLTK.Tables[0].Columns["Ton"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

                dgListPhanBo.Tables[0].Columns["DinhMucChung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
                dgListPhanBo.Tables[0].Columns["SoLuongXuat"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                dgListPhanBo.Tables[0].Columns["NhuCau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                dgListPhanBo.Tables[0].Columns["LuongTonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                dgListPhanBo.Tables[0].Columns["LuongPhanBo"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                dgListPhanBo.Tables[0].Columns["LuongCungUng"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                dgListPhanBo.Tables[0].Columns["LuongTonCuoi"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

                //Quan ly NPL ton
                BindDataNPLTonThucTe();

                //Thong ke luong ton theo to khai
                BindDataToKhaiNPLTon();
                dgListNPLTK.RootTable.GroupHeaderTotals[1].TotalFormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                dgListNPLTK.RootTable.GroupHeaderTotals[2].TotalFormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { Cursor = Cursors.Default; }
        }
        private void BindDataToKhaiNPLTon()
        {
            try
            {
                //Kiem tra xoa loi to khai co so to khai = 0 trong ton
                NPLNhapTonThucTe.FixSoToKhaiZeroInTonThucTe(this.HD.ID);

                Collection = NPLNhapTonThucTe.SelectCollectionDynamic("a.MaHaiQuan='" + HD.MaHaiQuan + "' and a.MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND a.ID_HopDong = " + this.HD.ID, "");
                dgListNPLTK.DataSource = Collection;
                dgListNPLTK.Refetch();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void dgListNPLTK_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["DVT"].Text = DonViTinh_GetName(e.Row.Cells["DVT"].Value);
            }
            catch { }
            
        }
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindDataNPLTonThucTe()
        {
            dgList.DataSource = NPLNhapTonThucTe.SelectNPLTonThucTeByHopDong(HD.ID);
            dgList.Refetch();
        }
        private void saveLuongTon()
        {
            try
            {
                NPLNhapTonThucTe.InsertUpdateCollection(Collection);
                try
                {
                    dgListNPLTK.DataSource = Collection;
                    dgListNPLTK.Refetch();
                }
                catch { dgListNPLTK.Refresh(); }
                showMsg("MSG_2702001");
                //ShowMessage("Cập nhật thành công.", false);
            }
            catch (Exception ex)
            {
                //ShowMessage(ex.Message, false);
                showMsg("MSG_2702004", ex.Message);
            }
        }

        private void searchToKhaiTonThucTe()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = " ID_HopDong  = " + this.HD.ID;
            string temp = "";
            if (txtSoTiepNhan.TextLength > 0)
            {
                List<int> soTk = new List<int>();
                if (txtSoTiepNhan.TextLength > 5)
                {
                    soTk = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKByVNACCS(Convert.ToDecimal(txtSoTiepNhan.Value));
                    if (soTk.Count > 0)
                    {
                        string InSTK = "( ";
                        foreach (int i in soTk)
                        {
                            InSTK += i + ",";
                        }
                        InSTK = InSTK.Substring(0, InSTK.Length - 1) + " )";
                        where += " AND a.SoToKhai in " + InSTK;
                    }
                    else
                        where += " AND a.SoToKhai = " + txtSoTiepNhan.Value;
                }
                else
                    where += " AND a.SoToKhai = " + txtSoTiepNhan.Value;
            }
            //if (txtNamTiepNhan.TextLength > 0)
            //{
            //    where += " AND a.NamDangKy = " + txtNamTiepNhan.Value;
            //}
            where += "AND a.NgayDangKy BETWEEN '" + ccFrom.Value.ToString("MM/dd/yyyy 00:00:00") + "' AND '" + ccTo.Value.ToString("MM/dd/yyyy 23:59:59") + "'";
            if (txtMaNPL.Text.Length > 0)
            {
                where += " AND a.MaNPL LIKE '%" + txtMaNPL.Text + "%'";
            }


            //where += " AND Cast(a.SoToKhai as varchar(10))+ a.MaLoaiHinh +  Cast(a.NamDangKy as varchar(4))+a.MaHaiQuan IN " +
            //"(SELECT Cast(SoToKhai as varchar(10))+ MaLoaiHinh +  Cast(NamDangKy as varchar(4))+MaHaiQuan FROM dbo.t_KDT_ToKhaiMauDich WHERE MaLoaiHinh LIKE 'N%'" + temp + ")";

            // Thực hiện tìm kiếm.                        
            Collection = NPLNhapTonThucTe.SelectCollectionDynamic(where, "");
            dgListNPLTK.DataSource = Collection;
            dgListNPLTK.Refetch();
        }
        private void TimToKhaiTon_Click(object sender, EventArgs e)
        {
            searchToKhaiTonThucTe();
        }

        private void inDanhSachTon_Click_1(object sender, EventArgs e)
        {
            gridEXPrintDocument1.Print();
        }

        private void XemPhanBo_Click(object sender, EventArgs e)
        {
            if (cbbToKhai.Text == "")
            {
                showMsg("MSG_WRN29");
                //ShowMessage("Bạn chưa chọn tờ khai xuất.", false);
                return;
            }

            long id = 0;
            try
            {
                id = Convert.ToInt64(cbbToKhai.Text);
            }
            catch
            {
                ShowMessage("Bạn hãy chọn tờ khai.", false);
                return;
            }
            string maLoaiHinh = Convert.ToString(((DataRowView)cbbToKhai.SelectedItem).Row["MaLoaiHinh"]);

            XemPhanBo(id, maLoaiHinh);
        }

        private void XemPhanBo(long id, string maLoaiHinh)
        {
            if (maLoaiHinh.StartsWith("E")) maLoaiHinh = "XV" + maLoaiHinh;
            if (maLoaiHinh.StartsWith("B")) maLoaiHinh = "XV" + maLoaiHinh;
            if ((maLoaiHinh.Contains("XGC") || maLoaiHinh.Contains("XV")) && maLoaiHinh.Trim() != "XVE54" && maLoaiHinh.Trim() != "XGC18" && maLoaiHinh.Trim() != "XGC19" && maLoaiHinh.Trim() != "XGC20")
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();

                try
                {
                    TKMD.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKMD.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (TKMD.TrangThaiPhanBo != 1)
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKMD.LoadHMDCollection();
                if (cbSanPham.Text.Trim() == "")
                    dgListPhanBo.DataSource = PhanBoToKhaiNhap.SelectCollectionPhanBoToKhaiXuatOfTKMD(TKMD.ID, GlobalSettings.SoThapPhan.LuongNPL);
                else
                    dgListPhanBo.DataSource = PhanBoToKhaiNhap.SelectCollectionPhanBoToKhaiXuatOfTKMDAndMaSP(TKMD.ID, cbSanPham.SelectedItem.Value.ToString(), GlobalSettings.SoThapPhan.LuongNPL);
                dgListPhanBo.Refetch();
                cbSanPham.Items.Clear();
                cbSanPham.Items.Add("", "");
                foreach (HangMauDich HMD in TKMD.HMDCollection)
                {
                    cbSanPham.Items.Add(HMD.TenHang + " / " + HMD.MaPhu, HMD.MaPhu);
                }
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();

                try
                {
                    TKCT.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                if (TKCT == null)
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (!TKCT.CheckPhanBo())
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKCT.LoadHCTCollection();
                if (cbSanPham.Text.Trim() == "")
                    dgListPhanBo.DataSource = PhanBoToKhaiNhap.SelectCollectionPhanBoToKhaiXuatOfTKCT(TKCT.ID, GlobalSettings.SoThapPhan.LuongNPL);
                else
                    dgListPhanBo.DataSource = PhanBoToKhaiNhap.SelectCollectionPhanBoToKhaiXuatOfTKMDAndMaSPTKCT(TKCT.ID, cbSanPham.SelectedItem.Value.ToString(), GlobalSettings.SoThapPhan.LuongNPL);
                dgListPhanBo.Refetch();
                cbSanPham.Items.Clear();
                cbSanPham.Items.Add("", "");
                foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                {
                    cbSanPham.Items.Add(HCT.TenHang + " / " + HCT.MaHang, HCT.MaHang);
                }

            }
        }

        private void dgListTKChuaPhanBo_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string loaiHinh = (e.Row.Cells["MaLoaiHinh"].Value.ToString());
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
            }
        }
        private void BindDataToKhaiChuaPhanBo()
        {
            //dgListTKChuaPhanBo.DataSource = ToKhaiMauDich.SelectToKhaiChuaPhanBo(HD.ID);
            //dgListTKChuaPhanBo.Refetch();
            DataTable data = ToKhaiMauDich.SelectToKhaiChuaPhanBo(HD.ID);
            SeToKhaiChuaPhanBo(data);
        }
        private void BindDataToKhaiDaPhanBo()
        {
            //TKMDDaPhanBoCollection =
            // if (dtDanhSachToKhaiDaPhanBo == null || dtDanhSachToKhaiDaPhanBo)
            dtDanhSachToKhaiDaPhanBo = ToKhaiMauDich.SelectToKhaiDaPhanBo(HD.ID);
            //dgToKhaiDaPhanBo.DataSource = dtDanhSachToKhaiDaPhanBo;
            //dgToKhaiDaPhanBo.Refetch();
            SeToKhaiDaPhanBo(dtDanhSachToKhaiDaPhanBo);
        }
        private void uiTabSanPham_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {
            switch (e.Page.Key)
            {
                case "tpQLNPLTon":
                    BindDataNPLTonThucTe();
                    break;
                case "tpPhanBoToKhaiXuat":
                    BindDataToKhaiChuaPhanBo();
                    break;
                case "tpDanhSachToKhaiDaPhanBo":
                    BindDataToKhaiDaPhanBo();
                    break;
                case "tpTheoDoiPhanBo":
                    BindToKhaiDaPhanBoToCombobox();
                    break;


            }
        }

        private void BindToKhaiDaPhanBoToCombobox()
        {

            cbbToKhai.DisplayMember = "ID";
            cbbToKhai.ValueMember = "ID";
            cbbToKhai.DataSource = ToKhaiMauDich.SelectToKhaiDaPhanBo(HD.ID);
        }

        private void PhanBo_Click(object sender, EventArgs e)
        {
            objDoWork = EWorkType.PhanBo.ToString();
            btnBeginProcess_Click(sender, e);
        }

        private void PhanBo(BackgroundWorker bgw, DoWorkEventArgs e)
        {
            if (bgw != null && bgw.CancellationPending)
                return;

            if (dgListTKChuaPhanBo.GetCheckedRows().Length <= 0)
            {
                showMsg("MSG_0203083");
                bgw.CancelAsync();
                return;
            }

            //TODO: Background - TotalProgressValue. Hungtq, 16/06/2014
            totalProgressValue = 4 * dgListTKChuaPhanBo.GetCheckedRows().Length; // 4 (buoc thuc hien) * Tong so to khai duoc CHECK
            totalProgressValue += 1;//Buoc sau cung, thuc hien load du lieu
            newProgressValue = 0;
            int cnt = 0;

            foreach (GridEXRow row in dgListTKChuaPhanBo.GetCheckedRows())
            {
                string maLoaiHinh = row.Cells["MaLoaiHinh"].Value.ToString();
                if (maLoaiHinh.StartsWith("E")) maLoaiHinh = "XV" + maLoaiHinh;
                if ((maLoaiHinh.Contains("XGC") || maLoaiHinh.Contains("XV"))
                    && maLoaiHinh.Trim() != "XGC18" && maLoaiHinh.Trim() != "XGC19" && maLoaiHinh.Trim() != "XGC20"
                    && maLoaiHinh.Trim() != "XVE54")
                {
                    if (bgw != null && bgw.CancellationPending)
                        return;

                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.ID = Convert.ToInt64(row.Cells["ID"].Value);
                    TKMD.Load();
                    TKMD.LoadHMDCollection();

                    try
                    {
                        if (TKMD.LoaiHangHoa == "S")
                        {
                            cnt = 0;
                            do
                            {
                                PhanBoToKhaiXuat.PhanBoChoToKhaiXuatGC(TKMD, GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.DinhMuc, ref cnt);
                                //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 16/06/2014
                                newProgressValue += cnt;
                                percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                                percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                                SetProgressBar_PhanBo(percentComplete);
                            } while (cnt < 4);
                        }
                        else
                        {
                            SelectNPLTaiXuatForm f = new SelectNPLTaiXuatForm();
                            f.TKMD = TKMD;
                            TKMD.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
                            foreach (HangMauDich HMD in TKMD.HMDCollection)
                            {
                                PhanBoToKhaiXuat pbToKhaiTaiXuat = new PhanBoToKhaiXuat();
                                pbToKhaiTaiXuat.ID_TKMD = TKMD.ID;
                                pbToKhaiTaiXuat.MaDoanhNghiep = TKMD.MaLoaiHinh;
                                pbToKhaiTaiXuat.MaSP = HMD.MaPhu;
                                pbToKhaiTaiXuat.SoLuongXuat = HMD.SoLuong;
                                TKMD.PhanBoToKhaiXuatCollection.Add(pbToKhaiTaiXuat);
                            }
                            f.pbToKhaiXuatCollection = TKMD.PhanBoToKhaiXuatCollection;
                            f.ShowDialog();
                            if (!f.isPhanBo)
                            {
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //showMsg("MSG_WRN28", TKMD.SoToKhai);
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        ShowMessage("Có lỗi : " + ex.Message + " trong tờ khai " + TKMD.SoToKhai, false);
                        bgw.CancelAsync();
                        return;
                    }
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.ID = Convert.ToInt64(row.Cells["ID"].Value);
                    TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                    TKCT.LoadHCTCollection();

                    try
                    {
                        if (TKCT.MaLoaiHinh.Trim() == "PHSPX" || TKCT.MaLoaiHinh.Trim() == "XGC19" )
                        {
                            cnt = 0;
                            do
                            {
                                PhanBoToKhaiXuat.PhanBoChoToKhaiChuyenTiep(TKCT, GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.DinhMuc, ref cnt);
                                //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 16/06/2014
                                newProgressValue += cnt;
                                percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                                percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                                SetProgressBar_PhanBo(percentComplete);
                            } while (cnt < 4);
                        }
                        else
                        {
                            SelectNPLTaiXuatTKCTForm f = new SelectNPLTaiXuatTKCTForm();
                            f.TKCT = TKCT;
                            TKCT.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
                            foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                            {
                                PhanBoToKhaiXuat pbToKhaiTaiXuat = new PhanBoToKhaiXuat();
                                pbToKhaiTaiXuat.ID_TKMD = TKCT.ID;
                                pbToKhaiTaiXuat.MaDoanhNghiep = TKCT.MaLoaiHinh;
                                pbToKhaiTaiXuat.MaSP = HCT.MaHang;
                                pbToKhaiTaiXuat.SoLuongXuat = HCT.SoLuong;
                                TKCT.PhanBoToKhaiXuatCollection.Add(pbToKhaiTaiXuat);
                            }
                            f.pbToKhaiXuatCollection = TKCT.PhanBoToKhaiXuatCollection;
                            f.ShowDialog();
                            if (!f.isPhanBo)
                            {
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //showMsg("MSG_WRN28", TKCT.SoToKhai);
                        ShowMessage("Có lỗi : " + ex.Message + " trong tờ khai " + TKCT.SoToKhai, false);
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        bgw.CancelAsync();
                        return;
                    }
                }
            }

            BindDataToKhaiChuaPhanBo();
            //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 16/06/2014
            newProgressValue += 1;
            percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
            percentComplete = (percentComplete > 100 ? 100 : percentComplete);
            SetProgressBar_PhanBo(percentComplete);

            //showMsg("MSG_240210");
            ShowMessage("Đã thực hiện phân bổ xong.", false);
        }

        private void dgListSPTON_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void dgListTKChuaPhanBo_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void dgListPhanBo_RecordUpdated(object sender, EventArgs e)
        {

        }

        private void dgListPhanBo_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                long TKMD_IDPhanBo = Convert.ToInt64(e.Row.Cells["TKXuat_ID"].Value);
                PhanBoToKhaiXuat pbTKXuat = PhanBoToKhaiXuat.Load(TKMD_IDPhanBo);
                Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
                SP.HopDong_ID = this.HD.ID;
                SP.Ma = pbTKXuat.MaSP;
                SP.Load();
                e.Row.Cells["TenSP"].Text = SP.Ten;
                e.Row.Cells["MaSP"].Text = pbTKXuat.MaSP;
                e.Row.Cells["SoLuongXuat"].Text = pbTKXuat.SoLuongXuat + "";
                e.Row.Cells["NhuCau"].Text = pbTKXuat.SoLuongXuat * Convert.ToDecimal(e.Row.Cells["DinhMucChung"].Text) + "";
                e.Row.Cells["TKXuat_ID"].Text = SP.Ten;
            }
        }

        private void uiButton9_Click(object sender, EventArgs e)
        {

        }

        private void dgListPhanBo_EditingCell(object sender, EditingCellEventArgs e)
        {
            ValueOld = Convert.ToDouble(e.Value);
        }

        private void cmdXemBangNPLCungUng_Click(object sender, EventArgs e)
        {
            long id = 0;
            try
            {
                id = Convert.ToInt64(cbbToKhai.Text);
            }
            catch
            {
                ShowMessage("Bạn hãy chọn tờ khai.", false);
                return;
            }
            string maLoaiHinh = Convert.ToString(((DataRowView)cbbToKhai.SelectedItem).Row["MaLoaiHinh"]);

            XemBangNPLCungUng(id, maLoaiHinh);
        }

        private void cmdViewBangPhanBoNPL_Click(object sender, EventArgs e)
        {
            long id = 0;
            try
            {
                id = Convert.ToInt64(cbbToKhai.Text);
            }
            catch
            {
                ShowMessage("Bạn hãy chọn tờ khai.", false);
                return;
            }
            string maLoaiHinh = Convert.ToString(((DataRowView)cbbToKhai.SelectedItem).Row["MaLoaiHinh"]);

            XemBangPhanBoNPL(id, maLoaiHinh);
        }

        private void XemBangPhanBoNPL(long id, string maLoaiHinh)
        {
            if (maLoaiHinh.Contains("E") && !maLoaiHinh.Contains("XV")) maLoaiHinh = "XV" + maLoaiHinh;
            if (maLoaiHinh.Contains("B") && !maLoaiHinh.Contains("XV")) maLoaiHinh = "XV" + maLoaiHinh;
            if ((maLoaiHinh.Contains("XGC") || maLoaiHinh.Contains("XV")) && maLoaiHinh.Trim() != "XVE54" && maLoaiHinh.Trim() != "XGC18" && maLoaiHinh.Trim() != "XGC19" && maLoaiHinh.Trim() != "XGC20")
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();

                try
                {
                    TKMD.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKMD.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (TKMD.TrangThaiPhanBo != 1)
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKMD.LoadHMDCollection();
                ViewPhanBoForm f = new ViewPhanBoForm();


                f.TKMD = TKMD;
                f.HD = this.HD;
                f.Show();
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();

                try
                {
                    TKCT.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);

                if (TKCT == null)
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (!TKCT.CheckPhanBo())
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKCT.LoadHCTCollection();
                ViewPhanBoTKCTForm f = new ViewPhanBoTKCTForm();
                f.TKCT = TKCT;
                f.HD = this.HD;
                f.Show();
            }
        }

        private void btnDeletePhanBo_Click(object sender, EventArgs e)
        {
            objDoWork = EWorkType.XoaPhanBo.ToString();
            btnBeginProcess_Click(sender, e);
        }

        private void XoaPhanBo(BackgroundWorker bgw, DoWorkEventArgs e)
        {
            if (bgw != null && bgw.CancellationPending)
                return;

            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa phân bổ tờ khai này ko?", true) == "Yes")
            {
                GridEXRow row = dgToKhaiDaPhanBo.GetRow();

                long id = Convert.ToInt64(row.Cells["ID"].Value);
                string maLoaiHinh = row.Cells["MaLoaiHinh"].Value.ToString();
                if (maLoaiHinh.StartsWith("E") || maLoaiHinh.StartsWith("B")) maLoaiHinh = "XV" + maLoaiHinh;
                if ((maLoaiHinh.Contains("XGC") || maLoaiHinh.Contains("XV")) && maLoaiHinh.Trim() != "XGC18" && maLoaiHinh.Trim() != "XGC19" && maLoaiHinh.Trim() != "XGC20" && maLoaiHinh.Trim() != "XVE54")
                {
                    //TODO: Background - TotalProgressValue. Hungtq, 16/06/2014
                    totalProgressValue = 9;

                    try
                    {
                        if (bgw != null && bgw.CancellationPending)
                            return;
                        ToKhaiMauDich TKMD = new ToKhaiMauDich();
                        TKMD.ID = id;
                        TKMD.Load();
                        //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 16/06/2014
                        newProgressValue += 1;
                        percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                        percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                        SetProgressBar_XoaPhanBo(percentComplete);

                        if (bgw != null && bgw.CancellationPending)
                            return;
                        DataTable dtNPLToKhaiNhap = TKMD.GetDanhSachToKhaiNhapPhanBo();
                        //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 16/06/2014
                        newProgressValue += 1;
                        percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                        percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                        SetProgressBar_XoaPhanBo(percentComplete);

                        do
                        {
                            if (bgw != null && bgw.CancellationPending)
                                return;
                            TKMD.DeletePhanBoVaFixData(dtNPLToKhaiNhap, ref newProgressValue); //Ham nay co 6 buoc thuc hien
                            //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 16/06/2014
                            percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                            percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                            SetProgressBar_XoaPhanBo(percentComplete);
                        } while (newProgressValue < 8);
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex); SetLogText(ex.Message);
                        throw ex;
                    }

                    if (bgw != null && bgw.CancellationPending)
                        return;
                    BindDataToKhaiDaPhanBo();
                    //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 16/06/2014
                    newProgressValue += 1;
                    percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                    percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                    SetProgressBar_XoaPhanBo(percentComplete);

                    ShowMessage("Xóa phân bổ và chỉnh dữ liệu phân bổ thành công", false);
                }
                else
                {
                    //TODO: Background - TotalProgressValue. Hungtq, 16/06/2014
                    totalProgressValue = 7;

                    try
                    {
                        if (bgw != null && bgw.CancellationPending)
                            return;
                        ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                        TKCT.ID = id;
                        TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                        //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 16/06/2014
                        newProgressValue += 1;
                        percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                        percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                        SetProgressBar_XoaPhanBo(percentComplete);

                        if (bgw != null && bgw.CancellationPending)
                            return;
                        do
                        {
                            TKCT.DeletePhanBo(ref newProgressValue);
                            //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 16/06/2014
                            newProgressValue += 1;
                            percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                            percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                            SetProgressBar_XoaPhanBo(percentComplete);
                        } while (newProgressValue < 7);
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex); SetLogText(ex.Message);
                        throw ex;
                    }

                    if (bgw != null && bgw.CancellationPending)
                        return;
                    BindDataToKhaiDaPhanBo();
                    //TODO: Background - NewProgressValue, PercentComplete. Hungtq, 16/06/2014
                    newProgressValue += 1;
                    percentComplete = (int)(newProgressValue * 100 / totalProgressValue);
                    percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                    SetProgressBar_XoaPhanBo(percentComplete);

                    //showMsg("MSG_DEL02");
                    ShowMessage("Xóa phân bổ và chỉnh dữ liệu phân bổ thành công", false);
                }
            }
        }

        #region Background Worker PhanBo - XoaPhanBo

        private BackgroundWorker bgwProgressDaPhanBo = new BackgroundWorker();
        private int totalProgressValue = 0, newProgressValue = 0, percentComplete = 0;
        private System.Windows.Forms.Timer timeElapsed = new System.Windows.Forms.Timer();
        private double secondElapsed = 0;
        private string secondElapsedString = string.Empty;
        private string objDoWork = string.Empty;
        private System.Windows.Forms.ToolTip toolTipProgressBar = new ToolTip();

        //ProgressBar
        public void ResetProgressBar_XoaPhanBo(int minPercent, int maxPercent)
        {
            if (progressBarDaPhanBo == null)
                return;

            MethodInvoker mi = new MethodInvoker(() => progressBarDaPhanBo.Minimum = minPercent);
            if (progressBarDaPhanBo.InvokeRequired)
            {
                progressBarDaPhanBo.Invoke(mi);
            }
            else
            {
                mi.Invoke();
            }

            mi = new MethodInvoker(() => progressBarDaPhanBo.Maximum = maxPercent);
            if (progressBarDaPhanBo.InvokeRequired)
            {
                progressBarDaPhanBo.Invoke(mi);
            }
            else
            {
                mi.Invoke();
            }

            SetProgressBar_XoaPhanBo(0);
        }
        public int GetProcessBarMaxValue_XoaPhanBo()
        {
            return progressBarDaPhanBo.Maximum;
        }
        public void SetProgressBar_XoaPhanBo(int percent)
        {
            if (progressBarDaPhanBo == null)
                return;

            MethodInvoker mi = new MethodInvoker(() => progressBarDaPhanBo.Value = percent);
            if (progressBarDaPhanBo.InvokeRequired)
            {
                progressBarDaPhanBo.Invoke(mi);
            }
            else
            {
                mi.Invoke();
            }

        }
        private void progressBar_MouseMove_XoaPhanBo(object sender, MouseEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                toolTipProgressBar.SetToolTip(progressBarDaPhanBo, string.Format("{0} %", percentComplete.ToString("#0")));
            });
        }

        public void ResetProgressBar_PhanBo(int minPercent, int maxPercent)
        {
            if (progressBarPhanBo == null)
                return;

            MethodInvoker mi = new MethodInvoker(() => progressBarPhanBo.Minimum = minPercent);
            if (progressBarPhanBo.InvokeRequired)
            {
                progressBarPhanBo.Invoke(mi);
            }
            else
            {
                mi.Invoke();
            }

            mi = new MethodInvoker(() => progressBarPhanBo.Maximum = maxPercent);
            if (progressBarPhanBo.InvokeRequired)
            {
                progressBarPhanBo.Invoke(mi);
            }
            else
            {
                mi.Invoke();
            }

            SetProgressBar_PhanBo(0);
        }
        public int GetProcessBarMaxValue_PhanBo()
        {
            return progressBarPhanBo.Maximum;
        }
        public void SetProgressBar_PhanBo(int percent)
        {
            if (progressBarPhanBo == null)
                return;

            MethodInvoker mi = new MethodInvoker(() => progressBarPhanBo.Value = percent);
            if (progressBarPhanBo.InvokeRequired)
            {
                progressBarPhanBo.Invoke(mi);
            }
            else
            {
                mi.Invoke();
            }

        }
        private void progressBar_MouseMove_PhanBo(object sender, MouseEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                toolTipProgressBar.SetToolTip(progressBarPhanBo, string.Format("{0} %", percentComplete.ToString("#0")));
            });
        }

        //Grid
        delegate void SetToKhaiDaPhanBoCallback(DataTable data);
        private void SeToKhaiDaPhanBo(DataTable data)
        {
            if (dgToKhaiDaPhanBo.InvokeRequired)
            {
                SetToKhaiDaPhanBoCallback d = new SetToKhaiDaPhanBoCallback(SeToKhaiDaPhanBo);
                Invoke(d, new object[] { data });
            }
            else
            {
                dgToKhaiDaPhanBo.DataSource = data;
                dgToKhaiDaPhanBo.Refetch();
            }
        }

        delegate void SetToKhaiChuaPhanBoCallback(DataTable data);
        private void SeToKhaiChuaPhanBo(DataTable data)
        {
            if (dgListTKChuaPhanBo.InvokeRequired)
            {
                SetToKhaiChuaPhanBoCallback d = new SetToKhaiChuaPhanBoCallback(SeToKhaiChuaPhanBo);
                Invoke(d, new object[] { data });
            }
            else
            {
                dgListTKChuaPhanBo.DataSource = data;
                dgListTKChuaPhanBo.Refetch();
            }
        }

        //Text
        delegate void SetLogTextCallback(string text);
        private void SetLogText(string text)
        {
            //if (txtLog.InvokeRequired)
            //{
            //    SetLogTextCallback d = new SetLogTextCallback(SetLogText);
            //    Invoke(d, new object[] { text });
            //}
            //else
            //{
            //    txtLog.AppendText("\r\n" + System.DateTime.Now + ":\r\n");
            //    txtLog.AppendText(text + "\r\n");
            //}
        }

        private void InitialBackgrounWorker()
        {
            newProgressValue = percentComplete = totalProgressValue = 0;
            ResetProgressBar_XoaPhanBo(0, 100);

            bgwProgressDaPhanBo = new BackgroundWorker();
            bgwProgressDaPhanBo.WorkerReportsProgress = true;
            bgwProgressDaPhanBo.WorkerSupportsCancellation = true;
            bgwProgressDaPhanBo.DoWork += new DoWorkEventHandler(bgwProgress_DoWork);
            bgwProgressDaPhanBo.ProgressChanged += new ProgressChangedEventHandler(bgwProgress_ProgressChanged);
            bgwProgressDaPhanBo.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwProgress_RunWorkerCompleted);

            //btnCancel.Click += btnCancel_Click;
            progressBarDaPhanBo.MouseMove += progressBar_MouseMove_XoaPhanBo;
            progressBarPhanBo.MouseMove += progressBar_MouseMove_PhanBo;
        }

        void timeElapsed_Tick(object sender, EventArgs e)
        {
            try
            {
                secondElapsed += 1;

                secondElapsedString = string.Format("{0}:{1}:{2}", TimeSpan.FromSeconds(secondElapsed).Hours.ToString("00"), TimeSpan.FromSeconds(secondElapsed).Minutes.ToString("00"), TimeSpan.FromSeconds(secondElapsed).Seconds.ToString("00"));
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex); SetLogText(ex.Message);
            }
        }

        private void btnBeginProcess_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.bgwProgressDaPhanBo.IsBusy)
                {
                    btnDeletePhanBo.Enabled = false;
                    contextMenuStripDaPhanBo.Enabled = false;
                    btnPhanBo.Enabled = false;
                    contextMenuStripPhanBo.Enabled = false;

                    //btnCancel.Enabled = true;

                    bgwProgressDaPhanBo.CancelAsync();
                }
                else
                {
                    newProgressValue = percentComplete = totalProgressValue = 0;
                    ResetProgressBar_XoaPhanBo(0, 100);

                    secondElapsed = 0;
                    timeElapsed.Tick += new EventHandler(timeElapsed_Tick);
                    timeElapsed.Interval = 1000;
                    timeElapsed.Enabled = true;

                    btnDeletePhanBo.Enabled = false;
                    contextMenuStripDaPhanBo.Enabled = false;
                    btnPhanBo.Enabled = false;
                    contextMenuStripPhanBo.Enabled = false;

                    //btnCancel.Enabled = true;

                    bgwProgressDaPhanBo.RunWorkerAsync(objDoWork);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex); SetLogText(ex.Message);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                bgwProgressDaPhanBo.CancelAsync();

                timeElapsed.Enabled = false;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); SetLogText(ex.Message); }
        }

        private void bgwProgress_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bgw = sender as BackgroundWorker;
            if (bgw != null && bgw.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            string val = e.Argument.ToString();

            if (val != "" && val == EWorkType.XoaPhanBo.ToString())
                XoaPhanBo(bgw, e);
            else if (val != "" && val == EWorkType.PhanBo.ToString())
                PhanBo(bgw, e);
        }

        private void bgwProgress_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage <= GetProcessBarMaxValue_XoaPhanBo())
            {
                SetProgressBar_XoaPhanBo(e.ProgressPercentage);
            }
        }

        /// <summary>
        /// Thực hiện sau khi load dữ liệu làm thêm theo quyền chấm công
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bgwProgress_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Logger.LocalLogger.Instance().WriteMessage(e.Error);
                SetLogText(e.Error.Message);
                MessageBox.Show(e.Error.Message);
            }

            // Check to see if the background process was cancelled.
            if (e.Cancelled)
            {

            }
            else
            {
                //if (objDoWork == EWorkType.XoaPhanBo.ToString()) { }
            }

            btnDeletePhanBo.Enabled = true;
            contextMenuStripDaPhanBo.Enabled = true;
            btnPhanBo.Enabled = true;
            contextMenuStripPhanBo.Enabled = true;
            //btnCancel.Enabled = false;

            timeElapsed.Enabled = false;

            //txtLog.ScrollToCaret();

            this.Cursor = Cursors.Default;
        }

        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (bgwProgressDaPhanBo.IsBusy) bgwProgressDaPhanBo.CancelAsync();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); SetLogText(ex.Message); }
            timeElapsed.Enabled = false;
        }

        #endregion

        private void uiButton4_Click(object sender, EventArgs e)
        {
            SaveFileDialog d = new SaveFileDialog();
            d.RestoreDirectory = true;
            d.InitialDirectory = Application.StartupPath;
            d.Filter = "Excel files| *.xls";
            d.ShowDialog();
            if (d.FileName != "")
            {
                Stream s = d.OpenFile();
                gridEXExporter1.Export(s);
                s.Close();
                if (showMsg("MSG_MAL08", true) == "Yes")
                //if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(d.FileName);
                }
            }
        }

        private void toolStripMenuItemBangPhanBo_Click(object sender, EventArgs e)
        {
            long id = Convert.ToInt64(dgToKhaiDaPhanBo.GetRow().Cells["ID"].Value);
            string maLoaiHinh = dgToKhaiDaPhanBo.GetRow().Cells["MaLoaiHinh"].Value.ToString();

            XemBangPhanBo(id, maLoaiHinh);
        }

        private void XemBangPhanBo(long id, string maLoaiHinh)
        {
            try
            {
                if (maLoaiHinh.StartsWith("E")) maLoaiHinh = "XV" + maLoaiHinh;
                if (maLoaiHinh.Contains("B") && !maLoaiHinh.Contains("XV")) maLoaiHinh = "XV" + maLoaiHinh;
                if ((maLoaiHinh.Contains("XGC") || maLoaiHinh.Contains("XV")) && maLoaiHinh.Trim() != "XVE54" && maLoaiHinh.Trim() != "XGC18" && maLoaiHinh.Trim() != "XGC19" && maLoaiHinh.Trim() != "XGC20")
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();

                    try
                    {
                        TKMD.ID = id;
                    }
                    catch
                    {
                        showMsg("MSG_240208");
                        //ShowMessage("ID của tờ khai không hợp lệ.", false);
                        return;
                    }
                    if (!TKMD.Load())
                    {
                        showMsg("MSG_240209");
                        //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                        return;
                    }
                    if (TKMD.TrangThaiPhanBo != 1)
                    {
                        showMsg("MSG_WRN27");
                        //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                        return;
                    }
                    TKMD.LoadHMDCollection();
                    ViewPhanBoForm f = new ViewPhanBoForm();
                    //try
                    //{
                    //    Application.OpenForms[0].Hide();

                    //    SOFTECH.ECS.TQDT.GC.Splash.SplashFrm.ShowSplash("Đang tổng hợp dữ liệu phân bổ, vui lòng đợi trong giây lát...");
                    f.TKMD = TKMD;
                    f.HD = this.HD;
                    f.Show();
                    //                     }
                    //                     catch (System.Exception ex)
                    //                     {
                    // 
                    //                     }
                    //                     finally
                    //                     {
                    //                         SOFTECH.ECS.TQDT.GC.Splash.SplashFrm.CloseSplash();
                    //                         Application.OpenForms[0].Show();
                    /* f.Activate();*/
                    /*     }*/

                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();

                    try
                    {
                        TKCT.ID = id;
                    }
                    catch
                    {
                        showMsg("MSG_240208");
                        //ShowMessage("ID của tờ khai không hợp lệ.", false);
                        return;
                    }
                    TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);

                    if (TKCT == null)
                    {
                        showMsg("MSG_240209");
                        //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                        return;
                    }
                    if (!TKCT.CheckPhanBo())
                    {
                        showMsg("MSG_WRN27");
                        //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                        return;
                    }
                    TKCT.LoadHCTCollection();
                    ViewPhanBoTKCTForm f = new ViewPhanBoTKCTForm();
                    //try
                    //                     {
                    //                         SOFTECH.ECS.TQDT.GC.Splash.SplashFrm.ShowSplash("Đang tổng hợp dữ liệu phân bổ, vui lòng đợi...");
                    f.TKCT = TKCT;
                    f.HD = this.HD;
                    f.Show();
                    //                     }
                    //                     catch (System.Exception ex)
                    //                     {
                    // 
                    //                     }
                    //                     finally
                    //                     {
                    //                         SOFTECH.ECS.TQDT.GC.Splash.SplashFrm.CloseSplash();
                    //                     }

                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void toolStripMenuItemBangNPLCungUng_Click(object sender, EventArgs e)
        {
            long id = Convert.ToInt64(dgToKhaiDaPhanBo.GetRow().Cells["ID"].Value);
            string maLoaiHinh = dgToKhaiDaPhanBo.GetRow().Cells["MaLoaiHinh"].Value.ToString();
            if (maLoaiHinh.StartsWith("E") || maLoaiHinh.StartsWith("B")) maLoaiHinh = "XV" + maLoaiHinh;
            XemBangNPLCungUng(id, maLoaiHinh);
        }

        private void XemBangNPLCungUng(long id, string maLoaiHinh)
        {
            try
            {
                if ((maLoaiHinh.Contains("XGC") || maLoaiHinh.Contains("XV")) && maLoaiHinh.Trim() != "XVE54" && maLoaiHinh.Trim() != "XGC18" && maLoaiHinh.Trim() != "XGC19" && maLoaiHinh.Trim() != "XGC20")
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    try
                    {
                        TKMD.ID = id;
                    }
                    catch
                    {
                        showMsg("MSG_240208");
                        //ShowMessage("ID của tờ khai không hợp lệ.", false);
                        return;
                    }
                    if (!TKMD.Load())
                    {
                        showMsg("MSG_240209");
                        //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                        return;
                    }
                    if (TKMD.TrangThaiPhanBo != 1)
                    {
                        showMsg("MSG_WRN27");
                        //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                        return;
                    }
                    TKMD.LoadHMDCollection();
                    ViewNPLCungUngForm f = new ViewNPLCungUngForm();
                    f.TKMD = TKMD;
                    f.HD = this.HD;
                    f.Show();
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    try
                    {
                        TKCT.ID = id;
                    }
                    catch
                    {
                        showMsg("MSG_240208");
                        //ShowMessage("ID của tờ khai không hợp lệ.", false);
                        return;
                    }
                    TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                    if (TKCT == null)
                    {
                        showMsg("MSG_240209");
                        //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                        return;
                    }
                    if (!TKCT.CheckPhanBo())
                    {
                        showMsg("MSG_WRN27");
                        //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                        return;
                    }
                    TKCT.LoadHCTCollection();
                    ViewNPLCungUngTKCTForm f = new ViewNPLCungUngTKCTForm();
                    f.TKCT = TKCT;
                    f.HD = this.HD;
                    f.Show();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void toolStripMenuItemPhanBo_Click(object sender, EventArgs e)
        {
            objDoWork = EWorkType.PhanBo.ToString();
            btnBeginProcess_Click(sender, e);
        }

        private void btnXuatExcelNPLTon_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.RestoreDirectory = true;
            sfNPL.InitialDirectory = Application.StartupPath;
            sfNPL.Filter = "Excel files| *.xls";
            sfNPL.ShowDialog();
            if (sfNPL.FileName != "")
            {
                Stream str = sfNPL.OpenFile();
                gridEXExporterNPL.Export(str);
                str.Close();
                if (showMsg("MSG_MAL08", true) == "Yes")
                //if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(sfNPL.FileName);
                }
            }
        }

        private void btnInNPLTon_Click(object sender, EventArgs e)
        {
            gridEXPrintDocumentNPL.Print();
        }

        private void btnXuatExcelNPLCungUng_Click(object sender, EventArgs e)
        {

            long id = 0;
            try
            {
                id = Convert.ToInt64(cbbToKhai.Text);
            }
            catch
            {
                ShowMessage("Bạn hãy chọn tờ khai.", false);
                return;
            }
            string maLoaiHinh = Convert.ToString(((DataRowView)cbbToKhai.SelectedItem).Row["MaLoaiHinh"]);
            if ((maLoaiHinh.Contains("XGC") || maLoaiHinh.Contains("XV")) && maLoaiHinh.Trim() != "XVE54" && maLoaiHinh.Trim() != "XGC18" && maLoaiHinh.Trim() != "XGC19" && maLoaiHinh.Trim() != "XGC20")
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                if (cbbToKhai.Text == "")
                {
                    showMsg("MSG_WRN29");
                    //ShowMessage("Bạn chưa chọn tờ khai xuất.", false);
                    return;
                }
                try
                {
                    TKMD.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKMD.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (TKMD.TrangThaiPhanBo != 1)
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKMD.LoadHMDCollection();
                XuatExcelNPLCungUng(TKMD);
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                if (cbbToKhai.Text == "")
                {
                    showMsg("MSG_WRN29");
                    //ShowMessage("Bạn chưa chọn tờ khai xuất.", false);
                    return;
                }
                try
                {
                    TKCT.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                if (TKCT == null)
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (!TKCT.CheckPhanBo())
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKCT.LoadHCTCollection();
                XuatExcelNPLCungUng(TKCT);
            }
        }
        private void XuatExcelNPLCungUng(ToKhaiMauDich TKMD)
        {
            Workbook wb = new Workbook();
            Company.GC.BLL.GC.DinhMuc DMtemp = new Company.GC.BLL.GC.DinhMuc();
            long soSheet1 = 0;
            foreach (HangMauDich HMD in TKMD.HMDCollection)
            {
                DataTable dt = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoCuaSanPhamGroupByMaNPL(TKMD.ID, this.HD.ID, HMD.MaPhu).Tables[0];
                if (dt.Rows.Count == 0) continue;
                Worksheet ws = wb.Worksheets.Add(HMD.MaPhu);
                soSheet1++;
                if (HMD.MaPhu == "ULHF583")
                {

                }

                WorksheetRowCollection wsrc = ws.Rows;
                WorksheetRow wsr0 = ws.Rows[0];
                ws.Columns[0].Width = 2500;
                ws.Columns[1].Width = 2500;
                ws.Columns[2].Width = 2500;
                ws.Columns[3].Width = 2500;
                ws.Columns[3].Width = 2500;
                wsr0.Cells[0].Value = "Mã SP";
                wsr0.Cells[1].Value = "Mã NPL";
                wsr0.Cells[2].Value = "Định mức";
                wsr0.Cells[3].Value = "TLHH";
                wsr0.Cells[4].Value = "Đơn giá";

                string maNPL = "";
                decimal LuongPhanBo = 0;
                decimal TongTriGia = 0;
                int t = 1;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    if (maNPL.Trim().ToUpper() != dr["MaNPL"].ToString())
                    {

                        LuongPhanBo = 0;
                        TongTriGia = 0;
                        WorksheetRow wsr = ws.Rows[t];
                        wsr.Cells[0].Value = DMtemp.MaSanPham = HMD.MaPhu;
                        wsr.Cells[1].Value = DMtemp.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                        DMtemp.HopDong_ID = this.HD.ID;
                        DMtemp.Load();
                        LuongPhanBo += Convert.ToDecimal(dr["LuongPhanBo"]);
                        wsr.Cells[2].Value = DMtemp.DinhMucSuDung;
                        wsr.Cells[3].Value = DMtemp.TyLeHaoHut;
                        if (dr["MaLoaiHinhNhap"].ToString().Contains("NSX"))
                        {
                            ToKhaiMauDich TKMDNhap = new ToKhaiMauDich();
                            TKMDNhap.SoToKhai = Convert.ToInt32(dr["SoToKhaiNhap"]);
                            TKMDNhap.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinhNhap"]);
                            TKMDNhap.NgayDangKy = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                            wsr.Cells[4].Value = TKMDNhap.SelectDonGia(dr["MaNPL"].ToString());
                            TongTriGia += LuongPhanBo * Convert.ToDecimal(wsr.Cells[4].Value);
                        }
                        else
                        {
                            wsr.Cells[4].Value = 0;
                        }
                        maNPL = dr["MaNPL"].ToString();
                        t++;
                    }
                    else
                    {
                        WorksheetRow wsr = ws.Rows[t - 1];
                        LuongPhanBo += Convert.ToDecimal(dr["LuongPhanBo"]);
                        wsr.Cells[2].Value = DMtemp.DinhMucSuDung;
                        if (dr["MaLoaiHinhNhap"].ToString().Contains("NSX"))
                        {
                            ToKhaiMauDich TKMDNhap = new ToKhaiMauDich();
                            TKMDNhap.SoToKhai = Convert.ToInt32(dr["SoToKhaiNhap"]);
                            TKMDNhap.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinhNhap"]);
                            TKMDNhap.NgayDangKy = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                            TongTriGia += Convert.ToDecimal(dr["LuongPhanBo"]) * TKMDNhap.SelectDonGia(dr["MaNPL"].ToString());
                            wsr.Cells[4].Value = TongTriGia / LuongPhanBo;
                        }
                        else
                        {
                            if (wsr.Cells[4].Value == null || wsr.Cells[4].Value.ToString() == "")
                                wsr.Cells[4].Value = 0;
                        }

                    }
                }


            }
            string fileName = "";

            DialogResult rs = saveFileDialog1.ShowDialog();
            if (rs == DialogResult.OK && soSheet1 > 0)
            {
                fileName = saveFileDialog1.FileName;
                wb.Save(fileName);
                if (showMsg("MSG_MAL08", true) == "Yes")
                {
                    Process.Start(fileName);
                }
            }

            //wb = Workbook.Load(fileName);

        }
        private void XuatExcelNPLCungUng(ToKhaiChuyenTiep TKCT)
        {
            Workbook wb = new Workbook();
            Company.GC.BLL.GC.DinhMuc DMtemp = new Company.GC.BLL.GC.DinhMuc();

            foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
            {
                DataTable dt = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoCuaSanPhamGroupByMaNPLTKCT(TKCT.ID, this.HD.ID, HCT.MaHang).Tables[0];
                if (dt.Rows.Count == 0) continue;
                Worksheet ws = wb.Worksheets.Add(HCT.MaHang);

                WorksheetRowCollection wsrc = ws.Rows;
                WorksheetRow wsr0 = ws.Rows[0];
                ws.Columns[0].Width = 2500;
                ws.Columns[1].Width = 2500;
                ws.Columns[2].Width = 2500;
                ws.Columns[3].Width = 2500;
                ws.Columns[3].Width = 2500;
                wsr0.Cells[0].Value = "Mã SP";
                wsr0.Cells[1].Value = "Mã NPL";
                wsr0.Cells[2].Value = "Định mức";
                wsr0.Cells[3].Value = "TLHH";
                wsr0.Cells[4].Value = "Đơn giá";

                string maNPL = "";
                decimal LuongPhanBo = 0;
                decimal TongTriGia = 0;
                int t = 1;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    if (maNPL.Trim().ToUpper() != dr["MaNPL"].ToString())
                    {
                        LuongPhanBo = 0;
                        TongTriGia = 0;
                        WorksheetRow wsr = ws.Rows[t];
                        wsr.Cells[0].Value = DMtemp.MaSanPham = HCT.MaHang;
                        wsr.Cells[1].Value = DMtemp.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                        DMtemp.HopDong_ID = this.HD.ID;
                        DMtemp.Load();
                        LuongPhanBo += Convert.ToDecimal(dr["LuongPhanBo"]);
                        wsr.Cells[2].Value = DMtemp.DinhMucSuDung;
                        wsr.Cells[3].Value = DMtemp.TyLeHaoHut;
                        if (dr["MaLoaiHinhNhap"].ToString().Contains("NSX"))
                        {
                            ToKhaiMauDich TKMDNhap = new ToKhaiMauDich();
                            TKMDNhap.SoToKhai = Convert.ToInt32(dr["SoToKhaiNhap"]);
                            TKMDNhap.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinhNhap"]);
                            TKMDNhap.NgayDangKy = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                            wsr.Cells[4].Value = TKMDNhap.SelectDonGia(dr["MaNPL"].ToString());
                            TongTriGia += LuongPhanBo * Convert.ToDecimal(wsr.Cells[4].Value);
                        }
                        else
                        {
                            wsr.Cells[4].Value = 0;
                        }
                        maNPL = dr["MaNPL"].ToString();
                        t++;
                    }
                    else
                    {
                        WorksheetRow wsr = ws.Rows[t - 1];
                        wsr.Cells[2].Value = DMtemp.DinhMucSuDung;
                        LuongPhanBo += Convert.ToDecimal(dr["LuongPhanBo"]);
                        if (dr["MaLoaiHinhNhap"].ToString().Contains("NSX"))
                        {
                            ToKhaiMauDich TKMDNhap = new ToKhaiMauDich();
                            TKMDNhap.SoToKhai = Convert.ToInt32(dr["SoToKhaiNhap"]);
                            TKMDNhap.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinhNhap"]);
                            TKMDNhap.NgayDangKy = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                            TongTriGia += Convert.ToDecimal(dr["LuongPhanBo"]) * TKMDNhap.SelectDonGia(dr["MaNPL"].ToString());
                            wsr.Cells[4].Value = TongTriGia / LuongPhanBo;
                        }
                        else
                        {
                            if (wsr.Cells[4].Value == null || wsr.Cells[4].Value.ToString() == "")
                                wsr.Cells[4].Value = 0;
                        }

                    }
                }


            }
            string fileName = "";

            DialogResult rs = saveFileDialog1.ShowDialog();
            if (rs == DialogResult.OK)
            {
                fileName = saveFileDialog1.FileName;
                wb.Save(fileName);
                if (showMsg("MSG_MAL08", true) == "Yes")
                {
                    Process.Start(fileName);
                }
            }

            //wb = Workbook.Load(fileName);

        }
        private void xuatExcelNPLCungUngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgToKhaiDaPhanBo.GetRow();
            string maLoaiHinh = row.Cells["MaLoaiHinh"].Value.ToString();
            if (maLoaiHinh.StartsWith("E")) maLoaiHinh = "XV" + maLoaiHinh;
            long id = Convert.ToInt64(row.Cells["ID"].Value);
            if (maLoaiHinh.Contains("XGC") || maLoaiHinh.Contains("XV"))
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();

                try
                {
                    TKMD.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                if (!TKMD.Load())
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (TKMD.TrangThaiPhanBo != 1)
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKMD.LoadHMDCollection();
                XuatExcelNPLCungUng(TKMD);
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();

                try
                {
                    TKCT.ID = id;
                }
                catch
                {
                    showMsg("MSG_240208");
                    //ShowMessage("ID của tờ khai không hợp lệ.", false);
                    return;
                }
                TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                if (TKCT == null)
                {
                    showMsg("MSG_240209");
                    //ShowMessage("Tờ khai không tồn tại trong hệ thống.", false);
                    return;
                }
                if (!TKCT.CheckPhanBo())
                {
                    showMsg("MSG_WRN27");
                    //ShowMessage("Tờ khai xuất này chưa được phân bổ.", false);
                    return;
                }
                TKCT.LoadHCTCollection();
                XuatExcelNPLCungUng(TKCT);
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void btnChuyenTonSXXK_Click(object sender, EventArgs e)
        {
            ChuyenTonSXXK f = new ChuyenTonSXXK();
            f.HD = this.HD;
            f.ShowDialog();
            BindDataToKhaiNPLTon();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            objDoWork = EWorkType.XoaPhanBo.ToString();
            btnBeginProcess_Click(sender, e);
        }

        private void uiTabSanPham_SelectedTabChanged_1(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {

        }

        private void btnLayDuLieuCungUng_Click(object sender, EventArgs e)
        {
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                TKMD.LayDuLieuCungUng();
                ShowMessage("Cập nhật thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (e.Row.Table.Columns.Contains("ChenhLech") && Convert.ToDecimal(e.Row.Cells["ChenhLech"].Text) > 0)
                    {
                        e.Row.RowStyle = new GridEXFormatStyle();
                        e.Row.RowStyle.ForeColor = Color.Red;
                    }
                    else
                    {
                        e.Row.RowStyle = new GridEXFormatStyle();
                        e.Row.RowStyle.BackColor = Color.White;
                        e.Row.RowStyle.ForeColor = Color.Black;
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnChinhDL_Click(object sender, EventArgs e)
        {
            try
            {
                NPLTon.XoaDuLieuSai(HD.ID);
                MessageBox.Show("Chỉnh sửa thành công","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            ThongkeHangTonForm_Load(sender,e);
        }

    }
}