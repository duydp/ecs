﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;

namespace Company.Interface.KDT.GC
{
    public partial class LoaiSanPhamGCEditForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public Company.GC.BLL.KDT.GC.NhomSanPham nhom = new Company.GC.BLL.KDT.GC.NhomSanPham();
        public HopDong HD = new HopDong();
        public LogHistory Log = new LogHistory();
        public String Caption;
        public bool IsChange;
        public LoaiSanPhamGCEditForm()
        {
            InitializeComponent();
            cbTen.ValueChanged -= new Company.Interface.Controls.LoaiSanPhamGCControl.ValueChangedEventHandler(txt_TextChanged);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BindData()
        {
            try
            {
                dgNhomSanPham.Refresh();
                dgNhomSanPham.DataSource = HD.NhomSPCollection;
                dgNhomSanPham.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || HD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void SetData()
        {
            try
            {
                cbTen.ValueChanged -= new Company.Interface.Controls.LoaiSanPhamGCControl.ValueChangedEventHandler(txt_TextChanged);
                cbTen.Ma = nhom.MaSanPham.Trim();
                cbTen.ValueChanged += new Company.Interface.Controls.LoaiSanPhamGCControl.ValueChangedEventHandler(txt_TextChanged);

                txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong.Text = nhom.SoLuong.ToString();
                txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

                txtGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtGia.Text = nhom.GiaGiaCong.ToString();
                txtGia.TextChanged += new EventHandler(txt_TextChanged);

                txtTriGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGia.Value = nhom.TriGia;
                txtTriGia.TextChanged += new EventHandler(txt_TextChanged);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LoaiSanPhamGCEditForm_Load(object sender, EventArgs e)
        {
            try
            {
                Caption = this.Text;
                SetData();
                //Edit by KHANHHN - 07/03/2012
                if (this.OpenType == OpenFormType.View)
                {
                    dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    btnAdd.Enabled = false;
                    btnXoa.Enabled = false;
                }
                txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

                txtGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtGia.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                txtGia.TextChanged += new EventHandler(txt_TextChanged);

                txtTriGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGia.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                txtTriGia.TextChanged += new EventHandler(txt_TextChanged);

                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void reset()
        {
            try
            {
                txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong.Text = "0.000";
                txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

                txtGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtGia.Text = "0.000";
                txtGia.TextChanged += new EventHandler(txt_TextChanged);

                txtTriGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGia.Text = "0.000";
                txtTriGia.TextChanged += new EventHandler(txt_TextChanged);

                nhom = new Company.GC.BLL.KDT.GC.NhomSanPham();
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateZero(txtTriGia, errorProvider, "Trị giá không cho phép nhỏ hơn hoặc bằng không");
                isValid &= ValidateControl.ValidateNull(txtSoLuong, errorProvider, "Số lượng không cho phép nhỏ hơn hoặc bằng không");
                isValid &= ValidateControl.ValidateNull(txtGia, errorProvider, "Gía gia công không cho phép nhỏ hơn hoặc bằng không");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                    if (!ValidateForm(true))
                        return;
                    checkExitsNhomSanPhamAndSTTHang();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void checkExitsNhomSanPhamAndSTTHang()
        {
            try
            {
                HD.NhomSPCollection.Remove(nhom);
                foreach (Company.GC.BLL.KDT.GC.NhomSanPham nhomSP in HD.NhomSPCollection)
                {
                    if (nhomSP.MaSanPham == cbTen.Ma.ToString())
                    {
                        showMsg("MSG_2702055");
                        return;
                    }
                }
                nhom.SoLuong = Convert.ToDecimal(txtSoLuong.Value);
                nhom.GiaGiaCong = Convert.ToDecimal(txtGia.Value);
                nhom.TenSanPham = cbTen.Ten.Trim();
                nhom.MaSanPham = cbTen.Ma.Trim();
                nhom.TriGia = Convert.ToDouble(txtTriGia.Value);
                nhom.HopDong_ID = HD.ID;
                HD.NhomSPCollection.Add(nhom);
                reset();
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void dgNhomSanPham_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)e.Row.DataRow;
                SetData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgNhomSanPham_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.Edit)
                {
                    string st = "";
                    foreach (GridEXSelectedItem row in dgNhomSanPham.SelectedItems)
                    {
                        Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                        foreach (SanPham sp in HD.SPCollection)
                        {
                            if (sp.NhomSanPham_ID == nhom.MaSanPham.Trim())
                            {
                                if (st.IndexOf(sp.Ma) < 0)
                                    st += sp.Ma + ";";
                            }
                        }
                    }
                    bool ok = false;
                    if (st == "")
                    {
                        if (showMsg("MSG_DEL01", true) == "Yes")
                        //if (ShowMessage("Bạn có muốn xóa nhóm sản phẩm này không?", true) == "Yes")
                        {
                            ok = true;
                        }
                        else
                        {
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        if (showMsg("MSG_240239", st, true) == "Yes")
                        //if (MLMessages("Nhóm sản phẩm này có liên quan đến các sản phẩm " + st + " Bạn có muốn xoá không", "MSG_DEL05", st, true) == "Yes")
                        {
                            ok = true;
                        }
                        else
                        {
                            e.Cancel = true;
                        }
                    }
                    if (ok)
                    {
                        foreach (GridEXSelectedItem row in dgNhomSanPham.SelectedItems)
                        {
                            Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                            if (HD.SPCollection.Count > 0)
                            {
                                SanPham sp = new SanPham();
                                sp.HopDong_ID = HD.ID;
                                sp.DeleteByNhomSanPhamTransaction(null, nhom.MaSanPham);
                                Company.GC.BLL.GC.GC_NhomSanPham.DeleteGC_NhomSanPham(HD.ID, nhom.MaSanPham);
                                SanPhamCollection SPCollectionTMP = new SanPhamCollection();
                                foreach (SanPham spDelete in HD.SPCollection)
                                {
                                    if (spDelete.NhomSanPham_ID == nhom.MaSanPham.Trim())
                                    {
                                        SPCollectionTMP.Add(spDelete);
                                    }
                                }
                                foreach (SanPham spDelete in SPCollectionTMP)
                                {
                                    foreach (SanPham SPHopDong in HD.SPCollection)
                                    {
                                        if (SPHopDong.Ma == spDelete.Ma)
                                        {
                                            Company.GC.BLL.GC.GC_SanPham.DeleteGC_SanPham(HD.ID, SPHopDong.Ma);
                                            KDT_GC_SanPham.DeleteKDT_GC_SanPham(HD.ID, SPHopDong.Ma);
                                            HD.SPCollection.Remove(SPHopDong);
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                nhom.Delete();
                                Company.GC.BLL.GC.GC_NhomSanPham.DeleteGC_NhomSanPham(HD.ID, nhom.MaSanPham.Trim());
                            }
                            HD.NhomSPCollection.Remove(nhom);
                            this.SetChange(true);
                        }
                        BindData();
                    }
                    else e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }            
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    string st = "";
                    GridEXSelectedItemCollection items = dgNhomSanPham.SelectedItems;
                    if (items.Count < 0) return;
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                        foreach (SanPham sp in HD.SPCollection)
                        {
                            if (sp.NhomSanPham_ID == nhom.MaSanPham.Trim())
                            {
                                if (st.IndexOf(sp.Ma) < 0)
                                    st += sp.Ma + ";";
                            }
                        }
                    }
                    bool ok = false;
                    if (st == "")
                    {
                        if (showMsg("MSG_DEL01", true) == "Yes")
                        //if (ShowMessage("Bạn có muốn xóa nhóm sản phẩm này không?", true) == "Yes")
                        {
                            ok = true;
                        }
                    }
                    else
                    {
                        if (showMsg("MSG_240239", st, true) == "Yes")
                        //if (MLMessages("Nhóm sản phẩm này có liên quan đến các sản phẩm " + st + " Bạn có muốn xoá không", "MSG_DEL05", st, true) == "Yes")
                        {
                            ok = true;
                        }

                    }
                    if (ok)
                    {
                        List<Company.GC.BLL.KDT.GC.NhomSanPham> NhomSPCollection = new List<Company.GC.BLL.KDT.GC.NhomSanPham>();
                        foreach (GridEXSelectedItem row in items)
                        {
                            Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                            NhomSPCollection.Add(nhom);
                            if (HD.SPCollection.Count > 0)
                            {
                                SanPham sp = new SanPham();
                                sp.HopDong_ID = HD.ID;
                                sp.DeleteByNhomSanPhamTransaction(null, nhom.MaSanPham);
                                Company.GC.BLL.GC.GC_NhomSanPham.DeleteGC_NhomSanPham(HD.ID, nhom.MaSanPham);
                                SanPhamCollection SPCollectionTMP = new SanPhamCollection();
                                foreach (SanPham spDelete in HD.SPCollection)
                                {
                                    if (spDelete.NhomSanPham_ID == nhom.MaSanPham.Trim())
                                    {
                                        SPCollectionTMP.Add(spDelete);
                                    }
                                }
                                foreach (SanPham spDelete in SPCollectionTMP)
                                {
                                    foreach (SanPham SPHopDong in HD.SPCollection)
                                    {
                                        if (SPHopDong.Ma == spDelete.Ma)
                                        {
                                            Company.GC.BLL.GC.GC_SanPham.DeleteGC_SanPham(HD.ID, SPHopDong.Ma);
                                            KDT_GC_SanPham.DeleteKDT_GC_SanPham(HD.ID, SPHopDong.Ma);
                                            HD.SPCollection.Remove(SPHopDong);
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                nhom.Delete();
                                Company.GC.BLL.GC.GC_NhomSanPham.DeleteGC_NhomSanPham(HD.ID, nhom.MaSanPham);
                            }
                        }
                        if (NhomSPCollection.Count > 0)
                        {
                            foreach (Company.GC.BLL.KDT.GC.NhomSanPham nhomSP in NhomSPCollection)
                            {
                                foreach (Company.GC.BLL.KDT.GC.NhomSanPham nhomSPHopDong in HD.NhomSPCollection)
                                {
                                    if (nhomSPHopDong.MaSanPham == nhomSP.MaSanPham.Trim())
                                    {
                                        HD.NhomSPCollection.Remove(nhomSPHopDong);
                                        break;
                                    }
                                }

                            }
                        }
                        BindData();
                        this.SetChange(true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }            
        }

        private void txtSoLuong_TextChanged(object sender, EventArgs e)
        {
            try
            {
                error.SetError(txtSoLuong, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void txtGia_TextChanged(object sender, EventArgs e)
        {
            try
            {
                error.SetError(txtGia, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgNhomSanPham_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType==OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgNhomSanPham.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)i.GetRow().DataRow;
                            SetData();
                        }
                    }   
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}