﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class BaoCaoQuyetToanManagerForm : BaseForm
    {
        public string where = "";
        public BaoCaoQuyetToanManagerForm()
        {
            InitializeComponent();
        }

        private void BaoCaoQuyetToanManagerForm_Load(object sender, EventArgs e)
        {
            cbStatus.SelectedValue = 1;
            btnSearch_Click(null, null);
            chkDate.Checked = false;
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ","Doanh nghiệp có muốn xóa dòng hàng này không ?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New();
                        goodItem = (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New)i.GetRow().DataRow;
                        goodItem.GoodItemsCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New.SelectCollectionBy_GoodItem_ID(goodItem.ID);
                        if (!String.IsNullOrEmpty(goodItem.GuidStr))
                        {
                            ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ","Báo cáo quyết toán này đã gửi lên HQ nên không được xóa !", false);
                        }
                        else
                        {
                            goodItem.DeleteFull();
                        }
                    }
                }
                ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", "Xóa thành công", false);
                btnSearch_Click(null,null);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1=1";
                if (!String.IsNullOrEmpty(txtSoTiepNhan.Text))
                    where += " AND SoTN = " + txtSoTiepNhan.Text;
                if (!String.IsNullOrEmpty(cbStatus.SelectedValue.ToString()))
                    where += " AND TrangThaiXuLy =" + cbStatus.SelectedValue;
                if (!String.IsNullOrEmpty(cbbType.SelectedValue.ToString()))
                    where += " AND LoaiBaoCao =" + cbbType.SelectedValue.ToString();
                if (chkDate.Checked==true)
                {
                    where += " AND NgayBatDauBC =" +clcNgayBatDauBC.Value;
                    where += " AND NgayKetThucBC =" + clcNgayKetThucBC.Value;
                }
                if (!String.IsNullOrEmpty(ctrCoQuanHaiQuan.Code))
                    where += " AND MaHQ = '" + ctrCoQuanHaiQuan.Code + "'";
                return where;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New.SelectCollectionDynamic(GetSearchWhere(), "ID");
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void chkDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDate.Checked==true)
            {
                grbDate.Enabled = true;
            }
            else
            {
                grbDate.Enabled = false;
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New();
                goodItem = (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New)e.Row.DataRow;
                BaoCaoQuyetToanForm f = new BaoCaoQuyetToanForm();
                f.IsActive = true;
                f.goodItem = goodItem;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                int LoaiBaoCao = Convert.ToInt32(e.Row.Cells["LoaiBaoCao"].Value);
                if (LoaiBaoCao == 1)
                    e.Row.Cells["LoaiBaoCao"].Text = " Nguyên liệu, vật liệu nhập khẩu ";
                else
                    e.Row.Cells["LoaiBaoCao"].Text = "Thành phẩm được sản xuất từ nguồn nhập khẩu";

                string TrangThai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (TrangThai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;
                    case "-3":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                        break;
                    case "10":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                        break;
                    case "4":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo Sửa";
                        break;
                    case "-2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt khai báo sửa";
                        break;
                    case "-4":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo Hủy";
                        break;
                    case "11":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt Khai báo hủy";
                        break;
                    case "5":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đang sửa";
                        break;
                }
            }
        }

        private void uiContextMenu1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdUpdateStatus":
                    UpdateStatus();
                    break;
            } 
        }
        private void UpdateStatus()
        {
            try
            {
                WSForm2 login = new WSForm2();
                login.ShowDialog(this);
                if (WSForm2.IsSuccess == true)
                {
                    if (dgList.SelectedItems.Count > 0)
                    {
                        List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New> BCQTCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New>();
                        foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                        {
                            BCQTCollection.Add((KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New)grItem.GetRow().DataRow);
                        }
                        for (int i = 0; i < BCQTCollection.Count; i++)
                        {
                            Company.Interface.KDT.GC.UpdateStatusForm f = new Company.Interface.KDT.GC.UpdateStatusForm();
                            f.BCQT = BCQTCollection[i];
                            f.formType = "BCQT";
                            f.ShowDialog(this);
                        }
                        this.btnSearch_Click(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
