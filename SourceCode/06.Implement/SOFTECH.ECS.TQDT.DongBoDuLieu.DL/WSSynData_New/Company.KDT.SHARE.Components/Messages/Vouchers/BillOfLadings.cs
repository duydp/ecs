﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class BillOfLadings
    {
        /// <summary>
        /// Thông tin về vận đơn
        /// </summary>
       [XmlElement("BillOfLading")]
       public List<Company.KDT.SHARE.Components.BillOfLading> BillOfLading { get; set; }
    }
}
